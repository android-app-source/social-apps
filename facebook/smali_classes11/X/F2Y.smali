.class public final LX/F2Y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/F2r;


# direct methods
.method public constructor <init>(LX/F2r;)V
    .locals 0

    .prologue
    .line 2193176
    iput-object p1, p0, LX/F2Y;->a:LX/F2r;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x5bdf0f25

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2193177
    iget-object v1, p0, LX/F2Y;->a:LX/F2r;

    iget-object v1, v1, LX/F2r;->o:LX/F3E;

    .line 2193178
    iget-object v3, v1, LX/F3E;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;

    iget-object v3, v3, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->b:LX/F2N;

    iget-object v4, v1, LX/F3E;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;

    iget-object v4, v4, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->j:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    .line 2193179
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2193180
    const-string p0, "group_edit_privacy_data"

    invoke-static {v5, p0, v4}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2193181
    invoke-static {v3}, LX/F2N;->b(LX/F2N;)Landroid/content/Intent;

    move-result-object p0

    .line 2193182
    invoke-virtual {p0, v5}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2193183
    const-string v5, "target_fragment"

    sget-object p1, LX/0cQ;->GROUP_EDIT_PRIVACY_FRAGMENT:LX/0cQ;

    invoke-virtual {p1}, LX/0cQ;->ordinal()I

    move-result p1

    invoke-virtual {p0, v5, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2193184
    move-object v3, p0

    .line 2193185
    iget-object v4, v1, LX/F3E;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;

    iget-object v4, v4, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v5, v1, LX/F3E;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;

    invoke-virtual {v5}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2193186
    const v1, -0x73f6ac37

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
