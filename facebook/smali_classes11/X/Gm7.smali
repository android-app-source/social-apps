.class public LX/Gm7;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field public a:LX/Gm9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:I

.field public c:LX/GmD;

.field public d:Lcom/facebook/growth/contactinviter/ContactInviterFragment;

.field public e:Lcom/facebook/widget/text/BetterTextView;

.field public f:Lcom/facebook/widget/text/BetterTextView;

.field public g:Lcom/facebook/fig/button/FigButton;

.field public h:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2391582
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 2391583
    const-class v0, LX/Gm7;

    invoke-static {v0, p0}, LX/Gm7;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2391584
    const v0, 0x7f030366

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2391585
    const v0, 0x7f0d02c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Gm7;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2391586
    const v0, 0x7f0d0945

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Gm7;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2391587
    const v0, 0x7f0d0b25

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    iput-object v0, p0, LX/Gm7;->g:Lcom/facebook/fig/button/FigButton;

    .line 2391588
    const v0, 0x7f0d0b26

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/Gm7;->h:Landroid/widget/ProgressBar;

    .line 2391589
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Gm7;

    invoke-static {p0}, LX/Gm9;->a(LX/0QB;)LX/Gm9;

    move-result-object p0

    check-cast p0, LX/Gm9;

    iput-object p0, p1, LX/Gm7;->a:LX/Gm9;

    return-void
.end method
