.class public LX/Fbi;
.super LX/FbF;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FbF",
        "<",
        "Lcom/facebook/graphql/model/GraphQLNode;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Fbi;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2260955
    invoke-direct {p0}, LX/FbF;-><init>()V

    .line 2260956
    return-void
.end method

.method public static a(LX/0QB;)LX/Fbi;
    .locals 3

    .prologue
    .line 2260957
    sget-object v0, LX/Fbi;->a:LX/Fbi;

    if-nez v0, :cond_1

    .line 2260958
    const-class v1, LX/Fbi;

    monitor-enter v1

    .line 2260959
    :try_start_0
    sget-object v0, LX/Fbi;->a:LX/Fbi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2260960
    if-eqz v2, :cond_0

    .line 2260961
    :try_start_1
    new-instance v0, LX/Fbi;

    invoke-direct {v0}, LX/Fbi;-><init>()V

    .line 2260962
    move-object v0, v0

    .line 2260963
    sput-object v0, LX/Fbi;->a:LX/Fbi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2260964
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2260965
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2260966
    :cond_1
    sget-object v0, LX/Fbi;->a:LX/Fbi;

    return-object v0

    .line 2260967
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2260968
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ILcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260969
    invoke-virtual {p2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260970
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNode;

    .line 2260971
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260972
    check-cast p1, Lcom/facebook/graphql/model/GraphQLNode;

    .line 2260973
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
