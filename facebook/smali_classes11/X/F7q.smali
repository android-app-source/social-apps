.class public LX/F7q;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2202633
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2202634
    return-void
.end method

.method public static a(LX/F7v;LX/89v;)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 2202635
    sget-object v0, LX/F7p;->a:[I

    invoke-virtual {p0}, LX/F7v;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2202636
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Friend finder doesn\'t support step "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, LX/F7v;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2202637
    :pswitch_0
    iget-object v0, p1, LX/89v;->value:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->a(LX/89v;Ljava/lang/String;Z)Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    move-result-object v0

    .line 2202638
    :goto_0
    return-object v0

    .line 2202639
    :pswitch_1
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;->a(LX/89v;Z)Lcom/facebook/growth/friendfinder/FriendFinderFriendableContactsFragment;

    move-result-object v0

    goto :goto_0

    .line 2202640
    :pswitch_2
    invoke-static {p1}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->a(LX/89v;)Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
