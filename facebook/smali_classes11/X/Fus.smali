.class public final LX/Fus;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Fuu;


# direct methods
.method public constructor <init>(LX/Fuu;)V
    .locals 0

    .prologue
    .line 2300590
    iput-object p1, p0, LX/Fus;->a:LX/Fuu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x234e947

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2300591
    iget-object v0, p0, LX/Fus;->a:LX/Fuu;

    iget-object v0, v0, LX/Fuu;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G3G;

    const-string v2, "profile_nux"

    const/4 v4, 0x0

    .line 2300592
    const-string v3, "profile_nux_start_button_tapped"

    invoke-static {v0, v2, v4, v4, v3}, LX/G3G;->a(LX/G3G;Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;Ljava/lang/String;)V

    .line 2300593
    iget-object v0, p0, LX/Fus;->a:LX/Fuu;

    iget-object v2, v0, LX/Fuu;->b:LX/G3q;

    iget-object v0, p0, LX/Fus;->a:LX/Fuu;

    iget-object v3, v0, LX/Fuu;->a:Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v4, Landroid/app/Activity;

    invoke-static {v0, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v2, v3, v0}, LX/G3q;->a(Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;Landroid/app/Activity;)V

    .line 2300594
    iget-object v0, p0, LX/Fus;->a:LX/Fuu;

    iget-object v0, v0, LX/Fuu;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fuv;

    iget-object v2, p0, LX/Fus;->a:LX/Fuu;

    iget-object v2, v2, LX/Fuu;->g:Ljava/lang/String;

    .line 2300595
    new-instance v3, LX/4Io;

    invoke-direct {v3}, LX/4Io;-><init>()V

    .line 2300596
    const-string v4, "actor_id"

    invoke-virtual {v3, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2300597
    new-instance v4, LX/5yM;

    invoke-direct {v4}, LX/5yM;-><init>()V

    move-object v4, v4

    .line 2300598
    const-string p0, "input"

    invoke-virtual {v4, p0, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2300599
    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 2300600
    iget-object v4, v0, LX/Fuv;->a:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2300601
    const v0, -0x77204796

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
