.class public LX/FRT;
.super Lcom/facebook/payments/ui/PaymentsComponentViewGroup;
.source ""

# interfaces
.implements LX/6vq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/payments/ui/PaymentsComponentViewGroup;",
        "LX/6vq",
        "<",
        "LX/FRp;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/6pC;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/payments/auth/pin/model/PaymentPin;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:Lcom/facebook/payments/ui/FloatingLabelTextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2240404
    invoke-direct {p0, p1}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;-><init>(Landroid/content/Context;)V

    .line 2240405
    const-class v0, LX/FRT;

    invoke-static {v0, p0}, LX/FRT;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2240406
    const v0, 0x7f0314f9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2240407
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, LX/FRT;->getContext()Landroid/content/Context;

    move-result-object v1

    const p1, 0x7f0a00d5

    invoke-static {v1, p1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p0, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2240408
    invoke-virtual {p0}, LX/FRT;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b139f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2240409
    invoke-virtual {p0, v0, v0, v0, v0}, LX/FRT;->setPadding(IIII)V

    .line 2240410
    const v0, 0x7f0d02c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/ui/FloatingLabelTextView;

    iput-object v0, p0, LX/FRT;->c:Lcom/facebook/payments/ui/FloatingLabelTextView;

    .line 2240411
    iget-object v0, p0, LX/FRT;->c:Lcom/facebook/payments/ui/FloatingLabelTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/FloatingLabelTextView;->a()V

    .line 2240412
    iget-object v0, p0, LX/FRT;->c:Lcom/facebook/payments/ui/FloatingLabelTextView;

    const v1, 0x7f081e16

    invoke-virtual {v0, v1}, Lcom/facebook/payments/ui/FloatingLabelTextView;->setText(I)V

    .line 2240413
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/FRT;

    invoke-static {p0}, LX/6pC;->a(LX/0QB;)LX/6pC;

    move-result-object p0

    check-cast p0, LX/6pC;

    iput-object p0, p1, LX/FRT;->a:LX/6pC;

    return-void
.end method

.method public static c(LX/FRT;)Z
    .locals 1

    .prologue
    .line 2240414
    iget-object v0, p0, LX/FRT;->b:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FRT;->b:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    invoke-virtual {v0}, Lcom/facebook/payments/auth/pin/model/PaymentPin;->a()LX/0am;

    move-result-object v0

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getIntent()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2240415
    invoke-static {p0}, LX/FRT;->c(LX/FRT;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2240416
    invoke-virtual {p0}, LX/FRT;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2240417
    new-instance v1, LX/6qQ;

    invoke-direct {v1}, LX/6qQ;-><init>()V

    move-object v1, v1

    .line 2240418
    new-instance v2, Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;

    invoke-direct {v2, v1}, Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;-><init>(LX/6qQ;)V

    move-object v1, v2

    .line 2240419
    invoke-static {v0, v1}, Lcom/facebook/payments/auth/settings/PaymentPinSettingsActivity;->a(Landroid/content/Context;Lcom/facebook/payments/auth/settings/PaymentPinSettingsParams;)Landroid/content/Intent;

    move-result-object v0

    .line 2240420
    :goto_0
    return-object v0

    .line 2240421
    :cond_0
    sget-object v0, LX/6pF;->CREATE:LX/6pF;

    invoke-static {v0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;->a(LX/6pF;)Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;

    move-result-object v0

    .line 2240422
    invoke-virtual {p0}, LX/FRT;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/facebook/payments/auth/pin/newpin/PaymentPinActivity;->a(Landroid/content/Context;Lcom/facebook/payments/auth/pin/newpin/PaymentPinParams;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public onClick()V
    .locals 2

    .prologue
    .line 2240423
    iget-object v0, p0, LX/FRT;->b:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2240424
    invoke-direct {p0}, LX/FRT;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x194

    invoke-virtual {p0, v0, v1}, Lcom/facebook/payments/ui/PaymentsComponentViewGroup;->a(Landroid/content/Intent;I)V

    .line 2240425
    return-void
.end method
