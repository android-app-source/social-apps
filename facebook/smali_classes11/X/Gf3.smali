.class public LX/Gf3;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Gf3",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2376668
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2376669
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Gf3;->b:LX/0Zi;

    .line 2376670
    iput-object p1, p0, LX/Gf3;->a:LX/0Ot;

    .line 2376671
    return-void
.end method

.method public static a(LX/0QB;)LX/Gf3;
    .locals 4

    .prologue
    .line 2376697
    const-class v1, LX/Gf3;

    monitor-enter v1

    .line 2376698
    :try_start_0
    sget-object v0, LX/Gf3;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2376699
    sput-object v2, LX/Gf3;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2376700
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2376701
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2376702
    new-instance v3, LX/Gf3;

    const/16 p0, 0x210c

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Gf3;-><init>(LX/0Ot;)V

    .line 2376703
    move-object v0, v3

    .line 2376704
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2376705
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gf3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2376706
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2376707
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 10

    .prologue
    .line 2376689
    check-cast p2, LX/Gf2;

    .line 2376690
    iget-object v0, p0, LX/Gf3;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeComponentSpec;

    iget-object v1, p2, LX/Gf2;->c:LX/25E;

    iget-object v2, p2, LX/Gf2;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v7, 0x0

    .line 2376691
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2376692
    check-cast v3, LX/16h;

    invoke-static {v1, v3}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v8

    .line 2376693
    iget-object v3, v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeComponentSpec;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1nA;

    invoke-interface {v1}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v4

    invoke-static {v4}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object v5

    iget-object v4, v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeComponentSpec;->f:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-interface {v1}, LX/25E;->x()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4, v8}, LX/17Q;->a(ZLX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    invoke-virtual {v8}, LX/162;->toString()Ljava/lang/String;

    move-result-object v9

    move-object v4, p1

    move-object v8, v7

    invoke-virtual/range {v3 .. v9}, LX/1nA;->a(Landroid/view/View;LX/1y5;Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 2376694
    return-void

    .line 2376695
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2376696
    const v0, -0x1d4f0f0a

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2376686
    check-cast p2, LX/Gf2;

    .line 2376687
    iget-object v0, p0, LX/Gf3;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeComponentSpec;

    iget-object v2, p2, LX/Gf2;->a:LX/1Pp;

    iget-object v3, p2, LX/Gf2;->b:LX/1f9;

    iget-object v4, p2, LX/Gf2;->c:LX/25E;

    iget-object v5, p2, LX/Gf2;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget v6, p2, LX/Gf2;->e:I

    iget-object v7, p2, LX/Gf2;->f:LX/2dx;

    iget-boolean v8, p2, LX/Gf2;->g:Z

    iget-boolean v9, p2, LX/Gf2;->h:Z

    move-object v1, p1

    invoke-virtual/range {v0 .. v9}, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeComponentSpec;->a(LX/1De;LX/1Pp;LX/1f9;LX/25E;Lcom/facebook/feed/rows/core/props/FeedProps;ILX/2dx;ZZ)LX/1Dg;

    move-result-object v0

    .line 2376688
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2376680
    invoke-static {}, LX/1dS;->b()V

    .line 2376681
    iget v0, p1, LX/1dQ;->b:I

    .line 2376682
    packed-switch v0, :pswitch_data_0

    .line 2376683
    :goto_0
    return-object v2

    .line 2376684
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2376685
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/Gf3;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1d4f0f0a
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/Gf1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/Gf3",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2376672
    new-instance v1, LX/Gf2;

    invoke-direct {v1, p0}, LX/Gf2;-><init>(LX/Gf3;)V

    .line 2376673
    iget-object v2, p0, LX/Gf3;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Gf1;

    .line 2376674
    if-nez v2, :cond_0

    .line 2376675
    new-instance v2, LX/Gf1;

    invoke-direct {v2, p0}, LX/Gf1;-><init>(LX/Gf3;)V

    .line 2376676
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/Gf1;->a$redex0(LX/Gf1;LX/1De;IILX/Gf2;)V

    .line 2376677
    move-object v1, v2

    .line 2376678
    move-object v0, v1

    .line 2376679
    return-object v0
.end method
