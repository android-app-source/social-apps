.class public final LX/G4E;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<*>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/G4B;

.field public final synthetic b:LX/G4K;

.field public final synthetic c:Lcom/facebook/timeline/service/ProfileLoadManager;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/service/ProfileLoadManager;LX/G4B;LX/G4K;)V
    .locals 0

    .prologue
    .line 2317191
    iput-object p1, p0, LX/G4E;->c:Lcom/facebook/timeline/service/ProfileLoadManager;

    iput-object p2, p0, LX/G4E;->a:LX/G4B;

    iput-object p3, p0, LX/G4E;->b:LX/G4K;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 9

    .prologue
    .line 2317192
    iget-object v1, p0, LX/G4E;->b:LX/G4K;

    .line 2317193
    iget p1, v1, LX/G4K;->b:I

    move p1, p1

    .line 2317194
    const/4 v0, 0x3

    if-ge p1, v0, :cond_3

    const/4 p1, 0x1

    :goto_0
    move p1, p1

    .line 2317195
    move v0, p1

    .line 2317196
    if-eqz v0, :cond_2

    .line 2317197
    iget-object v0, p0, LX/G4E;->c:Lcom/facebook/timeline/service/ProfileLoadManager;

    iget-object v1, p0, LX/G4E;->b:LX/G4K;

    .line 2317198
    iget-object v2, v0, Lcom/facebook/timeline/service/ProfileLoadManager;->c:LX/G4H;

    invoke-virtual {v1}, LX/G4K;->a()J

    move-result-wide v4

    invoke-virtual {v1}, LX/G4K;->b()Ljava/lang/String;

    move-result-object v3

    .line 2317199
    invoke-static {v2, v4, v5}, LX/G4H;->a(LX/G4H;J)Z

    move-result v6

    if-eqz v6, :cond_4

    const v6, 0x7f081634

    .line 2317200
    :goto_1
    invoke-static {v2, v4, v5, v3, v6}, LX/G4H;->a(LX/G4H;JLjava/lang/String;I)V

    .line 2317201
    iget-object v2, v0, Lcom/facebook/timeline/service/ProfileLoadManager;->m:LX/G4L;

    invoke-virtual {v2, v1}, LX/G4L;->c(LX/G4K;)V

    .line 2317202
    iget-object v2, v0, Lcom/facebook/timeline/service/ProfileLoadManager;->q:LX/0Yb;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/facebook/timeline/service/ProfileLoadManager;->q:LX/0Yb;

    invoke-virtual {v2}, LX/0Yb;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2317203
    :cond_0
    iget-object v2, v0, Lcom/facebook/timeline/service/ProfileLoadManager;->k:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Xl;

    invoke-interface {v2}, LX/0Xl;->a()LX/0YX;

    move-result-object v2

    const-string v3, "com.facebook.orca.ACTION_NETWORK_CONNECTIVITY_CHANGED"

    new-instance v4, LX/G4F;

    invoke-direct {v4, v0}, LX/G4F;-><init>(Lcom/facebook/timeline/service/ProfileLoadManager;)V

    invoke-interface {v2, v3, v4}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v2

    invoke-interface {v2}, LX/0YX;->a()LX/0Yb;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/timeline/service/ProfileLoadManager;->q:LX/0Yb;

    .line 2317204
    :cond_1
    iget-object v2, v0, Lcom/facebook/timeline/service/ProfileLoadManager;->q:LX/0Yb;

    invoke-virtual {v2}, LX/0Yb;->b()V

    .line 2317205
    :goto_2
    return-void

    .line 2317206
    :cond_2
    iget-object v0, p0, LX/G4E;->c:Lcom/facebook/timeline/service/ProfileLoadManager;

    iget-object v1, p0, LX/G4E;->b:LX/G4K;

    .line 2317207
    iget-object v2, v0, Lcom/facebook/timeline/service/ProfileLoadManager;->c:LX/G4H;

    invoke-virtual {v1}, LX/G4K;->a()J

    move-result-wide v4

    invoke-virtual {v1}, LX/G4K;->b()Ljava/lang/String;

    move-result-object v3

    .line 2317208
    invoke-static {v2, v4, v5}, LX/G4H;->a(LX/G4H;J)Z

    move-result v6

    if-eqz v6, :cond_6

    const v6, 0x7f08162e

    .line 2317209
    :goto_3
    invoke-static {v2, v4, v5, v3, v6}, LX/G4H;->a(LX/G4H;JLjava/lang/String;I)V

    .line 2317210
    iget-object v6, v2, LX/G4H;->e:LX/0Or;

    invoke-interface {v6}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/BQ9;

    const/4 v7, 0x1

    invoke-virtual {v6, v7, v4, v5}, LX/BQ9;->a(IJ)V

    .line 2317211
    iget-object v2, v0, Lcom/facebook/timeline/service/ProfileLoadManager;->a:LX/Fq9;

    invoke-virtual {v1}, LX/G4K;->a()J

    move-result-wide v4

    const/4 v3, 0x2

    invoke-virtual {v2, v4, v5, v3}, LX/Fq9;->a(JI)V

    .line 2317212
    invoke-static {v0, v1}, Lcom/facebook/timeline/service/ProfileLoadManager;->c$redex0(Lcom/facebook/timeline/service/ProfileLoadManager;LX/G4K;)V

    .line 2317213
    goto :goto_2

    :cond_3
    const/4 p1, 0x0

    goto/16 :goto_0

    .line 2317214
    :cond_4
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    const v6, 0x7f081635

    goto :goto_1

    :cond_5
    const v6, 0x7f081633

    goto :goto_1

    .line 2317215
    :cond_6
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_7

    const v6, 0x7f08162f

    goto :goto_3

    :cond_7
    const v6, 0x7f08162d

    goto :goto_3
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2317216
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2317217
    if-eqz p1, :cond_0

    .line 2317218
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2317219
    if-nez v0, :cond_1

    .line 2317220
    :cond_0
    :goto_0
    return-void

    .line 2317221
    :cond_1
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2317222
    instance-of v2, v1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;

    if-eqz v2, :cond_8

    .line 2317223
    check-cast v1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;

    .line 2317224
    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$UserTimelineSelfQueryFieldsModel;->a()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    move-result-object v1

    .line 2317225
    :goto_1
    move-object v2, v1

    .line 2317226
    move-object v1, v2

    .line 2317227
    iget-object v0, p0, LX/G4E;->a:LX/G4B;

    .line 2317228
    iget-object v2, v0, LX/G4B;->b:Ljava/lang/Object;

    move-object v0, v2

    .line 2317229
    check-cast v0, LX/G4C;

    .line 2317230
    iput-object v1, v0, LX/G4C;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    .line 2317231
    iget-object v0, p0, LX/G4E;->c:Lcom/facebook/timeline/service/ProfileLoadManager;

    iget-object v0, v0, Lcom/facebook/timeline/service/ProfileLoadManager;->b:Lcom/facebook/timeline/service/TimelineImagePrefetcher;

    new-instance v2, LX/G4P;

    iget-object v3, p0, LX/G4E;->a:LX/G4B;

    invoke-direct {v2, v3}, LX/G4P;-><init>(LX/G4B;)V

    .line 2317232
    iget-object v3, v0, Lcom/facebook/timeline/service/TimelineImagePrefetcher;->c:LX/G4M;

    .line 2317233
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2317234
    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->u()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v5

    .line 2317235
    if-eqz v5, :cond_9

    invoke-virtual {v5}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object v6

    if-eqz v6, :cond_9

    invoke-virtual {v5}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;->e()LX/1Fb;

    move-result-object v6

    if-eqz v6, :cond_9

    .line 2317236
    invoke-virtual {v5}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;->e()LX/1Fb;

    move-result-object v5

    invoke-interface {v5}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v5

    .line 2317237
    :goto_2
    move-object v5, v5

    .line 2317238
    if-eqz v5, :cond_2

    .line 2317239
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2317240
    :cond_2
    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->x()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v5

    if-eqz v5, :cond_a

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->x()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 2317241
    :goto_3
    if-eqz v5, :cond_3

    .line 2317242
    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2317243
    :cond_3
    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->v()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-result-object v7

    .line 2317244
    if-eqz v7, :cond_7

    .line 2317245
    if-eqz v7, :cond_b

    invoke-virtual {v7}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->cj_()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$IntroCardContextListItemsConnectionFieldsModel;

    move-result-object v5

    if-eqz v5, :cond_b

    invoke-virtual {v7}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->cj_()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$IntroCardContextListItemsConnectionFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$IntroCardContextListItemsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_b

    const/4 v5, 0x1

    :goto_4
    move v5, v5

    .line 2317246
    if-eqz v5, :cond_4

    .line 2317247
    invoke-virtual {v7}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->cj_()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$IntroCardContextListItemsConnectionFieldsModel;

    move-result-object v5

    .line 2317248
    invoke-virtual {v5}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$IntroCardContextListItemsConnectionFieldsModel;->a()LX/0Px;

    move-result-object p0

    invoke-virtual {p0}, LX/0Px;->size()I

    move-result p1

    const/4 v5, 0x0

    move v6, v5

    :goto_5
    if-ge v6, p1, :cond_4

    invoke-virtual {p0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    .line 2317249
    invoke-virtual {v5}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->cf_()LX/1Fb;

    move-result-object v5

    .line 2317250
    invoke-interface {v5}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2317251
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_5

    .line 2317252
    :cond_4
    invoke-virtual {v7}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->l()Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;

    move-result-object v5

    .line 2317253
    if-eqz v5, :cond_5

    .line 2317254
    invoke-virtual {v5}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotosFieldsModel$FavoritePhotosModel;->a()LX/0Px;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/G4M;->a(LX/G4M;Ljava/util/List;LX/0Px;)V

    .line 2317255
    :cond_5
    if-eqz v7, :cond_6

    invoke-virtual {v7}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->p()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$SuggestedPhotosModel;

    move-result-object v5

    if-nez v5, :cond_c

    .line 2317256
    :cond_6
    sget-object v5, LX/0Q7;->a:LX/0Px;

    move-object v5, v5

    .line 2317257
    :goto_6
    move-object v5, v5

    .line 2317258
    invoke-static {v5}, LX/Fx4;->a(LX/0Px;)LX/0Px;

    move-result-object v5

    invoke-static {v3, v4, v5}, LX/G4M;->a(LX/G4M;Ljava/util/List;LX/0Px;)V

    .line 2317259
    :cond_7
    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    move-object v3, v4

    .line 2317260
    invoke-static {v0, v2, v3}, Lcom/facebook/timeline/service/TimelineImagePrefetcher;->a(Lcom/facebook/timeline/service/TimelineImagePrefetcher;LX/G4P;Ljava/util/List;)V

    .line 2317261
    goto/16 :goto_0

    .line 2317262
    :cond_8
    check-cast v1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    goto/16 :goto_1

    :cond_9
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 2317263
    :cond_a
    const/4 v5, 0x0

    goto/16 :goto_3

    :cond_b
    const/4 v5, 0x0

    goto :goto_4

    :cond_c
    invoke-virtual {v7}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->p()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$SuggestedPhotosModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel$SuggestedPhotosModel;->a()LX/0Px;

    move-result-object v5

    goto :goto_6
.end method
