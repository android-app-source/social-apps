.class public final LX/Fpo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1L9;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1L9",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/G4x;

.field public final synthetic b:Lcom/facebook/timeline/BaseTimelineFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/BaseTimelineFragment;LX/G4x;)V
    .locals 0

    .prologue
    .line 2291902
    iput-object p1, p0, LX/Fpo;->b:Lcom/facebook/timeline/BaseTimelineFragment;

    iput-object p2, p0, LX/Fpo;->a:LX/G4x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2291903
    check-cast p1, Lcom/facebook/graphql/model/FeedUnit;

    .line 2291904
    iget-object v0, p0, LX/Fpo;->b:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lm_()LX/G4x;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/G4x;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2291905
    iget-object v0, p0, LX/Fpo;->b:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->t()LX/Frd;

    move-result-object v0

    .line 2291906
    if-eqz v0, :cond_0

    .line 2291907
    invoke-virtual {v0}, LX/Frd;->f()V

    .line 2291908
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 2291909
    check-cast p1, Lcom/facebook/graphql/model/FeedUnit;

    .line 2291910
    iget-object v0, p0, LX/Fpo;->a:LX/G4x;

    if-eqz v0, :cond_0

    .line 2291911
    iget-object v0, p0, LX/Fpo;->a:LX/G4x;

    invoke-virtual {v0, p1}, LX/G4x;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2291912
    :cond_0
    iget-object v0, p0, LX/Fpo;->b:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->t()LX/Frd;

    move-result-object v0

    .line 2291913
    if-eqz v0, :cond_1

    .line 2291914
    invoke-virtual {v0}, LX/Frd;->f()V

    .line 2291915
    :cond_1
    iget-object v0, p0, LX/Fpo;->b:Lcom/facebook/timeline/BaseTimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/BaseTimelineFragment;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "timeline_story_like_fail"

    invoke-virtual {v0, v1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2291916
    return-void
.end method

.method public final bridge synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2291917
    return-void
.end method

.method public final bridge synthetic c(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2291918
    return-void
.end method
