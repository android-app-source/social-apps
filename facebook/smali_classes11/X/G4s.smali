.class public LX/G4s;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/G4s;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FuW;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17P;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Ot;LX/0Or;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/FuW;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17P;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2318115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2318116
    const-string v0, "TimelineFilterHandler"

    iput-object v0, p0, LX/G4s;->a:Ljava/lang/String;

    .line 2318117
    iput-object p1, p0, LX/G4s;->b:LX/0Or;

    .line 2318118
    iput-object p2, p0, LX/G4s;->c:LX/0Ot;

    .line 2318119
    iput-object p3, p0, LX/G4s;->d:LX/0Or;

    .line 2318120
    return-void
.end method

.method public static a(LX/0QB;)LX/G4s;
    .locals 6

    .prologue
    .line 2318121
    sget-object v0, LX/G4s;->e:LX/G4s;

    if-nez v0, :cond_1

    .line 2318122
    const-class v1, LX/G4s;

    monitor-enter v1

    .line 2318123
    :try_start_0
    sget-object v0, LX/G4s;->e:LX/G4s;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2318124
    if-eqz v2, :cond_0

    .line 2318125
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2318126
    new-instance v3, LX/G4s;

    const/16 v4, 0x365d

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x761

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 p0, 0x259

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, v4, v5, p0}, LX/G4s;-><init>(LX/0Or;LX/0Ot;LX/0Or;)V

    .line 2318127
    move-object v0, v3

    .line 2318128
    sput-object v0, LX/G4s;->e:LX/G4s;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2318129
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2318130
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2318131
    :cond_1
    sget-object v0, LX/G4s;->e:LX/G4s;

    return-object v0

    .line 2318132
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2318133
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 4

    .prologue
    .line 2318112
    iget-object v0, p0, LX/G4s;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v2, "TimelineFilterHandler"

    const-string v3, "The object isn\'t supported: [FeedUnit: %s]"

    if-nez p1, :cond_0

    const-string v1, "null"

    :goto_0
    invoke-static {v3, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2318113
    return-void

    .line 2318114
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0Px;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2318103
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2318104
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_2

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2318105
    iget-object v1, p0, LX/G4s;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    invoke-static {v0}, LX/FuW;->a(Ljava/lang/Object;)LX/FuV;

    move-result-object v1

    sget-object v5, LX/FuV;->UNKNOWN:LX/FuV;

    if-ne v1, v5, :cond_1

    .line 2318106
    invoke-direct {p0, v0}, LX/G4s;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2318107
    :cond_0
    :goto_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2318108
    :cond_1
    iget-object v1, p0, LX/G4s;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/17P;

    invoke-virtual {v1, v0}, LX/17P;->a(Lcom/facebook/graphql/model/FeedUnit;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2318109
    if-eqz v0, :cond_0

    .line 2318110
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2318111
    :cond_2
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLTimelineSection;)Lcom/facebook/graphql/model/GraphQLTimelineSection;
    .locals 8

    .prologue
    .line 2318060
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2318061
    :cond_0
    iget-object v0, p0, LX/G4s;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "TimelineFilterHandler"

    const-string v2, "Incomplete section data"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2318062
    new-instance v1, Ljava/lang/IllegalStateException;

    if-nez p1, :cond_1

    const-string v0, "section is null"

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    const-string v0, "a required field in section result is null"

    goto :goto_0

    .line 2318063
    :cond_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2318064
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_5

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;

    .line 2318065
    iget-object v1, p0, LX/G4s;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    invoke-static {v1}, LX/FuW;->a(Ljava/lang/Object;)LX/FuV;

    move-result-object v1

    sget-object v6, LX/FuV;->UNKNOWN:LX/FuV;

    if-ne v1, v6, :cond_4

    .line 2318066
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    invoke-direct {p0, v0}, LX/G4s;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2318067
    :cond_3
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2318068
    :cond_4
    iget-object v1, p0, LX/G4s;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/17P;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v6

    invoke-virtual {v1, v6}, LX/17P;->a(Lcom/facebook/graphql/model/FeedUnit;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    .line 2318069
    if-eqz v1, :cond_3

    .line 2318070
    new-instance v6, LX/4ZE;

    invoke-direct {v6}, LX/4ZE;-><init>()V

    .line 2318071
    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2318072
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->a()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4ZE;->b:Ljava/lang/String;

    .line 2318073
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->j()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v7

    iput-object v7, v6, LX/4ZE;->c:Lcom/facebook/graphql/model/FeedUnit;

    .line 2318074
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;->k()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, LX/4ZE;->d:Ljava/lang/String;

    .line 2318075
    invoke-static {v6, v0}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 2318076
    move-object v0, v6

    .line 2318077
    iput-object v1, v0, LX/4ZE;->c:Lcom/facebook/graphql/model/FeedUnit;

    .line 2318078
    move-object v0, v0

    .line 2318079
    invoke-virtual {v0}, LX/4ZE;->a()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsEdge;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 2318080
    :cond_5
    new-instance v0, LX/4ZC;

    invoke-direct {v0}, LX/4ZC;-><init>()V

    .line 2318081
    invoke-virtual {p1}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2318082
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->j()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4ZC;->b:Ljava/lang/String;

    .line 2318083
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->k()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4ZC;->c:Ljava/lang/String;

    .line 2318084
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v1

    iput-object v1, v0, LX/4ZC;->d:Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    .line 2318085
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->m()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/4ZC;->e:Ljava/lang/String;

    .line 2318086
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->n()I

    move-result v1

    iput v1, v0, LX/4ZC;->f:I

    .line 2318087
    invoke-static {v0, p1}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 2318088
    move-object v0, v0

    .line 2318089
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLTimelineSection;->l()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v1

    .line 2318090
    new-instance v2, LX/4ZD;

    invoke-direct {v2}, LX/4ZD;-><init>()V

    .line 2318091
    invoke-virtual {v1}, Lcom/facebook/graphql/modelutil/BaseModel;->h()V

    .line 2318092
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;->a()LX/0Px;

    move-result-object v4

    iput-object v4, v2, LX/4ZD;->b:LX/0Px;

    .line 2318093
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v4

    iput-object v4, v2, LX/4ZD;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2318094
    invoke-static {v2, v1}, LX/0ur;->a(LX/0ur;Lcom/facebook/graphql/modelutil/BaseModel;)V

    .line 2318095
    move-object v1, v2

    .line 2318096
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2318097
    iput-object v2, v1, LX/4ZD;->b:LX/0Px;

    .line 2318098
    move-object v1, v1

    .line 2318099
    invoke-virtual {v1}, LX/4ZD;->a()Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    move-result-object v1

    .line 2318100
    iput-object v1, v0, LX/4ZC;->d:Lcom/facebook/graphql/model/GraphQLTimelineSectionUnitsConnection;

    .line 2318101
    move-object v0, v0

    .line 2318102
    invoke-virtual {v0}, LX/4ZC;->a()Lcom/facebook/graphql/model/GraphQLTimelineSection;

    move-result-object v0

    return-object v0
.end method
