.class public LX/G5W;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/7BK;",
            ">;"
        }
    .end annotation
.end field

.field public static final d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/7BK;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final e:LX/G5b;

.field public final f:LX/G5b;

.field public final g:LX/0Sh;

.field public final h:LX/0aG;

.field public final i:Lcom/facebook/search/api/GraphSearchQuery;

.field public final j:Ljava/lang/String;

.field public final k:J

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:LX/HeF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;"
        }
    .end annotation
.end field

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;"
        }
    .end annotation
.end field

.field public r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;"
        }
    .end annotation
.end field

.field public final s:Z

.field public final t:LX/G5P;

.field private final u:LX/7BO;

.field public final v:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 2318942
    const-class v0, LX/G5W;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/G5W;->a:Ljava/lang/String;

    .line 2318943
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    sput-object v0, LX/G5W;->b:Ljava/util/List;

    .line 2318944
    sget-object v0, LX/7BK;->APP:LX/7BK;

    sget-object v1, LX/7BK;->USER:LX/7BK;

    sget-object v2, LX/7BK;->PAGE:LX/7BK;

    sget-object v3, LX/7BK;->GROUP:LX/7BK;

    sget-object v4, LX/7BK;->EVENT:LX/7BK;

    sget-object v5, LX/7BK;->HASHTAG_EXACT:LX/7BK;

    invoke-static/range {v0 .. v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/G5W;->c:LX/0Px;

    .line 2318945
    sget-object v0, LX/7BK;->PAGE:LX/7BK;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/G5W;->d:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/G5b;LX/G5b;LX/0Sh;LX/0aG;ILcom/facebook/search/api/GraphSearchQuery;Ljava/lang/String;ZLX/G5P;LX/7BO;LX/03V;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2318924
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2318925
    iput-boolean v1, p0, LX/G5W;->l:Z

    .line 2318926
    iput-boolean v1, p0, LX/G5W;->m:Z

    .line 2318927
    iput-boolean v1, p0, LX/G5W;->n:Z

    .line 2318928
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G5b;

    iput-object v0, p0, LX/G5W;->e:LX/G5b;

    .line 2318929
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G5b;

    iput-object v0, p0, LX/G5W;->f:LX/G5b;

    .line 2318930
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    iput-object v0, p0, LX/G5W;->g:LX/0Sh;

    .line 2318931
    invoke-static {p4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0aG;

    iput-object v0, p0, LX/G5W;->h:LX/0aG;

    .line 2318932
    if-ltz p5, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "mRemoteFetchDelayIntervalMillis must be >= 0"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2318933
    int-to-long v0, p5

    iput-wide v0, p0, LX/G5W;->k:J

    .line 2318934
    invoke-static {p6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/api/GraphSearchQuery;

    iput-object v0, p0, LX/G5W;->i:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2318935
    iput-object p7, p0, LX/G5W;->j:Ljava/lang/String;

    .line 2318936
    iput-boolean p8, p0, LX/G5W;->s:Z

    .line 2318937
    iput-object p9, p0, LX/G5W;->t:LX/G5P;

    .line 2318938
    iput-object p10, p0, LX/G5W;->u:LX/7BO;

    .line 2318939
    iput-object p11, p0, LX/G5W;->v:LX/03V;

    .line 2318940
    return-void

    :cond_0
    move v0, v1

    .line 2318941
    goto :goto_0
.end method

.method public static a(LX/G5W;LX/7BK;)LX/0TF;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7BK;",
            ")",
            "LX/0TF",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2318923
    new-instance v0, LX/G5U;

    invoke-direct {v0, p0, p1}, LX/G5U;-><init>(LX/G5W;LX/7BK;)V

    return-object v0
.end method

.method public static g(LX/G5W;)V
    .locals 5

    .prologue
    .line 2318908
    iget-object v0, p0, LX/G5W;->o:LX/HeF;

    if-eqz v0, :cond_1

    .line 2318909
    iget-object v0, p0, LX/G5W;->r:Ljava/util/List;

    if-nez v0, :cond_2

    sget-object v0, LX/G5W;->b:Ljava/util/List;

    :goto_0
    iget-object v1, p0, LX/G5W;->p:Ljava/util/List;

    if-nez v1, :cond_3

    sget-object v1, LX/G5W;->b:Ljava/util/List;

    :goto_1
    iget-object v2, p0, LX/G5W;->q:Ljava/util/List;

    if-nez v2, :cond_4

    sget-object v2, LX/G5W;->b:Ljava/util/List;

    :goto_2
    invoke-static {v0, v1, v2}, LX/7BO;->a(Ljava/util/List;Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 2318910
    iget-object v1, p0, LX/G5W;->o:LX/HeF;

    .line 2318911
    iget-object v2, p0, LX/G5W;->r:Ljava/util/List;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/G5W;->p:Ljava/util/List;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/G5W;->q:Ljava/util/List;

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_3
    move v2, v2

    .line 2318912
    iget-object v4, v1, LX/HeF;->a:Lcom/facebook/uberbar/ui/UberbarResultsFragment;

    if-eqz v2, :cond_6

    sget-object v3, LX/He6;->COMPLETED:LX/He6;

    .line 2318913
    :goto_4
    iget-object p0, v4, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->l:LX/He8;

    if-nez p0, :cond_0

    .line 2318914
    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-static {p0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object p0

    check-cast p0, LX/03V;

    .line 2318915
    const-string v1, "ubersearch"

    const-string v2, "resultsUpdated called while mSearchResultsAdapter is null"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    .line 2318916
    invoke-virtual {p0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2318917
    :cond_0
    iget-object p0, v4, Lcom/facebook/uberbar/ui/UberbarResultsFragment;->l:LX/He8;

    invoke-virtual {p0, v0, v3}, LX/He8;->a(Ljava/util/List;LX/He6;)V

    .line 2318918
    :cond_1
    return-void

    .line 2318919
    :cond_2
    iget-object v0, p0, LX/G5W;->r:Ljava/util/List;

    goto :goto_0

    :cond_3
    iget-object v1, p0, LX/G5W;->p:Ljava/util/List;

    goto :goto_1

    :cond_4
    iget-object v2, p0, LX/G5W;->q:Ljava/util/List;

    goto :goto_2

    :cond_5
    const/4 v2, 0x0

    goto :goto_3

    .line 2318920
    :cond_6
    sget-object v3, LX/He6;->ONGOING:LX/He6;

    goto :goto_4
.end method


# virtual methods
.method public final b()V
    .locals 1

    .prologue
    .line 2318921
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/G5W;->l:Z

    .line 2318922
    return-void
.end method
