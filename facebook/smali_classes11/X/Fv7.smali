.class public LX/Fv7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0W9;

.field public final c:LX/G2m;


# direct methods
.method public constructor <init>(LX/0Or;LX/0W9;LX/G2m;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0W9;",
            "LX/G2m;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2300852
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2300853
    iput-object p1, p0, LX/Fv7;->a:LX/0Or;

    .line 2300854
    iput-object p2, p0, LX/Fv7;->b:LX/0W9;

    .line 2300855
    iput-object p3, p0, LX/Fv7;->c:LX/G2m;

    .line 2300856
    return-void
.end method

.method public static a(LX/0QB;)LX/Fv7;
    .locals 6

    .prologue
    .line 2300857
    const-class v1, LX/Fv7;

    monitor-enter v1

    .line 2300858
    :try_start_0
    sget-object v0, LX/Fv7;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2300859
    sput-object v2, LX/Fv7;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2300860
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2300861
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2300862
    new-instance v5, LX/Fv7;

    const/16 v3, 0x12cb

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v3

    check-cast v3, LX/0W9;

    invoke-static {v0}, LX/G2m;->a(LX/0QB;)LX/G2m;

    move-result-object v4

    check-cast v4, LX/G2m;

    invoke-direct {v5, p0, v3, v4}, LX/Fv7;-><init>(LX/0Or;LX/0W9;LX/G2m;)V

    .line 2300863
    move-object v0, v5

    .line 2300864
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2300865
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fv7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2300866
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2300867
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/Fv7;Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;LX/1EE;)V
    .locals 2

    .prologue
    .line 2300868
    iget-object v0, p0, LX/Fv7;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2300869
    iget-object v1, p2, LX/1EE;->i:Ljava/lang/String;

    move-object v1, v1

    .line 2300870
    invoke-virtual {p1, v1}, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->setComposerHintText(Ljava/lang/String;)V

    .line 2300871
    invoke-virtual {v0}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->a(Ljava/lang/String;)V

    .line 2300872
    iget-object v1, p0, LX/Fv7;->c:LX/G2m;

    invoke-virtual {v1}, LX/G2m;->a()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->setHeaderSectionOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2300873
    iget-object v1, p0, LX/Fv7;->c:LX/G2m;

    .line 2300874
    iget-object p0, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, p0

    .line 2300875
    invoke-virtual {v1, v0}, LX/G2m;->a(Ljava/lang/String;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/timeline/ui/common/InlineComposerHeaderView;->setProfileImageOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2300876
    return-void
.end method
