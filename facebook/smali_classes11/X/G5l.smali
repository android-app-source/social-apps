.class public LX/G5l;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/03V;

.field private final c:LX/61l;

.field private final d:LX/5Ov;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2319264
    const-class v0, LX/G5l;

    sput-object v0, LX/G5l;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/03V;LX/61l;LX/5Ov;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2319281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2319282
    iput-object p1, p0, LX/G5l;->b:LX/03V;

    .line 2319283
    iput-object p2, p0, LX/G5l;->c:LX/61l;

    .line 2319284
    iput-object p3, p0, LX/G5l;->d:LX/5Ov;

    .line 2319285
    return-void
.end method

.method public static a(LX/0QB;)LX/G5l;
    .locals 4

    .prologue
    .line 2319278
    new-instance v3, LX/G5l;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-static {p0}, LX/61l;->b(LX/0QB;)LX/61l;

    move-result-object v1

    check-cast v1, LX/61l;

    invoke-static {p0}, LX/5Ov;->b(LX/0QB;)LX/5Ov;

    move-result-object v2

    check-cast v2, LX/5Ov;

    invoke-direct {v3, v0, v1, v2}, LX/G5l;-><init>(LX/03V;LX/61l;LX/5Ov;)V

    .line 2319279
    move-object v0, v3

    .line 2319280
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/io/File;)LX/G5k;
    .locals 4

    .prologue
    .line 2319265
    const/4 v1, 0x0

    .line 2319266
    :try_start_0
    iget-object v0, p0, LX/G5l;->d:LX/5Ov;

    invoke-virtual {p1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/5Ov;->a(Ljava/lang/String;)Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;->a()Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;

    move-result-object v1

    .line 2319267
    iget-object v0, p0, LX/G5l;->c:LX/61l;

    invoke-virtual {v0, v1}, LX/61l;->b(Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;)LX/61k;

    move-result-object v2

    .line 2319268
    if-eqz v2, :cond_1

    new-instance v0, LX/G5k;

    iget-object v2, v2, LX/61k;->a:Ljava/lang/String;

    invoke-direct {v0, v2}, LX/G5k;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2319269
    :goto_0
    if-eqz v1, :cond_0

    .line 2319270
    invoke-virtual {v1}, Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;->g()V

    :cond_0
    return-object v0

    .line 2319271
    :cond_1
    :try_start_1
    new-instance v0, LX/G5k;

    invoke-direct {v0}, LX/G5k;-><init>()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2319272
    :catch_0
    move-exception v0

    .line 2319273
    :try_start_2
    sget-object v2, LX/G5l;->a:Ljava/lang/Class;

    const-string v3, "Exception"

    invoke-static {v2, v3, v0}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2319274
    iget-object v2, p0, LX/G5l;->b:LX/03V;

    const-string v3, "VideoProber_Exception"

    invoke-virtual {v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2319275
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2319276
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    .line 2319277
    invoke-virtual {v1}, Lcom/facebook/ffmpeg/FFMpegMediaDemuxer;->g()V

    :cond_2
    throw v0
.end method
