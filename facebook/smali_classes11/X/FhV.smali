.class public final LX/FhV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/7Hc",
        "<",
        "Lcom/facebook/search/api/SearchTypeaheadResult;",
        ">;",
        "LX/7Hc",
        "<",
        "Lcom/facebook/search/model/TypeaheadUnit;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;

.field public final synthetic b:LX/FhW;


# direct methods
.method public constructor <init>(LX/FhW;Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;)V
    .locals 0

    .prologue
    .line 2273096
    iput-object p1, p0, LX/FhV;->b:LX/FhW;

    iput-object p2, p0, LX/FhV;->a:Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2273097
    check-cast p1, LX/7Hc;

    .line 2273098
    iget-object v0, p0, LX/FhV;->b:LX/FhW;

    iget-object v0, v0, LX/FhW;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FhX;

    iget-object v1, p0, LX/FhV;->a:Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;

    iget-object v1, v1, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->n:LX/7BZ;

    iget-object v2, p0, LX/FhV;->a:Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;

    iget-object v2, v2, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;->f:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, p1, v1, v2}, LX/FhX;->a(LX/7Hc;LX/7BZ;Lcom/facebook/search/api/GraphSearchQuery;)LX/7Hc;

    move-result-object v0

    return-object v0
.end method
