.class public final LX/F3L;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;)V
    .locals 0

    .prologue
    .line 2194003
    iput-object p1, p0, LX/F3L;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2194004
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2194005
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2194006
    iget-object v1, p0, LX/F3L;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    .line 2194007
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2194008
    check-cast v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;

    .line 2194009
    iput-object v0, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->j:Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;

    .line 2194010
    iget-object v0, p0, LX/F3L;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->p:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/F3L;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->j:Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/F3L;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->j:Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;->k()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/F3L;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->j:Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;->k()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2194011
    iget-object v0, p0, LX/F3L;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    iget-object v1, p0, LX/F3L;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    iget-object v1, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->j:Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;->k()LX/0Px;

    move-result-object v1

    .line 2194012
    if-eqz v1, :cond_1

    .line 2194013
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel$GroupTopicTagsModel;

    .line 2194014
    if-eqz v2, :cond_0

    .line 2194015
    new-instance v5, LX/F3U;

    invoke-virtual {v2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel$GroupTopicTagsModel;->k()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel$GroupTopicTagsModel;->j()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, p1, v2}, LX/F3U;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2194016
    iget-object v2, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->l:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v2, v5}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    .line 2194017
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2194018
    :cond_1
    iget-object v0, p0, LX/F3L;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    iget-object v1, p0, LX/F3L;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    iget-object v1, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->j:Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupDescriptionModels$GroupNameDescriptionQueryModel;->k()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 2194019
    iput v1, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->s:I

    .line 2194020
    :cond_2
    return-void
.end method
