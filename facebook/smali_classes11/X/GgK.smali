.class public LX/GgK;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/GgJ;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/fig/components/contextrow/FigContextRowComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2378937
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/GgK;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/fig/components/contextrow/FigContextRowComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2378938
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2378939
    iput-object p1, p0, LX/GgK;->b:LX/0Ot;

    .line 2378940
    return-void
.end method

.method public static a(LX/0QB;)LX/GgK;
    .locals 4

    .prologue
    .line 2378941
    const-class v1, LX/GgK;

    monitor-enter v1

    .line 2378942
    :try_start_0
    sget-object v0, LX/GgK;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2378943
    sput-object v2, LX/GgK;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2378944
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2378945
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2378946
    new-instance v3, LX/GgK;

    const/16 p0, 0x21ff

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/GgK;-><init>(LX/0Ot;)V

    .line 2378947
    move-object v0, v3

    .line 2378948
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2378949
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GgK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2378950
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2378951
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2378952
    invoke-static {}, LX/1dS;->b()V

    .line 2378953
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(LX/1De;IILX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2378954
    check-cast p4, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;

    .line 2378955
    iget-object v0, p0, LX/GgK;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/components/contextrow/FigContextRowComponentSpec;

    iget-object v3, p4, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->a:Lcom/facebook/common/callercontext/CallerContext;

    iget v4, p4, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->b:I

    iget-object v5, p4, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->c:Ljava/lang/String;

    iget-object v6, p4, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->d:Ljava/lang/String;

    iget-object v7, p4, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;->e:Ljava/util/List;

    move-object v1, p1

    move v2, p2

    .line 2378956
    iget-object v8, v0, Lcom/facebook/fig/components/contextrow/FigContextRowComponentSpec;->c:LX/1vg;

    invoke-virtual {v8, v1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v8

    invoke-virtual {v8, v4}, LX/2xv;->h(I)LX/2xv;

    move-result-object v8

    const v9, 0x7f0a00a3

    invoke-virtual {v8, v9}, LX/2xv;->j(I)LX/2xv;

    move-result-object v8

    invoke-virtual {v8}, LX/1n6;->b()LX/1dc;

    move-result-object p1

    .line 2378957
    if-nez v7, :cond_2

    const/4 v8, 0x0

    move p0, v8

    .line 2378958
    :goto_0
    if-gtz p0, :cond_0

    if-nez v6, :cond_3

    :cond_0
    const/4 v8, 0x0

    move v9, v8

    .line 2378959
    :goto_1
    if-gtz p0, :cond_1

    const/4 v8, 0x3

    if-ne v9, v8, :cond_4

    :cond_1
    const/4 v8, 0x2

    .line 2378960
    :goto_2
    invoke-virtual {v1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    .line 2378961
    const p2, 0x7f0b1cb1

    invoke-virtual {p0, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p2

    .line 2378962
    const p3, 0x7f0b1cb2

    invoke-virtual {p0, p3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    .line 2378963
    const p4, 0x7f0b1caf

    invoke-virtual {p0, p4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p0

    .line 2378964
    invoke-static {v2}, LX/1mh;->b(I)I

    move-result p4

    mul-int/lit8 p0, p0, 0x3

    sub-int p0, p4, p0

    .line 2378965
    add-int/2addr p2, p3

    div-int/2addr p0, p2

    .line 2378966
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p2

    const/4 p3, 0x2

    invoke-interface {p2, p3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p2

    const/4 p3, 0x7

    const p4, 0x7f0b1cae

    invoke-interface {p2, p3, p4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object p2

    const/4 p3, 0x6

    const p4, 0x7f0b1caf

    invoke-interface {p2, p3, p4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object p2

    invoke-static {v1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p3

    invoke-virtual {p3, p1}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object p1

    invoke-virtual {p1}, LX/1X5;->c()LX/1Di;

    move-result-object p1

    const p3, 0x7f0b1cb0

    invoke-interface {p1, p3}, LX/1Di;->i(I)LX/1Di;

    move-result-object p1

    const p3, 0x7f0b1cb0

    invoke-interface {p1, p3}, LX/1Di;->q(I)LX/1Di;

    move-result-object p1

    const/4 p3, 0x5

    const p4, 0x7f0b1caf

    invoke-interface {p1, p3, p4}, LX/1Di;->c(II)LX/1Di;

    move-result-object p1

    invoke-interface {p2, p1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object p1

    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object p2

    const/4 p3, 0x0

    invoke-interface {p2, p3}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object p2

    const/high16 p3, 0x3f800000    # 1.0f

    invoke-interface {p2, p3}, LX/1Dh;->e(F)LX/1Dh;

    move-result-object p2

    const/high16 p3, 0x3f800000    # 1.0f

    invoke-interface {p2, p3}, LX/1Dh;->f(F)LX/1Dh;

    move-result-object p2

    const/4 p3, 0x0

    const p4, 0x7f0e0122

    invoke-static {v1, p3, p4}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object p3

    invoke-virtual {p3, v8}, LX/1ne;->j(I)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v8

    const/4 p3, 0x0

    invoke-virtual {v8, p3}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v8

    invoke-interface {p2, v8}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object p2

    if-nez v9, :cond_5

    const/4 v8, 0x0

    :goto_3
    invoke-interface {p2, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v9

    if-nez v7, :cond_6

    const/4 v8, 0x0

    :goto_4
    invoke-interface {v9, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    invoke-interface {p1, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v8

    invoke-interface {v8}, LX/1Di;->k()LX/1Dg;

    move-result-object v8

    move-object v0, v8

    .line 2378967
    return-object v0

    .line 2378968
    :cond_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    move p0, v8

    goto/16 :goto_0

    .line 2378969
    :cond_3
    const/4 v8, 0x3

    move v9, v8

    goto/16 :goto_1

    .line 2378970
    :cond_4
    const/4 v8, 0x3

    goto/16 :goto_2

    .line 2378971
    :cond_5
    const/4 v8, 0x0

    const p3, 0x7f0e012d

    invoke-static {v1, v8, p3}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, v9}, LX/1ne;->j(I)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    const/4 v9, 0x1

    const p3, 0x7f0b1cae

    invoke-interface {v8, v9, p3}, LX/1Di;->c(II)LX/1Di;

    move-result-object v8

    goto :goto_3

    :cond_6
    iget-object v8, v0, Lcom/facebook/fig/components/contextrow/FigContextRowComponentSpec;->b:LX/8yV;

    invoke-virtual {v8, v1}, LX/8yV;->c(LX/1De;)LX/8yU;

    move-result-object v8

    invoke-virtual {v8, v3}, LX/8yU;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/8yU;

    move-result-object v8

    invoke-virtual {v8, v7}, LX/8yU;->a(Ljava/util/List;)LX/8yU;

    move-result-object v8

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result p2

    invoke-static {p2, p0}, Ljava/lang/Math;->min(II)I

    move-result p0

    invoke-virtual {v8, p0}, LX/8yU;->m(I)LX/8yU;

    move-result-object v8

    const/4 p0, 0x0

    invoke-virtual {v8, p0}, LX/8yU;->d(F)LX/8yU;

    move-result-object v8

    const p0, 0x7f0b1cb1

    invoke-virtual {v8, p0}, LX/8yU;->h(I)LX/8yU;

    move-result-object v8

    const p0, 0x7f0b1cb2

    invoke-virtual {v8, p0}, LX/8yU;->l(I)LX/8yU;

    move-result-object v8

    const/4 p0, 0x0

    invoke-virtual {v8, p0}, LX/8yU;->c(F)LX/8yU;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    const/4 p0, 0x1

    const p2, 0x7f0b1cae

    invoke-interface {v8, p0, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object v8

    goto :goto_4
.end method

.method public final c(LX/1De;)LX/GgJ;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2378972
    new-instance v1, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;

    invoke-direct {v1, p0}, Lcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;-><init>(LX/GgK;)V

    .line 2378973
    sget-object v2, LX/GgK;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/GgJ;

    .line 2378974
    if-nez v2, :cond_0

    .line 2378975
    new-instance v2, LX/GgJ;

    invoke-direct {v2}, LX/GgJ;-><init>()V

    .line 2378976
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/GgJ;->a$redex0(LX/GgJ;LX/1De;IILcom/facebook/fig/components/contextrow/FigContextRowComponent$FigContextRowComponentImpl;)V

    .line 2378977
    move-object v1, v2

    .line 2378978
    move-object v0, v1

    .line 2378979
    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2378980
    const/4 v0, 0x1

    return v0
.end method
