.class public LX/Fxv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

.field public final c:LX/BPp;

.field private final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:Landroid/app/Activity;

.field public final f:LX/2dD;

.field public final g:LX/2yQ;

.field public h:Lcom/facebook/ipc/profile/TimelineFriendParams;

.field public i:LX/5OM;

.field public j:Landroid/view/MenuItem;

.field public k:LX/1B1;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/intent/feed/IFeedIntentBuilder;LX/BPp;Lcom/facebook/content/SecureContextHelper;Landroid/app/Activity;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2306154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2306155
    iput-object p1, p0, LX/Fxv;->a:Landroid/content/Context;

    .line 2306156
    iput-object p2, p0, LX/Fxv;->b:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 2306157
    iput-object p4, p0, LX/Fxv;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2306158
    iput-object p3, p0, LX/Fxv;->c:LX/BPp;

    .line 2306159
    iput-object p5, p0, LX/Fxv;->e:Landroid/app/Activity;

    .line 2306160
    new-instance v0, LX/Fxn;

    invoke-direct {v0, p0}, LX/Fxn;-><init>(LX/Fxv;)V

    iput-object v0, p0, LX/Fxv;->f:LX/2dD;

    .line 2306161
    new-instance v0, LX/Fxo;

    invoke-direct {v0, p0}, LX/Fxo;-><init>(LX/Fxv;)V

    iput-object v0, p0, LX/Fxv;->g:LX/2yQ;

    .line 2306162
    return-void
.end method

.method public static b(LX/Fxv;)V
    .locals 2

    .prologue
    .line 2306152
    iget-object v0, p0, LX/Fxv;->k:LX/1B1;

    iget-object v1, p0, LX/Fxv;->c:LX/BPp;

    invoke-virtual {v0, v1}, LX/1B1;->b(LX/0b4;)V

    .line 2306153
    return-void
.end method

.method public static e$redex0(LX/Fxv;)V
    .locals 2

    .prologue
    .line 2306146
    iget-object v0, p0, LX/Fxv;->h:Lcom/facebook/ipc/profile/TimelineFriendParams;

    iget-object v0, v0, Lcom/facebook/ipc/profile/TimelineFriendParams;->d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2306147
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v0, v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2306148
    if-eqz v0, :cond_0

    const v0, 0x7f08157d

    .line 2306149
    :goto_1
    iget-object v1, p0, LX/Fxv;->j:Landroid/view/MenuItem;

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 2306150
    return-void

    .line 2306151
    :cond_0
    const v0, 0x7f08157c

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
