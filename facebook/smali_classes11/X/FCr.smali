.class public LX/FCr;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/FCr;


# instance fields
.field private final a:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2210699
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2210700
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    const-wide/16 v2, 0x258

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, LX/0QN;->a(J)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/FCr;->a:LX/0QI;

    .line 2210701
    return-void
.end method

.method public static a(LX/0QB;)LX/FCr;
    .locals 3

    .prologue
    .line 2210702
    sget-object v0, LX/FCr;->b:LX/FCr;

    if-nez v0, :cond_1

    .line 2210703
    const-class v1, LX/FCr;

    monitor-enter v1

    .line 2210704
    :try_start_0
    sget-object v0, LX/FCr;->b:LX/FCr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2210705
    if-eqz v2, :cond_0

    .line 2210706
    :try_start_1
    new-instance v0, LX/FCr;

    invoke-direct {v0}, LX/FCr;-><init>()V

    .line 2210707
    move-object v0, v0

    .line 2210708
    sput-object v0, LX/FCr;->b:LX/FCr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2210709
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2210710
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2210711
    :cond_1
    sget-object v0, LX/FCr;->b:LX/FCr;

    return-object v0

    .line 2210712
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2210713
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 2210714
    iget-object v0, p0, LX/FCr;->a:LX/0QI;

    invoke-interface {v0, p1, p2}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2210715
    return-void
.end method
