.class public final LX/F9H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;)V
    .locals 0

    .prologue
    .line 2205305
    iput-object p1, p0, LX/F9H;->a:Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x26ce7f76

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2205306
    iget-object v1, p0, LX/F9H;->a:Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;

    .line 2205307
    new-instance v3, LX/6WS;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2205308
    invoke-virtual {v3}, LX/5OM;->c()LX/5OG;

    move-result-object v4

    .line 2205309
    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 2205310
    invoke-static {v1}, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->c(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2205311
    const v6, 0x7f083388

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v6

    new-instance p0, LX/F9N;

    invoke-direct {p0, v1}, LX/F9N;-><init>(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;)V

    invoke-virtual {v6, p0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2205312
    :cond_0
    const v6, 0x7f083389

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v4

    new-instance v5, LX/F9O;

    invoke-direct {v5, v1}, LX/F9O;-><init>(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;)V

    invoke-virtual {v4, v5}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2205313
    invoke-virtual {v3, p1}, LX/0ht;->a(Landroid/view/View;)V

    .line 2205314
    const v1, 0x2dce607e

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
