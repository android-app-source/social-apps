.class public LX/FKZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2Hu;

.field private final b:LX/0So;

.field private final c:LX/2Hv;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2Hu;LX/0So;LX/2Hv;LX/0Or;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsMessengerSyncEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/push/mqtt/service/MqttPushServiceClientManager;",
            "LX/0So;",
            "LX/2Hv;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2225262
    iput-object p1, p0, LX/FKZ;->a:LX/2Hu;

    .line 2225263
    iput-object p2, p0, LX/FKZ;->b:LX/0So;

    .line 2225264
    iput-object p3, p0, LX/FKZ;->c:LX/2Hv;

    .line 2225265
    iput-object p4, p0, LX/FKZ;->d:LX/0Or;

    .line 2225266
    return-void
.end method

.method private static b(LX/FKZ;LX/6iW;Lcom/facebook/messaging/service/model/MarkThreadFields;)[B
    .locals 22

    .prologue
    .line 2225267
    new-instance v15, LX/1so;

    new-instance v2, LX/1sp;

    invoke-direct {v2}, LX/1sp;-><init>()V

    invoke-direct {v15, v2}, LX/1so;-><init>(LX/1sq;)V

    .line 2225268
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v3

    .line 2225269
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    .line 2225270
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v4, v4, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v5, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v4, v5, :cond_0

    .line 2225271
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v4, v2, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    move-object v9, v2

    move-object v8, v3

    .line 2225272
    :goto_0
    invoke-virtual/range {p1 .. p1}, LX/6iW;->getApiName()Ljava/lang/String;

    move-result-object v3

    .line 2225273
    move-object/from16 v0, p2

    iget-boolean v2, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 2225274
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v7

    .line 2225275
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v6

    .line 2225276
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v5

    .line 2225277
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v10

    .line 2225278
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v12

    .line 2225279
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v13

    .line 2225280
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v14

    .line 2225281
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v16

    .line 2225282
    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->e:J

    move-wide/from16 v18, v0

    const-wide/16 v20, -0x1

    cmp-long v2, v18, v20

    if-eqz v2, :cond_1

    .line 2225283
    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->e:J

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    move-object v7, v5

    move-object v11, v2

    .line 2225284
    :goto_1
    new-instance v2, LX/6mU;

    invoke-virtual {v10}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual {v6}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v7}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v8}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v9}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    invoke-virtual {v12}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Long;

    invoke-virtual {v11}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Long;

    invoke-virtual {v13}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    invoke-virtual {v14}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Boolean;

    invoke-virtual/range {v16 .. v16}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/String;

    invoke-direct/range {v2 .. v14}, LX/6mU;-><init>(Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 2225285
    new-instance v3, LX/2ZA;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, LX/2ZA;-><init>(Ljava/lang/String;)V

    .line 2225286
    invoke-virtual {v15, v3}, LX/1so;->a(LX/1u2;)[B

    move-result-object v3

    .line 2225287
    invoke-virtual {v15, v2}, LX/1so;->a(LX/1u2;)[B

    move-result-object v2

    .line 2225288
    array-length v4, v3

    array-length v5, v2

    add-int/2addr v4, v5

    invoke-static {v3, v4}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v4

    .line 2225289
    const/4 v5, 0x0

    array-length v3, v3

    array-length v6, v2

    invoke-static {v2, v5, v4, v3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2225290
    return-object v4

    .line 2225291
    :cond_0
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v4, v3, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v3

    move-object v9, v2

    move-object v8, v3

    goto/16 :goto_0

    .line 2225292
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FKZ;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->d:J

    move-wide/from16 v18, v0

    const-wide/16 v20, -0x1

    cmp-long v2, v18, v20

    if-eqz v2, :cond_2

    .line 2225293
    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->d:J

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    move-object v11, v7

    move-object v7, v2

    goto/16 :goto_1

    .line 2225294
    :cond_2
    move-object/from16 v0, p2

    iget-wide v0, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->c:J

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    move-object v6, v2

    move-object v11, v7

    move-object v7, v5

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(LX/6iW;Lcom/facebook/messaging/service/model/MarkThreadFields;)Z
    .locals 8

    .prologue
    .line 2225295
    :try_start_0
    iget-object v0, p0, LX/FKZ;->a:LX/2Hu;

    invoke-virtual {v0}, LX/2Hu;->a()LX/2gV;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2225296
    :try_start_1
    invoke-static {p0, p1, p2}, LX/FKZ;->b(LX/FKZ;LX/6iW;Lcom/facebook/messaging/service/model/MarkThreadFields;)[B

    move-result-object v3

    .line 2225297
    new-instance v0, LX/FCM;

    invoke-direct {v0}, LX/FCM;-><init>()V

    .line 2225298
    iget-wide v4, v0, LX/FCM;->a:J

    .line 2225299
    const-string v2, "/t_mt_req"

    const-wide/16 v6, 0x0

    invoke-virtual/range {v1 .. v7}, LX/2gV;->a(Ljava/lang/String;[BJJ)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 2225300
    :try_start_2
    invoke-virtual {v1}, LX/2gV;->f()V

    .line 2225301
    :goto_0
    return v0

    .line 2225302
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/2gV;->f()V

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 2225303
    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method
