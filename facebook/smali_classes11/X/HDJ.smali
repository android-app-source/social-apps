.class public LX/HDJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/HDF;


# instance fields
.field private final a:LX/HMa;

.field private final b:LX/HC0;

.field public final c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;


# direct methods
.method public constructor <init>(LX/HMa;LX/HC0;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)V
    .locals 0
    .param p3    # Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2441119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2441120
    iput-object p1, p0, LX/HDJ;->a:LX/HMa;

    .line 2441121
    iput-object p2, p0, LX/HDJ;->b:LX/HC0;

    .line 2441122
    iput-object p3, p0, LX/HDJ;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    .line 2441123
    return-void
.end method


# virtual methods
.method public final a(Landroid/app/Activity;JLjava/lang/String;LX/HDH;LX/HBj;)V
    .locals 6

    .prologue
    .line 2441112
    sget-object v0, LX/HDH;->EDIT:LX/HDH;

    if-ne p5, v0, :cond_0

    .line 2441113
    iget-object v0, p0, LX/HDJ;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-interface {p6}, LX/HBj;->a()V

    .line 2441114
    iget-object v0, p0, LX/HDJ;->a:LX/HMa;

    iget-object v5, p0, LX/HDJ;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, LX/HMa;->a(Landroid/app/Activity;JLjava/lang/String;LX/9Y7;)V

    .line 2441115
    :goto_0
    return-void

    .line 2441116
    :cond_0
    sget-object v0, LX/HDH;->ADD:LX/HDH;

    if-ne p5, v0, :cond_1

    .line 2441117
    iget-object v0, p0, LX/HDJ;->b:LX/HC0;

    iget-object v1, p0, LX/HDJ;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v4

    move-object v1, p1

    move-wide v2, p2

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, LX/HC0;->a(Landroid/app/Activity;JLcom/facebook/graphql/enums/GraphQLPageActionType;LX/HBj;)V

    goto :goto_0

    .line 2441118
    :cond_1
    iget-object v0, p0, LX/HDJ;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    invoke-interface {p6}, LX/HBj;->b()V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "shouldShowEditSection"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2441124
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 2441111
    iget-object v0, p0, LX/HDJ;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/HDJ;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->PAGE_PREVIEW_ONLY_ACTION:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()I
    .locals 1
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation

    .prologue
    .line 2441109
    iget-object v0, p0, LX/HDJ;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    move-object v0, v0

    .line 2441110
    invoke-static {v0}, LX/HCF;->a(Lcom/facebook/graphql/enums/GraphQLPageActionType;)I

    move-result v0

    return v0
.end method

.method public final e()I
    .locals 2
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 2441105
    iget-object v0, p0, LX/HDJ;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v0

    .line 2441106
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_SERVICES:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne v0, v1, :cond_0

    .line 2441107
    const v0, 0x7f0836cc

    .line 2441108
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0836cd

    goto :goto_0
.end method
