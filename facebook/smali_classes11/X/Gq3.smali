.class public final LX/Gq3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Gq4;


# direct methods
.method public constructor <init>(LX/Gq4;)V
    .locals 0

    .prologue
    .line 2396117
    iput-object p1, p0, LX/Gq3;->a:LX/Gq4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x2

    const/4 v3, 0x1

    const v0, 0x130604b9

    invoke-static {v6, v3, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2396118
    iget-object v0, p0, LX/Gq3;->a:LX/Gq4;

    iget-object v0, v0, LX/Gq4;->f:LX/Gpu;

    if-eqz v0, :cond_0

    .line 2396119
    iget-object v0, p0, LX/Gq3;->a:LX/Gq4;

    iget-object v0, v0, LX/Gq4;->f:LX/Gpu;

    invoke-interface {v0, v2}, LX/Gpu;->setIsSelected(Z)V

    .line 2396120
    iget-object v0, p0, LX/Gq3;->a:LX/Gq4;

    iget-object v0, v0, LX/Gq4;->f:LX/Gpu;

    instance-of v0, v0, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;

    if-eqz v0, :cond_0

    .line 2396121
    iget-object v0, p0, LX/Gq3;->a:LX/Gq4;

    iget-object v0, v0, LX/Gq4;->c:LX/CIj;

    .line 2396122
    iput-boolean v2, v0, LX/CIj;->a:Z

    .line 2396123
    :cond_0
    iget-object v2, p0, LX/Gq3;->a:LX/Gq4;

    move-object v0, p1

    check-cast v0, LX/Gpu;

    .line 2396124
    iput-object v0, v2, LX/Gq4;->f:LX/Gpu;

    .line 2396125
    iget-object v0, p0, LX/Gq3;->a:LX/Gq4;

    iget-object v0, v0, LX/Gq4;->f:LX/Gpu;

    invoke-interface {v0, v3}, LX/Gpu;->setIsSelected(Z)V

    .line 2396126
    iget-object v0, p0, LX/Gq3;->a:LX/Gq4;

    iget-object v0, v0, LX/Gq4;->f:LX/Gpu;

    instance-of v0, v0, Lcom/facebook/instantshopping/view/widget/ColorPickerTextItemLayout;

    if-eqz v0, :cond_1

    .line 2396127
    iget-object v0, p0, LX/Gq3;->a:LX/Gq4;

    iget-object v0, v0, LX/Gq4;->c:LX/CIj;

    .line 2396128
    iput-boolean v3, v0, LX/CIj;->a:Z

    .line 2396129
    :cond_1
    move-object v0, p1

    .line 2396130
    check-cast v0, LX/Gpu;

    invoke-interface {v0}, LX/GoL;->getAction()LX/CHX;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2396131
    iget-object v0, p0, LX/Gq3;->a:LX/Gq4;

    iget-object v0, v0, LX/Gq4;->a:LX/GnF;

    iget-object v2, p0, LX/Gq3;->a:LX/Gq4;

    invoke-virtual {v2}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    check-cast p1, LX/Gpu;

    invoke-interface {p1}, LX/GoL;->getAction()LX/CHX;

    move-result-object v3

    iget-object v4, p0, LX/Gq3;->a:LX/Gq4;

    iget-object v4, v4, LX/Gq4;->g:LX/GoE;

    const/4 v5, 0x0

    invoke-virtual {v0, v2, v3, v4, v5}, LX/GnF;->a(Landroid/content/Context;LX/CHX;LX/GoE;Ljava/util/Map;)V

    .line 2396132
    :cond_2
    const v0, -0x70d09

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
