.class public LX/FrQ;
.super Landroid/widget/FrameLayout;
.source ""


# instance fields
.field public a:LX/0hB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Fv5;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/FvA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final d:Ljava/lang/String;

.field public e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public g:LX/FvG;

.field public h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public i:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

.field public j:Landroid/view/View;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2295526
    invoke-direct {p0, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2295527
    iput-object p1, p0, LX/FrQ;->d:Ljava/lang/String;

    .line 2295528
    const-class v0, LX/FrQ;

    invoke-static {v0, p0}, LX/FrQ;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2295529
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2295530
    const v0, 0x7f0314d9

    invoke-virtual {v1, v0, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v0, p0, LX/FrQ;->f:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2295531
    iget-object v0, p0, LX/FrQ;->f:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v2, LX/FrN;

    invoke-virtual {p0}, LX/FrQ;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/FrN;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2295532
    iget-object v0, p0, LX/FrQ;->f:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {p0, v0}, LX/FrQ;->addView(Landroid/view/View;)V

    .line 2295533
    const v0, 0x7f0314ec    # 1.742375E38f

    invoke-virtual {v1, v0, p0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, LX/FrQ;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2295534
    iget-object v0, p0, LX/FrQ;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setClickable(Z)V

    .line 2295535
    iget-object v0, p0, LX/FrQ;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2295536
    iget-object v0, p0, LX/FrQ;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const v1, 0x7f0a0114

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setBackgroundResource(I)V

    .line 2295537
    iget-object v0, p0, LX/FrQ;->h:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {p0, v0}, LX/FrQ;->addView(Landroid/view/View;)V

    .line 2295538
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v3

    check-cast p1, LX/FrQ;

    invoke-static {v3}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v1

    check-cast v1, LX/0hB;

    invoke-static {v3}, LX/Fv5;->a(LX/0QB;)LX/Fv5;

    move-result-object v2

    check-cast v2, LX/Fv5;

    const-class p0, LX/FvA;

    invoke-interface {v3, p0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/FvA;

    iput-object v1, p1, LX/FrQ;->a:LX/0hB;

    iput-object v2, p1, LX/FrQ;->b:LX/Fv5;

    iput-object v3, p1, LX/FrQ;->c:LX/FvA;

    return-void
.end method

.method public static c(LX/FrQ;)V
    .locals 3

    .prologue
    .line 2295539
    iget-object v0, p0, LX/FrQ;->i:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getMeasuredHeight()I

    move-result v1

    .line 2295540
    iget-object v0, p0, LX/FrQ;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 2295541
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-eq v1, v2, :cond_0

    .line 2295542
    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 2295543
    invoke-virtual {p0}, LX/FrQ;->requestLayout()V

    .line 2295544
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2295545
    iget-object v0, p0, LX/FrQ;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    if-nez v0, :cond_0

    .line 2295546
    iget-object v0, p0, LX/FrQ;->i:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    const v1, 0x7f0d2d71

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2295547
    const v1, 0x7f0314cc

    invoke-virtual {v0, v1}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2295548
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    iput-object v0, p0, LX/FrQ;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    .line 2295549
    :cond_0
    iget-object v0, p0, LX/FrQ;->e:Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;

    iget-object v1, p0, LX/FrQ;->d:Ljava/lang/String;

    iget-object v2, p0, LX/FrQ;->a:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->c()I

    move-result v2

    iget-object v3, p0, LX/FrQ;->i:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    .line 2295550
    iget p0, v3, LX/Ban;->c:I

    move v3, p0

    .line 2295551
    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/timeline/header/coverphoto/edit/CoverPhotoEditView;->a(Ljava/lang/String;II)V

    .line 2295552
    return-void
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 2295553
    iget-object v0, p0, LX/FrQ;->i:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    if-nez v0, :cond_0

    .line 2295554
    iget-object v0, p0, LX/FrQ;->g:LX/FvG;

    invoke-virtual {v0}, LX/FvG;->c()Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    move-result-object v0

    iput-object v0, p0, LX/FrQ;->i:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    .line 2295555
    :cond_0
    iget-object v0, p0, LX/FrQ;->i:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    if-eqz v0, :cond_1

    .line 2295556
    iget-object v0, p0, LX/FrQ;->i:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    invoke-virtual {v0, p1}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2295557
    invoke-virtual {p0}, LX/FrQ;->a()V

    .line 2295558
    invoke-static {p0}, LX/FrQ;->c(LX/FrQ;)V

    .line 2295559
    :cond_1
    return-void
.end method
