.class public LX/FMB;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/util/regex/Pattern;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;

.field private static final e:[Ljava/lang/String;

.field private static final f:[Ljava/lang/String;

.field private static final g:[Ljava/lang/String;

.field private static final h:[Ljava/lang/String;

.field private static final i:[Ljava/lang/String;

.field private static final j:[Ljava/lang/String;

.field private static final k:[Ljava/lang/String;

.field private static volatile u:LX/FMB;


# instance fields
.field private final l:LX/FNh;

.field public final m:Landroid/content/Context;

.field private final n:LX/2J0;

.field private final o:Lcom/facebook/messaging/model/messages/ParticipantInfo;

.field private final p:LX/FMb;

.field private final q:LX/FM6;

.field private final r:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation
.end field

.field private final s:LX/FMG;

.field private final t:LX/FN6;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2228925
    const-string v0, "(sticker:)(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/FMB;->a:Ljava/util/regex/Pattern;

    .line 2228926
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "transport_type"

    aput-object v1, v0, v3

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "thread_id"

    aput-object v1, v0, v5

    const-string v1, "date"

    aput-object v1, v0, v6

    const-string v1, "address"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "body"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "msg_box"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "st"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "m_size"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "sub"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "sub_cs"

    aput-object v2, v0, v1

    sput-object v0, LX/FMB;->b:[Ljava/lang/String;

    .line 2228927
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "transport_type"

    aput-object v1, v0, v3

    const-string v1, "_id"

    aput-object v1, v0, v4

    const-string v1, "thread_id"

    aput-object v1, v0, v5

    const-string v1, "date"

    aput-object v1, v0, v6

    const-string v1, "sub_id"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "address"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "body"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "type"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "msg_box"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "st"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "m_size"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "sub"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "sub_cs"

    aput-object v2, v0, v1

    sput-object v0, LX/FMB;->c:[Ljava/lang/String;

    .line 2228928
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "thread_id"

    aput-object v1, v0, v4

    const-string v1, "date"

    aput-object v1, v0, v5

    const-string v1, "address"

    aput-object v1, v0, v6

    const-string v1, "body"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "type"

    aput-object v2, v0, v1

    sput-object v0, LX/FMB;->d:[Ljava/lang/String;

    .line 2228929
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "thread_id"

    aput-object v1, v0, v4

    const-string v1, "date"

    aput-object v1, v0, v5

    const-string v1, "address"

    aput-object v1, v0, v6

    const-string v1, "body"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "sub_id"

    aput-object v2, v0, v1

    sput-object v0, LX/FMB;->e:[Ljava/lang/String;

    .line 2228930
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "thread_id"

    aput-object v1, v0, v4

    const-string v1, "date"

    aput-object v1, v0, v5

    const-string v1, "msg_box"

    aput-object v1, v0, v6

    const-string v1, "st"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "m_size"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "sub"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "sub_cs"

    aput-object v2, v0, v1

    sput-object v0, LX/FMB;->f:[Ljava/lang/String;

    .line 2228931
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "thread_id"

    aput-object v1, v0, v4

    const-string v1, "date"

    aput-object v1, v0, v5

    const-string v1, "msg_box"

    aput-object v1, v0, v6

    const-string v1, "st"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "m_size"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "sub"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "sub_cs"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "sub_id"

    aput-object v2, v0, v1

    sput-object v0, LX/FMB;->g:[Ljava/lang/String;

    .line 2228932
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "thread_id"

    aput-object v1, v0, v4

    const-string v1, "address"

    aput-object v1, v0, v5

    const-string v1, "body"

    aput-object v1, v0, v6

    const-string v1, "date"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "type"

    aput-object v2, v0, v1

    sput-object v0, LX/FMB;->h:[Ljava/lang/String;

    .line 2228933
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "thread_id"

    aput-object v1, v0, v4

    const-string v1, "address"

    aput-object v1, v0, v5

    const-string v1, "body"

    aput-object v1, v0, v6

    const-string v1, "date"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "sub_id"

    aput-object v2, v0, v1

    sput-object v0, LX/FMB;->i:[Ljava/lang/String;

    .line 2228934
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "thread_id"

    aput-object v1, v0, v4

    const-string v1, "date"

    aput-object v1, v0, v5

    const-string v1, "msg_box"

    aput-object v1, v0, v6

    const-string v1, "st"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "m_size"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "sub"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "sub_cs"

    aput-object v2, v0, v1

    sput-object v0, LX/FMB;->j:[Ljava/lang/String;

    .line 2228935
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "thread_id"

    aput-object v1, v0, v4

    const-string v1, "date"

    aput-object v1, v0, v5

    const-string v1, "msg_box"

    aput-object v1, v0, v6

    const-string v1, "st"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "m_size"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "sub"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "sub_cs"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "sub_id"

    aput-object v2, v0, v1

    sput-object v0, LX/FMB;->k:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/FNh;LX/2J0;LX/FMb;LX/FM6;LX/FMG;LX/FN6;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2228914
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2228915
    iput-object p1, p0, LX/FMB;->m:Landroid/content/Context;

    .line 2228916
    iput-object p2, p0, LX/FMB;->l:LX/FNh;

    .line 2228917
    iput-object p3, p0, LX/FMB;->n:LX/2J0;

    .line 2228918
    iput-object p4, p0, LX/FMB;->p:LX/FMb;

    .line 2228919
    iput-object p5, p0, LX/FMB;->q:LX/FM6;

    .line 2228920
    iput-object p6, p0, LX/FMB;->s:LX/FMG;

    .line 2228921
    iput-object p7, p0, LX/FMB;->t:LX/FN6;

    .line 2228922
    new-instance v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    new-instance v1, Lcom/facebook/user/model/UserKey;

    sget-object v2, LX/0XG;->EMAIL:LX/0XG;

    const-string v3, ""

    invoke-direct {v1, v2, v3}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    iput-object v0, p0, LX/FMB;->o:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2228923
    new-instance v0, LX/FMA;

    invoke-direct {v0, p0}, LX/FMA;-><init>(LX/FMB;)V

    iput-object v0, p0, LX/FMB;->r:Ljava/util/Comparator;

    .line 2228924
    return-void
.end method

.method private static a(Landroid/media/MediaMetadataRetriever;II)I
    .locals 7

    .prologue
    .line 2228908
    invoke-virtual {p0, p1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    .line 2228909
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2228910
    :goto_0
    return p2

    .line 2228911
    :cond_0
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p2

    goto :goto_0

    .line 2228912
    :catch_0
    move-exception v1

    .line 2228913
    const-string v2, "SmsMessageLoader"

    const-string v3, "wrong type for key %s : %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-static {v2, v1, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static a(JJ)J
    .locals 4

    .prologue
    .line 2228907
    invoke-static {p0, p1}, LX/6fa;->b(J)J

    move-result-wide v0

    const-wide/32 v2, 0xf4240

    rem-long v2, p2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method private static a(Landroid/media/MediaMetadataRetriever;IJ)J
    .locals 8

    .prologue
    .line 2228901
    invoke-virtual {p0, p1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    .line 2228902
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2228903
    :goto_0
    return-wide p2

    .line 2228904
    :cond_0
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p2

    goto :goto_0

    .line 2228905
    :catch_0
    move-exception v1

    .line 2228906
    const-string v2, "SmsMessageLoader"

    const-string v3, "wrong type for key %s : %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    invoke-static {v2, v1, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private a(LX/EdV;LX/6f7;)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/EdV;",
            "LX/6f7;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/ui/media/attachments/MediaResource;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2228866
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2228867
    iget-object v0, p1, LX/EdV;->b:LX/EdY;

    move-object v4, v0

    .line 2228868
    invoke-virtual {v4}, LX/EdY;->b()I

    move-result v5

    move v1, v2

    .line 2228869
    :goto_0
    if-ge v1, v5, :cond_5

    .line 2228870
    :try_start_0
    invoke-virtual {v4, v1}, LX/EdY;->a(I)LX/Edg;

    move-result-object v0

    .line 2228871
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/Edg;->g()[B

    move-result-object v6

    if-nez v6, :cond_1

    .line 2228872
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2228873
    :cond_1
    new-instance v6, Ljava/lang/String;

    invoke-virtual {v0}, LX/Edg;->g()[B

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>([B)V

    .line 2228874
    const-string v7, "text/plain"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2228875
    new-instance v6, Ljava/lang/String;

    .line 2228876
    iget-object v7, v0, LX/Edg;->f:[B

    move-object v0, v7

    .line 2228877
    invoke-direct {v6, v0}, Ljava/lang/String;-><init>([B)V

    .line 2228878
    iput-object v6, p2, LX/6f7;->f:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2228879
    goto :goto_1

    .line 2228880
    :catch_0
    move-exception v0

    .line 2228881
    const-string v6, "SmsMessageLoader"

    const-string v7, "Failed to load part %d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-static {v6, v0, v7, v8}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 2228882
    :cond_2
    :try_start_1
    const-string v7, "application/smil"

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2228883
    invoke-virtual {v0}, LX/Edg;->i()[B

    move-result-object v0

    invoke-static {v0}, LX/FMB;->a([B)Ljava/lang/String;

    move-result-object v0

    .line 2228884
    if-eqz v0, :cond_0

    .line 2228885
    iput-object v0, p2, LX/6f7;->k:Ljava/lang/String;

    .line 2228886
    goto :goto_1

    .line 2228887
    :cond_3
    invoke-virtual {v0}, LX/Edg;->i()[B

    move-result-object v7

    invoke-static {v7}, LX/FMB;->a([B)Ljava/lang/String;

    move-result-object v7

    .line 2228888
    invoke-static {v6}, LX/EdQ;->b(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    if-eqz v7, :cond_4

    .line 2228889
    iput-object v7, p2, LX/6f7;->k:Ljava/lang/String;

    .line 2228890
    goto :goto_1

    .line 2228891
    :cond_4
    invoke-static {p0, v0}, LX/FMB;->a(LX/FMB;LX/Edg;)Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 2228892
    if-eqz v0, :cond_0

    .line 2228893
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 2228894
    :cond_5
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2228895
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    .line 2228896
    iget-object v1, p2, LX/6f7;->k:Ljava/lang/String;

    move-object v1, v1

    .line 2228897
    if-nez v1, :cond_6

    .line 2228898
    invoke-static {v0}, Lcom/facebook/messaging/model/mms/MmsData;->a(LX/0Px;)Lcom/facebook/messaging/model/mms/MmsData;

    move-result-object v1

    .line 2228899
    invoke-virtual {p2, v1}, LX/6f7;->a(Lcom/facebook/messaging/model/mms/MmsData;)LX/6f7;

    .line 2228900
    :cond_6
    return-object v0
.end method

.method public static a(LX/0QB;)LX/FMB;
    .locals 11

    .prologue
    .line 2228853
    sget-object v0, LX/FMB;->u:LX/FMB;

    if-nez v0, :cond_1

    .line 2228854
    const-class v1, LX/FMB;

    monitor-enter v1

    .line 2228855
    :try_start_0
    sget-object v0, LX/FMB;->u:LX/FMB;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2228856
    if-eqz v2, :cond_0

    .line 2228857
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2228858
    new-instance v3, LX/FMB;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/FNh;->a(LX/0QB;)LX/FNh;

    move-result-object v5

    check-cast v5, LX/FNh;

    const-class v6, LX/2J0;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/2J0;

    invoke-static {v0}, LX/FMb;->b(LX/0QB;)LX/FMb;

    move-result-object v7

    check-cast v7, LX/FMb;

    invoke-static {v0}, LX/FM6;->a(LX/0QB;)LX/FM6;

    move-result-object v8

    check-cast v8, LX/FM6;

    invoke-static {v0}, LX/FMG;->b(LX/0QB;)LX/FMG;

    move-result-object v9

    check-cast v9, LX/FMG;

    invoke-static {v0}, LX/FN6;->b(LX/0QB;)LX/FN6;

    move-result-object v10

    check-cast v10, LX/FN6;

    invoke-direct/range {v3 .. v10}, LX/FMB;-><init>(Landroid/content/Context;LX/FNh;LX/2J0;LX/FMb;LX/FM6;LX/FMG;LX/FN6;)V

    .line 2228859
    move-object v0, v3

    .line 2228860
    sput-object v0, LX/FMB;->u:LX/FMB;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2228861
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2228862
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2228863
    :cond_1
    sget-object v0, LX/FMB;->u:LX/FMB;

    return-object v0

    .line 2228864
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2228865
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/FMB;Landroid/database/Cursor;Ljava/util/Map;)Lcom/facebook/messaging/model/messages/Message;
    .locals 16
    .param p1    # Landroid/database/Cursor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            ">;)",
            "Lcom/facebook/messaging/model/messages/Message;"
        }
    .end annotation

    .prologue
    .line 2228825
    const-string v2, "_id"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    .line 2228826
    const-string v2, "thread_id"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v6

    .line 2228827
    const-string v2, "address"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2228828
    const-string v3, "body"

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2228829
    const-string v3, "date"

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v10

    .line 2228830
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FMB;->s:LX/FMG;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, LX/FMG;->a(Landroid/database/Cursor;)I

    move-result v9

    .line 2228831
    const-string v3, "type"

    move-object/from16 v0, p1

    invoke-static {v0, v3}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v12

    .line 2228832
    invoke-static {v10, v11, v4, v5}, LX/FMB;->a(JJ)J

    move-result-wide v14

    .line 2228833
    invoke-static {v12}, LX/554;->a(I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2228834
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FMB;->l:LX/FNh;

    invoke-virtual {v2}, LX/FNh;->a()Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v2

    .line 2228835
    :goto_0
    if-nez v2, :cond_0

    .line 2228836
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FMB;->o:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2228837
    :cond_0
    invoke-static {v4, v5}, LX/FMQ;->a(J)Ljava/lang/String;

    move-result-object v5

    .line 2228838
    const/4 v4, 0x0

    .line 2228839
    const/4 v3, 0x0

    .line 2228840
    const/4 v13, 0x5

    if-ne v12, v13, :cond_6

    .line 2228841
    sget-object v4, LX/2uW;->FAILED_SEND:LX/2uW;

    .line 2228842
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FMB;->q:LX/FM6;

    invoke-virtual {v3, v5}, LX/FM6;->a(Ljava/lang/String;)LX/FMM;

    move-result-object v3

    .line 2228843
    move-object/from16 v0, p0

    iget-object v12, v0, LX/FMB;->p:LX/FMb;

    if-nez v3, :cond_1

    sget-object v3, LX/FMM;->GENERIC:LX/FMM;

    :cond_1
    invoke-virtual {v12, v3}, LX/FMb;->a(LX/FMM;)Lcom/facebook/messaging/model/send/SendError;

    move-result-object v3

    .line 2228844
    :cond_2
    :goto_1
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v12

    invoke-virtual {v12, v5}, LX/6f7;->a(Ljava/lang/String;)LX/6f7;

    move-result-object v12

    invoke-virtual {v12, v5}, LX/6f7;->d(Ljava/lang/String;)LX/6f7;

    move-result-object v5

    invoke-static {v6, v7}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/6f7;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/6f7;

    move-result-object v5

    invoke-virtual {v5, v8}, LX/6f7;->b(Ljava/lang/String;)LX/6f7;

    move-result-object v5

    invoke-virtual {v5, v10, v11}, LX/6f7;->a(J)LX/6f7;

    move-result-object v5

    invoke-virtual {v5, v14, v15}, LX/6f7;->c(J)LX/6f7;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/6f7;

    move-result-object v2

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, LX/6f7;->b(Z)LX/6f7;

    move-result-object v2

    sget-object v5, Lcom/facebook/messaging/model/messages/Publicity;->a:Lcom/facebook/messaging/model/messages/Publicity;

    invoke-virtual {v2, v5}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Publicity;)LX/6f7;

    move-result-object v2

    const-string v5, "sms"

    invoke-virtual {v2, v5}, LX/6f7;->e(Ljava/lang/String;)LX/6f7;

    move-result-object v2

    invoke-virtual {v2, v9}, LX/6f7;->a(I)LX/6f7;

    move-result-object v2

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, LX/6f7;->a(Z)LX/6f7;

    move-result-object v2

    .line 2228845
    if-eqz v4, :cond_3

    .line 2228846
    invoke-virtual {v2, v4}, LX/6f7;->a(LX/2uW;)LX/6f7;

    .line 2228847
    :cond_3
    if-eqz v3, :cond_4

    .line 2228848
    invoke-virtual {v2, v3}, LX/6f7;->a(Lcom/facebook/messaging/model/send/SendError;)LX/6f7;

    .line 2228849
    :cond_4
    invoke-virtual {v2}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    return-object v2

    .line 2228850
    :cond_5
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v2, v1}, LX/FMB;->a(Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v2

    goto :goto_0

    .line 2228851
    :cond_6
    const/4 v13, 0x6

    if-eq v12, v13, :cond_7

    const/4 v13, 0x4

    if-ne v12, v13, :cond_2

    .line 2228852
    :cond_7
    sget-object v4, LX/2uW;->PENDING_SEND:LX/2uW;

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/messaging/model/messages/ParticipantInfo;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            ">;)",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2228520
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2228521
    :cond_0
    :goto_0
    return-object v0

    .line 2228522
    :cond_1
    if-eqz p2, :cond_2

    .line 2228523
    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2228524
    :cond_2
    if-nez v0, :cond_3

    .line 2228525
    iget-object v0, p0, LX/FMB;->l:LX/FNh;

    invoke-virtual {v0, p1}, LX/FNh;->a(Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-static {v0}, LX/FNh;->a(Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v0

    .line 2228526
    :cond_3
    if-eqz p2, :cond_0

    .line 2228527
    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static a(LX/FMB;LX/Edg;)Lcom/facebook/ui/media/attachments/MediaResource;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2228734
    new-instance v0, LX/5zn;

    invoke-direct {v0}, LX/5zn;-><init>()V

    .line 2228735
    new-instance v1, Ljava/lang/String;

    invoke-virtual {p1}, LX/Edg;->g()[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    .line 2228736
    iget-object v2, p1, LX/Edg;->e:Landroid/net/Uri;

    move-object v2, v2

    .line 2228737
    if-eqz v2, :cond_0

    .line 2228738
    iput-object v2, v0, LX/5zn;->b:Landroid/net/Uri;

    .line 2228739
    move-object v3, v0

    .line 2228740
    iput-object v2, v3, LX/5zn;->f:Landroid/net/Uri;

    .line 2228741
    :cond_0
    invoke-static {v1}, LX/EdQ;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2228742
    const/4 v8, 0x1

    .line 2228743
    sget-object v4, LX/2MK;->PHOTO:LX/2MK;

    .line 2228744
    iput-object v4, v0, LX/5zn;->c:LX/2MK;

    .line 2228745
    iput-object v1, v0, LX/5zn;->q:Ljava/lang/String;

    .line 2228746
    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 2228747
    iput-boolean v8, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 2228748
    :try_start_0
    iget-object v5, p0, LX/FMB;->m:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "r"

    invoke-virtual {v5, v2, v6}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v5

    .line 2228749
    invoke-virtual {v5}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v6, v7, v4}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 2228750
    iget v6, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 2228751
    iput v6, v0, LX/5zn;->k:I

    .line 2228752
    iget v4, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 2228753
    iput v4, v0, LX/5zn;->j:I

    .line 2228754
    invoke-virtual {v5}, Landroid/os/ParcelFileDescriptor;->getStatSize()J

    move-result-wide v6

    .line 2228755
    iput-wide v6, v0, LX/5zn;->r:J

    .line 2228756
    if-eqz v5, :cond_1

    .line 2228757
    invoke-virtual {v5}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2228758
    :cond_1
    :goto_0
    iget-object v1, v0, LX/5zn;->c:LX/2MK;

    move-object v1, v1

    .line 2228759
    if-eqz v1, :cond_a

    .line 2228760
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 2228761
    :goto_1
    return-object v0

    .line 2228762
    :cond_2
    invoke-static {v1}, LX/EdQ;->d(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2228763
    sget-object v3, LX/2MK;->VIDEO:LX/2MK;

    invoke-static {p0, v0, v2, v3, v1}, LX/FMB;->a(LX/FMB;LX/5zn;Landroid/net/Uri;LX/2MK;Ljava/lang/String;)V

    goto :goto_0

    .line 2228764
    :cond_3
    invoke-static {v1}, LX/EdQ;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2228765
    sget-object v3, LX/2MK;->AUDIO:LX/2MK;

    invoke-static {p0, v0, v2, v3, v1}, LX/FMB;->a(LX/FMB;LX/5zn;Landroid/net/Uri;LX/2MK;Ljava/lang/String;)V

    goto :goto_0

    .line 2228766
    :cond_4
    const-string v2, "text/x-vCard"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    move v2, v2

    .line 2228767
    if-eqz v2, :cond_7

    .line 2228768
    new-instance v1, Ljava/lang/String;

    .line 2228769
    iget-object v2, p1, LX/Edg;->f:[B

    move-object v2, v2

    .line 2228770
    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    .line 2228771
    iget-object v2, p1, LX/Edg;->e:Landroid/net/Uri;

    move-object v2, v2

    .line 2228772
    iput-object v2, v0, LX/5zn;->z:Landroid/net/Uri;

    .line 2228773
    move-object v2, v0

    .line 2228774
    sget-object v3, LX/2MK;->OTHER:LX/2MK;

    .line 2228775
    iput-object v3, v2, LX/5zn;->c:LX/2MK;

    .line 2228776
    move-object v2, v2

    .line 2228777
    const-string v3, "text/x-vcard"

    .line 2228778
    iput-object v3, v2, LX/5zn;->q:Ljava/lang/String;

    .line 2228779
    const/4 p1, -0x1

    const/4 v2, 0x0

    .line 2228780
    if-nez v1, :cond_b

    .line 2228781
    :cond_5
    :goto_2
    move-object v1, v2

    .line 2228782
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2228783
    iget-object v1, p0, LX/FMB;->m:Landroid/content/Context;

    const v2, 0x7f0808fd

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2228784
    :cond_6
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2228785
    iput-object v1, v0, LX/5zn;->b:Landroid/net/Uri;

    .line 2228786
    goto :goto_0

    .line 2228787
    :cond_7
    const-string v2, "text/x-vCalendar"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    move v1, v2

    .line 2228788
    if-eqz v1, :cond_1

    .line 2228789
    new-instance v1, Ljava/lang/String;

    .line 2228790
    iget-object v2, p1, LX/Edg;->f:[B

    move-object v2, v2

    .line 2228791
    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([B)V

    .line 2228792
    iget-object v2, p1, LX/Edg;->e:Landroid/net/Uri;

    move-object v2, v2

    .line 2228793
    iput-object v2, v0, LX/5zn;->z:Landroid/net/Uri;

    .line 2228794
    move-object v2, v0

    .line 2228795
    sget-object v3, LX/2MK;->OTHER:LX/2MK;

    .line 2228796
    iput-object v3, v2, LX/5zn;->c:LX/2MK;

    .line 2228797
    move-object v2, v2

    .line 2228798
    const-string v3, "text/x-vcalendar"

    .line 2228799
    iput-object v3, v2, LX/5zn;->q:Ljava/lang/String;

    .line 2228800
    move-object v2, v2

    .line 2228801
    iget-object v3, p0, LX/FMB;->m:Landroid/content/Context;

    const v4, 0x7f0808fe

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2228802
    iput-object v3, v2, LX/5zn;->b:Landroid/net/Uri;

    .line 2228803
    const/4 p1, -0x1

    const/4 v2, 0x0

    .line 2228804
    if-nez v1, :cond_c

    .line 2228805
    :cond_8
    :goto_3
    move-object v1, v2

    .line 2228806
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2228807
    iget-object v1, p0, LX/FMB;->m:Landroid/content/Context;

    const v2, 0x7f0808fe

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2228808
    :cond_9
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2228809
    iput-object v1, v0, LX/5zn;->b:Landroid/net/Uri;

    .line 2228810
    goto/16 :goto_0

    .line 2228811
    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 2228812
    :catch_0
    move-exception v4

    .line 2228813
    const-string v5, "SmsMessageLoader"

    const-string v6, "failed to open file descriptor: %s"

    new-array v7, v8, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v4, v6, v7}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 2228814
    :cond_b
    const-string v3, "FN:"

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 2228815
    if-eq v3, p1, :cond_5

    .line 2228816
    const/16 v4, 0xa

    invoke-virtual {v1, v4, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    .line 2228817
    if-eq v4, p1, :cond_5

    .line 2228818
    add-int/lit8 v2, v3, 0x3

    invoke-virtual {v1, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 2228819
    :cond_c
    const-string v3, "SUMMARY"

    invoke-virtual {v1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 2228820
    if-eq v3, p1, :cond_8

    .line 2228821
    const/16 v4, 0x3a

    invoke-virtual {v1, v4, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    .line 2228822
    const/16 v4, 0xa

    invoke-virtual {v1, v4, v3}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    .line 2228823
    if-eq v4, p1, :cond_8

    .line 2228824
    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto :goto_3
.end method

.method private static a([B)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2228728
    if-eqz p0, :cond_0

    .line 2228729
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p0}, Ljava/lang/String;-><init>([B)V

    .line 2228730
    sget-object v1, LX/FMB;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 2228731
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2228732
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    .line 2228733
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(LX/FMB;LX/5zn;Landroid/net/Uri;LX/2MK;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 2228706
    sget-object v1, LX/2MK;->AUDIO:LX/2MK;

    if-eq p3, v1, :cond_0

    sget-object v1, LX/2MK;->VIDEO:LX/2MK;

    if-ne p3, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2228707
    iput-object p3, p1, LX/5zn;->c:LX/2MK;

    .line 2228708
    iput-object p4, p1, LX/5zn;->q:Ljava/lang/String;

    .line 2228709
    :try_start_0
    new-instance v0, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v0}, Landroid/media/MediaMetadataRetriever;-><init>()V

    .line 2228710
    iget-object v1, p0, LX/FMB;->m:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "r"

    invoke-virtual {v1, p2, v2}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 2228711
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/io/FileDescriptor;)V

    .line 2228712
    const/16 v2, 0x9

    const-wide/16 v4, 0x0

    invoke-static {v0, v2, v4, v5}, LX/FMB;->a(Landroid/media/MediaMetadataRetriever;IJ)J

    move-result-wide v2

    .line 2228713
    iput-wide v2, p1, LX/5zn;->i:J

    .line 2228714
    sget-object v2, LX/2MK;->VIDEO:LX/2MK;

    if-ne p3, v2, :cond_2

    .line 2228715
    const/16 v2, 0x12

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, LX/FMB;->a(Landroid/media/MediaMetadataRetriever;II)I

    move-result v2

    .line 2228716
    iput v2, p1, LX/5zn;->j:I

    .line 2228717
    const/16 v2, 0x13

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, LX/FMB;->a(Landroid/media/MediaMetadataRetriever;II)I

    move-result v2

    .line 2228718
    iput v2, p1, LX/5zn;->k:I

    .line 2228719
    const/16 v2, 0x18

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, LX/FMB;->a(Landroid/media/MediaMetadataRetriever;II)I

    move-result v2

    .line 2228720
    invoke-static {v2}, LX/47e;->a(I)LX/47d;

    move-result-object v2

    .line 2228721
    iput-object v2, p1, LX/5zn;->l:LX/47d;

    .line 2228722
    :cond_2
    invoke-virtual {v0}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 2228723
    if-eqz v1, :cond_3

    .line 2228724
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2228725
    :cond_3
    :goto_0
    return-void

    .line 2228726
    :catch_0
    move-exception v0

    .line 2228727
    const-string v1, "SmsMessageLoader"

    const-string v2, "Failed to extract meta data"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private b(JIJ)LX/0Px;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JIJ)",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2228661
    const-string v0, "thread_id"

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->a(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v8

    .line 2228662
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 2228663
    const/4 v7, 0x0

    .line 2228664
    :try_start_0
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v5

    .line 2228665
    invoke-virtual {v5, v8}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 2228666
    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-lez v0, :cond_0

    .line 2228667
    const-string v0, "date"

    invoke-static/range {p4 .. p5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->c(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 2228668
    :cond_0
    iget-object v0, p0, LX/FMB;->s:LX/FMG;

    sget-object v1, LX/554;->a:Landroid/net/Uri;

    sget-object v2, LX/FMB;->d:[Ljava/lang/String;

    sget-object v3, LX/FMB;->e:[Ljava/lang/String;

    invoke-virtual {v5}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v10, "date DESC LIMIT "

    invoke-direct {v6, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, LX/FMG;->a(Landroid/net/Uri;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_5

    move-result-object v1

    .line 2228669
    if-eqz v1, :cond_8

    .line 2228670
    :try_start_1
    invoke-static {v1}, LX/2J0;->a(Landroid/database/Cursor;)LX/6LS;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_6

    move-result-object v1

    .line 2228671
    :try_start_2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2228672
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2228673
    invoke-static {p0, v1, v0}, LX/FMB;->a(LX/FMB;Landroid/database/Cursor;Ljava/util/Map;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_7

    goto :goto_0

    .line 2228674
    :catch_0
    move-exception v0

    .line 2228675
    :goto_1
    :try_start_3
    const-string v2, "SmsMessageLoader"

    const-string v3, "Failed to fetch SMS messages for thread %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2228676
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2228677
    :catchall_0
    move-exception v0

    move-object v7, v1

    :goto_2
    if-eqz v7, :cond_1

    .line 2228678
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 2228679
    :cond_1
    throw v0

    :cond_2
    move-object v7, v1

    .line 2228680
    :goto_3
    if-eqz v7, :cond_3

    .line 2228681
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 2228682
    const/4 v7, 0x0

    .line 2228683
    :cond_3
    :try_start_4
    invoke-static {}, LX/0uu;->a()LX/0uw;

    move-result-object v5

    .line 2228684
    invoke-virtual {v5, v8}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 2228685
    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-lez v0, :cond_4

    .line 2228686
    const-string v0, "date"

    const-wide/16 v2, 0x3e8

    div-long v2, p4, v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0uu;->c(Ljava/lang/String;Ljava/lang/String;)LX/0ux;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0uw;->a(LX/0ux;)LX/0uw;

    .line 2228687
    :cond_4
    iget-object v0, p0, LX/FMB;->s:LX/FMG;

    sget-object v1, LX/2UG;->a:Landroid/net/Uri;

    sget-object v2, LX/FMB;->f:[Ljava/lang/String;

    sget-object v3, LX/FMB;->g:[Ljava/lang/String;

    invoke-virtual {v5}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "date DESC LIMIT "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, LX/FMG;->a(Landroid/net/Uri;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-object v1

    .line 2228688
    if-eqz v1, :cond_6

    .line 2228689
    :try_start_5
    invoke-static {v1}, LX/2J0;->a(Landroid/database/Cursor;)LX/6LS;
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move-result-object v1

    .line 2228690
    :try_start_6
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2228691
    :goto_4
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2228692
    invoke-static {p0, v1, v0}, LX/FMB;->b(LX/FMB;Landroid/database/Cursor;Ljava/util/Map;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    goto :goto_4

    .line 2228693
    :catch_1
    move-exception v0

    .line 2228694
    :goto_5
    :try_start_7
    const-string v2, "SmsMessageLoader"

    const-string v3, "Failed to fetch MMS messages for thread %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2228695
    invoke-static {v0}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 2228696
    :catchall_1
    move-exception v0

    move-object v7, v1

    :goto_6
    if-eqz v7, :cond_5

    .line 2228697
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0

    .line 2228698
    :cond_6
    if-eqz v1, :cond_7

    .line 2228699
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2228700
    :cond_7
    iget-object v0, p0, LX/FMB;->r:Ljava/util/Comparator;

    invoke-static {v9, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2228701
    const/4 v0, 0x0

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1, p3}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-interface {v9, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0

    .line 2228702
    :catchall_2
    move-exception v0

    goto :goto_6

    :catchall_3
    move-exception v0

    move-object v7, v1

    goto :goto_6

    :catchall_4
    move-exception v0

    move-object v7, v1

    goto :goto_6

    .line 2228703
    :catch_2
    move-exception v0

    move-object v1, v7

    goto :goto_5

    .line 2228704
    :catchall_5
    move-exception v0

    goto/16 :goto_2

    :catchall_6
    move-exception v0

    move-object v7, v1

    goto/16 :goto_2

    :catchall_7
    move-exception v0

    move-object v7, v1

    goto/16 :goto_2

    .line 2228705
    :catch_3
    move-exception v0

    move-object v1, v7

    goto/16 :goto_1

    :cond_8
    move-object v7, v1

    goto/16 :goto_3
.end method

.method private static b(LX/FMB;Landroid/database/Cursor;Ljava/util/Map;)Lcom/facebook/messaging/model/messages/Message;
    .locals 16
    .param p1    # Landroid/database/Cursor;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/messaging/model/messages/ParticipantInfo;",
            ">;)",
            "Lcom/facebook/messaging/model/messages/Message;"
        }
    .end annotation

    .prologue
    .line 2228596
    const-string v2, "_id"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v2

    .line 2228597
    sget-object v4, LX/2UG;->a:Landroid/net/Uri;

    invoke-static {v4, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    .line 2228598
    const-string v5, "thread_id"

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v6

    .line 2228599
    const-string v5, "date"

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/6LT;->b(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    .line 2228600
    invoke-static {v8, v9, v2, v3}, LX/FMB;->a(JJ)J

    move-result-wide v10

    .line 2228601
    const-string v5, "msg_box"

    move-object/from16 v0, p1

    invoke-static {v0, v5}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v5

    .line 2228602
    const-string v12, "st"

    move-object/from16 v0, p1

    invoke-static {v0, v12}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v12

    .line 2228603
    move-object/from16 v0, p0

    iget-object v13, v0, LX/FMB;->s:LX/FMG;

    move-object/from16 v0, p1

    invoke-virtual {v13, v0}, LX/FMG;->a(Landroid/database/Cursor;)I

    move-result v13

    .line 2228604
    invoke-static {v2, v3}, LX/FMQ;->b(J)Ljava/lang/String;

    move-result-object v14

    .line 2228605
    const/4 v3, 0x0

    .line 2228606
    const/4 v2, 0x0

    .line 2228607
    const/4 v15, 0x4

    if-ne v5, v15, :cond_1

    .line 2228608
    const/16 v3, 0x87

    if-ne v12, v3, :cond_d

    .line 2228609
    sget-object v3, LX/2uW;->FAILED_SEND:LX/2uW;

    .line 2228610
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FMB;->q:LX/FM6;

    invoke-virtual {v2, v14}, LX/FM6;->a(Ljava/lang/String;)LX/FMM;

    move-result-object v2

    .line 2228611
    move-object/from16 v0, p0

    iget-object v5, v0, LX/FMB;->p:LX/FMb;

    if-nez v2, :cond_0

    sget-object v2, LX/FMM;->GENERIC:LX/FMM;

    :cond_0
    invoke-virtual {v5, v2}, LX/FMb;->b(LX/FMM;)Lcom/facebook/messaging/model/send/SendError;

    move-result-object v2

    .line 2228612
    :cond_1
    :goto_0
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v5

    invoke-virtual {v5, v14}, LX/6f7;->a(Ljava/lang/String;)LX/6f7;

    move-result-object v5

    invoke-virtual {v5, v14}, LX/6f7;->d(Ljava/lang/String;)LX/6f7;

    move-result-object v5

    invoke-static {v6, v7}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/6f7;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/6f7;

    move-result-object v5

    invoke-virtual {v5, v8, v9}, LX/6f7;->a(J)LX/6f7;

    move-result-object v5

    invoke-virtual {v5, v10, v11}, LX/6f7;->c(J)LX/6f7;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, LX/6f7;->b(Z)LX/6f7;

    move-result-object v5

    sget-object v6, Lcom/facebook/messaging/model/messages/Publicity;->a:Lcom/facebook/messaging/model/messages/Publicity;

    invoke-virtual {v5, v6}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Publicity;)LX/6f7;

    move-result-object v5

    const-string v6, "sms"

    invoke-virtual {v5, v6}, LX/6f7;->e(Ljava/lang/String;)LX/6f7;

    move-result-object v5

    invoke-virtual {v5, v13}, LX/6f7;->a(I)LX/6f7;

    move-result-object v5

    .line 2228613
    if-eqz v3, :cond_2

    .line 2228614
    invoke-virtual {v5, v3}, LX/6f7;->a(LX/2uW;)LX/6f7;

    .line 2228615
    :cond_2
    if-eqz v2, :cond_3

    .line 2228616
    invoke-virtual {v5, v2}, LX/6f7;->a(Lcom/facebook/messaging/model/send/SendError;)LX/6f7;

    .line 2228617
    :cond_3
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FMB;->m:Landroid/content/Context;

    invoke-static {v2}, LX/Edh;->a(Landroid/content/Context;)LX/Edh;

    move-result-object v2

    .line 2228618
    invoke-virtual {v2, v4}, LX/Edh;->a(Landroid/net/Uri;)LX/EdM;

    move-result-object v2

    .line 2228619
    invoke-virtual {v2}, LX/EdM;->b()I

    move-result v6

    .line 2228620
    invoke-virtual {v2}, LX/EdM;->c()LX/EdS;

    move-result-object v3

    if-nez v3, :cond_e

    const/4 v3, 0x0

    .line 2228621
    :goto_1
    const/16 v7, 0x82

    if-ne v7, v6, :cond_f

    .line 2228622
    check-cast v2, LX/EdW;

    .line 2228623
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v3, v1}, LX/FMB;->a(Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v3

    invoke-virtual {v5, v3}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/6f7;

    .line 2228624
    invoke-virtual {v2}, LX/EdW;->e()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    invoke-virtual {v2}, LX/EdW;->f()J

    move-result-wide v2

    invoke-static {v6, v7, v2, v3}, Lcom/facebook/messaging/model/mms/MmsData;->a(JJ)Lcom/facebook/messaging/model/mms/MmsData;

    move-result-object v2

    .line 2228625
    invoke-virtual {v5, v2}, LX/6f7;->a(Lcom/facebook/messaging/model/mms/MmsData;)LX/6f7;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2228626
    :cond_4
    :goto_2
    invoke-virtual {v5}, LX/6f7;->e()Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v2

    if-nez v2, :cond_5

    .line 2228627
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FMB;->o:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v5, v2}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/6f7;

    .line 2228628
    :cond_5
    invoke-virtual {v5}, LX/6f7;->e()Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v2}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v2

    sget-object v3, LX/0XG;->EMAIL:LX/0XG;

    if-eq v2, v3, :cond_6

    invoke-virtual {v5}, LX/6f7;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {v5}, LX/6f7;->L()Lcom/facebook/messaging/model/mms/MmsData;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2228629
    :cond_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2228630
    const-string v2, "sub"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2228631
    const-string v4, "sub_cs"

    move-object/from16 v0, p1

    invoke-static {v0, v4}, LX/6LT;->a(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v4

    .line 2228632
    if-eqz v4, :cond_7

    if-eqz v2, :cond_7

    .line 2228633
    new-instance v6, LX/EdS;

    invoke-static {v2}, LX/Edh;->a(Ljava/lang/String;)[B

    move-result-object v2

    invoke-direct {v6, v4, v2}, LX/EdS;-><init>(I[B)V

    invoke-virtual {v6}, LX/EdS;->c()Ljava/lang/String;

    move-result-object v2

    .line 2228634
    :cond_7
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 2228635
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2228636
    :cond_8
    invoke-virtual {v5}, LX/6f7;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 2228637
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_9

    .line 2228638
    const-string v2, "\n\n"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2228639
    :cond_9
    invoke-virtual {v5}, LX/6f7;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2228640
    :cond_a
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/6f7;->b(Ljava/lang/String;)LX/6f7;

    .line 2228641
    :cond_b
    const/4 v2, 0x0

    .line 2228642
    invoke-virtual {v5}, LX/6f7;->L()Lcom/facebook/messaging/model/mms/MmsData;

    move-result-object v3

    if-eqz v3, :cond_13

    invoke-virtual {v5}, LX/6f7;->L()Lcom/facebook/messaging/model/mms/MmsData;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_13

    .line 2228643
    invoke-virtual {v5}, LX/6f7;->L()Lcom/facebook/messaging/model/mms/MmsData;

    move-result-object v2

    iget-object v2, v2, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v2}, LX/5zs;->fromOrNull(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zs;

    move-result-object v2

    if-eqz v2, :cond_12

    const/4 v2, 0x1

    .line 2228644
    :cond_c
    :goto_3
    if-nez v2, :cond_14

    const/4 v2, 0x1

    :goto_4
    invoke-virtual {v5, v2}, LX/6f7;->a(Z)LX/6f7;

    .line 2228645
    invoke-virtual {v5}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    return-object v2

    .line 2228646
    :cond_d
    sget-object v3, LX/2uW;->PENDING_SEND:LX/2uW;

    goto/16 :goto_0

    .line 2228647
    :cond_e
    :try_start_1
    invoke-virtual {v2}, LX/EdM;->c()LX/EdS;

    move-result-object v3

    invoke-virtual {v3}, LX/EdS;->c()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 2228648
    :cond_f
    const/16 v7, 0x80

    if-eq v7, v6, :cond_10

    const/16 v7, 0x84

    if-ne v7, v6, :cond_4

    .line 2228649
    :cond_10
    check-cast v2, LX/EdV;

    .line 2228650
    const/16 v7, 0x84

    if-ne v7, v6, :cond_11

    .line 2228651
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v3, v1}, LX/FMB;->a(Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v3

    invoke-virtual {v5, v3}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/6f7;

    .line 2228652
    :goto_5
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v5}, LX/FMB;->a(LX/EdV;LX/6f7;)LX/0Px;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_2

    .line 2228653
    :catch_0
    move-exception v2

    .line 2228654
    const-string v3, "SmsMessageLoader"

    const-string v6, "failed to load mms %s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v8

    invoke-static {v3, v2, v6, v7}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2228655
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FMB;->m:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0808f2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/6f7;->b(Ljava/lang/String;)LX/6f7;

    goto/16 :goto_2

    .line 2228656
    :cond_11
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FMB;->l:LX/FNh;

    invoke-virtual {v3}, LX/FNh;->a()Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v3

    invoke-virtual {v5, v3}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/ParticipantInfo;)LX/6f7;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_5

    .line 2228657
    :cond_12
    const/4 v2, 0x0

    goto :goto_3

    .line 2228658
    :cond_13
    invoke-virtual {v5}, LX/6f7;->k()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_c

    .line 2228659
    const/4 v2, 0x1

    goto :goto_3

    .line 2228660
    :cond_14
    const/4 v2, 0x0

    goto :goto_4
.end method

.method private b(Landroid/net/Uri;)Lcom/facebook/messaging/model/messages/Message;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 2228583
    :try_start_0
    iget-object v0, p0, LX/FMB;->s:LX/FMG;

    sget-object v2, LX/FMB;->h:[Ljava/lang/String;

    sget-object v3, LX/FMB;->i:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, LX/FMG;->a(Landroid/net/Uri;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2228584
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2228585
    invoke-static {v1}, LX/2J0;->a(Landroid/database/Cursor;)LX/6LS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v7

    .line 2228586
    const/4 v0, 0x0

    :try_start_2
    invoke-static {p0, v7, v0}, LX/FMB;->a(LX/FMB;Landroid/database/Cursor;Ljava/util/Map;)Lcom/facebook/messaging/model/messages/Message;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 2228587
    if-eqz v7, :cond_0

    .line 2228588
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 2228589
    :cond_0
    :goto_0
    return-object v0

    .line 2228590
    :cond_1
    if-eqz v1, :cond_2

    .line 2228591
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v0, v7

    .line 2228592
    goto :goto_0

    .line 2228593
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v7, :cond_3

    .line 2228594
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 2228595
    :catchall_1
    move-exception v0

    move-object v7, v1

    goto :goto_1
.end method

.method private c(Landroid/net/Uri;)Lcom/facebook/messaging/model/messages/Message;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 2228570
    :try_start_0
    iget-object v0, p0, LX/FMB;->s:LX/FMG;

    sget-object v2, LX/FMB;->j:[Ljava/lang/String;

    sget-object v3, LX/FMB;->k:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, LX/FMG;->a(Landroid/net/Uri;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2228571
    if-eqz v1, :cond_1

    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2228572
    invoke-static {v1}, LX/2J0;->a(Landroid/database/Cursor;)LX/6LS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v7

    .line 2228573
    const/4 v0, 0x0

    :try_start_2
    invoke-static {p0, v7, v0}, LX/FMB;->b(LX/FMB;Landroid/database/Cursor;Ljava/util/Map;)Lcom/facebook/messaging/model/messages/Message;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 2228574
    if-eqz v7, :cond_0

    .line 2228575
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 2228576
    :cond_0
    :goto_0
    return-object v0

    .line 2228577
    :cond_1
    if-eqz v1, :cond_2

    .line 2228578
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v0, v7

    .line 2228579
    goto :goto_0

    .line 2228580
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v7, :cond_3

    .line 2228581
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 2228582
    :catchall_1
    move-exception v0

    move-object v7, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(JIJ)LX/0Px;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JIJ)",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/messages/Message;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    const/4 v9, 0x0

    .line 2228535
    const-wide/16 v2, -0x64

    cmp-long v2, p1, v2

    if-eqz v2, :cond_0

    const-wide/16 v2, -0x65

    cmp-long v2, p1, v2

    if-nez v2, :cond_1

    .line 2228536
    :cond_0
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v2

    .line 2228537
    :goto_0
    return-object v2

    .line 2228538
    :cond_1
    cmp-long v2, p1, v4

    if-gez v2, :cond_2

    .line 2228539
    const-string v2, "SmsMessageLoader.getMessagesForCorruptedThread"

    const v3, -0x8f1cb79

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2228540
    :try_start_0
    invoke-direct/range {p0 .. p5}, LX/FMB;->b(JIJ)LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 2228541
    const v3, 0x4bce82e3    # 2.7067846E7f

    invoke-static {v3}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v2

    const v3, -0x13070400

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2

    .line 2228542
    :cond_2
    iget-object v2, p0, LX/FMB;->t:LX/FN6;

    invoke-virtual {v2}, LX/FN6;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/FMB;->t:LX/FN6;

    invoke-virtual {v2}, LX/FN6;->b()Z

    move-result v2

    if-nez v2, :cond_3

    .line 2228543
    const-string v2, "SmsMessageLoader.getMessagesForDualSimThreadWithoutSubId"

    const v3, 0x32fd62c0

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2228544
    :try_start_1
    invoke-direct/range {p0 .. p5}, LX/FMB;->b(JIJ)LX/0Px;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    .line 2228545
    const v3, -0x2748b608

    invoke-static {v3}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_1
    move-exception v2

    const v3, -0x3cacfd52

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2

    .line 2228546
    :cond_3
    const-string v2, "SmsMessageLoader.getMessagesForThread"

    const v3, 0x2e361b14

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2228547
    :try_start_2
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    .line 2228548
    cmp-long v2, p4, v4

    if-lez v2, :cond_9

    .line 2228549
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "normalized_date<="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p4

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    move-result-object v6

    .line 2228550
    :goto_1
    :try_start_3
    iget-object v2, p0, LX/FMB;->s:LX/FMG;

    sget-object v3, LX/2Up;->b:Landroid/net/Uri;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, LX/FMB;->b:[Ljava/lang/String;

    sget-object v5, LX/FMB;->c:[Ljava/lang/String;

    const/4 v7, 0x0

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v11, "normalized_date DESC LIMIT "

    invoke-direct {v8, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p3

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v2 .. v8}, LX/FMG;->a(Landroid/net/Uri;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 2228551
    if-eqz v9, :cond_7

    .line 2228552
    invoke-static {v9}, LX/2J0;->a(Landroid/database/Cursor;)LX/6LS;

    move-result-object v9

    .line 2228553
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2228554
    :cond_4
    :goto_2
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2228555
    const-string v3, "transport_type"

    invoke-static {v9, v3}, LX/6LT;->c(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2228556
    const-string v4, "sms"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2228557
    invoke-static {p0, v9, v2}, LX/FMB;->a(LX/FMB;Landroid/database/Cursor;Ljava/util/Map;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    invoke-virtual {v10, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_2

    .line 2228558
    :catch_0
    move-exception v2

    .line 2228559
    :try_start_4
    const-string v3, "SmsMessageLoader"

    const-string v4, "Failed to fetch messages for thread %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v2, v4, v5}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2228560
    invoke-static {v2}, LX/1Bz;->propagate(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v2

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 2228561
    :catchall_2
    move-exception v2

    if-eqz v9, :cond_5

    .line 2228562
    :try_start_5
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 2228563
    :catchall_3
    move-exception v2

    const v3, 0x5c06bf31

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2

    .line 2228564
    :cond_6
    :try_start_6
    const-string v4, "mms"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2228565
    invoke-static {p0, v9, v2}, LX/FMB;->b(LX/FMB;Landroid/database/Cursor;Ljava/util/Map;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    invoke-virtual {v10, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto :goto_2

    .line 2228566
    :cond_7
    if-eqz v9, :cond_8

    .line 2228567
    :try_start_7
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 2228568
    :cond_8
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    move-result-object v2

    .line 2228569
    const v3, -0x63ee798e

    invoke-static {v3}, LX/02m;->a(I)V

    goto/16 :goto_0

    :cond_9
    move-object v6, v9

    goto/16 :goto_1
.end method

.method public final a(Landroid/net/Uri;)Lcom/facebook/messaging/model/messages/Message;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2228528
    invoke-static {p1}, LX/FMQ;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 2228529
    invoke-static {v0}, LX/FMQ;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2228530
    invoke-static {v0}, LX/FMQ;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, LX/FMB;->b(Landroid/net/Uri;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2228531
    :goto_0
    return-object v0

    .line 2228532
    :cond_0
    invoke-static {v0}, LX/FMQ;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2228533
    invoke-static {v0}, LX/FMQ;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, LX/FMB;->c(Landroid/net/Uri;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    goto :goto_0

    .line 2228534
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
