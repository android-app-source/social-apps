.class public LX/G13;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/G13;


# instance fields
.field private final a:LX/7H7;


# direct methods
.method public constructor <init>(LX/7H7;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2310666
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2310667
    iput-object p1, p0, LX/G13;->a:LX/7H7;

    .line 2310668
    return-void
.end method

.method public static a(LX/0QB;)LX/G13;
    .locals 4

    .prologue
    .line 2310672
    sget-object v0, LX/G13;->b:LX/G13;

    if-nez v0, :cond_1

    .line 2310673
    const-class v1, LX/G13;

    monitor-enter v1

    .line 2310674
    :try_start_0
    sget-object v0, LX/G13;->b:LX/G13;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2310675
    if-eqz v2, :cond_0

    .line 2310676
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2310677
    new-instance p0, LX/G13;

    invoke-static {v0}, LX/7H7;->a(LX/0QB;)LX/7H7;

    move-result-object v3

    check-cast v3, LX/7H7;

    invoke-direct {p0, v3}, LX/G13;-><init>(LX/7H7;)V

    .line 2310678
    move-object v0, p0

    .line 2310679
    sput-object v0, LX/G13;->b:LX/G13;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2310680
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2310681
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2310682
    :cond_1
    sget-object v0, LX/G13;->b:LX/G13;

    return-object v0

    .line 2310683
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2310684
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(JLX/0am;ZZ)Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;ZZ)",
            "Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;"
        }
    .end annotation

    .prologue
    .line 2310669
    new-instance v1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;

    iget-object v0, p0, LX/G13;->a:LX/7H7;

    .line 2310670
    iget-object v2, v0, LX/7H7;->a:LX/0hB;

    invoke-virtual {v2}, LX/0hB;->c()I

    move-result v2

    iget-object v3, v0, LX/7H7;->a:LX/0hB;

    invoke-virtual {v3}, LX/0hB;->d()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    move v4, v2

    .line 2310671
    iget-object v0, p0, LX/G13;->a:LX/7H7;

    invoke-virtual {v0}, LX/7H7;->a()I

    move-result v5

    move-wide v2, p1

    move v6, p4

    move v7, p5

    move-object v8, p3

    invoke-direct/range {v1 .. v8}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;-><init>(JIIZZLX/0am;)V

    return-object v1
.end method
