.class public LX/Fa5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Fa1;


# instance fields
.field public final a:LX/FaA;

.field public final b:LX/Fa6;

.field public final c:LX/Fa7;

.field public final d:Ljava/util/concurrent/ExecutorService;

.field public final e:LX/03V;

.field public final f:LX/0kL;

.field public final g:LX/Cvp;


# direct methods
.method public constructor <init>(LX/FaA;LX/Fa6;LX/Fa7;Ljava/util/concurrent/ExecutorService;LX/03V;LX/0kL;LX/Cvp;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2258330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2258331
    iput-object p1, p0, LX/Fa5;->a:LX/FaA;

    .line 2258332
    iput-object p2, p0, LX/Fa5;->b:LX/Fa6;

    .line 2258333
    iput-object p3, p0, LX/Fa5;->c:LX/Fa7;

    .line 2258334
    iput-object p4, p0, LX/Fa5;->d:Ljava/util/concurrent/ExecutorService;

    .line 2258335
    iput-object p5, p0, LX/Fa5;->e:LX/03V;

    .line 2258336
    iput-object p6, p0, LX/Fa5;->f:LX/0kL;

    .line 2258337
    iput-object p7, p0, LX/Fa5;->g:LX/Cvp;

    .line 2258338
    return-void
.end method

.method public static a$redex0(LX/Fa5;LX/0Px;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2258339
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2258340
    :goto_0
    return-void

    .line 2258341
    :cond_0
    iget-object v0, p0, LX/Fa5;->a:LX/FaA;

    .line 2258342
    new-instance v1, LX/A1Z;

    invoke-direct {v1}, LX/A1Z;-><init>()V

    move-object v1, v1

    .line 2258343
    const-string v2, "topic_ids"

    invoke-virtual {v1, v2, p1}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v2

    const-string v3, "fetch_size"

    iget-object v4, v0, LX/FaA;->b:LX/0ad;

    sget v5, LX/100;->as:I

    const/16 v6, 0xa

    invoke-interface {v4, v5, v6}, LX/0ad;->a(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2258344
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2258345
    iget-object v2, v0, LX/FaA;->a:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 2258346
    new-instance v2, LX/Fa9;

    invoke-direct {v2, v0}, LX/Fa9;-><init>(LX/FaA;)V

    invoke-static {v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 2258347
    new-instance v1, LX/Fa4;

    invoke-direct {v1, p0, p1}, LX/Fa4;-><init>(LX/Fa5;LX/0Px;)V

    .line 2258348
    iget-object v2, p0, LX/Fa5;->d:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(LX/CwV;)V
    .locals 2

    .prologue
    .line 2258349
    iget-object v0, p0, LX/Fa5;->b:LX/Fa6;

    invoke-virtual {v0, p1}, LX/Fa6;->a(LX/CwV;)LX/CwV;

    .line 2258350
    iget-object v0, p1, LX/CwV;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2258351
    iget-boolean v1, p1, LX/CwV;->c:Z

    move v1, v1

    .line 2258352
    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Fa5;->b:LX/Fa6;

    .line 2258353
    iget-object p1, v1, LX/Fa6;->b:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    move v1, p1

    .line 2258354
    if-nez v1, :cond_0

    .line 2258355
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {p0, v0}, LX/Fa5;->a$redex0(LX/Fa5;LX/0Px;)V

    .line 2258356
    :cond_0
    return-void
.end method
