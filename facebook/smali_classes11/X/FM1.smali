.class public LX/FM1;
.super LX/6lf;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6lf",
        "<",
        "LX/FM0;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/CMq;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/03V;

.field private final d:LX/FLz;

.field private final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:LX/FFU;

.field private final g:LX/FFX;

.field private final h:LX/15W;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/03V;LX/FLz;Lcom/facebook/content/SecureContextHelper;LX/FFU;LX/FFX;LX/15W;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/CMq;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/FLz;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/FFU;",
            "LX/FFX;",
            "LX/15W;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2228281
    invoke-direct {p0}, LX/6lf;-><init>()V

    .line 2228282
    iput-object p1, p0, LX/FM1;->a:Landroid/content/Context;

    .line 2228283
    iput-object p2, p0, LX/FM1;->b:LX/0Ot;

    .line 2228284
    iput-object p3, p0, LX/FM1;->c:LX/03V;

    .line 2228285
    iput-object p4, p0, LX/FM1;->d:LX/FLz;

    .line 2228286
    iput-object p5, p0, LX/FM1;->e:Lcom/facebook/content/SecureContextHelper;

    .line 2228287
    iput-object p6, p0, LX/FM1;->f:LX/FFU;

    .line 2228288
    iput-object p7, p0, LX/FM1;->g:LX/FFX;

    .line 2228289
    iput-object p8, p0, LX/FM1;->h:LX/15W;

    .line 2228290
    return-void
.end method


# virtual methods
.method public final b(Landroid/view/ViewGroup;)LX/6le;
    .locals 3

    .prologue
    .line 2228278
    iget-object v0, p0, LX/FM1;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2228279
    const v1, 0x7f03130a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2228280
    new-instance v1, LX/FM0;

    invoke-direct {v1, v0}, LX/FM0;-><init>(Landroid/view/View;)V

    return-object v1
.end method
