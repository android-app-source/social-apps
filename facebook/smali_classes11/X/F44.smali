.class public final LX/F44;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 32

    .prologue
    .line 2195923
    const/16 v26, 0x0

    .line 2195924
    const/16 v25, 0x0

    .line 2195925
    const/16 v24, 0x0

    .line 2195926
    const/16 v21, 0x0

    .line 2195927
    const-wide/16 v22, 0x0

    .line 2195928
    const/16 v20, 0x0

    .line 2195929
    const/16 v19, 0x0

    .line 2195930
    const/16 v18, 0x0

    .line 2195931
    const/16 v17, 0x0

    .line 2195932
    const/16 v16, 0x0

    .line 2195933
    const/4 v15, 0x0

    .line 2195934
    const/4 v14, 0x0

    .line 2195935
    const/4 v13, 0x0

    .line 2195936
    const/4 v12, 0x0

    .line 2195937
    const/4 v11, 0x0

    .line 2195938
    const/4 v10, 0x0

    .line 2195939
    const/4 v9, 0x0

    .line 2195940
    const/4 v8, 0x0

    .line 2195941
    const/4 v7, 0x0

    .line 2195942
    const/4 v6, 0x0

    .line 2195943
    const/4 v5, 0x0

    .line 2195944
    const/4 v4, 0x0

    .line 2195945
    const/4 v3, 0x0

    .line 2195946
    const/4 v2, 0x0

    .line 2195947
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v27

    sget-object v28, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v27

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_1a

    .line 2195948
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2195949
    const/4 v2, 0x0

    .line 2195950
    :goto_0
    return v2

    .line 2195951
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v28, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v28

    if-eq v2, v0, :cond_14

    .line 2195952
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2195953
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2195954
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v28

    sget-object v29, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v28

    move-object/from16 v1, v29

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 2195955
    const-string v28, "can_viewer_change_cover_photo"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_1

    .line 2195956
    const/4 v2, 0x1

    .line 2195957
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v27, v7

    move v7, v2

    goto :goto_1

    .line 2195958
    :cond_1
    const-string v28, "can_viewer_change_name"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_2

    .line 2195959
    const/4 v2, 0x1

    .line 2195960
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v26, v6

    move v6, v2

    goto :goto_1

    .line 2195961
    :cond_2
    const-string v28, "cover_photo"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_3

    .line 2195962
    invoke-static/range {p0 .. p1}, LX/F3x;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v25, v2

    goto :goto_1

    .line 2195963
    :cond_3
    const-string v28, "description"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_4

    .line 2195964
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v24, v2

    goto :goto_1

    .line 2195965
    :cond_4
    const-string v28, "end_of_privacy_grace_period"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_5

    .line 2195966
    const/4 v2, 0x1

    .line 2195967
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 2195968
    :cond_5
    const-string v28, "group_members"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_6

    .line 2195969
    invoke-static/range {p0 .. p1}, LX/F3y;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 2195970
    :cond_6
    const-string v28, "group_purposes"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_7

    .line 2195971
    invoke-static/range {p0 .. p1}, LX/F3z;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v22, v2

    goto/16 :goto_1

    .line 2195972
    :cond_7
    const-string v28, "id"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_8

    .line 2195973
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v21, v2

    goto/16 :goto_1

    .line 2195974
    :cond_8
    const-string v28, "join_approval_setting"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_9

    .line 2195975
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v20, v2

    goto/16 :goto_1

    .line 2195976
    :cond_9
    const-string v28, "name"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_a

    .line 2195977
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 2195978
    :cond_a
    const-string v28, "parent_group"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_b

    .line 2195979
    invoke-static/range {p0 .. p1}, LX/F40;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 2195980
    :cond_b
    const-string v28, "possible_join_approval_settings"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_c

    .line 2195981
    invoke-static/range {p0 .. p1}, LX/F41;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v17, v2

    goto/16 :goto_1

    .line 2195982
    :cond_c
    const-string v28, "possible_post_permission_settings"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_d

    .line 2195983
    invoke-static/range {p0 .. p1}, LX/F42;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v16, v2

    goto/16 :goto_1

    .line 2195984
    :cond_d
    const-string v28, "possible_visibility_settings"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_e

    .line 2195985
    invoke-static/range {p0 .. p1}, LX/F43;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 2195986
    :cond_e
    const-string v28, "post_permission_setting"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_f

    .line 2195987
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 2195988
    :cond_f
    const-string v28, "previous_visibility"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_10

    .line 2195989
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 2195990
    :cond_10
    const-string v28, "privacy_change_threshold"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_11

    .line 2195991
    const/4 v2, 0x1

    .line 2195992
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    move v12, v9

    move v9, v2

    goto/16 :goto_1

    .line 2195993
    :cond_11
    const-string v28, "requires_post_approval"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v28

    if-eqz v28, :cond_12

    .line 2195994
    const/4 v2, 0x1

    .line 2195995
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    move v11, v8

    move v8, v2

    goto/16 :goto_1

    .line 2195996
    :cond_12
    const-string v28, "visibility"

    move-object/from16 v0, v28

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 2195997
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v10, v2

    goto/16 :goto_1

    .line 2195998
    :cond_13
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2195999
    :cond_14
    const/16 v2, 0x13

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2196000
    if-eqz v7, :cond_15

    .line 2196001
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2196002
    :cond_15
    if-eqz v6, :cond_16

    .line 2196003
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2196004
    :cond_16
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2196005
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2196006
    if-eqz v3, :cond_17

    .line 2196007
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2196008
    :cond_17
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2196009
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2196010
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2196011
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2196012
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2196013
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2196014
    const/16 v2, 0xb

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2196015
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2196016
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 2196017
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 2196018
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2196019
    if-eqz v9, :cond_18

    .line 2196020
    const/16 v2, 0x10

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12, v3}, LX/186;->a(III)V

    .line 2196021
    :cond_18
    if-eqz v8, :cond_19

    .line 2196022
    const/16 v2, 0x11

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->a(IZ)V

    .line 2196023
    :cond_19
    const/16 v2, 0x12

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2196024
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_1a
    move/from16 v27, v26

    move/from16 v26, v25

    move/from16 v25, v24

    move/from16 v24, v21

    move/from16 v21, v18

    move/from16 v18, v15

    move v15, v12

    move v12, v9

    move v9, v3

    move v3, v4

    move/from16 v30, v8

    move v8, v2

    move/from16 v31, v13

    move v13, v10

    move v10, v7

    move v7, v6

    move v6, v5

    move-wide/from16 v4, v22

    move/from16 v22, v19

    move/from16 v23, v20

    move/from16 v20, v17

    move/from16 v19, v16

    move/from16 v17, v14

    move/from16 v16, v31

    move v14, v11

    move/from16 v11, v30

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 9

    .prologue
    const/16 v8, 0x12

    const/16 v7, 0xf

    const/16 v6, 0xe

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 2196025
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2196026
    invoke-virtual {p0, p1, v4}, LX/15i;->b(II)Z

    move-result v0

    .line 2196027
    if-eqz v0, :cond_0

    .line 2196028
    const-string v1, "can_viewer_change_cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196029
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2196030
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2196031
    if-eqz v0, :cond_1

    .line 2196032
    const-string v1, "can_viewer_change_name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196033
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2196034
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2196035
    if-eqz v0, :cond_3

    .line 2196036
    const-string v1, "cover_photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196037
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2196038
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2196039
    if-eqz v1, :cond_2

    .line 2196040
    const-string v2, "photo"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196041
    invoke-static {p0, v1, p2}, LX/F3w;->a(LX/15i;ILX/0nX;)V

    .line 2196042
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2196043
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2196044
    if-eqz v0, :cond_4

    .line 2196045
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196046
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2196047
    :cond_4
    const/4 v0, 0x4

    const-wide/16 v2, 0x0

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2196048
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_5

    .line 2196049
    const-string v2, "end_of_privacy_grace_period"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196050
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2196051
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2196052
    if-eqz v0, :cond_7

    .line 2196053
    const-string v1, "group_members"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196054
    const/4 v1, 0x0

    .line 2196055
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2196056
    invoke-virtual {p0, v0, v1, v1}, LX/15i;->a(III)I

    move-result v1

    .line 2196057
    if-eqz v1, :cond_6

    .line 2196058
    const-string v2, "count"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196059
    invoke-virtual {p2, v1}, LX/0nX;->b(I)V

    .line 2196060
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2196061
    :cond_7
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2196062
    if-eqz v0, :cond_9

    .line 2196063
    const-string v1, "group_purposes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196064
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2196065
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2196066
    if-eqz v1, :cond_8

    .line 2196067
    const-string v2, "nodes"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196068
    invoke-static {p0, v1, p2, p3}, LX/F4I;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2196069
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2196070
    :cond_9
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2196071
    if-eqz v0, :cond_a

    .line 2196072
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196073
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2196074
    :cond_a
    invoke-virtual {p0, p1, v5}, LX/15i;->g(II)I

    move-result v0

    .line 2196075
    if-eqz v0, :cond_b

    .line 2196076
    const-string v0, "join_approval_setting"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196077
    invoke-virtual {p0, p1, v5}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2196078
    :cond_b
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2196079
    if-eqz v0, :cond_c

    .line 2196080
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196081
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2196082
    :cond_c
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2196083
    if-eqz v0, :cond_d

    .line 2196084
    const-string v1, "parent_group"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196085
    invoke-static {p0, v0, p2}, LX/F40;->a(LX/15i;ILX/0nX;)V

    .line 2196086
    :cond_d
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2196087
    if-eqz v0, :cond_e

    .line 2196088
    const-string v1, "possible_join_approval_settings"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196089
    invoke-static {p0, v0, p2, p3}, LX/F41;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2196090
    :cond_e
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2196091
    if-eqz v0, :cond_f

    .line 2196092
    const-string v1, "possible_post_permission_settings"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196093
    invoke-static {p0, v0, p2, p3}, LX/F42;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2196094
    :cond_f
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2196095
    if-eqz v0, :cond_15

    .line 2196096
    const-string v1, "possible_visibility_settings"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196097
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2196098
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2196099
    if-eqz v1, :cond_14

    .line 2196100
    const-string v2, "edges"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196101
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2196102
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_13

    .line 2196103
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v3

    const/4 p3, 0x2

    .line 2196104
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2196105
    const/4 v5, 0x0

    invoke-virtual {p0, v3, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 2196106
    if-eqz v5, :cond_10

    .line 2196107
    const-string v0, "description"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196108
    invoke-virtual {p2, v5}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2196109
    :cond_10
    const/4 v5, 0x1

    invoke-virtual {p0, v3, v5}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v5

    .line 2196110
    if-eqz v5, :cond_11

    .line 2196111
    const-string v0, "display_name"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196112
    invoke-virtual {p2, v5}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2196113
    :cond_11
    invoke-virtual {p0, v3, p3}, LX/15i;->g(II)I

    move-result v5

    .line 2196114
    if-eqz v5, :cond_12

    .line 2196115
    const-string v5, "node"

    invoke-virtual {p2, v5}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196116
    invoke-virtual {p0, v3, p3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2196117
    :cond_12
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2196118
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2196119
    :cond_13
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2196120
    :cond_14
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2196121
    :cond_15
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 2196122
    if-eqz v0, :cond_16

    .line 2196123
    const-string v0, "post_permission_setting"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196124
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2196125
    :cond_16
    invoke-virtual {p0, p1, v7}, LX/15i;->g(II)I

    move-result v0

    .line 2196126
    if-eqz v0, :cond_17

    .line 2196127
    const-string v0, "previous_visibility"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196128
    invoke-virtual {p0, p1, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2196129
    :cond_17
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0, v4}, LX/15i;->a(III)I

    move-result v0

    .line 2196130
    if-eqz v0, :cond_18

    .line 2196131
    const-string v1, "privacy_change_threshold"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196132
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2196133
    :cond_18
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2196134
    if-eqz v0, :cond_19

    .line 2196135
    const-string v1, "requires_post_approval"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196136
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2196137
    :cond_19
    invoke-virtual {p0, p1, v8}, LX/15i;->g(II)I

    move-result v0

    .line 2196138
    if-eqz v0, :cond_1a

    .line 2196139
    const-string v0, "visibility"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2196140
    invoke-virtual {p0, p1, v8}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2196141
    :cond_1a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2196142
    return-void
.end method
