.class public final LX/GfO;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/GfP;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field public d:Z

.field public final synthetic e:LX/GfP;


# direct methods
.method public constructor <init>(LX/GfP;)V
    .locals 1

    .prologue
    .line 2377279
    iput-object p1, p0, LX/GfO;->e:LX/GfP;

    .line 2377280
    move-object v0, p1

    .line 2377281
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2377282
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2377283
    const-string v0, "PagesYouMayLikeSmallFormatComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2377284
    if-ne p0, p1, :cond_1

    .line 2377285
    :cond_0
    :goto_0
    return v0

    .line 2377286
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2377287
    goto :goto_0

    .line 2377288
    :cond_3
    check-cast p1, LX/GfO;

    .line 2377289
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2377290
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2377291
    if-eq v2, v3, :cond_0

    .line 2377292
    iget-object v2, p0, LX/GfO;->a:LX/1Pn;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/GfO;->a:LX/1Pn;

    iget-object v3, p1, LX/GfO;->a:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2377293
    goto :goto_0

    .line 2377294
    :cond_5
    iget-object v2, p1, LX/GfO;->a:LX/1Pn;

    if-nez v2, :cond_4

    .line 2377295
    :cond_6
    iget-object v2, p0, LX/GfO;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/GfO;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/GfO;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2377296
    goto :goto_0

    .line 2377297
    :cond_8
    iget-object v2, p1, LX/GfO;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_7

    .line 2377298
    :cond_9
    iget v2, p0, LX/GfO;->c:I

    iget v3, p1, LX/GfO;->c:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 2377299
    goto :goto_0

    .line 2377300
    :cond_a
    iget-boolean v2, p0, LX/GfO;->d:Z

    iget-boolean v3, p1, LX/GfO;->d:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2377301
    goto :goto_0
.end method
