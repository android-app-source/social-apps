.class public final enum LX/H1O;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/H1O;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/H1O;

.field public static final enum CACHED:LX/H1O;

.field public static final enum DEFAULT:LX/H1O;

.field public static final enum LATEST:LX/H1O;

.field public static final enum OVERRIDE:LX/H1O;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2415096
    new-instance v0, LX/H1O;

    const-string v1, "OVERRIDE"

    invoke-direct {v0, v1, v2}, LX/H1O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H1O;->OVERRIDE:LX/H1O;

    .line 2415097
    new-instance v0, LX/H1O;

    const-string v1, "LATEST"

    invoke-direct {v0, v1, v3}, LX/H1O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H1O;->LATEST:LX/H1O;

    .line 2415098
    new-instance v0, LX/H1O;

    const-string v1, "CACHED"

    invoke-direct {v0, v1, v4}, LX/H1O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H1O;->CACHED:LX/H1O;

    .line 2415099
    new-instance v0, LX/H1O;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v5}, LX/H1O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H1O;->DEFAULT:LX/H1O;

    .line 2415100
    const/4 v0, 0x4

    new-array v0, v0, [LX/H1O;

    sget-object v1, LX/H1O;->OVERRIDE:LX/H1O;

    aput-object v1, v0, v2

    sget-object v1, LX/H1O;->LATEST:LX/H1O;

    aput-object v1, v0, v3

    sget-object v1, LX/H1O;->CACHED:LX/H1O;

    aput-object v1, v0, v4

    sget-object v1, LX/H1O;->DEFAULT:LX/H1O;

    aput-object v1, v0, v5

    sput-object v0, LX/H1O;->$VALUES:[LX/H1O;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2415102
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/H1O;
    .locals 1

    .prologue
    .line 2415103
    const-class v0, LX/H1O;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/H1O;

    return-object v0
.end method

.method public static values()[LX/H1O;
    .locals 1

    .prologue
    .line 2415101
    sget-object v0, LX/H1O;->$VALUES:[LX/H1O;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/H1O;

    return-object v0
.end method
