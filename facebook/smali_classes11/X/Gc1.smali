.class public final LX/Gc1;
.super Landroid/database/DataSetObserver;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;)V
    .locals 0

    .prologue
    .line 2370925
    iput-object p1, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onChanged()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2370926
    iget-object v0, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->i:Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    .line 2370927
    :cond_0
    return-void

    .line 2370928
    :cond_1
    iget-object v0, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    invoke-virtual {v0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->o()V

    .line 2370929
    iget-object v0, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    .line 2370930
    const/4 v0, 0x2

    new-array v4, v0, [I

    .line 2370931
    iget-object v0, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget-boolean v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->h:Z

    if-eqz v0, :cond_2

    move v0, v1

    .line 2370932
    :goto_0
    iget-object v3, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iput-boolean v1, v3, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->h:Z

    .line 2370933
    :goto_1
    if-le v2, v0, :cond_3

    .line 2370934
    iget-object v3, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget-object v3, v3, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->i:Landroid/view/ViewGroup;

    add-int/lit8 v5, v2, -0x1

    invoke-virtual {v3, v5}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 2370935
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 2370936
    :cond_2
    iget-object v0, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a:LX/Gbz;

    invoke-virtual {v0}, LX/Gbz;->getCount()I

    move-result v0

    goto :goto_0

    :cond_3
    move v2, v1

    .line 2370937
    :goto_2
    iget-object v0, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a:LX/Gbz;

    invoke-virtual {v0}, LX/Gbz;->getCount()I

    move-result v0

    if-ge v2, v0, :cond_9

    .line 2370938
    iget-object v0, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 2370939
    iget-object v0, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a:LX/Gbz;

    iget-object v3, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget-object v3, v3, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2, v5, v3}, LX/Gbz;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 2370940
    iget-object v0, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a:LX/Gbz;

    invoke-virtual {v0, v2}, LX/Gbz;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2370941
    iget-object v3, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget-boolean v3, v3, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->g:Z

    if-nez v3, :cond_7

    iget-object v3, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    invoke-virtual {v3, v0}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v3, 0x1

    .line 2370942
    :goto_3
    if-eqz v3, :cond_4

    .line 2370943
    iget-object v7, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    invoke-virtual {v7, v6}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a(Landroid/view/View;)V

    .line 2370944
    :cond_4
    if-eqz v3, :cond_8

    iget-object v3, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    invoke-virtual {v3}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->q()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 2370945
    if-ne v6, v5, :cond_5

    .line 2370946
    iget-object v3, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget-object v3, v3, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->i:Landroid/view/ViewGroup;

    invoke-virtual {v3, v6}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2370947
    :cond_5
    iget-object v3, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget-object v3, v3, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->i:Landroid/view/ViewGroup;

    invoke-virtual {v3, v6, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 2370948
    :cond_6
    :goto_4
    iget-object v3, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    invoke-virtual {v3, v2, v6}, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a(ILandroid/view/View;)V

    .line 2370949
    new-instance v3, LX/Gc0;

    invoke-direct {v3, p0, v2, v4, v0}, LX/Gc0;-><init>(LX/Gc1;I[ILcom/facebook/auth/credentials/DBLFacebookCredentials;)V

    invoke-virtual {v6, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2370950
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_7
    move v3, v1

    .line 2370951
    goto :goto_3

    .line 2370952
    :cond_8
    if-eq v6, v5, :cond_6

    .line 2370953
    iget-object v3, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget-object v3, v3, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->i:Landroid/view/ViewGroup;

    invoke-virtual {v3, v6, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    goto :goto_4

    .line 2370954
    :cond_9
    iget-object v0, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 2370955
    iget-object v1, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a:LX/Gbz;

    invoke-virtual {v1}, LX/Gbz;->getCount()I

    move-result v1

    .line 2370956
    :goto_5
    if-le v0, v1, :cond_0

    .line 2370957
    iget-object v2, p0, LX/Gc1;->a:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;

    iget-object v2, v2, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->i:Landroid/view/ViewGroup;

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 2370958
    add-int/lit8 v0, v0, -0x1

    goto :goto_5
.end method
