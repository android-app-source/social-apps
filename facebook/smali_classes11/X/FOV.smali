.class public final LX/FOV;
.super LX/9l7;
.source ""


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "LX/FOX;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/FOX;)V
    .locals 1

    .prologue
    .line 2235179
    invoke-direct {p0}, LX/9l7;-><init>()V

    .line 2235180
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, LX/FOV;->a:Ljava/lang/ref/WeakReference;

    .line 2235181
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/user/model/UserKey;LX/3Ox;)Z
    .locals 2

    .prologue
    .line 2235182
    iget-object v0, p0, LX/FOV;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FOX;

    .line 2235183
    if-nez v0, :cond_0

    .line 2235184
    const/4 v0, 0x0

    .line 2235185
    :goto_0
    return v0

    .line 2235186
    :cond_0
    iget-object v1, v0, LX/FOX;->n:Lcom/facebook/user/model/UserKey;

    if-nez v1, :cond_2

    .line 2235187
    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 2235188
    :cond_2
    iget-object v1, v0, LX/FOX;->n:Lcom/facebook/user/model/UserKey;

    invoke-static {p1, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2235189
    iget-object v1, v0, LX/FOX;->o:LX/3Ox;

    invoke-virtual {v1, p2}, LX/3Ox;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2235190
    iput-object p2, v0, LX/FOX;->o:LX/3Ox;

    .line 2235191
    iget-object v1, v0, LX/FOX;->m:Lcom/facebook/user/model/User;

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/FOX;->m:Lcom/facebook/user/model/User;

    invoke-virtual {v1}, Lcom/facebook/user/model/User;->P()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2235192
    iget-object v1, p2, LX/3Ox;->b:LX/3Oz;

    move-object v1, v1

    .line 2235193
    sget-object p0, LX/3Oz;->AVAILABLE:LX/3Oz;

    if-ne v1, p0, :cond_1

    .line 2235194
    const-string v1, "displayed_page_presence_online_values"

    const p0, 0x54001b

    .line 2235195
    iget-object p1, v0, LX/FOX;->a:LX/2CH;

    .line 2235196
    iget-object p2, p1, LX/2CH;->J:LX/2AL;

    move-object p1, p2

    .line 2235197
    sget-object p2, LX/2AL;->MQTT_DISCONNECTED:LX/2AL;

    if-ne p1, p2, :cond_3

    .line 2235198
    :goto_2
    goto :goto_1

    .line 2235199
    :cond_3
    iget-object p1, v0, LX/FOX;->d:LX/13Q;

    invoke-virtual {p1, v1}, LX/13Q;->a(Ljava/lang/String;)V

    .line 2235200
    iget-object p1, v0, LX/FOX;->e:LX/1BA;

    invoke-virtual {p1, p0}, LX/1BA;->a(I)V

    goto :goto_2
.end method
