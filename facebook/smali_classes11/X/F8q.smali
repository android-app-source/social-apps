.class public final enum LX/F8q;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/F8q;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/F8q;

.field public static final enum FEMALE:LX/F8q;

.field public static final enum MALE:LX/F8q;

.field public static final enum UNKNOWN:LX/F8q;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2204725
    new-instance v0, LX/F8q;

    const-string v1, "MALE"

    invoke-direct {v0, v1, v2}, LX/F8q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F8q;->MALE:LX/F8q;

    .line 2204726
    new-instance v0, LX/F8q;

    const-string v1, "FEMALE"

    invoke-direct {v0, v1, v3}, LX/F8q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F8q;->FEMALE:LX/F8q;

    .line 2204727
    new-instance v0, LX/F8q;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, LX/F8q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F8q;->UNKNOWN:LX/F8q;

    .line 2204728
    const/4 v0, 0x3

    new-array v0, v0, [LX/F8q;

    sget-object v1, LX/F8q;->MALE:LX/F8q;

    aput-object v1, v0, v2

    sget-object v1, LX/F8q;->FEMALE:LX/F8q;

    aput-object v1, v0, v3

    sget-object v1, LX/F8q;->UNKNOWN:LX/F8q;

    aput-object v1, v0, v4

    sput-object v0, LX/F8q;->$VALUES:[LX/F8q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2204729
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/F8q;
    .locals 1

    .prologue
    .line 2204730
    const-class v0, LX/F8q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/F8q;

    return-object v0
.end method

.method public static values()[LX/F8q;
    .locals 1

    .prologue
    .line 2204731
    sget-object v0, LX/F8q;->$VALUES:[LX/F8q;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/F8q;

    return-object v0
.end method
