.class public final LX/FIa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Qn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Qn",
        "<",
        "Ljava/lang/String;",
        "LX/FIb;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FIc;


# direct methods
.method public constructor <init>(LX/FIc;)V
    .locals 0

    .prologue
    .line 2222278
    iput-object p1, p0, LX/FIa;->a:LX/FIc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onRemoval(LX/4wi;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/4wi",
            "<",
            "Ljava/lang/String;",
            "LX/FIb;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2222279
    iget-object v0, p1, LX/4wi;->c:LX/0rs;

    move-object v0, v0

    .line 2222280
    sget-object v1, LX/FIZ;->a:[I

    invoke-virtual {v0}, LX/0rs;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2222281
    :goto_0
    return-void

    .line 2222282
    :pswitch_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "Upload time limit exceeded. Aborting upload with operationID: "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/4wi;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2222283
    :goto_1
    iget-object v2, p0, LX/FIa;->a:LX/FIc;

    invoke-virtual {p1}, LX/4wi;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIb;

    invoke-static {v2, v0, v1}, LX/FIc;->a$redex0(LX/FIc;LX/FIb;Ljava/lang/String;)V

    goto :goto_0

    .line 2222284
    :pswitch_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "Too many concurrent UDP requests. Aborting upload with operationID: "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, LX/4wi;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2222285
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
