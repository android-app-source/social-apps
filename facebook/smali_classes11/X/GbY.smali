.class public final LX/GbY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)V
    .locals 0

    .prologue
    .line 2369971
    iput-object p1, p0, LX/GbY;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 2

    .prologue
    .line 2369975
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2369976
    if-eqz v0, :cond_1

    .line 2369977
    iget-object v1, p0, LX/GbY;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-static {v1}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->r(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)V

    .line 2369978
    iget-object v1, p0, LX/GbY;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->r:LX/10M;

    invoke-virtual {v1, v0}, LX/10M;->a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V

    .line 2369979
    iget-object v1, p0, LX/GbY;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-static {v1}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->t(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2369980
    iget-object v1, p0, LX/GbY;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-static {v1}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->q(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)LX/Gb4;

    move-result-object v1

    invoke-interface {v1}, LX/Gb4;->d()V

    .line 2369981
    iget-object v1, p0, LX/GbY;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    iget-object v0, v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mIsPinSet:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2369982
    invoke-static {v1, v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->b$redex0(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;Z)V

    .line 2369983
    iget-object v0, p0, LX/GbY;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->setResult(I)V

    .line 2369984
    iget-object v0, p0, LX/GbY;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-virtual {v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->a()V

    .line 2369985
    :cond_0
    :goto_0
    return-void

    .line 2369986
    :cond_1
    sget-object v0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->A:Ljava/lang/Class;

    const-string v1, "Fetched result was null"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2369987
    iget-object v0, p0, LX/GbY;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->b(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2369973
    iget-object v0, p0, LX/GbY;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-static {v0, p1}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->b(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;Ljava/lang/Throwable;)V

    .line 2369974
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2369972
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {p0, p1}, LX/GbY;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
