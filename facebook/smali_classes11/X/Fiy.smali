.class public final LX/Fiy;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Fiz;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/search/model/KeywordTypeaheadUnit;

.field public final synthetic b:LX/Fiz;


# direct methods
.method public constructor <init>(LX/Fiz;)V
    .locals 1

    .prologue
    .line 2276035
    iput-object p1, p0, LX/Fiy;->b:LX/Fiz;

    .line 2276036
    move-object v0, p1

    .line 2276037
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2276038
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2276039
    const-string v0, "SearchTypeaheadSimpleKeywordComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2276040
    if-ne p0, p1, :cond_1

    .line 2276041
    :cond_0
    :goto_0
    return v0

    .line 2276042
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2276043
    goto :goto_0

    .line 2276044
    :cond_3
    check-cast p1, LX/Fiy;

    .line 2276045
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2276046
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2276047
    if-eq v2, v3, :cond_0

    .line 2276048
    iget-object v2, p0, LX/Fiy;->a:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/Fiy;->a:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    iget-object v3, p1, LX/Fiy;->a:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-virtual {v2, v3}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2276049
    goto :goto_0

    .line 2276050
    :cond_4
    iget-object v2, p1, LX/Fiy;->a:Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
