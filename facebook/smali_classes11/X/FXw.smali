.class public LX/FXw;
.super LX/FXP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FXP",
        "<",
        "Lcom/facebook/saved2/model/Saved2UnreadCountsTable_Queries$BaseQueryDAO;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/support/v4/app/FragmentActivity;

.field private final b:LX/FXk;

.field private final c:LX/FYj;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;LX/FXk;LX/FYj;)V
    .locals 0
    .param p1    # Landroid/support/v4/app/FragmentActivity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/FXk;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2255557
    invoke-direct {p0}, LX/FXP;-><init>()V

    .line 2255558
    iput-object p1, p0, LX/FXw;->a:Landroid/support/v4/app/FragmentActivity;

    .line 2255559
    iput-object p2, p0, LX/FXw;->b:LX/FXk;

    .line 2255560
    iput-object p3, p0, LX/FXw;->c:LX/FYj;

    .line 2255561
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 2255606
    invoke-virtual {p0, p1}, LX/FXP;->a(I)LX/AU0;

    move-result-object v0

    check-cast v0, LX/BOG;

    invoke-virtual {v0}, LX/BOG;->e()Ljava/lang/String;

    move-result-object v0

    .line 2255607
    const/4 v1, 0x0

    .line 2255608
    const/4 v2, -0x1

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v2, :pswitch_data_0

    .line 2255609
    sget-object v2, LX/79a;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string p1, "Unsupported section type: "

    invoke-direct {v3, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2255610
    :goto_1
    move v1, v1

    .line 2255611
    if-eqz v1, :cond_1

    iget-object v0, p0, LX/FXw;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    return-object v0

    .line 2255612
    :sswitch_0
    const-string v3, "ALL"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x0

    goto :goto_0

    :sswitch_1
    const-string v3, "LINKS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0

    :sswitch_2
    const-string v3, "VIDEOS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x2

    goto :goto_0

    :sswitch_3
    const-string v3, "PRODUCTS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x3

    goto :goto_0

    :sswitch_4
    const-string v3, "PHOTOS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x4

    goto :goto_0

    :sswitch_5
    const-string v3, "PLACES"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x5

    goto :goto_0

    :sswitch_6
    const-string v3, "MEDIA"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x6

    goto :goto_0

    :sswitch_7
    const-string v3, "MUSIC"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x7

    goto :goto_0

    :sswitch_8
    const-string v3, "BOOKS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0x8

    goto :goto_0

    :sswitch_9
    const-string v3, "MOVIES"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0x9

    goto/16 :goto_0

    :sswitch_a
    const-string v3, "TV_SHOWS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0xa

    goto/16 :goto_0

    :sswitch_b
    const-string v3, "EVENTS"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0xb

    goto/16 :goto_0

    :sswitch_c
    const-string v3, "ARCHIVED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0xc

    goto/16 :goto_0

    .line 2255613
    :pswitch_0
    const v1, 0x7f081aaa

    goto/16 :goto_1

    .line 2255614
    :pswitch_1
    const v1, 0x7f081aab

    goto/16 :goto_1

    .line 2255615
    :pswitch_2
    const v1, 0x7f081ab5

    goto/16 :goto_1

    .line 2255616
    :pswitch_3
    const v1, 0x7f081aac

    goto/16 :goto_1

    .line 2255617
    :pswitch_4
    const v1, 0x7f081aad

    goto/16 :goto_1

    .line 2255618
    :pswitch_5
    const v1, 0x7f081aae

    goto/16 :goto_1

    .line 2255619
    :pswitch_6
    const v1, 0x7f081aaf

    goto/16 :goto_1

    .line 2255620
    :pswitch_7
    const v1, 0x7f081ab0

    goto/16 :goto_1

    .line 2255621
    :pswitch_8
    const v1, 0x7f081ab1

    goto/16 :goto_1

    .line 2255622
    :pswitch_9
    const v1, 0x7f081ab2

    goto/16 :goto_1

    .line 2255623
    :pswitch_a
    const v1, 0x7f081ab3

    goto/16 :goto_1

    .line 2255624
    :pswitch_b
    const v1, 0x7f081ab4

    goto/16 :goto_1

    .line 2255625
    :pswitch_c
    const v1, 0x7f081ab6

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x7df56f07 -> :sswitch_a
        -0x781964bd -> :sswitch_9
        -0x7360837f -> :sswitch_4
        -0x732ec3b4 -> :sswitch_5
        -0x691a90a8 -> :sswitch_2
        -0x37a6d81e -> :sswitch_c
        -0xd2e3e9c -> :sswitch_3
        0xfd81 -> :sswitch_0
        0x3c72a8a -> :sswitch_8
        0x4515799 -> :sswitch_1
        0x45d77c4 -> :sswitch_6
        0x464f605 -> :sswitch_7
        0x7a9ad519 -> :sswitch_b
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method public final a(Ljava/lang/Object;)I
    .locals 3

    .prologue
    const/4 v1, -0x2

    .line 2255596
    check-cast p1, LX/FXv;

    .line 2255597
    iget-object v0, p0, LX/BYF;->a:Landroid/database/Cursor;

    move-object v0, v0

    .line 2255598
    if-nez v0, :cond_0

    move v0, v1

    .line 2255599
    :goto_0
    return v0

    .line 2255600
    :cond_0
    invoke-virtual {p0}, LX/FXP;->e()LX/AU0;

    move-result-object v0

    check-cast v0, LX/BOG;

    iget-object v2, p1, LX/FXv;->a:Ljava/lang/String;

    invoke-static {v0, v2}, LX/FXQ;->a(LX/BOG;Ljava/lang/String;)I

    move-result v0

    .line 2255601
    if-ltz v0, :cond_2

    .line 2255602
    iget v1, p1, LX/FXv;->b:I

    if-eq v0, v1, :cond_1

    .line 2255603
    iput v0, p1, LX/FXv;->b:I

    goto :goto_0

    .line 2255604
    :cond_1
    const/4 v0, -0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2255605
    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 2255574
    invoke-virtual {p0, p2}, LX/FXP;->a(I)LX/AU0;

    move-result-object v0

    check-cast v0, LX/BOG;

    invoke-virtual {v0}, LX/BOG;->e()Ljava/lang/String;

    move-result-object v1

    .line 2255575
    new-instance v6, LX/FXv;

    invoke-direct {v6}, LX/FXv;-><init>()V

    .line 2255576
    iput-object v1, v6, LX/FXv;->a:Ljava/lang/String;

    .line 2255577
    iput p2, v6, LX/FXv;->b:I

    .line 2255578
    iget-object v0, p0, LX/FXw;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f031254

    invoke-virtual {v0, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, v6, LX/FXv;->c:Landroid/view/View;

    .line 2255579
    iget-object v0, v6, LX/FXv;->c:Landroid/view/View;

    const v2, 0x7f0d2b20

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, v6, LX/FXv;->d:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2255580
    iget-object v0, p0, LX/FXw;->c:LX/FYj;

    iget-object v2, p0, LX/FXw;->a:Landroid/support/v4/app/FragmentActivity;

    .line 2255581
    new-instance v5, LX/FYi;

    const-class v3, LX/FYS;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/FYS;

    invoke-static {v0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v4

    check-cast v4, LX/0tQ;

    invoke-direct {v5, v2, v1, v3, v4}, LX/FYi;-><init>(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;LX/FYS;LX/0tQ;)V

    .line 2255582
    move-object v0, v5

    .line 2255583
    iput-object v0, v6, LX/FXv;->e:LX/FYi;

    .line 2255584
    iget-object v0, v6, LX/FXv;->c:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2255585
    iget-object v0, p0, LX/FXw;->b:LX/FXk;

    invoke-virtual {v0, v1}, LX/FXk;->a(Ljava/lang/String;)LX/FXj;

    move-result-object v0

    iget-object v1, v6, LX/FXv;->d:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v2, v6, LX/FXv;->e:LX/FYi;

    const v3, 0x7f0d2b1f

    const v4, 0x7f0d04e7

    const v5, 0x7f03124e

    .line 2255586
    iput-object v1, v0, LX/FXj;->j:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2255587
    iput-object v2, v0, LX/FXj;->m:LX/FYi;

    .line 2255588
    iput v3, v0, LX/FXj;->o:I

    .line 2255589
    iput v4, v0, LX/FXj;->p:I

    .line 2255590
    iput v5, v0, LX/FXj;->q:I

    .line 2255591
    iget-object v7, v0, LX/FXj;->j:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget p0, v0, LX/FXj;->p:I

    invoke-virtual {v7, p0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/support/v7/widget/RecyclerView;

    iput-object v7, v0, LX/FXj;->l:Landroid/support/v7/widget/RecyclerView;

    .line 2255592
    iget-object v7, v0, LX/FXj;->l:Landroid/support/v7/widget/RecyclerView;

    new-instance p0, LX/1P1;

    iget-object p1, v0, LX/FXj;->l:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    const/4 p1, 0x1

    const/4 p2, 0x0

    invoke-direct {p0, p1, p2}, LX/1P1;-><init>(IZ)V

    invoke-virtual {v7, p0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2255593
    iget-object v7, v0, LX/FXj;->a:LX/FXV;

    move-object v7, v7

    .line 2255594
    invoke-virtual {v7, v0}, LX/FXV;->b(LX/FXj;)V

    .line 2255595
    return-object v6
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2255563
    check-cast p3, LX/FXv;

    .line 2255564
    iget-object v0, p3, LX/FXv;->c:Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2255565
    iget-object v0, p0, LX/FXw;->b:LX/FXk;

    iget-object v1, p3, LX/FXv;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/FXk;->a(Ljava/lang/String;)LX/FXj;

    move-result-object v0

    const/4 p1, 0x0

    const/4 p0, -0x1

    .line 2255566
    iget-object v1, v0, LX/FXj;->a:LX/FXV;

    move-object v1, v1

    .line 2255567
    invoke-virtual {v1, v0}, LX/FXV;->c(LX/FXj;)V

    .line 2255568
    iput-object p1, v0, LX/FXj;->m:LX/FYi;

    .line 2255569
    iput-object p1, v0, LX/FXj;->j:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2255570
    iput p0, v0, LX/FXj;->o:I

    .line 2255571
    iput p0, v0, LX/FXj;->p:I

    .line 2255572
    const/4 v1, 0x0

    iput v1, v0, LX/FXj;->q:I

    .line 2255573
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2255562
    check-cast p2, LX/FXv;

    iget-object v0, p2, LX/FXv;->c:Landroid/view/View;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
