.class public final LX/Gub;
.super LX/GuX;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

.field public b:[Ljava/lang/String;

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2403580
    iput-object p1, p0, LX/Gub;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403581
    invoke-direct {p0, p2}, LX/GuX;-><init>(Landroid/os/Handler;)V

    .line 2403582
    iput v0, p0, LX/Gub;->c:I

    .line 2403583
    iput v0, p0, LX/Gub;->d:I

    .line 2403584
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/facebook/webview/FacebookWebView;)V
    .locals 9

    .prologue
    const/4 v8, -0x1

    const/4 v1, 0x0

    .line 2403585
    iget-object v0, p0, LX/Gub;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403586
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v2

    .line 2403587
    if-eqz v0, :cond_0

    iget-object v2, p0, LX/GuX;->g:LX/BWN;

    if-nez v2, :cond_1

    .line 2403588
    :cond_0
    :goto_0
    return-void

    .line 2403589
    :cond_1
    :try_start_0
    iget-object v2, p0, LX/GuX;->g:LX/BWN;

    .line 2403590
    iget-object v3, p2, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v3, v3

    .line 2403591
    const-string v4, "segments"

    invoke-interface {v2, v3, v4}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2403592
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, v2}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 2403593
    const v2, 0x7f0d11fe

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    .line 2403594
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    iput-object v2, p0, LX/Gub;->b:[Ljava/lang/String;

    .line 2403595
    invoke-virtual {v0}, Landroid/widget/RadioGroup;->removeAllViews()V

    .line 2403596
    invoke-virtual {v0}, Landroid/widget/RadioGroup;->clearCheck()V

    .line 2403597
    :goto_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 2403598
    invoke-virtual {v3, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 2403599
    const-string v4, "title"

    invoke-virtual {v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2403600
    const-string v5, "callback"

    invoke-virtual {v2, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2403601
    iget-object v5, p0, LX/Gub;->b:[Ljava/lang/String;

    aput-object v2, v5, v1

    .line 2403602
    iget-object v5, p0, LX/Gub;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v5}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f031461

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/RadioButton;

    .line 2403603
    const v6, 0x7f020625

    invoke-virtual {v5, v6}, Landroid/widget/RadioButton;->setButtonDrawable(I)V

    .line 2403604
    invoke-virtual {v5, v1}, Landroid/widget/RadioButton;->setId(I)V

    .line 2403605
    invoke-virtual {v5, v4}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 2403606
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/widget/RadioButton;->setSelected(Z)V

    .line 2403607
    move-object v4, v5

    .line 2403608
    invoke-virtual {v4, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2403609
    invoke-virtual {v0, v4}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    .line 2403610
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, 0x0

    const/4 v6, -0x1

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {v2, v5, v6, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v4, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2403611
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2403612
    :cond_2
    const/4 v1, 0x0

    iput v1, p0, LX/Gub;->c:I

    .line 2403613
    iget v1, p0, LX/Gub;->d:I

    if-ne v1, v8, :cond_4

    .line 2403614
    iget-object v1, p0, LX/GuX;->g:LX/BWN;

    .line 2403615
    iget-object v2, p2, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v2, v2

    .line 2403616
    const-string v3, "current_tab"

    invoke-interface {v1, v2, v3}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2403617
    if-eqz v1, :cond_3

    .line 2403618
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, LX/Gub;->c:I

    .line 2403619
    :cond_3
    :goto_2
    iget v1, p0, LX/Gub;->c:I

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->check(I)V

    .line 2403620
    new-instance v1, LX/Gua;

    invoke-direct {v1, p0}, LX/Gua;-><init>(LX/Gub;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 2403621
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setVisibility(I)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 2403622
    :catch_0
    move-exception v0

    .line 2403623
    iget-object v1, p0, LX/Gub;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v1}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->e()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Data format error"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 2403624
    :cond_4
    :try_start_1
    iget v1, p0, LX/Gub;->d:I

    iput v1, p0, LX/Gub;->c:I
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method
