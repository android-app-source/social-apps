.class public LX/F3T;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;


# direct methods
.method public constructor <init>(LX/0Px;Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;",
            ">;",
            "Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2194164
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2194165
    iput-object p1, p0, LX/F3T;->a:LX/0Px;

    .line 2194166
    iput-object p2, p0, LX/F3T;->b:Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    .line 2194167
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 2194145
    new-instance v0, Lcom/facebook/groups/editsettings/fragment/GroupPurposeItemView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/groups/editsettings/fragment/GroupPurposeItemView;-><init>(Landroid/content/Context;)V

    .line 2194146
    new-instance v1, LX/F3S;

    invoke-direct {v1, v0}, LX/F3S;-><init>(Lcom/facebook/groups/editsettings/fragment/GroupPurposeItemView;)V

    .line 2194147
    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 2194149
    iget-object v0, p0, LX/F3T;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    move-object v1, v0

    .line 2194150
    if-nez v1, :cond_0

    .line 2194151
    :goto_0
    return-void

    .line 2194152
    :cond_0
    check-cast p1, LX/F3S;

    .line 2194153
    iget-object v0, p0, LX/F3T;->b:Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v0

    iget-object v2, p0, LX/F3T;->b:Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    invoke-virtual {v2}, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 2194154
    :goto_1
    iget-object v2, p1, LX/F3S;->l:Lcom/facebook/groups/editsettings/fragment/GroupPurposeItemView;

    new-instance v3, LX/F3R;

    invoke-direct {v3, p0, v1}, LX/F3R;-><init>(LX/F3T;Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;)V

    .line 2194155
    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p0

    if-eqz p0, :cond_1

    .line 2194156
    iget-object p0, v2, Lcom/facebook/groups/editsettings/fragment/GroupPurposeItemView;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    sget-object p2, Lcom/facebook/groups/editsettings/fragment/GroupPurposeItemView;->j:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, p1, p2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2194157
    :cond_1
    iget-object p0, v2, Lcom/facebook/groups/editsettings/fragment/GroupPurposeItemView;->l:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2194158
    if-eqz v0, :cond_3

    const/4 p0, 0x0

    .line 2194159
    :goto_2
    iget-object p1, v2, Lcom/facebook/groups/editsettings/fragment/GroupPurposeItemView;->m:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {p1, p0}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2194160
    invoke-virtual {v2, v3}, Lcom/facebook/groups/editsettings/fragment/GroupPurposeItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2194161
    goto :goto_0

    .line 2194162
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2194163
    :cond_3
    const/16 p0, 0x8

    goto :goto_2
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2194148
    iget-object v0, p0, LX/F3T;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
