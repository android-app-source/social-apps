.class public LX/FfY;
.super LX/FfQ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FfQ",
        "<",
        "Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FfY;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2268602
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->LATEST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const v1, 0x7f08231c

    invoke-direct {p0, p1, v0, v1}, LX/FfQ;-><init>(Landroid/content/res/Resources;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;I)V

    .line 2268603
    return-void
.end method

.method public static a(LX/0QB;)LX/FfY;
    .locals 4

    .prologue
    .line 2268604
    sget-object v0, LX/FfY;->a:LX/FfY;

    if-nez v0, :cond_1

    .line 2268605
    const-class v1, LX/FfY;

    monitor-enter v1

    .line 2268606
    :try_start_0
    sget-object v0, LX/FfY;->a:LX/FfY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2268607
    if-eqz v2, :cond_0

    .line 2268608
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2268609
    new-instance p0, LX/FfY;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/FfY;-><init>(Landroid/content/res/Resources;)V

    .line 2268610
    move-object v0, p0

    .line 2268611
    sput-object v0, LX/FfY;->a:LX/FfY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2268612
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2268613
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2268614
    :cond_1
    sget-object v0, LX/FfY;->a:LX/FfY;

    return-object v0

    .line 2268615
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2268616
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/CwB;Ljava/lang/String;)LX/CwH;
    .locals 3
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2268617
    if-nez p2, :cond_0

    invoke-interface {p1}, LX/CwB;->jA_()Ljava/lang/String;

    move-result-object p2

    .line 2268618
    :cond_0
    invoke-interface {p1}, LX/CwB;->b()Ljava/lang/String;

    move-result-object v0

    .line 2268619
    invoke-interface {p1}, LX/CwB;->a()Ljava/lang/String;

    move-result-object v1

    .line 2268620
    const-string v2, "news_v2"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v0, v1}, LX/7BG;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2268621
    :goto_0
    invoke-virtual {p0, p1, p2}, LX/FfQ;->b(LX/CwB;Ljava/lang/String;)LX/CwH;

    move-result-object v1

    .line 2268622
    iput-object v0, v1, LX/CwH;->c:Ljava/lang/String;

    .line 2268623
    move-object v0, v1

    .line 2268624
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->LATEST:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2268625
    iput-object v1, v0, LX/CwH;->x:LX/0Px;

    .line 2268626
    move-object v0, v0

    .line 2268627
    return-object v0

    .line 2268628
    :cond_1
    invoke-static {v1}, LX/7BG;->o(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()LX/Fdv;
    .locals 1

    .prologue
    .line 2268629
    new-instance v0, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;

    invoke-direct {v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsLiveFeedFragment;-><init>()V

    return-object v0
.end method

.method public final synthetic c(LX/CwB;Ljava/lang/String;)LX/CwA;
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2268630
    invoke-virtual {p0, p1, p2}, LX/FfQ;->a(LX/CwB;Ljava/lang/String;)LX/CwH;

    move-result-object v0

    return-object v0
.end method
