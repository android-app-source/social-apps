.class public final enum LX/GMT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GMT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GMT;

.field public static final enum MAX:LX/GMT;

.field public static final enum MIN:LX/GMT;

.field public static final enum NONE:LX/GMT;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2344652
    new-instance v0, LX/GMT;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/GMT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GMT;->NONE:LX/GMT;

    .line 2344653
    new-instance v0, LX/GMT;

    const-string v1, "MIN"

    invoke-direct {v0, v1, v3}, LX/GMT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GMT;->MIN:LX/GMT;

    .line 2344654
    new-instance v0, LX/GMT;

    const-string v1, "MAX"

    invoke-direct {v0, v1, v4}, LX/GMT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GMT;->MAX:LX/GMT;

    .line 2344655
    const/4 v0, 0x3

    new-array v0, v0, [LX/GMT;

    sget-object v1, LX/GMT;->NONE:LX/GMT;

    aput-object v1, v0, v2

    sget-object v1, LX/GMT;->MIN:LX/GMT;

    aput-object v1, v0, v3

    sget-object v1, LX/GMT;->MAX:LX/GMT;

    aput-object v1, v0, v4

    sput-object v0, LX/GMT;->$VALUES:[LX/GMT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2344656
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GMT;
    .locals 1

    .prologue
    .line 2344657
    const-class v0, LX/GMT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GMT;

    return-object v0
.end method

.method public static values()[LX/GMT;
    .locals 1

    .prologue
    .line 2344658
    sget-object v0, LX/GMT;->$VALUES:[LX/GMT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GMT;

    return-object v0
.end method
