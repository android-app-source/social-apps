.class public LX/G4O;
.super LX/G4N;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BQg;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/EeJ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0Or;LX/0pi;LX/03V;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/BQg;",
            ">;",
            "LX/0Or",
            "<",
            "LX/EeJ;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0pi;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2317568
    invoke-direct {p0, p3, p5}, LX/G4N;-><init>(LX/0Or;LX/03V;)V

    .line 2317569
    iput-object p1, p0, LX/G4O;->a:LX/0Or;

    .line 2317570
    iput-object p2, p0, LX/G4O;->b:LX/0Or;

    .line 2317571
    return-void
.end method

.method public static b(LX/0QB;)LX/G4O;
    .locals 6

    .prologue
    .line 2317572
    new-instance v0, LX/G4O;

    const/16 v1, 0x36ab

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v2, 0x1764

    invoke-static {p0, v2}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    const/16 v3, 0xb83

    invoke-static {p0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0pi;->a(LX/0QB;)LX/0pi;

    move-result-object v4

    check-cast v4, LX/0pi;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct/range {v0 .. v5}, LX/G4O;-><init>(LX/0Or;LX/0Or;LX/0Or;LX/0pi;LX/03V;)V

    .line 2317573
    return-object v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2317574
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 2317575
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2317576
    const-string v2, "timeline_set_cover_photo"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2317577
    iget-object v0, p0, LX/G4O;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    const-string v2, "operationParams"

    invoke-virtual {p0, v1, v0, v2}, LX/G4N;->a(Landroid/os/Bundle;LX/0e6;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2317578
    :goto_0
    return-object v0

    .line 2317579
    :cond_0
    const-string v2, "timeline_set_profile_photo"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2317580
    iget-object v0, p0, LX/G4O;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0e6;

    const-string v2, "operationParams"

    invoke-virtual {p0, v1, v0, v2}, LX/G4N;->a(Landroid/os/Bundle;LX/0e6;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2317581
    :cond_1
    new-instance v1, LX/4B3;

    invoke-direct {v1, v0}, LX/4B3;-><init>(Ljava/lang/String;)V

    throw v1
.end method
