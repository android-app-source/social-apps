.class public LX/FA6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final b:Lorg/apache/http/client/ResponseHandler;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/http/client/ResponseHandler",
            "<",
            "LX/FA5;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/FA6;


# instance fields
.field public final a:Lcom/facebook/http/common/FbHttpRequestProcessor;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2206687
    new-instance v0, LX/FA4;

    invoke-direct {v0}, LX/FA4;-><init>()V

    sput-object v0, LX/FA6;->b:Lorg/apache/http/client/ResponseHandler;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/http/common/FbHttpRequestProcessor;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2206688
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2206689
    iput-object p1, p0, LX/FA6;->a:Lcom/facebook/http/common/FbHttpRequestProcessor;

    .line 2206690
    return-void
.end method

.method public static a(LX/0QB;)LX/FA6;
    .locals 4

    .prologue
    .line 2206691
    sget-object v0, LX/FA6;->c:LX/FA6;

    if-nez v0, :cond_1

    .line 2206692
    const-class v1, LX/FA6;

    monitor-enter v1

    .line 2206693
    :try_start_0
    sget-object v0, LX/FA6;->c:LX/FA6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2206694
    if-eqz v2, :cond_0

    .line 2206695
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2206696
    new-instance p0, LX/FA6;

    invoke-static {v0}, Lcom/facebook/http/common/FbHttpRequestProcessor;->a(LX/0QB;)Lcom/facebook/http/common/FbHttpRequestProcessor;

    move-result-object v3

    check-cast v3, Lcom/facebook/http/common/FbHttpRequestProcessor;

    invoke-direct {p0, v3}, LX/FA6;-><init>(Lcom/facebook/http/common/FbHttpRequestProcessor;)V

    .line 2206697
    move-object v0, p0

    .line 2206698
    sput-object v0, LX/FA6;->c:LX/FA6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2206699
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2206700
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2206701
    :cond_1
    sget-object v0, LX/FA6;->c:LX/FA6;

    return-object v0

    .line 2206702
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2206703
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
