.class public LX/GiW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "LX/GiV;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2386007
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2386008
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 3

    .prologue
    .line 2386009
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2386010
    const-string v1, "fields"

    const-string v2, "hash"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2386011
    const-string v1, "hash"

    invoke-static {}, LX/2Kx;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2386012
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "Get Devices from GLC"

    .line 2386013
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2386014
    move-object v1, v1

    .line 2386015
    const-string v2, "GET"

    .line 2386016
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2386017
    move-object v1, v1

    .line 2386018
    const-string v2, "glcdevices"

    .line 2386019
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2386020
    move-object v1, v1

    .line 2386021
    sget-object v2, LX/14S;->JSON:LX/14S;

    .line 2386022
    iput-object v2, v1, LX/14O;->k:LX/14S;

    .line 2386023
    move-object v1, v1

    .line 2386024
    invoke-virtual {v1, v0}, LX/14O;->a(Ljava/util/Map;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2386025
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2386026
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "data"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2386027
    if-eqz v0, :cond_0

    .line 2386028
    invoke-virtual {v0}, LX/0lF;->G()Ljava/util/Iterator;

    move-result-object v0

    .line 2386029
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2386030
    new-instance v1, LX/GiV;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0lF;

    invoke-direct {v1, v0}, LX/GiV;-><init>(LX/0lF;)V

    move-object v0, v1

    .line 2386031
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
