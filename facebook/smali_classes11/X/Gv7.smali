.class public LX/Gv7;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Gv7;


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2404797
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2404798
    sget-object v0, LX/0ax;->ce:Ljava/lang/String;

    const-class v1, Lcom/facebook/katana/activity/nearby/SubcategorySelectionActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2404799
    return-void
.end method

.method public static a(LX/0QB;)LX/Gv7;
    .locals 3

    .prologue
    .line 2404800
    sget-object v0, LX/Gv7;->a:LX/Gv7;

    if-nez v0, :cond_1

    .line 2404801
    const-class v1, LX/Gv7;

    monitor-enter v1

    .line 2404802
    :try_start_0
    sget-object v0, LX/Gv7;->a:LX/Gv7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2404803
    if-eqz v2, :cond_0

    .line 2404804
    :try_start_1
    new-instance v0, LX/Gv7;

    invoke-direct {v0}, LX/Gv7;-><init>()V

    .line 2404805
    move-object v0, v0

    .line 2404806
    sput-object v0, LX/Gv7;->a:LX/Gv7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2404807
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2404808
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2404809
    :cond_1
    sget-object v0, LX/Gv7;->a:LX/Gv7;

    return-object v0

    .line 2404810
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2404811
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
