.class public LX/Fsg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile n:LX/Fsg;


# instance fields
.field private final a:LX/0sa;

.field private final b:LX/0rq;

.field private final c:LX/0se;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0tI;

.field private final f:LX/0ad;

.field private final g:LX/0sU;

.field private final h:LX/0tG;

.field private final i:LX/A5q;

.field private final j:LX/0wo;

.field private final k:LX/0sX;

.field private final l:LX/0tO;

.field private final m:LX/0tQ;


# direct methods
.method public constructor <init>(LX/0sa;LX/0rq;LX/0se;LX/0Or;LX/0ad;LX/0tI;LX/0sU;LX/0tG;LX/A5q;LX/0wo;LX/0sX;LX/0tO;LX/0tQ;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0sa;",
            "LX/0rq;",
            "LX/0se;",
            "LX/0Or",
            "<",
            "LX/0wp;",
            ">;",
            "LX/0ad;",
            "LX/0tI;",
            "LX/0sU;",
            "LX/0tG;",
            "LX/A5q;",
            "LX/0wo;",
            "LX/0sX;",
            "LX/0tO;",
            "LX/0tQ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2297397
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2297398
    iput-object p1, p0, LX/Fsg;->a:LX/0sa;

    .line 2297399
    iput-object p2, p0, LX/Fsg;->b:LX/0rq;

    .line 2297400
    iput-object p3, p0, LX/Fsg;->c:LX/0se;

    .line 2297401
    iput-object p4, p0, LX/Fsg;->d:LX/0Or;

    .line 2297402
    iput-object p6, p0, LX/Fsg;->e:LX/0tI;

    .line 2297403
    iput-object p5, p0, LX/Fsg;->f:LX/0ad;

    .line 2297404
    iput-object p7, p0, LX/Fsg;->g:LX/0sU;

    .line 2297405
    iput-object p8, p0, LX/Fsg;->h:LX/0tG;

    .line 2297406
    iput-object p9, p0, LX/Fsg;->i:LX/A5q;

    .line 2297407
    iput-object p10, p0, LX/Fsg;->j:LX/0wo;

    .line 2297408
    iput-object p11, p0, LX/Fsg;->k:LX/0sX;

    .line 2297409
    iput-object p12, p0, LX/Fsg;->l:LX/0tO;

    .line 2297410
    iput-object p13, p0, LX/Fsg;->m:LX/0tQ;

    .line 2297411
    return-void
.end method

.method private a(LX/Fsp;I)LX/5xJ;
    .locals 9

    .prologue
    const/4 v6, 0x0

    .line 2297412
    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v0

    .line 2297413
    new-instance v1, LX/5xJ;

    invoke-direct {v1}, LX/5xJ;-><init>()V

    move-object v1, v1

    .line 2297414
    const-string v2, "with_actor_profile_video_playback"

    iget-object v3, p0, LX/Fsg;->f:LX/0ad;

    sget-short v4, LX/0wf;->aK:S

    invoke-interface {v3, v4, v6}, LX/0ad;->a(SZ)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "nodeId"

    .line 2297415
    iget-wide v7, p1, LX/Fsp;->a:J

    move-wide v4, v7

    .line 2297416
    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "timeline_stories"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "angora_attachment_cover_image_size"

    iget-object v4, p0, LX/Fsg;->a:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->s()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "angora_attachment_profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "reading_attachment_profile_image_width"

    iget-object v4, p0, LX/Fsg;->a:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->M()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "reading_attachment_profile_image_height"

    iget-object v4, p0, LX/Fsg;->a:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->N()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "goodwill_small_accent_image"

    iget-object v4, p0, LX/Fsg;->b:LX/0rq;

    invoke-virtual {v4}, LX/0rq;->h()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "image_large_aspect_height"

    iget-object v4, p0, LX/Fsg;->a:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->A()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "image_large_aspect_width"

    iget-object v4, p0, LX/Fsg;->a:LX/0sa;

    invoke-virtual {v4}, LX/0sa;->z()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "num_faceboxes_and_tags"

    iget-object v4, p0, LX/Fsg;->a:LX/0sa;

    .line 2297417
    iget-object v5, v4, LX/0sa;->b:Ljava/lang/Integer;

    move-object v4, v5

    .line 2297418
    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "default_image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v2

    const-string v3, "icon_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v2

    const-string v3, "include_replies_in_total_count"

    iget-object v4, p0, LX/Fsg;->f:LX/0ad;

    sget-short v5, LX/0wg;->i:S

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "action_location"

    sget-object v4, LX/0wD;->TIMELINE:LX/0wD;

    invoke-virtual {v4}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "omit_unseen_stories"

    .line 2297419
    iget-boolean v4, p1, LX/Fsp;->c:Z

    move v4, v4

    .line 2297420
    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "enable_download"

    iget-object v4, p0, LX/Fsg;->m:LX/0tQ;

    invoke-virtual {v4}, LX/0tQ;->c()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "automatic_photo_captioning_enabled"

    iget-object v4, p0, LX/Fsg;->k:LX/0sX;

    invoke-virtual {v4}, LX/0sX;->a()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "rich_text_posts_enabled"

    iget-object v4, p0, LX/Fsg;->l:LX/0tO;

    invoke-virtual {v4}, LX/0tO;->b()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "default_image_scale"

    if-nez v0, :cond_0

    sget-object v0, LX/0wB;->a:LX/0wC;

    :cond_0
    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2297421
    iget-object v0, p0, LX/Fsg;->d:LX/0Or;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Fsg;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    .line 2297422
    const/4 v0, 0x1

    move v0, v0

    .line 2297423
    if-eqz v0, :cond_1

    .line 2297424
    const-string v0, "scrubbing"

    const-string v2, "MPEG_DASH"

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2297425
    :cond_1
    iget-object v0, p1, LX/Fsp;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2297426
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2297427
    const-string v0, "timeline_section_after"

    .line 2297428
    iget-object v2, p1, LX/Fsp;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2297429
    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2297430
    :cond_2
    iget-object v0, p0, LX/Fsg;->c:LX/0se;

    iget-object v2, p0, LX/Fsg;->b:LX/0rq;

    invoke-virtual {v2}, LX/0rq;->c()LX/0wF;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0se;->a(LX/0gW;LX/0wF;)LX/0gW;

    .line 2297431
    iget-object v0, p0, LX/Fsg;->h:LX/0tG;

    invoke-virtual {v0, v1}, LX/0tG;->a(LX/0gW;)V

    .line 2297432
    iget-object v0, p0, LX/Fsg;->g:LX/0sU;

    const-string v2, "PROFILE"

    invoke-virtual {v0, v1, v2}, LX/0sU;->a(LX/0gW;Ljava/lang/String;)V

    .line 2297433
    iget-object v0, p0, LX/Fsg;->e:LX/0tI;

    invoke-virtual {v0, v1}, LX/0tI;->a(LX/0gW;)V

    .line 2297434
    invoke-static {v1}, LX/0wo;->a(LX/0gW;)V

    .line 2297435
    const/4 v0, 0x1

    .line 2297436
    iput-boolean v0, v1, LX/0gW;->l:Z

    .line 2297437
    return-object v1
.end method

.method public static a(LX/0QB;)LX/Fsg;
    .locals 3

    .prologue
    .line 2297438
    sget-object v0, LX/Fsg;->n:LX/Fsg;

    if-nez v0, :cond_1

    .line 2297439
    const-class v1, LX/Fsg;

    monitor-enter v1

    .line 2297440
    :try_start_0
    sget-object v0, LX/Fsg;->n:LX/Fsg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2297441
    if-eqz v2, :cond_0

    .line 2297442
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/Fsg;->b(LX/0QB;)LX/Fsg;

    move-result-object v0

    sput-object v0, LX/Fsg;->n:LX/Fsg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2297443
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2297444
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2297445
    :cond_1
    sget-object v0, LX/Fsg;->n:LX/Fsg;

    return-object v0

    .line 2297446
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2297447
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/Fsg;
    .locals 14

    .prologue
    .line 2297448
    new-instance v0, LX/Fsg;

    invoke-static {p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v1

    check-cast v1, LX/0sa;

    invoke-static {p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v2

    check-cast v2, LX/0rq;

    invoke-static {p0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v3

    check-cast v3, LX/0se;

    const/16 v4, 0x12e4

    invoke-static {p0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {p0}, LX/0tI;->a(LX/0QB;)LX/0tI;

    move-result-object v6

    check-cast v6, LX/0tI;

    invoke-static {p0}, LX/0sU;->a(LX/0QB;)LX/0sU;

    move-result-object v7

    check-cast v7, LX/0sU;

    invoke-static {p0}, LX/0tG;->a(LX/0QB;)LX/0tG;

    move-result-object v8

    check-cast v8, LX/0tG;

    invoke-static {p0}, LX/A5q;->a(LX/0QB;)LX/A5q;

    move-result-object v9

    check-cast v9, LX/A5q;

    invoke-static {p0}, LX/0wo;->a(LX/0QB;)LX/0wo;

    move-result-object v10

    check-cast v10, LX/0wo;

    invoke-static {p0}, LX/0sX;->b(LX/0QB;)LX/0sX;

    move-result-object v11

    check-cast v11, LX/0sX;

    invoke-static {p0}, LX/0tO;->a(LX/0QB;)LX/0tO;

    move-result-object v12

    check-cast v12, LX/0tO;

    invoke-static {p0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v13

    check-cast v13, LX/0tQ;

    invoke-direct/range {v0 .. v13}, LX/Fsg;-><init>(LX/0sa;LX/0rq;LX/0se;LX/0Or;LX/0ad;LX/0tI;LX/0sU;LX/0tG;LX/A5q;LX/0wo;LX/0sX;LX/0tO;LX/0tQ;)V

    .line 2297449
    return-object v0
.end method


# virtual methods
.method public final a(LX/Fsp;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;I)LX/0zO;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Fsp;",
            "LX/0zS;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "I)",
            "LX/0zO",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2297450
    invoke-direct {p0, p1, p5}, LX/Fsg;->a(LX/Fsp;I)LX/5xJ;

    move-result-object v0

    .line 2297451
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 2297452
    iput-object p4, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2297453
    move-object v0, v0

    .line 2297454
    iput p3, v0, LX/0zO;->B:I

    .line 2297455
    move-object v0, v0

    .line 2297456
    invoke-virtual {v0, p2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    return-object v0
.end method
