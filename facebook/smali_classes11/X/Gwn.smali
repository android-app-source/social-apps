.class public final LX/Gwn;
.super Landroid/webkit/WebChromeClient;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/view/LoggedOutWebViewActivity;)V
    .locals 0

    .prologue
    .line 2407362
    iput-object p1, p0, LX/Gwn;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/katana/view/LoggedOutWebViewActivity;B)V
    .locals 0

    .prologue
    .line 2407372
    invoke-direct {p0, p1}, LX/Gwn;-><init>(Lcom/facebook/katana/view/LoggedOutWebViewActivity;)V

    return-void
.end method


# virtual methods
.method public final onShowFileChooser(Landroid/webkit/WebView;Landroid/webkit/ValueCallback;Landroid/webkit/WebChromeClient$FileChooserParams;)Z
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ImprovedNewApi"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/WebView;",
            "Landroid/webkit/ValueCallback",
            "<[",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/webkit/WebChromeClient$FileChooserParams;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2407373
    iget-object v0, p0, LX/Gwn;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    iget-object v0, v0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->B:Landroid/webkit/ValueCallback;

    if-eqz v0, :cond_0

    .line 2407374
    iget-object v0, p0, LX/Gwn;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    iget-object v0, v0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->B:Landroid/webkit/ValueCallback;

    invoke-interface {v0, v4}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    .line 2407375
    iget-object v0, p0, LX/Gwn;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    .line 2407376
    iput-object v4, v0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->B:Landroid/webkit/ValueCallback;

    .line 2407377
    :cond_0
    iget-object v0, p0, LX/Gwn;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    .line 2407378
    iput-object p2, v0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->B:Landroid/webkit/ValueCallback;

    .line 2407379
    invoke-virtual {p3}, Landroid/webkit/WebChromeClient$FileChooserParams;->createIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2407380
    :try_start_0
    iget-object v1, p0, LX/Gwn;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    iget-object v1, v1, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->w:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x2

    iget-object v3, p0, LX/Gwn;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/app/Activity;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2407381
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 2407382
    :catch_0
    iget-object v0, p0, LX/Gwn;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    .line 2407383
    iput-object v4, v0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->B:Landroid/webkit/ValueCallback;

    .line 2407384
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2407370
    const-string v0, ""

    invoke-virtual {p0, p1, p2, v0}, LX/Gwn;->openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;Ljava/lang/String;)V

    .line 2407371
    return-void
.end method

.method public openFileChooser(Landroid/webkit/ValueCallback;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2407363
    iget-object v0, p0, LX/Gwn;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    .line 2407364
    iput-object p1, v0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->A:Landroid/webkit/ValueCallback;

    .line 2407365
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2407366
    const-string v1, "android.intent.category.OPENABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 2407367
    invoke-virtual {v0, p2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2407368
    :try_start_0
    iget-object v1, p0, LX/Gwn;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    iget-object v1, v1, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->w:Lcom/facebook/content/SecureContextHelper;

    const/4 v2, 0x1

    iget-object v3, p0, LX/Gwn;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/app/Activity;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2407369
    :goto_0
    return-void

    :catch_0
    goto :goto_0
.end method
