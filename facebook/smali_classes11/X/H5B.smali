.class public LX/H5B;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "LX/H58;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/DrB;

.field public final d:LX/H59;

.field public final e:Landroid/content/Context;

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/DrB;LX/H59;Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2423652
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2423653
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/H5B;->a:Ljava/util/LinkedHashMap;

    .line 2423654
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/H5B;->b:Ljava/util/List;

    .line 2423655
    iput-object p1, p0, LX/H5B;->c:LX/DrB;

    .line 2423656
    iput-object p2, p0, LX/H5B;->d:LX/H59;

    .line 2423657
    iput-object p3, p0, LX/H5B;->e:Landroid/content/Context;

    .line 2423658
    return-void
.end method

.method private a(LX/2nq;)LX/H58;
    .locals 13
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2423659
    invoke-interface {p1}, LX/2nq;->j()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 2423660
    :cond_0
    :goto_0
    return-object v0

    .line 2423661
    :cond_1
    invoke-interface {p1}, LX/2nq;->j()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_7

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    .line 2423662
    iget-object v5, p0, LX/H5B;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v5, p0, LX/H5B;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/H58;

    :goto_2
    move-object v0, v5

    .line 2423663
    if-eqz v0, :cond_6

    .line 2423664
    iget-object v6, v0, LX/H58;->d:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    invoke-virtual {v6}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->b()I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_2

    iget-object v6, v0, LX/H58;->c:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    iget-object v7, v0, LX/H58;->d:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    invoke-virtual {v7}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->b()I

    move-result v7

    if-ge v6, v7, :cond_a

    :cond_2
    const/4 v6, 0x1

    :goto_3
    move v6, v6

    .line 2423665
    if-eqz v6, :cond_9

    .line 2423666
    iget-object v6, v0, LX/H58;->d:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    invoke-virtual {v6}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->c()I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_3

    invoke-interface {p1}, LX/2nq;->l()I

    move-result v6

    iget-object v7, v0, LX/H58;->d:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    invoke-virtual {v7}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->c()I

    move-result v7

    if-gt v6, v7, :cond_b

    :cond_3
    const/4 v6, 0x1

    :goto_4
    move v6, v6

    .line 2423667
    if-eqz v6, :cond_9

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2423668
    iget-object v8, v0, LX/H58;->d:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    invoke-virtual {v8}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->e()Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;

    move-result-object v8

    .line 2423669
    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v9

    if-nez v9, :cond_c

    .line 2423670
    :cond_4
    :goto_5
    :pswitch_0
    move v6, v6

    .line 2423671
    if-eqz v6, :cond_9

    const/4 v7, 0x1

    .line 2423672
    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v8

    if-eqz v8, :cond_5

    iget-object v8, v0, LX/H58;->d:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    invoke-virtual {v8}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->d()I

    move-result v8

    const/4 v9, -0x1

    if-eq v8, v9, :cond_5

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v9

    const-wide/16 v11, 0x0

    cmp-long v8, v9, v11

    if-nez v8, :cond_f

    .line 2423673
    :cond_5
    :goto_6
    move v6, v7

    .line 2423674
    if-eqz v6, :cond_9

    .line 2423675
    iget-object v6, v0, LX/H58;->e:LX/H55;

    invoke-interface {v6, p1}, LX/H55;->a(LX/2nq;)Z

    move-result v6

    move v6, v6

    .line 2423676
    if-eqz v6, :cond_9

    const/4 v6, 0x1

    :goto_7
    move v5, v6

    .line 2423677
    if-nez v5, :cond_0

    .line 2423678
    :cond_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    :cond_7
    move-object v0, v1

    .line 2423679
    goto/16 :goto_0

    :cond_8
    const/4 v5, 0x0

    goto :goto_2

    :cond_9
    const/4 v6, 0x0

    goto :goto_7

    :cond_a
    const/4 v6, 0x0

    goto :goto_3

    :cond_b
    const/4 v6, 0x0

    goto :goto_4

    .line 2423680
    :cond_c
    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v9

    .line 2423681
    sget-object v10, LX/H57;->a:[I

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLNotificationSeenFilter;->ordinal()I

    move-result v8

    aget v8, v10, v8

    packed-switch v8, :pswitch_data_0

    move v6, v7

    .line 2423682
    goto :goto_5

    .line 2423683
    :pswitch_1
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->UNSEEN_AND_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-eq v9, v8, :cond_4

    .line 2423684
    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, LX/H58;->a(LX/H58;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    move v6, v7

    goto :goto_5

    .line 2423685
    :pswitch_2
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-eq v9, v8, :cond_d

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_BUT_UNREAD:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    if-ne v9, v8, :cond_e

    :cond_d
    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v8}, LX/H58;->a(LX/H58;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_4

    :cond_e
    move v6, v7

    goto/16 :goto_5

    .line 2423686
    :cond_f
    iget-object v8, v0, LX/H58;->b:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v9

    const-wide/16 v11, 0x3e8

    div-long/2addr v9, v11

    invoke-interface {p1}, LX/2nq;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLStory;->U()J

    move-result-wide v11

    sub-long/2addr v9, v11

    .line 2423687
    const-wide/16 v11, 0x3c

    div-long/2addr v9, v11

    iget-object v8, v0, LX/H58;->d:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    invoke-virtual {v8}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->d()I

    move-result v8

    int-to-long v11, v8

    cmp-long v8, v9, v11

    if-ltz v8, :cond_5

    const/4 v7, 0x0

    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static d(LX/H5B;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2423688
    iget-object v1, p0, LX/H5B;->f:LX/0Px;

    if-nez v1, :cond_3

    .line 2423689
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2423690
    iget-object v1, p0, LX/H5B;->a:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    move v2, v0

    .line 2423691
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2423692
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H58;

    .line 2423693
    iget-object v5, v0, LX/H58;->c:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    move v5, v5

    .line 2423694
    if-nez v5, :cond_0

    .line 2423695
    iget-object v5, v0, LX/H58;->d:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    move-object v5, v5

    .line 2423696
    invoke-virtual {v5}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->FALLBACK:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    if-eq v5, v6, :cond_0

    .line 2423697
    const/4 v2, 0x1

    .line 2423698
    iget-object v5, v0, LX/H58;->d:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    move-object v5, v5

    .line 2423699
    invoke-virtual {v5}, Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;->FRIEND_REQUESTS:Lcom/facebook/graphql/enums/GraphQLNotificationBucketType;

    if-ne v5, v6, :cond_1

    .line 2423700
    iget-object v1, v0, LX/H58;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    move v1, v1

    .line 2423701
    :cond_1
    new-instance v5, LX/3Ci;

    .line 2423702
    iget-object v6, v0, LX/H58;->d:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    move-object v6, v6

    .line 2423703
    invoke-direct {v5, v6, v1}, LX/3Ci;-><init>(Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;I)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2423704
    iget-object v5, v0, LX/H58;->c:Ljava/util/List;

    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v5

    move-object v5, v5

    .line 2423705
    invoke-interface {v3, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2423706
    new-instance v5, LX/3Ch;

    .line 2423707
    iget-object v6, v0, LX/H58;->d:Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;

    move-object v0, v6

    .line 2423708
    invoke-direct {v5, v0, v1}, LX/3Ch;-><init>(Lcom/facebook/notifications/protocol/FetchNotificationsBucketGraphQLModels$NotificationsBucketFieldsModel;I)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2423709
    new-instance v0, LX/3Cj;

    invoke-direct {v0}, LX/3Cj;-><init>()V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2423710
    :cond_2
    if-eqz v2, :cond_4

    .line 2423711
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v3, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2423712
    :goto_1
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/H5B;->f:LX/0Px;

    .line 2423713
    :cond_3
    iget-object v0, p0, LX/H5B;->f:LX/0Px;

    return-object v0

    .line 2423714
    :cond_4
    iget-object v0, p0, LX/H5B;->b:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/2nq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2423715
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2423716
    :goto_0
    return-void

    .line 2423717
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2nq;

    .line 2423718
    invoke-direct {p0, v0}, LX/H5B;->a(LX/2nq;)LX/H58;

    move-result-object v2

    .line 2423719
    if-eqz v2, :cond_1

    .line 2423720
    iget-object v3, v2, LX/H58;->c:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2423721
    goto :goto_1

    .line 2423722
    :cond_2
    iget-object v0, p0, LX/H5B;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2423723
    const/4 v0, 0x0

    iput-object v0, p0, LX/H5B;->f:LX/0Px;

    goto :goto_0
.end method
