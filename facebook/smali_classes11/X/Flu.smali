.class public final enum LX/Flu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Flu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Flu;

.field public static final enum DONATED:LX/Flu;

.field public static final enum INVITED:LX/Flu;

.field public static final enum NOT_DONATED:LX/Flu;

.field public static final enum SHARED:LX/Flu;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2281671
    new-instance v0, LX/Flu;

    const-string v1, "INVITED"

    invoke-direct {v0, v1, v2}, LX/Flu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Flu;->INVITED:LX/Flu;

    .line 2281672
    new-instance v0, LX/Flu;

    const-string v1, "SHARED"

    invoke-direct {v0, v1, v3}, LX/Flu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Flu;->SHARED:LX/Flu;

    .line 2281673
    new-instance v0, LX/Flu;

    const-string v1, "DONATED"

    invoke-direct {v0, v1, v4}, LX/Flu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Flu;->DONATED:LX/Flu;

    .line 2281674
    new-instance v0, LX/Flu;

    const-string v1, "NOT_DONATED"

    invoke-direct {v0, v1, v5}, LX/Flu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Flu;->NOT_DONATED:LX/Flu;

    .line 2281675
    const/4 v0, 0x4

    new-array v0, v0, [LX/Flu;

    sget-object v1, LX/Flu;->INVITED:LX/Flu;

    aput-object v1, v0, v2

    sget-object v1, LX/Flu;->SHARED:LX/Flu;

    aput-object v1, v0, v3

    sget-object v1, LX/Flu;->DONATED:LX/Flu;

    aput-object v1, v0, v4

    sget-object v1, LX/Flu;->NOT_DONATED:LX/Flu;

    aput-object v1, v0, v5

    sput-object v0, LX/Flu;->$VALUES:[LX/Flu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2281676
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/Flu;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2281677
    if-nez p0, :cond_0

    .line 2281678
    :goto_0
    return-object v0

    .line 2281679
    :cond_0
    const/4 v1, -0x1

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_1
    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2281680
    :pswitch_0
    sget-object v0, LX/Flu;->INVITED:LX/Flu;

    goto :goto_0

    .line 2281681
    :sswitch_0
    const-string v2, "INVITED"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x0

    goto :goto_1

    :sswitch_1
    const-string v2, "SHARED"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :sswitch_2
    const-string v2, "DONATED"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x2

    goto :goto_1

    :sswitch_3
    const-string v2, "NOT_DONATED"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v1, 0x3

    goto :goto_1

    .line 2281682
    :pswitch_1
    sget-object v0, LX/Flu;->SHARED:LX/Flu;

    goto :goto_0

    .line 2281683
    :pswitch_2
    sget-object v0, LX/Flu;->DONATED:LX/Flu;

    goto :goto_0

    .line 2281684
    :pswitch_3
    sget-object v0, LX/Flu;->NOT_DONATED:LX/Flu;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x7442fdd7 -> :sswitch_3
        -0x6e485f9b -> :sswitch_1
        -0x679f704b -> :sswitch_2
        -0x6060c1a5 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/Flu;
    .locals 1

    .prologue
    .line 2281685
    const-class v0, LX/Flu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Flu;

    return-object v0
.end method

.method public static values()[LX/Flu;
    .locals 1

    .prologue
    .line 2281686
    sget-object v0, LX/Flu;->$VALUES:[LX/Flu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Flu;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2281687
    invoke-virtual {p0}, LX/Flu;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
