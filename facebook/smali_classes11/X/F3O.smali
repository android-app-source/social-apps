.class public final LX/F3O;
.super LX/6oO;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;)V
    .locals 0

    .prologue
    .line 2194080
    iput-object p1, p0, LX/F3O;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    invoke-direct {p0}, LX/6oO;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .prologue
    .line 2194069
    iget-object v0, p0, LX/F3O;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->u:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 2194070
    iget-object v0, p0, LX/F3O;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->t:Landroid/os/Handler;

    iget-object v1, p0, LX/F3O;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    iget-object v1, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->u:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2194071
    :cond_0
    iget-object v0, p0, LX/F3O;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    .line 2194072
    iget-object v1, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->l:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getUserEnteredPlainText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2194073
    iget-object v1, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->n:Lcom/facebook/widget/text/BetterTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2194074
    :goto_0
    iget-object v0, p0, LX/F3O;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->m:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 2194075
    iget-object v0, p0, LX/F3O;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    new-instance v1, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment$8$1;

    invoke-direct {v1, p0}, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment$8$1;-><init>(LX/F3O;)V

    .line 2194076
    iput-object v1, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->u:Ljava/lang/Runnable;

    .line 2194077
    iget-object v0, p0, LX/F3O;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->t:Landroid/os/Handler;

    iget-object v1, p0, LX/F3O;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    iget-object v1, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->u:Ljava/lang/Runnable;

    const-wide/16 v2, 0x12c

    const v4, 0x35496f67

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2194078
    return-void

    .line 2194079
    :cond_1
    iget-object v1, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->n:Lcom/facebook/widget/text/BetterTextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    goto :goto_0
.end method
