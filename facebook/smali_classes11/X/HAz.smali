.class public final LX/HAz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;)V
    .locals 0

    .prologue
    .line 2436305
    iput-object p1, p0, LX/HAz;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x2

    const v0, -0x570cbb9e

    invoke-static {v7, v8, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2436306
    new-instance v1, LX/31Y;

    iget-object v2, p0, LX/HAz;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/31Y;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, LX/HAz;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08165e

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    iget-object v2, p0, LX/HAz;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08165f

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, LX/HAz;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    iget-object v6, v6, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v6}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->n()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    iget-object v5, p0, LX/HAz;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    iget-object v5, v5, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->l:Ljava/lang/String;

    aput-object v5, v4, v8

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080019

    new-instance v3, LX/HAy;

    invoke-direct {v3, p0}, LX/HAy;-><init>(LX/HAz;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080017

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    .line 2436307
    const v1, 0x485cd858    # 226145.38f

    invoke-static {v7, v7, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
