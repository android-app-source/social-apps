.class public LX/Gkq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:J

.field public e:J

.field public f:I

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

.field public m:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

.field public n:Z

.field public o:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;


# direct methods
.method public constructor <init>(LX/Gkq;)V
    .locals 4

    .prologue
    .line 2389588
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2389589
    iget-object v0, p1, LX/Gkq;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2389590
    iput-object v0, p0, LX/Gkq;->a:Ljava/lang/String;

    .line 2389591
    iget-object v0, p1, LX/Gkq;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2389592
    iput-object v0, p0, LX/Gkq;->b:Ljava/lang/String;

    .line 2389593
    iget-boolean v0, p1, LX/Gkq;->g:Z

    move v0, v0

    .line 2389594
    iput-boolean v0, p0, LX/Gkq;->g:Z

    .line 2389595
    iget-boolean v0, p1, LX/Gkq;->h:Z

    move v0, v0

    .line 2389596
    iput-boolean v0, p0, LX/Gkq;->h:Z

    .line 2389597
    iget-object v0, p1, LX/Gkq;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2389598
    iput-object v0, p0, LX/Gkq;->c:Ljava/lang/String;

    .line 2389599
    iget v0, p1, LX/Gkq;->f:I

    move v0, v0

    .line 2389600
    iput v0, p0, LX/Gkq;->f:I

    .line 2389601
    iget-boolean v0, p1, LX/Gkq;->i:Z

    move v0, v0

    .line 2389602
    iput-boolean v0, p0, LX/Gkq;->i:Z

    .line 2389603
    iget-wide v2, p1, LX/Gkq;->e:J

    move-wide v0, v2

    .line 2389604
    iput-wide v0, p0, LX/Gkq;->e:J

    .line 2389605
    iget-wide v2, p1, LX/Gkq;->d:J

    move-wide v0, v2

    .line 2389606
    iput-wide v0, p0, LX/Gkq;->d:J

    .line 2389607
    iget-boolean v0, p1, LX/Gkq;->j:Z

    move v0, v0

    .line 2389608
    iput-boolean v0, p0, LX/Gkq;->j:Z

    .line 2389609
    iget-object v0, p1, LX/Gkq;->l:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    move-object v0, v0

    .line 2389610
    iput-object v0, p0, LX/Gkq;->l:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    .line 2389611
    iget-object v0, p1, LX/Gkq;->m:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    move-object v0, v0

    .line 2389612
    iput-object v0, p0, LX/Gkq;->m:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    .line 2389613
    iget-boolean v0, p1, LX/Gkq;->n:Z

    move v0, v0

    .line 2389614
    iput-boolean v0, p0, LX/Gkq;->n:Z

    .line 2389615
    iget-boolean v0, p1, LX/Gkq;->k:Z

    move v0, v0

    .line 2389616
    iput-boolean v0, p0, LX/Gkq;->k:Z

    .line 2389617
    iget-object v0, p1, LX/Gkq;->o:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-object v0, v0

    .line 2389618
    iput-object v0, p0, LX/Gkq;->o:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 2389619
    return-void
.end method

.method public constructor <init>(Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2389553
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2389554
    invoke-virtual {p1}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;->p()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Gkq;->a:Ljava/lang/String;

    .line 2389555
    invoke-virtual {p1}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;->s()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Gkq;->b:Ljava/lang/String;

    .line 2389556
    invoke-virtual {p1}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;->n()Z

    move-result v0

    iput-boolean v0, p0, LX/Gkq;->g:Z

    .line 2389557
    invoke-virtual {p1}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;->o()Z

    move-result v0

    iput-boolean v0, p0, LX/Gkq;->h:Z

    .line 2389558
    invoke-virtual {p1}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;->j()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, LX/Gkq;->k:Z

    .line 2389559
    const/4 v0, 0x0

    iput-object v0, p0, LX/Gkq;->c:Ljava/lang/String;

    .line 2389560
    invoke-virtual {p1}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 2389561
    invoke-virtual {p1}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2389562
    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_6

    .line 2389563
    invoke-virtual {p1}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2389564
    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    if-eqz v0, :cond_0

    .line 2389565
    invoke-virtual {p1}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;->l()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Gkq;->c:Ljava/lang/String;

    .line 2389566
    :cond_0
    iput v2, p0, LX/Gkq;->f:I

    .line 2389567
    invoke-virtual {p1}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_1

    .line 2389568
    invoke-virtual {p1}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;->m()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v2}, LX/15i;->j(II)I

    move-result v0

    iput v0, p0, LX/Gkq;->f:I

    .line 2389569
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;->q()Z

    move-result v0

    iput-boolean v0, p0, LX/Gkq;->i:Z

    .line 2389570
    invoke-virtual {p1}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;->r()J

    move-result-wide v4

    iput-wide v4, p0, LX/Gkq;->e:J

    .line 2389571
    invoke-virtual {p1}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;->v()J

    move-result-wide v4

    iput-wide v4, p0, LX/Gkq;->d:J

    .line 2389572
    invoke-virtual {p1}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;->u()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->MEMBER:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    if-ne v0, v3, :cond_7

    move v0, v1

    :goto_3
    iput-boolean v0, p0, LX/Gkq;->j:Z

    .line 2389573
    invoke-virtual {p1}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;->x()Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    move-result-object v0

    iput-object v0, p0, LX/Gkq;->l:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    .line 2389574
    invoke-virtual {p1}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;->w()Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    move-result-object v0

    iput-object v0, p0, LX/Gkq;->m:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    .line 2389575
    invoke-virtual {p1}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;->k()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->INTEREST:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-ne v0, v3, :cond_8

    :goto_4
    iput-boolean v1, p0, LX/Gkq;->n:Z

    .line 2389576
    invoke-virtual {p1}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;->t()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v0

    iput-object v0, p0, LX/Gkq;->o:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    .line 2389577
    return-void

    :cond_2
    move v0, v2

    .line 2389578
    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 2389579
    goto/16 :goto_1

    :cond_4
    move v0, v2

    goto/16 :goto_1

    :cond_5
    move v0, v2

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    move v0, v2

    .line 2389580
    goto :goto_3

    :cond_8
    move v1, v2

    .line 2389581
    goto :goto_4
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2389587
    iget-object v0, p0, LX/Gkq;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2389586
    iget-boolean v0, p0, LX/Gkq;->g:Z

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 2389585
    iget-boolean v0, p0, LX/Gkq;->h:Z

    return v0
.end method

.method public final k()Lcom/facebook/graphql/enums/GraphQLGroupAdminType;
    .locals 1

    .prologue
    .line 2389584
    iget-object v0, p0, LX/Gkq;->o:Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    return-object v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 2389583
    iget-boolean v0, p0, LX/Gkq;->k:Z

    return v0
.end method

.method public final o()Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;
    .locals 1

    .prologue
    .line 2389582
    iget-object v0, p0, LX/Gkq;->l:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    return-object v0
.end method
