.class public final enum LX/Gdh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gdh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gdh;

.field public static final enum FETCH_LOCATION:LX/Gdh;

.field public static final enum FETCH_RADIUS:LX/Gdh;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2374419
    new-instance v0, LX/Gdh;

    const-string v1, "FETCH_LOCATION"

    invoke-direct {v0, v1, v2}, LX/Gdh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gdh;->FETCH_LOCATION:LX/Gdh;

    .line 2374420
    new-instance v0, LX/Gdh;

    const-string v1, "FETCH_RADIUS"

    invoke-direct {v0, v1, v3}, LX/Gdh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gdh;->FETCH_RADIUS:LX/Gdh;

    .line 2374421
    const/4 v0, 0x2

    new-array v0, v0, [LX/Gdh;

    sget-object v1, LX/Gdh;->FETCH_LOCATION:LX/Gdh;

    aput-object v1, v0, v2

    sget-object v1, LX/Gdh;->FETCH_RADIUS:LX/Gdh;

    aput-object v1, v0, v3

    sput-object v0, LX/Gdh;->$VALUES:[LX/Gdh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2374424
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gdh;
    .locals 1

    .prologue
    .line 2374423
    const-class v0, LX/Gdh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gdh;

    return-object v0
.end method

.method public static values()[LX/Gdh;
    .locals 1

    .prologue
    .line 2374422
    sget-object v0, LX/Gdh;->$VALUES:[LX/Gdh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gdh;

    return-object v0
.end method
