.class public LX/G4P;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1cj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/1cj",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/G4B;

.field private b:I
    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public c:I


# direct methods
.method public constructor <init>(LX/G4B;)V
    .locals 0

    .prologue
    .line 2317582
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2317583
    iput-object p1, p0, LX/G4P;->a:LX/G4B;

    .line 2317584
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2317585
    iget v0, p0, LX/G4P;->b:I

    iget v1, p0, LX/G4P;->c:I

    if-ne v0, v1, :cond_0

    .line 2317586
    iget-object v0, p0, LX/G4P;->a:LX/G4B;

    invoke-virtual {v0}, LX/G4B;->b()V

    .line 2317587
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(LX/1ca;)V
    .locals 1

    .prologue
    .line 2317588
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/G4P;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/G4P;->b:I

    .line 2317589
    invoke-direct {p0}, LX/G4P;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2317590
    monitor-exit p0

    return-void

    .line 2317591
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/1ca;)V
    .locals 1

    .prologue
    .line 2317592
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/G4P;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/G4P;->b:I

    .line 2317593
    invoke-direct {p0}, LX/G4P;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2317594
    monitor-exit p0

    return-void

    .line 2317595
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(LX/1ca;)V
    .locals 1

    .prologue
    .line 2317596
    monitor-enter p0

    :try_start_0
    iget v0, p0, LX/G4P;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/G4P;->b:I

    .line 2317597
    invoke-direct {p0}, LX/G4P;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2317598
    monitor-exit p0

    return-void

    .line 2317599
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d(LX/1ca;)V
    .locals 0

    .prologue
    .line 2317600
    return-void
.end method
