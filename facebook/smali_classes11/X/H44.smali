.class public final enum LX/H44;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/H44;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/H44;

.field public static final enum ALL:LX/H44;

.field public static final enum ARTS:LX/H44;

.field public static final enum COFFEE:LX/H44;

.field public static final enum HOTELS:LX/H44;

.field public static final enum INVALID:LX/H44;

.field public static final enum NIGHTLIFE:LX/H44;

.field public static final enum OUTDOORS:LX/H44;

.field public static final enum RESTAURANTS:LX/H44;

.field public static final enum SHOPPING:LX/H44;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2422285
    new-instance v0, LX/H44;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v3}, LX/H44;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H44;->INVALID:LX/H44;

    .line 2422286
    new-instance v0, LX/H44;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v4}, LX/H44;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H44;->ALL:LX/H44;

    .line 2422287
    new-instance v0, LX/H44;

    const-string v1, "RESTAURANTS"

    invoke-direct {v0, v1, v5}, LX/H44;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H44;->RESTAURANTS:LX/H44;

    .line 2422288
    new-instance v0, LX/H44;

    const-string v1, "COFFEE"

    invoke-direct {v0, v1, v6}, LX/H44;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H44;->COFFEE:LX/H44;

    .line 2422289
    new-instance v0, LX/H44;

    const-string v1, "NIGHTLIFE"

    invoke-direct {v0, v1, v7}, LX/H44;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H44;->NIGHTLIFE:LX/H44;

    .line 2422290
    new-instance v0, LX/H44;

    const-string v1, "OUTDOORS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/H44;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H44;->OUTDOORS:LX/H44;

    .line 2422291
    new-instance v0, LX/H44;

    const-string v1, "ARTS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/H44;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H44;->ARTS:LX/H44;

    .line 2422292
    new-instance v0, LX/H44;

    const-string v1, "HOTELS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/H44;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H44;->HOTELS:LX/H44;

    .line 2422293
    new-instance v0, LX/H44;

    const-string v1, "SHOPPING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/H44;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/H44;->SHOPPING:LX/H44;

    .line 2422294
    const/16 v0, 0x9

    new-array v0, v0, [LX/H44;

    sget-object v1, LX/H44;->INVALID:LX/H44;

    aput-object v1, v0, v3

    sget-object v1, LX/H44;->ALL:LX/H44;

    aput-object v1, v0, v4

    sget-object v1, LX/H44;->RESTAURANTS:LX/H44;

    aput-object v1, v0, v5

    sget-object v1, LX/H44;->COFFEE:LX/H44;

    aput-object v1, v0, v6

    sget-object v1, LX/H44;->NIGHTLIFE:LX/H44;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/H44;->OUTDOORS:LX/H44;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/H44;->ARTS:LX/H44;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/H44;->HOTELS:LX/H44;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/H44;->SHOPPING:LX/H44;

    aput-object v2, v0, v1

    sput-object v0, LX/H44;->$VALUES:[LX/H44;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2422295
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/H44;
    .locals 1

    .prologue
    .line 2422296
    const-class v0, LX/H44;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/H44;

    return-object v0
.end method

.method public static values()[LX/H44;
    .locals 1

    .prologue
    .line 2422297
    sget-object v0, LX/H44;->$VALUES:[LX/H44;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/H44;

    return-object v0
.end method
