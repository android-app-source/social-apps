.class public LX/HAF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/HAF;


# instance fields
.field private final a:LX/HDT;


# direct methods
.method public constructor <init>(LX/HDT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2434934
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2434935
    iput-object p1, p0, LX/HAF;->a:LX/HDT;

    .line 2434936
    return-void
.end method

.method public static a(LX/H8L;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/H8L;",
            ")",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2434937
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2434938
    invoke-interface {p0}, LX/H8L;->getSupportedActionTypes()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;

    .line 2434939
    sget-object v5, LX/HAE;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 2434940
    const/4 v5, 0x0

    :goto_1
    move-object v0, v5

    .line 2434941
    if-eqz v0, :cond_0

    .line 2434942
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2434943
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2434944
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    .line 2434945
    :pswitch_0
    new-instance v5, LX/HA8;

    invoke-direct {v5, p0}, LX/HA8;-><init>(LX/H8L;)V

    goto :goto_1

    .line 2434946
    :pswitch_1
    new-instance v5, LX/HA9;

    invoke-direct {v5, p0}, LX/HA9;-><init>(LX/H8L;)V

    goto :goto_1

    .line 2434947
    :pswitch_2
    new-instance v5, LX/HAA;

    invoke-direct {v5, p0}, LX/HAA;-><init>(LX/H8L;)V

    goto :goto_1

    .line 2434948
    :pswitch_3
    new-instance v5, LX/HAB;

    invoke-direct {v5, p0}, LX/HAB;-><init>(LX/H8L;)V

    goto :goto_1

    .line 2434949
    :pswitch_4
    new-instance v5, LX/HAD;

    invoke-direct {v5, p0}, LX/HAD;-><init>(LX/H8L;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(LX/0QB;)LX/HAF;
    .locals 4

    .prologue
    .line 2434950
    sget-object v0, LX/HAF;->b:LX/HAF;

    if-nez v0, :cond_1

    .line 2434951
    const-class v1, LX/HAF;

    monitor-enter v1

    .line 2434952
    :try_start_0
    sget-object v0, LX/HAF;->b:LX/HAF;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2434953
    if-eqz v2, :cond_0

    .line 2434954
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2434955
    new-instance p0, LX/HAF;

    invoke-static {v0}, LX/HDT;->a(LX/0QB;)LX/HDT;

    move-result-object v3

    check-cast v3, LX/HDT;

    invoke-direct {p0, v3}, LX/HAF;-><init>(LX/HDT;)V

    .line 2434956
    move-object v0, p0

    .line 2434957
    sput-object v0, LX/HAF;->b:LX/HAF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2434958
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2434959
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2434960
    :cond_1
    sget-object v0, LX/HAF;->b:LX/HAF;

    return-object v0

    .line 2434961
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2434962
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/H8Y;)V
    .locals 5

    .prologue
    .line 2434963
    invoke-interface {p1}, LX/H8Y;->d()LX/0Px;

    move-result-object v2

    .line 2434964
    if-nez v2, :cond_1

    .line 2434965
    :cond_0
    return-void

    .line 2434966
    :cond_1
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8E;

    .line 2434967
    iget-object v4, p0, LX/HAF;->a:LX/HDT;

    invoke-virtual {v4, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 2434968
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final b(LX/H8Y;)V
    .locals 5

    .prologue
    .line 2434969
    invoke-interface {p1}, LX/H8Y;->d()LX/0Px;

    move-result-object v2

    .line 2434970
    if-nez v2, :cond_1

    .line 2434971
    :cond_0
    return-void

    .line 2434972
    :cond_1
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8E;

    .line 2434973
    iget-object v4, p0, LX/HAF;->a:LX/HDT;

    invoke-virtual {v4, v0}, LX/0b4;->b(LX/0b2;)Z

    .line 2434974
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
