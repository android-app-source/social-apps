.class public LX/Fc6;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/7Hc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/results/protocol/filters/FilterValue;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/Fcp;

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/Fcr",
            "<",
            "Lcom/facebook/search/results/protocol/filters/FilterValue;",
            ">;>;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/Fc4;

.field public final g:LX/7Hj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7Hj",
            "<",
            "Lcom/facebook/search/results/protocol/filters/FilterValue;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/CwL;

.field public i:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2262046
    new-instance v0, LX/7Hc;

    .line 2262047
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2262048
    invoke-direct {v0, v1}, LX/7Hc;-><init>(LX/0Px;)V

    sput-object v0, LX/Fc6;->a:LX/7Hc;

    return-void
.end method

.method public constructor <init>(LX/Fc4;LX/Fcp;LX/Fc8;LX/7Hk;)V
    .locals 1
    .param p1    # LX/Fc4;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2262035
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2262036
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Fc6;->c:Ljava/util/Map;

    .line 2262037
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Fc6;->d:Ljava/util/Map;

    .line 2262038
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Fc6;->e:Ljava/util/Set;

    .line 2262039
    const-string v0, ""

    iput-object v0, p0, LX/Fc6;->i:Ljava/lang/String;

    .line 2262040
    iput-object p1, p0, LX/Fc6;->f:LX/Fc4;

    .line 2262041
    iput-object p2, p0, LX/Fc6;->b:LX/Fcp;

    .line 2262042
    invoke-static {p3}, LX/7Hk;->a(LX/7HW;)LX/7Hj;

    move-result-object v0

    iput-object v0, p0, LX/Fc6;->g:LX/7Hj;

    .line 2262043
    iget-object v0, p0, LX/Fc6;->b:LX/Fcp;

    new-instance p1, LX/Fc5;

    invoke-direct {p1, p0}, LX/Fc5;-><init>(LX/Fc6;)V

    .line 2262044
    iput-object p1, v0, LX/Fcp;->f:LX/Fc4;

    .line 2262045
    return-void
.end method

.method public static a$redex0(LX/Fc6;LX/7Hc;)V
    .locals 4

    .prologue
    .line 2261974
    iget-object v0, p0, LX/Fc6;->f:LX/Fc4;

    iget-object v1, p0, LX/Fc6;->h:LX/CwL;

    .line 2261975
    iget-object v2, v1, LX/CwL;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2261976
    iget-object v2, p0, LX/Fc6;->i:Ljava/lang/String;

    iget-object v3, p0, LX/Fc6;->h:LX/CwL;

    .line 2261977
    iget-object p0, v3, LX/CwL;->b:Ljava/lang/String;

    move-object v3, p0

    .line 2261978
    invoke-interface {v0, v1, v2, v3, p1}, LX/Fc4;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/7Hc;)V

    .line 2261979
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/util/List;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/results/protocol/filters/FilterValue;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/results/protocol/filters/FilterValue;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262027
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2262028
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/FilterValue;

    .line 2262029
    iget-boolean v3, v0, Lcom/facebook/search/results/protocol/filters/FilterValue;->d:Z

    move v3, v3

    .line 2262030
    if-nez v3, :cond_0

    .line 2262031
    iget-object v3, v0, Lcom/facebook/search/results/protocol/filters/FilterValue;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2262032
    invoke-static {v3}, LX/Fc6;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2262033
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2262034
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;LX/7Hi;)LX/7Hc;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/results/protocol/filters/FilterValue;",
            ">;)",
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/results/protocol/filters/FilterValue;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262015
    iget-object v0, p1, LX/7Hi;->a:LX/7B6;

    move-object v0, v0

    .line 2262016
    iget-object v0, v0, LX/7B6;->b:Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2262017
    iget-object v0, p1, LX/7Hi;->b:LX/7Hc;

    move-object v0, v0

    .line 2262018
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/7Hc;

    .line 2262019
    iget-object v1, p1, LX/7Hi;->b:LX/7Hc;

    move-object v1, v1

    .line 2262020
    iget-object v2, v1, LX/7Hc;->b:LX/0Px;

    move-object v1, v2

    .line 2262021
    invoke-static {p0, v1}, LX/Fc6;->b(Ljava/lang/String;Ljava/util/List;)LX/0Px;

    move-result-object v1

    .line 2262022
    iget-object v2, p1, LX/7Hi;->b:LX/7Hc;

    move-object v2, v2

    .line 2262023
    iget v3, v2, LX/7Hc;->c:I

    move v2, v3

    .line 2262024
    iget-object v3, p1, LX/7Hi;->b:LX/7Hc;

    move-object v3, v3

    .line 2262025
    iget p0, v3, LX/7Hc;->d:I

    move v3, p0

    .line 2262026
    invoke-direct {v0, v1, v2, v3}, LX/7Hc;-><init>(LX/0Px;II)V

    goto :goto_0
.end method

.method private static e(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2262014
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 2261980
    invoke-static {p1}, LX/Fc6;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2261981
    iput-object v0, p0, LX/Fc6;->i:Ljava/lang/String;

    .line 2261982
    iget-object v0, p0, LX/Fc6;->h:LX/CwL;

    .line 2261983
    iget-object v1, v0, LX/CwL;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2261984
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2261985
    iget-object v0, p0, LX/Fc6;->i:Ljava/lang/String;

    .line 2261986
    const/4 v1, 0x0

    .line 2261987
    iget-object v2, p0, LX/Fc6;->c:Ljava/util/Map;

    iget-object v3, p0, LX/Fc6;->h:LX/CwL;

    .line 2261988
    iget-object v4, v3, LX/CwL;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2261989
    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2261990
    iget-object v1, p0, LX/Fc6;->c:Ljava/util/Map;

    iget-object v2, p0, LX/Fc6;->h:LX/CwL;

    .line 2261991
    iget-object v3, v2, LX/CwL;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2261992
    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Fcr;

    invoke-virtual {v1, v0}, LX/Fcr;->b(Ljava/lang/String;)LX/7Hi;

    move-result-object v1

    .line 2261993
    :cond_0
    if-nez v1, :cond_5

    .line 2261994
    sget-object v2, LX/Fc6;->a:LX/7Hc;

    invoke-static {p0, v2}, LX/Fc6;->a$redex0(LX/Fc6;LX/7Hc;)V

    .line 2261995
    :goto_1
    if-eqz v1, :cond_1

    .line 2261996
    iget-object v2, v1, LX/7Hi;->a:LX/7B6;

    move-object v1, v2

    .line 2261997
    iget-object v1, v1, LX/7B6;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    :cond_1
    const/4 v1, 0x1

    :goto_2
    move v0, v1

    .line 2261998
    iget-object v1, p0, LX/Fc6;->e:Ljava/util/Set;

    iget-object v2, p0, LX/Fc6;->i:Ljava/lang/String;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    if-nez v0, :cond_4

    .line 2261999
    :cond_2
    :goto_3
    return-void

    .line 2262000
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 2262001
    :cond_4
    iget-object v0, p0, LX/Fc6;->e:Ljava/util/Set;

    iget-object v1, p0, LX/Fc6;->i:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2262002
    iget-object v0, p0, LX/Fc6;->b:LX/Fcp;

    iget-object v1, p0, LX/Fc6;->h:LX/CwL;

    .line 2262003
    iget-object v2, v1, LX/CwL;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2262004
    iget-object v2, p0, LX/Fc6;->i:Ljava/lang/String;

    iget-object v3, p0, LX/Fc6;->h:LX/CwL;

    .line 2262005
    iget-object v4, v3, LX/CwL;->b:Ljava/lang/String;

    move-object v3, v4

    .line 2262006
    const/16 v4, 0xc

    .line 2262007
    iget-object v5, v0, LX/Fcp;->f:LX/Fc4;

    invoke-static {v5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2262008
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_7

    const/4 v5, 0x1

    :goto_4
    invoke-static {v5}, LX/0PB;->checkState(Z)V

    .line 2262009
    iget-object v5, v0, LX/Fcp;->d:LX/1Ck;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string p1, "simple_search_loader_filter_value"

    invoke-direct {v6, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v1, v2, v4}, LX/Fcp;->a(LX/Fcp;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p1

    new-instance p0, LX/Fco;

    invoke-direct {p0, v0, v2, v1, v3}, LX/Fco;-><init>(LX/Fcp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6, p1, p0}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2262010
    goto :goto_3

    .line 2262011
    :cond_5
    invoke-static {v0, v1}, LX/Fc6;->b(Ljava/lang/String;LX/7Hi;)LX/7Hc;

    move-result-object v2

    invoke-static {p0, v2}, LX/Fc6;->a$redex0(LX/Fc6;LX/7Hc;)V

    goto :goto_1

    .line 2262012
    :cond_6
    const/4 v1, 0x0

    goto :goto_2

    .line 2262013
    :cond_7
    const/4 v5, 0x0

    goto :goto_4
.end method
