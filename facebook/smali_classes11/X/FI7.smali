.class public final enum LX/FI7;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FI7;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FI7;

.field public static final enum Audio:LX/FI7;

.field public static final enum UnKnown:LX/FI7;

.field public static final enum Video:LX/FI7;


# instance fields
.field private value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2221469
    new-instance v0, LX/FI7;

    const-string v1, "Audio"

    invoke-direct {v0, v1, v4, v2}, LX/FI7;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/FI7;->Audio:LX/FI7;

    .line 2221470
    new-instance v0, LX/FI7;

    const-string v1, "Video"

    invoke-direct {v0, v1, v2, v3}, LX/FI7;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/FI7;->Video:LX/FI7;

    .line 2221471
    new-instance v0, LX/FI7;

    const-string v1, "UnKnown"

    invoke-direct {v0, v1, v3, v5}, LX/FI7;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/FI7;->UnKnown:LX/FI7;

    .line 2221472
    new-array v0, v5, [LX/FI7;

    sget-object v1, LX/FI7;->Audio:LX/FI7;

    aput-object v1, v0, v4

    sget-object v1, LX/FI7;->Video:LX/FI7;

    aput-object v1, v0, v2

    sget-object v1, LX/FI7;->UnKnown:LX/FI7;

    aput-object v1, v0, v3

    sput-object v0, LX/FI7;->$VALUES:[LX/FI7;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2221466
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2221467
    iput p3, p0, LX/FI7;->value:I

    .line 2221468
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FI7;
    .locals 1

    .prologue
    .line 2221474
    const-class v0, LX/FI7;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FI7;

    return-object v0
.end method

.method public static values()[LX/FI7;
    .locals 1

    .prologue
    .line 2221475
    sget-object v0, LX/FI7;->$VALUES:[LX/FI7;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FI7;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 1

    .prologue
    .line 2221473
    iget v0, p0, LX/FI7;->value:I

    return v0
.end method
