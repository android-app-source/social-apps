.class public final LX/Ff3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;)V
    .locals 0

    .prologue
    .line 2267068
    iput-object p1, p0, LX/Ff3;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2267108
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 11
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2267069
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2267070
    if-eqz p1, :cond_0

    .line 2267071
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2267072
    if-eqz v0, :cond_0

    .line 2267073
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2267074
    check-cast v0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;->a()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2267075
    :cond_0
    :goto_0
    return-void

    .line 2267076
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2267077
    check-cast v0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;->a()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;->k()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2267078
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2267079
    check-cast v0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;->a()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;->k()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    move v1, v4

    :goto_1
    if-ge v1, v3, :cond_3

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel;

    .line 2267080
    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel;->j()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel$NodeModel;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel;->j()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel$NodeModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 2267081
    iget-object v6, p0, LX/Ff3;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    iget-object v6, v6, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->D:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel;->j()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel$NodeModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267082
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2267083
    :cond_3
    iget-object v0, p0, LX/Ff3;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    .line 2267084
    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2267085
    move-object v1, v1

    .line 2267086
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2267087
    check-cast v0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;->a()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->d(Ljava/lang/String;)V

    .line 2267088
    iget-object v0, p0, LX/Ff3;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    .line 2267089
    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2267090
    move-object v1, v1

    .line 2267091
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2267092
    check-cast v0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;->a()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e(Ljava/lang/String;)V

    .line 2267093
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2267094
    check-cast v0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;->a()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;->k()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2267095
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2267096
    check-cast v0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;->a()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;->k()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2267097
    iget-object v0, p0, LX/Ff3;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    iget-object v8, v0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->i:LX/CvY;

    iget-object v0, p0, LX/Ff3;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    .line 2267098
    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2267099
    move-object v9, v1

    .line 2267100
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2267101
    check-cast v0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;->j()Ljava/lang/String;

    move-result-object v10

    iget-object v0, p0, LX/Ff3;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->i:LX/CvY;

    iget-object v1, p0, LX/Ff3;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    .line 2267102
    iget-object v2, v1, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v2, v2

    .line 2267103
    move-object v1, v2

    .line 2267104
    iget-object v2, p0, LX/Ff3;->a:Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;

    .line 2267105
    iget v3, v2, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->B:I

    add-int/lit8 v6, v3, 0x1

    iput v6, v2, Lcom/facebook/search/results/fragment/photos/SearchResultsPandoraPhotoFragment;->B:I

    move v2, v3

    .line 2267106
    iget-object v3, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v3, v3

    .line 2267107
    check-cast v3, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;

    invoke-virtual {v3}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;->a()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;->k()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;IIILX/0Px;LX/8cg;LX/8cf;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-virtual {v8, v9, v10, v4, v0}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;ILcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto/16 :goto_0
.end method
