.class public final LX/FGM;
.super LX/1ci;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;)V
    .locals 0

    .prologue
    .line 2218824
    iput-object p1, p0, LX/FGM;->a:Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 2

    .prologue
    .line 2218825
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2218826
    iget-object v0, p0, LX/FGM;->a:Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;

    iget-object v0, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;->b:Lcom/facebook/messaging/media/retry/MediaRetryQueue;

    iget-object v0, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->l:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v1, p0, LX/FGM;->a:Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;

    iget-object v1, v1, Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2218827
    :cond_0
    return-void
.end method

.method public final f(LX/1ca;)V
    .locals 2

    .prologue
    .line 2218828
    iget-object v0, p0, LX/FGM;->a:Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;

    iget-object v0, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;->b:Lcom/facebook/messaging/media/retry/MediaRetryQueue;

    iget-object v0, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->l:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v1, p0, LX/FGM;->a:Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;

    iget-object v1, v1, Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FGK;

    .line 2218829
    if-eqz v0, :cond_0

    .line 2218830
    invoke-virtual {v0}, LX/FGK;->a()I

    .line 2218831
    :cond_0
    invoke-static {v0}, LX/FGK;->d(LX/FGK;)I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {v0}, LX/FGK;->b()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2218832
    if-nez v0, :cond_1

    .line 2218833
    iget-object v0, p0, LX/FGM;->a:Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;

    iget-object v0, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;->b:Lcom/facebook/messaging/media/retry/MediaRetryQueue;

    iget-object v0, v0, Lcom/facebook/messaging/media/retry/MediaRetryQueue;->l:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v1, p0, LX/FGM;->a:Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;

    iget-object v1, v1, Lcom/facebook/messaging/media/retry/MediaRetryQueue$1;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2218834
    :cond_1
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method
