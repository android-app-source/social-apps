.class public final LX/Ftm;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<",
        "Lcom/facebook/timeline/favmediapicker/rows/environments/FavoriteMediaPickerEnvironment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;LX/0Px;)V
    .locals 0

    .prologue
    .line 2299108
    iput-object p1, p0, LX/Ftm;->b:Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;

    iput-object p2, p0, LX/Ftm;->a:LX/0Px;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<",
            "Lcom/facebook/timeline/favmediapicker/rows/environments/FavoriteMediaPickerEnvironment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2299109
    iget-object v0, p0, LX/Ftm;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, LX/Ftm;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediaModel;

    .line 2299110
    iget-object v3, p0, LX/Ftm;->b:Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;

    .line 2299111
    invoke-virtual {v0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediaModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediaModel;->k()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v4

    .line 2299112
    :goto_1
    new-instance v5, LX/2f8;

    invoke-direct {v5}, LX/2f8;-><init>()V

    sget-object v6, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 2299113
    iput-object v6, v5, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 2299114
    move-object v5, v5

    .line 2299115
    invoke-virtual {v5, v4}, LX/2f8;->a(Ljava/lang/String;)LX/2f8;

    move-result-object v5

    invoke-virtual {v5}, LX/2f8;->a()LX/2f9;

    move-result-object v5

    .line 2299116
    new-instance v6, LX/Fte;

    invoke-virtual {v0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediaModel;->j()Ljava/lang/String;

    move-result-object v7

    .line 2299117
    new-instance v0, LX/Ftn;

    invoke-direct {v0, v3, v7, v4}, LX/Ftn;-><init>(Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v0

    .line 2299118
    invoke-direct {v6, v5, v4}, LX/Fte;-><init>(LX/2f9;Landroid/view/View$OnClickListener;)V

    move-object v0, v6

    .line 2299119
    iget-object v3, p0, LX/Ftm;->b:Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;

    iget-object v3, v3, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetRollPartDefinition;->c:Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;

    invoke-virtual {p1, v3, v0}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2299120
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2299121
    :cond_0
    return-void

    .line 2299122
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public final c(I)V
    .locals 0

    .prologue
    .line 2299123
    return-void
.end method
