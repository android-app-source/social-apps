.class public LX/F7o;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public a:LX/0gc;

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/F7v;",
            ">;"
        }
    .end annotation
.end field

.field public c:I


# direct methods
.method public constructor <init>(LX/F7w;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2202607
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2202608
    const/4 v0, 0x0

    iput v0, p0, LX/F7o;->c:I

    .line 2202609
    iget-object v0, p1, LX/F7w;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2202610
    iget-object v0, p1, LX/F7w;->a:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-virtual {v0}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2202611
    iget-object v0, p1, LX/F7w;->c:Ljava/util/List;

    sget-object v1, LX/F7v;->LEGAL_SCREEN:LX/F7v;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2202612
    :cond_0
    iget-object v0, p1, LX/F7w;->c:Ljava/util/List;

    sget-object v1, LX/F7v;->FRIENDABLE_CONTACTS:LX/F7v;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2202613
    iget-object v0, p1, LX/F7w;->b:LX/0Uh;

    const/16 v1, 0x32

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2202614
    iget-object v0, p1, LX/F7w;->c:Ljava/util/List;

    sget-object v1, LX/F7v;->INVITABLE_CONTACTS:LX/F7v;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2202615
    :cond_1
    iget-object v0, p1, LX/F7w;->c:Ljava/util/List;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v0, v0

    .line 2202616
    iput-object v0, p0, LX/F7o;->b:LX/0Px;

    .line 2202617
    return-void
.end method

.method public static a(LX/0QB;)LX/F7o;
    .locals 6

    .prologue
    .line 2202618
    const-class v1, LX/F7o;

    monitor-enter v1

    .line 2202619
    :try_start_0
    sget-object v0, LX/F7o;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2202620
    sput-object v2, LX/F7o;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2202621
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2202622
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2202623
    new-instance v4, LX/F7o;

    .line 2202624
    new-instance p0, LX/F7w;

    invoke-static {v0}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->b(LX/0QB;)Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    move-result-object v3

    check-cast v3, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-direct {p0, v3, v5}, LX/F7w;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;LX/0Uh;)V

    .line 2202625
    move-object v3, p0

    .line 2202626
    check-cast v3, LX/F7w;

    invoke-direct {v4, v3}, LX/F7o;-><init>(LX/F7w;)V

    .line 2202627
    move-object v0, v4

    .line 2202628
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2202629
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/F7o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2202630
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2202631
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 2202606
    iget v0, p0, LX/F7o;->c:I

    iget-object v1, p0, LX/F7o;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()LX/F7v;
    .locals 2

    .prologue
    .line 2202605
    iget-object v0, p0, LX/F7o;->b:LX/0Px;

    iget v1, p0, LX/F7o;->c:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F7v;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2202604
    iget-object v0, p0, LX/F7o;->b:LX/0Px;

    iget v1, p0, LX/F7o;->c:I

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F7v;

    invoke-virtual {v0}, LX/F7v;->name()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
