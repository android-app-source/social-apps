.class public final LX/GIF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GFR;


# instance fields
.field public final synthetic a:LX/GIG;


# direct methods
.method public constructor <init>(LX/GIG;)V
    .locals 0

    .prologue
    .line 2336076
    iput-object p1, p0, LX/GIF;->a:LX/GIG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2336077
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 2336078
    const-string v0, "CREATIVE_EDIT_DATA"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336079
    iget-object v1, p0, LX/GIF;->a:LX/GIG;

    iget-object v1, v1, LX/GIG;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336080
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v2, v2

    .line 2336081
    iput-object v2, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2336082
    iget-object v1, p0, LX/GIF;->a:LX/GIG;

    iget-object v1, v1, LX/GIG;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->Q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->h(Ljava/lang/String;)V

    .line 2336083
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->B()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2336084
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->B()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v1, v2, :cond_2

    .line 2336085
    iget-object v1, p0, LX/GIF;->a:LX/GIG;

    iget-object v1, v1, LX/GIG;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;

    iget-object v2, p0, LX/GIF;->a:LX/GIG;

    iget-object v2, v2, LX/GIG;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080a73

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;->setSelectedValues(Ljava/lang/CharSequence;)V

    .line 2336086
    :cond_0
    :goto_0
    iget-object v1, p0, LX/GIF;->a:LX/GIG;

    iget-object v1, v1, LX/GIG;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->B()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->B()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    if-eq v1, v2, :cond_1

    .line 2336087
    iget-object v1, p0, LX/GIF;->a:LX/GIG;

    iget-object v1, v1, LX/GIG;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->B()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    .line 2336088
    iput-object v0, v1, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->s:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2336089
    iget-object v0, p0, LX/GIF;->a:LX/GIG;

    .line 2336090
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2336091
    new-instance v1, LX/GFG;

    invoke-direct {v1}, LX/GFG;-><init>()V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2336092
    :cond_1
    return-void

    .line 2336093
    :cond_2
    iget-object v1, p0, LX/GIF;->a:LX/GIG;

    iget-object v1, v1, LX/GIG;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->B()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v2

    invoke-static {v2}, LX/GCi;->fromGraphQLTypeCallToAction(Lcom/facebook/graphql/enums/GraphQLCallToActionType;)LX/GCi;

    move-result-object v2

    iget-object v3, p0, LX/GIF;->a:LX/GIG;

    iget-object v3, v3, LX/GIG;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/GCi;->getText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesActionButtonView;->setSelectedValues(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
