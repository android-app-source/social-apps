.class public final LX/Fuh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Fuj;


# direct methods
.method public constructor <init>(LX/Fuj;)V
    .locals 0

    .prologue
    .line 2300319
    iput-object p1, p0, LX/Fuh;->a:LX/Fuj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x3930db0a

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2300320
    iget-object v1, p0, LX/Fuh;->a:LX/Fuj;

    .line 2300321
    const/4 v4, 0x0

    .line 2300322
    iget-object v3, v1, LX/Fuj;->d:LX/BQ1;

    invoke-virtual {v3}, LX/BQ1;->aa()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2300323
    iget-object v3, v1, LX/Fuj;->d:LX/BQ1;

    .line 2300324
    iget-object v4, v3, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v3, v4

    .line 2300325
    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->c()LX/174;

    move-result-object v3

    invoke-interface {v3}, LX/174;->a()Ljava/lang/String;

    move-result-object v4

    .line 2300326
    iget-object v3, v1, LX/Fuj;->g:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/BQ9;

    iget-object v5, v1, LX/Fuj;->c:LX/BP0;

    .line 2300327
    iget-wide v7, v5, LX/5SB;->b:J

    move-wide v5, v7

    .line 2300328
    const/4 v11, 0x0

    sget-object v12, LX/9lQ;->SELF:LX/9lQ;

    const-string v13, "bio_edit_click"

    move-object v8, v3

    move-wide v9, v5

    invoke-static/range {v8 .. v13}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    .line 2300329
    if-eqz v7, :cond_0

    .line 2300330
    iget-object v8, v3, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v8, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2300331
    :cond_0
    move-object v3, v4

    .line 2300332
    :goto_0
    const/4 v4, 0x0

    invoke-static {v1, v3, v4}, LX/Fuj;->a(LX/Fuj;Ljava/lang/String;Z)V

    .line 2300333
    const v1, 0x26415caf

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2300334
    :cond_1
    iget-object v3, v1, LX/Fuj;->g:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/BQ9;

    iget-object v5, v1, LX/Fuj;->c:LX/BP0;

    .line 2300335
    iget-wide v7, v5, LX/5SB;->b:J

    move-wide v5, v7

    .line 2300336
    const/4 v11, 0x0

    sget-object v12, LX/9lQ;->SELF:LX/9lQ;

    const-string v13, "bio_add_click"

    move-object v8, v3

    move-wide v9, v5

    invoke-static/range {v8 .. v13}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    .line 2300337
    if-eqz v7, :cond_2

    .line 2300338
    iget-object v8, v3, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v8, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2300339
    :cond_2
    move-object v3, v4

    goto :goto_0
.end method
