.class public LX/GYN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2365127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2365128
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 8

    .prologue
    .line 2365129
    sget-object v0, LX/GXu;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2365130
    sget-object v0, LX/GXu;->a:Ljava/lang/String;

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2365131
    const-string v2, "extra_finish_on_launch_view_shop"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 2365132
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 2365133
    new-instance v4, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    invoke-direct {v4}, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;-><init>()V

    .line 2365134
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2365135
    sget-object v6, LX/GXu;->a:Ljava/lang/String;

    invoke-virtual {v5, v6, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2365136
    const-string v6, "extra_finish_on_launch_view_shop"

    invoke-virtual {v5, v6, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2365137
    invoke-virtual {v4, v5}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2365138
    move-object v0, v4

    .line 2365139
    return-object v0

    .line 2365140
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method
