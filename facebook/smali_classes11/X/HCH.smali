.class public LX/HCH;
.super LX/3xj;
.source ""


# instance fields
.field private final a:LX/HCO;


# direct methods
.method public constructor <init>(LX/HCO;)V
    .locals 0

    .prologue
    .line 2438517
    invoke-direct {p0}, LX/3xj;-><init>()V

    .line 2438518
    iput-object p1, p0, LX/HCH;->a:LX/HCO;

    .line 2438519
    return-void
.end method


# virtual methods
.method public final a(LX/1a1;)I
    .locals 2

    .prologue
    .line 2438520
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/3xj;->b(II)I

    move-result v0

    return v0
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2438521
    if-eqz p2, :cond_0

    .line 2438522
    instance-of v0, p1, LX/HCL;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 2438523
    check-cast v0, LX/HCL;

    .line 2438524
    const/4 v2, 0x0

    .line 2438525
    iget-object v1, v0, LX/HCL;->m:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2438526
    iget-object v1, v0, LX/HCL;->o:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2438527
    iget-object v1, v0, LX/HCL;->p:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2438528
    :cond_0
    invoke-super {p0, p1, p2}, LX/3xj;->a(LX/1a1;I)V

    .line 2438529
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2438516
    const/4 v0, 0x1

    return v0
.end method

.method public final a(LX/1a1;LX/1a1;)Z
    .locals 3

    .prologue
    .line 2438475
    iget-object v0, p0, LX/HCH;->a:LX/HCO;

    invoke-virtual {p1}, LX/1a1;->e()I

    move-result v1

    invoke-virtual {p2}, LX/1a1;->e()I

    move-result v2

    const/4 p0, 0x0

    const/4 p1, 0x1

    .line 2438476
    iget-boolean p2, v0, LX/HCO;->l:Z

    if-eqz p2, :cond_1

    .line 2438477
    :cond_0
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2438478
    :cond_1
    if-nez v2, :cond_2

    iget-object p2, v0, LX/HCO;->a:Ljava/util/List;

    invoke-interface {p2, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object p0

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_HOME:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne p0, p2, :cond_2

    move v2, p1

    .line 2438479
    :cond_2
    iget-object p0, v0, LX/HCO;->a:Ljava/util/List;

    invoke-static {p0, v1, v2}, Ljava/util/Collections;->swap(Ljava/util/List;II)V

    .line 2438480
    invoke-virtual {v0, v1, v2}, LX/1OM;->b(II)V

    .line 2438481
    if-eq v1, v2, :cond_0

    .line 2438482
    iput-boolean p1, v0, LX/HCO;->m:Z

    goto :goto_0
.end method

.method public final b(Landroid/support/v7/widget/RecyclerView;LX/1a1;)V
    .locals 8

    .prologue
    .line 2438485
    invoke-super {p0, p1, p2}, LX/3xj;->b(Landroid/support/v7/widget/RecyclerView;LX/1a1;)V

    .line 2438486
    instance-of v0, p2, LX/HCL;

    if-eqz v0, :cond_0

    .line 2438487
    check-cast p2, LX/HCL;

    .line 2438488
    const/16 v2, 0x8

    .line 2438489
    iget-object v1, p2, LX/HCL;->m:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2438490
    iget-object v1, p2, LX/HCL;->o:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2438491
    iget-object v1, p2, LX/HCL;->p:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2438492
    iget-object v1, p2, LX/HCL;->n:LX/HCO;

    .line 2438493
    iget-boolean v3, v1, LX/HCO;->m:Z

    if-nez v3, :cond_1

    .line 2438494
    :cond_0
    :goto_0
    return-void

    .line 2438495
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2438496
    iget-object v3, v1, LX/HCO;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    .line 2438497
    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v3

    .line 2438498
    new-instance v6, LX/4Hc;

    invoke-direct {v6}, LX/4Hc;-><init>()V

    .line 2438499
    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLPageActionType;->name()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/4Hc;->a(Ljava/lang/String;)LX/4Hc;

    .line 2438500
    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->l()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 2438501
    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6, v3}, LX/4Hc;->b(Ljava/lang/String;)LX/4Hc;

    .line 2438502
    :cond_2
    invoke-virtual {v4, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2438503
    :cond_3
    new-instance v3, LX/4Ha;

    invoke-direct {v3}, LX/4Ha;-><init>()V

    const-string v5, "PROFILE_TAB_NAVIGATION"

    .line 2438504
    const-string v6, "channel_type"

    invoke-virtual {v3, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2438505
    move-object v3, v3

    .line 2438506
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 2438507
    const-string v5, "reordered_actions"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2438508
    move-object v3, v3

    .line 2438509
    iget-wide v5, v1, LX/HCO;->i:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    .line 2438510
    const-string v5, "page_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2438511
    move-object v3, v3

    .line 2438512
    new-instance v4, LX/9Xq;

    invoke-direct {v4}, LX/9Xq;-><init>()V

    move-object v4, v4

    .line 2438513
    const-string v5, "input"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/9Xq;

    .line 2438514
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    .line 2438515
    iget-object v3, v1, LX/HCO;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Ck;

    const-string v6, "update_tab_order"

    iget-object v4, v1, LX/HCO;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-virtual {v4, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance v5, LX/HCK;

    invoke-direct {v5, v1}, LX/HCK;-><init>(LX/HCO;)V

    invoke-virtual {v3, v6, v4, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto/16 :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2438484
    const/4 v0, 0x0

    return v0
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 2438483
    return-void
.end method
