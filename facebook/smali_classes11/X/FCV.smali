.class public final LX/FCV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation build Lcom/google/common/annotations/VisibleForTesting;
.end annotation


# instance fields
.field public graphAttempts:I

.field public final messageType:Ljava/lang/String;

.field public mqttAttempts:I

.field public outcome:LX/FCU;

.field public final sendAttemptTimestamp:J

.field public timeSinceFirstSendAttempt:J


# direct methods
.method public constructor <init>(JLjava/lang/String;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2210367
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2210368
    iput v0, p0, LX/FCV;->mqttAttempts:I

    .line 2210369
    iput v0, p0, LX/FCV;->graphAttempts:I

    .line 2210370
    const-wide/16 v0, -0x1

    iput-wide v0, p0, LX/FCV;->timeSinceFirstSendAttempt:J

    .line 2210371
    sget-object v0, LX/FCU;->UNKNOWN:LX/FCU;

    iput-object v0, p0, LX/FCV;->outcome:LX/FCU;

    .line 2210372
    iput-wide p1, p0, LX/FCV;->sendAttemptTimestamp:J

    .line 2210373
    iput-object p3, p0, LX/FCV;->messageType:Ljava/lang/String;

    .line 2210374
    return-void
.end method
