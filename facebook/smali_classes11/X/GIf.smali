.class public final LX/GIf;
.super LX/GFb;
.source ""


# instance fields
.field public final synthetic a:LX/GIr;


# direct methods
.method public constructor <init>(LX/GIr;)V
    .locals 0

    .prologue
    .line 2336930
    iput-object p1, p0, LX/GIf;->a:LX/GIr;

    invoke-direct {p0}, LX/GFb;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 2336931
    check-cast p1, LX/GFa;

    .line 2336932
    iget-object v0, p1, LX/GFa;->a:Landroid/location/Location;

    move-object v0, v0

    .line 2336933
    new-instance v1, LX/A9E;

    invoke-direct {v1}, LX/A9E;-><init>()V

    iget-object v2, p0, LX/GIf;->a:LX/GIr;

    iget-object v2, v2, LX/GIr;->k:LX/0W9;

    invoke-static {v2}, LX/GNL;->a(LX/0W9;)Ljava/lang/String;

    move-result-object v2

    .line 2336934
    iput-object v2, v1, LX/A9E;->d:Ljava/lang/String;

    .line 2336935
    move-object v1, v1

    .line 2336936
    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    .line 2336937
    iput-wide v2, v1, LX/A9E;->f:D

    .line 2336938
    move-object v1, v1

    .line 2336939
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->CUSTOM_LOCATION:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    .line 2336940
    iput-object v2, v1, LX/A9E;->g:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    .line 2336941
    move-object v1, v1

    .line 2336942
    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    .line 2336943
    iput-wide v2, v1, LX/A9E;->h:D

    .line 2336944
    move-object v0, v1

    .line 2336945
    iget-wide v4, p1, LX/GFa;->b:D

    move-wide v2, v4

    .line 2336946
    iput-wide v2, v0, LX/A9E;->j:D

    .line 2336947
    move-object v0, v0

    .line 2336948
    invoke-virtual {v0}, LX/A9E;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v0

    .line 2336949
    iget-object v1, p0, LX/GIf;->a:LX/GIr;

    iget-object v1, v1, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    .line 2336950
    iput-object v0, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2336951
    iget-object v0, p0, LX/GIf;->a:LX/GIr;

    invoke-virtual {v0}, LX/GIr;->b()V

    .line 2336952
    iget-object v0, p0, LX/GIf;->a:LX/GIr;

    iget-object v0, v0, LX/GIr;->i:LX/GL2;

    invoke-virtual {v0}, LX/GL2;->b()V

    .line 2336953
    return-void
.end method
