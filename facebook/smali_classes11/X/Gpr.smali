.class public LX/Gpr;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/instantshopping/view/block/ToggleButtonBlockView;",
        "Lcom/facebook/instantshopping/model/data/ToggleButtonBlockData;",
        ">;"
    }
.end annotation


# instance fields
.field public d:Z

.field private e:LX/8Z4;

.field private f:LX/8Z4;


# direct methods
.method public constructor <init>(LX/Gqp;)V
    .locals 0

    .prologue
    .line 2395960
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2395961
    return-void
.end method

.method private a(LX/8Z4;)LX/Clf;
    .locals 2

    .prologue
    .line 2395962
    new-instance v0, LX/Cle;

    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Cle;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, LX/Cle;->a(LX/8Z4;)LX/Cle;

    move-result-object v0

    const v1, 0x7f0e0a15

    invoke-virtual {v0, v1}, LX/Cle;->a(I)LX/Cle;

    move-result-object v0

    invoke-virtual {v0}, LX/Cle;->a()LX/Clf;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 6

    .prologue
    .line 2395963
    check-cast p1, LX/GpP;

    .line 2395964
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395965
    check-cast v0, LX/Gqp;

    .line 2395966
    iget-object v1, v0, LX/Gqp;->d:LX/CIb;

    .line 2395967
    iget-object v2, v1, LX/CIb;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2395968
    iget-object v2, v0, LX/Gqp;->c:LX/CIe;

    invoke-virtual {v2, v1}, LX/CIe;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2395969
    iget-object v2, v0, LX/Gqp;->c:LX/CIe;

    invoke-virtual {v2, v1}, LX/CIe;->a(Ljava/lang/String;)Z

    move-result v1

    .line 2395970
    :goto_0
    move v0, v1

    .line 2395971
    iput-boolean v0, p0, LX/Gpr;->d:Z

    .line 2395972
    iget-boolean v0, p0, LX/Gpr;->d:Z

    invoke-virtual {p0, v0}, LX/Gpr;->a(Z)V

    .line 2395973
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395974
    check-cast v0, LX/Gqp;

    .line 2395975
    iget-object v1, p1, LX/GpP;->a:LX/CHm;

    invoke-interface {v1}, LX/CHm;->A()LX/CHX;

    move-result-object v1

    move-object v1, v1

    .line 2395976
    iget-object v2, p1, LX/GpP;->a:LX/CHm;

    invoke-interface {v2}, LX/CHm;->B()LX/CHX;

    move-result-object v2

    move-object v2, v2

    .line 2395977
    invoke-interface {p1}, LX/GoY;->D()LX/GoE;

    move-result-object v3

    .line 2395978
    new-instance v4, LX/Gqo;

    invoke-direct {v4, v0, v2, v1, v3}, LX/Gqo;-><init>(LX/Gqp;LX/CHX;LX/CHX;LX/GoE;)V

    .line 2395979
    iget-object v5, v0, LX/Gqp;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v5, v4}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2395980
    iget-object v5, v0, LX/Gqp;->i:LX/CtG;

    invoke-virtual {v5, v4}, LX/CtG;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2395981
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395982
    check-cast v0, LX/Gqp;

    .line 2395983
    iget-object v1, p1, LX/GpP;->a:LX/CHm;

    invoke-interface {v1}, LX/CHl;->c()LX/CHa;

    move-result-object v1

    move-object v1, v1

    .line 2395984
    invoke-interface {v1}, LX/CHa;->j()Ljava/lang/String;

    move-result-object v1

    .line 2395985
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2395986
    :goto_1
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395987
    check-cast v0, LX/Gqp;

    invoke-interface {p1}, LX/GoY;->D()LX/GoE;

    move-result-object v1

    .line 2395988
    iget-object v2, v0, LX/Gqp;->g:LX/Go0;

    invoke-virtual {v2, v1}, LX/Go0;->a(LX/GoE;)V

    .line 2395989
    iget-object v0, p1, LX/GpP;->a:LX/CHm;

    invoke-interface {v0}, LX/CHl;->z()LX/8Z4;

    move-result-object v0

    move-object v0, v0

    .line 2395990
    iput-object v0, p0, LX/Gpr;->e:LX/8Z4;

    .line 2395991
    iget-object v0, p1, LX/GpP;->a:LX/CHm;

    invoke-interface {v0}, LX/CHl;->y()LX/8Z4;

    move-result-object v0

    move-object v0, v0

    .line 2395992
    iput-object v0, p0, LX/Gpr;->f:LX/8Z4;

    .line 2395993
    return-void

    .line 2395994
    :cond_0
    iget-object v1, p1, LX/GpP;->a:LX/CHm;

    invoke-interface {v1}, LX/CHl;->x()Z

    move-result v1

    move v1, v1

    .line 2395995
    goto :goto_0

    .line 2395996
    :cond_1
    :try_start_0
    iget-object v2, v0, LX/Gqp;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v4

    invoke-static {v3, v4}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/FrameLayout;->setMinimumHeight(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 2395997
    :catch_0
    goto :goto_1
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 2395998
    iput-boolean p1, p0, LX/Gpr;->d:Z

    .line 2395999
    iget-boolean v0, p0, LX/Gpr;->d:Z

    if-eqz v0, :cond_0

    .line 2396000
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2396001
    check-cast v0, LX/Gqp;

    iget-object v1, p0, LX/Gpr;->e:LX/8Z4;

    invoke-direct {p0, v1}, LX/Gpr;->a(LX/8Z4;)LX/Clf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Gqp;->a(LX/Clf;)V

    .line 2396002
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2396003
    check-cast v0, LX/Gqp;

    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00d2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/Gqp;->a(I)V

    .line 2396004
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2396005
    check-cast v0, LX/Gqp;

    const v1, 0x7f020db6

    invoke-virtual {v0, v1}, LX/Gqp;->b(I)V

    .line 2396006
    :goto_0
    return-void

    .line 2396007
    :cond_0
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2396008
    check-cast v0, LX/Gqp;

    iget-object v1, p0, LX/Gpr;->f:LX/8Z4;

    invoke-direct {p0, v1}, LX/Gpr;->a(LX/8Z4;)LX/Clf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Gqp;->a(LX/Clf;)V

    .line 2396009
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2396010
    check-cast v0, LX/Gqp;

    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a07b7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/Gqp;->a(I)V

    .line 2396011
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2396012
    check-cast v0, LX/Gqp;

    const v1, 0x7f020db5

    invoke-virtual {v0, v1}, LX/Gqp;->b(I)V

    goto :goto_0
.end method
