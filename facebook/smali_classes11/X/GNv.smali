.class public final LX/GNv;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

.field public final synthetic b:LX/6y8;

.field public final synthetic c:Lcom/facebook/adspayments/activity/AddPaymentCardActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/AddPaymentCardActivity;Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;LX/6y8;)V
    .locals 0

    .prologue
    .line 2346649
    iput-object p1, p0, LX/GNv;->c:Lcom/facebook/adspayments/activity/AddPaymentCardActivity;

    iput-object p2, p0, LX/GNv;->a:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    iput-object p3, p0, LX/GNv;->b:LX/6y8;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 3

    .prologue
    .line 2346653
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adspayments/protocol/AddPaymentCardResult;

    .line 2346654
    iget-object v1, p0, LX/GNv;->c:Lcom/facebook/adspayments/activity/AddPaymentCardActivity;

    iget-object v2, p0, LX/GNv;->b:LX/6y8;

    .line 2346655
    invoke-static {v1, v0, v2}, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->a$redex0(Lcom/facebook/adspayments/activity/AddPaymentCardActivity;Lcom/facebook/adspayments/protocol/AddPaymentCardResult;LX/6y8;)V

    .line 2346656
    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 2346651
    iget-object v0, p0, LX/GNv;->c:Lcom/facebook/adspayments/activity/AddPaymentCardActivity;

    iget-object v1, p0, LX/GNv;->a:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/adspayments/activity/PaymentCardActivity;->a(Lcom/facebook/fbservice/service/ServiceException;Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;)V

    .line 2346652
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2346650
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {p0, p1}, LX/GNv;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
