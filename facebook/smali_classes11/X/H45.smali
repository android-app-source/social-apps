.class public LX/H45;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field public a:Lcom/facebook/resources/ui/FbTextView;

.field public b:Landroid/widget/ImageView;

.field public c:LX/H44;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2422315
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/H45;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2422316
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2422313
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/H45;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2422314
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2422307
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2422308
    sget-object v0, LX/H44;->INVALID:LX/H44;

    iput-object v0, p0, LX/H45;->c:LX/H44;

    .line 2422309
    const v0, 0x7f030bbd

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2422310
    const v0, 0x7f0d1d35

    invoke-virtual {p0, v0}, LX/H45;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/H45;->b:Landroid/widget/ImageView;

    .line 2422311
    const v0, 0x7f0d1d36

    invoke-virtual {p0, v0}, LX/H45;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/H45;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 2422312
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;ILX/H44;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2422298
    invoke-direct {p0, p5}, LX/H45;-><init>(Landroid/content/Context;)V

    .line 2422299
    iput-object p4, p0, LX/H45;->c:LX/H44;

    .line 2422300
    iget-object p4, p0, LX/H45;->a:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p4, p1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2422301
    iget-object p1, p0, LX/H45;->b:Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-nez p1, :cond_0

    .line 2422302
    iget-object p1, p0, LX/H45;->b:Landroid/widget/ImageView;

    const p4, 0x7f0210be

    invoke-virtual {p1, p4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 2422303
    :cond_0
    iget-object p1, p0, LX/H45;->b:Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/widget/ImageView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    .line 2422304
    sget-object p4, Landroid/graphics/PorterDuff$Mode;->MULTIPLY:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {p1, p3, p4}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2422305
    iget-object p1, p0, LX/H45;->b:Landroid/widget/ImageView;

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2422306
    return-void
.end method
