.class public final LX/GMk;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:I

.field public final synthetic d:LX/GMm;


# direct methods
.method public constructor <init>(LX/GMm;Ljava/lang/String;Landroid/content/Context;I)V
    .locals 0

    .prologue
    .line 2345208
    iput-object p1, p0, LX/GMk;->d:LX/GMm;

    iput-object p2, p0, LX/GMk;->a:Ljava/lang/String;

    iput-object p3, p0, LX/GMk;->b:Landroid/content/Context;

    iput p4, p0, LX/GMk;->c:I

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2345209
    iget-object v0, p0, LX/GMk;->d:LX/GMm;

    iget-object v1, p0, LX/GMk;->a:Ljava/lang/String;

    iget-object v2, p0, LX/GMk;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, LX/GMm;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 2345210
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 2345211
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2345212
    iget v0, p0, LX/GMk;->c:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2345213
    return-void
.end method
