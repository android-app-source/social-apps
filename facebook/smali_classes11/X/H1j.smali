.class public final enum LX/H1j;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/H1j;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/H1j;

.field public static final enum CATEGORY_PRESET:LX/H1j;

.field public static final enum HISTORY_SUGGESTION:LX/H1j;

.field public static final enum PAGE:LX/H1j;

.field public static final enum TYPEAHEAD_SUGGESTION:LX/H1j;


# instance fields
.field public final mValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2415775
    new-instance v0, LX/H1j;

    const-string v1, "HISTORY_SUGGESTION"

    const-string v2, "history_suggestion"

    invoke-direct {v0, v1, v3, v2}, LX/H1j;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/H1j;->HISTORY_SUGGESTION:LX/H1j;

    .line 2415776
    new-instance v0, LX/H1j;

    const-string v1, "TYPEAHEAD_SUGGESTION"

    const-string v2, "typeahead_suggestion"

    invoke-direct {v0, v1, v4, v2}, LX/H1j;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/H1j;->TYPEAHEAD_SUGGESTION:LX/H1j;

    .line 2415777
    new-instance v0, LX/H1j;

    const-string v1, "CATEGORY_PRESET"

    const-string v2, "category_preset"

    invoke-direct {v0, v1, v5, v2}, LX/H1j;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/H1j;->CATEGORY_PRESET:LX/H1j;

    .line 2415778
    new-instance v0, LX/H1j;

    const-string v1, "PAGE"

    const-string v2, "page"

    invoke-direct {v0, v1, v6, v2}, LX/H1j;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/H1j;->PAGE:LX/H1j;

    .line 2415779
    const/4 v0, 0x4

    new-array v0, v0, [LX/H1j;

    sget-object v1, LX/H1j;->HISTORY_SUGGESTION:LX/H1j;

    aput-object v1, v0, v3

    sget-object v1, LX/H1j;->TYPEAHEAD_SUGGESTION:LX/H1j;

    aput-object v1, v0, v4

    sget-object v1, LX/H1j;->CATEGORY_PRESET:LX/H1j;

    aput-object v1, v0, v5

    sget-object v1, LX/H1j;->PAGE:LX/H1j;

    aput-object v1, v0, v6

    sput-object v0, LX/H1j;->$VALUES:[LX/H1j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2415770
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2415771
    iput-object p3, p0, LX/H1j;->mValue:Ljava/lang/String;

    .line 2415772
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/H1j;
    .locals 1

    .prologue
    .line 2415774
    const-class v0, LX/H1j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/H1j;

    return-object v0
.end method

.method public static values()[LX/H1j;
    .locals 1

    .prologue
    .line 2415773
    sget-object v0, LX/H1j;->$VALUES:[LX/H1j;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/H1j;

    return-object v0
.end method
