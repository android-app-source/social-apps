.class public final enum LX/G17;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/G17;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/G17;

.field public static final enum API_EC_POKE_INVALID_RECIP:LX/G17;

.field public static final enum API_EC_POKE_OUTSTANDING:LX/G17;

.field public static final enum API_EC_POKE_RATE:LX/G17;

.field public static final enum API_EC_POKE_USER_BLOCKED:LX/G17;


# instance fields
.field private final id:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2310726
    new-instance v0, LX/G17;

    const-string v1, "API_EC_POKE_INVALID_RECIP"

    const/16 v2, 0x1fe

    invoke-direct {v0, v1, v3, v2}, LX/G17;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/G17;->API_EC_POKE_INVALID_RECIP:LX/G17;

    .line 2310727
    new-instance v0, LX/G17;

    const-string v1, "API_EC_POKE_OUTSTANDING"

    const/16 v2, 0x1ff

    invoke-direct {v0, v1, v4, v2}, LX/G17;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/G17;->API_EC_POKE_OUTSTANDING:LX/G17;

    .line 2310728
    new-instance v0, LX/G17;

    const-string v1, "API_EC_POKE_RATE"

    const/16 v2, 0x200

    invoke-direct {v0, v1, v5, v2}, LX/G17;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/G17;->API_EC_POKE_RATE:LX/G17;

    .line 2310729
    new-instance v0, LX/G17;

    const-string v1, "API_EC_POKE_USER_BLOCKED"

    const/16 v2, 0x201

    invoke-direct {v0, v1, v6, v2}, LX/G17;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/G17;->API_EC_POKE_USER_BLOCKED:LX/G17;

    .line 2310730
    const/4 v0, 0x4

    new-array v0, v0, [LX/G17;

    sget-object v1, LX/G17;->API_EC_POKE_INVALID_RECIP:LX/G17;

    aput-object v1, v0, v3

    sget-object v1, LX/G17;->API_EC_POKE_OUTSTANDING:LX/G17;

    aput-object v1, v0, v4

    sget-object v1, LX/G17;->API_EC_POKE_RATE:LX/G17;

    aput-object v1, v0, v5

    sget-object v1, LX/G17;->API_EC_POKE_USER_BLOCKED:LX/G17;

    aput-object v1, v0, v6

    sput-object v0, LX/G17;->$VALUES:[LX/G17;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2310733
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2310734
    iput p3, p0, LX/G17;->id:I

    .line 2310735
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/G17;
    .locals 1

    .prologue
    .line 2310736
    const-class v0, LX/G17;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/G17;

    return-object v0
.end method

.method public static values()[LX/G17;
    .locals 1

    .prologue
    .line 2310732
    sget-object v0, LX/G17;->$VALUES:[LX/G17;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/G17;

    return-object v0
.end method


# virtual methods
.method public final getErrorCode()I
    .locals 1

    .prologue
    .line 2310731
    iget v0, p0, LX/G17;->id:I

    return v0
.end method
