.class public LX/GfM;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field private final a:LX/Gf6;

.field private final b:LX/Geu;

.field private final c:LX/Gex;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nA;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Q;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Gf6;LX/Geu;LX/Gex;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Gf6;",
            "LX/Geu;",
            "LX/Gex;",
            "LX/0Ot",
            "<",
            "LX/1nA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Q;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2377250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2377251
    iput-object p1, p0, LX/GfM;->a:LX/Gf6;

    .line 2377252
    iput-object p2, p0, LX/GfM;->b:LX/Geu;

    .line 2377253
    iput-object p3, p0, LX/GfM;->c:LX/Gex;

    .line 2377254
    iput-object p4, p0, LX/GfM;->d:LX/0Ot;

    .line 2377255
    iput-object p5, p0, LX/GfM;->e:LX/0Ot;

    .line 2377256
    return-void
.end method

.method private static a(LX/GfM;LX/1De;LX/1Pn;ZLX/25E;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/1Dg;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;Z",
            "LX/25E;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;Z)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2377246
    if-nez p3, :cond_0

    .line 2377247
    const/4 v0, 0x0

    .line 2377248
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/GfM;->a:LX/Gf6;

    invoke-virtual {v0, p1}, LX/Gf6;->c(LX/1De;)LX/Gf4;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/Gf4;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/Gf4;

    move-result-object v0

    check-cast p2, LX/1Pk;

    invoke-virtual {v0, p2}, LX/Gf4;->a(LX/1Pk;)LX/Gf4;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/Gf4;->a(LX/25E;)LX/Gf4;

    move-result-object v0

    invoke-virtual {v0, p6}, LX/Gf4;->b(Z)LX/Gf4;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Gf4;->a(Z)LX/Gf4;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->b()LX/1Dg;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/1De;LX/25E;)LX/1Di;
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 2377242
    invoke-static {p1}, LX/Gdi;->a(LX/25E;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2377243
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2377244
    const/4 v0, 0x0

    .line 2377245
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/1ne;->j(I)LX/1ne;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/1ne;->i(I)LX/1ne;

    move-result-object v0

    sget-object v1, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v0, v1}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v0

    const v1, 0x7f0a043a

    invoke-virtual {v0, v1}, LX/1ne;->n(I)LX/1ne;

    move-result-object v0

    const v1, 0x7f0b0050

    invoke-virtual {v0, v1}, LX/1ne;->q(I)LX/1ne;

    move-result-object v0

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const v1, 0x7f0b010d

    invoke-interface {v0, v2, v1}, LX/1Di;->c(II)LX/1Di;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(LX/GfM;LX/1De;LX/1f9;LX/25E;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;LX/2dx;ZLX/1Pn;)LX/1Di;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1f9;",
            "LX/25E;",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            "LX/2dx;",
            "ZTE;)",
            "LX/1Di;"
        }
    .end annotation

    .prologue
    .line 2377249
    iget-object v0, p0, LX/GfM;->c:LX/Gex;

    invoke-virtual {v0, p1}, LX/Gex;->c(LX/1De;)LX/Gev;

    move-result-object v0

    check-cast p7, LX/1Pp;

    invoke-virtual {v0, p7}, LX/Gev;->a(LX/1Pp;)LX/Gev;

    move-result-object v0

    invoke-virtual {v0, p4}, LX/Gev;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/Gev;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/Gev;->a(LX/1f9;)LX/Gev;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/Gev;->a(LX/25E;)LX/Gev;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/Gev;->a(LX/2dx;)LX/Gev;

    move-result-object v0

    invoke-virtual {v0, p6}, LX/Gev;->b(Z)LX/Gev;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/Gev;->a(Z)LX/Gev;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/0QB;)LX/GfM;
    .locals 9

    .prologue
    .line 2377231
    const-class v1, LX/GfM;

    monitor-enter v1

    .line 2377232
    :try_start_0
    sget-object v0, LX/GfM;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2377233
    sput-object v2, LX/GfM;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2377234
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2377235
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2377236
    new-instance v3, LX/GfM;

    invoke-static {v0}, LX/Gf6;->a(LX/0QB;)LX/Gf6;

    move-result-object v4

    check-cast v4, LX/Gf6;

    invoke-static {v0}, LX/Geu;->a(LX/0QB;)LX/Geu;

    move-result-object v5

    check-cast v5, LX/Geu;

    invoke-static {v0}, LX/Gex;->a(LX/0QB;)LX/Gex;

    move-result-object v6

    check-cast v6, LX/Gex;

    const/16 v7, 0x6b9

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x5d3

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/GfM;-><init>(LX/Gf6;LX/Geu;LX/Gex;LX/0Ot;LX/0Ot;)V

    .line 2377237
    move-object v0, v3

    .line 2377238
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2377239
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GfM;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2377240
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2377241
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1f9;LX/1Pn;LX/25E;Lcom/facebook/feed/rows/core/props/FeedProps;ILX/2dx;ZZ)LX/1Dg;
    .locals 11
    .param p2    # LX/1f9;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/1Pn;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # LX/25E;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p6    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p7    # LX/2dx;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p8    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p9    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "LX/1f9;",
            "TE;",
            "LX/25E;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;I",
            "LX/2dx;",
            "ZZ)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2377230
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const v2, 0x7f020a3d

    invoke-interface {v1, v2}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    move/from16 v0, p6

    invoke-interface {v1, v0}, LX/1Dh;->F(I)LX/1Dh;

    move-result-object v1

    const/16 v2, 0x8

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v8

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, -0x1

    invoke-interface {v1, v2}, LX/1Dh;->W(I)LX/1Dh;

    move-result-object v1

    const/16 v2, 0x8

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/1Dh;->r(II)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x6

    const v3, 0x7f0b0917

    invoke-interface {v1, v2, v3}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x3

    const/16 v3, 0x14

    invoke-interface {v1, v2, v3}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v9

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move/from16 v4, p8

    move-object v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p9

    invoke-static/range {v1 .. v7}, LX/GfM;->a(LX/GfM;LX/1De;LX/1Pn;ZLX/25E;Lcom/facebook/feed/rows/core/props/FeedProps;Z)LX/1Dg;

    move-result-object v1

    invoke-interface {v9, v1}, LX/1Dh;->a(LX/1Dg;)LX/1Dh;

    move-result-object v2

    iget-object v1, p0, LX/GfM;->b:LX/Geu;

    invoke-virtual {v1, p1}, LX/Geu;->c(LX/1De;)LX/Ges;

    move-result-object v3

    invoke-virtual/range {p5 .. p5}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-virtual {v3, v1}, LX/Ges;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/Ges;

    move-result-object v1

    invoke-virtual {v1, p4}, LX/Ges;->a(LX/25E;)LX/Ges;

    move-result-object v1

    invoke-interface {v2, v1}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v1

    invoke-static {p1, p4}, LX/GfM;->a(LX/1De;LX/25E;)LX/1Di;

    move-result-object v2

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v8, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v9

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v1

    const/16 v2, 0x8

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v10

    invoke-virtual/range {p5 .. p5}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v5

    check-cast v5, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object/from16 v6, p7

    move/from16 v7, p9

    move-object v8, p3

    invoke-static/range {v1 .. v8}, LX/GfM;->a(LX/GfM;LX/1De;LX/1f9;LX/25E;Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;LX/2dx;ZLX/1Pn;)LX/1Di;

    move-result-object v1

    invoke-interface {v10, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v9, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    if-eqz p9, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v2, v1}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    return-object v1

    :cond_0
    invoke-static {p1}, LX/GfL;->d(LX/1De;)LX/1dQ;

    move-result-object v1

    goto :goto_0
.end method
