.class public final enum LX/FpR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FpR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FpR;

.field public static final enum CURRENCY:LX/FpR;

.field public static final enum DIVIDER:LX/FpR;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2291583
    new-instance v0, LX/FpR;

    const-string v1, "CURRENCY"

    invoke-direct {v0, v1, v2}, LX/FpR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FpR;->CURRENCY:LX/FpR;

    .line 2291584
    new-instance v0, LX/FpR;

    const-string v1, "DIVIDER"

    invoke-direct {v0, v1, v3}, LX/FpR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FpR;->DIVIDER:LX/FpR;

    .line 2291585
    const/4 v0, 0x2

    new-array v0, v0, [LX/FpR;

    sget-object v1, LX/FpR;->CURRENCY:LX/FpR;

    aput-object v1, v0, v2

    sget-object v1, LX/FpR;->DIVIDER:LX/FpR;

    aput-object v1, v0, v3

    sput-object v0, LX/FpR;->$VALUES:[LX/FpR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2291586
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FpR;
    .locals 1

    .prologue
    .line 2291587
    const-class v0, LX/FpR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FpR;

    return-object v0
.end method

.method public static values()[LX/FpR;
    .locals 1

    .prologue
    .line 2291588
    sget-object v0, LX/FpR;->$VALUES:[LX/FpR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FpR;

    return-object v0
.end method
