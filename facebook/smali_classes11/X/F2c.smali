.class public final LX/F2c;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "LX/Daz;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/15i;

.field public final synthetic b:I

.field public final synthetic c:LX/F2r;


# direct methods
.method public constructor <init>(LX/F2r;LX/DML;LX/15i;I)V
    .locals 0

    .prologue
    .line 2193212
    iput-object p1, p0, LX/F2c;->c:LX/F2r;

    iput-object p3, p0, LX/F2c;->a:LX/15i;

    iput p4, p0, LX/F2c;->b:I

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 8

    .prologue
    .line 2193213
    check-cast p1, LX/Daz;

    const/4 v7, 0x1

    .line 2193214
    iget-object v1, p0, LX/F2c;->c:LX/F2r;

    iget-object v0, p0, LX/F2c;->a:LX/15i;

    iget v2, p0, LX/F2c;->b:I

    const-class v3, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    invoke-virtual {v0, v2, v7, v3, v4}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    iget-object v2, p0, LX/F2c;->c:LX/F2r;

    iget-object v2, v2, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->t()Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 2193215
    sget-object v3, LX/F2e;->b:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2193216
    const-string v3, ""

    :goto_0
    move-object v1, v3

    .line 2193217
    const/4 v2, 0x0

    iget-object v0, p0, LX/F2c;->a:LX/15i;

    iget v3, p0, LX/F2c;->b:I

    const-class v4, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    invoke-virtual {v0, v3, v7, v4, v5}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LX/F2c;->c:LX/F2r;

    iget-object v3, v3, LX/F2r;->p:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    iget-object v0, p0, LX/F2c;->a:LX/15i;

    iget v4, p0, LX/F2c;->b:I

    const-class v5, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    invoke-virtual {v0, v4, v7, v5, v6}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v2, v3, v0}, LX/Daz;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 2193218
    iget-object v0, p0, LX/F2c;->c:LX/F2r;

    iget-object v0, v0, LX/F2r;->m:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, LX/Daz;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2193219
    return-void

    .line 2193220
    :pswitch_0
    iget-object v3, v1, LX/F2r;->b:Landroid/content/res/Resources;

    const v4, 0x7f0831b7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 2193221
    :pswitch_1
    iget-object v3, v1, LX/F2r;->b:Landroid/content/res/Resources;

    const v4, 0x7f0831b8

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 2193222
    :pswitch_2
    iget-object v3, v1, LX/F2r;->b:Landroid/content/res/Resources;

    const v4, 0x7f0831b6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
