.class public LX/GhU;
.super LX/2s5;
.source ""

# interfaces
.implements LX/6Ub;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/GhT;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/GhR;

.field private d:LX/GhS;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;LX/GhR;LX/0gc;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "LX/GhT;",
            ">;",
            "LX/GhR;",
            "LX/0gc;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2384264
    invoke-direct {p0, p4}, LX/2s5;-><init>(LX/0gc;)V

    .line 2384265
    iput-object p1, p0, LX/GhU;->a:Ljava/lang/String;

    .line 2384266
    iput-object p2, p0, LX/GhU;->b:Ljava/util/List;

    .line 2384267
    iput-object p3, p0, LX/GhU;->c:LX/GhR;

    .line 2384268
    new-instance v0, LX/GhS;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, LX/GhS;-><init>(I)V

    iput-object v0, p0, LX/GhU;->d:LX/GhS;

    .line 2384269
    return-void
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 2384263
    iget-object v0, p0, LX/GhU;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GhT;

    iget-object v0, v0, LX/GhT;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 2384262
    const/4 v0, -0x2

    return v0
.end method

.method public final a()LX/6Ua;
    .locals 1

    .prologue
    .line 2384261
    iget-object v0, p0, LX/GhU;->d:LX/GhS;

    return-object v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 8

    .prologue
    .line 2384229
    iget-object v0, p0, LX/GhU;->c:LX/GhR;

    sget-object v1, LX/GhR;->LEAGUE:LX/GhR;

    if-ne v0, v1, :cond_3

    .line 2384230
    iget-object v0, p0, LX/GhU;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GhT;

    iget-object v0, v0, LX/GhT;->a:LX/GiR;

    iget-object v1, p0, LX/GhU;->a:Ljava/lang/String;

    iget-object v2, p0, LX/GhU;->b:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/GhT;

    iget-object v2, v2, LX/GhT;->b:Ljava/lang/String;

    iget-object v3, p0, LX/GhU;->b:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/GhT;

    iget-object v3, v3, LX/GhT;->d:Ljava/lang/String;

    iget-object v4, p0, LX/GhU;->b:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/GhT;

    iget-object v4, v4, LX/GhT;->e:Ljava/lang/String;

    iget-object v5, p0, LX/GhU;->b:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/GhT;

    iget-object v5, v5, LX/GhT;->f:Ljava/lang/String;

    .line 2384231
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2384232
    new-instance v7, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;

    invoke-direct {v7}, Lcom/facebook/gametime/ui/reaction/GametimeLeagueReactionFragment;-><init>()V

    .line 2384233
    const-string p0, "ptr_enabled"

    const/4 p1, 0x1

    invoke-virtual {v6, p0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2384234
    const-string p0, "league_id"

    invoke-virtual {v6, p0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2384235
    const-string p0, "load_type"

    invoke-virtual {v6, p0, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2384236
    const-string p0, "reaction_surface"

    invoke-virtual {v6, p0, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2384237
    if-eqz v3, :cond_0

    .line 2384238
    const-string p0, "empty_state_image_uri"

    invoke-virtual {v6, p0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2384239
    :cond_0
    if-eqz v4, :cond_1

    .line 2384240
    const-string p0, "empty_state_text"

    invoke-virtual {v6, p0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2384241
    :cond_1
    if-eqz v5, :cond_2

    .line 2384242
    const-string p0, "empty_state_sub_text"

    invoke-virtual {v6, p0, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2384243
    :cond_2
    invoke-virtual {v7, v6}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2384244
    move-object v0, v7

    .line 2384245
    :goto_0
    return-object v0

    :cond_3
    iget-object v0, p0, LX/GhU;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GhT;

    iget-object v0, v0, LX/GhT;->a:LX/GiR;

    iget-object v1, p0, LX/GhU;->a:Ljava/lang/String;

    iget-object v2, p0, LX/GhU;->b:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/GhT;

    iget-object v2, v2, LX/GhT;->b:Ljava/lang/String;

    iget-object v3, p0, LX/GhU;->b:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/GhT;

    iget-object v3, v3, LX/GhT;->d:Ljava/lang/String;

    iget-object v4, p0, LX/GhU;->b:Ljava/util/List;

    invoke-interface {v4, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/GhT;

    iget-object v4, v4, LX/GhT;->e:Ljava/lang/String;

    iget-object v5, p0, LX/GhU;->b:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/GhT;

    iget-object v5, v5, LX/GhT;->f:Ljava/lang/String;

    .line 2384246
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2384247
    new-instance v7, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;

    invoke-direct {v7}, Lcom/facebook/gametime/ui/reaction/GametimeMatchupFragment;-><init>()V

    .line 2384248
    const-string p0, "ptr_enabled"

    const/4 p1, 0x1

    invoke-virtual {v6, p0, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2384249
    const-string p0, "page_id"

    invoke-virtual {v6, p0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2384250
    const-string p0, "load_type"

    invoke-virtual {v6, p0, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2384251
    const-string p0, "reaction_surface"

    invoke-virtual {v6, p0, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2384252
    if-eqz v3, :cond_4

    .line 2384253
    const-string p0, "empty_state_image_uri"

    invoke-virtual {v6, p0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2384254
    :cond_4
    if-eqz v4, :cond_5

    .line 2384255
    const-string p0, "empty_state_text"

    invoke-virtual {v6, p0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2384256
    :cond_5
    if-eqz v5, :cond_6

    .line 2384257
    const-string p0, "empty_state_sub_text"

    invoke-virtual {v6, p0, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2384258
    :cond_6
    invoke-virtual {v7, v6}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2384259
    move-object v0, v7

    .line 2384260
    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2384226
    invoke-super {p0, p1, p2}, LX/2s5;->a(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v1

    .line 2384227
    iget-object v0, p0, LX/GhU;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GhT;

    iput-object v1, v0, LX/GhT;->g:Ljava/lang/Object;

    .line 2384228
    return-object v1
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 2384207
    iget-object v0, p0, LX/GhU;->d:LX/GhS;

    .line 2384208
    iget p0, v0, LX/GhS;->a:I

    if-ge p2, p0, :cond_0

    iget-object p0, v0, LX/GhS;->b:[I

    aget p0, p0, p2

    if-ne p0, p1, :cond_1

    .line 2384209
    :cond_0
    :goto_0
    return-void

    .line 2384210
    :cond_1
    iget-object p0, v0, LX/GhS;->b:[I

    aput p1, p0, p2

    .line 2384211
    invoke-virtual {v0}, LX/6UZ;->b()V

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2384225
    iget-object v0, p0, LX/GhU;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 2384220
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/GhU;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2384221
    iget-object v0, p0, LX/GhU;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GhT;

    iget-object v0, v0, LX/GhT;->g:Ljava/lang/Object;

    if-ne v0, p1, :cond_0

    .line 2384222
    :goto_1
    return v1

    .line 2384223
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2384224
    :cond_1
    const/4 v1, -0x2

    goto :goto_1
.end method

.method public final e(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2384217
    iget-object v0, p0, LX/GhU;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 2384218
    const/4 v0, 0x0

    .line 2384219
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/GhU;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GhT;

    iget-object v0, v0, LX/GhT;->g:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final f(I)I
    .locals 1

    .prologue
    .line 2384212
    iget-object v0, p0, LX/GhU;->d:LX/GhS;

    .line 2384213
    iget p0, v0, LX/GhS;->a:I

    if-lt p1, p0, :cond_0

    .line 2384214
    const/4 p0, 0x0

    .line 2384215
    :goto_0
    move v0, p0

    .line 2384216
    return v0

    :cond_0
    iget-object p0, v0, LX/GhS;->b:[I

    aget p0, p0, p1

    goto :goto_0
.end method
