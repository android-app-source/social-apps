.class public LX/GSs;
.super Lcom/facebook/widget/CustomViewGroup;
.source ""


# instance fields
.field public a:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2354879
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomViewGroup;-><init>(Landroid/content/Context;)V

    .line 2354880
    const p1, 0x7f0300f0

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2354881
    const p1, 0x7f0d0561

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    iput-object p1, p0, LX/GSs;->a:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    .line 2354882
    return-void
.end method


# virtual methods
.method public setBlocked(Z)V
    .locals 3

    .prologue
    .line 2354883
    if-eqz p1, :cond_0

    .line 2354884
    iget-object v0, p0, LX/GSs;->a:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setShowActionButton(Z)V

    .line 2354885
    iget-object v0, p0, LX/GSs;->a:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2354886
    :goto_0
    return-void

    .line 2354887
    :cond_0
    iget-object v0, p0, LX/GSs;->a:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setShowActionButton(Z)V

    .line 2354888
    iget-object v0, p0, LX/GSs;->a:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    invoke-virtual {p0}, LX/GSs;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f083792

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
