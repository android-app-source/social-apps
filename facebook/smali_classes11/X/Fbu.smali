.class public LX/Fbu;
.super LX/FbY;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FbY",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Fbu;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2261331
    invoke-direct {p0}, LX/FbY;-><init>()V

    .line 2261332
    return-void
.end method

.method public static a(LX/0QB;)LX/Fbu;
    .locals 3

    .prologue
    .line 2261333
    sget-object v0, LX/Fbu;->a:LX/Fbu;

    if-nez v0, :cond_1

    .line 2261334
    const-class v1, LX/Fbu;

    monitor-enter v1

    .line 2261335
    :try_start_0
    sget-object v0, LX/Fbu;->a:LX/Fbu;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2261336
    if-eqz v2, :cond_0

    .line 2261337
    :try_start_1
    new-instance v0, LX/Fbu;

    invoke-direct {v0}, LX/Fbu;-><init>()V

    .line 2261338
    move-object v0, v0

    .line 2261339
    sput-object v0, LX/Fbu;->a:LX/Fbu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2261340
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2261341
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2261342
    :cond_1
    sget-object v0, LX/Fbu;->a:LX/Fbu;

    return-object v0

    .line 2261343
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2261344
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261345
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->fM_()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->fM_()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 2261346
    :goto_0
    if-nez v4, :cond_1

    .line 2261347
    const/4 v0, 0x0

    .line 2261348
    :goto_1
    return-object v0

    .line 2261349
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->b()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    goto :goto_0

    .line 2261350
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->fN_()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;

    move-result-object v0

    invoke-static {v0}, LX/A0T;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;)Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v8

    .line 2261351
    invoke-static {v4, v8}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;)V

    .line 2261352
    new-instance v0, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;

    invoke-virtual {p2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->j()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    invoke-virtual {p2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    :goto_2
    invoke-virtual {p2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->o()LX/8dH;

    move-result-object v5

    invoke-virtual {p2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->e()Ljava/lang/String;

    move-result-object v6

    .line 2261353
    if-eqz v8, :cond_3

    .line 2261354
    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->l()LX/0Px;

    move-result-object v7

    .line 2261355
    if-eqz v7, :cond_3

    invoke-virtual {v7}, LX/0Px;->isEmpty()Z

    move-result p0

    if-nez p0, :cond_3

    .line 2261356
    const/4 p0, 0x0

    invoke-virtual {v7, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGraphSearchSnippet;->j()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    .line 2261357
    if-eqz v7, :cond_3

    .line 2261358
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v7

    .line 2261359
    :goto_3
    move-object v7, v7

    .line 2261360
    if-eqz v8, :cond_4

    invoke-virtual {v8}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;->m()Ljava/lang/String;

    move-result-object p0

    :goto_4
    move-object v8, p0

    .line 2261361
    invoke-direct/range {v0 .. v8}, Lcom/facebook/search/results/model/unit/SearchResultsStoryUnit;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Lcom/facebook/graphql/model/GraphQLStory;LX/8dH;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->a()LX/0Px;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    goto :goto_2

    :cond_3
    const/4 v7, 0x0

    goto :goto_3

    :cond_4
    const/4 p0, 0x0

    goto :goto_4
.end method
