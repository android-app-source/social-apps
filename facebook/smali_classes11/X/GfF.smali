.class public LX/GfF;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pj;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/GfD;

.field private final b:LX/3mL;

.field public final c:LX/1LV;

.field private final d:LX/25J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/25J",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/GfD;LX/3mL;LX/1LV;LX/Ge0;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2377079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2377080
    iput-object p1, p0, LX/GfF;->a:LX/GfD;

    .line 2377081
    iput-object p2, p0, LX/GfF;->b:LX/3mL;

    .line 2377082
    iput-object p3, p0, LX/GfF;->c:LX/1LV;

    .line 2377083
    iput-object p4, p0, LX/GfF;->d:LX/25J;

    .line 2377084
    return-void
.end method

.method public static a(LX/0QB;)LX/GfF;
    .locals 7

    .prologue
    .line 2377085
    const-class v1, LX/GfF;

    monitor-enter v1

    .line 2377086
    :try_start_0
    sget-object v0, LX/GfF;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2377087
    sput-object v2, LX/GfF;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2377088
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2377089
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2377090
    new-instance p0, LX/GfF;

    const-class v3, LX/GfD;

    invoke-interface {v0, v3}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v3

    check-cast v3, LX/GfD;

    invoke-static {v0}, LX/3mL;->a(LX/0QB;)LX/3mL;

    move-result-object v4

    check-cast v4, LX/3mL;

    invoke-static {v0}, LX/1LV;->a(LX/0QB;)LX/1LV;

    move-result-object v5

    check-cast v5, LX/1LV;

    invoke-static {v0}, LX/Ge0;->a(LX/0QB;)LX/Ge0;

    move-result-object v6

    check-cast v6, LX/Ge0;

    invoke-direct {p0, v3, v4, v5, v6}, LX/GfF;-><init>(LX/GfD;LX/3mL;LX/1LV;LX/Ge0;)V

    .line 2377091
    move-object v0, p0

    .line 2377092
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2377093
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GfF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2377094
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2377095
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1Pp;LX/99i;IZ)LX/1Dg;
    .locals 9
    .param p2    # LX/1Pp;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p3    # LX/99i;
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p4    # I
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .param p5    # Z
        .annotation build Lcom/facebook/components/annotations/Prop;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "TE;",
            "LX/99i",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;",
            ">;IZ)",
            "Lcom/facebook/components/ComponentLayout;"
        }
    .end annotation

    .prologue
    .line 2377096
    iget-object v1, p0, LX/GfF;->d:LX/25J;

    iget-object v0, p3, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2377097
    iget-object v2, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v2

    .line 2377098
    check-cast v0, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-virtual {v1, v0}, LX/25J;->c(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/0Px;

    move-result-object v2

    .line 2377099
    new-instance v6, LX/GfG;

    iget-object v1, p0, LX/GfF;->d:LX/25J;

    iget-object v0, p3, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2377100
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 2377101
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-direct {v6, v1, v0}, LX/GfG;-><init>(LX/25J;Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;)V

    .line 2377102
    new-instance v0, LX/GfE;

    invoke-direct {v0, p0, p3}, LX/GfE;-><init>(LX/GfF;LX/99i;)V

    .line 2377103
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v1

    .line 2377104
    iput-object v0, v1, LX/3mP;->g:LX/25K;

    .line 2377105
    move-object v0, v1

    .line 2377106
    const/4 v1, 0x0

    .line 2377107
    iput-boolean v1, v0, LX/3mP;->a:Z

    .line 2377108
    move-object v1, v0

    .line 2377109
    iget-object v0, p3, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2377110
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 2377111
    check-cast v0, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v0

    .line 2377112
    iput-object v0, v1, LX/3mP;->d:LX/25L;

    .line 2377113
    move-object v1, v1

    .line 2377114
    iget-object v0, p3, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2377115
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 2377116
    check-cast v0, LX/0jW;

    .line 2377117
    iput-object v0, v1, LX/3mP;->e:LX/0jW;

    .line 2377118
    move-object v0, v1

    .line 2377119
    invoke-virtual {v0}, LX/3mP;->a()LX/25M;

    move-result-object v5

    .line 2377120
    iget-object v0, p0, LX/GfF;->a:LX/GfD;

    iget-object v3, p3, LX/99i;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, p1

    move-object v4, p2

    move v7, p4

    move v8, p5

    invoke-virtual/range {v0 .. v8}, LX/GfD;->a(Landroid/content/Context;LX/0Px;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/Object;LX/25M;LX/GfG;IZ)LX/GfC;

    move-result-object v0

    .line 2377121
    iget-object v1, p0, LX/GfF;->b:LX/3mL;

    invoke-virtual {v1, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/4 v1, 0x7

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    return-object v0
.end method
