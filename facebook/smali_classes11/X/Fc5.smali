.class public final LX/Fc5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Fc4;


# instance fields
.field public final synthetic a:LX/Fc6;


# direct methods
.method public constructor <init>(LX/Fc6;)V
    .locals 0

    .prologue
    .line 2261918
    iput-object p1, p0, LX/Fc5;->a:LX/Fc6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2261919
    iget-object v0, p0, LX/Fc5;->a:LX/Fc6;

    iget-object v0, v0, LX/Fc6;->f:LX/Fc4;

    invoke-interface {v0, p1}, LX/Fc4;->a(Ljava/lang/String;)V

    .line 2261920
    iget-object v0, p0, LX/Fc5;->a:LX/Fc6;

    iget-object v0, v0, LX/Fc6;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2261921
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/7Hc;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/results/protocol/filters/FilterValue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2261922
    iget-object v0, p0, LX/Fc5;->a:LX/Fc6;

    .line 2261923
    iget-object v1, v0, LX/Fc6;->c:Ljava/util/Map;

    invoke-interface {v1, p3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2261924
    iget-object v1, v0, LX/Fc6;->c:Ljava/util/Map;

    new-instance v2, LX/Fcr;

    invoke-direct {v2}, LX/Fcr;-><init>()V

    invoke-interface {v1, p3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2261925
    :cond_0
    iget-object v1, v0, LX/Fc6;->c:Ljava/util/Map;

    invoke-interface {v1, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Fcr;

    move-object v0, v1

    .line 2261926
    iget-object v1, p0, LX/Fc5;->a:LX/Fc6;

    .line 2261927
    invoke-virtual {v0, p2}, LX/Fcr;->b(Ljava/lang/String;)LX/7Hi;

    move-result-object v3

    .line 2261928
    if-nez v3, :cond_2

    sget-object v2, LX/Fc6;->a:LX/7Hc;

    .line 2261929
    :goto_0
    iget-object v3, v1, LX/Fc6;->g:LX/7Hj;

    sget-object v4, LX/7HY;->UNSET:LX/7HY;

    invoke-virtual {v3, v2, p4, v4}, LX/7Hj;->a(LX/7Hc;LX/7Hc;LX/7HY;)LX/7Hc;

    move-result-object v2

    .line 2261930
    new-instance v3, LX/7Hi;

    new-instance v4, LX/7B6;

    invoke-direct {v4, p2}, LX/7B6;-><init>(Ljava/lang/String;)V

    sget-object v5, LX/7HY;->UNSET:LX/7HY;

    sget-object v6, LX/7Ha;->EXACT:LX/7Ha;

    invoke-direct {v3, v4, v2, v5, v6}, LX/7Hi;-><init>(LX/7B6;LX/7Hc;LX/7HY;LX/7Ha;)V

    invoke-virtual {v0, p2, v3}, LX/Fcr;->a(Ljava/lang/String;LX/7Hi;)V

    .line 2261931
    iget-object v2, v0, LX/Fcr;->a:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    move-object v2, v2

    .line 2261932
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2261933
    invoke-virtual {v2, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2261934
    invoke-virtual {v0, v2}, LX/Fcr;->a(Ljava/lang/String;)LX/7Hi;

    move-result-object v4

    .line 2261935
    iget-object v5, v4, LX/7Hi;->b:LX/7Hc;

    move-object v4, v5

    .line 2261936
    new-instance v5, LX/7Hc;

    .line 2261937
    iget-object v6, p4, LX/7Hc;->b:LX/0Px;

    move-object v6, v6

    .line 2261938
    invoke-static {v2, v6}, LX/Fc6;->b(Ljava/lang/String;Ljava/util/List;)LX/0Px;

    move-result-object v6

    .line 2261939
    iget v7, v4, LX/7Hc;->c:I

    move v7, v7

    .line 2261940
    iget v8, p4, LX/7Hc;->d:I

    move v8, v8

    .line 2261941
    invoke-direct {v5, v6, v7, v8}, LX/7Hc;-><init>(LX/0Px;II)V

    .line 2261942
    iget-object v6, v1, LX/Fc6;->g:LX/7Hj;

    sget-object v7, LX/7HY;->UNSET:LX/7HY;

    invoke-virtual {v6, v4, v5, v7}, LX/7Hj;->a(LX/7Hc;LX/7Hc;LX/7HY;)LX/7Hc;

    move-result-object v4

    .line 2261943
    new-instance v5, LX/7Hi;

    new-instance v6, LX/7B6;

    invoke-direct {v6, v2}, LX/7B6;-><init>(Ljava/lang/String;)V

    sget-object v7, LX/7HY;->UNSET:LX/7HY;

    sget-object v8, LX/7Ha;->EXACT:LX/7Ha;

    invoke-direct {v5, v6, v4, v7, v8}, LX/7Hi;-><init>(LX/7B6;LX/7Hc;LX/7HY;LX/7Ha;)V

    invoke-virtual {v0, v2, v5}, LX/Fcr;->a(Ljava/lang/String;LX/7Hi;)V

    goto :goto_1

    .line 2261944
    :cond_2
    new-instance v2, LX/7Hc;

    .line 2261945
    iget-object v4, v3, LX/7Hi;->b:LX/7Hc;

    move-object v4, v4

    .line 2261946
    iget-object v5, v4, LX/7Hc;->b:LX/0Px;

    move-object v4, v5

    .line 2261947
    invoke-static {p2, v4}, LX/Fc6;->b(Ljava/lang/String;Ljava/util/List;)LX/0Px;

    move-result-object v4

    .line 2261948
    iget-object v5, v3, LX/7Hi;->b:LX/7Hc;

    move-object v5, v5

    .line 2261949
    iget v6, v5, LX/7Hc;->c:I

    move v5, v6

    .line 2261950
    iget-object v6, v3, LX/7Hi;->b:LX/7Hc;

    move-object v3, v6

    .line 2261951
    iget v6, v3, LX/7Hc;->d:I

    move v3, v6

    .line 2261952
    invoke-direct {v2, v4, v5, v3}, LX/7Hc;-><init>(LX/0Px;II)V

    goto/16 :goto_0

    .line 2261953
    :cond_3
    iget-object v1, p0, LX/Fc5;->a:LX/Fc6;

    iget-object v1, v1, LX/Fc6;->d:Ljava/util/Map;

    invoke-interface {v1, p3, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2261954
    iget-object v1, p0, LX/Fc5;->a:LX/Fc6;

    iget-object v1, v1, LX/Fc6;->e:Ljava/util/Set;

    invoke-interface {v1, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2261955
    iget-object v1, p0, LX/Fc5;->a:LX/Fc6;

    iget-object v1, v1, LX/Fc6;->i:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2261956
    iget-object v1, p0, LX/Fc5;->a:LX/Fc6;

    invoke-virtual {v0, p2}, LX/Fcr;->a(Ljava/lang/String;)LX/7Hi;

    move-result-object v0

    .line 2261957
    iget-object v2, v0, LX/7Hi;->b:LX/7Hc;

    move-object v0, v2

    .line 2261958
    invoke-static {v1, v0}, LX/Fc6;->a$redex0(LX/Fc6;LX/7Hc;)V

    .line 2261959
    :cond_4
    :goto_2
    return-void

    .line 2261960
    :cond_5
    iget-object v1, p0, LX/Fc5;->a:LX/Fc6;

    iget-object v1, v1, LX/Fc6;->i:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2261961
    iget-object v1, p0, LX/Fc5;->a:LX/Fc6;

    iget-object v1, v1, LX/Fc6;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Fcr;->b(Ljava/lang/String;)LX/7Hi;

    move-result-object v0

    .line 2261962
    if-eqz v0, :cond_6

    iget-object v1, p0, LX/Fc5;->a:LX/Fc6;

    iget-object v1, v1, LX/Fc6;->i:Ljava/lang/String;

    invoke-static {v1, v0}, LX/Fc6;->b(Ljava/lang/String;LX/7Hi;)LX/7Hc;

    move-result-object v0

    .line 2261963
    :goto_3
    iget-object v1, p0, LX/Fc5;->a:LX/Fc6;

    iget-object v1, v1, LX/Fc6;->g:LX/7Hj;

    new-instance v2, LX/7Hc;

    iget-object v3, p0, LX/Fc5;->a:LX/Fc6;

    iget-object v3, v3, LX/Fc6;->i:Ljava/lang/String;

    .line 2261964
    iget-object v4, p4, LX/7Hc;->b:LX/0Px;

    move-object v4, v4

    .line 2261965
    invoke-static {v3, v4}, LX/Fc6;->b(Ljava/lang/String;Ljava/util/List;)LX/0Px;

    move-result-object v3

    .line 2261966
    iget v4, p4, LX/7Hc;->c:I

    move v4, v4

    .line 2261967
    iget v5, p4, LX/7Hc;->d:I

    move v5, v5

    .line 2261968
    invoke-direct {v2, v3, v4, v5}, LX/7Hc;-><init>(LX/0Px;II)V

    sget-object v3, LX/7HY;->UNSET:LX/7HY;

    invoke-virtual {v1, v0, v2, v3}, LX/7Hj;->a(LX/7Hc;LX/7Hc;LX/7HY;)LX/7Hc;

    move-result-object v0

    .line 2261969
    iget-object v1, p0, LX/Fc5;->a:LX/Fc6;

    invoke-static {v1, v0}, LX/Fc6;->a$redex0(LX/Fc6;LX/7Hc;)V

    goto :goto_2

    .line 2261970
    :cond_6
    sget-object v0, LX/Fc6;->a:LX/7Hc;

    goto :goto_3
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2261971
    iget-object v0, p0, LX/Fc5;->a:LX/Fc6;

    iget-object v0, v0, LX/Fc6;->f:LX/Fc4;

    invoke-interface {v0, p1}, LX/Fc4;->b(Ljava/lang/String;)V

    .line 2261972
    iget-object v0, p0, LX/Fc5;->a:LX/Fc6;

    iget-object v0, v0, LX/Fc6;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2261973
    return-void
.end method
