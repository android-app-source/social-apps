.class public final LX/FE2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;)V
    .locals 0

    .prologue
    .line 2215978
    iput-object p1, p0, LX/FE2;->a:Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2215979
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2215980
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2215981
    iget-object v0, p0, LX/FE2;->a:Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;

    iget-object v1, v0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->g:Lcom/facebook/messaging/event/sending/EventMessageParams;

    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 2215982
    :goto_0
    iput-object v0, v1, Lcom/facebook/messaging/event/sending/EventMessageParams;->a:Ljava/lang/String;

    .line 2215983
    iget-object v0, p0, LX/FE2;->a:Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;

    iget-object v0, v0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->i:LX/FEF;

    if-eqz v0, :cond_0

    .line 2215984
    iget-object v0, p0, LX/FE2;->a:Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;

    iget-object v0, v0, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->i:LX/FEF;

    iget-object v1, p0, LX/FE2;->a:Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;

    iget-object v1, v1, Lcom/facebook/messaging/event/sending/EventMessageDetailsFragment;->g:Lcom/facebook/messaging/event/sending/EventMessageParams;

    invoke-virtual {v0, v1}, LX/FEF;->a(Lcom/facebook/messaging/event/sending/EventMessageParams;)V

    .line 2215985
    :cond_0
    return-void

    .line 2215986
    :cond_1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
