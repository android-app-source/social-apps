.class public abstract LX/FfQ;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "LX/Fdv;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/search/results/fragment/spec/GraphSearchResultFragmentSpecification",
        "<TT;>;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

.field public final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;I)V
    .locals 1

    .prologue
    .line 2268480
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2268481
    iput-object p2, p0, LX/FfQ;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2268482
    invoke-virtual {p1, p3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/FfQ;->b:Ljava/lang/String;

    .line 2268483
    return-void
.end method

.method private d()Z
    .locals 2

    .prologue
    .line 2268479
    iget-object v0, p0, LX/FfQ;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->VIDEO_PUBLISHERS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(LX/CwB;Ljava/lang/String;)LX/CwH;
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2268468
    if-nez p2, :cond_0

    invoke-interface {p1}, LX/CwB;->jA_()Ljava/lang/String;

    move-result-object p2

    .line 2268469
    :cond_0
    invoke-interface {p1}, LX/CwB;->b()Ljava/lang/String;

    move-result-object v0

    .line 2268470
    invoke-interface {p1}, LX/CwB;->a()Ljava/lang/String;

    move-result-object v1

    .line 2268471
    iget-object v3, p0, LX/FfQ;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-static {v3, v1}, LX/7BG;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2268472
    const-string v3, "news_v2"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, LX/FfQ;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    if-ne v3, v4, :cond_4

    .line 2268473
    if-eqz v0, :cond_5

    const-string v3, "str/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x1

    :goto_0
    move v3, v3

    .line 2268474
    if-nez v3, :cond_4

    .line 2268475
    :goto_1
    invoke-virtual {p0, p1, p2}, LX/FfQ;->b(LX/CwB;Ljava/lang/String;)LX/CwH;

    move-result-object v1

    .line 2268476
    iput-object v0, v1, LX/CwH;->c:Ljava/lang/String;

    .line 2268477
    move-object v3, v1

    .line 2268478
    invoke-direct {p0}, LX/FfQ;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, v2

    :goto_2
    invoke-direct {p0}, LX/FfQ;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v1, v2

    :goto_3
    invoke-direct {p0}, LX/FfQ;->d()Z

    move-result v4

    if-eqz v4, :cond_3

    :goto_4
    invoke-virtual {v3, v0, v1, v2}, LX/CwH;->a(Ljava/lang/String;Ljava/lang/String;LX/103;)LX/CwH;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-interface {p1}, LX/CwB;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_2
    invoke-interface {p1}, LX/CwB;->j()Ljava/lang/String;

    move-result-object v1

    goto :goto_3

    :cond_3
    invoke-interface {p1}, LX/CwB;->k()LX/103;

    move-result-object v2

    goto :goto_4

    :cond_4
    move-object v0, v1

    goto :goto_1

    :cond_5
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/support/v4/app/Fragment;LX/CwB;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;)V
    .locals 1

    .prologue
    .line 2268484
    instance-of v0, p1, LX/Fdv;

    if-eqz v0, :cond_0

    .line 2268485
    check-cast p1, LX/Fdv;

    invoke-interface {p1, p2, p3, p4}, LX/Fdv;->a(LX/CwB;Lcom/facebook/search/logging/api/SearchTypeaheadSession;LX/8ci;)V

    .line 2268486
    :cond_0
    return-void
.end method

.method public final b(LX/CwB;Ljava/lang/String;)LX/CwH;
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2268438
    instance-of v0, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 2268439
    check-cast v0, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-static {v0}, LX/CwH;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)LX/CwH;

    move-result-object v0

    .line 2268440
    :goto_0
    if-nez p2, :cond_0

    invoke-interface {p1}, LX/CwB;->jA_()Ljava/lang/String;

    move-result-object p2

    .line 2268441
    :cond_0
    iput-object p2, v0, LX/CwH;->e:Ljava/lang/String;

    .line 2268442
    move-object v0, v0

    .line 2268443
    iget-object v1, p0, LX/FfQ;->a:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-object v1, v1

    .line 2268444
    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2268445
    iput-object v1, v0, LX/CwH;->x:LX/0Px;

    .line 2268446
    move-object v0, v0

    .line 2268447
    return-object v0

    .line 2268448
    :cond_1
    new-instance v0, LX/CwH;

    invoke-direct {v0}, LX/CwH;-><init>()V

    invoke-interface {p1}, LX/CwB;->a()Ljava/lang/String;

    move-result-object v1

    .line 2268449
    iput-object v1, v0, LX/CwH;->b:Ljava/lang/String;

    .line 2268450
    move-object v0, v0

    .line 2268451
    invoke-interface {p1}, LX/CwB;->c()Ljava/lang/String;

    move-result-object v1

    .line 2268452
    iput-object v1, v0, LX/CwH;->d:Ljava/lang/String;

    .line 2268453
    move-object v0, v0

    .line 2268454
    invoke-interface {p1}, LX/CwB;->b()Ljava/lang/String;

    move-result-object v1

    .line 2268455
    iput-object v1, v0, LX/CwH;->c:Ljava/lang/String;

    .line 2268456
    move-object v0, v0

    .line 2268457
    invoke-interface {p1}, LX/CwB;->e()Ljava/lang/Boolean;

    move-result-object v1

    .line 2268458
    iput-object v1, v0, LX/CwH;->f:Ljava/lang/Boolean;

    .line 2268459
    move-object v0, v0

    .line 2268460
    invoke-interface {p1}, LX/CwB;->h()LX/0P1;

    move-result-object v1

    .line 2268461
    iput-object v1, v0, LX/CwH;->y:LX/0P1;

    .line 2268462
    move-object v0, v0

    .line 2268463
    invoke-interface {p1}, LX/CwB;->i()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, LX/CwB;->j()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, LX/CwB;->k()LX/103;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/CwH;->a(Ljava/lang/String;Ljava/lang/String;LX/103;)LX/CwH;

    move-result-object v0

    invoke-interface {p1}, LX/CwB;->n()Lcom/facebook/search/model/ReactionSearchData;

    move-result-object v1

    .line 2268464
    iput-object v1, v0, LX/CwH;->k:Lcom/facebook/search/model/ReactionSearchData;

    .line 2268465
    move-object v0, v0

    .line 2268466
    goto :goto_0
.end method

.method public abstract b()LX/Fdv;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public synthetic c(LX/CwB;Ljava/lang/String;)LX/CwA;
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2268467
    invoke-virtual {p0, p1, p2}, LX/FfQ;->a(LX/CwB;Ljava/lang/String;)LX/CwH;

    move-result-object v0

    return-object v0
.end method
