.class public final LX/FyA;
.super LX/BPn;
.source ""


# instance fields
.field public final synthetic a:LX/FyD;


# direct methods
.method public constructor <init>(LX/FyD;)V
    .locals 0

    .prologue
    .line 2306380
    iput-object p1, p0, LX/FyA;->a:LX/FyD;

    invoke-direct {p0}, LX/BPn;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 2306381
    check-cast p1, LX/BPm;

    .line 2306382
    iget-object v0, p0, LX/FyA;->a:LX/FyD;

    iget-object v0, v0, LX/FyD;->b:LX/Fy4;

    if-eqz v0, :cond_0

    .line 2306383
    iget-object v0, p0, LX/FyA;->a:LX/FyD;

    iget-object v0, v0, LX/FyD;->b:LX/Fy4;

    iget-wide v2, p1, LX/BPm;->a:J

    .line 2306384
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 p0, 0x1

    invoke-static {v0, v2, v3, v1, p0}, LX/Fy4;->a$redex0(LX/Fy4;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 2306385
    iget-object v1, v0, LX/Fy4;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2dj;

    sget-object p0, LX/2hB;->PROFILE_BUTTON:LX/2hB;

    invoke-virtual {v1, v2, v3, p0}, LX/2dj;->a(JLX/2hB;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2306386
    new-instance p0, LX/Fxy;

    invoke-direct {p0, v0, v2, v3}, LX/Fxy;-><init>(LX/Fy4;J)V

    iget-object p1, v0, LX/Fy4;->i:Ljava/util/concurrent/Executor;

    invoke-static {v1, p0, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2306387
    :cond_0
    return-void
.end method
