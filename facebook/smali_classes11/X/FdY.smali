.class public final LX/FdY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;)V
    .locals 0

    .prologue
    .line 2263676
    iput-object p1, p0, LX/FdY;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x5a3465af

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2263677
    iget-object v0, p0, LX/FdY;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    iget-object v0, v0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->r:Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;

    if-eqz v0, :cond_0

    .line 2263678
    iget-object v0, p0, LX/FdY;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    iget-object v0, v0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->r:Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;

    .line 2263679
    iget-object v1, v0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->x:LX/FPa;

    .line 2263680
    iget-object v3, v1, LX/FPa;->a:LX/0Zb;

    const-string v0, "places_advanced_filters_reset"

    invoke-static {v1, v0}, LX/FPa;->a(LX/FPa;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v3, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2263681
    :cond_0
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2263682
    iget-object v0, p0, LX/FdY;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    iget-object v0, v0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->p:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_3

    iget-object v0, p0, LX/FdY;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    iget-object v0, v0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->p:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5uu;

    .line 2263683
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2263684
    invoke-interface {v0}, LX/5uu;->e()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    const/4 v5, 0x0

    move v6, v5

    :goto_1
    if-ge v6, v9, :cond_2

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    .line 2263685
    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->a(Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v10

    .line 2263686
    new-instance v11, LX/5ux;

    invoke-direct {v11}, LX/5ux;-><init>()V

    .line 2263687
    invoke-virtual {v10}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->a()Z

    move-result p1

    iput-boolean p1, v11, LX/5ux;->a:Z

    .line 2263688
    invoke-virtual {v10}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->b()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v11, LX/5ux;->b:Ljava/lang/String;

    .line 2263689
    invoke-virtual {v10}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->c()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v11, LX/5ux;->c:Ljava/lang/String;

    .line 2263690
    move-object v10, v11

    .line 2263691
    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->c()Ljava/lang/String;

    move-result-object v11

    const-string p1, "default"

    invoke-virtual {v11, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    .line 2263692
    iput-boolean v11, v10, LX/5ux;->a:Z

    .line 2263693
    invoke-interface {v0}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v11

    const-string p1, "set_search_open_now"

    invoke-virtual {v11, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->c()Ljava/lang/String;

    move-result-object v5

    const-string v11, "on"

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2263694
    const-string v5, "default"

    .line 2263695
    iput-object v5, v10, LX/5ux;->c:Ljava/lang/String;

    .line 2263696
    :cond_1
    new-instance v5, LX/5uz;

    invoke-direct {v5}, LX/5uz;-><init>()V

    invoke-virtual {v10}, LX/5ux;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v10

    .line 2263697
    iput-object v10, v5, LX/5uz;->a:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    .line 2263698
    move-object v5, v5

    .line 2263699
    invoke-virtual {v5}, LX/5uz;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    move-result-object v5

    invoke-virtual {v7, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2263700
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_1

    .line 2263701
    :cond_2
    new-instance v5, LX/5uy;

    invoke-direct {v5}, LX/5uy;-><init>()V

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 2263702
    iput-object v6, v5, LX/5uy;->a:LX/0Px;

    .line 2263703
    move-object v5, v5

    .line 2263704
    invoke-virtual {v5}, LX/5uy;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v5

    .line 2263705
    invoke-static {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;->a(LX/5uu;)Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;

    move-result-object v6

    invoke-static {v6}, LX/5v2;->a(Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;)LX/5v2;

    move-result-object v6

    .line 2263706
    iput-object v5, v6, LX/5v2;->d:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    .line 2263707
    move-object v5, v6

    .line 2263708
    invoke-virtual {v5}, LX/5v2;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageMainFilterFragmentModel$MainFilterModel;

    move-result-object v5

    move-object v0, v5

    .line 2263709
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2263710
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 2263711
    :cond_3
    iget-object v0, p0, LX/FdY;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2263712
    iput-object v1, v0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->p:LX/0Px;

    .line 2263713
    iget-object v0, p0, LX/FdY;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    iget-object v0, v0, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->s:LX/FdR;

    iget-object v1, p0, LX/FdY;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    iget-object v1, v1, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->p:LX/0Px;

    invoke-virtual {v0, v1}, LX/FdR;->a(LX/0Px;)V

    .line 2263714
    const v0, 0x39957417

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void
.end method
