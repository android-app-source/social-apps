.class public final LX/FyQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Landroid/graphics/drawable/Drawable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)V
    .locals 0

    .prologue
    .line 2306577
    iput-object p1, p0, LX/FyQ;->a:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2306572
    iget-object v0, p0, LX/FyQ;->a:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    iget-object v0, v0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->G:LX/BP0;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FyQ;->a:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    iget-object v0, v0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->B:LX/FwQ;

    if-nez v0, :cond_1

    .line 2306573
    :cond_0
    :goto_0
    return-void

    .line 2306574
    :cond_1
    iget-object v0, p0, LX/FyQ;->a:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    iget-object v0, v0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->B:LX/FwQ;

    .line 2306575
    const/4 p0, 0x0

    invoke-static {v0, p0}, LX/FwQ;->d(LX/FwQ;I)V

    .line 2306576
    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2306565
    check-cast p1, Landroid/graphics/drawable/Drawable;

    .line 2306566
    iget-object v0, p0, LX/FyQ;->a:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    iget-object v0, v0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->G:LX/BP0;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FyQ;->a:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    iget-object v0, v0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->B:LX/FwQ;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FyQ;->a:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    iget-object v0, v0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    if-nez v0, :cond_1

    .line 2306567
    :cond_0
    :goto_0
    return-void

    .line 2306568
    :cond_1
    iget-object v0, p0, LX/FyQ;->a:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    iget-object v0, v0, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {v0, p1}, Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;->setPlaceholderDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2306569
    iget-object v0, p0, LX/FyQ;->a:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    iget-object v0, v0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->B:LX/FwQ;

    .line 2306570
    const/4 p0, 0x0

    invoke-static {v0, p0}, LX/FwQ;->b(LX/FwQ;I)V

    .line 2306571
    goto :goto_0
.end method
