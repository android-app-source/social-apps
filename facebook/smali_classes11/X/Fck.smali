.class public LX/Fck;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FcI;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/FcI",
        "<",
        "LX/FdI;",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/FcE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/FcE",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:LX/FcE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/FcE",
            "<",
            "LX/FdI;",
            ">;"
        }
    .end annotation
.end field

.field private static e:LX/0Xm;


# instance fields
.field private final d:LX/0wM;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2262716
    new-instance v0, LX/Fch;

    invoke-direct {v0}, LX/Fch;-><init>()V

    sput-object v0, LX/Fck;->c:LX/FcE;

    .line 2262717
    new-instance v0, LX/Fci;

    invoke-direct {v0}, LX/Fci;-><init>()V

    sput-object v0, LX/Fck;->a:LX/FcE;

    .line 2262718
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const-string v1, "set_search_open_now"

    const v2, 0x7f0207fd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/Fck;->b:LX/0P1;

    return-void
.end method

.method public constructor <init>(LX/0wM;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2262719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2262720
    iput-object p1, p0, LX/Fck;->d:LX/0wM;

    .line 2262721
    return-void
.end method

.method public static a(LX/0QB;)LX/Fck;
    .locals 4

    .prologue
    .line 2262705
    const-class v1, LX/Fck;

    monitor-enter v1

    .line 2262706
    :try_start_0
    sget-object v0, LX/Fck;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2262707
    sput-object v2, LX/Fck;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2262708
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2262709
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2262710
    new-instance p0, LX/Fck;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    invoke-direct {p0, v3}, LX/Fck;-><init>(LX/0wM;)V

    .line 2262711
    move-object v0, p0

    .line 2262712
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2262713
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fck;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2262714
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2262715
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2262704
    if-eqz p0, :cond_0

    const-string v0, "default"

    invoke-static {p0, v0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2262703
    if-eqz p0, :cond_0

    const-string v0, "On"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Default"

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;)LX/CyH;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2262722
    invoke-virtual {p2}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    .line 2262723
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v5

    .line 2262724
    if-eqz v5, :cond_0

    .line 2262725
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 2262726
    invoke-static {v0}, LX/Fck;->a(Ljava/lang/String;)Z

    move-result v1

    .line 2262727
    invoke-static {v1}, LX/Fck;->d(Z)Ljava/lang/String;

    move-result-object v1

    .line 2262728
    :goto_1
    new-instance v2, LX/CyH;

    invoke-direct {v2, p1, v1, v0}, LX/CyH;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v2

    .line 2262729
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public final a()LX/FcE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/FcE",
            "<",
            "LX/FdI;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262702
    sget-object v0, LX/Fck;->c:LX/FcE;

    return-object v0
.end method

.method public final a(LX/CyH;)LX/FcT;
    .locals 5

    .prologue
    .line 2262693
    iget-object v0, p1, LX/CyH;->c:LX/4FP;

    move-object v0, v0

    .line 2262694
    invoke-virtual {v0}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v0

    const-string v1, "name"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2262695
    sget-object v1, LX/Fck;->b:LX/0P1;

    invoke-virtual {v1, v0}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2262696
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unimplemented filter "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2262697
    :cond_0
    new-instance v1, LX/FcT;

    .line 2262698
    iget-object v2, p1, LX/CyH;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2262699
    iget-object v3, p0, LX/Fck;->d:LX/0wM;

    sget-object v4, LX/Fck;->b:LX/0P1;

    invoke-virtual {v4, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const v4, -0xa76f01

    invoke-virtual {v3, v0, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2262700
    iget-object v3, p1, LX/CyH;->c:LX/4FP;

    move-object v3, v3

    .line 2262701
    invoke-direct {v1, v2, v0, v3}, LX/FcT;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;LX/4FP;)V

    return-object v1
.end method

.method public final a(LX/5uu;Landroid/view/View;LX/CyH;LX/FdQ;)V
    .locals 5

    .prologue
    .line 2262676
    check-cast p2, LX/FdI;

    .line 2262677
    invoke-interface {p1}, LX/5uu;->l()Ljava/lang/String;

    move-result-object v0

    .line 2262678
    iget-object v1, p2, LX/FdI;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2262679
    invoke-interface {p1}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v3

    .line 2262680
    if-nez v3, :cond_0

    .line 2262681
    :goto_0
    return-void

    .line 2262682
    :cond_0
    sget-object v0, LX/Fck;->b:LX/0P1;

    invoke-virtual {v0, v3}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2262683
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unimplemented filter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2262684
    :cond_1
    iget-object v0, p3, LX/CyH;->c:LX/4FP;

    move-object v0, v0

    .line 2262685
    invoke-virtual {v0}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v0

    const-string v1, "value"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2262686
    if-eqz v0, :cond_2

    const-string v1, "default"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    const v1, -0x6e685d

    move v2, v1

    .line 2262687
    :goto_1
    iget-object v4, p0, LX/Fck;->d:LX/0wM;

    sget-object v1, LX/Fck;->b:LX/0P1;

    invoke-virtual {v1, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v4, v1, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 2262688
    iget-object v2, p2, LX/FdI;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2262689
    iget-object v1, p2, LX/FdI;->b:Lcom/facebook/widget/SwitchCompat;

    move-object v1, v1

    .line 2262690
    invoke-static {v0}, LX/Fck;->a(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 2262691
    new-instance v0, LX/Fcj;

    invoke-direct {v0, p0, p4, p3, p2}, LX/Fcj;-><init>(LX/Fck;LX/FdQ;LX/CyH;LX/FdI;)V

    invoke-virtual {p2, v0}, LX/FdI;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2262692
    :cond_3
    const v1, -0xa76f01

    move v2, v1

    goto :goto_1
.end method

.method public final a(LX/5uu;Landroid/view/View;LX/CyH;LX/FdU;LX/FdQ;)V
    .locals 0

    .prologue
    .line 2262674
    return-void
.end method

.method public final b()LX/FcE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/FcE",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262675
    sget-object v0, LX/Fck;->a:LX/FcE;

    return-object v0
.end method
