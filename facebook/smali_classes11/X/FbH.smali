.class public abstract LX/FbH;
.super LX/FbD;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FbD",
        "<",
        "Lcom/facebook/search/results/model/SearchResultsBridge;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2259782
    invoke-direct {p0}, LX/FbD;-><init>()V

    return-void
.end method

.method public static a(LX/0Px;)LX/0Px;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLEntityAtRange;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel$RangesModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2259783
    invoke-virtual {p0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2259784
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2259785
    :goto_0
    return-object v0

    .line 2259786
    :cond_0
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2259787
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    .line 2259788
    new-instance v4, LX/4ag;

    invoke-direct {v4}, LX/4ag;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->c()I

    move-result v5

    .line 2259789
    iput v5, v4, LX/4ag;->c:I

    .line 2259790
    move-object v4, v4

    .line 2259791
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->b()I

    move-result v5

    .line 2259792
    iput v5, v4, LX/4ag;->b:I

    .line 2259793
    move-object v4, v4

    .line 2259794
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v5

    .line 2259795
    if-nez v5, :cond_2

    .line 2259796
    const/4 v6, 0x0

    .line 2259797
    :goto_2
    move-object v5, v6

    .line 2259798
    iput-object v5, v4, LX/4ag;->a:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesEntityFieldsModel;

    .line 2259799
    move-object v4, v4

    .line 2259800
    invoke-virtual {v4}, LX/4ag;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel$RangesModel;

    move-result-object v4

    move-object v0, v4

    .line 2259801
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2259802
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2259803
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v6, LX/4ad;

    invoke-direct {v6}, LX/4ad;-><init>()V

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLEntity;->e()Ljava/lang/String;

    move-result-object v0

    .line 2259804
    iput-object v0, v6, LX/4ad;->d:Ljava/lang/String;

    .line 2259805
    move-object v6, v6

    .line 2259806
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLEntity;->v_()Ljava/lang/String;

    move-result-object v0

    .line 2259807
    iput-object v0, v6, LX/4ad;->e:Ljava/lang/String;

    .line 2259808
    move-object v6, v6

    .line 2259809
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLEntity;->w_()Ljava/lang/String;

    move-result-object v0

    .line 2259810
    iput-object v0, v6, LX/4ad;->f:Ljava/lang/String;

    .line 2259811
    move-object v6, v6

    .line 2259812
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLEntity;->j()Ljava/lang/String;

    move-result-object v0

    .line 2259813
    iput-object v0, v6, LX/4ad;->g:Ljava/lang/String;

    .line 2259814
    move-object v6, v6

    .line 2259815
    invoke-virtual {v6}, LX/4ad;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesEntityFieldsModel;

    move-result-object v6

    goto :goto_2
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLImage;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .locals 2
    .param p0    # Lcom/facebook/graphql/model/GraphQLImage;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2259816
    if-nez p0, :cond_0

    .line 2259817
    const/4 v0, 0x0

    .line 2259818
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/4aM;

    invoke-direct {v0}, LX/4aM;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result v1

    .line 2259819
    iput v1, v0, LX/4aM;->a:I

    .line 2259820
    move-object v0, v0

    .line 2259821
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result v1

    .line 2259822
    iput v1, v0, LX/4aM;->c:I

    .line 2259823
    move-object v0, v0

    .line 2259824
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object v1

    .line 2259825
    iput-object v1, v0, LX/4aM;->b:Ljava/lang/String;

    .line 2259826
    move-object v0, v0

    .line 2259827
    invoke-virtual {v0}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;
    .locals 2
    .param p0    # Lcom/facebook/graphql/model/GraphQLTextWithEntities;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2259828
    if-nez p0, :cond_0

    .line 2259829
    const/4 v0, 0x0

    .line 2259830
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/4af;

    invoke-direct {v0}, LX/4af;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v1

    .line 2259831
    iput-object v1, v0, LX/4af;->b:Ljava/lang/String;

    .line 2259832
    move-object v0, v0

    .line 2259833
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/FbH;->a(LX/0Px;)LX/0Px;

    move-result-object v1

    .line 2259834
    iput-object v1, v0, LX/4af;->a:LX/0Px;

    .line 2259835
    move-object v0, v0

    .line 2259836
    invoke-virtual {v0}, LX/4af;->a()Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2259837
    invoke-virtual {p0, p1}, LX/FbH;->b(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)LX/8dO;

    move-result-object v0

    .line 2259838
    if-nez v0, :cond_0

    .line 2259839
    const/4 v0, 0x0

    .line 2259840
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 2259841
    iput-object v1, v0, LX/8dO;->c:Ljava/lang/String;

    .line 2259842
    move-object v0, v0

    .line 2259843
    invoke-virtual {v0}, LX/8dO;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/search/results/model/SearchResultsBridge;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;Ljava/lang/String;)Lcom/facebook/search/results/model/SearchResultsBridge;

    move-result-object v0

    goto :goto_0
.end method

.method public abstract b(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)LX/8dO;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
