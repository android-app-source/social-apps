.class public final LX/FSA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0rl",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLViewer;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Lcom/facebook/api/feed/FetchFeedParams;

.field public final synthetic c:Z

.field public final synthetic d:I

.field public final synthetic e:I

.field public final synthetic f:LX/FSC;

.field private g:I


# direct methods
.method public constructor <init>(LX/FSC;JLcom/facebook/api/feed/FetchFeedParams;ZII)V
    .locals 2

    .prologue
    .line 2241343
    iput-object p1, p0, LX/FSA;->f:LX/FSC;

    iput-wide p2, p0, LX/FSA;->a:J

    iput-object p4, p0, LX/FSA;->b:Lcom/facebook/api/feed/FetchFeedParams;

    iput-boolean p5, p0, LX/FSA;->c:Z

    iput p6, p0, LX/FSA;->d:I

    iput p7, p0, LX/FSA;->e:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2241344
    const/4 v0, 0x0

    iput v0, p0, LX/FSA;->g:I

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 2241345
    iget-object v0, p0, LX/FSA;->f:LX/FSC;

    const-string v1, "success"

    iget-wide v2, p0, LX/FSA;->a:J

    iget v4, p0, LX/FSA;->g:I

    int-to-long v4, v4

    iget-object v6, p0, LX/FSA;->b:Lcom/facebook/api/feed/FetchFeedParams;

    .line 2241346
    iget v7, v6, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v6, v7

    .line 2241347
    const-string v7, ""

    .line 2241348
    invoke-static/range {v0 .. v7}, LX/FSC;->a$redex0(LX/FSC;Ljava/lang/String;JJILjava/lang/String;)V

    .line 2241349
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, LX/FSA;->f:LX/FSC;

    iget-object v1, v1, LX/FSC;->s:LX/0oy;

    invoke-virtual {v1}, LX/0oy;->i()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 2241350
    iget-object v2, p0, LX/FSA;->f:LX/FSC;

    iget-object v2, v2, LX/FSC;->l:LX/0pn;

    iget-object v3, p0, LX/FSA;->f:LX/FSC;

    invoke-static {v3}, LX/FSC;->e(LX/FSC;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, LX/0pn;->a(Lcom/facebook/api/feedtype/FeedType;J)LX/0Px;

    move-result-object v1

    .line 2241351
    iget-boolean v0, p0, LX/FSA;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FSA;->f:LX/FSC;

    .line 2241352
    iget v2, v0, LX/FSC;->D:I

    add-int/lit8 v3, v2, 0x1

    iput v3, v0, LX/FSC;->D:I

    move v0, v2

    .line 2241353
    iget-object v2, p0, LX/FSA;->f:LX/FSC;

    .line 2241354
    iget-object v3, v2, LX/FSC;->w:LX/0ad;

    sget v4, LX/0fe;->U:I

    const/4 v5, 0x3

    invoke-interface {v3, v4, v5}, LX/0ad;->a(II)I

    move-result v3

    move v2, v3

    .line 2241355
    if-gt v0, v2, :cond_0

    .line 2241356
    iget v0, p0, LX/FSA;->d:I

    iget-object v2, p0, LX/FSA;->f:LX/FSC;

    .line 2241357
    iget-object v3, v2, LX/FSC;->w:LX/0ad;

    sget v4, LX/0fe;->V:I

    const/16 v5, 0xa

    invoke-interface {v3, v4, v5}, LX/0ad;->a(II)I

    move-result v3

    move v2, v3

    .line 2241358
    add-int/2addr v0, v2

    iget-object v2, p0, LX/FSA;->f:LX/FSC;

    iget v2, v2, LX/FSC;->C:I

    sub-int/2addr v0, v2

    .line 2241359
    if-lez v0, :cond_0

    .line 2241360
    iget-object v2, p0, LX/FSA;->f:LX/FSC;

    iget-object v3, p0, LX/FSA;->b:Lcom/facebook/api/feed/FetchFeedParams;

    .line 2241361
    iget-object v4, v3, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v3, v4

    .line 2241362
    invoke-virtual {v2, v0, v1, v3}, LX/FSC;->a(ILX/0Px;Ljava/lang/String;)Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v2

    .line 2241363
    if-eqz v2, :cond_0

    .line 2241364
    iget-object v0, v2, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2241365
    if-eqz v0, :cond_0

    .line 2241366
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v0

    .line 2241367
    iget-object v3, v2, Lcom/facebook/api/feed/FetchFeedParams;->e:Ljava/lang/String;

    move-object v3, v3

    .line 2241368
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    iget-object v3, p0, LX/FSA;->f:LX/FSC;

    iget-object v3, v3, LX/FSC;->l:LX/0pn;

    invoke-virtual {v3}, LX/0pn;->d()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 2241369
    iget-object v0, p0, LX/FSA;->f:LX/FSC;

    iget v1, p0, LX/FSA;->e:I

    iget v3, p0, LX/FSA;->d:I

    iget-boolean v4, p0, LX/FSA;->c:Z

    invoke-static {v0, v2, v1, v3, v4}, LX/FSC;->a$redex0(LX/FSC;Lcom/facebook/api/feed/FetchFeedParams;IIZ)V

    .line 2241370
    :goto_0
    return-void

    .line 2241371
    :cond_0
    iget-object v0, p0, LX/FSA;->f:LX/FSC;

    iget-boolean v2, p0, LX/FSA;->c:Z

    invoke-static {v0, v1, v2}, LX/FSC;->a$redex0(LX/FSC;LX/0Px;Z)V

    .line 2241372
    iget-object v0, p0, LX/FSA;->f:LX/FSC;

    iget-object v0, v0, LX/FSC;->q:LX/FSc;

    invoke-virtual {v0}, LX/FSc;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 2241373
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v6, 0x0

    .line 2241374
    iget-object v0, p0, LX/FSA;->f:LX/FSC;

    iget-object v0, v0, LX/FSC;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0pP;->e:LX/0Tn;

    .line 2241375
    iget-wide v10, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v2, v10

    .line 2241376
    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2241377
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2241378
    check-cast v0, Lcom/facebook/graphql/model/GraphQLViewer;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLViewer;->m()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2241379
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2241380
    check-cast v0, Lcom/facebook/graphql/model/GraphQLViewer;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLViewer;->m()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 2241381
    :cond_0
    :goto_0
    return-void

    .line 2241382
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2241383
    check-cast v0, Lcom/facebook/graphql/model/GraphQLViewer;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLViewer;->m()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->j()LX/0Px;

    move-result-object v4

    .line 2241384
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2241385
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v7

    move v3, v6

    :goto_1
    if-ge v3, v7, :cond_4

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;

    .line 2241386
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->n()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v2

    .line 2241387
    if-eqz v2, :cond_5

    .line 2241388
    iget-object v1, p0, LX/FSA;->f:LX/FSC;

    iget-object v1, v1, LX/FSC;->e:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->q_()LX/15i;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 2241389
    invoke-interface {v2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->q_()LX/15i;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/graphql/modelutil/BaseModel;->o_()I

    move-result v8

    const/4 v9, 0x6

    invoke-virtual {v1, v8, v9}, LX/15i;->g(II)I

    move-result v1

    .line 2241390
    if-eqz v1, :cond_2

    .line 2241391
    invoke-interface {v2}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->q_()LX/15i;

    move-result-object v8

    invoke-virtual {v8, v1}, LX/15i;->a(I)V

    :cond_2
    move-object v1, v2

    .line 2241392
    :goto_2
    new-instance v2, LX/1u8;

    invoke-direct {v2}, LX/1u8;-><init>()V

    .line 2241393
    iput-object v1, v2, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 2241394
    move-object v2, v2

    .line 2241395
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->k()Ljava/lang/String;

    move-result-object v8

    .line 2241396
    iput-object v8, v2, LX/1u8;->d:Ljava/lang/String;

    .line 2241397
    move-object v2, v2

    .line 2241398
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->p()Ljava/lang/String;

    move-result-object v8

    .line 2241399
    iput-object v8, v2, LX/1u8;->i:Ljava/lang/String;

    .line 2241400
    move-object v2, v2

    .line 2241401
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->j()Ljava/lang/String;

    move-result-object v8

    .line 2241402
    iput-object v8, v2, LX/1u8;->c:Ljava/lang/String;

    .line 2241403
    move-object v2, v2

    .line 2241404
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->o()D

    move-result-wide v8

    .line 2241405
    iput-wide v8, v2, LX/1u8;->h:D

    .line 2241406
    move-object v2, v2

    .line 2241407
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->m()Ljava/lang/String;

    move-result-object v8

    .line 2241408
    iput-object v8, v2, LX/1u8;->f:Ljava/lang/String;

    .line 2241409
    move-object v2, v2

    .line 2241410
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->l()Z

    move-result v8

    .line 2241411
    iput-boolean v8, v2, LX/1u8;->e:Z

    .line 2241412
    move-object v2, v2

    .line 2241413
    invoke-virtual {v2}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2241414
    iget-object v2, p0, LX/FSA;->f:LX/FSC;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v1, v0}, LX/FSC;->a$redex0(LX/FSC;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V

    .line 2241415
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 2241416
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNewsFeedEdge;->n()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v1

    invoke-static {v1}, LX/4XL;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    goto :goto_2

    .line 2241417
    :cond_4
    new-instance v0, Lcom/facebook/api/feed/FetchFeedResult;

    iget-object v1, p0, LX/FSA;->b:Lcom/facebook/api/feed/FetchFeedParams;

    new-instance v2, LX/0uq;

    invoke-direct {v2}, LX/0uq;-><init>()V

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2241418
    iput-object v3, v2, LX/0uq;->d:LX/0Px;

    .line 2241419
    move-object v3, v2

    .line 2241420
    iget-object v2, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 2241421
    check-cast v2, Lcom/facebook/graphql/model/GraphQLViewer;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLViewer;->m()Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNewsFeedConnection;->l()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v2

    .line 2241422
    iput-object v2, v3, LX/0uq;->g:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2241423
    move-object v2, v3

    .line 2241424
    invoke-virtual {v2}, LX/0uq;->a()Lcom/facebook/graphql/model/GraphQLFeedHomeStories;

    move-result-object v2

    sget-object v3, LX/0ta;->FROM_SERVER:LX/0ta;

    iget-object v4, p0, LX/FSA;->f:LX/FSC;

    iget-object v4, v4, LX/FSC;->h:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    invoke-direct/range {v0 .. v6}, Lcom/facebook/api/feed/FetchFeedResult;-><init>(Lcom/facebook/api/feed/FetchFeedParams;Lcom/facebook/graphql/model/GraphQLFeedHomeStories;LX/0ta;JZ)V

    .line 2241425
    iget-object v1, p0, LX/FSA;->f:LX/FSC;

    iget-object v1, v1, LX/FSC;->i:LX/0pm;

    invoke-virtual {v1, v0}, LX/0pm;->a(Lcom/facebook/api/feed/FetchFeedResult;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v1

    .line 2241426
    :try_start_0
    iget-object v2, p0, LX/FSA;->f:LX/FSC;

    iget-object v2, v2, LX/FSC;->m:LX/1fN;

    invoke-virtual {v2, v1}, LX/1fN;->a(Lcom/facebook/api/feed/FetchFeedResult;)LX/0Px;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2241427
    :goto_3
    iget-object v2, p0, LX/FSA;->f:LX/FSC;

    iget-object v2, v2, LX/FSC;->r:Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;

    invoke-virtual {v2, v1}, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;->a(Lcom/facebook/api/feed/FetchFeedResult;)V

    .line 2241428
    iget v1, p0, LX/FSA;->g:I

    invoke-virtual {v0}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, LX/FSA;->g:I

    .line 2241429
    iget-object v1, p0, LX/FSA;->f:LX/FSC;

    invoke-virtual {v0}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2241430
    iget v2, v1, LX/FSC;->C:I

    add-int/2addr v2, v0

    iput v2, v1, LX/FSC;->C:I

    .line 2241431
    goto/16 :goto_0

    .line 2241432
    :catch_0
    iget-object v2, p0, LX/FSA;->f:LX/FSC;

    iget-object v2, v2, LX/FSC;->j:LX/187;

    invoke-virtual {v2, v1}, LX/187;->b(Lcom/facebook/api/feed/FetchFeedResult;)LX/0Px;

    goto :goto_3

    :cond_5
    move-object v1, v2

    goto/16 :goto_2
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 8

    .prologue
    .line 2241433
    iget-object v0, p0, LX/FSA;->f:LX/FSC;

    const-string v1, "failure"

    iget-wide v2, p0, LX/FSA;->a:J

    iget v4, p0, LX/FSA;->g:I

    int-to-long v4, v4

    iget-object v6, p0, LX/FSA;->b:Lcom/facebook/api/feed/FetchFeedParams;

    .line 2241434
    iget v7, v6, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v6, v7

    .line 2241435
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v7

    .line 2241436
    invoke-static/range {v0 .. v7}, LX/FSC;->a$redex0(LX/FSC;Ljava/lang/String;JJILjava/lang/String;)V

    .line 2241437
    sget-object v0, LX/FSC;->a:Ljava/lang/Class;

    const-string v1, "loadDataForAsyncFeed error"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2241438
    iget-object v0, p0, LX/FSA;->f:LX/FSC;

    iget-object v0, v0, LX/FSC;->q:LX/FSc;

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v2, v3}, LX/FSc;->a(J)V

    .line 2241439
    return-void
.end method
