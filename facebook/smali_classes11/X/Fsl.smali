.class public LX/Fsl;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2297513
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0ad;LX/0zS;)LX/0zS;
    .locals 2

    .prologue
    .line 2297514
    sget-object v0, LX/0zS;->d:LX/0zS;

    if-ne p1, v0, :cond_0

    sget-short v0, LX/0wf;->as:S

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2297515
    sget-object p1, LX/0zS;->a:LX/0zS;

    .line 2297516
    :cond_0
    return-object p1
.end method

.method public static a(LX/0v6;LX/0tX;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;LX/0zS;J)LX/0zX;
    .locals 2
    .param p0    # LX/0v6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0v6;",
            "LX/0tX;",
            "Ljava/lang/String;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/0zS;",
            "J)",
            "LX/0zX",
            "<",
            "Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLInterfaces$TimelineTaggedMediaSetFields;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2297502
    new-instance v0, LX/5xd;

    invoke-direct {v0}, LX/5xd;-><init>()V

    move-object v0, v0

    .line 2297503
    const-string v1, "profile_id"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/5xd;

    .line 2297504
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 2297505
    iput p3, v0, LX/0zO;->B:I

    .line 2297506
    move-object v0, v0

    .line 2297507
    iput-object p4, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2297508
    move-object v0, v0

    .line 2297509
    invoke-virtual {v0, p6, p7}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    invoke-virtual {v0, p5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2297510
    if-eqz p0, :cond_0

    invoke-virtual {p0, v0}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v0

    .line 2297511
    :goto_0
    new-instance v1, LX/Fsk;

    invoke-direct {v1}, LX/Fsk;-><init>()V

    invoke-virtual {v0, v1}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v0

    return-object v0

    .line 2297512
    :cond_0
    invoke-virtual {p1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/FsA;->a(Lcom/google/common/util/concurrent/ListenableFuture;)LX/0zX;

    move-result-object v0

    goto :goto_0
.end method
