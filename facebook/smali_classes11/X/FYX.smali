.class public final LX/FYX;
.super LX/EkQ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EkQ",
        "<",
        "LX/BO1;",
        ">;"
    }
.end annotation


# static fields
.field private static final g:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FYh;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Sc;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/String;

.field private final e:LX/FYm;

.field private final f:Landroid/support/v4/app/FragmentActivity;

.field private final h:LX/19w;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2256079
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->VIDEO_WATCHED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/FYX;->g:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/FYm;Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;LX/19w;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/FYh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Sc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/15W;",
            ">;",
            "LX/FYm;",
            "Landroid/support/v4/app/FragmentActivity;",
            "Ljava/lang/String;",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2256080
    invoke-direct {p0}, LX/EkQ;-><init>()V

    .line 2256081
    iput-object p1, p0, LX/FYX;->a:LX/0Ot;

    .line 2256082
    iput-object p2, p0, LX/FYX;->b:LX/0Ot;

    .line 2256083
    iput-object p3, p0, LX/FYX;->c:LX/0Ot;

    .line 2256084
    iput-object p4, p0, LX/FYX;->e:LX/FYm;

    .line 2256085
    iput-object p6, p0, LX/FYX;->d:Ljava/lang/String;

    .line 2256086
    iput-object p5, p0, LX/FYX;->f:Landroid/support/v4/app/FragmentActivity;

    .line 2256087
    iput-object p7, p0, LX/FYX;->h:LX/19w;

    .line 2256088
    return-void
.end method


# virtual methods
.method public bridge synthetic onClick(Landroid/view/View;LX/AU0;)V
    .locals 0

    .prologue
    .line 2256089
    check-cast p2, LX/BO1;

    invoke-virtual {p0, p1, p2}, LX/FYX;->onClick(Landroid/view/View;LX/BO1;)V

    return-void
.end method

.method public onClick(Landroid/view/View;LX/BO1;)V
    .locals 9

    .prologue
    .line 2256090
    iget-object v0, p0, LX/FYX;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FYh;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/FYX;->d:Ljava/lang/String;

    .line 2256091
    invoke-interface {p2}, LX/BO1;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, LX/FYh;->a(LX/FYh;Ljava/lang/String;Ljava/lang/String;)V

    .line 2256092
    invoke-interface {p2}, LX/BO1;->F()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {p2}, LX/BO1;->e()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2256093
    invoke-interface {p2}, LX/BO1;->X()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    iget-object v3, v0, LX/FYh;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/19w;

    invoke-interface {p2}, LX/BO1;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/19w;->f(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2256094
    iget-object v3, v0, LX/FYh;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/19v;

    invoke-interface {p2}, LX/BO1;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/19v;->a(Ljava/lang/String;)V

    .line 2256095
    new-instance v3, LX/6WI;

    invoke-direct {v3, v1}, LX/6WI;-><init>(Landroid/content/Context;)V

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, LX/6WI;->a(Z)LX/6WI;

    move-result-object v3

    .line 2256096
    const v4, 0x7f083425

    invoke-virtual {v3, v4}, LX/6WI;->a(I)LX/6WI;

    move-result-object v3

    .line 2256097
    const v4, 0x7f083426

    invoke-virtual {v3, v4}, LX/6WI;->b(I)LX/6WI;

    move-result-object v3

    .line 2256098
    const-string v4, "OK"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, LX/6WI;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/6WI;

    .line 2256099
    invoke-virtual {v3}, LX/6WI;->a()LX/6WJ;

    move-result-object v3

    .line 2256100
    invoke-virtual {v3}, LX/6WJ;->show()V

    .line 2256101
    :cond_0
    :goto_0
    iget-object v0, p0, LX/FYX;->h:LX/19w;

    invoke-interface {p2}, LX/BO1;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/19w;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2256102
    iget-object v0, p0, LX/FYX;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/15W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, LX/FYX;->g:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v3, LX/0i1;

    check-cast p1, Lcom/facebook/saved2/ui/itemadapters/Saved2DashboardItemView;

    .line 2256103
    iget-object v4, p1, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v4, v4

    .line 2256104
    invoke-virtual {v0, v1, v2, v3, v4}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    .line 2256105
    :cond_1
    invoke-interface {p2}, LX/BO1;->F()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2256106
    iget-object v0, p0, LX/FYX;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Sc;

    invoke-interface {p2}, LX/AUV;->d()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/1Sc;->e(J)V

    .line 2256107
    :cond_2
    iget-object v0, p0, LX/FYX;->e:LX/FYm;

    iget-object v1, p0, LX/FYX;->f:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0, v1, p2}, LX/FYm;->a(Landroid/support/v4/app/FragmentActivity;LX/BO1;)V

    .line 2256108
    return-void

    .line 2256109
    :cond_3
    iget-object v3, v0, LX/FYh;->g:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/FYx;

    invoke-virtual {v3, v1, p2}, LX/FYx;->a(Landroid/content/Context;LX/BO1;)V

    goto :goto_0

    .line 2256110
    :cond_4
    invoke-interface {p2}, LX/BO1;->r()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2256111
    new-instance v3, LX/89k;

    invoke-direct {v3}, LX/89k;-><init>()V

    invoke-interface {p2}, LX/BO1;->s()Ljava/lang/String;

    move-result-object v4

    .line 2256112
    iput-object v4, v3, LX/89k;->b:Ljava/lang/String;

    .line 2256113
    move-object v3, v3

    .line 2256114
    invoke-virtual {v3}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v4

    .line 2256115
    iget-object v3, v0, LX/FYh;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0hy;

    invoke-interface {v3, v4}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v4

    .line 2256116
    iget-object v3, v0, LX/FYh;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v4, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2256117
    :cond_5
    invoke-interface {p2}, LX/BO1;->t()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2256118
    iget-object v3, v0, LX/FYh;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-interface {p2}, LX/BO1;->u()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 2256119
    :cond_6
    invoke-interface {p2}, LX/BO1;->p()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2256120
    invoke-interface {p2}, LX/BO1;->O()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 2256121
    const-string v3, ""

    .line 2256122
    invoke-interface {p2}, LX/BO1;->P()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_a

    .line 2256123
    invoke-interface {p2}, LX/BO1;->P()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    .line 2256124
    :goto_1
    const-string v5, ""

    .line 2256125
    iget-object v3, v0, LX/FYh;->h:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/FYy;

    invoke-virtual {v3}, LX/FYy;->b()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {p2}, LX/BO1;->Q()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 2256126
    invoke-interface {p2}, LX/BO1;->Q()Ljava/lang/String;

    move-result-object v3

    move-object v5, v3

    .line 2256127
    :cond_7
    iget-object v3, v0, LX/FYh;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/17W;

    sget-object v6, LX/0ax;->hp:Ljava/lang/String;

    invoke-interface {p2}, LX/BO1;->O()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLSavedState;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-interface {p2}, LX/BO1;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-static {v6, v7, v4, v8, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto/16 :goto_0

    .line 2256128
    :cond_8
    invoke-interface {p2}, LX/BO1;->J()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {p2}, LX/BO1;->K()I

    move-result v3

    const v4, 0xa7c5482

    if-ne v3, v4, :cond_9

    invoke-interface {p2}, LX/BO1;->v()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2256129
    new-instance v3, LX/89k;

    invoke-direct {v3}, LX/89k;-><init>()V

    invoke-interface {p2}, LX/BO1;->w()Ljava/lang/String;

    move-result-object v4

    .line 2256130
    iput-object v4, v3, LX/89k;->b:Ljava/lang/String;

    .line 2256131
    move-object v3, v3

    .line 2256132
    invoke-virtual {v3}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v4

    .line 2256133
    iget-object v3, v0, LX/FYh;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0hy;

    invoke-interface {v3, v4}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v4

    .line 2256134
    iget-object v3, v0, LX/FYh;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v4, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2256135
    :cond_9
    iget-object v3, v0, LX/FYh;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/17W;

    invoke-interface {p2}, LX/BO1;->q()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto/16 :goto_0

    :cond_a
    move-object v4, v3

    goto/16 :goto_1
.end method
