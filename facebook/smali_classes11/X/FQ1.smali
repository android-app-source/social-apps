.class public LX/FQ1;
.super LX/AOu;
.source ""


# static fields
.field public static final a:LX/FQ9;


# instance fields
.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public d:Z

.field public e:LX/FPR;

.field public f:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2CombinedResultsFragment;

.field private g:LX/FQA;

.field private final h:Landroid/view/View$OnClickListener;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2238364
    sget-object v0, LX/FQ9;->BOOKMARK:LX/FQ9;

    sput-object v0, LX/FQ1;->a:LX/FQ9;

    return-void
.end method

.method public constructor <init>(LX/0Sh;LX/FQA;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2238397
    invoke-direct {p0, p1}, LX/AOu;-><init>(LX/0Sh;)V

    .line 2238398
    new-instance v0, LX/FQ0;

    invoke-direct {v0, p0}, LX/FQ0;-><init>(LX/FQ1;)V

    iput-object v0, p0, LX/FQ1;->h:Landroid/view/View$OnClickListener;

    .line 2238399
    iput-object p2, p0, LX/FQ1;->g:LX/FQA;

    .line 2238400
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 2238390
    iget-object v0, p0, LX/FQ1;->c:Ljava/util/Map;

    if-eqz v0, :cond_1

    instance-of v0, p1, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    if-eqz v0, :cond_1

    .line 2238391
    check-cast p1, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    .line 2238392
    iget-object v0, p0, LX/FQ1;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2238393
    if-nez v0, :cond_0

    move v0, v1

    .line 2238394
    :goto_0
    return v0

    .line 2238395
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2238396
    goto :goto_0
.end method

.method public final a(I)Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2238389
    iget-object v0, p0, LX/FQ1;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, LX/FQ1;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/FQ1;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2238388
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2238387
    iget-object v0, p0, LX/FQ1;->b:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/FQ1;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final b(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2238367
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2238368
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2238369
    new-instance v1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    invoke-direct {v1, v0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->b(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->c(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->d(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->e(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->f(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->g(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    move-result-object v1

    sget-object v2, LX/FQ1;->a:LX/FQ9;

    invoke-virtual {v1, v2}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(LX/FQ9;)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    move-result-object v1

    .line 2238370
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f021810

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-static {v1, v2}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2238371
    move-object v1, v1

    .line 2238372
    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2238373
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 2238374
    const v4, 0x7f0b0060

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    .line 2238375
    mul-int/lit8 v4, v3, 0x2

    .line 2238376
    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5, v4, v3}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 2238377
    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-virtual {v2, v1, v3, v4}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;II)V

    .line 2238378
    move-object v0, v2

    .line 2238379
    iget-object v2, p0, LX/FQ1;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2238380
    invoke-virtual {p0, p2}, LX/FQ1;->a(I)Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    move-result-object v2

    .line 2238381
    if-eqz v2, :cond_0

    .line 2238382
    iget-object v3, p0, LX/FQ1;->g:LX/FQA;

    invoke-virtual {v3, v1, v2}, LX/FQA;->a(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;)V

    .line 2238383
    :cond_0
    const v1, 0x7f0d019b

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->setTag(ILjava/lang/Object;)V

    .line 2238384
    const v1, 0x7f0d019c

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->setTag(ILjava/lang/Object;)V

    .line 2238385
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2238386
    return-object v0
.end method

.method public final c(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 2238365
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2238366
    return-void
.end method
