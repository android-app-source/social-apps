.class public LX/G5f;
.super LX/G5b;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3Oq;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile d:LX/G5f;


# instance fields
.field public final b:LX/7BO;

.field public final c:LX/2Oj;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2319074
    sget-object v0, LX/3Oq;->PAGE:LX/3Oq;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/G5f;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/3fr;LX/0TD;LX/2RQ;LX/7BO;LX/2Oj;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/SearchRequestExecutor;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2319075
    invoke-direct {p0, p1, p2, p3}, LX/G5b;-><init>(LX/3fr;LX/0TD;LX/2RQ;)V

    .line 2319076
    iput-object p4, p0, LX/G5f;->b:LX/7BO;

    .line 2319077
    iput-object p5, p0, LX/G5f;->c:LX/2Oj;

    .line 2319078
    return-void
.end method

.method public static a(LX/0QB;)LX/G5f;
    .locals 9

    .prologue
    .line 2319079
    sget-object v0, LX/G5f;->d:LX/G5f;

    if-nez v0, :cond_1

    .line 2319080
    const-class v1, LX/G5f;

    monitor-enter v1

    .line 2319081
    :try_start_0
    sget-object v0, LX/G5f;->d:LX/G5f;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2319082
    if-eqz v2, :cond_0

    .line 2319083
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2319084
    new-instance v3, LX/G5f;

    invoke-static {v0}, LX/3fr;->a(LX/0QB;)LX/3fr;

    move-result-object v4

    check-cast v4, LX/3fr;

    invoke-static {v0}, LX/44g;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, LX/0TD;

    invoke-static {v0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v6

    check-cast v6, LX/2RQ;

    invoke-static {v0}, LX/7BO;->a(LX/0QB;)LX/7BO;

    move-result-object v7

    check-cast v7, LX/7BO;

    invoke-static {v0}, LX/2Oj;->a(LX/0QB;)LX/2Oj;

    move-result-object v8

    check-cast v8, LX/2Oj;

    invoke-direct/range {v3 .. v8}, LX/G5f;-><init>(LX/3fr;LX/0TD;LX/2RQ;LX/7BO;LX/2Oj;)V

    .line 2319085
    move-object v0, v3

    .line 2319086
    sput-object v0, LX/G5f;->d:LX/G5f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2319087
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2319088
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2319089
    :cond_1
    sget-object v0, LX/G5f;->d:LX/G5f;

    return-object v0

    .line 2319090
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2319091
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0QK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QK",
            "<",
            "LX/6N1;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2319092
    new-instance v0, LX/G5e;

    invoke-direct {v0, p0}, LX/G5e;-><init>(LX/G5f;)V

    return-object v0
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/3Oq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2319093
    sget-object v0, LX/G5f;->a:LX/0Px;

    return-object v0
.end method
