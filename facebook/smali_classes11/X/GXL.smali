.class public final enum LX/GXL;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GXL;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GXL;

.field public static final enum AVAILABLE:LX/GXL;

.field public static final enum NOT_AVAILABLE:LX/GXL;

.field public static final enum SOLD_OUT:LX/GXL;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2364017
    new-instance v0, LX/GXL;

    const-string v1, "AVAILABLE"

    invoke-direct {v0, v1, v2}, LX/GXL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GXL;->AVAILABLE:LX/GXL;

    .line 2364018
    new-instance v0, LX/GXL;

    const-string v1, "NOT_AVAILABLE"

    invoke-direct {v0, v1, v3}, LX/GXL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GXL;->NOT_AVAILABLE:LX/GXL;

    .line 2364019
    new-instance v0, LX/GXL;

    const-string v1, "SOLD_OUT"

    invoke-direct {v0, v1, v4}, LX/GXL;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GXL;->SOLD_OUT:LX/GXL;

    .line 2364020
    const/4 v0, 0x3

    new-array v0, v0, [LX/GXL;

    sget-object v1, LX/GXL;->AVAILABLE:LX/GXL;

    aput-object v1, v0, v2

    sget-object v1, LX/GXL;->NOT_AVAILABLE:LX/GXL;

    aput-object v1, v0, v3

    sget-object v1, LX/GXL;->SOLD_OUT:LX/GXL;

    aput-object v1, v0, v4

    sput-object v0, LX/GXL;->$VALUES:[LX/GXL;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2364014
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GXL;
    .locals 1

    .prologue
    .line 2364015
    const-class v0, LX/GXL;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GXL;

    return-object v0
.end method

.method public static values()[LX/GXL;
    .locals 1

    .prologue
    .line 2364016
    sget-object v0, LX/GXL;->$VALUES:[LX/GXL;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GXL;

    return-object v0
.end method
