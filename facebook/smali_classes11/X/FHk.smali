.class public final LX/FHk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4ck;
.implements LX/2BD;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field public final synthetic a:LX/FHn;

.field private b:Z

.field public c:J

.field private d:J

.field private e:J

.field private final f:Lcom/facebook/ui/media/attachments/MediaResource;

.field private g:LX/4ck;


# direct methods
.method public constructor <init>(LX/FHn;JLcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2220908
    iput-object p1, p0, LX/FHk;->a:LX/FHn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2220909
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/FHk;->b:Z

    .line 2220910
    iput-wide v2, p0, LX/FHk;->c:J

    .line 2220911
    iput-wide v2, p0, LX/FHk;->d:J

    .line 2220912
    iput-wide p2, p0, LX/FHk;->e:J

    .line 2220913
    iput-object p4, p0, LX/FHk;->f:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2220914
    new-instance v0, LX/CMu;

    iget-object v1, p1, LX/FHn;->e:LX/2Mo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, v1}, LX/CMu;-><init>(LX/2Mo;)V

    iput-object v0, p0, LX/FHk;->g:LX/4ck;

    .line 2220915
    return-void
.end method

.method private c(J)V
    .locals 7

    .prologue
    .line 2220916
    iget-wide v0, p0, LX/FHk;->c:J

    add-long/2addr v0, p1

    iget-wide v2, p0, LX/FHk;->d:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, LX/FHk;->d:J

    .line 2220917
    iget-object v0, p0, LX/FHk;->g:LX/4ck;

    iget-wide v2, p0, LX/FHk;->d:J

    iget-wide v4, p0, LX/FHk;->e:J

    invoke-interface {v0, v2, v3, v4, v5}, LX/4ck;->a(JJ)V

    .line 2220918
    iget-object v0, p0, LX/FHk;->a:LX/FHn;

    iget-object v0, v0, LX/FHn;->a:LX/0Xl;

    iget-object v1, p0, LX/FHk;->f:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-wide v2, p0, LX/FHk;->d:J

    long-to-double v2, v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v2, v4

    iget-wide v4, p0, LX/FHk;->e:J

    long-to-double v4, v4

    div-double/2addr v2, v4

    invoke-static {v1, v2, v3}, LX/FGn;->b(Lcom/facebook/ui/media/attachments/MediaResource;D)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 2220919
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2220920
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/FHk;->b:Z

    .line 2220921
    return-void
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 2220922
    iget-boolean v0, p0, LX/FHk;->b:Z

    if-eqz v0, :cond_0

    .line 2220923
    iget-wide v0, p0, LX/FHk;->c:J

    add-long/2addr v0, p1

    iput-wide v0, p0, LX/FHk;->c:J

    .line 2220924
    const-wide/16 v0, 0x0

    invoke-direct {p0, v0, v1}, LX/FHk;->c(J)V

    .line 2220925
    :cond_0
    return-void
.end method

.method public final a(JJ)V
    .locals 1

    .prologue
    .line 2220926
    iget-boolean v0, p0, LX/FHk;->b:Z

    if-nez v0, :cond_0

    .line 2220927
    invoke-direct {p0, p1, p2}, LX/FHk;->c(J)V

    .line 2220928
    :cond_0
    return-void
.end method
