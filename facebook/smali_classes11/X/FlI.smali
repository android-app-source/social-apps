.class public LX/FlI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/AhN;


# instance fields
.field public a:Landroid/app/Activity;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Zb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1nC;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public i:Landroid/support/v4/app/FragmentActivity;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public j:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private k:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private l:LX/BOb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private m:LX/03V;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public n:LX/FkM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private o:LX/9yI;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public p:Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;

.field public q:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2280045
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2280046
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2280047
    iput-object v0, p0, LX/FlI;->d:LX/0Ot;

    .line 2280048
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2280049
    iput-object v0, p0, LX/FlI;->e:LX/0Ot;

    .line 2280050
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2280051
    iput-object v0, p0, LX/FlI;->f:LX/0Ot;

    .line 2280052
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2280053
    iput-object v0, p0, LX/FlI;->g:LX/0Ot;

    .line 2280054
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2280055
    iput-object v0, p0, LX/FlI;->h:LX/0Ot;

    .line 2280056
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration;
    .locals 5

    .prologue
    .line 2279901
    sget-object v0, LX/21D;->FUNDRAISER_PAGE:LX/21D;

    const-string v1, "fundraiserWallPost"

    invoke-static {v0, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    new-instance v1, LX/89I;

    invoke-static {p0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    sget-object v4, LX/2rw;->FUNDRAISER:LX/2rw;

    invoke-direct {v1, v2, v3, v4}, LX/89I;-><init>(JLX/2rw;)V

    .line 2279902
    iput-object p1, v1, LX/89I;->c:Ljava/lang/String;

    .line 2279903
    move-object v1, v1

    .line 2279904
    iput-object p2, v1, LX/89I;->d:Ljava/lang/String;

    .line 2279905
    move-object v1, v1

    .line 2279906
    invoke-virtual {v1, p3}, LX/89I;->a(LX/2rX;)LX/89I;

    move-result-object v1

    invoke-virtual {v1}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableFriendTagging(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    .line 2279907
    if-eqz p4, :cond_0

    .line 2279908
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageName(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageProfilePicUrl(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPostAsPageViewerContext(Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2279909
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;LX/FlB;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/FlB;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserFollowMutationFieldsModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2279910
    sget-object v0, LX/FlB;->FOLLOW:LX/FlB;

    if-eq p2, v0, :cond_0

    sget-object v0, LX/FlB;->FOLLOWING:LX/FlB;

    if-eq p2, v0, :cond_0

    sget-object v0, LX/FlB;->FOLLOW_FUNDRAISER:LX/FlB;

    if-eq p2, v0, :cond_0

    sget-object v0, LX/FlB;->UNFOLLOW_FUNDRAISER:LX/FlB;

    if-ne p2, v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, LX/0Tp;->b(Z)V

    .line 2279911
    new-instance v0, LX/4F7;

    invoke-direct {v0}, LX/4F7;-><init>()V

    .line 2279912
    const-string v3, "fundraiser_id"

    invoke-virtual {v0, v3, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2279913
    move-object v0, v0

    .line 2279914
    sget-object v3, LX/FlB;->FOLLOW:LX/FlB;

    if-eq p2, v3, :cond_1

    sget-object v3, LX/FlB;->FOLLOW_FUNDRAISER:LX/FlB;

    if-ne p2, v3, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2279915
    const-string v2, "should_follow"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2279916
    move-object v0, v0

    .line 2279917
    new-instance v1, LX/FnV;

    invoke-direct {v1}, LX/FnV;-><init>()V

    move-object v1, v1

    .line 2279918
    const-string v2, "0"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/FnV;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2279919
    iget-object v1, p0, LX/FlI;->k:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    :cond_3
    move v0, v1

    .line 2279920
    goto :goto_0
.end method

.method public static a(LX/FlI;)V
    .locals 2

    .prologue
    .line 2279921
    iget-object v0, p0, LX/FlI;->p:Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;

    iget-boolean v1, p0, LX/FlI;->q:Z

    invoke-virtual {v0, v1}, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->setupActionBar(Z)V

    .line 2279922
    return-void
.end method

.method private static a(LX/FlI;Landroid/app/Activity;LX/0Zb;Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Landroid/support/v4/app/FragmentActivity;Ljava/util/concurrent/ExecutorService;LX/0tX;LX/BOb;LX/03V;LX/FkM;LX/9yI;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FlI;",
            "Landroid/app/Activity;",
            "LX/0Zb;",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "LX/1nC;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "Landroid/support/v4/app/FragmentActivity;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0tX;",
            "LX/BOb;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/FkM;",
            "LX/9yI;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2279923
    iput-object p1, p0, LX/FlI;->a:Landroid/app/Activity;

    iput-object p2, p0, LX/FlI;->b:LX/0Zb;

    iput-object p3, p0, LX/FlI;->c:Landroid/content/Context;

    iput-object p4, p0, LX/FlI;->d:LX/0Ot;

    iput-object p5, p0, LX/FlI;->e:LX/0Ot;

    iput-object p6, p0, LX/FlI;->f:LX/0Ot;

    iput-object p7, p0, LX/FlI;->g:LX/0Ot;

    iput-object p8, p0, LX/FlI;->h:LX/0Ot;

    iput-object p9, p0, LX/FlI;->i:Landroid/support/v4/app/FragmentActivity;

    iput-object p10, p0, LX/FlI;->j:Ljava/util/concurrent/ExecutorService;

    iput-object p11, p0, LX/FlI;->k:LX/0tX;

    iput-object p12, p0, LX/FlI;->l:LX/BOb;

    iput-object p13, p0, LX/FlI;->m:LX/03V;

    iput-object p14, p0, LX/FlI;->n:LX/FkM;

    iput-object p15, p0, LX/FlI;->o:LX/9yI;

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2279924
    new-instance v0, LX/FnM;

    invoke-direct {v0}, LX/FnM;-><init>()V

    move-object v0, v0

    .line 2279925
    const-string v1, "fundraiser_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/FnM;

    .line 2279926
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2279927
    iget-object v1, p0, LX/FlI;->k:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/FlG;

    invoke-direct {v1, p0}, LX/FlG;-><init>(LX/FlI;)V

    iget-object v2, p0, LX/FlI;->j:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2279928
    return-void
.end method

.method public static b(LX/0QB;)LX/FlI;
    .locals 17

    .prologue
    .line 2279929
    new-instance v1, LX/FlI;

    invoke-direct {v1}, LX/FlI;-><init>()V

    .line 2279930
    invoke-static/range {p0 .. p0}, LX/0kU;->a(LX/0QB;)Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Landroid/app/Activity;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    const-class v4, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    const/16 v5, 0xbdc

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x3be

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xbc6

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x455

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xc49

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static/range {p0 .. p0}, LX/1No;->a(LX/0QB;)Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    check-cast v10, Landroid/support/v4/app/FragmentActivity;

    invoke-static/range {p0 .. p0}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v11

    check-cast v11, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v12

    check-cast v12, LX/0tX;

    invoke-static/range {p0 .. p0}, LX/BOb;->a(LX/0QB;)LX/BOb;

    move-result-object v13

    check-cast v13, LX/BOb;

    invoke-static/range {p0 .. p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v14

    check-cast v14, LX/03V;

    invoke-static/range {p0 .. p0}, LX/FkM;->a(LX/0QB;)LX/FkM;

    move-result-object v15

    check-cast v15, LX/FkM;

    invoke-static/range {p0 .. p0}, LX/9yI;->a(LX/0QB;)LX/9yI;

    move-result-object v16

    check-cast v16, LX/9yI;

    invoke-static/range {v1 .. v16}, LX/FlI;->a(LX/FlI;Landroid/app/Activity;LX/0Zb;Landroid/content/Context;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Landroid/support/v4/app/FragmentActivity;Ljava/util/concurrent/ExecutorService;LX/0tX;LX/BOb;LX/03V;LX/FkM;LX/9yI;)V

    .line 2279931
    return-object v1
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2279932
    new-instance v0, LX/0ju;

    iget-object v1, p0, LX/FlI;->i:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {v0, v1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v1, 0x7f08332d

    invoke-virtual {v0, v1}, LX/0ju;->a(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f08332e

    invoke-virtual {v0, v1}, LX/0ju;->b(I)LX/0ju;

    move-result-object v0

    const v1, 0x7f08001c

    new-instance v2, LX/FlD;

    invoke-direct {v2, p0, p1}, LX/FlD;-><init>(LX/FlI;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f080017

    new-instance v2, LX/FlC;

    invoke-direct {v2, p0}, LX/FlC;-><init>(LX/FlI;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2279933
    return-void
.end method

.method public static c(LX/FlI;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/socialgood/protocol/FundraiserDeletionModels$FundraiserDeleteMutationModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2279934
    new-instance v0, LX/4F5;

    invoke-direct {v0}, LX/4F5;-><init>()V

    .line 2279935
    const-string v1, "fundraiser_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2279936
    move-object v0, v0

    .line 2279937
    new-instance v1, LX/FnH;

    invoke-direct {v1}, LX/FnH;-><init>()V

    move-object v1, v1

    .line 2279938
    const-string v2, "0"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/FnH;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2279939
    iget-object v1, p0, LX/FlI;->k:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;)Lcom/facebook/socialgood/model/Fundraiser;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2279940
    new-instance v0, LX/Fm9;

    invoke-direct {v0}, LX/Fm9;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->o()Ljava/lang/String;

    move-result-object v3

    .line 2279941
    iput-object v3, v0, LX/Fm9;->a:Ljava/lang/String;

    .line 2279942
    move-object v0, v0

    .line 2279943
    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->p()Ljava/lang/String;

    move-result-object v3

    .line 2279944
    iput-object v3, v0, LX/Fm9;->f:Ljava/lang/String;

    .line 2279945
    move-object v0, v0

    .line 2279946
    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->k()Ljava/lang/String;

    move-result-object v3

    .line 2279947
    iput-object v3, v0, LX/Fm9;->g:Ljava/lang/String;

    .line 2279948
    move-object v0, v0

    .line 2279949
    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->l()J

    move-result-wide v4

    .line 2279950
    iput-wide v4, v0, LX/Fm9;->h:J

    .line 2279951
    move-object v3, v0

    .line 2279952
    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->j()Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$CharityInterfaceModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2279953
    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->j()Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$CharityInterfaceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$CharityInterfaceModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2279954
    iput-object v0, v3, LX/Fm9;->b:Ljava/lang/String;

    .line 2279955
    move-object v4, v3

    .line 2279956
    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->j()Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$CharityInterfaceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$CharityInterfaceModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    const v5, 0x7fba55ef

    if-ne v0, v5, :cond_1

    move v0, v1

    .line 2279957
    :goto_0
    iput-boolean v0, v4, LX/Fm9;->c:Z

    .line 2279958
    move-object v0, v4

    .line 2279959
    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->j()Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$CharityInterfaceModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel$CharityInterfaceModel;->l()Ljava/lang/String;

    move-result-object v4

    .line 2279960
    iput-object v4, v0, LX/Fm9;->d:Ljava/lang/String;

    .line 2279961
    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->n()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->n()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2279962
    new-instance v0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->n()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->n()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserCoverPhotoFragmentModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2279963
    const/4 v4, 0x1

    iput-boolean v4, v0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->d:Z

    .line 2279964
    iput-object v0, v3, LX/Fm9;->e:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    .line 2279965
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 2279966
    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->m()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2279967
    invoke-virtual {p1}, Lcom/facebook/socialgood/protocol/FundraiserEditModels$FundraiserEditQueryModel;->m()LX/1vs;

    move-result-object v5

    iget-object v6, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 2279968
    invoke-virtual {v4, v0, v2}, LX/15i;->j(II)I

    move-result v0

    .line 2279969
    iput v0, v3, LX/Fm9;->i:I

    .line 2279970
    move-object v0, v3

    .line 2279971
    invoke-virtual {v6, v5, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2279972
    iput-object v1, v0, LX/Fm9;->j:Ljava/lang/String;

    .line 2279973
    :goto_1
    invoke-virtual {v3}, LX/Fm9;->a()Lcom/facebook/socialgood/model/Fundraiser;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_1
    move v0, v2

    .line 2279974
    goto :goto_0

    .line 2279975
    :cond_2
    iget-object v0, p0, LX/FlI;->m:LX/03V;

    const-string v1, "fundraiser_edit_missing_charity"

    const-string v2, "No charity information from edit query."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2279976
    const/4 v0, 0x0

    goto :goto_2

    .line 2279977
    :cond_3
    iget-object v0, p0, LX/FlI;->m:LX/03V;

    const-string v1, "fundraiser_edit_missing_goal_amount"

    const-string v2, "No goal amount in edit query."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2279978
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    invoke-static {v0}, LX/FlB;->fromOrdinal(I)LX/FlB;

    move-result-object v1

    .line 2279979
    if-eqz v1, :cond_0

    .line 2279980
    iget-object v0, p0, LX/FlI;->p:Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;

    .line 2279981
    iget-object v4, v0, Lcom/facebook/socialgood/fundraiserpage/actionbar/FundraiserPageActionBar;->c:LX/FlJ;

    move-object v4, v4

    .line 2279982
    iget-object v0, v4, LX/FlJ;->a:Ljava/lang/String;

    move-object v5, v0

    .line 2279983
    iget-object v0, v4, LX/FlJ;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2279984
    iget-object v6, v4, LX/FlJ;->e:Ljava/lang/String;

    move-object v6, v6

    .line 2279985
    sget-object v7, LX/FlE;->a:[I

    invoke-virtual {v1}, LX/FlB;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_0

    .line 2279986
    :cond_0
    :goto_0
    return v3

    .line 2279987
    :pswitch_0
    iget v1, v4, LX/FlJ;->h:I

    move v1, v1

    .line 2279988
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2279989
    iget-object v4, p0, LX/FlI;->b:LX/0Zb;

    const-string v7, "fundraiser"

    .line 2279990
    new-instance v8, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "fundraiser_page_tapped_share"

    invoke-direct {v8, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "fundraiser_page"

    .line 2279991
    iput-object p1, v8, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2279992
    move-object v8, v8

    .line 2279993
    const-string p1, "fundraiser_campaign_id"

    invoke-virtual {v8, p1, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string p1, "source"

    invoke-virtual {v8, p1, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    const-string p1, "referral_source"

    invoke-virtual {v8, p1, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    move-object v5, v8

    .line 2279994
    invoke-interface {v4, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2279995
    const v4, 0x5e1f75b

    if-ne v1, v4, :cond_6

    const/4 v4, 0x1

    :goto_1
    move v4, v4

    .line 2279996
    if-eqz v4, :cond_1

    .line 2279997
    const v1, 0x4462365a

    invoke-static {v0, v1}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    .line 2279998
    iget-object v0, p0, LX/FlI;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    sget-object v4, LX/21D;->FUNDRAISER_PAGE:LX/21D;

    const-string v5, "fundraiserCampaignShare"

    invoke-static {v1}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v1

    invoke-virtual {v1}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v1

    invoke-static {v4, v5, v1}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    iget-object v2, p0, LX/FlI;->c:Landroid/content/Context;

    invoke-interface {v0, v9, v1, v2}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    goto :goto_0

    .line 2279999
    :cond_1
    invoke-static {v1}, LX/Fkv;->b(I)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2280000
    const v4, 0x23637cfe

    if-ne v1, v4, :cond_8

    const/4 v4, 0x1

    :goto_2
    move v4, v4

    .line 2280001
    if-eqz v4, :cond_7

    :cond_2
    const/4 v4, 0x1

    :goto_3
    move v4, v4

    .line 2280002
    if-eqz v4, :cond_0

    .line 2280003
    invoke-static {v0, v1}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v4

    .line 2280004
    invoke-static {v1}, LX/Fkv;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2280005
    const-string v0, "fundraiserPersonToCharityShare"

    move-object v1, v0

    .line 2280006
    :goto_4
    iget-object v0, p0, LX/FlI;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    sget-object v5, LX/21D;->FUNDRAISER_PAGE:LX/21D;

    invoke-static {v4}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v4

    invoke-virtual {v4}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v4

    invoke-static {v5, v1, v4}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    iget-object v2, p0, LX/FlI;->c:Landroid/content/Context;

    invoke-interface {v0, v9, v1, v2}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2280007
    :cond_3
    const-string v0, "fundraiserPersonForPersonShare"

    move-object v1, v0

    goto :goto_4

    .line 2280008
    :pswitch_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2280009
    iget-object v0, p0, LX/FlI;->l:LX/BOb;

    const-string v1, "fundraiser"

    invoke-virtual {v0, v5, v1, v6}, LX/BOb;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2280010
    iget-object v0, p0, LX/FlI;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    iget-object v2, p0, LX/FlI;->c:Landroid/content/Context;

    invoke-interface {v0, v2, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2280011
    iget-object v0, p0, LX/FlI;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x82

    iget-object v4, p0, LX/FlI;->a:Landroid/app/Activity;

    invoke-interface {v0, v1, v2, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto/16 :goto_0

    .line 2280012
    :pswitch_2
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2280013
    iget-object v0, v4, LX/FlJ;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2280014
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2280015
    iget-object v0, p0, LX/FlI;->b:LX/0Zb;

    const-string v1, "fundraiser"

    .line 2280016
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v7, "fundraiser_page_tapped_go_to_page"

    invoke-direct {v2, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v7, "fundraiser_page"

    .line 2280017
    iput-object v7, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2280018
    move-object v2, v2

    .line 2280019
    const-string v7, "fundraiser_campaign_id"

    invoke-virtual {v2, v7, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v7, "source"

    invoke-virtual {v2, v7, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v7, "referral_source"

    invoke-virtual {v2, v7, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    move-object v1, v2

    .line 2280020
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2280021
    sget-object v0, LX/0ax;->aE:Ljava/lang/String;

    .line 2280022
    iget-object v1, v4, LX/FlJ;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2280023
    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2280024
    iget-object v0, p0, LX/FlI;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v2, p0, LX/FlI;->c:Landroid/content/Context;

    invoke-interface {v0, v2, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2280025
    iget-object v0, p0, LX/FlI;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/FlI;->c:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto/16 :goto_0

    .line 2280026
    :pswitch_3
    iget-object v0, p0, LX/FlI;->i:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    sget-object v1, LX/0wD;->FUNDRAISER_PERSON_TO_CHARITY:LX/0wD;

    invoke-virtual {v1}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v1

    const-string v2, "fundraiser_person_to_charity"

    invoke-static {v0, v9, v5, v1, v2}, LX/9yI;->a(LX/0gc;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2280027
    :pswitch_4
    iget-object v0, v4, LX/FlJ;->g:Ljava/lang/String;

    move-object v0, v0

    .line 2280028
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2280029
    iget-object v0, p0, LX/FlI;->c:Landroid/content/Context;

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 2280030
    iget-object v1, v4, LX/FlJ;->f:Ljava/lang/String;

    move-object v1, v1

    .line 2280031
    iget-object v2, v4, LX/FlJ;->g:Ljava/lang/String;

    move-object v2, v2

    .line 2280032
    invoke-static {v1, v2}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 2280033
    iget-object v0, p0, LX/FlI;->c:Landroid/content/Context;

    iget-object v1, p0, LX/FlI;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081cf5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 2280034
    :pswitch_5
    iget-object v0, v4, LX/FlJ;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2280035
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2280036
    iget-boolean v0, v4, LX/FlJ;->o:Z

    move v0, v0

    .line 2280037
    if-eqz v0, :cond_4

    .line 2280038
    iget-object v0, v4, LX/FlJ;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2280039
    invoke-direct {p0, v0}, LX/FlI;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2280040
    :cond_4
    :pswitch_6
    iget-boolean v0, p0, LX/FlI;->q:Z

    if-nez v0, :cond_5

    move v0, v2

    :goto_5
    iput-boolean v0, p0, LX/FlI;->q:Z

    .line 2280041
    invoke-static {p0}, LX/FlI;->a(LX/FlI;)V

    .line 2280042
    invoke-direct {p0, v5, v1}, LX/FlI;->a(Ljava/lang/String;LX/FlB;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/FlH;

    invoke-direct {v1, p0}, LX/FlH;-><init>(LX/FlI;)V

    iget-object v2, p0, LX/FlI;->j:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto/16 :goto_0

    :cond_5
    move v0, v3

    .line 2280043
    goto :goto_5

    .line 2280044
    :pswitch_7
    invoke-direct {p0, v5}, LX/FlI;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    const/4 v4, 0x0

    goto/16 :goto_1

    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_3

    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
