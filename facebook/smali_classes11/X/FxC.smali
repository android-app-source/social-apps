.class public LX/FxC;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;

.field private final c:LX/03V;

.field private final d:LX/Fx8;

.field private e:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2304567
    const-class v0, LX/FxC;

    sput-object v0, LX/FxC;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;LX/03V;LX/Fx8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2304568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2304569
    iput-object p1, p0, LX/FxC;->b:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;

    .line 2304570
    iput-object p2, p0, LX/FxC;->c:LX/03V;

    .line 2304571
    iput-object p3, p0, LX/FxC;->d:LX/Fx8;

    .line 2304572
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;LX/0Px;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLInterfaces$SuggestedPhoto;",
            ">;",
            "Landroid/view/View$OnClickListener;",
            "Landroid/view/View$OnClickListener;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2304558
    iput-object p2, p0, LX/FxC;->e:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    .line 2304559
    iget-object v0, p0, LX/FxC;->e:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->getSuggestedPhotosFigView()Lcom/facebook/fig/mediagrid/FigMediaGrid;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2304560
    iget-object v0, p0, LX/FxC;->b:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;

    iget-object v1, p0, LX/FxC;->e:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    invoke-virtual {v1}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->getSuggestedPhotosFigView()Lcom/facebook/fig/mediagrid/FigMediaGrid;

    move-result-object v1

    iget-object v2, p0, LX/FxC;->d:LX/Fx8;

    invoke-virtual {v0, p1, v1, p3, v2}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->a(Ljava/lang/String;Lcom/facebook/fig/mediagrid/FigMediaGrid;LX/0Px;LX/Fx7;)V

    .line 2304561
    :goto_0
    iget-object v0, p0, LX/FxC;->e:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    invoke-virtual {v0, p4}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->setOnEditListener(Landroid/view/View$OnClickListener;)V

    .line 2304562
    iget-object v0, p0, LX/FxC;->e:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    invoke-virtual {v0, p5}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->setOnCloseListener(Landroid/view/View$OnClickListener;)V

    .line 2304563
    return-void

    .line 2304564
    :cond_0
    iget-object v0, p0, LX/FxC;->e:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->getSuggestedPhotosMosaicView()Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2304565
    iget-object v0, p0, LX/FxC;->b:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;

    iget-object v1, p0, LX/FxC;->e:Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;

    invoke-virtual {v1}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderSuggestedPhotosView;->getSuggestedPhotosMosaicView()Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;

    move-result-object v1

    iget-object v2, p0, LX/FxC;->d:LX/Fx8;

    invoke-virtual {v0, p1, v1, p3, v2}, Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicBinder;->a(Ljava/lang/String;Lcom/facebook/timeline/header/intro/favphotos/TimelineHeaderFeaturedPhotosMosaicView;LX/0Px;LX/Fx7;)V

    goto :goto_0

    .line 2304566
    :cond_1
    iget-object v0, p0, LX/FxC;->c:LX/03V;

    sget-object v1, LX/FxC;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Favorite photo view not initialized"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
