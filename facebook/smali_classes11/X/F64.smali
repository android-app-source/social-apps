.class public final LX/F64;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/21D;

.field public final synthetic b:LX/F67;


# direct methods
.method public constructor <init>(LX/F67;LX/21D;)V
    .locals 0

    .prologue
    .line 2200128
    iput-object p1, p0, LX/F64;->b:LX/F67;

    iput-object p2, p0, LX/F64;->a:LX/21D;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2200129
    iget-object v0, p0, LX/F64;->b:LX/F67;

    iget-object v0, v0, LX/F67;->a:LX/5pX;

    const v1, 0x7f0831d3

    invoke-static {v0, v1}, LX/0kL;->a(Landroid/content/Context;I)V

    .line 2200130
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2200131
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2200132
    iget-object v0, p0, LX/F64;->b:LX/F67;

    iget-object v1, v0, LX/F67;->d:LX/1Kf;

    const/4 v2, 0x0

    iget-object v0, p0, LX/F64;->b:LX/F67;

    iget-object v0, v0, LX/F67;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v3, p0, LX/F64;->a:LX/21D;

    const-string v4, "groupsReactEditPostLauncher"

    invoke-interface {v0, v3, v4, p1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(LX/21D;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v0

    const/16 v3, 0x6de

    iget-object v4, p0, LX/F64;->b:LX/F67;

    iget-object v4, v4, LX/F67;->b:Landroid/app/Activity;

    invoke-interface {v1, v2, v0, v3, v4}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2200133
    return-void
.end method
