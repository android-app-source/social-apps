.class public LX/GAp;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2326165
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 2326166
    return-void
.end method

.method public static a(LX/0Uh;)LX/03R;
    .locals 1
    .param p0    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/account/recovery/annotations/IsBounceFromMSiteEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 2326167
    const/16 v0, 0x17

    invoke-virtual {p0, v0}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/2Cl;)LX/03R;
    .locals 1
    .annotation runtime Lcom/facebook/account/recovery/annotations/IsGkUnsetBounceFromMSiteEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 2326168
    sget-object v0, LX/2Cm;->AR_BOUNCE_FROM_MSITE:LX/2Cm;

    invoke-virtual {p0, v0}, LX/2Cl;->a(LX/2Cm;)LX/2KF;

    move-result-object v0

    invoke-virtual {v0}, LX/2KF;->a()LX/03R;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0Uh;)LX/03R;
    .locals 1
    .param p0    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/account/recovery/annotations/IsCaptchaEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 2326169
    const/16 v0, 0x1

    invoke-virtual {p0, v0}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/0Uh;)LX/03R;
    .locals 1
    .param p0    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/account/recovery/annotations/IsParallelSearchEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 2326170
    const/16 v0, 0x0

    invoke-virtual {p0, v0}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/0Uh;)LX/03R;
    .locals 1
    .param p0    # LX/0Uh;
        .annotation runtime Lcom/facebook/gk/sessionless/Sessionless;
        .end annotation
    .end param
    .annotation runtime Lcom/facebook/account/recovery/annotations/IsEmailListedBeforeSmsEnabled;
    .end annotation

    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Ljavax/inject/Singleton;
    .end annotation

    .prologue
    .line 2326171
    const/16 v0, 0x18

    invoke-virtual {p0, v0}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 2326172
    return-void
.end method
