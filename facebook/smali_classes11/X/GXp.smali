.class public LX/GXp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

.field public b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "[I>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;)V
    .locals 0

    .prologue
    .line 2364595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2364596
    iput-object p1, p0, LX/GXp;->a:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    .line 2364597
    return-void
.end method

.method private c(I)[I
    .locals 2

    .prologue
    .line 2364574
    invoke-static {p0}, LX/GXp;->d(LX/GXp;)V

    .line 2364575
    iget-object v0, p0, LX/GXp;->b:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    return-object v0
.end method

.method public static d(LX/GXp;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2364576
    iget-object v0, p0, LX/GXp;->b:Ljava/util/Map;

    if-eqz v0, :cond_1

    .line 2364577
    :cond_0
    return-void

    .line 2364578
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/GXp;->b:Ljava/util/Map;

    .line 2364579
    const/4 v2, 0x7

    move v3, v2

    .line 2364580
    move v2, v1

    .line 2364581
    :goto_0
    if-ge v2, v3, :cond_0

    .line 2364582
    iget-object v0, p0, LX/GXp;->a:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2364583
    const/4 v6, 0x4

    if-ne v2, v6, :cond_4

    .line 2364584
    iget-object v4, v0, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->h:LX/GXY;

    invoke-virtual {v4}, LX/GXY;->g()I

    move-result v4

    .line 2364585
    :cond_2
    :goto_1
    move v4, v4

    .line 2364586
    move v0, v1

    .line 2364587
    :goto_2
    if-ge v0, v4, :cond_3

    .line 2364588
    iget-object v5, p0, LX/GXp;->b:Ljava/util/Map;

    iget-object v6, p0, LX/GXp;->b:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    const/4 v7, 0x2

    new-array v7, v7, [I

    aput v2, v7, v1

    const/4 v8, 0x1

    aput v0, v7, v8

    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2364589
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2364590
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2364591
    :cond_4
    const/4 v6, 0x2

    if-ne v2, v6, :cond_5

    .line 2364592
    invoke-static {v0}, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->f(Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;)Z

    move-result v6

    if-nez v6, :cond_2

    move v4, v5

    goto :goto_1

    .line 2364593
    :cond_5
    const/4 v6, 0x3

    if-ne v2, v6, :cond_2

    .line 2364594
    invoke-static {v0}, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->f(Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;)Z

    move-result v6

    if-eqz v6, :cond_2

    move v4, v5

    goto :goto_1
.end method


# virtual methods
.method public final a(I)I
    .locals 2

    .prologue
    .line 2364573
    invoke-direct {p0, p1}, LX/GXp;->c(I)[I

    move-result-object v0

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public final b(I)I
    .locals 2

    .prologue
    .line 2364572
    invoke-direct {p0, p1}, LX/GXp;->c(I)[I

    move-result-object v0

    const/4 v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2364570
    const/4 v0, 0x0

    iput-object v0, p0, LX/GXp;->b:Ljava/util/Map;

    .line 2364571
    return-void
.end method
