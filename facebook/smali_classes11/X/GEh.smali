.class public LX/GEh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GEW",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/GI0;

.field private final b:LX/0ad;


# direct methods
.method public constructor <init>(LX/GI0;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2331992
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2331993
    iput-object p1, p0, LX/GEh;->a:LX/GI0;

    .line 2331994
    iput-object p2, p0, LX/GEh;->b:LX/0ad;

    .line 2331995
    return-void
.end method

.method public static a(LX/0QB;)LX/GEh;
    .locals 3

    .prologue
    .line 2332014
    new-instance v2, LX/GEh;

    invoke-static {p0}, LX/GI0;->b(LX/0QB;)LX/GI0;

    move-result-object v0

    check-cast v0, LX/GI0;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v1

    check-cast v1, LX/0ad;

    invoke-direct {v2, v0, v1}, LX/GEh;-><init>(LX/GI0;LX/0ad;)V

    .line 2332015
    move-object v0, v2

    .line 2332016
    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2332013
    const v0, 0x7f030054

    return v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 5

    .prologue
    .line 2331998
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    const v4, 0x4c046c8a    # 3.4714152E7f

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2331999
    if-eqz p1, :cond_0

    .line 2332000
    iget-object v2, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v2, v2

    .line 2332001
    if-nez v2, :cond_1

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_3

    move v2, v0

    :goto_1
    if-eqz v2, :cond_6

    :goto_2
    if-eqz v0, :cond_8

    move v0, v1

    .line 2332002
    :goto_3
    return v0

    .line 2332003
    :cond_1
    iget-object v2, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v2, v2

    .line 2332004
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->k()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2332005
    if-nez v2, :cond_2

    move v2, v0

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_0

    .line 2332006
    :cond_3
    iget-object v2, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v2, v2

    .line 2332007
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-static {v3, v2, v1, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    .line 2332008
    if-eqz v2, :cond_4

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_4
    if-nez v2, :cond_5

    move v2, v0

    goto :goto_1

    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto :goto_4

    :cond_5
    move v2, v1

    goto :goto_1

    .line 2332009
    :cond_6
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2332010
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->k()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v2, v0, v1, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2332011
    if-eqz v0, :cond_7

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_5
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    goto :goto_2

    :cond_7
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_5

    .line 2332012
    :cond_8
    iget-object v0, p0, LX/GEh;->b:LX/0ad;

    sget-short v2, LX/GDK;->i:S

    invoke-interface {v0, v2, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_3
.end method

.method public final b()LX/GHg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesViewController",
            "<",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesAYMTView;",
            "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2331997
    iget-object v0, p0, LX/GEh;->a:LX/GI0;

    return-object v0
.end method

.method public final c()LX/8wK;
    .locals 1

    .prologue
    .line 2331996
    sget-object v0, LX/8wK;->AYMT_CHANNEL:LX/8wK;

    return-object v0
.end method
