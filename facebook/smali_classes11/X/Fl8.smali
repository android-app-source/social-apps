.class public LX/Fl8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field public final b:LX/0kL;

.field public final c:LX/3iH;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;LX/0kL;LX/3iH;)V
    .locals 0
    .param p1    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2279658
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2279659
    iput-object p1, p0, LX/Fl8;->a:Ljava/util/concurrent/Executor;

    .line 2279660
    iput-object p2, p0, LX/Fl8;->b:LX/0kL;

    .line 2279661
    iput-object p3, p0, LX/Fl8;->c:LX/3iH;

    .line 2279662
    return-void
.end method

.method public static a(LX/0QB;)LX/Fl8;
    .locals 6

    .prologue
    .line 2279663
    const-class v1, LX/Fl8;

    monitor-enter v1

    .line 2279664
    :try_start_0
    sget-object v0, LX/Fl8;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2279665
    sput-object v2, LX/Fl8;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2279666
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2279667
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2279668
    new-instance p0, LX/Fl8;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v4

    check-cast v4, LX/0kL;

    invoke-static {v0}, LX/3iH;->b(LX/0QB;)LX/3iH;

    move-result-object v5

    check-cast v5, LX/3iH;

    invoke-direct {p0, v3, v4, v5}, LX/Fl8;-><init>(Ljava/util/concurrent/Executor;LX/0kL;LX/3iH;)V

    .line 2279669
    move-object v0, p0

    .line 2279670
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2279671
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fl8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2279672
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2279673
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
