.class public final LX/H6I;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/offers/graphql/OfferMutationsModels$OfferViewRemoveFromWalletMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/H6J;


# direct methods
.method public constructor <init>(LX/H6J;)V
    .locals 0

    .prologue
    .line 2426077
    iput-object p1, p0, LX/H6I;->a:LX/H6J;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2426066
    iget-object v0, p0, LX/H6I;->a:LX/H6J;

    iget-object v0, v0, LX/H6J;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    const/4 v1, 0x1

    .line 2426067
    iput-boolean v1, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->am:Z

    .line 2426068
    const-string v0, "OfferDetailPageFragment"

    const-string v1, "Unable to remove offer claim"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2426069
    iget-object v0, p0, LX/H6I;->a:LX/H6J;

    iget-object v0, v0, LX/H6J;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->b:LX/0Sh;

    new-instance v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment$7$1$2;

    invoke-direct {v1, p0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment$7$1$2;-><init>(LX/H6I;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2426070
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2426071
    iget-object v0, p0, LX/H6I;->a:LX/H6J;

    iget-object v0, v0, LX/H6J;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    const/4 v1, 0x0

    .line 2426072
    iput-boolean v1, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->am:Z

    .line 2426073
    iget-object v0, p0, LX/H6I;->a:LX/H6J;

    iget-object v0, v0, LX/H6J;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->i:LX/H82;

    const-string v1, "claim_deleted"

    iget-object v2, p0, LX/H6I;->a:LX/H6J;

    iget-object v2, v2, LX/H6J;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v2, v2, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    const-string v3, "details"

    invoke-virtual {v0, v1, v2, v3}, LX/H82;->a(Ljava/lang/String;LX/H83;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2426074
    iget-object v1, p0, LX/H6I;->a:LX/H6J;

    iget-object v1, v1, LX/H6J;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->h:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2426075
    iget-object v0, p0, LX/H6I;->a:LX/H6J;

    iget-object v0, v0, LX/H6J;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->b:LX/0Sh;

    new-instance v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment$7$1$1;

    invoke-direct {v1, p0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment$7$1$1;-><init>(LX/H6I;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2426076
    return-void
.end method
