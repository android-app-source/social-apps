.class public LX/FSZ;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/FSd;

.field public static final b:Ljava/lang/String;

.field private static final c:[Ljava/lang/Integer;

.field private static d:Lorg/json/JSONObject;


# instance fields
.field private e:Ljava/lang/Boolean;

.field public f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public h:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/GregorianCalendar;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Ljava/util/Random;

.field private final k:LX/0qX;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/16 v7, 0x18

    const/16 v6, 0x13

    const/16 v5, 0xe

    const/16 v3, 0x9

    const/4 v4, 0x0

    .line 2242210
    const-class v0, LX/FSZ;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/FSZ;->b:Ljava/lang/String;

    .line 2242211
    const/16 v0, 0x1c

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const/16 v2, 0x21

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x26

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x2b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x30

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x39

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    const/16 v1, 0xa

    const/16 v2, 0x3e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const/16 v2, 0x43

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const/16 v2, 0x48

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const/16 v2, 0x51

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x56

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    const/16 v1, 0xf

    const/16 v2, 0x5b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const/16 v2, 0x60

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0x69

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const/16 v2, 0x6e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x73

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    const/16 v1, 0x14

    const/16 v2, 0x78

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const/16 v2, 0x81

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const/16 v2, 0x86

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const/16 v2, 0x8b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x90

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    const/16 v1, 0x19

    const/16 v2, 0x99

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const/16 v2, 0x9e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const/16 v2, 0xa3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, LX/FSZ;->c:[Ljava/lang/Integer;

    .line 2242212
    const/4 v0, 0x0

    sput-object v0, LX/FSZ;->d:Lorg/json/JSONObject;

    .line 2242213
    new-instance v0, LX/FSd;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3, v4}, LX/FSd;-><init>(JI)V

    sput-object v0, LX/FSZ;->a:LX/FSd;

    return-void
.end method

.method public constructor <init>(LX/0Or;Ljava/util/Random;LX/0qX;)V
    .locals 1
    .param p2    # Ljava/util/Random;
        .annotation runtime Lcom/facebook/common/random/InsecureRandom;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/util/GregorianCalendar;",
            ">;",
            "Ljava/util/Random;",
            "LX/0qX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2242098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2242099
    iput-object p1, p0, LX/FSZ;->i:LX/0Or;

    .line 2242100
    iput-object v0, p0, LX/FSZ;->f:Ljava/util/ArrayList;

    .line 2242101
    iput-object v0, p0, LX/FSZ;->g:Ljava/util/ArrayList;

    .line 2242102
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/FSZ;->e:Ljava/lang/Boolean;

    .line 2242103
    iput-object p2, p0, LX/FSZ;->j:Ljava/util/Random;

    .line 2242104
    iput-object p3, p0, LX/FSZ;->k:LX/0qX;

    .line 2242105
    return-void
.end method

.method public static a(LX/FSZ;ILjava/util/GregorianCalendar;)I
    .locals 12

    .prologue
    const-wide v4, 0x7fffffffffffffffL

    const-wide/16 v10, 0x3c

    .line 2242198
    if-ltz p1, :cond_1

    iget-object v0, p0, LX/FSZ;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 2242199
    invoke-static {p2}, LX/FSZ;->d(Ljava/util/GregorianCalendar;)J

    move-result-wide v6

    .line 2242200
    iget-object v0, p0, LX/FSZ;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    mul-long/2addr v0, v10

    sub-long/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    .line 2242201
    add-int/lit8 v0, p1, -0x1

    if-ltz v0, :cond_2

    iget-object v0, p0, LX/FSZ;->f:Ljava/util/ArrayList;

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    mul-long/2addr v0, v10

    sub-long/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    move-wide v2, v0

    .line 2242202
    :goto_0
    add-int/lit8 v0, p1, 0x1

    iget-object v1, p0, LX/FSZ;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, LX/FSZ;->f:Ljava/util/ArrayList;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    mul-long/2addr v0, v10

    sub-long/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    .line 2242203
    :cond_0
    invoke-static {v8, v9, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 2242204
    cmp-long v2, v0, v2

    if-nez v2, :cond_3

    .line 2242205
    add-int/lit8 p1, p1, -0x1

    .line 2242206
    :cond_1
    :goto_1
    return p1

    :cond_2
    move-wide v2, v4

    .line 2242207
    goto :goto_0

    .line 2242208
    :cond_3
    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    .line 2242209
    add-int/lit8 p1, p1, 0x1

    goto :goto_1
.end method

.method public static a(LX/FSZ;Ljava/util/GregorianCalendar;)I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 2242188
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/FSZ;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    .line 2242189
    :goto_0
    return v0

    .line 2242190
    :cond_1
    invoke-static {p1}, LX/FSZ;->c(Ljava/util/GregorianCalendar;)J

    move-result-wide v4

    move v1, v2

    .line 2242191
    :goto_1
    :try_start_0
    iget-object v0, p0, LX/FSZ;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2242192
    iget-object v0, p0, LX/FSZ;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    cmp-long v0, v6, v4

    if-lez v0, :cond_2

    move v0, v1

    .line 2242193
    goto :goto_0

    .line 2242194
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    move v0, v2

    .line 2242195
    goto :goto_0

    .line 2242196
    :catch_0
    move-exception v0

    .line 2242197
    sget-object v1, LX/FSZ;->b:Ljava/lang/String;

    const-string v3, "IndexOutOfBoundsException in getNextNumStories."

    invoke-static {v1, v3, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v2

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/FSZ;
    .locals 1

    .prologue
    .line 2242187
    invoke-static {p0}, LX/FSZ;->b(LX/0QB;)LX/FSZ;

    move-result-object v0

    return-object v0
.end method

.method public static a()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 2242179
    sget-object v0, LX/FSZ;->d:Lorg/json/JSONObject;

    if-nez v0, :cond_0

    .line 2242180
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    sput-object v0, LX/FSZ;->d:Lorg/json/JSONObject;

    .line 2242181
    new-instance v0, Lorg/json/JSONArray;

    sget-object v1, LX/FSZ;->c:[Ljava/lang/Integer;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    .line 2242182
    sget-object v1, LX/FSZ;->d:Lorg/json/JSONObject;

    const-string v2, "localTZ"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 2242183
    sget-object v1, LX/FSZ;->d:Lorg/json/JSONObject;

    const-string v2, "hours"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2242184
    :cond_0
    :goto_0
    sget-object v0, LX/FSZ;->d:Lorg/json/JSONObject;

    return-object v0

    .line 2242185
    :catch_0
    move-exception v0

    .line 2242186
    sget-object v1, LX/FSZ;->b:Ljava/lang/String;

    const-string v2, "JSONException in getDefaultSchedule."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/FSZ;
    .locals 4

    .prologue
    .line 2242177
    new-instance v2, LX/FSZ;

    const/16 v0, 0x1616

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0U5;->a(LX/0QB;)Ljava/util/Random;

    move-result-object v0

    check-cast v0, Ljava/util/Random;

    invoke-static {p0}, LX/0qX;->a(LX/0QB;)LX/0qX;

    move-result-object v1

    check-cast v1, LX/0qX;

    invoke-direct {v2, v3, v0, v1}, LX/FSZ;-><init>(LX/0Or;Ljava/util/Random;LX/0qX;)V

    .line 2242178
    return-object v2
.end method

.method public static c(Ljava/util/GregorianCalendar;)J
    .locals 6

    .prologue
    .line 2242171
    if-nez p0, :cond_0

    .line 2242172
    const-wide/16 v0, 0x0

    .line 2242173
    :goto_0
    return-wide v0

    .line 2242174
    :cond_0
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-long v0, v0

    .line 2242175
    const/16 v2, 0xb

    invoke-virtual {p0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    int-to-long v2, v2

    .line 2242176
    const-wide/16 v4, 0x18

    mul-long/2addr v0, v4

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public static d(Ljava/util/GregorianCalendar;)J
    .locals 6

    .prologue
    .line 2242165
    if-nez p0, :cond_0

    .line 2242166
    const-wide/16 v0, 0x0

    .line 2242167
    :goto_0
    return-wide v0

    .line 2242168
    :cond_0
    invoke-static {p0}, LX/FSZ;->c(Ljava/util/GregorianCalendar;)J

    move-result-wide v0

    .line 2242169
    const/16 v2, 0xc

    invoke-virtual {p0, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v2

    int-to-long v2, v2

    .line 2242170
    const-wide/16 v4, 0x3c

    mul-long/2addr v0, v4

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method private d()Z
    .locals 1
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2242164
    iget-object v0, p0, LX/FSZ;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public static e(LX/FSZ;)Ljava/util/GregorianCalendar;
    .locals 2

    .prologue
    .line 2242158
    iget-object v0, p0, LX/FSZ;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/GregorianCalendar;

    .line 2242159
    if-eqz v0, :cond_0

    .line 2242160
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->setFirstDayOfWeek(I)V

    .line 2242161
    invoke-direct {p0}, LX/FSZ;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2242162
    const-string v1, "UTC"

    invoke-static {v1}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/GregorianCalendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 2242163
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x0

    .line 2242136
    if-nez p1, :cond_1

    .line 2242137
    :cond_0
    :goto_0
    return-void

    .line 2242138
    :cond_1
    :try_start_0
    const-string v1, "localTZ"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, LX/FSZ;->e:Ljava/lang/Boolean;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2242139
    :goto_1
    :try_start_1
    const-string v1, "hours"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 2242140
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, LX/FSZ;->f:Ljava/util/ArrayList;

    move v1, v0

    .line 2242141
    :goto_2
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_2

    .line 2242142
    iget-object v3, p0, LX/FSZ;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2242143
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2242144
    :catch_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, LX/FSZ;->e:Ljava/lang/Boolean;

    goto :goto_1

    .line 2242145
    :catch_1
    iput-object v6, p0, LX/FSZ;->f:Ljava/util/ArrayList;

    .line 2242146
    :cond_2
    :try_start_2
    const-string v1, "numStories"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 2242147
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, LX/FSZ;->g:Ljava/util/ArrayList;

    move v1, v0

    .line 2242148
    :goto_3
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_3

    .line 2242149
    iget-object v3, p0, LX/FSZ;->g:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 2242150
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 2242151
    :catch_2
    iput-object v6, p0, LX/FSZ;->g:Ljava/util/ArrayList;

    .line 2242152
    :cond_3
    :try_start_3
    const-string v1, "policy"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v1

    .line 2242153
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v2, p0, LX/FSZ;->h:Ljava/util/ArrayList;

    .line 2242154
    :goto_4
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 2242155
    iget-object v2, p0, LX/FSZ;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getInt(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_3

    .line 2242156
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 2242157
    :catch_3
    iput-object v6, p0, LX/FSZ;->h:Ljava/util/ArrayList;

    goto/16 :goto_0
.end method

.method public final c()LX/FSd;
    .locals 12

    .prologue
    .line 2242106
    iget-object v0, p0, LX/FSZ;->k:LX/0qX;

    invoke-static {}, LX/FSZ;->a()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0qX;->a(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v0

    .line 2242107
    invoke-virtual {p0, v0}, LX/FSZ;->a(Lorg/json/JSONObject;)V

    .line 2242108
    iget-object v0, p0, LX/FSZ;->g:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FSZ;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2242109
    :cond_0
    sget-object v0, LX/FSZ;->a:LX/FSd;

    .line 2242110
    :goto_0
    return-object v0

    .line 2242111
    :cond_1
    invoke-static {p0}, LX/FSZ;->e(LX/FSZ;)Ljava/util/GregorianCalendar;

    move-result-object v0

    const/4 v4, 0x0

    .line 2242112
    if-eqz v0, :cond_2

    iget-object v5, p0, LX/FSZ;->f:Ljava/util/ArrayList;

    if-nez v5, :cond_4

    .line 2242113
    :cond_2
    :goto_1
    move v1, v4

    .line 2242114
    :try_start_0
    iget-object v0, p0, LX/FSZ;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 2242115
    const/4 v6, 0x0

    .line 2242116
    iget-object v4, p0, LX/FSZ;->h:Ljava/util/ArrayList;

    if-eqz v4, :cond_3

    iget-object v4, p0, LX/FSZ;->h:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lt v1, v4, :cond_6

    :cond_3
    move v4, v6

    .line 2242117
    :goto_2
    move v1, v4

    .line 2242118
    new-instance v0, LX/FSd;

    invoke-direct {v0, v2, v3, v1}, LX/FSd;-><init>(JI)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2242119
    :catch_0
    move-exception v0

    .line 2242120
    sget-object v1, LX/FSZ;->b:Ljava/lang/String;

    const-string v2, "IndexOutOfBoundsException in getCurrentNumStories."

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2242121
    sget-object v0, LX/FSZ;->a:LX/FSd;

    goto :goto_0

    .line 2242122
    :cond_4
    invoke-static {v0}, LX/FSZ;->c(Ljava/util/GregorianCalendar;)J

    move-result-wide v8

    move v6, v4

    move v5, v4

    .line 2242123
    :goto_3
    :try_start_1
    iget-object v4, p0, LX/FSZ;->f:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v6, v4, :cond_5

    .line 2242124
    iget-object v4, p0, LX/FSZ;->f:Ljava/util/ArrayList;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v4, v10, v8

    if-gtz v4, :cond_5

    .line 2242125
    add-int/lit8 v4, v6, 0x1

    move v5, v6

    move v6, v4

    goto :goto_3

    .line 2242126
    :cond_5
    invoke-static {p0, v5, v0}, LX/FSZ;->a(LX/FSZ;ILjava/util/GregorianCalendar;)I
    :try_end_1
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v4

    goto :goto_1

    .line 2242127
    :catch_1
    move-exception v4

    .line 2242128
    sget-object v6, LX/FSZ;->b:Ljava/lang/String;

    const-string v7, "IndexOutOfBoundsException in getNextNumStories."

    invoke-static {v6, v7, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v4, v5

    goto :goto_1

    .line 2242129
    :cond_6
    iget-object v4, p0, LX/FSZ;->h:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 2242130
    const/4 v4, 0x2

    if-ne v5, v4, :cond_9

    const/4 v4, 0x1

    :goto_4
    move v4, v4

    .line 2242131
    if-nez v4, :cond_7

    move v4, v5

    .line 2242132
    goto :goto_2

    .line 2242133
    :cond_7
    iget-object v4, p0, LX/FSZ;->f:Ljava/util/ArrayList;

    if-eqz v4, :cond_8

    iget-object v4, p0, LX/FSZ;->f:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_8

    invoke-static {p0}, LX/FSZ;->e(LX/FSZ;)Ljava/util/GregorianCalendar;

    move-result-object v4

    invoke-static {v4}, LX/FSZ;->c(Ljava/util/GregorianCalendar;)J

    move-result-wide v8

    iget-object v4, p0, LX/FSZ;->f:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v4, v8, v10

    if-nez v4, :cond_8

    move v4, v5

    .line 2242134
    goto :goto_2

    :cond_8
    move v4, v6

    .line 2242135
    goto :goto_2

    :cond_9
    const/4 v4, 0x0

    goto :goto_4
.end method
