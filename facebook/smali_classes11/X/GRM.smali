.class public LX/GRM;
.super LX/1Cv;
.source ""


# instance fields
.field public final a:LX/GRX;

.field private final b:LX/GTB;


# direct methods
.method public constructor <init>(LX/GRX;LX/GTB;)V
    .locals 0
    .param p1    # LX/GRX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2351480
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2351481
    iput-object p1, p0, LX/GRM;->a:LX/GRX;

    .line 2351482
    iput-object p2, p0, LX/GRM;->b:LX/GTB;

    .line 2351483
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2351484
    new-instance v0, LX/GSz;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/GSz;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 2351485
    check-cast p2, LX/GRZ;

    .line 2351486
    iget-object v0, p0, LX/GRM;->b:LX/GTB;

    check-cast p3, LX/GSz;

    invoke-virtual {v0, p2, p3}, LX/GTB;->a(LX/GRZ;LX/GSz;)V

    .line 2351487
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2351488
    iget-object v0, p0, LX/GRM;->a:LX/GRX;

    invoke-virtual {v0}, LX/GRX;->a()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2351489
    iget-object v0, p0, LX/GRM;->a:LX/GRX;

    .line 2351490
    iget-object p0, v0, LX/GRX;->a:Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/GRZ;

    move-object v0, p0

    .line 2351491
    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2351492
    int-to-long v0, p1

    return-wide v0
.end method
