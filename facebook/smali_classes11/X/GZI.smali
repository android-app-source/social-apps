.class public LX/GZI;
.super LX/0gG;
.source ""


# instance fields
.field private a:LX/03V;

.field private final b:LX/7j6;

.field private final c:LX/0Zb;

.field public d:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

.field public e:LX/7iP;


# direct methods
.method public constructor <init>(LX/7j6;LX/03V;LX/0Zb;)V
    .locals 0

    .prologue
    .line 2366355
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 2366356
    iput-object p1, p0, LX/GZI;->b:LX/7j6;

    .line 2366357
    iput-object p2, p0, LX/GZI;->a:LX/03V;

    .line 2366358
    iput-object p3, p0, LX/GZI;->c:LX/0Zb;

    .line 2366359
    return-void
.end method

.method private b(I)Landroid/net/Uri;
    .locals 6

    .prologue
    const v5, 0x6a396f09

    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2366340
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v0

    if-le p1, v0, :cond_1

    :cond_0
    move-object v0, v3

    .line 2366341
    :goto_0
    return-object v0

    .line 2366342
    :cond_1
    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v0

    if-le v0, p1, :cond_4

    .line 2366343
    iget-object v0, p0, LX/GZI;->d:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v1, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, LX/3Sa;->f()LX/2uF;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2366344
    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    if-eqz v0, :cond_7

    .line 2366345
    iget-object v0, p0, LX/GZI;->d:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v4, v0, v1, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    invoke-virtual {v0, p1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2366346
    const-class v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v1, v4, v2, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->n()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel$ProductImageLargeModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 2366347
    const-class v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v1, v4, v2, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->n()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel$ProductImageLargeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel$ProductImageLargeModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2366348
    :goto_4
    if-nez v0, :cond_6

    .line 2366349
    iget-object v0, p0, LX/GZI;->a:LX/03V;

    const-string v1, "commerce_storefront_limage_adapter"

    const-string v2, "getHScrollLargeImageUri: image uri is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v3

    .line 2366350
    goto :goto_0

    .line 2366351
    :cond_2
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_1

    :cond_3
    move v0, v2

    .line 2366352
    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_2

    .line 2366353
    :cond_5
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_3

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2366354
    :cond_6
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    move-object v0, v3

    goto :goto_4
.end method

.method private e(I)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v4, 0x0

    const v6, 0x6a396f09

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2366323
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v0

    if-le p1, v0, :cond_2

    :cond_0
    move-object v0, v4

    .line 2366324
    :cond_1
    :goto_0
    return-object v0

    .line 2366325
    :cond_2
    const-string v3, ""

    .line 2366326
    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v0

    if-le v0, p1, :cond_5

    .line 2366327
    iget-object v0, p0, LX/GZI;->d:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v5, v0, v1, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, LX/3Sa;->f()LX/2uF;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2366328
    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_7

    .line 2366329
    iget-object v0, p0, LX/GZI;->d:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v5, v0, v1, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    invoke-virtual {v0, p1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    .line 2366330
    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2366331
    const-class v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v1, v5, v2, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 2366332
    const-class v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v1, v5, v2, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 2366333
    :goto_4
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2366334
    iget-object v0, p0, LX/GZI;->a:LX/03V;

    const-string v1, "commerce_storefront_limage_adapter"

    const-string v2, "getHScrollItemDescription: item description is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v4

    .line 2366335
    goto :goto_0

    .line 2366336
    :cond_3
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_1

    :cond_4
    move v0, v2

    .line 2366337
    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_2

    .line 2366338
    :cond_6
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_3

    .line 2366339
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_7
    move-object v0, v3

    goto :goto_4
.end method

.method private f(I)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v4, 0x0

    const v6, 0x6a396f09

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2366307
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v0

    if-le p1, v0, :cond_2

    :cond_0
    move-object v0, v4

    .line 2366308
    :cond_1
    :goto_0
    return-object v0

    .line 2366309
    :cond_2
    const-string v3, ""

    .line 2366310
    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v0

    if-le v0, p1, :cond_5

    .line 2366311
    iget-object v0, p0, LX/GZI;->d:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v5, v0, v1, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, LX/3Sa;->f()LX/2uF;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2366312
    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_7

    .line 2366313
    iget-object v0, p0, LX/GZI;->d:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v5, v0, v1, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    invoke-virtual {v0, p1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2366314
    const-class v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v1, v5, v2, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->j()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 2366315
    const-class v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v1, v5, v2, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->j()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v0

    .line 2366316
    invoke-static {v0}, LX/7j4;->a(Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;)Ljava/lang/String;

    move-result-object v0

    .line 2366317
    :goto_4
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2366318
    iget-object v0, p0, LX/GZI;->a:LX/03V;

    const-string v1, "commerce_storefront_limage_adapter"

    const-string v2, "getHscrollItemPrice: item price is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v4

    .line 2366319
    goto :goto_0

    .line 2366320
    :cond_3
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_1

    :cond_4
    move v0, v2

    .line 2366321
    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_2

    .line 2366322
    :cond_6
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_3

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_7
    move-object v0, v3

    goto :goto_4
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 2366306
    const/4 v0, -0x2

    return v0
.end method

.method public final a(Ljava/lang/String;)I
    .locals 7
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, -0x1

    .line 2366265
    if-nez p1, :cond_1

    move v0, v1

    .line 2366266
    :cond_0
    :goto_0
    return v0

    .line 2366267
    :cond_1
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p0}, LX/0gG;->b()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 2366268
    const v6, 0x6a396f09

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2366269
    iget-object v2, p0, LX/GZI;->d:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    if-eqz v2, :cond_5

    .line 2366270
    iget-object v2, p0, LX/GZI;->d:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v2}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;->j()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2366271
    if-eqz v2, :cond_4

    move v2, v3

    :goto_2
    if-eqz v2, :cond_8

    .line 2366272
    iget-object v2, p0, LX/GZI;->d:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v2}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;->j()LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-static {v5, v2, v3, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    .line 2366273
    if-eqz v2, :cond_6

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_3
    invoke-virtual {v2}, LX/39O;->a()Z

    move-result v2

    if-nez v2, :cond_7

    move v2, v3

    :goto_4
    if-eqz v2, :cond_a

    .line 2366274
    iget-object v2, p0, LX/GZI;->d:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v2}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;->j()LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-static {v5, v2, v3, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_5
    invoke-virtual {v2, v0}, LX/2uF;->a(I)LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const-class v5, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v3, v2, v4, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    .line 2366275
    :goto_6
    move-object v2, v2

    .line 2366276
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2366277
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 2366278
    goto :goto_0

    :cond_4
    move v2, v4

    .line 2366279
    goto :goto_2

    :cond_5
    move v2, v4

    goto :goto_2

    :cond_6
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto :goto_3

    :cond_7
    move v2, v4

    goto :goto_4

    :cond_8
    move v2, v4

    goto :goto_4

    .line 2366280
    :cond_9
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto :goto_5

    .line 2366281
    :cond_a
    const/4 v2, 0x0

    goto :goto_6
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v1, -0x1

    .line 2366292
    new-instance v7, Lcom/facebook/commerce/storefront/ui/StorefrontLargeHScrollCorexItem;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v7, v0}, Lcom/facebook/commerce/storefront/ui/StorefrontLargeHScrollCorexItem;-><init>(Landroid/content/Context;)V

    .line 2366293
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 2366294
    invoke-virtual {v7, v0}, Lcom/facebook/commerce/storefront/ui/StorefrontLargeHScrollCorexItem;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2366295
    invoke-direct {p0, p2}, LX/GZI;->b(I)Landroid/net/Uri;

    move-result-object v0

    .line 2366296
    iget-object v1, v7, Lcom/facebook/commerce/storefront/ui/StorefrontLargeHScrollCorexItem;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v2, Lcom/facebook/commerce/storefront/ui/StorefrontLargeHScrollCorexItem;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v1, v0, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2366297
    invoke-direct {p0, p2}, LX/GZI;->e(I)Ljava/lang/String;

    move-result-object v0

    .line 2366298
    iget-object v1, v7, Lcom/facebook/commerce/storefront/ui/StorefrontLargeHScrollCorexItem;->d:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2366299
    invoke-direct {p0, p2}, LX/GZI;->f(I)Ljava/lang/String;

    move-result-object v0

    .line 2366300
    iget-object v1, v7, Lcom/facebook/commerce/storefront/ui/StorefrontLargeHScrollCorexItem;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2366301
    invoke-virtual {p1, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2366302
    iget-object v0, p0, LX/GZI;->d:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x1

    const v3, 0x6a396f09

    invoke-static {v1, v0, v2, v3}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    invoke-virtual {v0, p2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2366303
    new-instance v0, LX/GZP;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/GZI;->b:LX/7j6;

    const/4 v5, 0x0

    const-class v6, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v3, v4, v5, v6}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    check-cast v3, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;

    invoke-virtual {v3}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceProductItemModel;->l()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/GZI;->c:LX/0Zb;

    sget-object v5, LX/7iO;->STOREFRONT_BANNER:LX/7iO;

    iget-object v6, p0, LX/GZI;->e:LX/7iP;

    invoke-direct/range {v0 .. v6}, LX/GZP;-><init>(Landroid/content/Context;LX/7j6;Ljava/lang/String;LX/0Zb;LX/7iO;LX/7iP;)V

    invoke-virtual {v7, v0}, Lcom/facebook/commerce/storefront/ui/StorefrontLargeHScrollCorexItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2366304
    return-object v7

    .line 2366305
    :cond_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 2366290
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2366291
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2366289
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 5

    .prologue
    const v4, 0x6a396f09

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2366282
    iget-object v0, p0, LX/GZI;->d:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    return v0

    .line 2366283
    :cond_0
    iget-object v0, p0, LX/GZI;->d:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2366284
    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 2366285
    :cond_2
    iget-object v0, p0, LX/GZI;->d:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2366286
    if-eqz v0, :cond_3

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    goto :goto_1

    :cond_3
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_3

    .line 2366287
    :cond_4
    iget-object v0, p0, LX/GZI;->d:Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0, v2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2366288
    if-eqz v0, :cond_5

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_4
    invoke-virtual {v0}, LX/39O;->c()I

    move-result v0

    goto :goto_2

    :cond_5
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_4
.end method
