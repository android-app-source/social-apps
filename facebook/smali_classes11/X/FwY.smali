.class public LX/FwY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final a:LX/17W;

.field public final b:LX/0Uh;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BQ9;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Lcom/facebook/content/SecureContextHelper;

.field public final e:LX/8p8;

.field public f:J

.field public g:LX/9lQ;


# direct methods
.method public constructor <init>(LX/17W;LX/0Uh;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/8p8;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/17W;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Or",
            "<",
            "LX/BQ9;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/8p8;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2303287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2303288
    iput-object p1, p0, LX/FwY;->a:LX/17W;

    .line 2303289
    iput-object p2, p0, LX/FwY;->b:LX/0Uh;

    .line 2303290
    iput-object p3, p0, LX/FwY;->c:LX/0Or;

    .line 2303291
    iput-object p4, p0, LX/FwY;->d:Lcom/facebook/content/SecureContextHelper;

    .line 2303292
    iput-object p5, p0, LX/FwY;->e:LX/8p8;

    .line 2303293
    return-void
.end method

.method public static a(LX/0QB;)LX/FwY;
    .locals 9

    .prologue
    .line 2303273
    const-class v1, LX/FwY;

    monitor-enter v1

    .line 2303274
    :try_start_0
    sget-object v0, LX/FwY;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2303275
    sput-object v2, LX/FwY;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2303276
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2303277
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2303278
    new-instance v3, LX/FwY;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v4

    check-cast v4, LX/17W;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    const/16 v6, 0x369b

    invoke-static {v0, v6}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/8p8;->b(LX/0QB;)LX/8p8;

    move-result-object v8

    check-cast v8, LX/8p8;

    invoke-direct/range {v3 .. v8}, LX/FwY;-><init>(LX/17W;LX/0Uh;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/8p8;)V

    .line 2303279
    move-object v0, v3

    .line 2303280
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2303281
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FwY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2303282
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2303283
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/FwY;Landroid/content/Context;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;)Ljava/lang/CharSequence;
    .locals 2
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 2303294
    invoke-virtual {p2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->c()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel$LinkTypeModel;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 2303295
    :goto_0
    invoke-static {p2}, LX/FwY;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2303296
    invoke-virtual {p2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 2303297
    :goto_1
    return-object v0

    .line 2303298
    :cond_0
    invoke-virtual {p2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->c()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel$LinkTypeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel$LinkTypeModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2303299
    :cond_1
    invoke-virtual {p2}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 2303300
    if-nez v0, :cond_3

    if-nez v1, :cond_3

    .line 2303301
    const/4 v1, 0x0

    .line 2303302
    :cond_2
    :goto_2
    move-object v0, v1

    .line 2303303
    goto :goto_1

    .line 2303304
    :cond_3
    if-eqz v0, :cond_2

    .line 2303305
    new-instance p0, Landroid/text/SpannableStringBuilder;

    invoke-direct {p0, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 2303306
    if-nez v1, :cond_4

    .line 2303307
    const/4 p2, 0x0

    invoke-static {p0, p2, p1}, LX/FwY;->a(Landroid/text/Spannable;ILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_2

    .line 2303308
    :cond_4
    const-string p2, ": "

    invoke-virtual {p0, p2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object p2

    invoke-virtual {p2, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2303309
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result p2

    invoke-static {p0, p2, p1}, LX/FwY;->a(Landroid/text/Spannable;ILandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_2
.end method

.method public static a(Landroid/text/Spannable;ILandroid/content/Context;)Ljava/lang/CharSequence;
    .locals 4

    .prologue
    .line 2303285
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    const v1, 0x7f0a00e6

    invoke-static {p2, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v1, 0x0

    invoke-interface {p0}, Landroid/text/Spannable;->length()I

    move-result v2

    sub-int/2addr v2, p1

    const/16 v3, 0x21

    invoke-interface {p0, v0, v1, v2, v3}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2303286
    return-object p0
.end method

.method public static a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;)Z
    .locals 1

    .prologue
    .line 2303284
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->b()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ExternalLinkModel;->b()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
