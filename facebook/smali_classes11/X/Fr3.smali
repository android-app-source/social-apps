.class public LX/Fr3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/D3l;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/D3l",
        "<",
        "LX/Fr7;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/Fsr;


# direct methods
.method public constructor <init>(LX/Fsr;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2294969
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2294970
    iput-object p1, p0, LX/Fr3;->a:LX/Fsr;

    .line 2294971
    return-void
.end method

.method public static a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;)Ljava/lang/Integer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2294966
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->cg_()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel$BadgeCountModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2294967
    const/4 v0, 0x0

    .line 2294968
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->cg_()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel$BadgeCountModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel$BadgeCountModel;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2294972
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 2294973
    :goto_0
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->a()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->a()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_4

    const v3, 0x25d6af

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->a()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    if-ne v3, v4, :cond_4

    move v3, v1

    .line 2294974
    :goto_1
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->COMPOSER:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->MESSAGE:Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->b()Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/graphql/enums/GraphQLTimelineContextListTargetType;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_0
    move v4, v1

    .line 2294975
    :goto_2
    if-nez v0, :cond_1

    if-nez v3, :cond_1

    if-eqz v4, :cond_2

    :cond_1
    move v2, v1

    :cond_2
    return v2

    :cond_3
    move v0, v2

    .line 2294976
    goto :goto_0

    :cond_4
    move v3, v2

    .line 2294977
    goto :goto_1

    :cond_5
    move v4, v2

    .line 2294978
    goto :goto_2
.end method

.method public static c(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;)Landroid/net/Uri;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2294963
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->cf_()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->cf_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2294964
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->cf_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2294965
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;)V
    .locals 3

    .prologue
    .line 2294958
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 2294959
    instance-of v1, v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    if-nez v1, :cond_0

    .line 2294960
    :goto_0
    return-void

    .line 2294961
    :cond_0
    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    .line 2294962
    iget-object v1, p0, LX/Fr3;->a:LX/Fsr;

    invoke-virtual {v1}, LX/Fsr;->r()LX/FrC;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LX/FrC;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;Z)V

    goto :goto_0
.end method

.method public a(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;LX/Fr7;)Z
    .locals 8

    .prologue
    const/4 v6, 0x1

    .line 2294929
    iget-object v0, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    if-nez v0, :cond_0

    .line 2294930
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setVisibility(I)V

    .line 2294931
    const/4 v0, 0x0

    .line 2294932
    :goto_0
    return v0

    .line 2294933
    :cond_0
    iput-object p0, p1, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->b:LX/D3l;

    .line 2294934
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 2294935
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0e87

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2294936
    const v0, 0x7f0b00b2

    invoke-virtual {v7, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    .line 2294937
    invoke-virtual {p1, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setMinimumHeight(I)V

    .line 2294938
    iget-object v0, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    const/4 v2, 0x0

    .line 2294939
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->cf_()LX/1Fb;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2294940
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->cf_()LX/1Fb;

    move-result-object v3

    invoke-interface {v3}, LX/1Fb;->c()I

    move-result v3

    sub-int v3, v1, v3

    div-int/lit8 v3, v3, 0x2

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 2294941
    :cond_1
    move v2, v2

    .line 2294942
    iget-object v0, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    const/4 v3, 0x0

    .line 2294943
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->cf_()LX/1Fb;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 2294944
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->cf_()LX/1Fb;

    move-result-object v4

    invoke-interface {v4}, LX/1Fb;->a()I

    move-result v4

    sub-int v4, v1, v4

    div-int/lit8 v4, v4, 0x2

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 2294945
    :cond_2
    move v3, v3

    .line 2294946
    iget-object v0, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    invoke-static {v0}, LX/Fr3;->c(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;)Landroid/net/Uri;

    move-result-object v4

    const-string v5, "timeline"

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(IIILandroid/net/Uri;Ljava/lang/String;)V

    .line 2294947
    iget-object v0, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->d()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->d()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v0

    :goto_1
    const/4 v1, 0x2

    const v2, 0x7f0b0050

    invoke-static {v7, v2}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v2

    invoke-virtual {p1, v0, v1, v2}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/CharSequence;II)V

    .line 2294948
    iget-object v0, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    invoke-static {v0}, LX/Fr3;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/Integer;)V

    .line 2294949
    iget-object v0, p2, LX/Fr7;->b:LX/G4n;

    .line 2294950
    sget-object v1, LX/G4n;->BEGIN:LX/G4n;

    if-eq v0, v1, :cond_3

    sget-object v1, LX/G4n;->MIDDLE:LX/G4n;

    if-ne v0, v1, :cond_5

    .line 2294951
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0e86

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2294952
    :goto_2
    move v0, v1

    .line 2294953
    iput v0, p1, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->k:I

    .line 2294954
    iput-boolean v6, p1, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->l:Z

    .line 2294955
    iget-object v0, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    invoke-static {v0}, LX/Fr3;->b(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;)Z

    move-result v0

    iget-object v1, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(ZLjava/lang/Object;)V

    move v0, v6

    .line 2294956
    goto/16 :goto_0

    .line 2294957
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_2
.end method
