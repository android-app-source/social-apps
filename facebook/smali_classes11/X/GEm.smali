.class public LX/GEm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GEW",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/GIw;


# direct methods
.method public constructor <init>(LX/GIw;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2332075
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2332076
    iput-object p1, p0, LX/GEm;->a:LX/GIw;

    .line 2332077
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2332078
    const v0, 0x7f03005c

    return v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2332071
    invoke-static {p1}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2332072
    :cond_0
    :goto_0
    return v0

    .line 2332073
    :cond_1
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    .line 2332074
    sget-object v2, LX/GGB;->INACTIVE:LX/GGB;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/GGB;->PAUSED:LX/GGB;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/GGB;->EXTENDABLE:LX/GGB;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/GGB;->FINISHED:LX/GGB;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/GGB;->PENDING:LX/GGB;

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()LX/GHg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesViewController",
            "<",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;",
            "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2332070
    iget-object v0, p0, LX/GEm;->a:LX/GIw;

    return-object v0
.end method

.method public final c()LX/8wK;
    .locals 1

    .prologue
    .line 2332069
    sget-object v0, LX/8wK;->FOOTER:LX/8wK;

    return-object v0
.end method
