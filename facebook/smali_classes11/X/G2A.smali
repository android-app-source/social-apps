.class public final LX/G2A;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/protiles/model/ProtileModel;

.field public final synthetic b:LX/1Pq;

.field public final synthetic c:Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;Lcom/facebook/timeline/protiles/model/ProtileModel;LX/1Pq;)V
    .locals 0

    .prologue
    .line 2313639
    iput-object p1, p0, LX/G2A;->c:Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;

    iput-object p2, p0, LX/G2A;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    iput-object p3, p0, LX/G2A;->b:LX/1Pq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x2

    const v1, -0x31d35be4

    invoke-static {v0, v3, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2313640
    iget-object v1, p0, LX/G2A;->c:Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;

    iget-object v1, v1, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;->f:LX/G2X;

    iget-object v2, p0, LX/G2A;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    const/4 v4, 0x0

    .line 2313641
    invoke-virtual {v1, v2}, LX/G2X;->c(Lcom/facebook/timeline/protiles/model/ProtileModel;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 2313642
    :goto_0
    move v1, v4

    .line 2313643
    if-eqz v1, :cond_1

    .line 2313644
    iget-object v1, p0, LX/G2A;->c:Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;

    iget-object v1, v1, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;->f:LX/G2X;

    iget-object v2, p0, LX/G2A;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 2313645
    invoke-virtual {v2}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-ne v5, v6, :cond_4

    .line 2313646
    iput-boolean v3, v1, LX/G2X;->d:Z

    .line 2313647
    :cond_0
    :goto_1
    iget-object v7, v1, LX/G2X;->b:LX/0SG;

    invoke-interface {v7}, LX/0SG;->a()J

    move-result-wide v7

    invoke-virtual {v2, v7, v8}, Lcom/facebook/graphql/model/BaseFeedUnit;->a(J)V

    .line 2313648
    iget-object v1, p0, LX/G2A;->b:LX/1Pq;

    invoke-interface {v1}, LX/1Pq;->iN_()V

    .line 2313649
    :goto_2
    const v1, 0x4809aa25

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2313650
    :cond_1
    iget-object v1, p0, LX/G2A;->c:Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;

    iget-object v1, v1, Lcom/facebook/timeline/protiles/rows/ProtilesHeaderPartDefinition;->b:LX/1K9;

    new-instance v2, LX/G1D;

    sget-object v3, LX/G1C;->CLICK_HEADER:LX/G1C;

    iget-object v4, p0, LX/G2A;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    invoke-direct {v2, v3, v4}, LX/G1D;-><init>(LX/G1C;Lcom/facebook/timeline/protiles/model/ProtileModel;)V

    invoke-virtual {v1, v2}, LX/1K9;->a(LX/1KJ;)V

    goto :goto_2

    .line 2313651
    :cond_2
    iget-boolean v5, v1, LX/G2X;->c:Z

    if-eqz v5, :cond_3

    .line 2313652
    iget-object v5, v1, LX/G2X;->a:LX/0ad;

    sget-short p1, LX/0wf;->q:S

    invoke-interface {v5, p1, v4}, LX/0ad;->a(SZ)Z

    move-result v4

    goto :goto_0

    .line 2313653
    :cond_3
    iget-object v5, v1, LX/G2X;->a:LX/0ad;

    sget-short p1, LX/0wf;->m:S

    invoke-interface {v5, p1, v4}, LX/0ad;->a(SZ)Z

    move-result v4

    goto :goto_0

    .line 2313654
    :cond_4
    invoke-virtual {v2}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-ne v5, v6, :cond_0

    .line 2313655
    iput-boolean v3, v1, LX/G2X;->e:Z

    goto :goto_1
.end method
