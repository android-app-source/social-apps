.class public LX/FF4;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/FF7;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/FOK;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/FFD;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/FF9;


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2216853
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2216854
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2216855
    iput-object v0, p0, LX/FF4;->b:LX/0Px;

    .line 2216856
    return-void
.end method

.method private e(I)LX/FFD;
    .locals 1

    .prologue
    .line 2216857
    iget-object v0, p0, LX/FF4;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FFD;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 2216858
    invoke-static {p2}, LX/FFF;->fromOridnal(I)LX/FFF;

    move-result-object v0

    .line 2216859
    iget-object v1, p0, LX/FF4;->a:LX/FOK;

    invoke-static {v0, p1, v1}, LX/FF7;->a(LX/FFF;Landroid/view/ViewGroup;LX/FOK;)LX/FF7;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 2

    .prologue
    .line 2216860
    check-cast p1, LX/FF7;

    .line 2216861
    invoke-direct {p0, p2}, LX/FF4;->e(I)LX/FFD;

    move-result-object v0

    iget-object v1, p0, LX/FF4;->c:LX/FF9;

    invoke-virtual {p1, p2, v0, v1}, LX/FF7;->a(ILX/FFD;LX/FF9;)V

    .line 2216862
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2216863
    invoke-direct {p0, p1}, LX/FF4;->e(I)LX/FFD;

    move-result-object v0

    invoke-interface {v0}, LX/FFD;->a()LX/FFF;

    move-result-object v0

    invoke-virtual {v0}, LX/FFF;->ordinal()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2216864
    iget-object v0, p0, LX/FF4;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
