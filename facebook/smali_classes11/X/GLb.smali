.class public LX/GLb;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:LX/GG6;

.field public c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

.field public d:LX/2U3;

.field public e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

.field private f:LX/GDy;

.field private g:LX/0Uh;

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/1Ck;

.field public j:I

.field private k:I

.field public l:I

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Z


# direct methods
.method public constructor <init>(LX/0Uh;LX/1Ck;LX/GDy;LX/GG6;LX/2U3;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2343202
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2343203
    iput-boolean v1, p0, LX/GLb;->m:Z

    .line 2343204
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GLb;->n:Z

    .line 2343205
    iput-boolean v1, p0, LX/GLb;->o:Z

    .line 2343206
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GLb;->h:Ljava/util/List;

    .line 2343207
    iput v1, p0, LX/GLb;->l:I

    .line 2343208
    iput-object p1, p0, LX/GLb;->g:LX/0Uh;

    .line 2343209
    iput-object p2, p0, LX/GLb;->i:LX/1Ck;

    .line 2343210
    iput-object p3, p0, LX/GLb;->f:LX/GDy;

    .line 2343211
    iput-object p4, p0, LX/GLb;->b:LX/GG6;

    .line 2343212
    iput-object p5, p0, LX/GLb;->d:LX/2U3;

    .line 2343213
    const/4 v0, 0x2

    iput v0, p0, LX/GLb;->k:I

    .line 2343214
    const/4 v0, 0x4

    iput v0, p0, LX/GLb;->j:I

    .line 2343215
    return-void
.end method

.method public static synthetic a(LX/GLb;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;I)Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;
    .locals 1

    .prologue
    .line 2343201
    invoke-direct {p0, p1, p2}, LX/GLb;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;I)Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;I)Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;
    .locals 4

    .prologue
    .line 2343099
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2343100
    invoke-direct {p0, p1}, LX/GLb;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2343101
    invoke-direct {p0, p1}, LX/GLb;->c(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;)Ljava/util/ArrayList;

    move-result-object v0

    move-object v1, v0

    .line 2343102
    :goto_0
    iget-object v0, p0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    .line 2343103
    sget-object v2, LX/GGB;->INACTIVE:LX/GGB;

    if-eq v0, v2, :cond_0

    sget-object v2, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    if-ne v0, v2, :cond_2

    :cond_0
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->m()LX/2uF;

    move-result-object v0

    :goto_1
    check-cast v0, LX/3Sb;

    .line 2343104
    iget-object v2, p0, LX/GLb;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1, v0, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->a(Ljava/lang/String;Ljava/util/List;LX/3Sb;I)Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;

    move-result-object v0

    .line 2343105
    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->setTag(Ljava/lang/Object;)V

    .line 2343106
    iget-boolean v1, p0, LX/GLb;->p:Z

    if-nez v1, :cond_3

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2343107
    :cond_1
    :goto_2
    return-object v0

    .line 2343108
    :cond_2
    new-instance v0, LX/4AD;

    invoke-direct {v0}, LX/4AD;-><init>()V

    goto :goto_1

    .line 2343109
    :cond_3
    iget-boolean v1, p0, LX/GLb;->m:Z

    if-eqz v1, :cond_1

    .line 2343110
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->l()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2343111
    new-instance v1, LX/GLX;

    invoke-direct {v1, p0, p1}, LX/GLX;-><init>(LX/GLb;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->setButtonOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2343112
    iget-object v1, p0, LX/GLb;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00f7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->setIconColor(I)V

    goto :goto_2

    .line 2343113
    :cond_4
    new-instance v1, LX/GLY;

    invoke-direct {v1, p0}, LX/GLY;-><init>(LX/GLb;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->setButtonOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2343114
    iget-object v1, p0, LX/GLb;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00fe

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->setIconColor(I)V

    .line 2343115
    iget-object v1, p0, LX/GLb;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080b87

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2343116
    iput-object v1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->f:Ljava/lang/String;

    .line 2343117
    goto :goto_2

    :cond_5
    move-object v1, v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;)Z
    .locals 2

    .prologue
    .line 2343199
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/GLb;ILandroid/widget/RadioGroup;)V
    .locals 4

    .prologue
    .line 2343216
    invoke-virtual {p2, p1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2343217
    instance-of v1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;

    if-eqz v1, :cond_0

    .line 2343218
    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    .line 2343219
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v1

    .line 2343220
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v2

    .line 2343221
    iget-object v3, p0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v3}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v3

    invoke-virtual {v3, v2, v1}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;)V

    .line 2343222
    iget-object v1, p0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->o()LX/0Px;

    move-result-object v2

    .line 2343223
    iput-object v2, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->l:LX/0Px;

    .line 2343224
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2343225
    new-instance v2, LX/GFx;

    invoke-direct {v2, v0}, LX/GFx;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;)V

    invoke-virtual {v1, v2}, LX/GCE;->a(LX/8wN;)V

    .line 2343226
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2343227
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2343228
    iget-object v1, p0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-virtual {v0, v1}, LX/GG3;->q(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2343229
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/GLb;
    .locals 7

    .prologue
    .line 2343230
    new-instance v0, LX/GLb;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v1

    check-cast v1, LX/0Uh;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v2

    check-cast v2, LX/1Ck;

    .line 2343231
    new-instance v6, LX/GDy;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/2U4;->a(LX/0QB;)LX/2U4;

    move-result-object v4

    check-cast v4, LX/2U4;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v5

    check-cast v5, LX/2U3;

    invoke-direct {v6, v3, v4, v5}, LX/GDy;-><init>(LX/0tX;LX/2U4;LX/2U3;)V

    .line 2343232
    move-object v3, v6

    .line 2343233
    check-cast v3, LX/GDy;

    invoke-static {p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v4

    check-cast v4, LX/GG6;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v5

    check-cast v5, LX/2U3;

    invoke-direct/range {v0 .. v5}, LX/GLb;-><init>(LX/0Uh;LX/1Ck;LX/GDy;LX/GG6;LX/2U3;)V

    .line 2343234
    return-object v0
.end method

.method private b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;)Z
    .locals 2

    .prologue
    .line 2343235
    sget-object v0, LX/GLa;->a:[I

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2343236
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2343237
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 2343238
    :pswitch_1
    iget-boolean v0, p0, LX/GLb;->p:Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private c(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;)Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2343239
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->FANS:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->GROUPER:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-ne v0, v1, :cond_c

    .line 2343240
    :cond_0
    new-instance v0, LX/GGE;

    iget-object v1, p0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    invoke-direct {v0, v1}, LX/GGE;-><init>(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->o()LX/0Px;

    move-result-object v1

    .line 2343241
    iput-object v1, v0, LX/GGE;->l:LX/0Px;

    .line 2343242
    move-object v0, v0

    .line 2343243
    invoke-virtual {v0}, LX/GGE;->a()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    iget-object v1, p0, LX/GLb;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2343244
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 2343245
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2343246
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->l:LX/0Px;

    move-object v2, v2

    .line 2343247
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->LOCATIONS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    invoke-virtual {v2, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2343248
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 2343249
    sget-object v2, LX/GG4;->e:[I

    .line 2343250
    iget-object v3, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    move-object v3, v3

    .line 2343251
    invoke-virtual {v3}, LX/GGG;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2343252
    :cond_1
    :goto_0
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_2

    .line 2343253
    const v2, 0x7f080aba

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2343254
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2343255
    :cond_2
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->l:LX/0Px;

    move-object v2, v2

    .line 2343256
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->AGE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    invoke-virtual {v2, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2343257
    const v2, 0x7f080a9e

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2343258
    iget v2, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->e:I

    move v2, v2

    .line 2343259
    const/16 v3, 0x41

    if-ne v2, v3, :cond_7

    const v2, 0x7f080b6c

    :goto_1
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 2343260
    iget v6, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->d:I

    move v6, v6

    .line 2343261
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    .line 2343262
    iget v6, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->e:I

    move v6, v6

    .line 2343263
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v4

    invoke-virtual {v7, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2343264
    :cond_3
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->l:LX/0Px;

    move-object v2, v2

    .line 2343265
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->GENDERS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    invoke-virtual {v2, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2343266
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->g:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    move-object v2, v2

    .line 2343267
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->ALL:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    if-eq v2, v3, :cond_4

    .line 2343268
    const v2, 0x7f080a89

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2343269
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->g:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    move-object v2, v2

    .line 2343270
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->MALE:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    if-ne v2, v3, :cond_8

    const v2, 0x7f080a8c

    :goto_2
    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2343271
    :cond_4
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->l:LX/0Px;

    move-object v2, v2

    .line 2343272
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;->INTERESTS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;

    invoke-virtual {v2, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 2343273
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    move-object v2, v2

    .line 2343274
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_b

    .line 2343275
    const v2, 0x7f080b88

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2343276
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 2343277
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    move-object v7, v2

    .line 2343278
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v9

    move v3, v4

    move v4, v5

    :goto_3
    if-ge v4, v9, :cond_a

    invoke-virtual {v7, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;

    .line 2343279
    if-eqz v3, :cond_9

    const-string v3, ""

    :goto_4
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2343280
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2343281
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v3, v5

    goto :goto_3

    .line 2343282
    :pswitch_0
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    move-object p0, v2

    .line 2343283
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result p1

    move v6, v5

    move v3, v4

    :goto_5
    if-ge v6, p1, :cond_1

    invoke-virtual {p0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2343284
    if-eqz v3, :cond_5

    const-string v3, ""

    :goto_6
    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2343285
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2343286
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v3, v5

    goto :goto_5

    .line 2343287
    :cond_5
    const-string v3, ", "

    goto :goto_6

    .line 2343288
    :pswitch_1
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v2, v2

    .line 2343289
    if-eqz v2, :cond_1

    .line 2343290
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v2, v2

    .line 2343291
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->c()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 2343292
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v2, v2

    .line 2343293
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->c()Ljava/lang/String;

    move-result-object v2

    :goto_7
    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :cond_6
    const v2, 0x7f080b36

    invoke-virtual {v7, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_7

    .line 2343294
    :cond_7
    const v2, 0x7f080b6d

    goto/16 :goto_1

    .line 2343295
    :cond_8
    const v2, 0x7f080a8b

    goto/16 :goto_2

    .line 2343296
    :cond_9
    const-string v3, ", "

    goto :goto_4

    .line 2343297
    :cond_a
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2343298
    :cond_b
    move-object v0, v8

    .line 2343299
    :goto_8
    return-object v0

    .line 2343300
    :cond_c
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2343301
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->q()LX/2uF;

    move-result-object v0

    .line 2343302
    invoke-interface {v0}, LX/3Sb;->b()LX/2sN;

    move-result-object v7

    :goto_9
    invoke-interface {v7}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v7}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2343303
    invoke-virtual {v1, v0, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 2343304
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2343305
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 2343306
    invoke-virtual {v1, v0, v6}, LX/15i;->o(II)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_d

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    :goto_a
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v9

    move v4, v5

    move v2, v6

    :goto_b
    if-ge v4, v9, :cond_f

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2343307
    if-eqz v2, :cond_e

    const-string v2, ""

    :goto_c
    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2343308
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v2, v5

    goto :goto_b

    .line 2343309
    :cond_d
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2343310
    move-object v1, v0

    goto :goto_a

    .line 2343311
    :cond_e
    const-string v2, ", "

    goto :goto_c

    .line 2343312
    :cond_f
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    :cond_10
    move-object v0, v3

    .line 2343313
    goto :goto_8

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private e()V
    .locals 2

    .prologue
    .line 2343314
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2343315
    new-instance v1, LX/GLT;

    invoke-direct {v1, p0}, LX/GLT;-><init>(LX/GLb;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2343316
    return-void
.end method

.method public static e(LX/GLb;Z)V
    .locals 1

    .prologue
    .line 2343317
    iget-object v0, p0, LX/GLb;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    if-eqz v0, :cond_0

    .line 2343318
    iget-object v0, p0, LX/GLb;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Z)V

    .line 2343319
    :cond_0
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 2343320
    iget-object v0, p0, LX/GLb;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    iget-object v1, p0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    .line 2343321
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v1, v2

    .line 2343322
    iget-object v2, p0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v2

    .line 2343323
    iget-object p0, v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v2, p0

    .line 2343324
    invoke-virtual {v0, v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->a(Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;Ljava/lang/String;)V

    .line 2343325
    return-void
.end method

.method public static synthetic g(LX/GLb;)I
    .locals 2

    .prologue
    .line 2343200
    iget v0, p0, LX/GLb;->l:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, LX/GLb;->l:I

    return v0
.end method

.method public static g(LX/GLb;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2343193
    iget-object v0, p0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->w()I

    move-result v3

    iget-boolean v0, p0, LX/GLb;->n:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    sub-int v0, v3, v0

    .line 2343194
    iget-object v3, p0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v3}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->w()I

    move-result v3

    if-le v3, v2, :cond_1

    iget-object v2, p0, LX/GLb;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v2, v0, :cond_1

    .line 2343195
    :goto_1
    iget-object v0, p0, LX/GLb;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->setMoreOptionsViewVisibility(I)V

    .line 2343196
    return-void

    :cond_0
    move v0, v2

    .line 2343197
    goto :goto_0

    .line 2343198
    :cond_1
    const/16 v1, 0x8

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2343187
    invoke-super {p0}, LX/GHg;->a()V

    .line 2343188
    const-string v0, "fetch_first_batch_audiences_task_key"

    invoke-virtual {p0, v0}, LX/GLb;->a(Ljava/lang/String;)V

    .line 2343189
    const-string v0, "fetch_unified_audiences_task_key"

    invoke-virtual {p0, v0}, LX/GLb;->a(Ljava/lang/String;)V

    .line 2343190
    const/4 v0, 0x0

    iput-object v0, p0, LX/GLb;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    .line 2343191
    const/4 v0, 0x0

    iput v0, p0, LX/GLb;->l:I

    .line 2343192
    return-void
.end method

.method public final a(II)V
    .locals 0

    .prologue
    .line 2343184
    iput p1, p0, LX/GLb;->k:I

    .line 2343185
    iput p2, p0, LX/GLb;->j:I

    .line 2343186
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2343183
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    invoke-virtual {p0, p1, p2}, LX/GLb;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2343181
    iput-object p1, p0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2343182
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 3
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2343172
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2343173
    iput-object p1, p0, LX/GLb;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    .line 2343174
    iput-object p2, p0, LX/GLb;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2343175
    iget-object v0, p0, LX/GLb;->g:LX/0Uh;

    const/16 v1, 0x2a2

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2343176
    :goto_0
    return-void

    .line 2343177
    :cond_0
    iget-object v0, p0, LX/GLb;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    new-instance v1, LX/GLS;

    invoke-direct {v1, p0}, LX/GLS;-><init>(LX/GLb;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->setOnCheckChangedListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    .line 2343178
    iget-object v0, p0, LX/GLb;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->setVisibility(I)V

    .line 2343179
    invoke-virtual {p0}, LX/GLb;->b()V

    .line 2343180
    invoke-direct {p0}, LX/GLb;->e()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2343170
    iget-object v0, p0, LX/GLb;->i:LX/1Ck;

    invoke-virtual {v0, p1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2343171
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;)V
    .locals 6
    .param p3    # Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2343167
    iget-object v0, p0, LX/GLb;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->setMoreOptionsViewVisibility(I)V

    .line 2343168
    const/4 v2, 0x0

    iget v4, p0, LX/GLb;->j:I

    new-instance v5, LX/GLW;

    invoke-direct {v5, p0, p3}, LX/GLW;-><init>(LX/GLb;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;)V

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, LX/GLb;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/0Vd;)V

    .line 2343169
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILX/0Vd;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "LX/0Vd",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2343164
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/GLb;->e(LX/GLb;Z)V

    .line 2343165
    iget-object v6, p0, LX/GLb;->i:LX/1Ck;

    iget-object v0, p0, LX/GLb;->f:LX/GDy;

    iget-object v1, p0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    iget-object v2, p0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v5

    move-object v2, p1

    move-object v3, p2

    move v4, p4

    invoke-virtual/range {v0 .. v5}, LX/GDy;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;ILX/8wL;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/GLZ;

    invoke-direct {v1, p0, p5}, LX/GLZ;-><init>(LX/GLb;LX/0Vd;)V

    invoke-virtual {v6, p3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2343166
    return-void
.end method

.method public final b()V
    .locals 14

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 2343124
    invoke-virtual {p0}, LX/GLb;->c()V

    .line 2343125
    iget-object v0, p0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->v()LX/0Px;

    move-result-object v0

    .line 2343126
    if-eqz v0, :cond_0

    iget-object v2, p0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->w()I

    move-result v2

    iget v3, p0, LX/GLb;->k:I

    if-ge v2, v3, :cond_1

    .line 2343127
    :cond_0
    :goto_0
    return-void

    .line 2343128
    :cond_1
    iget-object v2, p0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v2

    .line 2343129
    iget-object v3, v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v2, v3

    .line 2343130
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-ne v2, v3, :cond_2

    iget-boolean v2, p0, LX/GLb;->n:Z

    if-nez v2, :cond_2

    .line 2343131
    iget-object v2, p0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->v()LX/0Px;

    move-result-object v2

    .line 2343132
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v6

    const/4 v3, 0x0

    move v4, v3

    :goto_1
    if-ge v4, v6, :cond_d

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    .line 2343133
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-eq v7, v8, :cond_c

    .line 2343134
    :goto_2
    move-object v2, v3

    .line 2343135
    if-nez v2, :cond_4

    .line 2343136
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/GLb;->n:Z

    .line 2343137
    :cond_2
    :goto_3
    iget v2, p0, LX/GLb;->j:I

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 2343138
    iget v2, p0, LX/GLb;->l:I

    add-int/2addr v2, v6

    iput v2, p0, LX/GLb;->l:I

    .line 2343139
    iget-object v2, p0, LX/GLb;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->getMoreOptionsOffset()I

    move-result v7

    .line 2343140
    invoke-virtual {v0, v5, v6}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v4, v5

    move-object v2, v1

    move-object v3, v1

    :goto_4
    if-ge v4, v9, :cond_8

    invoke-virtual {v8, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    .line 2343141
    if-eqz v0, :cond_7

    .line 2343142
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->p()Z

    move-result v10

    if-eqz v10, :cond_3

    iget-object v10, p0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v10}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v10

    if-nez v10, :cond_3

    .line 2343143
    iget-object v10, p0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v10}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v10

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;)V

    .line 2343144
    :cond_3
    invoke-static {v0}, LX/GLb;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;)Z

    move-result v10

    if-eqz v10, :cond_5

    move-object v13, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v13

    .line 2343145
    :goto_5
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-object v3, v2

    move-object v2, v1

    move-object v1, v0

    goto :goto_4

    .line 2343146
    :cond_4
    iget-object v3, p0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v3}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;)V

    goto :goto_3

    .line 2343147
    :cond_5
    iget-boolean v10, p0, LX/GLb;->o:Z

    if-eqz v10, :cond_6

    iget-boolean v10, p0, LX/GLb;->p:Z

    if-nez v10, :cond_6

    .line 2343148
    sget-object v10, LX/GLa;->a:[I

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    .line 2343149
    :cond_6
    iget-object v10, p0, LX/GLb;->h:Ljava/util/List;

    add-int/lit8 v11, v7, 0x1

    invoke-direct {p0, v0, v11}, LX/GLb;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;I)Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_5

    :pswitch_0
    move-object v2, v3

    move-object v13, v0

    move-object v0, v1

    move-object v1, v13

    .line 2343150
    goto :goto_5

    :pswitch_1
    move-object v1, v2

    move-object v2, v3

    .line 2343151
    goto :goto_5

    .line 2343152
    :cond_8
    if-eqz v2, :cond_9

    iget v0, p0, LX/GLb;->k:I

    if-lt v6, v0, :cond_9

    .line 2343153
    iget-object v0, p0, LX/GLb;->h:Ljava/util/List;

    invoke-direct {p0, v2, v5}, LX/GLb;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;I)Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2343154
    :cond_9
    if-eqz v1, :cond_a

    iget v0, p0, LX/GLb;->k:I

    if-lt v6, v0, :cond_a

    .line 2343155
    iget-object v0, p0, LX/GLb;->h:Ljava/util/List;

    invoke-direct {p0, v1, v5}, LX/GLb;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;I)Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2343156
    :cond_a
    iget-boolean v0, p0, LX/GLb;->n:Z

    if-eqz v0, :cond_b

    if-eqz v3, :cond_b

    iget v0, p0, LX/GLb;->k:I

    if-lt v6, v0, :cond_b

    .line 2343157
    iget-object v0, p0, LX/GLb;->h:Ljava/util/List;

    invoke-direct {p0, v3, v5}, LX/GLb;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;I)Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2343158
    :cond_b
    invoke-static {p0}, LX/GLb;->g(LX/GLb;)V

    .line 2343159
    invoke-direct {p0}, LX/GLb;->f()V

    .line 2343160
    iget-object v0, p0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    .line 2343161
    iget-object v1, p0, LX/GLb;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    new-instance v2, LX/GLV;

    invoke-direct {v2, p0, v0}, LX/GLV;-><init>(LX/GLb;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)V

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->setMoreOptionsViewOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 2343162
    :cond_c
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto/16 :goto_1

    .line 2343163
    :cond_d
    const/4 v3, 0x0

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2343118
    iget-object v0, p0, LX/GLb;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;

    .line 2343119
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->a()V

    goto :goto_0

    .line 2343120
    :cond_0
    iget-object v0, p0, LX/GLb;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2343121
    const/4 v0, 0x0

    iput v0, p0, LX/GLb;->l:I

    .line 2343122
    iget-object v0, p0, LX/GLb;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->setMoreOptionsViewVisibility(I)V

    .line 2343123
    return-void
.end method
