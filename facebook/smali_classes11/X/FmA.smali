.class public final LX/FmA;
.super LX/2h1;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2h1",
        "<",
        "Lcom/facebook/privacy/model/PrivacyOptionsResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FmB;


# direct methods
.method public constructor <init>(LX/FmB;)V
    .locals 0

    .prologue
    .line 2282118
    iput-object p1, p0, LX/FmA;->a:LX/FmB;

    invoke-direct {p0}, LX/2h1;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 0

    .prologue
    .line 2282119
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2282120
    check-cast p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 2282121
    new-instance v0, LX/8QV;

    invoke-direct {v0}, LX/8QV;-><init>()V

    .line 2282122
    iput-object p1, v0, LX/8QV;->a:Lcom/facebook/privacy/model/PrivacyOptionsResult;

    .line 2282123
    move-object v0, v0

    .line 2282124
    iget-object v1, p1, Lcom/facebook/privacy/model/PrivacyOptionsResult;->selectedPrivacyOption:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-virtual {v0, v1}, LX/8QV;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)LX/8QV;

    move-result-object v0

    invoke-virtual {v0}, LX/8QV;->b()Lcom/facebook/privacy/model/SelectablePrivacyData;

    move-result-object v1

    .line 2282125
    iget-object v2, p0, LX/FmA;->a:LX/FmB;

    iget-object v0, p0, LX/FmA;->a:LX/FmB;

    iget-object v0, v0, LX/FmB;->f:Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;

    if-nez v0, :cond_0

    .line 2282126
    new-instance v0, LX/FmO;

    invoke-direct {v0}, LX/FmO;-><init>()V

    move-object v0, v0

    .line 2282127
    :goto_0
    iput-object v1, v0, LX/FmO;->a:Lcom/facebook/privacy/model/SelectablePrivacyData;

    .line 2282128
    move-object v0, v0

    .line 2282129
    invoke-virtual {v0}, LX/FmO;->a()Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;

    move-result-object v0

    .line 2282130
    iput-object v0, v2, LX/FmB;->f:Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;

    .line 2282131
    iget-object v0, p0, LX/FmA;->a:LX/FmB;

    iget-object v0, v0, LX/FmB;->d:LX/6qb;

    iget-object v1, p0, LX/FmA;->a:LX/FmB;

    iget-object v1, v1, LX/FmB;->f:Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;

    invoke-interface {v0, v1}, LX/6qb;->a(Landroid/os/Parcelable;)V

    .line 2282132
    return-void

    .line 2282133
    :cond_0
    iget-object v0, p0, LX/FmA;->a:LX/FmB;

    iget-object v0, v0, LX/FmB;->f:Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;

    invoke-static {v0}, Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;->a(Lcom/facebook/socialgood/payments/model/FundraiserDonationCheckoutData;)LX/FmO;

    move-result-object v0

    goto :goto_0
.end method
