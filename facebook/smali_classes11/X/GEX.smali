.class public LX/GEX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GEW",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/GIA;


# direct methods
.method public constructor <init>(LX/GIA;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2331848
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2331849
    iput-object p1, p0, LX/GEX;->a:LX/GIA;

    .line 2331850
    return-void
.end method

.method public static a(LX/0QB;)LX/GEX;
    .locals 1

    .prologue
    .line 2331860
    invoke-static {p0}, LX/GEX;->b(LX/0QB;)LX/GEX;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/GEX;
    .locals 2

    .prologue
    .line 2331858
    new-instance v1, LX/GEX;

    invoke-static {p0}, LX/GIA;->b(LX/0QB;)LX/GIA;

    move-result-object v0

    check-cast v0, LX/GIA;

    invoke-direct {v1, v0}, LX/GEX;-><init>(LX/GIA;)V

    .line 2331859
    return-object v1
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2331857
    const v0, 0x7f03003d

    return v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2331853
    invoke-static {p1}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2331854
    :cond_0
    :goto_0
    return v0

    .line 2331855
    :cond_1
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    .line 2331856
    sget-object v2, LX/GGB;->INACTIVE:LX/GGB;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/GGB;->ACTIVE:LX/GGB;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/GGB;->PAUSED:LX/GGB;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/GGB;->EXTENDABLE:LX/GGB;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/GGB;->FINISHED:LX/GGB;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/GGB;->PENDING:LX/GGB;

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()LX/GHg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesViewController",
            "<",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;",
            "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2331852
    iget-object v0, p0, LX/GEX;->a:LX/GIA;

    return-object v0
.end method

.method public final c()LX/8wK;
    .locals 1

    .prologue
    .line 2331851
    sget-object v0, LX/8wK;->ACCOUNT:LX/8wK;

    return-object v0
.end method
