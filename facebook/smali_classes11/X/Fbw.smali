.class public LX/Fbw;
.super LX/FbI;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2261387
    invoke-direct {p0}, LX/FbI;-><init>()V

    .line 2261388
    return-void
.end method

.method public static a(LX/0QB;)LX/Fbw;
    .locals 3

    .prologue
    .line 2261389
    const-class v1, LX/Fbw;

    monitor-enter v1

    .line 2261390
    :try_start_0
    sget-object v0, LX/Fbw;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2261391
    sput-object v2, LX/Fbw;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2261392
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2261393
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2261394
    new-instance v0, LX/Fbw;

    invoke-direct {v0}, LX/Fbw;-><init>()V

    .line 2261395
    move-object v0, v0

    .line 2261396
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2261397
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fbw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2261398
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2261399
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;)LX/8dV;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261400
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2261401
    if-nez v0, :cond_0

    .line 2261402
    const/4 v0, 0x0

    .line 2261403
    :goto_0
    return-object v0

    .line 2261404
    :cond_0
    new-instance v1, LX/8dX;

    invoke-direct {v1}, LX/8dX;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    .line 2261405
    iput-object v2, v1, LX/8dX;->L:Ljava/lang/String;

    .line 2261406
    move-object v1, v1

    .line 2261407
    new-instance v2, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v3, 0x736a6b81

    invoke-direct {v2, v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2261408
    iput-object v2, v1, LX/8dX;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2261409
    move-object v1, v1

    .line 2261410
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v2

    .line 2261411
    iput-object v2, v1, LX/8dX;->ad:Ljava/lang/String;

    .line 2261412
    move-object v1, v1

    .line 2261413
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fj()Lcom/facebook/graphql/model/GraphQLLocation;

    move-result-object v2

    .line 2261414
    if-nez v2, :cond_1

    .line 2261415
    const/4 v3, 0x0

    .line 2261416
    :goto_1
    move-object v2, v3

    .line 2261417
    iput-object v2, v1, LX/8dX;->aa:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    .line 2261418
    move-object v1, v1

    .line 2261419
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->bC()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v0

    .line 2261420
    if-nez v0, :cond_2

    .line 2261421
    const/4 v2, 0x0

    .line 2261422
    :goto_2
    move-object v0, v2

    .line 2261423
    iput-object v0, v1, LX/8dX;->q:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    .line 2261424
    move-object v0, v1

    .line 2261425
    invoke-virtual {v0}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2261426
    new-instance v1, LX/8dV;

    invoke-direct {v1}, LX/8dV;-><init>()V

    .line 2261427
    iput-object v0, v1, LX/8dV;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2261428
    move-object v0, v1

    .line 2261429
    goto :goto_0

    :cond_1
    new-instance v3, LX/8di;

    invoke-direct {v3}, LX/8di;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLLocation;->j()Ljava/lang/String;

    move-result-object p0

    .line 2261430
    iput-object p0, v3, LX/8di;->c:Ljava/lang/String;

    .line 2261431
    move-object v3, v3

    .line 2261432
    invoke-virtual {v3}, LX/8di;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$LocationModel;

    move-result-object v3

    goto :goto_1

    :cond_2
    new-instance v2, LX/8fv;

    invoke-direct {v2}, LX/8fv;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v3

    .line 2261433
    if-nez v3, :cond_3

    .line 2261434
    const/4 p0, 0x0

    .line 2261435
    :goto_3
    move-object v3, p0

    .line 2261436
    iput-object v3, v2, LX/8fv;->a:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel$PhotoModel;

    .line 2261437
    move-object v2, v2

    .line 2261438
    invoke-virtual {v2}, LX/8fv;->a()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v2

    goto :goto_2

    :cond_3
    new-instance p0, LX/8fw;

    invoke-direct {p0}, LX/8fw;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPhoto;->L()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    invoke-static {v0}, LX/FbH;->a(Lcom/facebook/graphql/model/GraphQLImage;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    .line 2261439
    iput-object v0, p0, LX/8fw;->a:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2261440
    move-object p0, p0

    .line 2261441
    invoke-virtual {p0}, LX/8fw;->a()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel$PhotoModel;

    move-result-object p0

    goto :goto_3
.end method
