.class public final LX/Fy6;
.super LX/2Ip;
.source ""


# instance fields
.field public final synthetic a:LX/FyD;


# direct methods
.method public constructor <init>(LX/FyD;)V
    .locals 0

    .prologue
    .line 2306351
    iput-object p1, p0, LX/Fy6;->a:LX/FyD;

    invoke-direct {p0}, LX/2Ip;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0b7;)Z
    .locals 6

    .prologue
    .line 2306352
    check-cast p1, LX/2f2;

    .line 2306353
    if-eqz p1, :cond_0

    iget-object v0, p0, LX/Fy6;->a:LX/FyD;

    iget-object v0, v0, LX/FyD;->a:LX/BP0;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fy6;->a:LX/FyD;

    iget-object v0, v0, LX/FyD;->a:LX/BP0;

    .line 2306354
    iget-wide v4, v0, LX/5SB;->b:J

    move-wide v0, v4

    .line 2306355
    iget-wide v2, p1, LX/2f2;->a:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/0b7;)V
    .locals 2

    .prologue
    .line 2306313
    check-cast p1, LX/2f2;

    .line 2306314
    iget-boolean v0, p1, LX/2f2;->c:Z

    if-eqz v0, :cond_0

    .line 2306315
    iget-object v0, p0, LX/Fy6;->a:LX/FyD;

    iget-object v1, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2306316
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v1, p0, :cond_3

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v1, p0, :cond_3

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v1, p0, :cond_3

    .line 2306317
    const/4 p0, 0x0

    iput-object p0, v0, LX/FyD;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2306318
    :goto_0
    return-void

    .line 2306319
    :cond_0
    iget-object v0, p0, LX/Fy6;->a:LX/FyD;

    iget-object v1, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2306320
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v1, p0, :cond_b

    iget-object p0, v0, LX/FyD;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne p0, p1, :cond_b

    const/4 p0, 0x1

    :goto_1
    move p0, p0

    .line 2306321
    if-nez p0, :cond_1

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v1, p0, :cond_1

    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v1, p0, :cond_8

    .line 2306322
    :cond_1
    iget-object p0, v0, LX/FyD;->f:LX/Fsr;

    invoke-virtual {p0}, LX/Fsr;->lk_()V

    .line 2306323
    :cond_2
    :goto_2
    goto :goto_0

    .line 2306324
    :cond_3
    iget-object p0, v0, LX/FyD;->e:LX/BQ1;

    invoke-virtual {p0}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object p0

    iput-object p0, v0, LX/FyD;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2306325
    iget-object p0, v0, LX/FyD;->e:LX/BQ1;

    .line 2306326
    iget-object p1, p0, LX/BQ1;->d:LX/5wQ;

    if-nez p1, :cond_7

    .line 2306327
    :goto_3
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v1, p0, :cond_4

    iget-object p0, v0, LX/FyD;->e:LX/BQ1;

    invoke-virtual {p0}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object p0

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {p0, p1}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_4

    .line 2306328
    iget-object p0, v0, LX/FyD;->e:LX/BQ1;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {p0, p1}, LX/BQ1;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 2306329
    :cond_4
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v1, p0, :cond_5

    iget-object p0, v0, LX/FyD;->e:LX/BQ1;

    invoke-virtual {p0}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object p0

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {p0, p1}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    .line 2306330
    iget-object p0, v0, LX/FyD;->e:LX/BQ1;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-virtual {p0, p1}, LX/BQ1;->a(Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 2306331
    :cond_5
    iget-object p0, v0, LX/FyD;->d:Lcom/facebook/timeline/TimelineFragment;

    if-eqz p0, :cond_6

    .line 2306332
    iget-object p0, v0, LX/FyD;->d:Lcom/facebook/timeline/TimelineFragment;

    .line 2306333
    invoke-virtual {p0}, Lcom/facebook/timeline/TimelineFragment;->E()V

    .line 2306334
    :cond_6
    goto :goto_0

    .line 2306335
    :cond_7
    invoke-static {p0}, LX/BQ1;->ao(LX/BQ1;)LX/5zT;

    move-result-object p1

    .line 2306336
    iput-object v1, p1, LX/5zT;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2306337
    move-object p1, p1

    .line 2306338
    invoke-virtual {p1}, LX/5zT;->a()Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$TimelineHeaderActionFieldsModel;

    move-result-object p1

    .line 2306339
    iput-object p1, p0, LX/BQ1;->d:LX/5wQ;

    .line 2306340
    invoke-virtual {p0}, LX/BPy;->m()V

    .line 2306341
    invoke-virtual {p0}, LX/BPy;->l()V

    goto :goto_3

    .line 2306342
    :cond_8
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v1, p0, :cond_9

    .line 2306343
    iget-object p0, v0, LX/FyD;->f:LX/Fsr;

    invoke-virtual {p0}, LX/Fsr;->mJ_()V

    goto :goto_2

    .line 2306344
    :cond_9
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v1, p0, :cond_c

    iget-object p0, v0, LX/FyD;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne p0, p1, :cond_c

    const/4 p0, 0x1

    :goto_4
    move p0, p0

    .line 2306345
    if-eqz p0, :cond_a

    .line 2306346
    iget-object p0, v0, LX/FyD;->f:LX/Fsr;

    invoke-virtual {p0}, LX/Fsr;->lk_()V

    goto :goto_2

    .line 2306347
    :cond_a
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v1, p0, :cond_d

    iget-object p0, v0, LX/FyD;->i:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne p0, p1, :cond_d

    const/4 p0, 0x1

    :goto_5
    move p0, p0

    .line 2306348
    if-eqz p0, :cond_2

    .line 2306349
    iget-object p0, v0, LX/FyD;->g:LX/0Or;

    invoke-interface {p0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Fqs;

    const-string p1, "RemoveFriendOnSuccess"

    invoke-virtual {p0, p1}, LX/Fqs;->a(Ljava/lang/String;)V

    .line 2306350
    iget-object p0, v0, LX/FyD;->f:LX/Fsr;

    invoke-virtual {p0}, LX/Fsr;->mJ_()V

    goto/16 :goto_2

    :cond_b
    const/4 p0, 0x0

    goto/16 :goto_1

    :cond_c
    const/4 p0, 0x0

    goto :goto_4

    :cond_d
    const/4 p0, 0x0

    goto :goto_5
.end method
