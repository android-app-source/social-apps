.class public final enum LX/Go5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Go5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Go5;

.field public static final enum ANDROID_BACK_BUTTON_PRESSED:LX/Go5;

.field public static final enum DOCUMENT_BACK_BUTTON_PRESSED:LX/Go5;

.field public static final enum DOCUMENT_CLOSED_ON_PRELAUNCH:LX/Go5;

.field public static final enum DOCUMENT_SWIPED_BACK:LX/Go5;

.field public static final enum UNKNOWN:LX/Go5;


# instance fields
.field private final mBackButtonReason:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2394069
    new-instance v0, LX/Go5;

    const-string v1, "DOCUMENT_BACK_BUTTON_PRESSED"

    const-string v2, "document_back_button_pressed"

    invoke-direct {v0, v1, v3, v2}, LX/Go5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Go5;->DOCUMENT_BACK_BUTTON_PRESSED:LX/Go5;

    .line 2394070
    new-instance v0, LX/Go5;

    const-string v1, "ANDROID_BACK_BUTTON_PRESSED"

    const-string v2, "android_back_button_pressed"

    invoke-direct {v0, v1, v4, v2}, LX/Go5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Go5;->ANDROID_BACK_BUTTON_PRESSED:LX/Go5;

    .line 2394071
    new-instance v0, LX/Go5;

    const-string v1, "DOCUMENT_SWIPED_BACK"

    const-string v2, "document_swipped_back"

    invoke-direct {v0, v1, v5, v2}, LX/Go5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Go5;->DOCUMENT_SWIPED_BACK:LX/Go5;

    .line 2394072
    new-instance v0, LX/Go5;

    const-string v1, "DOCUMENT_CLOSED_ON_PRELAUNCH"

    const-string v2, "document_closed_on_prelaunch"

    invoke-direct {v0, v1, v6, v2}, LX/Go5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Go5;->DOCUMENT_CLOSED_ON_PRELAUNCH:LX/Go5;

    .line 2394073
    new-instance v0, LX/Go5;

    const-string v1, "UNKNOWN"

    const-string v2, "unknown"

    invoke-direct {v0, v1, v7, v2}, LX/Go5;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Go5;->UNKNOWN:LX/Go5;

    .line 2394074
    const/4 v0, 0x5

    new-array v0, v0, [LX/Go5;

    sget-object v1, LX/Go5;->DOCUMENT_BACK_BUTTON_PRESSED:LX/Go5;

    aput-object v1, v0, v3

    sget-object v1, LX/Go5;->ANDROID_BACK_BUTTON_PRESSED:LX/Go5;

    aput-object v1, v0, v4

    sget-object v1, LX/Go5;->DOCUMENT_SWIPED_BACK:LX/Go5;

    aput-object v1, v0, v5

    sget-object v1, LX/Go5;->DOCUMENT_CLOSED_ON_PRELAUNCH:LX/Go5;

    aput-object v1, v0, v6

    sget-object v1, LX/Go5;->UNKNOWN:LX/Go5;

    aput-object v1, v0, v7

    sput-object v0, LX/Go5;->$VALUES:[LX/Go5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2394075
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2394076
    iput-object p3, p0, LX/Go5;->mBackButtonReason:Ljava/lang/String;

    .line 2394077
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Go5;
    .locals 1

    .prologue
    .line 2394078
    const-class v0, LX/Go5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Go5;

    return-object v0
.end method

.method public static values()[LX/Go5;
    .locals 1

    .prologue
    .line 2394079
    sget-object v0, LX/Go5;->$VALUES:[LX/Go5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Go5;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2394080
    iget-object v0, p0, LX/Go5;->mBackButtonReason:Ljava/lang/String;

    return-object v0
.end method
