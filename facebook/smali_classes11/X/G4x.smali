.class public LX/G4x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g1;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0g1",
        "<",
        "Ljava/lang/Object;",
        ">;",
        "Lcom/facebook/timeline/units/model/TimelineUnitsMutationCallbacks;",
        "Lcom/facebook/timeline/units/model/TimelineUnitsSubscriptionRegistrar;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final a:LX/03V;

.field public final b:LX/0So;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/G59;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/G4z;

.field public e:LX/0qm;

.field public f:Z

.field public g:LX/G4v;


# direct methods
.method public constructor <init>(LX/03V;LX/0qm;LX/0So;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2318312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2318313
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/G4x;->c:Ljava/util/List;

    .line 2318314
    new-instance v0, LX/G4z;

    invoke-direct {v0}, LX/G4z;-><init>()V

    iput-object v0, p0, LX/G4x;->d:LX/G4z;

    .line 2318315
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/G4x;->f:Z

    .line 2318316
    const/4 v0, 0x0

    iput-object v0, p0, LX/G4x;->g:LX/G4v;

    .line 2318317
    iput-object p1, p0, LX/G4x;->a:LX/03V;

    .line 2318318
    iput-object p2, p0, LX/G4x;->e:LX/0qm;

    .line 2318319
    iput-object p3, p0, LX/G4x;->b:LX/0So;

    .line 2318320
    return-void
.end method

.method public static a(LX/0QB;)LX/G4x;
    .locals 6

    .prologue
    .line 2318321
    const-class v1, LX/G4x;

    monitor-enter v1

    .line 2318322
    :try_start_0
    sget-object v0, LX/G4x;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2318323
    sput-object v2, LX/G4x;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2318324
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2318325
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2318326
    new-instance p0, LX/G4x;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v3

    check-cast v3, LX/03V;

    invoke-static {v0}, LX/0qm;->b(LX/0QB;)LX/0qm;

    move-result-object v4

    check-cast v4, LX/0qm;

    invoke-static {v0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-direct {p0, v3, v4, v5}, LX/G4x;-><init>(LX/03V;LX/0qm;LX/0So;)V

    .line 2318327
    move-object v0, p0

    .line 2318328
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2318329
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/G4x;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2318330
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2318331
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/G4x;LX/G58;)LX/G59;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2318332
    iget-object v0, p0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G59;

    .line 2318333
    iget-object v2, v0, LX/G59;->a:LX/G58;

    move-object v2, v2

    .line 2318334
    if-ne v2, p1, :cond_0

    .line 2318335
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/StoryVisibility;I)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 4

    .prologue
    .line 2318336
    invoke-static {p1}, LX/6X8;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/6X8;

    move-result-object v0

    iget-object v1, p0, LX/G4x;->b:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3, p2, p3}, LX/6X8;->a(JLcom/facebook/graphql/enums/StoryVisibility;I)LX/6X8;

    move-result-object v0

    invoke-virtual {v0}, LX/6X8;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    return-object v0
.end method

.method private a(Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2318337
    iget-object v0, p0, LX/G4x;->e:LX/0qm;

    invoke-virtual {v0, p1}, LX/0qm;->e(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2318338
    if-eqz v0, :cond_0

    .line 2318339
    iget-object v1, p0, LX/G4x;->e:LX/0qm;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->H_()I

    move-result v2

    invoke-direct {p0, v0, p2, v2}, LX/G4x;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/StoryVisibility;I)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0qm;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    .line 2318340
    :cond_0
    return-object v0
.end method

.method public static a(LX/G59;)V
    .locals 2

    .prologue
    .line 2318341
    iget-object v0, p0, LX/G59;->a:LX/G58;

    move-object v0, v0

    .line 2318342
    sget-object v1, LX/G58;->RECENT_SECTION:LX/G58;

    if-eq v0, v1, :cond_0

    .line 2318343
    :goto_0
    return-void

    .line 2318344
    :cond_0
    invoke-virtual {p0}, LX/G59;->d()V

    goto :goto_0
.end method

.method public static b(Lcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2318345
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->K()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->w()LX/0Px;

    move-result-object v0

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;->YEAR_OVERVIEW:Lcom/facebook/graphql/enums/GraphQLStoryAttachmentStyle;

    invoke-virtual {v0, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static e(LX/G4x;)V
    .locals 5

    .prologue
    .line 2318192
    const/4 v0, 0x0

    .line 2318193
    iget-object v1, p0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G59;

    .line 2318194
    invoke-virtual {v0}, LX/G59;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 2318195
    instance-of v4, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v4, :cond_1

    .line 2318196
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0, v1}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;I)V

    .line 2318197
    add-int/lit8 v1, v1, 0x1

    .line 2318198
    goto :goto_0

    .line 2318199
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)LX/16n;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2318346
    iget-object v0, p0, LX/G4x;->e:LX/0qm;

    invoke-virtual {v0, p2}, LX/0qm;->e(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2318347
    if-eqz v0, :cond_0

    .line 2318348
    :goto_0
    return-object v0

    .line 2318349
    :cond_0
    iget-object v0, p0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G59;

    .line 2318350
    invoke-virtual {v0, p1, p2}, LX/G59;->b(Ljava/lang/String;Ljava/lang/String;)LX/16n;

    move-result-object v0

    .line 2318351
    if-eqz v0, :cond_1

    goto :goto_0

    .line 2318352
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)LX/G59;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2318353
    if-eqz p1, :cond_1

    .line 2318354
    iget-object v0, p0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G59;

    .line 2318355
    iget-object v2, v0, LX/G59;->f:Ljava/lang/String;

    move-object v2, v2

    .line 2318356
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2318357
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2318358
    iget-boolean v0, p0, LX/G4x;->f:Z

    if-eqz v0, :cond_3

    .line 2318359
    if-nez p1, :cond_0

    .line 2318360
    iget-object v0, p0, LX/G4x;->d:LX/G4z;

    .line 2318361
    :goto_0
    return-object v0

    .line 2318362
    :cond_0
    add-int/lit8 v0, p1, -0x1

    .line 2318363
    :goto_1
    iget-object v1, p0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G59;

    .line 2318364
    invoke-virtual {v0}, LX/G59;->b()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 2318365
    invoke-virtual {v0, v1}, LX/G59;->a(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 2318366
    :cond_1
    invoke-virtual {v0}, LX/G59;->b()I

    move-result v0

    sub-int v0, v1, v0

    move v1, v0

    .line 2318367
    goto :goto_2

    .line 2318368
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid index:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move v0, p1

    goto :goto_1
.end method

.method public final a(LX/16n;Lcom/facebook/graphql/model/GraphQLFeedback;)V
    .locals 5

    .prologue
    .line 2318301
    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    .line 2318302
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {p1}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v0

    .line 2318303
    iput-object p2, v0, LX/23u;->D:Lcom/facebook/graphql/model/GraphQLFeedback;

    .line 2318304
    move-object v0, v0

    .line 2318305
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 2318306
    iput-wide v2, v0, LX/23u;->G:J

    .line 2318307
    move-object v0, v0

    .line 2318308
    invoke-virtual {v0}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2318309
    invoke-virtual {p0, v0}, LX/G4x;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2318310
    :goto_0
    return-void

    .line 2318311
    :cond_0
    iget-object v0, p0, LX/G4x;->a:LX/03V;

    const-string v1, "timeline_story_update_fail"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to update unit of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(LX/Fso;LX/G5A;)V
    .locals 2

    .prologue
    .line 2318369
    iget-boolean v0, p1, LX/Fso;->f:Z

    if-eqz v0, :cond_2

    .line 2318370
    sget-object v0, LX/G58;->RECENT_SECTION:LX/G58;

    invoke-static {p0, v0}, LX/G4x;->a(LX/G4x;LX/G58;)LX/G59;

    move-result-object v0

    .line 2318371
    if-eqz v0, :cond_1

    .line 2318372
    sget-object v1, LX/G5A;->COMPLETED:LX/G5A;

    invoke-virtual {p0, v1}, LX/G4x;->a(LX/G5A;)V

    .line 2318373
    invoke-virtual {v0, p2, p1}, LX/G59;->a(LX/G5A;LX/Fso;)V

    .line 2318374
    :goto_0
    sget-object v0, LX/G58;->UNSEEN_SECTION:LX/G58;

    invoke-static {p0, v0}, LX/G4x;->a(LX/G4x;LX/G58;)LX/G59;

    move-result-object v0

    .line 2318375
    if-eqz v0, :cond_0

    sget-object v1, LX/G5A;->COMPLETED:LX/G5A;

    if-ne p2, v1, :cond_0

    .line 2318376
    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, LX/G59;->a(LX/G5A;LX/Fso;)V

    .line 2318377
    :cond_0
    :goto_1
    return-void

    .line 2318378
    :cond_1
    invoke-virtual {p0, p2}, LX/G4x;->a(LX/G5A;)V

    goto :goto_0

    .line 2318379
    :cond_2
    iget-object v0, p1, LX/Fso;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, LX/G4x;->a(Ljava/lang/String;)LX/G59;

    move-result-object v0

    .line 2318380
    if-eqz v0, :cond_0

    .line 2318381
    invoke-virtual {v0, p2, p1}, LX/G59;->a(LX/G5A;LX/Fso;)V

    goto :goto_1
.end method

.method public final a(LX/G5A;)V
    .locals 1

    .prologue
    .line 2318298
    iget-object v0, p0, LX/G4x;->d:LX/G4z;

    .line 2318299
    iput-object p1, v0, LX/G4z;->c:LX/G5A;

    .line 2318300
    return-void
.end method

.method public final a(LX/G5A;LX/Fsp;)V
    .locals 2
    .param p2    # LX/Fsp;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2318280
    iget-object v0, p0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2318281
    invoke-virtual {p0, p1}, LX/G4x;->a(LX/G5A;)V

    .line 2318282
    :goto_0
    return-void

    .line 2318283
    :cond_0
    iget-object v0, p0, LX/G4x;->c:Ljava/util/List;

    iget-object v1, p0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G59;

    .line 2318284
    sget-object v1, LX/G5A;->LOADING:LX/G5A;

    if-ne p1, v1, :cond_2

    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, v0, LX/G59;->g:Z

    .line 2318285
    sget-object v1, LX/G57;->a:[I

    invoke-virtual {p1}, LX/G5A;->ordinal()I

    move-result p0

    aget v1, v1, p0

    packed-switch v1, :pswitch_data_0

    .line 2318286
    :cond_1
    :goto_2
    goto :goto_0

    .line 2318287
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 2318288
    :pswitch_0
    invoke-static {v0}, LX/G59;->m(LX/G59;)V

    .line 2318289
    new-instance v1, LX/G4z;

    invoke-direct {v1, p2}, LX/G4z;-><init>(LX/Fsp;)V

    .line 2318290
    iget-object p0, v0, LX/G59;->d:Ljava/util/List;

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2318291
    :goto_3
    :pswitch_1
    invoke-static {v0}, LX/G59;->n(LX/G59;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2318292
    iget-object v1, v0, LX/G59;->d:Ljava/util/List;

    iget-object p0, v0, LX/G59;->d:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    add-int/lit8 p0, p0, -0x1

    invoke-interface {v1, p0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_3

    .line 2318293
    :pswitch_2
    invoke-static {v0}, LX/G59;->n(LX/G59;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2318294
    iget-object v1, v0, LX/G59;->d:Ljava/util/List;

    iget-object p0, v0, LX/G59;->d:Ljava/util/List;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    add-int/lit8 p0, p0, -0x1

    invoke-interface {v1, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/G4z;

    .line 2318295
    sget-object p0, LX/G5A;->FAILED:LX/G5A;

    .line 2318296
    iput-object p0, v1, LX/G4z;->c:LX/G5A;

    .line 2318297
    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 4

    .prologue
    .line 2318266
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 2318267
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->al()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2318268
    iget-object v1, p0, LX/G4x;->e:LX/0qm;

    move-object v0, p1

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1, v0}, LX/0qm;->a(Lcom/facebook/graphql/model/GraphQLStory;)Z

    .line 2318269
    :cond_0
    iget-object v0, p0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G59;

    .line 2318270
    iget-object v2, v0, LX/G59;->k:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    invoke-static {p1, v2, v3}, LX/16t;->a(Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;J)V

    .line 2318271
    invoke-interface {p1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/G59;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 2318272
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 2318273
    iget-object v3, v0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v3, v2, p1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/FeedUnit;

    .line 2318274
    invoke-static {v2}, LX/0x1;->f(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v2

    .line 2318275
    invoke-static {p1, v2}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;I)V

    .line 2318276
    :cond_1
    goto :goto_0

    .line 2318277
    :cond_2
    iget-object v0, p0, LX/G4x;->g:LX/G4v;

    if-eqz v0, :cond_3

    instance-of v0, p1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_3

    move-object v0, p1

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2318278
    iget-object v0, p0, LX/G4x;->g:LX/G4v;

    check-cast p1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0, p1}, LX/G4v;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2318279
    :cond_3
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;LX/G5A;)V
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 2318250
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2318251
    :cond_0
    :goto_0
    return-void

    .line 2318252
    :cond_1
    new-instance v0, LX/G59;

    sget-object v1, LX/G58;->UNSEEN_SECTION:LX/G58;

    invoke-virtual {v1}, LX/G58;->name()Ljava/lang/String;

    move-result-object v1

    sget-object v3, LX/G58;->UNSEEN_SECTION:LX/G58;

    const/4 v4, 0x1

    iget-object v5, p0, LX/G4x;->b:LX/0So;

    move-object v6, v2

    invoke-direct/range {v0 .. v6}, LX/G59;-><init>(Ljava/lang/String;Ljava/lang/String;LX/G58;ZLX/0So;LX/0qm;)V

    .line 2318253
    iget-object v1, p0, LX/G4x;->c:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v1, v3, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2318254
    iget-object v7, v0, LX/G59;->a:LX/G58;

    sget-object v8, LX/G58;->UNSEEN_SECTION:LX/G58;

    if-ne v7, v8, :cond_2

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;->a()LX/0Px;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2318255
    :cond_2
    :goto_1
    invoke-virtual {v0, p2, v2}, LX/G59;->a(LX/G5A;LX/Fso;)V

    .line 2318256
    invoke-static {p0}, LX/G4x;->e(LX/G4x;)V

    .line 2318257
    iget-object v1, p0, LX/G4x;->g:LX/G4v;

    if-eqz v1, :cond_0

    .line 2318258
    iget-object v1, p0, LX/G4x;->g:LX/G4v;

    invoke-virtual {v1, v0}, LX/G4v;->a(Ljava/lang/Iterable;)V

    goto :goto_0

    .line 2318259
    :cond_3
    invoke-virtual {v0}, LX/G59;->d()V

    .line 2318260
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    const/4 v7, 0x0

    move v8, v7

    :goto_2
    if-ge v8, v10, :cond_4

    invoke-virtual {v9, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2318261
    iget-object v11, v0, LX/G59;->k:LX/0So;

    invoke-interface {v11}, LX/0So;->now()J

    move-result-wide v11

    invoke-static {v7, v11, v12}, LX/16t;->a(Lcom/facebook/graphql/model/GraphQLStory;J)V

    .line 2318262
    iget-object v11, v0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v11, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2318263
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_2

    .line 2318264
    :cond_4
    iget v7, v0, LX/G59;->e:I

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v8

    add-int/2addr v7, v8

    iput v7, v0, LX/G59;->e:I

    .line 2318265
    const/4 v7, 0x1

    iput-boolean v7, v0, LX/G59;->h:Z

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V
    .locals 2

    .prologue
    .line 2318245
    if-nez p1, :cond_0

    if-eqz p2, :cond_0

    .line 2318246
    invoke-direct {p0, p2, p3}, LX/G4x;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;)Lcom/facebook/graphql/model/GraphQLStory;

    .line 2318247
    :cond_0
    iget-object v0, p0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G59;

    .line 2318248
    invoke-virtual {v0, p1, p2, p3, p4}, LX/G59;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    goto :goto_0

    .line 2318249
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;)V
    .locals 3

    .prologue
    .line 2318234
    const/4 v0, 0x0

    .line 2318235
    if-eqz p3, :cond_2

    .line 2318236
    iget-object v0, p0, LX/G4x;->e:LX/0qm;

    invoke-virtual {v0, p3}, LX/0qm;->f(Ljava/lang/String;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2318237
    if-eqz v0, :cond_0

    .line 2318238
    iget-object v1, p0, LX/G4x;->e:LX/0qm;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->H_()I

    move-result v2

    invoke-direct {p0, v0, p4, v2}, LX/G4x;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/enums/StoryVisibility;I)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    invoke-virtual {v1, p3, v2}, LX/0qm;->a(Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2318239
    :cond_0
    :goto_0
    if-eqz v0, :cond_3

    .line 2318240
    :cond_1
    return-void

    .line 2318241
    :cond_2
    if-eqz p2, :cond_0

    .line 2318242
    invoke-direct {p0, p2, p4}, LX/G4x;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    goto :goto_0

    .line 2318243
    :cond_3
    iget-object v0, p0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G59;

    .line 2318244
    const/4 v2, -0x1

    invoke-virtual {v0, p1, p2, p4, v2}, LX/G59;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V

    goto :goto_1
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 2318224
    iget-object v0, p0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/G4x;->c:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G59;

    .line 2318225
    iget-object v1, v0, LX/G59;->a:LX/G58;

    move-object v0, v1

    .line 2318226
    sget-object v1, LX/G58;->UNSEEN_SECTION:LX/G58;

    if-ne v0, v1, :cond_0

    .line 2318227
    iget-object v0, p0, LX/G4x;->c:Ljava/util/List;

    const/4 v1, 0x1

    iget-object v2, p0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-interface {v0, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2318228
    :goto_0
    if-eqz p1, :cond_1

    .line 2318229
    iget-object v0, p0, LX/G4x;->e:LX/0qm;

    invoke-virtual {v0}, LX/0qm;->d()V

    .line 2318230
    :goto_1
    return-void

    .line 2318231
    :cond_0
    iget-object v0, p0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 2318232
    :cond_1
    iget-object v0, p0, LX/G4x;->e:LX/0qm;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v0, v1}, LX/0qm;->a(Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    .line 2318233
    iget-object v0, p0, LX/G4x;->e:LX/0qm;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;->SUCCESS:Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;

    invoke-virtual {v0, v1}, LX/0qm;->a(Lcom/facebook/graphql/enums/GraphQLFeedOptimisticPublishState;)V

    goto :goto_1
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 2318222
    iput-boolean p1, p0, LX/G4x;->f:Z

    .line 2318223
    return-void
.end method

.method public final d()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2318217
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2318218
    iget-object v0, p0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G59;

    .line 2318219
    iget-object p0, v0, LX/G59;->f:Ljava/lang/String;

    move-object v0, p0

    .line 2318220
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2318221
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2318207
    const/4 v0, 0x0

    .line 2318208
    iget-object v1, p0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G59;

    .line 2318209
    if-eqz v1, :cond_0

    .line 2318210
    iget-object v1, v0, LX/G59;->f:Ljava/lang/String;

    move-object v0, v1

    .line 2318211
    :goto_1
    return-object v0

    .line 2318212
    :cond_0
    iget-object p0, v0, LX/G59;->f:Ljava/lang/String;

    move-object v0, p0

    .line 2318213
    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2318214
    const/4 v0, 0x1

    :goto_2
    move v1, v0

    .line 2318215
    goto :goto_0

    .line 2318216
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final size()I
    .locals 3

    .prologue
    .line 2318200
    const/4 v0, 0x0

    .line 2318201
    iget-boolean v1, p0, LX/G4x;->f:Z

    if-eqz v1, :cond_0

    .line 2318202
    const/4 v0, 0x1

    .line 2318203
    :cond_0
    iget-object v1, p0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G59;

    .line 2318204
    invoke-virtual {v0}, LX/G59;->b()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 2318205
    goto :goto_0

    .line 2318206
    :cond_1
    return v1
.end method
