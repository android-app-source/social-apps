.class public LX/GsN;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/GAb;

.field public final c:Ljava/lang/String;

.field public d:Ljava/lang/StringBuilder;

.field public e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2399429
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/GsN;->a:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(LX/GAb;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2399422
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2399423
    const/4 v0, 0x3

    iput v0, p0, LX/GsN;->e:I

    .line 2399424
    const-string v0, "tag"

    invoke-static {p2, v0}, LX/Gsd;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2399425
    iput-object p1, p0, LX/GsN;->b:LX/GAb;

    .line 2399426
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "FacebookSDK."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GsN;->c:Ljava/lang/String;

    .line 2399427
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/GsN;->d:Ljava/lang/StringBuilder;

    .line 2399428
    return-void
.end method

.method public static a(LX/GAb;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2399414
    invoke-static {p0}, LX/GAK;->a(LX/GAb;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2399415
    invoke-static {p3}, LX/GsN;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2399416
    const-string v1, "FacebookSDK."

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2399417
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FacebookSDK."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    .line 2399418
    :cond_0
    invoke-static {p1, p2, v0}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 2399419
    sget-object v0, LX/GAb;->DEVELOPER_ERRORS:LX/GAb;

    if-ne p0, v0, :cond_1

    .line 2399420
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 2399421
    :cond_1
    return-void
.end method

.method public static a(LX/GAb;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2399412
    const/4 v0, 0x3

    invoke-static {p0, v0, p1, p2}, LX/GsN;->a(LX/GAb;ILjava/lang/String;Ljava/lang/String;)V

    .line 2399413
    return-void
.end method

.method public static varargs a(LX/GAb;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2399408
    invoke-static {p0}, LX/GAK;->a(LX/GAb;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2399409
    invoke-static {p2, p3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2399410
    const/4 v1, 0x3

    invoke-static {p0, v1, p1, v0}, LX/GsN;->a(LX/GAb;ILjava/lang/String;Ljava/lang/String;)V

    .line 2399411
    :cond_0
    return-void
.end method

.method public static declared-synchronized a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2399385
    const-class v1, LX/GsN;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/GAb;->INCLUDE_ACCESS_TOKENS:LX/GAb;

    invoke-static {v0}, LX/GAK;->a(LX/GAb;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2399386
    const-string v0, "ACCESS_TOKEN_REMOVED"

    invoke-static {p0, v0}, LX/GsN;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2399387
    :cond_0
    monitor-exit v1

    return-void

    .line 2399388
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2399405
    const-class v1, LX/GsN;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/GsN;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2399406
    monitor-exit v1

    return-void

    .line 2399407
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(LX/GsN;)Z
    .locals 1

    .prologue
    .line 2399404
    iget-object v0, p0, LX/GsN;->b:LX/GAb;

    invoke-static {v0}, LX/GAK;->a(LX/GAb;)Z

    move-result v0

    return v0
.end method

.method private static declared-synchronized c(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2399400
    const-class v2, LX/GsN;

    monitor-enter v2

    :try_start_0
    sget-object v0, LX/GsN;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2399401
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object p0

    goto :goto_0

    .line 2399402
    :cond_0
    monitor-exit v2

    return-object p0

    .line 2399403
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2399396
    iget-object v0, p0, LX/GsN;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2399397
    iget-object v1, p0, LX/GsN;->b:LX/GAb;

    iget v2, p0, LX/GsN;->e:I

    iget-object v3, p0, LX/GsN;->c:Ljava/lang/String;

    invoke-static {v1, v2, v3, v0}, LX/GsN;->a(LX/GAb;ILjava/lang/String;Ljava/lang/String;)V

    .line 2399398
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, LX/GsN;->d:Ljava/lang/StringBuilder;

    .line 2399399
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2399392
    const-string v0, "  %s:\t%s\n"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    .line 2399393
    invoke-static {p0}, LX/GsN;->b(LX/GsN;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2399394
    iget-object v2, p0, LX/GsN;->d:Ljava/lang/StringBuilder;

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2399395
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2399389
    invoke-static {p0}, LX/GsN;->b(LX/GsN;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2399390
    iget-object v0, p0, LX/GsN;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2399391
    :cond_0
    return-void
.end method
