.class public final LX/Fev;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;",
        ">;",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Few;


# direct methods
.method public constructor <init>(LX/Few;)V
    .locals 0

    .prologue
    .line 2266854
    iput-object p1, p0, LX/Fev;->a:LX/Few;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2266855
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2266856
    if-eqz p1, :cond_0

    .line 2266857
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2266858
    if-nez v0, :cond_1

    .line 2266859
    :cond_0
    sget-object v0, LX/1nY;->API_ERROR:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2266860
    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;

    .line 2266861
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2266862
    check-cast v0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;

    .line 2266863
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;->a()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;->a()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;->k()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;

    move-result-object v2

    if-nez v2, :cond_4

    .line 2266864
    :cond_2
    const/4 v2, 0x0

    .line 2266865
    :goto_1
    move-object v2, v2

    .line 2266866
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2266867
    check-cast v0, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;

    iget-object v3, p0, LX/Fev;->a:LX/Few;

    iget-object v3, v3, LX/Few;->e:LX/Fey;

    const/4 v6, 0x0

    const/4 p1, 0x1

    const/4 v8, 0x0

    .line 2266868
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;->a()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;->a()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;->k()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;->a()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;->k()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2266869
    :cond_3
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 2266870
    invoke-virtual {v3, v4, v8}, LX/Fey;->a(LX/0Px;Z)LX/0Px;

    move-result-object v4

    .line 2266871
    :goto_2
    move-object v0, v4

    .line 2266872
    invoke-direct {v1, v2, v0}, Lcom/facebook/photos/pandora/common/data/PandoraSlicedFeedResult;-><init>(Lcom/facebook/graphql/model/GraphQLPageInfo;LX/0Px;)V

    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;->a()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;->k()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoTailFieldsModel;

    move-result-object v2

    .line 2266873
    if-nez v2, :cond_5

    .line 2266874
    const/4 v3, 0x0

    .line 2266875
    :goto_3
    move-object v2, v3

    .line 2266876
    goto :goto_1

    .line 2266877
    :cond_5
    new-instance v3, LX/17L;

    invoke-direct {v3}, LX/17L;-><init>()V

    .line 2266878
    invoke-interface {v2}, LX/0ut;->a()Ljava/lang/String;

    move-result-object v0

    .line 2266879
    iput-object v0, v3, LX/17L;->c:Ljava/lang/String;

    .line 2266880
    invoke-interface {v2}, LX/0ut;->b()Z

    move-result v0

    .line 2266881
    iput-boolean v0, v3, LX/17L;->d:Z

    .line 2266882
    invoke-virtual {v3}, LX/17L;->a()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v3

    goto :goto_3

    .line 2266883
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;->a()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel$FilteredQueryModel;->k()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel;->a()LX/0Px;

    move-result-object v9

    .line 2266884
    new-instance v10, LX/0Pz;

    invoke-direct {v10}, LX/0Pz;-><init>()V

    .line 2266885
    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v11

    move v7, v8

    :goto_4
    if-ge v7, v11, :cond_a

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel;

    .line 2266886
    invoke-virtual {v4}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel;->j()Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel$NodeModel;

    move-result-object v12

    .line 2266887
    invoke-virtual {v4}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel;->k()LX/1vs;

    move-result-object v5

    iget v5, v5, LX/1vs;->b:I

    if-eqz v5, :cond_7

    .line 2266888
    invoke-virtual {v4}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel;->k()LX/1vs;

    move-result-object v5

    iget-object v13, v5, LX/1vs;->a:LX/15i;

    iget v5, v5, LX/1vs;->b:I

    .line 2266889
    invoke-virtual {v13, v5, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v5

    .line 2266890
    :goto_5
    invoke-virtual {v4}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel;->k()LX/1vs;

    move-result-object v13

    iget v13, v13, LX/1vs;->b:I

    if-eqz v13, :cond_8

    .line 2266891
    invoke-virtual {v4}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel;->k()LX/1vs;

    move-result-object v4

    iget-object v13, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    .line 2266892
    invoke-virtual {v13, v4, p1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    .line 2266893
    :goto_6
    invoke-virtual {v12}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel$NodeModel;->D()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v13

    if-nez v13, :cond_9

    .line 2266894
    new-instance v13, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;

    invoke-static {v12}, LX/Few;->a(Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel$NodeModel;)Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    move-result-object v12

    invoke-direct {v13, v12, v5, v4}, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;-><init>(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v13}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2266895
    :goto_7
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto :goto_4

    :cond_7
    move-object v5, v6

    .line 2266896
    goto :goto_5

    :cond_8
    move-object v4, v6

    .line 2266897
    goto :goto_6

    .line 2266898
    :cond_9
    new-instance v13, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;

    invoke-static {v12}, Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;->a(LX/8IH;)Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    move-result-object v12

    invoke-direct {v13, v12, v5, v4}, Lcom/facebook/photos/pandora/common/data/model/PandoraSingleMediaModel;-><init>(Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v13}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_7

    .line 2266899
    :cond_a
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    invoke-virtual {v3, v4, p1}, LX/Fey;->a(LX/0Px;Z)LX/0Px;

    move-result-object v4

    goto/16 :goto_2
.end method
