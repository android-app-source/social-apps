.class public final LX/GsL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/concurrent/Callable;

.field public final synthetic b:LX/GsM;


# direct methods
.method public constructor <init>(LX/GsM;Ljava/util/concurrent/Callable;)V
    .locals 0

    .prologue
    .line 2399375
    iput-object p1, p0, LX/GsL;->b:LX/GsM;

    iput-object p2, p0, LX/GsL;->a:Ljava/util/concurrent/Callable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2399376
    :try_start_0
    iget-object v0, p0, LX/GsL;->b:LX/GsM;

    iget-object v1, p0, LX/GsL;->a:Ljava/util/concurrent/Callable;

    invoke-interface {v1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v1

    .line 2399377
    iput-object v1, v0, LX/GsM;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2399378
    iget-object v0, p0, LX/GsL;->b:LX/GsM;

    iget-object v0, v0, LX/GsM;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 2399379
    const/4 v0, 0x0

    return-object v0

    .line 2399380
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/GsL;->b:LX/GsM;

    iget-object v1, v1, LX/GsM;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
.end method
