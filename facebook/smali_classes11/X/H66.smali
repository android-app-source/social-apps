.class public final LX/H66;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V
    .locals 0

    .prologue
    .line 2425824
    iput-object p1, p0, LX/H66;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    const/4 v0, 0x2

    const v1, 0x7b65b088

    invoke-static {v0, v4, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2425825
    iget-object v0, p0, LX/H66;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2425826
    iget-object v0, p0, LX/H66;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a:LX/H60;

    iget-object v1, p0, LX/H66;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v1}, LX/H83;->n()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/H66;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v2, v2, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    .line 2425827
    iget-object v3, v2, LX/H83;->b:LX/H70;

    move-object v2, v3

    .line 2425828
    iget-object v3, p0, LX/H66;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget v3, v3, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->at:I

    iget-object v5, p0, LX/H66;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v5, v5, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v5}, LX/H83;->q()Z

    move-result v5

    if-nez v5, :cond_0

    :goto_0
    iget-object v5, p0, LX/H66;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v5, v5, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->g:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, LX/H60;->a(Ljava/lang/String;LX/H70;IZLjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2425829
    iget-object v1, p0, LX/H66;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v1}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->B(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)LX/0TF;

    move-result-object v1

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2425830
    iget-object v0, p0, LX/H66;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v0, v6, v6}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a$redex0(Lcom/facebook/offers/fragment/OfferDetailPageFragment;ZZ)V

    .line 2425831
    :goto_1
    const v0, 0x3adcf75

    invoke-static {v0, v7}, LX/02F;->a(II)V

    return-void

    :cond_0
    move v4, v6

    .line 2425832
    goto :goto_0

    .line 2425833
    :cond_1
    iget-object v0, p0, LX/H66;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v1, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->a:LX/H60;

    iget-object v0, p0, LX/H66;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->w()LX/H72;

    move-result-object v0

    invoke-interface {v0}, LX/H71;->me_()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, LX/H66;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v3, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ax:Ljava/lang/String;

    iget-object v0, p0, LX/H66;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget v4, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->at:I

    iget-object v0, p0, LX/H66;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v0}, LX/H60;->a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2425834
    iget-object v1, p0, LX/H66;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    .line 2425835
    new-instance v2, LX/H67;

    invoke-direct {v2, v1}, LX/H67;-><init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    move-object v1, v2

    .line 2425836
    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    goto :goto_1
.end method
