.class public LX/FNG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/FNG;


# instance fields
.field public a:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/2Oq;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/2Uq;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2232351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2232352
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2232353
    iput-object v0, p0, LX/FNG;->d:LX/0Ot;

    .line 2232354
    invoke-static {}, LX/1Ab;->a()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, LX/FNG;->e:Ljava/util/Set;

    .line 2232355
    return-void
.end method

.method public static a(LX/0QB;)LX/FNG;
    .locals 7

    .prologue
    .line 2232357
    sget-object v0, LX/FNG;->g:LX/FNG;

    if-nez v0, :cond_1

    .line 2232358
    const-class v1, LX/FNG;

    monitor-enter v1

    .line 2232359
    :try_start_0
    sget-object v0, LX/FNG;->g:LX/FNG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2232360
    if-eqz v2, :cond_0

    .line 2232361
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2232362
    new-instance v6, LX/FNG;

    invoke-direct {v6}, LX/FNG;-><init>()V

    .line 2232363
    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/2Oq;->a(LX/0QB;)LX/2Oq;

    move-result-object v4

    check-cast v4, LX/2Oq;

    invoke-static {v0}, LX/2Uq;->a(LX/0QB;)LX/2Uq;

    move-result-object v5

    check-cast v5, LX/2Uq;

    const/16 p0, 0x2e3

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2232364
    iput-object v3, v6, LX/FNG;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v4, v6, LX/FNG;->b:LX/2Oq;

    iput-object v5, v6, LX/FNG;->c:LX/2Uq;

    iput-object p0, v6, LX/FNG;->d:LX/0Ot;

    .line 2232365
    move-object v0, v6

    .line 2232366
    sput-object v0, LX/FNG;->g:LX/FNG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2232367
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2232368
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2232369
    :cond_1
    sget-object v0, LX/FNG;->g:LX/FNG;

    return-object v0

    .line 2232370
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2232371
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 2232356
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, LX/FNG;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
