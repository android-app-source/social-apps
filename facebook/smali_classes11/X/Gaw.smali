.class public LX/Gaw;
.super LX/4ok;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field public final a:LX/03U;

.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;


# direct methods
.method public constructor <init>(LX/0Or;LX/03U;Lcom/facebook/prefs/shared/FbSharedPreferences;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/Context;",
            ">;",
            "LX/03U;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2368744
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {p0, v0}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2368745
    iput-object p2, p0, LX/Gaw;->a:LX/03U;

    .line 2368746
    iput-object p3, p0, LX/Gaw;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2368747
    sget-object v0, LX/0eJ;->g:LX/0Tn;

    invoke-virtual {v0}, LX/0To;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Gaw;->setKey(Ljava/lang/String;)V

    .line 2368748
    const-string v0, "Disable certain employee only features that cause performance problems. An app restart is needed for this to take effect"

    invoke-virtual {p0, v0}, LX/Gaw;->setSummary(Ljava/lang/CharSequence;)V

    .line 2368749
    const-string v0, "Non-employee mode"

    invoke-virtual {p0, v0}, LX/Gaw;->setTitle(Ljava/lang/CharSequence;)V

    .line 2368750
    new-instance v0, LX/Gav;

    invoke-direct {v0, p0}, LX/Gav;-><init>(LX/Gaw;)V

    invoke-virtual {p0, v0}, LX/Gaw;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2368751
    return-void
.end method

.method public static a(LX/0QB;)LX/Gaw;
    .locals 1

    .prologue
    .line 2368752
    invoke-static {p0}, LX/Gaw;->b(LX/0QB;)LX/Gaw;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/Gaw;
    .locals 4

    .prologue
    .line 2368753
    new-instance v2, LX/Gaw;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getProvider(Ljava/lang/Class;)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v0

    check-cast v0, LX/03U;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v1

    check-cast v1, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-direct {v2, v3, v0, v1}, LX/Gaw;-><init>(LX/0Or;LX/03U;Lcom/facebook/prefs/shared/FbSharedPreferences;)V

    .line 2368754
    return-object v2
.end method


# virtual methods
.method public final init()V
    .locals 2

    .prologue
    .line 2368755
    iget-object v0, p0, LX/Gaw;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    .line 2368756
    sget-object v1, LX/0eJ;->g:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 2368757
    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2368758
    return-void
.end method
