.class public final LX/Gm5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Gm7;


# direct methods
.method public constructor <init>(LX/Gm7;)V
    .locals 0

    .prologue
    .line 2391570
    iput-object p1, p0, LX/Gm5;->a:LX/Gm7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x1cbc4ba1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2391571
    iget-object v1, p0, LX/Gm5;->a:LX/Gm7;

    iget-object v1, v1, LX/Gm7;->d:Lcom/facebook/growth/contactinviter/ContactInviterFragment;

    if-eqz v1, :cond_0

    .line 2391572
    iget-object v1, p0, LX/Gm5;->a:LX/Gm7;

    iget-object v1, v1, LX/Gm7;->d:Lcom/facebook/growth/contactinviter/ContactInviterFragment;

    iget-object v2, p0, LX/Gm5;->a:LX/Gm7;

    iget-object v2, v2, LX/Gm7;->c:LX/GmD;

    .line 2391573
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "smsto:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v2, LX/GmD;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 2391574
    new-instance v5, Landroid/content/Intent;

    const-string p0, "android.intent.action.SENDTO"

    invoke-direct {v5, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2391575
    invoke-virtual {v5, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2391576
    const-string v4, "sms_body"

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "fb.com/inv?r="

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, LX/GmD;->a()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5, v4, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2391577
    iget-object v4, v1, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->b:Lcom/facebook/content/SecureContextHelper;

    const/16 p0, 0xc7

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    invoke-interface {v4, v5, p0, p1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2391578
    sget-object v4, LX/GmG;->SMS:LX/GmG;

    iget-object v5, v2, LX/GmD;->b:Ljava/lang/String;

    invoke-static {v1, v4, v5}, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->a(Lcom/facebook/growth/contactinviter/ContactInviterFragment;LX/GmG;Ljava/lang/String;)V

    .line 2391579
    iget-object v4, v1, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->g:LX/0if;

    sget-object v5, LX/0ig;->X:LX/0ih;

    const-string p0, "contact_invite_list_item_add_sms_button_click"

    invoke-virtual {v4, v5, p0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2391580
    :cond_0
    const v1, -0x592753bb

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
