.class public final LX/GTo;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLInterfaces$BackgroundLocationNuxFriendsSharingLocation;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;Z)V
    .locals 0

    .prologue
    .line 2355887
    iput-object p1, p0, LX/GTo;->b:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iput-boolean p2, p0, LX/GTo;->a:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 2355857
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;->a()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;->a()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 2355858
    :cond_0
    iget-object v0, p0, LX/GTo;->b:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-boolean v1, p0, LX/GTo;->a:Z

    .line 2355859
    invoke-static {v0, v1}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->c$redex0(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;Z)V

    .line 2355860
    :goto_0
    return-void

    .line 2355861
    :cond_1
    iget-object v0, p0, LX/GTo;->b:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;->a()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;->b()LX/0Px;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->a(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;LX/0Px;)LX/0Px;

    move-result-object v0

    .line 2355862
    iget-boolean v1, p0, LX/GTo;->a:Z

    if-nez v1, :cond_2

    .line 2355863
    iget-object v1, p0, LX/GTo;->b:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->O:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2355864
    iget-object v1, p0, LX/GTo;->b:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->P:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2355865
    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;->a()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel$FriendsSharingLocationConnectionModel;->a()I

    move-result v1

    .line 2355866
    iget-object v2, p0, LX/GTo;->b:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v2, v2, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->P:Landroid/widget/TextView;

    iget-object v3, p0, LX/GTo;->b:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v3, v3, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->q:LX/GVF;

    const v4, 0x7f0a00d2

    invoke-virtual {v3, v1, v0, v4}, LX/GVF;->a(ILX/0Px;I)Landroid/text/SpannableString;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2355867
    :cond_2
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 2355868
    iget-boolean v2, p0, LX/GTo;->a:Z

    if-eqz v2, :cond_6

    .line 2355869
    iget-object v2, p0, LX/GTo;->b:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-virtual {v2}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b23a5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2355870
    iget-object v3, p0, LX/GTo;->b:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-virtual {v3}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b23a6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2355871
    invoke-virtual {v1, v5, v2, v5, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 2355872
    :goto_1
    iget-object v2, p0, LX/GTo;->b:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v2, v2, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->I:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v2, v1}, Lcom/facebook/fbui/facepile/FacepileView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2355873
    iget-object v1, p0, LX/GTo;->b:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->I:Lcom/facebook/fbui/facepile/FacepileView;

    const/4 v3, 0x5

    const/4 v4, 0x0

    .line 2355874
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    if-le v2, v3, :cond_3

    .line 2355875
    invoke-virtual {v0, v4, v3}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v0

    .line 2355876
    :cond_3
    new-instance v6, Ljava/util/ArrayList;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    invoke-direct {v6, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2355877
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v7

    move v3, v4

    :goto_2
    if-ge v3, v7, :cond_5

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;

    .line 2355878
    if-eqz v2, :cond_4

    .line 2355879
    invoke-virtual {v1}, Lcom/facebook/fbui/facepile/FacepileView;->getContext()Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f021089

    invoke-static {v8, v9}, LX/1ED;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    .line 2355880
    invoke-virtual {v2}, Lcom/facebook/backgroundlocation/upsell/graphql/BackgroundLocationUpsellGraphQLModels$BackgroundLocationUpsellProfileModel;->d()LX/1vs;

    move-result-object v2

    iget-object v9, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    new-instance p1, LX/6UY;

    invoke-virtual {v9, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {p1, v2, v8}, LX/6UY;-><init>(Landroid/net/Uri;Landroid/graphics/drawable/Drawable;)V

    invoke-interface {v6, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2355881
    :cond_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 2355882
    :cond_5
    invoke-static {v6}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/facepile/FacepileView;->setFaces(Ljava/util/List;)V

    .line 2355883
    iget-object v0, p0, LX/GTo;->b:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->I:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    goto/16 :goto_0

    .line 2355884
    :cond_6
    iget-object v2, p0, LX/GTo;->b:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-virtual {v2}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b2396

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 2355885
    iget-object v3, p0, LX/GTo;->b:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-virtual {v3}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b2397

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2355886
    invoke-virtual {v1, v5, v2, v5, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    goto/16 :goto_1
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2355851
    iget-object v0, p0, LX/GTo;->b:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-boolean v1, p0, LX/GTo;->a:Z

    .line 2355852
    invoke-static {v0, v1}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->c$redex0(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;Z)V

    .line 2355853
    sget-object v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->B:Ljava/lang/Class;

    const-string v1, "failed to get upsell data"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2355854
    iget-object v0, p0, LX/GTo;->b:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->w:LX/03V;

    const-string v1, "background_location_one_nux_upsell_data_fetch_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2355855
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2355856
    check-cast p1, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    invoke-direct {p0, p1}, LX/GTo;->a(Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;)V

    return-void
.end method
