.class public LX/Fqw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0po;
.implements LX/0sj;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/Fqw;


# instance fields
.field public final a:LX/Fqv;
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final b:LX/0SG;

.field private final c:LX/0ox;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0t2;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Sy;

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0ad;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fr1;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/15j;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Fqv;LX/0SG;LX/0ox;LX/0Ot;LX/0Sy;LX/0Ot;LX/0pq;LX/0ad;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Fqv;",
            "LX/0SG;",
            "LX/0ox;",
            "LX/0Ot",
            "<",
            "LX/0t2;",
            ">;",
            "Lcom/facebook/common/userinteraction/UserInteractionController;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0pq;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/Fr1;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2294662
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2294663
    iput-object p1, p0, LX/Fqw;->a:LX/Fqv;

    .line 2294664
    iput-object p2, p0, LX/Fqw;->b:LX/0SG;

    .line 2294665
    iput-object p3, p0, LX/Fqw;->c:LX/0ox;

    .line 2294666
    iput-object p4, p0, LX/Fqw;->d:LX/0Ot;

    .line 2294667
    iput-object p5, p0, LX/Fqw;->e:LX/0Sy;

    .line 2294668
    iput-object p6, p0, LX/Fqw;->f:LX/0Ot;

    .line 2294669
    iput-object p9, p0, LX/Fqw;->h:LX/0Or;

    .line 2294670
    invoke-virtual {p7, p0}, LX/0pq;->a(LX/0po;)V

    .line 2294671
    iput-object p8, p0, LX/Fqw;->g:LX/0ad;

    .line 2294672
    return-void
.end method

.method public static a(LX/0QB;)LX/Fqw;
    .locals 13

    .prologue
    .line 2294820
    sget-object v0, LX/Fqw;->j:LX/Fqw;

    if-nez v0, :cond_1

    .line 2294821
    const-class v1, LX/Fqw;

    monitor-enter v1

    .line 2294822
    :try_start_0
    sget-object v0, LX/Fqw;->j:LX/Fqw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2294823
    if-eqz v2, :cond_0

    .line 2294824
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2294825
    new-instance v3, LX/Fqw;

    invoke-static {v0}, LX/Fqv;->a(LX/0QB;)LX/Fqv;

    move-result-object v4

    check-cast v4, LX/Fqv;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v5

    check-cast v5, LX/0SG;

    const-class v6, LX/0ox;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/0ox;

    const/16 v7, 0xb12

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    invoke-static {v0}, LX/0Sy;->a(LX/0QB;)LX/0Sy;

    move-result-object v8

    check-cast v8, LX/0Sy;

    const/16 v9, 0x259

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-static {v0}, LX/0pq;->a(LX/0QB;)LX/0pq;

    move-result-object v10

    check-cast v10, LX/0pq;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v11

    check-cast v11, LX/0ad;

    const/16 v12, 0x3626

    invoke-static {v0, v12}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v12

    invoke-direct/range {v3 .. v12}, LX/Fqw;-><init>(LX/Fqv;LX/0SG;LX/0ox;LX/0Ot;LX/0Sy;LX/0Ot;LX/0pq;LX/0ad;LX/0Or;)V

    .line 2294826
    move-object v0, v3

    .line 2294827
    sput-object v0, LX/Fqw;->j:LX/Fqw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2294828
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2294829
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2294830
    :cond_1
    sget-object v0, LX/Fqw;->j:LX/Fqw;

    return-object v0

    .line 2294831
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2294832
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;LX/0zO;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "LX/0zO",
            "<*>;[",
            "Ljava/lang/String;",
            ")",
            "Landroid/database/Cursor;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 2294817
    const-string v1, "cache"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/Fqt;->a:LX/0U1;

    .line 2294818
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2294819
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "= ?"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v0, p0, LX/Fqw;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0t2;

    invoke-virtual {p2, v0}, LX/0zO;->a(LX/0t2;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    move-object v0, p1

    move-object v2, p3

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private a([BLX/0w5;)Lcom/facebook/flatbuffers/Flattenable;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2294814
    new-instance v0, LX/15i;

    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    const/4 v4, 0x0

    invoke-static {p0}, LX/Fqw;->c(LX/Fqw;)LX/15j;

    move-result-object v5

    move-object v3, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    invoke-virtual {p2, v0}, LX/0w5;->b(LX/15i;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    .line 2294815
    const-string v1, "TimelineDbCache"

    invoke-static {v0, v1}, LX/1k0;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2294816
    return-object v0
.end method

.method private a(D)Ljava/lang/String;
    .locals 13
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2294785
    invoke-direct {p0}, LX/Fqw;->g()I

    move-result v8

    .line 2294786
    invoke-direct {p0}, LX/Fqw;->f()D

    move-result-wide v6

    .line 2294787
    const-string v1, "No deletion occurred"

    .line 2294788
    const/4 v0, 0x0

    .line 2294789
    iget-object v2, p0, LX/Fqw;->a:LX/Fqv;

    invoke-virtual {v2}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v9

    move-wide v4, v6

    move v2, v8

    .line 2294790
    :goto_0
    const-wide v10, 0x3f847ae147ae147bL    # 0.01

    add-double/2addr v10, p1

    cmpl-double v3, v4, v10

    if-ltz v3, :cond_1

    if-lez v2, :cond_1

    const/4 v3, 0x2

    if-ge v0, v3, :cond_1

    const-wide/16 v10, 0x0

    cmpl-double v3, v4, v10

    if-lez v3, :cond_1

    .line 2294791
    add-int/lit8 v3, v0, 0x1

    .line 2294792
    int-to-double v0, v2

    mul-double/2addr v0, p1

    div-double/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    sub-int v0, v2, v0

    .line 2294793
    if-ne v0, v2, :cond_0

    .line 2294794
    invoke-static {}, LX/Fqw;->d()Ljava/lang/String;

    move-result-object v1

    .line 2294795
    iget-object v0, p0, LX/Fqw;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fr1;

    invoke-virtual {v0, v9}, LX/Fr1;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    move-object v0, v1

    .line 2294796
    :goto_1
    const v1, 0x40500f57

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {v9, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v1, 0x4f92ce32

    invoke-static {v1}, LX/03h;->a(I)V

    .line 2294797
    invoke-direct {p0}, LX/Fqw;->g()I

    move-result v1

    .line 2294798
    invoke-direct {p0}, LX/Fqw;->f()D

    move-result-wide v4

    move v2, v1

    move-object v1, v0

    move v0, v3

    .line 2294799
    goto :goto_0

    .line 2294800
    :cond_0
    add-int/lit8 v0, v0, -0x1

    .line 2294801
    if-gez v0, :cond_3

    .line 2294802
    const/4 v0, 0x0

    move v2, v0

    .line 2294803
    :goto_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DELETE FROM cache WHERE "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, LX/Fqt;->b:LX/0U1;

    .line 2294804
    iget-object v4, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v4

    .line 2294805
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " <= ( SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/Fqt;->b:LX/0U1;

    .line 2294806
    iget-object v4, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v4

    .line 2294807
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM cache"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ORDER BY "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, LX/Fqt;->b:LX/0U1;

    .line 2294808
    iget-object v4, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v4

    .line 2294809
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ASC LIMIT 1 OFFSET "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2294810
    iget-object v0, p0, LX/Fqw;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fr1;

    invoke-virtual {v0, v9, v2}, LX/Fr1;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    move-object v0, v1

    goto :goto_1

    .line 2294811
    :cond_1
    if-lez v0, :cond_2

    if-eqz v9, :cond_2

    .line 2294812
    const-string v0, "VACUUM"

    const v3, -0x4e98799b

    invoke-static {v3}, LX/03h;->a(I)V

    invoke-virtual {v9, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x5fc19744

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2294813
    :cond_2
    const-string v0, "TimelineDbCache.purge() initial state: numRows=%d dbPercentage=%f   Final state: numRows=%d dbPercentage=%f   Final deletion command: [%s]  "

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v3, v9

    const/4 v8, 0x1

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v3, v8

    const/4 v6, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v6

    const/4 v2, 0x3

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v2, 0x4

    aput-object v1, v3, v2

    invoke-static {v0, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3
    move v2, v0

    goto/16 :goto_2
.end method

.method private a(LX/0zO;Lcom/facebook/flatbuffers/Flattenable;)Z
    .locals 12
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zO",
            "<*>;",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2294753
    iget-object v0, p0, LX/Fqw;->e:LX/0Sy;

    invoke-virtual {v0}, LX/0Sy;->c()V

    .line 2294754
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 2294755
    sget-object v0, LX/Fqt;->a:LX/0U1;

    .line 2294756
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v4, v4

    .line 2294757
    iget-object v0, p0, LX/Fqw;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0t2;

    invoke-virtual {p1, v0}, LX/0zO;->a(LX/0t2;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2294758
    sget-object v0, LX/Fqt;->b:LX/0U1;

    .line 2294759
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v4

    .line 2294760
    iget-object v4, p0, LX/Fqw;->b:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v5, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2294761
    sget-object v0, LX/Fqt;->c:LX/0U1;

    .line 2294762
    iget-object v4, v0, LX/0U1;->d:Ljava/lang/String;

    move-object v0, v4

    .line 2294763
    invoke-static {p2}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;)[B

    move-result-object v4

    invoke-virtual {v5, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 2294764
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 2294765
    iget-object v0, p0, LX/Fqw;->a:LX/Fqv;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v7

    move v4, v1

    .line 2294766
    :goto_0
    const/4 v0, 0x3

    if-ge v4, v0, :cond_0

    .line 2294767
    :try_start_0
    const-string v0, "cache"

    const/4 v8, 0x0

    const v9, -0x70d34d6f

    invoke-static {v9}, LX/03h;->a(I)V

    invoke-virtual {v7, v0, v8, v5}, Landroid/database/sqlite/SQLiteDatabase;->replaceOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, 0x79a31c2e

    invoke-static {v0}, LX/03h;->a(I)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v3

    .line 2294768
    :cond_0
    if-le v4, v3, :cond_1

    .line 2294769
    iget-object v0, p0, LX/Fqw;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v3, "TimelineDbCache"

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_1
    move v0, v1

    .line 2294770
    :goto_1
    return v0

    .line 2294771
    :catch_0
    move-exception v0

    .line 2294772
    const-wide/16 v8, 0x1e

    :try_start_1
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_3

    .line 2294773
    :goto_2
    const-string v2, " saveToDb attempt #"

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, " failed: "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, " "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2294774
    packed-switch v4, :pswitch_data_0

    .line 2294775
    :goto_3
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move-object v2, v0

    goto :goto_0

    .line 2294776
    :pswitch_0
    instance-of v2, v0, Landroid/database/sqlite/SQLiteFullException;

    if-eqz v2, :cond_2

    invoke-direct {p0}, LX/Fqw;->e()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v2, v8, v10

    if-nez v2, :cond_2

    move v0, v1

    .line 2294777
    goto :goto_1

    .line 2294778
    :cond_2
    const-wide v8, 0x3fe3333333333333L    # 0.6

    :try_start_2
    invoke-direct {p0, v8, v9}, LX/Fqw;->a(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 2294779
    :catch_1
    move-exception v2

    .line 2294780
    const-string v8, " Failed to purge table: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 2294781
    :pswitch_1
    :try_start_3
    const-string v2, "cache"

    const-string v8, "1"

    const/4 v9, 0x0

    invoke-virtual {v7, v2, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 2294782
    const-string v8, " Successfully deleted table with "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, " rows. "

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_3

    .line 2294783
    :catch_2
    move-exception v2

    .line 2294784
    const-string v8, " Failed to delete table: "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :catch_3
    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static declared-synchronized c(LX/Fqw;)LX/15j;
    .locals 2

    .prologue
    .line 2294749
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Fqw;->i:LX/15j;

    if-nez v0, :cond_0

    .line 2294750
    iget-object v0, p0, LX/Fqw;->c:LX/0ox;

    sget-object v1, LX/BQO;->d:LX/0Tn;

    invoke-virtual {v0, v1}, LX/0ox;->a(LX/0Tn;)LX/15j;

    move-result-object v0

    iput-object v0, p0, LX/Fqw;->i:LX/15j;

    .line 2294751
    :cond_0
    iget-object v0, p0, LX/Fqw;->i:LX/15j;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 2294752
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2294748
    const-string v0, "DELETE FROM cache"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private e()J
    .locals 2

    .prologue
    .line 2294746
    :try_start_0
    iget-object v0, p0, LX/Fqw;->a:LX/Fqv;

    invoke-virtual {v0}, LX/0Tr;->e()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 2294747
    :goto_0
    return-wide v0

    :catch_0
    iget-object v0, p0, LX/Fqw;->a:LX/Fqv;

    invoke-virtual {v0}, LX/Fqv;->c()J

    move-result-wide v0

    goto :goto_0
.end method

.method private f()D
    .locals 4

    .prologue
    .line 2294745
    invoke-direct {p0}, LX/Fqw;->e()J

    move-result-wide v0

    long-to-double v0, v0

    iget-object v2, p0, LX/Fqw;->a:LX/Fqv;

    invoke-virtual {v2}, LX/Fqv;->c()J

    move-result-wide v2

    long-to-double v2, v2

    div-double/2addr v0, v2

    return-wide v0
.end method

.method private g()I
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2294733
    const/4 v0, -0x1

    .line 2294734
    :try_start_0
    iget-object v2, p0, LX/Fqw;->a:LX/Fqv;

    invoke-virtual {v2}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2294735
    const-string v3, "SELECT Count(*) FROM cache"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2294736
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2294737
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2294738
    if-eqz v1, :cond_0

    .line 2294739
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2294740
    :cond_0
    :goto_0
    return v0

    .line 2294741
    :catch_0
    if-eqz v1, :cond_0

    .line 2294742
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2294743
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 2294744
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method


# virtual methods
.method public final U_()V
    .locals 4

    .prologue
    .line 2294730
    invoke-direct {p0}, LX/Fqw;->f()D

    move-result-wide v0

    const-wide v2, 0x3fc999999999999aL    # 0.2

    cmpl-double v0, v0, v2

    if-lez v0, :cond_0

    .line 2294731
    const-wide v0, 0x3fb999999999999aL    # 0.1

    invoke-direct {p0, v0, v1}, LX/Fqw;->a(D)Ljava/lang/String;

    .line 2294732
    :cond_0
    return-void
.end method

.method public final a(LX/0zO;Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 2294727
    iget-object v0, p2, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2294728
    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-direct {p0, p1, v0}, LX/Fqw;->a(LX/0zO;Lcom/facebook/flatbuffers/Flattenable;)Z

    .line 2294729
    return-void
.end method

.method public final b(LX/0zO;)Lcom/facebook/graphql/executor/GraphQLResult;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;)",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2294694
    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    sget-object v1, LX/Fqt;->b:LX/0U1;

    .line 2294695
    iget-object v4, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v4

    .line 2294696
    aput-object v1, v3, v0

    const/4 v0, 0x1

    sget-object v1, LX/Fqt;->c:LX/0U1;

    .line 2294697
    iget-object v4, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v4

    .line 2294698
    aput-object v1, v3, v0

    .line 2294699
    iget-object v0, p0, LX/Fqw;->a:LX/Fqv;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    .line 2294700
    const-wide/16 v0, 0x0

    .line 2294701
    :try_start_0
    invoke-direct {p0, v4, p1, v3}, LX/Fqw;->a(Landroid/database/sqlite/SQLiteDatabase;LX/0zO;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v6

    .line 2294702
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    if-gtz v3, :cond_1

    .line 2294703
    if-eqz v6, :cond_0

    :try_start_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3

    :cond_0
    move-object v0, v2

    .line 2294704
    :goto_0
    return-object v0

    .line 2294705
    :cond_1
    :try_start_3
    sget-object v3, LX/Fqt;->c:LX/0U1;

    invoke-virtual {v3, v6}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v3

    .line 2294706
    sget-object v4, LX/Fqt;->b:LX/0U1;

    invoke-virtual {v4, v6}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v4

    .line 2294707
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 2294708
    invoke-interface {v6, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    .line 2294709
    invoke-interface {v6, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 2294710
    iget-object v4, p1, LX/0zO;->n:LX/0w5;

    move-object v4, v4

    .line 2294711
    invoke-direct {p0, v3, v4}, LX/Fqw;->a([BLX/0w5;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v3

    move-wide v4, v0

    .line 2294712
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2294713
    const-string v0, "TimelineDbCache"

    const-string v1, "Multiple rows in timeline db with same primary key!"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2294714
    :cond_2
    if-eqz v6, :cond_3

    :try_start_4
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 2294715
    :cond_3
    if-nez v3, :cond_6

    move-object v0, v2

    .line 2294716
    goto :goto_0

    .line 2294717
    :catch_0
    move-exception v0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2294718
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_2
    if-eqz v6, :cond_4

    if-eqz v1, :cond_5

    :try_start_6
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    :cond_4
    :goto_3
    :try_start_7
    throw v0
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    :catch_1
    move-exception v0

    move-object v1, v0

    .line 2294719
    :goto_4
    iget-object v0, p0, LX/Fqw;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v3, "TimelineDbCache"

    const-string v4, "fetchFromDb failed"

    invoke-virtual {v0, v3, v4, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v2

    .line 2294720
    goto :goto_0

    .line 2294721
    :catch_2
    move-exception v3

    :try_start_8
    invoke-static {v1, v3}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :catch_3
    move-exception v0

    move-object v1, v0

    goto :goto_4

    :cond_5
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_8
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    goto :goto_3

    .line 2294722
    :cond_6
    iget-object v0, p0, LX/Fqw;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    sub-long/2addr v0, v4

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2294723
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-wide v6, p1, LX/0zO;->c:J

    cmp-long v0, v0, v6

    if-gez v0, :cond_7

    sget-object v0, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    .line 2294724
    :goto_5
    new-instance v1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {v1, v3, v0, v4, v5}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;J)V

    move-object v0, v1

    goto/16 :goto_0

    .line 2294725
    :cond_7
    sget-object v0, LX/0ta;->FROM_CACHE_STALE:LX/0ta;

    goto :goto_5

    .line 2294726
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    :cond_8
    move-wide v4, v0

    move-object v3, v2

    goto :goto_1
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2294689
    iget-object v0, p0, LX/Fqw;->a:LX/Fqv;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2294690
    iget-object v0, p0, LX/Fqw;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fr1;

    invoke-virtual {v0, v1}, LX/Fr1;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2294691
    invoke-static {}, LX/Fqw;->d()Ljava/lang/String;

    move-result-object v0

    const v2, 0x586bfac0

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x34bd812b

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2294692
    const-string v0, "VACUUM"

    const v2, 0x10db163d

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x2ffffbe1

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2294693
    return-void
.end method

.method public final d(LX/0zO;)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2294673
    new-array v2, v0, [Ljava/lang/String;

    sget-object v3, LX/Fqt;->b:LX/0U1;

    .line 2294674
    iget-object v4, v3, LX/0U1;->d:Ljava/lang/String;

    move-object v3, v4

    .line 2294675
    aput-object v3, v2, v1

    .line 2294676
    iget-object v3, p0, LX/Fqw;->a:LX/Fqv;

    invoke-virtual {v3}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 2294677
    invoke-direct {p0, v3, p1, v2}, LX/Fqw;->a(Landroid/database/sqlite/SQLiteDatabase;LX/0zO;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    const/4 v2, 0x0

    .line 2294678
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2294679
    sget-object v4, LX/Fqt;->b:LX/0U1;

    invoke-virtual {v4, v3}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 2294680
    iget-object v6, p0, LX/Fqw;->b:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v6

    sub-long v4, v6, v4

    .line 2294681
    iget-wide v6, p1, LX/0zO;->c:J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    cmp-long v2, v4, v6

    if-gez v2, :cond_1

    .line 2294682
    :goto_0
    if-eqz v3, :cond_0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 2294683
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v0, v1

    .line 2294684
    goto :goto_0

    .line 2294685
    :cond_2
    if-eqz v3, :cond_3

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_3
    move v0, v1

    .line 2294686
    goto :goto_1

    .line 2294687
    :catch_0
    move-exception v0

    :try_start_1
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2294688
    :catchall_0
    move-exception v1

    move-object v8, v1

    move-object v1, v0

    move-object v0, v8

    :goto_2
    if-eqz v3, :cond_4

    if-eqz v1, :cond_5

    :try_start_2
    invoke-interface {v3}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    :cond_4
    :goto_3
    throw v0

    :catch_1
    move-exception v2

    invoke-static {v1, v2}, LX/00r;->addSuppressed(Ljava/lang/Throwable;Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_5
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_3

    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method
