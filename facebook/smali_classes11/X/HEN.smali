.class public final LX/HEN;
.super LX/CXr;
.source ""


# instance fields
.field public final synthetic b:LX/HEQ;


# direct methods
.method public constructor <init>(LX/HEQ;Landroid/os/ParcelUuid;)V
    .locals 0

    .prologue
    .line 2441981
    iput-object p1, p0, LX/HEN;->b:LX/HEQ;

    invoke-direct {p0, p2}, LX/CXr;-><init>(Landroid/os/ParcelUuid;)V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 2441982
    check-cast p1, LX/CXq;

    const v3, 0x130080

    .line 2441983
    iget-object v0, p0, LX/HEN;->b:LX/HEQ;

    const/4 v1, 0x1

    iget-object v2, p1, LX/CXq;->c:LX/CXp;

    invoke-virtual {v2}, LX/CXp;->ordinal()I

    move-result v2

    shl-int/2addr v1, v2

    .line 2441984
    iget v2, v0, LX/HEQ;->f:I

    or-int/2addr v2, v1

    iput v2, v0, LX/HEQ;->f:I

    .line 2441985
    iget-object v0, p0, LX/HEN;->b:LX/HEQ;

    iget-object v1, p1, LX/CXq;->c:LX/CXp;

    .line 2441986
    iput-object v1, v0, LX/HEQ;->l:LX/CXp;

    .line 2441987
    sget-object v0, LX/HEO;->a:[I

    iget-object v1, p1, LX/CXq;->c:LX/CXp;

    invoke-virtual {v1}, LX/CXp;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2441988
    :goto_0
    iget-object v0, p1, LX/CXq;->d:LX/0Px;

    if-eqz v0, :cond_2

    .line 2441989
    iget-object v2, p1, LX/CXq;->d:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2441990
    iget-object v4, p0, LX/HEN;->b:LX/HEQ;

    invoke-virtual {v4, v0}, LX/HEQ;->a(Ljava/lang/String;)V

    .line 2441991
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2441992
    :pswitch_0
    iget-object v0, p0, LX/HEN;->b:LX/HEQ;

    iget-object v0, v0, LX/HEQ;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/HEN;->b:LX/HEQ;

    iget-object v1, v1, LX/HEQ;->j:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/16 v2, 0xb5

    invoke-interface {v0, v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    goto :goto_0

    .line 2441993
    :pswitch_1
    iget-object v0, p0, LX/HEN;->b:LX/HEQ;

    iget-object v0, v0, LX/HEQ;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/HEN;->b:LX/HEQ;

    iget-object v1, v1, LX/HEQ;->j:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/16 v2, 0xb6

    invoke-interface {v0, v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    goto :goto_0

    .line 2441994
    :pswitch_2
    iget-object v0, p0, LX/HEN;->b:LX/HEQ;

    iget-object v0, v0, LX/HEQ;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/HEN;->b:LX/HEQ;

    iget-object v1, v1, LX/HEQ;->j:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/16 v2, 0x5a

    invoke-interface {v0, v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    goto :goto_0

    .line 2441995
    :pswitch_3
    iget-object v0, p0, LX/HEN;->b:LX/HEQ;

    iget-object v0, v0, LX/HEQ;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/HEN;->b:LX/HEQ;

    iget-object v1, v1, LX/HEQ;->j:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/16 v2, 0x68

    invoke-interface {v0, v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    goto :goto_0

    .line 2441996
    :pswitch_4
    iget-object v0, p0, LX/HEN;->b:LX/HEQ;

    .line 2441997
    iget-object v1, p1, LX/CXq;->b:LX/0am;

    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, LX/CXq;->b:LX/0am;

    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v2, LX/0ta;->NO_DATA:LX/0ta;

    if-eq v1, v2, :cond_1

    .line 2441998
    :cond_0
    iget-object v1, v0, LX/HEQ;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x130080

    iget-object v3, v0, LX/HEQ;->j:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    const/16 v4, 0xb4

    invoke-interface {v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 2441999
    const-string v1, "HasCta"

    invoke-virtual {v0, v1}, LX/HEQ;->a(Ljava/lang/String;)V

    .line 2442000
    :cond_1
    goto/16 :goto_0

    .line 2442001
    :pswitch_5
    iget-object v0, p0, LX/HEN;->b:LX/HEQ;

    iget-object v0, v0, LX/HEQ;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/HEN;->b:LX/HEQ;

    iget-object v1, v1, LX/HEQ;->j:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/16 v2, 0xb3

    invoke-interface {v0, v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    goto/16 :goto_0

    .line 2442002
    :cond_2
    iget-object v0, p0, LX/HEN;->b:LX/HEQ;

    .line 2442003
    iget v1, v0, LX/HEQ;->e:I

    iget v2, v0, LX/HEQ;->f:I

    and-int/2addr v1, v2

    iget v2, v0, LX/HEQ;->e:I

    if-ne v1, v2, :cond_4

    .line 2442004
    iget-object v1, v0, LX/HEQ;->l:LX/CXp;

    if-eqz v1, :cond_3

    .line 2442005
    const-string v1, "LastEvent"

    iget-object v2, v0, LX/HEQ;->l:LX/CXp;

    invoke-virtual {v2}, LX/CXp;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/HEQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2442006
    :cond_3
    iget-object v1, v0, LX/HEQ;->k:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x0

    .line 2442007
    :goto_2
    iget-object v3, v0, LX/HEQ;->c:LX/0id;

    iget-object v2, v0, LX/HEQ;->i:LX/03R;

    sget-object v4, LX/03R;->YES:LX/03R;

    if-ne v2, v4, :cond_6

    const-string v2, "LoadPageHeaderAdmin"

    :goto_3
    invoke-virtual {v3, v2, v1}, LX/0id;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2442008
    iget-object v1, v0, LX/HEQ;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x130080

    iget-object v3, v0, LX/HEQ;->j:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    const/4 v4, 0x2

    invoke-interface {v1, v2, v3, v4}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2442009
    iget-object v1, v0, LX/HEQ;->d:LX/8E2;

    iget-object v2, v0, LX/HEQ;->h:Ljava/lang/String;

    sget-object v3, LX/8E7;->PAGES_HEADER_PERF_LOGGING_STOPPED:LX/8E7;

    invoke-static {v3}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/8E2;->a(Ljava/lang/String;LX/0Rf;)V

    .line 2442010
    :cond_4
    return-void

    .line 2442011
    :cond_5
    iget-object v1, v0, LX/HEQ;->k:Ljava/util/Map;

    invoke-static {v1}, LX/0P1;->copyOf(Ljava/util/Map;)LX/0P1;

    move-result-object v1

    goto :goto_2

    .line 2442012
    :cond_6
    const-string v2, "LoadPageHeaderNonAdmin"

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
