.class public final LX/FXJ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 18

    .prologue
    .line 2254684
    const/4 v14, 0x0

    .line 2254685
    const-wide/16 v12, 0x0

    .line 2254686
    const/4 v11, 0x0

    .line 2254687
    const/4 v10, 0x0

    .line 2254688
    const/4 v9, 0x0

    .line 2254689
    const/4 v8, 0x0

    .line 2254690
    const/4 v7, 0x0

    .line 2254691
    const/4 v6, 0x0

    .line 2254692
    const/4 v5, 0x0

    .line 2254693
    const/4 v4, 0x0

    .line 2254694
    const/4 v3, 0x0

    .line 2254695
    const/4 v2, 0x0

    .line 2254696
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_e

    .line 2254697
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2254698
    const/4 v2, 0x0

    .line 2254699
    :goto_0
    return v2

    .line 2254700
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v7, :cond_c

    .line 2254701
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2254702
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2254703
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v17, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v17

    if-eq v7, v0, :cond_0

    if-eqz v3, :cond_0

    .line 2254704
    const-string v7, "attribution_text"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 2254705
    invoke-static/range {p0 .. p1}, LX/FX9;->a(LX/15w;LX/186;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 2254706
    :cond_1
    const-string v7, "creation_time"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2254707
    const/4 v2, 0x1

    .line 2254708
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto :goto_1

    .line 2254709
    :cond_2
    const-string v7, "global_share"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2254710
    invoke-static/range {p0 .. p1}, LX/FXB;->a(LX/15w;LX/186;)I

    move-result v3

    move/from16 v16, v3

    goto :goto_1

    .line 2254711
    :cond_3
    const-string v7, "image"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 2254712
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v3

    move v15, v3

    goto :goto_1

    .line 2254713
    :cond_4
    const-string v7, "node"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2254714
    invoke-static/range {p0 .. p1}, LX/FX8;->a(LX/15w;LX/186;)I

    move-result v3

    move v14, v3

    goto :goto_1

    .line 2254715
    :cond_5
    const-string v7, "permalink_node"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2254716
    invoke-static/range {p0 .. p1}, LX/FXC;->a(LX/15w;LX/186;)I

    move-result v3

    move v13, v3

    goto :goto_1

    .line 2254717
    :cond_6
    const-string v7, "source_container"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2254718
    invoke-static/range {p0 .. p1}, LX/FXG;->a(LX/15w;LX/186;)I

    move-result v3

    move v12, v3

    goto :goto_1

    .line 2254719
    :cond_7
    const-string v7, "subtitle_text"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 2254720
    invoke-static/range {p0 .. p1}, LX/FXH;->a(LX/15w;LX/186;)I

    move-result v3

    move v11, v3

    goto/16 :goto_1

    .line 2254721
    :cond_8
    const-string v7, "title"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 2254722
    invoke-static/range {p0 .. p1}, LX/FXI;->a(LX/15w;LX/186;)I

    move-result v3

    move v10, v3

    goto/16 :goto_1

    .line 2254723
    :cond_9
    const-string v7, "url"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 2254724
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 2254725
    :cond_a
    const-string v7, "viewed_state"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 2254726
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedItemViewedState;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 2254727
    :cond_b
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2254728
    :cond_c
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2254729
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 2254730
    if-eqz v2, :cond_d

    .line 2254731
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2254732
    :cond_d
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2254733
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 2254734
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 2254735
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2254736
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2254737
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2254738
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2254739
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2254740
    const/16 v2, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2254741
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_e
    move v15, v10

    move/from16 v16, v11

    move v10, v5

    move v11, v6

    move v6, v14

    move v14, v9

    move v9, v4

    move-wide v4, v12

    move v13, v8

    move v12, v7

    move v8, v3

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/16 v3, 0xa

    .line 2254742
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2254743
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2254744
    if-eqz v0, :cond_1

    .line 2254745
    const-string v1, "attribution_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254746
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2254747
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2254748
    if-eqz v1, :cond_0

    .line 2254749
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254750
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2254751
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2254752
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2254753
    cmp-long v2, v0, v4

    if-eqz v2, :cond_2

    .line 2254754
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254755
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2254756
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2254757
    if-eqz v0, :cond_3

    .line 2254758
    const-string v1, "global_share"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254759
    invoke-static {p0, v0, p2, p3}, LX/FXB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2254760
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2254761
    if-eqz v0, :cond_4

    .line 2254762
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254763
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2254764
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2254765
    if-eqz v0, :cond_5

    .line 2254766
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254767
    invoke-static {p0, v0, p2, p3}, LX/FX8;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2254768
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2254769
    if-eqz v0, :cond_6

    .line 2254770
    const-string v1, "permalink_node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254771
    invoke-static {p0, v0, p2}, LX/FXC;->a(LX/15i;ILX/0nX;)V

    .line 2254772
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2254773
    if-eqz v0, :cond_7

    .line 2254774
    const-string v1, "source_container"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254775
    invoke-static {p0, v0, p2, p3}, LX/FXG;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2254776
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2254777
    if-eqz v0, :cond_9

    .line 2254778
    const-string v1, "subtitle_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254779
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2254780
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2254781
    if-eqz v1, :cond_8

    .line 2254782
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254783
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2254784
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2254785
    :cond_9
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2254786
    if-eqz v0, :cond_b

    .line 2254787
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254788
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2254789
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2254790
    if-eqz v1, :cond_a

    .line 2254791
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254792
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2254793
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2254794
    :cond_b
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2254795
    if-eqz v0, :cond_c

    .line 2254796
    const-string v1, "url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254797
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2254798
    :cond_c
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2254799
    if-eqz v0, :cond_d

    .line 2254800
    const-string v0, "viewed_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254801
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2254802
    :cond_d
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2254803
    return-void
.end method
