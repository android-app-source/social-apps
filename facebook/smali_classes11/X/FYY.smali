.class public final LX/FYY;
.super LX/EkR;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EkR",
        "<",
        "LX/BO1;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FYb;

.field private final b:Landroid/support/v4/app/FragmentActivity;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FY2;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/FYb;Landroid/support/v4/app/FragmentActivity;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentActivity;",
            "LX/0Ot",
            "<",
            "LX/FY2;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2256136
    iput-object p1, p0, LX/FYY;->a:LX/FYb;

    invoke-direct {p0}, LX/EkR;-><init>()V

    .line 2256137
    iput-object p2, p0, LX/FYY;->b:Landroid/support/v4/app/FragmentActivity;

    .line 2256138
    iput-object p3, p0, LX/FYY;->c:LX/0Ot;

    .line 2256139
    return-void
.end method

.method private a(Landroid/view/View;LX/BO1;)Z
    .locals 5

    .prologue
    .line 2256140
    iget-object v0, p0, LX/FYY;->a:LX/FYb;

    iget-object v1, v0, LX/FYb;->D:LX/FYP;

    iget-object v0, p0, LX/FYY;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FY2;

    iget-object v2, p0, LX/FYY;->b:Landroid/support/v4/app/FragmentActivity;

    invoke-static {p2}, LX/FZ5;->a(LX/BO1;)LX/BO1;

    move-result-object v3

    const-string v4, "long_click"

    invoke-virtual {v0, v2, v3, v4, p1}, LX/FY2;->a(Landroid/support/v4/app/FragmentActivity;LX/BO1;Ljava/lang/String;Landroid/view/View;)LX/3Af;

    move-result-object v0

    iput-object v0, v1, LX/FYP;->a:LX/3Af;

    .line 2256141
    const/4 v0, 0x1

    return v0
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/view/View;LX/AU0;)Z
    .locals 1

    .prologue
    .line 2256142
    check-cast p2, LX/BO1;

    invoke-direct {p0, p1, p2}, LX/FYY;->a(Landroid/view/View;LX/BO1;)Z

    move-result v0

    return v0
.end method
