.class public final LX/FRe;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/payments/shipping/model/MailingAddress;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FRt;

.field public final synthetic b:LX/0Px;

.field public final synthetic c:LX/6zj;

.field public final synthetic d:Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;

.field public final synthetic e:LX/FRg;


# direct methods
.method public constructor <init>(LX/FRg;LX/FRt;LX/0Px;LX/6zj;Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;)V
    .locals 0

    .prologue
    .line 2240563
    iput-object p1, p0, LX/FRe;->e:LX/FRg;

    iput-object p2, p0, LX/FRe;->a:LX/FRt;

    iput-object p3, p0, LX/FRe;->b:LX/0Px;

    iput-object p4, p0, LX/FRe;->c:LX/6zj;

    iput-object p5, p0, LX/FRe;->d:Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    .line 2240564
    iget-object v0, p0, LX/FRe;->e:LX/FRg;

    const-string v1, "startMailingAddressesFetch_failure"

    const/4 v6, 0x1

    .line 2240565
    const-class v2, LX/2Oo;

    invoke-static {p1, v2}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v2

    check-cast v2, LX/2Oo;

    .line 2240566
    if-eqz v2, :cond_0

    invoke-virtual {v2}, LX/2Oo;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 2240567
    :goto_0
    iget-object v3, v0, LX/FRg;->h:LX/03V;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, LX/FRg;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v2

    .line 2240568
    iput v6, v2, LX/0VK;->e:I

    .line 2240569
    move-object v2, v2

    .line 2240570
    iput-object p1, v2, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2240571
    move-object v2, v2

    .line 2240572
    iput-boolean v6, v2, LX/0VK;->d:Z

    .line 2240573
    move-object v2, v2

    .line 2240574
    invoke-virtual {v2}, LX/0VK;->g()LX/0VG;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/03V;->a(LX/0VG;)V

    .line 2240575
    iget-object v0, p0, LX/FRe;->e:LX/FRg;

    iget-object v1, p0, LX/FRe;->c:LX/6zj;

    iget-object v2, p0, LX/FRe;->d:Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;

    invoke-static {v0, v1, v2}, LX/FRg;->b(LX/FRg;LX/6zj;Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;)V

    .line 2240576
    return-void

    .line 2240577
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2240578
    check-cast p1, LX/0Px;

    .line 2240579
    iget-object v0, p0, LX/FRe;->a:LX/FRt;

    invoke-static {p1}, LX/71q;->a(LX/0Px;)LX/0am;

    move-result-object v1

    .line 2240580
    iput-object v1, v0, LX/FRt;->b:LX/0am;

    .line 2240581
    iget-object v0, p0, LX/FRe;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LX/FRe;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ve;

    .line 2240582
    invoke-interface {v0}, LX/0Vf;->isDisposed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2240583
    :goto_1
    return-void

    .line 2240584
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2240585
    :cond_1
    iget-object v0, p0, LX/FRe;->e:LX/FRg;

    iget-object v1, p0, LX/FRe;->c:LX/6zj;

    iget-object v2, p0, LX/FRe;->a:LX/FRt;

    .line 2240586
    iget-object p0, v0, LX/FRg;->b:LX/1Ck;

    invoke-virtual {p0}, LX/1Ck;->a()Z

    move-result p0

    if-nez p0, :cond_2

    const/4 p0, 0x1

    :goto_2
    invoke-static {p0}, LX/0PB;->checkArgument(Z)V

    .line 2240587
    iget-object p0, v0, LX/FRg;->i:LX/70k;

    invoke-virtual {p0}, LX/70k;->b()V

    .line 2240588
    invoke-virtual {v2}, LX/FRt;->g()Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;

    move-result-object p0

    invoke-interface {v1, p0}, LX/6zj;->a(Lcom/facebook/payments/picker/model/CoreClientData;)V

    .line 2240589
    goto :goto_1

    .line 2240590
    :cond_2
    const/4 p0, 0x0

    goto :goto_2
.end method
