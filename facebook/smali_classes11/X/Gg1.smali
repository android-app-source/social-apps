.class public LX/Gg1;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Gg3;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Gg1",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Gg3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2378388
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2378389
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Gg1;->b:LX/0Zi;

    .line 2378390
    iput-object p1, p0, LX/Gg1;->a:LX/0Ot;

    .line 2378391
    return-void
.end method

.method public static a(LX/0QB;)LX/Gg1;
    .locals 4

    .prologue
    .line 2378392
    const-class v1, LX/Gg1;

    monitor-enter v1

    .line 2378393
    :try_start_0
    sget-object v0, LX/Gg1;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2378394
    sput-object v2, LX/Gg1;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2378395
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2378396
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2378397
    new-instance v3, LX/Gg1;

    const/16 p0, 0x2128

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Gg1;-><init>(LX/0Ot;)V

    .line 2378398
    move-object v0, v3

    .line 2378399
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2378400
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gg1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2378401
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2378402
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2378403
    check-cast p2, LX/Gg0;

    .line 2378404
    iget-object v0, p0, LX/Gg1;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gg3;

    iget-object v1, p2, LX/Gg0;->a:LX/1Pn;

    iget-object v2, p2, LX/Gg0;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v6, 0x0

    .line 2378405
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2378406
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2378407
    check-cast v3, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    .line 2378408
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;

    move-result-object v4

    .line 2378409
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;->a()LX/0Px;

    move-result-object v8

    .line 2378410
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v5, v6

    :goto_0
    if-ge v5, v9, :cond_0

    invoke-virtual {v8, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;

    .line 2378411
    new-instance v10, LX/GfY;

    invoke-direct {v10, v3, v4}, LX/GfY;-><init>(Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;)V

    invoke-interface {v7, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2378412
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 2378413
    :cond_0
    new-instance v9, LX/Gfv;

    iget-object v4, v0, LX/Gg3;->d:LX/Ge4;

    invoke-direct {v9, v4, v3}, LX/Gfv;-><init>(LX/Ge4;Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;)V

    .line 2378414
    new-instance v4, LX/Gg2;

    invoke-direct {v4, v0, v3, v8}, LX/Gg2;-><init>(LX/Gg3;Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;LX/0Px;)V

    .line 2378415
    invoke-static {}, LX/3mP;->newBuilder()LX/3mP;

    move-result-object v5

    .line 2378416
    iput-object v4, v5, LX/3mP;->g:LX/25K;

    .line 2378417
    move-object v4, v5

    .line 2378418
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->g()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, LX/25L;->a(Ljava/lang/String;)LX/25L;

    move-result-object v5

    .line 2378419
    iput-object v5, v4, LX/3mP;->d:LX/25L;

    .line 2378420
    move-object v4, v4

    .line 2378421
    iput-object v3, v4, LX/3mP;->e:LX/0jW;

    .line 2378422
    move-object v3, v4

    .line 2378423
    iput-boolean v6, v3, LX/3mP;->a:Z

    .line 2378424
    move-object v3, v3

    .line 2378425
    invoke-virtual {v3}, LX/3mP;->a()LX/25M;

    move-result-object v8

    .line 2378426
    iget-object v3, v0, LX/Gg3;->a:LX/Gfn;

    invoke-static {v7}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v5

    move-object v4, p1

    move-object v6, v2

    move-object v7, v1

    invoke-virtual/range {v3 .. v9}, LX/Gfn;->a(Landroid/content/Context;LX/0Px;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/Object;LX/25M;LX/Gfv;)LX/Gfm;

    move-result-object v3

    .line 2378427
    iget-object v4, v0, LX/Gg3;->b:LX/3mL;

    invoke-virtual {v4, p1}, LX/3mL;->c(LX/1De;)LX/3ml;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/3ml;->a(LX/3mX;)LX/3ml;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const/4 v4, 0x7

    const/4 v5, 0x2

    invoke-interface {v3, v4, v5}, LX/1Di;->h(II)LX/1Di;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2378428
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2378429
    invoke-static {}, LX/1dS;->b()V

    .line 2378430
    const/4 v0, 0x0

    return-object v0
.end method
