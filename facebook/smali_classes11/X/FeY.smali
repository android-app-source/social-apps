.class public LX/FeY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field public final a:LX/1K9;

.field public final b:LX/0bH;

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FgJ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final d:LX/Cxo;

.field public final e:LX/CzE;

.field public final f:LX/CvY;

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/6Vi;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field public final i:LX/2Sc;

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3id;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/20h;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AnQ;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/FeV;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzE;LX/Cxo;LX/1K9;LX/0bH;LX/CvY;LX/2Sc;)V
    .locals 1
    .param p1    # Lcom/facebook/search/results/model/SearchResultsMutableContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/CzE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/Cxo;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2265964
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2265965
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2265966
    iput-object v0, p0, LX/FeY;->c:LX/0Ot;

    .line 2265967
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2265968
    iput-object v0, p0, LX/FeY;->j:LX/0Ot;

    .line 2265969
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2265970
    iput-object v0, p0, LX/FeY;->k:LX/0Ot;

    .line 2265971
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2265972
    iput-object v0, p0, LX/FeY;->l:LX/0Ot;

    .line 2265973
    iput-object p1, p0, LX/FeY;->h:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2265974
    iput-object p2, p0, LX/FeY;->e:LX/CzE;

    .line 2265975
    iput-object p3, p0, LX/FeY;->d:LX/Cxo;

    .line 2265976
    iput-object p4, p0, LX/FeY;->a:LX/1K9;

    .line 2265977
    iput-object p5, p0, LX/FeY;->b:LX/0bH;

    .line 2265978
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/FeY;->g:Ljava/util/List;

    .line 2265979
    iput-object p6, p0, LX/FeY;->f:LX/CvY;

    .line 2265980
    iput-object p7, p0, LX/FeY;->i:LX/2Sc;

    .line 2265981
    return-void
.end method

.method public static a$redex0(LX/FeY;LX/EJ2;Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 5

    .prologue
    .line 2265954
    iget-object v0, p1, LX/EJ2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 2265955
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 2265956
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->z()Z

    move-result v0

    .line 2265957
    iget-boolean v1, p1, LX/EJ2;->b:Z

    move v1, v1

    .line 2265958
    if-ne v0, v1, :cond_0

    .line 2265959
    :goto_0
    return-void

    .line 2265960
    :cond_0
    iget-object v0, p0, LX/FeY;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3id;

    .line 2265961
    iget-object v1, p1, LX/EJ2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 2265962
    iget-object v2, v1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v2

    .line 2265963
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    new-instance v4, LX/FeU;

    invoke-direct {v4, p0, p2, p3}, LX/FeU;-><init>(LX/FeY;Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v0, v1, v2, v3, v4}, LX/3id;->a(Lcom/facebook/graphql/model/GraphQLFeedback;Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;ZLX/1L9;)V

    goto :goto_0
.end method

.method public static a$redex0(LX/FeY;Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 2

    .prologue
    .line 2265946
    iget-object v0, p0, LX/FeY;->e:LX/CzE;

    invoke-virtual {v0, p1, p2}, LX/CzE;->a(Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2265947
    iget-object v0, p0, LX/FeY;->a:LX/1K9;

    new-instance v1, LX/EJ1;

    invoke-direct {v1, p1}, LX/EJ1;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v0, v1}, LX/1K9;->a(LX/1KJ;)V

    .line 2265948
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 3

    .prologue
    .line 2265949
    iget-object v0, p0, LX/FeY;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6Vi;

    .line 2265950
    iget-object v2, p0, LX/FeY;->a:LX/1K9;

    invoke-virtual {v2, v0}, LX/1K9;->a(LX/6Vi;)V

    goto :goto_0

    .line 2265951
    :cond_0
    iget-object v0, p0, LX/FeY;->b:LX/0bH;

    iget-object v1, p0, LX/FeY;->m:LX/FeV;

    invoke-virtual {v0, v1}, LX/0b4;->b(LX/0b2;)Z

    .line 2265952
    iget-object v0, p0, LX/FeY;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2265953
    return-void
.end method
