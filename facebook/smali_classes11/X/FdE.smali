.class public LX/FdE;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field public a:Lcom/facebook/widget/text/BetterTextView;

.field public b:Lcom/facebook/resources/ui/FbRadioButton;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2263324
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/FdE;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2263325
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2263322
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/FdE;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2263323
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2263315
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2263316
    const p1, 0x7f03129e

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2263317
    const p1, 0x7f0d02c4

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, LX/FdE;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2263318
    const p1, 0x7f0d2821

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbRadioButton;

    iput-object p1, p0, LX/FdE;->b:Lcom/facebook/resources/ui/FbRadioButton;

    .line 2263319
    invoke-virtual {p0}, LX/FdE;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0b14de

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 2263320
    invoke-virtual {p0}, LX/FdE;->getPaddingLeft()I

    move-result p2

    invoke-virtual {p0}, LX/FdE;->getPaddingRight()I

    move-result p3

    invoke-virtual {p0, p2, p1, p3, p1}, LX/FdE;->setPadding(IIII)V

    .line 2263321
    return-void
.end method


# virtual methods
.method public final isChecked()Z
    .locals 1

    .prologue
    .line 2263314
    iget-boolean v0, p0, LX/FdE;->c:Z

    return v0
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 2263309
    iget-boolean v0, p0, LX/FdE;->c:Z

    if-eq v0, p1, :cond_0

    .line 2263310
    iput-boolean p1, p0, LX/FdE;->c:Z

    .line 2263311
    iget-object v0, p0, LX/FdE;->b:Lcom/facebook/resources/ui/FbRadioButton;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbRadioButton;->setChecked(Z)V

    .line 2263312
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->refreshDrawableState()V

    .line 2263313
    :cond_0
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 2263307
    iget-object v0, p0, LX/FdE;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2263308
    return-void
.end method

.method public final toggle()V
    .locals 1

    .prologue
    .line 2263304
    iget-boolean v0, p0, LX/FdE;->c:Z

    if-nez v0, :cond_0

    .line 2263305
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/FdE;->setChecked(Z)V

    .line 2263306
    :cond_0
    return-void
.end method
