.class public final LX/FBM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3Mb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/3Mb",
        "<",
        "Ljava/lang/Void;",
        "LX/3MZ;",
        "Ljava/lang/Throwable;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;)V
    .locals 0

    .prologue
    .line 2209237
    iput-object p1, p0, LX/FBM;->a:Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;)V
    .locals 0

    .prologue
    .line 2209238
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2209199
    check-cast p2, LX/3MZ;

    .line 2209200
    iget-object v0, p0, LX/FBM;->a:Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;

    .line 2209201
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2209202
    :cond_0
    :goto_0
    return-void

    .line 2209203
    :cond_1
    if-eqz p2, :cond_0

    .line 2209204
    iget-object v1, p2, LX/3MZ;->g:LX/0Px;

    move-object v1, v1

    .line 2209205
    if-nez v1, :cond_2

    .line 2209206
    iget-object v1, p2, LX/3MZ;->h:LX/0Px;

    move-object v1, v1

    .line 2209207
    if-eqz v1, :cond_5

    .line 2209208
    :cond_2
    iget-object v1, p2, LX/3MZ;->g:LX/0Px;

    move-object v1, v1

    .line 2209209
    iget-object v2, p2, LX/3MZ;->h:LX/0Px;

    move-object v2, v2

    .line 2209210
    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    .line 2209211
    if-eqz v2, :cond_3

    .line 2209212
    invoke-interface {v3, v2}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 2209213
    :cond_3
    if-eqz v1, :cond_4

    .line 2209214
    invoke-interface {v3, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 2209215
    :cond_4
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Lcom/facebook/user/model/User;

    invoke-interface {v3, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->o:Ljava/util/List;

    .line 2209216
    :cond_5
    iget-boolean v1, p2, LX/3MZ;->A:Z

    move v1, v1

    .line 2209217
    if-nez v1, :cond_0

    .line 2209218
    iget-object v1, v0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->n:LX/3Lb;

    invoke-virtual {v1}, LX/3Lb;->b()V

    .line 2209219
    iget-object v1, v0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->o:Ljava/util/List;

    const/16 p2, 0xa

    const/4 p1, 0x2

    const/4 p0, 0x1

    const/4 v4, 0x0

    .line 2209220
    iget-object v2, v0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->v:Landroid/widget/TextView;

    if-eqz v2, :cond_6

    if-eqz v1, :cond_6

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2209221
    :cond_6
    :goto_1
    goto :goto_0

    .line 2209222
    :cond_7
    invoke-static {p2}, LX/0R9;->b(I)Ljava/util/ArrayList;

    move-result-object v5

    .line 2209223
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v3, v4

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    .line 2209224
    if-ge v3, p2, :cond_8

    .line 2209225
    invoke-virtual {v2}, Lcom/facebook/user/model/User;->u()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2209226
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    .line 2209227
    goto :goto_2

    .line 2209228
    :cond_8
    iget-object v2, v0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->u:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v2, v5}, Lcom/facebook/fbui/facepile/FacepileView;->setFaceUrls(Ljava/util/List;)V

    .line 2209229
    iget-object v2, v0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->u:Lcom/facebook/fbui/facepile/FacepileView;

    invoke-virtual {v2, v4}, Lcom/facebook/fbui/facepile/FacepileView;->setVisibility(I)V

    .line 2209230
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, p0, :cond_9

    .line 2209231
    const v3, 0x7f08321e

    new-array v5, p0, [Ljava/lang/Object;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    invoke-virtual {v2}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v4

    invoke-virtual {v0, v3, v5}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2209232
    :goto_3
    iget-object v3, v0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->v:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2209233
    iget-object v2, v0, Lcom/facebook/katana/orca/DiodeStaticFallbackFragment;->v:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 2209234
    :cond_9
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, p1, :cond_a

    .line 2209235
    const v3, 0x7f08321f

    new-array v5, p1, [Ljava/lang/Object;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    invoke-virtual {v2}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v4

    invoke-interface {v1, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    invoke-virtual {v2}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, p0

    invoke-virtual {v0, v3, v5}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 2209236
    :cond_a
    const v3, 0x7f083220

    const/4 v2, 0x3

    new-array v5, v2, [Ljava/lang/Object;

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    invoke-virtual {v2}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v4

    invoke-interface {v1, p0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    invoke-virtual {v2}, Lcom/facebook/user/model/User;->g()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, p0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, p1

    invoke-virtual {v0, v3, v5}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_3
.end method

.method public final bridge synthetic b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2209198
    return-void
.end method

.method public final bridge synthetic c(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2209197
    return-void
.end method
