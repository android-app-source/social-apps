.class public LX/FxK;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/03V;

.field private c:[LX/5vn;

.field private d:[LX/5vn;

.field private e:[LX/5vn;

.field private f:[LX/5vn;

.field private g:[LX/5vn;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2304714
    const-class v0, LX/FxK;

    sput-object v0, LX/FxK;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2304711
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2304712
    iput-object p1, p0, LX/FxK;->b:LX/03V;

    .line 2304713
    return-void
.end method

.method private static a(IIII)LX/5vn;
    .locals 5

    .prologue
    .line 2304715
    new-instance v0, LX/5vq;

    invoke-direct {v0}, LX/5vq;-><init>()V

    int-to-double v2, p0

    .line 2304716
    iput-wide v2, v0, LX/5vq;->c:D

    .line 2304717
    move-object v0, v0

    .line 2304718
    int-to-double v2, p1

    .line 2304719
    iput-wide v2, v0, LX/5vq;->d:D

    .line 2304720
    move-object v0, v0

    .line 2304721
    int-to-double v2, p2

    .line 2304722
    iput-wide v2, v0, LX/5vq;->b:D

    .line 2304723
    move-object v0, v0

    .line 2304724
    int-to-double v2, p3

    .line 2304725
    iput-wide v2, v0, LX/5vq;->a:D

    .line 2304726
    move-object v0, v0

    .line 2304727
    invoke-virtual {v0}, LX/5vq;->a()Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 13
    .param p1    # Ljava/util/ArrayList;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLInterfaces$FavoritePhoto;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLInterfaces$FavoritePhoto;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2304698
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 2304699
    :cond_0
    :goto_0
    return-object p1

    .line 2304700
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p0, v0}, LX/FxK;->a(I)[LX/5vn;

    move-result-object v5

    .line 2304701
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 2304702
    :goto_1
    array-length v0, v5

    if-ge v1, v0, :cond_3

    .line 2304703
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v2

    :goto_2
    if-ge v4, v6, :cond_0

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    .line 2304704
    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;->a()LX/5vn;

    move-result-object v7

    if-eqz v7, :cond_2

    aget-object v7, v5, v1

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;->a()LX/5vn;

    move-result-object v8

    .line 2304705
    invoke-interface {v7}, LX/5vn;->c()D

    move-result-wide v9

    invoke-interface {v8}, LX/5vn;->c()D

    move-result-wide v11

    cmpl-double v9, v9, v11

    if-nez v9, :cond_4

    invoke-interface {v7}, LX/5vn;->d()D

    move-result-wide v9

    invoke-interface {v8}, LX/5vn;->d()D

    move-result-wide v11

    cmpl-double v9, v9, v11

    if-nez v9, :cond_4

    invoke-interface {v7}, LX/5vn;->b()D

    move-result-wide v9

    invoke-interface {v8}, LX/5vn;->b()D

    move-result-wide v11

    cmpl-double v9, v9, v11

    if-nez v9, :cond_4

    invoke-interface {v7}, LX/5vn;->a()D

    move-result-wide v9

    invoke-interface {v8}, LX/5vn;->a()D

    move-result-wide v11

    cmpl-double v9, v9, v11

    if-nez v9, :cond_4

    const/4 v9, 0x1

    :goto_3
    move v7, v9

    .line 2304706
    if-eqz v7, :cond_2

    .line 2304707
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2304708
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2304709
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    :cond_3
    move-object p1, v3

    .line 2304710
    goto :goto_0

    :cond_4
    const/4 v9, 0x0

    goto :goto_3
.end method

.method public final a(I)[LX/5vn;
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x3

    .line 2304680
    packed-switch p1, :pswitch_data_0

    .line 2304681
    iget-object v0, p0, LX/FxK;->b:LX/03V;

    sget-object v1, LX/FxK;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "featured photos should be between 1 and 5. actual is "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2304682
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2304683
    :pswitch_0
    iget-object v0, p0, LX/FxK;->c:[LX/5vn;

    if-nez v0, :cond_0

    .line 2304684
    new-array v0, v5, [LX/5vn;

    const/4 v1, 0x6

    const/4 v2, 0x6

    invoke-static {v3, v3, v1, v2}, LX/FxK;->a(IIII)LX/5vn;

    move-result-object v1

    aput-object v1, v0, v3

    iput-object v0, p0, LX/FxK;->c:[LX/5vn;

    .line 2304685
    :cond_0
    iget-object v0, p0, LX/FxK;->c:[LX/5vn;

    goto :goto_0

    .line 2304686
    :pswitch_1
    iget-object v0, p0, LX/FxK;->d:[LX/5vn;

    if-nez v0, :cond_1

    .line 2304687
    new-array v0, v4, [LX/5vn;

    invoke-static {v3, v3, v2, v2}, LX/FxK;->a(IIII)LX/5vn;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v2, v3, v2, v2}, LX/FxK;->a(IIII)LX/5vn;

    move-result-object v1

    aput-object v1, v0, v5

    iput-object v0, p0, LX/FxK;->d:[LX/5vn;

    .line 2304688
    :cond_1
    iget-object v0, p0, LX/FxK;->d:[LX/5vn;

    goto :goto_0

    .line 2304689
    :pswitch_2
    iget-object v0, p0, LX/FxK;->e:[LX/5vn;

    if-nez v0, :cond_2

    .line 2304690
    new-array v0, v2, [LX/5vn;

    const/4 v1, 0x6

    invoke-static {v3, v3, v2, v1}, LX/FxK;->a(IIII)LX/5vn;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v2, v3, v2, v2}, LX/FxK;->a(IIII)LX/5vn;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-static {v2, v2, v2, v2}, LX/FxK;->a(IIII)LX/5vn;

    move-result-object v1

    aput-object v1, v0, v4

    iput-object v0, p0, LX/FxK;->e:[LX/5vn;

    .line 2304691
    :cond_2
    iget-object v0, p0, LX/FxK;->e:[LX/5vn;

    goto :goto_0

    .line 2304692
    :pswitch_3
    iget-object v0, p0, LX/FxK;->f:[LX/5vn;

    if-nez v0, :cond_3

    .line 2304693
    new-array v0, v6, [LX/5vn;

    invoke-static {v3, v3, v2, v2}, LX/FxK;->a(IIII)LX/5vn;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v2, v3, v2, v2}, LX/FxK;->a(IIII)LX/5vn;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-static {v3, v2, v2, v2}, LX/FxK;->a(IIII)LX/5vn;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v2, v2, v2, v2}, LX/FxK;->a(IIII)LX/5vn;

    move-result-object v1

    aput-object v1, v0, v2

    iput-object v0, p0, LX/FxK;->f:[LX/5vn;

    .line 2304694
    :cond_3
    iget-object v0, p0, LX/FxK;->f:[LX/5vn;

    goto :goto_0

    .line 2304695
    :pswitch_4
    iget-object v0, p0, LX/FxK;->g:[LX/5vn;

    if-nez v0, :cond_4

    .line 2304696
    const/4 v0, 0x5

    new-array v0, v0, [LX/5vn;

    invoke-static {v3, v3, v2, v2}, LX/FxK;->a(IIII)LX/5vn;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-static {v2, v3, v2, v2}, LX/FxK;->a(IIII)LX/5vn;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-static {v3, v2, v4, v4}, LX/FxK;->a(IIII)LX/5vn;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v4, v2, v4, v4}, LX/FxK;->a(IIII)LX/5vn;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v6, v2, v4, v4}, LX/FxK;->a(IIII)LX/5vn;

    move-result-object v1

    aput-object v1, v0, v6

    iput-object v0, p0, LX/FxK;->g:[LX/5vn;

    .line 2304697
    :cond_4
    iget-object v0, p0, LX/FxK;->g:[LX/5vn;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
