.class public LX/FPI;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public final a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FQA;",
            ">;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

.field public d:LX/FPU;

.field public final e:Landroid/view/View$OnClickListener;

.field public final f:LX/FPC;


# direct methods
.method public constructor <init>(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;LX/0Ot;)V
    .locals 1
    .param p1    # Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;",
            "LX/0Ot",
            "<",
            "LX/FQA;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2236903
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2236904
    new-instance v0, LX/FPB;

    invoke-direct {v0, p0}, LX/FPB;-><init>(LX/FPI;)V

    iput-object v0, p0, LX/FPI;->e:Landroid/view/View$OnClickListener;

    .line 2236905
    new-instance v0, LX/FPD;

    invoke-direct {v0, p0}, LX/FPD;-><init>(LX/FPI;)V

    iput-object v0, p0, LX/FPI;->f:LX/FPC;

    .line 2236906
    if-nez p1, :cond_0

    .line 2236907
    new-instance v0, LX/FPY;

    invoke-direct {v0}, LX/FPY;-><init>()V

    invoke-virtual {v0}, LX/FPY;->a()Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    move-result-object v0

    iput-object v0, p0, LX/FPI;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    .line 2236908
    :goto_0
    iput-object p2, p0, LX/FPI;->b:LX/0Ot;

    .line 2236909
    return-void

    .line 2236910
    :cond_0
    iput-object p1, p0, LX/FPI;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2237009
    iget-object v0, p0, LX/FPI;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2237010
    const/4 v0, 0x0

    .line 2237011
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/FPI;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2237012
    iget-object p0, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v0, p0

    .line 2237013
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->h()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)V
    .locals 1

    .prologue
    .line 2237005
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2237006
    iput-object p1, p0, LX/FPI;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2237007
    const v0, 0x68eaf17f

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2237008
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2236999
    iget-object v0, p0, LX/FPI;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2237000
    const/4 v0, 0x0

    .line 2237001
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/FPI;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2237002
    iget-object p0, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v0, p0

    .line 2237003
    iget-boolean p0, v0, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->l:Z

    move v0, p0

    .line 2237004
    goto :goto_0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 2236994
    iget-object v0, p0, LX/FPI;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2236995
    const/4 v0, 0x0

    .line 2236996
    :cond_0
    :goto_0
    return v0

    .line 2236997
    :cond_1
    invoke-virtual {p0}, LX/FPI;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 2236998
    invoke-virtual {p0}, LX/FPI;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2236987
    invoke-virtual {p0}, LX/FPI;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2236988
    invoke-virtual {p0}, LX/FPI;->b()Z

    move-result v1

    .line 2236989
    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt p1, v2, :cond_1

    if-nez v1, :cond_1

    .line 2236990
    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 2236991
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne p1, v2, :cond_2

    if-eqz v1, :cond_2

    .line 2236992
    const/4 v0, 0x0

    .line 2236993
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2236979
    invoke-virtual {p0}, LX/FPI;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2236980
    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt p1, v1, :cond_1

    invoke-virtual {p0}, LX/FPI;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2236981
    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 2236982
    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne p1, v1, :cond_2

    .line 2236983
    const-wide/16 v0, -0x1

    .line 2236984
    :goto_0
    return-wide v0

    .line 2236985
    :cond_2
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2236986
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-wide/16 v0, 0x0

    goto :goto_0

    :cond_3
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2236970
    if-ltz p1, :cond_0

    iget-object v0, p0, LX/FPI;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2236971
    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 2236972
    :cond_1
    invoke-virtual {p0}, LX/FPI;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_3

    .line 2236973
    invoke-virtual {p0}, LX/FPI;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2236974
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 2236975
    :cond_2
    sget-object v0, LX/FPH;->PAGINATION_LOADING_CELL:LX/FPH;

    invoke-virtual {v0}, LX/FPH;->ordinal()I

    move-result v0

    .line 2236976
    :goto_0
    return v0

    :cond_3
    iget-object v0, p0, LX/FPI;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    .line 2236977
    iget-boolean p0, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->a:Z

    move v0, p0

    .line 2236978
    if-eqz v0, :cond_4

    sget-object v0, LX/FPH;->SET_SEARCH_CELL:LX/FPH;

    invoke-virtual {v0}, LX/FPH;->ordinal()I

    move-result v0

    goto :goto_0

    :cond_4
    sget-object v0, LX/FPH;->HUGE_CELL:LX/FPH;

    invoke-virtual {v0}, LX/FPH;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2236912
    invoke-static {}, LX/FPH;->values()[LX/FPH;

    move-result-object v0

    invoke-virtual {p0, p1}, LX/FPI;->getItemViewType(I)I

    move-result v1

    aget-object v0, v0, v1

    .line 2236913
    sget-object v1, LX/FPG;->a:[I

    invoke-virtual {v0}, LX/FPH;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2236914
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2236915
    :pswitch_0
    if-nez p2, :cond_4

    .line 2236916
    new-instance p2, LX/FPs;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, LX/FPs;-><init>(Landroid/content/Context;)V

    .line 2236917
    :goto_0
    move-object v0, p2

    .line 2236918
    :goto_1
    return-object v0

    .line 2236919
    :pswitch_1
    if-nez p2, :cond_5

    .line 2236920
    new-instance p2, LX/FQK;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, LX/FQK;-><init>(Landroid/content/Context;)V

    .line 2236921
    :goto_2
    invoke-virtual {p0, p1}, LX/FPI;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    .line 2236922
    const/4 v1, 0x0

    .line 2236923
    iget-object v2, p0, LX/FPI;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2236924
    iget-object p3, v2, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v2, p3

    .line 2236925
    if-eqz v2, :cond_0

    iget-object v2, p0, LX/FPI;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2236926
    iget-object p3, v2, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v2, p3

    .line 2236927
    iget-object p3, v2, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->a:Landroid/location/Location;

    move-object v2, p3

    .line 2236928
    if-eqz v2, :cond_0

    .line 2236929
    iget-object v1, p0, LX/FPI;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2236930
    iget-object v2, v1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v1, v2

    .line 2236931
    iget-object v2, v1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->a:Landroid/location/Location;

    move-object v1, v2

    .line 2236932
    :cond_0
    iget-object v2, p0, LX/FPI;->c:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2236933
    iget-object v3, v2, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    .line 2236934
    iget-object p3, v3, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->e:LX/FPn;

    move-object v3, p3

    .line 2236935
    sget-object p3, LX/FPn;->USER_CENTERED:LX/FPn;

    if-ne v3, p3, :cond_6

    const/4 v3, 0x1

    :goto_3
    move v2, v3

    .line 2236936
    invoke-virtual {p2, v0, v1, v2}, LX/FQK;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Landroid/location/Location;Z)V

    .line 2236937
    new-instance v1, LX/FPF;

    invoke-direct {v1, p0, p1}, LX/FPF;-><init>(LX/FPI;I)V

    .line 2236938
    iput-object v1, p2, LX/FQK;->i:LX/FPE;

    .line 2236939
    iget-object v1, p0, LX/FPI;->d:LX/FPU;

    if-eqz v1, :cond_1

    .line 2236940
    iget-object v1, p0, LX/FPI;->d:LX/FPU;

    invoke-virtual {v1, v0}, LX/FPU;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;)V

    .line 2236941
    :cond_1
    move-object v0, p2

    .line 2236942
    goto :goto_1

    .line 2236943
    :pswitch_2
    const/4 v2, 0x1

    .line 2236944
    if-eqz p2, :cond_2

    instance-of v0, p2, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    if-nez v0, :cond_8

    .line 2236945
    :cond_2
    new-instance v0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, LX/FPI;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    .line 2236946
    iget-boolean p2, v1, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->c:Z

    move v1, p2

    .line 2236947
    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->b(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->c(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    move-result-object v1

    iget-object v0, p0, LX/FPI;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    .line 2236948
    iget-boolean p2, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->d:Z

    move v0, p2

    .line 2236949
    if-eqz v0, :cond_7

    sget-object v0, LX/FQ9;->BOOKMARK:LX/FQ9;

    :goto_4
    invoke-virtual {v1, v0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a(LX/FQ9;)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    move-result-object v0

    iget-object v1, p0, LX/FPI;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    .line 2236950
    iget-boolean p2, v1, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->e:Z

    move v1, p2

    .line 2236951
    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->e(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->f(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    move-result-object v0

    iget-object v1, p0, LX/FPI;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;

    .line 2236952
    iget-boolean v2, v1, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultsFragment$Options;->f:Z

    move v1, v2

    .line 2236953
    invoke-virtual {v0, v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->g(Z)Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    move-result-object p2

    .line 2236954
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-static {p2, v0}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2236955
    iget-object v0, p0, LX/FPI;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2236956
    iget-object v0, p0, LX/FPI;->f:LX/FPC;

    .line 2236957
    iput-object v0, p2, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->K:LX/FPC;

    .line 2236958
    :goto_5
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->setTag(Ljava/lang/Object;)V

    .line 2236959
    invoke-virtual {p0, p1}, LX/FPI;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    .line 2236960
    iget-object v1, p0, LX/FPI;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FQA;

    invoke-virtual {v1, p2, v0}, LX/FQA;->a(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;)V

    .line 2236961
    iget-object v1, p0, LX/FPI;->d:LX/FPU;

    if-eqz v1, :cond_3

    .line 2236962
    iget-object v1, p0, LX/FPI;->d:LX/FPU;

    invoke-virtual {v1, v0}, LX/FPU;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;)V

    .line 2236963
    :cond_3
    move-object v0, p2

    .line 2236964
    goto/16 :goto_1

    .line 2236965
    :cond_4
    instance-of v0, p2, LX/FPs;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    goto/16 :goto_0

    .line 2236966
    :cond_5
    instance-of v0, p2, LX/FQK;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2236967
    check-cast p2, LX/FQK;

    goto/16 :goto_2

    :cond_6
    const/4 v3, 0x0

    goto/16 :goto_3

    .line 2236968
    :cond_7
    sget-object v0, LX/FQ9;->NONE:LX/FQ9;

    goto :goto_4

    .line 2236969
    :cond_8
    check-cast p2, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2236911
    sget-object v0, LX/FPH;->COUNT:LX/FPH;

    invoke-virtual {v0}, LX/FPH;->ordinal()I

    move-result v0

    return v0
.end method
