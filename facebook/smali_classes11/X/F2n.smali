.class public final LX/F2n;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/F2o;


# direct methods
.method public constructor <init>(LX/F2o;)V
    .locals 0

    .prologue
    .line 2193302
    iput-object p1, p0, LX/F2n;->a:LX/F2o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x2aa500ce

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2193303
    iget-object v1, p0, LX/F2n;->a:LX/F2o;

    iget-object v1, v1, LX/F2o;->a:LX/F2r;

    iget-object v1, v1, LX/F2r;->c:LX/F2N;

    iget-object v2, p0, LX/F2n;->a:LX/F2o;

    iget-object v2, v2, LX/F2o;->a:LX/F2r;

    iget-object v2, v2, LX/F2r;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    .line 2193304
    invoke-static {v1}, LX/F2N;->b(LX/F2N;)Landroid/content/Intent;

    move-result-object v3

    .line 2193305
    const-string v5, "edit_tags_group_id"

    invoke-virtual {v2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->q()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2193306
    const-string v5, "target_fragment"

    sget-object v6, LX/0cQ;->GROUP_EDIT_TAGS_FRAGMENT:LX/0cQ;

    invoke-virtual {v6}, LX/0cQ;->ordinal()I

    move-result v6

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2193307
    move-object v1, v3

    .line 2193308
    iget-object v2, p0, LX/F2n;->a:LX/F2o;

    iget-object v2, v2, LX/F2o;->a:LX/F2r;

    iget-object v2, v2, LX/F2r;->d:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2193309
    const v1, 0x68184b77

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
