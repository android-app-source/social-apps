.class public final enum LX/F5L;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/F5L;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/F5L;

.field public static final enum READ_CONTACTS_DB:LX/F5L;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2198486
    new-instance v0, LX/F5L;

    const-string v1, "READ_CONTACTS_DB"

    invoke-direct {v0, v1, v2}, LX/F5L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F5L;->READ_CONTACTS_DB:LX/F5L;

    .line 2198487
    const/4 v0, 0x1

    new-array v0, v0, [LX/F5L;

    sget-object v1, LX/F5L;->READ_CONTACTS_DB:LX/F5L;

    aput-object v1, v0, v2

    sput-object v0, LX/F5L;->$VALUES:[LX/F5L;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2198488
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/F5L;
    .locals 1

    .prologue
    .line 2198489
    const-class v0, LX/F5L;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/F5L;

    return-object v0
.end method

.method public static values()[LX/F5L;
    .locals 1

    .prologue
    .line 2198490
    sget-object v0, LX/F5L;->$VALUES:[LX/F5L;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/F5L;

    return-object v0
.end method
