.class public LX/Grp;
.super LX/Cts;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/view/View;

.field public b:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

.field private c:LX/Ctg;

.field private d:Landroid/view/View;

.field private e:I


# direct methods
.method public constructor <init>(LX/Ctg;Lcom/facebook/storelocator/StoreLocatorMapDelegate;Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2398737
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 2398738
    iput-object p1, p0, LX/Grp;->c:LX/Ctg;

    .line 2398739
    iput-object p2, p0, LX/Grp;->b:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    .line 2398740
    iput-object p3, p0, LX/Grp;->a:Landroid/view/View;

    .line 2398741
    iput-object p4, p0, LX/Grp;->d:Landroid/view/View;

    .line 2398742
    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1c30

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Grp;->e:I

    .line 2398743
    iget-object v0, p0, LX/Grp;->d:Landroid/view/View;

    new-instance v1, LX/Gro;

    invoke-direct {v1, p0}, LX/Gro;-><init>(LX/Grp;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2398744
    iget-object v0, p0, LX/Grp;->b:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    iget-object v1, p0, LX/Grp;->d:Landroid/view/View;

    .line 2398745
    iput-object v1, v0, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->D:Landroid/view/View;

    .line 2398746
    return-void
.end method


# virtual methods
.method public final a(LX/CrS;)V
    .locals 6

    .prologue
    .line 2398747
    iget-object v0, p0, LX/Grp;->c:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v0

    invoke-interface {v0}, LX/Ct1;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v0

    .line 2398748
    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {v0}, LX/CrW;->i()I

    move-result v2

    iget v3, p0, LX/Grp;->e:I

    add-int/2addr v2, v3

    invoke-virtual {v0}, LX/CrW;->g()I

    move-result v3

    iget-object v4, p0, LX/Grp;->a:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p0, LX/Grp;->e:I

    add-int/2addr v3, v4

    invoke-virtual {v0}, LX/CrW;->j()I

    move-result v4

    iget v5, p0, LX/Grp;->e:I

    sub-int/2addr v4, v5

    invoke-virtual {v0}, LX/CrW;->h()I

    move-result v0

    iget v5, p0, LX/Grp;->e:I

    sub-int/2addr v0, v5

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2398749
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    move-object v0, v0

    .line 2398750
    iget-object v2, p0, LX/Grp;->d:Landroid/view/View;

    invoke-interface {v0, v2, v1}, LX/Ctg;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2398751
    return-void
.end method
