.class public final LX/Gac;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

.field public final synthetic c:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;)V
    .locals 0

    .prologue
    .line 2368354
    iput-object p1, p0, LX/Gac;->c:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    iput-object p2, p0, LX/Gac;->a:Ljava/lang/String;

    iput-object p3, p0, LX/Gac;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/productionprompts/graphql/FetchProductionPromptsGraphQLModels$FetchProductionPromptsQueryModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x1

    .line 2368355
    iget-object v0, p0, LX/Gac;->c:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    iget-object v1, p0, LX/Gac;->a:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->a(Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/String;)Lcom/facebook/productionprompts/model/ProductionPrompt;

    move-result-object v6

    .line 2368356
    if-eqz v6, :cond_0

    .line 2368357
    invoke-virtual {v6}, Lcom/facebook/productionprompts/model/ProductionPrompt;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6}, Lcom/facebook/productionprompts/model/ProductionPrompt;->i()Ljava/lang/String;

    move-result-object v1

    const-string v2, "FROM_CULTURAL_MOMENTS"

    invoke-virtual {v6}, Lcom/facebook/productionprompts/model/ProductionPrompt;->u()Ljava/lang/String;

    move-result-object v4

    move-object v5, v3

    invoke-static/range {v0 .. v5}, Lcom/facebook/productionprompts/logging/PromptAnalytics;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/productionprompts/logging/PromptAnalytics;

    move-result-object v1

    .line 2368358
    invoke-static {v6, v1, v7}, Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;->a(Lcom/facebook/productionprompts/model/ProductionPrompt;Lcom/facebook/productionprompts/logging/PromptAnalytics;Z)Lcom/facebook/productionprompts/composer/ProductionPromptsPluginConfig;

    move-result-object v2

    .line 2368359
    iget-object v0, p0, LX/Gac;->c:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    iget-object v0, v0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->z:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5oW;

    invoke-virtual {v0, v1}, LX/5oW;->b(Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    .line 2368360
    iget-object v0, p0, LX/Gac;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    invoke-static {v0, v6}, LX/BMU;->a(Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;Lcom/facebook/productionprompts/model/ProductionPrompt;)V

    .line 2368361
    iget-object v1, p0, LX/Gac;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    iget-object v0, p0, LX/Gac;->c:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    iget-object v0, v0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->y:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Nq;

    invoke-virtual {v0, v2}, LX/1Nq;->a(LX/88f;)Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPluginConfig(Lcom/facebook/ipc/composer/intent/SerializedComposerPluginConfig;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2368362
    iget-object v0, p0, LX/Gac;->c:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    iget-object v1, p0, LX/Gac;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2368363
    invoke-static {v0, v1}, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->a$redex0(Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;)V

    .line 2368364
    :goto_0
    return-void

    .line 2368365
    :cond_0
    iget-object v0, p0, LX/Gac;->c:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    iget-object v0, v0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->t:LX/03V;

    sget-object v1, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->u:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "prompt with id: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/Gac;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v7}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2368366
    iget-object v0, p0, LX/Gac;->c:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    iget-object v1, p0, LX/Gac;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2368367
    invoke-static {v0, v1}, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->a$redex0(Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;)V

    .line 2368368
    goto :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2368369
    invoke-static {p1}, LX/3d6;->forException(Ljava/lang/Throwable;)LX/1nY;

    move-result-object v0

    sget-object v1, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v0, v1, :cond_0

    .line 2368370
    iget-object v0, p0, LX/Gac;->c:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    iget-object v0, v0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->H:Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v0}, Lcom/facebook/widget/error/GenericErrorView;->a()V

    .line 2368371
    :goto_0
    iget-object v0, p0, LX/Gac;->c:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    iget-object v0, v0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->G:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2368372
    iget-object v0, p0, LX/Gac;->c:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    iget-object v0, v0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->H:Lcom/facebook/widget/error/GenericErrorView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/error/GenericErrorView;->setVisibility(I)V

    .line 2368373
    return-void

    .line 2368374
    :cond_0
    iget-object v0, p0, LX/Gac;->c:Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;

    iget-object v0, v0, Lcom/facebook/composer/shareintent/prefill/PrefilledComposerLauncherActivity;->H:Lcom/facebook/widget/error/GenericErrorView;

    invoke-virtual {v0}, Lcom/facebook/widget/error/GenericErrorView;->b()V

    goto :goto_0
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2368353
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, p1}, LX/Gac;->a(Lcom/facebook/graphql/executor/GraphQLResult;)V

    return-void
.end method
