.class public LX/FdI;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field public a:Lcom/facebook/fbui/widget/contentview/ContentView;

.field public b:Lcom/facebook/widget/SwitchCompat;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2263386
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/FdI;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2263387
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2263374
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2263375
    const/4 p3, 0x0

    .line 2263376
    const v0, 0x7f0312a3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2263377
    const v0, 0x7f0217ca

    invoke-virtual {p0, v0}, LX/FdI;->setBackgroundResource(I)V

    .line 2263378
    const v0, 0x7f0d02c4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    iput-object v0, p0, LX/FdI;->a:Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2263379
    const v0, 0x7f0d2b95

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/CustomFrameLayout;

    .line 2263380
    new-instance p2, Lcom/facebook/widget/SwitchCompat;

    invoke-direct {p2, p1}, Lcom/facebook/widget/SwitchCompat;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, LX/FdI;->b:Lcom/facebook/widget/SwitchCompat;

    .line 2263381
    iget-object p2, p0, LX/FdI;->b:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {p2, p3}, Lcom/facebook/widget/SwitchCompat;->setClickable(Z)V

    .line 2263382
    iget-object p2, p0, LX/FdI;->b:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {p2, p3}, Lcom/facebook/widget/SwitchCompat;->setFocusable(Z)V

    .line 2263383
    iget-object p2, p0, LX/FdI;->b:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/CustomFrameLayout;->addView(Landroid/view/View;)V

    .line 2263384
    return-void
.end method


# virtual methods
.method public final isChecked()Z
    .locals 1

    .prologue
    .line 2263385
    iget-object v0, p0, LX/FdI;->b:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0}, Lcom/facebook/widget/SwitchCompat;->isChecked()Z

    move-result v0

    return v0
.end method

.method public final performClick()Z
    .locals 1

    .prologue
    .line 2263368
    invoke-virtual {p0}, LX/FdI;->toggle()V

    .line 2263369
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->performClick()Z

    move-result v0

    return v0
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 2263370
    iget-object v0, p0, LX/FdI;->b:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/SwitchCompat;->setChecked(Z)V

    .line 2263371
    return-void
.end method

.method public final toggle()V
    .locals 1

    .prologue
    .line 2263372
    iget-object v0, p0, LX/FdI;->b:Lcom/facebook/widget/SwitchCompat;

    invoke-virtual {v0}, Lcom/facebook/widget/SwitchCompat;->toggle()V

    .line 2263373
    return-void
.end method
