.class public final LX/H1B;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;)V
    .locals 0

    .prologue
    .line 2414862
    iput-object p1, p0, LX/H1B;->a:Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2414863
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2414864
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2414865
    iget-object v0, p0, LX/H1B;->a:Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    iget-object v0, v0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->v:LX/0gc;

    const v1, 0x7f0d1c0e

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2414866
    instance-of v1, v0, Lcom/facebook/mobileconfig/ui/SearchFragment;

    if-eqz v1, :cond_0

    .line 2414867
    check-cast v0, Lcom/facebook/mobileconfig/ui/SearchFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/mobileconfig/ui/SearchFragment;->a(Ljava/lang/CharSequence;)V

    .line 2414868
    :cond_0
    return-void
.end method
