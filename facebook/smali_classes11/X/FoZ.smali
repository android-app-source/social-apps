.class public final enum LX/FoZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FoZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FoZ;

.field public static final enum CHARITY:LX/FoZ;

.field public static final enum DAF_DISCLOSURE:LX/FoZ;

.field public static final enum DIVIDER:LX/FoZ;

.field public static final enum HIGHLIGHTED_CHARITY:LX/FoZ;

.field public static final enum LOADING:LX/FoZ;

.field public static final enum OTHER_CHARITIES_HEADER:LX/FoZ;

.field public static final enum SEE_MORE:LX/FoZ;

.field public static final enum SUBTITLE:LX/FoZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2289714
    new-instance v0, LX/FoZ;

    const-string v1, "SUBTITLE"

    invoke-direct {v0, v1, v3}, LX/FoZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FoZ;->SUBTITLE:LX/FoZ;

    .line 2289715
    new-instance v0, LX/FoZ;

    const-string v1, "SEE_MORE"

    invoke-direct {v0, v1, v4}, LX/FoZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FoZ;->SEE_MORE:LX/FoZ;

    .line 2289716
    new-instance v0, LX/FoZ;

    const-string v1, "CHARITY"

    invoke-direct {v0, v1, v5}, LX/FoZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FoZ;->CHARITY:LX/FoZ;

    .line 2289717
    new-instance v0, LX/FoZ;

    const-string v1, "HIGHLIGHTED_CHARITY"

    invoke-direct {v0, v1, v6}, LX/FoZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FoZ;->HIGHLIGHTED_CHARITY:LX/FoZ;

    .line 2289718
    new-instance v0, LX/FoZ;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v7}, LX/FoZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FoZ;->LOADING:LX/FoZ;

    .line 2289719
    new-instance v0, LX/FoZ;

    const-string v1, "OTHER_CHARITIES_HEADER"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/FoZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FoZ;->OTHER_CHARITIES_HEADER:LX/FoZ;

    .line 2289720
    new-instance v0, LX/FoZ;

    const-string v1, "DIVIDER"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/FoZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FoZ;->DIVIDER:LX/FoZ;

    .line 2289721
    new-instance v0, LX/FoZ;

    const-string v1, "DAF_DISCLOSURE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/FoZ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FoZ;->DAF_DISCLOSURE:LX/FoZ;

    .line 2289722
    const/16 v0, 0x8

    new-array v0, v0, [LX/FoZ;

    sget-object v1, LX/FoZ;->SUBTITLE:LX/FoZ;

    aput-object v1, v0, v3

    sget-object v1, LX/FoZ;->SEE_MORE:LX/FoZ;

    aput-object v1, v0, v4

    sget-object v1, LX/FoZ;->CHARITY:LX/FoZ;

    aput-object v1, v0, v5

    sget-object v1, LX/FoZ;->HIGHLIGHTED_CHARITY:LX/FoZ;

    aput-object v1, v0, v6

    sget-object v1, LX/FoZ;->LOADING:LX/FoZ;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/FoZ;->OTHER_CHARITIES_HEADER:LX/FoZ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/FoZ;->DIVIDER:LX/FoZ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/FoZ;->DAF_DISCLOSURE:LX/FoZ;

    aput-object v2, v0, v1

    sput-object v0, LX/FoZ;->$VALUES:[LX/FoZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2289724
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FoZ;
    .locals 1

    .prologue
    .line 2289725
    const-class v0, LX/FoZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FoZ;

    return-object v0
.end method

.method public static values()[LX/FoZ;
    .locals 1

    .prologue
    .line 2289723
    sget-object v0, LX/FoZ;->$VALUES:[LX/FoZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FoZ;

    return-object v0
.end method
