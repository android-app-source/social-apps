.class public final LX/Gvs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/4At;

.field public final synthetic b:LX/Gvy;


# direct methods
.method public constructor <init>(LX/Gvy;LX/4At;)V
    .locals 0

    .prologue
    .line 2406166
    iput-object p1, p0, LX/Gvs;->b:LX/Gvy;

    iput-object p2, p0, LX/Gvs;->a:LX/4At;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2406167
    iget-object v0, p0, LX/Gvs;->b:LX/Gvy;

    iget-object v0, v0, LX/Gvy;->c:LX/0Zb;

    iget-object v1, p0, LX/Gvs;->b:LX/Gvy;

    const-string v2, "platform_share_failed_publish"

    invoke-virtual {v1, v2}, LX/Gvy;->b(Ljava/lang/String;)LX/8Or;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/8Or;->a(Ljava/lang/Throwable;)LX/8Or;

    move-result-object v1

    invoke-virtual {v1}, LX/8Or;->a()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2406168
    iget-object v0, p0, LX/Gvs;->b:LX/Gvy;

    iget-object v1, p0, LX/Gvs;->b:LX/Gvy;

    iget-object v1, v1, LX/Gvy;->f:Lcom/facebook/platform/common/action/PlatformAppCall;

    const-string v2, "Error publishing message"

    invoke-static {v1, p1, v2}, LX/4hW;->a(Lcom/facebook/platform/common/action/PlatformAppCall;Ljava/lang/Throwable;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 2406169
    invoke-virtual {v0, v1}, LX/4hY;->c(Landroid/os/Bundle;)V

    .line 2406170
    iget-object v0, p0, LX/Gvs;->a:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2406171
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2406172
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2406173
    iget-object v0, p0, LX/Gvs;->b:LX/Gvy;

    iget-object v0, v0, LX/Gvy;->c:LX/0Zb;

    iget-object v1, p0, LX/Gvs;->b:LX/Gvy;

    const-string v2, "platform_share_finished_publish"

    invoke-virtual {v1, v2}, LX/Gvy;->b(Ljava/lang/String;)LX/8Or;

    move-result-object v1

    invoke-virtual {v1}, LX/8Or;->a()Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2406174
    iget-object v0, p1, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v1, v0

    .line 2406175
    if-nez v1, :cond_2

    .line 2406176
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2406177
    if-eqz v0, :cond_2

    .line 2406178
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    .line 2406179
    :goto_0
    iget-object v1, p0, LX/Gvs;->b:LX/Gvy;

    iget-object v2, p0, LX/Gvs;->b:LX/Gvy;

    iget-object v2, v2, LX/Gvy;->f:Lcom/facebook/platform/common/action/PlatformAppCall;

    iget-object v3, p0, LX/Gvs;->b:LX/Gvy;

    iget-boolean v3, v3, LX/Gvy;->n:Z

    iget-object v4, p0, LX/Gvs;->b:LX/Gvy;

    iget-boolean v4, v4, LX/Gvy;->o:Z

    const/4 p1, 0x1

    .line 2406180
    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/facebook/platform/common/action/PlatformAppCall;->d()Z

    move-result v5

    if-eqz v5, :cond_3

    move v8, p1

    .line 2406181
    :goto_1
    if-eqz v8, :cond_4

    const-string v5, "didComplete"

    move-object v7, v5

    .line 2406182
    :goto_2
    if-eqz v8, :cond_5

    const-string v5, "completionGesture"

    move-object v6, v5

    .line 2406183
    :goto_3
    if-eqz v8, :cond_6

    const-string v5, "postId"

    .line 2406184
    :goto_4
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 2406185
    invoke-virtual {v8, v7, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2406186
    if-eqz v3, :cond_0

    .line 2406187
    const-string v7, "post"

    invoke-virtual {v8, v6, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2406188
    :cond_0
    if-eqz v4, :cond_1

    .line 2406189
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 2406190
    invoke-virtual {v8, v5, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2406191
    :cond_1
    move-object v0, v8

    .line 2406192
    invoke-virtual {v1, v0}, LX/4hY;->d(Landroid/os/Bundle;)V

    .line 2406193
    iget-object v0, p0, LX/Gvs;->a:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2406194
    return-void

    :cond_2
    move-object v0, v1

    goto :goto_0

    .line 2406195
    :cond_3
    const/4 v5, 0x0

    move v8, v5

    goto :goto_1

    .line 2406196
    :cond_4
    const-string v5, "com.facebook.platform.extra.DID_COMPLETE"

    move-object v7, v5

    goto :goto_2

    .line 2406197
    :cond_5
    const-string v5, "com.facebook.platform.extra.COMPLETION_GESTURE"

    move-object v6, v5

    goto :goto_3

    .line 2406198
    :cond_6
    const-string v5, "com.facebook.platform.extra.POST_ID"

    goto :goto_4
.end method
