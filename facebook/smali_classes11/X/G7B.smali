.class public final enum LX/G7B;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/G7B;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/G7B;

.field public static final enum DIRECT_CALL:LX/G7B;

.field public static final enum SMS_ZERO_DIALOG:LX/G7B;

.field public static final enum STANDARD_ZERO_DIALOG:LX/G7B;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2321032
    new-instance v0, LX/G7B;

    const-string v1, "DIRECT_CALL"

    invoke-direct {v0, v1, v2}, LX/G7B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G7B;->DIRECT_CALL:LX/G7B;

    .line 2321033
    new-instance v0, LX/G7B;

    const-string v1, "STANDARD_ZERO_DIALOG"

    invoke-direct {v0, v1, v3}, LX/G7B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G7B;->STANDARD_ZERO_DIALOG:LX/G7B;

    .line 2321034
    new-instance v0, LX/G7B;

    const-string v1, "SMS_ZERO_DIALOG"

    invoke-direct {v0, v1, v4}, LX/G7B;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G7B;->SMS_ZERO_DIALOG:LX/G7B;

    .line 2321035
    const/4 v0, 0x3

    new-array v0, v0, [LX/G7B;

    sget-object v1, LX/G7B;->DIRECT_CALL:LX/G7B;

    aput-object v1, v0, v2

    sget-object v1, LX/G7B;->STANDARD_ZERO_DIALOG:LX/G7B;

    aput-object v1, v0, v3

    sget-object v1, LX/G7B;->SMS_ZERO_DIALOG:LX/G7B;

    aput-object v1, v0, v4

    sput-object v0, LX/G7B;->$VALUES:[LX/G7B;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2321036
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/G7B;
    .locals 1

    .prologue
    .line 2321037
    const-class v0, LX/G7B;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/G7B;

    return-object v0
.end method

.method public static values()[LX/G7B;
    .locals 1

    .prologue
    .line 2321038
    sget-object v0, LX/G7B;->$VALUES:[LX/G7B;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/G7B;

    return-object v0
.end method
