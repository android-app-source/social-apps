.class public LX/GoW;
.super LX/GoF;
.source ""

# interfaces
.implements LX/GoG;


# instance fields
.field public final a:LX/8Z4;

.field private b:LX/CHZ;

.field private c:Z


# direct methods
.method public constructor <init>(LX/CHZ;)V
    .locals 1

    .prologue
    .line 2394519
    invoke-interface {p1}, LX/CHY;->c()LX/CHa;

    move-result-object v0

    invoke-direct {p0, v0}, LX/GoF;-><init>(LX/CHa;)V

    .line 2394520
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GoW;->c:Z

    .line 2394521
    invoke-interface {p1}, LX/CHY;->d()LX/8Z4;

    move-result-object v0

    iput-object v0, p0, LX/GoW;->a:LX/8Z4;

    .line 2394522
    iput-object p1, p0, LX/GoW;->b:LX/CHZ;

    .line 2394523
    return-void
.end method


# virtual methods
.method public final f()LX/GoE;
    .locals 3

    .prologue
    .line 2394524
    new-instance v0, LX/GoE;

    iget-object v1, p0, LX/GoW;->b:LX/CHZ;

    invoke-interface {v1}, LX/CHY;->jt_()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GoW;->b:LX/CHZ;

    invoke-interface {v2}, LX/CHY;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/GoE;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final g()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394525
    iget-object v0, p0, LX/GoW;->b:LX/CHZ;

    invoke-interface {v0}, LX/CHY;->c()LX/CHa;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/GoW;->b:LX/CHZ;

    invoke-interface {v0}, LX/CHY;->c()LX/CHa;

    move-result-object v0

    invoke-interface {v0}, LX/CHa;->jv_()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;

    move-result-object v0

    goto :goto_0
.end method
