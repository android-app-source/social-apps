.class public final LX/GdY;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 27

    .prologue
    .line 2374189
    const/16 v23, 0x0

    .line 2374190
    const/16 v22, 0x0

    .line 2374191
    const/16 v21, 0x0

    .line 2374192
    const/16 v20, 0x0

    .line 2374193
    const/16 v19, 0x0

    .line 2374194
    const/16 v18, 0x0

    .line 2374195
    const/16 v17, 0x0

    .line 2374196
    const/16 v16, 0x0

    .line 2374197
    const/4 v15, 0x0

    .line 2374198
    const/4 v14, 0x0

    .line 2374199
    const/4 v13, 0x0

    .line 2374200
    const/4 v12, 0x0

    .line 2374201
    const/4 v11, 0x0

    .line 2374202
    const/4 v10, 0x0

    .line 2374203
    const/4 v9, 0x0

    .line 2374204
    const/4 v8, 0x0

    .line 2374205
    const/4 v7, 0x0

    .line 2374206
    const/4 v6, 0x0

    .line 2374207
    const/4 v5, 0x0

    .line 2374208
    const/4 v4, 0x0

    .line 2374209
    const/4 v3, 0x0

    .line 2374210
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_1

    .line 2374211
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2374212
    const/4 v3, 0x0

    .line 2374213
    :goto_0
    return v3

    .line 2374214
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2374215
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_11

    .line 2374216
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v24

    .line 2374217
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2374218
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_1

    if-eqz v24, :cond_1

    .line 2374219
    const-string v25, "__type__"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-nez v25, :cond_2

    const-string v25, "__typename"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_3

    .line 2374220
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v23

    move-object/from16 v0, p1

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v23

    goto :goto_1

    .line 2374221
    :cond_3
    const-string v25, "composer_actions"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_4

    .line 2374222
    invoke-static/range {p0 .. p1}, LX/A69;->a(LX/15w;LX/186;)I

    move-result v22

    goto :goto_1

    .line 2374223
    :cond_4
    const-string v25, "composer_placeholder_text"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_5

    .line 2374224
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    goto :goto_1

    .line 2374225
    :cond_5
    const-string v25, "customizable"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 2374226
    const/4 v8, 0x1

    .line 2374227
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v20

    goto :goto_1

    .line 2374228
    :cond_6
    const-string v25, "disabled_favorite_icon"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_7

    .line 2374229
    invoke-static/range {p0 .. p1}, LX/A6A;->a(LX/15w;LX/186;)I

    move-result v19

    goto :goto_1

    .line 2374230
    :cond_7
    const-string v25, "enabled_favorite_icon"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_8

    .line 2374231
    invoke-static/range {p0 .. p1}, LX/A6B;->a(LX/15w;LX/186;)I

    move-result v18

    goto/16 :goto_1

    .line 2374232
    :cond_8
    const-string v25, "feed_section_type"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_9

    .line 2374233
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/facebook/graphql/enums/GraphQLFeedSectionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLFeedSectionType;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v17

    goto/16 :goto_1

    .line 2374234
    :cond_9
    const-string v25, "header_image"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 2374235
    invoke-static/range {p0 .. p1}, LX/A6C;->a(LX/15w;LX/186;)I

    move-result v16

    goto/16 :goto_1

    .line 2374236
    :cond_a
    const-string v25, "id"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 2374237
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 2374238
    :cond_b
    const-string v25, "is_favorited"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_c

    .line 2374239
    const/4 v7, 0x1

    .line 2374240
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto/16 :goto_1

    .line 2374241
    :cond_c
    const-string v25, "live_video_count"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_d

    .line 2374242
    const/4 v6, 0x1

    .line 2374243
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    goto/16 :goto_1

    .line 2374244
    :cond_d
    const-string v25, "name"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_e

    .line 2374245
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 2374246
    :cond_e
    const-string v25, "searchable"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_f

    .line 2374247
    const/4 v5, 0x1

    .line 2374248
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto/16 :goto_1

    .line 2374249
    :cond_f
    const-string v25, "should_prefetch"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_10

    .line 2374250
    const/4 v4, 0x1

    .line 2374251
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto/16 :goto_1

    .line 2374252
    :cond_10
    const-string v25, "show_audience_header"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_0

    .line 2374253
    const/4 v3, 0x1

    .line 2374254
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto/16 :goto_1

    .line 2374255
    :cond_11
    const/16 v24, 0xf

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2374256
    const/16 v24, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2374257
    const/16 v23, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2374258
    const/16 v22, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2374259
    if-eqz v8, :cond_12

    .line 2374260
    const/4 v8, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 2374261
    :cond_12
    const/4 v8, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 2374262
    const/4 v8, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 2374263
    const/4 v8, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 2374264
    const/4 v8, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v8, v1}, LX/186;->b(II)V

    .line 2374265
    const/16 v8, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v15}, LX/186;->b(II)V

    .line 2374266
    if-eqz v7, :cond_13

    .line 2374267
    const/16 v7, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v14}, LX/186;->a(IZ)V

    .line 2374268
    :cond_13
    if-eqz v6, :cond_14

    .line 2374269
    const/16 v6, 0xa

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v13, v7}, LX/186;->a(III)V

    .line 2374270
    :cond_14
    const/16 v6, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12}, LX/186;->b(II)V

    .line 2374271
    if-eqz v5, :cond_15

    .line 2374272
    const/16 v5, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11}, LX/186;->a(IZ)V

    .line 2374273
    :cond_15
    if-eqz v4, :cond_16

    .line 2374274
    const/16 v4, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->a(IZ)V

    .line 2374275
    :cond_16
    if-eqz v3, :cond_17

    .line 2374276
    const/16 v3, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->a(IZ)V

    .line 2374277
    :cond_17
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x0

    .line 2374278
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2374279
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2374280
    if-eqz v0, :cond_0

    .line 2374281
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2374282
    invoke-static {p0, p1, v2, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2374283
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2374284
    if-eqz v0, :cond_1

    .line 2374285
    const-string v1, "composer_actions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2374286
    invoke-static {p0, v0, p2, p3}, LX/A69;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2374287
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2374288
    if-eqz v0, :cond_2

    .line 2374289
    const-string v1, "composer_placeholder_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2374290
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2374291
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2374292
    if-eqz v0, :cond_3

    .line 2374293
    const-string v1, "customizable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2374294
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2374295
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2374296
    if-eqz v0, :cond_4

    .line 2374297
    const-string v1, "disabled_favorite_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2374298
    invoke-static {p0, v0, p2}, LX/A6A;->a(LX/15i;ILX/0nX;)V

    .line 2374299
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2374300
    if-eqz v0, :cond_5

    .line 2374301
    const-string v1, "enabled_favorite_icon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2374302
    invoke-static {p0, v0, p2}, LX/A6B;->a(LX/15i;ILX/0nX;)V

    .line 2374303
    :cond_5
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2374304
    if-eqz v0, :cond_6

    .line 2374305
    const-string v0, "feed_section_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2374306
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2374307
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2374308
    if-eqz v0, :cond_7

    .line 2374309
    const-string v1, "header_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2374310
    invoke-static {p0, v0, p2}, LX/A6C;->a(LX/15i;ILX/0nX;)V

    .line 2374311
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2374312
    if-eqz v0, :cond_8

    .line 2374313
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2374314
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2374315
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2374316
    if-eqz v0, :cond_9

    .line 2374317
    const-string v1, "is_favorited"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2374318
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2374319
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2374320
    if-eqz v0, :cond_a

    .line 2374321
    const-string v1, "live_video_count"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2374322
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2374323
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2374324
    if-eqz v0, :cond_b

    .line 2374325
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2374326
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2374327
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2374328
    if-eqz v0, :cond_c

    .line 2374329
    const-string v1, "searchable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2374330
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2374331
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2374332
    if-eqz v0, :cond_d

    .line 2374333
    const-string v1, "should_prefetch"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2374334
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2374335
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2374336
    if-eqz v0, :cond_e

    .line 2374337
    const-string v1, "show_audience_header"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2374338
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2374339
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2374340
    return-void
.end method
