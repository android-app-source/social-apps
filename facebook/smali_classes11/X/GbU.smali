.class public final enum LX/GbU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GbU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GbU;

.field public static final enum DBL_WITHOUT_PIN:LX/GbU;

.field public static final enum DBL_WITHOUT_PIN_CAN_SAVE_ID:LX/GbU;

.field public static final enum DBL_WITH_PIN:LX/GbU;

.field public static final enum DBL_WITH_PIN_CAN_SAVE_ID:LX/GbU;

.field public static final enum NO_DBL:LX/GbU;

.field public static final enum SAVED_ID:LX/GbU;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2369773
    new-instance v0, LX/GbU;

    const-string v1, "NO_DBL"

    invoke-direct {v0, v1, v3}, LX/GbU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GbU;->NO_DBL:LX/GbU;

    .line 2369774
    new-instance v0, LX/GbU;

    const-string v1, "DBL_WITHOUT_PIN"

    invoke-direct {v0, v1, v4}, LX/GbU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GbU;->DBL_WITHOUT_PIN:LX/GbU;

    .line 2369775
    new-instance v0, LX/GbU;

    const-string v1, "DBL_WITH_PIN"

    invoke-direct {v0, v1, v5}, LX/GbU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GbU;->DBL_WITH_PIN:LX/GbU;

    .line 2369776
    new-instance v0, LX/GbU;

    const-string v1, "SAVED_ID"

    invoke-direct {v0, v1, v6}, LX/GbU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GbU;->SAVED_ID:LX/GbU;

    .line 2369777
    new-instance v0, LX/GbU;

    const-string v1, "DBL_WITHOUT_PIN_CAN_SAVE_ID"

    invoke-direct {v0, v1, v7}, LX/GbU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GbU;->DBL_WITHOUT_PIN_CAN_SAVE_ID:LX/GbU;

    .line 2369778
    new-instance v0, LX/GbU;

    const-string v1, "DBL_WITH_PIN_CAN_SAVE_ID"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/GbU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GbU;->DBL_WITH_PIN_CAN_SAVE_ID:LX/GbU;

    .line 2369779
    const/4 v0, 0x6

    new-array v0, v0, [LX/GbU;

    sget-object v1, LX/GbU;->NO_DBL:LX/GbU;

    aput-object v1, v0, v3

    sget-object v1, LX/GbU;->DBL_WITHOUT_PIN:LX/GbU;

    aput-object v1, v0, v4

    sget-object v1, LX/GbU;->DBL_WITH_PIN:LX/GbU;

    aput-object v1, v0, v5

    sget-object v1, LX/GbU;->SAVED_ID:LX/GbU;

    aput-object v1, v0, v6

    sget-object v1, LX/GbU;->DBL_WITHOUT_PIN_CAN_SAVE_ID:LX/GbU;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/GbU;->DBL_WITH_PIN_CAN_SAVE_ID:LX/GbU;

    aput-object v2, v0, v1

    sput-object v0, LX/GbU;->$VALUES:[LX/GbU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2369780
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GbU;
    .locals 1

    .prologue
    .line 2369781
    const-class v0, LX/GbU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GbU;

    return-object v0
.end method

.method public static values()[LX/GbU;
    .locals 1

    .prologue
    .line 2369782
    sget-object v0, LX/GbU;->$VALUES:[LX/GbU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GbU;

    return-object v0
.end method
