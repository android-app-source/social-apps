.class public LX/GG6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/GG6;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/154;

.field private final c:Ljava/text/NumberFormat;

.field private final d:LX/0SG;

.field public final e:LX/2U3;

.field public final f:LX/0hy;

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/154;LX/0SG;LX/2U3;LX/0Or;LX/0hy;)V
    .locals 2
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/res/Resources;",
            "LX/154;",
            "LX/0SG;",
            "LX/2U3;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/0hy;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2333055
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2333056
    iput-object p1, p0, LX/GG6;->a:Landroid/content/res/Resources;

    .line 2333057
    iput-object p2, p0, LX/GG6;->b:LX/154;

    .line 2333058
    iput-object p3, p0, LX/GG6;->d:LX/0SG;

    .line 2333059
    iput-object p4, p0, LX/GG6;->e:LX/2U3;

    .line 2333060
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, LX/GG6;->c:Ljava/text/NumberFormat;

    .line 2333061
    iget-object v0, p0, LX/GG6;->c:Ljava/text/NumberFormat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 2333062
    iget-object v0, p0, LX/GG6;->c:Ljava/text/NumberFormat;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 2333063
    iput-object p5, p0, LX/GG6;->g:LX/0Or;

    .line 2333064
    iput-object p6, p0, LX/GG6;->f:LX/0hy;

    .line 2333065
    return-void
.end method

.method public static a(JJ)I
    .locals 4

    .prologue
    .line 2333066
    sub-long v0, p2, p0

    const-wide/32 v2, 0x5265c00

    div-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public static a(LX/0Px;Ljava/lang/String;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;",
            ">;",
            "Ljava/lang/String;",
            ")I"
        }
    .end annotation

    .prologue
    .line 2333067
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2333068
    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->t()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2333069
    :goto_1
    return v1

    .line 2333070
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2333071
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public static a(LX/15i;I)LX/0Px;
    .locals 8
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getBoostedComponentAudienceModels"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15i;",
            "I)",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    const v6, -0x356d9279    # -4798147.5f

    .line 2333072
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 2333073
    :goto_0
    invoke-static {p0, p1, v7, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_1
    invoke-virtual {v2}, LX/39O;->c()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 2333074
    invoke-static {p0, p1, v7, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_2
    invoke-virtual {v2, v0}, LX/2uF;->a(I)LX/1vs;

    move-result-object v2

    iget-object v4, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const-class v5, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    invoke-virtual {v4, v2, v1, v5}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2333075
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto :goto_1

    .line 2333076
    :cond_1
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto :goto_2

    .line 2333077
    :cond_2
    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$GeoLocationsModel;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$GeoLocationsModel;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2333078
    if-eqz p0, :cond_0

    .line 2333079
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$GeoLocationsModel;->a()LX/0Px;

    move-result-object v0

    .line 2333080
    :goto_0
    return-object v0

    .line 2333081
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2333082
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/GG6;
    .locals 10

    .prologue
    .line 2333083
    sget-object v0, LX/GG6;->h:LX/GG6;

    if-nez v0, :cond_1

    .line 2333084
    const-class v1, LX/GG6;

    monitor-enter v1

    .line 2333085
    :try_start_0
    sget-object v0, LX/GG6;->h:LX/GG6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2333086
    if-eqz v2, :cond_0

    .line 2333087
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2333088
    new-instance v3, LX/GG6;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static {v0}, LX/154;->a(LX/0QB;)LX/154;

    move-result-object v5

    check-cast v5, LX/154;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static {v0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v7

    check-cast v7, LX/2U3;

    const/16 v8, 0xc

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v9

    check-cast v9, LX/0hy;

    invoke-direct/range {v3 .. v9}, LX/GG6;-><init>(Landroid/content/res/Resources;LX/154;LX/0SG;LX/2U3;LX/0Or;LX/0hy;)V

    .line 2333089
    move-object v0, v3

    .line 2333090
    sput-object v0, LX/GG6;->h:LX/GG6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2333091
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2333092
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2333093
    :cond_1
    sget-object v0, LX/GG6;->h:LX/GG6;

    return-object v0

    .line 2333094
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2333095
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;)LX/GGB;
    .locals 2

    .prologue
    .line 2333096
    sget-object v0, LX/GG4;->c:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2333097
    sget-object v0, LX/GGB;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:LX/GGB;

    :goto_0
    return-object v0

    .line 2333098
    :pswitch_0
    sget-object v0, LX/GGB;->INACTIVE:LX/GGB;

    goto :goto_0

    .line 2333099
    :pswitch_1
    sget-object v0, LX/GGB;->ACTIVE:LX/GGB;

    goto :goto_0

    .line 2333100
    :pswitch_2
    sget-object v0, LX/GGB;->PAUSED:LX/GGB;

    goto :goto_0

    .line 2333101
    :pswitch_3
    sget-object v0, LX/GGB;->FINISHED:LX/GGB;

    goto :goto_0

    .line 2333102
    :pswitch_4
    sget-object v0, LX/GGB;->REJECTED:LX/GGB;

    goto :goto_0

    .line 2333103
    :pswitch_5
    sget-object v0, LX/GGB;->PENDING:LX/GGB;

    goto :goto_0

    .line 2333104
    :pswitch_6
    sget-object v0, LX/GGB;->ERROR:LX/GGB;

    goto :goto_0

    .line 2333105
    :pswitch_7
    sget-object v0, LX/GGB;->CREATING:LX/GGB;

    goto :goto_0

    .line 2333106
    :pswitch_8
    sget-object v0, LX/GGB;->EXTENDABLE:LX/GGB;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public static a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;Landroid/content/res/Resources;)Landroid/text/Spanned;
    .locals 12

    .prologue
    .line 2333107
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v0

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    invoke-static {v1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v2

    invoke-static {p0}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v1

    invoke-static {v0, v2, v3, v1}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v0

    .line 2333108
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v1, v1

    .line 2333109
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->F()J

    move-result-wide v2

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j()I

    move-result v1

    int-to-long v4, v1

    const-wide/32 v6, 0x15180

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    .line 2333110
    const/4 v8, 0x1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v9

    iget-object v9, v9, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v8, v9}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v8

    .line 2333111
    new-instance v9, Ljava/util/Date;

    const-wide/16 v10, 0x3e8

    mul-long/2addr v10, v2

    invoke-direct {v9, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v8, v9}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    move-object v1, v8

    .line 2333112
    const v2, 0x7f080b90

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v1, v3, v0

    invoke-virtual {p2, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;Ljava/lang/String;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;LX/0Px;)Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
            "Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;",
            "Ljava/lang/String;",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;",
            ">;)",
            "Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;"
        }
    .end annotation

    .prologue
    .line 2333113
    invoke-virtual {p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$GeoLocationsModel;

    move-result-object v0

    invoke-static {v0}, LX/GG6;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$GeoLocationsModel;)LX/0Px;

    move-result-object v0

    .line 2333114
    sget-object v1, LX/GGG;->REGION:LX/GGG;

    .line 2333115
    instance-of v2, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v2, :cond_3

    .line 2333116
    check-cast p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0, p0}, LX/GG6;->a(LX/0Px;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v2

    .line 2333117
    :goto_0
    move-object v2, v2

    .line 2333118
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->is_()Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->CUSTOM_LOCATION:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    if-ne v0, v3, :cond_2

    .line 2333119
    sget-object v0, LX/GGG;->ADDRESS:LX/GGG;

    .line 2333120
    :goto_1
    sget-object v1, LX/GGG;->ADDRESS:LX/GGG;

    if-ne v0, v1, :cond_1

    sget-object v1, LX/GGF;->HOME:LX/GGF;

    sget-object v3, LX/GGF;->RECENT:LX/GGF;

    invoke-static {v1, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2333121
    :goto_2
    invoke-virtual {p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->a()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2333122
    new-instance v5, LX/GGE;

    invoke-direct {v5}, LX/GGE;-><init>()V

    .line 2333123
    iput-object p1, v5, LX/GGE;->g:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2333124
    move-object v5, v5

    .line 2333125
    iput-object p2, v5, LX/GGE;->i:Ljava/lang/String;

    .line 2333126
    move-object v5, v5

    .line 2333127
    invoke-virtual {p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->n()I

    move-result v6

    .line 2333128
    iput v6, v5, LX/GGE;->b:I

    .line 2333129
    move-object v5, v5

    .line 2333130
    invoke-virtual {p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->m()I

    move-result v6

    .line 2333131
    iput v6, v5, LX/GGE;->c:I

    .line 2333132
    move-object v5, v5

    .line 2333133
    invoke-virtual {p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->j()LX/0Px;

    move-result-object v6

    .line 2333134
    sget-object p0, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->ALL:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    .line 2333135
    if-eqz v6, :cond_0

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result p1

    const/4 p2, 0x1

    if-ne p1, p2, :cond_0

    .line 2333136
    const/4 p0, 0x0

    invoke-virtual {v6, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    .line 2333137
    :cond_0
    move-object v6, p0

    .line 2333138
    iput-object v6, v5, LX/GGE;->a:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    .line 2333139
    move-object v5, v5

    .line 2333140
    invoke-virtual {p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$InterestsModel;

    move-result-object v6

    .line 2333141
    if-eqz v6, :cond_4

    .line 2333142
    invoke-virtual {v6}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$InterestsModel;->a()LX/0Px;

    move-result-object p0

    .line 2333143
    :goto_3
    move-object v6, p0

    .line 2333144
    iput-object v6, v5, LX/GGE;->e:LX/0Px;

    .line 2333145
    move-object v5, v5

    .line 2333146
    invoke-static {v4, v3}, LX/GG6;->b(LX/15i;I)LX/0Px;

    move-result-object v3

    .line 2333147
    iput-object v3, v5, LX/GGE;->f:LX/0Px;

    .line 2333148
    move-object v3, v5

    .line 2333149
    invoke-virtual {p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$GeoLocationsModel;

    move-result-object v4

    invoke-static {v4}, LX/GG6;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$GeoLocationsModel;)LX/0Px;

    move-result-object v4

    .line 2333150
    iput-object v4, v3, LX/GGE;->d:LX/0Px;

    .line 2333151
    move-object v3, v3

    .line 2333152
    iput-object v2, v3, LX/GGE;->j:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2333153
    move-object v2, v3

    .line 2333154
    iput-object v1, v2, LX/GGE;->h:LX/0Px;

    .line 2333155
    move-object v1, v2

    .line 2333156
    iput-object v0, v1, LX/GGE;->k:LX/GGG;

    .line 2333157
    move-object v0, v1

    .line 2333158
    iput-object p4, v0, LX/GGE;->l:LX/0Px;

    .line 2333159
    move-object v0, v0

    .line 2333160
    invoke-virtual {v0}, LX/GGE;->a()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    return-object v0

    .line 2333161
    :cond_1
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2333162
    goto :goto_2

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    invoke-static {v0}, LX/GG6;->d(LX/0Px;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v2

    goto/16 :goto_0

    .line 2333163
    :cond_4
    sget-object p0, LX/0Q7;->a:LX/0Px;

    move-object p0, p0

    .line 2333164
    goto :goto_3
.end method

.method public static a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;LX/GGB;)Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;
    .locals 2

    .prologue
    .line 2333165
    new-instance v0, LX/GGN;

    invoke-direct {v0, p0}, LX/GGN;-><init>(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    invoke-virtual {v0}, LX/GGN;->b()Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    move-result-object v0

    .line 2333166
    iput-object p1, v0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->f:LX/GGB;

    .line 2333167
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v1, v1

    .line 2333168
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 2333169
    return-object v0
.end method

.method public static a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;
    .locals 5

    .prologue
    .line 2333170
    const-string v0, "Ad interfaces data is null"

    invoke-static {p0, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2333171
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    const-string v1, "Admin Info is null"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2333172
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v0

    const-string v1, "Ad accounts are null"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2333173
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;->a()LX/0Px;

    move-result-object v0

    const-string v1, "No ad accounts found"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2333174
    const-string v0, "No ad account id specified"

    invoke-static {p1, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2333175
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    .line 2333176
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->t()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, p1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2333177
    return-object v0

    .line 2333178
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2333179
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ad account "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was not found"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(LX/0Px;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
            ">;",
            "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
            ")",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;"
        }
    .end annotation

    .prologue
    .line 2333190
    invoke-static {p0}, LX/GG6;->d(LX/0Px;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v0

    .line 2333191
    if-eqz v0, :cond_0

    .line 2333192
    :goto_0
    return-object v0

    .line 2333193
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-ne v0, v1, :cond_1

    .line 2333194
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->d:Lcom/facebook/adinterfaces/model/EventSpecModel;

    move-object v0, v0

    .line 2333195
    if-eqz v0, :cond_1

    .line 2333196
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->d:Lcom/facebook/adinterfaces/model/EventSpecModel;

    move-object v0, v0

    .line 2333197
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/EventSpecModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v0, v1

    .line 2333198
    if-eqz v0, :cond_1

    .line 2333199
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->d:Lcom/facebook/adinterfaces/model/EventSpecModel;

    move-object v0, v0

    .line 2333200
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/EventSpecModel;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v0, v1

    .line 2333201
    invoke-static {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v0

    goto :goto_0

    .line 2333202
    :cond_1
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v0, v0

    .line 2333203
    invoke-static {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/GGB;)Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;
    .locals 2

    .prologue
    .line 2333180
    sget-object v0, LX/GG4;->d:[I

    invoke-virtual {p0}, LX/GGB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2333181
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    :goto_0
    return-object v0

    .line 2333182
    :pswitch_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->INACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto :goto_0

    .line 2333183
    :pswitch_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto :goto_0

    .line 2333184
    :pswitch_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->PAUSED:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto :goto_0

    .line 2333185
    :pswitch_3
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->FINISHED:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto :goto_0

    .line 2333186
    :pswitch_4
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->EXTENDABLE:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto :goto_0

    .line 2333187
    :pswitch_5
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->REJECTED:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto :goto_0

    .line 2333188
    :pswitch_6
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->PENDING:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto :goto_0

    .line 2333189
    :pswitch_7
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;->ERROR:Lcom/facebook/graphql/enums/GraphQLBoostedPostStatus;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static a(ILandroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2333283
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    .line 2333284
    int-to-long v2, p0

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(JLandroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2333281
    const/4 v0, 0x1

    const/4 v1, 0x3

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0, v1, v2}, Ljava/text/DateFormat;->getDateTimeInstance(IILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    .line 2333282
    new-instance v1, Ljava/util/Date;

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p0

    invoke-direct {v1, v2, v3}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/GG6;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;LX/A9E;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;)V
    .locals 9

    .prologue
    .line 2333274
    if-eqz p3, :cond_0

    .line 2333275
    const/4 v0, 0x1

    new-array v8, v0, [F

    .line 2333276
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->ir_()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->j()D

    move-result-wide v2

    invoke-virtual {p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->ir_()D

    move-result-wide v4

    invoke-virtual {p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->j()D

    move-result-wide v6

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    .line 2333277
    const/4 v0, 0x0

    aget v0, v8, v0

    const/high16 v1, 0x42c80000    # 100.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    invoke-virtual {p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2333278
    :goto_0
    iput-object v0, p2, LX/A9E;->a:Ljava/lang/String;

    .line 2333279
    :cond_0
    return-void

    .line 2333280
    :cond_1
    iget-object v0, p0, LX/GG6;->a:Landroid/content/res/Resources;

    const v1, 0x7f080b36

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V
    .locals 2

    .prologue
    .line 2333269
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    .line 2333270
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->m()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->LOCAL:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-ne v0, v1, :cond_0

    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_POST:LX/8wL;

    if-ne v0, v1, :cond_0

    .line 2333271
    sget-object v0, LX/GGG;->ADDRESS:LX/GGG;

    .line 2333272
    iput-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    .line 2333273
    :cond_0
    return-void
.end method

.method public static a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;Z)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2333252
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2333253
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->v()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v3

    move v1, v3

    :goto_0
    if-ge v4, v7, :cond_1

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    .line 2333254
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 2333255
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    .line 2333256
    :goto_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v0

    goto :goto_0

    .line 2333257
    :cond_0
    if-eqz p2, :cond_5

    .line 2333258
    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v2

    .line 2333259
    goto :goto_1

    .line 2333260
    :cond_1
    if-nez v1, :cond_2

    .line 2333261
    :goto_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 2333262
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->FANS:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-eq v0, v4, :cond_3

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->GROUPER:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-eq v0, v4, :cond_3

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v0

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-eq v0, v4, :cond_3

    .line 2333263
    invoke-virtual {v5, v3, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 2333264
    :goto_3
    if-nez v2, :cond_2

    .line 2333265
    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2333266
    :cond_2
    invoke-static {v5}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(LX/0Px;)V

    .line 2333267
    return-void

    .line 2333268
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_4
    move v2, v1

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public static a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2332715
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2332716
    :cond_0
    :goto_0
    return v0

    .line 2332717
    :cond_1
    iget-object v2, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v2, v2

    .line 2332718
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->o()Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;->LIFETIME_BUDGET:Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    if-ne v2, v3, :cond_3

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v2

    sget-object v3, LX/8wL;->BOOSTED_COMPONENT_EDIT_DURATION_BUDGET:LX/8wL;

    if-eq v2, v3, :cond_3

    .line 2332719
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v2

    sget-object v3, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-eq v2, v3, :cond_2

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v2

    sget-object v3, LX/8wL;->BOOST_POST:LX/8wL;

    if-ne v2, v3, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 2332720
    :cond_3
    iget-object v2, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v2, v2

    .line 2332721
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2332722
    iget-object v2, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v2, v2

    .line 2332723
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Z)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    const/4 v0, 0x1

    .line 2333232
    iget-object v2, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v2, v2

    .line 2333233
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-result-object v2

    .line 2333234
    if-nez v2, :cond_1

    .line 2333235
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 2333236
    :cond_1
    sget-object v3, LX/GG4;->a:[I

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v4

    invoke-virtual {v4}, LX/8wL;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2333237
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v3

    .line 2333238
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v4

    invoke-static {p0, v5, v5, v4, v5}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;Ljava/lang/String;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;LX/0Px;)Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v4

    .line 2333239
    iget-object v5, v3, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v5, v5

    .line 2333240
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v6

    if-ne v5, v6, :cond_0

    .line 2333241
    iget-object v5, v3, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v5, v5

    .line 2333242
    if-eqz v5, :cond_2

    .line 2333243
    iget-object v5, v3, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v5, v5

    .line 2333244
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2333245
    :cond_2
    iget-object v5, v3, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->q:Ljava/lang/String;

    move-object v5, v5

    .line 2333246
    if-eqz v5, :cond_3

    .line 2333247
    iget-object v5, v3, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->q:Ljava/lang/String;

    move-object v5, v5

    .line 2333248
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->n()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2333249
    :cond_3
    if-nez p1, :cond_4

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    move v0, v1

    .line 2333250
    goto :goto_0

    .line 2333251
    :cond_4
    invoke-virtual {v3, v4}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static b(I)J
    .locals 4

    .prologue
    const-wide/32 v0, 0x15180

    .line 2333230
    if-gtz p0, :cond_0

    .line 2333231
    :goto_0
    return-wide v0

    :cond_0
    int-to-long v2, p0

    mul-long/2addr v0, v2

    goto :goto_0
.end method

.method private static b(LX/15i;I)LX/0Px;
    .locals 5
    .param p0    # LX/15i;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p1    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDetailedTargetingsFromFlexibleSpec"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/15i;",
            "I)",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;",
            ">;"
        }
    .end annotation

    .prologue
    const v4, 0x7935731c

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2333215
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2333216
    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 2333217
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2333218
    :goto_1
    return-object v0

    .line 2333219
    :cond_0
    invoke-static {p0, p1, v1, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    goto :goto_0

    :cond_1
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_2

    .line 2333220
    :cond_2
    invoke-static {p0, p1, v1, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2333221
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2333222
    const-class v4, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;

    invoke-virtual {v1, v0, v2, v4}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    :goto_4
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    :goto_5
    if-ge v2, v4, :cond_5

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;

    .line 2333223
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2333224
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 2333225
    :cond_3
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_3

    .line 2333226
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2333227
    :cond_4
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2333228
    move-object v1, v0

    goto :goto_4

    .line 2333229
    :cond_5
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_1
.end method

.method public static b(LX/0Px;Ljava/lang/String;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2333209
    if-nez p1, :cond_1

    move-object v0, v1

    .line 2333210
    :cond_0
    :goto_0
    return-object v0

    .line 2333211
    :cond_1
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_2

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    .line 2333212
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2333213
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 2333214
    goto :goto_0
.end method

.method public static c(JJ)I
    .locals 6

    .prologue
    const-wide/32 v4, 0x5265c00

    .line 2333204
    sub-long v2, p2, p0

    .line 2333205
    div-long v0, v2, v4

    .line 2333206
    rem-long/2addr v2, v4

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 2333207
    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    .line 2333208
    :cond_0
    long-to-int v0, v0

    return v0
.end method

.method public static d(LX/0Px;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
            ">;)",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;"
        }
    .end annotation

    .prologue
    .line 2333047
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2333048
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->is_()Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->CUSTOM_LOCATION:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    if-ne v3, v4, :cond_0

    .line 2333049
    :goto_1
    return-object v0

    .line 2333050
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2333051
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;
    .locals 2

    .prologue
    .line 2333052
    const-string v0, "Ad interfaces data is null"

    invoke-static {p0, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2333053
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v0

    const-string v1, "No selected ad account Id"

    invoke-static {v0, v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2333054
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    return-object v0
.end method

.method public static f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)I
    .locals 1

    .prologue
    .line 2332666
    invoke-static {p0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v0

    return v0
.end method

.method public static g(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;
    .locals 4

    .prologue
    .line 2332667
    new-instance v0, LX/GGN;

    invoke-direct {v0}, LX/GGN;-><init>()V

    .line 2332668
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v1, v1

    .line 2332669
    iput-object v1, v0, LX/GGN;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 2332670
    move-object v0, v0

    .line 2332671
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v1

    .line 2332672
    iput-object v1, v0, LX/GGI;->e:LX/GGB;

    .line 2332673
    move-object v0, v0

    .line 2332674
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c()Ljava/lang/String;

    move-result-object v1

    .line 2332675
    iput-object v1, v0, LX/GGI;->c:Ljava/lang/String;

    .line 2332676
    move-object v0, v0

    .line 2332677
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d()Ljava/lang/String;

    move-result-object v1

    .line 2332678
    iput-object v1, v0, LX/GGI;->d:Ljava/lang/String;

    .line 2332679
    move-object v0, v0

    .line 2332680
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v1

    .line 2332681
    iput-object v1, v0, LX/GGI;->b:LX/8wL;

    .line 2332682
    move-object v0, v0

    .line 2332683
    new-instance v1, LX/A93;

    invoke-direct {v1}, LX/A93;-><init>()V

    new-instance v2, LX/A92;

    invoke-direct {v2}, LX/A92;-><init>()V

    invoke-static {p0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v3

    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 2332684
    iput-object v3, v2, LX/A92;->a:LX/0Px;

    .line 2332685
    move-object v2, v2

    .line 2332686
    invoke-virtual {v2}, LX/A92;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v2

    .line 2332687
    iput-object v2, v1, LX/A93;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    .line 2332688
    move-object v1, v1

    .line 2332689
    invoke-virtual {v1}, LX/A93;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v1

    .line 2332690
    iput-object v1, v0, LX/GGI;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    .line 2332691
    move-object v0, v0

    .line 2332692
    invoke-virtual {v0}, LX/GGI;->a()Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    return-object v0
.end method

.method public static g(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2332693
    invoke-static {p0}, LX/GG6;->h(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    .line 2332694
    :goto_0
    return v0

    .line 2332695
    :cond_1
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->u()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2332696
    goto :goto_0

    .line 2332697
    :cond_2
    invoke-static {p0}, LX/GG6;->i(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    .line 2332698
    goto :goto_0

    .line 2332699
    :cond_3
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v3, v2

    :goto_1
    if-ge v3, v5, :cond_5

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    .line 2332700
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->q()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    .line 2332701
    goto :goto_0

    .line 2332702
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_5
    move v0, v2

    .line 2332703
    goto :goto_0
.end method

.method public static h(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 1

    .prologue
    .line 2332704
    invoke-static {p0}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2332705
    :try_start_0
    invoke-static {p0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2332706
    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 2332707
    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 1

    .prologue
    .line 2332708
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static i(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Z
    .locals 1

    .prologue
    .line 2332709
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2332710
    if-eqz v0, :cond_0

    .line 2332711
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2332712
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2332713
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2332714
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 2332724
    invoke-static {p1}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2332725
    iget-object v1, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v1, v1

    .line 2332726
    if-eqz v1, :cond_1

    .line 2332727
    iget-object v1, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v1, v1

    .line 2332728
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->G()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    .line 2332729
    :goto_0
    if-eqz v1, :cond_4

    .line 2332730
    iget-object v1, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v1, v1

    .line 2332731
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-result-object v3

    .line 2332732
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v1

    move-object v2, v1

    :goto_1
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2332733
    :cond_0
    iget-object v1, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v1, v1

    .line 2332734
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->G()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v4

    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->o()LX/0Px;

    move-result-object v1

    :goto_2
    invoke-static {p1, v2, v0, v4, v1}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;Ljava/lang/String;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;LX/0Px;)Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2332735
    sget-object v1, LX/GGG;->ADDRESS:LX/GGG;

    .line 2332736
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    .line 2332737
    sget-object v1, LX/GGF;->HOME:LX/GGF;

    sget-object v2, LX/GGF;->RECENT:LX/GGF;

    invoke-static {v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2332738
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->n:LX/0Px;

    .line 2332739
    :goto_3
    return-object v0

    :cond_1
    move v1, v5

    .line 2332740
    goto :goto_0

    .line 2332741
    :cond_2
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v2, v1

    goto :goto_1

    :cond_3
    sget-object v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->c:LX/0Px;

    goto :goto_2

    .line 2332742
    :cond_4
    const/16 v3, 0xd

    .line 2332743
    const/16 v1, 0x41

    .line 2332744
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->ALL:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    .line 2332745
    iget-object v4, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v4, v4

    .line 2332746
    if-eqz v4, :cond_7

    .line 2332747
    iget-object v4, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v4, v4

    .line 2332748
    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 2332749
    iget-object v4, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v4, v4

    .line 2332750
    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 2332751
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2332752
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v1

    .line 2332753
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->n()I

    move-result v4

    .line 2332754
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->m()I

    move-result v3

    .line 2332755
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2332756
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    move v2, v3

    move v3, v4

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    .line 2332757
    :goto_4
    new-instance v4, LX/GGE;

    invoke-direct {v4}, LX/GGE;-><init>()V

    .line 2332758
    iput v3, v4, LX/GGE;->b:I

    .line 2332759
    move-object v3, v4

    .line 2332760
    iput v2, v3, LX/GGE;->c:I

    .line 2332761
    move-object v2, v3

    .line 2332762
    iput-object v1, v2, LX/GGE;->a:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    .line 2332763
    move-object v1, v2

    .line 2332764
    sget-object v2, LX/GGG;->ADDRESS:LX/GGG;

    .line 2332765
    iput-object v2, v1, LX/GGE;->k:LX/GGG;

    .line 2332766
    move-object v1, v1

    .line 2332767
    sget-object v2, LX/GGF;->HOME:LX/GGF;

    sget-object v3, LX/GGF;->RECENT:LX/GGF;

    invoke-static {v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2332768
    iput-object v2, v1, LX/GGE;->h:LX/0Px;

    .line 2332769
    move-object v1, v1

    .line 2332770
    sget-object v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->c:LX/0Px;

    .line 2332771
    iput-object v2, v1, LX/GGE;->l:LX/0Px;

    .line 2332772
    move-object v1, v1

    .line 2332773
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$GeoLocationsModel;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$GeoLocationsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$GeoLocationsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    .line 2332774
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$GeoLocationsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$GeoLocationsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/GG6;->d(LX/0Px;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v0

    .line 2332775
    invoke-static {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v2

    .line 2332776
    new-instance v7, LX/A9E;

    invoke-direct {v7}, LX/A9E;-><init>()V

    .line 2332777
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->a()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, LX/A9E;->a:Ljava/lang/String;

    .line 2332778
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->b()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, LX/A9E;->b:Ljava/lang/String;

    .line 2332779
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->c()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, LX/A9E;->c:Ljava/lang/String;

    .line 2332780
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->d()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, LX/A9E;->d:Ljava/lang/String;

    .line 2332781
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->e()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, LX/A9E;->e:Ljava/lang/String;

    .line 2332782
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->ir_()D

    move-result-wide v9

    iput-wide v9, v7, LX/A9E;->f:D

    .line 2332783
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->is_()Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    move-result-object v8

    iput-object v8, v7, LX/A9E;->g:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    .line 2332784
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->j()D

    move-result-wide v9

    iput-wide v9, v7, LX/A9E;->h:D

    .line 2332785
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->k()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, LX/A9E;->i:Ljava/lang/String;

    .line 2332786
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->l()D

    move-result-wide v9

    iput-wide v9, v7, LX/A9E;->j:D

    .line 2332787
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->m()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, LX/A9E;->k:Ljava/lang/String;

    .line 2332788
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->n()Z

    move-result v8

    iput-boolean v8, v7, LX/A9E;->l:Z

    .line 2332789
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->o()Z

    move-result v8

    iput-boolean v8, v7, LX/A9E;->m:Z

    .line 2332790
    move-object v2, v7

    .line 2332791
    check-cast p1, Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    .line 2332792
    iget-object v3, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v3, v3

    .line 2332793
    invoke-static {p0, v0, v2, v3}, LX/GG6;->a(LX/GG6;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;LX/A9E;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;)V

    .line 2332794
    invoke-virtual {v2}, LX/A9E;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2332795
    iput-object v0, v1, LX/GGE;->d:LX/0Px;

    .line 2332796
    invoke-virtual {v2}, LX/A9E;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-static {v0, p1}, LX/GG6;->a(LX/0Px;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v0

    .line 2332797
    iput-object v0, v1, LX/GGE;->j:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2332798
    :cond_5
    invoke-virtual {v1}, LX/GGE;->a()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    goto/16 :goto_3

    :cond_6
    move-object v0, v1

    move-object v1, v2

    move v2, v3

    move v3, v4

    goto/16 :goto_4

    :cond_7
    move-object v6, v2

    move v2, v1

    move-object v1, v6

    goto/16 :goto_4
.end method

.method public static j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 2

    .prologue
    .line 2332799
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    .line 2332800
    sget-object v1, LX/GGB;->ACTIVE:LX/GGB;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/GGB;->EXTENDABLE:LX/GGB;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/GGB;->PENDING:LX/GGB;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/GGB;->PAUSED:LX/GGB;

    if-ne v0, v1, :cond_1

    .line 2332801
    :cond_0
    const/4 v0, 0x1

    .line 2332802
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static k(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 2

    .prologue
    .line 2332803
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    .line 2332804
    sget-object v1, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/GGB;->REJECTED:LX/GGB;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/GGB;->INACTIVE:LX/GGB;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/GGB;->FINISHED:LX/GGB;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2332805
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->v()LX/0Px;

    move-result-object v3

    .line 2332806
    if-nez v3, :cond_1

    move-object v0, v1

    .line 2332807
    :cond_0
    :goto_0
    return-object v0

    .line 2332808
    :cond_1
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    .line 2332809
    if-eqz v0, :cond_2

    .line 2332810
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v5

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-ne v5, v6, :cond_2

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 2332811
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 2332812
    goto :goto_0
.end method

.method public static o(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2332813
    invoke-static {p0}, LX/GG6;->s(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2332814
    invoke-static {p0}, LX/GG6;->t(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2332815
    return-void
.end method

.method private static q(LX/GG6;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2332816
    instance-of v0, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v0, :cond_8

    move-object v0, p1

    .line 2332817
    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2332818
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v2

    sget-object v3, LX/8wL;->LOCAL_AWARENESS:LX/8wL;

    if-ne v2, v3, :cond_0

    .line 2332819
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-direct {p0, p1}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2332820
    :goto_0
    return-object v0

    .line 2332821
    :cond_0
    invoke-static {p1}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2332822
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v2, v2

    .line 2332823
    if-eqz v2, :cond_6

    .line 2332824
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v2, v2

    .line 2332825
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->G()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 2332826
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v2, v2

    .line 2332827
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-result-object v4

    .line 2332828
    if-eqz v4, :cond_4

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v2

    move-object v3, v2

    :goto_1
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 2332829
    :cond_1
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v2, v2

    .line 2332830
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->G()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v5

    if-eqz v4, :cond_5

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->o()LX/0Px;

    move-result-object v2

    :goto_2
    invoke-static {p1, v3, v1, v5, v2}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;Ljava/lang/String;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;LX/0Px;)Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v2

    .line 2332831
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v1, v1

    .line 2332832
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->G()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->a()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2332833
    invoke-static {v3, v1}, LX/GG6;->b(LX/15i;I)LX/0Px;

    move-result-object v1

    .line 2332834
    iput-object v1, v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    .line 2332835
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->SAVED_AUDIENCE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-ne v1, v3, :cond_2

    .line 2332836
    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->n()Ljava/lang/String;

    move-result-object v1

    .line 2332837
    iput-object v1, v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->q:Ljava/lang/String;

    .line 2332838
    :cond_2
    iget-object v1, v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    move-object v1, v1

    .line 2332839
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_3

    .line 2332840
    iget-object v1, v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    move-object v1, v1

    .line 2332841
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->is_()Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->CUSTOM_LOCATION:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    if-ne v1, v3, :cond_3

    .line 2332842
    sget-object v1, LX/GGG;->ADDRESS:LX/GGG;

    .line 2332843
    iput-object v1, v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    .line 2332844
    invoke-static {v0}, LX/GG6;->i(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2332845
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v1

    .line 2332846
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$GeoLocationsModel;

    move-result-object v0

    invoke-static {v0}, LX/GG6;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$GeoLocationsModel;)LX/0Px;

    move-result-object v0

    .line 2332847
    iput-object v0, v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    .line 2332848
    :cond_3
    move-object v0, v2

    .line 2332849
    goto/16 :goto_0

    .line 2332850
    :cond_4
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v3, v2

    goto :goto_1

    :cond_5
    sget-object v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->a:LX/0Px;

    goto :goto_2

    .line 2332851
    :cond_6
    invoke-static {v0}, LX/GG6;->i(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2332852
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2332853
    iget-object v3, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v3

    .line 2332854
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v0

    sget-object v3, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->a:LX/0Px;

    invoke-static {p1, v2, v1, v0, v3}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;Ljava/lang/String;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;LX/0Px;)Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2332855
    invoke-static {p1, v0}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    goto/16 :goto_0

    .line 2332856
    :cond_7
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    .line 2332857
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->v()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 2332858
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->v()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v0

    sget-object v3, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->a:LX/0Px;

    invoke-static {p1, v2, v1, v0, v3}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;Ljava/lang/String;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;LX/0Px;)Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2332859
    invoke-static {p1, v0}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    goto/16 :goto_0

    .line 2332860
    :cond_8
    new-instance v0, LX/GGE;

    invoke-direct {v0}, LX/GGE;-><init>()V

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2332861
    iput-object v2, v0, LX/GGE;->g:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2332862
    move-object v0, v0

    .line 2332863
    iput-object v1, v0, LX/GGE;->i:Ljava/lang/String;

    .line 2332864
    move-object v0, v0

    .line 2332865
    const/16 v1, 0xd

    .line 2332866
    iput v1, v0, LX/GGE;->b:I

    .line 2332867
    move-object v0, v0

    .line 2332868
    const/16 v1, 0x41

    .line 2332869
    iput v1, v0, LX/GGE;->c:I

    .line 2332870
    move-object v0, v0

    .line 2332871
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->ALL:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    .line 2332872
    iput-object v1, v0, LX/GGE;->a:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    .line 2332873
    move-object v0, v0

    .line 2332874
    sget-object v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->a:LX/0Px;

    .line 2332875
    iput-object v1, v0, LX/GGE;->l:LX/0Px;

    .line 2332876
    move-object v0, v0

    .line 2332877
    invoke-virtual {v0}, LX/GGE;->a()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private static r(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)LX/1vs;
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getDefaultAvailableAudiences"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 2332878
    instance-of v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-nez v0, :cond_0

    .line 2332879
    invoke-static {v3, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    .line 2332880
    :goto_0
    return-object v0

    .line 2332881
    :cond_0
    check-cast p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2332882
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2332883
    if-eqz v0, :cond_2

    .line 2332884
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2332885
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2332886
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2332887
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2332888
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    .line 2332889
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2332890
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2332891
    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_1

    .line 2332892
    :cond_3
    invoke-static {v3, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    goto :goto_0
.end method

.method private static s(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 3

    .prologue
    .line 2332893
    invoke-static {p0}, LX/GG6;->r(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2332894
    if-eqz v0, :cond_0

    .line 2332895
    invoke-static {v1, v0}, LX/GG6;->a(LX/15i;I)LX/0Px;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a(LX/0Px;)V

    .line 2332896
    :cond_0
    return-void

    .line 2332897
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static t(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2332898
    invoke-static {p0}, LX/GG6;->r(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2332899
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2332900
    if-eqz v0, :cond_0

    .line 2332901
    invoke-virtual {v1, v0, v3}, LX/15i;->j(II)I

    move-result v0

    invoke-interface {p0, v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->d(I)V

    .line 2332902
    :goto_0
    return-void

    .line 2332903
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2332904
    :cond_0
    invoke-interface {p0, v3}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->d(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(J)I
    .locals 7

    .prologue
    const-wide/32 v4, 0x5265c00

    .line 2332905
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 2332906
    const/4 v0, 0x0

    .line 2332907
    :cond_0
    :goto_0
    return v0

    .line 2332908
    :cond_1
    iget-object v0, p0, LX/GG6;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    sub-long v0, p1, v0

    .line 2332909
    add-long/2addr v0, v4

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    div-long/2addr v0, v4

    long-to-int v0, v0

    .line 2332910
    if-gtz v0, :cond_0

    .line 2332911
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a()J
    .locals 4

    .prologue
    .line 2332912
    iget-object v0, p0, LX/GG6;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public final a(I)Ljava/lang/String;
    .locals 12

    .prologue
    .line 2332913
    iget-object v0, p0, LX/GG6;->b:LX/154;

    int-to-double v2, p1

    iget-object v1, p0, LX/GG6;->c:Ljava/text/NumberFormat;

    .line 2332914
    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 2332915
    iget v5, v0, LX/154;->f:I

    int-to-double v5, v5

    cmpl-double v5, v2, v5

    if-ltz v5, :cond_0

    .line 2332916
    invoke-static {v0, v2, v3}, LX/154;->a(LX/154;D)I

    move-result v5

    .line 2332917
    const-wide/high16 v7, 0x4024000000000000L    # 10.0

    int-to-double v9, v5

    invoke-static {v7, v8, v9, v10}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v7

    div-double v7, v2, v7

    invoke-virtual {v1, v7, v8}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v6

    .line 2332918
    invoke-static {v0, v6, v4, v5}, LX/154;->a(LX/154;Ljava/lang/String;Ljava/lang/Integer;I)Ljava/lang/String;

    move-result-object v5

    .line 2332919
    if-eqz v5, :cond_0

    .line 2332920
    :goto_0
    move-object v4, v5

    .line 2332921
    move-object v0, v4

    .line 2332922
    return-object v0

    :cond_0
    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v5

    goto :goto_0
.end method

.method public final b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;
    .locals 10

    .prologue
    const-wide/16 v4, 0x3e8

    const/4 v0, 0x0

    .line 2332923
    if-nez p1, :cond_0

    .line 2332924
    iget-object v1, p0, LX/GG6;->e:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "Null data cannot be initialized"

    invoke-virtual {v1, v2, v3}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    move-object p1, v0

    .line 2332925
    :goto_0
    return-object p1

    .line 2332926
    :cond_0
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2332927
    iget-object v1, p0, LX/GG6;->e:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "Data cannot be initialized without admin info"

    invoke-virtual {v1, v2, v3}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    move-object p1, v0

    .line 2332928
    goto :goto_0

    .line 2332929
    :cond_1
    invoke-virtual {p0, p1}, LX/GG6;->c(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b(Ljava/lang/String;)V

    .line 2332930
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2332931
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->j()LX/0Px;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_2
    move-object v2, v7

    .line 2332932
    :goto_1
    move-object v1, v2

    .line 2332933
    if-eqz v1, :cond_4

    .line 2332934
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V

    .line 2332935
    :goto_2
    instance-of v0, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v0, :cond_5

    move-object v0, p1

    .line 2332936
    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2332937
    iget-object v6, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v6, v6

    .line 2332938
    if-eqz v6, :cond_b

    invoke-static {v0}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 2332939
    iget-object v6, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v6, v6

    .line 2332940
    invoke-virtual {v6}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->F()J

    move-result-wide v6

    .line 2332941
    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    invoke-virtual {p0, v6, v7}, LX/GG6;->a(J)I

    move-result v6

    .line 2332942
    :goto_3
    move v0, v6

    .line 2332943
    invoke-interface {p1, v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a(I)V

    .line 2332944
    :goto_4
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-ne v0, v1, :cond_3

    invoke-static {p1}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, p1

    .line 2332945
    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2332946
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->d:Lcom/facebook/adinterfaces/model/EventSpecModel;

    move-object v1, v1

    .line 2332947
    iget-wide v6, v1, Lcom/facebook/adinterfaces/model/EventSpecModel;->e:J

    move-wide v2, v6

    .line 2332948
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v1

    .line 2332949
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->F()J

    move-result-wide v0

    .line 2332950
    mul-long/2addr v0, v4

    mul-long/2addr v2, v4

    .line 2332951
    invoke-static {v0, v1, v2, v3}, LX/GG6;->c(JJ)I

    move-result v6

    int-to-long v6, v6

    .line 2332952
    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-gez v8, :cond_e

    .line 2332953
    const/4 v6, 0x0

    .line 2332954
    :goto_5
    move v0, v6

    .line 2332955
    invoke-interface {p1, v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b(I)V

    .line 2332956
    :cond_3
    invoke-static {p0, p1}, LX/GG6;->q(LX/GG6;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    .line 2332957
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v0

    invoke-static {p1, v0}, LX/GMU;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;)Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2332958
    invoke-static {p1}, LX/GG6;->o(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    goto/16 :goto_0

    .line 2332959
    :cond_4
    invoke-interface {p1, v0, v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V

    goto :goto_2

    .line 2332960
    :cond_5
    sget-object v0, LX/GG5;->CONTINUOUS:LX/GG5;

    invoke-virtual {v0}, LX/GG5;->getDuration()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a(I)V

    goto :goto_4

    :cond_6
    move v3, v6

    .line 2332961
    :goto_6
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_9

    .line 2332962
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;

    .line 2332963
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;->a()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_7

    .line 2332964
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;

    move-result-object v2

    goto/16 :goto_1

    .line 2332965
    :cond_7
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_6

    .line 2332966
    :cond_8
    add-int/lit8 v6, v6, 0x1

    :cond_9
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v6, v2, :cond_a

    .line 2332967
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;

    .line 2332968
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_8

    .line 2332969
    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;

    move-result-object v2

    goto/16 :goto_1

    :cond_a
    move-object v2, v7

    .line 2332970
    goto/16 :goto_1

    .line 2332971
    :cond_b
    iget-object v6, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v6, v6

    .line 2332972
    if-eqz v6, :cond_c

    .line 2332973
    iget-object v6, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v6, v6

    .line 2332974
    invoke-virtual {v6}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v6

    if-eqz v6, :cond_c

    .line 2332975
    iget-object v6, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v6, v6

    .line 2332976
    invoke-virtual {v6}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->o()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel;

    move-result-object v6

    if-nez v6, :cond_d

    .line 2332977
    :cond_c
    const/4 v6, 0x2

    goto/16 :goto_3

    .line 2332978
    :cond_d
    const/4 v6, 0x1

    .line 2332979
    iget-object v7, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v7, v7

    .line 2332980
    invoke-virtual {v7}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->o()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel$DurationSuggestionsModel;->a()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    goto/16 :goto_3

    :cond_e
    long-to-int v6, v6

    goto/16 :goto_5
.end method

.method public final c(I)J
    .locals 6

    .prologue
    const-wide/32 v4, 0x15180

    .line 2332981
    sget-object v0, LX/GG5;->CONTINUOUS:LX/GG5;

    invoke-virtual {v0}, LX/GG5;->getDuration()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 2332982
    const-wide/16 v0, 0x0

    .line 2332983
    :goto_0
    return-wide v0

    .line 2332984
    :cond_0
    iget-object v0, p0, LX/GG6;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 2332985
    if-gtz p1, :cond_1

    .line 2332986
    add-long/2addr v0, v4

    goto :goto_0

    .line 2332987
    :cond_1
    int-to-long v2, p1

    mul-long/2addr v2, v4

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method public final c(LX/0Px;)Ljava/lang/String;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 2332988
    :try_start_0
    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3}, Lorg/json/JSONArray;-><init>()V

    .line 2332989
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v4

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;

    .line 2332990
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 2332991
    const-string v6, "id"

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;->l()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2332992
    const-string v6, "name"

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;->m()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2332993
    const-string v6, "target_type"

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2332994
    const-string v6, "audience_size"

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;->j()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    .line 2332995
    const-string v6, "description"

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;->k()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2332996
    const-string v6, "path"

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;->n()LX/0Px;

    move-result-object v0

    .line 2332997
    new-instance v9, Lorg/json/JSONArray;

    invoke-direct {v9}, Lorg/json/JSONArray;-><init>()V

    .line 2332998
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v10

    const/4 v7, 0x0

    move v8, v7

    :goto_1
    if-ge v8, v10, :cond_0

    invoke-virtual {v0, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 2332999
    invoke-virtual {v9, v7}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 2333000
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    goto :goto_1

    .line 2333001
    :cond_0
    move-object v0, v9

    .line 2333002
    invoke-virtual {v5, v6, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2333003
    invoke-virtual {v3, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 2333004
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2333005
    :cond_1
    invoke-virtual {v3}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "UTF-8"

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    const-string v2, "UTF-8"

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    .line 2333006
    const-string v2, "UTF-8"

    invoke-static {v0, v2}, Ljava/net/URLEncoder;->encode(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 2333007
    :goto_2
    return-object v0

    .line 2333008
    :catch_0
    move-exception v0

    .line 2333009
    iget-object v2, p0, LX/GG6;->e:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "Catch JSONException: "

    invoke-virtual {v2, v3, v4, v0}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 2333010
    goto :goto_2

    .line 2333011
    :catch_1
    move-exception v0

    .line 2333012
    iget-object v2, p0, LX/GG6;->e:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "Catch UnsupportedEncodingException: "

    invoke-virtual {v2, v3, v4, v0}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 2333013
    goto :goto_2
.end method

.method public final c(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2333014
    instance-of v0, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v0, :cond_1

    .line 2333015
    invoke-static {p1}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2333016
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2333017
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2333018
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountBasicFieldsModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2333019
    :goto_0
    return-object v0

    .line 2333020
    :cond_0
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2333021
    :cond_1
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2333022
    if-nez v0, :cond_2

    .line 2333023
    iget-object v1, p0, LX/GG6;->e:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string p1, "Insufficient data to find default ad account id; adAccounts is null"

    invoke-virtual {v1, v3, p1}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    move-object v1, v2

    .line 2333024
    :goto_1
    move-object v0, v1

    .line 2333025
    goto :goto_0

    .line 2333026
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2333027
    :cond_3
    iget-object v1, p0, LX/GG6;->e:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string p1, "Insufficient data to find default ad account id; adAccounts.getNodes() is null"

    invoke-virtual {v1, v3, p1}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    move-object v1, v2

    .line 2333028
    goto :goto_1

    .line 2333029
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_5

    .line 2333030
    iget-object v1, p0, LX/GG6;->e:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string p1, "Insufficient data to find default ad account id; first add is null"

    invoke-virtual {v1, v3, p1}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    move-object v1, v2

    .line 2333031
    goto :goto_1

    .line 2333032
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->t()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_6

    .line 2333033
    iget-object v1, p0, LX/GG6;->e:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string p1, "Insufficient data to find default ad account id; first ad account id is null"

    invoke-virtual {v1, v3, p1}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    move-object v1, v2

    .line 2333034
    goto :goto_1

    .line 2333035
    :cond_6
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->t()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public final c(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Z
    .locals 4

    .prologue
    .line 2333036
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2333037
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->F()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    invoke-virtual {p0, v0, v1}, LX/GG6;->a(J)I

    move-result v0

    .line 2333038
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i()I

    move-result v1

    .line 2333039
    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;
    .locals 12

    .prologue
    .line 2333040
    invoke-static {p1}, LX/GG6;->m(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-result-object v11

    .line 2333041
    if-eqz v11, :cond_0

    invoke-virtual {v11}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2333042
    :cond_0
    invoke-static {p0, p1}, LX/GG6;->q(LX/GG6;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2333043
    :goto_0
    return-object v0

    .line 2333044
    :cond_1
    invoke-virtual {v11}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v6

    .line 2333045
    invoke-virtual {v6}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->a()LX/1vs;

    move-result-object v0

    iget-object v7, v0, LX/1vs;->a:LX/15i;

    iget v8, v0, LX/1vs;->b:I

    .line 2333046
    new-instance v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    invoke-virtual {v6}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->j()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {v6}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->j()LX/0Px;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    :goto_1
    invoke-virtual {v6}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->n()I

    move-result v2

    invoke-virtual {v6}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->m()I

    move-result v3

    invoke-virtual {v6}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$GeoLocationsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$GeoLocationsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v6}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$GeoLocationsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$GeoLocationsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v6}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$InterestsModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel$InterestsModel;->a()LX/0Px;

    move-result-object v6

    invoke-static {v7, v8}, LX/GG6;->b(LX/15i;I)LX/0Px;

    move-result-object v7

    sget-object v8, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    invoke-virtual {v11}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v9

    sget-object v10, LX/GGG;->REGION:LX/GGG;

    invoke-virtual {v11}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->o()LX/0Px;

    move-result-object v11

    invoke-direct/range {v0 .. v11}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;-><init>(Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;IILX/0Px;LX/0Px;LX/0Px;LX/0Px;Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;Ljava/lang/String;LX/GGG;LX/0Px;)V

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;->ALL:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    goto :goto_1
.end method
