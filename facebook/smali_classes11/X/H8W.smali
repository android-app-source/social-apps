.class public LX/H8W;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/H8W;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HDT;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Bgf;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/intent_builder/IPageIdentityIntentBuilder;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/HDT;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Bgf;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/intent_builder/IPageIdentityIntentBuilder;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2433478
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2433479
    iput-object p1, p0, LX/H8W;->a:LX/0Ot;

    .line 2433480
    iput-object p2, p0, LX/H8W;->b:LX/0Ot;

    .line 2433481
    iput-object p3, p0, LX/H8W;->c:LX/0Ot;

    .line 2433482
    iput-object p4, p0, LX/H8W;->d:LX/0Ot;

    .line 2433483
    iput-object p5, p0, LX/H8W;->e:LX/0Ot;

    .line 2433484
    iput-object p6, p0, LX/H8W;->f:LX/0Ot;

    .line 2433485
    iput-object p7, p0, LX/H8W;->g:LX/0Ot;

    .line 2433486
    return-void
.end method

.method public static a(LX/0QB;)LX/H8W;
    .locals 11

    .prologue
    .line 2433465
    sget-object v0, LX/H8W;->h:LX/H8W;

    if-nez v0, :cond_1

    .line 2433466
    const-class v1, LX/H8W;

    monitor-enter v1

    .line 2433467
    :try_start_0
    sget-object v0, LX/H8W;->h:LX/H8W;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2433468
    if-eqz v2, :cond_0

    .line 2433469
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2433470
    new-instance v3, LX/H8W;

    const/16 v4, 0x2b68

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2b5d

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x12c4

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x259

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1a59

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x455

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x2c1f

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, LX/H8W;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2433471
    move-object v0, v3

    .line 2433472
    sput-object v0, LX/H8W;->h:LX/H8W;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2433473
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2433474
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2433475
    :cond_1
    sget-object v0, LX/H8W;->h:LX/H8W;

    return-object v0

    .line 2433476
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2433477
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Z)LX/CSL;
    .locals 1

    .prologue
    .line 2433460
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/H8W;->a(ZZ)LX/CSL;

    move-result-object v0

    return-object v0
.end method

.method public final a(ZZ)LX/CSL;
    .locals 2

    .prologue
    .line 2433461
    iget-object v0, p0, LX/H8W;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CSL;

    .line 2433462
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setIsPageVerified(Z)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setHasTaggableProducts(Z)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v1

    .line 2433463
    iput-object v1, v0, LX/CSL;->a:Lcom/facebook/ipc/composer/intent/ComposerPageData;

    .line 2433464
    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2433458
    iget-object v0, p0, LX/H8W;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    invoke-direct {v1, p1}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2433459
    return-void
.end method

.method public final a(LX/9X2;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2433454
    iget-object v0, p0, LX/H8W;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9XE;

    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, p1, v2, v3}, LX/9XE;->a(LX/9X2;J)V

    .line 2433455
    return-void
.end method

.method public final a(LX/HDS;)V
    .locals 1

    .prologue
    .line 2433456
    iget-object v0, p0, LX/H8W;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HDT;

    invoke-virtual {v0, p1}, LX/0b4;->a(LX/0b7;)V

    .line 2433457
    return-void
.end method
