.class public final LX/Gww;
.super LX/44w;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/44w",
        "<",
        "LX/44w",
        "<",
        "LX/Gwv;",
        "Ljava/lang/String;",
        ">;",
        "Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/Gwv;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2407563
    new-instance v0, LX/44w;

    invoke-direct {v0, p1, p2}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2407564
    return-void
.end method

.method public constructor <init>(Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;)V
    .locals 1

    .prologue
    .line 2407565
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2407566
    return-void
.end method


# virtual methods
.method public final a()LX/44w;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/44w",
            "<",
            "LX/Gwv;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2407567
    iget-object v0, p0, LX/44w;->a:Ljava/lang/Object;

    check-cast v0, LX/44w;

    return-object v0
.end method

.method public final b()Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;
    .locals 1

    .prologue
    .line 2407568
    iget-object v0, p0, LX/44w;->b:Ljava/lang/Object;

    check-cast v0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2407569
    iget-object v0, p0, LX/44w;->a:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2407570
    const-string v0, "FwCSC.Value<error=%s fcs=%s>"

    invoke-virtual {p0}, LX/Gww;->a()LX/44w;

    move-result-object v1

    invoke-virtual {p0}, LX/Gww;->b()Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
