.class public final LX/G08;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/friends/model/FriendRequest;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;Z)V
    .locals 0

    .prologue
    .line 2308493
    iput-object p1, p0, LX/G08;->b:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    iput-boolean p2, p0, LX/G08;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friends/model/FriendRequest;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2308494
    iget-boolean v0, p0, LX/G08;->a:Z

    if-eqz v0, :cond_0

    .line 2308495
    iget-object v0, p0, LX/G08;->b:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->s:LX/2dj;

    invoke-virtual {v0}, LX/2dj;->j()V

    .line 2308496
    :cond_0
    iget-object v0, p0, LX/G08;->b:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->s:LX/2dj;

    iget-object v1, p0, LX/G08;->b:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    iget-object v1, v1, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->r:Ljava/lang/String;

    const/16 v2, 0xa

    sget-object v3, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->v:Lcom/facebook/common/callercontext/CallerContext;

    .line 2308497
    invoke-static {v0}, LX/2dj;->l(LX/2dj;)Ljava/lang/String;

    move-result-object v4

    .line 2308498
    iget-object v5, v0, LX/2dj;->k:LX/2dk;

    const/4 v8, 0x1

    .line 2308499
    if-lez v2, :cond_1

    move v7, v8

    :goto_0
    invoke-static {v7}, LX/0PB;->checkArgument(Z)V

    .line 2308500
    new-instance v7, LX/851;

    invoke-direct {v7}, LX/851;-><init>()V

    move-object v7, v7

    .line 2308501
    const-string v9, "id"

    invoke-virtual {v7, v9, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v9

    const-string v10, "after_param"

    invoke-virtual {v9, v10, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v9

    const-string v10, "first_param"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v9

    const-string v10, "media_type"

    iget-object v11, v5, LX/2dk;->e:LX/0rq;

    invoke-virtual {v11}, LX/0rq;->b()LX/0wF;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v9

    const-string v10, "picture_size"

    iget v11, v5, LX/2dk;->a:I

    invoke-static {v11}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2308502
    invoke-static {v7}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v7

    sget-object v9, LX/0zS;->a:LX/0zS;

    invoke-virtual {v7, v9}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v7

    const-string v9, "REQUESTS_TAB_MEMORIAL_QUERY_TAG"

    invoke-static {v9}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v9

    .line 2308503
    iput-object v9, v7, LX/0zO;->d:Ljava/util/Set;

    .line 2308504
    move-object v7, v7

    .line 2308505
    iput-boolean v8, v7, LX/0zO;->p:Z

    .line 2308506
    move-object v7, v7

    .line 2308507
    sget-object v9, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v7, v9}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v7

    .line 2308508
    iput-object v3, v7, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2308509
    move-object v7, v7

    .line 2308510
    const-wide/16 v9, 0xe10

    invoke-virtual {v7, v9, v10}, LX/0zO;->a(J)LX/0zO;

    move-result-object v7

    .line 2308511
    iget-object v9, v7, LX/0zO;->m:LX/0gW;

    move-object v9, v9

    .line 2308512
    iput-boolean v8, v9, LX/0gW;->l:Z

    .line 2308513
    iget-object v8, v5, LX/2dk;->c:LX/2dl;

    const-string v9, "REQUESTS_TAB_MEMORIAL_QUERY_TAG"

    invoke-virtual {v8, v7, v9}, LX/2dl;->a(LX/0zO;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    .line 2308514
    new-instance v8, LX/842;

    invoke-direct {v8, v5}, LX/842;-><init>(LX/2dk;)V

    iget-object v9, v5, LX/2dk;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v7, v8, v9}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v4, v7

    .line 2308515
    new-instance v5, LX/83y;

    invoke-direct {v5, v0}, LX/83y;-><init>(LX/2dj;)V

    iget-object v6, v0, LX/2dj;->h:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, v5, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 2308516
    return-object v0

    .line 2308517
    :cond_1
    const/4 v7, 0x0

    goto/16 :goto_0
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2308518
    invoke-direct {p0}, LX/G08;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
