.class public final LX/GeN;
.super LX/1S3;
.source ""


# static fields
.field private static a:LX/GeN;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/GeL;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private c:LX/GeO;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2375574
    const/4 v0, 0x0

    sput-object v0, LX/GeN;->a:LX/GeN;

    .line 2375575
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/GeN;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2375576
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2375577
    new-instance v0, LX/GeO;

    invoke-direct {v0}, LX/GeO;-><init>()V

    iput-object v0, p0, LX/GeN;->c:LX/GeO;

    .line 2375578
    return-void
.end method

.method public static declared-synchronized q()LX/GeN;
    .locals 2

    .prologue
    .line 2375579
    const-class v1, LX/GeN;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/GeN;->a:LX/GeN;

    if-nez v0, :cond_0

    .line 2375580
    new-instance v0, LX/GeN;

    invoke-direct {v0}, LX/GeN;-><init>()V

    sput-object v0, LX/GeN;->a:LX/GeN;

    .line 2375581
    :cond_0
    sget-object v0, LX/GeN;->a:LX/GeN;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2375582
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2375583
    check-cast p2, LX/GeM;

    .line 2375584
    iget-object v0, p2, LX/GeM;->a:Lcom/facebook/java2js/JSValue;

    .line 2375585
    const-string v3, "maximumNumberOfLines"

    invoke-virtual {v0, v3}, Lcom/facebook/java2js/JSValue;->getNumberProperty(Ljava/lang/String;)D

    move-result-wide v3

    .line 2375586
    invoke-static {v3, v4}, Ljava/lang/Double;->isNaN(D)Z

    move-result v5

    if-nez v5, :cond_0

    double-to-int v3, v3

    :goto_0
    move v1, v3

    .line 2375587
    const-string v2, "text"

    invoke-virtual {v0, v2}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    invoke-static {p1, v2}, LX/5Kv;->b(LX/1De;Lcom/facebook/java2js/JSValue;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/1ne;->j(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/1ne;->i(I)LX/1ne;

    move-result-object v1

    sget-object v2, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v1, v2}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v1

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v1

    const v2, 0x7f0a043a

    invoke-virtual {v1, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v1

    const v2, 0x7f0b0050

    invoke-virtual {v1, v2}, LX/1ne;->q(I)LX/1ne;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2375588
    return-object v0

    :cond_0
    const/4 v3, -0x1

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2375589
    invoke-static {}, LX/1dS;->b()V

    .line 2375590
    const/4 v0, 0x0

    return-object v0
.end method
