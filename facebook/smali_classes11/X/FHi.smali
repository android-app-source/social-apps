.class public final enum LX/FHi;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FHi;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FHi;

.field public static final enum FILE_KEY_COMPUTED:LX/FHi;

.field public static final enum START_UPLOAD:LX/FHi;

.field public static final enum TIMED_OUT_POST_PROCESS:LX/FHi;

.field public static final enum TIMED_OUT_UPLOAD:LX/FHi;

.field public static final enum UPLOAD_STARTED:LX/FHi;

.field public static final enum USER_CANCELLED:LX/FHi;

.field public static final enum VIDEO_POST_PROCESS_COMPLETED:LX/FHi;

.field public static final enum VIDEO_UPLOAD_COMPLETED:LX/FHi;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2220877
    new-instance v0, LX/FHi;

    const-string v1, "START_UPLOAD"

    invoke-direct {v0, v1, v3}, LX/FHi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHi;->START_UPLOAD:LX/FHi;

    .line 2220878
    new-instance v0, LX/FHi;

    const-string v1, "FILE_KEY_COMPUTED"

    invoke-direct {v0, v1, v4}, LX/FHi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHi;->FILE_KEY_COMPUTED:LX/FHi;

    .line 2220879
    new-instance v0, LX/FHi;

    const-string v1, "UPLOAD_STARTED"

    invoke-direct {v0, v1, v5}, LX/FHi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHi;->UPLOAD_STARTED:LX/FHi;

    .line 2220880
    new-instance v0, LX/FHi;

    const-string v1, "TIMED_OUT_UPLOAD"

    invoke-direct {v0, v1, v6}, LX/FHi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHi;->TIMED_OUT_UPLOAD:LX/FHi;

    .line 2220881
    new-instance v0, LX/FHi;

    const-string v1, "USER_CANCELLED"

    invoke-direct {v0, v1, v7}, LX/FHi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHi;->USER_CANCELLED:LX/FHi;

    .line 2220882
    new-instance v0, LX/FHi;

    const-string v1, "VIDEO_UPLOAD_COMPLETED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/FHi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHi;->VIDEO_UPLOAD_COMPLETED:LX/FHi;

    .line 2220883
    new-instance v0, LX/FHi;

    const-string v1, "TIMED_OUT_POST_PROCESS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/FHi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHi;->TIMED_OUT_POST_PROCESS:LX/FHi;

    .line 2220884
    new-instance v0, LX/FHi;

    const-string v1, "VIDEO_POST_PROCESS_COMPLETED"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/FHi;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHi;->VIDEO_POST_PROCESS_COMPLETED:LX/FHi;

    .line 2220885
    const/16 v0, 0x8

    new-array v0, v0, [LX/FHi;

    sget-object v1, LX/FHi;->START_UPLOAD:LX/FHi;

    aput-object v1, v0, v3

    sget-object v1, LX/FHi;->FILE_KEY_COMPUTED:LX/FHi;

    aput-object v1, v0, v4

    sget-object v1, LX/FHi;->UPLOAD_STARTED:LX/FHi;

    aput-object v1, v0, v5

    sget-object v1, LX/FHi;->TIMED_OUT_UPLOAD:LX/FHi;

    aput-object v1, v0, v6

    sget-object v1, LX/FHi;->USER_CANCELLED:LX/FHi;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/FHi;->VIDEO_UPLOAD_COMPLETED:LX/FHi;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/FHi;->TIMED_OUT_POST_PROCESS:LX/FHi;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/FHi;->VIDEO_POST_PROCESS_COMPLETED:LX/FHi;

    aput-object v2, v0, v1

    sput-object v0, LX/FHi;->$VALUES:[LX/FHi;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2220886
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FHi;
    .locals 1

    .prologue
    .line 2220887
    const-class v0, LX/FHi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FHi;

    return-object v0
.end method

.method public static values()[LX/FHi;
    .locals 1

    .prologue
    .line 2220888
    sget-object v0, LX/FHi;->$VALUES:[LX/FHi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FHi;

    return-object v0
.end method
