.class public final LX/F3C;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;)V
    .locals 0

    .prologue
    .line 2193820
    iput-object p1, p0, LX/F3C;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2193821
    iget-object v0, p0, LX/F3C;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->f:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    if-nez v0, :cond_0

    .line 2193822
    const/4 v0, 0x0

    .line 2193823
    :goto_0
    return-object v0

    .line 2193824
    :cond_0
    new-instance v0, LX/F3l;

    invoke-direct {v0}, LX/F3l;-><init>()V

    move-object v0, v0

    .line 2193825
    const-string v1, "group_id"

    iget-object v2, p0, LX/F3C;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    iget-object v2, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->f:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->q()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2193826
    const-string v1, "purpose_pog_size"

    iget-object v2, p0, LX/F3C;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    iget-object v2, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->a:Landroid/content/res/Resources;

    const v3, 0x7f0b20f8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2193827
    const-string v1, "purpose_default_cover_photo_size"

    iget-object v2, p0, LX/F3C;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    iget-object v2, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->a:Landroid/content/res/Resources;

    const v3, 0x7f0b20f8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2193828
    iget-object v1, p0, LX/F3C;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    iget-object v1, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->b:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->d:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const/4 v2, 0x1

    .line 2193829
    iput-boolean v2, v0, LX/0zO;->p:Z

    .line 2193830
    move-object v0, v0

    .line 2193831
    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    goto :goto_0
.end method
