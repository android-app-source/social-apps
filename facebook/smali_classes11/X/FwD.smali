.class public LX/FwD;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/FwD;


# instance fields
.field public final a:LX/0ad;

.field private final b:LX/Fx5;

.field private final c:LX/Fwh;

.field public final d:LX/8p8;


# direct methods
.method public constructor <init>(LX/0ad;LX/Fx5;LX/Fwh;LX/8p8;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2302860
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2302861
    iput-object p1, p0, LX/FwD;->a:LX/0ad;

    .line 2302862
    iput-object p2, p0, LX/FwD;->b:LX/Fx5;

    .line 2302863
    iput-object p3, p0, LX/FwD;->c:LX/Fwh;

    .line 2302864
    iput-object p4, p0, LX/FwD;->d:LX/8p8;

    .line 2302865
    return-void
.end method

.method public static a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;)I
    .locals 1

    .prologue
    .line 2302912
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->cj_()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$IntroCardContextListItemsConnectionFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2302913
    :cond_0
    const/4 v0, 0x0

    .line 2302914
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->cj_()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$IntroCardContextListItemsConnectionFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$IntroCardContextListItemsConnectionFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/FwD;
    .locals 7

    .prologue
    .line 2302899
    sget-object v0, LX/FwD;->e:LX/FwD;

    if-nez v0, :cond_1

    .line 2302900
    const-class v1, LX/FwD;

    monitor-enter v1

    .line 2302901
    :try_start_0
    sget-object v0, LX/FwD;->e:LX/FwD;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2302902
    if-eqz v2, :cond_0

    .line 2302903
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2302904
    new-instance p0, LX/FwD;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static {v0}, LX/Fx5;->a(LX/0QB;)LX/Fx5;

    move-result-object v4

    check-cast v4, LX/Fx5;

    invoke-static {v0}, LX/Fwh;->a(LX/0QB;)LX/Fwh;

    move-result-object v5

    check-cast v5, LX/Fwh;

    invoke-static {v0}, LX/8p8;->b(LX/0QB;)LX/8p8;

    move-result-object v6

    check-cast v6, LX/8p8;

    invoke-direct {p0, v3, v4, v5, v6}, LX/FwD;-><init>(LX/0ad;LX/Fx5;LX/Fwh;LX/8p8;)V

    .line 2302905
    move-object v0, p0

    .line 2302906
    sput-object v0, LX/FwD;->e:LX/FwD;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2302907
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2302908
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2302909
    :cond_1
    sget-object v0, LX/FwD;->e:LX/FwD;

    return-object v0

    .line 2302910
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2302911
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/5SB;LX/BQ1;)Z
    .locals 1

    .prologue
    .line 2302898
    invoke-virtual {p0}, LX/5SB;->i()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, LX/BQ1;->ad()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, LX/BQ1;->ag()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/BQ1;)Z
    .locals 1

    .prologue
    .line 2302915
    iget-object v0, p0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v0, v0

    .line 2302916
    if-eqz v0, :cond_0

    .line 2302917
    iget-object v0, p0, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v0, v0

    .line 2302918
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/BQ1;LX/5SB;LX/Fve;)Ljava/lang/Integer;
    .locals 5
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getViewTypeForFavoritePhotos"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v1, 0x3

    .line 2302881
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 2302882
    invoke-virtual {p1}, LX/BPy;->j()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2302883
    iget-object v3, p1, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v3, v3

    .line 2302884
    if-eqz v3, :cond_0

    iget-object v3, p0, LX/FwD;->a:LX/0ad;

    sget-short v4, LX/0wf;->W:S

    invoke-interface {v3, v4, v0}, LX/0ad;->a(SZ)Z

    move-result v3

    if-nez v3, :cond_6

    :cond_0
    move v0, v2

    .line 2302885
    :cond_1
    :goto_0
    move v0, v0

    .line 2302886
    if-eqz v0, :cond_2

    .line 2302887
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2302888
    :goto_1
    return-object v0

    .line 2302889
    :cond_2
    invoke-virtual {p1}, LX/BQ1;->ad()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2302890
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 2302891
    :cond_3
    invoke-virtual {p2}, LX/5SB;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2302892
    iget-object v0, p0, LX/FwD;->b:LX/Fx5;

    invoke-virtual {v0}, LX/Fx5;->h()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, LX/BQ1;->ae()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2302893
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 2302894
    :cond_4
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 2302895
    :cond_5
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 2302896
    :cond_6
    sget-object v3, LX/Fve;->EXPANDED:LX/Fve;

    if-eq p3, v3, :cond_1

    .line 2302897
    invoke-virtual {p1}, LX/BQ1;->aa()Z

    move-result v3

    if-nez v3, :cond_7

    invoke-virtual {p1}, LX/BQ1;->ac()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_7
    move v0, v2

    goto :goto_0
.end method

.method public final a(LX/BQ1;LX/5SB;Z)Ljava/lang/Integer;
    .locals 4
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getViewTypeForBio"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 2302867
    invoke-virtual {p1}, LX/BPy;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2302868
    iget-object v0, p1, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v0, v0

    .line 2302869
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FwD;->a:LX/0ad;

    sget-short v1, LX/0wf;->W:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2302870
    :cond_0
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 2302871
    :goto_0
    return-object v0

    .line 2302872
    :cond_1
    invoke-virtual {p1}, LX/BQ1;->aa()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2302873
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 2302874
    :cond_2
    if-eqz p3, :cond_4

    invoke-virtual {p2}, LX/5SB;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2302875
    iget-object v0, p0, LX/FwD;->c:LX/Fwh;

    invoke-virtual {v0}, LX/Fwh;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2302876
    iget-object v0, p1, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    if-eqz v0, :cond_5

    iget-object v0, p1, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->o()LX/174;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p1, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->o()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2302877
    if-eqz v0, :cond_3

    .line 2302878
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 2302879
    :cond_3
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 2302880
    :cond_4
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(LX/5SB;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2302866
    invoke-virtual {p1}, LX/5SB;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/FwD;->a:LX/0ad;

    sget-short v2, LX/0wf;->W:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
