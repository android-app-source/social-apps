.class public LX/G6y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/63p;


# instance fields
.field private final a:LX/11w;

.field private b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/11w;LX/0Or;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/iorg/common/zero/annotations/IsZeroRatingCampaignEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/11w;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2320933
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2320934
    iput-object p1, p0, LX/G6y;->a:LX/11w;

    .line 2320935
    iput-object p2, p0, LX/G6y;->b:LX/0Or;

    .line 2320936
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)LX/03R;
    .locals 2

    .prologue
    .line 2320937
    iget-object v0, p0, LX/G6y;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2320938
    sget-object v0, LX/03R;->UNSET:LX/03R;

    .line 2320939
    :goto_0
    return-object v0

    .line 2320940
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 2320941
    if-nez v0, :cond_1

    .line 2320942
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_0

    .line 2320943
    :cond_1
    iget-object v1, p0, LX/G6y;->a:LX/11w;

    invoke-virtual {v1}, LX/11w;->c()Lcom/facebook/zero/sdk/request/ZeroIndicatorData;

    move-result-object v1

    .line 2320944
    if-nez v1, :cond_2

    .line 2320945
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_0

    .line 2320946
    :cond_2
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/facebook/zero/sdk/request/ZeroIndicatorData;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2320947
    sget-object v0, LX/03R;->YES:LX/03R;

    goto :goto_0

    .line 2320948
    :cond_3
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_0
.end method
