.class public final LX/F5z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:Ljava/util/ArrayList;

.field public final synthetic f:Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel;

.field public final synthetic g:LX/F60;


# direct methods
.method public constructor <init>(LX/F60;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel;)V
    .locals 0

    .prologue
    .line 2200002
    iput-object p1, p0, LX/F5z;->g:LX/F60;

    iput-object p2, p0, LX/F5z;->a:Ljava/lang/String;

    iput-object p3, p0, LX/F5z;->b:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    iput-object p4, p0, LX/F5z;->c:Ljava/lang/String;

    iput-object p5, p0, LX/F5z;->d:Ljava/lang/String;

    iput-object p6, p0, LX/F5z;->e:Ljava/util/ArrayList;

    iput-object p7, p0, LX/F5z;->f:Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, -0x33806b44    # -6.6999024E7f

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2200003
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2200004
    const-string v2, "group_name"

    iget-object v3, p0, LX/F5z;->g:LX/F60;

    iget-object v3, v3, LX/F60;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbTextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200005
    const-string v2, "group_members"

    iget-object v3, p0, LX/F5z;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200006
    const-string v2, "group_visibility"

    iget-object v4, p0, LX/F5z;->b:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 2200007
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->OPEN:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v5, v4}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2200008
    const-string v5, "Open"

    .line 2200009
    :goto_0
    move-object v5, v5

    .line 2200010
    move-object v3, v5

    .line 2200011
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200012
    const-string v2, "ref"

    .line 2200013
    const-string v4, "GROUPS_YOU_SHOULD_CREATE_TAB"

    move-object v3, v4

    .line 2200014
    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200015
    const-string v2, "suggestion_category"

    iget-object v3, p0, LX/F5z;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200016
    const-string v2, "suggestion_identifier"

    iget-object v3, p0, LX/F5z;->d:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200017
    iget-object v2, p0, LX/F5z;->g:LX/F60;

    iget-object v2, v2, LX/F60;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2200018
    const-string v2, "group_members_tokens"

    iget-object v3, p0, LX/F5z;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2200019
    iget-object v2, p0, LX/F5z;->f:Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel;

    if-eqz v2, :cond_0

    .line 2200020
    iget-object v2, p0, LX/F5z;->f:Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel;

    invoke-virtual {v2}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel;->l()Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel;

    move-result-object v2

    .line 2200021
    const-string v3, "group_purpose_extra_key"

    new-instance v4, Lcom/facebook/work/groups/create/api/GroupPurpose;

    invoke-virtual {v2}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel$GroupPurposeModel;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v7, p0, LX/F5z;->b:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-direct {v4, v5, v6, v2, v7}, Lcom/facebook/work/groups/create/api/GroupPurpose;-><init>(Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)V

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2200022
    const-string v2, "group_extra_settings"

    new-instance v3, Lcom/facebook/work/groups/create/api/GroupExtraSettings;

    iget-object v4, p0, LX/F5z;->f:Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel;

    invoke-virtual {v4}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel;->a()Z

    move-result v4

    iget-object v5, p0, LX/F5z;->f:Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel;

    invoke-virtual {v5}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel;->j()Z

    move-result v5

    iget-object v6, p0, LX/F5z;->f:Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel;

    invoke-virtual {v6}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel$ExtraSettingModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/facebook/work/groups/create/api/GroupExtraSettings;-><init>(ZZLjava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2200023
    :cond_0
    iget-object v2, p0, LX/F5z;->g:LX/F60;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2200024
    iget-object v4, v2, LX/F60;->a:LX/17W;

    sget-object v5, LX/0ax;->N:Ljava/lang/String;

    invoke-virtual {v4, v3, v5, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2200025
    iget-object v1, p0, LX/F5z;->g:LX/F60;

    iget-object v1, v1, LX/F60;->b:LX/1g8;

    iget-object v2, p0, LX/F5z;->c:Ljava/lang/String;

    iget-object v3, p0, LX/F5z;->d:Ljava/lang/String;

    .line 2200026
    const-string v5, "GROUPS_YOU_SHOULD_CREATE_TAB"

    move-object v4, v5

    .line 2200027
    new-instance v5, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v6, "gysc_click"

    invoke-direct {v5, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v6, "suggestion_type"

    invoke-virtual {v5, v6, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "suggestion_id"

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "ref"

    invoke-virtual {v5, v6, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "group_creation"

    .line 2200028
    iput-object v6, v5, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2200029
    move-object v5, v5

    .line 2200030
    iget-object v6, v1, LX/1g8;->a:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2200031
    invoke-static {v1, v5}, LX/1g8;->a(LX/1g8;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2200032
    const v1, 0x6add6325

    invoke-static {v8, v8, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2200033
    :cond_1
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->CLOSED:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v5, v4}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2200034
    const-string v5, "Closed"

    goto/16 :goto_0

    .line 2200035
    :cond_2
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->SECRET:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v5, v4}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2200036
    const-string v5, "Secret"

    goto/16 :goto_0

    .line 2200037
    :cond_3
    const/4 v5, 0x0

    goto/16 :goto_0
.end method
