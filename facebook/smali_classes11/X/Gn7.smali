.class public LX/Gn7;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Gn7;


# instance fields
.field public final a:LX/Gny;


# direct methods
.method public constructor <init>(LX/Gny;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2393425
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2393426
    sget-object v0, LX/0ax;->hj:Ljava/lang/String;

    const-string v1, "catalog_id"

    const-string v2, "catalog_view"

    const-string v3, "product_id"

    const-string v4, "product_view"

    invoke-static {v0, v1, v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/Gn6;

    invoke-direct {v1, p0}, LX/Gn6;-><init>(LX/Gn7;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2393427
    sget-object v0, LX/0ax;->hk:Ljava/lang/String;

    const-string v1, "id"

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/Gn6;

    invoke-direct {v1, p0}, LX/Gn6;-><init>(LX/Gn7;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2393428
    iput-object p1, p0, LX/Gn7;->a:LX/Gny;

    .line 2393429
    return-void
.end method

.method public static a(LX/0QB;)LX/Gn7;
    .locals 4

    .prologue
    .line 2393430
    sget-object v0, LX/Gn7;->b:LX/Gn7;

    if-nez v0, :cond_1

    .line 2393431
    const-class v1, LX/Gn7;

    monitor-enter v1

    .line 2393432
    :try_start_0
    sget-object v0, LX/Gn7;->b:LX/Gn7;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2393433
    if-eqz v2, :cond_0

    .line 2393434
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2393435
    new-instance p0, LX/Gn7;

    invoke-static {v0}, LX/Gny;->a(LX/0QB;)LX/Gny;

    move-result-object v3

    check-cast v3, LX/Gny;

    invoke-direct {p0, v3}, LX/Gn7;-><init>(LX/Gny;)V

    .line 2393436
    move-object v0, p0

    .line 2393437
    sput-object v0, LX/Gn7;->b:LX/Gn7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2393438
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2393439
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2393440
    :cond_1
    sget-object v0, LX/Gn7;->b:LX/Gn7;

    return-object v0

    .line 2393441
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2393442
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
