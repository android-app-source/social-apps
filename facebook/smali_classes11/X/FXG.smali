.class public final LX/FXG;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 2254540
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_a

    .line 2254541
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2254542
    :goto_0
    return v1

    .line 2254543
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2254544
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_9

    .line 2254545
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2254546
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2254547
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 2254548
    const-string v8, "__type__"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    const-string v8, "__typename"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2254549
    :cond_2
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v6

    goto :goto_1

    .line 2254550
    :cond_3
    const-string v8, "item_attribution"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2254551
    invoke-static {p0, p1}, LX/FXD;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2254552
    :cond_4
    const-string v8, "message_object"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 2254553
    const/4 v7, 0x0

    .line 2254554
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v8, :cond_10

    .line 2254555
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2254556
    :goto_2
    move v4, v7

    .line 2254557
    goto :goto_1

    .line 2254558
    :cond_5
    const-string v8, "savable_actors"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_7

    .line 2254559
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2254560
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, v8, :cond_6

    .line 2254561
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, v8, :cond_6

    .line 2254562
    invoke-static {p0, p1}, LX/FXE;->b(LX/15w;LX/186;)I

    move-result v7

    .line 2254563
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2254564
    :cond_6
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 2254565
    goto/16 :goto_1

    .line 2254566
    :cond_7
    const-string v8, "savable_container_category"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 2254567
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSaveContainerCategoryEnum;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    goto/16 :goto_1

    .line 2254568
    :cond_8
    const-string v8, "story"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2254569
    invoke-static {p0, p1}, LX/FXF;->a(LX/15w;LX/186;)I

    move-result v0

    goto/16 :goto_1

    .line 2254570
    :cond_9
    const/4 v7, 0x6

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2254571
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 2254572
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 2254573
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2254574
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2254575
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2254576
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2254577
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto/16 :goto_1

    .line 2254578
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2254579
    :cond_c
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_f

    .line 2254580
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2254581
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2254582
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_c

    if-eqz v9, :cond_c

    .line 2254583
    const-string v10, "__type__"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_d

    const-string v10, "__typename"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 2254584
    :cond_d
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v8

    goto :goto_4

    .line 2254585
    :cond_e
    const-string v10, "message_thread_key"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 2254586
    const/4 v9, 0x0

    .line 2254587
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v10, :cond_15

    .line 2254588
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2254589
    :goto_5
    move v4, v9

    .line 2254590
    goto :goto_4

    .line 2254591
    :cond_f
    const/4 v9, 0x2

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2254592
    invoke-virtual {p1, v7, v8}, LX/186;->b(II)V

    .line 2254593
    const/4 v7, 0x1

    invoke-virtual {p1, v7, v4}, LX/186;->b(II)V

    .line 2254594
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto/16 :goto_2

    :cond_10
    move v4, v7

    move v8, v7

    goto :goto_4

    .line 2254595
    :cond_11
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2254596
    :cond_12
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_14

    .line 2254597
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2254598
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2254599
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_12

    if-eqz v11, :cond_12

    .line 2254600
    const-string v12, "other_user_id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_13

    .line 2254601
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_6

    .line 2254602
    :cond_13
    const-string v12, "thread_fbid"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_11

    .line 2254603
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_6

    .line 2254604
    :cond_14
    const/4 v11, 0x2

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2254605
    invoke-virtual {p1, v9, v10}, LX/186;->b(II)V

    .line 2254606
    const/4 v9, 0x1

    invoke-virtual {p1, v9, v4}, LX/186;->b(II)V

    .line 2254607
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto :goto_5

    :cond_15
    move v4, v9

    move v10, v9

    goto :goto_6
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 2254608
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2254609
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2254610
    if-eqz v0, :cond_0

    .line 2254611
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254612
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2254613
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2254614
    if-eqz v0, :cond_1

    .line 2254615
    const-string v1, "item_attribution"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254616
    invoke-static {p0, v0, p2}, LX/FXD;->a(LX/15i;ILX/0nX;)V

    .line 2254617
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2254618
    if-eqz v0, :cond_6

    .line 2254619
    const-string v1, "message_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254620
    const/4 v3, 0x0

    .line 2254621
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2254622
    invoke-virtual {p0, v0, v3}, LX/15i;->g(II)I

    move-result v1

    .line 2254623
    if-eqz v1, :cond_2

    .line 2254624
    const-string v1, "__type__"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254625
    invoke-static {p0, v0, v3, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2254626
    :cond_2
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2254627
    if-eqz v1, :cond_5

    .line 2254628
    const-string v3, "message_thread_key"

    invoke-virtual {p2, v3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254629
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2254630
    const/4 v3, 0x0

    invoke-virtual {p0, v1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2254631
    if-eqz v3, :cond_3

    .line 2254632
    const-string v0, "other_user_id"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254633
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2254634
    :cond_3
    const/4 v3, 0x1

    invoke-virtual {p0, v1, v3}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v3

    .line 2254635
    if-eqz v3, :cond_4

    .line 2254636
    const-string v0, "thread_fbid"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254637
    invoke-virtual {p2, v3}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2254638
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2254639
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2254640
    :cond_6
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2254641
    if-eqz v0, :cond_8

    .line 2254642
    const-string v1, "savable_actors"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254643
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2254644
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v3

    if-ge v1, v3, :cond_7

    .line 2254645
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v3

    invoke-static {p0, v3, p2, p3}, LX/FXE;->b(LX/15i;ILX/0nX;LX/0my;)V

    .line 2254646
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2254647
    :cond_7
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2254648
    :cond_8
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2254649
    if-eqz v0, :cond_9

    .line 2254650
    const-string v0, "savable_container_category"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254651
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2254652
    :cond_9
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2254653
    if-eqz v0, :cond_a

    .line 2254654
    const-string v1, "story"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254655
    invoke-static {p0, v0, p2}, LX/FXF;->a(LX/15i;ILX/0nX;)V

    .line 2254656
    :cond_a
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2254657
    return-void
.end method
