.class public final LX/GkX;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupUnarchiveMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GkY;


# direct methods
.method public constructor <init>(LX/GkY;)V
    .locals 0

    .prologue
    .line 2389127
    iput-object p1, p0, LX/GkX;->a:LX/GkY;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2389124
    iget-object v0, p0, LX/GkX;->a:LX/GkY;

    iget-object v0, v0, LX/GkY;->b:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    const v1, 0x7f083049

    invoke-static {v0, v1}, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->a$redex0(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;I)V

    .line 2389125
    iget-object v0, p0, LX/GkX;->a:LX/GkY;

    iget-object v0, v0, LX/GkY;->b:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->D:Ljava/lang/String;

    const-string v2, "Group unarchive action failed."

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2389126
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2389121
    iget-object v0, p0, LX/GkX;->a:LX/GkY;

    iget-object v0, v0, LX/GkY;->b:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    const v1, 0x7f083048

    invoke-static {v0, v1}, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->a$redex0(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;I)V

    .line 2389122
    iget-object v0, p0, LX/GkX;->a:LX/GkY;

    iget-object v0, v0, LX/GkY;->b:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->o:LX/Gkk;

    check-cast v0, LX/Gl6;

    invoke-virtual {v0}, LX/Gl6;->b()V

    .line 2389123
    return-void
.end method
