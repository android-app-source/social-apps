.class public LX/H5o;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Ljava/util/Map",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Integer;",
        ">;>;"
    }
.end annotation


# instance fields
.field public a:LX/2LL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/10M;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0dC;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2425330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2425331
    return-void
.end method

.method public static b(LX/0QB;)LX/H5o;
    .locals 5

    .prologue
    .line 2425277
    new-instance v4, LX/H5o;

    invoke-direct {v4}, LX/H5o;-><init>()V

    .line 2425278
    invoke-static {p0}, LX/2LL;->a(LX/0QB;)LX/2LL;

    move-result-object v0

    check-cast v0, LX/2LL;

    invoke-static {p0}, LX/10M;->b(LX/0QB;)LX/10M;

    move-result-object v1

    check-cast v1, LX/10M;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v2

    check-cast v2, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v3

    check-cast v3, LX/0dC;

    .line 2425279
    iput-object v0, v4, LX/H5o;->a:LX/2LL;

    iput-object v1, v4, LX/H5o;->b:LX/10M;

    iput-object v2, v4, LX/H5o;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v3, v4, LX/H5o;->d:LX/0dC;

    .line 2425280
    return-object v4
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2425305
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "logged_out_badge"

    .line 2425306
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 2425307
    move-object v0, v0

    .line 2425308
    const-string v1, "GET"

    .line 2425309
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 2425310
    move-object v0, v0

    .line 2425311
    const-string v1, "dbl/badgecounts"

    .line 2425312
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 2425313
    move-object v0, v0

    .line 2425314
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2425315
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "summary"

    const-string v4, "true"

    invoke-direct {v1, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2425316
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "machine_id"

    iget-object v4, p0, LX/H5o;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v5, LX/26p;->f:LX/0Tn;

    const/4 p1, 0x0

    invoke-interface {v4, v5, p1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2425317
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "device_id"

    iget-object v4, p0, LX/H5o;->d:LX/0dC;

    invoke-virtual {v4}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2425318
    iget-object v1, p0, LX/H5o;->b:LX/10M;

    invoke-virtual {v1}, LX/10M;->a()Ljava/util/List;

    move-result-object v1

    .line 2425319
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2425320
    if-eqz v1, :cond_0

    iget-object v4, v1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mAlternativeAccessToken:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 2425321
    const-string v4, "uid"

    iget-object v5, v1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    const-string p1, "session_token"

    iget-object v1, v1, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mAlternativeAccessToken:Ljava/lang/String;

    invoke-static {v4, v5, p1, v1}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v1

    .line 2425322
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "accounts[]"

    invoke-static {v1}, LX/16N;->a(Ljava/lang/Object;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2425323
    :cond_1
    move-object v1, v2

    .line 2425324
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2425325
    move-object v0, v0

    .line 2425326
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2425327
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2425328
    move-object v0, v0

    .line 2425329
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2425281
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2425282
    const/4 v1, 0x0

    .line 2425283
    if-eqz v0, :cond_0

    const-string v2, "summary"

    invoke-virtual {v0, v2}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2425284
    const-string v2, "summary"

    invoke-virtual {v0, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 2425285
    const-string p1, "total_count"

    invoke-virtual {v2, p1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    const-string v1, "total_count"

    invoke-virtual {v2, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-virtual {v1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 2425286
    :cond_0
    move v1, v1

    .line 2425287
    iget-object v2, p0, LX/H5o;->a:LX/2LL;

    invoke-virtual {v2, v1}, LX/2LL;->a(I)V

    .line 2425288
    if-eqz v0, :cond_1

    const-string v1, "data"

    invoke-virtual {v0, v1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2425289
    :cond_1
    const/4 v1, 0x0

    .line 2425290
    :goto_0
    move-object v0, v1

    .line 2425291
    if-nez v0, :cond_5

    .line 2425292
    :goto_1
    return-object v0

    .line 2425293
    :cond_2
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2425294
    const-string v1, "data"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 2425295
    const/4 v1, 0x0

    :goto_2
    invoke-virtual {v3}, LX/0lF;->e()I

    move-result v4

    if-ge v1, v4, :cond_4

    .line 2425296
    invoke-virtual {v3, v1}, LX/0lF;->a(I)LX/0lF;

    move-result-object v4

    .line 2425297
    const-string p1, "account_id"

    invoke-virtual {v4, p1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    const-string p1, "account_id"

    invoke-virtual {v4, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p1

    invoke-virtual {p1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p1

    if-nez p1, :cond_3

    const-string p1, "badge_count"

    invoke-virtual {v4, p1}, LX/0lF;->c(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_3

    .line 2425298
    const-string p1, "account_id"

    invoke-virtual {v4, p1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object p1

    invoke-virtual {p1}, LX/0lF;->B()Ljava/lang/String;

    move-result-object p1

    const-string p2, "badge_count"

    invoke-virtual {v4, p2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v4

    invoke-virtual {v4}, LX/0lF;->C()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2425299
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    move-object v1, v2

    .line 2425300
    goto :goto_0

    .line 2425301
    :cond_5
    iget-object v1, p0, LX/H5o;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0hM;->O:LX/0Tn;

    invoke-interface {v1, v2}, LX/0hN;->b(LX/0Tn;)LX/0hN;

    move-result-object v3

    .line 2425302
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 2425303
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, LX/0hM;->b(Ljava/lang/String;)LX/0Tn;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v3, v2, v1}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    goto :goto_3

    .line 2425304
    :cond_6
    invoke-interface {v3}, LX/0hN;->commit()V

    goto/16 :goto_1
.end method
