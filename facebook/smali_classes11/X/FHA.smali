.class public LX/FHA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/FH9;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# static fields
.field private static final g:Ljava/util/concurrent/atomic/AtomicLong;


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/5zm;

.field private final c:LX/FGl;

.field public final d:LX/03V;

.field private final e:LX/2Mp;

.field private final f:LX/2ML;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2219895
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, LX/FHA;->g:Ljava/util/concurrent/atomic/AtomicLong;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/5zm;LX/FGl;LX/03V;LX/2Mp;LX/2ML;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2219896
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2219897
    iput-object p1, p0, LX/FHA;->a:LX/0SG;

    .line 2219898
    iput-object p2, p0, LX/FHA;->b:LX/5zm;

    .line 2219899
    iput-object p3, p0, LX/FHA;->c:LX/FGl;

    .line 2219900
    iput-object p4, p0, LX/FHA;->d:LX/03V;

    .line 2219901
    iput-object p5, p0, LX/FHA;->e:LX/2Mp;

    .line 2219902
    iput-object p6, p0, LX/FHA;->f:LX/2ML;

    .line 2219903
    return-void
.end method

.method public static b(LX/0QB;)LX/FHA;
    .locals 7

    .prologue
    .line 2219904
    new-instance v0, LX/FHA;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, LX/5zm;->a(LX/0QB;)LX/5zm;

    move-result-object v2

    check-cast v2, LX/5zm;

    invoke-static {p0}, LX/FGl;->b(LX/0QB;)LX/FGl;

    move-result-object v3

    check-cast v3, LX/FGl;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-static {p0}, LX/2Mp;->a(LX/0QB;)LX/2Mp;

    move-result-object v5

    check-cast v5, LX/2Mp;

    invoke-static {p0}, LX/2ML;->a(LX/0QB;)LX/2ML;

    move-result-object v6

    check-cast v6, LX/2ML;

    invoke-direct/range {v0 .. v6}, LX/FHA;-><init>(LX/0SG;LX/5zm;LX/FGl;LX/03V;LX/2Mp;LX/2ML;)V

    .line 2219905
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 9

    .prologue
    .line 2219906
    check-cast p1, LX/FH9;

    .line 2219907
    iget-object v1, p1, LX/FH9;->c:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2219908
    iget-object v0, p0, LX/FHA;->b:LX/5zm;

    invoke-virtual {v0, v1}, LX/5zm;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zl;

    move-result-object v0

    .line 2219909
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 2219910
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2219911
    iget-boolean v4, p1, LX/FH9;->a:Z

    if-eqz v4, :cond_3

    .line 2219912
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "chunked_session_id"

    iget-object v5, p1, LX/FH9;->b:Ljava/lang/String;

    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2219913
    :goto_0
    iget-object v0, v1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v4, LX/2MK;->PHOTO:LX/2MK;

    if-ne v0, v4, :cond_6

    .line 2219914
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "image_type"

    .line 2219915
    iget-object v5, v1, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v5}, LX/5zj;->isQuickCamSource()Z

    move-result v5

    if-eqz v5, :cond_c

    sget-object v5, LX/5dT;->QUICKCAM:LX/5dT;

    iget-object v5, v5, LX/5dT;->apiStringValue:Ljava/lang/String;

    :goto_1
    move-object v5, v5

    .line 2219916
    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2219917
    :cond_0
    :goto_2
    iget-object v0, p0, LX/FHA;->e:LX/2Mp;

    invoke-virtual {v0, v1}, LX/2Mp;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/FHA;->f:LX/2ML;

    invoke-virtual {v0, v1}, LX/2ML;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2219918
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "media_hash"

    iget-object v5, p0, LX/FHA;->f:LX/2ML;

    invoke-virtual {v5, v1}, LX/2ML;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2219919
    :cond_1
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "extra_logging_data"

    iget-object v5, p1, LX/FH9;->e:Ljava/util/Map;

    invoke-static {v5}, LX/16N;->a(Ljava/util/Map;)LX/0m9;

    move-result-object v5

    invoke-virtual {v5}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2219920
    iget-object v0, v1, Lcom/facebook/ui/media/attachments/MediaResource;->A:Lcom/facebook/messaging/model/attribution/ContentAppAttribution;

    .line 2219921
    if-eqz v0, :cond_2

    .line 2219922
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "attribution_app_id"

    iget-object v6, v0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->b:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2219923
    iget-boolean v4, v1, Lcom/facebook/ui/media/attachments/MediaResource;->C:Z

    if-eqz v4, :cond_9

    .line 2219924
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "skip_android_hash_check"

    const-string v6, "1"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2219925
    :goto_3
    iget-object v4, v0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->f:Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 2219926
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "attribution_app_metadata"

    iget-object v0, v0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->f:Ljava/lang/String;

    invoke-direct {v4, v5, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2219927
    :cond_2
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "render_as_sticker"

    iget-boolean v0, v1, Lcom/facebook/ui/media/attachments/MediaResource;->E:Z

    if-eqz v0, :cond_a

    const-string v0, "1"

    :goto_4
    invoke-direct {v4, v5, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2219928
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "is_voicemail"

    iget-boolean v0, v1, Lcom/facebook/ui/media/attachments/MediaResource;->F:Z

    if-eqz v0, :cond_b

    const-string v0, "1"

    :goto_5
    invoke-direct {v4, v5, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2219929
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "call_id"

    iget-object v5, v1, Lcom/facebook/ui/media/attachments/MediaResource;->G:Ljava/lang/String;

    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2219930
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "media-"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p1, LX/FH9;->d:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2219931
    iput-object v4, v0, LX/14O;->b:Ljava/lang/String;

    .line 2219932
    move-object v0, v0

    .line 2219933
    const-string v4, "POST"

    .line 2219934
    iput-object v4, v0, LX/14O;->c:Ljava/lang/String;

    .line 2219935
    move-object v0, v0

    .line 2219936
    invoke-static {v1}, LX/FGl;->a(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v1

    .line 2219937
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 2219938
    move-object v0, v0

    .line 2219939
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2219940
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2219941
    move-object v0, v0

    .line 2219942
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2219943
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2219944
    move-object v0, v0

    .line 2219945
    iput-object v2, v0, LX/14O;->l:Ljava/util/List;

    .line 2219946
    move-object v0, v0

    .line 2219947
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 2219948
    :cond_3
    iget-object v4, p0, LX/FHA;->c:LX/FGl;

    invoke-virtual {v4, v1}, LX/FGl;->b(Lcom/facebook/ui/media/attachments/MediaResource;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2219949
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "external_attachment_url"

    iget-object v5, v1, Lcom/facebook/ui/media/attachments/MediaResource;->B:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2219950
    iget-boolean v0, v1, Lcom/facebook/ui/media/attachments/MediaResource;->C:Z

    if-eqz v0, :cond_4

    .line 2219951
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "skip_attachment_hash_check"

    const-string v5, "1"

    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_0

    .line 2219952
    :cond_4
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "external_attachment_sha256_hash"

    iget-object v5, p0, LX/FHA;->f:LX/2ML;

    invoke-virtual {v5, v1}, LX/2ML;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_0

    .line 2219953
    :cond_5
    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, LX/FHA;->a:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "_"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, LX/FHA;->g:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2219954
    new-instance v5, LX/4cQ;

    invoke-direct {v5, v4, v0}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 2219955
    :cond_6
    iget-object v0, v1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v4, LX/2MK;->VIDEO:LX/2MK;

    if-ne v0, v4, :cond_8

    .line 2219956
    iget-object v0, v1, Lcom/facebook/ui/media/attachments/MediaResource;->g:Landroid/net/Uri;

    .line 2219957
    if-eqz v0, :cond_d

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    const-string v5, "file"

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 2219958
    new-instance v4, Ljava/io/File;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2219959
    new-instance v0, LX/4cQ;

    const-string v5, "video_thumbnail"

    new-instance v6, LX/4ct;

    const-string v7, "image/jpeg"

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v4, v7, v8}, LX/4ct;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v0, v5, v6}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    .line 2219960
    :goto_6
    move-object v0, v0

    .line 2219961
    if-eqz v0, :cond_7

    .line 2219962
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2219963
    :cond_7
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "video_type"

    .line 2219964
    invoke-static {v1}, LX/6eg;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5dX;

    move-result-object v5

    iget-object v5, v5, LX/5dX;->apiStringValue:Ljava/lang/String;

    move-object v5, v5

    .line 2219965
    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_2

    .line 2219966
    :cond_8
    iget-object v0, v1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v4, LX/2MK;->AUDIO:LX/2MK;

    if-ne v0, v4, :cond_0

    .line 2219967
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "audio_type"

    const-string v5, "VOICE_MESSAGE"

    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2219968
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v4, "duration"

    iget-wide v6, v1, Lcom/facebook/ui/media/attachments/MediaResource;->j:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_2

    .line 2219969
    :cond_9
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "android_key_hash"

    iget-object v6, v0, Lcom/facebook/messaging/model/attribution/ContentAppAttribution;->d:Ljava/lang/String;

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto/16 :goto_3

    .line 2219970
    :cond_a
    const-string v0, "0"

    goto/16 :goto_4

    .line 2219971
    :cond_b
    const-string v0, "0"

    goto/16 :goto_5

    :cond_c
    sget-object v5, LX/5dT;->NONQUICKCAM:LX/5dT;

    iget-object v5, v5, LX/5dT;->apiStringValue:Ljava/lang/String;

    goto/16 :goto_1

    .line 2219972
    :cond_d
    iget-object v0, p0, LX/FHA;->d:LX/03V;

    const-string v4, "SendMessageParameterHelper_missing_video_thumbnail"

    const-string v5, "No thumbnail was set for video"

    invoke-virtual {v0, v4, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2219973
    const/4 v0, 0x0

    goto :goto_6
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2219974
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2219975
    invoke-virtual {v0}, LX/0lF;->o()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2219976
    invoke-virtual {v0}, LX/0lF;->B()Ljava/lang/String;

    move-result-object v0

    .line 2219977
    :goto_0
    return-object v0

    :cond_0
    const-string v1, "id"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
