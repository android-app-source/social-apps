.class public final LX/Gsx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/InternSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/InternSettingsActivity;)V
    .locals 0

    .prologue
    .line 2400365
    iput-object p1, p0, LX/Gsx;->a:Lcom/facebook/katana/InternSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 2400366
    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2400367
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 2400368
    iget-object v0, p0, LX/Gsx;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v0, v0, Lcom/facebook/katana/InternSettingsActivity;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/0uQ;->f:LX/0Tn;

    invoke-interface {v0, v1}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2400369
    :goto_0
    return v8

    .line 2400370
    :cond_0
    iget-object v1, p0, LX/Gsx;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v1, v1, Lcom/facebook/katana/InternSettingsActivity;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0uQ;->f:LX/0Tn;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-interface {v1, v2, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2400371
    iget-object v0, p0, LX/Gsx;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v0, v0, Lcom/facebook/katana/InternSettingsActivity;->M:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2CZ;

    invoke-virtual {v0}, LX/2CZ;->a()V

    goto :goto_0
.end method
