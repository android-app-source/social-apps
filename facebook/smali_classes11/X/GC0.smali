.class public final LX/GC0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/account/recovery/fragment/FriendSearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/account/recovery/fragment/FriendSearchFragment;)V
    .locals 0

    .prologue
    .line 2327904
    iput-object p1, p0, LX/GC0;->a:Lcom/facebook/account/recovery/fragment/FriendSearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x38512ef8

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2327905
    iget-object v1, p0, LX/GC0;->a:Lcom/facebook/account/recovery/fragment/FriendSearchFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->b:Lcom/facebook/ui/search/SearchEditText;

    iget-object v2, p0, LX/GC0;->a:Lcom/facebook/account/recovery/fragment/FriendSearchFragment;

    iget-object v2, v2, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->b:Lcom/facebook/ui/search/SearchEditText;

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/facebook/ui/search/SearchEditText;->onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z

    .line 2327906
    iget-object v1, p0, LX/GC0;->a:Lcom/facebook/account/recovery/fragment/FriendSearchFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->c:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 2327907
    iget-object v1, p0, LX/GC0;->a:Lcom/facebook/account/recovery/fragment/FriendSearchFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->d:Landroid/view/View;

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 2327908
    iget-object v1, p0, LX/GC0;->a:Lcom/facebook/account/recovery/fragment/FriendSearchFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->b:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1, v6}, Lcom/facebook/ui/search/SearchEditText;->setEnabled(Z)V

    .line 2327909
    const v1, 0x167ad0be

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
