.class public LX/GPl;
.super LX/6sZ;
.source ""


# direct methods
.method public constructor <init>(LX/18V;LX/GPW;LX/GPa;LX/GPp;LX/GPe;LX/GPc;LX/GPm;LX/GPg;LX/GPo;LX/GPj;LX/GPi;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2349438
    const/16 v0, 0xb

    new-array v0, v0, [LX/6sU;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    const/4 v1, 0x1

    aput-object p3, v0, v1

    const/4 v1, 0x2

    aput-object p4, v0, v1

    const/4 v1, 0x3

    aput-object p5, v0, v1

    const/4 v1, 0x4

    aput-object p6, v0, v1

    const/4 v1, 0x5

    aput-object p7, v0, v1

    const/4 v1, 0x6

    aput-object p8, v0, v1

    const/4 v1, 0x7

    aput-object p9, v0, v1

    const/16 v1, 0x8

    aput-object p6, v0, v1

    const/16 v1, 0x9

    aput-object p10, v0, v1

    const/16 v1, 0xa

    aput-object p11, v0, v1

    invoke-direct {p0, p1, v0}, LX/6sZ;-><init>(LX/18V;[LX/6sU;)V

    .line 2349439
    return-void
.end method

.method public static b(LX/0QB;)LX/GPl;
    .locals 12

    .prologue
    .line 2349436
    new-instance v0, LX/GPl;

    invoke-static {p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v1

    check-cast v1, LX/18V;

    invoke-static {p0}, LX/GPW;->b(LX/0QB;)LX/GPW;

    move-result-object v2

    check-cast v2, LX/GPW;

    invoke-static {p0}, LX/GPa;->b(LX/0QB;)LX/GPa;

    move-result-object v3

    check-cast v3, LX/GPa;

    invoke-static {p0}, LX/GPp;->b(LX/0QB;)LX/GPp;

    move-result-object v4

    check-cast v4, LX/GPp;

    invoke-static {p0}, LX/GPe;->a(LX/0QB;)LX/GPe;

    move-result-object v5

    check-cast v5, LX/GPe;

    invoke-static {p0}, LX/GPc;->a(LX/0QB;)LX/GPc;

    move-result-object v6

    check-cast v6, LX/GPc;

    invoke-static {p0}, LX/GPm;->b(LX/0QB;)LX/GPm;

    move-result-object v7

    check-cast v7, LX/GPm;

    invoke-static {p0}, LX/GPg;->b(LX/0QB;)LX/GPg;

    move-result-object v8

    check-cast v8, LX/GPg;

    invoke-static {p0}, LX/GPo;->b(LX/0QB;)LX/GPo;

    move-result-object v9

    check-cast v9, LX/GPo;

    invoke-static {p0}, LX/GPj;->a(LX/0QB;)LX/GPj;

    move-result-object v10

    check-cast v10, LX/GPj;

    invoke-static {p0}, LX/GPi;->a(LX/0QB;)LX/GPi;

    move-result-object v11

    check-cast v11, LX/GPi;

    invoke-direct/range {v0 .. v11}, LX/GPl;-><init>(LX/18V;LX/GPW;LX/GPa;LX/GPp;LX/GPe;LX/GPc;LX/GPm;LX/GPg;LX/GPo;LX/GPj;LX/GPi;)V

    .line 2349437
    return-object v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 2349433
    const-string v0, "PaymentsWebServiceHandler"

    const v1, 0x1c657f9f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2349434
    :try_start_0
    invoke-super {p0, p1, p2}, LX/6sZ;->handleOperation(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2349435
    const v1, -0x24b34ef0

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, 0x1eda17eb

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
