.class public final enum LX/F6I;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/F6I;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/F6I;

.field public static final enum FETCH_HEADER:LX/F6I;

.field public static final enum FETCH_PERMALINK_STORY:LX/F6I;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2200336
    new-instance v0, LX/F6I;

    const-string v1, "FETCH_HEADER"

    invoke-direct {v0, v1, v2}, LX/F6I;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F6I;->FETCH_HEADER:LX/F6I;

    .line 2200337
    new-instance v0, LX/F6I;

    const-string v1, "FETCH_PERMALINK_STORY"

    invoke-direct {v0, v1, v3}, LX/F6I;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F6I;->FETCH_PERMALINK_STORY:LX/F6I;

    .line 2200338
    const/4 v0, 0x2

    new-array v0, v0, [LX/F6I;

    sget-object v1, LX/F6I;->FETCH_HEADER:LX/F6I;

    aput-object v1, v0, v2

    sget-object v1, LX/F6I;->FETCH_PERMALINK_STORY:LX/F6I;

    aput-object v1, v0, v3

    sput-object v0, LX/F6I;->$VALUES:[LX/F6I;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2200339
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/F6I;
    .locals 1

    .prologue
    .line 2200340
    const-class v0, LX/F6I;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/F6I;

    return-object v0
.end method

.method public static values()[LX/F6I;
    .locals 1

    .prologue
    .line 2200341
    sget-object v0, LX/F6I;->$VALUES:[LX/F6I;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/F6I;

    return-object v0
.end method
