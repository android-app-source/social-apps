.class public final LX/Fa0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;


# direct methods
.method public constructor <init>(Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;)V
    .locals 0

    .prologue
    .line 2258239
    iput-object p1, p0, LX/Fa0;->a:Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x2

    const v0, 0x6491ca50

    invoke-static {v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2258240
    instance-of v0, p1, Landroid/widget/CheckedTextView;

    if-nez v0, :cond_0

    .line 2258241
    const v0, -0x7ec8fe24

    invoke-static {v1, v1, v0, v3}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2258242
    :goto_0
    return-void

    .line 2258243
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 2258244
    instance-of v0, v1, Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 2258245
    const v0, 0x1da64b13

    invoke-static {v0, v3}, LX/02F;->a(II)V

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 2258246
    check-cast v0, Ljava/lang/Integer;

    .line 2258247
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ltz v4, :cond_2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v4, p0, LX/Fa0;->a:Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;

    iget-object v4, v4, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;->b:LX/Fa6;

    invoke-virtual {v4}, LX/Fa6;->a()I

    move-result v4

    if-lt v0, v4, :cond_3

    .line 2258248
    :cond_2
    const v0, -0x783e52a4

    invoke-static {v0, v3}, LX/02F;->a(II)V

    goto :goto_0

    .line 2258249
    :cond_3
    iget-object v0, p0, LX/Fa0;->a:Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;

    iget-object v0, v0, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;->b:LX/Fa6;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, LX/Fa6;->a(I)LX/CwV;

    move-result-object v1

    .line 2258250
    check-cast p1, Landroid/widget/CheckedTextView;

    .line 2258251
    iget-boolean v0, v1, LX/CwV;->d:Z

    move v0, v0

    .line 2258252
    if-nez v0, :cond_4

    move v0, v2

    :goto_1
    invoke-virtual {p1, v0}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 2258253
    iget-object v0, p0, LX/Fa0;->a:Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;

    iget-object v0, v0, Lcom/facebook/search/nullstate/SearchCategoryTopicContainerView;->c:LX/Fa1;

    invoke-interface {v0, v1}, LX/Fa1;->onClick(LX/CwV;)V

    .line 2258254
    const v0, -0xf1e60f5

    invoke-static {v0, v3}, LX/02F;->a(II)V

    goto :goto_0

    .line 2258255
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method
