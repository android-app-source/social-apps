.class public LX/Gcz;
.super LX/0gG;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/events/common/EventAnalyticsParams;

.field public final c:Lcom/facebook/events/common/ActionMechanism;

.field private final d:Ljava/lang/String;

.field public final e:LX/1nQ;

.field public final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/carousel/EventCardViewBinder;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/Bm1;

.field public final i:LX/Blh;

.field public j:LX/Gcv;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/util/List;Lcom/facebook/events/common/ActionSource;Lcom/facebook/events/common/ActionMechanism;Ljava/lang/String;LX/1nQ;LX/Bm1;LX/Blh;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/events/common/EventAnalyticsParams;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/events/common/ActionSource;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Lcom/facebook/events/common/ActionMechanism;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/events/common/EventAnalyticsParams;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/events/carousel/EventCardViewBinder;",
            ">;",
            "Lcom/facebook/events/common/ActionSource;",
            "Lcom/facebook/events/common/ActionMechanism;",
            "Ljava/lang/String;",
            "LX/1nQ;",
            "LX/Bm1;",
            "LX/Blh;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2372922
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 2372923
    iput-object p1, p0, LX/Gcz;->a:Landroid/content/Context;

    .line 2372924
    new-instance v0, Lcom/facebook/events/common/EventAnalyticsParams;

    invoke-direct {v0, p2, p4}, Lcom/facebook/events/common/EventAnalyticsParams;-><init>(Lcom/facebook/events/common/EventAnalyticsParams;Lcom/facebook/events/common/ActionSource;)V

    move-object v0, v0

    .line 2372925
    iput-object v0, p0, LX/Gcz;->b:Lcom/facebook/events/common/EventAnalyticsParams;

    .line 2372926
    iput-object p5, p0, LX/Gcz;->c:Lcom/facebook/events/common/ActionMechanism;

    .line 2372927
    invoke-static {p3}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/Gcz;->f:Ljava/util/List;

    .line 2372928
    iget-object v0, p0, LX/Gcz;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 2372929
    new-instance p3, Ljava/util/HashMap;

    invoke-direct {p3, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 2372930
    const/4 p1, 0x0

    move p2, p1

    :goto_0
    iget-object p1, p0, LX/Gcz;->f:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p1

    if-ge p2, p1, :cond_0

    .line 2372931
    iget-object p1, p0, LX/Gcz;->f:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/events/carousel/EventCardViewBinder;

    .line 2372932
    iget-object p4, p1, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    move-object p1, p4

    .line 2372933
    iget-object p4, p1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object p1, p4

    .line 2372934
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    invoke-virtual {p3, p1, p4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2372935
    add-int/lit8 p1, p2, 0x1

    move p2, p1

    goto :goto_0

    .line 2372936
    :cond_0
    move-object v0, p3

    .line 2372937
    iput-object v0, p0, LX/Gcz;->g:Ljava/util/HashMap;

    .line 2372938
    iput-object p7, p0, LX/Gcz;->e:LX/1nQ;

    .line 2372939
    iput-object p6, p0, LX/Gcz;->d:Ljava/lang/String;

    .line 2372940
    iput-object p8, p0, LX/Gcz;->h:LX/Bm1;

    .line 2372941
    iput-object p9, p0, LX/Gcz;->i:LX/Blh;

    .line 2372942
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 2372921
    const/4 v0, -0x2

    return v0
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2372887
    new-instance v1, Lcom/facebook/events/widget/eventcard/EventsCardView;

    iget-object v0, p0, LX/Gcz;->a:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/facebook/events/widget/eventcard/EventsCardView;-><init>(Landroid/content/Context;)V

    .line 2372888
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v0, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 2372889
    invoke-virtual {v1, v0}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2372890
    iget-object v0, p0, LX/Gcz;->f:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/carousel/EventCardViewBinder;

    .line 2372891
    iget-object v2, p0, LX/Gcz;->c:Lcom/facebook/events/common/ActionMechanism;

    iget-object v3, p0, LX/Gcz;->d:Ljava/lang/String;

    .line 2372892
    iput-object v1, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->w:Lcom/facebook/events/widget/eventcard/EventsCardView;

    .line 2372893
    iput-object v2, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->x:Lcom/facebook/events/common/ActionMechanism;

    .line 2372894
    iput-object p0, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->y:LX/Gcz;

    .line 2372895
    iput-object v3, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->z:Ljava/lang/String;

    .line 2372896
    iget-object v4, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->k:LX/Bl6;

    iget-object v5, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->B:LX/Gcs;

    invoke-virtual {v4, v5}, LX/0b4;->a(LX/0b2;)Z

    .line 2372897
    iget-object v4, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->f:Landroid/content/res/Resources;

    const v5, 0x7f020640

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/facebook/events/widget/eventcard/EventsCardView;->a(Landroid/graphics/drawable/Drawable;)V

    .line 2372898
    invoke-virtual {v1}, Lcom/facebook/events/widget/eventcard/EventsCardView;->c()V

    .line 2372899
    iget-object v4, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->d:LX/1aZ;

    invoke-virtual {v1, v4}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setCoverPhotoController(LX/1aZ;)V

    .line 2372900
    iget-object v4, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->e:Landroid/graphics/PointF;

    invoke-virtual {v1, v4}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setCoverPhotoFocusPoint(Landroid/graphics/PointF;)V

    .line 2372901
    iget-object v4, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->o:Ljava/lang/CharSequence;

    invoke-virtual {v1, v4}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2372902
    iget-object v4, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->p:Ljava/util/Date;

    invoke-virtual {v1, v4}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setCalendarFormatStartDate(Ljava/util/Date;)V

    .line 2372903
    iget-object v4, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->q:Ljava/lang/CharSequence;

    iget-object v5, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->r:Ljava/lang/CharSequence;

    invoke-virtual {v1, v4, v5}, Lcom/facebook/events/widget/eventcard/EventsCardView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2372904
    iget-object v4, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->s:Ljava/lang/CharSequence;

    invoke-virtual {v1, v4}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setSocialContextText(Ljava/lang/CharSequence;)V

    .line 2372905
    invoke-virtual {v1}, Lcom/facebook/events/widget/eventcard/EventsCardView;->getActionButton()Lcom/facebook/events/widget/eventcard/EventActionButtonView;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->g:LX/Bne;

    iget-object v6, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->u:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    iget-object v7, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->t:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    iget-object v8, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->v:Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;

    invoke-virtual {v5, v6, v7, v8}, LX/Bne;->a(Lcom/facebook/graphql/enums/GraphQLConnectionStyle;Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;Lcom/facebook/graphql/enums/GraphQLEventWatchStatus;)LX/BnW;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/events/widget/eventcard/EventActionButtonView;->a(LX/BnW;)V

    .line 2372906
    invoke-virtual {v1}, Lcom/facebook/events/widget/eventcard/EventsCardView;->getRemoveButton()Landroid/view/View;

    move-result-object v4

    .line 2372907
    iget-object v5, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->A:Ljava/util/List;

    if-eqz v5, :cond_0

    iget-object v5, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->A:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2372908
    :cond_0
    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2372909
    :goto_0
    iget-object v5, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->A:Ljava/util/List;

    if-eqz v5, :cond_1

    iget-object v5, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->A:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    .line 2372910
    iget-object v5, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->A:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_3

    .line 2372911
    iget-object v5, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->A:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Gco;

    .line 2372912
    iget-object v6, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->b:Landroid/content/Context;

    .line 2372913
    iget-object v7, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    move-object v7, v7

    .line 2372914
    invoke-virtual {v5, v6, v7}, LX/Gco;->a(Landroid/content/Context;Lcom/facebook/events/model/Event;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2372915
    :cond_1
    :goto_1
    new-instance v5, LX/Gcp;

    invoke-direct {v5, v0, v4}, LX/Gcp;-><init>(Lcom/facebook/events/carousel/EventCardViewBinder;Landroid/view/View;)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2372916
    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2372917
    new-instance v2, LX/Gcx;

    invoke-direct {v2, p0, v0, p2}, LX/Gcx;-><init>(LX/Gcz;Lcom/facebook/events/carousel/EventCardViewBinder;I)V

    invoke-virtual {v1, v2}, Lcom/facebook/events/widget/eventcard/EventsCardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2372918
    return-object v1

    .line 2372919
    :cond_2
    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2372920
    :cond_3
    iget-object v5, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->f:Landroid/content/res/Resources;

    const v6, 0x7f082130

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final a(LX/7oa;)V
    .locals 2

    .prologue
    .line 2372880
    iget-object v0, p0, LX/Gcz;->g:Ljava/util/HashMap;

    invoke-interface {p1}, LX/7oa;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2372881
    if-nez v0, :cond_0

    .line 2372882
    :goto_0
    return-void

    .line 2372883
    :cond_0
    iget-object v1, p0, LX/Gcz;->f:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/carousel/EventCardViewBinder;

    .line 2372884
    invoke-static {p1}, LX/Bm1;->c(LX/7oa;)LX/7vC;

    move-result-object v1

    invoke-virtual {v1}, LX/7vC;->b()Lcom/facebook/events/model/Event;

    move-result-object v1

    .line 2372885
    invoke-virtual {v0, v1}, Lcom/facebook/events/carousel/EventCardViewBinder;->a(Lcom/facebook/events/model/Event;)V

    .line 2372886
    invoke-virtual {p0}, LX/0gG;->kV_()V

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2372875
    if-ltz p2, :cond_0

    iget-object v0, p0, LX/Gcz;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 2372876
    iget-object v0, p0, LX/Gcz;->f:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/carousel/EventCardViewBinder;

    .line 2372877
    invoke-virtual {v0}, Lcom/facebook/events/carousel/EventCardViewBinder;->a()V

    .line 2372878
    :cond_0
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2372879
    return-void
.end method

.method public final a(Lcom/facebook/events/carousel/EventCardViewBinder;LX/Gco;)V
    .locals 5

    .prologue
    .line 2372855
    iget-object v0, p1, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    move-object v0, v0

    .line 2372856
    iget-object v1, v0, Lcom/facebook/events/model/Event;->an:Ljava/lang/String;

    move-object v1, v1

    .line 2372857
    invoke-virtual {p2}, LX/Gco;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2372858
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 2372859
    iget-object v0, p0, LX/Gcz;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/carousel/EventCardViewBinder;

    .line 2372860
    iget-object v4, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    move-object v4, v4

    .line 2372861
    iget-object p1, v4, Lcom/facebook/events/model/Event;->an:Ljava/lang/String;

    move-object v4, p1

    .line 2372862
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2372863
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2372864
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/events/carousel/EventCardViewBinder;

    .line 2372865
    invoke-virtual {v0}, Lcom/facebook/events/carousel/EventCardViewBinder;->a()V

    .line 2372866
    iget-object v2, p0, LX/Gcz;->f:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2372867
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/events/carousel/EventCardViewBinder;->a()V

    .line 2372868
    iget-object v0, p0, LX/Gcz;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2372869
    :cond_3
    invoke-virtual {p0}, LX/0gG;->kV_()V

    .line 2372870
    iget-object v0, p0, LX/Gcz;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/Gcz;->j:LX/Gcv;

    if-eqz v0, :cond_4

    .line 2372871
    iget-object v0, p0, LX/Gcz;->j:LX/Gcv;

    invoke-interface {v0}, LX/Gcv;->a()V

    .line 2372872
    :cond_4
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2372874
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2372873
    iget-object v0, p0, LX/Gcz;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
