.class public final LX/H2S;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2417240
    const-class v1, Lcom/facebook/nearby/protocol/NearbyTilesGraphQLModels$NearbyTilesModel;

    const v0, -0x29016a21

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "NearbyTiles"

    const-string v6, "981e89becc2fdf93957a7dbd30b13121"

    const-string v7, "tiled_places_search"

    const-string v8, "10155069968221729"

    const-string v9, "10155259089366729"

    .line 2417241
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2417242
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2417243
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2417244
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2417245
    sparse-switch v0, :sswitch_data_0

    .line 2417246
    :goto_0
    return-object p1

    .line 2417247
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2417248
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2417249
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2417250
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2417251
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    .line 2417252
    :sswitch_5
    const-string p1, "5"

    goto :goto_0

    .line 2417253
    :sswitch_6
    const-string p1, "6"

    goto :goto_0

    .line 2417254
    :sswitch_7
    const-string p1, "7"

    goto :goto_0

    .line 2417255
    :sswitch_8
    const-string p1, "8"

    goto :goto_0

    .line 2417256
    :sswitch_9
    const-string p1, "9"

    goto :goto_0

    .line 2417257
    :sswitch_a
    const-string p1, "10"

    goto :goto_0

    .line 2417258
    :sswitch_b
    const-string p1, "11"

    goto :goto_0

    .line 2417259
    :sswitch_c
    const-string p1, "12"

    goto :goto_0

    .line 2417260
    :sswitch_d
    const-string p1, "13"

    goto :goto_0

    .line 2417261
    :sswitch_e
    const-string p1, "14"

    goto :goto_0

    .line 2417262
    :sswitch_f
    const-string p1, "15"

    goto :goto_0

    .line 2417263
    :sswitch_10
    const-string p1, "16"

    goto :goto_0

    .line 2417264
    :sswitch_11
    const-string p1, "17"

    goto :goto_0

    .line 2417265
    :sswitch_12
    const-string p1, "18"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7f0f4707 -> :sswitch_0
        -0x75a8ea72 -> :sswitch_11
        -0x63aee64a -> :sswitch_4
        -0x5d4e11c2 -> :sswitch_e
        -0x4adfaf68 -> :sswitch_a
        -0x25df1291 -> :sswitch_9
        -0x1ef3c294 -> :sswitch_7
        -0x1ef3c293 -> :sswitch_8
        -0x18a514ef -> :sswitch_b
        0x3aa5ec6 -> :sswitch_2
        0x6890047 -> :sswitch_f
        0x1b78a6d9 -> :sswitch_10
        0x34e3348a -> :sswitch_12
        0x49bce27d -> :sswitch_6
        0x50c497ca -> :sswitch_d
        0x5ab845a5 -> :sswitch_5
        0x67c4cb49 -> :sswitch_c
        0x73162643 -> :sswitch_3
        0x79634aa2 -> :sswitch_1
    .end sparse-switch
.end method
