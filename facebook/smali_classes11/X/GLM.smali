.class public LX/GLM;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "LX/GLL;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/GLL;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .param p2    # Ljava/util/List;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "LX/GLL;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2342685
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 2342686
    iput-object p2, p0, LX/GLM;->a:Ljava/util/List;

    .line 2342687
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/GLM;->b:Landroid/view/LayoutInflater;

    .line 2342688
    return-void
.end method


# virtual methods
.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2342689
    if-nez p2, :cond_0

    iget-object v0, p0, LX/GLM;->b:Landroid/view/LayoutInflater;

    const v1, 0x7f030042

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 2342690
    :cond_0
    const v0, 0x7f0d03ce

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2342691
    iget-object v1, p0, LX/GLM;->a:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/GLL;

    .line 2342692
    iget-object v2, v1, LX/GLL;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2342693
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2342694
    return-object p2
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2342695
    if-nez p2, :cond_0

    new-instance p2, LX/GLO;

    invoke-virtual {p0}, LX/GLM;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p2, v0}, LX/GLO;-><init>(Landroid/content/Context;)V

    .line 2342696
    :goto_0
    invoke-virtual {p0, p1}, LX/GLM;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GLL;

    .line 2342697
    iget-object v1, v0, LX/GLL;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2342698
    iget-object p0, v0, LX/GLL;->b:Ljava/lang/String;

    move-object v0, p0

    .line 2342699
    iget-object p0, p2, LX/GLO;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2342700
    if-nez v0, :cond_1

    .line 2342701
    iget-object p0, p2, LX/GLO;->b:Lcom/facebook/widget/text/BetterTextView;

    const/16 p1, 0x8

    invoke-virtual {p0, p1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2342702
    :goto_1
    return-object p2

    .line 2342703
    :cond_0
    check-cast p2, LX/GLO;

    goto :goto_0

    .line 2342704
    :cond_1
    iget-object p0, p2, LX/GLO;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
