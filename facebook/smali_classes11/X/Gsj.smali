.class public final LX/Gsj;
.super Landroid/webkit/WebViewClient;
.source ""


# instance fields
.field public final synthetic a:LX/GsI;


# direct methods
.method public constructor <init>(LX/GsI;)V
    .locals 0

    .prologue
    .line 2400125
    iput-object p1, p0, LX/Gsj;->a:LX/GsI;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2400068
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 2400069
    iget-object v0, p0, LX/Gsj;->a:LX/GsI;

    iget-boolean v0, v0, LX/GsI;->i:Z

    if-nez v0, :cond_0

    .line 2400070
    iget-object v0, p0, LX/Gsj;->a:LX/GsI;

    iget-object v0, v0, LX/GsI;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 2400071
    :cond_0
    iget-object v0, p0, LX/Gsj;->a:LX/GsI;

    iget-object v0, v0, LX/GsI;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundColor(I)V

    .line 2400072
    iget-object v0, p0, LX/Gsj;->a:LX/GsI;

    iget-object v0, v0, LX/GsI;->d:Landroid/webkit/WebView;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setVisibility(I)V

    .line 2400073
    iget-object v0, p0, LX/Gsj;->a:LX/GsI;

    iget-object v0, v0, LX/GsI;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2400074
    iget-object v0, p0, LX/Gsj;->a:LX/GsI;

    const/4 v1, 0x1

    .line 2400075
    iput-boolean v1, v0, LX/GsI;->j:Z

    .line 2400076
    return-void
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 2400077
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Webview loading URL: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 2400078
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 2400079
    iget-object v0, p0, LX/Gsj;->a:LX/GsI;

    iget-boolean v0, v0, LX/GsI;->i:Z

    if-nez v0, :cond_0

    .line 2400080
    iget-object v0, p0, LX/Gsj;->a:LX/GsI;

    iget-object v0, v0, LX/GsI;->e:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 2400081
    :cond_0
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2400082
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 2400083
    iget-object v0, p0, LX/Gsj;->a:LX/GsI;

    new-instance v1, LX/GAB;

    invoke-direct {v1, p3, p2, p4}, LX/GAB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, LX/GsI;->a(Ljava/lang/Throwable;)V

    .line 2400084
    return-void
.end method

.method public final onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2400085
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V

    .line 2400086
    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->cancel()V

    .line 2400087
    iget-object v0, p0, LX/Gsj;->a:LX/GsI;

    new-instance v1, LX/GAB;

    const/16 v2, -0xb

    invoke-direct {v1, v3, v2, v3}, LX/GAB;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, LX/GsI;->a(Ljava/lang/Throwable;)V

    .line 2400088
    return-void
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    const/4 v3, -0x1

    .line 2400089
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "Redirect URL: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 2400090
    iget-object v1, p0, LX/Gsj;->a:LX/GsI;

    iget-object v1, v1, LX/GsI;->b:Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2400091
    iget-object v0, p0, LX/Gsj;->a:LX/GsI;

    invoke-virtual {v0, p2}, LX/GsI;->a(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    .line 2400092
    const-string v0, "error"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2400093
    if-nez v0, :cond_0

    .line 2400094
    const-string v0, "error_type"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2400095
    :cond_0
    const-string v1, "error_msg"

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2400096
    if-nez v1, :cond_1

    .line 2400097
    const-string v1, "error_message"

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2400098
    :cond_1
    if-nez v1, :cond_2

    .line 2400099
    const-string v1, "error_description"

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2400100
    :cond_2
    const-string v2, "error_code"

    invoke-virtual {v5, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2400101
    invoke-static {v2}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_b

    .line 2400102
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 2400103
    :goto_0
    invoke-static {v0}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-static {v1}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    if-ne v2, v3, :cond_5

    .line 2400104
    iget-object v0, p0, LX/Gsj;->a:LX/GsI;

    .line 2400105
    iget-object v1, v0, LX/GsI;->c:LX/GsB;

    if-eqz v1, :cond_3

    iget-boolean v1, v0, LX/GsI;->h:Z

    if-nez v1, :cond_3

    .line 2400106
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/GsI;->h:Z

    .line 2400107
    iget-object v1, v0, LX/GsI;->c:LX/GsB;

    const/4 v2, 0x0

    invoke-interface {v1, v5, v2}, LX/GsB;->a(Landroid/os/Bundle;LX/GAA;)V

    .line 2400108
    invoke-virtual {v0}, LX/GsI;->dismiss()V

    .line 2400109
    :cond_3
    :goto_1
    move v0, v4

    .line 2400110
    :cond_4
    :goto_2
    return v0

    .line 2400111
    :catch_0
    move v2, v3

    goto :goto_0

    .line 2400112
    :cond_5
    if-eqz v0, :cond_7

    const-string v3, "access_denied"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "OAuthAccessDeniedException"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2400113
    :cond_6
    iget-object v0, p0, LX/Gsj;->a:LX/GsI;

    invoke-virtual {v0}, LX/GsI;->cancel()V

    goto :goto_1

    .line 2400114
    :cond_7
    const/16 v3, 0x1069

    if-ne v2, v3, :cond_8

    .line 2400115
    iget-object v0, p0, LX/Gsj;->a:LX/GsI;

    invoke-virtual {v0}, LX/GsI;->cancel()V

    goto :goto_1

    .line 2400116
    :cond_8
    new-instance v3, LX/GAF;

    invoke-direct {v3, v2, v0, v1}, LX/GAF;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 2400117
    iget-object v0, p0, LX/Gsj;->a:LX/GsI;

    new-instance v2, LX/GAM;

    invoke-direct {v2, v3, v1}, LX/GAM;-><init>(LX/GAF;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/GsI;->a(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2400118
    :cond_9
    const-string v1, "fbconnect://cancel"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2400119
    iget-object v0, p0, LX/Gsj;->a:LX/GsI;

    invoke-virtual {v0}, LX/GsI;->cancel()V

    move v0, v4

    .line 2400120
    goto :goto_2

    .line 2400121
    :cond_a
    const-string v1, "touch"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2400122
    :try_start_1
    iget-object v1, p0, LX/Gsj;->a:LX/GsI;

    invoke-virtual {v1}, LX/GsI;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {v2, v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move v0, v4

    .line 2400123
    goto :goto_2

    .line 2400124
    :catch_1
    goto :goto_2

    :cond_b
    move v2, v3

    goto/16 :goto_0
.end method
