.class public LX/Gi4;
.super Landroid/view/View;
.source ""


# instance fields
.field public final a:LX/1nq;

.field public b:Landroid/text/Layout;

.field public c:Landroid/text/Layout;

.field public d:Landroid/text/Layout;

.field public final e:I

.field public f:F

.field public g:F

.field private final h:F

.field private final i:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2385237
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2385238
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const v1, 0x7f0a00d5

    invoke-static {p1, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, LX/Gi4;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2385239
    const v0, 0x7f0a010e

    invoke-static {p1, v0}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    iput v0, p0, LX/Gi4;->e:I

    .line 2385240
    invoke-virtual {p0}, LX/Gi4;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b161d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, LX/Gi4;->h:F

    .line 2385241
    invoke-virtual {p0}, LX/Gi4;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0082

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, LX/Gi4;->i:F

    .line 2385242
    new-instance v0, LX/1nq;

    invoke-direct {v0}, LX/1nq;-><init>()V

    invoke-virtual {p0}, LX/Gi4;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b004e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, LX/1nq;->b(I)LX/1nq;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/1nq;->b(Z)LX/1nq;

    move-result-object v0

    iget v1, p0, LX/Gi4;->i:F

    invoke-virtual {v0, v1}, LX/1nq;->a(F)LX/1nq;

    move-result-object v0

    iput-object v0, p0, LX/Gi4;->a:LX/1nq;

    .line 2385243
    return-void
.end method


# virtual methods
.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2385244
    iget v0, p0, LX/Gi4;->h:F

    .line 2385245
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, LX/Gi4;->f:F

    sub-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    .line 2385246
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, LX/Gi4;->h:F

    sub-float/2addr v2, v3

    iget v3, p0, LX/Gi4;->g:F

    sub-float/2addr v2, v3

    .line 2385247
    iget v3, p0, LX/Gi4;->i:F

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2385248
    iget-object v3, p0, LX/Gi4;->c:Landroid/text/Layout;

    invoke-virtual {v3, p1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 2385249
    sub-float v0, v1, v0

    invoke-virtual {p1, v0, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2385250
    iget-object v0, p0, LX/Gi4;->b:Landroid/text/Layout;

    invoke-virtual {v0, p1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 2385251
    sub-float v0, v2, v1

    invoke-virtual {p1, v0, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2385252
    iget-object v0, p0, LX/Gi4;->d:Landroid/text/Layout;

    invoke-virtual {v0, p1}, Landroid/text/Layout;->draw(Landroid/graphics/Canvas;)V

    .line 2385253
    return-void
.end method

.method public final onMeasure(II)V
    .locals 4

    .prologue
    .line 2385254
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 2385255
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 2385256
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 2385257
    const/high16 v3, 0x40000000    # 2.0f

    if-eq v2, v3, :cond_0

    .line 2385258
    iget-object v0, p0, LX/Gi4;->a:LX/1nq;

    invoke-virtual {v0}, LX/1nq;->b()F

    move-result v0

    const/high16 v2, 0x40000000    # 2.0f

    iget v3, p0, LX/Gi4;->i:F

    mul-float/2addr v2, v3

    add-float/2addr v0, v2

    float-to-int v0, v0

    .line 2385259
    :cond_0
    invoke-virtual {p0, v1, v0}, LX/Gi4;->setMeasuredDimension(II)V

    .line 2385260
    return-void
.end method
