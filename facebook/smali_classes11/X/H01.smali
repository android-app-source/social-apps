.class public LX/H01;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Ri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ri",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;",
            "Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0Ri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ri",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;",
            "Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    .line 2412725
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->FULL_NAME:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    sget-object v1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;->NAME:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->EMAIL:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    sget-object v3, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;->EMAIL:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->PHONE:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    sget-object v5, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;->PHONE:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->STREET_ADDRESS:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    sget-object v7, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;->ADDRESS:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    .line 2412726
    const/4 v8, 0x4

    new-array v8, v8, [Ljava/util/Map$Entry;

    const/4 v9, 0x0

    invoke-static {v0, v1}, LX/0P1;->entryOf(Ljava/lang/Object;Ljava/lang/Object;)LX/0P3;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v2, v3}, LX/0P1;->entryOf(Ljava/lang/Object;Ljava/lang/Object;)LX/0P3;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-static {v4, v5}, LX/0P1;->entryOf(Ljava/lang/Object;Ljava/lang/Object;)LX/0P3;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x3

    invoke-static {v6, v7}, LX/0P1;->entryOf(Ljava/lang/Object;Ljava/lang/Object;)LX/0P3;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v8}, LX/0Rg;->a([Ljava/util/Map$Entry;)LX/0Rg;

    move-result-object v8

    move-object v0, v8

    .line 2412727
    sput-object v0, LX/H01;->a:LX/0Ri;

    .line 2412728
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->TEXT:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    sget-object v1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;->TEXT:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    invoke-static {v0, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    sput-object v0, LX/H01;->b:LX/0Ri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2412729
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
