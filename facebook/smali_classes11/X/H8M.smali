.class public LX/H8M;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8L;


# instance fields
.field public final a:LX/HDT;

.field public final b:LX/H9D;

.field public final c:LX/HAF;

.field public final d:LX/CXj;

.field private final e:LX/0kb;

.field private final f:LX/03V;

.field public final g:LX/8Do;

.field public final h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/H8P;

.field public k:Lcom/facebook/fig/actionbar/FigActionBar;

.field public l:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Integer;",
            "LX/H8Z;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/H8F;

.field public o:LX/H8H;

.field public p:Landroid/os/ParcelUuid;

.field private q:Z

.field public r:Z

.field public s:Z

.field public t:I

.field public u:Z


# direct methods
.method public constructor <init>(LX/H9E;LX/HDT;LX/HAF;LX/CXj;LX/0kb;LX/03V;LX/8Do;LX/H8P;Lcom/facebook/fig/actionbar/FigActionBar;)V
    .locals 3
    .param p9    # Lcom/facebook/fig/actionbar/FigActionBar;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2433171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2433172
    iput-boolean v2, p0, LX/H8M;->r:Z

    .line 2433173
    iput-boolean v2, p0, LX/H8M;->s:Z

    .line 2433174
    const/4 v0, -0x1

    iput v0, p0, LX/H8M;->t:I

    .line 2433175
    iput-boolean v2, p0, LX/H8M;->u:Z

    .line 2433176
    iput-object p3, p0, LX/H8M;->c:LX/HAF;

    .line 2433177
    iput-object p8, p0, LX/H8M;->j:LX/H8P;

    .line 2433178
    invoke-virtual {p1, p9}, LX/H9E;->a(Landroid/view/View;)LX/H9D;

    move-result-object v0

    iput-object v0, p0, LX/H8M;->b:LX/H9D;

    .line 2433179
    iput-object p9, p0, LX/H8M;->k:Lcom/facebook/fig/actionbar/FigActionBar;

    .line 2433180
    iput-object p2, p0, LX/H8M;->a:LX/HDT;

    .line 2433181
    iput-object p4, p0, LX/H8M;->d:LX/CXj;

    .line 2433182
    iput-object p5, p0, LX/H8M;->e:LX/0kb;

    .line 2433183
    iput-object p6, p0, LX/H8M;->f:LX/03V;

    .line 2433184
    iget-object v0, p0, LX/H8M;->k:Lcom/facebook/fig/actionbar/FigActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, LX/EtL;->a(ZZ)V

    .line 2433185
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/H8M;->l:Ljava/util/LinkedHashMap;

    .line 2433186
    iput-object p7, p0, LX/H8M;->g:LX/8Do;

    .line 2433187
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/H8M;->h:Ljava/util/HashMap;

    .line 2433188
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/H8M;->i:Ljava/util/HashSet;

    .line 2433189
    const/4 v0, 0x0

    .line 2433190
    iget-object v1, p0, LX/H8M;->k:Lcom/facebook/fig/actionbar/FigActionBar;

    new-instance v2, LX/H8K;

    invoke-direct {v2, p0}, LX/H8K;-><init>(LX/H8M;)V

    .line 2433191
    iput-object v2, v1, LX/EtL;->b:LX/EtN;

    .line 2433192
    iget-object v1, p0, LX/H8M;->g:LX/8Do;

    .line 2433193
    iget-object v2, v1, LX/8Do;->a:LX/0ad;

    sget-object p1, LX/0c0;->Live:LX/0c0;

    sget-short p2, LX/8Dn;->e:S

    const/4 p3, 0x0

    invoke-interface {v2, p1, p2, p3}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v2

    move v1, v2

    .line 2433194
    if-nez v1, :cond_0

    .line 2433195
    iget-object v1, p0, LX/H8M;->a:LX/HDT;

    invoke-static {p0}, LX/H8M;->g(LX/H8M;)LX/H8F;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2433196
    :goto_0
    iget-object v1, p0, LX/H8M;->a:LX/HDT;

    .line 2433197
    new-instance v2, LX/H8I;

    invoke-direct {v2, p0}, LX/H8I;-><init>(LX/H8M;)V

    move-object v2, v2

    .line 2433198
    iput-object v2, p0, LX/H8M;->o:LX/H8H;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2433199
    invoke-static {p0}, LX/HAF;->a(LX/H8L;)LX/0Px;

    move-result-object v1

    iput-object v1, p0, LX/H8M;->m:LX/0Px;

    .line 2433200
    iget-object v1, p0, LX/H8M;->m:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_1

    iget-object v0, p0, LX/H8M;->m:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8E;

    .line 2433201
    iget-object p1, p0, LX/H8M;->a:LX/HDT;

    invoke-virtual {p1, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 2433202
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2433203
    :cond_0
    iget-object v1, p0, LX/H8M;->a:LX/HDT;

    invoke-static {p0}, LX/H8M;->g(LX/H8M;)LX/H8F;

    move-result-object v2

    iput-object v2, p0, LX/H8M;->n:LX/H8F;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    goto :goto_0

    .line 2433204
    :cond_1
    return-void
.end method

.method public static f(LX/H8M;)V
    .locals 3

    .prologue
    .line 2433287
    iget-object v0, p0, LX/H8M;->l:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8Z;

    .line 2433288
    iget-object v2, p0, LX/H8M;->c:LX/HAF;

    invoke-virtual {v2, v0}, LX/HAF;->b(LX/H8Y;)V

    goto :goto_0

    .line 2433289
    :cond_0
    iget-object v0, p0, LX/H8M;->l:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 2433290
    return-void
.end method

.method public static g(LX/H8M;)LX/H8F;
    .locals 1

    .prologue
    .line 2433286
    new-instance v0, LX/H8G;

    invoke-direct {v0, p0}, LX/H8G;-><init>(LX/H8M;)V

    return-object v0
.end method

.method public static i(LX/H8M;)V
    .locals 3

    .prologue
    .line 2433291
    iget-boolean v0, p0, LX/H8M;->q:Z

    if-eqz v0, :cond_1

    .line 2433292
    :cond_0
    :goto_0
    return-void

    .line 2433293
    :cond_1
    iget-object v0, p0, LX/H8M;->e:LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2433294
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/H8M;->q:Z

    .line 2433295
    iget-object v0, p0, LX/H8M;->d:LX/CXj;

    new-instance v1, LX/CXn;

    iget-object v2, p0, LX/H8M;->p:Landroid/os/ParcelUuid;

    invoke-direct {v1, v2}, LX/CXn;-><init>(Landroid/os/ParcelUuid;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 11

    .prologue
    const/4 v5, 0x0

    .line 2433221
    iget-object v0, p0, LX/H8M;->k:Lcom/facebook/fig/actionbar/FigActionBar;

    invoke-virtual {v0}, LX/EtL;->clear()V

    .line 2433222
    iget-object v0, p0, LX/H8M;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 2433223
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 2433224
    const/4 v3, 0x0

    .line 2433225
    iget-object v0, p0, LX/H8M;->l:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v4, v5

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2433226
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/H8Z;

    .line 2433227
    invoke-interface {v1}, LX/H8Z;->a()LX/HA7;

    move-result-object v8

    .line 2433228
    iget-boolean v2, v8, LX/HA7;->f:Z

    move v2, v2

    .line 2433229
    if-eqz v2, :cond_0

    .line 2433230
    iget-object v2, v8, LX/HA7;->c:Ljava/lang/String;

    move-object v2, v2

    .line 2433231
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2433232
    iget-object v9, p0, LX/H8M;->k:Lcom/facebook/fig/actionbar/FigActionBar;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2433233
    iget-object v10, v8, LX/HA7;->c:Ljava/lang/String;

    move-object v10, v10

    .line 2433234
    invoke-virtual {v9, v5, v2, v5, v10}, LX/EtL;->a(IIILjava/lang/CharSequence;)LX/3qv;

    move-result-object v2

    .line 2433235
    :goto_1
    iget v9, v8, LX/HA7;->e:I

    move v9, v9

    .line 2433236
    invoke-interface {v2, v9}, LX/3qv;->setShowAsActionFlags(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 2433237
    iget-boolean v9, v8, LX/HA7;->f:Z

    move v9, v9

    .line 2433238
    invoke-interface {v2, v9}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    move-result-object v2

    .line 2433239
    iget-boolean v9, v8, LX/HA7;->g:Z

    move v9, v9

    .line 2433240
    invoke-interface {v2, v9}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    move-result-object v2

    .line 2433241
    iget-boolean v9, v8, LX/HA7;->h:Z

    move v9, v9

    .line 2433242
    invoke-interface {v2, v9}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    move-result-object v2

    .line 2433243
    iget v9, v8, LX/HA7;->d:I

    move v9, v9

    .line 2433244
    invoke-interface {v2, v9}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2433245
    iget-object v2, v8, LX/HA7;->i:Ljava/lang/String;

    move-object v2, v2

    .line 2433246
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2433247
    iget-object v2, p0, LX/H8M;->h:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    .line 2433248
    iget-object v10, v8, LX/HA7;->i:Ljava/lang/String;

    move-object v10, v10

    .line 2433249
    invoke-virtual {v2, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2433250
    :cond_1
    add-int/lit8 v2, v4, 0x1

    .line 2433251
    invoke-virtual {v8}, LX/HA7;->j()I

    move-result v4

    if-eqz v4, :cond_2

    .line 2433252
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v8}, LX/HA7;->j()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v6, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2433253
    :cond_2
    instance-of v0, v1, LX/H94;

    if-eqz v0, :cond_9

    .line 2433254
    check-cast v1, LX/H94;

    :goto_2
    move v4, v2

    move-object v3, v1

    .line 2433255
    goto/16 :goto_0

    .line 2433256
    :cond_3
    iget v2, v8, LX/HA7;->b:I

    move v2, v2

    .line 2433257
    if-lez v2, :cond_4

    .line 2433258
    iget-object v9, p0, LX/H8M;->k:Lcom/facebook/fig/actionbar/FigActionBar;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2433259
    iget v10, v8, LX/HA7;->b:I

    move v10, v10

    .line 2433260
    invoke-virtual {v9, v5, v2, v5, v10}, LX/EtL;->a(IIII)LX/3qv;

    move-result-object v2

    goto :goto_1

    .line 2433261
    :cond_4
    iget-object v0, p0, LX/H8M;->f:LX/03V;

    const-class v1, LX/H8M;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "No valid text string or resource available for page action."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2433262
    :cond_5
    invoke-interface {v6}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2433263
    iget-object v0, p0, LX/H8M;->k:Lcom/facebook/fig/actionbar/FigActionBar;

    new-instance v1, LX/H8D;

    invoke-direct {v1, p0, v6}, LX/H8D;-><init>(LX/H8M;Ljava/util/Map;)V

    .line 2433264
    iget-object v2, v0, LX/EtL;->a:LX/EtR;

    .line 2433265
    iput-object v1, v2, LX/EtR;->h:LX/H8D;

    .line 2433266
    :cond_6
    iget-object v0, p0, LX/H8M;->k:Lcom/facebook/fig/actionbar/FigActionBar;

    invoke-virtual {v0}, Lcom/facebook/fig/actionbar/FigActionBar;->requestLayout()V

    .line 2433267
    iget-object v0, p0, LX/H8M;->k:Lcom/facebook/fig/actionbar/FigActionBar;

    invoke-virtual {v0}, Lcom/facebook/fig/actionbar/FigActionBar;->invalidate()V

    .line 2433268
    iget v0, p0, LX/H8M;->t:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_7

    if-eqz v3, :cond_7

    .line 2433269
    iget-object v0, v3, LX/H94;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->C()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    move-object v0, v0

    .line 2433270
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v0, v1, :cond_7

    .line 2433271
    iget-object v0, p0, LX/H8M;->j:LX/H8P;

    iget-object v1, p0, LX/H8M;->k:Lcom/facebook/fig/actionbar/FigActionBar;

    iget v2, p0, LX/H8M;->t:I

    invoke-virtual {v1, v2}, Lcom/facebook/fig/actionbar/FigActionBar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2433272
    iget-object v2, v0, LX/H8P;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2433273
    :cond_7
    :goto_3
    iget-object v0, p0, LX/H8M;->k:Lcom/facebook/fig/actionbar/FigActionBar;

    invoke-virtual {v0}, LX/EtL;->b()V

    .line 2433274
    iget-boolean v0, p0, LX/H8M;->u:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, LX/H8M;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 2433275
    iget-object v0, p0, LX/H8M;->k:Lcom/facebook/fig/actionbar/FigActionBar;

    new-instance v1, Lcom/facebook/pages/common/actionchannel/actionbar/PagesActionBarChannelView$2;

    invoke-direct {v1, p0}, Lcom/facebook/pages/common/actionchannel/actionbar/PagesActionBarChannelView$2;-><init>(LX/H8M;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/actionbar/FigActionBar;->post(Ljava/lang/Runnable;)Z

    .line 2433276
    :cond_8
    return-void

    :cond_9
    move-object v1, v3

    goto/16 :goto_2

    .line 2433277
    :cond_a
    iget-object v2, v0, LX/H8P;->a:LX/0iA;

    sget-object v3, LX/H8Q;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v4, LX/H8Q;

    invoke-virtual {v2, v3, v4}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v2

    check-cast v2, LX/H8Q;

    .line 2433278
    if-eqz v2, :cond_7

    .line 2433279
    new-instance v3, LX/0hs;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    const/4 v5, 0x2

    invoke-direct {v3, v4, v5}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2433280
    const/4 v4, -0x1

    .line 2433281
    iput v4, v3, LX/0hs;->t:I

    .line 2433282
    const v4, 0x7f08368a

    invoke-virtual {v3, v4}, LX/0hs;->b(I)V

    .line 2433283
    invoke-virtual {v3, v1}, LX/0ht;->a(Landroid/view/View;)V

    .line 2433284
    iget-object v3, v2, LX/H8Q;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/H8Q;->b:LX/0Tn;

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 2433285
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v0, LX/H8P;->b:Ljava/lang/Boolean;

    goto :goto_3
.end method

.method public final a(LX/CZd;)V
    .locals 4

    .prologue
    .line 2433207
    invoke-virtual {p1}, LX/CZd;->c()Z

    move-result v0

    iput-boolean v0, p0, LX/H8M;->u:Z

    .line 2433208
    iget-object v0, p1, LX/CZd;->i:LX/0Px;

    move-object v0, v0

    .line 2433209
    invoke-static {p0}, LX/H8M;->f(LX/H8M;)V

    .line 2433210
    if-nez v0, :cond_1

    .line 2433211
    :cond_0
    invoke-virtual {p0}, LX/H8M;->a()V

    .line 2433212
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/H8M;->r:Z

    .line 2433213
    return-void

    .line 2433214
    :cond_1
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    if-ge v2, v1, :cond_0

    .line 2433215
    iget-object v3, p0, LX/H8M;->b:LX/H9D;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v3, v1}, LX/H9D;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)LX/H8Y;

    move-result-object v1

    check-cast v1, LX/H8Z;

    .line 2433216
    iget-object v3, p0, LX/H8M;->l:Ljava/util/LinkedHashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v3, p1, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2433217
    iget-object v3, p0, LX/H8M;->c:LX/HAF;

    invoke-virtual {v3, v1}, LX/HAF;->a(LX/H8Y;)V

    .line 2433218
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLPageActionType;->FOLLOW:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    if-ne v1, v3, :cond_2

    .line 2433219
    iput v2, p0, LX/H8M;->t:I

    .line 2433220
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0
.end method

.method public final getSupportedActionTypes()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPageActionType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2433205
    sget-object v0, LX/H9D;->a:LX/0Px;

    move-object v0, v0

    .line 2433206
    return-object v0
.end method
