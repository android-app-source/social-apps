.class public LX/Frd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/Fs2;

.field public final b:LX/FqO;

.field private final c:LX/5SB;

.field private final d:LX/G11;

.field private final e:LX/Fs3;

.field public final f:LX/G4x;

.field private final g:LX/G1I;

.field private final h:LX/G2n;

.field private final i:LX/G4m;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final j:LX/G1N;

.field private final k:Landroid/content/Context;

.field private final l:LX/Frw;

.field private final m:LX/Frv;

.field private final n:Lcom/facebook/common/callercontext/CallerContext;

.field private final o:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G4i;",
            ">;"
        }
    .end annotation
.end field

.field private final p:LX/0SG;

.field public final q:LX/03V;

.field private r:J

.field private final s:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

.field private final t:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0zi;",
            ">;"
        }
    .end annotation
.end field

.field public final u:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/FqO;LX/5SB;LX/G11;LX/G4x;LX/G4m;LX/G1N;Lcom/facebook/common/callercontext/CallerContext;LX/G1I;LX/G2n;LX/Fs3;LX/Frx;LX/Frv;LX/0hx;LX/0Or;LX/0SG;LX/03V;LX/1JD;LX/0Or;)V
    .locals 9
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/FqO;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/5SB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/G11;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/G4x;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/G4m;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/G1N;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/FqO;",
            "LX/5SB;",
            "LX/G11;",
            "LX/G4x;",
            "LX/G4m;",
            "LX/G1N;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/G1I;",
            "LX/G2n;",
            "LX/Fs3;",
            "LX/Frx;",
            "LX/Frv;",
            "LX/0hx;",
            "LX/0Or",
            "<",
            "LX/G4i;",
            ">;",
            "LX/0SG;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/1JD;",
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2295982
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2295983
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, LX/Frd;->t:Ljava/util/List;

    .line 2295984
    iput-object p1, p0, LX/Frd;->k:Landroid/content/Context;

    .line 2295985
    iput-object p4, p0, LX/Frd;->d:LX/G11;

    .line 2295986
    move-object/from16 v0, p11

    iput-object v0, p0, LX/Frd;->e:LX/Fs3;

    .line 2295987
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FqO;

    iput-object v2, p0, LX/Frd;->b:LX/FqO;

    .line 2295988
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/5SB;

    iput-object v2, p0, LX/Frd;->c:LX/5SB;

    .line 2295989
    invoke-static {p5}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/G4x;

    iput-object v2, p0, LX/Frd;->f:LX/G4x;

    .line 2295990
    move-object/from16 v0, p9

    iput-object v0, p0, LX/Frd;->g:LX/G1I;

    .line 2295991
    move-object/from16 v0, p10

    iput-object v0, p0, LX/Frd;->h:LX/G2n;

    .line 2295992
    iput-object p6, p0, LX/Frd;->i:LX/G4m;

    .line 2295993
    move-object/from16 v0, p7

    iput-object v0, p0, LX/Frd;->j:LX/G1N;

    .line 2295994
    move-object/from16 v0, p12

    move-object/from16 v1, p8

    invoke-virtual {v0, v1, p3}, LX/Frx;->a(Lcom/facebook/common/callercontext/CallerContext;LX/5SB;)LX/Frw;

    move-result-object v2

    iput-object v2, p0, LX/Frd;->l:LX/Frw;

    .line 2295995
    move-object/from16 v0, p13

    iput-object v0, p0, LX/Frd;->m:LX/Frv;

    .line 2295996
    move-object/from16 v0, p8

    iput-object v0, p0, LX/Frd;->n:Lcom/facebook/common/callercontext/CallerContext;

    .line 2295997
    move-object/from16 v0, p15

    iput-object v0, p0, LX/Frd;->o:LX/0Or;

    .line 2295998
    move-object/from16 v0, p17

    iput-object v0, p0, LX/Frd;->q:LX/03V;

    .line 2295999
    move-object/from16 v0, p16

    iput-object v0, p0, LX/Frd;->p:LX/0SG;

    .line 2296000
    move-object/from16 v0, p19

    iput-object v0, p0, LX/Frd;->u:LX/0Or;

    .line 2296001
    new-instance v2, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

    iget-object v3, p0, LX/Frd;->c:LX/5SB;

    iget-object v4, p0, LX/Frd;->b:LX/FqO;

    iget-object v5, p0, LX/Frd;->f:LX/G4x;

    move-object/from16 v6, p17

    move-object/from16 v7, p14

    move-object/from16 v8, p18

    invoke-direct/range {v2 .. v8}, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;-><init>(LX/5SB;LX/FqO;LX/G4x;LX/03V;LX/0hx;LX/1JD;)V

    iput-object v2, p0, LX/Frd;->s:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

    .line 2296002
    return-void
.end method

.method public static g(LX/Frd;)LX/Fs2;
    .locals 10

    .prologue
    .line 2295975
    iget-object v0, p0, LX/Frd;->a:LX/Fs2;

    if-nez v0, :cond_0

    .line 2295976
    iget-object v0, p0, LX/Frd;->e:LX/Fs3;

    iget-object v1, p0, LX/Frd;->k:Landroid/content/Context;

    new-instance v2, LX/9lP;

    iget-object v3, p0, LX/Frd;->c:LX/5SB;

    .line 2295977
    iget-wide v8, v3, LX/5SB;->b:J

    move-wide v4, v8

    .line 2295978
    iget-object v3, p0, LX/Frd;->c:LX/5SB;

    .line 2295979
    iget-wide v8, v3, LX/5SB;->a:J

    move-wide v6, v8

    .line 2295980
    invoke-direct {v2, v4, v5, v6, v7}, LX/9lP;-><init>(JJ)V

    iget-object v3, p0, LX/Frd;->n:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2, v3}, LX/Fs3;->a(Landroid/content/Context;LX/9lP;Lcom/facebook/common/callercontext/CallerContext;)LX/Fs2;

    move-result-object v0

    iput-object v0, p0, LX/Frd;->a:LX/Fs2;

    .line 2295981
    :cond_0
    iget-object v0, p0, LX/Frd;->a:LX/Fs2;

    return-object v0
.end method


# virtual methods
.method public final a(LX/Fso;)LX/0zX;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Fso;",
            ")",
            "LX/0zX",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineSection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2295959
    iget-object v0, p0, LX/Frd;->s:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

    invoke-virtual {v0, p1}, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->a(LX/Fso;)V

    .line 2295960
    iget-object v0, p0, LX/Frd;->l:LX/Frw;

    .line 2295961
    iget-object v1, v0, LX/Frw;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FsX;

    iget-object v2, v0, LX/Frw;->a:Lcom/facebook/common/callercontext/CallerContext;

    const/4 v7, 0x0

    .line 2295962
    new-instance v4, LX/0v6;

    const-string v3, "TimelineSectionQuery"

    invoke-direct {v4, v3}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 2295963
    iget-object v3, v1, LX/FsX;->b:LX/FsW;

    .line 2295964
    iget-object v5, v1, LX/FsX;->c:LX/0ad;

    sget v6, LX/0wf;->aT:I

    const/16 v8, 0x8

    invoke-interface {v5, v6, v8}, LX/0ad;->a(II)I

    move-result v5

    move v5, v5

    .line 2295965
    invoke-virtual {v3, p1, v5}, LX/FsW;->a(LX/Fso;I)LX/5xI;

    move-result-object v3

    .line 2295966
    sget-object v5, LX/0zS;->c:LX/0zS;

    invoke-static {v3, v5, v7, v2}, LX/FsX;->a(LX/5xI;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v3

    .line 2295967
    invoke-virtual {v4, v3}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v5

    invoke-static {v5}, LX/FsD;->a(LX/0zX;)LX/0zX;

    move-result-object v10

    .line 2295968
    invoke-static {p1, v3}, LX/FsX;->a(LX/Fso;LX/0zO;)LX/Fso;

    move-result-object v5

    sget-object v6, LX/0zS;->c:LX/0zS;

    .line 2295969
    iget-object v3, v1, LX/FsX;->c:LX/0ad;

    sget v8, LX/0wf;->aU:I

    const/4 v9, 0x1

    invoke-interface {v3, v8, v9}, LX/0ad;->a(II)I

    move-result v3

    move v3, v3

    .line 2295970
    add-int/lit8 v9, v3, -0x1

    move-object v3, v1

    move-object v8, v2

    invoke-static/range {v3 .. v9}, LX/FsX;->a(LX/FsX;LX/0v6;LX/Fso;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;I)LX/0zX;

    move-result-object v3

    .line 2295971
    iget-object v5, v1, LX/FsX;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0v6;)V

    .line 2295972
    if-eqz v3, :cond_0

    invoke-virtual {v10, v3}, LX/0zX;->b(LX/0zX;)LX/0zX;

    move-result-object v3

    :goto_0
    move-object v1, v3

    .line 2295973
    move-object v0, v1

    .line 2295974
    return-object v0

    :cond_0
    move-object v3, v10

    goto :goto_0
.end method

.method public final a(LX/Fsp;)LX/0zX;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Fsp;",
            ")",
            "LX/0zX",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2295946
    if-nez p1, :cond_0

    .line 2295947
    const/4 v0, 0x0

    .line 2295948
    :goto_0
    return-object v0

    .line 2295949
    :cond_0
    iget-object v0, p0, LX/Frd;->s:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

    .line 2295950
    iget-object v1, v0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->c:LX/G4x;

    sget-object v2, LX/G5A;->LOADING:LX/G5A;

    invoke-virtual {v1, v2, p1}, LX/G4x;->a(LX/G5A;LX/Fsp;)V

    .line 2295951
    iget-object v1, v0, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->b:LX/FqO;

    invoke-interface {v1}, LX/FqO;->lo_()V

    .line 2295952
    iget-object v0, p0, LX/Frd;->l:LX/Frw;

    .line 2295953
    iget-object v1, v0, LX/Frw;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Fsj;

    iget-object v2, v0, LX/Frw;->a:Lcom/facebook/common/callercontext/CallerContext;

    .line 2295954
    iget-object v3, v1, LX/Fsj;->a:LX/Fsg;

    sget-object v5, LX/0zS;->c:LX/0zS;

    const/4 v6, 0x0

    const/16 v8, 0x8

    move-object v4, p1

    move-object v7, v2

    invoke-virtual/range {v3 .. v8}, LX/Fsg;->a(LX/Fsp;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;I)LX/0zO;

    move-result-object v3

    .line 2295955
    iget-object v4, v1, LX/Fsj;->b:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    invoke-static {v3}, LX/FsA;->a(Lcom/google/common/util/concurrent/ListenableFuture;)LX/0zX;

    move-result-object v3

    .line 2295956
    invoke-static {v3}, LX/FsD;->b(LX/0zX;)LX/0zX;

    move-result-object v3

    new-instance v4, LX/Fsi;

    invoke-direct {v4, v1}, LX/Fsi;-><init>(LX/Fsj;)V

    invoke-virtual {v3, v4}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v3

    move-object v1, v3

    .line 2295957
    move-object v0, v1

    .line 2295958
    goto :goto_0
.end method

.method public final a(Z)LX/FsJ;
    .locals 3

    .prologue
    .line 2295866
    iget-object v0, p0, LX/Frd;->l:LX/Frw;

    iget-object v1, p0, LX/Frd;->d:LX/G11;

    .line 2295867
    sget-object v2, LX/G11;->PAGE:LX/G11;

    if-ne v1, v2, :cond_0

    .line 2295868
    iget-object v2, v0, LX/Frw;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FrR;

    invoke-static {v0, v2, p1}, LX/Frw;->a(LX/Frw;LX/FrR;Z)LX/FsJ;

    move-result-object v2

    .line 2295869
    :goto_0
    move-object v0, v2

    .line 2295870
    invoke-virtual {p0, v0}, LX/Frd;->a(LX/FsJ;)V

    .line 2295871
    return-object v0

    .line 2295872
    :cond_0
    iget-object v2, v0, LX/Frw;->b:LX/5SB;

    invoke-virtual {v2}, LX/5SB;->i()Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, LX/G11;->USER:LX/G11;

    if-ne v1, v2, :cond_1

    .line 2295873
    iget-object v2, v0, LX/Frw;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FrR;

    invoke-static {v0, v2, p1}, LX/Frw;->a(LX/Frw;LX/FrR;Z)LX/FsJ;

    move-result-object v2

    goto :goto_0

    .line 2295874
    :cond_1
    iget-object v2, v0, LX/Frw;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FrR;

    invoke-static {v0, v2, p1}, LX/Frw;->a(LX/Frw;LX/FrR;Z)LX/FsJ;

    move-result-object v2

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 2295943
    invoke-static {p0}, LX/Frd;->g(LX/Frd;)LX/Fs2;

    move-result-object v0

    sget-object v1, LX/Fs1;->REFRESH_ON_RESUME:LX/Fs1;

    .line 2295944
    iput-object v1, v0, LX/Fs2;->i:LX/Fs1;

    .line 2295945
    return-void
.end method

.method public final a(LX/0zX;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zX",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2295933
    iget-object v0, p0, LX/Frd;->m:LX/Frv;

    iget-object v1, p0, LX/Frd;->f:LX/G4x;

    iget-object v2, p0, LX/Frd;->s:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

    .line 2295934
    new-instance v3, LX/Frt;

    invoke-direct {v3, v0}, LX/Frt;-><init>(LX/Frv;)V

    move-object v3, v3

    .line 2295935
    invoke-virtual {p1, v3}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v3

    .line 2295936
    new-instance v4, LX/Fru;

    invoke-direct {v4, v0}, LX/Fru;-><init>(LX/Frv;)V

    move-object v4, v4

    .line 2295937
    invoke-virtual {v3, v4}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v3

    .line 2295938
    iget-object v4, v0, LX/Frv;->b:LX/4V5;

    new-instance v5, LX/Frs;

    invoke-direct {v5, v0, v1, v2}, LX/Frs;-><init>(LX/Frv;LX/G4x;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)V

    invoke-virtual {v4, v3, v5}, LX/4V5;->a(LX/0zX;LX/0rl;)LX/0zi;

    move-result-object v3

    .line 2295939
    move-object v0, v3

    .line 2295940
    if-eqz v0, :cond_0

    .line 2295941
    iget-object v1, p0, LX/Frd;->t:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2295942
    :cond_0
    return-void
.end method

.method public final a(LX/FsJ;)V
    .locals 2

    .prologue
    .line 2295928
    iget-object v0, p1, LX/FsJ;->a:LX/0zX;

    if-nez v0, :cond_0

    iget-object v0, p1, LX/FsJ;->c:LX/0zX;

    if-nez v0, :cond_0

    .line 2295929
    :goto_0
    return-void

    .line 2295930
    :cond_0
    iget-object v0, p0, LX/Frd;->p:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/Frd;->r:J

    .line 2295931
    invoke-static {}, LX/Fsq;->a()LX/Fso;

    move-result-object v0

    .line 2295932
    iget-object v1, p0, LX/Frd;->s:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

    invoke-virtual {v1, v0}, Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;->a(LX/Fso;)V

    goto :goto_0
.end method

.method public final a(LX/Fso;LX/0zX;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Fso;",
            "LX/0zX",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineSection;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2295924
    iget-object v0, p0, LX/Frd;->m:LX/Frv;

    iget-object v1, p0, LX/Frd;->b:LX/FqO;

    iget-object v2, p0, LX/Frd;->s:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

    invoke-virtual {v0, p2, p1, v1, v2}, LX/Frv;->a(LX/0zX;LX/Fso;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)LX/0zi;

    move-result-object v0

    .line 2295925
    if-eqz v0, :cond_0

    .line 2295926
    iget-object v1, p0, LX/Frd;->t:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2295927
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 2295898
    new-instance v0, Lcom/facebook/api/feed/DeleteStoryMethod$Params;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v1}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    sget-object v2, LX/55G;->LOCAL_AND_SERVER:LX/55G;

    invoke-direct {v0, p1, v1, p3, v2}, Lcom/facebook/api/feed/DeleteStoryMethod$Params;-><init>(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;LX/55G;)V

    .line 2295899
    new-instance v1, LX/5AE;

    invoke-direct {v1}, LX/5AE;-><init>()V

    .line 2295900
    iput-object p3, v1, LX/5AE;->a:Ljava/lang/String;

    .line 2295901
    move-object v1, v1

    .line 2295902
    sget-object v2, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/StoryVisibility;->name()Ljava/lang/String;

    move-result-object v2

    .line 2295903
    iput-object v2, v1, LX/5AE;->c:Ljava/lang/String;

    .line 2295904
    move-object v1, v1

    .line 2295905
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DELETE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->name()Ljava/lang/String;

    move-result-object v2

    .line 2295906
    iput-object v2, v1, LX/5AE;->b:Ljava/lang/String;

    .line 2295907
    move-object v1, v1

    .line 2295908
    invoke-virtual {v1}, LX/5AE;->a()Lcom/facebook/api/graphql/feed/StoryMutationModels$HideableStoryMutationFieldsModel;

    move-result-object v1

    .line 2295909
    invoke-static {p0}, LX/Frd;->g(LX/Frd;)LX/Fs2;

    move-result-object v2

    const-string v3, "timeline_delete_story"

    new-instance v4, LX/Frc;

    invoke-direct {v4, p0, p2, p1}, LX/Frc;-><init>(LX/Frd;Ljava/lang/String;Ljava/lang/String;)V

    .line 2295910
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 2295911
    const-string v9, "operationParams"

    invoke-virtual {v8, v9, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2295912
    iget-object v9, v2, LX/Fs2;->b:LX/9lP;

    .line 2295913
    iget-object v10, v9, LX/9lP;->a:Ljava/lang/String;

    move-object v9, v10

    .line 2295914
    invoke-static {v9}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 2295915
    const-string v9, "profileId"

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2295916
    move-object v5, v8

    .line 2295917
    iget-object v6, v2, LX/Fs2;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 2295918
    iget-object v7, v2, LX/Fs2;->e:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/0aG;

    const v8, 0x7ac6f5df

    invoke-static {v7, v3, v5, v6, v8}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v7

    invoke-interface {v7}, LX/1MF;->start()LX/1ML;

    move-result-object v7

    move-object v5, v7

    .line 2295919
    iget-object v6, v2, LX/Fs2;->f:LX/1dy;

    invoke-virtual {v6, v5, v1}, LX/1dy;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0jT;)V

    .line 2295920
    iget-object v6, v2, LX/Fs2;->h:Ljava/util/concurrent/Executor;

    invoke-static {v5, v4, v6}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2295921
    iget-object v6, v2, LX/Fs2;->g:Ljava/util/Set;

    invoke-interface {v6, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2295922
    new-instance v6, LX/Frz;

    invoke-direct {v6, v2, v5}, LX/Frz;-><init>(LX/Fs2;Lcom/google/common/util/concurrent/ListenableFuture;)V

    iget-object v7, v2, LX/Fs2;->h:Ljava/util/concurrent/Executor;

    invoke-static {v5, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2295923
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2295893
    invoke-static {p0}, LX/Frd;->g(LX/Frd;)LX/Fs2;

    move-result-object v0

    invoke-virtual {v0}, LX/Fs2;->a()V

    .line 2295894
    iget-object v0, p0, LX/Frd;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zi;

    .line 2295895
    invoke-virtual {v0}, LX/0zi;->a()V

    goto :goto_0

    .line 2295896
    :cond_0
    iget-object v0, p0, LX/Frd;->t:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2295897
    return-void
.end method

.method public final b(LX/FsJ;)V
    .locals 13

    .prologue
    .line 2295890
    iget-object v1, p0, LX/Frd;->m:LX/Frv;

    iget-object v3, p0, LX/Frd;->i:LX/G4m;

    iget-object v4, p0, LX/Frd;->j:LX/G1N;

    iget-object v5, p0, LX/Frd;->g:LX/G1I;

    iget-wide v6, p0, LX/Frd;->r:J

    iget-object v8, p0, LX/Frd;->f:LX/G4x;

    iget-object v9, p0, LX/Frd;->c:LX/5SB;

    iget-object v10, p0, LX/Frd;->h:LX/G2n;

    iget-object v11, p0, LX/Frd;->b:LX/FqO;

    iget-object v12, p0, LX/Frd;->s:Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;

    move-object v2, p1

    invoke-virtual/range {v1 .. v12}, LX/Frv;->a(LX/FsJ;LX/G4m;LX/G1N;LX/G1I;JLX/G4x;LX/5SB;LX/G2n;LX/FqO;Lcom/facebook/timeline/datafetcher/TimelineViewCallbackUtil;)LX/0Px;

    move-result-object v0

    .line 2295891
    iget-object v1, p0, LX/Frd;->t:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2295892
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2295879
    invoke-static {p0}, LX/Frd;->g(LX/Frd;)LX/Fs2;

    move-result-object v0

    .line 2295880
    iget-object v1, v0, LX/Fs2;->i:LX/Fs1;

    move-object v0, v1

    .line 2295881
    sget-object v1, LX/Fs1;->VISIBLE:LX/Fs1;

    if-ne v0, v1, :cond_0

    .line 2295882
    invoke-static {p0}, LX/Frd;->g(LX/Frd;)LX/Fs2;

    move-result-object v0

    invoke-virtual {v0}, LX/Fs2;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2295883
    invoke-static {p0}, LX/Frd;->g(LX/Frd;)LX/Fs2;

    move-result-object v0

    sget-object v1, LX/Fs1;->PAUSED:LX/Fs1;

    .line 2295884
    iput-object v1, v0, LX/Fs2;->i:LX/Fs1;

    .line 2295885
    :cond_0
    :goto_0
    return-void

    .line 2295886
    :cond_1
    invoke-static {p0}, LX/Frd;->g(LX/Frd;)LX/Fs2;

    move-result-object v0

    invoke-virtual {v0}, LX/Fs2;->a()V

    .line 2295887
    invoke-static {p0}, LX/Frd;->g(LX/Frd;)LX/Fs2;

    move-result-object v0

    sget-object v1, LX/Fs1;->CANCELLED:LX/Fs1;

    .line 2295888
    iput-object v1, v0, LX/Fs2;->i:LX/Fs1;

    .line 2295889
    goto :goto_0
.end method

.method public final e()LX/FsJ;
    .locals 1

    .prologue
    .line 2295878
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Frd;->a(Z)LX/FsJ;

    move-result-object v0

    return-object v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2295875
    iget-object v0, p0, LX/Frd;->o:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G4i;

    invoke-virtual {v0}, LX/G4i;->a()V

    .line 2295876
    iget-object v0, p0, LX/Frd;->b:LX/FqO;

    invoke-interface {v0}, LX/FqO;->lo_()V

    .line 2295877
    return-void
.end method
