.class public LX/H1X;
.super LX/1OM;
.source ""

# interfaces
.implements Landroid/widget/Filterable;


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/H0v;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/H0v;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/H1W;

.field public d:Ljava/lang/String;

.field public e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "LX/H0v;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2415452
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2415453
    new-instance v0, LX/H1W;

    invoke-direct {v0, p0}, LX/H1W;-><init>(LX/H1X;)V

    iput-object v0, p0, LX/H1X;->c:LX/H1W;

    .line 2415454
    const-string v0, ""

    iput-object v0, p0, LX/H1X;->d:Ljava/lang/String;

    .line 2415455
    iput-object p1, p0, LX/H1X;->e:Landroid/content/Context;

    .line 2415456
    invoke-virtual {p0, p2}, LX/H1X;->a(Ljava/util/List;)V

    .line 2415457
    return-void
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2415474
    int-to-long v0, p1

    return-wide v0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 2

    .prologue
    .line 2415464
    new-instance v0, Lcom/facebook/fig/listitem/FigListItem;

    iget-object v1, p0, LX/H1X;->e:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;)V

    .line 2415465
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setBodyTextAppearenceType(I)V

    .line 2415466
    new-instance v1, LX/62U;

    invoke-direct {v1, v0}, LX/62U;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2415467
    iget-object v0, p1, LX/1a1;->a:Landroid/view/View;

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    .line 2415468
    iget-object v1, p0, LX/H1X;->b:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/H0v;

    .line 2415469
    iget-object v2, p0, LX/H1X;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/H0v;->c(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2415470
    iget-object v2, p0, LX/H1X;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/H0v;->d(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    .line 2415471
    iget-object v2, p0, LX/H1X;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/H0v;->e(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2415472
    new-instance v2, LX/H1V;

    invoke-direct {v2, p0, v1}, LX/H1V;-><init>(LX/H1X;LX/H0v;)V

    invoke-virtual {v0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2415473
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/H0v;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2415460
    iput-object p1, p0, LX/H1X;->a:Ljava/util/List;

    .line 2415461
    iput-object p1, p0, LX/H1X;->b:Ljava/util/List;

    .line 2415462
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2415463
    return-void
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 2415459
    iget-object v0, p0, LX/H1X;->c:LX/H1W;

    return-object v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2415458
    iget-object v0, p0, LX/H1X;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
