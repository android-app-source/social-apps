.class public LX/Gzp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2412453
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2412454
    iput-object p1, p0, LX/Gzp;->a:Landroid/content/Context;

    .line 2412455
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 6

    .prologue
    .line 2412456
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2412457
    const-string v1, "arg_page_id"

    const-string v2, "com.facebook.katana.profile.id"

    const-wide/16 v4, -0x1

    invoke-virtual {p1, v2, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2412458
    const-string v1, "extra_should_use_client_default_get_quote_description"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2412459
    const-string v1, "arg_get_quote_description"

    iget-object v2, p0, LX/Gzp;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081525

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2412460
    :cond_0
    new-instance v1, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    invoke-direct {v1}, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;-><init>()V

    .line 2412461
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2412462
    return-object v1
.end method
