.class public final LX/FPF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FPE;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/FPI;


# direct methods
.method public constructor <init>(LX/FPI;I)V
    .locals 0

    .prologue
    .line 2236858
    iput-object p1, p0, LX/FPF;->b:LX/FPI;

    iput p2, p0, LX/FPF;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2236859
    iget-object v0, p0, LX/FPF;->b:LX/FPI;

    iget-object v0, v0, LX/FPI;->d:LX/FPU;

    if-nez v0, :cond_0

    .line 2236860
    :goto_0
    return-void

    .line 2236861
    :cond_0
    iget-object v0, p0, LX/FPF;->b:LX/FPI;

    iget-object v0, v0, LX/FPI;->d:LX/FPU;

    iget v1, p0, LX/FPF;->a:I

    invoke-virtual {v0, v1}, LX/FPU;->a(I)V

    goto :goto_0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 2236862
    iget-object v0, p0, LX/FPF;->b:LX/FPI;

    iget-object v0, v0, LX/FPI;->d:LX/FPU;

    if-nez v0, :cond_0

    .line 2236863
    :goto_0
    return-void

    .line 2236864
    :cond_0
    iget-object v0, p0, LX/FPF;->b:LX/FPI;

    iget-object v0, v0, LX/FPI;->d:LX/FPU;

    iget v1, p0, LX/FPF;->a:I

    invoke-virtual {v0, v1, p1}, LX/FPU;->a(II)V

    goto :goto_0
.end method

.method public final a(LX/FQN;)V
    .locals 12

    .prologue
    .line 2236865
    iget-object v0, p0, LX/FPF;->b:LX/FPI;

    iget-object v0, v0, LX/FPI;->d:LX/FPU;

    if-nez v0, :cond_0

    .line 2236866
    :goto_0
    return-void

    .line 2236867
    :cond_0
    iget-object v0, p0, LX/FPF;->b:LX/FPI;

    iget-object v0, v0, LX/FPI;->d:LX/FPU;

    iget v1, p0, LX/FPF;->a:I

    const/4 v5, 0x0

    .line 2236868
    if-ltz v1, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2236869
    iget-object v2, v0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-static {v2, v1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->d(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;I)Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    move-result-object v4

    .line 2236870
    if-nez v4, :cond_2

    .line 2236871
    :goto_2
    goto :goto_0

    .line 2236872
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 2236873
    :cond_2
    sget-object v2, LX/FPV;->a:[I

    invoke-virtual {p1}, LX/FQN;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2236874
    iget-object v2, v0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    iget-object v2, v2, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->b:LX/FPr;

    iget-object v3, v0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v6, v0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    iget-object v6, v6, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->i:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    iget-object v7, v0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    sget-object v8, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->e:Ljava/lang/Class;

    invoke-virtual/range {v2 .. v8}, LX/FPr;->a(Landroid/content/Context;Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Landroid/location/Location;Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;Landroid/support/v4/app/Fragment;Ljava/lang/Class;)V

    .line 2236875
    sget-object v9, LX/FPZ;->CELL:LX/FPZ;

    .line 2236876
    :goto_3
    iget-object v6, v0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    move v7, v1

    move-object v8, v4

    move-object v10, v5

    move-object v11, v5

    invoke-static/range {v6 .. v11}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->a(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;ILcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;LX/FPZ;Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto :goto_2

    .line 2236877
    :pswitch_0
    iget-object v2, v0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    iget-object v2, v2, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->b:LX/FPr;

    iget-object v3, v0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v6, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->e:Ljava/lang/Class;

    invoke-virtual {v2, v4, v3, v6}, LX/FPr;->b(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Landroid/content/Context;Ljava/lang/Class;)V

    .line 2236878
    sget-object v9, LX/FPZ;->DISTANCE:LX/FPZ;

    goto :goto_3

    .line 2236879
    :pswitch_1
    iget-object v2, v0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-static {v2, v4}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->a$redex0(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;)V

    .line 2236880
    sget-object v9, LX/FPZ;->LIKES:LX/FPZ;

    goto :goto_3

    .line 2236881
    :pswitch_2
    iget-object v2, v0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    iget-object v2, v2, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->b:LX/FPr;

    iget-object v3, v0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v6, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->e:Ljava/lang/Class;

    invoke-virtual {v2, v4, v3, v6}, LX/FPr;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Landroid/content/Context;Ljava/lang/Class;)V

    .line 2236882
    sget-object v9, LX/FPZ;->REVIEW_RATING:LX/FPZ;

    goto :goto_3

    .line 2236883
    :pswitch_3
    iget-object v2, v0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    iget-object v2, v2, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->b:LX/FPr;

    invoke-virtual {v4}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v6, v0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-virtual {v6}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    sget-object v7, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->e:Ljava/lang/Class;

    invoke-virtual {v2, v3, v6, v7}, LX/FPr;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/Class;)V

    .line 2236884
    sget-object v9, LX/FPZ;->OPEN_NOW:LX/FPZ;

    goto :goto_3

    .line 2236885
    :pswitch_4
    iget-object v2, v0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    iget-object v2, v2, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->b:LX/FPr;

    invoke-virtual {v4}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v6, v0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-virtual {v6}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    sget-object v7, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->e:Ljava/lang/Class;

    invoke-virtual {v2, v3, v6, v7}, LX/FPr;->a(Ljava/lang/String;Landroid/content/Context;Ljava/lang/Class;)V

    .line 2236886
    sget-object v9, LX/FPZ;->PRICE_RATING:LX/FPZ;

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2236887
    iget-object v0, p0, LX/FPF;->b:LX/FPI;

    iget-object v0, v0, LX/FPI;->d:LX/FPU;

    if-nez v0, :cond_0

    .line 2236888
    :goto_0
    return-void

    .line 2236889
    :cond_0
    iget-object v0, p0, LX/FPF;->b:LX/FPI;

    iget-object v0, v0, LX/FPI;->d:LX/FPU;

    iget v1, p0, LX/FPF;->a:I

    .line 2236890
    iget-object v2, v0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-static {v2, v1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->d(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;I)Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    move-result-object v2

    .line 2236891
    if-nez v2, :cond_1

    .line 2236892
    :goto_1
    goto :goto_0

    .line 2236893
    :cond_1
    iget-object p0, v0, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-static {p0, v2}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->a$redex0(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;)V

    goto :goto_1
.end method
