.class public final LX/F6q;
.super LX/0Vd;
.source ""


# instance fields
.field public final synthetic a:LX/F6r;


# direct methods
.method public constructor <init>(LX/F6r;)V
    .locals 0

    .prologue
    .line 2200906
    iput-object p1, p0, LX/F6q;->a:LX/F6r;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2200907
    iget-object v0, p0, LX/F6q;->a:LX/F6r;

    iget-object v0, v0, LX/F6r;->a:LX/F6t;

    iget-object v0, v0, LX/F6t;->b:LX/30Y;

    invoke-virtual {v0}, LX/30Y;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2200908
    iget-object v0, p0, LX/F6q;->a:LX/F6r;

    iget-object v0, v0, LX/F6r;->a:LX/F6t;

    iget-object v0, v0, LX/F6t;->d:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-virtual {v0, v3}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->a(Z)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2200909
    iget-object v0, p0, LX/F6q;->a:LX/F6r;

    iget-object v0, v0, LX/F6r;->a:LX/F6t;

    iget-object v0, v0, LX/F6t;->f:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0833bf

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2200910
    :cond_0
    iget-object v0, p0, LX/F6q;->a:LX/F6r;

    iget-object v0, v0, LX/F6r;->a:LX/F6t;

    invoke-virtual {v0, v3}, LX/F6t;->setEnabled(Z)V

    .line 2200911
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2200912
    iget-object v0, p0, LX/F6q;->a:LX/F6r;

    iget-object v0, v0, LX/F6r;->a:LX/F6t;

    iget-object v0, v0, LX/F6t;->b:LX/30Y;

    invoke-virtual {v0}, LX/30Y;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2200913
    iget-object v0, p0, LX/F6q;->a:LX/F6r;

    iget-object v0, v0, LX/F6r;->a:LX/F6t;

    iget-object v0, v0, LX/F6t;->f:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0833be

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2200914
    :cond_0
    iget-object v0, p0, LX/F6q;->a:LX/F6r;

    iget-object v0, v0, LX/F6r;->a:LX/F6t;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/F6t;->setEnabled(Z)V

    .line 2200915
    return-void
.end method
