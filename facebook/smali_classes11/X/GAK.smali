.class public final LX/GAK;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "LX/GAb;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:Ljava/util/concurrent/Executor;

.field private static volatile d:Ljava/lang/String;

.field private static volatile e:Ljava/lang/String;

.field public static volatile f:Ljava/lang/String;

.field public static volatile g:I

.field public static volatile h:Ljava/lang/String;

.field private static i:Ljava/util/concurrent/atomic/AtomicLong;

.field public static volatile j:Z

.field public static k:Z

.field private static l:LX/GsM;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/GsM",
            "<",
            "Ljava/io/File;",
            ">;"
        }
    .end annotation
.end field

.field public static m:Landroid/content/Context;

.field public static n:I

.field private static final o:Ljava/lang/Object;

.field private static final p:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private static final q:Ljava/util/concurrent/ThreadFactory;

.field private static r:Ljava/lang/Boolean;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2325022
    const-class v0, LX/GAK;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/GAK;->a:Ljava/lang/String;

    .line 2325023
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x1

    new-array v1, v1, [LX/GAb;

    sget-object v2, LX/GAb;->DEVELOPER_ERRORS:LX/GAb;

    aput-object v2, v1, v4

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, LX/GAK;->b:Ljava/util/HashSet;

    .line 2325024
    const-string v0, "facebook.com"

    sput-object v0, LX/GAK;->h:Ljava/lang/String;

    .line 2325025
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/32 v2, 0x10000

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, LX/GAK;->i:Ljava/util/concurrent/atomic/AtomicLong;

    .line 2325026
    const/16 v0, 0x0

    sput-boolean v0, LX/GAK;->j:Z

    .line 2325027
    sput-boolean v4, LX/GAK;->k:Z

    .line 2325028
    const v0, 0xface

    sput v0, LX/GAK;->n:I

    .line 2325029
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/GAK;->o:Ljava/lang/Object;

    .line 2325030
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    sput-object v0, LX/GAK;->p:Ljava/util/concurrent/BlockingQueue;

    .line 2325031
    new-instance v0, LX/GAG;

    invoke-direct {v0}, LX/GAG;-><init>()V

    sput-object v0, LX/GAK;->q:Ljava/util/concurrent/ThreadFactory;

    .line 2325032
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, LX/GAK;->r:Ljava/lang/Boolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2325020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2325021
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2325017
    const-class v0, LX/GAK;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0, v1}, LX/GAK;->a(Landroid/content/Context;LX/GAJ;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2325018
    monitor-exit v0

    return-void

    .line 2325019
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static declared-synchronized a(Landroid/content/Context;LX/GAJ;)V
    .locals 6

    .prologue
    .line 2324950
    const-class v1, LX/GAK;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/GAK;->r:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 2324951
    :goto_0
    monitor-exit v1

    return-void

    .line 2324952
    :cond_0
    :try_start_1
    const-string v0, "applicationContext"

    invoke-static {p0, v0}, LX/Gsd;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2324953
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/Gsd;->b(Landroid/content/Context;Z)V

    .line 2324954
    const/4 v0, 0x0

    invoke-static {p0, v0}, LX/Gsd;->a(Landroid/content/Context;Z)V

    .line 2324955
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 2324956
    sput-object v0, LX/GAK;->m:Landroid/content/Context;

    invoke-static {v0}, LX/GAK;->c(Landroid/content/Context;)V

    .line 2324957
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, LX/GAK;->r:Ljava/lang/Boolean;

    .line 2324958
    sget-object v0, LX/GAK;->m:Landroid/content/Context;

    sget-object v2, LX/GAK;->d:Ljava/lang/String;

    const/4 p0, 0x1

    const/4 v5, 0x0

    .line 2324959
    sget-object v3, LX/Gsc;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v3, v5, p0}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z

    move-result v3

    .line 2324960
    invoke-static {v2}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    sget-object v4, LX/Gsc;->b:Ljava/util/Map;

    invoke-interface {v4, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    if-nez v3, :cond_2

    .line 2324961
    :cond_1
    :goto_1
    invoke-static {}, LX/GsS;->b()V

    .line 2324962
    sget-object v0, LX/GAK;->m:Landroid/content/Context;

    .line 2324963
    sget-object v2, LX/Grz;->a:LX/Grz;

    if-eqz v2, :cond_3

    .line 2324964
    :goto_2
    new-instance v0, LX/GsM;

    new-instance v2, LX/GAH;

    invoke-direct {v2}, LX/GAH;-><init>()V

    invoke-direct {v0, v2}, LX/GsM;-><init>(Ljava/util/concurrent/Callable;)V

    sput-object v0, LX/GAK;->l:LX/GsM;

    .line 2324965
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v2, LX/GAI;

    invoke-direct {v2, p1}, LX/GAI;-><init>(LX/GAJ;)V

    invoke-direct {v0, v2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 2324966
    invoke-static {}, LX/GAK;->d()Ljava/util/concurrent/Executor;

    move-result-object v2

    const v3, 0x3eeafca4

    invoke-static {v2, v0, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2324967
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 2324968
    :cond_2
    :try_start_2
    const-string v3, "com.facebook.internal.APP_SETTINGS.%s"

    new-array v4, p0, [Ljava/lang/Object;

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2324969
    invoke-static {}, LX/GAK;->d()Ljava/util/concurrent/Executor;

    move-result-object v4

    new-instance v5, Lcom/facebook/internal/Utility$1;

    invoke-direct {v5, v0, v3, v2}, Lcom/facebook/internal/Utility$1;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const v3, -0x38eb1d3b

    invoke-static {v4, v5, v3}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    goto :goto_1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2324970
    :cond_3
    new-instance v2, LX/Grz;

    invoke-direct {v2, v0}, LX/Grz;-><init>(Landroid/content/Context;)V

    .line 2324971
    sput-object v2, LX/Grz;->a:LX/Grz;

    .line 2324972
    iget-object v3, v2, LX/Grz;->b:Landroid/content/Context;

    invoke-static {v3}, LX/0Xw;->a(Landroid/content/Context;)LX/0Xw;

    move-result-object v3

    .line 2324973
    new-instance v4, Landroid/content/IntentFilter;

    const-string v0, "com.parse.bolts.measurement_event"

    invoke-direct {v4, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2, v4}, LX/0Xw;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 2324974
    goto :goto_2
.end method

.method public static declared-synchronized a()Z
    .locals 2

    .prologue
    .line 2325016
    const-class v1, LX/GAK;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/GAK;->r:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(LX/GAb;)Z
    .locals 2

    .prologue
    .line 2325012
    sget-object v1, LX/GAK;->b:Ljava/util/HashSet;

    monitor-enter v1

    .line 2325013
    :try_start_0
    sget-boolean v0, LX/GAK;->j:Z

    move v0, v0

    .line 2325014
    if-eqz v0, :cond_0

    sget-object v0, LX/GAK;->b:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2325015
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static c(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 2324989
    if-nez p0, :cond_1

    .line 2324990
    :cond_0
    :goto_0
    return-void

    .line 2324991
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2324992
    if-eqz v1, :cond_0

    iget-object v0, v1, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 2324993
    sget-object v0, LX/GAK;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 2324994
    iget-object v0, v1, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    const-string v2, "com.facebook.sdk.ApplicationId"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 2324995
    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 2324996
    check-cast v0, Ljava/lang/String;

    .line 2324997
    sget-object v2, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-virtual {v0, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "fb"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2324998
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/GAK;->d:Ljava/lang/String;

    .line 2324999
    :cond_2
    :goto_1
    sget-object v0, LX/GAK;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 2325000
    iget-object v0, v1, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    const-string v2, "com.facebook.sdk.ApplicationName"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/GAK;->e:Ljava/lang/String;

    .line 2325001
    :cond_3
    sget-object v0, LX/GAK;->f:Ljava/lang/String;

    if-nez v0, :cond_4

    .line 2325002
    iget-object v0, v1, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    const-string v2, "com.facebook.sdk.ClientToken"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/GAK;->f:Ljava/lang/String;

    .line 2325003
    :cond_4
    sget v0, LX/GAK;->g:I

    if-nez v0, :cond_0

    .line 2325004
    iget-object v0, v1, Landroid/content/pm/PackageItemInfo;->metaData:Landroid/os/Bundle;

    const-string v1, "com.facebook.sdk.WebDialogTheme"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 2325005
    if-eqz v0, :cond_7

    :goto_2
    sput v0, LX/GAK;->g:I

    .line 2325006
    goto :goto_0

    .line 2325007
    :cond_5
    sput-object v0, LX/GAK;->d:Ljava/lang/String;

    goto :goto_1

    .line 2325008
    :cond_6
    instance-of v0, v0, Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2325009
    new-instance v0, LX/GAA;

    const-string v1, "App Ids cannot be directly placed in the manifest.They must be prefixed by \'fb\' or be placed in the string resource file."

    invoke-direct {v0, v1}, LX/GAA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2325010
    :catch_0
    goto :goto_0

    .line 2325011
    :cond_7
    const v0, 0x1030010

    goto :goto_2
.end method

.method public static d()Ljava/util/concurrent/Executor;
    .locals 2

    .prologue
    .line 2324983
    sget-object v1, LX/GAK;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 2324984
    :try_start_0
    sget-object v0, LX/GAK;->c:Ljava/util/concurrent/Executor;

    if-nez v0, :cond_0

    .line 2324985
    sget-object v0, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    sput-object v0, LX/GAK;->c:Ljava/util/concurrent/Executor;

    .line 2324986
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2324987
    sget-object v0, LX/GAK;->c:Ljava/util/concurrent/Executor;

    return-object v0

    .line 2324988
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static f()Landroid/content/Context;
    .locals 1

    .prologue
    .line 2324981
    invoke-static {}, LX/Gsd;->a()V

    .line 2324982
    sget-object v0, LX/GAK;->m:Landroid/content/Context;

    return-object v0
.end method

.method public static h()J
    .locals 2

    .prologue
    .line 2324979
    invoke-static {}, LX/Gsd;->a()V

    .line 2324980
    sget-object v0, LX/GAK;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public static i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2324977
    invoke-static {}, LX/Gsd;->a()V

    .line 2324978
    sget-object v0, LX/GAK;->d:Ljava/lang/String;

    return-object v0
.end method

.method public static k()I
    .locals 1

    .prologue
    .line 2324975
    invoke-static {}, LX/Gsd;->a()V

    .line 2324976
    sget v0, LX/GAK;->g:I

    return v0
.end method
