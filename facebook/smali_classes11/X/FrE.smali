.class public final LX/FrE;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/timeline/protocol/FetchTimelineContextItemsGraphQLModels$TimelineContextItemsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FrF;


# direct methods
.method public constructor <init>(LX/FrF;)V
    .locals 0

    .prologue
    .line 2295221
    iput-object p1, p0, LX/FrE;->a:LX/FrF;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2295222
    iget-object v0, p0, LX/FrE;->a:LX/FrF;

    iget-object v0, v0, LX/FrF;->g:LX/FrH;

    invoke-virtual {v0}, LX/BPB;->h()V

    .line 2295223
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2295224
    check-cast p1, Lcom/facebook/timeline/protocol/FetchTimelineContextItemsGraphQLModels$TimelineContextItemsModel;

    .line 2295225
    iget-object v0, p0, LX/FrE;->a:LX/FrF;

    iget-object v0, v0, LX/FrF;->g:LX/FrH;

    invoke-virtual {v0}, LX/BPB;->h()V

    .line 2295226
    new-instance v0, LX/Fr5;

    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelineContextItemsGraphQLModels$TimelineContextItemsModel;->a()LX/0Px;

    move-result-object v1

    new-instance v2, LX/5wW;

    invoke-direct {v2}, LX/5wW;-><init>()V

    const/4 v3, 0x0

    .line 2295227
    iput-boolean v3, v2, LX/5wW;->a:Z

    .line 2295228
    move-object v2, v2

    .line 2295229
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 2295230
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 2295231
    invoke-virtual {v4, v8}, LX/186;->c(I)V

    .line 2295232
    iget-boolean v5, v2, LX/5wW;->a:Z

    invoke-virtual {v4, v7, v5}, LX/186;->a(IZ)V

    .line 2295233
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 2295234
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 2295235
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 2295236
    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2295237
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2295238
    new-instance v5, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextItemsPageInfoModel;

    invoke-direct {v5, v4}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextItemsPageInfoModel;-><init>(LX/15i;)V

    .line 2295239
    move-object v2, v5

    .line 2295240
    invoke-direct {v0, v1, v2}, LX/Fr5;-><init>(LX/0Px;Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextItemsPageInfoModel;)V

    .line 2295241
    iget-object v1, p0, LX/FrE;->a:LX/FrF;

    iget-object v1, v1, LX/FrF;->g:LX/FrH;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, LX/FrH;->a(LX/Fr5;I)V

    .line 2295242
    iget-object v0, p0, LX/FrE;->a:LX/FrF;

    iget-object v0, v0, LX/FrF;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BPp;

    new-instance v1, LX/BPI;

    invoke-direct {v1}, LX/BPI;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2295243
    return-void
.end method
