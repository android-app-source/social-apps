.class public final LX/GUK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GUn;

.field public final synthetic b:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;LX/GUn;)V
    .locals 0

    .prologue
    .line 2357352
    iput-object p1, p0, LX/GUK;->b:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iput-object p2, p0, LX/GUK;->a:LX/GUn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/backgroundlocation/settings/graphql/BackgroundLocationSettingsGraphQLModels$LocationSharingPrivacyOptionsQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2357349
    iget-object v0, p0, LX/GUK;->a:LX/GUn;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2357350
    iget-object v1, p0, LX/GUK;->b:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->r:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2357351
    invoke-direct {p0}, LX/GUK;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
