.class public LX/Fqq;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field public a:LX/Fql;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2294569
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2294570
    const-class v0, LX/Fqq;

    invoke-static {v0, p0}, LX/Fqq;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2294571
    const v0, 0x7f0312e1

    invoke-static {p1, v0, p0}, LX/Fqq;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2294572
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0dd2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2294573
    invoke-virtual {p0, v0, v0, v0, v0}, LX/Fqq;->setPadding(IIII)V

    .line 2294574
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Fqq;->setOrientation(I)V

    .line 2294575
    const v0, 0x7f0d245c

    invoke-static {p0, v0}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Fqq;->b:Landroid/widget/TextView;

    .line 2294576
    iget-object v0, p0, LX/Fqq;->a:LX/Fql;

    invoke-virtual {v0, p0}, LX/0ht;->d(Landroid/view/View;)V

    .line 2294577
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Fqq;

    invoke-static {p0}, LX/Fql;->b(LX/0QB;)LX/Fql;

    move-result-object p0

    check-cast p0, LX/Fql;

    iput-object p0, p1, LX/Fqq;->a:LX/Fql;

    return-void
.end method


# virtual methods
.method public setSeeFirstUserName(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2294578
    iget-object v0, p0, LX/Fqq;->b:Landroid/widget/TextView;

    iget-object v1, p0, LX/Fqq;->b:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2294579
    const p0, 0x7f081586

    invoke-virtual {v1, p0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    move-object v1, p0

    .line 2294580
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2294581
    return-void
.end method
