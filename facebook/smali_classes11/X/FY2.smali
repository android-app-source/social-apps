.class public LX/FY2;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/16H;

.field public final b:LX/FXz;

.field public final c:LX/FY5;

.field private final d:LX/FY9;

.field private final e:LX/FYB;

.field public final f:LX/FYG;

.field public final g:LX/FYI;

.field public final h:LX/FYK;

.field public final i:LX/FYE;

.field public final j:LX/0tQ;


# direct methods
.method public constructor <init>(LX/0tQ;LX/16H;LX/FXz;LX/FY9;LX/FYG;LX/FYI;LX/FYK;LX/FYB;LX/FY5;LX/FYE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2255725
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2255726
    iput-object p1, p0, LX/FY2;->j:LX/0tQ;

    .line 2255727
    iput-object p2, p0, LX/FY2;->a:LX/16H;

    .line 2255728
    iput-object p3, p0, LX/FY2;->b:LX/FXz;

    .line 2255729
    iput-object p4, p0, LX/FY2;->d:LX/FY9;

    .line 2255730
    iput-object p5, p0, LX/FY2;->f:LX/FYG;

    .line 2255731
    iput-object p6, p0, LX/FY2;->g:LX/FYI;

    .line 2255732
    iput-object p7, p0, LX/FY2;->h:LX/FYK;

    .line 2255733
    iput-object p10, p0, LX/FY2;->i:LX/FYE;

    .line 2255734
    iput-object p8, p0, LX/FY2;->e:LX/FYB;

    .line 2255735
    iput-object p9, p0, LX/FY2;->c:LX/FY5;

    .line 2255736
    return-void
.end method

.method public static a(Ljava/util/ArrayList;LX/BO1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/FXy;",
            ">;",
            "LX/BO1;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2255717
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 2255718
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2255719
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FXy;

    .line 2255720
    invoke-virtual {v0, p1}, LX/FXy;->a(LX/BO1;)LX/FXy;

    move-result-object v0

    .line 2255721
    iget-boolean p0, v0, LX/FXy;->a:Z

    move v0, p0

    .line 2255722
    if-nez v0, :cond_0

    .line 2255723
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 2255724
    :cond_1
    return-void
.end method

.method public static b(LX/FY2;Landroid/support/v4/app/FragmentActivity;LX/BO1;Landroid/view/View;)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v4/app/FragmentActivity;",
            "LX/BO1;",
            "Landroid/view/View;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "LX/FXy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2255697
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2255698
    iget-object v1, p0, LX/FY2;->e:LX/FYB;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2255699
    iget-object v1, p0, LX/FY2;->c:LX/FY5;

    invoke-virtual {v1, p3}, LX/FY5;->a(Landroid/view/View;)LX/FY4;

    move-result-object v1

    .line 2255700
    iget-object v2, v1, LX/FY4;->a:LX/19w;

    invoke-interface {p2}, LX/BO1;->f()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v2, p3}, LX/19w;->b(Ljava/lang/String;)Z

    move-result v2

    move v2, v2

    .line 2255701
    if-eqz v2, :cond_0

    .line 2255702
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2255703
    iget-object v1, p0, LX/FY2;->b:LX/FXz;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2255704
    iget-object v1, p0, LX/FY2;->g:LX/FYI;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2255705
    :goto_0
    iget-object v1, p0, LX/FY2;->d:LX/FY9;

    .line 2255706
    new-instance v3, LX/FY8;

    invoke-static {v1}, LX/BUA;->a(LX/0QB;)LX/BUA;

    move-result-object v5

    check-cast v5, LX/BUA;

    invoke-static {v1}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v6

    check-cast v6, LX/0tQ;

    invoke-static {v1}, LX/19w;->a(LX/0QB;)LX/19w;

    move-result-object v7

    check-cast v7, LX/19w;

    invoke-static {v1}, LX/BYZ;->b(LX/0QB;)LX/BYZ;

    move-result-object v8

    check-cast v8, LX/BYZ;

    move-object v4, p1

    invoke-direct/range {v3 .. v8}, LX/FY8;-><init>(Landroid/support/v4/app/FragmentActivity;LX/BUA;LX/0tQ;LX/19w;LX/BYZ;)V

    .line 2255707
    move-object v1, v3

    .line 2255708
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2255709
    iget-object v1, p0, LX/FY2;->h:LX/FYK;

    invoke-virtual {v1, p1}, LX/FYK;->a(Landroid/content/Context;)LX/FYJ;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2255710
    iget-object v1, p0, LX/FY2;->f:LX/FYG;

    invoke-virtual {v1, p1}, LX/FYG;->a(Landroid/support/v4/app/FragmentActivity;)LX/FYF;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2255711
    iget-object v1, p0, LX/FY2;->i:LX/FYE;

    invoke-virtual {v1, p1}, LX/FYE;->a(Landroid/support/v4/app/FragmentActivity;)LX/FYD;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2255712
    invoke-static {v0, p2}, LX/FY2;->a(Ljava/util/ArrayList;LX/BO1;)V

    .line 2255713
    return-object v0

    .line 2255714
    :cond_0
    iget-object v2, p0, LX/FY2;->b:LX/FXz;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2255715
    iget-object v2, p0, LX/FY2;->g:LX/FYI;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2255716
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/FragmentActivity;LX/BO1;Ljava/lang/String;Landroid/view/View;)LX/3Af;
    .locals 8
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param

    .prologue
    .line 2255655
    new-instance v2, LX/3Af;

    invoke-direct {v2, p1}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 2255656
    new-instance v3, LX/7TY;

    invoke-direct {v3, p1}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 2255657
    iget-object v0, p0, LX/FY2;->j:LX/0tQ;

    invoke-virtual {v0}, LX/0tQ;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2255658
    invoke-static {p0, p1, p2, p4}, LX/FY2;->b(LX/FY2;Landroid/support/v4/app/FragmentActivity;LX/BO1;Landroid/view/View;)Ljava/util/ArrayList;

    move-result-object v0

    .line 2255659
    :goto_0
    move-object v4, v0

    .line 2255660
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2255661
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2255662
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FXy;

    .line 2255663
    invoke-virtual {v0, p1}, LX/FXy;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v5

    .line 2255664
    invoke-virtual {v0}, LX/FXy;->a()I

    move-result v6

    invoke-virtual {v5, v6}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2255665
    new-instance v6, LX/FY1;

    iget-object v7, p0, LX/FY2;->a:LX/16H;

    invoke-direct {v6, v0, v7}, LX/FY1;-><init>(LX/FXy;LX/16H;)V

    .line 2255666
    invoke-interface {p2}, LX/AU0;->a()Landroid/database/Cursor;

    move-result-object v7

    instance-of v7, v7, Landroid/database/MatrixCursor;

    if-nez v7, :cond_0

    .line 2255667
    sget-object v7, LX/FY0;->a:Ljava/lang/String;

    const-string p4, "This is not a frozenDao! Please freeze before passing in!"

    invoke-static {v7, p4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2255668
    :cond_0
    iput-object p2, v6, LX/FY0;->b:LX/BO1;

    .line 2255669
    move-object v6, v6

    .line 2255670
    invoke-virtual {v5, v6}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2255671
    invoke-virtual {v0, p1}, LX/FXy;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 2255672
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 2255673
    invoke-virtual {v5, v0}, LX/3Ai;->a(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 2255674
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2255675
    :cond_2
    invoke-virtual {v2, v3}, LX/3Af;->a(LX/1OM;)V

    .line 2255676
    invoke-virtual {v2}, LX/3Af;->show()V

    .line 2255677
    invoke-interface {p2}, LX/BO1;->f()Ljava/lang/String;

    move-result-object v0

    .line 2255678
    iget-object v1, p0, LX/FY2;->a:LX/16H;

    const-string v3, "saved_dashboard"

    .line 2255679
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 2255680
    new-instance p0, Ljava/util/ArrayList;

    invoke-direct {p0, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 2255681
    const/4 v5, 0x0

    move v6, v5

    :goto_2
    if-ge v6, v7, :cond_3

    .line 2255682
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/FXy;

    invoke-virtual {v5}, LX/FXy;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2255683
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_2

    .line 2255684
    :cond_3
    move-object v5, p0

    .line 2255685
    invoke-virtual {v1, v3, v0, v5, p3}, LX/16H;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    .line 2255686
    :cond_4
    return-object v2

    .line 2255687
    :cond_5
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2255688
    iget-object v1, p0, LX/FY2;->f:LX/FYG;

    invoke-virtual {v1, p1}, LX/FYG;->a(Landroid/support/v4/app/FragmentActivity;)LX/FYF;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2255689
    iget-object v1, p0, LX/FY2;->i:LX/FYE;

    invoke-virtual {v1, p1}, LX/FYE;->a(Landroid/support/v4/app/FragmentActivity;)LX/FYD;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2255690
    iget-object v1, p0, LX/FY2;->h:LX/FYK;

    invoke-virtual {v1, p1}, LX/FYK;->a(Landroid/content/Context;)LX/FYJ;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2255691
    iget-object v1, p0, LX/FY2;->b:LX/FXz;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2255692
    iget-object v1, p0, LX/FY2;->g:LX/FYI;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2255693
    iget-object v1, p0, LX/FY2;->c:LX/FY5;

    invoke-virtual {v1, p4}, LX/FY5;->a(Landroid/view/View;)LX/FY4;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2255694
    invoke-static {v0, p2}, LX/FY2;->a(Ljava/util/ArrayList;LX/BO1;)V

    .line 2255695
    move-object v0, v0

    .line 2255696
    goto/16 :goto_0
.end method
