.class public final LX/Fp2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/ui/FundraiserCreationFragment;)V
    .locals 0

    .prologue
    .line 2290521
    iput-object p1, p0, LX/Fp2;->a:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 2290522
    iget-object v0, p0, LX/Fp2;->a:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->o:Landroid/support/design/widget/TextInputLayout;

    const/4 v1, 0x1

    .line 2290523
    iput-boolean v1, v0, Landroid/support/design/widget/TextInputLayout;->r:Z

    .line 2290524
    if-nez p2, :cond_0

    .line 2290525
    iget-object v0, p0, LX/Fp2;->a:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->g:LX/BOa;

    .line 2290526
    iget-object v1, v0, LX/BOa;->a:LX/0Zb;

    const-string p0, "fundraiser_creation_changed_description"

    const/4 p1, 0x0

    invoke-static {v0, p0, p1}, LX/BOa;->a(LX/BOa;Ljava/lang/String;Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    invoke-interface {v1, p0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2290527
    :cond_0
    return-void
.end method
