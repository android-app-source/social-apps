.class public LX/FYI;
.super LX/FXy;
.source ""


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5up;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Sc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .param p3    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/5up;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Sc;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2255936
    invoke-direct {p0}, LX/FXy;-><init>()V

    .line 2255937
    iput-object p1, p0, LX/FYI;->a:LX/0Ot;

    .line 2255938
    iput-object p2, p0, LX/FYI;->b:LX/0Ot;

    .line 2255939
    iput-object p3, p0, LX/FYI;->c:LX/0Ot;

    .line 2255940
    iput-object p4, p0, LX/FYI;->d:LX/0Ot;

    .line 2255941
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2255942
    const v0, 0x7f0217a4

    return v0
.end method

.method public final a(LX/BO1;)LX/FXy;
    .locals 2

    .prologue
    .line 2255943
    invoke-interface {p1}, LX/BO1;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLSavedState;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, LX/BO1;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2255944
    :goto_0
    iput-boolean v0, p0, LX/FXy;->a:Z

    .line 2255945
    return-object p0

    .line 2255946
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2255947
    const v0, 0x7f081aca

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation

    .prologue
    .line 2255948
    const-string v0, "unarchive_button"

    return-object v0
.end method

.method public final b(LX/BO1;)Z
    .locals 7

    .prologue
    .line 2255949
    invoke-interface {p1}, LX/AUV;->d()J

    move-result-wide v2

    .line 2255950
    invoke-interface {p1}, LX/BO1;->f()Ljava/lang/String;

    move-result-object v5

    .line 2255951
    invoke-interface {p1}, LX/BO1;->W()Ljava/lang/String;

    move-result-object v4

    .line 2255952
    iget-object v0, p0, LX/FYI;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/facebook/saved2/ui/contextmenu/Saved2UnarchiveMenuItem$1;-><init>(LX/FYI;JLjava/lang/String;Ljava/lang/String;)V

    const v1, -0x7f2f58ad

    invoke-static {v6, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2255953
    const/4 v0, 0x1

    return v0
.end method
