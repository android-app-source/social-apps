.class public LX/FKN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/service/model/AddMembersParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/FKo;


# direct methods
.method public constructor <init>(LX/FKo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225054
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2225055
    iput-object p1, p0, LX/FKN;->a:LX/FKo;

    .line 2225056
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 2225059
    check-cast p1, Lcom/facebook/messaging/service/model/AddMembersParams;

    .line 2225060
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2225061
    iget-object v0, p1, Lcom/facebook/messaging/service/model/AddMembersParams;->b:LX/0Px;

    move-object v0, v0

    .line 2225062
    invoke-static {v0}, LX/FKo;->a(Ljava/util/List;)LX/0lF;

    move-result-object v0

    .line 2225063
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "id"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "t_"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2225064
    iget-object v5, p1, Lcom/facebook/messaging/service/model/AddMembersParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v5, v5

    .line 2225065
    invoke-virtual {v5}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225066
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "to"

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225067
    new-instance v0, LX/14N;

    const-string v1, "addMembers"

    const-string v2, "POST"

    const-string v3, "/participants"

    sget-object v5, LX/14S;->STRING:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2225057
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2225058
    const/4 v0, 0x0

    return-object v0
.end method
