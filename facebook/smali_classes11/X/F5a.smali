.class public final LX/F5a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5pc;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:I

.field public final synthetic c:Lcom/facebook/groups/fb4a/react/GroupsReactFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/fb4a/react/GroupsReactFragment;II)V
    .locals 0

    .prologue
    .line 2198940
    iput-object p1, p0, LX/F5a;->c:Lcom/facebook/groups/fb4a/react/GroupsReactFragment;

    iput p2, p0, LX/F5a;->a:I

    iput p3, p0, LX/F5a;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2198931
    const-string v0, "CREATE_REACT_CONTEXT_START"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2198932
    iget v0, p0, LX/F5a;->a:I

    if-eqz v0, :cond_0

    .line 2198933
    iget-object v0, p0, LX/F5a;->c:Lcom/facebook/groups/fb4a/react/GroupsReactFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/react/GroupsReactFragment;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/F5a;->a:I

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    .line 2198934
    :cond_0
    :goto_0
    return-void

    .line 2198935
    :cond_1
    const-string v0, "CREATE_REACT_CONTEXT_END"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2198936
    iget v0, p0, LX/F5a;->a:I

    if-eqz v0, :cond_2

    .line 2198937
    iget-object v0, p0, LX/F5a;->c:Lcom/facebook/groups/fb4a/react/GroupsReactFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/react/GroupsReactFragment;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/F5a;->a:I

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2198938
    :cond_2
    iget v0, p0, LX/F5a;->b:I

    if-eqz v0, :cond_0

    .line 2198939
    iget-object v0, p0, LX/F5a;->c:Lcom/facebook/groups/fb4a/react/GroupsReactFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/react/GroupsReactFragment;->g:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget v1, p0, LX/F5a;->b:I

    invoke-interface {v0, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(I)V

    goto :goto_0
.end method
