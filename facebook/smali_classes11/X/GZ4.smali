.class public LX/GZ4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public a:Lcom/facebook/ipc/media/MediaItem;
    .annotation build Landroid/support/annotation/VisibleForTesting;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/74n;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/GXS;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Ot",
            "<",
            "LX/74n;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2365909
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2365910
    iput-object p1, p0, LX/GZ4;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2365911
    iput-object p2, p0, LX/GZ4;->c:LX/0Ot;

    .line 2365912
    return-void
.end method

.method public static a(LX/0QB;)LX/GZ4;
    .locals 3

    .prologue
    .line 2365947
    new-instance v1, LX/GZ4;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x2e13

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    invoke-direct {v1, v0, v2}, LX/GZ4;-><init>(Lcom/facebook/content/SecureContextHelper;LX/0Ot;)V

    .line 2365948
    move-object v0, v1

    .line 2365949
    return-object v0
.end method


# virtual methods
.method public final a(IILandroid/content/Intent;)Z
    .locals 6
    .param p3    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2365913
    const/16 v0, 0x24cc

    if-ne p1, v0, :cond_1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 2365914
    if-eqz p3, :cond_0

    iget-object v0, p0, LX/GZ4;->a:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GZ4;->d:LX/GXS;

    if-nez v0, :cond_2

    .line 2365915
    :cond_0
    :goto_0
    const/4 v0, 0x1

    .line 2365916
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 2365917
    :cond_2
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 2365918
    if-nez v0, :cond_4

    const/4 v0, 0x0

    :goto_2
    move-object v0, v0

    .line 2365919
    const/4 v1, 0x0

    .line 2365920
    if-eqz v0, :cond_3

    iget-object v2, p0, LX/GZ4;->a:Lcom/facebook/ipc/media/MediaItem;

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GZ4;->d:LX/GXS;

    if-nez v2, :cond_5

    .line 2365921
    :cond_3
    :goto_3
    goto :goto_0

    .line 2365922
    :cond_4
    iget-object p3, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, p3

    .line 2365923
    goto :goto_2

    .line 2365924
    :cond_5
    iget-object v2, p0, LX/GZ4;->a:Lcom/facebook/ipc/media/MediaItem;

    invoke-virtual {v2}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object v2

    .line 2365925
    if-nez v0, :cond_8

    .line 2365926
    :cond_6
    :goto_4
    move-object v2, v2

    .line 2365927
    iget-object v3, p0, LX/GZ4;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/74n;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    sget-object v0, LX/74j;->DEFAULT:LX/74j;

    invoke-virtual {v3, v4, v0}, LX/74n;->a(Landroid/net/Uri;LX/74j;)Lcom/facebook/ipc/media/MediaItem;

    move-result-object v3

    move-object v2, v3

    .line 2365928
    if-eqz v2, :cond_3

    .line 2365929
    iget-object v3, p0, LX/GZ4;->d:LX/GXS;

    iget-object v4, p0, LX/GZ4;->a:Lcom/facebook/ipc/media/MediaItem;

    const/4 p2, -0x1

    .line 2365930
    const/4 v5, 0x0

    move p1, v5

    :goto_5
    iget-object v5, v3, LX/GXS;->a:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge p1, v5, :cond_b

    .line 2365931
    iget-object v5, v3, LX/GXS;->a:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/GXR;

    .line 2365932
    iget-object p3, v5, LX/GXR;->b:LX/GXQ;

    move-object p3, p3

    .line 2365933
    sget-object v0, LX/GXQ;->MEDIA_ITEM:LX/GXQ;

    if-ne p3, v0, :cond_a

    .line 2365934
    iget-object p3, v5, LX/GXR;->a:Ljava/lang/Object;

    move-object v5, p3

    .line 2365935
    check-cast v5, Lcom/facebook/ipc/media/MediaItem;

    .line 2365936
    invoke-virtual {v5, v4}, Lcom/facebook/ipc/media/MediaItem;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 2365937
    :goto_6
    if-eq p1, p2, :cond_7

    .line 2365938
    iget-object v5, v3, LX/GXS;->a:Ljava/util/List;

    invoke-interface {v5, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 2365939
    iget-object v5, v3, LX/GXS;->a:Ljava/util/List;

    new-instance p2, LX/GXR;

    invoke-direct {p2, v2}, LX/GXR;-><init>(Ljava/lang/Object;)V

    invoke-interface {v5, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2365940
    :cond_7
    iput-object v1, p0, LX/GZ4;->a:Lcom/facebook/ipc/media/MediaItem;

    .line 2365941
    goto :goto_3

    .line 2365942
    :cond_8
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_9

    .line 2365943
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 2365944
    :cond_9
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 2365945
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getEditedUri()Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 2365946
    :cond_a
    add-int/lit8 v5, p1, 0x1

    move p1, v5

    goto :goto_5

    :cond_b
    move p1, p2

    goto :goto_6
.end method
