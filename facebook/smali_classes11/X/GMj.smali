.class public final LX/GMj;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:LX/GCE;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:I

.field public final synthetic d:LX/GMm;


# direct methods
.method public constructor <init>(LX/GMm;LX/GCE;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2345202
    iput-object p1, p0, LX/GMj;->d:LX/GMm;

    iput-object p2, p0, LX/GMj;->a:LX/GCE;

    iput-object p3, p0, LX/GMj;->b:Ljava/lang/String;

    iput p4, p0, LX/GMj;->c:I

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 2345203
    iget-object v0, p0, LX/GMj;->a:LX/GCE;

    new-instance v1, LX/GFS;

    iget-object v2, p0, LX/GMj;->b:Ljava/lang/String;

    invoke-static {v2}, LX/GMm;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const/4 v3, 0x5

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, LX/GFS;-><init>(Landroid/content/Intent;IZ)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2345204
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 2345205
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2345206
    iget v0, p0, LX/GMj;->c:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2345207
    return-void
.end method
