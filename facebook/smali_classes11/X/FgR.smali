.class public LX/FgR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QR;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QR",
        "<",
        "Lcom/facebook/ui/search/SearchEditText;",
        ">;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:Lcom/facebook/ui/search/SearchEditText;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2270310
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2270311
    return-void
.end method

.method public static a(LX/0QB;)LX/FgR;
    .locals 3

    .prologue
    .line 2270297
    const-class v1, LX/FgR;

    monitor-enter v1

    .line 2270298
    :try_start_0
    sget-object v0, LX/FgR;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2270299
    sput-object v2, LX/FgR;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2270300
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2270301
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2270302
    new-instance v0, LX/FgR;

    invoke-direct {v0}, LX/FgR;-><init>()V

    .line 2270303
    move-object v0, v0

    .line 2270304
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2270305
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FgR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2270306
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2270307
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final synthetic get()Ljava/lang/Object;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2270308
    iget-object v0, p0, LX/FgR;->a:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, v0

    .line 2270309
    return-object v0
.end method
