.class public final LX/HAK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

.field public final synthetic c:I

.field public final synthetic d:Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;JLcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;I)V
    .locals 0

    .prologue
    .line 2435006
    iput-object p1, p0, LX/HAK;->d:Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;

    iput-wide p2, p0, LX/HAK;->a:J

    iput-object p4, p0, LX/HAK;->b:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    iput p5, p0, LX/HAK;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v0, 0x1

    const v1, -0x7a7c1455

    invoke-static {v10, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v7

    .line 2435007
    iget-object v0, p0, LX/HAK;->d:Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;

    iget-object v0, v0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->e:LX/17Y;

    iget-object v1, p0, LX/HAK;->d:Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;

    iget-object v1, v1, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->m:Landroid/content/Context;

    sget-object v2, LX/0ax;->be:Ljava/lang/String;

    iget-wide v4, p0, LX/HAK;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    .line 2435008
    const-string v0, "com.facebook.katana.profile.id"

    iget-wide v2, p0, LX/HAK;->a:J

    invoke-virtual {v8, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2435009
    const-string v9, "extra_config_action_data"

    new-instance v0, LX/CYE;

    iget-object v1, p0, LX/HAK;->d:Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;

    iget-object v1, v1, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->d:LX/0Uh;

    sget v2, LX/8Dm;->i:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    const-string v2, "PRIMARY_BUTTONS"

    iget-object v3, p0, LX/HAK;->b:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v3

    iget-object v4, p0, LX/HAK;->b:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->l()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, LX/HAK;->c:I

    iget-object v6, p0, LX/HAK;->b:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-static {v6}, LX/H8X;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)Z

    move-result v6

    invoke-direct/range {v0 .. v6}, LX/CYE;-><init>(ZLjava/lang/String;Lcom/facebook/graphql/enums/GraphQLPageActionType;Ljava/lang/String;IZ)V

    invoke-virtual {v8, v9, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2435010
    const-string v0, "extra_action_channel_edit_action"

    iget-object v1, p0, LX/HAK;->b:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    invoke-static {v8, v0, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2435011
    iget-object v0, p0, LX/HAK;->d:Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;

    iget-object v1, v0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->f:Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0x2785

    iget-object v0, p0, LX/HAK;->d:Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;

    iget-object v0, v0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->m:Landroid/content/Context;

    const-class v3, Landroid/app/Activity;

    invoke-static {v0, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v1, v8, v2, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2435012
    const v0, 0x53b7444c

    invoke-static {v10, v10, v0, v7}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
