.class public final LX/Gy9;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2409480
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2409481
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2409482
    :goto_0
    return v1

    .line 2409483
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2409484
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_4

    .line 2409485
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2409486
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2409487
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2409488
    const-string v4, "filter_type"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2409489
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    goto :goto_1

    .line 2409490
    :cond_2
    const-string v4, "items"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2409491
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2409492
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-ne v3, v4, :cond_3

    .line 2409493
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2409494
    invoke-static {p0, p1}, LX/Gy8;->b(LX/15w;LX/186;)I

    move-result v3

    .line 2409495
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2409496
    :cond_3
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 2409497
    goto :goto_1

    .line 2409498
    :cond_4
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2409499
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2409500
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2409501
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2409502
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2409503
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2409504
    if-eqz v0, :cond_0

    .line 2409505
    const-string v1, "filter_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2409506
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2409507
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2409508
    if-eqz v0, :cond_2

    .line 2409509
    const-string v1, "items"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2409510
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2409511
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result p1

    if-ge v1, p1, :cond_1

    .line 2409512
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result p1

    invoke-static {p0, p1, p2}, LX/Gy8;->a(LX/15i;ILX/0nX;)V

    .line 2409513
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2409514
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2409515
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2409516
    return-void
.end method
