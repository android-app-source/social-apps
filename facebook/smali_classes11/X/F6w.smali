.class public final synthetic LX/F6w;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2200983
    invoke-static {}, LX/F71;->values()[LX/F71;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/F6w;->a:[I

    :try_start_0
    sget-object v0, LX/F6w;->a:[I

    sget-object v1, LX/F71;->CONTACTS_HEADER:LX/F71;

    invoke-virtual {v1}, LX/F71;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_8

    :goto_0
    :try_start_1
    sget-object v0, LX/F6w;->a:[I

    sget-object v1, LX/F71;->UNRESPONDED_CONTACT:LX/F71;

    invoke-virtual {v1}, LX/F71;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_7

    :goto_1
    :try_start_2
    sget-object v0, LX/F6w;->a:[I

    sget-object v1, LX/F71;->CONTACT:LX/F71;

    invoke-virtual {v1}, LX/F71;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_6

    :goto_2
    :try_start_3
    sget-object v0, LX/F6w;->a:[I

    sget-object v1, LX/F71;->LOADING:LX/F71;

    invoke-virtual {v1}, LX/F71;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_5

    :goto_3
    :try_start_4
    sget-object v0, LX/F6w;->a:[I

    sget-object v1, LX/F71;->FAILURE:LX/F71;

    invoke-virtual {v1}, LX/F71;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_4

    :goto_4
    :try_start_5
    sget-object v0, LX/F6w;->a:[I

    sget-object v1, LX/F71;->MANAGE_CONTACTS:LX/F71;

    invoke-virtual {v1}, LX/F71;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_3

    :goto_5
    :try_start_6
    sget-object v0, LX/F6w;->a:[I

    sget-object v1, LX/F71;->PYMK_HEADER:LX/F71;

    invoke-virtual {v1}, LX/F71;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_2

    :goto_6
    :try_start_7
    sget-object v0, LX/F6w;->a:[I

    sget-object v1, LX/F71;->UNRESPONDED_PYMK:LX/F71;

    invoke-virtual {v1}, LX/F71;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_1

    :goto_7
    :try_start_8
    sget-object v0, LX/F6w;->a:[I

    sget-object v1, LX/F71;->PYMK:LX/F71;

    invoke-virtual {v1}, LX/F71;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_0

    :goto_8
    return-void

    :catch_0
    goto :goto_8

    :catch_1
    goto :goto_7

    :catch_2
    goto :goto_6

    :catch_3
    goto :goto_5

    :catch_4
    goto :goto_4

    :catch_5
    goto :goto_3

    :catch_6
    goto :goto_2

    :catch_7
    goto :goto_1

    :catch_8
    goto :goto_0
.end method
