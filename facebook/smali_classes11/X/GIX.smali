.class public LX/GIX;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/GIT;

.field private b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field private c:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;


# direct methods
.method public constructor <init>(LX/GIT;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2336798
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2336799
    iput-object p1, p0, LX/GIX;->a:LX/GIT;

    .line 2336800
    return-void
.end method

.method public static a(LX/0QB;)LX/GIX;
    .locals 1

    .prologue
    .line 2336797
    invoke-static {p0}, LX/GIX;->b(LX/0QB;)LX/GIX;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V
    .locals 1

    .prologue
    .line 2336794
    iget-object v0, p0, LX/GIX;->a:LX/GIT;

    invoke-virtual {v0, p1}, LX/GIT;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    .line 2336795
    iput-object p1, p0, LX/GIX;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336796
    return-void
.end method

.method private a(Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 2

    .prologue
    .line 2336786
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2336787
    iget-object v0, p0, LX/GIX;->a:LX/GIT;

    const/4 v1, 0x0

    .line 2336788
    iput-boolean v1, v0, LX/GIT;->g:Z

    .line 2336789
    iget-object v1, p0, LX/GIX;->a:LX/GIT;

    const v0, 0x7f0d03de

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    invoke-virtual {v1, v0, p2}, LX/GIT;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2336790
    iput-object p1, p0, LX/GIX;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;

    .line 2336791
    invoke-static {p0}, LX/GIX;->b(LX/GIX;)V

    .line 2336792
    invoke-direct {p0}, LX/GIX;->c()V

    .line 2336793
    return-void
.end method

.method public static a$redex0(LX/GIX;Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2336753
    iget-object v2, p0, LX/GIX;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336754
    iget-object v3, p0, LX/GHg;->b:LX/GCE;

    move-object v3, v3

    .line 2336755
    iget-object v4, v3, LX/GCE;->e:LX/0ad;

    move-object v3, v4

    .line 2336756
    sget v4, LX/GDK;->k:I

    invoke-interface {v3, v4, v0}, LX/0ad;->a(II)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    move v0, v1

    :cond_0
    invoke-static {p1, v2, v0}, LX/GGl;->a(Landroid/content/Context;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Z)Landroid/content/Intent;

    move-result-object v0

    .line 2336757
    iget-object v2, p0, LX/GHg;->b:LX/GCE;

    move-object v2, v2

    .line 2336758
    new-instance v3, LX/GFS;

    const/16 v4, 0xc

    invoke-direct {v3, v0, v4, v1}, LX/GFS;-><init>(Landroid/content/Intent;IZ)V

    invoke-virtual {v2, v3}, LX/GCE;->a(LX/8wN;)V

    .line 2336759
    return-void
.end method

.method private static b(LX/0QB;)LX/GIX;
    .locals 2

    .prologue
    .line 2336784
    new-instance v1, LX/GIX;

    invoke-static {p0}, LX/GIT;->b(LX/0QB;)LX/GIT;

    move-result-object v0

    check-cast v0, LX/GIT;

    invoke-direct {v1, v0}, LX/GIX;-><init>(LX/GIT;)V

    .line 2336785
    return-object v1
.end method

.method public static b(LX/GIX;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2336770
    iget-object v3, p0, LX/GIX;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;

    iget-object v0, p0, LX/GIX;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080b78

    new-array v6, v1, [Ljava/lang/Object;

    iget-object v0, p0, LX/GIX;->a:LX/GIT;

    .line 2336771
    iget-object v7, v0, LX/GIT;->b:LX/GLb;

    .line 2336772
    iget-boolean v0, v7, LX/GLb;->n:Z

    move v7, v0

    .line 2336773
    move v0, v7

    .line 2336774
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GIX;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->w()I

    move-result v0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v2

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;->setSeeAllButtonText(Ljava/lang/String;)V

    .line 2336775
    iget-object v0, p0, LX/GIX;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->w()I

    move-result v0

    const/4 v3, 0x4

    if-le v0, v3, :cond_1

    move v0, v1

    .line 2336776
    :goto_1
    iget-object v1, p0, LX/GIX;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;->setSeeAllButtonVisibility(Z)V

    .line 2336777
    iget-object v1, p0, LX/GIX;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;

    iget-object v2, p0, LX/GIX;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-eqz v0, :cond_2

    const v0, 0x7f080b79

    :goto_2
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;->setCreateNewButtonText(Ljava/lang/String;)V

    .line 2336778
    iget-object v0, p0, LX/GIX;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;

    new-instance v1, LX/GIU;

    invoke-direct {v1, p0}, LX/GIU;-><init>(LX/GIX;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;->setCreateNewButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2336779
    iget-object v0, p0, LX/GIX;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;

    new-instance v1, LX/GIV;

    invoke-direct {v1, p0}, LX/GIV;-><init>(LX/GIX;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;->setSeeAllButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2336780
    return-void

    .line 2336781
    :cond_0
    iget-object v0, p0, LX/GIX;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->w()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 2336782
    goto :goto_1

    .line 2336783
    :cond_2
    const v0, 0x7f080b7a

    goto :goto_2
.end method

.method public static b$redex0(LX/GIX;Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 2336801
    iget-object v0, p0, LX/GIX;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336802
    sget-object v1, LX/8wL;->AUDIENCE_MANAGEMENT:LX/8wL;

    const v2, 0x7f080b84

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->n()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v1, v2, v3}, LX/8wJ;->a(Landroid/content/Context;LX/8wL;Ljava/lang/Integer;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2336803
    invoke-static {v0}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    move-result-object v2

    .line 2336804
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    .line 2336805
    iget-object v3, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v3, v3

    .line 2336806
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->k()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    .line 2336807
    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, LX/15i;->j(II)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d(I)V

    .line 2336808
    const-string v3, "data_extra"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2336809
    const-string v2, "see_all_extra"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2336810
    move-object v0, v1

    .line 2336811
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2336812
    new-instance v2, LX/GFS;

    const/16 v3, 0xf

    const/4 v4, 0x1

    invoke-direct {v2, v0, v3, v4}, LX/GFS;-><init>(Landroid/content/Intent;IZ)V

    invoke-virtual {v1, v2}, LX/GCE;->a(LX/8wN;)V

    .line 2336813
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2336767
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2336768
    new-instance v1, LX/GIW;

    invoke-direct {v1, p0}, LX/GIW;-><init>(LX/GIX;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2336769
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2336763
    invoke-super {p0}, LX/GHg;->a()V

    .line 2336764
    iget-object v0, p0, LX/GIX;->a:LX/GIT;

    invoke-virtual {v0}, LX/GHg;->a()V

    .line 2336765
    const/4 v0, 0x0

    iput-object v0, p0, LX/GIX;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;

    .line 2336766
    return-void
.end method

.method public final a(LX/GCE;)V
    .locals 1

    .prologue
    .line 2336760
    invoke-super {p0, p1}, LX/GHg;->a(LX/GCE;)V

    .line 2336761
    iget-object v0, p0, LX/GIX;->a:LX/GIT;

    invoke-virtual {v0, p1}, LX/GHg;->a(LX/GCE;)V

    .line 2336762
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2336750
    invoke-super {p0, p1}, LX/GHg;->a(Landroid/os/Bundle;)V

    .line 2336751
    iget-object v0, p0, LX/GIX;->a:LX/GIT;

    invoke-virtual {v0, p1}, LX/GHg;->a(Landroid/os/Bundle;)V

    .line 2336752
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2336749
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;

    invoke-direct {p0, p1, p2}, LX/GIX;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceSelectorView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2336748
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-direct {p0, p1}, LX/GIX;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2336745
    invoke-super {p0, p1}, LX/GHg;->b(Landroid/os/Bundle;)V

    .line 2336746
    iget-object v0, p0, LX/GIX;->a:LX/GIT;

    invoke-virtual {v0, p1}, LX/GHg;->b(Landroid/os/Bundle;)V

    .line 2336747
    return-void
.end method
