.class public LX/GRP;
.super LX/1Cv;
.source ""


# instance fields
.field public final a:LX/GRa;

.field private final b:LX/GRS;

.field public final c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(LX/GRa;Ljava/lang/Integer;LX/GRS;)V
    .locals 0
    .param p2    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/GRS;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2351522
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2351523
    iput-object p1, p0, LX/GRP;->a:LX/GRa;

    .line 2351524
    iput-object p2, p0, LX/GRP;->c:Ljava/lang/Integer;

    .line 2351525
    iput-object p3, p0, LX/GRP;->b:LX/GRS;

    .line 2351526
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2351527
    new-instance v0, LX/GSs;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/GSs;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 2351528
    check-cast p2, LX/GRR;

    .line 2351529
    check-cast p3, LX/GSs;

    .line 2351530
    iget-object v0, p2, LX/GRR;->b:Ljava/lang/String;

    .line 2351531
    iget-object p1, p3, LX/GSs;->a:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2351532
    iget-object v0, p2, LX/GRR;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2351533
    iget-object p1, p3, LX/GSs;->a:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2351534
    iget-boolean v0, p2, LX/GRR;->d:Z

    invoke-virtual {p3, v0}, LX/GSs;->setBlocked(Z)V

    .line 2351535
    new-instance v0, LX/GRO;

    invoke-direct {v0, p0, p2, p3}, LX/GRO;-><init>(LX/GRP;LX/GRR;LX/GSs;)V

    .line 2351536
    iget-object p0, p3, LX/GSs;->a:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2351537
    iget-object p0, p3, LX/GSs;->a:Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    invoke-virtual {p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->getSubtitleView()Landroid/view/View;

    move-result-object p0

    invoke-virtual {p0, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2351538
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2351539
    iget-object v0, p0, LX/GRP;->b:LX/GRS;

    .line 2351540
    iget-object p0, v0, LX/GRS;->a:Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result p0

    move v0, p0

    .line 2351541
    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2351542
    iget-object v0, p0, LX/GRP;->b:LX/GRS;

    .line 2351543
    iget-object p0, v0, LX/GRS;->a:Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/GRR;

    move-object v0, p0

    .line 2351544
    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2351545
    int-to-long v0, p1

    return-wide v0
.end method
