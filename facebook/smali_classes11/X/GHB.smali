.class public LX/GHB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GGc;
.implements LX/GGk;


# instance fields
.field public final i:LX/GF4;

.field private final j:LX/GCB;

.field public final k:LX/GDm;

.field public final l:LX/GG3;

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation
.end field

.field public final n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/GEL;",
            ">;"
        }
    .end annotation
.end field

.field private o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/0W3;

.field private final q:LX/GGX;

.field private final r:LX/GGX;

.field public s:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;


# direct methods
.method public constructor <init>(LX/GF0;LX/GHx;LX/GJb;LX/GKv;LX/GIL;LX/GJO;LX/GLj;LX/GF4;LX/GCB;LX/GEy;LX/GDm;LX/GG3;LX/0W3;LX/0Ot;LX/0Ot;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/GF0;",
            "LX/GHx;",
            "LX/GJb;",
            "LX/GKv;",
            "LX/GIL;",
            "LX/GJO;",
            "LX/GLj;",
            "LX/GF4;",
            "LX/GCB;",
            "LX/GEy;",
            "LX/GDm;",
            "LX/GG3;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/GEL;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2334821
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2334822
    new-instance v1, LX/GH6;

    invoke-direct {v1, p0}, LX/GH6;-><init>(LX/GHB;)V

    iput-object v1, p0, LX/GHB;->q:LX/GGX;

    .line 2334823
    new-instance v1, LX/GH7;

    invoke-direct {v1, p0}, LX/GH7;-><init>(LX/GHB;)V

    iput-object v1, p0, LX/GHB;->r:LX/GGX;

    .line 2334824
    move-object/from16 v0, p13

    iput-object v0, p0, LX/GHB;->p:LX/0W3;

    .line 2334825
    move-object/from16 v0, p14

    iput-object v0, p0, LX/GHB;->m:LX/0Ot;

    .line 2334826
    move-object/from16 v0, p15

    iput-object v0, p0, LX/GHB;->n:LX/0Ot;

    .line 2334827
    iput-object p8, p0, LX/GHB;->i:LX/GF4;

    .line 2334828
    iput-object p9, p0, LX/GHB;->j:LX/GCB;

    .line 2334829
    move-object/from16 v0, p11

    iput-object v0, p0, LX/GHB;->k:LX/GDm;

    .line 2334830
    move-object/from16 v0, p12

    iput-object v0, p0, LX/GHB;->l:LX/GG3;

    .line 2334831
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    invoke-virtual {v1, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030055

    iget-object v4, p0, LX/GHB;->r:LX/GGX;

    sget-object v5, LX/8wK;->OFFERS:LX/8wK;

    invoke-direct {v2, v3, p3, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030058

    sget-object v4, LX/GHB;->a:LX/GGX;

    sget-object v5, LX/8wK;->AD_CREATIVE:LX/8wK;

    invoke-direct {v2, v3, p2, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030053

    iget-object v4, p0, LX/GHB;->q:LX/GGX;

    sget-object v5, LX/8wK;->CALL_TO_ACTION:LX/8wK;

    invoke-direct {v2, v3, p6, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030051

    iget-object v4, p0, LX/GHB;->q:LX/GGX;

    sget-object v5, LX/8wK;->PHONE_NUMBER:LX/8wK;

    invoke-direct {v2, v3, p4, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03005e

    iget-object v4, p0, LX/GHB;->q:LX/GGX;

    sget-object v5, LX/8wK;->ADDRESS:LX/8wK;

    invoke-direct {v2, v3, p5, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030097

    iget-object v4, p0, LX/GHB;->q:LX/GGX;

    sget-object v5, LX/8wK;->WEBSITE_URL:LX/8wK;

    invoke-direct {v2, v3, p7, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    move-object/from16 v0, p10

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    .line 2334832
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, p0, LX/GHB;->o:LX/0Px;

    .line 2334833
    return-void
.end method

.method public static synthetic a(LX/GHB;Landroid/content/Context;)V
    .locals 13

    .prologue
    .line 2334813
    iget-object v0, p0, LX/GHB;->l:LX/GG3;

    iget-object v1, p0, LX/GHB;->s:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    const/4 v8, 0x0

    .line 2334814
    const-string v6, "exit_flow"

    const-string v7, "creative_edit"

    const-string v10, "promote_dialog"

    const/4 v12, 0x1

    move-object v4, v0

    move-object v5, v1

    move-object v9, v8

    move-object v11, v8

    invoke-static/range {v4 .. v12}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2334815
    iget-object v0, p0, LX/GHB;->s:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2334816
    iget-object v0, p0, LX/GHB;->k:LX/GDm;

    iget-object v1, p0, LX/GHB;->s:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0, v1, p1}, LX/GDm;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;)V

    .line 2334817
    :goto_0
    return-void

    .line 2334818
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2334819
    const-string v1, "CREATIVE_EDIT_DATA"

    iget-object v2, p0, LX/GHB;->s:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2334820
    iget-object v1, p0, LX/GHB;->i:LX/GF4;

    new-instance v2, LX/GFS;

    const/4 v3, 0x1

    invoke-direct {v2, v0, v3}, LX/GFS;-><init>(Landroid/content/Intent;Z)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/GHB;
    .locals 18

    .prologue
    .line 2334897
    new-instance v2, LX/GHB;

    invoke-static/range {p0 .. p0}, LX/GF0;->a(LX/0QB;)LX/GF0;

    move-result-object v3

    check-cast v3, LX/GF0;

    invoke-static/range {p0 .. p0}, LX/GHx;->a(LX/0QB;)LX/GHx;

    move-result-object v4

    check-cast v4, LX/GHx;

    invoke-static/range {p0 .. p0}, LX/GJb;->a(LX/0QB;)LX/GJb;

    move-result-object v5

    check-cast v5, LX/GJb;

    invoke-static/range {p0 .. p0}, LX/GKv;->a(LX/0QB;)LX/GKv;

    move-result-object v6

    check-cast v6, LX/GKv;

    invoke-static/range {p0 .. p0}, LX/GIL;->a(LX/0QB;)LX/GIL;

    move-result-object v7

    check-cast v7, LX/GIL;

    invoke-static/range {p0 .. p0}, LX/GJO;->a(LX/0QB;)LX/GJO;

    move-result-object v8

    check-cast v8, LX/GJO;

    invoke-static/range {p0 .. p0}, LX/GLj;->a(LX/0QB;)LX/GLj;

    move-result-object v9

    check-cast v9, LX/GLj;

    invoke-static/range {p0 .. p0}, LX/GF4;->a(LX/0QB;)LX/GF4;

    move-result-object v10

    check-cast v10, LX/GF4;

    invoke-static/range {p0 .. p0}, LX/GCB;->a(LX/0QB;)LX/GCB;

    move-result-object v11

    check-cast v11, LX/GCB;

    invoke-static/range {p0 .. p0}, LX/GEy;->a(LX/0QB;)LX/GEy;

    move-result-object v12

    check-cast v12, LX/GEy;

    invoke-static/range {p0 .. p0}, LX/GDm;->a(LX/0QB;)LX/GDm;

    move-result-object v13

    check-cast v13, LX/GDm;

    invoke-static/range {p0 .. p0}, LX/GG3;->a(LX/0QB;)LX/GG3;

    move-result-object v14

    check-cast v14, LX/GG3;

    invoke-static/range {p0 .. p0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v15

    check-cast v15, LX/0W3;

    const/16 v16, 0x12b1

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x1699

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    invoke-direct/range {v2 .. v17}, LX/GHB;-><init>(LX/GF0;LX/GHx;LX/GJb;LX/GKv;LX/GIL;LX/GJO;LX/GLj;LX/GF4;LX/GCB;LX/GEy;LX/GDm;LX/GG3;LX/0W3;LX/0Ot;LX/0Ot;)V

    .line 2334898
    return-object v2
.end method

.method public static c(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)LX/8wL;
    .locals 2

    .prologue
    .line 2334885
    invoke-static {p0}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2334886
    sget-object v0, LX/GHA;->a:[I

    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v1

    invoke-virtual {v1}, LX/8wL;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2334887
    const/4 v0, 0x0

    :goto_0
    move-object v0, v0

    .line 2334888
    :goto_1
    return-object v0

    .line 2334889
    :cond_0
    sget-object v0, LX/GHA;->a:[I

    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v1

    invoke-virtual {v1}, LX/8wL;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 2334890
    const/4 v0, 0x0

    goto :goto_1

    .line 2334891
    :pswitch_0
    sget-object v0, LX/8wL;->LOCAL_AWARENESS_EDIT_CREATIVE:LX/8wL;

    goto :goto_1

    .line 2334892
    :pswitch_1
    sget-object v0, LX/8wL;->PAGE_LIKE_EDIT_CREATIVE:LX/8wL;

    goto :goto_1

    .line 2334893
    :pswitch_2
    sget-object v0, LX/8wL;->PROMOTE_WEBSITE_EDIT_CREATIVE:LX/8wL;

    goto :goto_1

    .line 2334894
    :pswitch_3
    sget-object v0, LX/8wL;->PROMOTE_CTA_EDIT_CREATIVE:LX/8wL;

    goto :goto_1

    .line 2334895
    :pswitch_4
    sget-object v0, LX/8wL;->BOOST_EVENT_EDIT_CREATIVE:LX/8wL;

    goto :goto_1

    .line 2334896
    :pswitch_5
    sget-object v0, LX/8wL;->PAGE_LIKE_EDIT_RUNNING_CREATIVE:LX/8wL;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2334899
    iget-object v0, p0, LX/GHB;->o:LX/0Px;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;LX/GCY;)V
    .locals 6

    .prologue
    .line 2334836
    const-string v0, "data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2334837
    invoke-static {v0}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2334838
    new-instance v1, LX/GGN;

    invoke-direct {v1}, LX/GGN;-><init>()V

    invoke-virtual {v1}, LX/GGN;->b()Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    move-result-object v2

    .line 2334839
    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    .line 2334840
    iput-object v1, v2, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->f:LX/GGB;

    .line 2334841
    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->c()Ljava/lang/String;

    move-result-object v1

    .line 2334842
    iput-object v1, v2, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d:Ljava/lang/String;

    .line 2334843
    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(Ljava/lang/String;)V

    .line 2334844
    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v1

    .line 2334845
    iput-object v1, v2, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c:LX/8wL;

    .line 2334846
    move-object v1, v0

    .line 2334847
    check-cast v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2334848
    iget-object v3, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v1, v3

    .line 2334849
    iput-object v1, v2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 2334850
    move-object v1, v0

    .line 2334851
    check-cast v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2334852
    iget-object v3, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v3, v3

    .line 2334853
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->q()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel;

    move-result-object v3

    .line 2334854
    if-nez v3, :cond_1

    .line 2334855
    const/4 v3, 0x0

    .line 2334856
    :goto_0
    move-object v1, v3

    .line 2334857
    iput-object v1, v2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2334858
    new-instance v1, LX/A93;

    invoke-direct {v1}, LX/A93;-><init>()V

    new-instance v3, LX/A92;

    invoke-direct {v3}, LX/A92;-><init>()V

    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    .line 2334859
    iput-object v4, v3, LX/A92;->a:LX/0Px;

    .line 2334860
    move-object v3, v3

    .line 2334861
    invoke-virtual {v3}, LX/A92;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v3

    .line 2334862
    iput-object v3, v1, LX/A93;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    .line 2334863
    move-object v1, v1

    .line 2334864
    invoke-virtual {v1}, LX/A93;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;)V

    .line 2334865
    move-object v1, v2

    .line 2334866
    iput-object v1, p0, LX/GHB;->s:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2334867
    invoke-interface {p2, v1}, LX/GCY;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2334868
    :goto_1
    return-void

    .line 2334869
    :cond_0
    invoke-static {v0}, LX/GHB;->c(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)LX/8wL;

    move-result-object v1

    .line 2334870
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c:LX/8wL;

    .line 2334871
    iput-object v0, p0, LX/GHB;->s:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2334872
    invoke-interface {p2, v0}, LX/GCY;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    goto :goto_1

    .line 2334873
    :cond_1
    new-instance v4, LX/GGK;

    invoke-direct {v4}, LX/GGK;-><init>()V

    .line 2334874
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c()Ljava/lang/String;

    move-result-object v5

    .line 2334875
    iput-object v5, v4, LX/GGK;->c:Ljava/lang/String;

    .line 2334876
    iget-object v5, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v5, v5

    .line 2334877
    iget-object p1, v5, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-object v5, p1

    .line 2334878
    iput-object v5, v4, LX/GGK;->a:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2334879
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel;->j()Ljava/lang/String;

    move-result-object v5

    .line 2334880
    iput-object v5, v4, LX/GGK;->e:Ljava/lang/String;

    .line 2334881
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel;->k()LX/1vs;

    move-result-object v5

    iget v5, v5, LX/1vs;->b:I

    if-eqz v5, :cond_2

    .line 2334882
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel;->k()LX/1vs;

    move-result-object v3

    iget-object v5, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const/4 p1, 0x0

    invoke-virtual {v5, v3, p1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 2334883
    iput-object v3, v4, LX/GGK;->h:Ljava/lang/String;

    .line 2334884
    :cond_2
    invoke-virtual {v4}, LX/GGK;->a()Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-result-object v3

    goto :goto_0
.end method

.method public final b()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;
    .locals 1

    .prologue
    .line 2334835
    iget-object v0, p0, LX/GHB;->j:LX/GCB;

    invoke-virtual {v0}, LX/GCB;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/GGh;
    .locals 1

    .prologue
    .line 2334834
    new-instance v0, LX/GH8;

    invoke-direct {v0, p0}, LX/GH8;-><init>(LX/GHB;)V

    return-object v0
.end method
