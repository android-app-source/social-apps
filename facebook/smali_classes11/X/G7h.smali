.class public final LX/G7h;
.super LX/G7f;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/G7f",
        "<",
        "Lcom/google/android/gms/common/api/Status;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic d:Lcom/google/android/gms/auth/api/credentials/Credential;

.field public final synthetic e:LX/G7k;


# direct methods
.method public constructor <init>(LX/G7k;LX/2wX;Lcom/google/android/gms/auth/api/credentials/Credential;)V
    .locals 0

    iput-object p1, p0, LX/G7h;->e:LX/G7k;

    iput-object p3, p0, LX/G7h;->d:Lcom/google/android/gms/auth/api/credentials/Credential;

    invoke-direct {p0, p2}, LX/G7f;-><init>(LX/2wX;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)LX/2NW;
    .locals 1

    return-object p1
.end method

.method public final a(LX/G7p;)V
    .locals 3

    new-instance v0, LX/G7j;

    invoke-direct {v0, p0}, LX/G7j;-><init>(LX/2wh;)V

    new-instance v1, Lcom/google/android/gms/auth/api/credentials/internal/DeleteRequest;

    iget-object v2, p0, LX/G7h;->d:Lcom/google/android/gms/auth/api/credentials/Credential;

    invoke-direct {v1, v2}, Lcom/google/android/gms/auth/api/credentials/internal/DeleteRequest;-><init>(Lcom/google/android/gms/auth/api/credentials/Credential;)V

    invoke-interface {p1, v0, v1}, LX/G7p;->a(LX/G7Z;Lcom/google/android/gms/auth/api/credentials/internal/DeleteRequest;)V

    return-void
.end method
