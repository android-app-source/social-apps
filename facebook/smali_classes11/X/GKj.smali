.class public LX/GKj;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/PacingOptionsView;",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:I
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public final b:Landroid/view/inputmethod/InputMethodManager;

.field public c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public d:Lcom/facebook/adinterfaces/ui/PacingOptionsView;

.field public e:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

.field public f:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

.field public g:Landroid/text/TextWatcher;

.field public h:I

.field private i:LX/GJ4;


# direct methods
.method public constructor <init>(Landroid/view/inputmethod/InputMethodManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2341364
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2341365
    iput-object p1, p0, LX/GKj;->b:Landroid/view/inputmethod/InputMethodManager;

    .line 2341366
    return-void
.end method

.method public static a(LX/0QB;)LX/GKj;
    .locals 1

    .prologue
    .line 2341363
    invoke-static {p0}, LX/GKj;->b(LX/0QB;)LX/GKj;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(LX/GKj;I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2341356
    iget-object v0, p0, LX/GKj;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0}, LX/GMU;->h(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)I

    move-result v0

    int-to-long v2, p1

    .line 2341357
    iget-object v1, p0, LX/GKj;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v1}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v4

    move-object v1, v4

    .line 2341358
    check-cast v1, Ljava/text/DecimalFormat;

    invoke-virtual {v1}, Ljava/text/DecimalFormat;->getDecimalFormatSymbols()Ljava/text/DecimalFormatSymbols;

    move-result-object p1

    .line 2341359
    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/text/DecimalFormatSymbols;->setCurrencySymbol(Ljava/lang/String;)V

    move-object v1, v4

    .line 2341360
    check-cast v1, Ljava/text/DecimalFormat;

    invoke-virtual {v1, p1}, Ljava/text/DecimalFormat;->setDecimalFormatSymbols(Ljava/text/DecimalFormatSymbols;)V

    .line 2341361
    move-object v1, v4

    .line 2341362
    invoke-static {v0, v2, v3, v1}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/GKj;
    .locals 2

    .prologue
    .line 2341354
    new-instance v1, LX/GKj;

    invoke-static {p0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-direct {v1, v0}, LX/GKj;-><init>(Landroid/view/inputmethod/InputMethodManager;)V

    .line 2341355
    return-object v1
.end method

.method public static h(LX/GKj;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2341351
    iget-object v0, p0, LX/GKj;->b:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, LX/GKj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2341352
    iget-object v0, p0, LX/GKj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->setCursorVisible(Z)V

    .line 2341353
    return-void
.end method

.method public static j(LX/GKj;)I
    .locals 4

    .prologue
    .line 2341367
    :try_start_0
    iget-object v0, p0, LX/GKj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->getBidAmountText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    iget-object v2, p0, LX/GKj;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v2}, LX/GMU;->h(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    int-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    .line 2341368
    :goto_0
    return v0

    :catch_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private n()LX/GJ4;
    .locals 1

    .prologue
    .line 2341286
    iget-object v0, p0, LX/GKj;->i:LX/GJ4;

    if-nez v0, :cond_0

    .line 2341287
    new-instance v0, LX/GKg;

    invoke-direct {v0, p0}, LX/GKg;-><init>(LX/GKj;)V

    iput-object v0, p0, LX/GKj;->i:LX/GJ4;

    .line 2341288
    :cond_0
    iget-object v0, p0, LX/GKj;->i:LX/GJ4;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2341289
    invoke-super {p0}, LX/GHg;->a()V

    .line 2341290
    const/4 v3, 0x0

    .line 2341291
    iget-object v1, p0, LX/GKj;->d:Lcom/facebook/adinterfaces/ui/PacingOptionsView;

    .line 2341292
    iput-object v3, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->b:LX/GJ4;

    .line 2341293
    iget-object v1, p0, LX/GKj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    iget-object v2, p0, LX/GKj;->g:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->b(Landroid/text/TextWatcher;)V

    .line 2341294
    iget-object v1, p0, LX/GKj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    invoke-virtual {v1, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2341295
    iget-object v1, p0, LX/GKj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    invoke-virtual {v1, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->setOnFocusChangeListenerEditText(Landroid/view/View$OnFocusChangeListener;)V

    .line 2341296
    iput-object v0, p0, LX/GKj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    .line 2341297
    iput-object v0, p0, LX/GKj;->d:Lcom/facebook/adinterfaces/ui/PacingOptionsView;

    .line 2341298
    iput-object v0, p0, LX/GKj;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2341299
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 2
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2341300
    check-cast p1, Lcom/facebook/adinterfaces/ui/PacingOptionsView;

    .line 2341301
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2341302
    iput-object p1, p0, LX/GKj;->d:Lcom/facebook/adinterfaces/ui/PacingOptionsView;

    .line 2341303
    iput-object p2, p0, LX/GKj;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2341304
    iget-object v0, p0, LX/GKj;->d:Lcom/facebook/adinterfaces/ui/PacingOptionsView;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2341305
    iget-object v0, p0, LX/GKj;->d:Lcom/facebook/adinterfaces/ui/PacingOptionsView;

    .line 2341306
    iget-object v1, v0, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    move-object v0, v1

    .line 2341307
    iput-object v0, p0, LX/GKj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    .line 2341308
    iget-object v0, p0, LX/GKj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    iget-object v1, p0, LX/GKj;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v1}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v1

    invoke-virtual {v1}, Ljava/text/NumberFormat;->getCurrency()Ljava/util/Currency;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Currency;->getSymbol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->setCurrencySymbol(Ljava/lang/CharSequence;)V

    .line 2341309
    new-instance v0, LX/GKe;

    invoke-direct {v0, p0}, LX/GKe;-><init>(LX/GKj;)V

    move-object v0, v0

    .line 2341310
    iput-object v0, p0, LX/GKj;->g:Landroid/text/TextWatcher;

    .line 2341311
    iget-object v0, p0, LX/GKj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    iget-object v1, p0, LX/GKj;->g:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->a(Landroid/text/TextWatcher;)V

    .line 2341312
    iget-object v0, p0, LX/GKj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    new-instance v1, LX/GKd;

    invoke-direct {v1, p0}, LX/GKd;-><init>(LX/GKj;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2341313
    iget-object v0, p0, LX/GKj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;

    .line 2341314
    new-instance v1, LX/GKf;

    invoke-direct {v1, p0}, LX/GKf;-><init>(LX/GKj;)V

    move-object v1, v1

    .line 2341315
    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBidAmountEditView;->setOnFocusChangeListenerEditText(Landroid/view/View$OnFocusChangeListener;)V

    .line 2341316
    iget-object v0, p0, LX/GKj;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080b40

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2341317
    iget-object v1, p0, LX/GKj;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const p1, 0x7f021965

    invoke-virtual {v1, v0, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Ljava/lang/String;I)V

    .line 2341318
    iget-object v0, p0, LX/GKj;->d:Lcom/facebook/adinterfaces/ui/PacingOptionsView;

    invoke-direct {p0}, LX/GKj;->n()LX/GJ4;

    move-result-object v1

    .line 2341319
    iput-object v1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->b:LX/GJ4;

    .line 2341320
    iget-object v0, p0, LX/GKj;->d:Lcom/facebook/adinterfaces/ui/PacingOptionsView;

    iget v1, p0, LX/GKj;->a:I

    .line 2341321
    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->setSelected(I)V

    .line 2341322
    invoke-direct {p0}, LX/GKj;->n()LX/GJ4;

    move-result-object v0

    iget v1, p0, LX/GKj;->a:I

    invoke-interface {v0, v1}, LX/GJ4;->a(I)V

    .line 2341323
    if-eqz p2, :cond_0

    iget-object v0, p0, LX/GKj;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOSTED_COMPONENT_EDIT_PACING:LX/8wL;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/GKj;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2341324
    new-instance v0, LX/GKh;

    invoke-direct {v0, p0}, LX/GKh;-><init>(LX/GKj;)V

    .line 2341325
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2341326
    const/16 p1, 0x11

    invoke-virtual {v1, p1, v0}, LX/GCE;->a(ILX/GFR;)V

    .line 2341327
    iget-object v0, p0, LX/GKj;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v1, p0, LX/GKj;->d:Lcom/facebook/adinterfaces/ui/PacingOptionsView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f080bb9

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setCallToActionText(Ljava/lang/String;)V

    .line 2341328
    iget-object v0, p0, LX/GKj;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    new-instance v1, LX/GKi;

    invoke-direct {v1, p0}, LX/GKi;-><init>(LX/GKj;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setCallToActionClickListener(Landroid/view/View$OnClickListener;)V

    .line 2341329
    iget-object v1, p0, LX/GKj;->d:Lcom/facebook/adinterfaces/ui/PacingOptionsView;

    iget v0, p0, LX/GKj;->a:I

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/PacingOptionsView;->a(Z)V

    .line 2341330
    :cond_0
    return-void

    .line 2341331
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 5

    .prologue
    .line 2341332
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2341333
    iput-object p1, p0, LX/GKj;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2341334
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2341335
    iget-object v0, p0, LX/GKj;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2341336
    iget-object v3, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v3, v3

    .line 2341337
    if-eqz v3, :cond_2

    .line 2341338
    iget-object v0, p0, LX/GKj;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->l()I

    move-result v0

    :goto_0
    iput v0, p0, LX/GKj;->h:I

    .line 2341339
    iget-object v0, p0, LX/GKj;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->A()Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    move-result-object v0

    .line 2341340
    :goto_1
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->STANDARD:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    if-ne v0, v3, :cond_3

    move v0, v1

    :goto_2
    iput v0, p0, LX/GKj;->a:I

    .line 2341341
    return-void

    .line 2341342
    :cond_0
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->l()I

    move-result v0

    goto :goto_0

    .line 2341343
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->p()Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    move-result-object v0

    goto :goto_1

    .line 2341344
    :cond_2
    iget-object v0, p0, LX/GKj;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->n()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v1}, LX/15i;->j(II)I

    move-result v0

    iput v0, p0, LX/GKj;->h:I

    .line 2341345
    iget-object v0, p0, LX/GKj;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->n()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    sget-object p1, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    invoke-virtual {v3, v0, v2, v4, p1}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    goto :goto_1

    :cond_3
    move v0, v2

    .line 2341346
    goto :goto_2
.end method

.method public final b()Z
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 2341347
    iget-object v1, p0, LX/GKj;->d:Lcom/facebook/adinterfaces/ui/PacingOptionsView;

    .line 2341348
    iget v2, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->c:I

    move v1, v2

    .line 2341349
    if-nez v1, :cond_1

    .line 2341350
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, LX/GKj;->j(LX/GKj;)I

    move-result v1

    if-gtz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method
