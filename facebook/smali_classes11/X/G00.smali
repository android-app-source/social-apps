.class public final LX/G00;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2308388
    iput-object p1, p0, LX/G00;->c:Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;

    iput-object p2, p0, LX/G00;->a:Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;

    iput-object p3, p0, LX/G00;->b:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x2b705eda

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2308389
    iget-object v0, p0, LX/G00;->c:Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;

    iget-object v2, v0, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->t:Lcom/facebook/content/SecureContextHelper;

    iget-object v0, p0, LX/G00;->c:Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;->x:LX/G0B;

    iget-object v3, p0, LX/G00;->a:Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;

    iget-object v4, p0, LX/G00;->c:Lcom/facebook/timeline/legacycontact/MemorialCoverPhotoActivity;

    iget-object v4, v4, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->r:Ljava/lang/String;

    sget-object v5, LX/8AB;->COVERPHOTO:LX/8AB;

    invoke-virtual {v0, v3, v4, v5}, LX/G0B;->a(Lcom/facebook/timeline/legacycontact/protocol/MemorializedContactModels$MemorializedContactModel;Ljava/lang/String;LX/8AB;)Landroid/content/Intent;

    move-result-object v3

    const/16 v4, 0x713

    iget-object v0, p0, LX/G00;->b:Landroid/content/Context;

    const-class v5, Landroid/app/Activity;

    invoke-static {v0, v5}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v3, v4, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2308390
    const v0, -0x5895db8a

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
