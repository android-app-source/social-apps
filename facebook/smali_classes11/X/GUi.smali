.class public LX/GUi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/GUi;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2357892
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2357893
    iput-object p1, p0, LX/GUi;->a:LX/0Zb;

    .line 2357894
    return-void
.end method

.method public static a(LX/0QB;)LX/GUi;
    .locals 4

    .prologue
    .line 2357879
    sget-object v0, LX/GUi;->b:LX/GUi;

    if-nez v0, :cond_1

    .line 2357880
    const-class v1, LX/GUi;

    monitor-enter v1

    .line 2357881
    :try_start_0
    sget-object v0, LX/GUi;->b:LX/GUi;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2357882
    if-eqz v2, :cond_0

    .line 2357883
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2357884
    new-instance p0, LX/GUi;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/GUi;-><init>(LX/0Zb;)V

    .line 2357885
    move-object v0, p0

    .line 2357886
    sput-object v0, LX/GUi;->b:LX/GUi;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2357887
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2357888
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2357889
    :cond_1
    sget-object v0, LX/GUi;->b:LX/GUi;

    return-object v0

    .line 2357890
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2357891
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 2357873
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "background_location"

    .line 2357874
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2357875
    move-object v0, v0

    .line 2357876
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2357877
    iget-object v0, p0, LX/GUi;->a:LX/0Zb;

    const-string v1, "friends_nearby_settings_pause_option_select"

    invoke-static {v1}, LX/GUi;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "pause_option"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2357878
    return-void
.end method
