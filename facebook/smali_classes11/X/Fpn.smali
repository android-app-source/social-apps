.class public final LX/Fpn;
.super LX/95p;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/95p",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/G4x;

.field public final synthetic b:Lcom/facebook/timeline/BaseTimelineFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/BaseTimelineFragment;LX/G4x;)V
    .locals 0

    .prologue
    .line 2291901
    iput-object p1, p0, LX/Fpn;->b:Lcom/facebook/timeline/BaseTimelineFragment;

    iput-object p2, p0, LX/Fpn;->a:LX/G4x;

    invoke-direct {p0}, LX/95p;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2291897
    check-cast p1, Lcom/facebook/graphql/model/FeedUnit;

    .line 2291898
    iget-object v0, p0, LX/Fpn;->a:LX/G4x;

    invoke-virtual {v0, p1}, LX/G4x;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2291899
    iget-object v0, p0, LX/Fpn;->b:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2291900
    return-void
.end method

.method public final a(Ljava/lang/Object;Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 2291889
    check-cast p1, Lcom/facebook/graphql/model/FeedUnit;

    .line 2291890
    iget-object v0, p0, LX/Fpn;->b:Lcom/facebook/timeline/BaseTimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/BaseTimelineFragment;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "timeline_story_notify_me_fail"

    invoke-virtual {v0, v1, p2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2291891
    iget-object v0, p0, LX/Fpn;->a:LX/G4x;

    if-eqz v0, :cond_0

    .line 2291892
    iget-object v0, p0, LX/Fpn;->a:LX/G4x;

    invoke-virtual {v0, p1}, LX/G4x;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2291893
    :cond_0
    iget-object v0, p0, LX/Fpn;->b:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->v()LX/1OP;

    move-result-object v0

    .line 2291894
    if-eqz v0, :cond_1

    .line 2291895
    iget-object v0, p0, LX/Fpn;->b:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2291896
    :cond_1
    return-void
.end method
