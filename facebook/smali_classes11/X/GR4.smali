.class public LX/GR4;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field public static d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/GQv;",
            "LX/GR3;",
            ">;"
        }
    .end annotation
.end field

.field private static e:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

.field private static f:LX/GQy;

.field private static g:Z

.field public static h:Landroid/content/Context;

.field public static i:Ljava/lang/Object;

.field private static j:Ljava/lang/String;

.field public static k:Z


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:LX/GQv;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2351289
    const-class v0, LX/GR4;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/GR4;->a:Ljava/lang/String;

    .line 2351290
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, LX/GR4;->d:Ljava/util/Map;

    .line 2351291
    sget-object v0, LX/GQy;->AUTO:LX/GQy;

    sput-object v0, LX/GR4;->f:LX/GQy;

    .line 2351292
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/GR4;->i:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/AccessToken;)V
    .locals 2

    .prologue
    .line 2351264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2351265
    const-string v0, "context"

    invoke-static {p1, v0}, LX/Gsd;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2351266
    if-nez p1, :cond_5

    .line 2351267
    const-string v0, "null"

    .line 2351268
    :goto_0
    move-object v0, v0

    .line 2351269
    iput-object v0, p0, LX/GR4;->b:Ljava/lang/String;

    .line 2351270
    if-nez p3, :cond_0

    .line 2351271
    invoke-static {}, Lcom/facebook/AccessToken;->a()Lcom/facebook/AccessToken;

    move-result-object p3

    .line 2351272
    :cond_0
    if-eqz p3, :cond_3

    if-eqz p2, :cond_1

    .line 2351273
    iget-object v0, p3, Lcom/facebook/AccessToken;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2351274
    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2351275
    :cond_1
    new-instance v0, LX/GQv;

    invoke-direct {v0, p3}, LX/GQv;-><init>(Lcom/facebook/AccessToken;)V

    iput-object v0, p0, LX/GR4;->c:LX/GQv;

    .line 2351276
    :goto_1
    sget-object v1, LX/GR4;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 2351277
    :try_start_0
    sget-object v0, LX/GR4;->h:Landroid/content/Context;

    if-nez v0, :cond_2

    .line 2351278
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sput-object v0, LX/GR4;->h:Landroid/content/Context;

    .line 2351279
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2351280
    invoke-static {}, LX/GR4;->g()V

    .line 2351281
    return-void

    .line 2351282
    :cond_3
    if-nez p2, :cond_4

    .line 2351283
    invoke-static {p1}, LX/Gsc;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p2

    .line 2351284
    :cond_4
    new-instance v0, LX/GQv;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p2}, LX/GQv;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, LX/GR4;->c:LX/GQv;

    goto :goto_1

    .line 2351285
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2351286
    :cond_5
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-ne p1, v0, :cond_6

    .line 2351287
    const-string v0, "unknown"

    goto :goto_0

    .line 2351288
    :cond_6
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/GQv;LX/GR3;ZLX/GR1;)LX/GAU;
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 2351242
    iget-object v0, p0, LX/GQv;->applicationId:Ljava/lang/String;

    move-object v0, v0

    .line 2351243
    invoke-static {v0, v5}, LX/Gsc;->a(Ljava/lang/String;Z)LX/Gsa;

    move-result-object v3

    .line 2351244
    const-string v2, "%s/activities"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2351245
    new-instance v6, LX/GAU;

    const/4 v9, 0x0

    sget-object v10, LX/GAZ;->POST:LX/GAZ;

    move-object v7, v1

    move-object v8, v0

    move-object v11, v1

    invoke-direct/range {v6 .. v11}, LX/GAU;-><init>(Lcom/facebook/AccessToken;Ljava/lang/String;Landroid/os/Bundle;LX/GAZ;LX/GA2;)V

    .line 2351246
    iput-object v1, v6, LX/GAU;->g:Lorg/json/JSONObject;

    .line 2351247
    move-object v2, v6

    .line 2351248
    iget-object v0, v2, LX/GAU;->k:Landroid/os/Bundle;

    move-object v0, v0

    .line 2351249
    if-nez v0, :cond_0

    .line 2351250
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2351251
    :cond_0
    const-string v4, "access_token"

    .line 2351252
    iget-object v5, p0, LX/GQv;->accessTokenString:Ljava/lang/String;

    move-object v5, v5

    .line 2351253
    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2351254
    iput-object v0, v2, LX/GAU;->k:Landroid/os/Bundle;

    .line 2351255
    if-nez v3, :cond_1

    move-object v0, v1

    .line 2351256
    :goto_0
    return-object v0

    .line 2351257
    :cond_1
    iget-boolean v0, v3, LX/Gsa;->a:Z

    move v0, v0

    .line 2351258
    invoke-virtual {p1, v2, v0, p2}, LX/GR3;->a(LX/GAU;ZZ)I

    move-result v0

    .line 2351259
    if-nez v0, :cond_2

    move-object v0, v1

    .line 2351260
    goto :goto_0

    .line 2351261
    :cond_2
    iget v1, p3, LX/GR1;->a:I

    add-int/2addr v0, v1

    iput v0, p3, LX/GR1;->a:I

    .line 2351262
    new-instance v0, LX/GQt;

    invoke-direct {v0, p0, v2, p1, p3}, LX/GQt;-><init>(LX/GQv;LX/GAU;LX/GR3;LX/GR1;)V

    invoke-virtual {v2, v0}, LX/GAU;->a(LX/GA2;)V

    move-object v0, v2

    .line 2351263
    goto :goto_0
.end method

.method public static a()LX/GQy;
    .locals 2

    .prologue
    .line 2351239
    sget-object v1, LX/GR4;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 2351240
    :try_start_0
    sget-object v0, LX/GR4;->f:LX/GQy;

    monitor-exit v1

    return-object v0

    .line 2351241
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(LX/GQv;)LX/GR3;
    .locals 2

    .prologue
    .line 2351236
    sget-object v1, LX/GR4;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 2351237
    :try_start_0
    sget-object v0, LX/GR4;->d:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GR3;

    monitor-exit v1

    return-object v0

    .line 2351238
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)LX/GR4;
    .locals 2

    .prologue
    .line 2351144
    new-instance v0, LX/GR4;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, LX/GR4;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/AccessToken;)V

    return-object v0
.end method

.method public static a(LX/GR4;Ljava/lang/String;Ljava/lang/Double;Landroid/os/Bundle;Z)V
    .locals 6

    .prologue
    .line 2351227
    new-instance v0, LX/GQx;

    iget-object v1, p0, LX/GR4;->b:Ljava/lang/String;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, LX/GQx;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Double;Landroid/os/Bundle;Z)V

    .line 2351228
    sget-object v1, LX/GR4;->h:Landroid/content/Context;

    iget-object v2, p0, LX/GR4;->c:LX/GQv;

    .line 2351229
    invoke-static {}, LX/GAK;->d()Ljava/util/concurrent/Executor;

    move-result-object v3

    new-instance v4, Lcom/facebook/appevents/AppEventsLogger$5;

    invoke-direct {v4, v1, v2, v0}, Lcom/facebook/appevents/AppEventsLogger$5;-><init>(Landroid/content/Context;LX/GQv;LX/GQx;)V

    const v5, -0x45c2bfa6

    invoke-static {v3, v4, v5}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2351230
    iget-boolean v3, v0, LX/GQx;->isImplicit:Z

    if-nez v3, :cond_0

    sget-boolean v3, LX/GR4;->k:Z

    if-nez v3, :cond_0

    .line 2351231
    iget-object v3, v0, LX/GQx;->name:Ljava/lang/String;

    move-object v3, v3

    .line 2351232
    const-string v4, "fb_mobile_activate_app"

    if-ne v3, v4, :cond_1

    .line 2351233
    const/4 v3, 0x1

    sput-boolean v3, LX/GR4;->k:Z

    .line 2351234
    :cond_0
    :goto_0
    return-void

    .line 2351235
    :cond_1
    sget-object v3, LX/GAb;->APP_EVENTS:LX/GAb;

    const-string v4, "AppEvents"

    const-string v5, "Warning: Please call AppEventsLogger.activateApp(...)from the long-lived activity\'s onResume() methodbefore logging other app events."

    invoke-static {v3, v4, v5}, LX/GsN;->a(LX/GAb;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;LX/GQv;)LX/GR3;
    .locals 5

    .prologue
    .line 2351293
    sget-object v0, LX/GR4;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GR3;

    .line 2351294
    const/4 v1, 0x0

    .line 2351295
    if-nez v0, :cond_0

    .line 2351296
    invoke-static {p0}, LX/Gry;->a(Landroid/content/Context;)LX/Gry;

    move-result-object v0

    move-object v1, v0

    .line 2351297
    :cond_0
    sget-object v2, LX/GR4;->i:Ljava/lang/Object;

    monitor-enter v2

    .line 2351298
    :try_start_0
    sget-object v0, LX/GR4;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GR3;

    .line 2351299
    if-nez v0, :cond_1

    .line 2351300
    new-instance v0, LX/GR3;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0}, LX/GR4;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v3, v4}, LX/GR3;-><init>(LX/Gry;Ljava/lang/String;Ljava/lang/String;)V

    .line 2351301
    sget-object v1, LX/GR4;->d:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2351302
    :cond_1
    monitor-exit v2

    return-object v0

    .line 2351303
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2351216
    sget-object v0, LX/GR4;->j:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 2351217
    sget-object v1, LX/GR4;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 2351218
    :try_start_0
    sget-object v0, LX/GR4;->j:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2351219
    const-string v0, "com.facebook.sdk.appEventPreferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2351220
    const-string v2, "anonymousAppDeviceGUID"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2351221
    sput-object v0, LX/GR4;->j:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2351222
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "XZ"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/GR4;->j:Ljava/lang/String;

    .line 2351223
    const-string v0, "com.facebook.sdk.appEventPreferences"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "anonymousAppDeviceGUID"

    sget-object v3, LX/GR4;->j:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 2351224
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2351225
    :cond_1
    sget-object v0, LX/GR4;->j:Ljava/lang/String;

    return-object v0

    .line 2351226
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static c(LX/GQz;)V
    .locals 10

    .prologue
    .line 2351171
    sget-object v1, LX/GR4;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 2351172
    :try_start_0
    sget-boolean v0, LX/GR4;->g:Z

    if-eqz v0, :cond_1

    .line 2351173
    monitor-exit v1

    .line 2351174
    :cond_0
    :goto_0
    return-void

    .line 2351175
    :cond_1
    const/4 v0, 0x1

    sput-boolean v0, LX/GR4;->g:Z

    .line 2351176
    new-instance v2, Ljava/util/HashSet;

    sget-object v0, LX/GR4;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 2351177
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2351178
    sget-object v0, LX/GR4;->h:Landroid/content/Context;

    invoke-static {v0}, LX/GR2;->a(Landroid/content/Context;)LX/GR2;

    move-result-object v3

    .line 2351179
    iget-object v1, v3, LX/GR2;->c:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    move-object v1, v1

    .line 2351180
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GQv;

    .line 2351181
    sget-object v5, LX/GR4;->h:Landroid/content/Context;

    invoke-static {v5, v0}, LX/GR4;->b(Landroid/content/Context;LX/GQv;)LX/GR3;

    move-result-object v5

    .line 2351182
    iget-object v6, v3, LX/GR2;->c:Ljava/util/HashMap;

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/List;

    move-object v0, v6

    .line 2351183
    invoke-virtual {v5, v0}, LX/GR3;->a(Ljava/util/List;)V

    .line 2351184
    invoke-interface {v0}, Ljava/util/List;->size()I

    .line 2351185
    goto :goto_1

    .line 2351186
    :cond_2
    const/4 v0, 0x0

    .line 2351187
    :try_start_1
    const/4 v9, 0x0

    .line 2351188
    new-instance v3, LX/GR1;

    invoke-direct {v3}, LX/GR1;-><init>()V

    .line 2351189
    sget-object v1, LX/GR4;->h:Landroid/content/Context;

    const/4 v6, 0x0

    .line 2351190
    invoke-static {}, LX/Gsd;->a()V

    .line 2351191
    const-string v4, "com.facebook.sdk.appEventPreferences"

    invoke-virtual {v1, v4, v6}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 2351192
    const-string v5, "limitEventUsage"

    invoke-interface {v4, v5, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    move v4, v4

    .line 2351193
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2351194
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/GQv;

    .line 2351195
    invoke-static {v1}, LX/GR4;->a(LX/GQv;)LX/GR3;

    move-result-object v7

    .line 2351196
    if-eqz v7, :cond_3

    .line 2351197
    invoke-static {v1, v7, v4, v3}, LX/GR4;->a(LX/GQv;LX/GR3;ZLX/GR1;)LX/GAU;

    move-result-object v1

    .line 2351198
    if-eqz v1, :cond_3

    .line 2351199
    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2351200
    :cond_4
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_6

    .line 2351201
    sget-object v1, LX/GAb;->APP_EVENTS:LX/GAb;

    sget-object v4, LX/GR4;->a:Ljava/lang/String;

    const-string v6, "Flushing %d events due to %s."

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    iget v8, v3, LX/GR1;->a:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    const/4 v8, 0x1

    invoke-virtual {p0}, LX/GQz;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v1, v4, v6, v7}, LX/GsN;->a(LX/GAb;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2351202
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/GAU;

    .line 2351203
    invoke-virtual {v1}, LX/GAU;->f()LX/GAY;

    goto :goto_3

    :cond_5
    move-object v1, v3

    .line 2351204
    :goto_4
    move-object v0, v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2351205
    :goto_5
    sget-object v1, LX/GR4;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 2351206
    const/4 v2, 0x0

    :try_start_2
    sput-boolean v2, LX/GR4;->g:Z

    .line 2351207
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2351208
    if-eqz v0, :cond_0

    .line 2351209
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.sdk.APP_EVENTS_FLUSHED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2351210
    const-string v2, "com.facebook.sdk.APP_EVENTS_NUM_EVENTS_FLUSHED"

    iget v3, v0, LX/GR1;->a:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2351211
    const-string v2, "com.facebook.sdk.APP_EVENTS_FLUSH_RESULT"

    iget-object v0, v0, LX/GR1;->b:LX/GR0;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2351212
    sget-object v0, LX/GR4;->h:Landroid/content/Context;

    invoke-static {v0}, LX/0Xw;->a(Landroid/content/Context;)LX/0Xw;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Xw;->a(Landroid/content/Intent;)Z

    goto/16 :goto_0

    .line 2351213
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 2351214
    :catch_0
    goto :goto_5

    .line 2351215
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :cond_6
    const/4 v1, 0x0

    goto :goto_4
.end method

.method private static g()V
    .locals 7

    .prologue
    const-wide/16 v2, 0x0

    .line 2351160
    sget-object v1, LX/GR4;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 2351161
    :try_start_0
    sget-object v0, LX/GR4;->e:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    if-eqz v0, :cond_0

    .line 2351162
    monitor-exit v1

    .line 2351163
    :goto_0
    return-void

    .line 2351164
    :cond_0
    new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v4, 0x1

    invoke-direct {v0, v4}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    sput-object v0, LX/GR4;->e:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 2351165
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2351166
    new-instance v1, Lcom/facebook/appevents/AppEventsLogger$3;

    invoke-direct {v1}, Lcom/facebook/appevents/AppEventsLogger$3;-><init>()V

    .line 2351167
    sget-object v0, LX/GR4;->e:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const-wide/16 v4, 0xf

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v0 .. v6}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 2351168
    new-instance v1, Lcom/facebook/appevents/AppEventsLogger$4;

    invoke-direct {v1}, Lcom/facebook/appevents/AppEventsLogger$4;-><init>()V

    .line 2351169
    sget-object v0, LX/GR4;->e:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const-wide/32 v4, 0x15180

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v0 .. v6}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->scheduleAtFixedRate(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    goto :goto_0

    .line 2351170
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static h()V
    .locals 5

    .prologue
    .line 2351154
    sget-object v1, LX/GR4;->i:Ljava/lang/Object;

    monitor-enter v1

    .line 2351155
    :try_start_0
    invoke-static {}, LX/GR4;->a()LX/GQy;

    move-result-object v0

    sget-object v2, LX/GQy;->EXPLICIT_ONLY:LX/GQy;

    if-eq v0, v2, :cond_0

    .line 2351156
    invoke-static {}, LX/GR4;->i()I

    move-result v0

    const/16 v2, 0x64

    if-le v0, v2, :cond_0

    .line 2351157
    sget-object v0, LX/GQz;->EVENT_THRESHOLD:LX/GQz;

    .line 2351158
    invoke-static {}, LX/GAK;->d()Ljava/util/concurrent/Executor;

    move-result-object v2

    new-instance v3, Lcom/facebook/appevents/AppEventsLogger$6;

    invoke-direct {v3, v0}, Lcom/facebook/appevents/AppEventsLogger$6;-><init>(LX/GQz;)V

    const v4, 0x76aa24aa

    invoke-static {v2, v3, v4}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2351159
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static i()I
    .locals 4

    .prologue
    .line 2351147
    sget-object v2, LX/GR4;->i:Ljava/lang/Object;

    monitor-enter v2

    .line 2351148
    const/4 v0, 0x0

    .line 2351149
    :try_start_0
    sget-object v1, LX/GR4;->d:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GR3;

    .line 2351150
    invoke-virtual {v0}, LX/GR3;->a()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 2351151
    goto :goto_0

    .line 2351152
    :cond_0
    monitor-exit v2

    return v1

    .line 2351153
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/Double;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2351145
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, p3, v0}, LX/GR4;->a(LX/GR4;Ljava/lang/String;Ljava/lang/Double;Landroid/os/Bundle;Z)V

    .line 2351146
    return-void
.end method
