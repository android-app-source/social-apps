.class public final enum LX/H0f;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/H0f;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/H0f;

.field public static final enum DESCRIPTION:LX/H0f;

.field public static final enum TITLE:LX/H0f;


# instance fields
.field private final fieldType:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2414092
    new-instance v0, LX/H0f;

    const-string v1, "TITLE"

    invoke-direct {v0, v1, v2, v2}, LX/H0f;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/H0f;->TITLE:LX/H0f;

    .line 2414093
    new-instance v0, LX/H0f;

    const-string v1, "DESCRIPTION"

    invoke-direct {v0, v1, v3, v3}, LX/H0f;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/H0f;->DESCRIPTION:LX/H0f;

    .line 2414094
    const/4 v0, 0x2

    new-array v0, v0, [LX/H0f;

    sget-object v1, LX/H0f;->TITLE:LX/H0f;

    aput-object v1, v0, v2

    sget-object v1, LX/H0f;->DESCRIPTION:LX/H0f;

    aput-object v1, v0, v3

    sput-object v0, LX/H0f;->$VALUES:[LX/H0f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2414095
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2414096
    iput p3, p0, LX/H0f;->fieldType:I

    .line 2414097
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/H0f;
    .locals 1

    .prologue
    .line 2414098
    const-class v0, LX/H0f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/H0f;

    return-object v0
.end method

.method public static values()[LX/H0f;
    .locals 1

    .prologue
    .line 2414099
    sget-object v0, LX/H0f;->$VALUES:[LX/H0f;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/H0f;

    return-object v0
.end method


# virtual methods
.method public final toInt()I
    .locals 1

    .prologue
    .line 2414100
    iget v0, p0, LX/H0f;->fieldType:I

    return v0
.end method
