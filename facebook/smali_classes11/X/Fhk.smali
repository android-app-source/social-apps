.class public LX/Fhk;
.super LX/Fhd;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/Fhk;


# instance fields
.field private final e:LX/Fhg;


# direct methods
.method public constructor <init>(LX/11i;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Uh;LX/0ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2273659
    invoke-direct {p0, p1, p2, p4}, LX/Fhd;-><init>(LX/11i;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V

    .line 2273660
    sget v0, LX/2SU;->e:I

    invoke-virtual {p3, v0}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2273661
    new-instance v0, LX/Fhg;

    const v1, 0x7001e

    const-string p1, "GraphSearchRemoteSuggestionsTypeahead"

    invoke-direct {v0, v1, p1}, LX/Fhg;-><init>(ILjava/lang/String;)V

    move-object v0, v0

    .line 2273662
    :goto_0
    iput-object v0, p0, LX/Fhk;->e:LX/Fhg;

    .line 2273663
    return-void

    .line 2273664
    :cond_0
    new-instance v0, LX/Fhg;

    const v1, 0x70013

    const-string p1, "SimpleSearchRemoteSuggestionsTypeahead"

    invoke-direct {v0, v1, p1}, LX/Fhg;-><init>(ILjava/lang/String;)V

    move-object v0, v0

    .line 2273665
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/Fhk;
    .locals 7

    .prologue
    .line 2273666
    sget-object v0, LX/Fhk;->f:LX/Fhk;

    if-nez v0, :cond_1

    .line 2273667
    const-class v1, LX/Fhk;

    monitor-enter v1

    .line 2273668
    :try_start_0
    sget-object v0, LX/Fhk;->f:LX/Fhk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2273669
    if-eqz v2, :cond_0

    .line 2273670
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2273671
    new-instance p0, LX/Fhk;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v4

    check-cast v4, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {p0, v3, v4, v5, v6}, LX/Fhk;-><init>(LX/11i;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Uh;LX/0ad;)V

    .line 2273672
    move-object v0, p0

    .line 2273673
    sput-object v0, LX/Fhk;->f:LX/Fhk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2273674
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2273675
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2273676
    :cond_1
    sget-object v0, LX/Fhk;->f:LX/Fhk;

    return-object v0

    .line 2273677
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2273678
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/Fhg;
    .locals 1

    .prologue
    .line 2273679
    iget-object v0, p0, LX/Fhk;->e:LX/Fhg;

    return-object v0
.end method
