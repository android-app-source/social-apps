.class public final LX/Fzk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseProfilePictureCoreMutationModel$PhotoModel;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Fzo;


# direct methods
.method public constructor <init>(LX/Fzo;)V
    .locals 0

    .prologue
    .line 2308230
    iput-object p1, p0, LX/Fzk;->a:LX/Fzo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2308231
    check-cast p1, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseProfilePictureCoreMutationModel$PhotoModel;

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2308232
    if-nez p1, :cond_1

    :cond_0
    :goto_0
    if-eqz v0, :cond_2

    .line 2308233
    const/4 v0, 0x0

    .line 2308234
    :goto_1
    return-object v0

    .line 2308235
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseProfilePictureCoreMutationModel$PhotoModel;->j()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 2308236
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/timeline/legacycontact/protocol/MemorialContactMutationsModels$MemorializedUserChooseProfilePictureCoreMutationModel$PhotoModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
