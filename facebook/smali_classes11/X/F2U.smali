.class public LX/F2U;
.super LX/1Cv;
.source ""


# instance fields
.field public a:LX/F2L;

.field public b:LX/0gc;

.field private c:LX/F39;

.field public d:Landroid/content/res/Resources;

.field public e:LX/0W9;

.field public f:LX/F4L;

.field public g:Landroid/view/View$OnClickListener;

.field public h:Ljava/lang/String;

.field public i:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

.field private j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/DMB;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0gc;LX/F39;LX/F4L;LX/F2L;Landroid/content/res/Resources;LX/0W9;)V
    .locals 1
    .param p1    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/F39;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2193127
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2193128
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2193129
    iput-object v0, p0, LX/F2U;->j:LX/0Px;

    .line 2193130
    iput-object p1, p0, LX/F2U;->b:LX/0gc;

    .line 2193131
    iput-object p2, p0, LX/F2U;->c:LX/F39;

    .line 2193132
    iput-object p3, p0, LX/F2U;->f:LX/F4L;

    .line 2193133
    iput-object p4, p0, LX/F2U;->a:LX/F2L;

    .line 2193134
    iput-object p5, p0, LX/F2U;->d:Landroid/content/res/Resources;

    .line 2193135
    iput-object p6, p0, LX/F2U;->e:LX/0W9;

    .line 2193136
    new-instance v0, LX/F2S;

    invoke-direct {v0, p0}, LX/F2S;-><init>(LX/F2U;)V

    iput-object v0, p0, LX/F2U;->g:Landroid/view/View$OnClickListener;

    .line 2193137
    return-void
.end method

.method public static b(LX/F2U;)V
    .locals 8

    .prologue
    .line 2193139
    iget-object v0, p0, LX/F2U;->a:LX/F2L;

    iget-object v1, p0, LX/F2U;->c:LX/F39;

    .line 2193140
    iput-object v1, v0, LX/F2L;->j:LX/F39;

    .line 2193141
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2193142
    iput-object v0, p0, LX/F2U;->j:LX/0Px;

    .line 2193143
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2193144
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2193145
    iget-object v1, p0, LX/F2U;->i:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    if-nez v1, :cond_4

    move v1, v2

    :goto_0
    if-nez v1, :cond_0

    iget-object v1, p0, LX/F2U;->i:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->B()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v1

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    if-ne v1, v4, :cond_6

    :cond_0
    move v1, v2

    :goto_1
    if-nez v1, :cond_1

    iget-object v1, p0, LX/F2U;->i:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-static {v1}, LX/F4L;->b(Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2193146
    :cond_1
    :goto_2
    const v5, 0x37a88ba0

    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2193147
    iget-object v3, p0, LX/F2U;->i:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v3}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->w()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    if-nez v3, :cond_a

    :cond_2
    :goto_3
    if-eqz v1, :cond_c

    .line 2193148
    :cond_3
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/F2U;->j:LX/0Px;

    .line 2193149
    const v0, -0x5520985d

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2193150
    return-void

    .line 2193151
    :cond_4
    iget-object v1, p0, LX/F2U;->i:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->o()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    .line 2193152
    if-nez v1, :cond_5

    move v1, v2

    goto :goto_0

    :cond_5
    move v1, v3

    goto :goto_0

    .line 2193153
    :cond_6
    iget-object v1, p0, LX/F2U;->i:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->o()LX/1vs;

    move-result-object v1

    iget-object v4, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2193154
    invoke-virtual {v4, v1, v3}, LX/15i;->j(II)I

    move-result v1

    iget-object v4, p0, LX/F2U;->i:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v4}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->z()I

    move-result v4

    if-ge v1, v4, :cond_7

    move v1, v2

    goto :goto_1

    :cond_7
    move v1, v3

    goto :goto_1

    .line 2193155
    :cond_8
    iget-object v1, p0, LX/F2U;->f:LX/F4L;

    iget-object v4, p0, LX/F2U;->i:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v1, v4}, LX/F4L;->a(Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;)Z

    move-result v1

    if-eqz v1, :cond_9

    iget-object v1, p0, LX/F2U;->d:Landroid/content/res/Resources;

    const v4, 0x7f0831ca

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, LX/F2U;->f:LX/F4L;

    iget-object v6, p0, LX/F2U;->i:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    iget-object v7, p0, LX/F2U;->i:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v7}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->y()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/F4L;->a(Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-virtual {v1, v4, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2193156
    :goto_4
    new-instance v2, LX/F2P;

    sget-object v3, LX/F2X;->c:LX/DML;

    invoke-direct {v2, p0, v3, v1}, LX/F2P;-><init>(LX/F2U;LX/DML;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2193157
    invoke-static {}, LX/F2U;->c()LX/DMB;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 2193158
    :cond_9
    iget-object v1, p0, LX/F2U;->d:Landroid/content/res/Resources;

    const v4, 0x7f0831c9

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v5, p0, LX/F2U;->i:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v5}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->z()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-virtual {v1, v4, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_4

    .line 2193159
    :cond_a
    iget-object v3, p0, LX/F2U;->i:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v3}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->w()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    invoke-static {v4, v3, v2, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v3

    .line 2193160
    if-eqz v3, :cond_b

    invoke-static {v3}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v3

    :goto_5
    if-eqz v3, :cond_2

    move v1, v2

    goto/16 :goto_3

    :cond_b
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v3

    goto :goto_5

    .line 2193161
    :cond_c
    iget-object v1, p0, LX/F2U;->i:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->w()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-static {v3, v1, v2, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v1

    if-eqz v1, :cond_e

    invoke-static {v1}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v1

    :goto_6
    invoke-virtual {v1}, LX/3Sa;->e()LX/3Sh;

    move-result-object v2

    :cond_d
    :goto_7
    invoke-interface {v2}, LX/2sN;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v2}, LX/2sN;->b()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v4, v1, LX/1vs;->b:I

    .line 2193162
    if-eqz v4, :cond_d

    const-class v1, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v3, v4, v6, v1, v5}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v1

    if-eqz v1, :cond_d

    const-class v1, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v3, v4, v6, v1, v5}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->name()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_d

    .line 2193163
    new-instance v1, LX/F2Q;

    sget-object v5, LX/F2X;->a:LX/DML;

    invoke-direct {v1, p0, v5, v3, v4}, LX/F2Q;-><init>(LX/F2U;LX/DML;LX/15i;I)V

    move-object v1, v1

    .line 2193164
    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2193165
    invoke-static {}, LX/F2U;->c()LX/DMB;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_7

    .line 2193166
    :cond_e
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v1

    goto :goto_6
.end method

.method public static c()LX/DMB;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/DMB",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2193138
    new-instance v0, LX/DaP;

    sget-object v1, LX/F2X;->b:LX/DML;

    invoke-direct {v0, v1}, LX/DaP;-><init>(LX/DML;)V

    return-object v0
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2193123
    sget-object v0, LX/F2X;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DML;

    invoke-interface {v0, p2}, LX/DML;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2193124
    check-cast p2, LX/DMB;

    .line 2193125
    invoke-interface {p2, p3}, LX/DMB;->a(Landroid/view/View;)V

    .line 2193126
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2193122
    iget-object v0, p0, LX/F2U;->j:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2193121
    iget-object v0, p0, LX/F2U;->j:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2193120
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 2193119
    sget-object v1, LX/F2X;->d:LX/0Px;

    iget-object v0, p0, LX/F2U;->j:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DMB;

    invoke-interface {v0}, LX/DMB;->a()LX/DML;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2193118
    sget-object v0, LX/F2X;->d:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
