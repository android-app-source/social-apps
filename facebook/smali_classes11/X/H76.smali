.class public final LX/H76;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:J

.field public c:I

.field public d:Z

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "message"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "message"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "photo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "photo"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2427727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2427728
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;
    .locals 15

    .prologue
    .line 2427729
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2427730
    iget-object v1, p0, LX/H76;->a:Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    invoke-virtual {v0, v1}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v1

    .line 2427731
    iget-object v2, p0, LX/H76;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2427732
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v3, p0, LX/H76;->i:LX/15i;

    iget v4, p0, LX/H76;->j:I

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v2, 0x57feaeeb

    invoke-static {v3, v4, v2}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v2

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2427733
    iget-object v2, p0, LX/H76;->k:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2427734
    iget-object v2, p0, LX/H76;->l:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2427735
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    iget-object v3, p0, LX/H76;->m:LX/15i;

    iget v4, p0, LX/H76;->n:I

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const v2, 0x420c3d46

    invoke-static {v3, v4, v2}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v2

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 2427736
    iget-object v2, p0, LX/H76;->o:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 2427737
    iget-object v2, p0, LX/H76;->p:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    .line 2427738
    iget-object v2, p0, LX/H76;->q:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 2427739
    iget-object v2, p0, LX/H76;->r:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 2427740
    const/16 v2, 0x10

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2427741
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2427742
    const/4 v1, 0x1

    iget-wide v2, p0, LX/H76;->b:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2427743
    const/4 v1, 0x2

    iget v2, p0, LX/H76;->c:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/186;->a(III)V

    .line 2427744
    const/4 v1, 0x3

    iget-boolean v2, p0, LX/H76;->d:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 2427745
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2427746
    const/4 v1, 0x5

    iget-boolean v2, p0, LX/H76;->f:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 2427747
    const/4 v1, 0x6

    iget-boolean v2, p0, LX/H76;->g:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 2427748
    const/4 v1, 0x7

    iget-boolean v2, p0, LX/H76;->h:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 2427749
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 2427750
    const/16 v1, 0x9

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 2427751
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 2427752
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 2427753
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 2427754
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 2427755
    const/16 v1, 0xe

    invoke-virtual {v0, v1, v13}, LX/186;->b(II)V

    .line 2427756
    const/16 v1, 0xf

    invoke-virtual {v0, v1, v14}, LX/186;->b(II)V

    .line 2427757
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 2427758
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2427759
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 2427760
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2427761
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2427762
    new-instance v1, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    invoke-direct {v1, v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;-><init>(LX/15i;)V

    .line 2427763
    return-object v1

    .line 2427764
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2427765
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
