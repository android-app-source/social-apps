.class public LX/H1U;
.super LX/H0v;
.source ""


# instance fields
.field public final a:LX/H1b;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/H1T;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/H0w;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/H1b;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/H1b;",
            "Ljava/util/List",
            "<",
            "LX/H1T;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2415422
    invoke-direct {p0, p1}, LX/H0v;-><init>(Ljava/lang/String;)V

    .line 2415423
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/H1U;->g:Ljava/util/Set;

    .line 2415424
    iput-object p2, p0, LX/H1U;->a:LX/H1b;

    .line 2415425
    iput-object p3, p0, LX/H1U;->f:Ljava/util/List;

    .line 2415426
    const-string v0, "QE"

    iput-object v0, p0, LX/H1U;->c:Ljava/lang/CharSequence;

    .line 2415427
    const-string v0, "Universe"

    iput-object v0, p0, LX/H1U;->d:Ljava/lang/CharSequence;

    .line 2415428
    iget-object v0, p0, LX/H1U;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H1T;

    .line 2415429
    iget-object v0, v0, LX/H1T;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 2415430
    iget-object p2, p0, LX/H1U;->g:Ljava/util/Set;

    iget-object p3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-interface {p2, p3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_1

    .line 2415431
    iget-object p2, p0, LX/H1U;->g:Ljava/util/Set;

    iget-object p3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-interface {p2, p3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2415432
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, LX/H0w;

    iget-object v0, v0, LX/H0w;->j:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2415433
    :cond_2
    return-void
.end method

.method public static a$redex0(LX/H1U;Landroid/content/Context;Landroid/view/ViewGroup;)V
    .locals 6

    .prologue
    .line 2415390
    const v0, 0x7f0d1c1b

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    .line 2415391
    invoke-static {p0}, LX/H1U;->h(LX/H1U;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2415392
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LX/H0v;->b()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " (Selected)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2415393
    :goto_0
    const v0, 0x7f0d1c1c

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    .line 2415394
    iget-object v1, p0, LX/H1U;->a:LX/H1b;

    invoke-virtual {v1}, LX/H0v;->b()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2415395
    new-instance v1, LX/H1P;

    invoke-direct {v1, p0, p1}, LX/H1P;-><init>(LX/H1U;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2415396
    const v0, 0x7f0d1c1d

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2415397
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2415398
    iget-object v1, p0, LX/H1U;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/H1T;

    .line 2415399
    new-instance v3, Lcom/facebook/fig/listitem/FigListItem;

    invoke-direct {v3, p1}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;)V

    .line 2415400
    invoke-static {p0}, LX/H1U;->h(LX/H1U;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, LX/H1U;->a:LX/H1b;

    iget-object v4, v4, LX/H1b;->h:Ljava/lang/String;

    iget-object v5, v1, LX/H1T;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, LX/H1U;->a:LX/H1b;

    iget-object v4, v4, LX/H1b;->h:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, LX/H1U;->a:LX/H1b;

    iget-object v4, v4, LX/H1b;->f:Ljava/lang/String;

    iget-object v5, v1, LX/H1T;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_0
    const/4 v4, 0x1

    :goto_2
    move v4, v4

    .line 2415401
    if-eqz v4, :cond_1

    .line 2415402
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v1, LX/H1T;->a:Ljava/lang/String;

    invoke-static {v5}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " (Selected)"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2415403
    :goto_3
    new-instance v4, LX/H1Q;

    invoke-direct {v4, p0, p1, v1, p2}, LX/H1Q;-><init>(LX/H1U;Landroid/content/Context;LX/H1T;Landroid/view/ViewGroup;)V

    invoke-virtual {v3, v4}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2415404
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 2415405
    :cond_1
    iget-object v4, v1, LX/H1T;->a:Ljava/lang/String;

    invoke-static {v4}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    goto :goto_3

    .line 2415406
    :cond_2
    const v0, 0x7f0d1c09

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/button/FigButton;

    .line 2415407
    new-instance v1, LX/H1R;

    invoke-direct {v1, p0, p1, p2}, LX/H1R;-><init>(LX/H1U;Landroid/content/Context;Landroid/view/ViewGroup;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2415408
    invoke-static {p0}, LX/H1U;->h(LX/H1U;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, LX/H1U;->a:LX/H1b;

    iget-object v1, v1, LX/H1b;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x1

    :goto_4
    move v1, v1

    .line 2415409
    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2415410
    const v0, 0x7f0d1c1e

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2415411
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2415412
    iget-object v1, p0, LX/H1U;->g:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/H0w;

    .line 2415413
    new-instance v3, Lcom/facebook/fig/listitem/FigListItem;

    const/4 v4, 0x2

    invoke-direct {v3, p1, v4}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;I)V

    .line 2415414
    invoke-virtual {v1}, LX/H0v;->b()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2415415
    invoke-virtual {v1}, LX/H0v;->d()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    .line 2415416
    invoke-virtual {v1}, LX/H0v;->f()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2415417
    invoke-virtual {v1}, LX/H0w;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/fig/listitem/FigListItem;->setActionText(Ljava/lang/CharSequence;)V

    .line 2415418
    new-instance v4, LX/H1S;

    invoke-direct {v4, p0, p1, v1}, LX/H1S;-><init>(LX/H1U;Landroid/content/Context;LX/H0w;)V

    invoke-virtual {v3, v4}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2415419
    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_5

    .line 2415420
    :cond_3
    return-void

    .line 2415421
    :cond_4
    invoke-virtual {p0}, LX/H0v;->b()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_5
    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_6
    const/4 v1, 0x0

    goto :goto_4
.end method

.method public static h(LX/H1U;)Z
    .locals 2

    .prologue
    .line 2415389
    iget-object v0, p0, LX/H1U;->a:LX/H1b;

    iget-object v0, v0, LX/H1b;->g:Ljava/lang/String;

    iget-object v1, p0, LX/H0v;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/H1U;->a:LX/H1b;

    iget-object v0, v0, LX/H1b;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/H1U;->a:LX/H1b;

    iget-object v0, v0, LX/H1b;->a:Ljava/lang/String;

    iget-object v1, p0, LX/H0v;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2415383
    move-object v0, p1

    check-cast v0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    .line 2415384
    invoke-virtual {v0}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030b27

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2415385
    invoke-static {p0, p1, v0}, LX/H1U;->a$redex0(LX/H1U;Landroid/content/Context;Landroid/view/ViewGroup;)V

    .line 2415386
    return-object v0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 2415388
    invoke-super {p0, p1}, LX/H0v;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/H1U;->a:LX/H1b;

    iget-object v0, v0, LX/H0v;->b:Ljava/lang/String;

    invoke-static {v0}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1}, LX/H0v;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2415387
    iget-object v0, p0, LX/H1U;->a:LX/H1b;

    iget-object v0, v0, LX/H0v;->b:Ljava/lang/String;

    return-object v0
.end method
