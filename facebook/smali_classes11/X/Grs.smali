.class public LX/Grs;
.super LX/Cts;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

.field private final b:LX/GrF;

.field private c:LX/Ctg;


# direct methods
.method public constructor <init>(LX/Ctg;)V
    .locals 2

    .prologue
    .line 2398812
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 2398813
    new-instance v0, LX/Grr;

    invoke-direct {v0, p0}, LX/Grr;-><init>(LX/Grs;)V

    iput-object v0, p0, LX/Grs;->b:LX/GrF;

    .line 2398814
    iput-object p1, p0, LX/Grs;->c:LX/Ctg;

    .line 2398815
    iget-object v0, p0, LX/Grs;->c:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v0

    const v1, 0x7f0d1803

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    iput-object v0, p0, LX/Grs;->a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    .line 2398816
    iget-object v0, p0, LX/Grs;->a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    const v1, 0x40551eb8    # 3.33f

    .line 2398817
    iput v1, v0, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->z:F

    .line 2398818
    iget-object v0, p0, LX/Grs;->c:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v0

    invoke-interface {v0}, LX/Ct1;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;

    .line 2398819
    iget-object v1, p0, LX/Grs;->b:LX/GrF;

    .line 2398820
    iput-object v1, v0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->s:LX/GrF;

    .line 2398821
    iget-object v0, p0, LX/Grs;->c:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v0

    invoke-interface {v0}, LX/Ct1;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a07bd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 2398822
    iget-object v1, p0, LX/Grs;->a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setStrokeColor(I)V

    .line 2398823
    iget-object v0, p0, LX/Grs;->a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setStrokeStyle(Landroid/graphics/Paint$Style;)V

    .line 2398824
    iget-object v0, p0, LX/Grs;->a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setStrokeWidth(F)V

    .line 2398825
    return-void
.end method


# virtual methods
.method public final a(LX/CrS;)V
    .locals 5

    .prologue
    .line 2398826
    invoke-virtual {p0}, LX/Cts;->i()Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v0

    .line 2398827
    iget-object v1, v0, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v0, v1

    .line 2398828
    iget-object v1, p0, LX/Grs;->c:LX/Ctg;

    invoke-interface {v1}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v1

    invoke-interface {v1}, LX/Ct1;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x41800000    # 16.0f

    invoke-static {v1, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    .line 2398829
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget-object v3, p0, LX/Grs;->a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    invoke-virtual {v3}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    .line 2398830
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v3, p0, LX/Grs;->a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    invoke-virtual {v3}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v1, v3

    sub-int/2addr v0, v1

    .line 2398831
    new-instance v1, Landroid/graphics/Rect;

    iget-object v3, p0, LX/Grs;->a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    invoke-virtual {v3}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v2

    iget-object v4, p0, LX/Grs;->a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    invoke-virtual {v4}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-direct {v1, v2, v0, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2398832
    iget-object v0, p0, LX/Grs;->c:LX/Ctg;

    iget-object v2, p0, LX/Grs;->a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    invoke-interface {v0, v2, v1}, LX/Ctg;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2398833
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2398834
    iget-object v0, p0, LX/Grs;->a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    invoke-virtual {v0}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->a()V

    .line 2398835
    return-void
.end method
