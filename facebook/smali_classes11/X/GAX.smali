.class public LX/GAX;
.super Ljava/util/AbstractList;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractList",
        "<",
        "LX/GAU;",
        ">;"
    }
.end annotation


# static fields
.field private static a:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field public b:Landroid/os/Handler;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/GAU;",
            ">;"
        }
    .end annotation
.end field

.field public d:I

.field public final e:Ljava/lang/String;

.field public f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/GA5;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2325688
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, LX/GAX;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2325689
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 2325690
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GAX;->c:Ljava/util/List;

    .line 2325691
    const/4 v0, 0x0

    iput v0, p0, LX/GAX;->d:I

    .line 2325692
    sget-object v0, LX/GAX;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GAX;->e:Ljava/lang/String;

    .line 2325693
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GAX;->f:Ljava/util/List;

    .line 2325694
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GAX;->c:Ljava/util/List;

    .line 2325695
    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "LX/GAU;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2325663
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 2325664
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GAX;->c:Ljava/util/List;

    .line 2325665
    const/4 v0, 0x0

    iput v0, p0, LX/GAX;->d:I

    .line 2325666
    sget-object v0, LX/GAX;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GAX;->e:Ljava/lang/String;

    .line 2325667
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GAX;->f:Ljava/util/List;

    .line 2325668
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/GAX;->c:Ljava/util/List;

    .line 2325669
    return-void
.end method

.method public varargs constructor <init>([LX/GAU;)V
    .locals 1

    .prologue
    .line 2325681
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    .line 2325682
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GAX;->c:Ljava/util/List;

    .line 2325683
    const/4 v0, 0x0

    iput v0, p0, LX/GAX;->d:I

    .line 2325684
    sget-object v0, LX/GAX;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GAX;->e:Ljava/lang/String;

    .line 2325685
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GAX;->f:Ljava/util/List;

    .line 2325686
    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/GAX;->c:Ljava/util/List;

    .line 2325687
    return-void
.end method


# virtual methods
.method public final a(I)LX/GAU;
    .locals 1

    .prologue
    .line 2325680
    iget-object v0, p0, LX/GAX;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GAU;

    return-object v0
.end method

.method public final add(ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2325677
    check-cast p2, LX/GAU;

    .line 2325678
    iget-object v0, p0, LX/GAX;->c:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 2325679
    return-void
.end method

.method public final add(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2325696
    check-cast p1, LX/GAU;

    .line 2325697
    iget-object v0, p0, LX/GAX;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final clear()V
    .locals 1

    .prologue
    .line 2325675
    iget-object v0, p0, LX/GAX;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2325676
    return-void
.end method

.method public final synthetic get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2325674
    invoke-virtual {p0, p1}, LX/GAX;->a(I)LX/GAU;

    move-result-object v0

    return-object v0
.end method

.method public final remove(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2325673
    iget-object v0, p0, LX/GAX;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GAU;

    return-object v0
.end method

.method public final set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2325671
    check-cast p2, LX/GAU;

    .line 2325672
    iget-object v0, p0, LX/GAX;->c:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GAU;

    return-object v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2325670
    iget-object v0, p0, LX/GAX;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
