.class public final LX/Fxe;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field private A:I

.field public final synthetic a:LX/Fxf;

.field private final b:Landroid/view/View;

.field private c:F

.field public d:Z

.field public e:F

.field public f:F

.field public g:Z

.field public h:F

.field public i:F

.field public j:Z

.field public k:F

.field public l:F

.field public m:Z

.field public n:F

.field public o:F

.field public p:Z

.field public q:I

.field public r:I

.field public s:Z

.field public t:I

.field public u:I

.field private v:Z

.field private w:I

.field private x:I

.field private y:Z

.field private z:I


# direct methods
.method public constructor <init>(LX/Fxf;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2305656
    iput-object p1, p0, LX/Fxe;->a:LX/Fxf;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 2305657
    iput-object p2, p0, LX/Fxe;->b:Landroid/view/View;

    .line 2305658
    iget-object v0, p1, LX/Fxf;->p:Landroid/view/animation/Interpolator;

    invoke-virtual {p0, v0}, LX/Fxe;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2305659
    iget v0, p1, LX/Fxf;->o:I

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, LX/Fxe;->setDuration(J)V

    .line 2305660
    return-void
.end method

.method private static a(FFF)F
    .locals 1

    .prologue
    .line 2305655
    sub-float v0, p1, p0

    mul-float/2addr v0, p2

    add-float/2addr v0, p0

    return v0
.end method

.method private static a(IIF)I
    .locals 2

    .prologue
    .line 2305654
    int-to-float v0, p0

    sub-int v1, p1, p0

    int-to-float v1, v1

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method private a(FZ)V
    .locals 3

    .prologue
    .line 2305643
    if-eqz p2, :cond_0

    iget v0, p0, LX/Fxe;->e:F

    move v1, v0

    .line 2305644
    :goto_0
    if-eqz p2, :cond_1

    iget v0, p0, LX/Fxe;->f:F

    .line 2305645
    :goto_1
    iget v2, p0, LX/Fxe;->c:F

    invoke-static {v1, v0, v2}, LX/Fxe;->a(FFF)F

    move-result v2

    .line 2305646
    invoke-static {v1, v0, p1}, LX/Fxe;->a(FFF)F

    move-result v0

    .line 2305647
    sub-float/2addr v0, v2

    .line 2305648
    if-eqz p2, :cond_2

    .line 2305649
    iget-object v1, p0, LX/Fxe;->b:Landroid/view/View;

    iget-object v2, p0, LX/Fxe;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTranslationX()F

    move-result v2

    add-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 2305650
    :goto_2
    return-void

    .line 2305651
    :cond_0
    iget v0, p0, LX/Fxe;->h:F

    move v1, v0

    goto :goto_0

    .line 2305652
    :cond_1
    iget v0, p0, LX/Fxe;->i:F

    goto :goto_1

    .line 2305653
    :cond_2
    iget-object v1, p0, LX/Fxe;->b:Landroid/view/View;

    iget-object v2, p0, LX/Fxe;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTranslationY()F

    move-result v2

    add-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_2
.end method

.method public static a(LX/Fxe;)V
    .locals 2

    .prologue
    .line 2305640
    iget-boolean v0, p0, LX/Fxe;->j:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/Fxe;->d:Z

    if-eqz v0, :cond_0

    .line 2305641
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "it makes no sense to animate those two together ..."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2305642
    :cond_0
    return-void
.end method

.method public static b(LX/Fxe;)V
    .locals 2

    .prologue
    .line 2305637
    iget-boolean v0, p0, LX/Fxe;->m:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/Fxe;->g:Z

    if-eqz v0, :cond_0

    .line 2305638
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "it makes no sense to animate those two together ..."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2305639
    :cond_0
    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 3

    .prologue
    .line 2305616
    iget-boolean v0, p0, LX/Fxe;->d:Z

    if-eqz v0, :cond_0

    .line 2305617
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, LX/Fxe;->a(FZ)V

    .line 2305618
    :cond_0
    iget-boolean v0, p0, LX/Fxe;->g:Z

    if-eqz v0, :cond_1

    .line 2305619
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Fxe;->a(FZ)V

    .line 2305620
    :cond_1
    iget-boolean v0, p0, LX/Fxe;->j:Z

    if-eqz v0, :cond_2

    .line 2305621
    iget-object v0, p0, LX/Fxe;->b:Landroid/view/View;

    iget v1, p0, LX/Fxe;->k:F

    iget v2, p0, LX/Fxe;->l:F

    invoke-static {v1, v2, p1}, LX/Fxe;->a(FFF)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 2305622
    :cond_2
    iget-boolean v0, p0, LX/Fxe;->m:Z

    if-eqz v0, :cond_3

    .line 2305623
    iget-object v0, p0, LX/Fxe;->b:Landroid/view/View;

    iget v1, p0, LX/Fxe;->n:F

    iget v2, p0, LX/Fxe;->o:F

    invoke-static {v1, v2, p1}, LX/Fxe;->a(FFF)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 2305624
    :cond_3
    iget-object v0, p0, LX/Fxe;->b:Landroid/view/View;

    invoke-static {v0}, LX/Fxf;->d(Landroid/view/View;)Landroid/widget/FrameLayout$LayoutParams;

    move-result-object v0

    .line 2305625
    iget-boolean v1, p0, LX/Fxe;->p:Z

    if-eqz v1, :cond_4

    .line 2305626
    iget v1, p0, LX/Fxe;->q:I

    iget v2, p0, LX/Fxe;->r:I

    invoke-static {v1, v2, p1}, LX/Fxe;->a(IIF)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 2305627
    :cond_4
    iget-boolean v1, p0, LX/Fxe;->s:Z

    if-eqz v1, :cond_5

    .line 2305628
    iget v1, p0, LX/Fxe;->t:I

    iget v2, p0, LX/Fxe;->u:I

    invoke-static {v1, v2, p1}, LX/Fxe;->a(IIF)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 2305629
    :cond_5
    iget-boolean v1, p0, LX/Fxe;->v:Z

    if-eqz v1, :cond_6

    .line 2305630
    iget v1, p0, LX/Fxe;->w:I

    iget v2, p0, LX/Fxe;->x:I

    invoke-static {v1, v2, p1}, LX/Fxe;->a(IIF)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 2305631
    :cond_6
    iget-boolean v1, p0, LX/Fxe;->y:Z

    if-eqz v1, :cond_7

    .line 2305632
    iget v1, p0, LX/Fxe;->z:I

    iget v2, p0, LX/Fxe;->A:I

    invoke-static {v1, v2, p1}, LX/Fxe;->a(IIF)I

    move-result v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->height:I

    .line 2305633
    :cond_7
    iget-boolean v0, p0, LX/Fxe;->p:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, LX/Fxe;->s:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, LX/Fxe;->v:Z

    if-nez v0, :cond_8

    iget-boolean v0, p0, LX/Fxe;->y:Z

    if-eqz v0, :cond_9

    .line 2305634
    :cond_8
    iget-object v0, p0, LX/Fxe;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 2305635
    :cond_9
    iput p1, p0, LX/Fxe;->c:F

    .line 2305636
    return-void
.end method

.method public final c(II)V
    .locals 1

    .prologue
    .line 2305596
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fxe;->v:Z

    .line 2305597
    iput p1, p0, LX/Fxe;->w:I

    .line 2305598
    iput p2, p0, LX/Fxe;->x:I

    .line 2305599
    return-void
.end method

.method public final cancel()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2305605
    invoke-super {p0}, Landroid/view/animation/Animation;->cancel()V

    .line 2305606
    const/4 v0, 0x0

    iput v0, p0, LX/Fxe;->c:F

    .line 2305607
    iput-boolean v1, p0, LX/Fxe;->d:Z

    .line 2305608
    iput-boolean v1, p0, LX/Fxe;->g:Z

    .line 2305609
    iput-boolean v1, p0, LX/Fxe;->j:Z

    .line 2305610
    iput-boolean v1, p0, LX/Fxe;->m:Z

    .line 2305611
    iput-boolean v1, p0, LX/Fxe;->p:Z

    .line 2305612
    iput-boolean v1, p0, LX/Fxe;->s:Z

    .line 2305613
    iput-boolean v1, p0, LX/Fxe;->v:Z

    .line 2305614
    iput-boolean v1, p0, LX/Fxe;->y:Z

    .line 2305615
    return-void
.end method

.method public final d(II)V
    .locals 1

    .prologue
    .line 2305601
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fxe;->y:Z

    .line 2305602
    iput p1, p0, LX/Fxe;->z:I

    .line 2305603
    iput p2, p0, LX/Fxe;->A:I

    .line 2305604
    return-void
.end method

.method public final willChangeBounds()Z
    .locals 1

    .prologue
    .line 2305600
    const/4 v0, 0x1

    return v0
.end method
