.class public LX/FR5;
.super LX/6vv;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6vv",
        "<",
        "Lcom/facebook/payments/history/picker/PaymentHistoryPickerRunTimeData;",
        "Lcom/facebook/payments/history/picker/PaymentHistoryPickerScreenConfig;",
        "Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;",
        "Lcom/facebook/payments/history/picker/PaymentHistoryCoreClientData;",
        "LX/FRJ;",
        ">;"
    }
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2240036
    invoke-direct {p0}, LX/6vv;-><init>()V

    .line 2240037
    return-void
.end method

.method public static a(LX/0QB;)LX/FR5;
    .locals 3

    .prologue
    .line 2240025
    const-class v1, LX/FR5;

    monitor-enter v1

    .line 2240026
    :try_start_0
    sget-object v0, LX/FR5;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2240027
    sput-object v2, LX/FR5;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2240028
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2240029
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2240030
    new-instance v0, LX/FR5;

    invoke-direct {v0}, LX/FR5;-><init>()V

    .line 2240031
    move-object v0, v0

    .line 2240032
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2240033
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FR5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2240034
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2240035
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/picker/model/PickerScreenConfig;)Lcom/facebook/payments/picker/model/PickerRunTimeData;
    .locals 1

    .prologue
    .line 2240023
    check-cast p1, Lcom/facebook/payments/history/picker/PaymentHistoryPickerScreenConfig;

    .line 2240024
    new-instance v0, Lcom/facebook/payments/history/picker/PaymentHistoryPickerRunTimeData;

    invoke-direct {v0, p1}, Lcom/facebook/payments/history/picker/PaymentHistoryPickerRunTimeData;-><init>(Lcom/facebook/payments/history/picker/PaymentHistoryPickerScreenConfig;)V

    return-object v0
.end method

.method public final a(Lcom/facebook/payments/picker/model/PickerScreenConfig;Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;Lcom/facebook/payments/picker/model/CoreClientData;LX/0P1;)Lcom/facebook/payments/picker/model/PickerRunTimeData;
    .locals 1

    .prologue
    .line 2240021
    check-cast p1, Lcom/facebook/payments/history/picker/PaymentHistoryPickerScreenConfig;

    check-cast p2, Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;

    check-cast p3, Lcom/facebook/payments/history/picker/PaymentHistoryCoreClientData;

    .line 2240022
    new-instance v0, Lcom/facebook/payments/history/picker/PaymentHistoryPickerRunTimeData;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/facebook/payments/history/picker/PaymentHistoryPickerRunTimeData;-><init>(Lcom/facebook/payments/history/picker/PaymentHistoryPickerScreenConfig;Lcom/facebook/payments/picker/model/SimplePickerScreenFetcherParams;Lcom/facebook/payments/history/picker/PaymentHistoryCoreClientData;LX/0P1;)V

    return-object v0
.end method
