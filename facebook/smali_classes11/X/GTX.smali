.class public final LX/GTX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;",
        ">;",
        "Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLInterfaces$BackgroundLocationPrivacyPickerOptions;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GTZ;


# direct methods
.method public constructor <init>(LX/GTZ;)V
    .locals 0

    .prologue
    .line 2355579
    iput-object p1, p0, LX/GTX;->a:LX/GTZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2355580
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2355581
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2355582
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2355583
    check-cast v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;

    return-object v0
.end method
