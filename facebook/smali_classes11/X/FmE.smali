.class public final LX/FmE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;)V
    .locals 0

    .prologue
    .line 2282246
    iput-object p1, p0, LX/FmE;->a:Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x23312e2d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2282247
    new-instance v2, Lcom/facebook/privacy/selector/AudienceFragmentDialog;

    invoke-direct {v2}, Lcom/facebook/privacy/selector/AudienceFragmentDialog;-><init>()V

    .line 2282248
    new-instance v0, LX/FmF;

    iget-object v3, p0, LX/FmE;->a:Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;

    invoke-direct {v0, v3}, LX/FmF;-><init>(Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;)V

    .line 2282249
    iput-object v0, v2, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->q:LX/8Q5;

    .line 2282250
    new-instance v0, LX/FmG;

    iget-object v3, p0, LX/FmE;->a:Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;

    invoke-direct {v0, v3}, LX/FmG;-><init>(Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;)V

    invoke-virtual {v2, v0}, Lcom/facebook/privacy/selector/AudienceFragmentDialog;->a(LX/8Qm;)V

    .line 2282251
    iget-object v0, p0, LX/FmE;->a:Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;

    iget-object v0, v0, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;->c:LX/8RJ;

    sget-object v3, LX/8RI;->PAYMENTS_CHECKOUT_ACTIVITY:LX/8RI;

    invoke-virtual {v0, v3}, LX/8RJ;->a(LX/8RI;)V

    .line 2282252
    iget-object v0, p0, LX/FmE;->a:Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;

    invoke-virtual {v0}, Lcom/facebook/socialgood/payments/FundraiserDonationCheckoutPrivacySelectorView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v3, Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0, v3}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/FragmentActivity;

    .line 2282253
    if-eqz v0, :cond_0

    .line 2282254
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->iC_()LX/0gc;

    move-result-object v0

    const-string v3, "FUNDRAISER_DONATION_AUDIENCE_FRAGMENT_TAG"

    invoke-virtual {v2, v0, v3}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2282255
    :cond_0
    const v0, 0x2243df45

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
