.class public final LX/GKM;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GKQ;


# direct methods
.method public constructor <init>(LX/GKQ;)V
    .locals 0

    .prologue
    .line 2340661
    iput-object p1, p0, LX/GKM;->a:LX/GKQ;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2340662
    iget-object v0, p0, LX/GKM;->a:LX/GKQ;

    iget-object v0, v0, LX/GKQ;->m:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Z)V

    .line 2340663
    iget-object v0, p0, LX/GKM;->a:LX/GKQ;

    iget-object v0, v0, LX/GKQ;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;->setIsLoading(Z)V

    .line 2340664
    iget-object v0, p0, LX/GKM;->a:LX/GKQ;

    iget-object v0, v0, LX/GKQ;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;

    .line 2340665
    iget-object v1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;->b:Landroid/view/ViewGroup;

    move-object v0, v1

    .line 2340666
    if-nez p1, :cond_0

    .line 2340667
    iget-object v1, p0, LX/GKM;->a:LX/GKQ;

    iget-object v2, p0, LX/GKM;->a:LX/GKQ;

    invoke-static {v2}, LX/GKQ;->d(LX/GKQ;)Ljava/lang/String;

    move-result-object v2

    .line 2340668
    invoke-static {v1, v2}, LX/GKQ;->a$redex0(LX/GKQ;Ljava/lang/String;)V

    .line 2340669
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2340670
    :goto_0
    return-void

    .line 2340671
    :cond_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 2340672
    iget-object v1, p0, LX/GKM;->a:LX/GKQ;

    iget-object v1, v1, LX/GKQ;->h:LX/GGW;

    invoke-virtual {v1, v0, p1}, LX/GGW;->a(Landroid/view/ViewGroup;Lcom/facebook/graphql/model/FeedUnit;)V

    goto :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2340673
    iget-object v0, p0, LX/GKM;->a:LX/GKQ;

    iget-object v0, v0, LX/GKQ;->m:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Z)V

    .line 2340674
    iget-object v0, p0, LX/GKM;->a:LX/GKQ;

    iget-object v0, v0, LX/GKQ;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesNativePreviewView;->setIsLoading(Z)V

    .line 2340675
    iget-object v0, p0, LX/GKM;->a:LX/GKQ;

    .line 2340676
    invoke-static {v0}, LX/GKQ;->j$redex0(LX/GKQ;)V

    .line 2340677
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2340678
    check-cast p1, Lcom/facebook/graphql/model/FeedUnit;

    invoke-direct {p0, p1}, LX/GKM;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    return-void
.end method
