.class public LX/G62;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2319629
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2319630
    iput-object p1, p0, LX/G62;->a:LX/0Zb;

    .line 2319631
    return-void
.end method

.method public static a(LX/G62;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 2319632
    const-string v0, "sahayta_checklist"

    .line 2319633
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2319634
    iget-object v0, p0, LX/G62;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2319635
    return-void
.end method

.method public static b(LX/0QB;)LX/G62;
    .locals 2

    .prologue
    .line 2319636
    new-instance v1, LX/G62;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/G62;-><init>(LX/0Zb;)V

    .line 2319637
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2319638
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "checklist_mutation_error"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2319639
    const-string v1, "suggestions"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2319640
    invoke-static {p0, v0}, LX/G62;->a(LX/G62;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2319641
    return-void
.end method
