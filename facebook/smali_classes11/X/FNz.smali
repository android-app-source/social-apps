.class public final LX/FNz;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2234050
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2234051
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2234052
    :goto_0
    return v1

    .line 2234053
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2234054
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2234055
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2234056
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2234057
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2234058
    const-string v4, "group_threads"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2234059
    const/4 v3, 0x0

    .line 2234060
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_9

    .line 2234061
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2234062
    :goto_2
    move v2, v3

    .line 2234063
    goto :goto_1

    .line 2234064
    :cond_2
    const-string v4, "id"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2234065
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    .line 2234066
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2234067
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2234068
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2234069
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1

    .line 2234070
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2234071
    :cond_6
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_8

    .line 2234072
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2234073
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2234074
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 2234075
    const-string v5, "edges"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2234076
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2234077
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->START_ARRAY:LX/15z;

    if-ne v4, v5, :cond_7

    .line 2234078
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_ARRAY:LX/15z;

    if-eq v4, v5, :cond_7

    .line 2234079
    const/4 v5, 0x0

    .line 2234080
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v4, v6, :cond_d

    .line 2234081
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2234082
    :goto_5
    move v4, v5

    .line 2234083
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2234084
    :cond_7
    invoke-static {v2, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v2

    move v2, v2

    .line 2234085
    goto :goto_3

    .line 2234086
    :cond_8
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2234087
    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 2234088
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_2

    :cond_9
    move v2, v3

    goto :goto_3

    .line 2234089
    :cond_a
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2234090
    :cond_b
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->END_OBJECT:LX/15z;

    if-eq v6, v7, :cond_c

    .line 2234091
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v6

    .line 2234092
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2234093
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v7, v8, :cond_b

    if-eqz v6, :cond_b

    .line 2234094
    const-string v7, "node"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 2234095
    invoke-static {p0, p1}, LX/FNy;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_6

    .line 2234096
    :cond_c
    const/4 v6, 0x1

    invoke-virtual {p1, v6}, LX/186;->c(I)V

    .line 2234097
    invoke-virtual {p1, v5, v4}, LX/186;->b(II)V

    .line 2234098
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto :goto_5

    :cond_d
    move v4, v5

    goto :goto_6
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    .line 2234099
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2234100
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2234101
    if-eqz v0, :cond_3

    .line 2234102
    const-string v1, "group_threads"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2234103
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2234104
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->g(II)I

    move-result v1

    .line 2234105
    if-eqz v1, :cond_2

    .line 2234106
    const-string v2, "edges"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2234107
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2234108
    const/4 v2, 0x0

    :goto_0
    invoke-virtual {p0, v1}, LX/15i;->c(I)I

    move-result v3

    if-ge v2, v3, :cond_1

    .line 2234109
    invoke-virtual {p0, v1, v2}, LX/15i;->q(II)I

    move-result v3

    .line 2234110
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2234111
    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, LX/15i;->g(II)I

    move-result v4

    .line 2234112
    if-eqz v4, :cond_0

    .line 2234113
    const-string v0, "node"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2234114
    invoke-static {p0, v4, p2, p3}, LX/FNy;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2234115
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2234116
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2234117
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2234118
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2234119
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2234120
    if-eqz v0, :cond_4

    .line 2234121
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2234122
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2234123
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2234124
    return-void
.end method
