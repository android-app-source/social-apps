.class public LX/GJb;
.super LX/GHg;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field private final a:LX/GE7;

.field public final b:LX/1Ck;

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

.field public e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public f:Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;

.field public g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;",
            ">;"
        }
    .end annotation
.end field

.field private h:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private i:LX/Bc9;

.field private j:Landroid/text/SpannableString;


# direct methods
.method public constructor <init>(LX/GE7;LX/1Ck;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/GE7;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2339285
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2339286
    iput-object p1, p0, LX/GJb;->a:LX/GE7;

    .line 2339287
    iput-object p2, p0, LX/GJb;->b:LX/1Ck;

    .line 2339288
    iput-object p3, p0, LX/GJb;->c:LX/0Ot;

    .line 2339289
    invoke-direct {p0}, LX/GJb;->b()V

    .line 2339290
    invoke-direct {p0}, LX/GJb;->c()V

    .line 2339291
    return-void
.end method

.method public static a(LX/0QB;)LX/GJb;
    .locals 3

    .prologue
    .line 2339190
    const-class v1, LX/GJb;

    monitor-enter v1

    .line 2339191
    :try_start_0
    sget-object v0, LX/GJb;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2339192
    sput-object v2, LX/GJb;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2339193
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2339194
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/GJb;->b(LX/0QB;)LX/GJb;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2339195
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GJb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2339196
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2339197
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 2
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2339276
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2339277
    iput-object p2, p0, LX/GJb;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2339278
    iget-object v0, p0, LX/GJb;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    if-eqz v0, :cond_0

    .line 2339279
    iget-object v0, p0, LX/GJb;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setHeaderVisibility(I)V

    .line 2339280
    :cond_0
    iput-object p1, p0, LX/GJb;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;

    .line 2339281
    invoke-direct {p0}, LX/GJb;->d()V

    .line 2339282
    iget-object v0, p0, LX/GJb;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;

    iget-object v1, p0, LX/GJb;->h:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2339283
    iget-object v0, p0, LX/GJb;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;

    iget-object v1, p0, LX/GJb;->i:LX/Bc9;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->setOnRadioButtonCheckedListener(LX/Bc9;)V

    .line 2339284
    return-void
.end method

.method public static a$redex0(LX/GJb;Ljava/lang/Integer;)V
    .locals 5

    .prologue
    .line 2339267
    iget-object v0, p0, LX/GJb;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GJb;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 2339268
    iget-object v0, p0, LX/GJb;->g:Ljava/util/List;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;

    .line 2339269
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2339270
    new-instance v2, LX/GFc;

    const/4 v3, 0x1

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/GFc;-><init>(ZLjava/lang/String;)V

    invoke-virtual {v1, v2}, LX/GCE;->a(LX/8wN;)V

    .line 2339271
    iget-object v1, p0, LX/GJb;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2339272
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v1, v2

    .line 2339273
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2339274
    iput-object v0, v1, Lcom/facebook/adinterfaces/model/CreativeAdModel;->i:Ljava/lang/String;

    .line 2339275
    :cond_0
    return-void
.end method

.method private static b(LX/0QB;)LX/GJb;
    .locals 4

    .prologue
    .line 2339262
    new-instance v2, LX/GJb;

    .line 2339263
    new-instance v3, LX/GE7;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/2U4;->a(LX/0QB;)LX/2U4;

    move-result-object v1

    check-cast v1, LX/2U4;

    invoke-direct {v3, v0, v1}, LX/GE7;-><init>(LX/0tX;LX/2U4;)V

    .line 2339264
    move-object v0, v3

    .line 2339265
    check-cast v0, LX/GE7;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    const/16 v3, 0x259

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, LX/GJb;-><init>(LX/GE7;LX/1Ck;LX/0Ot;)V

    .line 2339266
    return-object v2
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2339260
    new-instance v0, LX/GJY;

    invoke-direct {v0, p0}, LX/GJY;-><init>(LX/GJb;)V

    iput-object v0, p0, LX/GJb;->h:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 2339261
    return-void
.end method

.method public static b(LX/GJb;Z)V
    .locals 5

    .prologue
    .line 2339247
    iget-object v0, p0, LX/GJb;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    if-nez v0, :cond_0

    .line 2339248
    :goto_0
    return-void

    .line 2339249
    :cond_0
    iget-object v0, p0, LX/GJb;->j:Landroid/text/SpannableString;

    if-nez v0, :cond_1

    .line 2339250
    iget-object v0, p0, LX/GJb;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080bb8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2339251
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v1

    .line 2339252
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 2339253
    const/4 v3, 0x5

    iget-object v4, p0, LX/GJb;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/util/Calendar;->add(II)V

    .line 2339254
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2339255
    new-instance v2, LX/47x;

    iget-object v3, p0, LX/GJb;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2339256
    invoke-virtual {v2, v0}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2339257
    const-string v0, "[[date]]"

    invoke-virtual {v2, v0, v1}, LX/47x;->a(Ljava/lang/String;Ljava/lang/CharSequence;)LX/47x;

    .line 2339258
    invoke-virtual {v2}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    iput-object v0, p0, LX/GJb;->j:Landroid/text/SpannableString;

    .line 2339259
    :cond_1
    iget-object v1, p0, LX/GJb;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    if-eqz p1, :cond_2

    iget-object v0, p0, LX/GJb;->j:Landroid/text/SpannableString;

    :goto_1
    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static b(LX/GJb;ZLjava/lang/String;)V
    .locals 3
    .param p1    # Z
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2339231
    if-nez p1, :cond_0

    .line 2339232
    iget-object v0, p0, LX/GJb;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->setOnRadioButtonCheckedListener(LX/Bc9;)V

    .line 2339233
    :cond_0
    iget-boolean v0, p0, LX/GHg;->a:Z

    move v0, v0

    .line 2339234
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/GJb;->g:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 2339235
    iget-object v0, p0, LX/GJb;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;

    iget-object v1, p0, LX/GJb;->g:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->setDiscountTypes(Ljava/util/List;)V

    .line 2339236
    if-nez p2, :cond_3

    .line 2339237
    iget-object v0, p0, LX/GJb;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->a()V

    .line 2339238
    :cond_1
    :goto_0
    if-nez p1, :cond_2

    .line 2339239
    iget-object v0, p0, LX/GJb;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;

    iget-object v1, p0, LX/GJb;->i:LX/Bc9;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->setOnRadioButtonCheckedListener(LX/Bc9;)V

    .line 2339240
    :cond_2
    return-void

    .line 2339241
    :cond_3
    const/4 v0, 0x0

    .line 2339242
    iget-object v1, p0, LX/GJb;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;

    .line 2339243
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/LWIOfferDiscountTypesGraphQLModels$LWIOfferDiscountTypesQueryModel$DiscountsModel;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2339244
    iget-object v0, p0, LX/GJb;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->setRadioChecked(I)V

    goto :goto_0

    .line 2339245
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 2339246
    goto :goto_1
.end method

.method private c()V
    .locals 1

    .prologue
    .line 2339229
    new-instance v0, LX/GJZ;

    invoke-direct {v0, p0}, LX/GJZ;-><init>(LX/GJb;)V

    iput-object v0, p0, LX/GJb;->i:LX/Bc9;

    .line 2339230
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2339214
    iget-object v0, p0, LX/GJb;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GJb;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2339215
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v0, v1

    .line 2339216
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->i:Ljava/lang/String;

    move-object v0, v1

    .line 2339217
    if-eqz v0, :cond_0

    .line 2339218
    iget-object v0, p0, LX/GJb;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->setOfferEnabled(Z)V

    .line 2339219
    iget-object v0, p0, LX/GJb;->g:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/GJb;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2339220
    iget-object v0, p0, LX/GJb;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2339221
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v0, v1

    .line 2339222
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->i:Ljava/lang/String;

    move-object v0, v1

    .line 2339223
    invoke-static {p0, v2, v0}, LX/GJb;->b(LX/GJb;ZLjava/lang/String;)V

    .line 2339224
    :cond_0
    :goto_0
    return-void

    .line 2339225
    :cond_1
    iget-object v0, p0, LX/GJb;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2339226
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v0, v1

    .line 2339227
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->i:Ljava/lang/String;

    move-object v0, v1

    .line 2339228
    invoke-virtual {p0, v2, v0}, LX/GJb;->a(ZLjava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2339210
    invoke-super {p0}, LX/GHg;->a()V

    .line 2339211
    iget-object v0, p0, LX/GJb;->b:LX/1Ck;

    const-string v1, "fetch_offer_discount_types_task_key"

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2339212
    const/4 v0, 0x0

    iput-object v0, p0, LX/GJb;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;

    .line 2339213
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2339209
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;

    invoke-direct {p0, p1, p2}, LX/GJb;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2339206
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2339207
    iput-object p1, p0, LX/GJb;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2339208
    return-void
.end method

.method public final a(ZLjava/lang/String;)V
    .locals 6
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2339198
    iget-object v0, p0, LX/GJb;->b:LX/1Ck;

    const-string v1, "fetch_offer_discount_types_task_key"

    iget-object v2, p0, LX/GJb;->a:LX/GE7;

    iget-object v3, p0, LX/GJb;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c()Ljava/lang/String;

    move-result-object v3

    .line 2339199
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2339200
    new-instance v4, LX/ACA;

    invoke-direct {v4}, LX/ACA;-><init>()V

    move-object v4, v4

    .line 2339201
    const-string v5, "page_id"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    move-object v4, v4

    .line 2339202
    invoke-static {v4}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v4

    move-object v4, v4

    .line 2339203
    iget-object v5, v2, LX/GE7;->a:LX/0tX;

    invoke-virtual {v5, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    move-object v2, v4

    .line 2339204
    new-instance v3, LX/GJa;

    invoke-direct {v3, p0, p1, p2}, LX/GJa;-><init>(LX/GJb;ZLjava/lang/String;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2339205
    return-void
.end method
