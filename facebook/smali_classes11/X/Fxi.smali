.class public final LX/Fxi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ipc/media/MediaItem;

.field public final synthetic b:Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;Lcom/facebook/ipc/media/MediaItem;)V
    .locals 0

    .prologue
    .line 2306059
    iput-object p1, p0, LX/Fxi;->b:Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;

    iput-object p2, p0, LX/Fxi;->a:Lcom/facebook/ipc/media/MediaItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2306060
    iget-object v0, p0, LX/Fxi;->b:Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;

    iget-object v0, v0, Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;->d:LX/8OJ;

    iget-object v1, p0, LX/Fxi;->a:Lcom/facebook/ipc/media/MediaItem;

    iget-object v2, p0, LX/Fxi;->b:Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;

    iget-object v2, v2, Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;->e:Ljava/lang/String;

    const-string v3, "favorite_photos_batch"

    iget-object v4, p0, LX/Fxi;->b:Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;

    .line 2306061
    iget-object v5, v4, Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;->c:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2306062
    iget-boolean p0, v5, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move p0, p0

    .line 2306063
    if-eqz p0, :cond_0

    :goto_0
    move-object v4, v5

    .line 2306064
    sget-object v5, Lcom/facebook/timeline/header/intro/favphotos/edit/UploadFavoritePhotoController;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual/range {v0 .. v5}, LX/8OJ;->a(Lcom/facebook/ipc/media/MediaItem;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method
