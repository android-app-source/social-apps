.class public final enum LX/GmG;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GmG;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GmG;

.field public static final enum INVITE:LX/GmG;

.field public static final enum SMS:LX/GmG;

.field public static final enum UNDO:LX/GmG;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2391680
    new-instance v0, LX/GmG;

    const-string v1, "INVITE"

    invoke-direct {v0, v1, v2}, LX/GmG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GmG;->INVITE:LX/GmG;

    new-instance v0, LX/GmG;

    const-string v1, "UNDO"

    invoke-direct {v0, v1, v3}, LX/GmG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GmG;->UNDO:LX/GmG;

    new-instance v0, LX/GmG;

    const-string v1, "SMS"

    invoke-direct {v0, v1, v4}, LX/GmG;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GmG;->SMS:LX/GmG;

    const/4 v0, 0x3

    new-array v0, v0, [LX/GmG;

    sget-object v1, LX/GmG;->INVITE:LX/GmG;

    aput-object v1, v0, v2

    sget-object v1, LX/GmG;->UNDO:LX/GmG;

    aput-object v1, v0, v3

    sget-object v1, LX/GmG;->SMS:LX/GmG;

    aput-object v1, v0, v4

    sput-object v0, LX/GmG;->$VALUES:[LX/GmG;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2391679
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GmG;
    .locals 1

    .prologue
    .line 2391677
    const-class v0, LX/GmG;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GmG;

    return-object v0
.end method

.method public static values()[LX/GmG;
    .locals 1

    .prologue
    .line 2391678
    sget-object v0, LX/GmG;->$VALUES:[LX/GmG;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GmG;

    return-object v0
.end method
