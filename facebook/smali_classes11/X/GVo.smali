.class public LX/GVo;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "FBUserAgent"
.end annotation


# instance fields
.field private final a:LX/5pY;


# direct methods
.method public constructor <init>(LX/5pY;)V
    .locals 0

    .prologue
    .line 2359950
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2359951
    iput-object p1, p0, LX/GVo;->a:LX/5pY;

    .line 2359952
    return-void
.end method


# virtual methods
.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2359953
    const-string v0, "FBUserAgent"

    return-object v0
.end method

.method public getWebViewLikeUserAgent(Lcom/facebook/react/bridge/Callback;)V
    .locals 11
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2359954
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/GVo;->a:LX/5pY;

    .line 2359955
    const/4 v4, 0x0

    const-string v5, "Mozilla/5.0 (Linux; U; Android %s AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 %s Safari/534.30"

    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    sget-object v7, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    aput-object v7, v6, v3

    const/4 v7, 0x1

    .line 2359956
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    .line 2359957
    const/16 v8, 0x258

    if-lt v3, v8, :cond_1

    const/4 v3, 0x1

    :goto_0
    move v3, v3

    .line 2359958
    if-eqz v3, :cond_0

    const-string v3, ""

    :goto_1
    aput-object v3, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2359959
    invoke-static {v2}, LX/GdI;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 2359960
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 2359961
    invoke-static {v3}, LX/GVp;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2359962
    const-string v6, " ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2359963
    new-instance v6, LX/GVn;

    invoke-direct {v6, v2}, LX/GVn;-><init>(Landroid/content/Context;)V

    .line 2359964
    const/4 v7, 0x0

    const-string v8, "%s/%s;%s/%s;%s/%d;%s/%d;"

    const/16 v9, 0x8

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-string v3, "FBAN"

    aput-object v3, v9, v10

    const/4 v10, 0x1

    .line 2359965
    iget-object v3, v6, LX/GVn;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2359966
    invoke-static {v3}, LX/GVp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v9, v10

    const/4 v10, 0x2

    const-string v3, "FBAV"

    aput-object v3, v9, v10

    const/4 v10, 0x3

    .line 2359967
    iget-object v3, v6, LX/GVn;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2359968
    invoke-static {v3}, LX/GVp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v9, v10

    const/4 v10, 0x4

    const-string v3, "FBBV"

    aput-object v3, v9, v10

    const/4 v10, 0x5

    .line 2359969
    iget v3, v6, LX/GVn;->d:I

    move v3, v3

    .line 2359970
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v9, v10

    const/4 v10, 0x6

    const-string v3, "FBRV"

    aput-object v3, v9, v10

    const/4 v10, 0x7

    .line 2359971
    iget v3, v6, LX/GVn;->e:I

    move v6, v3

    .line 2359972
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v9, v10

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2359973
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "%s/%s;%s/%s;%s/%s;%s/%s;%s/%s;%s/%s:%s;%s/%s;%s/%s;"

    const/16 v8, 0x11

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string v10, "FBLC"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v4}, LX/GVp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v8, v9

    const/4 v4, 0x2

    const-string v9, "FBMF"

    aput-object v9, v8, v4

    const/4 v4, 0x3

    sget-object v9, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-static {v9}, LX/GVp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    const/4 v4, 0x4

    const-string v9, "FBBD"

    aput-object v9, v8, v4

    const/4 v4, 0x5

    sget-object v9, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-static {v9}, LX/GVp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    const/4 v4, 0x6

    const-string v9, "FBDV"

    aput-object v9, v8, v4

    const/4 v4, 0x7

    sget-object v9, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v9}, LX/GVp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    const/16 v4, 0x8

    const-string v9, "FBSV"

    aput-object v9, v8, v4

    const/16 v4, 0x9

    sget-object v9, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-static {v9}, LX/GVp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    const/16 v4, 0xa

    const-string v9, "FBCA"

    aput-object v9, v8, v4

    const/16 v4, 0xb

    sget-object v9, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-static {v9}, LX/GVp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    const/16 v4, 0xc

    sget-object v9, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-static {v9}, LX/GVp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    const/16 v4, 0xd

    const-string v9, "FBDM"

    aput-object v9, v8, v4

    const/16 v4, 0xe

    .line 2359974
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v10

    .line 2359975
    new-instance p0, Landroid/graphics/Point;

    invoke-direct {p0}, Landroid/graphics/Point;-><init>()V

    .line 2359976
    sget v9, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xe

    if-lt v9, v3, :cond_2

    .line 2359977
    const-string v9, "window"

    invoke-virtual {v2, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    invoke-static {v9}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/WindowManager;

    .line 2359978
    invoke-interface {v9}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    invoke-virtual {v9, p0}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2359979
    :goto_2
    new-instance v9, Ljava/lang/StringBuilder;

    const-string v3, "{density="

    invoke-direct {v9, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v10, Landroid/util/DisplayMetrics;->density:F

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ",width="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Landroid/graphics/Point;->x:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ",height="

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Landroid/graphics/Point;->y:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "}"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-object v9, v9

    .line 2359980
    invoke-static {v9}, LX/GVp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    const/16 v4, 0xf

    const-string v9, "FB_FW"

    aput-object v9, v8, v4

    const/16 v4, 0x10

    const-string v9, "1"

    invoke-static {v9}, LX/GVp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v4

    invoke-static {v6, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2359981
    const-string v4, "]"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2359982
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 2359983
    move-object v2, v3

    .line 2359984
    aput-object v2, v0, v1

    invoke-interface {p1, v0}, Lcom/facebook/react/bridge/Callback;->a([Ljava/lang/Object;)V

    .line 2359985
    return-void

    .line 2359986
    :cond_0
    const-string v3, "Mobile"

    goto/16 :goto_1

    :cond_1
    const/4 v3, 0x0

    goto/16 :goto_0

    .line 2359987
    :cond_2
    iget v9, v10, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v9, p0, Landroid/graphics/Point;->x:I

    .line 2359988
    iget v9, v10, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v9, p0, Landroid/graphics/Point;->y:I

    goto :goto_2
.end method
