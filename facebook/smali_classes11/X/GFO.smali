.class public final LX/GFO;
.super LX/8wN;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2332324
    invoke-direct {p0}, LX/8wN;-><init>()V

    .line 2332325
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2332326
    invoke-direct {p0}, LX/8wN;-><init>()V

    .line 2332327
    iput-object p1, p0, LX/GFO;->a:Ljava/lang/String;

    .line 2332328
    iput-object p2, p0, LX/GFO;->b:Ljava/lang/String;

    .line 2332329
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2332330
    invoke-direct {p0}, LX/8wN;-><init>()V

    .line 2332331
    const-class v0, LX/2Oo;

    invoke-static {p1, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/2Oo;

    .line 2332332
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/2Oo;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, LX/2Oo;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2332333
    invoke-virtual {v0}, LX/2Oo;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/GFO;->a:Ljava/lang/String;

    .line 2332334
    invoke-virtual {v0}, LX/2Oo;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GFO;->b:Ljava/lang/String;

    .line 2332335
    :cond_0
    :goto_0
    return-void

    .line 2332336
    :cond_1
    const-class v0, LX/4Ua;

    invoke-static {p1, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/4Ua;

    .line 2332337
    if-eqz v0, :cond_0

    .line 2332338
    iget-object v1, v0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v1, v1

    .line 2332339
    if-eqz v1, :cond_0

    .line 2332340
    iget-object v1, v0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v1, v1

    .line 2332341
    iget-object v1, v1, Lcom/facebook/graphql/error/GraphQLError;->description:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2332342
    iget-object v1, v0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v1, v1

    .line 2332343
    iget-object v1, v1, Lcom/facebook/graphql/error/GraphQLError;->summary:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2332344
    iget-object v1, v0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v1, v1

    .line 2332345
    iget-object v1, v1, Lcom/facebook/graphql/error/GraphQLError;->summary:Ljava/lang/String;

    iput-object v1, p0, LX/GFO;->a:Ljava/lang/String;

    .line 2332346
    iget-object v1, v0, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    move-object v0, v1

    .line 2332347
    iget-object v0, v0, Lcom/facebook/graphql/error/GraphQLError;->description:Ljava/lang/String;

    iput-object v0, p0, LX/GFO;->b:Ljava/lang/String;

    goto :goto_0
.end method
