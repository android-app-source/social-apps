.class public LX/FBa;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile d:LX/FBa;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/2Lx;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2209555
    sget-object v0, LX/0ax;->ae:Ljava/lang/String;

    sput-object v0, LX/FBa;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/2Lx;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2209556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2209557
    iput-object p1, p0, LX/FBa;->b:Landroid/content/Context;

    .line 2209558
    iput-object p2, p0, LX/FBa;->c:LX/2Lx;

    .line 2209559
    return-void
.end method

.method public static a(LX/0QB;)LX/FBa;
    .locals 5

    .prologue
    .line 2209542
    sget-object v0, LX/FBa;->d:LX/FBa;

    if-nez v0, :cond_1

    .line 2209543
    const-class v1, LX/FBa;

    monitor-enter v1

    .line 2209544
    :try_start_0
    sget-object v0, LX/FBa;->d:LX/FBa;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2209545
    if-eqz v2, :cond_0

    .line 2209546
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2209547
    new-instance p0, LX/FBa;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/2Lx;->b(LX/0QB;)LX/2Lx;

    move-result-object v4

    check-cast v4, LX/2Lx;

    invoke-direct {p0, v3, v4}, LX/FBa;-><init>(Landroid/content/Context;LX/2Lx;)V

    .line 2209548
    move-object v0, p0

    .line 2209549
    sput-object v0, LX/FBa;->d:LX/FBa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2209550
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2209551
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2209552
    :cond_1
    sget-object v0, LX/FBa;->d:LX/FBa;

    return-object v0

    .line 2209553
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2209554
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b(LX/FBa;Ljava/lang/String;)Landroid/net/Uri;
    .locals 4

    .prologue
    .line 2209541
    iget-object v0, p0, LX/FBa;->c:LX/2Lx;

    iget-object v1, p0, LX/FBa;->b:Landroid/content/Context;

    const-string v2, "messages/thread/%s"

    invoke-static {p1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/2Lx;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
