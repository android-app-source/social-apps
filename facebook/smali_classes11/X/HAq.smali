.class public final LX/HAq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;)V
    .locals 0

    .prologue
    .line 2436078
    iput-object p1, p0, LX/HAq;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, -0x23141989

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v8

    .line 2436079
    iget-object v1, p0, LX/HAq;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->n:LX/HB7;

    iget-object v2, p0, LX/HAq;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    .line 2436080
    iget-object v3, v2, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->i:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2436081
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v2

    invoke-virtual {v1, v2}, LX/HB7;->e(I)Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    move-result-object v9

    .line 2436082
    if-eqz v9, :cond_1

    .line 2436083
    iget-object v1, p0, LX/HAq;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/9XE;

    iget-object v2, p0, LX/HAq;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    iget-wide v2, v2, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->g:J

    invoke-virtual {v9}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->p()Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    move-result-object v4

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->UNREAD:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    if-ne v4, v6, :cond_2

    move v4, v0

    :goto_0
    invoke-virtual {v9}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->p()Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;->RESPONDED:Lcom/facebook/graphql/enums/GraphQLPageLeadGenInfoState;

    if-ne v6, v7, :cond_0

    move v5, v0

    :cond_0
    iget-object v0, p0, LX/HAq;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HBP;

    invoke-virtual {v9}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->m()J

    move-result-wide v6

    const-wide/16 v10, 0x3e8

    mul-long/2addr v6, v10

    invoke-virtual {v0, v6, v7}, LX/HBP;->a(J)J

    move-result-wide v6

    long-to-int v6, v6

    invoke-virtual {v9}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->o()Ljava/lang/String;

    move-result-object v7

    .line 2436084
    iget-object v0, v1, LX/9XE;->a:LX/0Zb;

    sget-object v10, LX/9X6;->EVENT_ADMIN_CONTACT_INBOX_TAPPED_ONE_REQUEST:LX/9X6;

    invoke-static {v10, v2, v3}, LX/9XE;->c(LX/9X2;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "first_time_opened"

    invoke-virtual {v10, v11, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "is_responded"

    invoke-virtual {v10, v11, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "days_to_expire"

    invoke-virtual {v10, v11, v6}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    const-string v11, "leadgen_id"

    invoke-virtual {v10, v11, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v10

    invoke-interface {v0, v10}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2436085
    iget-object v0, p0, LX/HAq;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    iget-object v1, p0, LX/HAq;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    iget-object v2, p0, LX/HAq;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    iget-wide v2, v2, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->g:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/HAq;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;

    iget-object v3, v3, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxListFragment;->h:Ljava/lang/String;

    .line 2436086
    new-instance v4, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    invoke-direct {v4}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;-><init>()V

    .line 2436087
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 2436088
    const-string v6, "extra_request_key"

    invoke-static {v5, v6, v9}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2436089
    const-string v6, "extra_page_id"

    invoke-virtual {v5, v6, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2436090
    const-string v6, "extra_page_name"

    invoke-virtual {v5, v6, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2436091
    invoke-virtual {v4, v5}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2436092
    move-object v2, v4

    .line 2436093
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v3

    invoke-virtual {v3}, LX/0gc;->a()LX/0hH;

    move-result-object v3

    const v4, 0x7f0400d6

    const v5, 0x7f0400e1

    const v6, 0x7f0400d5

    const v7, 0x7f0400e2

    invoke-virtual {v3, v4, v5, v6, v7}, LX/0hH;->a(IIII)LX/0hH;

    move-result-object v3

    .line 2436094
    iget v4, v1, Landroid/support/v4/app/Fragment;->mFragmentId:I

    move v4, v4

    .line 2436095
    invoke-virtual {v3, v4, v2}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, LX/0hH;->a(Ljava/lang/String;)LX/0hH;

    move-result-object v3

    invoke-virtual {v3}, LX/0hH;->b()I

    .line 2436096
    :cond_1
    const v0, 0x4a805839    # 4205596.5f

    invoke-static {v0, v8}, LX/02F;->a(II)V

    return-void

    :cond_2
    move v4, v5

    .line 2436097
    goto/16 :goto_0
.end method
