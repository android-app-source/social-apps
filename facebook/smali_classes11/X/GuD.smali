.class public final LX/GuD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BWI;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;)V
    .locals 0

    .prologue
    .line 2403142
    iput-object p1, p0, LX/GuD;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/webkit/ValueCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2403163
    const-string v0, ""

    invoke-virtual {p0, p1, v0}, LX/GuD;->b(Landroid/webkit/ValueCallback;Ljava/lang/String;)V

    .line 2403164
    return-void
.end method

.method public final a(Landroid/webkit/ValueCallback;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2403165
    invoke-virtual {p0, p1, p2}, LX/GuD;->b(Landroid/webkit/ValueCallback;Ljava/lang/String;)V

    .line 2403166
    return-void
.end method

.method public final a(Landroid/webkit/ValueCallback;Landroid/webkit/WebChromeClient$FileChooserParams;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<[",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/webkit/WebChromeClient$FileChooserParams;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    const/4 v4, 0x0

    .line 2403150
    iget-object v1, p0, LX/GuD;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v1, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->A:Landroid/webkit/ValueCallback;

    if-eqz v1, :cond_0

    .line 2403151
    iget-object v1, p0, LX/GuD;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v1, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->A:Landroid/webkit/ValueCallback;

    invoke-interface {v1, v4}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    .line 2403152
    iget-object v1, p0, LX/GuD;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403153
    iput-object v4, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->A:Landroid/webkit/ValueCallback;

    .line 2403154
    :cond_0
    iget-object v1, p0, LX/GuD;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403155
    iput-object p1, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->A:Landroid/webkit/ValueCallback;

    .line 2403156
    invoke-virtual {p2}, Landroid/webkit/WebChromeClient$FileChooserParams;->createIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2403157
    :try_start_0
    iget-object v2, p0, LX/GuD;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    const/16 v3, 0xd

    invoke-virtual {v2, v1, v3}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2403158
    :goto_0
    return v0

    .line 2403159
    :catch_0
    iget-object v1, p0, LX/GuD;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403160
    iput-object v4, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->A:Landroid/webkit/ValueCallback;

    .line 2403161
    iget-object v1, p0, LX/GuD;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "Cannot open file chooser"

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2403162
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/webkit/ValueCallback;Ljava/lang/String;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/webkit/ValueCallback",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2403143
    iget-object v0, p0, LX/GuD;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403144
    iput-object p1, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->z:Landroid/webkit/ValueCallback;

    .line 2403145
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2403146
    const-string v1, "android.intent.category.OPENABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 2403147
    invoke-virtual {v0, p2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2403148
    iget-object v1, p0, LX/GuD;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v2, p0, LX/GuD;->a:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    const v3, 0x7f083157

    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2403149
    return-void
.end method
