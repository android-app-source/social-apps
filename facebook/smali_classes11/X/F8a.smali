.class public final LX/F8a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)V
    .locals 0

    .prologue
    .line 2204170
    iput-object p1, p0, LX/F8a;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 2204171
    iget-object v0, p0, LX/F8a;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    invoke-virtual {v0}, LX/F8M;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    .line 2204172
    iget-object v0, p0, LX/F8a;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-boolean v0, v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->L:Z

    if-eqz v0, :cond_0

    .line 2204173
    iget-object v0, p0, LX/F8a;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    iget-object v1, v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->A:Landroid/view/View;

    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2204174
    :cond_0
    return-void

    .line 2204175
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2204176
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2204177
    return-void
.end method
