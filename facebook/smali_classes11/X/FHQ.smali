.class public final enum LX/FHQ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FHQ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FHQ;

.field public static final enum completed:LX/FHQ;

.field public static final enum failure_ignored:LX/FHQ;

.field public static final enum results_discarded:LX/FHQ;

.field public static final enum skipped:LX/FHQ;

.field public static final enum unknown:LX/FHQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2220340
    new-instance v0, LX/FHQ;

    const-string v1, "skipped"

    invoke-direct {v0, v1, v2}, LX/FHQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHQ;->skipped:LX/FHQ;

    new-instance v0, LX/FHQ;

    const-string v1, "completed"

    invoke-direct {v0, v1, v3}, LX/FHQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHQ;->completed:LX/FHQ;

    new-instance v0, LX/FHQ;

    const-string v1, "results_discarded"

    invoke-direct {v0, v1, v4}, LX/FHQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHQ;->results_discarded:LX/FHQ;

    new-instance v0, LX/FHQ;

    const-string v1, "failure_ignored"

    invoke-direct {v0, v1, v5}, LX/FHQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHQ;->failure_ignored:LX/FHQ;

    new-instance v0, LX/FHQ;

    const-string v1, "unknown"

    invoke-direct {v0, v1, v6}, LX/FHQ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHQ;->unknown:LX/FHQ;

    .line 2220341
    const/4 v0, 0x5

    new-array v0, v0, [LX/FHQ;

    sget-object v1, LX/FHQ;->skipped:LX/FHQ;

    aput-object v1, v0, v2

    sget-object v1, LX/FHQ;->completed:LX/FHQ;

    aput-object v1, v0, v3

    sget-object v1, LX/FHQ;->results_discarded:LX/FHQ;

    aput-object v1, v0, v4

    sget-object v1, LX/FHQ;->failure_ignored:LX/FHQ;

    aput-object v1, v0, v5

    sget-object v1, LX/FHQ;->unknown:LX/FHQ;

    aput-object v1, v0, v6

    sput-object v0, LX/FHQ;->$VALUES:[LX/FHQ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2220342
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FHQ;
    .locals 1

    .prologue
    .line 2220343
    const-class v0, LX/FHQ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FHQ;

    return-object v0
.end method

.method public static values()[LX/FHQ;
    .locals 1

    .prologue
    .line 2220344
    sget-object v0, LX/FHQ;->$VALUES:[LX/FHQ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FHQ;

    return-object v0
.end method
