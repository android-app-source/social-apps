.class public LX/F1r;
.super Landroid/widget/LinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

.field public b:Landroid/widget/LinearLayout;

.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/TextView;

.field public e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public f:Lcom/facebook/fbui/widget/megaphone/Megaphone;

.field public g:Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;

.field public h:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

.field public i:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

.field public j:Landroid/widget/TextView;

.field public k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public l:Z

.field public m:Z

.field public n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 2192577
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 2192578
    const/4 v2, 0x1

    .line 2192579
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2192580
    const v1, 0x7f0307dc

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 2192581
    invoke-virtual {p0, v2}, LX/F1r;->setOrientation(I)V

    .line 2192582
    const v0, 0x7f0d14c1

    invoke-virtual {p0, v0}, LX/F1r;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    iput-object v0, p0, LX/F1r;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    .line 2192583
    const v0, 0x7f0d14c2

    invoke-virtual {p0, v0}, LX/F1r;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/F1r;->b:Landroid/widget/LinearLayout;

    .line 2192584
    const v0, 0x7f0d14c6

    invoke-virtual {p0, v0}, LX/F1r;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;

    iput-object v0, p0, LX/F1r;->g:Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;

    .line 2192585
    const v0, 0x7f0d14c5

    invoke-virtual {p0, v0}, LX/F1r;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/megaphone/Megaphone;

    iput-object v0, p0, LX/F1r;->f:Lcom/facebook/fbui/widget/megaphone/Megaphone;

    .line 2192586
    const v0, 0x7f0d2f00

    invoke-virtual {p0, v0}, LX/F1r;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/F1r;->c:Landroid/widget/TextView;

    .line 2192587
    const v0, 0x7f0d2eff

    invoke-virtual {p0, v0}, LX/F1r;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/F1r;->d:Landroid/widget/TextView;

    .line 2192588
    const v0, 0x7f0d14c0

    invoke-virtual {p0, v0}, LX/F1r;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    iput-object v0, p0, LX/F1r;->h:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 2192589
    iget-object v0, p0, LX/F1r;->d:Landroid/widget/TextView;

    sget-object v1, LX/0xq;->ROBOTO:LX/0xq;

    sget-object v2, LX/0xr;->LIGHT:LX/0xr;

    iget-object v3, p0, LX/F1r;->d:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/0xs;->a(Landroid/widget/TextView;LX/0xq;LX/0xr;Landroid/graphics/Typeface;)V

    .line 2192590
    const v0, 0x7f0d2efe

    invoke-virtual {p0, v0}, LX/F1r;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/F1r;->e:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2192591
    const v0, 0x7f0d2efd

    invoke-virtual {p0, v0}, LX/F1r;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    iput-object v0, p0, LX/F1r;->i:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    .line 2192592
    const v0, 0x7f0d2f01

    invoke-virtual {p0, v0}, LX/F1r;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/F1r;->j:Landroid/widget/TextView;

    .line 2192593
    const v0, 0x7f0d14b1

    invoke-virtual {p0, v0}, LX/F1r;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/F1r;->k:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2192594
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2192595
    invoke-virtual {p0}, LX/F1r;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0400f9

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    .line 2192596
    new-instance v0, LX/F1q;

    invoke-direct {v0, p0}, LX/F1q;-><init>(LX/F1r;)V

    invoke-virtual {v1, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2192597
    iget-boolean v0, p0, LX/F1r;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/F1r;->g:Lcom/facebook/goodwill/feed/ui/ThrowbackMegaphone;

    :goto_0
    invoke-virtual {v0, v1}, Lcom/facebook/widget/CustomFrameLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2192598
    return-void

    .line 2192599
    :cond_0
    iget-object v0, p0, LX/F1r;->f:Lcom/facebook/fbui/widget/megaphone/Megaphone;

    goto :goto_0
.end method
