.class public final LX/GXk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/GZ2;

.field public final synthetic b:LX/GXR;

.field public final synthetic c:Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;LX/GZ2;LX/GXR;)V
    .locals 0

    .prologue
    .line 2364492
    iput-object p1, p0, LX/GXk;->c:Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;

    iput-object p2, p0, LX/GXk;->a:LX/GZ2;

    iput-object p3, p0, LX/GXk;->b:LX/GXR;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x332e3e8c

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2364493
    iget-object v1, p0, LX/GXk;->c:Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;

    iget-object v1, v1, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->g:LX/GYY;

    if-eqz v1, :cond_0

    .line 2364494
    iget-object v1, p0, LX/GXk;->c:Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;

    iget-object v1, v1, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->g:LX/GYY;

    iget-object v2, p0, LX/GXk;->a:LX/GZ2;

    invoke-virtual {v2}, LX/1a1;->e()I

    iget-object v2, p0, LX/GXk;->b:LX/GXR;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    .line 2364495
    if-eqz v2, :cond_0

    .line 2364496
    iget-object v4, v2, LX/GXR;->b:LX/GXQ;

    move-object v4, v4

    .line 2364497
    sget-object v5, LX/GXQ;->MEDIA_ITEM:LX/GXQ;

    if-ne v4, v5, :cond_0

    .line 2364498
    iget-object v4, v1, LX/GYY;->a:Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;

    iget-object v5, v4, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->o:LX/GZ4;

    iget-object v6, v1, LX/GYY;->a:Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;

    .line 2364499
    iget-object v4, v2, LX/GXR;->a:Ljava/lang/Object;

    move-object v4, v4

    .line 2364500
    check-cast v4, Lcom/facebook/ipc/media/MediaItem;

    const/4 v2, 0x1

    .line 2364501
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2364502
    new-instance p0, LX/5Rw;

    invoke-direct {p0}, LX/5Rw;-><init>()V

    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->e()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {v4}, Lcom/facebook/ipc/media/MediaItem;->d()Lcom/facebook/ipc/media/MediaIdKey;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/media/MediaIdKey;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, LX/5Rw;->a(Landroid/net/Uri;Ljava/lang/String;)LX/5Rw;

    move-result-object p0

    sget-object p1, LX/5Rr;->DOODLE:LX/5Rr;

    invoke-virtual {p0, p1}, LX/5Rw;->a(LX/5Rr;)LX/5Rw;

    move-result-object p0

    sget-object p1, LX/5Rr;->TEXT:LX/5Rr;

    invoke-virtual {p0, p1}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object p0

    sget-object p1, LX/5Rq;->DEFAULT_CROP:LX/5Rq;

    invoke-virtual {p0, p1}, LX/5Rw;->a(LX/5Rq;)LX/5Rw;

    move-result-object p0

    .line 2364503
    iput-boolean v2, p0, LX/5Rw;->f:Z

    .line 2364504
    move-object p0, p0

    .line 2364505
    iput-boolean v2, p0, LX/5Rw;->i:Z

    .line 2364506
    move-object p0, p0

    .line 2364507
    invoke-virtual {p0}, LX/5Rw;->a()Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    move-result-object p0

    .line 2364508
    invoke-virtual {v6}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p1

    sget-object v1, LX/5Rz;->PRODUCT_ITEM:LX/5Rz;

    invoke-virtual {v1}, LX/5Rz;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1, p0}, LX/5Rs;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)Landroid/content/Intent;

    move-result-object p0

    .line 2364509
    iput-object v4, v5, LX/GZ4;->a:Lcom/facebook/ipc/media/MediaItem;

    .line 2364510
    iget-object p1, v5, LX/GZ4;->b:Lcom/facebook/content/SecureContextHelper;

    const/16 v1, 0x24cc

    invoke-interface {p1, p0, v1, v6}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2364511
    :cond_0
    const v1, 0x30236811

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
