.class public LX/GyV;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public final a:Landroid/widget/TextView;

.field public final b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2410182
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/GyV;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2410183
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2410184
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/GyV;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2410185
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2410186
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2410187
    const v0, 0x7f030a4b

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2410188
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/GyV;->setOrientation(I)V

    .line 2410189
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, LX/GyV;->setGravity(I)V

    .line 2410190
    const v0, 0x7f0207fb

    invoke-virtual {p0, v0}, LX/GyV;->setBackgroundResource(I)V

    .line 2410191
    const v0, 0x7f0d19f7

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/GyV;->a:Landroid/widget/TextView;

    .line 2410192
    const v0, 0x7f0d19f8

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/GyV;->b:Landroid/widget/TextView;

    .line 2410193
    return-void
.end method
