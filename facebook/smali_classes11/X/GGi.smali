.class public final LX/GGi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GGh;


# instance fields
.field public final synthetic a:LX/GGl;


# direct methods
.method public constructor <init>(LX/GGl;)V
    .locals 0

    .prologue
    .line 2334457
    iput-object p1, p0, LX/GGi;->a:LX/GGl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 2334458
    sget-object v0, LX/GGj;->a:[I

    iget-object v1, p0, LX/GGi;->a:LX/GGl;

    iget-object v1, v1, LX/GGl;->o:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    .line 2334459
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v1, v2

    .line 2334460
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2334461
    :goto_0
    return-void

    .line 2334462
    :pswitch_0
    iget-object v0, p0, LX/GGi;->a:LX/GGl;

    iget-object v0, v0, LX/GGl;->o:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2334463
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v0, v1

    .line 2334464
    if-nez v0, :cond_0

    .line 2334465
    iget-object v0, p0, LX/GGi;->a:LX/GGl;

    iget-object v0, v0, LX/GGl;->l:LX/GDk;

    iget-object v1, p0, LX/GGi;->a:LX/GGl;

    iget-object v1, v1, LX/GGl;->o:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    new-instance v2, LX/GGg;

    invoke-direct {v2, p0}, LX/GGg;-><init>(LX/GGi;)V

    invoke-virtual {v0, v1, p1, v2}, LX/GDk;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;LX/GDi;)V

    goto :goto_0

    .line 2334466
    :cond_0
    iget-object v0, p0, LX/GGi;->a:LX/GGl;

    iget-object v0, v0, LX/GGl;->m:LX/GDp;

    iget-object v1, p0, LX/GGi;->a:LX/GGl;

    iget-object v1, v1, LX/GGl;->o:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2334467
    iput-object v1, v0, LX/GDp;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2334468
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v2

    .line 2334469
    iget-object v3, v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->q:Ljava/lang/String;

    move-object v2, v3

    .line 2334470
    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v2

    .line 2334471
    iget-object v3, v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->q:Ljava/lang/String;

    move-object v2, v3

    .line 2334472
    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2334473
    :cond_1
    iget-object v2, v0, LX/GDp;->d:LX/GF4;

    new-instance v3, LX/GFP;

    const v4, 0x7f080b83

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, LX/GFP;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0b4;->a(LX/0b7;)V

    .line 2334474
    :goto_1
    goto :goto_0

    .line 2334475
    :pswitch_1
    iget-object v0, p0, LX/GGi;->a:LX/GGl;

    invoke-virtual {v0}, LX/GGl;->d()V

    goto :goto_0

    .line 2334476
    :cond_2
    new-instance v2, LX/4Ca;

    invoke-direct {v2}, LX/4Ca;-><init>()V

    .line 2334477
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v3

    .line 2334478
    iget-object v4, v3, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v3, v4

    .line 2334479
    const-string v4, "ad_audience_id"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2334480
    iget-object v3, v0, LX/GDp;->f:LX/GNL;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/GNL;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)LX/4Ch;

    move-result-object v3

    .line 2334481
    const-string v4, "target_spec"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2334482
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v3

    .line 2334483
    iget-object v4, v3, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->q:Ljava/lang/String;

    move-object v3, v4

    .line 2334484
    const-string v4, "name"

    invoke-virtual {v2, v4, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2334485
    new-instance v3, LX/4At;

    const v4, 0x7f080b7e

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, p1, v4}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v3, v0, LX/GDp;->a:LX/4At;

    .line 2334486
    iget-object v3, v0, LX/GDp;->a:LX/4At;

    invoke-virtual {v3}, LX/4At;->beginShowingProgress()V

    .line 2334487
    iget-object v3, v0, LX/GDp;->b:LX/1Ck;

    sget-object v4, LX/GDo;->EDIT_SAVED_AUDIENCE:LX/GDo;

    iget-object p0, v0, LX/GDp;->c:LX/0tX;

    .line 2334488
    new-instance v1, LX/ABA;

    invoke-direct {v1}, LX/ABA;-><init>()V

    move-object v1, v1

    .line 2334489
    const-string p1, "input"

    invoke-virtual {v1, p1, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v1

    check-cast v1, LX/ABA;

    move-object v2, v1

    .line 2334490
    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    new-instance p0, LX/GDn;

    invoke-direct {p0, v0}, LX/GDn;-><init>(LX/GDp;)V

    invoke-virtual {v3, v4, v2, p0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
