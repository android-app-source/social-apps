.class public final LX/FBW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/List",
        "<",
        "LX/FBS;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;)V
    .locals 0

    .prologue
    .line 2209449
    iput-object p1, p0, LX/FBW;->a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2209450
    iget-object v0, p0, LX/FBW;->a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    invoke-static {v0}, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->c(Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;)V

    .line 2209451
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2209452
    check-cast p1, Ljava/util/List;

    const/16 v2, 0x8

    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 2209453
    iget-object v0, p0, LX/FBW;->a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2209454
    :cond_0
    :goto_0
    return-void

    .line 2209455
    :cond_1
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2209456
    :cond_2
    iget-object v0, p0, LX/FBW;->a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    invoke-static {v0}, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->c(Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;)V

    goto :goto_0

    .line 2209457
    :cond_3
    iget-object v0, p0, LX/FBW;->a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    iget-object v0, v0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->k:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2209458
    iget-object v0, p0, LX/FBW;->a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    iget-object v0, v0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->i:Lcom/facebook/katana/orca/DiodeUnreadThreadView;

    invoke-virtual {v0, v5}, Lcom/facebook/katana/orca/DiodeUnreadThreadView;->setVisibility(I)V

    .line 2209459
    iget-object v0, p0, LX/FBW;->a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    iget-object v1, v0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->i:Lcom/facebook/katana/orca/DiodeUnreadThreadView;

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FBS;

    invoke-virtual {v1, v0}, Lcom/facebook/katana/orca/DiodeUnreadThreadView;->a(LX/FBS;)V

    .line 2209460
    iget-object v0, p0, LX/FBW;->a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    iget v0, v0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->l:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    iget-object v0, p0, LX/FBW;->a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    iget v0, v0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->l:I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 2209461
    iget-object v0, p0, LX/FBW;->a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    iget-object v0, v0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->j:Lcom/facebook/katana/orca/DiodeUnreadThreadView;

    invoke-virtual {v0, v5}, Lcom/facebook/katana/orca/DiodeUnreadThreadView;->setVisibility(I)V

    .line 2209462
    iget-object v0, p0, LX/FBW;->a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    iget-object v1, v0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->j:Lcom/facebook/katana/orca/DiodeUnreadThreadView;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FBS;

    invoke-virtual {v1, v0}, Lcom/facebook/katana/orca/DiodeUnreadThreadView;->a(LX/FBS;)V

    goto :goto_0

    .line 2209463
    :cond_4
    iget-object v0, p0, LX/FBW;->a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    iget-object v0, v0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->j:Lcom/facebook/katana/orca/DiodeUnreadThreadView;

    invoke-virtual {v0, v2}, Lcom/facebook/katana/orca/DiodeUnreadThreadView;->setVisibility(I)V

    .line 2209464
    iget-object v0, p0, LX/FBW;->a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    iget v0, v0, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->l:I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 2209465
    iget-object v0, p0, LX/FBW;->a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    const v1, 0x7f0d0cb9

    .line 2209466
    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v2

    move-object v0, v2

    .line 2209467
    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2209468
    iget-object v1, p0, LX/FBW;->a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    const v2, 0x7f083235

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, LX/FBW;->a:Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;

    iget v4, v4, Lcom/facebook/katana/orca/DiodeUnreadThreadsFragment;->l:I

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2209469
    invoke-virtual {v0, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto/16 :goto_0
.end method
