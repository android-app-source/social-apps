.class public LX/GAz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0SG;

.field public final b:LX/2UC;

.field public final c:LX/2Dt;

.field public d:LX/GaP;

.field public e:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

.field public f:J


# direct methods
.method public constructor <init>(LX/0SG;LX/2UC;LX/GaQ;LX/2Dt;)V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v2, 0xfa

    .line 2326264
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2326265
    iput-object p1, p0, LX/GAz;->a:LX/0SG;

    .line 2326266
    iput-object p2, p0, LX/GAz;->b:LX/2UC;

    .line 2326267
    iput-object p4, p0, LX/GAz;->c:LX/2Dt;

    .line 2326268
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, LX/GaQ;->a(Ljava/lang/Long;Ljava/lang/Long;)LX/GaP;

    move-result-object v0

    iput-object v0, p0, LX/GAz;->d:LX/GaP;

    .line 2326269
    iget-object v0, p0, LX/GAz;->d:LX/GaP;

    new-instance v1, LX/GAy;

    invoke-direct {v1, p0}, LX/GAy;-><init>(LX/GAz;)V

    .line 2326270
    iput-object v1, v0, LX/GaP;->d:LX/GAx;

    .line 2326271
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/GAz;->f:J

    .line 2326272
    return-void
.end method

.method public static a(LX/GAz;J)V
    .locals 1

    .prologue
    .line 2326275
    iput-wide p1, p0, LX/GAz;->f:J

    .line 2326276
    iget-object v0, p0, LX/GAz;->d:LX/GaP;

    invoke-virtual {v0}, LX/GaP;->c()V

    .line 2326277
    return-void
.end method

.method public static d(LX/GAz;)V
    .locals 4

    .prologue
    .line 2326273
    iget-object v0, p0, LX/GAz;->a:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    const-wide/16 v2, 0xfa

    sub-long/2addr v0, v2

    invoke-static {p0, v0, v1}, LX/GAz;->a(LX/GAz;J)V

    .line 2326274
    return-void
.end method
