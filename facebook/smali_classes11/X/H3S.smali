.class public LX/H3S;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static j:LX/H3S;


# instance fields
.field public final a:Lcom/facebook/nearby/common/NearbyTopic;

.field public final b:Lcom/facebook/nearby/common/NearbyTopic;

.field public final c:Lcom/facebook/nearby/common/NearbyTopic;

.field public final d:Lcom/facebook/nearby/common/NearbyTopic;

.field public final e:Lcom/facebook/nearby/common/NearbyTopic;

.field public final f:Lcom/facebook/nearby/common/NearbyTopic;

.field public final g:Lcom/facebook/nearby/common/NearbyTopic;

.field public final h:Lcom/facebook/nearby/common/NearbyTopic;

.field public final i:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/common/NearbyTopic;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 10

    .prologue
    .line 2420996
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2420997
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2420998
    new-instance v1, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Long;

    const/4 v3, 0x0

    const-wide/16 v4, -0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v2

    const v3, 0x7f0820df

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    iput-object v1, p0, LX/H3S;->a:Lcom/facebook/nearby/common/NearbyTopic;

    .line 2420999
    new-instance v1, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Long;

    const/4 v3, 0x0

    const-wide v4, 0xf909a93d1a4bL    # 1.352850004886427E-309

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v2

    const v3, 0x7f0820e1

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    iput-object v1, p0, LX/H3S;->b:Lcom/facebook/nearby/common/NearbyTopic;

    .line 2421000
    new-instance v1, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Long;

    const/4 v3, 0x0

    const-wide v4, 0x75070f9d82c7L

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-wide v4, 0xb3f685ecf5e9L

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v2

    const v3, 0x7f0820e2

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    iput-object v1, p0, LX/H3S;->c:Lcom/facebook/nearby/common/NearbyTopic;

    .line 2421001
    new-instance v1, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Long;

    const/4 v3, 0x0

    const-wide v4, 0x142362b71da64L

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v2

    const v3, 0x7f0820e3

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    iput-object v1, p0, LX/H3S;->d:Lcom/facebook/nearby/common/NearbyTopic;

    .line 2421002
    new-instance v1, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Long;

    const/4 v3, 0x0

    const-wide v4, 0xf80629aea174L

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v2

    const v3, 0x7f0820e4

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    iput-object v1, p0, LX/H3S;->e:Lcom/facebook/nearby/common/NearbyTopic;

    .line 2421003
    new-instance v1, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Long;

    const/4 v3, 0x0

    const-wide v4, 0x795c29b08c39L

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v2

    const v3, 0x7f0820e5

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    iput-object v1, p0, LX/H3S;->f:Lcom/facebook/nearby/common/NearbyTopic;

    .line 2421004
    new-instance v1, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Long;

    const/4 v3, 0x0

    const-wide v4, 0x9560d20fa359L

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v2

    const v3, 0x7f0820e6

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    iput-object v1, p0, LX/H3S;->g:Lcom/facebook/nearby/common/NearbyTopic;

    .line 2421005
    new-instance v1, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Long;

    const/4 v3, 0x0

    const-wide v4, 0xb671e0da2960L

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v2}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v2

    const v3, 0x7f0820e7

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    iput-object v1, p0, LX/H3S;->h:Lcom/facebook/nearby/common/NearbyTopic;

    .line 2421006
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    .line 2421007
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2421008
    new-instance v3, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    const-wide v6, 0xf909a93d1a4bL    # 1.352850004886427E-309

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    const v5, 0x7f0820e9

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421009
    new-instance v3, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    const-wide v6, 0x883ecdfa361aL

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    const v5, 0x7f0820ec

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421010
    new-instance v3, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    const-wide v6, 0xaad3ff4ab37eL

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    const v5, 0x7f0820ed

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421011
    new-instance v3, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    const-wide v6, 0x88e8ee17f853L

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    const v5, 0x7f0820ee

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421012
    new-instance v3, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    const-wide v6, 0xaeb8addcf006L

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    const v5, 0x7f0820ef

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421013
    new-instance v3, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    const-wide v6, 0xb3f685ecf5e9L

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    const v5, 0x7f0820f0

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421014
    new-instance v3, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    const-wide v6, 0x9eb1300d9230L

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    const v5, 0x7f0820f1

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421015
    new-instance v3, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    const-wide v6, 0xab49fcef9a31L

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    const v5, 0x7f0820f2

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421016
    new-instance v3, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    const-wide v6, 0x99aeeb3a33c1L

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    const v5, 0x7f0820f3

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421017
    new-instance v3, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    const-wide v6, 0xb6af406fc128L

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-wide v6, 0xc3ce33b9c40dL

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    const v5, 0x7f0820f4

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421018
    new-instance v3, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    const-wide v6, 0x75d0dc8b9db8L

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    const v5, 0x7f0820f5

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421019
    new-instance v3, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    const-wide v6, 0xb049f64258eaL

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    const v5, 0x7f0820f6

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421020
    new-instance v3, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    const-wide v6, 0xb50573791266L

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    const v5, 0x7f0820f7

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421021
    new-instance v3, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    const-wide v6, 0x893d40b55d7aL

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    const v5, 0x7f0820f8

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421022
    new-instance v3, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    const-wide v6, 0xa0f1c9273275L

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    const v5, 0x7f0820f9

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421023
    new-instance v3, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    const-wide v6, 0xb55520f9a4deL

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    const v5, 0x7f0820fa

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421024
    new-instance v3, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    const-wide v6, 0xa3f123bcb665L

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    const v5, 0x7f0820fb

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421025
    new-instance v3, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    const-wide v6, 0xa7c40b18afcfL

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    const v5, 0x7f0820fc

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421026
    new-instance v3, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    const-wide v6, 0xb46a0c5b5be2L    # 9.8006600028193E-310

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    const v5, 0x7f0820fd

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421027
    new-instance v3, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Long;

    const/4 v5, 0x0

    const-wide v6, 0x8d18a0e1b0e5L

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-wide v6, 0xbff607e95c4dL

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v4

    const v5, 0x7f0820fe

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421028
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2421029
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2421030
    new-instance v4, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Long;

    const/4 v6, 0x0

    const-wide v8, 0x795c29b08c39L

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v5}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v5

    const v6, 0x7f0820ea

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421031
    new-instance v4, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Long;

    const/4 v6, 0x0

    const-wide v8, 0xb3851986cd34L

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v5}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v5

    const v6, 0x7f082110

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421032
    new-instance v4, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Long;

    const/4 v6, 0x0

    const-wide v8, 0xaf167bc6c9d8L

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v5}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v5

    const v6, 0x7f082111

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421033
    new-instance v4, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Long;

    const/4 v6, 0x0

    const-wide v8, 0xb3e9eeb48f81L

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v5}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v5

    const v6, 0x7f082112

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421034
    new-instance v4, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Long;

    const/4 v6, 0x0

    const-wide v8, 0xa3a85858cbccL

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-wide v8, 0x5e132f077c31L

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-wide v8, 0xa55c35397073L

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const-wide v8, 0xbfcbc70f5848L

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const-wide v8, 0xbc878bd58856L

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v5}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v5

    const v6, 0x7f082113

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421035
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2421036
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2421037
    new-instance v5, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Long;

    const/4 v7, 0x0

    const-wide v8, 0xb671e0da2960L

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v6

    const v7, 0x7f0820eb

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421038
    new-instance v5, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Long;

    const/4 v7, 0x0

    const-wide v8, 0x17c1c3f37256fL

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v6

    const v7, 0x7f0820ff

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421039
    new-instance v5, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Long;

    const/4 v7, 0x0

    const-wide v8, 0xb3370450c0fbL

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v6

    const v7, 0x7f082100

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421040
    new-instance v5, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Long;

    const/4 v7, 0x0

    const-wide v8, 0xa960441d9688L

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v6

    const v7, 0x7f082101

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421041
    new-instance v5, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Long;

    const/4 v7, 0x0

    const-wide v8, 0x7f2b3d899715L

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v6

    const v7, 0x7f082102

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421042
    new-instance v5, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Long;

    const/4 v7, 0x0

    const-wide v8, 0xb621435798f2L

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v6

    const v7, 0x7f082103

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421043
    new-instance v5, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Long;

    const/4 v7, 0x0

    const-wide v8, 0x13954261447d6L

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v6

    const v7, 0x7f082104

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421044
    new-instance v5, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Long;

    const/4 v7, 0x0

    const-wide v8, 0x8885d7bfff8dL

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v6

    const v7, 0x7f082105

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421045
    new-instance v5, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Long;

    const/4 v7, 0x0

    const-wide v8, 0x941b7de3aedeL

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v6

    const v7, 0x7f082106

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421046
    new-instance v5, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Long;

    const/4 v7, 0x0

    const-wide v8, 0x629d27547a2bL

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v6

    const v7, 0x7f082107

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421047
    new-instance v5, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Long;

    const/4 v7, 0x0

    const-wide v8, 0xab038400a45dL    # 9.2899947907537E-310

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v6

    const v7, 0x7f082108

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421048
    new-instance v5, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Long;

    const/4 v7, 0x0

    const-wide v8, 0x182832e99b8f3L

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v6

    const v7, 0x7f082109

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421049
    new-instance v5, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Long;

    const/4 v7, 0x0

    const-wide v8, 0x644592a3b5a3L

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v6

    const v7, 0x7f08210a

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421050
    new-instance v5, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Long;

    const/4 v7, 0x0

    const-wide v8, 0x639d630798aeL

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v6

    const v7, 0x7f08210b

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421051
    new-instance v5, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Long;

    const/4 v7, 0x0

    const-wide v8, 0x7ea001c3c4aeL

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v6

    const v7, 0x7f08210c

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421052
    new-instance v5, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Long;

    const/4 v7, 0x0

    const-wide v8, 0xaa0f26f27e19L

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v6

    const v7, 0x7f08210d

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421053
    new-instance v5, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Long;

    const/4 v7, 0x0

    const-wide v8, 0xa28242eb716bL

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v6

    const v7, 0x7f08210e

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421054
    new-instance v5, Lcom/facebook/nearby/common/NearbyTopic;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Long;

    const/4 v7, 0x0

    const-wide v8, 0xaa23d0437156L

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v6}, LX/0RA;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v6

    const v7, 0x7f08210f

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/facebook/nearby/common/NearbyTopic;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2421055
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    .line 2421056
    const v5, 0x7f0820e1

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2421057
    const v2, 0x7f0820e5

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2421058
    const v2, 0x7f0820e7

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2421059
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/H3S;->i:LX/0P1;

    .line 2421060
    return-void
.end method
