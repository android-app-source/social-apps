.class public final enum LX/GDj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GDj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GDj;

.field public static final enum CREATE_SAVED_AUDIENCE:LX/GDj;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2330288
    new-instance v0, LX/GDj;

    const-string v1, "CREATE_SAVED_AUDIENCE"

    invoke-direct {v0, v1, v2}, LX/GDj;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GDj;->CREATE_SAVED_AUDIENCE:LX/GDj;

    .line 2330289
    const/4 v0, 0x1

    new-array v0, v0, [LX/GDj;

    sget-object v1, LX/GDj;->CREATE_SAVED_AUDIENCE:LX/GDj;

    aput-object v1, v0, v2

    sput-object v0, LX/GDj;->$VALUES:[LX/GDj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2330285
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GDj;
    .locals 1

    .prologue
    .line 2330286
    const-class v0, LX/GDj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GDj;

    return-object v0
.end method

.method public static values()[LX/GDj;
    .locals 1

    .prologue
    .line 2330287
    sget-object v0, LX/GDj;->$VALUES:[LX/GDj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GDj;

    return-object v0
.end method
