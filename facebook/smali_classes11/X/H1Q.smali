.class public final LX/H1Q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/content/Context;

.field public final synthetic b:LX/H1T;

.field public final synthetic c:Landroid/view/ViewGroup;

.field public final synthetic d:LX/H1U;


# direct methods
.method public constructor <init>(LX/H1U;Landroid/content/Context;LX/H1T;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2415364
    iput-object p1, p0, LX/H1Q;->d:LX/H1U;

    iput-object p2, p0, LX/H1Q;->a:Landroid/content/Context;

    iput-object p3, p0, LX/H1Q;->b:LX/H1T;

    iput-object p4, p0, LX/H1Q;->c:Landroid/view/ViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x7b1155d7

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2415365
    iget-object v0, p0, LX/H1Q;->a:Landroid/content/Context;

    check-cast v0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    .line 2415366
    iget-object v2, p0, LX/H1Q;->d:LX/H1U;

    iget-object v3, p0, LX/H1Q;->b:LX/H1T;

    invoke-virtual {v0, v2, v3}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->a(LX/H1U;LX/H1T;)Z

    move-result v2

    .line 2415367
    if-nez v2, :cond_0

    .line 2415368
    const-string v2, "Failed to update QE override group."

    invoke-virtual {v0, v2}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->a(Ljava/lang/CharSequence;)LX/3oW;

    move-result-object v0

    invoke-virtual {v0}, LX/3oW;->b()V

    .line 2415369
    :goto_0
    const v0, -0x64d72805

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2415370
    :cond_0
    iget-object v0, p0, LX/H1Q;->d:LX/H1U;

    iget-object v2, p0, LX/H1Q;->a:Landroid/content/Context;

    iget-object v3, p0, LX/H1Q;->c:Landroid/view/ViewGroup;

    invoke-static {v0, v2, v3}, LX/H1U;->a$redex0(LX/H1U;Landroid/content/Context;Landroid/view/ViewGroup;)V

    goto :goto_0
.end method
