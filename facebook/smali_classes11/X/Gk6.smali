.class public LX/Gk6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;

.field public b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/Gkn;",
            "Lcom/facebook/groups/groupsections/GroupsSectionInterface;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2388777
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2388778
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Gk6;->b:Ljava/util/HashMap;

    .line 2388779
    iput-object p1, p0, LX/Gk6;->a:Landroid/content/res/Resources;

    .line 2388780
    return-void
.end method

.method public static a(LX/Gkp;)I
    .locals 1

    .prologue
    .line 2388781
    if-eqz p0, :cond_0

    invoke-virtual {p0}, LX/Gkp;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2388782
    :cond_0
    const/4 v0, 0x0

    .line 2388783
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, LX/Gkp;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(I)LX/Gkn;
    .locals 5
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2388784
    invoke-static {}, LX/Gkn;->values()[LX/Gkn;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v1, v3, v2

    .line 2388785
    iget-object v0, p0, LX/Gk6;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkp;

    .line 2388786
    invoke-static {v0}, LX/Gk6;->a(LX/Gkp;)I

    move-result v0

    .line 2388787
    if-ge p1, v0, :cond_0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 2388788
    :goto_1
    return-object v0

    .line 2388789
    :cond_0
    sub-int/2addr p1, v0

    .line 2388790
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2388791
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b(I)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2388792
    invoke-static {}, LX/Gkn;->values()[LX/Gkn;

    move-result-object v4

    array-length v5, v4

    move v3, v2

    move v1, v2

    :goto_0
    if-ge v3, v5, :cond_1

    aget-object v0, v4, v3

    .line 2388793
    if-ne p1, v1, :cond_0

    .line 2388794
    const/4 v0, 0x1

    .line 2388795
    :goto_1
    return v0

    .line 2388796
    :cond_0
    iget-object v6, p0, LX/Gk6;->b:Ljava/util/HashMap;

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkp;

    .line 2388797
    if-eqz v0, :cond_2

    .line 2388798
    invoke-static {v0}, LX/Gk6;->a(LX/Gkp;)I

    move-result v0

    add-int/2addr v0, v1

    .line 2388799
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 2388800
    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public final c(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2388801
    invoke-virtual {p0, p1}, LX/Gk6;->a(I)LX/Gkn;

    move-result-object v0

    .line 2388802
    if-eqz v0, :cond_0

    .line 2388803
    sget-object v1, LX/Gk5;->a:[I

    invoke-virtual {v0}, LX/Gkn;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2388804
    :cond_0
    const-string v0, ""

    :goto_0
    return-object v0

    .line 2388805
    :pswitch_0
    iget-object v0, p0, LX/Gk6;->a:Landroid/content/res/Resources;

    const v1, 0x7f08373b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2388806
    :pswitch_1
    iget-object v0, p0, LX/Gk6;->a:Landroid/content/res/Resources;

    const v1, 0x7f08373a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
