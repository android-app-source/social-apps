.class public final enum LX/FEj;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FEj;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FEj;

.field public static final enum GAME_LIST_DISMISS:LX/FEj;

.field public static final enum GAME_LIST_FETCH:LX/FEj;

.field public static final enum GAME_LIST_SHOW:LX/FEj;

.field public static final enum GAME_SELECT:LX/FEj;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2216622
    new-instance v0, LX/FEj;

    const-string v1, "GAME_LIST_FETCH"

    const-string v2, "game_list_fetch"

    invoke-direct {v0, v1, v3, v2}, LX/FEj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FEj;->GAME_LIST_FETCH:LX/FEj;

    .line 2216623
    new-instance v0, LX/FEj;

    const-string v1, "GAME_LIST_SHOW"

    const-string v2, "game_list_show"

    invoke-direct {v0, v1, v4, v2}, LX/FEj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FEj;->GAME_LIST_SHOW:LX/FEj;

    .line 2216624
    new-instance v0, LX/FEj;

    const-string v1, "GAME_SELECT"

    const-string v2, "game_select"

    invoke-direct {v0, v1, v5, v2}, LX/FEj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FEj;->GAME_SELECT:LX/FEj;

    .line 2216625
    new-instance v0, LX/FEj;

    const-string v1, "GAME_LIST_DISMISS"

    const-string v2, "game_list_dismiss"

    invoke-direct {v0, v1, v6, v2}, LX/FEj;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FEj;->GAME_LIST_DISMISS:LX/FEj;

    .line 2216626
    const/4 v0, 0x4

    new-array v0, v0, [LX/FEj;

    sget-object v1, LX/FEj;->GAME_LIST_FETCH:LX/FEj;

    aput-object v1, v0, v3

    sget-object v1, LX/FEj;->GAME_LIST_SHOW:LX/FEj;

    aput-object v1, v0, v4

    sget-object v1, LX/FEj;->GAME_SELECT:LX/FEj;

    aput-object v1, v0, v5

    sget-object v1, LX/FEj;->GAME_LIST_DISMISS:LX/FEj;

    aput-object v1, v0, v6

    sput-object v0, LX/FEj;->$VALUES:[LX/FEj;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2216627
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2216628
    iput-object p3, p0, LX/FEj;->value:Ljava/lang/String;

    .line 2216629
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FEj;
    .locals 1

    .prologue
    .line 2216630
    const-class v0, LX/FEj;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FEj;

    return-object v0
.end method

.method public static values()[LX/FEj;
    .locals 1

    .prologue
    .line 2216631
    sget-object v0, LX/FEj;->$VALUES:[LX/FEj;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FEj;

    return-object v0
.end method
