.class public final LX/FCt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FCv;

.field public final synthetic b:Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;LX/FCv;)V
    .locals 0

    .prologue
    .line 2210732
    iput-object p1, p0, LX/FCt;->b:Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;

    iput-object p2, p0, LX/FCt;->a:LX/FCv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2210733
    iget-object v0, p0, LX/FCt;->b:Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;

    iget-object v1, p0, LX/FCt;->a:LX/FCv;

    .line 2210734
    invoke-virtual {v1}, LX/FCv;->b()LX/FCa;

    move-result-object v3

    .line 2210735
    iget-object v2, v0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->b:LX/2Vw;

    invoke-virtual {v2, v3}, LX/2Vx;->d(LX/2WG;)Landroid/net/Uri;

    move-result-object v2

    .line 2210736
    if-eqz v2, :cond_0

    .line 2210737
    iget-object v3, v0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->c:LX/FCr;

    .line 2210738
    iget-object p0, v1, LX/FCv;->a:Landroid/net/Uri;

    move-object p0, p0

    .line 2210739
    invoke-virtual {v3, p0, v2}, LX/FCr;->a(Landroid/net/Uri;Landroid/net/Uri;)V

    .line 2210740
    :goto_0
    move-object v0, v2

    .line 2210741
    return-object v0

    .line 2210742
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string p0, "Miss to hit the audio cache. Start downloading "

    invoke-direct {v2, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2210743
    iget-object p0, v3, LX/FCa;->a:Landroid/net/Uri;

    move-object v3, p0

    .line 2210744
    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2210745
    invoke-static {v0, v1}, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->c(Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;LX/FCv;)Landroid/net/Uri;

    move-result-object v2

    .line 2210746
    iget-object v3, v0, Lcom/facebook/messaging/audio/playback/FetchAudioExecutor;->c:LX/FCr;

    .line 2210747
    iget-object p0, v1, LX/FCv;->a:Landroid/net/Uri;

    move-object p0, p0

    .line 2210748
    invoke-virtual {v3, p0, v2}, LX/FCr;->a(Landroid/net/Uri;Landroid/net/Uri;)V

    goto :goto_0
.end method
