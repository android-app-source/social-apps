.class public LX/Fw7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/FwD;

.field public b:LX/Fve;


# direct methods
.method public constructor <init>(LX/FwD;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2302797
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2302798
    sget-object v0, LX/Fve;->UNKNOWN:LX/Fve;

    iput-object v0, p0, LX/Fw7;->b:LX/Fve;

    .line 2302799
    iput-object p1, p0, LX/Fw7;->a:LX/FwD;

    .line 2302800
    return-void
.end method

.method public static a(LX/0QB;)LX/Fw7;
    .locals 4

    .prologue
    .line 2302801
    const-class v1, LX/Fw7;

    monitor-enter v1

    .line 2302802
    :try_start_0
    sget-object v0, LX/Fw7;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2302803
    sput-object v2, LX/Fw7;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2302804
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2302805
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2302806
    new-instance p0, LX/Fw7;

    invoke-static {v0}, LX/FwD;->a(LX/0QB;)LX/FwD;

    move-result-object v3

    check-cast v3, LX/FwD;

    invoke-direct {p0, v3}, LX/Fw7;-><init>(LX/FwD;)V

    .line 2302807
    move-object v0, p0

    .line 2302808
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2302809
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fw7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2302810
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2302811
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/5SB;LX/BQ1;)LX/Fve;
    .locals 2

    .prologue
    .line 2302812
    iget-object v0, p0, LX/Fw7;->b:LX/Fve;

    sget-object v1, LX/Fve;->UNKNOWN:LX/Fve;

    if-ne v0, v1, :cond_0

    .line 2302813
    invoke-virtual {p2}, LX/BPy;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2302814
    sget-object v0, LX/Fve;->UNKNOWN:LX/Fve;

    .line 2302815
    :goto_0
    move-object v0, v0

    .line 2302816
    iput-object v0, p0, LX/Fw7;->b:LX/Fve;

    .line 2302817
    :cond_0
    iget-object v0, p0, LX/Fw7;->b:LX/Fve;

    return-object v0

    .line 2302818
    :cond_1
    invoke-virtual {p2}, LX/BQ1;->aa()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p2}, LX/BQ1;->ac()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p1, p2}, LX/FwD;->b(LX/5SB;LX/BQ1;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2302819
    sget-object v0, LX/Fve;->EXPANDED:LX/Fve;

    goto :goto_0

    .line 2302820
    :cond_2
    invoke-static {p2}, LX/FwD;->b(LX/BQ1;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, LX/Fve;->COLLAPSED:LX/Fve;

    goto :goto_0

    :cond_3
    sget-object v0, LX/Fve;->EXPANDED:LX/Fve;

    goto :goto_0
.end method
