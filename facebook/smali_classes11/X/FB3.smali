.class public final LX/FB3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Z

.field public final synthetic c:Z

.field public final synthetic d:LX/2CR;


# direct methods
.method public constructor <init>(LX/2CR;Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 2208685
    iput-object p1, p0, LX/FB3;->d:LX/2CR;

    iput-object p2, p0, LX/FB3;->a:Ljava/lang/String;

    iput-boolean p3, p0, LX/FB3;->b:Z

    iput-boolean p4, p0, LX/FB3;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 2208686
    iget-object v0, p0, LX/FB3;->d:LX/2CR;

    iget-object v1, p0, LX/FB3;->a:Ljava/lang/String;

    iget-boolean v2, p0, LX/FB3;->b:Z

    iget-boolean v3, p0, LX/FB3;->c:Z

    .line 2208687
    invoke-virtual {v0}, LX/2CR;->a()V

    .line 2208688
    if-nez v2, :cond_1

    .line 2208689
    new-instance p0, Landroid/content/Intent;

    const-string p1, "android.intent.action.VIEW"

    invoke-direct {p0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object p0

    .line 2208690
    const/high16 p1, 0x10000000

    invoke-virtual {p0, p1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2208691
    iget-object p1, v0, LX/2CR;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object p2, v0, LX/2CR;->c:Landroid/app/Activity;

    invoke-interface {p1, p0, p2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2208692
    :goto_0
    if-eqz v3, :cond_0

    .line 2208693
    iget-object p0, v0, LX/2CR;->c:Landroid/app/Activity;

    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    .line 2208694
    :cond_0
    return-void

    .line 2208695
    :cond_1
    new-instance p0, Landroid/content/Intent;

    iget-object p1, v0, LX/2CR;->c:Landroid/app/Activity;

    iget-object p2, v0, LX/2CR;->g:Ljava/lang/Class;

    invoke-direct {p0, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object p0

    .line 2208696
    iget-object p1, v0, LX/2CR;->d:Lcom/facebook/content/SecureContextHelper;

    iget-object p2, v0, LX/2CR;->c:Landroid/app/Activity;

    invoke-interface {p1, p0, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method
