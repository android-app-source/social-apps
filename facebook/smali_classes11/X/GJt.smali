.class public LX/GJt;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public c:Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;

.field private d:I

.field private e:I

.field public f:J

.field private g:LX/2U3;

.field private h:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2339956
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/GJt;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/2U3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2339953
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2339954
    iput-object p1, p0, LX/GJt;->g:LX/2U3;

    .line 2339955
    return-void
.end method

.method private a(Ljava/lang/String;I)Landroid/text/Spanned;
    .locals 5

    .prologue
    .line 2339952
    iget-object v0, p0, LX/GJt;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f004b

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/text/Spanned;)V
    .locals 2

    .prologue
    .line 2339947
    iget-object v0, p0, LX/GJt;->h:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a()Z

    move-result v0

    .line 2339948
    iget-object v1, p0, LX/GJt;->h:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v1, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    .line 2339949
    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    .line 2339950
    iget-object v0, p0, LX/GJt;->h:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-static {v0}, LX/GMo;->a(Landroid/view/View;)V

    .line 2339951
    :cond_0
    return-void
.end method

.method private a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2339939
    iput-object p1, p0, LX/GJt;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2339940
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i()I

    move-result v0

    iput v0, p0, LX/GJt;->d:I

    .line 2339941
    iget v0, p0, LX/GJt;->d:I

    if-gez v0, :cond_0

    .line 2339942
    iput v1, p0, LX/GJt;->d:I

    .line 2339943
    invoke-virtual {p1, v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(I)V

    .line 2339944
    iget-object v0, p0, LX/GJt;->g:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Negative duration."

    invoke-virtual {v0, v1, v2}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2339945
    :cond_0
    iget-object v0, p0, LX/GJt;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j()I

    move-result v0

    iput v0, p0, LX/GJt;->e:I

    .line 2339946
    return-void
.end method

.method private a(Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 8
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2339905
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2339906
    iput-object p1, p0, LX/GJt;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;

    .line 2339907
    iput-object p2, p0, LX/GJt;->h:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2339908
    invoke-direct {p0}, LX/GJt;->d()V

    .line 2339909
    iget-object v0, p0, LX/GJt;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j()I

    move-result v3

    .line 2339910
    if-nez v3, :cond_0

    .line 2339911
    iget-object v0, p0, LX/GJt;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;

    iget-object v1, p0, LX/GJt;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;

    .line 2339912
    iget-object v2, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->c:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    move-object v1, v2

    .line 2339913
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->d(I)V

    .line 2339914
    :cond_0
    if-eqz v3, :cond_1

    .line 2339915
    iget-object v0, p0, LX/GJt;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;

    iget-object v1, p0, LX/GJt;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;

    .line 2339916
    iget-object v2, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->d:Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    move-object v1, v2

    .line 2339917
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->d(I)V

    .line 2339918
    :cond_1
    iget-object v0, p0, LX/GJt;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2339919
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v1

    .line 2339920
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->F()J

    move-result-wide v0

    int-to-long v4, v3

    const-wide/32 v6, 0x15180

    mul-long/2addr v4, v6

    add-long/2addr v0, v4

    const-wide/16 v4, 0x3e8

    mul-long/2addr v0, v4

    iput-wide v0, p0, LX/GJt;->f:J

    .line 2339921
    iget-object v0, p0, LX/GJt;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;

    iget-wide v4, p0, LX/GJt;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->setCustomDurationDate(Ljava/lang/Long;)V

    .line 2339922
    invoke-direct {p0}, LX/GJt;->f()V

    .line 2339923
    iget-object v0, p0, LX/GJt;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->a()V

    .line 2339924
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    sget-object v0, LX/GJt;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 2339925
    sget-object v0, LX/GJt;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, LX/GJt;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->k()I

    move-result v1

    if-gt v0, v1, :cond_3

    .line 2339926
    iget-object v0, p0, LX/GJt;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->c(I)V

    .line 2339927
    iget-object v0, p0, LX/GJt;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;

    .line 2339928
    iget-object v1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->b:LX/0Px;

    move-object v0, v1

    .line 2339929
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    sget-object v1, LX/GJt;->a:LX/0Px;

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, LX/GJt;->c(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setTextTextViewStart(Ljava/lang/CharSequence;)V

    .line 2339930
    sget-object v0, LX/GJt;->a:LX/0Px;

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v3, v0, :cond_2

    .line 2339931
    iget-object v1, p0, LX/GJt;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;

    iget-object v0, p0, LX/GJt;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;

    .line 2339932
    iget-object v4, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->b:LX/0Px;

    move-object v0, v4

    .line 2339933
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->getId()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->d(I)V

    .line 2339934
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2339935
    :cond_3
    iget-object v0, p0, LX/GJt;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->k()I

    move-result v0

    if-nez v0, :cond_4

    .line 2339936
    iget-object v0, p0, LX/GJt;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->setCustomDurationButtonVisibility(I)V

    .line 2339937
    :cond_4
    invoke-direct {p0}, LX/GJt;->c()V

    .line 2339938
    return-void
.end method

.method private a(II)Z
    .locals 2

    .prologue
    .line 2339904
    if-le p1, p2, :cond_0

    iget-object v0, p0, LX/GJt;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-static {v0}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v0

    iget-object v1, p0, LX/GJt;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->y()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    invoke-static {v1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/GJt;I)V
    .locals 3

    .prologue
    .line 2339957
    iget v0, p0, LX/GJt;->e:I

    if-ne p1, v0, :cond_0

    .line 2339958
    :goto_0
    return-void

    .line 2339959
    :cond_0
    iget-object v0, p0, LX/GJt;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget v1, p0, LX/GJt;->d:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(I)V

    .line 2339960
    iget-object v0, p0, LX/GJt;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2339961
    iput p1, v0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->k:I

    .line 2339962
    iput p1, p0, LX/GJt;->e:I

    .line 2339963
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2339964
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2339965
    iget-object v1, p0, LX/GJt;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0, v1}, LX/GG3;->r(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2339966
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2339967
    new-instance v1, LX/GFM;

    iget v2, p0, LX/GJt;->d:I

    add-int/2addr v2, p1

    invoke-direct {v1, v2}, LX/GFM;-><init>(I)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2339968
    invoke-static {p0}, LX/GJt;->e(LX/GJt;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/GJt;
    .locals 2

    .prologue
    .line 2339902
    new-instance v1, LX/GJt;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v0

    check-cast v0, LX/2U3;

    invoke-direct {v1, v0}, LX/GJt;-><init>(LX/2U3;)V

    .line 2339903
    return-object v1
.end method

.method private b(I)V
    .locals 4

    .prologue
    .line 2339898
    iget-object v0, p0, LX/GJt;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->v()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    .line 2339899
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v0

    iget-object v1, p0, LX/GJt;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v1}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->v()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    invoke-static {v1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v2

    iget-object v1, p0, LX/GJt;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v1}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v1

    invoke-static {v0, v2, v3, v1}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v0

    .line 2339900
    invoke-direct {p0, v0, p1}, LX/GJt;->a(Ljava/lang/String;I)Landroid/text/Spanned;

    move-result-object v0

    invoke-direct {p0, v0}, LX/GJt;->a(Landroid/text/Spanned;)V

    .line 2339901
    return-void
.end method

.method private c(I)Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 2339897
    iget-object v0, p0, LX/GJt;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f004a

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, p1, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 2339894
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2339895
    new-instance v1, LX/GJp;

    invoke-direct {v1, p0}, LX/GJp;-><init>(LX/GJt;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2339896
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 2339892
    iget-object v0, p0, LX/GJt;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;

    new-instance v1, LX/GJq;

    invoke-direct {v1, p0}, LX/GJq;-><init>(LX/GJt;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->setOnCheckChangedListener(LX/Bc9;)V

    .line 2339893
    return-void
.end method

.method public static e(LX/GJt;)V
    .locals 2

    .prologue
    .line 2339887
    iget-object v0, p0, LX/GJt;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget v1, p0, LX/GJt;->d:I

    invoke-static {v0, v1}, LX/GMU;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;I)I

    move-result v0

    .line 2339888
    iget-object v1, p0, LX/GJt;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j()I

    move-result v1

    invoke-direct {p0, v1, v0}, LX/GJt;->a(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2339889
    invoke-direct {p0, v0}, LX/GJt;->b(I)V

    .line 2339890
    :goto_0
    return-void

    .line 2339891
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/GJt;->a(Landroid/text/Spanned;)V

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 2339885
    iget-object v0, p0, LX/GJt;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;

    new-instance v1, LX/GJs;

    invoke-direct {v1, p0}, LX/GJs;-><init>(LX/GJt;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;->setDateOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2339886
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2339884
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;

    invoke-direct {p0, p1, p2}, LX/GJt;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseDurationView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2339883
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-direct {p0, p1}, LX/GJt;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    return-void
.end method
