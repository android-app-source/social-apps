.class public final LX/Gek;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Gel;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/java2js/JSValue;

.field public b:LX/5KI;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2376003
    invoke-static {}, LX/Gel;->q()LX/Gel;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2376004
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2376005
    const-string v0, "CSFIGCardContentLandscapeAttachment"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2376006
    if-ne p0, p1, :cond_1

    .line 2376007
    :cond_0
    :goto_0
    return v0

    .line 2376008
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2376009
    goto :goto_0

    .line 2376010
    :cond_3
    check-cast p1, LX/Gek;

    .line 2376011
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2376012
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2376013
    if-eq v2, v3, :cond_0

    .line 2376014
    iget-object v2, p0, LX/Gek;->a:Lcom/facebook/java2js/JSValue;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Gek;->a:Lcom/facebook/java2js/JSValue;

    iget-object v3, p1, LX/Gek;->a:Lcom/facebook/java2js/JSValue;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2376015
    goto :goto_0

    .line 2376016
    :cond_5
    iget-object v2, p1, LX/Gek;->a:Lcom/facebook/java2js/JSValue;

    if-nez v2, :cond_4

    .line 2376017
    :cond_6
    iget-object v2, p0, LX/Gek;->b:LX/5KI;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/Gek;->b:LX/5KI;

    iget-object v3, p1, LX/Gek;->b:LX/5KI;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2376018
    goto :goto_0

    .line 2376019
    :cond_7
    iget-object v2, p1, LX/Gek;->b:LX/5KI;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
