.class public final LX/Fgk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3TV;


# instance fields
.field public final synthetic a:LX/FhA;

.field public final synthetic b:LX/Fgp;


# direct methods
.method public constructor <init>(LX/Fgp;LX/FhA;)V
    .locals 0

    .prologue
    .line 2270855
    iput-object p1, p0, LX/Fgk;->b:LX/Fgp;

    iput-object p2, p0, LX/Fgk;->a:LX/FhA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;I)Z
    .locals 7

    .prologue
    .line 2270856
    iget-object v0, p0, LX/Fgk;->a:LX/FhA;

    iget-object v1, p0, LX/Fgk;->b:LX/Fgp;

    iget-object v1, v1, LX/Fgp;->p:LX/0g8;

    iget-object v2, p0, LX/Fgk;->b:LX/Fgp;

    iget-object v2, v2, LX/Fgp;->o:LX/1Qq;

    const/4 v5, 0x0

    .line 2270857
    if-nez v1, :cond_0

    move v3, v5

    .line 2270858
    :goto_0
    move v0, v3

    .line 2270859
    return v0

    .line 2270860
    :cond_0
    invoke-interface {v1}, LX/0g8;->t()I

    move-result v3

    sub-int v3, p2, v3

    .line 2270861
    if-ltz v3, :cond_1

    invoke-interface {v2}, LX/1Qq;->getCount()I

    move-result v4

    if-lt v3, v4, :cond_2

    .line 2270862
    :cond_1
    iget-object v4, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v4, v4, Lcom/facebook/search/suggestions/SuggestionsFragment;->w:LX/2Sc;

    sget-object v6, LX/3Ql;->TYPEAHEAD_LONG_PRESS_INDEX_OUT_OF_BOUNDS:LX/3Ql;

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "Long click index out of bounds. Index: "

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string p0, ", Size: "

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v2}, LX/1Qq;->getCount()I

    move-result p0

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v6, v3}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    move v3, v5

    .line 2270863
    goto :goto_0

    .line 2270864
    :cond_2
    invoke-interface {v2}, LX/1Qq;->getCount()I

    move-result v4

    if-lt v3, v4, :cond_3

    move v3, v5

    .line 2270865
    goto :goto_0

    .line 2270866
    :cond_3
    invoke-interface {v2, v3}, LX/1Qq;->getItem(I)Ljava/lang/Object;

    move-result-object v4

    instance-of v4, v4, LX/1Rk;

    if-eqz v4, :cond_4

    .line 2270867
    invoke-interface {v2, v3}, LX/1Qq;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1Rk;

    .line 2270868
    invoke-virtual {v3}, LX/1Rk;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/model/TypeaheadUnit;

    move-object v4, v3

    .line 2270869
    :goto_1
    iget-object v3, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v3, v3, Lcom/facebook/search/suggestions/SuggestionsFragment;->L:LX/2Sd;

    sget-object v6, Lcom/facebook/search/suggestions/SuggestionsFragment;->b:Ljava/lang/String;

    sget-object p0, LX/7CQ;->TYPEAHEAD_UNIT_LONG_CLICKED:LX/7CQ;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, v6, p0, p1}, LX/2Sd;->a(Ljava/lang/String;LX/7CQ;Ljava/lang/String;)V

    .line 2270870
    iget-object v3, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v3, v3, Lcom/facebook/search/suggestions/SuggestionsFragment;->u:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0gh;

    const-string v6, "tap_search_result"

    invoke-virtual {v3, v6}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2270871
    instance-of v3, v4, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    if-eqz v3, :cond_5

    .line 2270872
    iget-object v3, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v4, v0, LX/FhA;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    .line 2270873
    iget-object v5, v4, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v4, v5

    .line 2270874
    new-instance v5, LX/0ht;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, LX/0ht;-><init>(Landroid/content/Context;)V

    .line 2270875
    new-instance v6, LX/Fhm;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v6, p0}, LX/Fhm;-><init>(Landroid/content/Context;)V

    .line 2270876
    iput-object v5, v6, LX/Fhm;->c:LX/0ht;

    .line 2270877
    new-instance p0, LX/Fh0;

    invoke-direct {p0, v3}, LX/Fh0;-><init>(Lcom/facebook/search/suggestions/SuggestionsFragment;)V

    .line 2270878
    iput-object p0, v6, LX/Fhm;->b:LX/Fh0;

    .line 2270879
    invoke-virtual {v5, v6}, LX/0ht;->d(Landroid/view/View;)V

    .line 2270880
    invoke-virtual {v5, v4}, LX/0ht;->c(Landroid/view/View;)V

    .line 2270881
    sget-object v6, LX/3AV;->CENTER:LX/3AV;

    invoke-virtual {v5, v6}, LX/0ht;->a(LX/3AV;)V

    .line 2270882
    invoke-virtual {v5}, LX/0ht;->d()V

    .line 2270883
    const/4 v3, 0x1

    goto/16 :goto_0

    .line 2270884
    :cond_4
    invoke-interface {v2, v3}, LX/1Qq;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/model/TypeaheadUnit;

    move-object v4, v3

    goto :goto_1

    :cond_5
    move v3, v5

    .line 2270885
    goto/16 :goto_0
.end method
