.class public final enum LX/F0I;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/F0I;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/F0I;

.field public static final enum STATUS_SUBSCRIBED_ALL:LX/F0I;

.field public static final enum STATUS_SUBSCRIBED_HIGHLIGHTS:LX/F0I;

.field public static final enum STATUS_UNKNOW:LX/F0I;

.field public static final enum STATUS_UNSUBSCRIBED:LX/F0I;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2188385
    new-instance v0, LX/F0I;

    const-string v1, "STATUS_UNKNOW"

    invoke-direct {v0, v1, v2}, LX/F0I;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F0I;->STATUS_UNKNOW:LX/F0I;

    .line 2188386
    new-instance v0, LX/F0I;

    const-string v1, "STATUS_UNSUBSCRIBED"

    invoke-direct {v0, v1, v3}, LX/F0I;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F0I;->STATUS_UNSUBSCRIBED:LX/F0I;

    .line 2188387
    new-instance v0, LX/F0I;

    const-string v1, "STATUS_SUBSCRIBED_ALL"

    invoke-direct {v0, v1, v4}, LX/F0I;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F0I;->STATUS_SUBSCRIBED_ALL:LX/F0I;

    .line 2188388
    new-instance v0, LX/F0I;

    const-string v1, "STATUS_SUBSCRIBED_HIGHLIGHTS"

    invoke-direct {v0, v1, v5}, LX/F0I;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F0I;->STATUS_SUBSCRIBED_HIGHLIGHTS:LX/F0I;

    .line 2188389
    const/4 v0, 0x4

    new-array v0, v0, [LX/F0I;

    sget-object v1, LX/F0I;->STATUS_UNKNOW:LX/F0I;

    aput-object v1, v0, v2

    sget-object v1, LX/F0I;->STATUS_UNSUBSCRIBED:LX/F0I;

    aput-object v1, v0, v3

    sget-object v1, LX/F0I;->STATUS_SUBSCRIBED_ALL:LX/F0I;

    aput-object v1, v0, v4

    sget-object v1, LX/F0I;->STATUS_SUBSCRIBED_HIGHLIGHTS:LX/F0I;

    aput-object v1, v0, v5

    sput-object v0, LX/F0I;->$VALUES:[LX/F0I;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2188390
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/F0I;
    .locals 1

    .prologue
    .line 2188384
    const-class v0, LX/F0I;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/F0I;

    return-object v0
.end method

.method public static values()[LX/F0I;
    .locals 1

    .prologue
    .line 2188383
    sget-object v0, LX/F0I;->$VALUES:[LX/F0I;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/F0I;

    return-object v0
.end method
