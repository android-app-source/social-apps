.class public final LX/FXj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/FXV;

.field public final b:LX/FXs;

.field public final c:LX/AUC;

.field public final d:LX/FWt;

.field public final e:LX/0kL;

.field public final f:Ljava/lang/String;

.field public final g:LX/FXU;

.field public final h:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public i:I

.field public j:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public k:Lcom/facebook/widget/FbSwipeRefreshLayout;

.field public l:Landroid/support/v7/widget/RecyclerView;

.field public m:LX/FYi;

.field public n:Lcom/facebook/saved2/ui/Saved2DashboardEmptyView;

.field public o:I
    .annotation build Landroid/support/annotation/IdRes;
    .end annotation
.end field

.field public p:I
    .annotation build Landroid/support/annotation/IdRes;
    .end annotation
.end field

.field public q:I
    .annotation build Landroid/support/annotation/LayoutRes;
    .end annotation
.end field

.field public r:LX/BO1;

.field public s:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/FXs;LX/AUC;LX/FWt;LX/0kL;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2255340
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2255341
    sget-object v0, LX/FXV;->a:LX/FXV;

    iput-object v0, p0, LX/FXj;->a:LX/FXV;

    .line 2255342
    iput v1, p0, LX/FXj;->i:I

    .line 2255343
    iput v1, p0, LX/FXj;->o:I

    .line 2255344
    iput v1, p0, LX/FXj;->p:I

    .line 2255345
    const/4 v0, 0x0

    iput v0, p0, LX/FXj;->q:I

    .line 2255346
    iput-object p1, p0, LX/FXj;->b:LX/FXs;

    .line 2255347
    iput-object p2, p0, LX/FXj;->c:LX/AUC;

    .line 2255348
    iput-object p3, p0, LX/FXj;->d:LX/FWt;

    .line 2255349
    iput-object p4, p0, LX/FXj;->e:LX/0kL;

    .line 2255350
    iput-object p6, p0, LX/FXj;->f:Ljava/lang/String;

    .line 2255351
    new-instance v0, LX/FXU;

    invoke-direct {v0, p1}, LX/FXU;-><init>(LX/FXs;)V

    iput-object v0, p0, LX/FXj;->g:LX/FXU;

    .line 2255352
    iput-object p5, p0, LX/FXj;->h:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2255353
    return-void
.end method

.method public static a$redex0(LX/FXj;LX/FXV;)V
    .locals 1

    .prologue
    .line 2255357
    iget-object v0, p0, LX/FXj;->a:LX/FXV;

    .line 2255358
    if-eq v0, p1, :cond_0

    .line 2255359
    iput-object p1, p0, LX/FXj;->a:LX/FXV;

    .line 2255360
    invoke-virtual {p1, p0}, LX/FXV;->a(LX/FXj;)V

    .line 2255361
    :cond_0
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 1

    .prologue
    .line 2255354
    iget-object v0, p0, LX/FXj;->a:LX/FXV;

    move-object v0, v0

    .line 2255355
    invoke-virtual {v0, p0}, LX/FXV;->d(LX/FXj;)V

    .line 2255356
    return-void
.end method
