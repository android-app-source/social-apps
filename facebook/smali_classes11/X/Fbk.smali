.class public LX/Fbk;
.super LX/FbF;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FbF",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field private static volatile b:LX/Fbk;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2260990
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x3b14ce17

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    sput-object v0, LX/Fbk;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2260991
    invoke-direct {p0}, LX/FbF;-><init>()V

    .line 2260992
    return-void
.end method

.method public static a(LX/0QB;)LX/Fbk;
    .locals 3

    .prologue
    .line 2260993
    sget-object v0, LX/Fbk;->b:LX/Fbk;

    if-nez v0, :cond_1

    .line 2260994
    const-class v1, LX/Fbk;

    monitor-enter v1

    .line 2260995
    :try_start_0
    sget-object v0, LX/Fbk;->b:LX/Fbk;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2260996
    if-eqz v2, :cond_0

    .line 2260997
    :try_start_1
    new-instance v0, LX/Fbk;

    invoke-direct {v0}, LX/Fbk;-><init>()V

    .line 2260998
    move-object v0, v0

    .line 2260999
    sput-object v0, LX/Fbk;->b:LX/Fbk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2261000
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2261001
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2261002
    :cond_1
    sget-object v0, LX/Fbk;->b:LX/Fbk;

    return-object v0

    .line 2261003
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2261004
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ILcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261005
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(ILcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;Ljava/lang/String;)LX/0Px;
    .locals 6
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$ModuleResultEdge;",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2261006
    invoke-virtual {p2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2261007
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->gu()Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->gu()Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;->a()Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItemsConnection;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->gu()Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;->a()Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItemsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItemsConnection;->a()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2261008
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2261009
    :goto_0
    return-object v0

    .line 2261010
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2261011
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->gu()Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysis;->a()Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItemsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItemsConnection;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    .line 2261012
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItemsEdge;

    .line 2261013
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItemsEdge;->a()Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 2261014
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItemsEdge;->a()Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2261015
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2261016
    :cond_3
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261017
    sget-object v0, LX/Fbk;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final c(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261018
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;

    .line 2261019
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPhrasesAnalysisItem;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
