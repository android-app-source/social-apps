.class public final LX/Fny;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 2288017
    const/4 v13, 0x0

    .line 2288018
    const/4 v12, 0x0

    .line 2288019
    const/4 v11, 0x0

    .line 2288020
    const/4 v10, 0x0

    .line 2288021
    const/4 v9, 0x0

    .line 2288022
    const/4 v8, 0x0

    .line 2288023
    const/4 v7, 0x0

    .line 2288024
    const/4 v6, 0x0

    .line 2288025
    const/4 v5, 0x0

    .line 2288026
    const/4 v4, 0x0

    .line 2288027
    const/4 v3, 0x0

    .line 2288028
    const/4 v2, 0x0

    .line 2288029
    const/4 v1, 0x0

    .line 2288030
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_1

    .line 2288031
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2288032
    const/4 v1, 0x0

    .line 2288033
    :goto_0
    return v1

    .line 2288034
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2288035
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v14, v15, :cond_a

    .line 2288036
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v14

    .line 2288037
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2288038
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_1

    if-eqz v14, :cond_1

    .line 2288039
    const-string v15, "__type__"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_2

    const-string v15, "__typename"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 2288040
    :cond_2
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v13

    goto :goto_1

    .line 2288041
    :cond_3
    const-string v15, "can_viewer_delete"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 2288042
    const/4 v5, 0x1

    .line 2288043
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto :goto_1

    .line 2288044
    :cond_4
    const-string v15, "can_viewer_edit"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 2288045
    const/4 v4, 0x1

    .line 2288046
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v11

    goto :goto_1

    .line 2288047
    :cond_5
    const-string v15, "can_viewer_post"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 2288048
    const/4 v3, 0x1

    .line 2288049
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v10

    goto :goto_1

    .line 2288050
    :cond_6
    const-string v15, "can_viewer_report"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 2288051
    const/4 v2, 0x1

    .line 2288052
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v9

    goto :goto_1

    .line 2288053
    :cond_7
    const-string v15, "focused_cover_photo"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 2288054
    invoke-static/range {p0 .. p1}, LX/Fnx;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 2288055
    :cond_8
    const-string v15, "is_viewer_following"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 2288056
    const/4 v1, 0x1

    .line 2288057
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    goto/16 :goto_1

    .line 2288058
    :cond_9
    const-string v15, "privacy_scope"

    invoke-virtual {v14, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_0

    .line 2288059
    invoke-static/range {p0 .. p1}, LX/5R2;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 2288060
    :cond_a
    const/16 v14, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, LX/186;->c(I)V

    .line 2288061
    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 2288062
    if-eqz v5, :cond_b

    .line 2288063
    const/4 v5, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12}, LX/186;->a(IZ)V

    .line 2288064
    :cond_b
    if-eqz v4, :cond_c

    .line 2288065
    const/4 v4, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->a(IZ)V

    .line 2288066
    :cond_c
    if-eqz v3, :cond_d

    .line 2288067
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10}, LX/186;->a(IZ)V

    .line 2288068
    :cond_d
    if-eqz v2, :cond_e

    .line 2288069
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->a(IZ)V

    .line 2288070
    :cond_e
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2288071
    if-eqz v1, :cond_f

    .line 2288072
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->a(IZ)V

    .line 2288073
    :cond_f
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2288074
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method
