.class public final LX/F4N;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Landroid/view/View$OnClickListener;

.field public final synthetic d:LX/F4Q;


# direct methods
.method public constructor <init>(LX/F4Q;LX/DML;Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 2196884
    iput-object p1, p0, LX/F4N;->d:LX/F4Q;

    iput-object p3, p0, LX/F4N;->a:Ljava/lang/String;

    iput-object p4, p0, LX/F4N;->b:Ljava/lang/String;

    iput-object p5, p0, LX/F4N;->c:Landroid/view/View$OnClickListener;

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 2196885
    check-cast p1, Landroid/widget/LinearLayout;

    .line 2196886
    const v0, 0x7f0d1559

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2196887
    iget-object v1, p0, LX/F4N;->a:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2196888
    iget-object v1, p0, LX/F4N;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageURI(Landroid/net/Uri;)V

    .line 2196889
    :goto_0
    iget-object v0, p0, LX/F4N;->b:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2196890
    const v0, 0x7f0d155a

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2196891
    iget-object v1, p0, LX/F4N;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2196892
    :cond_0
    iget-object v0, p0, LX/F4N;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2196893
    return-void

    .line 2196894
    :cond_1
    iget-object v1, p0, LX/F4N;->d:LX/F4Q;

    iget-object v1, v1, LX/F4Q;->c:LX/0wM;

    const v2, 0x7f020c4e

    iget-object v3, p0, LX/F4N;->d:LX/F4Q;

    iget-object v3, v3, LX/F4Q;->a:Landroid/content/res/Resources;

    const v4, 0x7f0a00d2

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, LX/0wM;->a(IIZ)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
