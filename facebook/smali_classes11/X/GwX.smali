.class public final LX/GwX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/GwY;


# direct methods
.method public constructor <init>(LX/GwY;Z)V
    .locals 0

    .prologue
    .line 2407079
    iput-object p1, p0, LX/GwX;->b:LX/GwY;

    iput-boolean p2, p0, LX/GwX;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2407080
    iget-object v0, p0, LX/GwX;->b:LX/GwY;

    .line 2407081
    sget-object p0, LX/GwY;->a:Ljava/lang/Class;

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    const-string p1, "Unexpected failure of GetPendingAppCallForMediaUploadOperation call."

    invoke-static {p0, p1}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object p0

    invoke-virtual {p0}, LX/0VK;->g()LX/0VG;

    move-result-object p0

    .line 2407082
    iget-object p1, v0, LX/GwY;->c:LX/03V;

    invoke-virtual {p1, p0}, LX/03V;->a(LX/0VG;)V

    .line 2407083
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2407084
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2407085
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/common/action/PlatformAppCall;

    .line 2407086
    if-nez v0, :cond_0

    .line 2407087
    :goto_0
    return-void

    .line 2407088
    :cond_0
    iget-object v1, p0, LX/GwX;->b:LX/GwY;

    iget-object v1, v1, LX/GwY;->d:LX/4hb;

    iget-boolean v2, p0, LX/GwX;->a:Z

    .line 2407089
    iget-object v3, v1, LX/4hb;->b:LX/3N2;

    .line 2407090
    iget-object v4, v0, Lcom/facebook/platform/common/action/PlatformAppCall;->d:Ljava/lang/String;

    move-object v4, v4

    .line 2407091
    invoke-virtual {v3, v4}, LX/3N2;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2407092
    if-eqz v3, :cond_1

    .line 2407093
    iget-object v4, v0, Lcom/facebook/platform/common/action/PlatformAppCall;->g:Ljava/lang/String;

    move-object v4, v4

    .line 2407094
    invoke-virtual {v3, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2407095
    :cond_1
    :goto_1
    iget-object v1, p0, LX/GwX;->b:LX/GwY;

    iget-object v1, v1, LX/GwY;->e:LX/4hp;

    .line 2407096
    iget-object v2, v0, Lcom/facebook/platform/common/action/PlatformAppCall;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2407097
    invoke-virtual {v1, v0}, LX/4hp;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0

    .line 2407098
    :cond_2
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.facebook.platform.AppCallResultBroadcast"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2407099
    iget-object v4, v0, Lcom/facebook/platform/common/action/PlatformAppCall;->d:Ljava/lang/String;

    move-object v4, v4

    .line 2407100
    invoke-virtual {v3, v4}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2407101
    iget-object v4, v0, Lcom/facebook/platform/common/action/PlatformAppCall;->i:Ljava/lang/String;

    move-object v4, v4

    .line 2407102
    if-eqz v4, :cond_3

    .line 2407103
    const-string v4, "com.facebook.platform.protocol.PROTOCOL_ACTION"

    .line 2407104
    iget-object p1, v0, Lcom/facebook/platform/common/action/PlatformAppCall;->i:Ljava/lang/String;

    move-object p1, p1

    .line 2407105
    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2407106
    :cond_3
    const-string v4, "com.facebook.platform.protocol.CALL_ID"

    .line 2407107
    iget-object p1, v0, Lcom/facebook/platform/common/action/PlatformAppCall;->a:Ljava/lang/String;

    move-object p1, p1

    .line 2407108
    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2407109
    if-eqz v2, :cond_4

    .line 2407110
    const-string v4, "com.facebook.platform.extra.DID_COMPLETE"

    const/4 p1, 0x1

    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2407111
    :goto_2
    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2407112
    iget-object v4, v1, LX/4hb;->a:Landroid/content/Context;

    invoke-virtual {v4, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 2407113
    :cond_4
    const-string v4, "com.facebook.platform.status.ERROR_TYPE"

    const-string p1, "UnknownError"

    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2
.end method
