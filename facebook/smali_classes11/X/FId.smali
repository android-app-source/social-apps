.class public final enum LX/FId;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FId;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FId;

.field public static final enum CHUNK:LX/FId;

.field public static final enum FINISHED:LX/FId;

.field public static final enum HOLE:LX/FId;

.field public static final enum OTHER:LX/FId;

.field public static final enum STUNPING:LX/FId;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2222403
    new-instance v0, LX/FId;

    const-string v1, "HOLE"

    invoke-direct {v0, v1, v2}, LX/FId;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FId;->HOLE:LX/FId;

    .line 2222404
    new-instance v0, LX/FId;

    const-string v1, "CHUNK"

    invoke-direct {v0, v1, v3}, LX/FId;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FId;->CHUNK:LX/FId;

    .line 2222405
    new-instance v0, LX/FId;

    const-string v1, "STUNPING"

    invoke-direct {v0, v1, v4}, LX/FId;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FId;->STUNPING:LX/FId;

    .line 2222406
    new-instance v0, LX/FId;

    const-string v1, "FINISHED"

    invoke-direct {v0, v1, v5}, LX/FId;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FId;->FINISHED:LX/FId;

    .line 2222407
    new-instance v0, LX/FId;

    const-string v1, "OTHER"

    invoke-direct {v0, v1, v6}, LX/FId;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FId;->OTHER:LX/FId;

    .line 2222408
    const/4 v0, 0x5

    new-array v0, v0, [LX/FId;

    sget-object v1, LX/FId;->HOLE:LX/FId;

    aput-object v1, v0, v2

    sget-object v1, LX/FId;->CHUNK:LX/FId;

    aput-object v1, v0, v3

    sget-object v1, LX/FId;->STUNPING:LX/FId;

    aput-object v1, v0, v4

    sget-object v1, LX/FId;->FINISHED:LX/FId;

    aput-object v1, v0, v5

    sget-object v1, LX/FId;->OTHER:LX/FId;

    aput-object v1, v0, v6

    sput-object v0, LX/FId;->$VALUES:[LX/FId;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2222409
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FId;
    .locals 1

    .prologue
    .line 2222410
    const-class v0, LX/FId;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FId;

    return-object v0
.end method

.method public static values()[LX/FId;
    .locals 1

    .prologue
    .line 2222411
    sget-object v0, LX/FId;->$VALUES:[LX/FId;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FId;

    return-object v0
.end method
