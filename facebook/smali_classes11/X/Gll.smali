.class public LX/Gll;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Landroid/content/res/Resources;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/graphics/Paint;

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field private g:Z

.field public h:Landroid/widget/TextView;

.field public i:Landroid/widget/TextView;

.field public j:Landroid/widget/ImageView;

.field public k:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2391252
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2391253
    const-class v0, LX/Gll;

    invoke-static {v0, p0}, LX/Gll;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2391254
    iget-object v0, p0, LX/Gll;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b236c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Gll;->e:I

    .line 2391255
    iget-object v0, p0, LX/Gll;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b236d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Gll;->f:I

    .line 2391256
    iget-object v0, p0, LX/Gll;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b11e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Gll;->c:I

    .line 2391257
    iget-object v0, p0, LX/Gll;->a:Landroid/content/res/Resources;

    const v1, 0x7f0b236e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Gll;->d:I

    .line 2391258
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/Gll;->b:Landroid/graphics/Paint;

    .line 2391259
    iget-object v0, p0, LX/Gll;->b:Landroid/graphics/Paint;

    iget-object v1, p0, LX/Gll;->a:Landroid/content/res/Resources;

    const p1, 0x7f0a05ed

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2391260
    iget-object v0, p0, LX/Gll;->b:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2391261
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/Gll;->setWillNotDraw(Z)V

    .line 2391262
    const v0, 0x7f030869

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2391263
    const v0, 0x7f0d1065

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Gll;->h:Landroid/widget/TextView;

    .line 2391264
    const v0, 0x7f0d15e3

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/Gll;->i:Landroid/widget/TextView;

    .line 2391265
    const v0, 0x7f0d15e4

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Gll;->j:Landroid/widget/ImageView;

    .line 2391266
    const v0, 0x7f0d15e2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/Gll;->k:Landroid/view/View;

    .line 2391267
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Gll;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object p0

    check-cast p0, Landroid/content/res/Resources;

    iput-object p0, p1, LX/Gll;->a:Landroid/content/res/Resources;

    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Landroid/view/View$OnClickListener;Z)V
    .locals 4
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Landroid/graphics/drawable/Drawable;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 2391268
    iget-object v0, p0, LX/Gll;->k:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setId(I)V

    .line 2391269
    iput-boolean p6, p0, LX/Gll;->g:Z

    .line 2391270
    if-eqz p6, :cond_2

    iget v0, p0, LX/Gll;->f:I

    :goto_0
    move v0, v0

    .line 2391271
    iget v1, p0, LX/Gll;->e:I

    invoke-virtual {p0, v2, v0, v2, v1}, LX/Gll;->setPadding(IIII)V

    .line 2391272
    invoke-virtual {p0}, LX/Gll;->invalidate()V

    .line 2391273
    iget-object v0, p0, LX/Gll;->h:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2391274
    if-eqz p3, :cond_0

    .line 2391275
    iget-object v0, p0, LX/Gll;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2391276
    iget-object v0, p0, LX/Gll;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2391277
    :goto_1
    if-eqz p4, :cond_1

    .line 2391278
    iget-object v0, p0, LX/Gll;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, p4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2391279
    iget-object v0, p0, LX/Gll;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2391280
    :goto_2
    iget-object v0, p0, LX/Gll;->k:Landroid/view/View;

    invoke-virtual {v0, p5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2391281
    return-void

    .line 2391282
    :cond_0
    iget-object v0, p0, LX/Gll;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 2391283
    :cond_1
    iget-object v0, p0, LX/Gll;->j:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_2

    :cond_2
    iget v0, p0, LX/Gll;->e:I

    goto :goto_0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2391284
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    .line 2391285
    iget-boolean v0, p0, LX/Gll;->g:Z

    if-eqz v0, :cond_0

    .line 2391286
    iget v0, p0, LX/Gll;->c:I

    int-to-float v1, v0

    iget v0, p0, LX/Gll;->d:I

    int-to-float v2, v0

    invoke-virtual {p0}, LX/Gll;->getWidth()I

    move-result v0

    iget v3, p0, LX/Gll;->c:I

    sub-int/2addr v0, v3

    int-to-float v3, v0

    iget v0, p0, LX/Gll;->d:I

    int-to-float v0, v0

    const/high16 v4, 0x3f800000    # 1.0f

    add-float/2addr v4, v0

    iget-object v5, p0, LX/Gll;->b:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2391287
    :cond_0
    return-void
.end method
