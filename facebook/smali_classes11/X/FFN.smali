.class public final LX/FFN;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2217356
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_6

    .line 2217357
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2217358
    :goto_0
    return v1

    .line 2217359
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_4

    .line 2217360
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2217361
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2217362
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v5, :cond_0

    .line 2217363
    const-string v6, "count"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2217364
    invoke-virtual {p0}, LX/15w;->E()I

    move-result v0

    move v4, v0

    move v0, v2

    goto :goto_1

    .line 2217365
    :cond_1
    const-string v6, "nodes"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2217366
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2217367
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->START_ARRAY:LX/15z;

    if-ne v5, v6, :cond_2

    .line 2217368
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_ARRAY:LX/15z;

    if-eq v5, v6, :cond_2

    .line 2217369
    const/4 v6, 0x0

    .line 2217370
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v7, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v7, :cond_a

    .line 2217371
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2217372
    :goto_3
    move v5, v6

    .line 2217373
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2217374
    :cond_2
    invoke-static {v3, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v3

    move v3, v3

    .line 2217375
    goto :goto_1

    .line 2217376
    :cond_3
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2217377
    :cond_4
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2217378
    if-eqz v0, :cond_5

    .line 2217379
    invoke-virtual {p1, v1, v4, v1}, LX/186;->a(III)V

    .line 2217380
    :cond_5
    invoke-virtual {p1, v2, v3}, LX/186;->b(II)V

    .line 2217381
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1

    .line 2217382
    :cond_7
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2217383
    :cond_8
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_9

    .line 2217384
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2217385
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2217386
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_8

    if-eqz v7, :cond_8

    .line 2217387
    const-string v8, "messaging_actor"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2217388
    const/4 v7, 0x0

    .line 2217389
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v8, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v8, :cond_10

    .line 2217390
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2217391
    :goto_5
    move v5, v7

    .line 2217392
    goto :goto_4

    .line 2217393
    :cond_9
    const/4 v7, 0x1

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2217394
    invoke-virtual {p1, v6, v5}, LX/186;->b(II)V

    .line 2217395
    invoke-virtual {p1}, LX/186;->d()I

    move-result v6

    goto :goto_3

    :cond_a
    move v5, v6

    goto :goto_4

    .line 2217396
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2217397
    :cond_c
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v9

    sget-object v10, LX/15z;->END_OBJECT:LX/15z;

    if-eq v9, v10, :cond_f

    .line 2217398
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v9

    .line 2217399
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2217400
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v10, v11, :cond_c

    if-eqz v9, :cond_c

    .line 2217401
    const-string v10, "__type__"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_d

    const-string v10, "__typename"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 2217402
    :cond_d
    invoke-static {p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v8

    goto :goto_6

    .line 2217403
    :cond_e
    const-string v10, "profile_picture"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 2217404
    const/4 v9, 0x0

    .line 2217405
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v10, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v10, :cond_14

    .line 2217406
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2217407
    :goto_7
    move v5, v9

    .line 2217408
    goto :goto_6

    .line 2217409
    :cond_f
    const/4 v9, 0x2

    invoke-virtual {p1, v9}, LX/186;->c(I)V

    .line 2217410
    invoke-virtual {p1, v7, v8}, LX/186;->b(II)V

    .line 2217411
    const/4 v7, 0x1

    invoke-virtual {p1, v7, v5}, LX/186;->b(II)V

    .line 2217412
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto :goto_5

    :cond_10
    move v5, v7

    move v8, v7

    goto :goto_6

    .line 2217413
    :cond_11
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2217414
    :cond_12
    :goto_8
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_13

    .line 2217415
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2217416
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2217417
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_12

    if-eqz v10, :cond_12

    .line 2217418
    const-string v11, "uri"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_11

    .line 2217419
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_8

    .line 2217420
    :cond_13
    const/4 v10, 0x1

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2217421
    invoke-virtual {p1, v9, v5}, LX/186;->b(II)V

    .line 2217422
    invoke-virtual {p1}, LX/186;->d()I

    move-result v9

    goto :goto_7

    :cond_14
    move v5, v9

    goto :goto_8
.end method
