.class public LX/F8u;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0ad;

.field public final d:Z


# direct methods
.method public constructor <init>(Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;LX/0Uh;LX/0Or;LX/0ad;)V
    .locals 2
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/growth/annotations/IsNUXImportGoogleAccountProfilePhotoEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2204782
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2204783
    iput-object p1, p0, LX/F8u;->a:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    .line 2204784
    iput-object p3, p0, LX/F8u;->b:LX/0Or;

    .line 2204785
    iput-object p4, p0, LX/F8u;->c:LX/0ad;

    .line 2204786
    const/16 v0, 0x596

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/F8u;->d:Z

    .line 2204787
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 2204788
    const-string v0, "contact_importer"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2204789
    iget-object v0, p0, LX/F8u;->c:LX/0ad;

    sget-short v1, LX/F94;->c:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2204790
    new-instance v0, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;

    invoke-direct {v0}, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;-><init>()V

    .line 2204791
    :goto_0
    return-object v0

    .line 2204792
    :cond_0
    iget-object v0, p0, LX/F8u;->a:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    invoke-virtual {v0}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2204793
    sget-object v0, LX/89v;->NEW_ACCOUNT_NUX:LX/89v;

    sget-object v1, LX/89v;->NEW_ACCOUNT_NUX:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->a(LX/89v;Ljava/lang/String;Z)Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    move-result-object v0

    goto :goto_0

    .line 2204794
    :cond_1
    sget-object v0, LX/89v;->NEW_ACCOUNT_NUX:LX/89v;

    invoke-static {v0}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->a(LX/89v;)Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    move-result-object v0

    goto :goto_0

    .line 2204795
    :cond_2
    const-string v0, "people_you_may_know"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2204796
    const/4 v4, 0x0

    .line 2204797
    sget-object v3, LX/2h7;->NUX:LX/2h7;

    const/4 v6, 0x1

    iget-boolean v7, p0, LX/F8u;->d:Z

    iget-boolean v8, p0, LX/F8u;->d:Z

    move v5, v4

    invoke-static/range {v3 .. v8}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->a(LX/2h7;ZZZZZ)Lcom/facebook/friending/jewel/FriendRequestsFragment;

    move-result-object v3

    move-object v0, v3

    .line 2204798
    goto :goto_0

    .line 2204799
    :cond_3
    const-string v0, "upload_profile_pic"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2204800
    iget-object v0, p0, LX/F8u;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    move v0, v0

    .line 2204801
    if-eqz v0, :cond_4

    .line 2204802
    new-instance v0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;

    invoke-direct {v0}, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;-><init>()V

    goto :goto_0

    .line 2204803
    :cond_4
    new-instance v0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;

    invoke-direct {v0}, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;-><init>()V

    goto :goto_0

    .line 2204804
    :cond_5
    const-string v0, "native_name"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2204805
    new-instance v0, Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;

    invoke-direct {v0}, Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;-><init>()V

    goto :goto_0

    .line 2204806
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "User Account NUX: step "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not supported"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
