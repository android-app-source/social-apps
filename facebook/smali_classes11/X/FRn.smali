.class public LX/FRn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6wK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6wK",
        "<",
        "LX/FRw;",
        "Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2241013
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2241014
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/payments/picker/model/PickerRunTimeData;)LX/0Px;
    .locals 3

    .prologue
    .line 2241015
    check-cast p1, Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;

    .line 2241016
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2241017
    sget-object v0, LX/FRw;->PAYMENT_METHODS:LX/FRw;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2241018
    sget-object v0, LX/FRw;->DOUBLE_ROW_DIVIDER:LX/FRw;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2241019
    sget-object v0, LX/FRw;->PAYMENT_HISTORY:LX/FRw;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2241020
    sget-object v0, LX/FRw;->DOUBLE_ROW_DIVIDER:LX/FRw;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2241021
    sget-object v0, LX/FRw;->SECURITY:LX/FRw;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2241022
    sget-object v0, LX/FRw;->DOUBLE_ROW_DIVIDER:LX/FRw;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2241023
    sget-object v0, LX/FRw;->ORDER_INFORMATION:LX/FRw;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2241024
    sget-object v0, LX/FRw;->DOUBLE_ROW_DIVIDER:LX/FRw;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2241025
    sget-object v0, LX/FRw;->SUPPORT:LX/FRw;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2241026
    sget-object v0, LX/FRw;->DOUBLE_ROW_DIVIDER:LX/FRw;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2241027
    sget-object v0, LX/FRw;->ADS_MANAGER:LX/FRw;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2241028
    sget-object v0, LX/FRw;->DOUBLE_ROW_DIVIDER:LX/FRw;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2241029
    iget-object v0, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v0, v0

    .line 2241030
    check-cast v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;

    .line 2241031
    sget-object v2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    iget-object p0, v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2241032
    iget-object p1, p0, Lcom/facebook/payments/currency/CurrencyAmount;->c:Ljava/math/BigDecimal;

    move-object p0, p1

    .line 2241033
    invoke-virtual {v2, p0}, Ljava/math/BigDecimal;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, v0, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->e:I

    if-eqz v2, :cond_2

    :cond_0
    const/4 v2, 0x1

    :goto_0
    move v0, v2

    .line 2241034
    if-eqz v0, :cond_1

    .line 2241035
    sget-object v0, LX/FRw;->FACEBOOK_GAMES:LX/FRw;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2241036
    sget-object v0, LX/FRw;->DOUBLE_ROW_DIVIDER:LX/FRw;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2241037
    :cond_1
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method
