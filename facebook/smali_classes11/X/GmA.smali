.class public final LX/GmA;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GmB;


# direct methods
.method public constructor <init>(LX/GmB;)V
    .locals 0

    .prologue
    .line 2391611
    iput-object p1, p0, LX/GmA;->a:LX/GmB;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2391612
    iget-object v0, p0, LX/GmA;->a:LX/GmB;

    iget-object v0, v0, LX/GmB;->a:LX/F9r;

    invoke-virtual {v0}, LX/F9r;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2391613
    check-cast p1, Ljava/util/List;

    .line 2391614
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2391615
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;

    .line 2391616
    iget-object v3, v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->phone:Ljava/lang/String;

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2391617
    new-instance v3, LX/GmD;

    iget-object v4, v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->name:Ljava/lang/String;

    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->phone:Ljava/lang/String;

    invoke-direct {v3, v4, v0}, LX/GmD;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2391618
    :cond_1
    iget-object v3, v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a:Ljava/util/List;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 2391619
    new-instance v3, LX/GmD;

    iget-object v4, v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->name:Ljava/lang/String;

    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a:Ljava/util/List;

    const/4 v5, 0x0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v3, v4, v0}, LX/GmD;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2391620
    :cond_2
    iget-object v0, p0, LX/GmA;->a:LX/GmB;

    iget-object v0, v0, LX/GmB;->b:LX/Glx;

    if-eqz v0, :cond_5

    .line 2391621
    iget-object v0, p0, LX/GmA;->a:LX/GmB;

    iget-object v0, v0, LX/GmB;->b:LX/Glx;

    .line 2391622
    iget-object v2, v0, LX/Glx;->a:Lcom/facebook/growth/contactinviter/ContactInviterFragment;

    .line 2391623
    iget-object v3, v2, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->n:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v3}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2391624
    iget-object v3, v2, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->f:LX/Gm8;

    .line 2391625
    new-instance v4, LX/GmE;

    invoke-direct {v4}, LX/GmE;-><init>()V

    move-object v5, v4

    .line 2391626
    const-string v4, "type"

    const-string v6, "INVITABLE"

    invoke-virtual {v5, v4, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2391627
    const-string v4, "meta_data"

    const-string v6, ""

    invoke-virtual {v5, v4, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2391628
    if-eqz v1, :cond_4

    .line 2391629
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2391630
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/GmD;

    .line 2391631
    new-instance p1, LX/4HF;

    invoke-direct {p1}, LX/4HF;-><init>()V

    iget-object v0, v4, LX/GmD;->a:Ljava/lang/String;

    .line 2391632
    const-string v1, "name"

    invoke-virtual {p1, v1, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2391633
    move-object p1, p1

    .line 2391634
    iget-object v4, v4, LX/GmD;->b:Ljava/lang/String;

    .line 2391635
    const-string v0, "contact_point"

    invoke-virtual {p1, v0, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2391636
    move-object v4, p1

    .line 2391637
    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2391638
    :cond_3
    const-string v4, "list"

    invoke-virtual {v5, v4, v6}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    .line 2391639
    :cond_4
    iget-object v4, v3, LX/Gm8;->a:LX/0tX;

    invoke-static {v5}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v4

    move-object v3, v4

    .line 2391640
    iget-object v4, v2, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->c:LX/1Ck;

    const-string v5, "upload_contacts_task"

    new-instance v6, LX/Gly;

    invoke-direct {v6, v2}, LX/Gly;-><init>(Lcom/facebook/growth/contactinviter/ContactInviterFragment;)V

    invoke-virtual {v4, v5, v3, v6}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2391641
    :cond_5
    return-void
.end method
