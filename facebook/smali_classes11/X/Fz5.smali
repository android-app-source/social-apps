.class public LX/Fz5;
.super LX/Fz4;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2307505
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Fz5;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2307506
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2307503
    const v0, 0x7f0102b5

    invoke-direct {p0, p1, p2, v0}, LX/Fz5;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2307504
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2307480
    invoke-direct {p0, p1, p2, p3}, LX/Fz4;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2307481
    sget-object v0, LX/6VF;->MEDIUM:LX/6VF;

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2307482
    return-void
.end method


# virtual methods
.method public getOptionType()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nonnull;
    .end annotation

    .prologue
    .line 2307502
    const-string v0, "page"

    return-object v0
.end method

.method public setInferenceData(Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;)V
    .locals 3
    .param p1    # Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2307485
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->c()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->b()LX/174;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->c()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2307486
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, LX/Fz5;->setVisibility(I)V

    .line 2307487
    :goto_0
    return-void

    .line 2307488
    :cond_1
    invoke-virtual {p0, v1}, LX/Fz5;->setVisibility(I)V

    .line 2307489
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->a()LX/174;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->a()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2307490
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->a()LX/174;

    move-result-object v0

    invoke-interface {v0}, LX/174;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2307491
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->c()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;->b()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_6

    .line 2307492
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->c()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2307493
    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    .line 2307494
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->c()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2307495
    :cond_3
    invoke-virtual {p0, p1}, LX/Fz5;->setTitle(Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;)V

    .line 2307496
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->c()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;->d()Ljava/lang/String;

    move-result-object v0

    .line 2307497
    iput-object v0, p0, LX/Fz4;->j:Ljava/lang/String;

    .line 2307498
    if-nez v0, :cond_4

    .line 2307499
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, LX/Fz4;->setVisibility(I)V

    .line 2307500
    :cond_4
    goto :goto_0

    :cond_5
    move v0, v1

    .line 2307501
    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public setTitle(Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;)V
    .locals 1
    .param p1    # Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2307483
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel;->c()Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/ProfileQuestionGraphQLModels$InferenceWithExplanationFragmentModel$PageModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2307484
    return-void
.end method
