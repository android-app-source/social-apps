.class public LX/H88;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0pQ;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;

.field public static final g:LX/0Tn;

.field public static final h:LX/0Tn;

.field public static final i:LX/0Tn;

.field public static final j:LX/0Tn;

.field public static final k:LX/0Tn;

.field public static final l:LX/0Tn;

.field public static final m:LX/0Tn;

.field public static final n:LX/0Tn;

.field public static final o:LX/0Tn;

.field public static final p:LX/0Tn;

.field public static final q:LX/0Tn;

.field public static final r:LX/0Tn;

.field public static final s:LX/0Tn;

.field public static final t:LX/0Tn;

.field public static final u:LX/0Tn;

.field public static final v:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2432986
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "pages_manager_app/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2432987
    sput-object v0, LX/H88;->a:LX/0Tn;

    const-string v1, "mute_until_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/H88;->b:LX/0Tn;

    .line 2432988
    sget-object v0, LX/H88;->a:LX/0Tn;

    const-string v1, "app_icon_badge"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/H88;->c:LX/0Tn;

    .line 2432989
    sget-object v0, LX/H88;->a:LX/0Tn;

    const-string v1, "video_upload_raw"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/H88;->d:LX/0Tn;

    .line 2432990
    sget-object v0, LX/H88;->a:LX/0Tn;

    const-string v1, "photos_upload_hd"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/H88;->e:LX/0Tn;

    .line 2432991
    sget-object v0, LX/H88;->a:LX/0Tn;

    const-string v1, "notifications/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2432992
    sput-object v0, LX/H88;->f:LX/0Tn;

    const-string v1, "tips_and_reminders"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/H88;->g:LX/0Tn;

    .line 2432993
    sget-object v0, LX/H88;->f:LX/0Tn;

    const-string v1, "sound_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/H88;->h:LX/0Tn;

    .line 2432994
    sget-object v0, LX/H88;->f:LX/0Tn;

    const-string v1, "ringtone_uri"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/H88;->i:LX/0Tn;

    .line 2432995
    sget-object v0, LX/H88;->f:LX/0Tn;

    const-string v1, "vibrate_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/H88;->j:LX/0Tn;

    .line 2432996
    sget-object v0, LX/H88;->f:LX/0Tn;

    const-string v1, "push_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/H88;->k:LX/0Tn;

    .line 2432997
    sget-object v0, LX/H88;->a:LX/0Tn;

    const-string v1, "last_open_page_id"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/H88;->l:LX/0Tn;

    .line 2432998
    sget-object v0, LX/H88;->a:LX/0Tn;

    const-string v1, "publish_dialog_enabled"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/H88;->m:LX/0Tn;

    .line 2432999
    sget-object v0, LX/H88;->a:LX/0Tn;

    const-string v1, "publish_dialog_disabled_timestamp"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/H88;->n:LX/0Tn;

    .line 2433000
    sget-object v0, LX/H88;->a:LX/0Tn;

    const-string v1, "internal/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2433001
    sput-object v0, LX/H88;->o:LX/0Tn;

    const-string v1, "sandbox"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/H88;->p:LX/0Tn;

    .line 2433002
    sget-object v0, LX/H88;->o:LX/0Tn;

    const-string v1, "webview_tier"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/H88;->q:LX/0Tn;

    .line 2433003
    sget-object v0, LX/H88;->o:LX/0Tn;

    const-string v1, "webview_reset"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/H88;->r:LX/0Tn;

    .line 2433004
    sget-object v0, LX/H88;->o:LX/0Tn;

    const-string v1, "nux_visibility"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/H88;->s:LX/0Tn;

    .line 2433005
    sget-object v0, LX/H88;->a:LX/0Tn;

    const-string v1, "drafts_post_megaphone_visible"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/H88;->t:LX/0Tn;

    .line 2433006
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "messenger/version_promo/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2433007
    sput-object v0, LX/H88;->u:LX/0Tn;

    const-string v1, "dismissed_time"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/H88;->v:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2433008
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2433009
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2433010
    sget-object v0, LX/H88;->l:LX/0Tn;

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
