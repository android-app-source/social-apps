.class public final LX/Fke;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/Fkf;


# direct methods
.method public constructor <init>(LX/Fkf;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2278829
    iput-object p1, p0, LX/Fke;->b:LX/Fkf;

    iput-object p2, p0, LX/Fke;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2278850
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2278830
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v3, 0x0

    .line 2278831
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2278832
    const/4 v1, 0x0

    .line 2278833
    if-eqz p1, :cond_2

    .line 2278834
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2278835
    if-eqz v0, :cond_2

    .line 2278836
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2278837
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchModel;->a()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchModel$SearchResultsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2278838
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2278839
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchModel;->a()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchModel$SearchResultsModel;

    move-result-object v5

    .line 2278840
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchModel$SearchResultsModel;->j()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_3

    .line 2278841
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchModel$SearchResultsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->h(II)Z

    move-result v1

    .line 2278842
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchModel$SearchResultsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v2, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    move v2, v1

    move-object v1, v0

    .line 2278843
    :goto_0
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchModel$SearchResultsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2278844
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchModel$SearchResultsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    :goto_1
    if-ge v3, v6, :cond_1

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchModel$SearchResultsModel$EdgesModel;

    .line 2278845
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchModel$SearchResultsModel$EdgesModel;->a()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 2278846
    new-instance v7, LX/FkY;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchModel$SearchResultsModel$EdgesModel;->a()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;

    move-result-object v0

    invoke-direct {v7, v0}, LX/FkY;-><init>(Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;)V

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2278847
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    move v3, v2

    .line 2278848
    :cond_2
    iget-object v0, p0, LX/Fke;->b:LX/Fkf;

    iget-object v0, v0, LX/Fkf;->c:Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;

    iget-object v2, p0, LX/Fke;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v4, v3, v1}, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->a(Ljava/lang/String;Ljava/util/ArrayList;ZLjava/lang/String;)V

    .line 2278849
    return-void

    :cond_3
    move v2, v3

    goto :goto_0
.end method
