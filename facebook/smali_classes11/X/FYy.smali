.class public LX/FYy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8tH;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8tH;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2256763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2256764
    iput-object p1, p0, LX/FYy;->a:LX/0Ot;

    .line 2256765
    iput-object p2, p0, LX/FYy;->b:LX/0Ot;

    .line 2256766
    iput-object p3, p0, LX/FYy;->c:LX/0Ot;

    .line 2256767
    return-void
.end method

.method public static a(LX/0QB;)LX/FYy;
    .locals 1

    .prologue
    .line 2256768
    invoke-static {p0}, LX/FYy;->b(LX/0QB;)LX/FYy;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/FYy;
    .locals 4

    .prologue
    .line 2256773
    new-instance v0, LX/FYy;

    const/16 v1, 0xdf4

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x37f2

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0xac0

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, LX/FYy;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2256774
    return-object v0
.end method

.method public static d(LX/BO1;)I
    .locals 2

    .prologue
    .line 2256769
    invoke-interface {p0}, LX/BO1;->H()I

    move-result v0

    .line 2256770
    if-gtz v0, :cond_0

    .line 2256771
    const/4 v0, 0x0

    .line 2256772
    :goto_0
    return v0

    :cond_0
    invoke-interface {p0}, LX/BO1;->I()I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    div-int v0, v1, v0

    goto :goto_0
.end method

.method public static d(LX/FYy;)I
    .locals 4

    .prologue
    .line 2256762
    iget-object v0, p0, LX/FYy;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v2, LX/0X5;->el:J

    const/16 v1, 0x2710

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    return v0
.end method

.method public static e(LX/FYy;)I
    .locals 4

    .prologue
    .line 2256748
    iget-object v0, p0, LX/FYy;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v2, LX/0X5;->ej:J

    const/16 v1, 0x64

    invoke-interface {v0, v2, v3, v1}, LX/0W4;->a(JI)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/BO1;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2256758
    invoke-interface {p1}, LX/BO1;->I()I

    move-result v0

    invoke-static {p0}, LX/FYy;->d(LX/FYy;)I

    move-result v2

    if-lt v0, v2, :cond_0

    invoke-static {p1}, LX/FYy;->d(LX/BO1;)I

    move-result v0

    invoke-static {p0}, LX/FYy;->e(LX/FYy;)I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-interface {p1}, LX/BO1;->o()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    :cond_0
    move v0, v1

    .line 2256759
    :goto_0
    return v0

    .line 2256760
    :cond_1
    invoke-interface {p1}, LX/BO1;->I()I

    move-result v2

    iget-object v0, p0, LX/FYy;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v4, LX/0X5;->ei:J

    const/16 v3, 0x1388

    invoke-interface {v0, v4, v5, v3}, LX/0W4;->a(JI)I

    move-result v0

    sub-int v0, v2, v0

    .line 2256761
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0
.end method

.method public final a(LX/BO1;Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v6, 0x0

    .line 2256749
    invoke-interface {p1}, LX/BO1;->H()I

    move-result v1

    .line 2256750
    invoke-virtual {p0, p1}, LX/FYy;->a(LX/BO1;)I

    move-result v0

    .line 2256751
    if-nez v0, :cond_0

    .line 2256752
    const v2, 0x7f08341e

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v0, p0, LX/FYy;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8tH;

    int-to-long v4, v1

    invoke-virtual {v0, v4, v5}, LX/8tH;->a(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {p2, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2256753
    :goto_0
    return-object v0

    .line 2256754
    :cond_0
    sub-int/2addr v1, v0

    .line 2256755
    const v2, 0x7f08341d

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v0, p0, LX/FYy;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8tH;

    int-to-long v4, v1

    invoke-virtual {v0, v4, v5}, LX/8tH;->a(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {p2, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Z
    .locals 4

    .prologue
    .line 2256756
    iget-object v0, p0, LX/FYy;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0W3;

    sget-wide v2, LX/0X5;->eh:J

    invoke-interface {v0, v2, v3}, LX/0W4;->a(J)Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 2256757
    iget-object v0, p0, LX/FYy;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    const/16 v1, 0x441

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
