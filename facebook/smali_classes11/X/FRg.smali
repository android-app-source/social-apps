.class public LX/FRg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6w1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6w1",
        "<",
        "Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/1Ck;

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/737;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/70D;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/FRy;

.field public final f:LX/FRR;

.field public final g:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

.field public final h:LX/03V;

.field public i:LX/70k;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2240594
    const-class v0, LX/FRg;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/FRg;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1Ck;LX/03V;LX/0Or;LX/FRR;Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;LX/FRy;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ck;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "LX/737;",
            ">;",
            "LX/FRR;",
            "Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;",
            "LX/FRy;",
            "LX/0Or",
            "<",
            "LX/70D;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2240595
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2240596
    iput-object p1, p0, LX/FRg;->b:LX/1Ck;

    .line 2240597
    iput-object p2, p0, LX/FRg;->h:LX/03V;

    .line 2240598
    iput-object p3, p0, LX/FRg;->c:LX/0Or;

    .line 2240599
    iput-object p4, p0, LX/FRg;->f:LX/FRR;

    .line 2240600
    iput-object p5, p0, LX/FRg;->g:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    .line 2240601
    iput-object p6, p0, LX/FRg;->e:LX/FRy;

    .line 2240602
    iput-object p7, p0, LX/FRg;->d:LX/0Or;

    .line 2240603
    return-void
.end method

.method public static b(LX/FRg;LX/6zj;Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;)V
    .locals 2

    .prologue
    .line 2240604
    iget-object v0, p0, LX/FRg;->b:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2240605
    iget-object v0, p0, LX/FRg;->i:LX/70k;

    new-instance v1, LX/FRf;

    invoke-direct {v1, p0, p1, p2}, LX/FRf;-><init>(LX/FRg;LX/6zj;Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;)V

    invoke-virtual {v0, v1}, LX/70k;->a(LX/1DI;)V

    .line 2240606
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2240607
    iget-object v0, p0, LX/FRg;->b:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2240608
    return-void
.end method

.method public final bridge synthetic a(LX/6zj;Lcom/facebook/payments/picker/model/PickerRunTimeData;)V
    .locals 0

    .prologue
    .line 2240609
    check-cast p2, Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;

    invoke-virtual {p0, p1, p2}, LX/FRg;->a(LX/6zj;Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;)V

    return-void
.end method

.method public final a(LX/6zj;Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;)V
    .locals 9

    .prologue
    .line 2240610
    iget-object v0, p0, LX/FRg;->i:LX/70k;

    invoke-virtual {v0}, LX/70k;->a()V

    .line 2240611
    invoke-static {}, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->newBuilder()LX/FRt;

    move-result-object v0

    .line 2240612
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2240613
    new-instance v4, LX/FRa;

    invoke-direct {v4, p0, v0, p1, p2}, LX/FRa;-><init>(LX/FRg;LX/FRt;LX/6zj;Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;)V

    .line 2240614
    iget-object v2, p2, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->b:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    move-object v2, v2

    .line 2240615
    check-cast v2, Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;

    iget-boolean v2, v2, Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;->a:Z

    if-eqz v2, :cond_0

    .line 2240616
    iget-object v2, p0, LX/FRg;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/70D;

    invoke-virtual {p2}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v3

    check-cast v3, Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;

    invoke-virtual {v3}, Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->d:LX/6xg;

    invoke-static {v3}, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a(LX/6xg;)LX/70C;

    move-result-object v3

    invoke-virtual {v3}, LX/70C;->a()Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/6u3;->b(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2240617
    :goto_0
    iget-object v3, p0, LX/FRg;->b:LX/1Ck;

    const-string v5, "payment_setting_task_key"

    invoke-virtual {v3, v5, v2, v4}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2240618
    move-object v2, v4

    .line 2240619
    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240620
    new-instance v2, LX/FRb;

    invoke-direct {v2, p0, v0, p1, p2}, LX/FRb;-><init>(LX/FRg;LX/FRt;LX/6zj;Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;)V

    .line 2240621
    iget-object v3, p0, LX/FRg;->b:LX/1Ck;

    const-string v4, "payment_setting_task_key"

    iget-object v5, p0, LX/FRg;->f:LX/FRR;

    invoke-static {}, Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;->newBuilder()LX/FRO;

    move-result-object v6

    invoke-virtual {v6}, LX/FRO;->c()Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/FRR;->a(Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v2}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2240622
    move-object v2, v2

    .line 2240623
    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240624
    new-instance v2, LX/FRc;

    invoke-direct {v2, p0, v0, p1, p2}, LX/FRc;-><init>(LX/FRg;LX/FRt;LX/6zj;Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;)V

    .line 2240625
    iget-object v3, p0, LX/FRg;->b:LX/1Ck;

    const-string v4, "payment_setting_task_key"

    iget-object v5, p0, LX/FRg;->g:Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;

    invoke-virtual {v5}, Lcom/facebook/payments/auth/pin/protocol/PaymentPinProtocolUtil;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v2}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2240626
    move-object v2, v2

    .line 2240627
    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240628
    new-instance v2, LX/FRd;

    invoke-direct {v2, p0, v0, p1, p2}, LX/FRd;-><init>(LX/FRg;LX/FRt;LX/6zj;Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;)V

    .line 2240629
    iget-object v3, p0, LX/FRg;->b:LX/1Ck;

    const-string v4, "payment_setting_task_key"

    iget-object v5, p0, LX/FRg;->e:LX/FRy;

    invoke-virtual {v5}, LX/6tx;->a()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    invoke-virtual {v3, v4, v5, v2}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2240630
    move-object v2, v2

    .line 2240631
    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2240632
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2240633
    new-instance v3, LX/FRe;

    move-object v4, p0

    move-object v5, v0

    move-object v6, v1

    move-object v7, p1

    move-object v8, p2

    invoke-direct/range {v3 .. v8}, LX/FRe;-><init>(LX/FRg;LX/FRt;LX/0Px;LX/6zj;Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;)V

    .line 2240634
    iget-object v4, p2, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->b:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    move-object v4, v4

    .line 2240635
    check-cast v4, Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;

    iget-boolean v4, v4, Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;->b:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, LX/FRg;->c:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/737;

    invoke-virtual {v4}, LX/737;->g()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2240636
    :goto_1
    iget-object v5, p0, LX/FRg;->b:LX/1Ck;

    const-string v6, "payment_setting_task_key"

    invoke-virtual {v5, v6, v4, v3}, LX/1Ck;->c(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2240637
    return-void

    .line 2240638
    :cond_0
    iget-object v2, p0, LX/FRg;->d:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/70D;

    invoke-virtual {p2}, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->a()Lcom/facebook/payments/picker/model/PickerScreenConfig;

    move-result-object v3

    check-cast v3, Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;

    invoke-virtual {v3}, Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->d:LX/6xg;

    invoke-static {v3}, Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;->a(LX/6xg;)LX/70C;

    move-result-object v3

    invoke-virtual {v3}, LX/70C;->a()Lcom/facebook/payments/paymentmethods/picker/protocol/GetPaymentMethodsInfoParams;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/6u3;->c(Landroid/os/Parcelable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    goto/16 :goto_0

    .line 2240639
    :cond_1
    iget-object v4, p0, LX/FRg;->c:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/737;

    invoke-virtual {v4}, LX/737;->h()Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    goto :goto_1
.end method

.method public final a(LX/70k;)V
    .locals 0

    .prologue
    .line 2240640
    iput-object p1, p0, LX/FRg;->i:LX/70k;

    .line 2240641
    return-void
.end method

.method public final bridge synthetic b(LX/6zj;Lcom/facebook/payments/picker/model/PickerRunTimeData;)V
    .locals 0

    .prologue
    .line 2240642
    return-void
.end method
