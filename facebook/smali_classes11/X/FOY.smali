.class public LX/FOY;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/3Kw;

.field private final b:Z

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/01T;


# direct methods
.method public constructor <init>(LX/0Or;Ljava/lang/Boolean;LX/3Kw;LX/01T;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/IsPartialAccount;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Ljava/lang/Boolean;",
            "LX/3Kw;",
            "LX/01T;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2235228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2235229
    iput-object p1, p0, LX/FOY;->c:LX/0Or;

    .line 2235230
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/FOY;->b:Z

    .line 2235231
    iput-object p3, p0, LX/FOY;->a:LX/3Kw;

    .line 2235232
    iput-object p4, p0, LX/FOY;->d:LX/01T;

    .line 2235233
    return-void
.end method
