.class public LX/FjI;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "Lcom/facebook/search/suggestions/environment/CanVisitTypeaheadSuggestion;",
        ":",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FjJ;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/FjI",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/FjJ;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2276700
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2276701
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/FjI;->b:LX/0Zi;

    .line 2276702
    iput-object p1, p0, LX/FjI;->a:LX/0Ot;

    .line 2276703
    return-void
.end method

.method public static a(LX/0QB;)LX/FjI;
    .locals 4

    .prologue
    .line 2276657
    const-class v1, LX/FjI;

    monitor-enter v1

    .line 2276658
    :try_start_0
    sget-object v0, LX/FjI;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2276659
    sput-object v2, LX/FjI;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2276660
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2276661
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2276662
    new-instance v3, LX/FjI;

    const/16 p0, 0x3508

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/FjI;-><init>(LX/0Ot;)V

    .line 2276663
    move-object v0, v3

    .line 2276664
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2276665
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FjI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2276666
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2276667
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2276699
    const v0, -0x31b40b1e

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 11

    .prologue
    .line 2276677
    check-cast p2, LX/FjH;

    .line 2276678
    iget-object v0, p0, LX/FjI;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FjJ;

    iget-object v1, p2, LX/FjH;->a:Lcom/facebook/search/model/NullStateModuleSuggestionUnit;

    const/4 v5, 0x0

    .line 2276679
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/39p;->a(LX/1De;)LX/39s;

    move-result-object v3

    sget-object v4, LX/1Wk;->NEWSFEED_FULLBLEED_SHADOW:LX/1Wk;

    invoke-virtual {v3, v4}, LX/39s;->a(LX/1Wk;)LX/39s;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->c(LX/1n6;)LX/1Dh;

    move-result-object v2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2276680
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v8}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x6

    const v6, 0x7f0b0060

    invoke-interface {v3, v4, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x7

    const v6, 0x7f0b17d1

    invoke-interface {v3, v4, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v3

    .line 2276681
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v6, 0x2

    invoke-interface {v4, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v6, 0x1

    invoke-interface {v4, v6}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    const/4 p2, 0x4

    .line 2276682
    iget-object v6, v0, LX/FjJ;->a:LX/0ad;

    sget-short v9, LX/100;->bO:S

    const/4 v10, 0x0

    invoke-interface {v6, v9, v10}, LX/0ad;->a(SZ)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2276683
    const/4 v6, 0x0

    .line 2276684
    :goto_0
    move-object v6, v6

    .line 2276685
    invoke-interface {v4, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const/4 v6, 0x0

    const v9, 0x7f0e0977

    invoke-static {p1, v6, v9}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v6

    .line 2276686
    iget-object v9, v1, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->b:Ljava/lang/String;

    move-object v9, v9

    .line 2276687
    invoke-virtual {v6, v9}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    sget-object v9, LX/1nd;->CENTER:LX/1nd;

    invoke-virtual {v6, v9}, LX/1ne;->a(LX/1nd;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-interface {v6, v9}, LX/1Di;->a(F)LX/1Di;

    move-result-object v6

    invoke-interface {v4, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    move-object v4, v4

    .line 2276688
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0e012d

    invoke-static {p1, v7, v4}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v4

    invoke-virtual {v1}, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->o()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v8}, LX/1ne;->j(I)LX/1ne;

    move-result-object v4

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 v6, 0x4

    const/16 v7, 0x24

    invoke-interface {v4, v6, v7}, LX/1Di;->h(II)LX/1Di;

    move-result-object v4

    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    move-object v3, v3

    .line 2276689
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    iget-object v2, v0, LX/FjJ;->a:LX/0ad;

    sget-short v4, LX/100;->bB:S

    invoke-interface {v2, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    :goto_1
    invoke-interface {v3, v2}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    .line 2276690
    const v3, -0x31b40b1e

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2276691
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    .line 2276692
    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2276693
    return-object v0

    .line 2276694
    :cond_0
    invoke-static {p1}, LX/Fii;->c(LX/1De;)LX/Fig;

    move-result-object v2

    goto :goto_1

    :cond_1
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v9

    iget-object v6, v0, LX/FjJ;->b:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/1vg;

    invoke-virtual {v6, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v10

    .line 2276695
    iget-object v6, v1, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->a:LX/3bj;

    move-object v6, v6

    .line 2276696
    sget-object p0, LX/3bj;->ns_trending:LX/3bj;

    if-ne v6, p0, :cond_2

    const v6, -0xbf7f01

    :goto_2
    invoke-virtual {v10, v6}, LX/2xv;->i(I)LX/2xv;

    move-result-object v10

    .line 2276697
    iget-object v6, v1, Lcom/facebook/search/model/NullStateModuleSuggestionUnit;->a:LX/3bj;

    move-object v6, v6

    .line 2276698
    sget-object p0, LX/3bj;->ns_trending:LX/3bj;

    if-ne v6, p0, :cond_3

    const v6, 0x7f020a10

    :goto_3
    invoke-virtual {v10, v6}, LX/2xv;->h(I)LX/2xv;

    move-result-object v6

    invoke-virtual {v9, v6}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/4 v9, 0x1

    invoke-interface {v6, v9, p2}, LX/1Di;->d(II)LX/1Di;

    move-result-object v6

    invoke-interface {v6, p2, p2}, LX/1Di;->d(II)LX/1Di;

    move-result-object v6

    const/4 v9, 0x5

    const/16 v10, 0x10

    invoke-interface {v6, v9, v10}, LX/1Di;->d(II)LX/1Di;

    move-result-object v6

    goto/16 :goto_0

    :cond_2
    const v6, -0x6f6b64

    goto :goto_2

    :cond_3
    const v6, 0x7f02091d

    goto :goto_3
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2276668
    invoke-static {}, LX/1dS;->b()V

    .line 2276669
    iget v0, p1, LX/1dQ;->b:I

    .line 2276670
    packed-switch v0, :pswitch_data_0

    .line 2276671
    :goto_0
    return-object v1

    .line 2276672
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 2276673
    check-cast v0, LX/FjH;

    .line 2276674
    iget-object v2, p0, LX/FjI;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FjJ;

    iget-object p1, v0, LX/FjH;->a:Lcom/facebook/search/model/NullStateModuleSuggestionUnit;

    iget-object p2, v0, LX/FjH;->b:LX/FhE;

    .line 2276675
    invoke-virtual {p2, p1}, LX/FhE;->a(Lcom/facebook/search/model/TypeaheadUnit;)V

    .line 2276676
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x31b40b1e
        :pswitch_0
    .end packed-switch
.end method
