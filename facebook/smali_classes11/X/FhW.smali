.class public LX/FhW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/FhW;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/SearchTypeaheadNetworkExecutor;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0TV;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FhX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/7Ba;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/7BX;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2273099
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2273100
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2273101
    iput-object v0, p0, LX/FhW;->a:LX/0Ot;

    .line 2273102
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2273103
    iput-object v0, p0, LX/FhW;->b:LX/0Ot;

    .line 2273104
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2273105
    iput-object v0, p0, LX/FhW;->c:LX/0Ot;

    .line 2273106
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2273107
    iput-object v0, p0, LX/FhW;->d:LX/0Ot;

    .line 2273108
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2273109
    iput-object v0, p0, LX/FhW;->e:LX/0Ot;

    .line 2273110
    return-void
.end method

.method public static a(LX/0QB;)LX/FhW;
    .locals 10

    .prologue
    .line 2273111
    sget-object v0, LX/FhW;->h:LX/FhW;

    if-nez v0, :cond_1

    .line 2273112
    const-class v1, LX/FhW;

    monitor-enter v1

    .line 2273113
    :try_start_0
    sget-object v0, LX/FhW;->h:LX/FhW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2273114
    if-eqz v2, :cond_0

    .line 2273115
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2273116
    new-instance v3, LX/FhW;

    invoke-direct {v3}, LX/FhW;-><init>()V

    .line 2273117
    const/16 v4, 0xb83

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x277

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xac0

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1032

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x34c2

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x32b0

    invoke-static {v0, v9}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v9

    const/16 p0, 0x32af

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2273118
    iput-object v4, v3, LX/FhW;->a:LX/0Ot;

    iput-object v5, v3, LX/FhW;->b:LX/0Ot;

    iput-object v6, v3, LX/FhW;->c:LX/0Ot;

    iput-object v7, v3, LX/FhW;->d:LX/0Ot;

    iput-object v8, v3, LX/FhW;->e:LX/0Ot;

    iput-object v9, v3, LX/FhW;->f:LX/0Or;

    iput-object p0, v3, LX/FhW;->g:LX/0Or;

    .line 2273119
    move-object v0, v3

    .line 2273120
    sput-object v0, LX/FhW;->h:LX/FhW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2273121
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2273122
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2273123
    :cond_1
    sget-object v0, LX/FhW;->h:LX/FhW;

    return-object v0

    .line 2273124
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2273125
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
