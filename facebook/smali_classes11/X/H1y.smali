.class public final LX/H1y;
.super LX/0Tz;
.source ""


# static fields
.field public static final a:LX/0sv;

.field private static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0sv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    .line 2416098
    new-instance v0, LX/0su;

    sget-object v1, LX/H1x;->a:LX/0U1;

    sget-object v2, LX/H1x;->b:LX/0U1;

    sget-object v3, LX/H1x;->c:LX/0U1;

    invoke-static {v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0su;-><init>(LX/0Px;)V

    sput-object v0, LX/H1y;->a:LX/0sv;

    .line 2416099
    sget-object v0, LX/H1x;->a:LX/0U1;

    sget-object v1, LX/H1x;->b:LX/0U1;

    sget-object v2, LX/H1x;->c:LX/0U1;

    sget-object v3, LX/H1x;->d:LX/0U1;

    sget-object v4, LX/H1x;->e:LX/0U1;

    sget-object v5, LX/H1x;->f:LX/0U1;

    sget-object v6, LX/H1x;->g:LX/0U1;

    sget-object v7, LX/H1x;->h:LX/0U1;

    sget-object v8, LX/H1x;->i:LX/0U1;

    sget-object v9, LX/H1x;->j:LX/0U1;

    invoke-static/range {v0 .. v9}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/H1y;->b:LX/0Px;

    .line 2416100
    sget-object v0, LX/H1y;->a:LX/0sv;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/H1y;->c:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 2416093
    const-string v0, "nearby_tiles"

    sget-object v1, LX/H1y;->b:LX/0Px;

    sget-object v2, LX/H1y;->c:LX/0Px;

    invoke-direct {p0, v0, v1, v2}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;LX/0Px;)V

    .line 2416094
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 2416095
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2416096
    const-string v0, "nearby_tiles"

    const-string v1, "nearby_tiles_index"

    sget-object v2, LX/H1x;->j:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x3cb3afba

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x72c6d6

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2416097
    return-void
.end method
