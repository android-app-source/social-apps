.class public final enum LX/G8u;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/G8u;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/G8u;

.field public static final enum AZTEC_LAYERS:LX/G8u;

.field public static final enum CHARACTER_SET:LX/G8u;

.field public static final enum DATA_MATRIX_SHAPE:LX/G8u;

.field public static final enum ERROR_CORRECTION:LX/G8u;

.field public static final enum MARGIN:LX/G8u;

.field public static final enum MAX_SIZE:LX/G8u;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum MIN_SIZE:LX/G8u;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum PDF417_COMPACT:LX/G8u;

.field public static final enum PDF417_COMPACTION:LX/G8u;

.field public static final enum PDF417_DIMENSIONS:LX/G8u;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2321289
    new-instance v0, LX/G8u;

    const-string v1, "ERROR_CORRECTION"

    invoke-direct {v0, v1, v3}, LX/G8u;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8u;->ERROR_CORRECTION:LX/G8u;

    .line 2321290
    new-instance v0, LX/G8u;

    const-string v1, "CHARACTER_SET"

    invoke-direct {v0, v1, v4}, LX/G8u;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8u;->CHARACTER_SET:LX/G8u;

    .line 2321291
    new-instance v0, LX/G8u;

    const-string v1, "DATA_MATRIX_SHAPE"

    invoke-direct {v0, v1, v5}, LX/G8u;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8u;->DATA_MATRIX_SHAPE:LX/G8u;

    .line 2321292
    new-instance v0, LX/G8u;

    const-string v1, "MIN_SIZE"

    invoke-direct {v0, v1, v6}, LX/G8u;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8u;->MIN_SIZE:LX/G8u;

    .line 2321293
    new-instance v0, LX/G8u;

    const-string v1, "MAX_SIZE"

    invoke-direct {v0, v1, v7}, LX/G8u;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8u;->MAX_SIZE:LX/G8u;

    .line 2321294
    new-instance v0, LX/G8u;

    const-string v1, "MARGIN"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/G8u;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8u;->MARGIN:LX/G8u;

    .line 2321295
    new-instance v0, LX/G8u;

    const-string v1, "PDF417_COMPACT"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/G8u;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8u;->PDF417_COMPACT:LX/G8u;

    .line 2321296
    new-instance v0, LX/G8u;

    const-string v1, "PDF417_COMPACTION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/G8u;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8u;->PDF417_COMPACTION:LX/G8u;

    .line 2321297
    new-instance v0, LX/G8u;

    const-string v1, "PDF417_DIMENSIONS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/G8u;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8u;->PDF417_DIMENSIONS:LX/G8u;

    .line 2321298
    new-instance v0, LX/G8u;

    const-string v1, "AZTEC_LAYERS"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/G8u;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8u;->AZTEC_LAYERS:LX/G8u;

    .line 2321299
    const/16 v0, 0xa

    new-array v0, v0, [LX/G8u;

    sget-object v1, LX/G8u;->ERROR_CORRECTION:LX/G8u;

    aput-object v1, v0, v3

    sget-object v1, LX/G8u;->CHARACTER_SET:LX/G8u;

    aput-object v1, v0, v4

    sget-object v1, LX/G8u;->DATA_MATRIX_SHAPE:LX/G8u;

    aput-object v1, v0, v5

    sget-object v1, LX/G8u;->MIN_SIZE:LX/G8u;

    aput-object v1, v0, v6

    sget-object v1, LX/G8u;->MAX_SIZE:LX/G8u;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/G8u;->MARGIN:LX/G8u;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/G8u;->PDF417_COMPACT:LX/G8u;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/G8u;->PDF417_COMPACTION:LX/G8u;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/G8u;->PDF417_DIMENSIONS:LX/G8u;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/G8u;->AZTEC_LAYERS:LX/G8u;

    aput-object v2, v0, v1

    sput-object v0, LX/G8u;->$VALUES:[LX/G8u;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2321300
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/G8u;
    .locals 1

    .prologue
    .line 2321301
    const-class v0, LX/G8u;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/G8u;

    return-object v0
.end method

.method public static values()[LX/G8u;
    .locals 1

    .prologue
    .line 2321302
    sget-object v0, LX/G8u;->$VALUES:[LX/G8u;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/G8u;

    return-object v0
.end method
