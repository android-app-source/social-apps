.class public final LX/GgF;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/view/View;

.field public final b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/widget/TextView;

.field public final e:Landroid/widget/TextView;

.field public final f:Lcom/facebook/fbui/glyph/GlyphView;

.field public final g:Landroid/view/ViewStub;

.field public h:Landroid/view/View;

.field public final synthetic i:LX/GgG;


# direct methods
.method public constructor <init>(LX/GgG;)V
    .locals 2

    .prologue
    .line 2378818
    iput-object p1, p0, LX/GgF;->i:LX/GgG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2378819
    const v0, 0x7f0d0d47

    .line 2378820
    invoke-virtual {p1, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2378821
    iput-object v0, p0, LX/GgF;->a:Landroid/view/View;

    .line 2378822
    const v0, 0x7f0d0d48

    .line 2378823
    invoke-virtual {p1, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2378824
    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/GgF;->b:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2378825
    const v0, 0x7f0d0d49

    .line 2378826
    invoke-virtual {p1, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2378827
    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/GgF;->c:Landroid/widget/TextView;

    .line 2378828
    const v0, 0x7f0d0d4b

    .line 2378829
    invoke-virtual {p1, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2378830
    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/GgF;->d:Landroid/widget/TextView;

    .line 2378831
    const v0, 0x7f0d0d4c

    .line 2378832
    invoke-virtual {p1, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2378833
    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/GgF;->e:Landroid/widget/TextView;

    .line 2378834
    const v0, 0x7f0d0d4d

    .line 2378835
    invoke-virtual {p1, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2378836
    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/GgF;->f:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2378837
    const v0, 0x7f0d0d46

    .line 2378838
    invoke-virtual {p1, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    .line 2378839
    check-cast v0, Landroid/view/ViewStub;

    iput-object v0, p0, LX/GgF;->g:Landroid/view/ViewStub;

    .line 2378840
    iget-object v0, p0, LX/GgF;->c:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2378841
    iget-object v0, p0, LX/GgF;->d:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2378842
    iget-object v0, p0, LX/GgF;->e:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2378843
    iget-object v0, p0, LX/GgF;->c:Landroid/widget/TextView;

    sget-object v1, LX/1vY;->TITLE:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 2378844
    iget-object v0, p0, LX/GgF;->d:Landroid/widget/TextView;

    sget-object v1, LX/1vY;->SUBTITLE:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 2378845
    iget-object v0, p0, LX/GgF;->e:Landroid/widget/TextView;

    sget-object v1, LX/1vY;->DESCRIPTION:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 2378846
    iget-object v0, p0, LX/GgF;->f:Lcom/facebook/fbui/glyph/GlyphView;

    sget-object v1, LX/1vY;->ACTION_ICON:LX/1vY;

    invoke-static {v0, v1}, LX/1vZ;->a(Landroid/view/View;LX/1vY;)V

    .line 2378847
    return-void
.end method
