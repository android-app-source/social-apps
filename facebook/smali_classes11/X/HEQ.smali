.class public LX/HEQ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/CXj;

.field public final b:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field public final c:LX/0id;

.field public final d:LX/8E2;

.field public e:I

.field public f:I

.field public g:LX/CXr;

.field public h:Ljava/lang/String;

.field public i:LX/03R;

.field public j:Ljava/lang/String;

.field public final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/CXp;


# direct methods
.method public constructor <init>(LX/CXj;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0id;LX/8E2;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2442020
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2442021
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/HEQ;->k:Ljava/util/Map;

    .line 2442022
    iput-object p1, p0, LX/HEQ;->a:LX/CXj;

    .line 2442023
    iput-object p2, p0, LX/HEQ;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 2442024
    iput-object p3, p0, LX/HEQ;->c:LX/0id;

    .line 2442025
    iput-object p4, p0, LX/HEQ;->d:LX/8E2;

    .line 2442026
    return-void
.end method

.method public static a(LX/0QB;)LX/HEQ;
    .locals 5

    .prologue
    .line 2442027
    new-instance v4, LX/HEQ;

    invoke-static {p0}, LX/CXj;->a(LX/0QB;)LX/CXj;

    move-result-object v0

    check-cast v0, LX/CXj;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v1

    check-cast v1, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p0}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v2

    check-cast v2, LX/0id;

    invoke-static {p0}, LX/8E2;->a(LX/0QB;)LX/8E2;

    move-result-object v3

    check-cast v3, LX/8E2;

    invoke-direct {v4, v0, v1, v2, v3}, LX/HEQ;-><init>(LX/CXj;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0id;LX/8E2;)V

    .line 2442028
    move-object v0, v4

    .line 2442029
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2442030
    iget-object v0, p0, LX/HEQ;->k:Ljava/util/Map;

    const-string v1, "true"

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2442031
    iget-object v0, p0, LX/HEQ;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x130080

    iget-object v2, p0, LX/HEQ;->j:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    invoke-interface {v0, v1, v2, p1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 2442032
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    const v5, 0x130080

    .line 2442033
    iget-object v0, p0, LX/HEQ;->k:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2442034
    iget-object v0, p0, LX/HEQ;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/HEQ;->j:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    invoke-interface {v0, v5, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->j(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2442035
    iget-object v0, p0, LX/HEQ;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    iget-object v1, p0, LX/HEQ;->j:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    const-string v2, "%s=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v5, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 2442036
    :cond_0
    return-void
.end method
