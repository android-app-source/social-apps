.class public final LX/GVy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GVw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GVw",
        "<",
        "LX/GWj;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2360262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static c(Landroid/view/ViewGroup;)LX/GWj;
    .locals 2

    .prologue
    .line 2360260
    new-instance v0, LX/GWj;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/GWj;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2360261
    invoke-static {p1}, LX/GVy;->c(Landroid/view/ViewGroup;)LX/GWj;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Landroid/view/ViewGroup;LX/7iP;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2360259
    invoke-static {p1}, LX/GVy;->c(Landroid/view/ViewGroup;)LX/GWj;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;
    .locals 1

    .prologue
    .line 2360258
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->INSIGHTS_AND_PROMOTION:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    return-object v0
.end method

.method public final a(Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2360247
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2360248
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2360249
    if-eqz v2, :cond_2

    .line 2360250
    invoke-virtual {v3, v2, v1}, LX/15i;->j(II)I

    move-result v4

    const/4 v5, 0x2

    invoke-virtual {v3, v2, v5}, LX/15i;->h(II)Z

    move-result v2

    invoke-static {v4, v2}, LX/7j5;->a(IZ)Z

    move-result v3

    .line 2360251
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->m()Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    move-result-object v2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;->VISIBLE:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    if-ne v2, v4, :cond_0

    move v2, v0

    .line 2360252
    :goto_0
    if-eqz v3, :cond_1

    if-eqz v2, :cond_1

    .line 2360253
    :goto_1
    return v0

    .line 2360254
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move v2, v1

    .line 2360255
    goto :goto_0

    :cond_1
    move v0, v1

    .line 2360256
    goto :goto_1

    :cond_2
    move v0, v1

    .line 2360257
    goto :goto_1
.end method
