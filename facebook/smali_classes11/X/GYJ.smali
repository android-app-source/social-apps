.class public final LX/GYJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;)V
    .locals 0

    .prologue
    .line 2364984
    iput-object p1, p0, LX/GYJ;->a:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    .line 2364985
    iget-object v0, p0, LX/GYJ;->a:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    .line 2364986
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->m:LX/4At;

    invoke-virtual {v1}, LX/4At;->beginShowingProgress()V

    .line 2364987
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->b:LX/1Ck;

    sget-object v2, LX/GYM;->DELETE_SHOP_MUTATION:LX/GYM;

    iget-object v3, v0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->c:LX/GY1;

    iget-object v4, v0, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->q:LX/GXY;

    .line 2364988
    invoke-static {v4}, LX/GXY;->m(LX/GXY;)Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel$CommerceMerchantSettingsModel;

    move-result-object v5

    .line 2364989
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/facebook/commerce/publishing/graphql/FetchCommerceStoreQueryModels$CommerceStoreFieldsModel$CommerceMerchantSettingsModel;->k()Ljava/lang/String;

    move-result-object v5

    :goto_0
    move-object v4, v5

    .line 2364990
    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2364991
    new-instance v5, LX/7jt;

    invoke-direct {v5}, LX/7jt;-><init>()V

    move-object v5, v5

    .line 2364992
    const-string p0, "input"

    new-instance p1, LX/4DW;

    invoke-direct {p1}, LX/4DW;-><init>()V

    .line 2364993
    const-string p2, "commerce_merchant_settings_id"

    invoke-virtual {p1, p2, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2364994
    move-object p1, p1

    .line 2364995
    invoke-virtual {v5, p0, p1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v5

    check-cast v5, LX/7jt;

    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v5

    .line 2364996
    iget-object p0, v3, LX/GY1;->a:LX/0tX;

    invoke-virtual {p0, v5}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    invoke-static {v5}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 2364997
    new-instance p0, LX/GXz;

    invoke-direct {p0, v3}, LX/GXz;-><init>(LX/GY1;)V

    invoke-static {v5, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2364998
    move-object v3, v5

    .line 2364999
    new-instance v4, LX/GYH;

    invoke-direct {v4, v0}, LX/GYH;-><init>(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2365000
    return-void

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method
