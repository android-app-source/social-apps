.class public LX/Gp2;
.super LX/Goq;
.source ""

# interfaces
.implements LX/GoX;
.implements LX/GoY;
.implements LX/GoZ;
.implements LX/Gob;


# instance fields
.field public final a:LX/CHh;


# direct methods
.method public constructor <init>(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;I)V
    .locals 2

    .prologue
    .line 2394692
    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->M()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel$ElementDescriptorModel;

    move-result-object v0

    const/16 v1, 0x69

    invoke-direct {p0, v0, v1, p2}, LX/Goq;-><init>(LX/CHa;II)V

    .line 2394693
    iput-object p1, p0, LX/Gp2;->a:LX/CHh;

    .line 2394694
    return-void
.end method


# virtual methods
.method public final D()LX/GoE;
    .locals 3

    .prologue
    .line 2394698
    new-instance v0, LX/GoE;

    iget-object v1, p0, LX/Gp2;->a:LX/CHh;

    invoke-interface {v1}, LX/CHf;->jt_()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Gp2;->a:LX/CHh;

    invoke-interface {v2}, LX/CHf;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/GoE;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final e()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394695
    iget-object v0, p0, LX/Gp2;->a:LX/CHh;

    invoke-interface {v0}, LX/CHh;->q()LX/CHZ;

    move-result-object v0

    invoke-interface {v0}, LX/CHY;->c()LX/CHa;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2394696
    iget-object v0, p0, LX/Gp2;->a:LX/CHh;

    invoke-interface {v0}, LX/CHh;->q()LX/CHZ;

    move-result-object v0

    invoke-interface {v0}, LX/CHY;->c()LX/CHa;

    move-result-object v0

    invoke-interface {v0}, LX/CHa;->jv_()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;

    move-result-object v0

    .line 2394697
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
