.class public final LX/GRC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:LX/GRD;


# direct methods
.method public constructor <init>(LX/GRD;I)V
    .locals 0

    .prologue
    .line 2351417
    iput-object p1, p0, LX/GRC;->b:LX/GRD;

    iput p2, p0, LX/GRC;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x20934135

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2351418
    iget-object v1, p0, LX/GRC;->b:LX/GRD;

    iget-object v1, v1, LX/GRD;->b:LX/GTB;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/GRC;->b:LX/GRD;

    iget-object v3, v3, LX/GRD;->a:LX/GRZ;

    iget v4, p0, LX/GRC;->a:I

    const/4 v8, 0x0

    .line 2351419
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f0300f7

    const/4 v9, 0x0

    invoke-virtual {v6, v7, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    .line 2351420
    const v6, 0x7f0d057e

    invoke-virtual {v7, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/facebook/appinvites/ui/AppInviteContentView;

    .line 2351421
    invoke-virtual {v1, v6, v3, v4}, LX/GTB;->a(Lcom/facebook/appinvites/ui/AppInviteContentView;LX/GRZ;I)V

    .line 2351422
    const v9, 0x7fffffff

    invoke-virtual {v6, v9}, Lcom/facebook/appinvites/ui/AppInviteContentView;->setMessageMaxLines(I)V

    .line 2351423
    const v6, 0x7f0d056e

    invoke-virtual {v7, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;

    .line 2351424
    invoke-static {v1, v6, v3}, LX/GTB;->a(LX/GTB;Lcom/facebook/appinvites/ui/AppInviteAppDetailsView;LX/GRZ;)V

    .line 2351425
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v9, 0x7f0b0060

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v6

    float-to-int v9, v6

    .line 2351426
    new-instance v6, LX/0ju;

    invoke-direct {v6, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    move v10, v8

    move v11, v9

    invoke-virtual/range {v6 .. v11}, LX/0ju;->a(Landroid/view/View;IIII)LX/0ju;

    move-result-object v6

    invoke-virtual {v6}, LX/0ju;->a()LX/2EJ;

    move-result-object v6

    invoke-virtual {v6}, LX/2EJ;->show()V

    .line 2351427
    const v1, 0x8f07b5c

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
