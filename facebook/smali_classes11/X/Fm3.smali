.class public final LX/Fm3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserSendInvitesMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;)V
    .locals 0

    .prologue
    .line 2281961
    iput-object p1, p0, LX/Fm3;->a:Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2281965
    iget-object v0, p0, LX/Fm3;->a:Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;->x:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f083348

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2281966
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2281962
    iget-object v0, p0, LX/Fm3;->a:Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;->x:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f082233

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2281963
    iget-object v0, p0, LX/Fm3;->a:Lcom/facebook/socialgood/inviter/FundraiserPageInviterFragment;

    invoke-virtual {v0}, Lcom/facebook/widget/friendselector/GenericFriendsSelectorFragment;->z()V

    .line 2281964
    return-void
.end method
