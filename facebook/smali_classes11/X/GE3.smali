.class public LX/GE3;
.super Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod",
        "<",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/2U3;

.field private final b:LX/0W9;


# direct methods
.method public constructor <init>(LX/0sa;LX/0rq;LX/0tX;LX/1Ck;LX/GF4;LX/GG6;LX/2U3;LX/0ad;LX/0W9;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2330817
    move-object v1, p0

    move-object v2, p3

    move-object v3, p2

    move-object v4, p1

    move-object v5, p5

    move-object v6, p4

    move-object v7, p6

    move-object/from16 v8, p8

    invoke-direct/range {v1 .. v8}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;-><init>(LX/0tX;LX/0rq;LX/0sa;LX/GF4;LX/1Ck;LX/GG6;LX/0ad;)V

    .line 2330818
    move-object/from16 v0, p7

    iput-object v0, p0, LX/GE3;->a:LX/2U3;

    .line 2330819
    move-object/from16 v0, p9

    iput-object v0, p0, LX/GE3;->b:LX/0W9;

    .line 2330820
    return-void
.end method

.method public static a(LX/0QB;)LX/GE3;
    .locals 11

    .prologue
    .line 2330821
    new-instance v1, LX/GE3;

    invoke-static {p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v2

    check-cast v2, LX/0sa;

    invoke-static {p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v3

    check-cast v3, LX/0rq;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {p0}, LX/GF4;->a(LX/0QB;)LX/GF4;

    move-result-object v6

    check-cast v6, LX/GF4;

    invoke-static {p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v7

    check-cast v7, LX/GG6;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v8

    check-cast v8, LX/2U3;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v10

    check-cast v10, LX/0W9;

    invoke-direct/range {v1 .. v10}, LX/GE3;-><init>(LX/0sa;LX/0rq;LX/0tX;LX/1Ck;LX/GF4;LX/GG6;LX/2U3;LX/0ad;LX/0W9;)V

    .line 2330822
    move-object v0, v1

    .line 2330823
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;Ljava/lang/String;)Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;
    .locals 4

    .prologue
    .line 2330824
    new-instance v0, LX/GGN;

    invoke-direct {v0}, LX/GGN;-><init>()V

    invoke-virtual {p0, p1}, LX/GE3;->a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-result-object v1

    .line 2330825
    iput-object v1, v0, LX/GGN;->a:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2330826
    move-object v0, v0

    .line 2330827
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    iget-object v1, p0, LX/GE3;->b:LX/0W9;

    invoke-static {p1, v2, v3, v1}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;DLX/0W9;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v1

    .line 2330828
    iput-object v1, v0, LX/GGN;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2330829
    move-object v0, v0

    .line 2330830
    invoke-virtual {p0, p1}, LX/GE3;->b(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v1

    .line 2330831
    iput-object v1, v0, LX/GGN;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 2330832
    move-object v0, v0

    .line 2330833
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v1

    .line 2330834
    iput-object v1, v0, LX/GGI;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    .line 2330835
    move-object v0, v0

    .line 2330836
    iput-object p2, v0, LX/GGI;->c:Ljava/lang/String;

    .line 2330837
    move-object v0, v0

    .line 2330838
    invoke-virtual {p0, p1}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->c(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)LX/GGB;

    move-result-object v1

    .line 2330839
    iput-object v1, v0, LX/GGI;->e:LX/GGB;

    .line 2330840
    move-object v0, v0

    .line 2330841
    sget-object v1, LX/8wL;->PROMOTE_CTA:LX/8wL;

    .line 2330842
    iput-object v1, v0, LX/GGI;->b:LX/8wL;

    .line 2330843
    move-object v0, v0

    .line 2330844
    const-string v1, "boosted_cta_mobile"

    .line 2330845
    iput-object v1, v0, LX/GGI;->m:Ljava/lang/String;

    .line 2330846
    move-object v0, v0

    .line 2330847
    invoke-virtual {p0, p1}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->d(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)LX/0Px;

    move-result-object v1

    .line 2330848
    iput-object v1, v0, LX/GGI;->n:LX/0Px;

    .line 2330849
    move-object v0, v0

    .line 2330850
    invoke-virtual {p0, p1}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->e(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)I

    move-result v1

    .line 2330851
    iput v1, v0, LX/GGI;->o:I

    .line 2330852
    move-object v0, v0

    .line 2330853
    invoke-virtual {v0}, LX/GGI;->a()Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2330854
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->t()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2330855
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->t()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v1

    .line 2330856
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->g:Ljava/lang/String;

    .line 2330857
    :cond_0
    return-object v0
.end method

.method public final a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/model/CreativeAdModel;
    .locals 9

    .prologue
    const/4 v7, 0x4

    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2330858
    invoke-virtual {p0, p1}, LX/GE3;->b(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v3

    .line 2330859
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->s()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v5, v0, LX/1vs;->b:I

    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2330860
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_5

    move v0, v1

    :goto_2
    if-eqz v0, :cond_8

    .line 2330861
    iget-object v0, p0, LX/GE3;->a:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v3, "Default Spec is null"

    invoke-virtual {v0, v1, v3}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2330862
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->o()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_7

    .line 2330863
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->o()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2330864
    const-class v3, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2330865
    :goto_3
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->j()LX/1vs;

    move-result-object v1

    iget-object v3, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2330866
    new-instance v6, LX/GGK;

    invoke-direct {v6}, LX/GGK;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->p()Ljava/lang/String;

    move-result-object v7

    .line 2330867
    iput-object v7, v6, LX/GGK;->c:Ljava/lang/String;

    .line 2330868
    move-object v6, v6

    .line 2330869
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->r()Ljava/lang/String;

    move-result-object v7

    .line 2330870
    iput-object v7, v6, LX/GGK;->d:Ljava/lang/String;

    .line 2330871
    move-object v6, v6

    .line 2330872
    invoke-virtual {v3, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2330873
    iput-object v1, v6, LX/GGK;->e:Ljava/lang/String;

    .line 2330874
    move-object v1, v6

    .line 2330875
    invoke-virtual {v4, v5, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    .line 2330876
    iput-object v3, v1, LX/GGK;->f:Ljava/lang/String;

    .line 2330877
    move-object v1, v1

    .line 2330878
    iput-object v0, v1, LX/GGK;->h:Ljava/lang/String;

    .line 2330879
    move-object v1, v1

    .line 2330880
    const-class v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v4, v5, v2, v0, v3}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2330881
    iput-object v0, v1, LX/GGK;->a:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2330882
    move-object v0, v1

    .line 2330883
    invoke-virtual {v4, v5, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2330884
    iput-object v1, v0, LX/GGK;->b:Ljava/lang/String;

    .line 2330885
    move-object v0, v0

    .line 2330886
    invoke-virtual {v0}, LX/GGK;->a()Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-result-object v0

    .line 2330887
    :goto_4
    return-object v0

    .line 2330888
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2330889
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2330890
    if-nez v0, :cond_2

    move v0, v1

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto/16 :goto_0

    .line 2330891
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->m()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2330892
    invoke-virtual {v6, v0, v7}, LX/15i;->g(II)I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    goto/16 :goto_1

    :cond_4
    move v0, v2

    goto/16 :goto_1

    .line 2330893
    :cond_5
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->m()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2330894
    invoke-virtual {v6, v0, v7}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v6, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    goto/16 :goto_2

    :cond_6
    move v0, v2

    goto/16 :goto_2

    .line 2330895
    :cond_7
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->u()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_3

    .line 2330896
    :cond_8
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->m()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3, v0, v7}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2330897
    new-instance v6, LX/GGK;

    invoke-direct {v6}, LX/GGK;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->p()Ljava/lang/String;

    move-result-object v7

    .line 2330898
    iput-object v7, v6, LX/GGK;->c:Ljava/lang/String;

    .line 2330899
    move-object v6, v6

    .line 2330900
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->r()Ljava/lang/String;

    move-result-object v7

    .line 2330901
    iput-object v7, v6, LX/GGK;->d:Ljava/lang/String;

    .line 2330902
    move-object v6, v6

    .line 2330903
    invoke-virtual {v3, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2330904
    iput-object v1, v6, LX/GGK;->e:Ljava/lang/String;

    .line 2330905
    move-object v1, v6

    .line 2330906
    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    .line 2330907
    iput-object v6, v1, LX/GGK;->f:Ljava/lang/String;

    .line 2330908
    move-object v1, v1

    .line 2330909
    invoke-virtual {v3, v0, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2330910
    iput-object v0, v1, LX/GGK;->h:Ljava/lang/String;

    .line 2330911
    move-object v1, v1

    .line 2330912
    const-class v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v4, v5, v2, v0, v3}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2330913
    iput-object v0, v1, LX/GGK;->a:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2330914
    move-object v0, v1

    .line 2330915
    invoke-virtual {v4, v5, v8}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2330916
    iput-object v1, v0, LX/GGK;->b:Ljava/lang/String;

    .line 2330917
    move-object v0, v0

    .line 2330918
    invoke-virtual {v0}, LX/GGK;->a()Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-result-object v0

    goto/16 :goto_4

    .line 2330919
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2330920
    const-string v0, "cta_promotion_key"

    return-object v0
.end method

.method public final b(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2330921
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteCTAAdminInfoModel$BoostedCtaPromotionsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteCTAAdminInfoModel$BoostedCtaPromotionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteCTAAdminInfoModel$BoostedCtaPromotionsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2330922
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteCTAAdminInfoModel$BoostedCtaPromotionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteCTAAdminInfoModel$BoostedCtaPromotionsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 2330923
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2330924
    const-string v0, "boosted_cta_mobile"

    return-object v0
.end method
