.class public LX/G4l;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/Fsr;

.field public d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/Fsr;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/Fsr;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2317954
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2317955
    iput-object p1, p0, LX/G4l;->a:Landroid/content/Context;

    .line 2317956
    iput-object p2, p0, LX/G4l;->b:LX/0Or;

    .line 2317957
    iput-object p3, p0, LX/G4l;->c:LX/Fsr;

    .line 2317958
    return-void
.end method

.method public static a(LX/0QB;)LX/G4l;
    .locals 6

    .prologue
    .line 2317959
    const-class v1, LX/G4l;

    monitor-enter v1

    .line 2317960
    :try_start_0
    sget-object v0, LX/G4l;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2317961
    sput-object v2, LX/G4l;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2317962
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2317963
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2317964
    new-instance v5, LX/G4l;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    const/16 v4, 0x122d

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/Fsr;->a(LX/0QB;)LX/Fsr;

    move-result-object v4

    check-cast v4, LX/Fsr;

    invoke-direct {v5, v3, p0, v4}, LX/G4l;-><init>(Landroid/content/Context;LX/0Or;LX/Fsr;)V

    .line 2317965
    move-object v0, v5

    .line 2317966
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2317967
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/G4l;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2317968
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2317969
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/G4l;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2317970
    invoke-static {p1}, LX/7Fv;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2317971
    iget-object v0, p0, LX/G4l;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gt;

    .line 2317972
    iput-object p1, v0, LX/0gt;->a:Ljava/lang/String;

    .line 2317973
    move-object v0, v0

    .line 2317974
    iget-object v1, p0, LX/G4l;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/0gt;->b(Landroid/content/Context;)V

    .line 2317975
    :goto_0
    return-void

    .line 2317976
    :cond_0
    iget-object v0, p0, LX/G4l;->c:LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->F()V

    goto :goto_0
.end method


# virtual methods
.method public final a(Z)V
    .locals 0

    .prologue
    .line 2317977
    iput-boolean p1, p0, LX/G4l;->d:Z

    .line 2317978
    return-void
.end method
