.class public LX/GpZ;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "LX/GqA;",
        "Lcom/facebook/instantshopping/model/data/CulturalMomentPopularMediaBlockData;",
        ">;"
    }
.end annotation


# instance fields
.field public d:LX/Go7;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/Go4;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private f:LX/GoE;


# direct methods
.method public constructor <init>(LX/GqA;)V
    .locals 2

    .prologue
    .line 2395150
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2395151
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/GpZ;

    invoke-static {v0}, LX/Go7;->a(LX/0QB;)LX/Go7;

    move-result-object p1

    check-cast p1, LX/Go7;

    invoke-static {v0}, LX/Go4;->a(LX/0QB;)LX/Go4;

    move-result-object v0

    check-cast v0, LX/Go4;

    iput-object p1, p0, LX/GpZ;->d:LX/Go7;

    iput-object v0, p0, LX/GpZ;->e:LX/Go4;

    .line 2395152
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 14

    .prologue
    .line 2395153
    check-cast p1, LX/Gp0;

    .line 2395154
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395155
    check-cast v0, LX/GqA;

    .line 2395156
    iget-object v1, p1, LX/Gp0;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2395157
    iget-object v2, v0, LX/GqA;->e:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2395158
    iget-object v1, p1, LX/Gp0;->d:Ljava/util/List;

    move-object v1, v1

    .line 2395159
    const/4 v13, 0x3

    const/4 v12, 0x0

    const/4 v11, 0x4

    const/4 v10, 0x2

    .line 2395160
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x5

    if-eq v2, v3, :cond_0

    .line 2395161
    :goto_0
    iget-object v1, p1, LX/Gp0;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2395162
    iget-object v2, p1, LX/Gp0;->c:LX/CHX;

    move-object v2, v2

    .line 2395163
    invoke-interface {p1}, LX/GoY;->D()LX/GoE;

    move-result-object v3

    .line 2395164
    invoke-interface {v2}, LX/CHW;->e()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-interface {v2}, LX/CHW;->e()Ljava/lang/String;

    move-result-object v4

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    if-eqz v1, :cond_4

    const-string v4, ""

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    const/4 v4, 0x1

    :goto_1
    move v4, v4

    .line 2395165
    if-eqz v4, :cond_3

    .line 2395166
    iget-object v4, v0, LX/GqA;->f:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2395167
    iget-object v4, v0, LX/GqA;->f:Landroid/widget/TextView;

    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2395168
    iget-object v4, v0, LX/GqA;->f:Landroid/widget/TextView;

    new-instance v5, LX/Gq9;

    invoke-direct {v5, v0, v2, v3}, LX/Gq9;-><init>(LX/GqA;LX/CHX;LX/GoE;)V

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2395169
    :goto_2
    invoke-interface {p1}, LX/GoY;->D()LX/GoE;

    move-result-object v0

    iput-object v0, p0, LX/GpZ;->f:LX/GoE;

    .line 2395170
    return-void

    .line 2395171
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2395172
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2395173
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CHi;

    .line 2395174
    invoke-interface {v2}, LX/CHg;->o()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingPhotoElementFragmentModel$ElementPhotoModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 2395175
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v4, v2

    .line 2395176
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/CHi;

    .line 2395177
    new-instance v6, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2395178
    invoke-interface {v2}, LX/CHg;->p()LX/1Fb;

    move-result-object v7

    invoke-interface {v7}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setImageURI(Landroid/net/Uri;)V

    .line 2395179
    new-instance v7, LX/1Uo;

    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-direct {v7, v8}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    sget-object v8, LX/1Up;->h:LX/1Up;

    invoke-virtual {v7, v8}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v7

    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0136

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/1Uo;->g(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v7

    invoke-virtual {v7}, LX/1Uo;->u()LX/1af;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2395180
    new-instance v7, LX/Gq8;

    invoke-direct {v7, v0, v4, v2}, LX/Gq8;-><init>(LX/GqA;LX/0Px;LX/CHi;)V

    invoke-virtual {v6, v7}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2395181
    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2395182
    :cond_2
    iget-object v4, v0, LX/GqA;->d:Lcom/facebook/widget/mosaic/MosaicGridLayout;

    invoke-interface {v3, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    new-instance v5, LX/G6g;

    invoke-direct {v5, v12, v12, v13, v11}, LX/G6g;-><init>(IIII)V

    invoke-virtual {v4, v2, v5}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2395183
    iget-object v4, v0, LX/GqA;->d:Lcom/facebook/widget/mosaic/MosaicGridLayout;

    const/4 v2, 0x1

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    new-instance v5, LX/G6g;

    invoke-direct {v5, v13, v12, v13, v11}, LX/G6g;-><init>(IIII)V

    invoke-virtual {v4, v2, v5}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2395184
    iget-object v4, v0, LX/GqA;->d:Lcom/facebook/widget/mosaic/MosaicGridLayout;

    invoke-interface {v3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    new-instance v5, LX/G6g;

    invoke-direct {v5, v12, v11, v10, v10}, LX/G6g;-><init>(IIII)V

    invoke-virtual {v4, v2, v5}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2395185
    iget-object v4, v0, LX/GqA;->d:Lcom/facebook/widget/mosaic/MosaicGridLayout;

    invoke-interface {v3, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    new-instance v5, LX/G6g;

    invoke-direct {v5, v10, v11, v10, v10}, LX/G6g;-><init>(IIII)V

    invoke-virtual {v4, v2, v5}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2395186
    iget-object v4, v0, LX/GqA;->d:Lcom/facebook/widget/mosaic/MosaicGridLayout;

    invoke-interface {v3, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    new-instance v3, LX/G6g;

    invoke-direct {v3, v11, v11, v10, v10}, LX/G6g;-><init>(IIII)V

    invoke-virtual {v4, v2, v3}, Lcom/facebook/widget/mosaic/MosaicGridLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    .line 2395187
    :cond_3
    iget-object v4, v0, LX/GqA;->f:Landroid/widget/TextView;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    :cond_4
    const/4 v4, 0x0

    goto/16 :goto_1
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2395188
    invoke-super {p0, p1}, LX/CnT;->a(Landroid/os/Bundle;)V

    .line 2395189
    iget-object v0, p0, LX/GpZ;->d:LX/Go7;

    const-string v1, "cultural_moment_popular_media_start"

    iget-object v2, p0, LX/GpZ;->f:LX/GoE;

    invoke-virtual {v2}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2395190
    iget-object v0, p0, LX/GpZ;->e:LX/Go4;

    iget-object v1, p0, LX/GpZ;->f:LX/GoE;

    invoke-virtual {v1}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Go4;->a(Ljava/lang/String;)V

    .line 2395191
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2395192
    invoke-super {p0, p1}, LX/CnT;->b(Landroid/os/Bundle;)V

    .line 2395193
    iget-object v0, p0, LX/GpZ;->d:LX/Go7;

    const-string v1, "cultural_moment_popular_media_end"

    iget-object v2, p0, LX/GpZ;->f:LX/GoE;

    invoke-virtual {v2}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2395194
    iget-object v0, p0, LX/GpZ;->e:LX/Go4;

    iget-object v1, p0, LX/GpZ;->f:LX/GoE;

    invoke-virtual {v1}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Go4;->b(Ljava/lang/String;)V

    .line 2395195
    return-void
.end method
