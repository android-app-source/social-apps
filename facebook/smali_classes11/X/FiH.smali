.class public LX/FiH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FiI;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fi9;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FiA;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FiE;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FiK;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FiC;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FiG;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FiF;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/8ht;

.field public final j:LX/0ad;

.field public k:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Uh;LX/8ht;LX/0ad;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/FiI;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fi9;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FiA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FiE;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FiK;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FiC;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FiG;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FiF;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/8ht;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2274890
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2274891
    iput-object p1, p0, LX/FiH;->a:LX/0Ot;

    .line 2274892
    iput-object p2, p0, LX/FiH;->b:LX/0Ot;

    .line 2274893
    iput-object p4, p0, LX/FiH;->d:LX/0Ot;

    .line 2274894
    iput-object p5, p0, LX/FiH;->e:LX/0Ot;

    .line 2274895
    iput-object p6, p0, LX/FiH;->f:LX/0Ot;

    .line 2274896
    iput-object p7, p0, LX/FiH;->g:LX/0Ot;

    .line 2274897
    iput-object p8, p0, LX/FiH;->h:LX/0Ot;

    .line 2274898
    iput-object p10, p0, LX/FiH;->i:LX/8ht;

    .line 2274899
    iput-object p3, p0, LX/FiH;->c:LX/0Ot;

    .line 2274900
    iput-object p9, p0, LX/FiH;->k:LX/0Uh;

    .line 2274901
    iput-object p11, p0, LX/FiH;->j:LX/0ad;

    .line 2274902
    return-void
.end method
