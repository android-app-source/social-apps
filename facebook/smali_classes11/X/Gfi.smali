.class public final LX/Gfi;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Gfj;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/Gfj;


# direct methods
.method public constructor <init>(LX/Gfj;)V
    .locals 1

    .prologue
    .line 2377768
    iput-object p1, p0, LX/Gfi;->c:LX/Gfj;

    .line 2377769
    move-object v0, p1

    .line 2377770
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2377771
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2377772
    const-string v0, "PaginatedPagesYouMayLikeHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2377773
    if-ne p0, p1, :cond_1

    .line 2377774
    :cond_0
    :goto_0
    return v0

    .line 2377775
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2377776
    goto :goto_0

    .line 2377777
    :cond_3
    check-cast p1, LX/Gfi;

    .line 2377778
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2377779
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2377780
    if-eq v2, v3, :cond_0

    .line 2377781
    iget-object v2, p0, LX/Gfi;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Gfi;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/Gfi;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2377782
    goto :goto_0

    .line 2377783
    :cond_5
    iget-object v2, p1, LX/Gfi;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2377784
    :cond_6
    iget-object v2, p0, LX/Gfi;->b:LX/1Pn;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/Gfi;->b:LX/1Pn;

    iget-object v3, p1, LX/Gfi;->b:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2377785
    goto :goto_0

    .line 2377786
    :cond_7
    iget-object v2, p1, LX/Gfi;->b:LX/1Pn;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
