.class public final LX/H2q;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 22

    .prologue
    .line 2419106
    const/16 v16, 0x0

    .line 2419107
    const-wide/16 v14, 0x0

    .line 2419108
    const-wide/16 v12, 0x0

    .line 2419109
    const-wide/16 v10, 0x0

    .line 2419110
    const/4 v9, 0x0

    .line 2419111
    const/4 v8, 0x0

    .line 2419112
    const/4 v7, 0x0

    .line 2419113
    const/4 v6, 0x0

    .line 2419114
    const/4 v5, 0x0

    .line 2419115
    const/4 v4, 0x0

    .line 2419116
    const/4 v3, 0x0

    .line 2419117
    const/4 v2, 0x0

    .line 2419118
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v17

    sget-object v18, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    if-eq v0, v1, :cond_e

    .line 2419119
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2419120
    const/4 v2, 0x0

    .line 2419121
    :goto_0
    return v2

    .line 2419122
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_9

    .line 2419123
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2419124
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2419125
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 2419126
    const-string v6, "bounds"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2419127
    invoke-static/range {p0 .. p1}, LX/H2X;->a(LX/15w;LX/186;)I

    move-result v2

    move v15, v2

    goto :goto_1

    .line 2419128
    :cond_1
    const-string v6, "creation_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2419129
    const/4 v2, 0x1

    .line 2419130
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 2419131
    :cond_2
    const-string v6, "max_zoom"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2419132
    const/4 v2, 0x1

    .line 2419133
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v10, v2

    move-wide/from16 v18, v6

    goto :goto_1

    .line 2419134
    :cond_3
    const-string v6, "min_zoom"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2419135
    const/4 v2, 0x1

    .line 2419136
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v6

    move v9, v2

    move-wide/from16 v16, v6

    goto :goto_1

    .line 2419137
    :cond_4
    const-string v6, "placesRenderPriority1"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2419138
    invoke-static/range {p0 .. p1}, LX/H2g;->a(LX/15w;LX/186;)I

    move-result v2

    move v14, v2

    goto :goto_1

    .line 2419139
    :cond_5
    const-string v6, "placesRenderPriority2"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2419140
    invoke-static/range {p0 .. p1}, LX/H2p;->a(LX/15w;LX/186;)I

    move-result v2

    move v13, v2

    goto :goto_1

    .line 2419141
    :cond_6
    const-string v6, "tile_key"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2419142
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move v12, v2

    goto/16 :goto_1

    .line 2419143
    :cond_7
    const-string v6, "ttl"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2419144
    const/4 v2, 0x1

    .line 2419145
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v6

    move v8, v2

    move v11, v6

    goto/16 :goto_1

    .line 2419146
    :cond_8
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2419147
    :cond_9
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2419148
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 2419149
    if-eqz v3, :cond_a

    .line 2419150
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2419151
    :cond_a
    if-eqz v10, :cond_b

    .line 2419152
    const/4 v3, 0x2

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v18

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 2419153
    :cond_b
    if-eqz v9, :cond_c

    .line 2419154
    const/4 v3, 0x3

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 2419155
    :cond_c
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 2419156
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2419157
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2419158
    if-eqz v8, :cond_d

    .line 2419159
    const/4 v2, 0x7

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11, v3}, LX/186;->a(III)V

    .line 2419160
    :cond_d
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_e
    move-wide/from16 v18, v12

    move v12, v7

    move v13, v8

    move v8, v2

    move/from16 v20, v3

    move v3, v5

    move/from16 v21, v4

    move-wide v4, v14

    move v14, v9

    move/from16 v15, v16

    move-wide/from16 v16, v10

    move/from16 v9, v20

    move/from16 v10, v21

    move v11, v6

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    .line 2419161
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2419162
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2419163
    if-eqz v0, :cond_0

    .line 2419164
    const-string v1, "bounds"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2419165
    invoke-static {p0, v0, p2}, LX/H2X;->a(LX/15i;ILX/0nX;)V

    .line 2419166
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v6, v7}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2419167
    cmp-long v2, v0, v6

    if-eqz v2, :cond_1

    .line 2419168
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2419169
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2419170
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 2419171
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_2

    .line 2419172
    const-string v2, "max_zoom"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2419173
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 2419174
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 2419175
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_3

    .line 2419176
    const-string v2, "min_zoom"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2419177
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 2419178
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2419179
    if-eqz v0, :cond_4

    .line 2419180
    const-string v1, "placesRenderPriority1"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2419181
    invoke-static {p0, v0, p2, p3}, LX/H2g;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2419182
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2419183
    if-eqz v0, :cond_5

    .line 2419184
    const-string v1, "placesRenderPriority2"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2419185
    invoke-static {p0, v0, p2, p3}, LX/H2p;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2419186
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2419187
    if-eqz v0, :cond_6

    .line 2419188
    const-string v1, "tile_key"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2419189
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2419190
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2419191
    if-eqz v0, :cond_7

    .line 2419192
    const-string v1, "ttl"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2419193
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2419194
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2419195
    return-void
.end method
