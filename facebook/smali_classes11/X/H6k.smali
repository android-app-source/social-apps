.class public final LX/H6k;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 2427270
    const-class v1, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel;

    const v0, -0x60ed568a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "OfferClaimMarkAsUsedMutation"

    const-string v6, "9d8e93fd3307569c7b75bfffae65e89a"

    const-string v7, "offer_claim_mark_as_used"

    const-string v8, "0"

    const-string v9, "10155252718106729"

    const/4 v10, 0x0

    .line 2427271
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 2427272
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2427273
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2427274
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2427275
    sparse-switch v0, :sswitch_data_0

    .line 2427276
    :goto_0
    return-object p1

    .line 2427277
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2427278
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2427279
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2427280
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2427281
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3ede4845 -> :sswitch_1
        -0x3924a673 -> :sswitch_2
        0x5fb57ca -> :sswitch_0
        0x45476ab5 -> :sswitch_3
        0x63a62026 -> :sswitch_4
    .end sparse-switch
.end method
