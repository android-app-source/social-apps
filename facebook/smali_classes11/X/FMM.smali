.class public final enum LX/FMM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FMM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FMM;

.field public static final enum APN_FAILURE:LX/FMM;

.field public static final enum CONFIG_ERROR:LX/FMM;

.field public static final enum CONNECTION_ERROR:LX/FMM;

.field public static final enum EXPIRED_MESSAGE:LX/FMM;

.field public static final enum FILE_PROVIDER:LX/FMM;

.field public static final enum GENERIC:LX/FMM;

.field public static final enum IO_ERROR:LX/FMM;

.field public static final enum LIMIT_EXCEEDED:LX/FMM;

.field public static final enum NO_CONNECTION:LX/FMM;

.field public static final enum NO_ERROR:LX/FMM;

.field public static final enum OVERSIZE:LX/FMM;

.field public static final enum PROCESSING_ERROR:LX/FMM;

.field public static final enum SERVER_ERROR:LX/FMM;

.field public static final enum STICKER_FAIL:LX/FMM;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2229624
    new-instance v0, LX/FMM;

    const-string v1, "NO_ERROR"

    invoke-direct {v0, v1, v3}, LX/FMM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMM;->NO_ERROR:LX/FMM;

    .line 2229625
    new-instance v0, LX/FMM;

    const-string v1, "GENERIC"

    invoke-direct {v0, v1, v4}, LX/FMM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMM;->GENERIC:LX/FMM;

    .line 2229626
    new-instance v0, LX/FMM;

    const-string v1, "NO_CONNECTION"

    invoke-direct {v0, v1, v5}, LX/FMM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMM;->NO_CONNECTION:LX/FMM;

    .line 2229627
    new-instance v0, LX/FMM;

    const-string v1, "CONNECTION_ERROR"

    invoke-direct {v0, v1, v6}, LX/FMM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMM;->CONNECTION_ERROR:LX/FMM;

    .line 2229628
    new-instance v0, LX/FMM;

    const-string v1, "OVERSIZE"

    invoke-direct {v0, v1, v7}, LX/FMM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMM;->OVERSIZE:LX/FMM;

    .line 2229629
    new-instance v0, LX/FMM;

    const-string v1, "SERVER_ERROR"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/FMM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMM;->SERVER_ERROR:LX/FMM;

    .line 2229630
    new-instance v0, LX/FMM;

    const-string v1, "CONFIG_ERROR"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/FMM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMM;->CONFIG_ERROR:LX/FMM;

    .line 2229631
    new-instance v0, LX/FMM;

    const-string v1, "APN_FAILURE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/FMM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMM;->APN_FAILURE:LX/FMM;

    .line 2229632
    new-instance v0, LX/FMM;

    const-string v1, "IO_ERROR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/FMM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMM;->IO_ERROR:LX/FMM;

    .line 2229633
    new-instance v0, LX/FMM;

    const-string v1, "PROCESSING_ERROR"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/FMM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMM;->PROCESSING_ERROR:LX/FMM;

    .line 2229634
    new-instance v0, LX/FMM;

    const-string v1, "EXPIRED_MESSAGE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/FMM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMM;->EXPIRED_MESSAGE:LX/FMM;

    .line 2229635
    new-instance v0, LX/FMM;

    const-string v1, "STICKER_FAIL"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/FMM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMM;->STICKER_FAIL:LX/FMM;

    .line 2229636
    new-instance v0, LX/FMM;

    const-string v1, "FILE_PROVIDER"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/FMM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMM;->FILE_PROVIDER:LX/FMM;

    .line 2229637
    new-instance v0, LX/FMM;

    const-string v1, "LIMIT_EXCEEDED"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/FMM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FMM;->LIMIT_EXCEEDED:LX/FMM;

    .line 2229638
    const/16 v0, 0xe

    new-array v0, v0, [LX/FMM;

    sget-object v1, LX/FMM;->NO_ERROR:LX/FMM;

    aput-object v1, v0, v3

    sget-object v1, LX/FMM;->GENERIC:LX/FMM;

    aput-object v1, v0, v4

    sget-object v1, LX/FMM;->NO_CONNECTION:LX/FMM;

    aput-object v1, v0, v5

    sget-object v1, LX/FMM;->CONNECTION_ERROR:LX/FMM;

    aput-object v1, v0, v6

    sget-object v1, LX/FMM;->OVERSIZE:LX/FMM;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/FMM;->SERVER_ERROR:LX/FMM;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/FMM;->CONFIG_ERROR:LX/FMM;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/FMM;->APN_FAILURE:LX/FMM;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/FMM;->IO_ERROR:LX/FMM;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/FMM;->PROCESSING_ERROR:LX/FMM;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/FMM;->EXPIRED_MESSAGE:LX/FMM;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/FMM;->STICKER_FAIL:LX/FMM;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/FMM;->FILE_PROVIDER:LX/FMM;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/FMM;->LIMIT_EXCEEDED:LX/FMM;

    aput-object v2, v0, v1

    sput-object v0, LX/FMM;->$VALUES:[LX/FMM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2229639
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromOrdinal(I)LX/FMM;
    .locals 1

    .prologue
    .line 2229640
    invoke-static {}, LX/FMM;->values()[LX/FMM;

    move-result-object v0

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/FMM;
    .locals 1

    .prologue
    .line 2229641
    const-class v0, LX/FMM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FMM;

    return-object v0
.end method

.method public static values()[LX/FMM;
    .locals 1

    .prologue
    .line 2229642
    sget-object v0, LX/FMM;->$VALUES:[LX/FMM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FMM;

    return-object v0
.end method
