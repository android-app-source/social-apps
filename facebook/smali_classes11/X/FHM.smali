.class public final LX/FHM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4ck;


# instance fields
.field public final synthetic a:LX/FHO;

.field private final b:Lcom/facebook/ui/media/attachments/MediaResource;

.field private c:D

.field private d:J

.field private e:LX/4ck;


# direct methods
.method public constructor <init>(LX/FHO;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 2

    .prologue
    .line 2220215
    iput-object p1, p0, LX/FHM;->a:LX/FHO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2220216
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/FHM;->c:D

    .line 2220217
    const-wide/16 v0, 0x0

    iput-wide v0, p0, LX/FHM;->d:J

    .line 2220218
    iput-object p2, p0, LX/FHM;->b:Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2220219
    new-instance v0, LX/CMu;

    iget-object v1, p1, LX/FHO;->i:LX/2Mo;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, v1}, LX/CMu;-><init>(LX/2Mo;)V

    iput-object v0, p0, LX/FHM;->e:LX/4ck;

    .line 2220220
    return-void
.end method


# virtual methods
.method public final a(JJ)V
    .locals 7

    .prologue
    .line 2220221
    iget-object v0, p0, LX/FHM;->e:LX/4ck;

    invoke-interface {v0, p1, p2, p3, p4}, LX/4ck;->a(JJ)V

    .line 2220222
    iget-object v0, p0, LX/FHM;->a:LX/FHO;

    iget-object v0, v0, LX/FHO;->d:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    .line 2220223
    iget-wide v2, p0, LX/FHM;->d:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0xf

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    cmp-long v2, p1, p3

    if-nez v2, :cond_1

    .line 2220224
    :cond_0
    long-to-double v2, p1

    long-to-double v4, p3

    div-double/2addr v2, v4

    .line 2220225
    iget-wide v4, p0, LX/FHM;->c:D

    cmpl-double v4, v2, v4

    if-nez v4, :cond_2

    .line 2220226
    :cond_1
    :goto_0
    return-void

    .line 2220227
    :cond_2
    iput-wide v2, p0, LX/FHM;->c:D

    .line 2220228
    iput-wide v0, p0, LX/FHM;->d:J

    .line 2220229
    iget-object v0, p0, LX/FHM;->a:LX/FHO;

    iget-object v0, v0, LX/FHO;->e:LX/0Xl;

    iget-object v1, p0, LX/FHM;->b:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-static {v1, v2, v3}, LX/FGn;->b(Lcom/facebook/ui/media/attachments/MediaResource;D)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Xl;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method
