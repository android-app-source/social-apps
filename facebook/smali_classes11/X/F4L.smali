.class public LX/F4L;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/res/Resources;

.field public b:Lcom/facebook/content/SecureContextHelper;

.field public c:LX/17Y;

.field private d:LX/0W9;

.field private e:LX/0So;

.field public f:LX/01T;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/0W9;LX/0So;LX/01T;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2196863
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2196864
    iput-object p1, p0, LX/F4L;->a:Landroid/content/res/Resources;

    .line 2196865
    iput-object p2, p0, LX/F4L;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2196866
    iput-object p3, p0, LX/F4L;->c:LX/17Y;

    .line 2196867
    iput-object p4, p0, LX/F4L;->d:LX/0W9;

    .line 2196868
    iput-object p5, p0, LX/F4L;->e:LX/0So;

    .line 2196869
    iput-object p6, p0, LX/F4L;->f:LX/01T;

    .line 2196870
    return-void
.end method

.method public static b(LX/0QB;)LX/F4L;
    .locals 7

    .prologue
    .line 2196831
    new-instance v0, LX/F4L;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v3

    check-cast v3, LX/17Y;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v4

    check-cast v4, LX/0W9;

    invoke-static {p0}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v5

    check-cast v5, LX/0So;

    invoke-static {p0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v6

    check-cast v6, LX/01T;

    invoke-direct/range {v0 .. v6}, LX/F4L;-><init>(Landroid/content/res/Resources;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/0W9;LX/0So;LX/01T;)V

    .line 2196832
    return-object v0
.end method

.method public static b(Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;)Z
    .locals 2

    .prologue
    .line 2196833
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->t()Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->t()Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel$ParentGroupModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->COMPANY:Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    if-ne v0, v1, :cond_0

    .line 2196834
    const/4 v0, 0x1

    .line 2196835
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/text/SpannableString;
    .locals 8

    .prologue
    .line 2196836
    new-instance v0, LX/F4K;

    invoke-direct {v0, p0}, LX/F4K;-><init>(LX/F4L;)V

    .line 2196837
    const/16 v7, 0x21

    .line 2196838
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2196839
    iget-object v2, p0, LX/F4L;->a:Landroid/content/res/Resources;

    const v3, 0x7f0831ce

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2196840
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2196841
    invoke-static {v1}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v1

    .line 2196842
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2196843
    invoke-static {v2}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v0, v1, v4, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2196844
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    iget-object v5, p0, LX/F4L;->a:Landroid/content/res/Resources;

    const v6, 0x7f0a05ee

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-direct {v4, v5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 2196845
    invoke-static {v2}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v3, v4, v1, v2, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2196846
    move-object v0, v3

    .line 2196847
    return-object v0
.end method

.method public final a(Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;Lcom/facebook/graphql/enums/GraphQLGroupVisibility;)Ljava/lang/String;
    .locals 7

    .prologue
    const v4, 0x37a88ba0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2196848
    if-eqz p1, :cond_2

    .line 2196849
    invoke-virtual {p1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->w()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2196850
    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_5

    .line 2196851
    invoke-virtual {p1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->w()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2196852
    if-eqz v0, :cond_3

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    if-eqz v0, :cond_7

    .line 2196853
    invoke-virtual {p1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->w()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2196854
    if-eqz v2, :cond_0

    invoke-virtual {v3, v2, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    const/4 v4, 0x2

    const-class v5, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v3, v2, v4, v5, v6}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v4

    if-ne v4, p2, :cond_0

    .line 2196855
    invoke-virtual {v3, v2, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/F4L;->d:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 2196856
    :goto_4
    return-object v0

    :cond_1
    move v0, v2

    .line 2196857
    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_2

    .line 2196858
    :cond_6
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_3

    .line 2196859
    :cond_7
    const/4 v0, 0x0

    goto :goto_4
.end method

.method public final a(Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;)Z
    .locals 4

    .prologue
    .line 2196860
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->n()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    iget-object v2, p0, LX/F4L;->e:LX/0So;

    invoke-interface {v2}, LX/0So;->now()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 2196861
    const/4 v0, 0x1

    .line 2196862
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
