.class public abstract LX/Ffz;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/os/Handler;

.field public final b:I

.field public final c:LX/0SG;

.field public d:J

.field public e:J

.field public f:J

.field public g:Z

.field public final h:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LX/0SG;Landroid/os/Handler;IZ)V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 2269192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2269193
    iput-wide v0, p0, LX/Ffz;->d:J

    .line 2269194
    iput-wide v0, p0, LX/Ffz;->e:J

    .line 2269195
    iput-wide v0, p0, LX/Ffz;->f:J

    .line 2269196
    new-instance v0, Lcom/facebook/search/results/livefeed/loader/RefreshController$1;

    invoke-direct {v0, p0}, Lcom/facebook/search/results/livefeed/loader/RefreshController$1;-><init>(LX/Ffz;)V

    iput-object v0, p0, LX/Ffz;->h:Ljava/lang/Runnable;

    .line 2269197
    iput-object p1, p0, LX/Ffz;->c:LX/0SG;

    .line 2269198
    iput-object p2, p0, LX/Ffz;->a:Landroid/os/Handler;

    .line 2269199
    iput p3, p0, LX/Ffz;->b:I

    .line 2269200
    iput-boolean p4, p0, LX/Ffz;->g:Z

    .line 2269201
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 2269182
    iget-object v0, p0, LX/Ffz;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2269183
    iget-wide v0, p0, LX/Ffz;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 2269184
    iget-object v0, p0, LX/Ffz;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-wide v2, p0, LX/Ffz;->e:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, LX/Ffz;->f:J

    .line 2269185
    :cond_0
    iget-boolean v0, p0, LX/Ffz;->g:Z

    if-eqz v0, :cond_1

    .line 2269186
    iget-object v0, p0, LX/Ffz;->a:Landroid/os/Handler;

    iget-object v1, p0, LX/Ffz;->h:Ljava/lang/Runnable;

    const v2, -0x165dd30e

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2269187
    :goto_0
    return-void

    .line 2269188
    :cond_1
    iget-object v0, p0, LX/Ffz;->a:Landroid/os/Handler;

    iget-object v1, p0, LX/Ffz;->h:Ljava/lang/Runnable;

    iget v2, p0, LX/Ffz;->b:I

    int-to-long v2, v2

    const v4, -0x6a635b82

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2269189
    iget-object v0, p0, LX/Ffz;->c:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iput-wide v0, p0, LX/Ffz;->e:J

    .line 2269190
    iget-object v0, p0, LX/Ffz;->a:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2269191
    return-void
.end method
