.class public LX/FEZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/content/SecureContextHelper;

.field public final b:Landroid/content/Context;

.field private final c:LX/FEi;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;LX/FEi;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/content/SecureContextHelper;",
            "Landroid/content/Context;",
            "LX/FEi;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2216518
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2216519
    iput-object p1, p0, LX/FEZ;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2216520
    iput-object p2, p0, LX/FEZ;->b:Landroid/content/Context;

    .line 2216521
    iput-object p3, p0, LX/FEZ;->c:LX/FEi;

    .line 2216522
    iput-object p4, p0, LX/FEZ;->d:LX/0Ot;

    .line 2216523
    return-void
.end method

.method public static b(LX/0QB;)LX/FEZ;
    .locals 5

    .prologue
    .line 2216524
    new-instance v3, LX/FEZ;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/FEi;->a(LX/0QB;)LX/FEi;

    move-result-object v2

    check-cast v2, LX/FEi;

    const/16 v4, 0x97

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-direct {v3, v0, v1, v2, v4}, LX/FEZ;-><init>(Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;LX/FEi;LX/0Ot;)V

    .line 2216525
    return-object v3
.end method
