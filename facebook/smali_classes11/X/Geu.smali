.class public LX/Geu;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Ges;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeBodyHeaderComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2376228
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Geu;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeBodyHeaderComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2376229
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2376230
    iput-object p1, p0, LX/Geu;->b:LX/0Ot;

    .line 2376231
    return-void
.end method

.method public static a(LX/0QB;)LX/Geu;
    .locals 4

    .prologue
    .line 2376217
    const-class v1, LX/Geu;

    monitor-enter v1

    .line 2376218
    :try_start_0
    sget-object v0, LX/Geu;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2376219
    sput-object v2, LX/Geu;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2376220
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2376221
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2376222
    new-instance v3, LX/Geu;

    const/16 p0, 0x2106

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Geu;-><init>(LX/0Ot;)V

    .line 2376223
    move-object v0, v3

    .line 2376224
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2376225
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Geu;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2376226
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2376227
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 8

    .prologue
    .line 2376211
    check-cast p2, LX/Get;

    .line 2376212
    iget-object v0, p0, LX/Geu;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeBodyHeaderComponentSpec;

    iget-object v1, p2, LX/Get;->a:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    iget-object v2, p2, LX/Get;->b:LX/25E;

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 2376213
    iget-object v5, v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeBodyHeaderComponentSpec;->b:LX/GeH;

    invoke-virtual {v5, v1, v2}, LX/GeH;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;LX/25E;)Landroid/text/Spannable;

    move-result-object v5

    .line 2376214
    invoke-static {p1, v2}, LX/Gdi;->b(Landroid/content/Context;LX/25E;)Ljava/lang/String;

    move-result-object v6

    .line 2376215
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v7

    iget-object p0, v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeBodyHeaderComponentSpec;->c:LX/CE0;

    invoke-virtual {p0, p1}, LX/CE0;->c(LX/1De;)LX/CDy;

    move-result-object p0

    invoke-virtual {p0, v5}, LX/CDy;->a(Ljava/lang/CharSequence;)LX/CDy;

    move-result-object v5

    iget-object p0, v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeBodyHeaderComponentSpec;->d:LX/CE9;

    invoke-virtual {p0, p1}, LX/CE9;->c(LX/1De;)LX/CE8;

    move-result-object p0

    sget-object p2, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeBodyHeaderComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {p0, p2}, LX/CE8;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/CE8;

    move-result-object p0

    const p2, 0x7f0b08f9

    invoke-virtual {p0, p2}, LX/CE8;->h(I)LX/CE8;

    move-result-object p0

    invoke-static {v2}, LX/Gdi;->c(LX/25E;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object p2

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, LX/CE8;->b(Ljava/lang/String;)LX/CE8;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->d()LX/1X1;

    move-result-object p0

    invoke-virtual {v5, p0}, LX/CDy;->a(LX/1X1;)LX/CDy;

    move-result-object v5

    invoke-virtual {v5, v4}, LX/CDy;->h(I)LX/CDy;

    move-result-object v5

    invoke-virtual {v5, v6}, LX/CDy;->b(Ljava/lang/CharSequence;)LX/CDy;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/CDy;->a(Z)LX/CDy;

    move-result-object v5

    invoke-interface {v2}, LX/25E;->x()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v6

    if-eqz v6, :cond_0

    move v3, v4

    :cond_0
    invoke-virtual {v5, v3}, LX/CDy;->b(Z)LX/CDy;

    move-result-object v3

    invoke-interface {v7, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    const/16 v5, 0x8

    invoke-interface {v3, v4, v5}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x3

    const/4 v5, 0x2

    invoke-interface {v3, v4, v5}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2376216
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2376201
    invoke-static {}, LX/1dS;->b()V

    .line 2376202
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(LX/1De;)LX/Ges;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2376203
    new-instance v1, LX/Get;

    invoke-direct {v1, p0}, LX/Get;-><init>(LX/Geu;)V

    .line 2376204
    sget-object v2, LX/Geu;->a:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Ges;

    .line 2376205
    if-nez v2, :cond_0

    .line 2376206
    new-instance v2, LX/Ges;

    invoke-direct {v2}, LX/Ges;-><init>()V

    .line 2376207
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/Ges;->a$redex0(LX/Ges;LX/1De;IILX/Get;)V

    .line 2376208
    move-object v1, v2

    .line 2376209
    move-object v0, v1

    .line 2376210
    return-object v0
.end method
