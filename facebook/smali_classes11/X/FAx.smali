.class public final LX/FAx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field public final synthetic a:Landroid/widget/EditText;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/FAz;

.field public final synthetic e:LX/FAy;


# direct methods
.method public constructor <init>(LX/FAy;Landroid/widget/EditText;Landroid/content/Context;Ljava/lang/String;LX/FAz;)V
    .locals 0

    .prologue
    .line 2208560
    iput-object p1, p0, LX/FAx;->e:LX/FAy;

    iput-object p2, p0, LX/FAx;->a:Landroid/widget/EditText;

    iput-object p3, p0, LX/FAx;->b:Landroid/content/Context;

    iput-object p4, p0, LX/FAx;->c:Ljava/lang/String;

    iput-object p5, p0, LX/FAx;->d:LX/FAz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2208561
    const/4 v1, 0x6

    if-ne p2, v1, :cond_0

    .line 2208562
    iget-object v1, p0, LX/FAx;->a:Landroid/widget/EditText;

    if-ne p1, v1, :cond_0

    .line 2208563
    iget-object v2, p0, LX/FAx;->a:Landroid/widget/EditText;

    .line 2208564
    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    .line 2208565
    move-object p1, p1

    .line 2208566
    move-object v1, p1

    .line 2208567
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p1

    if-nez p1, :cond_2

    const/4 p1, 0x1

    :goto_0
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    move-object p1, p1

    .line 2208568
    move-object v2, p1

    .line 2208569
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-nez v2, :cond_1

    .line 2208570
    iget-object v1, p0, LX/FAx;->b:Landroid/content/Context;

    iget-object v2, p0, LX/FAx;->c:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2208571
    iget-object v0, p0, LX/FAx;->a:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 2208572
    const/4 v0, 0x0

    .line 2208573
    :cond_0
    :goto_1
    return v0

    .line 2208574
    :cond_1
    iget-object v2, p0, LX/FAx;->d:LX/FAz;

    invoke-interface {v2, v1}, LX/FAz;->c_(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    goto :goto_0
.end method
