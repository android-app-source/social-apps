.class public final LX/HEm;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/pages/common/models/bookmark_favorites/PageFavoriteBookmarksGraphQLModels$FBBookmarkRemoveFromFavoritesMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 2442720
    const-class v1, Lcom/facebook/pages/common/models/bookmark_favorites/PageFavoriteBookmarksGraphQLModels$FBBookmarkRemoveFromFavoritesMutationModel;

    const v0, 0xab75bb9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FBBookmarkRemoveFromFavoritesMutation"

    const-string v6, "a3e067564b72b8d238dd443771c27212"

    const-string v7, "bookmark_remove_from_favorites"

    const-string v8, "0"

    const-string v9, "10155069964051729"

    const/4 v10, 0x0

    .line 2442721
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 2442722
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2442723
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2442724
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2442725
    packed-switch v0, :pswitch_data_0

    .line 2442726
    :goto_0
    return-object p1

    .line 2442727
    :pswitch_0
    const-string p1, "0"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x5fb57ca
        :pswitch_0
    .end packed-switch
.end method
