.class public final LX/F4v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;)V
    .locals 0

    .prologue
    .line 2197762
    iput-object p1, p0, LX/F4v;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x7d55b833

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2197763
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 2197764
    const v2, 0x7f0d1077

    if-ne v1, v2, :cond_0

    .line 2197765
    iget-object v2, p0, LX/F4v;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->b:LX/F53;

    const-string v3, "CLOSED"

    invoke-virtual {v2, v3}, LX/F53;->e(Ljava/lang/String;)V

    .line 2197766
    :cond_0
    const v2, 0x7f0d1076

    if-ne v1, v2, :cond_1

    .line 2197767
    iget-object v2, p0, LX/F4v;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;

    iget-object v2, v2, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->b:LX/F53;

    const-string v3, "OPEN"

    invoke-virtual {v2, v3}, LX/F53;->e(Ljava/lang/String;)V

    .line 2197768
    :cond_1
    const v2, 0x7f0d1078

    if-ne v1, v2, :cond_2

    .line 2197769
    iget-object v1, p0, LX/F4v;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;

    iget-object v1, v1, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreatePrivacySelectorFragment;->b:LX/F53;

    const-string v2, "SECRET"

    invoke-virtual {v1, v2}, LX/F53;->e(Ljava/lang/String;)V

    .line 2197770
    :cond_2
    const v1, -0x4590a62f

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
