.class public LX/FEk;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Zb;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;Ljava/lang/String;)V
    .locals 0
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2216643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2216644
    iput-object p1, p0, LX/FEk;->a:LX/0Zb;

    .line 2216645
    iput-object p2, p0, LX/FEk;->b:Ljava/lang/String;

    .line 2216646
    return-void
.end method

.method public static a(LX/0QB;)LX/FEk;
    .locals 5

    .prologue
    .line 2216632
    const-class v1, LX/FEk;

    monitor-enter v1

    .line 2216633
    :try_start_0
    sget-object v0, LX/FEk;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2216634
    sput-object v2, LX/FEk;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2216635
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2216636
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2216637
    new-instance p0, LX/FEk;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {p0, v3, v4}, LX/FEk;-><init>(LX/0Zb;Ljava/lang/String;)V

    .line 2216638
    move-object v0, p0

    .line 2216639
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2216640
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FEk;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2216641
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2216642
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
