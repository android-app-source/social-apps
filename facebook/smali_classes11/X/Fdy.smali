.class public final LX/Fdy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

.field public b:Z


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/fragment/SearchResultsFragment;)V
    .locals 1

    .prologue
    .line 2264543
    iput-object p1, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2264544
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fdy;->b:Z

    return-void
.end method


# virtual methods
.method public final a(LX/Cym;)V
    .locals 13

    .prologue
    .line 2264556
    iget-object v0, p1, LX/Cym;->a:Lcom/facebook/graphql/executor/GraphQLResult;

    move-object v7, v0

    .line 2264557
    iget-object v0, v7, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2264558
    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->a()Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;

    move-result-object v4

    .line 2264559
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    .line 2264560
    iget-object v1, v0, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2264561
    iput-object v4, v1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->x:LX/8ef;

    .line 2264562
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->r:LX/CyZ;

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->a()LX/0Px;

    move-result-object v2

    .line 2264563
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v6

    const/4 v3, 0x0

    move v5, v3

    :goto_0
    if-ge v5, v6, :cond_2

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    .line 2264564
    invoke-static {v3}, LX/CyZ;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Z

    move-result v3

    if-nez v3, :cond_13

    .line 2264565
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2264566
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v8

    const/4 v3, 0x0

    move v5, v3

    :goto_1
    if-ge v5, v8, :cond_1

    invoke-virtual {v2, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    .line 2264567
    invoke-static {v3}, LX/CyZ;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 2264568
    iget-object v9, v0, LX/CyZ;->a:LX/2Sc;

    sget-object v10, LX/3Ql;->INVALID_SEARCH_RESULT:LX/3Ql;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v10, v3}, LX/2Sc;->a(LX/3Ql;Ljava/lang/String;)V

    .line 2264569
    :goto_2
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_1

    .line 2264570
    :cond_0
    invoke-virtual {v6, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 2264571
    :cond_1
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    move-object v2, v3

    .line 2264572
    :cond_2
    move-object v8, v2

    .line 2264573
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2264574
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    :goto_3
    if-ge v2, v5, :cond_3

    invoke-virtual {v8, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    .line 2264575
    new-instance v6, LX/Cz3;

    invoke-direct {v6, v0}, LX/Cz3;-><init>(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)V

    invoke-virtual {v3, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2264576
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 2264577
    :cond_3
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    move-object v5, v0

    .line 2264578
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2264579
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    iget-object v2, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    .line 2264580
    iget-object v3, v2, Lcom/facebook/search/results/fragment/SearchResultsFragment;->A:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0x9;

    .line 2264581
    sget-object v9, Lcom/facebook/search/results/fragment/SearchResultsFragment;->I:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-virtual {v3, v9}, LX/0x9;->a(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;)Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, LX/7BG;->f(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-virtual {v8}, LX/0Px;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_14

    .line 2264582
    :cond_4
    sget-object v3, LX/0Q7;->a:LX/0Px;

    move-object v3, v3

    .line 2264583
    :goto_4
    move-object v2, v3

    .line 2264584
    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/CzA;->a(LX/0Px;LX/80n;)V

    .line 2264585
    const-string v0, "bootstrap_entities"

    invoke-virtual {p1}, LX/Cym;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 2264586
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    invoke-virtual {v0}, LX/CzA;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2264587
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->G:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cyz;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2264588
    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_16

    .line 2264589
    :cond_5
    :goto_5
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v2, v0

    :goto_6
    if-ge v2, v3, :cond_6

    invoke-virtual {v8, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    .line 2264590
    invoke-static {v0, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    invoke-virtual {v6, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2264591
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 2264592
    :cond_6
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->b()LX/80n;

    move-result-object v2

    invoke-virtual {v0, v5, v2}, LX/CzA;->a(LX/0Px;LX/80n;)V

    .line 2264593
    :cond_7
    :goto_7
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2264594
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 2264595
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->S:LX/0g8;

    invoke-interface {v0}, LX/0g8;->q()I

    move-result v0

    .line 2264596
    iget-object v2, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v2, v2, Lcom/facebook/search/results/fragment/SearchResultsFragment;->N:LX/1Qq;

    invoke-interface {v2}, LX/1Cw;->notifyDataSetChanged()V

    .line 2264597
    if-gtz v0, :cond_8

    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    invoke-virtual {v0}, LX/CzA;->a()Z

    move-result v0

    if-nez v0, :cond_8

    .line 2264598
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->S:LX/0g8;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, LX/0g8;->g(I)V

    .line 2264599
    :cond_8
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->p:LX/FgI;

    iget-object v2, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v2, v2, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    iget-object v4, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-boolean v4, v4, Lcom/facebook/search/results/fragment/SearchResultsFragment;->W:Z

    if-eqz v4, :cond_f

    iget-object v4, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v4, v4, Lcom/facebook/search/results/fragment/SearchResultsFragment;->w:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/FcD;

    invoke-virtual {v4}, LX/FcD;->g()LX/0Px;

    move-result-object v4

    :goto_8
    iget-object v5, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget v5, v5, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ae:I

    .line 2264600
    iget-object v6, v7, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v6, v6

    .line 2264601
    check-cast v6, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;

    invoke-virtual {v6}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, LX/FgI;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/CzA;LX/0Px;LX/0Px;ILjava/lang/String;)V

    .line 2264602
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    invoke-virtual {v0}, LX/CzA;->b()V

    .line 2264603
    :cond_9
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    .line 2264604
    iget-boolean v2, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->W:Z

    if-eqz v2, :cond_a

    .line 2264605
    iget-object v2, v7, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 2264606
    check-cast v2, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-nez v2, :cond_19

    .line 2264607
    :cond_a
    :goto_9
    iget-boolean v0, p0, LX/Fdy;->b:Z

    if-eqz v0, :cond_10

    invoke-virtual {v8}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    :goto_a
    iput-boolean v0, p0, LX/Fdy;->b:Z

    .line 2264608
    invoke-virtual {v8}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_b

    .line 2264609
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    const/4 v2, 0x0

    .line 2264610
    iput-boolean v2, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ac:Z

    .line 2264611
    :cond_b
    invoke-virtual {v8}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_12

    .line 2264612
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->U:LX/Cyn;

    invoke-virtual {p1}, LX/Cym;->b()Ljava/lang/String;

    move-result-object v2

    .line 2264613
    iget-object v3, v0, LX/Cyn;->f:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2264614
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v2, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->C:LX/Cve;

    invoke-virtual {p1}, LX/Cym;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->U:LX/Cyn;

    .line 2264615
    iget-object v4, v0, LX/Cyn;->o:LX/Cyd;

    move-object v0, v4

    .line 2264616
    sget-object v4, LX/Cyd;->LOAD_MORE:LX/Cyd;

    if-eq v0, v4, :cond_11

    const/4 v0, 0x1

    :goto_b
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v3, v0}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v0, v3}, LX/Cve;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/util/Map;Z)Z

    .line 2264617
    :goto_c
    return-void

    .line 2264618
    :cond_c
    new-instance v9, LX/0Pz;

    invoke-direct {v9}, LX/0Pz;-><init>()V

    .line 2264619
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v10

    const/4 v0, 0x0

    move v3, v0

    :goto_d
    if-ge v3, v10, :cond_e

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cyv;

    .line 2264620
    iget-object v2, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v2, v2, Lcom/facebook/search/results/fragment/SearchResultsFragment;->G:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Cyz;

    .line 2264621
    invoke-static {v0}, LX/Cyz;->b(LX/Cyv;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v11

    .line 2264622
    invoke-static {v2, v11}, LX/Cyz;->a(LX/Cyz;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)Z

    move-result v11

    move v2, v11

    .line 2264623
    if-eqz v2, :cond_d

    .line 2264624
    iget-object v2, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v2, v2, Lcom/facebook/search/results/fragment/SearchResultsFragment;->G:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Cyz;

    iget-object v11, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v11, v11, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    invoke-virtual {v2, v0, v11, v9, v6}, LX/Cyz;->a(LX/Cyv;LX/CzA;LX/0Pz;LX/0Pz;)V

    .line 2264625
    :goto_e
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_d

    .line 2264626
    :cond_d
    invoke-interface {v0}, LX/Cyv;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v2

    invoke-interface {v0}, LX/Cyv;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v11

    invoke-static {v2, v11}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-virtual {v6, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2264627
    const/4 v2, 0x1

    new-array v2, v2, [LX/Cyv;

    const/4 v11, 0x0

    aput-object v0, v2, v11

    invoke-virtual {v9, v2}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    goto :goto_e

    .line 2264628
    :cond_e
    invoke-virtual {v9}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2264629
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->G:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cyz;

    invoke-virtual {v0, v2}, LX/Cyz;->c(LX/0Px;)V

    .line 2264630
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel$CombinedResultsModel;->b()LX/80n;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/CzA;->a(LX/0Px;LX/80n;)V

    goto/16 :goto_7

    .line 2264631
    :cond_f
    const/4 v4, 0x0

    goto/16 :goto_8

    .line 2264632
    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_a

    .line 2264633
    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_b

    .line 2264634
    :cond_12
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    sget-object v1, LX/EQG;->LOADING_MORE:LX/EQG;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->a$redex0(Lcom/facebook/search/results/fragment/SearchResultsFragment;LX/EQG;Z)V

    goto/16 :goto_c

    .line 2264635
    :cond_13
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto/16 :goto_0

    .line 2264636
    :cond_14
    sget-object v9, Lcom/facebook/search/results/fragment/SearchResultsFragment;->I:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    const-string v10, "browse_session_id"

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->A()LX/8ef;

    move-result-object v11

    invoke-interface {v11}, LX/8ef;->c()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v10

    invoke-virtual {v3, v9, v10}, LX/0x9;->a(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;LX/0P1;)V

    .line 2264637
    sget-object v9, Lcom/facebook/search/results/fragment/SearchResultsFragment;->I:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    const/4 v10, 0x1

    invoke-virtual {v3, v9, v10}, LX/0x9;->a(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;Z)V

    .line 2264638
    iget-object v9, v3, LX/0x9;->i:Ljava/util/Map;

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->LEARNING_NUX_SERP_SUCCESS:Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    invoke-interface {v9, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, LX/FaZ;

    .line 2264639
    if-eqz v9, :cond_15

    .line 2264640
    new-instance v10, LX/Cyw;

    iget-object v3, v9, LX/FaZ;->a:LX/CwT;

    .line 2264641
    iget-object v9, v3, LX/CwT;->a:LX/A0Z;

    move-object v3, v9

    .line 2264642
    invoke-direct {v10, v3}, LX/Cyw;-><init>(LX/A0Z;)V

    invoke-static {v10}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v10

    move-object v9, v10

    .line 2264643
    :goto_f
    move-object v3, v9

    .line 2264644
    goto/16 :goto_4

    .line 2264645
    :cond_15
    sget-object v9, LX/0Q7;->a:LX/0Px;

    move-object v9, v9

    .line 2264646
    goto :goto_f

    .line 2264647
    :cond_16
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v9

    if-ne v9, v2, :cond_17

    :goto_10
    const-string v9, "BEM results should only have one module"

    invoke-static {v2, v9}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2264648
    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Cyv;

    .line 2264649
    invoke-interface {v2}, LX/Cyv;->d()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v9

    invoke-virtual {v9}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->aw()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;

    move-result-object v9

    .line 2264650
    invoke-virtual {v9}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_5

    .line 2264651
    new-instance v10, LX/0cA;

    invoke-direct {v10}, LX/0cA;-><init>()V

    .line 2264652
    invoke-virtual {v9}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    move v9, v3

    :goto_11
    if-ge v9, v12, :cond_18

    invoke-virtual {v11, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;

    .line 2264653
    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel;->fl_()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;->dW_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v10, v3}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    .line 2264654
    add-int/lit8 v3, v9, 0x1

    move v9, v3

    goto :goto_11

    :cond_17
    move v2, v3

    .line 2264655
    goto :goto_10

    .line 2264656
    :cond_18
    invoke-interface {v2}, LX/Cyv;->c()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v2

    invoke-static {v0, v10, v2}, LX/Cyz;->a(LX/Cyz;LX/0cA;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;)V

    goto/16 :goto_5

    .line 2264657
    :cond_19
    iget-object v2, v7, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v2, v2

    .line 2264658
    check-cast v2, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/SearchResultsGraphQLModels$SearchResultsGraphQLModel;->j()LX/0Px;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;

    invoke-virtual {v2}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;->a()LX/0Px;

    move-result-object v3

    .line 2264659
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 2264660
    iget-object v2, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->x:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Sh;

    new-instance v4, Lcom/facebook/search/results/fragment/SearchResultsFragment$4;

    invoke-direct {v4, v0, v3}, Lcom/facebook/search/results/fragment/SearchResultsFragment$4;-><init>(Lcom/facebook/search/results/fragment/SearchResultsFragment;LX/0Px;)V

    invoke-virtual {v2, v4}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto/16 :goto_9
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2264545
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    const/4 v1, 0x0

    .line 2264546
    iput-boolean v1, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->X:Z

    .line 2264547
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    invoke-static {v0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;->C(Lcom/facebook/search/results/fragment/SearchResultsFragment;)V

    .line 2264548
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->C:LX/Cve;

    invoke-virtual {v0}, LX/Cve;->c()V

    .line 2264549
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->j:LX/2Sc;

    sget-object v1, LX/3Ql;->RESULTS_DATA_LOADER_ERROR:LX/3Ql;

    invoke-virtual {v0, v1, p1}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V

    .line 2264550
    iget-object v0, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;->q:LX/CvY;

    iget-object v1, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    .line 2264551
    iget-object v2, v1, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v2

    .line 2264552
    iget-object v2, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget-object v2, v2, Lcom/facebook/search/results/fragment/SearchResultsFragment;->L:LX/CzA;

    .line 2264553
    iget v3, v2, LX/CzA;->c:I

    move v2, v3

    .line 2264554
    iget-object v3, p0, LX/Fdy;->a:Lcom/facebook/search/results/fragment/SearchResultsFragment;

    iget v3, v3, Lcom/facebook/search/results/fragment/SearchResultsFragment;->ae:I

    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;IILjava/lang/String;)V

    .line 2264555
    return-void
.end method
