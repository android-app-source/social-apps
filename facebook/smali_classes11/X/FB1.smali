.class public LX/FB1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6G0;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/FB1;


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;

.field private final c:LX/0xC;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0xC;)V
    .locals 8
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2208581
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2208582
    iput-object p1, p0, LX/FB1;->b:Landroid/content/Context;

    .line 2208583
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 2208584
    iput-object p2, p0, LX/FB1;->c:LX/0xC;

    .line 2208585
    const/16 v1, 0x1e

    new-array v1, v1, [Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    const/4 v2, 0x0

    const v3, 0x7f081900

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x13311d38701dfL

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->b(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const v3, 0x7f081902

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x10ee5de20282dL

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->b(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const v3, 0x7f081903

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x7e03179efa26L

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const v3, 0x7f081907

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f081908

    invoke-direct {p0, v4}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v4

    const-wide v6, 0x16c25e946e792L

    invoke-static {v3, v4, v6, v7}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(Ljava/lang/String;Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const v3, 0x7f081909

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x5ddbfd8e4f68L

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->b(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const v3, 0x7f08190a

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f08190b

    invoke-direct {p0, v4}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v4

    const-wide v6, 0x104e02b717630L

    invoke-static {v3, v4, v6, v7}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(Ljava/lang/String;Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const v3, 0x7f08190c

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0xcf12d3dadf6fL

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const v3, 0x7f08190d

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x228264aa0679aL    # 2.99944723636107E-309

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->b(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const v3, 0x7f08190e

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f08190f

    invoke-direct {p0, v4}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v4

    const-wide v6, 0x165b63cee4dc5L

    invoke-static {v3, v4, v6, v7}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(Ljava/lang/String;Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const v3, 0x7f081910

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x832f4646743dL

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const v3, 0x7f081911

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x19e8177fe1864L

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->b(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const v3, 0x7f081912

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x1ecffa3cfefedL

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const v3, 0x7f081914

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f081915

    invoke-direct {p0, v4}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v4

    const-wide v6, 0x1645502bcff15L

    invoke-static {v3, v4, v6, v7}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(Ljava/lang/String;Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const v3, 0x7f081916

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x110c733fce97aL

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const v3, 0x7f081917

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x7cbeb7d438eeL

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const v3, 0x7f08192e

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x36bb477212c57L    # 4.757100038891937E-309

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->b(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const v3, 0x7f081918

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x73fa5c4a8838L

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const v3, 0x7f081919

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x2333a2592393bL

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const v3, 0x7f08191b

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x1f4f089d1d620L

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const v3, 0x7f08191d

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x10cce0c2ea175L

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const v3, 0x7f08191e

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x1963f36bb3c2eL

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const v3, 0x7f08192d

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x260f75bc7e143L

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->b(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const v3, 0x7f08191c

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x15dc32146a603L    # 1.90001656953293E-309

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const v3, 0x7f081920

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x1af5b08493673L

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const v3, 0x7f081922

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0xa795854cf461L

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x19

    const v3, 0x7f081924

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x6144b4c518be9L

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->b(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x1a

    const v3, 0x7f081925

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x201a94e08dea3L

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->b(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x1b

    const v3, 0x7f081928

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0xbf148a6a18adL

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->b(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x1c

    const v3, 0x7f081935

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x699eb5716fd17L

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->b(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x1d

    const v3, 0x7f081901

    invoke-direct {p0, v3}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v3

    const-wide v4, 0x5cfe17752557dL

    invoke-static {v3, v4, v5}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->b(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/0Pz;->b([Ljava/lang/Object;)LX/0Pz;

    .line 2208586
    iget-object v1, p0, LX/FB1;->c:LX/0xC;

    invoke-virtual {v1}, LX/0xC;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2208587
    const v1, 0x7f081934

    invoke-direct {p0, v1}, LX/FB1;->a(I)Ljava/lang/String;

    move-result-object v1

    const-wide v2, 0x23307124cfd26L

    invoke-static {v1, v2, v3}, Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;->a(Ljava/lang/String;J)Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2208588
    :cond_0
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/FB1;->a:LX/0Px;

    .line 2208589
    return-void
.end method

.method public static a(LX/0QB;)LX/FB1;
    .locals 5

    .prologue
    .line 2208590
    sget-object v0, LX/FB1;->d:LX/FB1;

    if-nez v0, :cond_1

    .line 2208591
    const-class v1, LX/FB1;

    monitor-enter v1

    .line 2208592
    :try_start_0
    sget-object v0, LX/FB1;->d:LX/FB1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2208593
    if-eqz v2, :cond_0

    .line 2208594
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2208595
    new-instance p0, LX/FB1;

    const-class v3, Landroid/content/Context;

    const-class v4, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v3, v4}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0xC;->a(LX/0QB;)LX/0xC;

    move-result-object v4

    check-cast v4, LX/0xC;

    invoke-direct {p0, v3, v4}, LX/FB1;-><init>(Landroid/content/Context;LX/0xC;)V

    .line 2208596
    move-object v0, p0

    .line 2208597
    sput-object v0, LX/FB1;->d:LX/FB1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2208598
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2208599
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2208600
    :cond_1
    sget-object v0, LX/FB1;->d:LX/FB1;

    return-object v0

    .line 2208601
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2208602
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2208603
    iget-object v0, p0, LX/FB1;->b:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/bugreporter/activity/categorylist/CategoryInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2208604
    iget-object v0, p0, LX/FB1;->a:LX/0Px;

    return-object v0
.end method

.method public final b()LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/bugreporter/activity/chooser/ChooserOption;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2208605
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2208606
    sget-boolean v1, LX/007;->j:Z

    move v1, v1

    .line 2208607
    if-nez v1, :cond_0

    .line 2208608
    new-instance v1, LX/6GW;

    const v2, 0x7f0818fa

    sget-object v3, LX/0ax;->do:Ljava/lang/String;

    const-string v4, "https://m.facebook.com/help/contact/268228883256323?refid=69"

    invoke-static {v4}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/6GW;-><init>(ILjava/lang/String;)V

    const v2, 0x7f0818fb

    .line 2208609
    iput v2, v1, LX/6GW;->c:I

    .line 2208610
    move-object v1, v1

    .line 2208611
    sget-object v2, LX/6GY;->RAP_SELECT_FEEDBACK:LX/6GY;

    .line 2208612
    iput-object v2, v1, LX/6GW;->d:LX/6GY;

    .line 2208613
    move-object v1, v1

    .line 2208614
    invoke-virtual {v1}, LX/6GW;->a()Lcom/facebook/bugreporter/activity/chooser/ChooserOption;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2208615
    :cond_0
    new-instance v1, LX/6GW;

    const v2, 0x7f0818fc

    sget-object v3, Lcom/facebook/bugreporter/activity/chooser/ChooserOption;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, LX/6GW;-><init>(ILjava/lang/String;)V

    const v2, 0x7f0818fd

    .line 2208616
    iput v2, v1, LX/6GW;->c:I

    .line 2208617
    move-object v1, v1

    .line 2208618
    sget-object v2, LX/6GY;->RAP_SELECT_BUG:LX/6GY;

    .line 2208619
    iput-object v2, v1, LX/6GW;->d:LX/6GY;

    .line 2208620
    move-object v1, v1

    .line 2208621
    invoke-virtual {v1}, LX/6GW;->a()Lcom/facebook/bugreporter/activity/chooser/ChooserOption;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2208622
    sget-boolean v1, LX/007;->j:Z

    move v1, v1

    .line 2208623
    if-nez v1, :cond_1

    .line 2208624
    new-instance v1, LX/6GW;

    const v2, 0x7f0818fe

    sget-object v3, LX/0ax;->do:Ljava/lang/String;

    const-string v4, "https://m.facebook.com/help/319931211461990?refid=69"

    invoke-static {v4}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, LX/6GW;-><init>(ILjava/lang/String;)V

    const v2, 0x7f0818ff

    .line 2208625
    iput v2, v1, LX/6GW;->c:I

    .line 2208626
    move-object v1, v1

    .line 2208627
    sget-object v2, LX/6GY;->RAP_SELECT_ABUSE:LX/6GY;

    .line 2208628
    iput-object v2, v1, LX/6GW;->d:LX/6GY;

    .line 2208629
    move-object v1, v1

    .line 2208630
    invoke-virtual {v1}, LX/6GW;->a()Lcom/facebook/bugreporter/activity/chooser/ChooserOption;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2208631
    :cond_1
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
