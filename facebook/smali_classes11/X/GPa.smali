.class public LX/GPa;
.super LX/6u5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6u5",
        "<",
        "Lcom/facebook/adspayments/protocol/EditPaymentCardParams;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2349130
    invoke-direct {p0, p1}, LX/6u5;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 2349131
    return-void
.end method

.method public static b(LX/0QB;)LX/GPa;
    .locals 2

    .prologue
    .line 2349132
    new-instance v1, LX/GPa;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-direct {v1, v0}, LX/GPa;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 2349133
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 2

    .prologue
    .line 2349134
    check-cast p1, Lcom/facebook/adspayments/protocol/EditPaymentCardParams;

    .line 2349135
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "edit_credit_card"

    .line 2349136
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 2349137
    move-object v0, v0

    .line 2349138
    const-string v1, "POST"

    .line 2349139
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 2349140
    move-object v0, v0

    .line 2349141
    iget-object v1, p1, Lcom/facebook/adspayments/protocol/EditPaymentCardParams;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2349142
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 2349143
    move-object v0, v0

    .line 2349144
    invoke-virtual {p1}, Lcom/facebook/adspayments/protocol/PaymentCardParams;->f()Ljava/util/List;

    move-result-object v1

    .line 2349145
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2349146
    move-object v0, v0

    .line 2349147
    sget-object v1, LX/14S;->STRING:LX/14S;

    .line 2349148
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2349149
    move-object v0, v0

    .line 2349150
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2349151
    const-string v0, "edit_payments_card"

    return-object v0
.end method
