.class public LX/GQm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Landroid/content/IntentFilter;


# instance fields
.field private final c:Landroid/content/Context;

.field public final d:LX/0So;

.field public final e:LX/03V;

.field private final f:LX/GQk;

.field private final g:LX/0Yd;

.field public h:Z

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:J

.field public o:I

.field public p:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2350427
    const-class v0, LX/GQm;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/GQm;->a:Ljava/lang/String;

    .line 2350428
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, LX/GQm;->b:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0So;LX/03V;LX/GQk;)V
    .locals 6
    .param p2    # LX/0So;
        .annotation runtime Lcom/facebook/common/time/ElapsedRealtimeSinceBoot;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    const/4 v3, -0x1

    .line 2350429
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2350430
    new-instance v0, LX/0Yd;

    const-string v1, "android.intent.action.BATTERY_CHANGED"

    new-instance v2, LX/GQl;

    invoke-direct {v2, p0}, LX/GQl;-><init>(LX/GQm;)V

    invoke-direct {v0, v1, v2}, LX/0Yd;-><init>(Ljava/lang/String;LX/0YZ;)V

    iput-object v0, p0, LX/GQm;->g:LX/0Yd;

    .line 2350431
    iput v3, p0, LX/GQm;->i:I

    .line 2350432
    iput v3, p0, LX/GQm;->j:I

    .line 2350433
    iput v3, p0, LX/GQm;->k:I

    .line 2350434
    iput v3, p0, LX/GQm;->l:I

    .line 2350435
    iput v3, p0, LX/GQm;->m:I

    .line 2350436
    iput-wide v4, p0, LX/GQm;->n:J

    .line 2350437
    iput v3, p0, LX/GQm;->o:I

    .line 2350438
    iput-wide v4, p0, LX/GQm;->p:J

    .line 2350439
    iput-object p1, p0, LX/GQm;->c:Landroid/content/Context;

    .line 2350440
    iput-object p2, p0, LX/GQm;->d:LX/0So;

    .line 2350441
    iput-object p3, p0, LX/GQm;->e:LX/03V;

    .line 2350442
    iput-object p4, p0, LX/GQm;->f:LX/GQk;

    .line 2350443
    return-void
.end method

.method public static a(LX/GQm;)V
    .locals 13

    .prologue
    const-wide/16 v10, -0x1

    const/4 v8, -0x1

    .line 2350444
    iget v0, p0, LX/GQm;->m:I

    if-eq v0, v8, :cond_0

    iget v0, p0, LX/GQm;->o:I

    if-eq v0, v8, :cond_0

    .line 2350445
    iget-object v0, p0, LX/GQm;->f:LX/GQk;

    iget v1, p0, LX/GQm;->m:I

    iget v2, p0, LX/GQm;->o:I

    iget v3, p0, LX/GQm;->l:I

    iget-wide v4, p0, LX/GQm;->p:J

    iget-wide v6, p0, LX/GQm;->n:J

    sub-long/2addr v4, v6

    .line 2350446
    iget-object v6, v0, LX/GQk;->a:LX/0Zb;

    const-string v7, "foreground_battery_drain"

    const/4 v9, 0x0

    invoke-interface {v6, v7, v9}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v6

    .line 2350447
    invoke-virtual {v6}, LX/0oG;->a()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2350448
    const-string v7, "start_battery_level"

    invoke-virtual {v6, v7, v1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v6

    const-string v7, "end_battery_level"

    invoke-virtual {v6, v7, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v6

    const-string v7, "max_battery_level"

    invoke-virtual {v6, v7, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    move-result-object v6

    const-string v7, "duration_ms"

    invoke-virtual {v6, v7, v4, v5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    move-result-object v6

    invoke-virtual {v6}, LX/0oG;->d()V

    .line 2350449
    :cond_0
    iput v8, p0, LX/GQm;->i:I

    .line 2350450
    iput v8, p0, LX/GQm;->l:I

    .line 2350451
    iput v8, p0, LX/GQm;->o:I

    .line 2350452
    iput-wide v10, p0, LX/GQm;->p:J

    .line 2350453
    iput v8, p0, LX/GQm;->m:I

    .line 2350454
    iput-wide v10, p0, LX/GQm;->n:J

    .line 2350455
    return-void
.end method

.method public static a(I)Z
    .locals 1

    .prologue
    .line 2350456
    const/4 v0, 0x3

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
