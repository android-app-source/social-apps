.class public final LX/FiX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:LX/Fib;

.field public final synthetic c:F

.field public final synthetic d:Ljava/lang/Integer;

.field public final synthetic e:LX/0wS;


# direct methods
.method public constructor <init>(LX/0wS;Landroid/view/View;LX/Fib;FLjava/lang/Integer;)V
    .locals 0

    .prologue
    .line 2275479
    iput-object p1, p0, LX/FiX;->e:LX/0wS;

    iput-object p2, p0, LX/FiX;->a:Landroid/view/View;

    iput-object p3, p0, LX/FiX;->b:LX/Fib;

    iput p4, p0, LX/FiX;->c:F

    iput-object p5, p0, LX/FiX;->d:Ljava/lang/Integer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 8

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/high16 v5, -0x40800000    # -1.0f

    const/4 v4, 0x0

    .line 2275480
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 2275481
    iget-object v0, p0, LX/FiX;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2275482
    sget-object v2, LX/Fia;->a:[I

    iget-object v3, p0, LX/FiX;->b:LX/Fib;

    invoke-virtual {v3}, LX/Fib;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2275483
    iget-object v1, p0, LX/FiX;->e:LX/0wS;

    iget-object v1, v1, LX/0wS;->f:LX/0hL;

    invoke-virtual {v1}, LX/0hL;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    int-to-float v1, v1

    .line 2275484
    :goto_0
    iget-object v2, p0, LX/FiX;->e:LX/0wS;

    iget-object v2, v2, LX/0wS;->f:LX/0hL;

    invoke-virtual {v2}, LX/0hL;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    int-to-float v2, v2

    move v7, v2

    move v2, v1

    move v1, v7

    .line 2275485
    :goto_1
    iget-object v3, p0, LX/FiX;->e:LX/0wS;

    iget-object v3, v3, LX/0wS;->f:LX/0hL;

    invoke-virtual {v3}, LX/0hL;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2275486
    float-to-int v1, v1

    float-to-int v2, v2

    invoke-virtual {v0, v1, v4, v2, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 2275487
    :goto_2
    iget-object v1, p0, LX/FiX;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2275488
    iget-object v0, p0, LX/FiX;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    .line 2275489
    return-void

    .line 2275490
    :pswitch_0
    iget v2, p0, LX/FiX;->c:F

    sub-float v3, v6, v1

    mul-float/2addr v2, v3

    .line 2275491
    iget-object v3, p0, LX/FiX;->d:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v1, v3

    mul-float/2addr v1, v5

    .line 2275492
    goto :goto_1

    .line 2275493
    :pswitch_1
    iget v2, p0, LX/FiX;->c:F

    mul-float/2addr v2, v1

    .line 2275494
    iget-object v3, p0, LX/FiX;->d:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-float v3, v3

    sub-float v1, v6, v1

    mul-float/2addr v1, v3

    mul-float/2addr v1, v5

    .line 2275495
    goto :goto_1

    .line 2275496
    :cond_0
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    int-to-float v1, v1

    goto :goto_0

    .line 2275497
    :cond_1
    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    int-to-float v2, v2

    move v7, v2

    move v2, v1

    move v1, v7

    goto :goto_1

    .line 2275498
    :cond_2
    float-to-int v2, v2

    float-to-int v1, v1

    invoke-virtual {v0, v2, v4, v1, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
