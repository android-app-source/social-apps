.class public final LX/GDW;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/3G4;

.field public final synthetic b:LX/GDY;


# direct methods
.method public constructor <init>(LX/GDY;LX/3G4;)V
    .locals 0

    .prologue
    .line 2329892
    iput-object p1, p0, LX/GDW;->b:LX/GDY;

    iput-object p2, p0, LX/GDW;->a:LX/3G4;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2329893
    iget-object v0, p0, LX/GDW;->b:LX/GDY;

    iget-object v0, v0, LX/GDY;->e:LX/2U3;

    const-class v1, LX/GDY;

    const-string v2, "Hit a NonCancellationFailure in offline mutation"

    invoke-virtual {v0, v1, v2, p1}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2329894
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2329895
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2329896
    if-eqz p1, :cond_0

    .line 2329897
    iget-object v0, p0, LX/GDW;->b:LX/GDY;

    iget-object v1, p0, LX/GDW;->a:LX/3G4;

    iget-object v1, v1, LX/3G3;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/GDY;->a(Ljava/lang/String;)V

    .line 2329898
    :goto_0
    return-void

    .line 2329899
    :cond_0
    iget-object v0, p0, LX/GDW;->b:LX/GDY;

    iget-object v1, p0, LX/GDW;->a:LX/3G4;

    iget-object v1, v1, LX/3G3;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/GDY;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
