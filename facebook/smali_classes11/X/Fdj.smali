.class public LX/Fdj;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2263965
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    const-string v1, "friends"

    const v2, 0x7f0208a5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "city"

    const v2, 0x7f020964

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "employer"

    const v2, 0x7f02078a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    const-string v1, "school"

    const v2, 0x7f02084d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/Fdj;->a:LX/0P1;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2263966
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;LX/0Px;)Ljava/lang/String;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 2263967
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_0

    .line 2263968
    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CyH;

    .line 2263969
    iget-object v1, v0, LX/CyH;->b:Ljava/lang/String;

    move-object v0, v1

    .line 2263970
    :goto_0
    return-object v0

    .line 2263971
    :cond_0
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    const/4 v2, 0x3

    if-le v0, v2, :cond_1

    .line 2263972
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0820aa

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v4

    invoke-virtual {v0, v2, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2263973
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 2263974
    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CyH;

    .line 2263975
    iget-object v3, v0, LX/CyH;->b:Ljava/lang/String;

    move-object v0, v3

    .line 2263976
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2263977
    :goto_1
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2263978
    const-string v0, " \u2022 "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2263979
    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CyH;

    .line 2263980
    iget-object v3, v0, LX/CyH;->b:Ljava/lang/String;

    move-object v0, v3

    .line 2263981
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2263982
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2263983
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
