.class public final LX/Gqr;
.super LX/Cqf;
.source ""


# instance fields
.field public final synthetic g:LX/Gqs;


# direct methods
.method public constructor <init>(LX/Gqs;LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V
    .locals 7

    .prologue
    .line 2396993
    iput-object p1, p0, LX/Gqr;->g:LX/Gqs;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, LX/Cqf;-><init>(LX/Cqw;LX/Ctg;LX/Cqd;LX/Cqe;LX/Cqc;Ljava/lang/Float;)V

    return-void
.end method


# virtual methods
.method public final g()V
    .locals 5

    .prologue
    .line 2396994
    invoke-virtual {p0}, LX/Cqf;->e()Landroid/graphics/Rect;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2396995
    invoke-virtual {p0}, LX/Cqf;->m()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2396996
    invoke-virtual {p0}, LX/Cqf;->e()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    .line 2396997
    iget-object v1, p0, LX/Gqr;->g:LX/Gqs;

    iget v1, v1, LX/Gqs;->b:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2396998
    invoke-virtual {p0}, LX/Cqf;->m()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2396999
    invoke-virtual {p0}, LX/Cqf;->e()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    iget-object v1, p0, LX/Gqr;->g:LX/Gqs;

    iget v1, v1, LX/Gqs;->a:I

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    .line 2397000
    new-instance v1, Landroid/graphics/Rect;

    const/4 v2, 0x0

    iget-object v3, p0, LX/Gqr;->g:LX/Gqs;

    iget v3, v3, LX/Gqs;->a:I

    add-int/2addr v3, v0

    iget-object v4, p0, LX/Gqr;->g:LX/Gqs;

    iget v4, v4, LX/Gqs;->b:I

    invoke-direct {v1, v0, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2397001
    invoke-virtual {p0}, LX/Cqf;->m()Landroid/view/View;

    move-result-object v0

    new-instance v2, LX/CrW;

    invoke-direct {v2, v1}, LX/CrW;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {p0, v0, v2}, LX/Cqf;->a(Landroid/view/View;LX/CqY;)V

    .line 2397002
    :goto_0
    return-void

    .line 2397003
    :cond_0
    invoke-super {p0}, LX/Cqf;->g()V

    goto :goto_0
.end method
