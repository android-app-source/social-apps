.class public final LX/F7I;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;)V
    .locals 0

    .prologue
    .line 2201379
    iput-object p1, p0, LX/F7I;->a:Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 2201380
    sget-object v0, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->p:Ljava/lang/Class;

    const-string v1, "Error while fetching PYMK"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2201381
    iget-object v0, p0, LX/F7I;->a:Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->a(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;Z)V

    .line 2201382
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2201383
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2201384
    iget-object v1, p0, LX/F7I;->a:Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 2201385
    :goto_0
    if-nez v0, :cond_2

    .line 2201386
    :cond_0
    :goto_1
    iget-object v0, p0, LX/F7I;->a:Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->a(Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;Z)V

    .line 2201387
    return-void

    .line 2201388
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Result;

    goto :goto_0

    .line 2201389
    :cond_2
    invoke-virtual {v0}, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Result;->a()Ljava/util/List;

    move-result-object v2

    .line 2201390
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2201391
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v3

    .line 2201392
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;

    .line 2201393
    invoke-static {v2}, LX/F7L;->b(Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;)LX/F7L;

    move-result-object v5

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2201394
    iget-object v5, v1, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->z:Ljava/util/Set;

    iget-wide v6, v2, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->userId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2201395
    :cond_3
    iget-object v2, v1, Lcom/facebook/growth/friendfinder/FriendFinderAddFriendsFragment;->y:LX/F73;

    invoke-virtual {v2, v3}, LX/F73;->b(Ljava/util/List;)V

    goto :goto_1
.end method
