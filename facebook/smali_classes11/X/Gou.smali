.class public LX/Gou;
.super LX/Goq;
.source ""

# interfaces
.implements LX/GoX;
.implements LX/GoL;
.implements LX/GoY;
.implements LX/GoZ;
.implements LX/Gob;


# instance fields
.field public final a:LX/CHc;


# direct methods
.method public constructor <init>(LX/CHc;)V
    .locals 3

    .prologue
    .line 2394649
    invoke-interface {p1}, LX/CHb;->c()LX/CHa;

    move-result-object v0

    const/16 v1, 0x71

    invoke-interface {p1}, LX/CHb;->e()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, LX/Goq;-><init>(LX/CHa;II)V

    .line 2394650
    iput-object p1, p0, LX/Gou;->a:LX/CHc;

    .line 2394651
    return-void
.end method


# virtual methods
.method public final C()I
    .locals 1

    .prologue
    .line 2394652
    iget-object v0, p0, LX/Gou;->a:LX/CHc;

    invoke-interface {v0}, LX/CHb;->e()I

    move-result v0

    return v0
.end method

.method public final D()LX/GoE;
    .locals 3

    .prologue
    .line 2394648
    new-instance v0, LX/GoE;

    iget-object v1, p0, LX/Gou;->a:LX/CHc;

    invoke-interface {v1}, LX/CHb;->jt_()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Gou;->a:LX/CHc;

    invoke-interface {v2}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/GoE;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final e()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394647
    iget-object v0, p0, LX/Gou;->a:LX/CHc;

    invoke-interface {v0}, LX/CHb;->c()LX/CHa;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Gou;->a:LX/CHc;

    invoke-interface {v0}, LX/CHb;->c()LX/CHa;

    move-result-object v0

    invoke-interface {v0}, LX/CHa;->jv_()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getAction()LX/CHX;
    .locals 1

    .prologue
    .line 2394646
    iget-object v0, p0, LX/Gou;->a:LX/CHc;

    invoke-interface {v0}, LX/CHc;->j()LX/CHX;

    move-result-object v0

    return-object v0
.end method
