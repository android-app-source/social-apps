.class public final LX/Fh7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/3sI;


# instance fields
.field public final synthetic a:Lcom/facebook/search/suggestions/SuggestionsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/suggestions/SuggestionsFragment;)V
    .locals 0

    .prologue
    .line 2271294
    iput-object p1, p0, LX/Fh7;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;F)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 2271295
    const/high16 v0, -0x40800000    # -1.0f

    cmpg-float v0, p2, v0

    if-lez v0, :cond_0

    cmpl-float v0, p2, v2

    if-ltz v0, :cond_1

    .line 2271296
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p2

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 2271297
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    .line 2271298
    :goto_0
    return-void

    .line 2271299
    :cond_1
    cmpl-float v0, p2, v1

    if-nez v0, :cond_2

    .line 2271300
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p2

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 2271301
    invoke-virtual {p1, v2}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 2271302
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    neg-float v1, p2

    mul-float/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 2271303
    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sub-float v0, v2, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method
