.class public final LX/F1Q;
.super LX/0wa;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;)V
    .locals 0

    .prologue
    .line 2191901
    iput-object p1, p0, LX/F1Q;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    invoke-direct {p0}, LX/0wa;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 2191902
    iget-object v0, p0, LX/F1Q;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    iget-object v0, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v3

    move v1, v3

    :goto_0
    if-ge v2, v4, :cond_3

    iget-object v0, p0, LX/F1Q;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    iget-object v0, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;

    .line 2191903
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->c()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2191904
    iget-object v5, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->a:LX/F1P;

    move-object v5, v5

    .line 2191905
    sget-object v6, LX/F1P;->BEFORE_INITIAL_ANIMATION:LX/F1P;

    if-ne v5, v6, :cond_0

    .line 2191906
    iput-wide p1, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->c:J

    .line 2191907
    sget-object v6, LX/F1P;->DURING_INITIAL_ANIMATION:LX/F1P;

    .line 2191908
    iput-object v6, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->a:LX/F1P;

    .line 2191909
    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->b(J)V

    .line 2191910
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->c()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2191911
    const/4 v0, 0x1

    .line 2191912
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 2191913
    :cond_1
    sget-object v6, LX/F1P;->DURING_INITIAL_ANIMATION:LX/F1P;

    if-ne v5, v6, :cond_2

    .line 2191914
    sget-object v5, LX/F1P;->AFTER_INITIAL_ANIMATION:LX/F1P;

    .line 2191915
    iput-object v5, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView$AnimatedObject;->a:LX/F1P;

    .line 2191916
    :cond_2
    move v0, v1

    goto :goto_1

    .line 2191917
    :cond_3
    if-eqz v1, :cond_4

    .line 2191918
    iget-object v0, p0, LX/F1Q;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    iget-object v0, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->a:LX/0wY;

    iget-object v1, p0, LX/F1Q;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    iget-object v1, v1, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->x:LX/0wa;

    invoke-interface {v0, v1}, LX/0wY;->a(LX/0wa;)V

    .line 2191919
    :goto_2
    return-void

    .line 2191920
    :cond_4
    iget-object v0, p0, LX/F1Q;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;

    .line 2191921
    iput-boolean v3, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackAnimatedHeaderView;->y:Z

    .line 2191922
    goto :goto_2
.end method
