.class public LX/Gid;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/Gic;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2386424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2386425
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 2386395
    check-cast p1, LX/Gic;

    .line 2386396
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2386397
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "filetype"

    const-string v3, "1"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2386398
    iget-object v1, p1, LX/Gic;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    iget-object v1, p1, LX/Gic;->a:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    .line 2386399
    new-instance v1, LX/4ct;

    iget-object v2, p1, LX/Gic;->a:Ljava/io/File;

    const-string v3, "application/octet-stream"

    iget-object v4, p1, LX/Gic;->a:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/4ct;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 2386400
    new-instance v2, LX/4cQ;

    const-string v3, "lib"

    invoke-direct {v2, v3, v1}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    .line 2386401
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v3, "Upload library to GLC"

    .line 2386402
    iput-object v3, v1, LX/14O;->b:Ljava/lang/String;

    .line 2386403
    move-object v1, v1

    .line 2386404
    const-string v3, "POST"

    .line 2386405
    iput-object v3, v1, LX/14O;->c:Ljava/lang/String;

    .line 2386406
    move-object v1, v1

    .line 2386407
    iget-object v3, p1, LX/Gic;->b:Ljava/lang/String;

    .line 2386408
    iput-object v3, v1, LX/14O;->d:Ljava/lang/String;

    .line 2386409
    move-object v1, v1

    .line 2386410
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2386411
    move-object v0, v1

    .line 2386412
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2386413
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2386414
    move-object v0, v0

    .line 2386415
    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2386416
    iput-object v1, v0, LX/14O;->l:Ljava/util/List;

    .line 2386417
    move-object v0, v0

    .line 2386418
    sget-object v1, LX/14R;->MULTI_PART_ENTITY:LX/14R;

    .line 2386419
    iput-object v1, v0, LX/14O;->w:LX/14R;

    .line 2386420
    move-object v0, v0

    .line 2386421
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2386422
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2386423
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "success"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
