.class public LX/FKT;
.super LX/4Zv;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4Zv",
        "<",
        "Lcom/facebook/messaging/service/model/ModifyThreadParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/FFR;


# direct methods
.method public constructor <init>(LX/0sO;LX/FFR;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2225095
    invoke-direct {p0, p1}, LX/4Zv;-><init>(LX/0sO;)V

    .line 2225096
    iput-object p2, p0, LX/FKT;->b:LX/FFR;

    .line 2225097
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2225119
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2225120
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2225118
    const/4 v0, 0x0

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 2225098
    check-cast p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;

    .line 2225099
    iget-object v0, p0, LX/FKT;->b:LX/FFR;

    .line 2225100
    iget-object v1, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2225101
    iget-object v2, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->q:LX/03R;

    move-object v2, v2

    .line 2225102
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/03R;->asBoolean(Z)Z

    move-result v2

    .line 2225103
    new-instance p0, LX/4HB;

    invoke-direct {p0}, LX/4HB;-><init>()V

    .line 2225104
    const-string v3, "thread_fbid"

    invoke-virtual {p0, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2225105
    move-object p1, p0

    .line 2225106
    iget-object v3, v0, LX/FFR;->a:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2225107
    const-string v0, "actor_id"

    invoke-virtual {p1, v0, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2225108
    move-object p1, p1

    .line 2225109
    if-eqz v2, :cond_0

    const-string v3, "APPROVALS"

    .line 2225110
    :goto_0
    const-string v0, "mode"

    invoke-virtual {p1, v0, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2225111
    move-object v0, p0

    .line 2225112
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2225113
    const-string v2, "client_mutation_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2225114
    move-object v0, v0

    .line 2225115
    new-instance v1, LX/5d4;

    invoke-direct {v1}, LX/5d4;-><init>()V

    move-object v1, v1

    .line 2225116
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    return-object v0

    .line 2225117
    :cond_0
    const-string v3, "OPEN"

    goto :goto_0
.end method
