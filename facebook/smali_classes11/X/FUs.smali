.class public LX/FUs;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2250049
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2250050
    return-void
.end method

.method public static a()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2250051
    const-string v0, "scrollTo"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, LX/5ps;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/FUp;Ljava/lang/Object;ILX/5pC;)V
    .locals 7
    .param p3    # LX/5pC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/FUp",
            "<TT;>;TT;I",
            "LX/5pC;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2250052
    invoke-static {p0}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2250053
    invoke-static {p1}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2250054
    invoke-static {p3}, LX/0nE;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2250055
    packed-switch p2, :pswitch_data_0

    .line 2250056
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported command %d received by %s."

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2250057
    :pswitch_0
    invoke-interface {p3, v4}, LX/5pC;->getDouble(I)D

    move-result-wide v0

    invoke-static {v0, v1}, LX/5r2;->a(D)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 2250058
    invoke-interface {p3, v5}, LX/5pC;->getDouble(I)D

    move-result-wide v2

    invoke-static {v2, v3}, LX/5r2;->a(D)F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 2250059
    invoke-interface {p3, v6}, LX/5pC;->getBoolean(I)Z

    move-result v2

    .line 2250060
    new-instance v3, LX/FUr;

    invoke-direct {v3, v0, v1, v2}, LX/FUr;-><init>(IIZ)V

    invoke-interface {p0, p1, v3}, LX/FUp;->a(Ljava/lang/Object;LX/FUr;)V

    .line 2250061
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
