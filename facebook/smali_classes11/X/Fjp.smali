.class public LX/Fjp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile x:LX/Fjp;


# instance fields
.field private final b:Landroid/content/Context;

.field public final c:LX/0cZ;

.field public final d:LX/0YO;

.field public final e:LX/0SG;

.field public final f:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final g:LX/0WV;

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fjj;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/EmQ;

.field public final j:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fjr;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fjf;",
            ">;"
        }
    .end annotation
.end field

.field public final l:Ljava/util/concurrent/ExecutorService;

.field private final m:LX/0en;

.field private final n:LX/0V8;

.field private final o:LX/EmX;

.field private final p:Lcom/facebook/content/SecureContextHelper;

.field public final q:Z

.field public final r:LX/0lC;

.field private final s:LX/Fjk;

.field private final t:LX/Fjl;

.field private final u:LX/Fjm;

.field private final v:LX/Fjn;

.field private final w:LX/Fjo;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2277797
    const-class v0, LX/Fjp;

    sput-object v0, LX/Fjp;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0cZ;LX/0YO;LX/0WV;Landroid/content/Context;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/EmQ;LX/0Or;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0en;LX/0V8;LX/EmX;Lcom/facebook/content/SecureContextHelper;LX/04P;LX/0lC;)V
    .locals 3
    .param p11    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0cZ;",
            "LX/0YO;",
            "LX/0WV;",
            "Landroid/content/Context;",
            "LX/0SG;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Ot",
            "<",
            "LX/Fjj;",
            ">;",
            "LX/EmQ;",
            "LX/0Or",
            "<",
            "LX/Fjr;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fjf;",
            ">;",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0en;",
            "LX/0V8;",
            "LX/EmX;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/04P;",
            "LX/0lC;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2277766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2277767
    new-instance v1, LX/Fjk;

    invoke-direct {v1, p0}, LX/Fjk;-><init>(LX/Fjp;)V

    iput-object v1, p0, LX/Fjp;->s:LX/Fjk;

    .line 2277768
    new-instance v1, LX/Fjl;

    invoke-direct {v1, p0}, LX/Fjl;-><init>(LX/Fjp;)V

    iput-object v1, p0, LX/Fjp;->t:LX/Fjl;

    .line 2277769
    new-instance v1, LX/Fjm;

    invoke-direct {v1, p0}, LX/Fjm;-><init>(LX/Fjp;)V

    iput-object v1, p0, LX/Fjp;->u:LX/Fjm;

    .line 2277770
    new-instance v1, LX/Fjn;

    invoke-direct {v1, p0}, LX/Fjn;-><init>(LX/Fjp;)V

    iput-object v1, p0, LX/Fjp;->v:LX/Fjn;

    .line 2277771
    new-instance v1, LX/Fjo;

    invoke-direct {v1, p0}, LX/Fjo;-><init>(LX/Fjp;)V

    iput-object v1, p0, LX/Fjp;->w:LX/Fjo;

    .line 2277772
    iput-object p1, p0, LX/Fjp;->c:LX/0cZ;

    .line 2277773
    iput-object p2, p0, LX/Fjp;->d:LX/0YO;

    .line 2277774
    iput-object p3, p0, LX/Fjp;->g:LX/0WV;

    .line 2277775
    iput-object p4, p0, LX/Fjp;->b:Landroid/content/Context;

    .line 2277776
    iput-object p5, p0, LX/Fjp;->e:LX/0SG;

    .line 2277777
    iput-object p6, p0, LX/Fjp;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2277778
    iput-object p7, p0, LX/Fjp;->h:LX/0Ot;

    .line 2277779
    iput-object p8, p0, LX/Fjp;->i:LX/EmQ;

    .line 2277780
    iput-object p9, p0, LX/Fjp;->j:LX/0Or;

    .line 2277781
    iput-object p10, p0, LX/Fjp;->k:LX/0Ot;

    .line 2277782
    iput-object p11, p0, LX/Fjp;->l:Ljava/util/concurrent/ExecutorService;

    .line 2277783
    iput-object p12, p0, LX/Fjp;->m:LX/0en;

    .line 2277784
    move-object/from16 v0, p13

    iput-object v0, p0, LX/Fjp;->n:LX/0V8;

    .line 2277785
    move-object/from16 v0, p14

    iput-object v0, p0, LX/Fjp;->o:LX/EmX;

    .line 2277786
    move-object/from16 v0, p15

    iput-object v0, p0, LX/Fjp;->p:Lcom/facebook/content/SecureContextHelper;

    .line 2277787
    move-object/from16 v0, p17

    iput-object v0, p0, LX/Fjp;->r:LX/0lC;

    .line 2277788
    iget-object v1, p0, LX/Fjp;->o:LX/EmX;

    iget-object v2, p0, LX/Fjp;->s:LX/Fjk;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2277789
    iget-object v1, p0, LX/Fjp;->o:LX/EmX;

    iget-object v2, p0, LX/Fjp;->t:LX/Fjl;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2277790
    iget-object v1, p0, LX/Fjp;->o:LX/EmX;

    iget-object v2, p0, LX/Fjp;->u:LX/Fjm;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2277791
    iget-object v1, p0, LX/Fjp;->o:LX/EmX;

    iget-object v2, p0, LX/Fjp;->v:LX/Fjn;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2277792
    iget-object v1, p0, LX/Fjp;->o:LX/EmX;

    iget-object v2, p0, LX/Fjp;->w:LX/Fjo;

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2277793
    const-string v1, "com.facebook.selfupdate.enabled"

    move-object/from16 v0, p16

    invoke-virtual {v0, v1}, LX/04P;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2277794
    if-eqz v1, :cond_0

    const-string v2, "true"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, LX/Fjp;->q:Z

    .line 2277795
    return-void

    .line 2277796
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/Fjp;
    .locals 3

    .prologue
    .line 2277756
    sget-object v0, LX/Fjp;->x:LX/Fjp;

    if-nez v0, :cond_1

    .line 2277757
    const-class v1, LX/Fjp;

    monitor-enter v1

    .line 2277758
    :try_start_0
    sget-object v0, LX/Fjp;->x:LX/Fjp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2277759
    if-eqz v2, :cond_0

    .line 2277760
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/Fjp;->b(LX/0QB;)LX/Fjp;

    move-result-object v0

    sput-object v0, LX/Fjp;->x:LX/Fjp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2277761
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2277762
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2277763
    :cond_1
    sget-object v0, LX/Fjp;->x:LX/Fjp;

    return-object v0

    .line 2277764
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2277765
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static declared-synchronized a$redex0(LX/Fjp;J)V
    .locals 3

    .prologue
    .line 2277699
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Fjp;->i:LX/EmQ;

    sget-object v1, LX/EmM;->APP_UPDATE:LX/EmM;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, p2, v1, v2}, LX/EmQ;->a(JLX/EmM;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2277700
    monitor-exit p0

    return-void

    .line 2277701
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b(LX/0QB;)LX/Fjp;
    .locals 19

    .prologue
    .line 2277754
    new-instance v1, LX/Fjp;

    invoke-static/range {p0 .. p0}, LX/0cZ;->b(LX/0QB;)LX/0cZ;

    move-result-object v2

    check-cast v2, LX/0cZ;

    invoke-static/range {p0 .. p0}, LX/0YO;->b(LX/0QB;)LX/0YO;

    move-result-object v3

    check-cast v3, LX/0YO;

    invoke-static/range {p0 .. p0}, LX/0WD;->a(LX/0QB;)LX/0WV;

    move-result-object v4

    check-cast v4, LX/0WV;

    const-class v5, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v8, 0x3523

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-static/range {p0 .. p0}, LX/EmQ;->a(LX/0QB;)LX/EmQ;

    move-result-object v9

    check-cast v9, LX/EmQ;

    const/16 v10, 0x3526

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    const/16 v11, 0x3522

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v12

    check-cast v12, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0en;->a(LX/0QB;)LX/0en;

    move-result-object v13

    check-cast v13, LX/0en;

    invoke-static/range {p0 .. p0}, LX/0V7;->a(LX/0QB;)LX/0V8;

    move-result-object v14

    check-cast v14, LX/0V8;

    invoke-static/range {p0 .. p0}, LX/EmX;->a(LX/0QB;)LX/EmX;

    move-result-object v15

    check-cast v15, LX/EmX;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v16

    check-cast v16, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, LX/1Mp;->a(LX/0QB;)LX/04P;

    move-result-object v17

    check-cast v17, LX/04P;

    invoke-static/range {p0 .. p0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v18

    check-cast v18, LX/0lC;

    invoke-direct/range {v1 .. v18}, LX/Fjp;-><init>(LX/0cZ;LX/0YO;LX/0WV;Landroid/content/Context;LX/0SG;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Ot;LX/EmQ;LX/0Or;LX/0Ot;Ljava/util/concurrent/ExecutorService;LX/0en;LX/0V8;LX/EmX;Lcom/facebook/content/SecureContextHelper;LX/04P;LX/0lC;)V

    .line 2277755
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;J)V
    .locals 18

    .prologue
    .line 2277707
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fjp;->r:LX/0lC;

    invoke-virtual {v2}, LX/0lC;->e()LX/0m9;

    move-result-object v11

    .line 2277708
    const/4 v8, 0x1

    .line 2277709
    const/4 v9, 0x0

    .line 2277710
    const/4 v10, 0x0

    .line 2277711
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fjp;->i:LX/EmQ;

    sget-object v6, LX/EmM;->APP_UPDATE:LX/EmM;

    const/4 v7, 0x0

    move-object/from16 v3, p1

    move-wide/from16 v4, p2

    invoke-virtual/range {v2 .. v7}, LX/EmQ;->a(Ljava/lang/String;JLX/EmM;Ljava/lang/String;)Ljava/io/File;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    move-object v3, v2

    .line 2277712
    :goto_0
    :try_start_1
    const-string v4, "file_null"

    if-nez v3, :cond_3

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v11, v4, v2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 2277713
    if-eqz v3, :cond_7

    .line 2277714
    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v5

    .line 2277715
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fjp;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v4, LX/Fjg;->h:LX/0Tn;

    invoke-interface {v2, v4, v5}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v2

    sget-object v4, LX/Fjg;->n:LX/0Tn;

    const/4 v6, 0x2

    invoke-interface {v2, v4, v6}, LX/0hN;->a(LX/0Tn;I)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2277716
    :try_start_2
    invoke-static {v3}, LX/0en;->b(Ljava/io/File;)Ljava/util/jar/JarFile;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v2

    move-object v4, v2

    .line 2277717
    :goto_2
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fjp;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/Fjg;->r:LX/0Tn;

    const-string v6, ""

    invoke-interface {v2, v3, v6}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2277718
    const-string v2, "source"

    invoke-virtual {v11, v2, v6}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2277719
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fjp;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fjj;

    const-string v3, "selfupdate_download_success_file_uri"

    const-string v7, "local_file_uri"

    const-string v9, "source"

    invoke-static {v7, v5, v9, v6}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v7

    invoke-virtual {v2, v3, v7}, LX/Fjj;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2277720
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fjp;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/Fjg;->p:LX/0Tn;

    const-string v7, "application/vnd.android.package-archive"

    invoke-interface {v2, v3, v7}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2277721
    const-string v2, "mime_type"

    invoke-virtual {v11, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2277722
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fjp;->k:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fjf;

    invoke-virtual {v2, v4, v3}, LX/Fjf;->a(Ljava/util/jar/JarFile;Ljava/lang/String;)Z

    move-result v7

    .line 2277723
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fjp;->n:LX/0V8;

    sget-object v9, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v2, v9}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v12

    .line 2277724
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fjp;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v9, LX/Fjg;->q:LX/0Tn;

    const-wide/32 v14, 0x1e00000

    invoke-interface {v2, v9, v14, v15}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v14

    .line 2277725
    cmp-long v2, v12, v14

    if-ltz v2, :cond_4

    const/4 v2, 0x1

    .line 2277726
    :goto_3
    const-string v9, "valid_file"

    invoke-virtual {v11, v9, v7}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 2277727
    const-string v9, "free_space"

    invoke-virtual {v11, v9, v12, v13}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 2277728
    const-string v9, "file_size"

    invoke-virtual {v11, v9, v14, v15}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 2277729
    const-string v9, "has_required_space"

    invoke-virtual {v11, v9, v2}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 2277730
    if-eqz v7, :cond_5

    if-eqz v2, :cond_5

    .line 2277731
    const-string v2, "application/java-archive"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2277732
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fjp;->j:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fjr;

    invoke-virtual {v2}, LX/Fjr;->b()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2277733
    :cond_0
    const/4 v3, 0x0

    .line 2277734
    :try_start_4
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fjp;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fjj;

    const-string v7, "selfupdate_download_success"

    const-string v8, "source"

    invoke-static {v8, v6}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v6

    invoke-virtual {v2, v7, v6}, LX/Fjj;->a(Ljava/lang/String;Ljava/util/Map;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2277735
    :goto_4
    if-eqz v4, :cond_1

    .line 2277736
    :try_start_5
    invoke-virtual {v4}, Ljava/util/jar/JarFile;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 2277737
    :cond_1
    :goto_5
    if-eqz v3, :cond_2

    .line 2277738
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fjp;->j:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fjr;

    const-string v3, "selfupdate_download_validation_failure"

    invoke-virtual {v2, v3, v11}, LX/Fjr;->a(Ljava/lang/String;LX/0m9;)V

    .line 2277739
    :cond_2
    return-void

    .line 2277740
    :catch_0
    move-exception v2

    move-object v3, v2

    .line 2277741
    :try_start_6
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fjp;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fjj;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to open downloaded file: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, v3}, LX/Fjj;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v3, v10

    goto/16 :goto_0

    .line 2277742
    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 2277743
    :catch_1
    move-exception v3

    .line 2277744
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fjp;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fjj;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Failed to open JarFile: "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, v3}, LX/Fjj;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v4, v9

    .line 2277745
    goto/16 :goto_2

    .line 2277746
    :catch_2
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fjp;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fjj;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to open JarFile by OOM: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Fjj;->a(Ljava/lang/String;)V

    move-object v4, v9

    goto/16 :goto_2

    .line 2277747
    :cond_4
    const/4 v2, 0x0

    goto/16 :goto_3

    .line 2277748
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fjp;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fjj;

    const-string v3, "Downloaded package is invalid or corrupt"

    invoke-virtual {v2, v3}, LX/Fjj;->a(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move v3, v8

    goto/16 :goto_4

    .line 2277749
    :catch_3
    :try_start_7
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fjp;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fjj;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "Failed to close JarFile: "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/Fjj;->a(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_5

    .line 2277750
    :catchall_0
    move-exception v2

    move-object/from16 v16, v2

    move v2, v3

    move-object/from16 v3, v16

    :goto_6
    if-eqz v2, :cond_6

    .line 2277751
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fjp;->j:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fjr;

    const-string v4, "selfupdate_download_validation_failure"

    invoke-virtual {v2, v4, v11}, LX/Fjr;->a(Ljava/lang/String;LX/0m9;)V

    :cond_6
    throw v3

    .line 2277752
    :cond_7
    :try_start_8
    move-object/from16 v0, p0

    iget-object v2, v0, LX/Fjp;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Fjj;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Download succeeded, but file the is missing: id= "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p2

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Fjj;->a(Ljava/lang/String;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move v3, v8

    goto/16 :goto_5

    .line 2277753
    :catchall_1
    move-exception v2

    move-object v3, v2

    move v2, v8

    goto :goto_6
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 2277702
    iget-object v0, p0, LX/Fjp;->i:LX/EmQ;

    invoke-virtual {v0}, LX/EmQ;->a()V

    .line 2277703
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/Fjp;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/selfupdate/SelfUpdateFetchService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2277704
    const-string v1, "force_update"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2277705
    iget-object v1, p0, LX/Fjp;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 2277706
    return-void
.end method
