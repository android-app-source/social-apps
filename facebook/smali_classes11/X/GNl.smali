.class public final LX/GNl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adspayments/graphql/FetchPaymentOptionsGraphQLModels$FetchPaymentOptionsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/support/v4/app/DialogFragment;

.field public final synthetic b:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

.field public final synthetic c:Lcom/facebook/common/locale/Country;

.field public final synthetic d:Ljava/lang/String;

.field public final synthetic e:I

.field public final synthetic f:LX/GNm;


# direct methods
.method public constructor <init>(LX/GNm;Landroid/support/v4/app/DialogFragment;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 2346487
    iput-object p1, p0, LX/GNl;->f:LX/GNm;

    iput-object p2, p0, LX/GNl;->a:Landroid/support/v4/app/DialogFragment;

    iput-object p3, p0, LX/GNl;->b:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    iput-object p4, p0, LX/GNl;->c:Lcom/facebook/common/locale/Country;

    iput-object p5, p0, LX/GNl;->d:Ljava/lang/String;

    iput p6, p0, LX/GNl;->e:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 6

    .prologue
    .line 2346488
    iget-object v0, p0, LX/GNl;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2346489
    iget-object v0, p0, LX/GNl;->f:LX/GNm;

    iget-object v0, v0, LX/GNm;->d:LX/ADW;

    iget-object v1, p0, LX/GNl;->b:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-virtual {v0, p1, v1}, LX/ADW;->a(Ljava/lang/Throwable;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)Lcom/facebook/adspayments/analytics/PaymentsReliabilityErrorLogEvent;

    .line 2346490
    iget-object v0, p0, LX/GNl;->f:LX/GNm;

    iget-object v1, v0, LX/GNm;->d:LX/ADW;

    new-instance v0, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    const-string v2, "payments_fetch_payment_option_failed"

    iget-object v3, p0, LX/GNl;->b:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-direct {v0, v2, v3}, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;-><init>(Ljava/lang/String;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    invoke-virtual {v0, p1}, Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;->a(Ljava/lang/Throwable;)Lcom/facebook/adspayments/analytics/BasePaymentsLogEvent;

    move-result-object v0

    check-cast v0, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    invoke-virtual {v1, v0}, LX/ADW;->a(Lcom/facebook/adspayments/analytics/PaymentsLogEvent;)V

    .line 2346491
    iget-object v0, p0, LX/GNl;->f:LX/GNm;

    iget-object v1, p0, LX/GNl;->b:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    iget-object v2, p0, LX/GNl;->c:Lcom/facebook/common/locale/Country;

    sget-object v3, LX/GNm;->a:LX/0Px;

    iget-object v4, p0, LX/GNl;->d:Ljava/lang/String;

    iget v5, p0, LX/GNl;->e:I

    .line 2346492
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2346493
    const-string p1, "payments_flow_context_key"

    invoke-virtual {p0, p1, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2346494
    const-string p1, "country"

    invoke-virtual {p0, p1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2346495
    const-string p1, "payment_options"

    invoke-virtual {p0, p1, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2346496
    const-string p1, "request_code"

    invoke-virtual {p0, p1, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2346497
    new-instance p1, Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;

    invoke-direct {p1}, Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;-><init>()V

    .line 2346498
    invoke-virtual {p1, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2346499
    move-object p0, p1

    .line 2346500
    iget-object p1, v0, LX/GNm;->b:LX/0gc;

    invoke-virtual {p0, p1, v4}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2346501
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2346502
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2346503
    iget-object v0, p0, LX/GNl;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2346504
    iget-object v0, p0, LX/GNl;->f:LX/GNm;

    iget-object v0, v0, LX/GNm;->d:LX/ADW;

    const-string v1, "payments_fetch_payment_option_successful"

    iget-object v2, p0, LX/GNl;->b:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-virtual {v0, v1, v2}, LX/ADW;->a(Ljava/lang/String;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V

    .line 2346505
    if-eqz p1, :cond_0

    .line 2346506
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2346507
    if-nez v0, :cond_1

    .line 2346508
    :cond_0
    iget-object v0, p0, LX/GNl;->f:LX/GNm;

    iget-object v1, p0, LX/GNl;->b:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    iget-object v2, p0, LX/GNl;->c:Lcom/facebook/common/locale/Country;

    sget-object v3, LX/GNm;->a:LX/0Px;

    iget-object v4, p0, LX/GNl;->d:Ljava/lang/String;

    iget v5, p0, LX/GNl;->e:I

    .line 2346509
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2346510
    const-string p1, "payments_flow_context_key"

    invoke-virtual {p0, p1, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2346511
    const-string p1, "country"

    invoke-virtual {p0, p1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2346512
    const-string p1, "payment_options"

    invoke-virtual {p0, p1, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2346513
    const-string p1, "request_code"

    invoke-virtual {p0, p1, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2346514
    new-instance p1, Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;

    invoke-direct {p1}, Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;-><init>()V

    .line 2346515
    invoke-virtual {p1, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2346516
    move-object p0, p1

    .line 2346517
    iget-object p1, v0, LX/GNm;->b:LX/0gc;

    invoke-virtual {p0, p1, v4}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2346518
    :goto_0
    return-void

    .line 2346519
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2346520
    check-cast v0, Lcom/facebook/adspayments/graphql/FetchPaymentOptionsGraphQLModels$FetchPaymentOptionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adspayments/graphql/FetchPaymentOptionsGraphQLModels$FetchPaymentOptionsQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x0

    const-class v3, Lcom/facebook/graphql/enums/GraphQLMobilePaymentOption;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->g(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2346521
    if-eqz v0, :cond_2

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    .line 2346522
    :goto_1
    iget-object v0, p0, LX/GNl;->f:LX/GNm;

    iget-object v1, p0, LX/GNl;->b:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    iget-object v2, p0, LX/GNl;->c:Lcom/facebook/common/locale/Country;

    iget-object v4, p0, LX/GNl;->d:Ljava/lang/String;

    iget v5, p0, LX/GNl;->e:I

    .line 2346523
    new-instance p0, Landroid/os/Bundle;

    invoke-direct {p0}, Landroid/os/Bundle;-><init>()V

    .line 2346524
    const-string p1, "payments_flow_context_key"

    invoke-virtual {p0, p1, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2346525
    const-string p1, "country"

    invoke-virtual {p0, p1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2346526
    const-string p1, "payment_options"

    invoke-virtual {p0, p1, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2346527
    const-string p1, "request_code"

    invoke-virtual {p0, p1, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2346528
    new-instance p1, Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;

    invoke-direct {p1}, Lcom/facebook/adspayments/activity/SelectPaymentMethodDialogFragment;-><init>()V

    .line 2346529
    invoke-virtual {p1, p0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2346530
    move-object p0, p1

    .line 2346531
    iget-object p1, v0, LX/GNm;->b:LX/0gc;

    invoke-virtual {p0, p1, v4}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2346532
    goto :goto_0

    .line 2346533
    :cond_2
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v3, v0

    .line 2346534
    goto :goto_1
.end method
