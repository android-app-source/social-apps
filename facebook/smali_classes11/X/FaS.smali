.class public LX/FaS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FaB;


# instance fields
.field public final a:LX/CwT;

.field public final b:LX/13B;

.field private final c:LX/FaY;

.field public final d:LX/0ad;

.field private final e:LX/0SG;

.field public f:Z


# direct methods
.method public constructor <init>(LX/CwT;LX/13B;LX/FaY;LX/0ad;LX/0SG;)V
    .locals 1
    .param p1    # LX/CwT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2258811
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2258812
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2258813
    iput-object p1, p0, LX/FaS;->a:LX/CwT;

    .line 2258814
    iput-object p2, p0, LX/FaS;->b:LX/13B;

    .line 2258815
    iput-object p3, p0, LX/FaS;->c:LX/FaY;

    .line 2258816
    iput-object p4, p0, LX/FaS;->d:LX/0ad;

    .line 2258817
    iput-object p5, p0, LX/FaS;->e:LX/0SG;

    .line 2258818
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/FaS;->f:Z

    .line 2258819
    return-void
.end method

.method public static a(LX/0Px;I)Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;I)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 2258810
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    invoke-virtual {p0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0P1;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 2258820
    iget-object v0, p0, LX/FaS;->b:LX/13B;

    iget-object v1, p0, LX/FaS;->a:LX/CwT;

    invoke-virtual {v0, v1, p1}, LX/13B;->a(LX/CwT;LX/0P1;)V

    .line 2258821
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/FaS;->f:Z

    .line 2258822
    return-void
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 2258805
    const/4 v0, 0x0

    .line 2258806
    iget-object v1, p0, LX/FaS;->d:LX/0ad;

    sget-short v2, LX/100;->ae:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/FaS;->d:LX/0ad;

    sget-short v2, LX/100;->af:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    move v0, v0

    .line 2258807
    if-eqz v0, :cond_2

    iget-object v0, p0, LX/FaS;->c:LX/FaY;

    sget-object v1, LX/FaF;->LEARNING_NUX:LX/FaF;

    invoke-virtual {v0, v1}, LX/FaY;->a(LX/FaF;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, LX/FaS;->a:LX/CwT;

    .line 2258808
    iget-boolean v1, v0, LX/CwT;->e:Z

    move v0, v1

    .line 2258809
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2258799
    iget-object v0, p0, LX/FaS;->a:LX/CwT;

    .line 2258800
    iget-boolean v1, v0, LX/CwT;->e:Z

    move v0, v1

    .line 2258801
    if-eqz v0, :cond_0

    .line 2258802
    iget-object v0, p0, LX/FaS;->a:LX/CwT;

    const/4 v1, 0x0

    .line 2258803
    iput-boolean v1, v0, LX/CwT;->e:Z

    .line 2258804
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2258795
    iget-boolean v0, p0, LX/FaS;->f:Z

    if-eqz v0, :cond_0

    .line 2258796
    iget-object v0, p0, LX/FaS;->b:LX/13B;

    iget-object v1, p0, LX/FaS;->a:LX/CwT;

    const-string v2, "dismissed_by_user"

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/13B;->b(LX/CwT;LX/0P1;)V

    .line 2258797
    iput-boolean v4, p0, LX/FaS;->f:Z

    .line 2258798
    :cond_0
    return-void
.end method
