.class public LX/FBc;
.super LX/398;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile f:LX/FBc;


# instance fields
.field public final c:LX/14x;

.field public final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2209580
    const-class v0, LX/FBc;

    sput-object v0, LX/FBc;->b:Ljava/lang/Class;

    .line 2209581
    sget-object v0, LX/0ax;->ae:Ljava/lang/String;

    sput-object v0, LX/FBc;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/14x;LX/0Or;LX/03V;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/14x;",
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2209582
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2209583
    iput-object p1, p0, LX/FBc;->c:LX/14x;

    .line 2209584
    iput-object p2, p0, LX/FBc;->d:LX/0Or;

    .line 2209585
    iput-object p3, p0, LX/FBc;->e:LX/03V;

    .line 2209586
    sget-object v0, LX/0ax;->ae:Ljava/lang/String;

    sget-object v1, LX/3RH;->g:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2209587
    sget-object v0, LX/0ax;->af:Ljava/lang/String;

    const-string v1, "fb-messenger-lite://threads"

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2209588
    sget-object v0, LX/0ax;->aj:Ljava/lang/String;

    sget-object v1, LX/3RH;->h:Ljava/lang/String;

    sget-object v2, LX/3RH;->g:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, LX/FBc;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2209589
    sget-object v0, LX/0ax;->ak:Ljava/lang/String;

    sget-object v1, LX/3RH;->h:Ljava/lang/String;

    sget-object v2, LX/3RH;->g:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, LX/FBc;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2209590
    sget-object v0, LX/0ax;->an:Ljava/lang/String;

    sget-object v1, LX/3RH;->g:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2209591
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/0ax;->al:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{user}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/3RH;->B:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "<user>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2209592
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/0ax;->ah:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{user}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/3RH;->B:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "<user>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2209593
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/0ax;->ap:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{thread_id}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/3RH;->y:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "<thread_id>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2209594
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/0ax;->an:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{groupthreadfbid}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/FBb;

    invoke-direct {v1, p0}, LX/FBb;-><init>(LX/FBc;)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2209595
    return-void
.end method

.method public static a(LX/0QB;)LX/FBc;
    .locals 6

    .prologue
    .line 2209596
    sget-object v0, LX/FBc;->f:LX/FBc;

    if-nez v0, :cond_1

    .line 2209597
    const-class v1, LX/FBc;

    monitor-enter v1

    .line 2209598
    :try_start_0
    sget-object v0, LX/FBc;->f:LX/FBc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2209599
    if-eqz v2, :cond_0

    .line 2209600
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2209601
    new-instance v5, LX/FBc;

    invoke-static {v0}, LX/14x;->a(LX/0QB;)LX/14x;

    move-result-object v3

    check-cast v3, LX/14x;

    const/16 v4, 0xce8

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {v5, v3, p0, v4}, LX/FBc;-><init>(LX/14x;LX/0Or;LX/03V;)V

    .line 2209602
    move-object v0, v5

    .line 2209603
    sput-object v0, LX/FBc;->f:LX/FBc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2209604
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2209605
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2209606
    :cond_1
    sget-object v0, LX/FBc;->f:LX/FBc;

    return-object v0

    .line 2209607
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2209608
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2209609
    iget-object v0, p0, LX/FBc;->c:LX/14x;

    const-string v1, "2.7.0"

    invoke-virtual {v0, v1}, LX/14x;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2209610
    :goto_0
    return-object p1

    :cond_0
    move-object p1, p2

    goto :goto_0
.end method
