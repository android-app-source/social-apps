.class public final LX/H5c;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:LX/DqC;

.field public final synthetic b:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic c:LX/1Pn;

.field public final synthetic d:LX/H5d;


# direct methods
.method public constructor <init>(LX/H5d;LX/DqC;Lcom/facebook/graphql/model/GraphQLStory;LX/1Pn;)V
    .locals 0

    .prologue
    .line 2424617
    iput-object p1, p0, LX/H5c;->d:LX/H5d;

    iput-object p2, p0, LX/H5c;->a:LX/DqC;

    iput-object p3, p0, LX/H5c;->b:Lcom/facebook/graphql/model/GraphQLStory;

    iput-object p4, p0, LX/H5c;->c:LX/1Pn;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 2424618
    iget-object v0, p0, LX/H5c;->d:LX/H5d;

    iget-object v0, v0, LX/H5d;->b:LX/2jO;

    invoke-virtual {v0}, LX/2jO;->a()V

    .line 2424619
    iget-object v0, p0, LX/H5c;->d:LX/H5d;

    iget-object v1, v0, LX/H5d;->a:LX/33W;

    iget-object v0, p0, LX/H5c;->a:LX/DqC;

    .line 2424620
    iget-object v2, v0, LX/DqC;->d:Ljava/lang/String;

    move-object v2, v2

    .line 2424621
    const-string v3, "long_press"

    iget-object v4, p0, LX/H5c;->b:Lcom/facebook/graphql/model/GraphQLStory;

    iget-object v0, p0, LX/H5c;->c:LX/1Pn;

    check-cast v0, LX/2kk;

    iget-object v5, p0, LX/H5c;->b:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0, v5}, LX/2kk;->e_(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v2, v3, v4, v0}, LX/33W;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;I)V

    .line 2424622
    iget-object v0, p0, LX/H5c;->c:LX/1Pn;

    check-cast v0, LX/1Pq;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 2424623
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 2424624
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 2424625
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2424626
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 2424627
    iget-object v0, p0, LX/H5c;->c:LX/1Pn;

    invoke-interface {v0}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a00d6

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2424628
    return-void
.end method
