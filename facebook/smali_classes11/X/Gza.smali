.class public LX/Gza;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2412120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2412121
    return-void
.end method

.method public static a(Landroid/content/Context;LX/0Px;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Lcom/facebook/payments/p2p/model/PaymentCard;",
            ">;)",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2412122
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2412123
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/p2p/model/PaymentCard;

    .line 2412124
    invoke-static {p0, v0}, LX/Dud;->a(Landroid/content/Context;Lcom/facebook/payments/p2p/model/PaymentCard;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2412125
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2412126
    :cond_0
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;IIILandroid/content/DialogInterface$OnClickListener;)LX/2EJ;
    .locals 3
    .param p1    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p2    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .param p3    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 2412127
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v0, v1, v2, p4}, LX/Gza;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/2EJ;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;LX/0Px;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/GzZ;)LX/2EJ;
    .locals 5
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/GzZ;",
            ")",
            "LX/2EJ;"
        }
    .end annotation

    .prologue
    .line 2412128
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 2412129
    invoke-virtual {v0, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2412130
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2412131
    invoke-virtual {v0, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2412132
    :cond_0
    new-instance v1, LX/GzX;

    invoke-direct {v1, p1, p5, p2}, LX/GzX;-><init>(LX/0Px;LX/GzZ;Ljava/lang/String;)V

    .line 2412133
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2412134
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    .line 2412135
    new-instance v3, LX/31Y;

    invoke-direct {v3, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    .line 2412136
    new-instance v4, LX/Gzb;

    invoke-direct {v4, p0}, LX/Gzb;-><init>(Landroid/content/Context;)V

    .line 2412137
    iget-object p0, v4, LX/Gzb;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, p3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2412138
    invoke-static {p4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2412139
    iget-object p0, v4, LX/Gzb;->b:Lcom/facebook/widget/text/BetterTextView;

    const/16 p3, 0x8

    invoke-virtual {p0, p3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2412140
    :goto_0
    move-object v4, v4

    .line 2412141
    invoke-virtual {v3, v4}, LX/0ju;->a(Landroid/view/View;)LX/0ju;

    move-result-object v3

    invoke-virtual {v0, v2}, LX/0Px;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    invoke-virtual {v3, v0, v1}, LX/0ju;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    move-result-object v0

    return-object v0

    .line 2412142
    :cond_1
    iget-object p0, v4, LX/Gzb;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, p4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/2EJ;
    .locals 2

    .prologue
    .line 2412143
    const v0, 0x7f080016

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/GzY;

    invoke-direct {v1}, LX/GzY;-><init>()V

    invoke-static {p0, p1, p2, v0, v1}, LX/Gza;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/2EJ;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/2EJ;
    .locals 1

    .prologue
    .line 2412144
    new-instance v0, LX/31Y;

    invoke-direct {v0, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, LX/0ju;->c(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;Ljava/lang/String;Landroid/content/DialogInterface$OnClickListener;)LX/2EJ;
    .locals 1

    .prologue
    .line 2412145
    new-instance v0, LX/31Y;

    invoke-direct {v0, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, p5, p6}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2412146
    invoke-static {p1}, LX/1dx;->b(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2412147
    invoke-static {p0}, LX/6up;->a(Landroid/content/Context;)V

    .line 2412148
    :goto_0
    return-void

    .line 2412149
    :cond_0
    const-class v0, LX/2Oo;

    invoke-static {p1, v0}, LX/23D;->a(Ljava/lang/Throwable;Ljava/lang/Class;)Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/2Oo;

    .line 2412150
    if-eqz v0, :cond_1

    invoke-virtual {v0}, LX/2Oo;->b()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, LX/2Oo;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2412151
    invoke-virtual {v0}, LX/2Oo;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, LX/2Oo;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, v0}, LX/Gza;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    goto :goto_0

    .line 2412152
    :cond_1
    invoke-static {p0, p2, p3}, LX/Gza;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    goto :goto_0
.end method
