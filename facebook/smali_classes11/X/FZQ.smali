.class public final LX/FZQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:Landroid/preference/Preference;

.field public final synthetic b:Lcom/facebook/search/debug/SearchDebugActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/search/debug/SearchDebugActivity;Landroid/preference/Preference;)V
    .locals 0

    .prologue
    .line 2257045
    iput-object p1, p0, LX/FZQ;->b:Lcom/facebook/search/debug/SearchDebugActivity;

    iput-object p2, p0, LX/FZQ;->a:Landroid/preference/Preference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2257046
    iget-object v0, p0, LX/FZQ;->b:Lcom/facebook/search/debug/SearchDebugActivity;

    invoke-static {v0}, Lcom/facebook/search/debug/SearchDebugActivity;->m(Lcom/facebook/search/debug/SearchDebugActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2257047
    :goto_0
    iget-object v3, p0, LX/FZQ;->b:Lcom/facebook/search/debug/SearchDebugActivity;

    iget-object v3, v3, Lcom/facebook/search/debug/SearchDebugActivity;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v4, LX/7CP;->g:LX/0Tn;

    invoke-interface {v3, v4, v0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v3

    invoke-interface {v3}, LX/0hN;->commit()V

    .line 2257048
    iget-object v3, p0, LX/FZQ;->b:Lcom/facebook/search/debug/SearchDebugActivity;

    invoke-virtual {v3}, Lcom/facebook/search/debug/SearchDebugActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "Updated Tutorial Nux Debug Mode.  Don\'t forget to kill the app!"

    invoke-static {v3, v4, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 2257049
    iget-object v2, p0, LX/FZQ;->a:Landroid/preference/Preference;

    invoke-static {v2, v0}, Lcom/facebook/search/debug/SearchDebugActivity;->b(Landroid/preference/Preference;Z)V

    .line 2257050
    return v1

    :cond_0
    move v0, v2

    .line 2257051
    goto :goto_0
.end method
