.class public final LX/GcN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GcK;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2371813
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/auth/credentials/DBLFacebookCredentials;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 2371793
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2371794
    const-string v1, "pin"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2371795
    const-string v1, "nonce_to_keep"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2371796
    return-object v0
.end method

.method public final a(LX/Gbb;II)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 2371799
    new-instance v0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;

    invoke-direct {v0}, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;-><init>()V

    .line 2371800
    iput-object p1, v0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->a:LX/Gbb;

    .line 2371801
    if-nez p3, :cond_0

    const p3, 0x7f08348b

    .line 2371802
    :cond_0
    iput p3, v0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->e:I

    .line 2371803
    iget-object p0, v0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->i:Landroid/widget/TextView;

    if-eqz p0, :cond_1

    .line 2371804
    iget-object p0, v0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->i:Landroid/widget/TextView;

    iget p1, v0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->e:I

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 2371805
    :cond_1
    if-nez p2, :cond_2

    const p2, 0x7f083479

    .line 2371806
    :cond_2
    iput p2, v0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->d:I

    .line 2371807
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object p0, p0

    .line 2371808
    if-eqz p0, :cond_3

    .line 2371809
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object p0, p0

    .line 2371810
    const p1, 0x7f0d0541

    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p0

    check-cast p0, Landroid/widget/TextView;

    .line 2371811
    iget p1, v0, Lcom/facebook/devicebasedlogin/ui/DBLChangePasscodeAfterIncorrectPasscodeFragment;->d:I

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 2371812
    :cond_3
    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2371798
    const-string v0, "set_nonce"

    return-object v0
.end method

.method public final b()LX/GcT;
    .locals 1

    .prologue
    .line 2371797
    sget-object v0, LX/GcT;->CHANGE_PASSCODE_FROM_LOGIN_FLOW:LX/GcT;

    return-object v0
.end method
