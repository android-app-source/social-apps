.class public final LX/H78;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:J

.field public c:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:Z

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Z

.field public k:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2428103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;)LX/H78;
    .locals 4

    .prologue
    .line 2428104
    new-instance v0, LX/H78;

    invoke-direct {v0}, LX/H78;-><init>()V

    .line 2428105
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->t()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H78;->a:Ljava/lang/String;

    .line 2428106
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->u()J

    move-result-wide v2

    iput-wide v2, v0, LX/H78;->b:J

    .line 2428107
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->j()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v1

    iput-object v1, v0, LX/H78;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    .line 2428108
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->v()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H78;->d:Ljava/lang/String;

    .line 2428109
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->w()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H78;->e:Ljava/lang/String;

    .line 2428110
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->me_()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H78;->f:Ljava/lang/String;

    .line 2428111
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->x()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H78;->g:Ljava/lang/String;

    .line 2428112
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->y()Z

    move-result v1

    iput-boolean v1, v0, LX/H78;->h:Z

    .line 2428113
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->z()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H78;->i:Ljava/lang/String;

    .line 2428114
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->A()Z

    move-result v1

    iput-boolean v1, v0, LX/H78;->j:Z

    .line 2428115
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->k()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    move-result-object v1

    iput-object v1, v0, LX/H78;->k:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    .line 2428116
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->B()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, LX/H78;->l:Ljava/lang/String;

    .line 2428117
    invoke-virtual {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->l()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    move-result-object v1

    iput-object v1, v0, LX/H78;->m:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    .line 2428118
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;
    .locals 15

    .prologue
    .line 2428119
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2428120
    iget-object v1, p0, LX/H78;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2428121
    iget-object v2, p0, LX/H78;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2428122
    iget-object v2, p0, LX/H78;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2428123
    iget-object v2, p0, LX/H78;->e:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2428124
    iget-object v2, p0, LX/H78;->f:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    .line 2428125
    iget-object v2, p0, LX/H78;->g:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 2428126
    iget-object v2, p0, LX/H78;->i:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 2428127
    iget-object v2, p0, LX/H78;->k:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 2428128
    iget-object v2, p0, LX/H78;->l:Ljava/lang/String;

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 2428129
    iget-object v2, p0, LX/H78;->m:Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;

    invoke-static {v0, v2}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v14

    .line 2428130
    const/16 v2, 0xd

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2428131
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2428132
    const/4 v1, 0x1

    iget-wide v2, p0, LX/H78;->b:J

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2428133
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2428134
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 2428135
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 2428136
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 2428137
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v10}, LX/186;->b(II)V

    .line 2428138
    const/4 v1, 0x7

    iget-boolean v2, p0, LX/H78;->h:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 2428139
    const/16 v1, 0x8

    invoke-virtual {v0, v1, v11}, LX/186;->b(II)V

    .line 2428140
    const/16 v1, 0x9

    iget-boolean v2, p0, LX/H78;->j:Z

    invoke-virtual {v0, v1, v2}, LX/186;->a(IZ)V

    .line 2428141
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v12}, LX/186;->b(II)V

    .line 2428142
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v13}, LX/186;->b(II)V

    .line 2428143
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v14}, LX/186;->b(II)V

    .line 2428144
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 2428145
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2428146
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 2428147
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2428148
    new-instance v0, LX/15i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2428149
    new-instance v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    invoke-direct {v1, v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;-><init>(LX/15i;)V

    .line 2428150
    return-object v1
.end method
