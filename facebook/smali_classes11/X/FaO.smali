.class public final LX/FaO;
.super LX/1ci;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1ci",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/1bf;

.field public final synthetic b:Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;


# direct methods
.method public constructor <init>(Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;LX/1bf;)V
    .locals 0

    .prologue
    .line 2258737
    iput-object p1, p0, LX/FaO;->b:Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;

    iput-object p2, p0, LX/FaO;->a:LX/1bf;

    invoke-direct {p0}, LX/1ci;-><init>()V

    return-void
.end method


# virtual methods
.method public final e(LX/1ca;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2258738
    invoke-interface {p1}, LX/1ca;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2258739
    :goto_0
    return-void

    .line 2258740
    :cond_0
    iget-object v0, p0, LX/FaO;->b:Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;

    iget-object v1, p0, LX/FaO;->a:LX/1bf;

    .line 2258741
    iget-object v2, v0, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;->d:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2258742
    iget-object v2, v0, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;->d:Ljava/util/Map;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2258743
    :goto_1
    goto :goto_0

    .line 2258744
    :cond_1
    iget-object v2, v0, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;->c:LX/03V;

    const-string v3, "SearchAwareness"

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "Tried to mark fetch complete for imageRequest that wasn\'t started: "

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, v3, p0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final f(LX/1ca;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2258745
    iget-object v0, p0, LX/FaO;->b:Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;

    iget-object v0, v0, Lcom/facebook/search/quickpromotion/SearchAwarenessImageFetcher;->c:LX/03V;

    const-string v1, "SearchAwareness"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Image prefetch failure: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, LX/1ca;->e()Ljava/lang/Throwable;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2258746
    return-void
.end method
