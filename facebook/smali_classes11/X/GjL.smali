.class public LX/GjL;
.super Landroid/widget/ViewSwitcher;
.source ""

# interfaces
.implements LX/Gik;


# instance fields
.field public a:LX/Gj1;

.field public b:Landroid/view/View;

.field public c:Lcom/facebook/greetingcards/create/MomentView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2387399
    invoke-direct {p0, p1}, Landroid/widget/ViewSwitcher;-><init>(Landroid/content/Context;)V

    .line 2387400
    const/4 v2, 0x0

    .line 2387401
    invoke-virtual {p0, v2}, LX/GjL;->setMeasureAllChildren(Z)V

    .line 2387402
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2387403
    const v1, 0x7f030889

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 2387404
    invoke-virtual {p0, v2}, LX/GjL;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/GjL;->b:Landroid/view/View;

    .line 2387405
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/GjL;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/create/MomentView;

    iput-object v0, p0, LX/GjL;->c:Lcom/facebook/greetingcards/create/MomentView;

    .line 2387406
    iget-object v0, p0, LX/GjL;->b:Landroid/view/View;

    new-instance v1, LX/GjK;

    invoke-direct {v1, p0}, LX/GjK;-><init>(LX/GjL;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2387407
    return-void
.end method

.method public static c(LX/GjL;)V
    .locals 2

    .prologue
    .line 2387408
    invoke-virtual {p0}, LX/GjL;->getDisplayedChild()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 2387409
    invoke-virtual {p0}, LX/GjL;->showNext()V

    .line 2387410
    iget-object v0, p0, LX/GjL;->a:LX/Gj1;

    invoke-virtual {v0}, LX/Gj1;->c()V

    .line 2387411
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/greetingcards/model/CardPhoto;)V
    .locals 1

    .prologue
    .line 2387412
    invoke-static {p0}, LX/GjL;->c(LX/GjL;)V

    .line 2387413
    iget-object v0, p0, LX/GjL;->c:Lcom/facebook/greetingcards/create/MomentView;

    invoke-virtual {v0, p1}, Lcom/facebook/greetingcards/create/MomentView;->a(Lcom/facebook/greetingcards/model/CardPhoto;)V

    .line 2387414
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2387415
    invoke-virtual {p0}, LX/GjL;->getDisplayedChild()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2387416
    invoke-virtual {p0}, LX/GjL;->getDisplayedChild()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 2387417
    invoke-virtual {p0}, LX/GjL;->showPrevious()V

    .line 2387418
    :cond_0
    return-void
.end method
