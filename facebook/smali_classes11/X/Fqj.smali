.class public LX/Fqj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/Fsr;


# direct methods
.method public constructor <init>(LX/Fsr;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2294423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2294424
    iput-object p1, p0, LX/Fqj;->a:LX/Fsr;

    .line 2294425
    return-void
.end method

.method public static b(LX/0QB;)LX/Fqj;
    .locals 2

    .prologue
    .line 2294426
    new-instance v1, LX/Fqj;

    invoke-static {p0}, LX/Fsr;->a(LX/0QB;)LX/Fsr;

    move-result-object v0

    check-cast v0, LX/Fsr;

    invoke-direct {v1, v0}, LX/Fqj;-><init>(LX/Fsr;)V

    .line 2294427
    return-object v1
.end method


# virtual methods
.method public final a(ILandroid/view/View;Z)V
    .locals 4
    .param p1    # I
        .annotation build Lcom/facebook/timeline/widget/actionbar/PersonActionBarItems;
        .end annotation
    .end param

    .prologue
    .line 2294428
    iget-object v0, p0, LX/Fqj;->a:LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->q()LX/FqT;

    move-result-object v0

    .line 2294429
    invoke-static {p1}, LX/BS2;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 2294430
    packed-switch p1, :pswitch_data_0

    .line 2294431
    :pswitch_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown item type for TimelineActionBar.getEvent "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2294432
    :pswitch_1
    invoke-interface {v0, v1}, LX/FqT;->a(Ljava/lang/String;)V

    .line 2294433
    :goto_0
    return-void

    .line 2294434
    :pswitch_2
    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-interface {v0, v1, v2}, LX/FqT;->a(Ljava/lang/String;Landroid/view/View;)V

    goto :goto_0

    .line 2294435
    :pswitch_3
    const-string v2, "timeline_message_button"

    invoke-interface {v0, v1, v2}, LX/FqT;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2294436
    :pswitch_4
    const-string v2, "timeline_manage_button"

    invoke-virtual {p2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, LX/FqT;->a(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V

    goto :goto_0

    .line 2294437
    :pswitch_5
    if-eqz p3, :cond_0

    .line 2294438
    invoke-interface {v0}, LX/FqT;->a()V

    goto :goto_0

    .line 2294439
    :cond_0
    invoke-interface {v0, v1}, LX/FqT;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 2294440
    :pswitch_6
    invoke-interface {v0, v1}, LX/FqT;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 2294441
    :pswitch_7
    const-string v2, "tap_activity_log_action_item"

    invoke-interface {v0, v1, v2}, LX/FqT;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2294442
    :pswitch_8
    invoke-interface {v0, v1}, LX/FqT;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 2294443
    :pswitch_9
    invoke-interface {v0, v1}, LX/FqT;->g(Ljava/lang/String;)V

    goto :goto_0

    .line 2294444
    :pswitch_a
    invoke-interface {v0, v1}, LX/FqT;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 2294445
    :pswitch_b
    invoke-interface {v0, v1}, LX/FqT;->f(Ljava/lang/String;)V

    goto :goto_0

    .line 2294446
    :pswitch_c
    invoke-interface {v0, v1}, LX/FqT;->h(Ljava/lang/String;)V

    goto :goto_0

    .line 2294447
    :pswitch_d
    invoke-interface {v0, v1}, LX/FqT;->j(Ljava/lang/String;)V

    goto :goto_0

    .line 2294448
    :pswitch_e
    invoke-interface {v0, v1}, LX/FqT;->k(Ljava/lang/String;)V

    goto :goto_0

    .line 2294449
    :pswitch_f
    invoke-interface {v0, v1}, LX/FqT;->l(Ljava/lang/String;)V

    goto :goto_0

    .line 2294450
    :pswitch_10
    invoke-interface {v0, v1}, LX/FqT;->i(Ljava/lang/String;)V

    goto :goto_0

    .line 2294451
    :pswitch_11
    invoke-interface {v0, v1}, LX/FqT;->m(Ljava/lang/String;)V

    goto :goto_0

    .line 2294452
    :pswitch_12
    invoke-interface {v0, v1}, LX/FqT;->n(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_8
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_10
        :pswitch_5
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_7
        :pswitch_9
        :pswitch_11
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_12
    .end packed-switch
.end method
