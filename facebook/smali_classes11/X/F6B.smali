.class public final LX/F6B;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/composer/publish/common/PublishPostParams;

.field public final synthetic b:LX/F6D;


# direct methods
.method public constructor <init>(LX/F6D;Lcom/facebook/composer/publish/common/PublishPostParams;)V
    .locals 0

    .prologue
    .line 2200189
    iput-object p1, p0, LX/F6B;->b:LX/F6D;

    iput-object p2, p0, LX/F6B;->a:Lcom/facebook/composer/publish/common/PublishPostParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2200190
    iget-object v0, p0, LX/F6B;->b:LX/F6D;

    sget-object v1, LX/F6C;->ERROR:LX/F6C;

    iget-object v2, p0, LX/F6B;->a:Lcom/facebook/composer/publish/common/PublishPostParams;

    iget-wide v2, v2, Lcom/facebook/composer/publish/common/PublishPostParams;->targetId:J

    invoke-static {v0, v1, v2, v3}, LX/F6D;->a$redex0(LX/F6D;LX/F6C;J)V

    .line 2200191
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2200192
    iget-object v0, p0, LX/F6B;->b:LX/F6D;

    sget-object v1, LX/F6C;->CONFIRMED:LX/F6C;

    iget-object v2, p0, LX/F6B;->a:Lcom/facebook/composer/publish/common/PublishPostParams;

    iget-wide v2, v2, Lcom/facebook/composer/publish/common/PublishPostParams;->targetId:J

    invoke-static {v0, v1, v2, v3}, LX/F6D;->a$redex0(LX/F6D;LX/F6C;J)V

    .line 2200193
    return-void
.end method
