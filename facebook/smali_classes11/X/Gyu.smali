.class public final enum LX/Gyu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gyu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gyu;

.field public static final enum EVERYONE:LX/Gyu;

.field public static final enum FRIENDS:LX/Gyu;

.field public static final enum NONE:LX/Gyu;

.field public static final enum ONLY_ME:LX/Gyu;


# instance fields
.field private final nativeProtocolAudience:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2410654
    new-instance v0, LX/Gyu;

    const-string v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v3, v2}, LX/Gyu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Gyu;->NONE:LX/Gyu;

    .line 2410655
    new-instance v0, LX/Gyu;

    const-string v1, "ONLY_ME"

    const-string v2, "only_me"

    invoke-direct {v0, v1, v4, v2}, LX/Gyu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Gyu;->ONLY_ME:LX/Gyu;

    .line 2410656
    new-instance v0, LX/Gyu;

    const-string v1, "FRIENDS"

    const-string v2, "friends"

    invoke-direct {v0, v1, v5, v2}, LX/Gyu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Gyu;->FRIENDS:LX/Gyu;

    .line 2410657
    new-instance v0, LX/Gyu;

    const-string v1, "EVERYONE"

    const-string v2, "everyone"

    invoke-direct {v0, v1, v6, v2}, LX/Gyu;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/Gyu;->EVERYONE:LX/Gyu;

    .line 2410658
    const/4 v0, 0x4

    new-array v0, v0, [LX/Gyu;

    sget-object v1, LX/Gyu;->NONE:LX/Gyu;

    aput-object v1, v0, v3

    sget-object v1, LX/Gyu;->ONLY_ME:LX/Gyu;

    aput-object v1, v0, v4

    sget-object v1, LX/Gyu;->FRIENDS:LX/Gyu;

    aput-object v1, v0, v5

    sget-object v1, LX/Gyu;->EVERYONE:LX/Gyu;

    aput-object v1, v0, v6

    sput-object v0, LX/Gyu;->$VALUES:[LX/Gyu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2410651
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2410652
    iput-object p3, p0, LX/Gyu;->nativeProtocolAudience:Ljava/lang/String;

    .line 2410653
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gyu;
    .locals 1

    .prologue
    .line 2410659
    const-class v0, LX/Gyu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gyu;

    return-object v0
.end method

.method public static values()[LX/Gyu;
    .locals 1

    .prologue
    .line 2410649
    sget-object v0, LX/Gyu;->$VALUES:[LX/Gyu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gyu;

    return-object v0
.end method


# virtual methods
.method public final getNativeProtocolAudience()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2410650
    iget-object v0, p0, LX/Gyu;->nativeProtocolAudience:Ljava/lang/String;

    return-object v0
.end method
