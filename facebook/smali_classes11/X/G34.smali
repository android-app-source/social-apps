.class public LX/G34;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/BP0;

.field public b:LX/BQ1;

.field public c:LX/0gc;

.field public d:Landroid/view/View;

.field private e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2hf;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;


# direct methods
.method public constructor <init>(LX/BP0;LX/BQ1;LX/0gc;Landroid/view/View;LX/0Or;LX/0Or;)V
    .locals 0
    .param p1    # LX/BP0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/BP0;",
            "LX/BQ1;",
            "LX/0gc;",
            "Landroid/view/View;",
            "LX/0Or",
            "<",
            "LX/0iA;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2hf;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2314790
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2314791
    iput-object p1, p0, LX/G34;->a:LX/BP0;

    .line 2314792
    iput-object p2, p0, LX/G34;->b:LX/BQ1;

    .line 2314793
    iput-object p3, p0, LX/G34;->c:LX/0gc;

    .line 2314794
    iput-object p4, p0, LX/G34;->d:Landroid/view/View;

    .line 2314795
    iput-object p5, p0, LX/G34;->e:LX/0Or;

    .line 2314796
    iput-object p6, p0, LX/G34;->f:LX/0Or;

    .line 2314797
    return-void
.end method

.method public static a(LX/G34;Lcom/facebook/interstitial/manager/InterstitialTrigger;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2314798
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2314799
    iget-object v1, p0, LX/G34;->b:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->b()Lcom/facebook/timeline/profilevideo/playback/protocol/FetchProfileVideoGraphQLModels$ProfileVideoHeaderFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2314800
    const-string v1, "context_profile_has_profile_video"

    const-string v2, "1"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2314801
    :cond_0
    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;

    invoke-direct {v1, v0}, Lcom/facebook/interstitial/manager/InterstitialTriggerContext;-><init>(Ljava/util/Map;)V

    .line 2314802
    iget-object v0, p0, LX/G34;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0iA;

    new-instance v2, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-direct {v2, p1, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger;Lcom/facebook/interstitial/manager/InterstitialTriggerContext;)V

    const-class v1, LX/77s;

    invoke-virtual {v0, v2, v1}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/77s;

    .line 2314803
    if-nez v0, :cond_1

    .line 2314804
    const/4 v0, 0x0

    .line 2314805
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, LX/G34;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/13D;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final c()V
    .locals 2

    .prologue
    .line 2314806
    iget-object v0, p0, LX/G34;->g:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    if-eqz v0, :cond_0

    .line 2314807
    iget-object v0, p0, LX/G34;->c:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    iget-object v1, p0, LX/G34;->g:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    invoke-virtual {v0, v1}, LX/0hH;->a(Landroid/support/v4/app/Fragment;)LX/0hH;

    move-result-object v0

    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2314808
    const/4 v0, 0x0

    iput-object v0, p0, LX/G34;->g:Lcom/facebook/quickpromotion/ui/QuickPromotionFooterFragment;

    .line 2314809
    :cond_0
    iget-object v0, p0, LX/G34;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2314810
    return-void
.end method
