.class public final LX/Gud;
.super LX/BWM;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 2403633
    iput-object p1, p0, LX/Gud;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403634
    invoke-direct {p0, p2}, LX/BWM;-><init>(Landroid/os/Handler;)V

    .line 2403635
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/webview/FacebookWebView;LX/BWN;)V
    .locals 5

    .prologue
    .line 2403636
    iget-object v0, p0, LX/Gud;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403637
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v1

    .line 2403638
    if-nez v0, :cond_0

    .line 2403639
    :goto_0
    return-void

    .line 2403640
    :cond_0
    iget-object v0, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2403641
    const-string v1, "buttons"

    invoke-interface {p2, v0, v1}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2403642
    const-string v1, "true"

    .line 2403643
    iget-object v2, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v2, v2

    .line 2403644
    const-string v3, "hide_cancel_button"

    invoke-interface {p2, v2, v3}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 2403645
    iget-object v2, p0, LX/Gud;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v2, v2, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->G:LX/0lp;

    iget-object v3, p0, LX/Gud;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v3, v3, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    .line 2403646
    new-instance v4, Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;

    invoke-direct {v4, v2}, Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;-><init>(LX/0lp;)V

    .line 2403647
    iput-object v3, v4, Lcom/facebook/katana/activity/faceweb/dialog/FacewebActionSheetDialogFragment;->m:Lcom/facebook/webview/FacebookWebView;

    .line 2403648
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 2403649
    const-string p2, "action_sheet_buttons"

    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2403650
    const-string p2, "action_sheet_hide_cancel"

    invoke-virtual {p1, p2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    .line 2403651
    invoke-virtual {v4, p1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2403652
    move-object v0, v4

    .line 2403653
    iget-object v1, p0, LX/Gud;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403654
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v1, v2

    .line 2403655
    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto :goto_0
.end method
