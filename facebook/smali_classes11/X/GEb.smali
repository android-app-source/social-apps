.class public LX/GEb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GEW",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/GKy;

.field private final b:LX/GMM;


# direct methods
.method public constructor <init>(LX/GKy;LX/GMM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2331921
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2331922
    iput-object p1, p0, LX/GEb;->a:LX/GKy;

    .line 2331923
    iput-object p2, p0, LX/GEb;->b:LX/GMM;

    .line 2331924
    return-void
.end method

.method public static a(LX/0QB;)LX/GEb;
    .locals 7

    .prologue
    .line 2331925
    new-instance v2, LX/GEb;

    invoke-static {p0}, LX/GKy;->b(LX/0QB;)LX/GKy;

    move-result-object v0

    check-cast v0, LX/GKy;

    .line 2331926
    new-instance v5, LX/GMM;

    invoke-static {p0}, LX/GKy;->b(LX/0QB;)LX/GKy;

    move-result-object v1

    check-cast v1, LX/GKy;

    const/16 v3, 0xbd2

    invoke-static {p0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v3

    check-cast v3, LX/GG6;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-direct {v5, v1, v6, v3, v4}, LX/GMM;-><init>(LX/GKy;LX/0Or;LX/GG6;Landroid/content/res/Resources;)V

    .line 2331927
    move-object v1, v5

    .line 2331928
    check-cast v1, LX/GMM;

    invoke-direct {v2, v0, v1}, LX/GEb;-><init>(LX/GKy;LX/GMM;)V

    .line 2331929
    move-object v0, v2

    .line 2331930
    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2331931
    const v0, 0x7f03007a

    return v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2331932
    invoke-static {p1}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2331933
    :cond_0
    :goto_0
    return v1

    .line 2331934
    :cond_1
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v3

    .line 2331935
    sget-object v0, LX/GGB;->CREATING:LX/GGB;

    if-eq v3, v0, :cond_2

    sget-object v0, LX/GGB;->EXTENDABLE:LX/GGB;

    if-eq v3, v0, :cond_2

    sget-object v0, LX/GGB;->ACTIVE:LX/GGB;

    if-eq v3, v0, :cond_2

    sget-object v0, LX/GGB;->PENDING:LX/GGB;

    if-eq v3, v0, :cond_2

    sget-object v0, LX/GGB;->PAUSED:LX/GGB;

    if-ne v3, v0, :cond_3

    :cond_2
    move v0, v2

    .line 2331936
    :goto_1
    sget-object v4, LX/GGB;->FINISHED:LX/GGB;

    if-ne v3, v4, :cond_4

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v3

    sget-object v4, LX/8wL;->BOOST_POST:LX/8wL;

    if-ne v3, v4, :cond_4

    move v3, v2

    :goto_2
    or-int/2addr v0, v3

    .line 2331937
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GEb;->a:LX/GKy;

    invoke-virtual {v0}, LX/GKy;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    .line 2331938
    goto :goto_0

    :cond_3
    move v0, v1

    .line 2331939
    goto :goto_1

    :cond_4
    move v3, v1

    .line 2331940
    goto :goto_2
.end method

.method public final b()LX/GHg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesViewController",
            "<",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;",
            "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2331941
    iget-object v0, p0, LX/GEb;->b:LX/GMM;

    return-object v0
.end method

.method public final c()LX/8wK;
    .locals 1

    .prologue
    .line 2331942
    sget-object v0, LX/8wK;->RESULTS:LX/8wK;

    return-object v0
.end method
