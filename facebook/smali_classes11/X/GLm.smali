.class public final LX/GLm;
.super LX/GFq;
.source ""


# instance fields
.field public final synthetic a:LX/GLp;


# direct methods
.method public constructor <init>(LX/GLp;)V
    .locals 0

    .prologue
    .line 2343633
    iput-object p1, p0, LX/GLm;->a:LX/GLp;

    invoke-direct {p0}, LX/GFq;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 2343621
    iget-object v0, p0, LX/GLm;->a:LX/GLp;

    .line 2343622
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2343623
    sget-object v1, LX/GG8;->SERVER_VALIDATION_ERROR:LX/GG8;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/GCE;->a(LX/GG8;Z)V

    .line 2343624
    iget-object v0, p0, LX/GLm;->a:LX/GLp;

    iget-object v0, v0, LX/GLp;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2343625
    instance-of v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v1, :cond_1

    .line 2343626
    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2343627
    :goto_0
    move-object v0, v0

    .line 2343628
    if-eqz v0, :cond_0

    .line 2343629
    iget-object v1, p0, LX/GLm;->a:LX/GLp;

    iget-object v1, v1, LX/GLp;->b:LX/GEU;

    iget-object v2, p0, LX/GLm;->a:LX/GLp;

    iget-object v2, v2, LX/GLp;->d:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2343630
    const/4 p0, 0x0

    const/4 p1, 0x1

    invoke-virtual {v1, v0, v2, p0, p1}, LX/GEU;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;IZ)V

    .line 2343631
    :goto_1
    return-void

    .line 2343632
    :cond_0
    iget-object v0, p0, LX/GLm;->a:LX/GLp;

    iget-object v0, v0, LX/GLp;->f:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "data cannot be null, please add validation support to this data model"

    invoke-virtual {v0, v1, v2}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
