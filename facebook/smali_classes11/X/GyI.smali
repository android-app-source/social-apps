.class public final LX/GyI;
.super LX/BcO;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BcO",
        "<",
        "LX/GyJ;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public b:LX/2kW;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/controller/connectioncontroller/common/ConnectionController",
            "<",
            "Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;",
            "TPlaceNode;>;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/GyJ;


# direct methods
.method public constructor <init>(LX/GyJ;)V
    .locals 1

    .prologue
    .line 2409816
    iput-object p1, p0, LX/GyI;->c:LX/GyJ;

    .line 2409817
    move-object v0, p1

    .line 2409818
    invoke-direct {p0, v0}, LX/BcO;-><init>(LX/BcS;)V

    .line 2409819
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2409820
    if-ne p0, p1, :cond_1

    .line 2409821
    :cond_0
    :goto_0
    return v0

    .line 2409822
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2409823
    goto :goto_0

    .line 2409824
    :cond_3
    check-cast p1, LX/GyI;

    .line 2409825
    iget-object v2, p0, LX/GyI;->b:LX/2kW;

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/GyI;->b:LX/2kW;

    iget-object v3, p1, LX/GyI;->b:LX/2kW;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2409826
    goto :goto_0

    .line 2409827
    :cond_4
    iget-object v2, p1, LX/GyI;->b:LX/2kW;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
