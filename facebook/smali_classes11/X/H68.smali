.class public final LX/H68;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V
    .locals 0

    .prologue
    .line 2425878
    iput-object p1, p0, LX/H68;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2425896
    iget-object v0, p0, LX/H68;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->C(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    .line 2425897
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2425879
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2425880
    if-eqz p1, :cond_2

    .line 2425881
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2425882
    if-eqz v0, :cond_2

    .line 2425883
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2425884
    check-cast v0, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2425885
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2425886
    check-cast v0, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimMarkAsUsedMutationModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v0

    .line 2425887
    iget-object v1, p0, LX/H68;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->ak:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->me_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2425888
    iget-object v1, p0, LX/H68;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v0}, LX/H83;->a(LX/H70;)LX/H83;

    move-result-object v0

    .line 2425889
    iput-object v0, v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    .line 2425890
    iget-object v0, p0, LX/H68;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->b:LX/0Sh;

    new-instance v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment$15$1;

    invoke-direct {v1, p0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment$15$1;-><init>(LX/H68;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2425891
    iget-object v0, p0, LX/H68;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    invoke-virtual {v0}, LX/H83;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "mark_as_used"

    .line 2425892
    :goto_0
    iget-object v1, p0, LX/H68;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->h:LX/0Zb;

    iget-object v2, p0, LX/H68;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v2, v2, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->i:LX/H82;

    iget-object v3, p0, LX/H68;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v3, v3, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    const-string v4, "details"

    invoke-virtual {v2, v0, v3, v4}, LX/H82;->a(Ljava/lang/String;LX/H83;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2425893
    :cond_0
    :goto_1
    return-void

    .line 2425894
    :cond_1
    const-string v0, "mark_as_unused"

    goto :goto_0

    .line 2425895
    :cond_2
    iget-object v0, p0, LX/H68;->a:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->C(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    goto :goto_1
.end method
