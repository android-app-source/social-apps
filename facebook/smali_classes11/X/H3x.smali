.class public final LX/H3x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;)V
    .locals 0

    .prologue
    .line 2422119
    iput-object p1, p0, LX/H3x;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x2c9dcabd

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2422120
    iget-object v1, p0, LX/H3x;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    iget-object v1, v1, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->k:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    .line 2422121
    sget-object v3, LX/H3i;->b:[I

    iget-object v4, v1, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2422122
    iget-object p0, v4, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v4, p0

    .line 2422123
    iget-object p0, v4, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->a:LX/FOw;

    move-object v4, p0

    .line 2422124
    invoke-virtual {v4}, LX/FOw;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2422125
    :goto_0
    const v1, 0x584de9b2

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2422126
    :pswitch_0
    iget-object v3, v1, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->g:LX/0ad;

    sget-short v4, LX/H3l;->a:S

    const/4 p0, 0x0

    invoke-interface {v3, v4, p0}, LX/0ad;->a(SZ)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2422127
    new-instance v3, LX/2si;

    invoke-direct {v3}, LX/2si;-><init>()V

    .line 2422128
    iget-object v4, v1, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->f:LX/6Zb;

    const-string p0, "surface_checkin_niem_controller"

    const-string p1, "mechanism_niem"

    invoke-virtual {v4, v3, p0, p1}, LX/6Zb;->a(LX/2si;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2422129
    :cond_0
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2422130
    invoke-virtual {v1, v3}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 2422131
    :pswitch_1
    iget-object v3, v1, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->t:LX/0i5;

    sget-object v4, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->j:[Ljava/lang/String;

    new-instance p0, LX/H3h;

    invoke-direct {p0, v1}, LX/H3h;-><init>(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)V

    invoke-virtual {v3, v4, p0}, LX/0i5;->a([Ljava/lang/String;LX/6Zx;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
