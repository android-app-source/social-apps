.class public LX/FsX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/FsW;

.field public final c:LX/0ad;


# direct methods
.method public constructor <init>(LX/0tX;LX/FsW;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2297205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2297206
    iput-object p1, p0, LX/FsX;->a:LX/0tX;

    .line 2297207
    iput-object p2, p0, LX/FsX;->b:LX/FsW;

    .line 2297208
    iput-object p3, p0, LX/FsX;->c:LX/0ad;

    .line 2297209
    return-void
.end method

.method public static a(LX/5xI;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zO;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5xI;",
            "LX/0zS;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineSection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2297199
    invoke-static {p0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2297200
    iput p2, v0, LX/0zO;->B:I

    .line 2297201
    move-object v0, v0

    .line 2297202
    iput-object p3, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2297203
    move-object v0, v0

    .line 2297204
    return-object v0
.end method

.method public static a(LX/FsX;LX/0v6;LX/Fso;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;I)LX/0zX;
    .locals 7
    .param p0    # LX/FsX;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0v6;",
            "LX/Fso;",
            "LX/0zS;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "I)",
            "LX/0zX",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineSection;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2297214
    if-nez p6, :cond_0

    .line 2297215
    const/4 v0, 0x0

    .line 2297216
    :goto_0
    return-object v0

    .line 2297217
    :cond_0
    invoke-static {}, LX/0zX;->b()LX/0zX;

    move-result-object v1

    .line 2297218
    const/4 v0, 0x0

    move v2, v0

    move-object v0, p2

    :goto_1
    if-ge v2, p6, :cond_2

    .line 2297219
    iget-object v3, p0, LX/FsX;->b:LX/FsW;

    .line 2297220
    iget-object v4, p0, LX/FsX;->c:LX/0ad;

    sget v5, LX/0wf;->aS:I

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0ad;->a(II)I

    move-result v4

    move v4, v4

    .line 2297221
    invoke-virtual {v3, v0, v4}, LX/FsW;->a(LX/Fso;I)LX/5xI;

    move-result-object v0

    .line 2297222
    invoke-static {v0, p3, p4, p5}, LX/FsX;->a(LX/5xI;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v4

    .line 2297223
    if-eqz p1, :cond_1

    invoke-virtual {p1, v4}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v0

    invoke-static {v0}, LX/FsD;->a(LX/0zX;)LX/0zX;

    move-result-object v0

    .line 2297224
    :goto_2
    invoke-virtual {v1, v0}, LX/0zX;->b(LX/0zX;)LX/0zX;

    move-result-object v3

    .line 2297225
    invoke-static {p2, v4}, LX/FsX;->a(LX/Fso;LX/0zO;)LX/Fso;

    move-result-object v1

    .line 2297226
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-object v0, v1

    move-object v1, v3

    goto :goto_1

    .line 2297227
    :cond_1
    iget-object v0, p0, LX/FsX;->a:LX/0tX;

    invoke-virtual {v0, v4}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/FsA;->a(Lcom/google/common/util/concurrent/ListenableFuture;)LX/0zX;

    move-result-object v0

    goto :goto_2

    :cond_2
    move-object v0, v1

    .line 2297228
    goto :goto_0
.end method

.method public static a(LX/Fso;LX/0zO;)LX/Fso;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Fso;",
            "LX/0zO",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineSection;",
            ">;)",
            "LX/Fso;"
        }
    .end annotation

    .prologue
    .line 2297229
    invoke-virtual {p0}, LX/Fso;->a()LX/Fsn;

    move-result-object v0

    const-string v1, "end_cursor"

    sget-object v2, LX/4Zz;->FIRST:LX/4Zz;

    sget-object v3, LX/4a0;->SKIP:LX/4a0;

    invoke-virtual {p1, v1, v2, v3}, LX/0zO;->a(Ljava/lang/String;LX/4Zz;LX/4a0;)LX/4a1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Fsn;->b(LX/4a1;)LX/Fsn;

    move-result-object v0

    invoke-virtual {v0}, LX/Fsn;->b()LX/Fso;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/FsX;
    .locals 4

    .prologue
    .line 2297212
    new-instance v3, LX/FsX;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/FsW;->a(LX/0QB;)LX/FsW;

    move-result-object v1

    check-cast v1, LX/FsW;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-direct {v3, v0, v1, v2}, LX/FsX;-><init>(LX/0tX;LX/FsW;LX/0ad;)V

    .line 2297213
    return-object v3
.end method


# virtual methods
.method public final a(LX/0v6;LX/Fso;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zX;
    .locals 7
    .param p1    # LX/0v6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0v6;",
            "LX/Fso;",
            "LX/0zS;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zX",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineSection;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2297210
    iget-object v0, p0, LX/FsX;->c:LX/0ad;

    sget v1, LX/0wf;->aQ:I

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    move v0, v0

    .line 2297211
    add-int/lit8 v6, v0, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v6}, LX/FsX;->a(LX/FsX;LX/0v6;LX/Fso;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;I)LX/0zX;

    move-result-object v0

    return-object v0
.end method
