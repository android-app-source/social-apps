.class public final LX/GjQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CEO;


# instance fields
.field public final synthetic a:Lcom/facebook/greetingcards/create/PreviewCardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/greetingcards/create/PreviewCardFragment;)V
    .locals 0

    .prologue
    .line 2387511
    iput-object p1, p0, LX/GjQ;->a:Lcom/facebook/greetingcards/create/PreviewCardFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/greetingcards/verve/model/VMAction;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2387512
    const-string v0, "button1"

    iget-object v1, p1, Lcom/facebook/greetingcards/verve/model/VMAction;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2387513
    iget-object v0, p0, LX/GjQ;->a:Lcom/facebook/greetingcards/create/PreviewCardFragment;

    invoke-static {v0}, Lcom/facebook/greetingcards/create/PreviewCardFragment;->o(Lcom/facebook/greetingcards/create/PreviewCardFragment;)V

    .line 2387514
    :cond_0
    :goto_0
    return-void

    .line 2387515
    :cond_1
    const-string v0, "button2"

    iget-object v1, p1, Lcom/facebook/greetingcards/verve/model/VMAction;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2387516
    iget-object v0, p0, LX/GjQ;->a:Lcom/facebook/greetingcards/create/PreviewCardFragment;

    .line 2387517
    iget-object v1, v0, Lcom/facebook/greetingcards/render/RenderCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    invoke-static {v1}, Lcom/facebook/greetingcards/model/GreetingCard;->a(Lcom/facebook/greetingcards/model/GreetingCard;)Lcom/facebook/greetingcards/model/GreetingCard;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    .line 2387518
    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p0, "greeting_card_edit"

    invoke-direct {v1, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p0, "greeting_card_id"

    iget-object p1, v0, Lcom/facebook/greetingcards/render/RenderCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    iget-object p1, p1, Lcom/facebook/greetingcards/model/GreetingCard;->f:Ljava/lang/String;

    invoke-virtual {v1, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2387519
    iget-object p0, v0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->h:LX/0Zb;

    invoke-interface {p0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2387520
    iget-object v1, v0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->f:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    .line 2387521
    iget-object p1, v0, Lcom/facebook/greetingcards/render/RenderCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    move-object p1, p1

    .line 2387522
    invoke-static {p0, p1}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->a(Landroid/content/Context;Lcom/facebook/greetingcards/model/GreetingCard;)Landroid/content/Intent;

    move-result-object p0

    const/4 p1, 0x7

    invoke-interface {v1, p0, p1, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2387523
    goto :goto_0
.end method
