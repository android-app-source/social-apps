.class public LX/GJ2;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

.field private b:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

.field public c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

.field private d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

.field public e:LX/GMm;


# direct methods
.method public constructor <init>(LX/GMm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2337689
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2337690
    iput-object p1, p0, LX/GJ2;->e:LX/GMm;

    .line 2337691
    return-void
.end method

.method public static a(LX/0QB;)LX/GJ2;
    .locals 2

    .prologue
    .line 2337687
    new-instance v1, LX/GJ2;

    invoke-static {p0}, LX/GMm;->a(LX/0QB;)LX/GMm;

    move-result-object v0

    check-cast v0, LX/GMm;

    invoke-direct {v1, v0}, LX/GJ2;-><init>(LX/GMm;)V

    .line 2337688
    return-object v1
.end method

.method public static a(LX/GJ2;Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    .line 2337684
    iget-object v0, p0, LX/GJ2;->a:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iget-object v1, p0, LX/GJ2;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/GMm;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;I)V

    .line 2337685
    iget-object v0, p0, LX/GJ2;->a:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setText(Ljava/lang/CharSequence;)V

    .line 2337686
    return-void
.end method

.method public static b(LX/0QB;)LX/GJ2;
    .locals 1

    .prologue
    .line 2337634
    invoke-static {p0}, LX/GJ2;->a(LX/0QB;)LX/GJ2;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 5

    .prologue
    .line 2337680
    iget-object v0, p0, LX/GJ2;->e:LX/GMm;

    const v1, 0x7f080a39

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "https://m.facebook.com/ads/manage/billing?account_id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/GJ2;->d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->t()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/GJ2;->a:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2337681
    iget-object v4, p0, LX/GHg;->b:LX/GCE;

    move-object v4, v4

    .line 2337682
    invoke-virtual {v0, v1, v2, v3, v4}, LX/GMm;->a(ILjava/lang/String;Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;LX/GCE;)Landroid/text/SpannableString;

    move-result-object v0

    invoke-static {p0, v0}, LX/GJ2;->a(LX/GJ2;Ljava/lang/CharSequence;)V

    .line 2337683
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2337675
    invoke-super {p0}, LX/GHg;->a()V

    .line 2337676
    iput-object v0, p0, LX/GJ2;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2337677
    iput-object v0, p0, LX/GJ2;->a:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2337678
    iput-object v0, p0, LX/GJ2;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2337679
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 2337672
    iget-object v0, p0, LX/GJ2;->a:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    iget-object v1, p0, LX/GJ2;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, LX/GMm;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;I)V

    .line 2337673
    iget-object v0, p0, LX/GJ2;->a:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/GJ2;->a(LX/GJ2;Ljava/lang/CharSequence;)V

    .line 2337674
    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2337671
    check-cast p1, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    invoke-virtual {p0, p1, p2}, LX/GJ2;->a(Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 1

    .prologue
    .line 2337668
    iput-object p1, p0, LX/GJ2;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2337669
    iget-object v0, p0, LX/GJ2;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    iput-object v0, p0, LX/GJ2;->d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    .line 2337670
    return-void
.end method

.method public a(Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 3
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2337635
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2337636
    iput-object p1, p0, LX/GJ2;->a:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2337637
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2337638
    iget-object v1, v0, LX/GCE;->e:LX/0ad;

    move-object v0, v1

    .line 2337639
    iget-object v1, p0, LX/GJ2;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    sget-object v2, LX/GGB;->PAUSED:LX/GGB;

    if-ne v1, v2, :cond_1

    sget-short v1, LX/GDK;->A:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2337640
    iget-object v0, p0, LX/GJ2;->a:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setVisibility(I)V

    .line 2337641
    :cond_0
    :goto_0
    return-void

    .line 2337642
    :cond_1
    iput-object p2, p0, LX/GJ2;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2337643
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2337644
    const/4 v1, 0x5

    new-instance v2, LX/GJu;

    invoke-direct {v2, p0}, LX/GJu;-><init>(LX/GJ2;)V

    invoke-virtual {v0, v1, v2}, LX/GCE;->a(ILX/GFR;)V

    .line 2337645
    iget-object v0, p0, LX/GJ2;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/GJ2;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2337646
    const v0, 0x7f080a37

    invoke-virtual {p0, v0}, LX/GJ2;->a(I)V

    goto :goto_0

    .line 2337647
    :cond_2
    iget-object v0, p0, LX/GJ2;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2337648
    const v0, 0x7f080a38

    invoke-virtual {p0, v0}, LX/GJ2;->a(I)V

    goto :goto_0

    .line 2337649
    :cond_3
    iget-object v0, p0, LX/GJ2;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GG6;->i(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2337650
    iget-object v0, p0, LX/GJ2;->e:LX/GMm;

    const v1, 0x7f080a3b

    const-string v2, "https://m.facebook.com/ads/manage/accounts/?select"

    iget-object p1, p0, LX/GJ2;->a:Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;

    .line 2337651
    iget-object p2, p0, LX/GHg;->b:LX/GCE;

    move-object p2, p2

    .line 2337652
    invoke-virtual {v0, v1, v2, p1, p2}, LX/GMm;->a(ILjava/lang/String;Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;LX/GCE;)Landroid/text/SpannableString;

    move-result-object v0

    invoke-static {p0, v0}, LX/GJ2;->a(LX/GJ2;Ljava/lang/CharSequence;)V

    .line 2337653
    goto :goto_0

    .line 2337654
    :cond_4
    iget-object v0, p0, LX/GJ2;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    .line 2337655
    sget-object v1, LX/GJv;->a:[I

    invoke-virtual {v0}, LX/GGB;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 2337656
    :pswitch_0
    const v0, 0x7f080a3d

    invoke-virtual {p0, v0}, LX/GJ2;->a(I)V

    .line 2337657
    iget-object v0, p0, LX/GJ2;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GMU;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2337658
    const v0, 0x7f080a3e

    invoke-virtual {p0, v0}, LX/GJ2;->a(I)V

    goto/16 :goto_0

    .line 2337659
    :pswitch_1
    iget-object v0, p0, LX/GJ2;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GMU;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2337660
    const v0, 0x7f080a3e

    invoke-virtual {p0, v0}, LX/GJ2;->a(I)V

    .line 2337661
    :cond_5
    iget-object v0, p0, LX/GJ2;->d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    invoke-static {v0}, LX/GNI;->c(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2337662
    invoke-direct {p0}, LX/GJ2;->c()V

    goto/16 :goto_0

    .line 2337663
    :pswitch_2
    const v0, 0x7f080a40

    invoke-virtual {p0, v0}, LX/GJ2;->a(I)V

    .line 2337664
    iget-object v0, p0, LX/GJ2;->d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    invoke-static {v0}, LX/GNI;->c(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2337665
    invoke-direct {p0}, LX/GJ2;->c()V

    goto/16 :goto_0

    .line 2337666
    :pswitch_3
    const v0, 0x7f080b1e

    invoke-virtual {p0, v0}, LX/GJ2;->a(I)V

    goto/16 :goto_0

    .line 2337667
    :pswitch_4
    const v0, 0x7f080b1f

    invoke-virtual {p0, v0}, LX/GJ2;->a(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
