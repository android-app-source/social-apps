.class public LX/FK9;
.super LX/4ok;
.source ""


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/297;

.field private c:Lcom/facebook/messaging/model/threads/NotificationSetting;

.field private d:LX/4og;

.field private e:Z

.field private f:Lcom/facebook/messaging/model/threadkey/ThreadKey;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/297;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2224661
    invoke-direct {p0, p1}, LX/4ok;-><init>(Landroid/content/Context;)V

    .line 2224662
    iput-object p2, p0, LX/FK9;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2224663
    iput-object p3, p0, LX/FK9;->b:LX/297;

    .line 2224664
    iget-object v0, p0, LX/FK9;->b:LX/297;

    invoke-virtual {v0}, LX/297;->a()Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v0

    iput-object v0, p0, LX/FK9;->c:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 2224665
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/FK9;->setDefaultValue(Ljava/lang/Object;)V

    .line 2224666
    sget-object v0, LX/0db;->I:LX/0Tn;

    invoke-virtual {v0}, LX/0To;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/FK9;->setKey(Ljava/lang/String;)V

    .line 2224667
    invoke-direct {p0}, LX/FK9;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/FK9;->setSummary(Ljava/lang/CharSequence;)V

    .line 2224668
    const v0, 0x7f08039a

    invoke-virtual {p0, v0}, LX/FK9;->setTitle(I)V

    .line 2224669
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2224670
    iget-object v0, p0, LX/FK9;->c:Lcom/facebook/messaging/model/threads/NotificationSetting;

    iget-boolean v0, v0, Lcom/facebook/messaging/model/threads/NotificationSetting;->d:Z

    if-eqz v0, :cond_0

    .line 2224671
    const/4 v0, 0x0

    .line 2224672
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/FK9;->b:LX/297;

    iget-object v1, p0, LX/FK9;->c:Lcom/facebook/messaging/model/threads/NotificationSetting;

    invoke-virtual {v0, v1}, LX/297;->a(Lcom/facebook/messaging/model/threads/NotificationSetting;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final onSetInitialValue(ZLjava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2224673
    iput-boolean v0, p0, LX/FK9;->e:Z

    .line 2224674
    iget-object v1, p0, LX/FK9;->c:Lcom/facebook/messaging/model/threads/NotificationSetting;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/NotificationSetting;->c()LX/6fk;

    move-result-object v1

    sget-object v2, LX/6fk;->TemporarilyMuted:LX/6fk;

    if-eq v1, v2, :cond_0

    :goto_0
    invoke-virtual {p0, v0}, LX/FK9;->setChecked(Z)V

    .line 2224675
    return-void

    .line 2224676
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final setChecked(Z)V
    .locals 4

    .prologue
    .line 2224677
    invoke-super {p0, p1}, LX/4ok;->setChecked(Z)V

    .line 2224678
    if-eqz p1, :cond_0

    .line 2224679
    sget-object v0, Lcom/facebook/messaging/model/threads/NotificationSetting;->a:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 2224680
    :goto_0
    iget-boolean v1, p0, LX/FK9;->e:Z

    if-eqz v1, :cond_1

    .line 2224681
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/FK9;->e:Z

    .line 2224682
    :goto_1
    return-void

    .line 2224683
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    const-wide/16 v2, 0x7080

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threads/NotificationSetting;->a(J)Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v0

    goto :goto_0

    .line 2224684
    :cond_1
    iget-object v1, p0, LX/FK9;->d:LX/4og;

    if-eqz v1, :cond_2

    .line 2224685
    iget-object v1, p0, LX/FK9;->c:Lcom/facebook/messaging/model/threads/NotificationSetting;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/NotificationSetting;->d()LX/0m9;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/NotificationSetting;->d()LX/0m9;

    .line 2224686
    :cond_2
    iput-object v0, p0, LX/FK9;->c:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 2224687
    iget-object v0, p0, LX/FK9;->f:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/FK9;->f:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, LX/0db;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)LX/0Tn;

    move-result-object v0

    .line 2224688
    :goto_2
    iget-object v1, p0, LX/FK9;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 2224689
    iget-object v2, p0, LX/FK9;->c:Lcom/facebook/messaging/model/threads/NotificationSetting;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threads/NotificationSetting;->a()J

    move-result-wide v2

    invoke-interface {v1, v0, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    .line 2224690
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2224691
    invoke-direct {p0}, LX/FK9;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/FK9;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 2224692
    :cond_3
    sget-object v0, LX/0db;->J:LX/0Tn;

    goto :goto_2
.end method
