.class public final LX/G7F;
.super LX/12O;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/12O",
        "<",
        "LX/4g4;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/G7G;


# direct methods
.method public constructor <init>(LX/G7G;)V
    .locals 0

    .prologue
    .line 2321137
    iput-object p1, p0, LX/G7F;->a:LX/G7G;

    invoke-direct {p0}, LX/12O;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/4g4;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2321138
    const-class v0, LX/4g4;

    return-object v0
.end method

.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 2321139
    check-cast p1, LX/4g4;

    const/4 v4, 0x0

    .line 2321140
    iget-object v0, p0, LX/G7F;->a:LX/G7G;

    iget-object v0, v0, LX/121;->a:Ljava/util/Map;

    iget-object v1, p1, LX/4g4;->a:LX/0yY;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3J6;

    .line 2321141
    if-eqz v0, :cond_0

    iget-object v1, v0, LX/3J6;->d:LX/39A;

    if-nez v1, :cond_1

    .line 2321142
    :cond_0
    :goto_0
    return-void

    .line 2321143
    :cond_1
    sget-object v1, LX/G7E;->a:[I

    iget-object v2, p1, LX/4g4;->b:LX/4g3;

    invoke-virtual {v2}, LX/4g3;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2321144
    iget-object v0, p0, LX/G7F;->a:LX/G7G;

    invoke-virtual {v0}, LX/G7G;->c()Ljava/lang/Class;

    goto :goto_0

    .line 2321145
    :pswitch_0
    iget-object v1, p0, LX/G7F;->a:LX/G7G;

    iget-object v1, v1, LX/G7G;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/G7H;

    const-string v2, "dialog_ok"

    iget-object v3, p1, LX/4g4;->a:LX/0yY;

    invoke-virtual {v1, v2, v3, v4}, LX/G7H;->a(Ljava/lang/String;LX/0yY;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2321146
    iget-object v0, v0, LX/3J6;->d:LX/39A;

    iget-object v1, p1, LX/4g4;->e:Ljava/lang/Object;

    invoke-interface {v0, v1}, LX/39A;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 2321147
    :pswitch_1
    iget-object v1, p0, LX/G7F;->a:LX/G7G;

    iget-object v1, v1, LX/G7G;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/G7H;

    const-string v2, "dialog_cancel"

    iget-object v3, p1, LX/4g4;->a:LX/0yY;

    invoke-virtual {v1, v2, v3, v4}, LX/G7H;->a(Ljava/lang/String;LX/0yY;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2321148
    iget-object v0, v0, LX/3J6;->d:LX/39A;

    invoke-interface {v0}, LX/39A;->a()V

    goto :goto_0

    .line 2321149
    :pswitch_2
    iget-object v1, p0, LX/G7F;->a:LX/G7G;

    iget-object v1, v1, LX/G7G;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/G7H;

    const-string v2, "show_dialog_failure"

    iget-object v3, p1, LX/4g4;->a:LX/0yY;

    invoke-virtual {v1, v2, v3, v4}, LX/G7H;->a(Ljava/lang/String;LX/0yY;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2321150
    iget-object v1, v0, LX/3J6;->i:LX/0gc;

    if-nez v1, :cond_2

    .line 2321151
    :cond_2
    iget-object v1, v0, LX/3J6;->i:LX/0gc;

    invoke-virtual {v1}, LX/0gc;->c()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2321152
    :cond_3
    iget-object v1, v0, LX/3J6;->d:LX/39A;

    if-eqz v1, :cond_4

    .line 2321153
    iget-object v1, v0, LX/3J6;->d:LX/39A;

    iget-object v2, p1, LX/4g4;->e:Ljava/lang/Object;

    invoke-interface {v1, v2}, LX/39A;->a(Ljava/lang/Object;)V

    .line 2321154
    :cond_4
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
