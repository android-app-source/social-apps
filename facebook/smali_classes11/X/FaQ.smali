.class public final LX/FaQ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FaR;


# direct methods
.method public constructor <init>(LX/FaR;)V
    .locals 0

    .prologue
    .line 2258768
    iput-object p1, p0, LX/FaQ;->a:LX/FaR;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2258769
    iget-object v0, p0, LX/FaQ;->a:LX/FaR;

    new-instance v1, LX/7C4;

    sget-object v2, LX/3Ql;->FETCH_AWARENESS_LEARNING_CONFIG_FAIL:LX/3Ql;

    invoke-direct {v1, v2, p1}, LX/7C4;-><init>(LX/3Ql;Ljava/lang/Throwable;)V

    .line 2258770
    iget-object v2, v0, LX/FaR;->d:LX/0x9;

    if-eqz v2, :cond_0

    .line 2258771
    iget-object v2, v0, LX/FaR;->d:LX/0x9;

    .line 2258772
    iget-object p0, v2, LX/0x9;->n:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/03V;

    const-string v0, "SearchAwareness"

    invoke-virtual {p0, v0, v1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2258773
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 13

    .prologue
    .line 2258774
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2258775
    if-eqz p1, :cond_2

    .line 2258776
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2258777
    if-eqz v0, :cond_2

    .line 2258778
    iget-object v1, p0, LX/FaQ;->a:LX/FaR;

    .line 2258779
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2258780
    check-cast v0, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;

    .line 2258781
    iget-object v2, v1, LX/FaR;->d:LX/0x9;

    if-eqz v2, :cond_1

    .line 2258782
    iget-object v2, v1, LX/FaR;->d:LX/0x9;

    .line 2258783
    invoke-virtual {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;->k()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;->k()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/0x9;->a(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2258784
    iput-object v0, v2, LX/0x9;->q:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;

    .line 2258785
    :cond_0
    iget-object v5, v2, LX/0x9;->o:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v5}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v7

    sget-object v8, LX/7CP;->e:LX/0Tn;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;->j()Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, v2, LX/0x9;->p:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    const-wide/32 v9, 0x5265c00

    add-long/2addr v5, v9

    :goto_0
    invoke-interface {v7, v8, v5, v6}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v5

    invoke-interface {v5}, LX/0hN;->commit()V

    .line 2258786
    :cond_1
    iget-object v2, v1, LX/FaR;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/7CP;->f:LX/0Tn;

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 2258787
    :goto_1
    return-void

    .line 2258788
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Result was null"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, LX/FaQ;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2258789
    :cond_3
    iget-object v5, v2, LX/0x9;->p:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v5

    invoke-virtual {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$LearningNuxConfigurationModel;->a()I

    move-result v9

    int-to-long v9, v9

    const-wide/32 v11, 0x36ee80

    mul-long/2addr v9, v11

    add-long/2addr v5, v9

    goto :goto_0
.end method
