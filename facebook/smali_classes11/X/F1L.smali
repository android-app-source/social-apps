.class public final LX/F1L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;

.field private final b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 0

    .prologue
    .line 2191571
    iput-object p1, p0, LX/F1L;->a:Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2191572
    iput-object p2, p0, LX/F1L;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2191573
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x1acaa027

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2191574
    iget-object v1, p0, LX/F1L;->b:Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    invoke-static {v1}, LX/9hF;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)LX/9hE;

    move-result-object v1

    sget-object v2, LX/74S;->NEWSFEED:LX/74S;

    invoke-virtual {v1, v2}, LX/9hD;->a(LX/74S;)LX/9hD;

    move-result-object v1

    invoke-virtual {v1}, LX/9hD;->b()Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;

    move-result-object v1

    .line 2191575
    iget-object v2, p0, LX/F1L;->a:Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;

    iget-object v2, v2, Lcom/facebook/goodwill/feed/rows/ThrowbackPhotoAttachmentPagePartDefinition;->h:LX/23R;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v2, v3, v1, v4}, LX/23R;->a(Landroid/content/Context;Lcom/facebook/photos/mediagallery/launcher/MediaGalleryLauncherParams;LX/9hN;)V

    .line 2191576
    const v1, -0x5046384f

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
