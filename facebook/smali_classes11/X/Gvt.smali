.class public final LX/Gvt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
        ">;",
        "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

.field public final synthetic b:LX/Gvy;


# direct methods
.method public constructor <init>(LX/Gvy;Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;)V
    .locals 0

    .prologue
    .line 2406204
    iput-object p1, p0, LX/Gvt;->b:LX/Gvy;

    iput-object p2, p0, LX/Gvt;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2406200
    check-cast p1, LX/0Px;

    .line 2406201
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2406202
    iget-object v0, p0, LX/Gvt;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    invoke-virtual {v0, p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTaggedUsers(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2406203
    :cond_0
    iget-object v0, p0, LX/Gvt;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2406199
    const/4 v0, 0x0

    return v0
.end method
