.class public final LX/GQ0;
.super Ljava/util/AbstractList;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/AbstractList",
        "<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2349642
    iput-object p1, p0, LX/GQ0;->a:Landroid/view/ViewGroup;

    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    return-void
.end method

.method private a(I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2349643
    iget-object v0, p0, LX/GQ0;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final add(ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 2349644
    check-cast p2, Landroid/view/View;

    .line 2349645
    iget-object v0, p0, LX/GQ0;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, p2, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 2349646
    return-void
.end method

.method public final add(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2349647
    check-cast p1, Landroid/view/View;

    .line 2349648
    iget-object v0, p0, LX/GQ0;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2349649
    const/4 v0, 0x1

    return v0
.end method

.method public final synthetic get(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2349650
    invoke-direct {p0, p1}, LX/GQ0;->a(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final remove(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2349651
    invoke-direct {p0, p1}, LX/GQ0;->a(I)Landroid/view/View;

    move-result-object v0

    .line 2349652
    iget-object v1, p0, LX/GQ0;->a:Landroid/view/ViewGroup;

    invoke-virtual {v1, p1}, Landroid/view/ViewGroup;->removeViewAt(I)V

    .line 2349653
    return-object v0
.end method

.method public final remove(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2349654
    iget-object v0, p0, LX/GQ0;->a:Landroid/view/ViewGroup;

    check-cast p1, Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2349655
    const/4 v0, 0x1

    return v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 2349656
    iget-object v0, p0, LX/GQ0;->a:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    return v0
.end method
