.class public LX/GT0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1TG;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/GT0;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2355100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2355101
    return-void
.end method

.method public static a(LX/0QB;)LX/GT0;
    .locals 3

    .prologue
    .line 2355102
    sget-object v0, LX/GT0;->a:LX/GT0;

    if-nez v0, :cond_1

    .line 2355103
    const-class v1, LX/GT0;

    monitor-enter v1

    .line 2355104
    :try_start_0
    sget-object v0, LX/GT0;->a:LX/GT0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2355105
    if-eqz v2, :cond_0

    .line 2355106
    :try_start_1
    new-instance v0, LX/GT0;

    invoke-direct {v0}, LX/GT0;-><init>()V

    .line 2355107
    move-object v0, v0

    .line 2355108
    sput-object v0, LX/GT0;->a:LX/GT0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2355109
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2355110
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2355111
    :cond_1
    sget-object v0, LX/GT0;->a:LX/GT0;

    return-object v0

    .line 2355112
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2355113
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0ja;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 2355114
    const-class v0, Lcom/facebook/appinvites/ui/AppInviteContentView;

    new-instance v1, LX/3AL;

    const/4 v2, 0x6

    invoke-direct {v1, v2, v4}, LX/3AL;-><init>(II)V

    new-instance v2, LX/3AM;

    const/4 v3, 0x3

    invoke-direct {v2, v3, v4}, LX/3AM;-><init>(II)V

    invoke-virtual {p1, v0, v1, v2}, LX/0ja;->a(Ljava/lang/Class;LX/3AL;LX/3AM;)V

    .line 2355115
    return-void
.end method
