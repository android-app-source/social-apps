.class public final enum LX/Fod;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Fod;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Fod;

.field public static final enum CHARITIES:LX/Fod;

.field public static final enum DAF_DISCLOSURE:LX/Fod;

.field public static final enum HIGHLIGHTED_CHARITY:LX/Fod;

.field public static final enum PREFILL_FIELDS:LX/Fod;

.field public static final enum TITLES:LX/Fod;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2289887
    new-instance v0, LX/Fod;

    const-string v1, "CHARITIES"

    invoke-direct {v0, v1, v2}, LX/Fod;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fod;->CHARITIES:LX/Fod;

    .line 2289888
    new-instance v0, LX/Fod;

    const-string v1, "TITLES"

    invoke-direct {v0, v1, v3}, LX/Fod;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fod;->TITLES:LX/Fod;

    .line 2289889
    new-instance v0, LX/Fod;

    const-string v1, "PREFILL_FIELDS"

    invoke-direct {v0, v1, v4}, LX/Fod;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fod;->PREFILL_FIELDS:LX/Fod;

    .line 2289890
    new-instance v0, LX/Fod;

    const-string v1, "HIGHLIGHTED_CHARITY"

    invoke-direct {v0, v1, v5}, LX/Fod;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fod;->HIGHLIGHTED_CHARITY:LX/Fod;

    .line 2289891
    new-instance v0, LX/Fod;

    const-string v1, "DAF_DISCLOSURE"

    invoke-direct {v0, v1, v6}, LX/Fod;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fod;->DAF_DISCLOSURE:LX/Fod;

    .line 2289892
    const/4 v0, 0x5

    new-array v0, v0, [LX/Fod;

    sget-object v1, LX/Fod;->CHARITIES:LX/Fod;

    aput-object v1, v0, v2

    sget-object v1, LX/Fod;->TITLES:LX/Fod;

    aput-object v1, v0, v3

    sget-object v1, LX/Fod;->PREFILL_FIELDS:LX/Fod;

    aput-object v1, v0, v4

    sget-object v1, LX/Fod;->HIGHLIGHTED_CHARITY:LX/Fod;

    aput-object v1, v0, v5

    sget-object v1, LX/Fod;->DAF_DISCLOSURE:LX/Fod;

    aput-object v1, v0, v6

    sput-object v0, LX/Fod;->$VALUES:[LX/Fod;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2289893
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Fod;
    .locals 1

    .prologue
    .line 2289886
    const-class v0, LX/Fod;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Fod;

    return-object v0
.end method

.method public static values()[LX/Fod;
    .locals 1

    .prologue
    .line 2289885
    sget-object v0, LX/Fod;->$VALUES:[LX/Fod;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Fod;

    return-object v0
.end method
