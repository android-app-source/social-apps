.class public final LX/HCc;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2439043
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_a

    .line 2439044
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2439045
    :goto_0
    return v1

    .line 2439046
    :cond_0
    const-string v11, "is_current"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 2439047
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v3

    move v7, v3

    move v3, v2

    .line 2439048
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_7

    .line 2439049
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2439050
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2439051
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 2439052
    const-string v11, "description"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 2439053
    const/4 v10, 0x0

    .line 2439054
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v9

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v9, v11, :cond_e

    .line 2439055
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2439056
    :goto_2
    move v9, v10

    .line 2439057
    goto :goto_1

    .line 2439058
    :cond_2
    const-string v11, "image"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2439059
    const/4 v10, 0x0

    .line 2439060
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v8, v11, :cond_12

    .line 2439061
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2439062
    :goto_3
    move v8, v10

    .line 2439063
    goto :goto_1

    .line 2439064
    :cond_3
    const-string v11, "is_recommended"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 2439065
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v6, v0

    move v0, v2

    goto :goto_1

    .line 2439066
    :cond_4
    const-string v11, "name"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 2439067
    const/4 v10, 0x0

    .line 2439068
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v5, v11, :cond_16

    .line 2439069
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2439070
    :goto_4
    move v5, v10

    .line 2439071
    goto :goto_1

    .line 2439072
    :cond_5
    const-string v11, "template_type"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 2439073
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto :goto_1

    .line 2439074
    :cond_6
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2439075
    :cond_7
    const/4 v10, 0x6

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2439076
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 2439077
    invoke-virtual {p1, v2, v8}, LX/186;->b(II)V

    .line 2439078
    if-eqz v3, :cond_8

    .line 2439079
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v7}, LX/186;->a(IZ)V

    .line 2439080
    :cond_8
    if-eqz v0, :cond_9

    .line 2439081
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v6}, LX/186;->a(IZ)V

    .line 2439082
    :cond_9
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v5}, LX/186;->b(II)V

    .line 2439083
    const/4 v0, 0x5

    invoke-virtual {p1, v0, v4}, LX/186;->b(II)V

    .line 2439084
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_a
    move v0, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    move v7, v1

    move v8, v1

    move v9, v1

    goto/16 :goto_1

    .line 2439085
    :cond_b
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2439086
    :cond_c
    :goto_5
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_d

    .line 2439087
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2439088
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2439089
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_c

    if-eqz v11, :cond_c

    .line 2439090
    const-string v12, "text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_b

    .line 2439091
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {p1, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_5

    .line 2439092
    :cond_d
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2439093
    invoke-virtual {p1, v10, v9}, LX/186;->b(II)V

    .line 2439094
    invoke-virtual {p1}, LX/186;->d()I

    move-result v10

    goto/16 :goto_2

    :cond_e
    move v9, v10

    goto :goto_5

    .line 2439095
    :cond_f
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2439096
    :cond_10
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_11

    .line 2439097
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2439098
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2439099
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_10

    if-eqz v11, :cond_10

    .line 2439100
    const-string v12, "uri"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_f

    .line 2439101
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto :goto_6

    .line 2439102
    :cond_11
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2439103
    invoke-virtual {p1, v10, v8}, LX/186;->b(II)V

    .line 2439104
    invoke-virtual {p1}, LX/186;->d()I

    move-result v10

    goto/16 :goto_3

    :cond_12
    move v8, v10

    goto :goto_6

    .line 2439105
    :cond_13
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2439106
    :cond_14
    :goto_7
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_15

    .line 2439107
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2439108
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2439109
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_14

    if-eqz v11, :cond_14

    .line 2439110
    const-string v12, "text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_13

    .line 2439111
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    goto :goto_7

    .line 2439112
    :cond_15
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2439113
    invoke-virtual {p1, v10, v5}, LX/186;->b(II)V

    .line 2439114
    invoke-virtual {p1}, LX/186;->d()I

    move-result v10

    goto/16 :goto_4

    :cond_16
    move v5, v10

    goto :goto_7
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x5

    .line 2439115
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2439116
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2439117
    if-eqz v0, :cond_1

    .line 2439118
    const-string v1, "description"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439119
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2439120
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2439121
    if-eqz v1, :cond_0

    .line 2439122
    const-string p3, "text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439123
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2439124
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2439125
    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2439126
    if-eqz v0, :cond_3

    .line 2439127
    const-string v1, "image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439128
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2439129
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2439130
    if-eqz v1, :cond_2

    .line 2439131
    const-string p3, "uri"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439132
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2439133
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2439134
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2439135
    if-eqz v0, :cond_4

    .line 2439136
    const-string v1, "is_current"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439137
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2439138
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2439139
    if-eqz v0, :cond_5

    .line 2439140
    const-string v1, "is_recommended"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439141
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2439142
    :cond_5
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2439143
    if-eqz v0, :cond_7

    .line 2439144
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439145
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2439146
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2439147
    if-eqz v1, :cond_6

    .line 2439148
    const-string p3, "text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439149
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2439150
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2439151
    :cond_7
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2439152
    if-eqz v0, :cond_8

    .line 2439153
    const-string v0, "template_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2439154
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2439155
    :cond_8
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2439156
    return-void
.end method
