.class public final LX/GjS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$FetchPrefilledGreetingCardQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/greetingcards/create/PreviewCardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/greetingcards/create/PreviewCardFragment;)V
    .locals 0

    .prologue
    .line 2387525
    iput-object p1, p0, LX/GjS;->a:Lcom/facebook/greetingcards/create/PreviewCardFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2387526
    sget-object v0, Lcom/facebook/greetingcards/create/PreviewCardFragment;->c:Ljava/lang/Class;

    const-string v1, "Failure loading prefilled card."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2387527
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 14
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2387528
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2387529
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2387530
    check-cast v0, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$FetchPrefilledGreetingCardQueryModel;

    invoke-virtual {v0}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$FetchPrefilledGreetingCardQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2387531
    const/4 v13, 0x2

    const/4 v8, 0x0

    const/4 v6, 0x0

    .line 2387532
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2387533
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v2

    const v3, -0x15caa70d

    invoke-static {v1, v2, v6, v3}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v2

    .line 2387534
    if-eqz v2, :cond_1

    invoke-static {v2}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v2

    :goto_0
    invoke-virtual {v2}, LX/3Sa;->e()LX/3Sh;

    move-result-object v9

    move v4, v6

    move-object v5, v8

    move-object v3, v8

    :cond_0
    :goto_1
    invoke-interface {v9}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2387535
    invoke-interface {v9}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    .line 2387536
    iget-object v10, v2, LX/1vs;->a:LX/15i;

    iget v11, v2, LX/1vs;->b:I

    .line 2387537
    const-class v2, Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;

    sget-object v12, Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;

    invoke-virtual {v10, v11, v13, v2, v12}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;

    .line 2387538
    sget-object v12, LX/5QN;->a:[I

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLGreetingCardSlideType;->ordinal()I

    move-result v2

    aget v2, v12, v2

    packed-switch v2, :pswitch_data_0

    goto :goto_1

    .line 2387539
    :pswitch_0
    invoke-static {v10, v11}, LX/5QO;->b(LX/15i;I)Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    move-result-object v3

    goto :goto_1

    .line 2387540
    :cond_1
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v2

    goto :goto_0

    .line 2387541
    :pswitch_1
    const/16 v2, 0x8

    if-ge v4, v2, :cond_0

    .line 2387542
    invoke-static {v10, v11}, LX/5QO;->b(LX/15i;I)Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    move-result-object v2

    invoke-virtual {v7, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2387543
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 2387544
    :pswitch_2
    invoke-static {v10, v11}, LX/5QO;->b(LX/15i;I)Lcom/facebook/greetingcards/model/GreetingCard$Slide;

    move-result-object v5

    goto :goto_1

    .line 2387545
    :cond_2
    new-instance v2, Lcom/facebook/greetingcards/model/GreetingCard;

    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    const-class v7, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$GreetingCardTemplateModel;

    invoke-virtual {v1, v0, v6, v7}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v6

    check-cast v6, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$GreetingCardTemplateModel;

    invoke-virtual {v6}, Lcom/facebook/greetingcards/model/GreetingCardGraphQLModels$PrefilledGreetingCardFieldsModel$GreetingCardTemplateModel;->j()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v0, v13}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v2 .. v8}, Lcom/facebook/greetingcards/model/GreetingCard;-><init>(Lcom/facebook/greetingcards/model/GreetingCard$Slide;LX/0Px;Lcom/facebook/greetingcards/model/GreetingCard$Slide;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    .line 2387546
    iget-object v1, p0, LX/GjS;->a:Lcom/facebook/greetingcards/create/PreviewCardFragment;

    invoke-virtual {v1, v0}, Lcom/facebook/greetingcards/render/RenderCardFragment;->a(Lcom/facebook/greetingcards/model/GreetingCard;)V

    .line 2387547
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
