.class public final enum LX/FvJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FvJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FvJ;

.field public static final enum NAVTILES:LX/FvJ;

.field private static mValues:[LX/FvJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2301211
    new-instance v0, LX/FvJ;

    const-string v1, "NAVTILES"

    invoke-direct {v0, v1, v2}, LX/FvJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FvJ;->NAVTILES:LX/FvJ;

    .line 2301212
    const/4 v0, 0x1

    new-array v0, v0, [LX/FvJ;

    sget-object v1, LX/FvJ;->NAVTILES:LX/FvJ;

    aput-object v1, v0, v2

    sput-object v0, LX/FvJ;->$VALUES:[LX/FvJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2301213
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static cachedValues()[LX/FvJ;
    .locals 1

    .prologue
    .line 2301214
    sget-object v0, LX/FvJ;->mValues:[LX/FvJ;

    if-nez v0, :cond_0

    .line 2301215
    invoke-static {}, LX/FvJ;->values()[LX/FvJ;

    move-result-object v0

    sput-object v0, LX/FvJ;->mValues:[LX/FvJ;

    .line 2301216
    :cond_0
    sget-object v0, LX/FvJ;->mValues:[LX/FvJ;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/FvJ;
    .locals 1

    .prologue
    .line 2301217
    const-class v0, LX/FvJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FvJ;

    return-object v0
.end method

.method public static values()[LX/FvJ;
    .locals 1

    .prologue
    .line 2301218
    sget-object v0, LX/FvJ;->$VALUES:[LX/FvJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FvJ;

    return-object v0
.end method
