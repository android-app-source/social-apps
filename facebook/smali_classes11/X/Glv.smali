.class public LX/Glv;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/growth/contactinviter/ContactInviterFragment;

.field public b:LX/0W9;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/GmD;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/Gm9;


# direct methods
.method public constructor <init>(LX/0W9;LX/Gm9;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2391408
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2391409
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Glv;->c:Ljava/util/List;

    .line 2391410
    iput-object p1, p0, LX/Glv;->b:LX/0W9;

    .line 2391411
    iput-object p2, p0, LX/Glv;->d:LX/Gm9;

    .line 2391412
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2391330
    if-nez p2, :cond_0

    .line 2391331
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2391332
    const v1, 0x7f030365

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2391333
    new-instance v0, LX/Glt;

    invoke-direct {v0, v1}, LX/Glt;-><init>(Landroid/view/View;)V

    .line 2391334
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/Glu;

    new-instance v1, LX/Gm7;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/Gm7;-><init>(Landroid/content/Context;)V

    invoke-direct {v0, v1}, LX/Glu;-><init>(LX/Gm7;)V

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 2391339
    instance-of v0, p1, LX/Glt;

    if-eqz v0, :cond_2

    .line 2391340
    check-cast p1, LX/Glt;

    .line 2391341
    iget-object v0, p0, LX/Glv;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2391342
    iget-object v0, p1, LX/Glt;->m:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2391343
    :goto_0
    return-void

    .line 2391344
    :cond_0
    iget-object v0, p0, LX/Glv;->d:LX/Gm9;

    iget-object v0, v0, LX/Gm9;->a:Ljava/lang/String;

    .line 2391345
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2391346
    iget-object v1, p1, LX/Glt;->m:Lcom/facebook/widget/text/BetterTextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2391347
    iget-object v1, p1, LX/Glt;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2391348
    iget-object v1, p1, LX/Glt;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2391349
    :cond_1
    iget-object v1, p1, LX/Glt;->m:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2391350
    iget-object v0, p1, LX/Glt;->l:Lcom/facebook/widget/text/BetterTextView;

    const v1, 0x7f0833c5

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    goto :goto_0

    .line 2391351
    :cond_2
    check-cast p1, LX/Glu;

    .line 2391352
    iget-object v0, p1, LX/Glu;->l:LX/Gm7;

    .line 2391353
    add-int/lit8 v1, p2, -0x1

    move v1, v1

    .line 2391354
    add-int/lit8 v2, p2, -0x1

    move v2, v2

    .line 2391355
    if-gez v2, :cond_3

    .line 2391356
    const/4 v2, 0x0

    .line 2391357
    :goto_1
    move-object v2, v2

    .line 2391358
    const-class v3, LX/Gm7;

    invoke-static {v3, v0}, LX/Gm7;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2391359
    iput v1, v0, LX/Gm7;->b:I

    .line 2391360
    iput-object v2, v0, LX/Gm7;->c:LX/GmD;

    .line 2391361
    iget-object v3, v0, LX/Gm7;->e:Lcom/facebook/widget/text/BetterTextView;

    iget-object v4, v2, LX/GmD;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2391362
    const/4 v2, 0x0

    const/16 v5, 0x8

    const/4 v1, 0x0

    .line 2391363
    iget-object v3, v0, LX/Gm7;->c:LX/GmD;

    iget-object v3, v3, LX/GmD;->c:LX/GmC;

    sget-object v4, LX/GmC;->UNINVITED:LX/GmC;

    if-ne v3, v4, :cond_4

    .line 2391364
    iget-object v3, v0, LX/Gm7;->g:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v3, v1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2391365
    iget-object v3, v0, LX/Gm7;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2391366
    iget-object v3, v0, LX/Gm7;->g:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0}, LX/Gm7;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0833d2

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2391367
    iget-object v3, v0, LX/Gm7;->g:Lcom/facebook/fig/button/FigButton;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2391368
    iget-object v3, v0, LX/Gm7;->g:Lcom/facebook/fig/button/FigButton;

    new-instance v4, LX/Gm3;

    invoke-direct {v4, v0}, LX/Gm3;-><init>(LX/Gm7;)V

    invoke-virtual {v3, v4}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2391369
    :goto_2
    sget-object v3, LX/Gm6;->a:[I

    iget-object v4, v0, LX/Gm7;->c:LX/GmD;

    iget-object v4, v4, LX/GmD;->c:LX/GmC;

    invoke-virtual {v4}, LX/GmC;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2391370
    :goto_3
    iget-object v0, p1, LX/Glu;->l:LX/Gm7;

    iget-object v1, p0, LX/Glv;->a:Lcom/facebook/growth/contactinviter/ContactInviterFragment;

    .line 2391371
    iput-object v1, v0, LX/Gm7;->d:Lcom/facebook/growth/contactinviter/ContactInviterFragment;

    .line 2391372
    goto/16 :goto_0

    :cond_3
    iget-object v2, p0, LX/Glv;->c:Ljava/util/List;

    .line 2391373
    add-int/lit8 v3, p2, -0x1

    move v3, v3

    .line 2391374
    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/GmD;

    goto :goto_1

    .line 2391375
    :cond_4
    iget-object v3, v0, LX/Gm7;->c:LX/GmD;

    iget-object v3, v3, LX/GmD;->c:LX/GmC;

    sget-object v4, LX/GmC;->PENDING:LX/GmC;

    if-ne v3, v4, :cond_5

    .line 2391376
    iget-object v3, v0, LX/Gm7;->g:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v3, v5}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2391377
    iget-object v3, v0, LX/Gm7;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2391378
    iget-object v3, v0, LX/Gm7;->g:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v3, v2}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 2391379
    :cond_5
    iget-object v3, v0, LX/Gm7;->g:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v3, v1}, Lcom/facebook/fig/button/FigButton;->setVisibility(I)V

    .line 2391380
    iget-object v3, v0, LX/Gm7;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v3, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2391381
    iget-object v3, v0, LX/Gm7;->g:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0}, LX/Gm7;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0833d3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2391382
    iget-object v3, v0, LX/Gm7;->g:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v3, v1}, Lcom/facebook/fig/button/FigButton;->setEnabled(Z)V

    .line 2391383
    iget-object v3, v0, LX/Gm7;->g:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v3, v2}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 2391384
    :pswitch_0
    iget-object v3, v0, LX/Gm7;->f:Lcom/facebook/widget/text/BetterTextView;

    iget-object v4, v0, LX/Gm7;->c:LX/GmD;

    iget-object v4, v4, LX/GmD;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2391385
    iget-object v3, v0, LX/Gm7;->f:Lcom/facebook/widget/text/BetterTextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_3

    .line 2391386
    :pswitch_1
    iget-object v3, v0, LX/Gm7;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2391387
    invoke-virtual {v0}, LX/Gm7;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0833d4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2391388
    invoke-virtual {v0}, LX/Gm7;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p2, 0x7f0833d6

    invoke-virtual {v5, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2391389
    new-instance p2, LX/47x;

    invoke-virtual {v0}, LX/Gm7;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {p2, v1}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2391390
    invoke-virtual {p2, v4}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2391391
    const-string v4, " "

    invoke-virtual {p2, v4}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2391392
    new-instance v4, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {v0}, LX/Gm7;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a008a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v4, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 2391393
    const/16 v1, 0x21

    invoke-virtual {p2, v4, v1}, LX/47x;->a(Ljava/lang/Object;I)LX/47x;

    .line 2391394
    invoke-virtual {p2, v5}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2391395
    iget-object v4, v0, LX/Gm7;->f:Lcom/facebook/widget/text/BetterTextView;

    new-instance v5, LX/Gm4;

    invoke-direct {v5, v0}, LX/Gm4;-><init>(LX/Gm7;)V

    invoke-virtual {v4, v5}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2391396
    invoke-virtual {p2}, LX/47x;->a()LX/47x;

    move-result-object v4

    invoke-virtual {v4}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v4

    move-object v4, v4

    .line 2391397
    invoke-virtual {v3, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 2391398
    :pswitch_2
    iget-object v3, v0, LX/Gm7;->f:Lcom/facebook/widget/text/BetterTextView;

    .line 2391399
    iget-object v4, v0, LX/Gm7;->a:LX/Gm9;

    iget-object v4, v4, LX/Gm9;->b:Ljava/lang/String;

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, v0, LX/Gm7;->a:LX/Gm9;

    iget-object v4, v4, LX/Gm9;->b:Ljava/lang/String;

    .line 2391400
    :goto_4
    new-instance v5, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {v0}, LX/Gm7;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const v1, 0x7f0a008a

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    invoke-direct {v5, p2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    .line 2391401
    new-instance p2, LX/47x;

    invoke-virtual {v0}, LX/Gm7;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {p2, v1}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2391402
    const/16 v1, 0x21

    invoke-virtual {p2, v5, v1}, LX/47x;->a(Ljava/lang/Object;I)LX/47x;

    .line 2391403
    invoke-virtual {p2, v4}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2391404
    iget-object v4, v0, LX/Gm7;->f:Lcom/facebook/widget/text/BetterTextView;

    new-instance v5, LX/Gm5;

    invoke-direct {v5, v0}, LX/Gm5;-><init>(LX/Gm7;)V

    invoke-virtual {v4, v5}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2391405
    invoke-virtual {p2}, LX/47x;->a()LX/47x;

    move-result-object v4

    invoke-virtual {v4}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v4

    move-object v4, v4

    .line 2391406
    invoke-virtual {v3, v4}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 2391407
    :cond_6
    invoke-virtual {v0}, LX/Gm7;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0833d5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2391336
    if-nez p1, :cond_0

    .line 2391337
    const/4 v0, 0x0

    .line 2391338
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2391335
    iget-object v0, p0, LX/Glv;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method
