.class public LX/FcO;
.super LX/FcJ;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FcJ",
        "<",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        "LX/FdB;",
        "LX/FdA;",
        ">;"
    }
.end annotation


# static fields
.field public static final a:LX/FcE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/FcE",
            "<",
            "LX/FdB;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/FcE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/FcE",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation
.end field

.field private static final g:LX/FcE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/FcE",
            "<",
            "LX/FdA;",
            ">;"
        }
    .end annotation
.end field

.field private static h:LX/0Xm;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2262359
    new-instance v0, LX/FcL;

    invoke-direct {v0}, LX/FcL;-><init>()V

    sput-object v0, LX/FcO;->b:LX/FcE;

    .line 2262360
    new-instance v0, LX/FcM;

    invoke-direct {v0}, LX/FcM;-><init>()V

    sput-object v0, LX/FcO;->a:LX/FcE;

    .line 2262361
    new-instance v0, LX/FcN;

    invoke-direct {v0}, LX/FcN;-><init>()V

    sput-object v0, LX/FcO;->g:LX/FcE;

    return-void
.end method

.method public constructor <init>(LX/0wM;LX/0lC;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2262357
    invoke-direct {p0, p1, p2, p3}, LX/FcJ;-><init>(LX/0wM;LX/0lC;LX/03V;)V

    .line 2262358
    return-void
.end method

.method public static a(LX/0QB;)LX/FcO;
    .locals 6

    .prologue
    .line 2262325
    const-class v1, LX/FcO;

    monitor-enter v1

    .line 2262326
    :try_start_0
    sget-object v0, LX/FcO;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2262327
    sput-object v2, LX/FcO;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2262328
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2262329
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2262330
    new-instance p0, LX/FcO;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v3

    check-cast v3, LX/0wM;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lC;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    invoke-direct {p0, v3, v4, v5}, LX/FcO;-><init>(LX/0wM;LX/0lC;LX/03V;)V

    .line 2262331
    move-object v0, p0

    .line 2262332
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2262333
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FcO;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2262334
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2262335
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/FcE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/FcE",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262356
    sget-object v0, LX/FcO;->b:LX/FcE;

    return-object v0
.end method

.method public final a(LX/5uu;Landroid/view/View;LX/CyH;LX/FdQ;)V
    .locals 4

    .prologue
    .line 2262346
    check-cast p2, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2262347
    invoke-interface {p1}, LX/5uu;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2262348
    invoke-interface {p1}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v2

    .line 2262349
    if-nez v2, :cond_0

    .line 2262350
    :goto_0
    return-void

    .line 2262351
    :cond_0
    sget-object v0, LX/FcJ;->c:LX/0P1;

    invoke-virtual {v0, v2}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2262352
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unimplemented filter "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2262353
    :cond_1
    invoke-virtual {p0}, LX/FcJ;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, -0xa76f01

    move v1, v0

    .line 2262354
    :goto_1
    iget-object v3, p0, LX/FcJ;->d:LX/0wM;

    sget-object v0, LX/FcJ;->c:LX/0P1;

    invoke-virtual {v0, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3, v0, v1}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 2262355
    :cond_2
    const v0, -0x6e685d

    move v1, v0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;Landroid/view/View;LX/CyH;Z)V
    .locals 1

    .prologue
    .line 2262339
    check-cast p2, LX/FdA;

    .line 2262340
    invoke-virtual {p1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v0

    .line 2262341
    if-nez v0, :cond_0

    const-string v0, ""

    .line 2262342
    :goto_0
    iget-object p0, p2, LX/FdA;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2262343
    invoke-virtual {p2, p4}, LX/FdA;->setChecked(Z)V

    .line 2262344
    return-void

    .line 2262345
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b()LX/FcE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/FcE",
            "<",
            "LX/FdB;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262338
    sget-object v0, LX/FcO;->a:LX/FcE;

    return-object v0
.end method

.method public final c()LX/FcE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/FcE",
            "<",
            "LX/FdA;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262337
    sget-object v0, LX/FcO;->g:LX/FcE;

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2262336
    const/4 v0, 0x5

    return v0
.end method
