.class public final LX/FGw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FHx;

.field public final synthetic b:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final synthetic c:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final synthetic d:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;LX/FHx;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 0

    .prologue
    .line 2219666
    iput-object p1, p0, LX/FGw;->d:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iput-object p2, p0, LX/FGw;->a:LX/FHx;

    iput-object p3, p0, LX/FGw;->b:Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object p4, p0, LX/FGw;->c:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2219667
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    const/4 v3, 0x0

    .line 2219668
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2219669
    if-nez v0, :cond_0

    .line 2219670
    :goto_0
    return-object v3

    .line 2219671
    :cond_0
    iget-object v1, p0, LX/FGw;->a:LX/FHx;

    sget-object v2, LX/FHx;->PHASE_ONE:LX/FHx;

    if-ne v1, v2, :cond_1

    .line 2219672
    iget-object v1, p0, LX/FGw;->d:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->h:LX/2MO;

    iget-object v2, p0, LX/FGw;->b:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v1, v2, v0}, LX/2MO;->a(Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/ui/media/attachments/MediaResource;)V

    goto :goto_0

    .line 2219673
    :cond_1
    iget-object v1, p0, LX/FGw;->d:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, v1, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->h:LX/2MO;

    iget-object v2, p0, LX/FGw;->c:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v1, v2, v0}, LX/2MO;->b(Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/ui/media/attachments/MediaResource;)V

    goto :goto_0
.end method
