.class public abstract LX/GGI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

.field public b:LX/8wL;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:LX/GGB;

.field public f:Ljava/lang/String;

.field public g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

.field public h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

.field public i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

.field public j:I

.field public k:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;",
            ">;"
        }
    .end annotation
.end field

.field public o:I

.field public p:Lcom/facebook/graphql/enums/GraphQLCallToActionType;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2333742
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2333743
    const-string v0, ""

    iput-object v0, p0, LX/GGI;->c:Ljava/lang/String;

    .line 2333744
    const/4 v0, 0x0

    iput-object v0, p0, LX/GGI;->d:Ljava/lang/String;

    .line 2333745
    const-string v0, ""

    iput-object v0, p0, LX/GGI;->f:Ljava/lang/String;

    .line 2333746
    iput v1, p0, LX/GGI;->j:I

    .line 2333747
    iput v1, p0, LX/GGI;->o:I

    .line 2333748
    return-void
.end method

.method public constructor <init>(Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2333749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2333750
    const-string v0, ""

    iput-object v0, p0, LX/GGI;->c:Ljava/lang/String;

    .line 2333751
    const/4 v0, 0x0

    iput-object v0, p0, LX/GGI;->d:Ljava/lang/String;

    .line 2333752
    const-string v0, ""

    iput-object v0, p0, LX/GGI;->f:Ljava/lang/String;

    .line 2333753
    iput v1, p0, LX/GGI;->j:I

    .line 2333754
    iput v1, p0, LX/GGI;->o:I

    .line 2333755
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v0

    iput-object v0, p0, LX/GGI;->b:LX/8wL;

    .line 2333756
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    iput-object v0, p0, LX/GGI;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    .line 2333757
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v0

    iput-object v0, p0, LX/GGI;->e:LX/GGB;

    .line 2333758
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GGI;->c:Ljava/lang/String;

    .line 2333759
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GGI;->d:Ljava/lang/String;

    .line 2333760
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GGI;->f:Ljava/lang/String;

    .line 2333761
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    iput-object v0, p0, LX/GGI;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    .line 2333762
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->f()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    iput-object v0, p0, LX/GGI;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    .line 2333763
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->g()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v0

    iput-object v0, p0, LX/GGI;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    .line 2333764
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i()I

    move-result v0

    iput v0, p0, LX/GGI;->j:I

    .line 2333765
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    iput-object v0, p0, LX/GGI;->k:Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    .line 2333766
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GGI;->l:Ljava/lang/String;

    .line 2333767
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->o()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GGI;->m:Ljava/lang/String;

    .line 2333768
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->w()I

    move-result v0

    iput v0, p0, LX/GGI;->o:I

    .line 2333769
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->v()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/GGI;->n:LX/0Px;

    .line 2333770
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->B()Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-result-object v0

    iput-object v0, p0, LX/GGI;->p:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2333771
    return-void
.end method


# virtual methods
.method public abstract a()Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method
