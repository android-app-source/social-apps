.class public final LX/Gw3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Ljava/lang/Void;",
        "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Gw8;


# direct methods
.method public constructor <init>(LX/Gw8;)V
    .locals 0

    .prologue
    .line 2406491
    iput-object p1, p0, LX/Gw3;->a:LX/Gw8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3

    .prologue
    .line 2406492
    iget-object v0, p0, LX/Gw3;->a:LX/Gw8;

    .line 2406493
    iget-object v1, v0, LX/Gw8;->n:LX/8PB;

    .line 2406494
    iget-object v2, v1, LX/8PB;->j:Ljava/lang/String;

    move-object v1, v2

    .line 2406495
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, LX/47E;->a(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2406496
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2406497
    sget-object v2, LX/21D;->THIRD_PARTY_APP_VIA_FB_API:LX/21D;

    const-string p0, "feedDialogActionExecutorVideo"

    invoke-static {v2, p0}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object p0

    iget-object p1, v0, LX/Gw8;->p:LX/74n;

    invoke-static {p0, p1}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Ljava/util/Collection;LX/74n;)LX/0Px;

    move-result-object p0

    invoke-virtual {v2, p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAttachments(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    const-string p0, "ANDROID_PLATFORM_COMPOSER"

    invoke-virtual {v2, p0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setReactionSurface(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    move-object v1, v2

    .line 2406498
    invoke-static {v1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2406499
    :goto_0
    move-object v0, v1

    .line 2406500
    return-object v0

    .line 2406501
    :cond_0
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2406502
    const-string p0, "type"

    sget-object p1, LX/4hX;->VIDEO:LX/4hX;

    invoke-virtual {p1}, LX/4hX;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2406503
    const-string p0, "uri"

    invoke-virtual {v2, p0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2406504
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/os/Bundle;

    const/4 p0, 0x0

    aput-object v2, v1, p0

    invoke-static {v1}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, LX/Gw8;->a(LX/Gw8;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_0
.end method
