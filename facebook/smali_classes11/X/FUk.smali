.class public final enum LX/FUk;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FUk;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FUk;

.field public static final enum BOOKMARK:LX/FUk;

.field public static final enum FRIENDS_CENTER:LX/FUk;

.field public static final enum SEARCH:LX/FUk;

.field public static final enum UNKNOWN:LX/FUk;


# instance fields
.field public final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2249569
    new-instance v0, LX/FUk;

    const-string v1, "BOOKMARK"

    const-string v2, "bookmark"

    invoke-direct {v0, v1, v3, v2}, LX/FUk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FUk;->BOOKMARK:LX/FUk;

    .line 2249570
    new-instance v0, LX/FUk;

    const-string v1, "FRIENDS_CENTER"

    const-string v2, "friends_center"

    invoke-direct {v0, v1, v4, v2}, LX/FUk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FUk;->FRIENDS_CENTER:LX/FUk;

    .line 2249571
    new-instance v0, LX/FUk;

    const-string v1, "SEARCH"

    const-string v2, "search"

    invoke-direct {v0, v1, v5, v2}, LX/FUk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FUk;->SEARCH:LX/FUk;

    .line 2249572
    new-instance v0, LX/FUk;

    const-string v1, "UNKNOWN"

    const-string v2, "unknown"

    invoke-direct {v0, v1, v6, v2}, LX/FUk;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FUk;->UNKNOWN:LX/FUk;

    .line 2249573
    const/4 v0, 0x4

    new-array v0, v0, [LX/FUk;

    sget-object v1, LX/FUk;->BOOKMARK:LX/FUk;

    aput-object v1, v0, v3

    sget-object v1, LX/FUk;->FRIENDS_CENTER:LX/FUk;

    aput-object v1, v0, v4

    sget-object v1, LX/FUk;->SEARCH:LX/FUk;

    aput-object v1, v0, v5

    sget-object v1, LX/FUk;->UNKNOWN:LX/FUk;

    aput-object v1, v0, v6

    sput-object v0, LX/FUk;->$VALUES:[LX/FUk;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2249574
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2249575
    iput-object p3, p0, LX/FUk;->value:Ljava/lang/String;

    .line 2249576
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/FUk;
    .locals 5

    .prologue
    .line 2249577
    invoke-static {}, LX/FUk;->values()[LX/FUk;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2249578
    iget-object v4, v0, LX/FUk;->value:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2249579
    :goto_1
    return-object v0

    .line 2249580
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2249581
    :cond_1
    sget-object v0, LX/FUk;->UNKNOWN:LX/FUk;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/FUk;
    .locals 1

    .prologue
    .line 2249582
    const-class v0, LX/FUk;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FUk;

    return-object v0
.end method

.method public static values()[LX/FUk;
    .locals 1

    .prologue
    .line 2249583
    sget-object v0, LX/FUk;->$VALUES:[LX/FUk;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FUk;

    return-object v0
.end method
