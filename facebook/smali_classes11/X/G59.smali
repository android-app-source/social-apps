.class public LX/G59;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Iterable;


# instance fields
.field public a:LX/G58;

.field private b:LX/G54;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:LX/0qm;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public e:I

.field public final f:Ljava/lang/String;

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public final k:LX/0So;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;LX/G58;ZLX/0So;LX/0qm;)V
    .locals 5
    .param p6    # LX/0qm;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2318548
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2318549
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/G59;->d:Ljava/util/List;

    .line 2318550
    iput v2, p0, LX/G59;->e:I

    .line 2318551
    iput-boolean v2, p0, LX/G59;->g:Z

    .line 2318552
    iput-boolean v2, p0, LX/G59;->h:Z

    .line 2318553
    iput-boolean v1, p0, LX/G59;->i:Z

    .line 2318554
    iput-boolean v2, p0, LX/G59;->j:Z

    .line 2318555
    iput-object p1, p0, LX/G59;->f:Ljava/lang/String;

    .line 2318556
    sget-object v0, LX/G58;->YEAR_SECTION:LX/G58;

    if-ne p3, v0, :cond_0

    .line 2318557
    iget-object v3, p0, LX/G59;->d:Ljava/util/List;

    new-instance v4, LX/G55;

    if-nez p4, :cond_3

    move v0, v1

    :goto_0
    invoke-direct {v4, p1, p2, v0}, LX/G55;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2318558
    :cond_0
    iput-object p3, p0, LX/G59;->a:LX/G58;

    .line 2318559
    sget-object v0, LX/G58;->YEAR_SECTION:LX/G58;

    if-ne p3, v0, :cond_1

    if-nez p4, :cond_2

    :cond_1
    move v2, v1

    :cond_2
    iput-boolean v2, p0, LX/G59;->i:Z

    .line 2318560
    iput-boolean p4, p0, LX/G59;->j:Z

    .line 2318561
    iput-object p5, p0, LX/G59;->k:LX/0So;

    .line 2318562
    iput-object p6, p0, LX/G59;->c:LX/0qm;

    .line 2318563
    return-void

    :cond_3
    move v0, v2

    .line 2318564
    goto :goto_0
.end method

.method public static j(LX/G59;)I
    .locals 2

    .prologue
    .line 2318545
    iget-object v0, p0, LX/G59;->a:LX/G58;

    sget-object v1, LX/G58;->RECENT_SECTION:LX/G58;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/G59;->c:LX/0qm;

    if-nez v0, :cond_1

    .line 2318546
    :cond_0
    const/4 v0, 0x0

    .line 2318547
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/G59;->c:LX/0qm;

    invoke-virtual {v0}, LX/0qm;->b()I

    move-result v0

    goto :goto_0
.end method

.method private k()I
    .locals 1

    .prologue
    .line 2318544
    iget-object v0, p0, LX/G59;->b:LX/G54;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static m(LX/G59;)V
    .locals 2

    .prologue
    .line 2318541
    :goto_0
    iget-object v0, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/G59;->d:Ljava/util/List;

    iget-object v1, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/G4y;

    if-eqz v0, :cond_0

    .line 2318542
    iget-object v0, p0, LX/G59;->d:Ljava/util/List;

    iget-object v1, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 2318543
    :cond_0
    return-void
.end method

.method public static n(LX/G59;)Z
    .locals 2

    .prologue
    .line 2318540
    iget-object v0, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/G59;->d:Ljava/util/List;

    iget-object v1, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/G4z;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 2318524
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 2318525
    iget-object v0, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 2318526
    instance-of v0, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_1

    move-object v0, v1

    .line 2318527
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2318528
    invoke-static {v0, p1}, LX/182;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2318529
    if-eqz v0, :cond_1

    .line 2318530
    :cond_0
    :goto_1
    return v2

    .line 2318531
    :cond_1
    instance-of v0, v1, LX/16n;

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 2318532
    check-cast v0, LX/16n;

    .line 2318533
    if-eqz p1, :cond_2

    instance-of v3, v1, Lcom/facebook/graphql/model/FeedUnit;

    if-eqz v3, :cond_2

    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    invoke-interface {v1}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2318534
    :cond_2
    if-eqz p2, :cond_3

    .line 2318535
    invoke-static {p2}, LX/4Zt;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2318536
    invoke-interface {v0}, LX/16n;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    .line 2318537
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2318538
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2318539
    :cond_4
    const/4 v2, -0x1

    goto :goto_1
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2318511
    iget-boolean v0, p0, LX/G59;->i:Z

    if-nez v0, :cond_0

    .line 2318512
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Requesting data while inactive. Index:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2318513
    :cond_0
    invoke-direct {p0}, LX/G59;->k()I

    move-result v0

    .line 2318514
    if-ge p1, v0, :cond_1

    .line 2318515
    iget-object v0, p0, LX/G59;->b:LX/G54;

    .line 2318516
    :goto_0
    return-object v0

    .line 2318517
    :cond_1
    sub-int v0, p1, v0

    .line 2318518
    invoke-static {p0}, LX/G59;->j(LX/G59;)I

    move-result v1

    .line 2318519
    if-ge v0, v1, :cond_2

    .line 2318520
    iget-object v1, p0, LX/G59;->c:LX/0qm;

    invoke-virtual {v1}, LX/0qm;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    .line 2318521
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    goto :goto_0

    .line 2318522
    :cond_2
    sub-int/2addr v0, v1

    .line 2318523
    iget-object v1, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/G5A;LX/Fso;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2318486
    sget-object v1, LX/G5A;->COMPLETED:LX/G5A;

    if-eq p1, v1, :cond_4

    .line 2318487
    iget-object v1, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2318488
    sget-object v1, LX/G5A;->LOADING:LX/G5A;

    if-ne p1, v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, LX/G59;->g:Z

    .line 2318489
    iget-object v0, p0, LX/G59;->d:Ljava/util/List;

    iget-object v1, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 2318490
    instance-of v1, v0, LX/G52;

    if-eqz v1, :cond_2

    .line 2318491
    check-cast v0, LX/G52;

    .line 2318492
    iput-object p1, v0, LX/G52;->c:LX/G5A;

    .line 2318493
    :cond_1
    :goto_0
    return-void

    .line 2318494
    :cond_2
    iget-object v0, p0, LX/G59;->d:Ljava/util/List;

    iget-object v1, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/G4z;

    if-eqz v0, :cond_3

    .line 2318495
    iget-object v0, p0, LX/G59;->d:Ljava/util/List;

    iget-object v1, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G4z;

    .line 2318496
    iput-object p1, v0, LX/G4z;->c:LX/G5A;

    .line 2318497
    goto :goto_0

    .line 2318498
    :cond_3
    invoke-static {p0}, LX/G59;->m(LX/G59;)V

    .line 2318499
    new-instance v0, LX/G4z;

    invoke-direct {v0, p2}, LX/G4z;-><init>(LX/Fso;)V

    .line 2318500
    iget-object v1, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2318501
    goto :goto_0

    .line 2318502
    :cond_4
    iget-object v1, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2318503
    iput-boolean v0, p0, LX/G59;->g:Z

    .line 2318504
    iget-object v0, p0, LX/G59;->d:Ljava/util/List;

    iget-object v1, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 2318505
    instance-of v1, v0, LX/G52;

    if-eqz v1, :cond_5

    .line 2318506
    check-cast v0, LX/G52;

    .line 2318507
    iput-object p1, v0, LX/G52;->c:LX/G5A;

    .line 2318508
    goto :goto_0

    .line 2318509
    :cond_5
    :goto_1
    iget-object v0, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/G59;->d:Ljava/util/List;

    iget-object v1, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, LX/G4z;

    if-eqz v0, :cond_1

    .line 2318510
    iget-object v0, p0, LX/G59;->d:Ljava/util/List;

    iget-object v1, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/graphql/enums/StoryVisibility;I)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 2318565
    invoke-virtual {p0, p1, p2}, LX/G59;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 2318566
    if-ne v2, v6, :cond_1

    .line 2318567
    :cond_0
    :goto_0
    return-void

    .line 2318568
    :cond_1
    invoke-virtual {p0, p1, p2}, LX/G59;->b(Ljava/lang/String;Ljava/lang/String;)LX/16n;

    move-result-object v0

    .line 2318569
    iget-object v1, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, v0, :cond_2

    const/4 v1, 0x1

    .line 2318570
    :goto_1
    instance-of v3, v0, Lcom/facebook/graphql/model/HideableUnit;

    if-eqz v3, :cond_0

    .line 2318571
    check-cast v0, Lcom/facebook/graphql/model/HideableUnit;

    .line 2318572
    invoke-static {v0}, LX/6X8;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/6X8;

    move-result-object v3

    iget-object v4, p0, LX/G59;->k:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    if-eq p4, v6, :cond_3

    :goto_2
    invoke-virtual {v3, v4, v5, p3, p4}, LX/6X8;->a(JLcom/facebook/graphql/enums/StoryVisibility;I)LX/6X8;

    move-result-object v0

    invoke-virtual {v0}, LX/6X8;->a()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v0

    .line 2318573
    if-eqz v1, :cond_0

    .line 2318574
    iget-object v1, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v1, v2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2318575
    :cond_2
    const/4 v1, 0x0

    goto :goto_1

    .line 2318576
    :cond_3
    invoke-interface {v0}, Lcom/facebook/graphql/model/HideableUnit;->H_()I

    move-result p4

    goto :goto_2
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 2318477
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2318478
    iget-boolean v0, p0, LX/G59;->j:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, LX/G59;->g()Z

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, LX/G59;->g:Z

    if-nez v0, :cond_3

    move v0, v1

    .line 2318479
    :goto_0
    iget-boolean v3, p0, LX/G59;->i:Z

    if-eqz v3, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    move v0, v2

    .line 2318480
    if-eqz v0, :cond_2

    .line 2318481
    const/4 v0, 0x0

    .line 2318482
    :goto_1
    return v0

    .line 2318483
    :cond_2
    invoke-direct {p0}, LX/G59;->k()I

    move-result v0

    .line 2318484
    invoke-static {p0}, LX/G59;->j(LX/G59;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    :cond_3
    move v0, v2

    .line 2318485
    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)LX/16n;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2318434
    invoke-virtual {p0, p1, p2}, LX/G59;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 2318435
    const/4 v2, -0x1

    if-eq v0, v2, :cond_2

    iget-object v2, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_2

    .line 2318436
    if-eqz p1, :cond_1

    .line 2318437
    iget-object v2, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2318438
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    invoke-static {v0, p1}, LX/182;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2318439
    if-eqz v0, :cond_0

    .line 2318440
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 2318441
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2318442
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 2318443
    goto :goto_0

    .line 2318444
    :cond_1
    iget-object v1, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 2318445
    goto :goto_0
.end method

.method public final b(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2318467
    const/4 v0, 0x0

    .line 2318468
    invoke-virtual {p0}, LX/G59;->b()I

    move-result v1

    if-ge p1, v1, :cond_2

    .line 2318469
    invoke-virtual {p0, p1}, LX/G59;->a(I)Ljava/lang/Object;

    move-result-object v1

    .line 2318470
    instance-of v2, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 2318471
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    .line 2318472
    :cond_0
    instance-of v2, v1, LX/G52;

    if-eqz v2, :cond_1

    move-object v0, v1

    .line 2318473
    check-cast v0, LX/G52;

    iget-object v0, v0, LX/G52;->a:Ljava/lang/String;

    .line 2318474
    :cond_1
    instance-of v1, v1, LX/G4y;

    if-eqz v1, :cond_2

    .line 2318475
    const-string v0, "PLACEHOLDER_ID"

    .line 2318476
    :cond_2
    return-object v0
.end method

.method public final b(LX/G4y;)V
    .locals 1

    .prologue
    .line 2318464
    invoke-static {p0}, LX/G59;->m(LX/G59;)V

    .line 2318465
    iget-object v0, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2318466
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/FeedUnit;)V
    .locals 2

    .prologue
    .line 2318458
    invoke-static {p0}, LX/G59;->m(LX/G59;)V

    .line 2318459
    iget-object v0, p0, LX/G59;->k:LX/0So;

    invoke-interface {v0}, LX/0So;->now()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, LX/16t;->a(Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;J)V

    .line 2318460
    iget-object v0, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2318461
    iget v0, p0, LX/G59;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/G59;->e:I

    .line 2318462
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/G59;->h:Z

    .line 2318463
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2318450
    iget-object v0, p0, LX/G59;->b:LX/G54;

    if-eqz v0, :cond_1

    .line 2318451
    :cond_0
    :goto_0
    return-void

    .line 2318452
    :cond_1
    iget-object v0, p0, LX/G59;->a:LX/G58;

    sget-object v1, LX/G58;->UNSEEN_SECTION:LX/G58;

    if-ne v0, v1, :cond_2

    .line 2318453
    new-instance v0, LX/G54;

    sget-object v1, LX/G58;->UNSEEN_SECTION:LX/G58;

    invoke-direct {v0, v1}, LX/G54;-><init>(LX/G58;)V

    move-object v0, v0

    .line 2318454
    iput-object v0, p0, LX/G59;->b:LX/G54;

    goto :goto_0

    .line 2318455
    :cond_2
    iget-object v0, p0, LX/G59;->a:LX/G58;

    sget-object v1, LX/G58;->RECENT_SECTION:LX/G58;

    if-ne v0, v1, :cond_0

    .line 2318456
    new-instance v0, LX/G54;

    sget-object v1, LX/G58;->RECENT_SECTION:LX/G58;

    invoke-direct {v0, v1}, LX/G54;-><init>(LX/G58;)V

    move-object v0, v0

    .line 2318457
    iput-object v0, p0, LX/G59;->b:LX/G54;

    goto :goto_0
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 2318448
    iget v0, p0, LX/G59;->e:I

    invoke-static {p0}, LX/G59;->j(LX/G59;)I

    move-result v1

    add-int/2addr v0, v1

    move v0, v0

    .line 2318449
    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 2318447
    iget-object v0, p0, LX/G59;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2318446
    iget-object v0, p0, LX/G59;->f:Ljava/lang/String;

    return-object v0
.end method
