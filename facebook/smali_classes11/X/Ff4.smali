.class public final LX/Ff4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Ff5;


# direct methods
.method public constructor <init>(LX/Ff5;)V
    .locals 0

    .prologue
    .line 2267204
    iput-object p1, p0, LX/Ff4;->a:LX/Ff5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x491eef09

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2267205
    iget-object v1, p0, LX/Ff4;->a:LX/Ff5;

    iget-object v1, v1, LX/Ff5;->a:Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->x:LX/FPa;

    .line 2267206
    iget-object v2, v1, LX/FPa;->a:LX/0Zb;

    const-string v3, "places_advanced_filters_entered"

    invoke-static {v1, v3}, LX/FPa;->a(LX/FPa;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2267207
    iget-object v1, p0, LX/Ff4;->a:LX/Ff5;

    iget-object v1, v1, LX/Ff5;->a:Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;

    invoke-static {v1}, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;->u(Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;)LX/0Px;

    move-result-object v1

    iget-object v2, p0, LX/Ff4;->a:LX/Ff5;

    iget-object v2, v2, LX/Ff5;->a:Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;

    iget-object v3, p0, LX/Ff4;->a:LX/Ff5;

    iget-object v3, v3, LX/Ff5;->a:Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;

    invoke-static {v1, v2, v3}, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->a(LX/0Px;LX/FdZ;Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;)Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    move-result-object v1

    .line 2267208
    iget-object v2, p0, LX/Ff4;->a:LX/Ff5;

    iget-object v2, v2, LX/Ff5;->a:Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;

    .line 2267209
    iget-object v3, v2, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v2, v3

    .line 2267210
    invoke-virtual {v2}, LX/0gc;->a()LX/0hH;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, LX/0hH;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)LX/0hH;

    move-result-object v1

    invoke-virtual {v1}, LX/0hH;->b()I

    .line 2267211
    const v1, -0x4003c068

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
