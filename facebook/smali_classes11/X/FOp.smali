.class public LX/FOp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final h:Ljava/lang/Object;


# instance fields
.field public final a:LX/3N0;

.field public final b:LX/6cy;

.field public final c:LX/2Ow;

.field private final d:LX/0tX;

.field private final e:LX/3MV;

.field private final f:LX/3MW;

.field public g:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/cache/FacebookMessages;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2235908
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/FOp;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/3N0;LX/6cy;LX/2Ow;LX/0tX;LX/3MV;LX/3MW;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2235972
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2235973
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2235974
    iput-object v0, p0, LX/FOp;->g:LX/0Ot;

    .line 2235975
    iput-object p1, p0, LX/FOp;->a:LX/3N0;

    .line 2235976
    iput-object p2, p0, LX/FOp;->b:LX/6cy;

    .line 2235977
    iput-object p3, p0, LX/FOp;->c:LX/2Ow;

    .line 2235978
    iput-object p4, p0, LX/FOp;->d:LX/0tX;

    .line 2235979
    iput-object p5, p0, LX/FOp;->e:LX/3MV;

    .line 2235980
    iput-object p6, p0, LX/FOp;->f:LX/3MW;

    .line 2235981
    return-void
.end method

.method public static a(LX/0QB;)LX/FOp;
    .locals 14

    .prologue
    .line 2235941
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2235942
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2235943
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2235944
    if-nez v1, :cond_0

    .line 2235945
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2235946
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2235947
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2235948
    sget-object v1, LX/FOp;->h:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2235949
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2235950
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2235951
    :cond_1
    if-nez v1, :cond_4

    .line 2235952
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2235953
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2235954
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2235955
    new-instance v7, LX/FOp;

    invoke-static {v0}, LX/3N0;->a(LX/0QB;)LX/3N0;

    move-result-object v8

    check-cast v8, LX/3N0;

    invoke-static {v0}, LX/6cy;->a(LX/0QB;)LX/6cy;

    move-result-object v9

    check-cast v9, LX/6cy;

    invoke-static {v0}, LX/2Ow;->a(LX/0QB;)LX/2Ow;

    move-result-object v10

    check-cast v10, LX/2Ow;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v11

    check-cast v11, LX/0tX;

    invoke-static {v0}, LX/3MV;->a(LX/0QB;)LX/3MV;

    move-result-object v12

    check-cast v12, LX/3MV;

    invoke-static {v0}, LX/3MW;->b(LX/0QB;)LX/3MW;

    move-result-object v13

    check-cast v13, LX/3MW;

    invoke-direct/range {v7 .. v13}, LX/FOp;-><init>(LX/3N0;LX/6cy;LX/2Ow;LX/0tX;LX/3MV;LX/3MW;)V

    .line 2235956
    const/16 v8, 0xce5

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    .line 2235957
    iput-object v8, v7, LX/FOp;->g:LX/0Ot;

    .line 2235958
    move-object v1, v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2235959
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2235960
    if-nez v1, :cond_2

    .line 2235961
    sget-object v0, LX/FOp;->h:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FOp;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2235962
    :goto_1
    if-eqz v0, :cond_3

    .line 2235963
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2235964
    :goto_3
    check-cast v0, LX/FOp;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2235965
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2235966
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2235967
    :catchall_1
    move-exception v0

    .line 2235968
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2235969
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2235970
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2235971
    :cond_2
    :try_start_8
    sget-object v0, LX/FOp;->h:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FOp;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method public static a(LX/FOp;J)Landroid/util/Pair;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;>;"
        }
    .end annotation

    .prologue
    const v7, -0x3d37bcb0

    const/4 v6, 0x0

    .line 2235909
    new-instance v0, LX/0v6;

    const-string v1, "UserRefreshBatch"

    invoke-direct {v0, v1}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 2235910
    const-wide/16 v2, 0x1

    add-long/2addr v2, p1

    .line 2235911
    new-instance v1, LX/FOq;

    invoke-direct {v1}, LX/FOq;-><init>()V

    move-object v1, v1

    .line 2235912
    const-string v4, "count"

    const/16 v5, 0x14

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2235913
    const-string v4, "after_time_ms"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2235914
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    .line 2235915
    invoke-virtual {v0, v1}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2235916
    const-string v3, "user_id"

    sget-object v4, LX/4Zz;->ALL:LX/4Zz;

    sget-object v5, LX/4a0;->SKIP:LX/4a0;

    invoke-virtual {v1, v3, v4, v5}, LX/0zO;->a(Ljava/lang/String;LX/4Zz;LX/4a0;)LX/4a1;

    move-result-object v1

    .line 2235917
    iget-object v3, p0, LX/FOp;->f:LX/3MW;

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, LX/3MW;->a(LX/4a1;Lcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v1

    .line 2235918
    invoke-virtual {v0, v1}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2235919
    iget-object v3, p0, LX/FOp;->d:LX/0tX;

    invoke-virtual {v3, v0}, LX/0tX;->a(LX/0v6;)V

    .line 2235920
    const v0, 0x1888f113

    invoke-static {v1, v0}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-virtual {v0}, Lcom/facebook/graphql/executor/GraphQLResult;->d()Ljava/util/Collection;

    move-result-object v0

    .line 2235921
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    .line 2235922
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2235923
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/UserInfoModels$UserInfoModel;

    .line 2235924
    iget-object v4, p0, LX/FOp;->e:LX/3MV;

    invoke-virtual {v4, v0}, LX/3MV;->a(LX/5Vu;)Lcom/facebook/user/model/User;

    move-result-object v0

    .line 2235925
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2235926
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2235927
    const v0, 0x1747aef4

    invoke-static {v2, v0}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2235928
    iget-object v2, v0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v2

    .line 2235929
    check-cast v0, Lcom/facebook/messaging/users/refresh/graphql/MostRecentConversationUserQueriesModels$UsersAfterTimeQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/users/refresh/graphql/MostRecentConversationUserQueriesModels$UsersAfterTimeQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    .line 2235930
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2235931
    const v0, 0x4ba15f3a    # 2.1151348E7f

    invoke-static {v2, v3, v0}, LX/3Sm;->a(LX/15i;II)LX/1vs;

    .line 2235932
    invoke-static {v2, v3, v6, v7}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2235933
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2235934
    :goto_2
    new-instance v0, Landroid/util/Pair;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    .line 2235935
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2235936
    :cond_1
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_1

    .line 2235937
    :cond_2
    invoke-static {v2, v3, v6, v7}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    invoke-virtual {v0, v6}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2235938
    const/4 v3, 0x2

    invoke-virtual {v2, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2235939
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide p1

    goto :goto_2

    .line 2235940
    :cond_3
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_3
.end method
