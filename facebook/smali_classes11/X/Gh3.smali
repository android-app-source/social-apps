.class public final LX/Gh3;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 26

    .prologue
    .line 2383023
    const/16 v20, 0x0

    .line 2383024
    const/16 v19, 0x0

    .line 2383025
    const/16 v18, 0x0

    .line 2383026
    const/16 v17, 0x0

    .line 2383027
    const/16 v16, 0x0

    .line 2383028
    const/4 v15, 0x0

    .line 2383029
    const/4 v14, 0x0

    .line 2383030
    const/4 v13, 0x0

    .line 2383031
    const/4 v12, 0x0

    .line 2383032
    const/4 v11, 0x0

    .line 2383033
    const/4 v10, 0x0

    .line 2383034
    const-wide/16 v8, 0x0

    .line 2383035
    const/4 v7, 0x0

    .line 2383036
    const/4 v6, 0x0

    .line 2383037
    const/4 v5, 0x0

    .line 2383038
    const/4 v4, 0x0

    .line 2383039
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_12

    .line 2383040
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2383041
    const/4 v4, 0x0

    .line 2383042
    :goto_0
    return v4

    .line 2383043
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2383044
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v21

    sget-object v22, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v21

    move-object/from16 v1, v22

    if-eq v0, v1, :cond_d

    .line 2383045
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v21

    .line 2383046
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2383047
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v22

    sget-object v23, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v22

    move-object/from16 v1, v23

    if-eq v0, v1, :cond_1

    if-eqz v21, :cond_1

    .line 2383048
    const-string v22, "active_team_with_ball"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_2

    .line 2383049
    invoke-static/range {p0 .. p1}, LX/Gh4;->a(LX/15w;LX/186;)I

    move-result v20

    goto :goto_1

    .line 2383050
    :cond_2
    const-string v22, "first_team_object"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_3

    .line 2383051
    invoke-static/range {p0 .. p1}, LX/Gh0;->a(LX/15w;LX/186;)I

    move-result v19

    goto :goto_1

    .line 2383052
    :cond_3
    const-string v22, "first_team_primary_color"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_4

    .line 2383053
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    goto :goto_1

    .line 2383054
    :cond_4
    const-string v22, "first_team_score"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_5

    .line 2383055
    const/4 v9, 0x1

    .line 2383056
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v17

    goto :goto_1

    .line 2383057
    :cond_5
    const-string v22, "has_match_started"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_6

    .line 2383058
    const/4 v8, 0x1

    .line 2383059
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    goto :goto_1

    .line 2383060
    :cond_6
    const-string v22, "id"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_7

    .line 2383061
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto :goto_1

    .line 2383062
    :cond_7
    const-string v22, "second_team_object"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_8

    .line 2383063
    invoke-static/range {p0 .. p1}, LX/Gh2;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 2383064
    :cond_8
    const-string v22, "second_team_primary_color"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_9

    .line 2383065
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_1

    .line 2383066
    :cond_9
    const-string v22, "second_team_score"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_a

    .line 2383067
    const/4 v5, 0x1

    .line 2383068
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v12

    goto/16 :goto_1

    .line 2383069
    :cond_a
    const-string v22, "status"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_b

    .line 2383070
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 2383071
    :cond_b
    const-string v22, "status_text"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v22

    if-eqz v22, :cond_c

    .line 2383072
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 2383073
    :cond_c
    const-string v22, "updated_time"

    invoke-virtual/range {v21 .. v22}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v21

    if-eqz v21, :cond_0

    .line 2383074
    const/4 v4, 0x1

    .line 2383075
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    goto/16 :goto_1

    .line 2383076
    :cond_d
    const/16 v21, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2383077
    const/16 v21, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2383078
    const/16 v20, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2383079
    const/16 v19, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2383080
    if-eqz v9, :cond_e

    .line 2383081
    const/4 v9, 0x3

    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v9, v1, v2}, LX/186;->a(III)V

    .line 2383082
    :cond_e
    if-eqz v8, :cond_f

    .line 2383083
    const/4 v8, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v8, v1}, LX/186;->a(IZ)V

    .line 2383084
    :cond_f
    const/4 v8, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v15}, LX/186;->b(II)V

    .line 2383085
    const/4 v8, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v14}, LX/186;->b(II)V

    .line 2383086
    const/4 v8, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v13}, LX/186;->b(II)V

    .line 2383087
    if-eqz v5, :cond_10

    .line 2383088
    const/16 v5, 0x8

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v12, v8}, LX/186;->a(III)V

    .line 2383089
    :cond_10
    const/16 v5, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v11}, LX/186;->b(II)V

    .line 2383090
    const/16 v5, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v10}, LX/186;->b(II)V

    .line 2383091
    if-eqz v4, :cond_11

    .line 2383092
    const/16 v5, 0xb

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 2383093
    :cond_11
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0

    :cond_12
    move/from16 v24, v6

    move/from16 v25, v7

    move-wide v6, v8

    move/from16 v8, v24

    move/from16 v9, v25

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 2383094
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2383095
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2383096
    if-eqz v0, :cond_0

    .line 2383097
    const-string v1, "active_team_with_ball"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383098
    invoke-static {p0, v0, p2}, LX/Gh4;->a(LX/15i;ILX/0nX;)V

    .line 2383099
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2383100
    if-eqz v0, :cond_1

    .line 2383101
    const-string v1, "first_team_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383102
    invoke-static {p0, v0, p2, p3}, LX/Gh0;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2383103
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2383104
    if-eqz v0, :cond_2

    .line 2383105
    const-string v1, "first_team_primary_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383106
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2383107
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2383108
    if-eqz v0, :cond_3

    .line 2383109
    const-string v1, "first_team_score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383110
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2383111
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2383112
    if-eqz v0, :cond_4

    .line 2383113
    const-string v1, "has_match_started"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383114
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2383115
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2383116
    if-eqz v0, :cond_5

    .line 2383117
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383118
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2383119
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2383120
    if-eqz v0, :cond_6

    .line 2383121
    const-string v1, "second_team_object"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383122
    invoke-static {p0, v0, p2, p3}, LX/Gh2;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2383123
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2383124
    if-eqz v0, :cond_7

    .line 2383125
    const-string v1, "second_team_primary_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383126
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2383127
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2383128
    if-eqz v0, :cond_8

    .line 2383129
    const-string v1, "second_team_score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383130
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2383131
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2383132
    if-eqz v0, :cond_9

    .line 2383133
    const-string v1, "status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383134
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2383135
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2383136
    if-eqz v0, :cond_a

    .line 2383137
    const-string v1, "status_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383138
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2383139
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2383140
    cmp-long v2, v0, v4

    if-eqz v2, :cond_b

    .line 2383141
    const-string v2, "updated_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383142
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2383143
    :cond_b
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2383144
    return-void
.end method
