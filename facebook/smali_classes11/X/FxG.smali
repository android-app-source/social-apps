.class public final LX/FxG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Landroid/support/v4/app/Fragment;

.field public final synthetic b:LX/FxI;


# direct methods
.method public constructor <init>(LX/FxI;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 2304623
    iput-object p1, p0, LX/FxG;->b:LX/FxI;

    iput-object p2, p0, LX/FxG;->a:Landroid/support/v4/app/Fragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 11

    .prologue
    .line 2304624
    iget-object v0, p0, LX/FxG;->b:LX/FxI;

    iget-object v0, v0, LX/FxI;->d:LX/BQ9;

    iget-object v1, p0, LX/FxG;->b:LX/FxI;

    iget-wide v2, v1, LX/FxI;->a:J

    .line 2304625
    const/4 v8, 0x0

    sget-object v9, LX/9lQ;->SELF:LX/9lQ;

    const-string v10, "fav_photos_upload_click"

    move-object v5, v0

    move-wide v6, v2

    invoke-static/range {v5 .. v10}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2304626
    if-eqz v4, :cond_0

    .line 2304627
    iget-object v5, v0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2304628
    :cond_0
    iget-object v0, p0, LX/FxG;->b:LX/FxI;

    iget-object v1, p0, LX/FxG;->a:Landroid/support/v4/app/Fragment;

    .line 2304629
    iget-object v2, v0, LX/FxI;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    .line 2304630
    iget-object v3, v0, LX/FxI;->j:Landroid/content/Intent;

    if-nez v3, :cond_1

    .line 2304631
    iget-object v3, v0, LX/FxI;->h:Landroid/content/Context;

    new-instance v4, LX/8AA;

    sget-object v5, LX/8AB;->FAVORITE_PHOTOS:LX/8AB;

    invoke-direct {v4, v5}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v4}, LX/8AA;->i()LX/8AA;

    move-result-object v4

    invoke-virtual {v4}, LX/8AA;->j()LX/8AA;

    move-result-object v4

    sget-object v5, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v4, v5}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v3

    iput-object v3, v0, LX/FxI;->j:Landroid/content/Intent;

    .line 2304632
    :cond_1
    iget-object v3, v0, LX/FxI;->j:Landroid/content/Intent;

    move-object v3, v3

    .line 2304633
    const/4 v4, 0x2

    invoke-interface {v2, v3, v4, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2304634
    const/4 v0, 0x1

    return v0
.end method
