.class public LX/FK4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/messaging/model/threads/NotificationSetting;

.field public final b:Lcom/facebook/messaging/model/threads/NotificationSetting;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/model/threads/NotificationSetting;Lcom/facebook/messaging/model/threads/NotificationSetting;)V
    .locals 0

    .prologue
    .line 2224472
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2224473
    iput-object p1, p0, LX/FK4;->a:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 2224474
    iput-object p2, p0, LX/FK4;->b:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 2224475
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2224476
    iget-object v1, p0, LX/FK4;->a:Lcom/facebook/messaging/model/threads/NotificationSetting;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/FK4;->b:Lcom/facebook/messaging/model/threads/NotificationSetting;

    if-nez v1, :cond_1

    .line 2224477
    :cond_0
    :goto_0
    return v0

    .line 2224478
    :cond_1
    iget-object v1, p0, LX/FK4;->a:Lcom/facebook/messaging/model/threads/NotificationSetting;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/NotificationSetting;->a()J

    move-result-wide v2

    iget-object v1, p0, LX/FK4;->b:Lcom/facebook/messaging/model/threads/NotificationSetting;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/NotificationSetting;->a()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 2224479
    const/4 v0, 0x1

    goto :goto_0
.end method
