.class public LX/Gqe;
.super LX/CpT;
.source ""


# instance fields
.field public A:LX/Go0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public B:LX/Gqx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/GnF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/CIi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/Gn1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private H:LX/Gps;

.field public I:Z

.field public J:LX/Grc;

.field public K:Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;

.field public L:Z

.field public M:Z

.field public N:Z

.field public O:Z

.field public P:Z

.field private Q:Z

.field public R:LX/GrZ;

.field public S:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/GrY;",
            ">;"
        }
    .end annotation
.end field

.field public a:LX/CIb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public z:LX/1Bv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/Ctg;Landroid/view/View;)V
    .locals 9

    .prologue
    .line 2396778
    invoke-direct {p0, p1, p2}, LX/CpT;-><init>(LX/Ctg;Landroid/view/View;)V

    .line 2396779
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/Gqe;

    invoke-static {v0}, LX/CIb;->a(LX/0QB;)LX/CIb;

    move-result-object v3

    check-cast v3, LX/CIb;

    invoke-static {v0}, LX/1Bv;->a(LX/0QB;)LX/1Bv;

    move-result-object v4

    check-cast v4, LX/1Bv;

    invoke-static {v0}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object v5

    check-cast v5, LX/Go0;

    invoke-static {v0}, LX/Gqx;->b(LX/0QB;)LX/Gqx;

    move-result-object v6

    check-cast v6, LX/Gqx;

    invoke-static {v0}, LX/GnF;->a(LX/0QB;)LX/GnF;

    move-result-object v7

    check-cast v7, LX/GnF;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object p1

    check-cast p1, LX/0Uh;

    invoke-static {v0}, LX/CIi;->a(LX/0QB;)LX/CIi;

    move-result-object p2

    check-cast p2, LX/CIi;

    invoke-static {v0}, LX/Gn1;->b(LX/0QB;)LX/Gn1;

    move-result-object v0

    check-cast v0, LX/Gn1;

    iput-object v3, v2, LX/Gqe;->a:LX/CIb;

    iput-object v4, v2, LX/Gqe;->z:LX/1Bv;

    iput-object v5, v2, LX/Gqe;->A:LX/Go0;

    iput-object v6, v2, LX/Gqe;->B:LX/Gqx;

    iput-object v7, v2, LX/Gqe;->C:LX/GnF;

    iput-object v8, v2, LX/Gqe;->D:LX/0ad;

    iput-object p1, v2, LX/Gqe;->E:LX/0Uh;

    iput-object p2, v2, LX/Gqe;->F:LX/CIi;

    iput-object v0, v2, LX/Gqe;->G:LX/Gn1;

    .line 2396780
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2396781
    iget-object v0, p0, LX/Gqe;->F:LX/CIi;

    sget-object v3, LX/Gn0;->f:LX/0Tn;

    invoke-virtual {v0, v3}, LX/CIi;->a(LX/0Tn;)Z

    move-result v0

    .line 2396782
    iget-object v3, p0, LX/Gqe;->F:LX/CIi;

    invoke-static {p0}, LX/Gqe;->n(LX/Gqe;)Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoPlayer;

    move-result-object v4

    sget-object v5, LX/Gn0;->f:LX/0Tn;

    invoke-virtual {v3, v4, v5}, LX/CIi;->a(LX/0dN;LX/0Tn;)V

    .line 2396783
    iget-object v3, p0, LX/Gqe;->G:LX/Gn1;

    invoke-virtual {v3}, LX/Chj;->b()LX/ChL;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2396784
    iget-object v3, p0, LX/Gqe;->G:LX/Gn1;

    invoke-virtual {v3}, LX/Chj;->b()LX/ChL;

    move-result-object v3

    invoke-interface {v3}, LX/ChL;->k()LX/ChZ;

    move-result-object v3

    sget-object v4, LX/ChZ;->ON:LX/ChZ;

    if-ne v3, v4, :cond_1

    move v0, v1

    .line 2396785
    :cond_0
    :goto_0
    invoke-static {p0}, LX/Gqe;->n(LX/Gqe;)Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoPlayer;

    move-result-object v3

    if-nez v0, :cond_2

    :goto_1
    sget-object v0, LX/04g;->BY_ANDROID:LX/04g;

    invoke-virtual {v3, v1, v0}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 2396786
    return-void

    .line 2396787
    :cond_1
    iget-object v3, p0, LX/Gqe;->G:LX/Gn1;

    invoke-virtual {v3}, LX/Chj;->b()LX/ChL;

    move-result-object v3

    invoke-interface {v3}, LX/ChL;->k()LX/ChZ;

    move-result-object v3

    sget-object v4, LX/ChZ;->OFF:LX/ChZ;

    if-ne v3, v4, :cond_0

    move v0, v2

    .line 2396788
    goto :goto_0

    :cond_2
    move v1, v2

    .line 2396789
    goto :goto_1
.end method

.method public static a(LX/15i;I)Landroid/graphics/RectF;
    .locals 6

    .prologue
    .line 2396775
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->l(II)D

    move-result-wide v0

    double-to-float v0, v0

    .line 2396776
    const/4 v1, 0x3

    invoke-virtual {p0, p1, v1}, LX/15i;->l(II)D

    move-result-wide v2

    double-to-float v1, v2

    .line 2396777
    new-instance v2, Landroid/graphics/RectF;

    const/4 v3, 0x1

    invoke-virtual {p0, p1, v3}, LX/15i;->l(II)D

    move-result-wide v4

    double-to-float v3, v4

    sub-float/2addr v3, v0

    const/4 v4, 0x0

    invoke-virtual {p0, p1, v4}, LX/15i;->l(II)D

    move-result-wide v4

    double-to-float v4, v4

    sub-float/2addr v4, v1

    invoke-direct {v2, v0, v1, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v2
.end method

.method public static m(LX/Gqe;)V
    .locals 2

    .prologue
    .line 2396709
    new-instance v0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;

    .line 2396710
    iget-object v1, p0, LX/Cos;->a:LX/Ctg;

    move-object v1, v1

    .line 2396711
    invoke-direct {v0, v1}, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;-><init>(LX/Ctg;)V

    iput-object v0, p0, LX/Gqe;->K:Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;

    .line 2396712
    iget-object v0, p0, LX/Gqe;->K:Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 2396713
    return-void
.end method

.method public static n(LX/Gqe;)Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoPlayer;
    .locals 1

    .prologue
    .line 2396774
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoPlayer;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/video/engine/VideoPlayerParams;DLX/0P2;)LX/2pa;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/video/engine/VideoPlayerParams;",
            "D",
            "LX/0P2",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)",
            "LX/2pa;"
        }
    .end annotation

    .prologue
    .line 2396759
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/2oH;->a(Lcom/facebook/video/engine/VideoPlayerParams;)LX/2oH;

    move-result-object v1

    iget-object v0, p0, LX/Gqe;->a:LX/CIb;

    .line 2396760
    iget-object p0, v0, LX/CIb;->c:LX/0lF;

    move-object v0, p0

    .line 2396761
    check-cast v0, LX/162;

    .line 2396762
    iput-object v0, v1, LX/2oH;->e:LX/162;

    .line 2396763
    move-object v0, v1

    .line 2396764
    const/4 v1, 0x1

    .line 2396765
    iput-boolean v1, v0, LX/2oH;->f:Z

    .line 2396766
    move-object v0, v0

    .line 2396767
    invoke-virtual {v0}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v0

    .line 2396768
    new-instance v1, LX/2pZ;

    invoke-direct {v1}, LX/2pZ;-><init>()V

    .line 2396769
    iput-object v0, v1, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 2396770
    move-object v0, v1

    .line 2396771
    iput-wide p2, v0, LX/2pZ;->e:D

    .line 2396772
    move-object v0, v0

    .line 2396773
    invoke-virtual {p4}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v0

    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Ctg;LX/CrN;Z)LX/CqX;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 2396758
    iget-object v0, p0, LX/Gqe;->B:LX/Gqx;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-boolean v1, p0, LX/Gqe;->I:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Gqe;->D:LX/0ad;

    sget-short v3, LX/CHN;->k:S

    invoke-interface {v1, v3, v5}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v5, 0x1

    :cond_0
    iget-boolean v6, p0, LX/Gqe;->N:Z

    move-object v1, p2

    move-object v3, p1

    move v4, p3

    invoke-virtual/range {v0 .. v6}, LX/Gqx;->a(LX/CrN;Landroid/content/Context;LX/Ctg;ZZZ)LX/CqX;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Uh;)LX/Cso;
    .locals 1

    .prologue
    .line 2396755
    iget-object v0, p0, LX/Gqe;->H:LX/Gps;

    if-nez v0, :cond_0

    .line 2396756
    new-instance v0, LX/Gps;

    invoke-direct {v0, p1}, LX/Gps;-><init>(LX/0Uh;)V

    iput-object v0, p0, LX/Gqe;->H:LX/Gps;

    .line 2396757
    :cond_0
    iget-object v0, p0, LX/Gqe;->H:LX/Gps;

    return-object v0
.end method

.method public final a(LX/Cli;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2396736
    iget-boolean v0, p1, LX/Cli;->i:Z

    move v0, v0

    .line 2396737
    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/Gqe;->O:Z

    if-eqz v0, :cond_0

    .line 2396738
    const/4 v0, 0x1

    .line 2396739
    iput-boolean v0, p0, LX/CpT;->ae:Z

    .line 2396740
    :cond_0
    invoke-super {p0, p1, p2}, LX/CpT;->a(LX/Cli;Ljava/lang/String;)V

    .line 2396741
    iget-boolean v0, p1, LX/Cli;->i:Z

    move v0, v0

    .line 2396742
    iput-boolean v0, p0, LX/Gqe;->Q:Z

    .line 2396743
    iget-boolean v0, p0, LX/Gqe;->Q:Z

    if-eqz v0, :cond_1

    .line 2396744
    const-class v0, LX/CuI;

    invoke-virtual {p0, v0}, LX/Cos;->b(Ljava/lang/Class;)V

    .line 2396745
    invoke-static {p0}, LX/Gqe;->n(LX/Gqe;)Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoPlayer;

    move-result-object v0

    .line 2396746
    new-instance v1, LX/CuX;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {v1, p1}, LX/CuX;-><init>(Landroid/content/Context;)V

    .line 2396747
    invoke-static {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->d(Lcom/facebook/video/player/RichVideoPlayer;LX/2oy;)V

    .line 2396748
    const-class p1, LX/Gre;

    invoke-virtual {p0, p1}, LX/Cos;->a(Ljava/lang/Class;)LX/Ctr;

    move-result-object p1

    if-nez p1, :cond_1

    .line 2396749
    new-instance p1, LX/Gre;

    .line 2396750
    iget-object p2, p0, LX/Cos;->a:LX/Ctg;

    move-object p2, p2

    .line 2396751
    invoke-direct {p1, p2, v1}, LX/Gre;-><init>(LX/Ctg;LX/CuX;)V

    invoke-virtual {p0, p1}, LX/Cos;->a(LX/Ctr;)V

    .line 2396752
    :cond_1
    invoke-static {p0}, LX/Gqe;->n(LX/Gqe;)Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoPlayer;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Gqe;->z:LX/1Bv;

    invoke-virtual {v0}, LX/1Bv;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2396753
    invoke-static {p0}, LX/Gqe;->n(LX/Gqe;)Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoPlayer;

    move-result-object v0

    invoke-static {p0}, LX/Gqe;->n(LX/Gqe;)Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoPlayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/video/player/RichVideoPlayer;->k()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;->b(Z)V

    .line 2396754
    :cond_2
    return-void
.end method

.method public final a(LX/Ctg;)V
    .locals 1

    .prologue
    .line 2396790
    const/4 v0, 0x1

    .line 2396791
    iput-boolean v0, p0, LX/CpT;->v:Z

    .line 2396792
    invoke-super {p0, p1}, LX/CpT;->a(LX/Ctg;)V

    .line 2396793
    new-instance v0, LX/Grc;

    invoke-direct {v0, p1}, LX/Grc;-><init>(LX/Ctg;)V

    iput-object v0, p0, LX/Gqe;->J:LX/Grc;

    .line 2396794
    iget-object v0, p0, LX/Gqe;->J:LX/Grc;

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 2396795
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2396726
    iget-object v0, p0, LX/Gqe;->J:LX/Grc;

    invoke-virtual {v0}, LX/Grc;->a()V

    .line 2396727
    iget-object v0, p0, LX/Gqe;->K:Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;

    if-eqz v0, :cond_0

    .line 2396728
    iget-object v0, p0, LX/Gqe;->K:Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->k()V

    .line 2396729
    :cond_0
    iget-object v0, p0, LX/Gqe;->R:LX/GrZ;

    if-eqz v0, :cond_1

    .line 2396730
    iget-object v0, p0, LX/Gqe;->R:LX/GrZ;

    invoke-virtual {v0}, LX/GrZ;->a()V

    .line 2396731
    :cond_1
    invoke-super {p0, p1}, LX/CpT;->a(Landroid/os/Bundle;)V

    .line 2396732
    const/4 v0, 0x0

    .line 2396733
    iget-object p1, p0, LX/Cos;->a:LX/Ctg;

    move-object p1, p1

    .line 2396734
    invoke-interface {p1, v0}, LX/Ctg;->setOverlayBackgroundColor(I)V

    .line 2396735
    return-void
.end method

.method public final c(LX/Cli;)V
    .locals 10

    .prologue
    const/4 v4, 0x1

    .line 2396721
    iget-object v0, p0, LX/CpT;->r:LX/Cuw;

    move-object v0, v0

    .line 2396722
    invoke-static {p0}, LX/Gqe;->n(LX/Gqe;)Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoPlayer;

    move-result-object v1

    .line 2396723
    iget-object v2, p0, LX/CpT;->O:LX/Cun;

    move-object v2, v2

    .line 2396724
    iget-boolean v3, p0, LX/Gqe;->O:Z

    iget-boolean v5, p0, LX/CpT;->x:Z

    iget-boolean v6, p0, LX/Gqe;->P:Z

    iget-boolean v8, p0, LX/Gqe;->Q:Z

    iget-object v7, p0, LX/Gqe;->E:LX/0Uh;

    invoke-virtual {p0, v7}, LX/Gqe;->a(LX/0Uh;)LX/Cso;

    move-result-object v9

    move v7, v4

    invoke-virtual/range {v0 .. v9}, LX/Cuw;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/Cun;ZZZZZZLX/Cso;)V

    .line 2396725
    return-void
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 2396720
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic t()Lcom/facebook/richdocument/view/widget/RichDocumentVideoPlayer;
    .locals 1

    .prologue
    .line 2396719
    invoke-static {p0}, LX/Gqe;->n(LX/Gqe;)Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoPlayer;

    move-result-object v0

    return-object v0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 2396718
    iget-object v0, p0, LX/Gqe;->z:LX/1Bv;

    invoke-virtual {v0}, LX/1Bv;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/Gqe;->M:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final w()LX/Cud;
    .locals 2

    .prologue
    .line 2396715
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2396716
    iget-object v1, p0, LX/Cos;->a:LX/Ctg;

    move-object v1, v1

    .line 2396717
    invoke-interface {v1}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/instantshopping/view/widget/InstantShoppingVideoControlsView;->a(Landroid/content/Context;Landroid/view/ViewGroup;)LX/Cud;

    move-result-object v0

    return-object v0
.end method

.method public final x()Z
    .locals 1

    .prologue
    .line 2396714
    const/4 v0, 0x0

    return v0
.end method
