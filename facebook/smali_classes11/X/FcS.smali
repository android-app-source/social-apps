.class public LX/FcS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FcI;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/FcI",
        "<",
        "Lcom/facebook/fbui/widget/contentview/ContentView;",
        "Lcom/facebook/search/results/filters/ui/SearchResultFilterDistanceSeekbar;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/FcE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/FcE",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/FcE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/FcE",
            "<",
            "Lcom/facebook/search/results/filters/ui/SearchResultFilterDistanceSeekbar;",
            ">;"
        }
    .end annotation
.end field

.field private static f:LX/0Xm;


# instance fields
.field private final c:Landroid/content/res/Resources;

.field private final d:LX/0wM;

.field private final e:LX/0lB;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2262414
    new-instance v0, LX/FcP;

    invoke-direct {v0}, LX/FcP;-><init>()V

    sput-object v0, LX/FcS;->a:LX/FcE;

    .line 2262415
    new-instance v0, LX/FcQ;

    invoke-direct {v0}, LX/FcQ;-><init>()V

    sput-object v0, LX/FcS;->b:LX/FcE;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;LX/0lB;LX/0wM;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2262409
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2262410
    iput-object p1, p0, LX/FcS;->c:Landroid/content/res/Resources;

    .line 2262411
    iput-object p2, p0, LX/FcS;->e:LX/0lB;

    .line 2262412
    iput-object p3, p0, LX/FcS;->d:LX/0wM;

    .line 2262413
    return-void
.end method

.method private a(Ljava/lang/String;F)F
    .locals 2

    .prologue
    .line 2262402
    if-nez p1, :cond_1

    .line 2262403
    :cond_0
    :goto_0
    return p2

    .line 2262404
    :cond_1
    :try_start_0
    iget-object v0, p0, LX/FcS;->e:LX/0lB;

    const-class v1, Ljava/util/HashMap;

    invoke-virtual {v0, p1, v1}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    .line 2262405
    const-string v1, "filter_radius_km"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2262406
    const-string v1, "filter_radius_km"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F
    :try_end_0
    .catch LX/2aQ; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    const v1, 0x3fcdfeda

    div-float p2, v0, v1

    goto :goto_0

    .line 2262407
    :catch_0
    goto :goto_0

    .line 2262408
    :catch_1
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/FcS;
    .locals 6

    .prologue
    .line 2262391
    const-class v1, LX/FcS;

    monitor-enter v1

    .line 2262392
    :try_start_0
    sget-object v0, LX/FcS;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2262393
    sput-object v2, LX/FcS;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2262394
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2262395
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2262396
    new-instance p0, LX/FcS;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v4

    check-cast v4, LX/0lB;

    invoke-static {v0}, LX/0wM;->a(LX/0QB;)LX/0wM;

    move-result-object v5

    check-cast v5, LX/0wM;

    invoke-direct {p0, v3, v4, v5}, LX/FcS;-><init>(Landroid/content/res/Resources;LX/0lB;LX/0wM;)V

    .line 2262397
    move-object v0, p0

    .line 2262398
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2262399
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FcS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2262400
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2262401
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static c(F)I
    .locals 1

    .prologue
    .line 2262390
    const/high16 v0, 0x40a00000    # 5.0f

    div-float v0, p0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x5

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;)LX/CyH;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2262416
    invoke-virtual {p2}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v2, v0

    move-object v1, v3

    :goto_0
    if-ge v2, v5, :cond_0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    .line 2262417
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->c()Ljava/lang/String;

    move-result-object v6

    const-string v7, "default"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->a()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2262418
    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 2262419
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    .line 2262420
    :cond_0
    new-instance v0, LX/CyH;

    invoke-direct {v0, p1, v3, v1}, LX/CyH;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public final a()LX/FcE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/FcE",
            "<",
            "Lcom/facebook/fbui/widget/contentview/ContentView;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262389
    sget-object v0, LX/FcS;->a:LX/FcE;

    return-object v0
.end method

.method public final a(LX/CyH;)LX/FcT;
    .locals 6

    .prologue
    .line 2262384
    iget-object v0, p1, LX/CyH;->c:LX/4FP;

    move-object v0, v0

    .line 2262385
    invoke-virtual {v0}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v0

    const-string v1, "value"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/high16 v1, 0x42c80000    # 100.0f

    invoke-direct {p0, v0, v1}, LX/FcS;->a(Ljava/lang/String;F)F

    move-result v0

    .line 2262386
    new-instance v1, LX/FcT;

    iget-object v2, p0, LX/FcS;->c:Landroid/content/res/Resources;

    const v3, 0x7f08208c    # 1.80944E38f

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/FcS;->d:LX/0wM;

    const v3, 0x7f020965

    const v4, -0xa76f01

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2262387
    iget-object v3, p1, LX/CyH;->c:LX/4FP;

    move-object v3, v3

    .line 2262388
    invoke-direct {v1, v0, v2, v3}, LX/FcT;-><init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;LX/4FP;)V

    return-object v1
.end method

.method public final a(LX/5uu;Landroid/view/View;LX/CyH;LX/FdQ;)V
    .locals 3

    .prologue
    .line 2262376
    check-cast p2, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2262377
    invoke-interface {p1}, LX/5uu;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2262378
    iget-object v0, p3, LX/CyH;->c:LX/4FP;

    move-object v0, v0

    .line 2262379
    invoke-virtual {v0}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v0

    const-string v1, "value"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2262380
    if-eqz v0, :cond_0

    const-string v1, "default"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const v0, -0x6e685d

    .line 2262381
    :goto_0
    iget-object v1, p0, LX/FcS;->d:LX/0wM;

    const v2, 0x7f020965

    invoke-virtual {v1, v2, v0}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2262382
    return-void

    .line 2262383
    :cond_1
    const v0, -0xa76f01

    goto :goto_0
.end method

.method public final a(LX/5uu;Landroid/view/View;LX/CyH;LX/FdU;LX/FdQ;)V
    .locals 4

    .prologue
    .line 2262368
    check-cast p2, Lcom/facebook/search/results/filters/ui/SearchResultFilterDistanceSeekbar;

    const/high16 v3, 0x42c80000    # 100.0f

    .line 2262369
    iget-object v0, p3, LX/CyH;->c:LX/4FP;

    move-object v0, v0

    .line 2262370
    invoke-virtual {v0}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v0

    const-string v1, "value"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0, v3}, LX/FcS;->a(Ljava/lang/String;F)F

    move-result v1

    .line 2262371
    const v0, 0x7f0d2b85

    invoke-virtual {p2, v0}, Lcom/facebook/search/results/filters/ui/SearchResultFilterDistanceSeekbar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/seekbar/FbSeekBar;

    .line 2262372
    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v0, v2, v3}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->e(FF)V

    .line 2262373
    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->setCurrentSelectedValue(F)V

    .line 2262374
    new-instance v1, LX/FcR;

    invoke-direct {v1, p0, p2, p5, p3}, LX/FcR;-><init>(LX/FcS;Lcom/facebook/search/results/filters/ui/SearchResultFilterDistanceSeekbar;LX/FdQ;LX/CyH;)V

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/seekbar/FbSeekBar;->setOnSeekBarChangeListener(LX/FcR;)V

    .line 2262375
    return-void
.end method

.method public final b()LX/FcE;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/FcE",
            "<",
            "Lcom/facebook/search/results/filters/ui/SearchResultFilterDistanceSeekbar;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2262367
    sget-object v0, LX/FcS;->b:LX/FcE;

    return-object v0
.end method
