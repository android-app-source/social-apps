.class public final LX/GxJ;
.super LX/BWJ;
.source ""


# instance fields
.field public final synthetic b:LX/GxN;

.field private d:Landroid/content/Context;


# direct methods
.method public constructor <init>(LX/GxN;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2408036
    iput-object p1, p0, LX/GxJ;->b:LX/GxN;

    invoke-direct {p0, p1}, LX/BWJ;-><init>(Lcom/facebook/webview/FacebookWebView;)V

    .line 2408037
    iput-object p2, p0, LX/GxJ;->d:Landroid/content/Context;

    .line 2408038
    return-void
.end method


# virtual methods
.method public final onGeolocationPermissionsShowPrompt(Ljava/lang/String;Landroid/webkit/GeolocationPermissions$Callback;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2408039
    invoke-super {p0, p1, p2}, LX/BWJ;->onGeolocationPermissionsShowPrompt(Ljava/lang/String;Landroid/webkit/GeolocationPermissions$Callback;)V

    .line 2408040
    new-instance v0, Landroid/widget/CheckBox;

    iget-object v1, p0, LX/GxJ;->d:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    .line 2408041
    const v1, 0x7f08355b

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 2408042
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setTextColor(I)V

    .line 2408043
    new-instance v1, LX/0ju;

    iget-object v2, p0, LX/GxJ;->d:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2408044
    iget-object v2, p0, LX/GxJ;->d:Landroid/content/Context;

    const v3, 0x7f08355e

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1, v5}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const v2, 0x7f08355c

    new-instance v3, LX/GxI;

    invoke-direct {v3, p0, p2, p1, v0}, LX/GxI;-><init>(LX/GxJ;Landroid/webkit/GeolocationPermissions$Callback;Ljava/lang/String;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f08355d

    new-instance v3, LX/GxH;

    invoke-direct {v3, p0, p2, p1, v0}, LX/GxH;-><init>(LX/GxJ;Landroid/webkit/GeolocationPermissions$Callback;Ljava/lang/String;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2408045
    return-void
.end method
