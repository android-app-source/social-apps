.class public final LX/Fpq;
.super LX/1Yy;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/BaseTimelineFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/BaseTimelineFragment;)V
    .locals 0

    .prologue
    .line 2291928
    iput-object p1, p0, LX/Fpq;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-direct {p0}, LX/1Yy;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 11

    .prologue
    .line 2291929
    check-cast p1, LX/1Zj;

    .line 2291930
    iget-object v0, p0, LX/Fpq;->a:Lcom/facebook/timeline/BaseTimelineFragment;

    .line 2291931
    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lm_()LX/G4x;

    move-result-object v10

    .line 2291932
    iget-object v1, p1, LX/1Zj;->a:Ljava/lang/String;

    iget-object v2, p1, LX/1Zj;->c:Ljava/lang/String;

    invoke-virtual {v10, v1, v2}, LX/G4x;->a(Ljava/lang/String;Ljava/lang/String;)LX/16n;

    move-result-object v1

    .line 2291933
    if-eqz v1, :cond_0

    invoke-interface {v1}, LX/16n;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    if-eqz v2, :cond_0

    instance-of v2, v1, Lcom/facebook/graphql/model/GraphQLStory;

    if-nez v2, :cond_2

    .line 2291934
    :cond_0
    if-nez v1, :cond_1

    const-string v1, "oldUnit"

    move-object v2, v1

    .line 2291935
    :goto_0
    iget-object v1, v0, Lcom/facebook/timeline/BaseTimelineFragment;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/03V;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "timeline_story_like_fail_no_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not find a unit in SectionData to modify. "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " is null."

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2291936
    :goto_1
    return-void

    .line 2291937
    :cond_1
    const-string v1, "feedback"

    move-object v2, v1

    goto :goto_0

    .line 2291938
    :cond_2
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 2291939
    iget-object v1, v0, Lcom/facebook/timeline/BaseTimelineFragment;->B:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/967;

    iget-boolean v4, p1, LX/1Zj;->e:Z

    iget-wide v5, p1, LX/1Zj;->g:J

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->z()Ljava/lang/String;

    move-result-object v8

    new-instance v9, LX/Fpo;

    invoke-direct {v9, v0, v10}, LX/Fpo;-><init>(Lcom/facebook/timeline/BaseTimelineFragment;LX/G4x;)V

    invoke-virtual/range {v2 .. v9}, LX/967;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ZJLjava/lang/String;Ljava/lang/String;LX/1L9;)V

    goto :goto_1
.end method
