.class public LX/F2N;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2193054
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2193055
    iput-object p1, p0, LX/F2N;->a:LX/0Or;

    .line 2193056
    return-void
.end method

.method public static a(LX/0QB;)LX/F2N;
    .locals 1

    .prologue
    .line 2193044
    invoke-static {p0}, LX/F2N;->b(LX/0QB;)LX/F2N;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/os/Bundle;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2193051
    invoke-virtual {p1, p0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2193052
    const-string v0, "target_fragment"

    sget-object v1, LX/0cQ;->GROUP_EDIT_NAME_DESC_FRAGMENT:LX/0cQ;

    invoke-virtual {v1}, LX/0cQ;->ordinal()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2193053
    return-void
.end method

.method public static b(LX/0QB;)LX/F2N;
    .locals 2

    .prologue
    .line 2193057
    new-instance v0, LX/F2N;

    const/16 v1, 0xc

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    invoke-direct {v0, v1}, LX/F2N;-><init>(LX/0Or;)V

    .line 2193058
    return-object v0
.end method

.method public static b(LX/F2N;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2193050
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v0, p0, LX/F2N;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2193045
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2193046
    const-string v1, "has_no_data"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2193047
    invoke-static {p0}, LX/F2N;->b(LX/F2N;)Landroid/content/Intent;

    move-result-object v1

    .line 2193048
    invoke-static {v0, v1}, LX/F2N;->a(Landroid/os/Bundle;Landroid/content/Intent;)V

    .line 2193049
    return-object v1
.end method
