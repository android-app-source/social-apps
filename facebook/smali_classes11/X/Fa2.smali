.class public final LX/Fa2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/0Px",
        "<",
        "LX/CwV;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Fa5;


# direct methods
.method public constructor <init>(LX/Fa5;)V
    .locals 0

    .prologue
    .line 2258286
    iput-object p1, p0, LX/Fa2;->a:LX/Fa5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2258287
    iget-object v0, p0, LX/Fa2;->a:LX/Fa5;

    iget-object v0, v0, LX/Fa5;->e:LX/03V;

    sget-object v1, LX/3Ql;->FETCH_NULL_STATE_TOP_TOPIC_FAIL:LX/3Ql;

    invoke-virtual {v1}, LX/3Ql;->name()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Fail to fetch null state top topics"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2258288
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2258289
    move-object v1, v1

    .line 2258290
    const/4 v2, 0x0

    .line 2258291
    iput-boolean v2, v1, LX/0VK;->d:Z

    .line 2258292
    move-object v1, v1

    .line 2258293
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2258294
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2258295
    check-cast p1, LX/0Px;

    .line 2258296
    if-nez p1, :cond_0

    .line 2258297
    iget-object v0, p0, LX/Fa2;->a:LX/Fa5;

    iget-object v0, v0, LX/Fa5;->e:LX/03V;

    sget-object v1, LX/3Ql;->FETCH_NULL_STATE_TOP_TOPIC_FAIL:LX/3Ql;

    invoke-virtual {v1}, LX/3Ql;->name()Ljava/lang/String;

    move-result-object v1

    const-string v2, "The result of fetching null state top topic is null"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2258298
    :goto_0
    return-void

    .line 2258299
    :cond_0
    iget-object v0, p0, LX/Fa2;->a:LX/Fa5;

    iget-object v0, v0, LX/Fa5;->b:LX/Fa6;

    invoke-virtual {v0, p1}, LX/Fa6;->a(LX/0Px;)V

    .line 2258300
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2258301
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CwV;

    .line 2258302
    iget-boolean v4, v0, LX/CwV;->d:Z

    move v4, v4

    .line 2258303
    if-eqz v4, :cond_1

    .line 2258304
    iget-object v4, v0, LX/CwV;->a:Ljava/lang/String;

    move-object v0, v4

    .line 2258305
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2258306
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2258307
    :cond_2
    iget-object v0, p0, LX/Fa2;->a:LX/Fa5;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-static {v0, v1}, LX/Fa5;->a$redex0(LX/Fa5;LX/0Px;)V

    goto :goto_0
.end method
