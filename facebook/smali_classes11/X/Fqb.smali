.class public final LX/Fqb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2do;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/5SB;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fqs;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/content/Context;

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/4BY;

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/5SB;LX/0Or;Landroid/content/Context;LX/0Ot;LX/4BY;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/2do;",
            ">;",
            "LX/5SB;",
            "LX/0Or",
            "<",
            "LX/Fqs;",
            ">;",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/4BY;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2294004
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2294005
    iput-object p1, p0, LX/Fqb;->a:LX/0Or;

    .line 2294006
    iput-object p2, p0, LX/Fqb;->b:LX/5SB;

    .line 2294007
    iput-object p3, p0, LX/Fqb;->c:LX/0Or;

    .line 2294008
    iput-object p4, p0, LX/Fqb;->d:Landroid/content/Context;

    .line 2294009
    iput-object p5, p0, LX/Fqb;->e:LX/0Ot;

    .line 2294010
    iput-object p6, p0, LX/Fqb;->f:LX/4BY;

    .line 2294011
    iput-object p7, p0, LX/Fqb;->g:LX/0Or;

    .line 2294012
    iput-object p8, p0, LX/Fqb;->h:LX/0Or;

    .line 2294013
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 2293973
    iget-object v0, p0, LX/Fqb;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2do;

    new-instance v1, LX/2iE;

    iget-object v2, p0, LX/Fqb;->b:LX/5SB;

    .line 2293974
    iget-wide v4, v2, LX/5SB;->b:J

    move-wide v2, v4

    .line 2293975
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, LX/2iE;-><init>(J)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2293976
    iget-object v0, p0, LX/Fqb;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fqs;

    const-string v1, "BlockUserFromActionBarOnSuccess"

    invoke-virtual {v0, v1}, LX/Fqs;->a(Ljava/lang/String;)V

    .line 2293977
    invoke-direct {p0}, LX/Fqb;->b()V

    .line 2293978
    return-void
.end method

.method private static a(Ljava/lang/Throwable;)Z
    .locals 2

    .prologue
    .line 2293979
    instance-of v0, p0, Lcom/facebook/fbservice/service/ServiceException;

    if-eqz v0, :cond_0

    .line 2293980
    check-cast p0, Lcom/facebook/fbservice/service/ServiceException;

    .line 2293981
    iget-object v0, p0, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2293982
    iget-object v1, v0, Lcom/facebook/fbservice/service/OperationResult;->errorThrowable:Ljava/lang/Throwable;

    move-object v0, v1

    .line 2293983
    instance-of v0, v0, LX/2Oo;

    if-eqz v0, :cond_0

    .line 2293984
    iget-object v0, p0, Lcom/facebook/fbservice/service/ServiceException;->result:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2293985
    iget-object v1, v0, Lcom/facebook/fbservice/service/OperationResult;->errorThrowable:Ljava/lang/Throwable;

    move-object v0, v1

    .line 2293986
    check-cast v0, LX/2Oo;

    .line 2293987
    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    const/16 v1, 0xeda

    if-ne v0, v1, :cond_0

    .line 2293988
    const/4 v0, 0x1

    .line 2293989
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 3

    .prologue
    .line 2293990
    iget-object v0, p0, LX/Fqb;->f:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2293991
    iget-object v0, p0, LX/Fqb;->f:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2293992
    :cond_0
    iget-object v0, p0, LX/Fqb;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v1, p0, LX/Fqb;->d:Landroid/content/Context;

    sget-object v2, LX/0ax;->cL:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2293993
    iget-object v0, p0, LX/Fqb;->h:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/Fqb;->d:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2293994
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 2293995
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    .line 2293996
    iget-object v0, p0, LX/Fqb;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fqs;

    const-string v1, "BlockUserFromActionBarOnFailure"

    invoke-virtual {v0, v1}, LX/Fqs;->a(Ljava/lang/String;)V

    .line 2293997
    invoke-static {p1}, LX/Fqb;->a(Ljava/lang/Throwable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2293998
    iget-object v0, p0, LX/Fqb;->d:Landroid/content/Context;

    const v1, 0x7f0815b2

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2293999
    :goto_0
    iget-object v0, p0, LX/Fqb;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "timeline_block_user_failed"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2294000
    :cond_0
    invoke-direct {p0}, LX/Fqb;->b()V

    .line 2294001
    return-void

    .line 2294002
    :cond_1
    iget-object v0, p0, LX/Fqb;->d:Landroid/content/Context;

    const v1, 0x7f0815b1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2294003
    invoke-direct {p0}, LX/Fqb;->a()V

    return-void
.end method
