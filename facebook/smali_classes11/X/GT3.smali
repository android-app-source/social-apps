.class public final LX/GT3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/GRZ;

.field public final synthetic b:I

.field public final synthetic c:Lcom/facebook/appinvites/ui/AppInviteContentView;

.field public final synthetic d:LX/GTB;


# direct methods
.method public constructor <init>(LX/GTB;LX/GRZ;ILcom/facebook/appinvites/ui/AppInviteContentView;)V
    .locals 0

    .prologue
    .line 2355145
    iput-object p1, p0, LX/GT3;->d:LX/GTB;

    iput-object p2, p0, LX/GT3;->a:LX/GRZ;

    iput p3, p0, LX/GT3;->b:I

    iput-object p4, p0, LX/GT3;->c:Lcom/facebook/appinvites/ui/AppInviteContentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x61ea88e9

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2355146
    iget-object v1, p0, LX/GT3;->d:LX/GTB;

    iget-object v1, v1, LX/GTB;->h:LX/GRv;

    const-string v2, "app_invite_sender_did_tapped"

    iget-object v3, p0, LX/GT3;->a:LX/GRZ;

    iget v4, p0, LX/GT3;->b:I

    invoke-virtual {v3, v4}, LX/GRZ;->b(I)Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/GRv;->a(Ljava/lang/String;Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;)V

    .line 2355147
    sget-object v1, LX/0ax;->bE:Ljava/lang/String;

    iget-object v2, p0, LX/GT3;->a:LX/GRZ;

    iget v3, p0, LX/GT3;->b:I

    invoke-virtual {v2, v3}, LX/GRZ;->b(I)Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->j()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2355148
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    .line 2355149
    iget-object v2, p0, LX/GT3;->d:LX/GTB;

    iget-object v2, v2, LX/GTB;->g:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/GT3;->c:Lcom/facebook/appinvites/ui/AppInviteContentView;

    invoke-virtual {v3}, Lcom/facebook/appinvites/ui/AppInviteContentView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2355150
    const v1, 0x38dd9cfe

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
