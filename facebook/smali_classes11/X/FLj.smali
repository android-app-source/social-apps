.class public final LX/FLj;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 19

    .prologue
    .line 2228019
    const/4 v13, 0x0

    .line 2228020
    const/4 v12, 0x0

    .line 2228021
    const/4 v11, 0x0

    .line 2228022
    const/4 v10, 0x0

    .line 2228023
    const-wide/16 v8, 0x0

    .line 2228024
    const/4 v7, 0x0

    .line 2228025
    const/4 v6, 0x0

    .line 2228026
    const/4 v5, 0x0

    .line 2228027
    const/4 v4, 0x0

    .line 2228028
    const/4 v3, 0x0

    .line 2228029
    const/4 v2, 0x0

    .line 2228030
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->START_OBJECT:LX/15z;

    if-eq v14, v15, :cond_e

    .line 2228031
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2228032
    const/4 v2, 0x0

    .line 2228033
    :goto_0
    return v2

    .line 2228034
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v15, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v15, :cond_c

    .line 2228035
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2228036
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2228037
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_0

    if-eqz v3, :cond_0

    .line 2228038
    const-string v15, "__type__"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_1

    const-string v15, "__typename"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .line 2228039
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v3

    move v14, v3

    goto :goto_1

    .line 2228040
    :cond_2
    const-string v15, "adjusted_size"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_3

    .line 2228041
    invoke-static/range {p0 .. p1}, LX/FLd;->a(LX/15w;LX/186;)I

    move-result v3

    move v13, v3

    goto :goto_1

    .line 2228042
    :cond_3
    const-string v15, "animated_gif"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_4

    .line 2228043
    invoke-static/range {p0 .. p1}, LX/FLe;->a(LX/15w;LX/186;)I

    move-result v3

    move v7, v3

    goto :goto_1

    .line 2228044
    :cond_4
    const-string v15, "animated_image"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_5

    .line 2228045
    invoke-static/range {p0 .. p1}, LX/FLf;->a(LX/15w;LX/186;)I

    move-result v3

    move v6, v3

    goto :goto_1

    .line 2228046
    :cond_5
    const-string v15, "creation_time"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 2228047
    const/4 v2, 0x1

    .line 2228048
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto :goto_1

    .line 2228049
    :cond_6
    const-string v15, "creator"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_7

    .line 2228050
    invoke-static/range {p0 .. p1}, LX/FLg;->a(LX/15w;LX/186;)I

    move-result v3

    move v12, v3

    goto :goto_1

    .line 2228051
    :cond_7
    const-string v15, "imageThumbnail"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_8

    .line 2228052
    invoke-static/range {p0 .. p1}, LX/FLh;->a(LX/15w;LX/186;)I

    move-result v3

    move v11, v3

    goto/16 :goto_1

    .line 2228053
    :cond_8
    const-string v15, "legacy_attachment_id"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 2228054
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v10, v3

    goto/16 :goto_1

    .line 2228055
    :cond_9
    const-string v15, "mediaUrl"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_a

    .line 2228056
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    move v9, v3

    goto/16 :goto_1

    .line 2228057
    :cond_a
    const-string v15, "original_dimensions"

    invoke-virtual {v3, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 2228058
    invoke-static/range {p0 .. p1}, LX/FLi;->a(LX/15w;LX/186;)I

    move-result v3

    move v8, v3

    goto/16 :goto_1

    .line 2228059
    :cond_b
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2228060
    :cond_c
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, LX/186;->c(I)V

    .line 2228061
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v14}, LX/186;->b(II)V

    .line 2228062
    const/4 v3, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v13}, LX/186;->b(II)V

    .line 2228063
    const/4 v3, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 2228064
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 2228065
    if-eqz v2, :cond_d

    .line 2228066
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2228067
    :cond_d
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->b(II)V

    .line 2228068
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2228069
    const/4 v2, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2228070
    const/16 v2, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2228071
    const/16 v2, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2228072
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_e
    move v14, v13

    move v13, v12

    move v12, v7

    move v7, v11

    move v11, v6

    move v6, v10

    move v10, v5

    move-wide/from16 v17, v8

    move v9, v4

    move v8, v3

    move-wide/from16 v4, v17

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 2228073
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2228074
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2228075
    if-eqz v0, :cond_0

    .line 2228076
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2228077
    invoke-static {p0, p1, v1, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2228078
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2228079
    if-eqz v0, :cond_2

    .line 2228080
    const-string v1, "adjusted_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2228081
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2228082
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2228083
    if-eqz v1, :cond_1

    .line 2228084
    const-string p3, "uri"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2228085
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2228086
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2228087
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2228088
    if-eqz v0, :cond_4

    .line 2228089
    const-string v1, "animated_gif"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2228090
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2228091
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2228092
    if-eqz v1, :cond_3

    .line 2228093
    const-string p3, "uri"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2228094
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2228095
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2228096
    :cond_4
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2228097
    if-eqz v0, :cond_6

    .line 2228098
    const-string v1, "animated_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2228099
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2228100
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2228101
    if-eqz v1, :cond_5

    .line 2228102
    const-string p3, "uri"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2228103
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2228104
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2228105
    :cond_6
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2228106
    cmp-long v2, v0, v2

    if-eqz v2, :cond_7

    .line 2228107
    const-string v2, "creation_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2228108
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2228109
    :cond_7
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2228110
    if-eqz v0, :cond_8

    .line 2228111
    const-string v1, "creator"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2228112
    invoke-static {p0, v0, p2}, LX/FLg;->a(LX/15i;ILX/0nX;)V

    .line 2228113
    :cond_8
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2228114
    if-eqz v0, :cond_a

    .line 2228115
    const-string v1, "imageThumbnail"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2228116
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2228117
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2228118
    if-eqz v1, :cond_9

    .line 2228119
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2228120
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2228121
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2228122
    :cond_a
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2228123
    if-eqz v0, :cond_b

    .line 2228124
    const-string v1, "legacy_attachment_id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2228125
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2228126
    :cond_b
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2228127
    if-eqz v0, :cond_c

    .line 2228128
    const-string v1, "mediaUrl"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2228129
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2228130
    :cond_c
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2228131
    if-eqz v0, :cond_f

    .line 2228132
    const-string v1, "original_dimensions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2228133
    const-wide/16 v8, 0x0

    .line 2228134
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2228135
    const/4 v4, 0x0

    invoke-virtual {p0, v0, v4, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v4

    .line 2228136
    cmpl-double v6, v4, v8

    if-eqz v6, :cond_d

    .line 2228137
    const-string v6, "x"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2228138
    invoke-virtual {p2, v4, v5}, LX/0nX;->a(D)V

    .line 2228139
    :cond_d
    const/4 v4, 0x1

    invoke-virtual {p0, v0, v4, v8, v9}, LX/15i;->a(IID)D

    move-result-wide v4

    .line 2228140
    cmpl-double v6, v4, v8

    if-eqz v6, :cond_e

    .line 2228141
    const-string v6, "y"

    invoke-virtual {p2, v6}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2228142
    invoke-virtual {p2, v4, v5}, LX/0nX;->a(D)V

    .line 2228143
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2228144
    :cond_f
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2228145
    return-void
.end method
