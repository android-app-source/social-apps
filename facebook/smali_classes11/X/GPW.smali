.class public LX/GPW;
.super LX/6sV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6sV",
        "<",
        "Lcom/facebook/adspayments/protocol/AddPaymentCardParams;",
        "Lcom/facebook/adspayments/protocol/AddPaymentCardResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:LX/ADT;


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;LX/ADT;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2348969
    const-class v0, Lcom/facebook/adspayments/protocol/AddPaymentCardResult;

    invoke-direct {p0, p1, v0}, LX/6sV;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 2348970
    iput-object p2, p0, LX/GPW;->c:LX/ADT;

    .line 2348971
    return-void
.end method

.method public static b(LX/0QB;)LX/GPW;
    .locals 3

    .prologue
    .line 2348972
    new-instance v2, LX/GPW;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-static {p0}, LX/ADT;->b(LX/0QB;)LX/ADT;

    move-result-object v1

    check-cast v1, LX/ADT;

    invoke-direct {v2, v0, v1}, LX/GPW;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;LX/ADT;)V

    .line 2348973
    return-object v2
.end method

.method private static b(Lcom/facebook/adspayments/protocol/AddPaymentCardParams;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2348974
    iget-object v0, p0, Lcom/facebook/adspayments/protocol/AddPaymentCardParams;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2348975
    const-string v1, "0"

    invoke-static {v1}, LX/0Rj;->equalTo(Ljava/lang/Object;)LX/0Rl;

    move-result-object v1

    invoke-static {v1}, LX/0Rj;->not(LX/0Rl;)LX/0Rl;

    move-result-object v1

    const-string v2, "AccountId can NOT be 0 for Ads invoice"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2, v3}, LX/GPq;->a(Ljava/lang/Object;LX/0Rl;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 2348976
    check-cast p1, Lcom/facebook/adspayments/protocol/AddPaymentCardParams;

    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 2348977
    invoke-virtual {p1}, Lcom/facebook/adspayments/protocol/PaymentCardParams;->f()Ljava/util/List;

    move-result-object v3

    .line 2348978
    const-string v1, "creditCardNumber"

    .line 2348979
    iget-object v4, p1, Lcom/facebook/adspayments/protocol/AddPaymentCardParams;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2348980
    invoke-static {v3, v1, v4}, LX/6sU;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2348981
    iget-object v1, p1, Lcom/facebook/adspayments/protocol/PaymentCardParams;->a:LX/6xg;

    move-object v1, v1

    .line 2348982
    sget-object v4, LX/6xg;->INVOICE:LX/6xg;

    if-ne v1, v4, :cond_0

    iget-object v1, p0, LX/GPW;->c:LX/ADT;

    const/16 v4, 0x2

    invoke-virtual {v1, v4}, LX/ADT;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v0

    .line 2348983
    :goto_0
    if-eqz v1, :cond_1

    const-string v4, "/act_%s/creditcards"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1}, LX/GPW;->b(Lcom/facebook/adspayments/protocol/AddPaymentCardParams;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v2

    invoke-static {v4, v0}, LX/73t;->a(Ljava/lang/String;[Ljava/lang/Object;)LX/14O;

    move-result-object v0

    .line 2348984
    :goto_1
    if-eqz v1, :cond_2

    .line 2348985
    const-string v1, "should_support_tricky_bin"

    .line 2348986
    iget-boolean v2, p1, Lcom/facebook/adspayments/protocol/AddPaymentCardParams;->d:Z

    move v2, v2

    .line 2348987
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v3, v1, v2}, LX/6sU;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2348988
    :goto_2
    const-string v1, "add_credit_cards"

    .line 2348989
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 2348990
    move-object v0, v0

    .line 2348991
    const-string v1, "POST"

    .line 2348992
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 2348993
    move-object v0, v0

    .line 2348994
    iput-object v3, v0, LX/14O;->g:Ljava/util/List;

    .line 2348995
    move-object v0, v0

    .line 2348996
    sget-object v1, LX/14S;->JSONPARSER:LX/14S;

    .line 2348997
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2348998
    move-object v0, v0

    .line 2348999
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    :cond_0
    move v1, v2

    .line 2349000
    goto :goto_0

    .line 2349001
    :cond_1
    const-string v4, "/%d/creditcards"

    new-array v0, v0, [Ljava/lang/Object;

    .line 2349002
    iget-object v5, p1, Lcom/facebook/adspayments/protocol/AddPaymentCardParams;->b:Ljava/lang/String;

    move-object v5, v5

    .line 2349003
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v0, v2

    invoke-static {v4, v0}, LX/73t;->a(Ljava/lang/String;[Ljava/lang/Object;)LX/14O;

    move-result-object v0

    goto :goto_1

    .line 2349004
    :cond_2
    const-string v1, "account_id"

    invoke-static {p1}, LX/GPW;->b(Lcom/facebook/adspayments/protocol/AddPaymentCardParams;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v1, v2}, LX/6sU;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2349005
    const-string v0, "add_payments_card"

    return-object v0
.end method
