.class public LX/FiB;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2274581
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2274582
    return-void
.end method

.method public static a(Lcom/facebook/search/model/EntityTypeaheadUnit;Ljava/util/Locale;)Lcom/facebook/search/model/KeywordTypeaheadUnit;
    .locals 14

    .prologue
    .line 2274583
    iget-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2274584
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2274585
    iget-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2274586
    invoke-virtual {v0, p1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    move-object v12, v0

    .line 2274587
    :goto_0
    new-instance v0, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

    .line 2274588
    iget-object v1, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2274589
    iget-object v2, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2274590
    iget-object v3, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v3, v3

    .line 2274591
    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v3

    .line 2274592
    iget-object v4, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->g:Ljava/lang/String;

    move-object v4, v4

    .line 2274593
    iget-object v5, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->f:Ljava/lang/String;

    move-object v5, v5

    .line 2274594
    iget-object v6, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v6, v6

    .line 2274595
    if-nez v6, :cond_1

    const/4 v6, 0x0

    .line 2274596
    :goto_1
    iget-boolean v7, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->h:Z

    move v7, v7

    .line 2274597
    iget-object v8, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v8, v8

    .line 2274598
    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->name()Ljava/lang/String;

    move-result-object v8

    .line 2274599
    iget-object v9, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->i:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-object v9, v9

    .line 2274600
    invoke-virtual {v9}, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->name()Ljava/lang/String;

    move-result-object v9

    .line 2274601
    iget-object v10, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->l:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-object v10, v10

    .line 2274602
    invoke-virtual {v10}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->name()Ljava/lang/String;

    move-result-object v10

    .line 2274603
    iget-boolean v11, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->k:Z

    move v11, v11

    .line 2274604
    invoke-direct/range {v0 .. v11}, Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2274605
    iget-boolean v1, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->m:Z

    move v1, v1

    .line 2274606
    if-eqz v1, :cond_2

    sget-object v1, LX/CwI;->ENTITY_BOOTSTRAP:LX/CwI;

    :goto_2
    invoke-static {v12, v1}, LX/CwH;->a(Ljava/lang/String;LX/CwI;)LX/CwH;

    move-result-object v1

    .line 2274607
    iget-boolean v2, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->m:Z

    move v2, v2

    .line 2274608
    iput-boolean v2, v1, LX/CwH;->n:Z

    .line 2274609
    move-object v1, v1

    .line 2274610
    invoke-virtual {p0}, Lcom/facebook/search/model/EntityTypeaheadUnit;->A()D

    move-result-wide v2

    .line 2274611
    iput-wide v2, v1, LX/CwH;->o:D

    .line 2274612
    move-object v1, v1

    .line 2274613
    sget-object v2, LX/CwF;->keyword:LX/CwF;

    .line 2274614
    iput-object v2, v1, LX/CwH;->g:LX/CwF;

    .line 2274615
    move-object v1, v1

    .line 2274616
    const-string v2, "squashed"

    .line 2274617
    iput-object v2, v1, LX/CwH;->h:Ljava/lang/String;

    .line 2274618
    move-object v1, v1

    .line 2274619
    iput-object v0, v1, LX/CwH;->i:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

    .line 2274620
    move-object v0, v1

    .line 2274621
    iget-object v1, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->d:Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-object v1, v1

    .line 2274622
    iput-object v1, v0, LX/CwH;->E:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2274623
    move-object v0, v0

    .line 2274624
    iget-object v1, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2274625
    iput-object v1, v0, LX/CwH;->F:Ljava/lang/String;

    .line 2274626
    move-object v0, v0

    .line 2274627
    invoke-virtual {p0}, Lcom/facebook/search/model/EntityTypeaheadUnit;->K()Z

    move-result v1

    .line 2274628
    iput-boolean v1, v0, LX/CwH;->G:Z

    .line 2274629
    move-object v0, v0

    .line 2274630
    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    return-object v0

    .line 2274631
    :cond_0
    iget-object v0, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2274632
    move-object v12, v0

    goto :goto_0

    .line 2274633
    :cond_1
    iget-object v6, p0, Lcom/facebook/search/model/EntityTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v6, v6

    .line 2274634
    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_1

    .line 2274635
    :cond_2
    sget-object v1, LX/CwI;->ENTITY_REMOTE:LX/CwI;

    goto :goto_2
.end method

.method public static a(Lcom/facebook/search/model/KeywordTypeaheadUnit;Lcom/facebook/search/model/KeywordTypeaheadUnit;)Lcom/facebook/search/model/KeywordTypeaheadUnit;
    .locals 6
    .param p0    # Lcom/facebook/search/model/KeywordTypeaheadUnit;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    .line 2274636
    if-nez p0, :cond_1

    .line 2274637
    :cond_0
    :goto_0
    return-object p1

    .line 2274638
    :cond_1
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->l:LX/CwI;

    move-object v0, v0

    .line 2274639
    sget-object v1, LX/CwI;->ENTITY_BOOTSTRAP:LX/CwI;

    if-ne v0, v1, :cond_2

    .line 2274640
    invoke-static {p1}, LX/CwH;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)LX/CwH;

    move-result-object v0

    sget-object v1, LX/CwI;->ENTITY_BOOTSTRAP:LX/CwI;

    .line 2274641
    iput-object v1, v0, LX/CwH;->l:LX/CwI;

    .line 2274642
    move-object v0, v0

    .line 2274643
    iput-boolean v2, v0, LX/CwH;->n:Z

    .line 2274644
    move-object v0, v0

    .line 2274645
    iget-wide v4, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->o:D

    move-wide v2, v4

    .line 2274646
    iput-wide v2, v0, LX/CwH;->o:D

    .line 2274647
    move-object v0, v0

    .line 2274648
    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object p1

    goto :goto_0

    .line 2274649
    :cond_2
    iget-object v0, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->l:LX/CwI;

    move-object v0, v0

    .line 2274650
    sget-object v1, LX/CwI;->ENTITY_REMOTE:LX/CwI;

    if-eq v0, v1, :cond_0

    .line 2274651
    invoke-static {p0}, LX/CwH;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)LX/CwH;

    move-result-object v0

    .line 2274652
    iget-boolean v1, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->n:Z

    move v1, v1

    .line 2274653
    if-eqz v1, :cond_3

    .line 2274654
    iget-object v1, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->l:LX/CwI;

    move-object v1, v1

    .line 2274655
    iput-object v1, v0, LX/CwH;->l:LX/CwI;

    .line 2274656
    move-object v1, v0

    .line 2274657
    iput-boolean v2, v1, LX/CwH;->n:Z

    .line 2274658
    move-object v1, v1

    .line 2274659
    iget-wide v4, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->o:D

    move-wide v2, v4

    .line 2274660
    iput-wide v2, v1, LX/CwH;->o:D

    .line 2274661
    :goto_1
    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object p1

    goto :goto_0

    .line 2274662
    :cond_3
    iget-boolean v1, p0, Lcom/facebook/search/model/KeywordTypeaheadUnit;->n:Z

    move v1, v1

    .line 2274663
    if-eqz v1, :cond_4

    .line 2274664
    iput-boolean v2, v0, LX/CwH;->n:Z

    .line 2274665
    move-object v1, v0

    .line 2274666
    iget-wide v4, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->o:D

    move-wide v2, v4

    .line 2274667
    iput-wide v2, v1, LX/CwH;->o:D

    .line 2274668
    goto :goto_1

    .line 2274669
    :cond_4
    const/4 v1, 0x0

    .line 2274670
    iput-boolean v1, v0, LX/CwH;->n:Z

    .line 2274671
    goto :goto_1
.end method
