.class public LX/FKY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/FKo;

.field private final b:LX/FKy;


# direct methods
.method public constructor <init>(LX/FKo;LX/FKy;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225257
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2225258
    iput-object p1, p0, LX/FKY;->a:LX/FKo;

    .line 2225259
    iput-object p2, p0, LX/FKY;->b:LX/FKy;

    .line 2225260
    return-void
.end method

.method public static a(LX/0QB;)LX/FKY;
    .locals 3

    .prologue
    .line 2225254
    new-instance v2, LX/FKY;

    invoke-static {p0}, LX/FKo;->a(LX/0QB;)LX/FKo;

    move-result-object v0

    check-cast v0, LX/FKo;

    invoke-static {p0}, LX/FKy;->b(LX/0QB;)LX/FKy;

    move-result-object v1

    check-cast v1, LX/FKy;

    invoke-direct {v2, v0, v1}, LX/FKY;-><init>(LX/FKo;LX/FKy;)V

    .line 2225255
    move-object v0, v2

    .line 2225256
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2225227
    check-cast p1, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;

    .line 2225228
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2225229
    iget-object v1, p1, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2225230
    if-eqz v1, :cond_0

    .line 2225231
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "name"

    .line 2225232
    iget-object v3, p1, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2225233
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225234
    :cond_0
    iget-object v1, p1, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->b:Lcom/facebook/messaging/model/messages/Message;

    move-object v1, v1

    .line 2225235
    invoke-static {v0, v1}, LX/FKy;->a(Ljava/util/List;Lcom/facebook/messaging/model/messages/Message;)V

    .line 2225236
    iget-object v1, p1, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->c:LX/0Px;

    move-object v1, v1

    .line 2225237
    invoke-static {v1}, LX/FKo;->a(Ljava/util/List;)LX/0lF;

    move-result-object v1

    .line 2225238
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "to"

    invoke-virtual {v1}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225239
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "createThread"

    .line 2225240
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2225241
    move-object v1, v1

    .line 2225242
    const-string v2, "POST"

    .line 2225243
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2225244
    move-object v1, v1

    .line 2225245
    const-string v2, "me/threads"

    .line 2225246
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2225247
    move-object v1, v1

    .line 2225248
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2225249
    move-object v0, v1

    .line 2225250
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2225251
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2225252
    move-object v0, v0

    .line 2225253
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2225225
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2225226
    const-string v1, "tid"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
