.class public LX/Fbq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FbA;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/FbA",
        "<",
        "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$KeywordSearchModuleFragment;",
        "Lcom/facebook/search/results/model/SearchResultsBridge;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0Xu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xu",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "LX/D0G;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Fb9;",
            "LX/0Ot",
            "<+",
            "LX/FbH;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Fbd;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fbh;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fc0;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fc2;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fbz;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fbw;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FbS;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FbJ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fbr;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fbx;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fbb;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fbe;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FbL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FbK;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FbN;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FbM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fbt;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fbp;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2261171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2261172
    invoke-static {}, LX/0Xq;->t()LX/0Xq;

    move-result-object v1

    iput-object v1, p0, LX/Fbq;->a:LX/0Xu;

    .line 2261173
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, LX/Fbq;->b:Ljava/util/Map;

    .line 2261174
    sget-object v1, LX/D0F;->l:LX/CzO;

    invoke-direct {p0, v1, p5}, LX/Fbq;->a(LX/CzO;LX/0Ot;)V

    .line 2261175
    sget-object v1, LX/D0F;->g:LX/CzO;

    invoke-direct {p0, v1, p1}, LX/Fbq;->a(LX/CzO;LX/0Ot;)V

    .line 2261176
    sget-object v1, LX/D0F;->k:LX/CzO;

    invoke-direct {p0, v1, p1}, LX/Fbq;->a(LX/CzO;LX/0Ot;)V

    .line 2261177
    sget-object v1, LX/D0F;->h:LX/CzO;

    invoke-direct {p0, v1, p12}, LX/Fbq;->a(LX/CzO;LX/0Ot;)V

    .line 2261178
    sget-object v1, LX/D0F;->c:LX/CzO;

    invoke-direct {p0, v1, p13}, LX/Fbq;->a(LX/CzO;LX/0Ot;)V

    .line 2261179
    sget-object v1, LX/D0F;->d:LX/CzO;

    move-object/from16 v0, p14

    invoke-direct {p0, v1, v0}, LX/Fbq;->a(LX/CzO;LX/0Ot;)V

    .line 2261180
    sget-object v1, LX/D0F;->e:LX/CzO;

    move-object/from16 v0, p15

    invoke-direct {p0, v1, v0}, LX/Fbq;->a(LX/CzO;LX/0Ot;)V

    .line 2261181
    sget-object v1, LX/D0F;->f:LX/CzO;

    move-object/from16 v0, p16

    invoke-direct {p0, v1, v0}, LX/Fbq;->a(LX/CzO;LX/0Ot;)V

    .line 2261182
    sget-object v1, LX/D0F;->i:LX/CzO;

    invoke-direct {p0, v1, p2}, LX/Fbq;->a(LX/CzO;LX/0Ot;)V

    .line 2261183
    sget-object v1, LX/D0F;->m:LX/CzO;

    invoke-direct {p0, v1, p3}, LX/Fbq;->a(LX/CzO;LX/0Ot;)V

    .line 2261184
    sget-object v1, LX/D0F;->n:LX/CzO;

    invoke-direct {p0, v1, p4}, LX/Fbq;->a(LX/CzO;LX/0Ot;)V

    .line 2261185
    sget-object v1, LX/D0F;->o:LX/CzO;

    invoke-direct {p0, v1, p6}, LX/Fbq;->a(LX/CzO;LX/0Ot;)V

    .line 2261186
    sget-object v1, LX/D0F;->p:LX/CzO;

    invoke-direct {p0, v1, p7}, LX/Fbq;->a(LX/CzO;LX/0Ot;)V

    .line 2261187
    sget-object v1, LX/D0F;->q:LX/CzO;

    invoke-direct {p0, v1, p8}, LX/Fbq;->a(LX/CzO;LX/0Ot;)V

    .line 2261188
    sget-object v1, LX/D0F;->s:LX/CzO;

    invoke-direct {p0, v1, p9}, LX/Fbq;->a(LX/CzO;LX/0Ot;)V

    .line 2261189
    sget-object v1, LX/D0F;->u:LX/CzO;

    invoke-direct {p0, v1, p10}, LX/Fbq;->a(LX/CzO;LX/0Ot;)V

    .line 2261190
    sget-object v1, LX/D0F;->j:LX/CzO;

    invoke-direct {p0, v1, p11}, LX/Fbq;->a(LX/CzO;LX/0Ot;)V

    .line 2261191
    sget-object v1, LX/D0F;->t:LX/CzO;

    move-object/from16 v0, p17

    invoke-direct {p0, v1, v0}, LX/Fbq;->a(LX/CzO;LX/0Ot;)V

    .line 2261192
    sget-object v1, LX/D0F;->r:LX/CzO;

    move-object/from16 v0, p18

    invoke-direct {p0, v1, v0}, LX/Fbq;->a(LX/CzO;LX/0Ot;)V

    .line 2261193
    return-void
.end method

.method private a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/FbH;
    .locals 3
    .param p2    # Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2261194
    iget-object v0, p0, LX/Fbq;->a:LX/0Xu;

    invoke-interface {v0, p1}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Rl;

    .line 2261195
    invoke-interface {v0, p2}, LX/0Rl;->apply(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2261196
    new-instance v1, LX/Fb9;

    invoke-direct {v1, p1, v0}, LX/Fb9;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0Rl;)V

    .line 2261197
    iget-object v0, p0, LX/Fbq;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FbH;

    return-object v0

    .line 2261198
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bridge factory not found for role: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". You must register all bridge factories in the bridge meta factory."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(LX/0QB;)LX/Fbq;
    .locals 3

    .prologue
    .line 2261199
    const-class v1, LX/Fbq;

    monitor-enter v1

    .line 2261200
    :try_start_0
    sget-object v0, LX/Fbq;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2261201
    sput-object v2, LX/Fbq;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2261202
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2261203
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/Fbq;->b(LX/0QB;)LX/Fbq;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2261204
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fbq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2261205
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2261206
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/CzO;LX/0Ot;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CzO",
            "<",
            "Lcom/facebook/search/results/model/SearchResultsBridge;",
            ">;",
            "LX/0Ot",
            "<+",
            "LX/FbH;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2261207
    iget-object v0, p1, LX/CzO;->b:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 2261208
    iget-object v2, p0, LX/Fbq;->a:LX/0Xu;

    iget-object v3, p1, LX/CzO;->c:LX/D0G;

    invoke-interface {v2, v0, v3}, LX/0Xu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2261209
    new-instance v2, LX/Fb9;

    iget-object v3, p1, LX/CzO;->c:LX/D0G;

    invoke-direct {v2, v0, v3}, LX/Fb9;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0Rl;)V

    .line 2261210
    iget-object v0, p0, LX/Fbq;->b:Ljava/util/Map;

    invoke-interface {v0, v2, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2261211
    :cond_0
    return-void
.end method

.method private static b(LX/0QB;)LX/Fbq;
    .locals 21

    .prologue
    .line 2261212
    new-instance v2, LX/Fbq;

    const/16 v3, 0x3315

    move-object/from16 v0, p0

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x3318

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x332b

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x332d

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x332a

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x3327

    move-object/from16 v0, p0

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x330d

    move-object/from16 v0, p0

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x3304

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x3322

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x3328

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x3313

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    const/16 v14, 0x3316

    move-object/from16 v0, p0

    invoke-static {v0, v14}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v14

    const/16 v15, 0x3306

    move-object/from16 v0, p0

    invoke-static {v0, v15}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v15

    const/16 v16, 0x3305

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v16

    const/16 v17, 0x3308

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v17

    const/16 v18, 0x3307

    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v18

    const/16 v19, 0x3324

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v19

    const/16 v20, 0x3320

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v20

    invoke-direct/range {v2 .. v20}, LX/Fbq;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2261213
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Px;
    .locals 2

    .prologue
    .line 2261214
    check-cast p1, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;

    .line 2261215
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2261216
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2261217
    :goto_0
    return-object v0

    .line 2261218
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v0

    invoke-static {p1}, LX/Fbf;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/Fbq;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/FbH;

    move-result-object v0

    .line 2261219
    invoke-virtual {v0, p1}, LX/FbD;->c(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
