.class public final LX/Gwo;
.super Landroid/webkit/WebViewClient;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/view/LoggedOutWebViewActivity;)V
    .locals 0

    .prologue
    .line 2407386
    iput-object p1, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/katana/view/LoggedOutWebViewActivity;B)V
    .locals 0

    .prologue
    .line 2407385
    invoke-direct {p0, p1}, LX/Gwo;-><init>(Lcom/facebook/katana/view/LoggedOutWebViewActivity;)V

    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 2407426
    :try_start_0
    iget-object v0, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    iget-object v0, v0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->q:LX/Gwp;

    invoke-virtual {v0}, LX/Gwp;->show()V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2407427
    :goto_0
    return-void

    .line 2407428
    :catch_0
    move-exception v0

    .line 2407429
    iget-object v1, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    iget-object v1, v1, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->u:LX/03V;

    sget-object v2, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->C:Ljava/lang/String;

    const-string v3, "failed to show spinner, bad token"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2407430
    :catch_1
    move-exception v0

    .line 2407431
    iget-object v1, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    iget-object v1, v1, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->u:LX/03V;

    sget-object v2, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->C:Ljava/lang/String;

    const-string v3, "failed to show spinner, bad state"

    invoke-virtual {v1, v2, v3, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method public final onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2407424
    iget-object v0, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    iget-object v0, v0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->t:LX/44G;

    invoke-virtual {v0, p2}, LX/44G;->a(Ljava/lang/String;)V

    .line 2407425
    return-void
.end method

.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2407419
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 2407420
    const/4 v0, 0x3

    invoke-static {v0}, LX/01m;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2407421
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "loaded url: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2407422
    :cond_0
    iget-object v0, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    iget-object v0, v0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->y:LX/48V;

    const-string v1, "javascript:fbLoggedOutWebViewIsErrorPage.setFailing(typeof JX == \'undefined\')"

    invoke-virtual {v0, p1, v1}, LX/48V;->b(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 2407423
    return-void
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 2407416
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 2407417
    invoke-direct {p0}, LX/Gwo;->a()V

    .line 2407418
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2407412
    iget-object v0, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    iget-object v0, v0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->u:LX/03V;

    sget-object v1, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->C:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "failure loading. url="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " error="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2407413
    iget-object v0, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    invoke-static {v0}, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->l(Lcom/facebook/katana/view/LoggedOutWebViewActivity;)V

    .line 2407414
    iget-object v0, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    iget-object v0, v0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->s:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2407415
    return-void
.end method

.method public final onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 4

    .prologue
    .line 2407404
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V

    .line 2407405
    iget-object v0, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    iget-object v0, v0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->x:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dU;->j:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 2407406
    if-nez v0, :cond_0

    .line 2407407
    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->proceed()V

    .line 2407408
    :goto_0
    return-void

    .line 2407409
    :cond_0
    iget-object v0, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    iget-object v0, v0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->u:LX/03V;

    sget-object v1, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->C:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onReceivedSslError:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3}, Landroid/net/http/SslError;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2407410
    iget-object v0, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    invoke-static {v0}, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->l(Lcom/facebook/katana/view/LoggedOutWebViewActivity;)V

    .line 2407411
    iget-object v0, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    iget-object v0, v0, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->s:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 2407387
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2407388
    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    .line 2407389
    invoke-virtual {v1}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v3

    .line 2407390
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    .line 2407391
    const-string v5, "fblogin"

    invoke-static {v5, v2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2407392
    iget-object v2, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    iget-object v5, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    iget-object v5, v5, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->z:Ljava/lang/Class;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v3, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->startActivity(Landroid/content/Intent;)V

    .line 2407393
    iget-object v1, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    invoke-virtual {v1}, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->finish()V

    .line 2407394
    :goto_0
    return v0

    .line 2407395
    :cond_0
    const-string v5, "fbredirect"

    invoke-static {v5, v2}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2407396
    const-string v2, "uri"

    invoke-virtual {v1, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2407397
    iget-object v2, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v3, v4, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v2, v3}, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->startActivity(Landroid/content/Intent;)V

    .line 2407398
    iget-object v1, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    invoke-virtual {v1}, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->finish()V

    goto :goto_0

    .line 2407399
    :cond_1
    const-string v5, "http"

    invoke-static {v2, v5}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    const-string v5, "https"

    invoke-static {v2, v5}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    const-string v2, ".facebook.com"

    invoke-virtual {v3, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2407400
    :cond_3
    iget-object v2, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v3, v4, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v2, v3}, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 2407401
    :cond_4
    const-string v1, "/"

    invoke-static {v4, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "/login.php"

    invoke-static {v4, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2407402
    :cond_5
    iget-object v1, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    iget-object v1, v1, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->w:Lcom/facebook/content/SecureContextHelper;

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    iget-object v3, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    iget-object v3, v3, Lcom/facebook/katana/view/LoggedOutWebViewActivity;->v:Landroid/content/ComponentName;

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v2

    iget-object v3, p0, LX/Gwo;->a:Lcom/facebook/katana/view/LoggedOutWebViewActivity;

    invoke-interface {v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2407403
    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method
