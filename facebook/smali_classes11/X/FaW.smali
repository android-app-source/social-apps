.class public final enum LX/FaW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FaW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FaW;

.field public static final enum OTHER_DISMISS_ACTION:LX/FaW;

.field public static final enum PRIMARY_ACTION:LX/FaW;

.field public static final enum SECONDARY_ACTION:LX/FaW;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2258842
    new-instance v0, LX/FaW;

    const-string v1, "PRIMARY_ACTION"

    const-string v2, "primary_action"

    invoke-direct {v0, v1, v3, v2}, LX/FaW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FaW;->PRIMARY_ACTION:LX/FaW;

    .line 2258843
    new-instance v0, LX/FaW;

    const-string v1, "SECONDARY_ACTION"

    const-string v2, "secondary_action"

    invoke-direct {v0, v1, v4, v2}, LX/FaW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FaW;->SECONDARY_ACTION:LX/FaW;

    .line 2258844
    new-instance v0, LX/FaW;

    const-string v1, "OTHER_DISMISS_ACTION"

    const-string v2, "other_dismiss_action"

    invoke-direct {v0, v1, v5, v2}, LX/FaW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FaW;->OTHER_DISMISS_ACTION:LX/FaW;

    .line 2258845
    const/4 v0, 0x3

    new-array v0, v0, [LX/FaW;

    sget-object v1, LX/FaW;->PRIMARY_ACTION:LX/FaW;

    aput-object v1, v0, v3

    sget-object v1, LX/FaW;->SECONDARY_ACTION:LX/FaW;

    aput-object v1, v0, v4

    sget-object v1, LX/FaW;->OTHER_DISMISS_ACTION:LX/FaW;

    aput-object v1, v0, v5

    sput-object v0, LX/FaW;->$VALUES:[LX/FaW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2258849
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2258850
    iput-object p3, p0, LX/FaW;->value:Ljava/lang/String;

    .line 2258851
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FaW;
    .locals 1

    .prologue
    .line 2258848
    const-class v0, LX/FaW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FaW;

    return-object v0
.end method

.method public static values()[LX/FaW;
    .locals 1

    .prologue
    .line 2258847
    sget-object v0, LX/FaW;->$VALUES:[LX/FaW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FaW;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2258846
    iget-object v0, p0, LX/FaW;->value:Ljava/lang/String;

    return-object v0
.end method
