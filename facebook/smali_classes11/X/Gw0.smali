.class public final LX/Gw0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;",
        "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Gw8;


# direct methods
.method public constructor <init>(LX/Gw8;)V
    .locals 0

    .prologue
    .line 2406436
    iput-object p1, p0, LX/Gw0;->a:LX/Gw8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 5

    .prologue
    .line 2406437
    check-cast p1, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2406438
    iget-object v0, p0, LX/Gw0;->a:LX/Gw8;

    const/4 v4, 0x1

    .line 2406439
    const-string v1, "platform_composer"

    invoke-virtual {p1, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setNectarModule(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisablePhotos(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2406440
    iget-object v1, v0, LX/Gw8;->n:LX/8PB;

    .line 2406441
    iget-object v2, v1, LX/8PB;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2406442
    invoke-static {v1}, LX/Gvy;->c(Ljava/lang/String;)Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;

    move-result-object v1

    .line 2406443
    if-eqz v1, :cond_0

    .line 2406444
    invoke-static {}, Lcom/facebook/ipc/composer/model/ComposerLocationInfo;->newBuilder()LX/5RO;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/5RO;->b(Lcom/facebook/places/graphql/PlacesGraphQLModels$CheckinPlaceModel;)LX/5RO;

    move-result-object v1

    invoke-virtual {v1}, LX/5RO;->b()Lcom/facebook/ipc/composer/model/ComposerLocationInfo;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialLocationInfo(Lcom/facebook/ipc/composer/model/ComposerLocationInfo;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2406445
    :cond_0
    iget-object v1, v0, LX/Gw8;->n:LX/8PB;

    .line 2406446
    iget-object v2, v1, LX/8PB;->l:Ljava/lang/String;

    move-object v1, v2

    .line 2406447
    iget-object v2, v0, LX/Gw8;->n:LX/8PB;

    .line 2406448
    iget-object v3, v2, LX/8PB;->h:Ljava/lang/String;

    move-object v2, v3

    .line 2406449
    if-nez v2, :cond_1

    iget-boolean v2, v0, LX/Gvy;->d:Z

    if-nez v2, :cond_1

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2406450
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration;->getPlatformConfiguration()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/PlatformConfiguration;->a()LX/2ro;

    move-result-object v2

    .line 2406451
    iget-object v3, v0, LX/Gw8;->n:LX/8PB;

    .line 2406452
    iget-object p0, v3, LX/8PB;->h:Ljava/lang/String;

    move-object v3, p0

    .line 2406453
    if-eqz v3, :cond_2

    .line 2406454
    iget-object v3, v0, LX/Gw8;->n:LX/8PB;

    .line 2406455
    iget-object p0, v3, LX/8PB;->h:Ljava/lang/String;

    move-object v3, p0

    .line 2406456
    iput-object v3, v2, LX/2ro;->d:Ljava/lang/String;

    .line 2406457
    :cond_2
    iget-boolean v3, v0, LX/Gvy;->d:Z

    if-eqz v3, :cond_3

    .line 2406458
    iput-boolean v4, v2, LX/2ro;->c:Z

    .line 2406459
    :cond_3
    iput-object v1, v2, LX/2ro;->e:Ljava/lang/String;

    .line 2406460
    invoke-virtual {v2}, LX/2ro;->a()Lcom/facebook/ipc/composer/intent/PlatformConfiguration;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setPlatformConfiguration(Lcom/facebook/ipc/composer/intent/PlatformConfiguration;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2406461
    :cond_4
    iget-object v1, v0, LX/Gw8;->n:LX/8PB;

    .line 2406462
    iget-object v2, v1, LX/8PB;->b:Ljava/util/ArrayList;

    move-object v1, v2

    .line 2406463
    if-eqz v1, :cond_5

    iget-object v1, v0, LX/Gw8;->n:LX/8PB;

    .line 2406464
    iget-object v2, v1, LX/8PB;->b:Ljava/util/ArrayList;

    move-object v1, v2

    .line 2406465
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 2406466
    iget-object v1, v0, LX/Gw8;->n:LX/8PB;

    .line 2406467
    iget-object v2, v1, LX/8PB;->b:Ljava/util/ArrayList;

    move-object v1, v2

    .line 2406468
    invoke-virtual {v0, v1, p1}, LX/Gvy;->a(Ljava/util/ArrayList;Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2406469
    :goto_0
    move-object v0, v1

    .line 2406470
    return-object v0

    :cond_5
    invoke-static {p1}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_0
.end method
