.class public final LX/F3D;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/editsettings/protocol/FetchGroupPossiblePurposesModels$FetchGroupPossiblePurposesModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;)V
    .locals 0

    .prologue
    .line 2193832
    iput-object p1, p0, LX/F3D;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2193833
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2193834
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2193835
    if-eqz p1, :cond_0

    .line 2193836
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2193837
    if-nez v0, :cond_1

    .line 2193838
    :cond_0
    :goto_0
    return-void

    .line 2193839
    :cond_1
    iget-object v3, p0, LX/F3D;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    .line 2193840
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2193841
    check-cast v0, Lcom/facebook/groups/editsettings/protocol/FetchGroupPossiblePurposesModels$FetchGroupPossiblePurposesModel;

    .line 2193842
    iput-object v0, v3, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->g:Lcom/facebook/groups/editsettings/protocol/FetchGroupPossiblePurposesModels$FetchGroupPossiblePurposesModel;

    .line 2193843
    const/4 v3, 0x0

    .line 2193844
    iget-object v0, p0, LX/F3D;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->f:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->p()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_4

    .line 2193845
    iget-object v0, p0, LX/F3D;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->f:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->p()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v5, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    invoke-virtual {v4, v0, v2, v5}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2193846
    if-eqz v0, :cond_2

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    if-eqz v0, :cond_7

    .line 2193847
    iget-object v0, p0, LX/F3D;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->f:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->p()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v5, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    invoke-virtual {v4, v0, v2, v5}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2193848
    if-eqz v0, :cond_5

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    :goto_4
    if-eqz v1, :cond_9

    .line 2193849
    iget-object v0, p0, LX/F3D;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->f:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->p()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    invoke-virtual {v1, v0, v2, v3}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_5
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    .line 2193850
    :goto_6
    iget-object v1, p0, LX/F3D;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    new-instance v2, LX/F3T;

    iget-object v3, p0, LX/F3D;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    iget-object v3, v3, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->g:Lcom/facebook/groups/editsettings/protocol/FetchGroupPossiblePurposesModels$FetchGroupPossiblePurposesModel;

    invoke-virtual {v3}, Lcom/facebook/groups/editsettings/protocol/FetchGroupPossiblePurposesModels$FetchGroupPossiblePurposesModel;->a()LX/0Px;

    move-result-object v3

    invoke-direct {v2, v3, v0}, LX/F3T;-><init>(LX/0Px;Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;)V

    .line 2193851
    iput-object v2, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->h:LX/F3T;

    .line 2193852
    iget-object v0, p0, LX/F3D;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->i:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/F3D;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    iget-object v1, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->h:LX/F3T;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    goto/16 :goto_0

    .line 2193853
    :cond_2
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2193854
    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_2

    .line 2193855
    :cond_5
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2193856
    goto :goto_3

    :cond_6
    move v1, v2

    goto :goto_4

    :cond_7
    move v1, v2

    goto :goto_4

    .line 2193857
    :cond_8
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2193858
    goto :goto_5

    :cond_9
    move-object v0, v3

    goto :goto_6
.end method
