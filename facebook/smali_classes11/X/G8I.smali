.class public LX/G8I;
.super LX/G8E;
.source ""


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, LX/G8E;-><init>()V

    iput-object p1, p0, LX/G8I;->a:Landroid/content/Context;

    return-void
.end method

.method private b()V
    .locals 4

    iget-object v0, p0, LX/G8I;->a:Landroid/content/Context;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-static {v0, v1}, LX/2Cs;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x34

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Calling UID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not Google Play services."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private c()V
    .locals 4

    iget-object v0, p0, LX/G8I;->a:Landroid/content/Context;

    invoke-static {v0}, LX/4sO;->a(Landroid/content/Context;)LX/4sO;

    move-result-object v1

    invoke-virtual {v1}, LX/4sO;->a()Lcom/google/android/gms/auth/api/signin/GoogleSignInAccount;

    move-result-object v2

    sget-object v0, Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;->d:Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;

    if-eqz v2, :cond_0

    invoke-virtual {v1}, LX/4sO;->b()Lcom/google/android/gms/auth/api/signin/GoogleSignInOptions;

    move-result-object v0

    :cond_0
    new-instance v1, LX/2vz;

    iget-object v3, p0, LX/G8I;->a:Landroid/content/Context;

    invoke-direct {v1, v3}, LX/2vz;-><init>(Landroid/content/Context;)V

    sget-object v3, LX/G7R;->f:LX/2vs;

    invoke-virtual {v1, v3, v0}, LX/2vz;->a(LX/2vs;LX/2w6;)LX/2vz;

    move-result-object v0

    invoke-virtual {v0}, LX/2vz;->b()LX/2wX;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, LX/2wX;->f()Lcom/google/android/gms/common/ConnectionResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/ConnectionResult;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v2, :cond_2

    sget-object v0, LX/G7R;->k:LX/G82;

    invoke-interface {v0, v1}, LX/G82;->a(LX/2wX;)LX/2wg;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    invoke-virtual {v1}, LX/2wX;->g()V

    return-void

    :cond_2
    :try_start_1
    invoke-virtual {v1}, LX/2wX;->h()LX/2wg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/2wX;->g()V

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    invoke-direct {p0}, LX/G8I;->b()V

    invoke-direct {p0}, LX/G8I;->c()V

    return-void
.end method
