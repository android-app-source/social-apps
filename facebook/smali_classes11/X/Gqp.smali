.class public LX/Gqp;
.super LX/Cod;
.source ""

# interfaces
.implements LX/CnG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/Gpr;",
        ">;",
        "Lcom/facebook/instantshopping/view/block/ToggleButtonBlockView;"
    }
.end annotation


# instance fields
.field public a:LX/GnF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/CIe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/CIb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/1mR;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Go0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final h:Landroid/widget/FrameLayout;

.field public final i:LX/CtG;

.field public j:LX/Gpr;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 8

    .prologue
    .line 2396957
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 2396958
    const v0, 0x7f0d05fe

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/Gqp;->h:Landroid/widget/FrameLayout;

    .line 2396959
    const v0, 0x7f0d184f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2396960
    iget-object p1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, p1

    .line 2396961
    iput-object v0, p0, LX/Gqp;->i:LX/CtG;

    .line 2396962
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/Gqp;

    invoke-static {v0}, LX/GnF;->a(LX/0QB;)LX/GnF;

    move-result-object v3

    check-cast v3, LX/GnF;

    invoke-static {v0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v4

    check-cast v4, LX/Chv;

    invoke-static {v0}, LX/CIe;->a(LX/0QB;)LX/CIe;

    move-result-object v5

    check-cast v5, LX/CIe;

    invoke-static {v0}, LX/CIb;->a(LX/0QB;)LX/CIb;

    move-result-object v6

    check-cast v6, LX/CIb;

    invoke-static {v0}, LX/1mR;->a(LX/0QB;)LX/1mR;

    move-result-object v7

    check-cast v7, LX/1mR;

    invoke-static {v0}, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->a(LX/0QB;)Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    move-result-object p1

    check-cast p1, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    invoke-static {v0}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object v0

    check-cast v0, LX/Go0;

    iput-object v3, v2, LX/Gqp;->a:LX/GnF;

    iput-object v4, v2, LX/Gqp;->b:LX/Chv;

    iput-object v5, v2, LX/Gqp;->c:LX/CIe;

    iput-object v6, v2, LX/Gqp;->d:LX/CIb;

    iput-object v7, v2, LX/Gqp;->e:LX/1mR;

    iput-object p1, v2, LX/Gqp;->f:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    iput-object v0, v2, LX/Gqp;->g:LX/Go0;

    .line 2396963
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 2396964
    iget-object v0, p0, LX/Gqp;->i:LX/CtG;

    invoke-virtual {v0, p1}, LX/CtG;->setTextColor(I)V

    .line 2396965
    return-void
.end method

.method public final a(LX/Clf;)V
    .locals 1

    .prologue
    .line 2396966
    if-eqz p1, :cond_0

    .line 2396967
    iget-object v0, p0, LX/Gqp;->i:LX/CtG;

    invoke-virtual {v0, p1}, LX/CtG;->setText(LX/Clf;)V

    .line 2396968
    :cond_0
    return-void
.end method

.method public final a(LX/CnT;)V
    .locals 0

    .prologue
    .line 2396969
    check-cast p1, LX/Gpr;

    .line 2396970
    invoke-super {p0, p1}, LX/Cod;->a(LX/CnT;)V

    .line 2396971
    iput-object p1, p0, LX/Gqp;->j:LX/Gpr;

    .line 2396972
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2396973
    invoke-super {p0, p1}, LX/Cod;->a(Landroid/os/Bundle;)V

    .line 2396974
    invoke-virtual {p0}, LX/Cod;->c()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2396975
    iget-object v0, p0, LX/Gqp;->i:LX/CtG;

    invoke-virtual {v0}, LX/CtG;->a()V

    .line 2396976
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 2396977
    iget-object v0, p0, LX/Gqp;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 2396978
    return-void
.end method
