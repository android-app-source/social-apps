.class public LX/GwZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

.field public final b:LX/0aG;


# direct methods
.method public constructor <init>(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/0aG;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2407149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2407150
    iput-object p1, p0, LX/GwZ;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2407151
    iput-object p2, p0, LX/GwZ;->b:LX/0aG;

    .line 2407152
    return-void
.end method

.method public static a(LX/0QB;)LX/GwZ;
    .locals 3

    .prologue
    .line 2407146
    new-instance v2, LX/GwZ;

    invoke-static {p0}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(LX/0QB;)Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-static {p0}, LX/0aF;->createInstance__com_facebook_fbservice_ops_DefaultBlueServiceOperationFactory__INJECTED_BY_TemplateInjector(LX/0QB;)LX/0aF;

    move-result-object v1

    check-cast v1, LX/0aG;

    invoke-direct {v2, v0, v1}, LX/GwZ;-><init>(Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/0aG;)V

    .line 2407147
    move-object v0, v2

    .line 2407148
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;Ljava/lang/String;LX/0m9;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Intent;",
            "Ljava/lang/String;",
            "LX/0m9;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2407138
    const-string v0, "publishPostParams"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/facebook/composer/publish/common/PublishPostParams;

    .line 2407139
    new-instance v0, Lcom/facebook/platform/params/PlatformComposerParams;

    iget-object v1, v10, Lcom/facebook/composer/publish/common/PublishPostParams;->androidKeyHash:Ljava/lang/String;

    iget-boolean v2, v10, Lcom/facebook/composer/publish/common/PublishPostParams;->isExplicitLocation:Z

    iget-boolean v3, v10, Lcom/facebook/composer/publish/common/PublishPostParams;->isTagsUserSelected:Z

    iget-object v4, v10, Lcom/facebook/composer/publish/common/PublishPostParams;->placeTag:Ljava/lang/String;

    iget-object v5, v10, Lcom/facebook/composer/publish/common/PublishPostParams;->privacy:Ljava/lang/String;

    iget-object v6, v10, Lcom/facebook/composer/publish/common/PublishPostParams;->proxiedAppId:Ljava/lang/String;

    iget-object v7, v10, Lcom/facebook/composer/publish/common/PublishPostParams;->proxiedAppName:Ljava/lang/String;

    iget-object v8, v10, Lcom/facebook/composer/publish/common/PublishPostParams;->rawMessage:Ljava/lang/String;

    iget-object v9, v10, Lcom/facebook/composer/publish/common/PublishPostParams;->taggedIds:LX/0Px;

    invoke-static {v9}, LX/0RA;->b(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, Lcom/facebook/platform/params/PlatformComposerParams;-><init>(Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2407140
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2407141
    const-string v2, "publishPostParams"

    invoke-virtual {v1, v2, v10}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2407142
    const-string v2, "platformParams"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2407143
    new-instance v0, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionOperation$Params;

    invoke-direct {v0, p2, p3}, Lcom/facebook/platform/opengraph/server/PublishOpenGraphActionOperation$Params;-><init>(Ljava/lang/String;LX/0m9;)V

    .line 2407144
    const-string v2, "platform_publish_open_graph_action_params"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2407145
    iget-object v0, p0, LX/GwZ;->a:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    const-string v2, "platform_publish_open_graph_action"

    invoke-virtual {v0, v1, v2}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->a(Landroid/os/Bundle;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
