.class public final enum LX/FFF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FFF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FFF;

.field public static final enum ITEM_ROW:LX/FFF;

.field public static final enum SECTION_HEADER:LX/FFF;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2216972
    new-instance v0, LX/FFF;

    const-string v1, "SECTION_HEADER"

    invoke-direct {v0, v1, v2}, LX/FFF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FFF;->SECTION_HEADER:LX/FFF;

    .line 2216973
    new-instance v0, LX/FFF;

    const-string v1, "ITEM_ROW"

    invoke-direct {v0, v1, v3}, LX/FFF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FFF;->ITEM_ROW:LX/FFF;

    .line 2216974
    const/4 v0, 0x2

    new-array v0, v0, [LX/FFF;

    sget-object v1, LX/FFF;->SECTION_HEADER:LX/FFF;

    aput-object v1, v0, v2

    sget-object v1, LX/FFF;->ITEM_ROW:LX/FFF;

    aput-object v1, v0, v3

    sput-object v0, LX/FFF;->$VALUES:[LX/FFF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2216971
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static fromOridnal(I)LX/FFF;
    .locals 1

    .prologue
    .line 2216970
    invoke-static {}, LX/FFF;->values()[LX/FFF;

    move-result-object v0

    aget-object v0, v0, p0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/FFF;
    .locals 1

    .prologue
    .line 2216968
    const-class v0, LX/FFF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FFF;

    return-object v0
.end method

.method public static values()[LX/FFF;
    .locals 1

    .prologue
    .line 2216969
    sget-object v0, LX/FFF;->$VALUES:[LX/FFF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FFF;

    return-object v0
.end method
