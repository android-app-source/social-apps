.class public LX/GMm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static c:LX/01T;

.field private static volatile f:LX/GMm;


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;

.field private final b:LX/0ad;

.field private final d:LX/0Uh;

.field public final e:LX/16I;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/0ad;LX/01T;LX/0Uh;LX/16I;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2345223
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2345224
    iput-object p1, p0, LX/GMm;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2345225
    iput-object p2, p0, LX/GMm;->b:LX/0ad;

    .line 2345226
    sput-object p3, LX/GMm;->c:LX/01T;

    .line 2345227
    iput-object p4, p0, LX/GMm;->d:LX/0Uh;

    .line 2345228
    iput-object p5, p0, LX/GMm;->e:LX/16I;

    .line 2345229
    return-void
.end method

.method public static a(LX/0QB;)LX/GMm;
    .locals 9

    .prologue
    .line 2345230
    sget-object v0, LX/GMm;->f:LX/GMm;

    if-nez v0, :cond_1

    .line 2345231
    const-class v1, LX/GMm;

    monitor-enter v1

    .line 2345232
    :try_start_0
    sget-object v0, LX/GMm;->f:LX/GMm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2345233
    if-eqz v2, :cond_0

    .line 2345234
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2345235
    new-instance v3, LX/GMm;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v6

    check-cast v6, LX/01T;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    invoke-static {v0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v8

    check-cast v8, LX/16I;

    invoke-direct/range {v3 .. v8}, LX/GMm;-><init>(Lcom/facebook/content/SecureContextHelper;LX/0ad;LX/01T;LX/0Uh;LX/16I;)V

    .line 2345236
    move-object v0, v3

    .line 2345237
    sput-object v0, LX/GMm;->f:LX/GMm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2345238
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2345239
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2345240
    :cond_1
    sget-object v0, LX/GMm;->f:LX/GMm;

    return-object v0

    .line 2345241
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2345242
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2345243
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const/4 p0, 0x1

    .line 2345244
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2345245
    const-string v2, "force_in_app_browser"

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2345246
    const-string v2, "should_hide_menu"

    invoke-virtual {v1, v2, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2345247
    sget-object v2, LX/GMm;->c:LX/01T;

    sget-object p0, LX/01T;->PAA:LX/01T;

    if-ne v2, p0, :cond_0

    .line 2345248
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2345249
    :cond_0
    sget-object v2, LX/GMm;->c:LX/01T;

    sget-object p0, LX/01T;->FB4A:LX/01T;

    if-ne v2, p0, :cond_1

    .line 2345250
    sget-object v2, LX/0ax;->do:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2345251
    :cond_1
    move-object v0, v1

    .line 2345252
    return-object v0
.end method

.method public static a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;I)V
    .locals 0

    .prologue
    .line 2345253
    invoke-virtual {p0, p2}, Landroid/view/View;->setVisibility(I)V

    .line 2345254
    if-eqz p1, :cond_0

    .line 2345255
    invoke-virtual {p1, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setVisibility(I)V

    .line 2345256
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(ILjava/lang/String;Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;LX/GCE;)Landroid/text/SpannableString;
    .locals 5

    .prologue
    .line 2345257
    invoke-virtual {p3}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2345258
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2345259
    const v2, 0x7f080a3c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2345260
    const v3, 0x7f0a0124

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 2345261
    new-instance v4, LX/47x;

    invoke-direct {v4, v0}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2345262
    invoke-virtual {v4, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2345263
    const-string v0, "[[ads_manager_link]]"

    .line 2345264
    new-instance v1, LX/GMj;

    invoke-direct {v1, p0, p4, p2, v3}, LX/GMj;-><init>(LX/GMm;LX/GCE;Ljava/lang/String;I)V

    move-object v1, v1

    .line 2345265
    const/16 v3, 0x21

    invoke-virtual {v4, v0, v2, v1, v3}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2345266
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2345267
    invoke-virtual {v4}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;ILandroid/content/Context;)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 2345268
    new-instance v0, LX/GMk;

    invoke-direct {v0, p0, p1, p3, p2}, LX/GMk;-><init>(LX/GMm;Ljava/lang/String;Landroid/content/Context;I)V

    return-object v0
.end method

.method public final a(Landroid/content/Context;J)V
    .locals 2

    .prologue
    .line 2345269
    invoke-virtual {p0}, LX/GMm;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2345270
    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    const-string v1, "pages_manager_activity_tab"

    .line 2345271
    sget-object p2, LX/8wL;->PAGE_LIKE:LX/8wL;

    const p3, 0x7f080b04

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-static {p1, p2, p3, v1}, LX/8wJ;->a(Landroid/content/Context;LX/8wL;Ljava/lang/Integer;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p2

    .line 2345272
    const-string p3, "page_id"

    invoke-virtual {p2, p3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2345273
    move-object v0, p2

    .line 2345274
    iget-object v1, p0, LX/GMm;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2345275
    :goto_0
    return-void

    .line 2345276
    :cond_0
    const-string v0, "https://m.facebook.com/pages/boosted_page_like/view/?pid=%s&hide_chrome=1"

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2345277
    const/4 p3, 0x1

    .line 2345278
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2345279
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p2

    invoke-virtual {v1, p2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2345280
    const-string p2, "force_in_app_browser"

    invoke-virtual {v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2345281
    const-string p2, "should_hide_menu"

    invoke-virtual {v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2345282
    iget-object p2, p0, LX/GMm;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p2, v1, p1}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2345283
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2345284
    invoke-static {p1}, LX/GMm;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2345285
    sget-object v1, LX/GMm;->c:LX/01T;

    sget-object v2, LX/01T;->PAA:LX/01T;

    if-ne v1, v2, :cond_1

    .line 2345286
    iget-object v1, p0, LX/GMm;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2345287
    :cond_0
    :goto_0
    return-void

    .line 2345288
    :cond_1
    sget-object v1, LX/GMm;->c:LX/01T;

    sget-object v2, LX/01T;->FB4A:LX/01T;

    if-ne v1, v2, :cond_0

    .line 2345289
    iget-object v1, p0, LX/GMm;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final c()Z
    .locals 3

    .prologue
    .line 2345290
    iget-object v0, p0, LX/GMm;->d:LX/0Uh;

    const/16 v1, 0x2f2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method
