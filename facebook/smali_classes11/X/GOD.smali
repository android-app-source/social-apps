.class public final LX/GOD;
.super LX/GOC;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/adspayments/activity/BrazilianAddressActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;)V
    .locals 0

    .prologue
    .line 2347323
    iput-object p1, p0, LX/GOD;->a:Lcom/facebook/adspayments/activity/BrazilianAddressActivity;

    invoke-direct {p0}, LX/GOC;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 2347324
    iget-object v0, p0, LX/GOD;->a:Lcom/facebook/adspayments/activity/BrazilianAddressActivity;

    iget-object v0, v0, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->L:Lcom/facebook/payments/ui/PaymentFormEditTextView;

    invoke-virtual {v0}, Lcom/facebook/payments/ui/PaymentFormEditTextView;->getInputText()Ljava/lang/String;

    move-result-object v0

    .line 2347325
    sget-object v1, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;->s:Lcom/facebook/common/locale/Country;

    invoke-static {v0, v1}, LX/46H;->a(Ljava/lang/CharSequence;Lcom/facebook/common/locale/Country;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2347326
    iget-object v1, p0, LX/GOD;->a:Lcom/facebook/adspayments/activity/BrazilianAddressActivity;

    invoke-static {v1, v0}, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->b(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;Ljava/lang/String;)V

    .line 2347327
    :goto_0
    return-void

    .line 2347328
    :cond_0
    iget-object v0, p0, LX/GOD;->a:Lcom/facebook/adspayments/activity/BrazilianAddressActivity;

    invoke-static {v0}, Lcom/facebook/adspayments/activity/BrazilianAddressActivity;->q(Lcom/facebook/adspayments/activity/BrazilianAddressActivity;)V

    goto :goto_0
.end method
