.class public final LX/FPS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# instance fields
.field public final synthetic a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;)V
    .locals 0

    .prologue
    .line 2237225
    iput-object p1, p0, LX/FPS;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2237226
    iget-object v0, p0, LX/FPS;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    sget-object v1, LX/FPW;->DISPLAYING_RESULTS:LX/FPW;

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->a$redex0(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;LX/FPW;)V

    .line 2237227
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2237228
    check-cast p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    .line 2237229
    if-nez p1, :cond_1

    .line 2237230
    :cond_0
    :goto_0
    return-void

    .line 2237231
    :cond_1
    iget-object v0, p0, LX/FPS;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    .line 2237232
    iget-object v1, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    invoke-virtual {v1, p1}, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a(Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;)V

    .line 2237233
    iget-object v1, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->s:LX/FPI;

    iget-object v2, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    invoke-virtual {v1, v2}, LX/FPI;->a(Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)V

    .line 2237234
    iget-object v1, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->s:LX/FPI;

    const v2, -0x63628a8d

    invoke-static {v1, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2237235
    iget-object v1, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->l:LX/FPO;

    if-eqz v1, :cond_2

    .line 2237236
    iget-object v1, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->l:LX/FPO;

    iget-object v2, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->t:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    invoke-interface {v1, v2}, LX/FPO;->a(Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;)V

    .line 2237237
    :cond_2
    iget-object v1, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->v:LX/FPa;

    invoke-virtual {v1}, LX/FPa;->a()V

    .line 2237238
    iget-object v1, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->v:LX/FPa;

    .line 2237239
    iget-object v2, v1, LX/FPa;->a:LX/0Zb;

    const-string v0, "nearby_places_results_list_received_results"

    invoke-static {v1, v0}, LX/FPa;->a(LX/FPa;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v2, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2237240
    iget-object v0, p0, LX/FPS;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    sget-object v1, LX/FPW;->DISPLAYING_RESULTS:LX/FPW;

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->a$redex0(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;LX/FPW;)V

    .line 2237241
    if-eqz p1, :cond_3

    .line 2237242
    iget-boolean v0, p1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->k:Z

    move v0, v0

    .line 2237243
    if-eqz v0, :cond_0

    .line 2237244
    :cond_3
    iget-object v0, p0, LX/FPS;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->q:Lcom/facebook/widget/listview/BetterListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->smoothScrollToPosition(I)V

    goto :goto_0
.end method
