.class public LX/GfG;
.super LX/3mU;
.source ""


# instance fields
.field private final a:LX/25J;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/25J",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;


# direct methods
.method public constructor <init>(LX/25J;Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/25J",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2377122
    invoke-direct {p0}, LX/3mU;-><init>()V

    .line 2377123
    iput-object p1, p0, LX/GfG;->a:LX/25J;

    .line 2377124
    iput-object p2, p0, LX/GfG;->b:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    .line 2377125
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2377129
    iget-object v0, p0, LX/GfG;->a:LX/25J;

    iget-object v1, p0, LX/GfG;->b:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-virtual {v0, v1}, LX/25J;->b(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V

    .line 2377130
    return-void
.end method

.method public final a(I)Z
    .locals 2

    .prologue
    .line 2377126
    iget-object v0, p0, LX/GfG;->a:LX/25J;

    iget-object v1, p0, LX/GfG;->b:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-virtual {v0, v1, p1}, LX/25J;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GfG;->a:LX/25J;

    iget-object v1, p0, LX/GfG;->b:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-virtual {v0, v1}, LX/25J;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2377127
    const/4 v0, 0x1

    .line 2377128
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
