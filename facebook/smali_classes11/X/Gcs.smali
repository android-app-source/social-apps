.class public final LX/Gcs;
.super LX/BlU;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/events/carousel/EventCardViewBinder;


# direct methods
.method public constructor <init>(Lcom/facebook/events/carousel/EventCardViewBinder;)V
    .locals 0

    .prologue
    .line 2372665
    iput-object p1, p0, LX/Gcs;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    invoke-direct {p0}, LX/BlU;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 2372666
    check-cast p1, LX/BlT;

    .line 2372667
    iget-object v0, p0, LX/Gcs;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v0, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Gcs;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v0, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    .line 2372668
    iget-object v1, v0, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2372669
    iget-object v1, p1, LX/BlT;->c:Lcom/facebook/events/model/Event;

    .line 2372670
    iget-object v2, v1, Lcom/facebook/events/model/Event;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2372671
    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2372672
    iget-object v0, p1, LX/BlT;->a:LX/BlI;

    sget-object v1, LX/BlI;->FAILURE:LX/BlI;

    if-ne v0, v1, :cond_1

    .line 2372673
    iget-object v0, p0, LX/Gcs;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v1, p1, LX/BlT;->b:Lcom/facebook/events/model/Event;

    .line 2372674
    iput-object v1, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    .line 2372675
    :goto_0
    iget-object v0, p0, LX/Gcs;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    invoke-static {v0}, Lcom/facebook/events/carousel/EventCardViewBinder;->c(Lcom/facebook/events/carousel/EventCardViewBinder;)V

    .line 2372676
    :cond_0
    return-void

    .line 2372677
    :cond_1
    iget-object v0, p0, LX/Gcs;->a:Lcom/facebook/events/carousel/EventCardViewBinder;

    iget-object v1, p1, LX/BlT;->c:Lcom/facebook/events/model/Event;

    .line 2372678
    iput-object v1, v0, Lcom/facebook/events/carousel/EventCardViewBinder;->n:Lcom/facebook/events/model/Event;

    .line 2372679
    goto :goto_0
.end method
