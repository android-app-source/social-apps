.class public final LX/G6N;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Landroid/widget/TextView;

.field public final synthetic c:LX/G6O;


# direct methods
.method public constructor <init>(LX/G6O;Ljava/lang/String;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 2320000
    iput-object p1, p0, LX/G6N;->c:LX/G6O;

    iput-object p2, p0, LX/G6N;->a:Ljava/lang/String;

    iput-object p3, p0, LX/G6N;->b:Landroid/widget/TextView;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 2320001
    new-instance v1, LX/G6M;

    invoke-direct {v1, p0}, LX/G6M;-><init>(LX/G6N;)V

    .line 2320002
    iget-object v0, p0, LX/G6N;->c:LX/G6O;

    iget-object v0, v0, LX/G6O;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G6f;

    iget-object v2, p0, LX/G6N;->a:Ljava/lang/String;

    .line 2320003
    new-instance v3, LX/4DG;

    invoke-direct {v3}, LX/4DG;-><init>()V

    .line 2320004
    const-string v4, "photo_fbid"

    invoke-virtual {v3, v4, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2320005
    move-object v4, v3

    .line 2320006
    iget-object v3, v0, LX/G6f;->b:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2320007
    const-string v5, "actor_id"

    invoke-virtual {v4, v5, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2320008
    move-object v3, v4

    .line 2320009
    new-instance v4, LX/G6R;

    invoke-direct {v4}, LX/G6R;-><init>()V

    .line 2320010
    const-string v5, "input"

    invoke-virtual {v4, v5, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2320011
    invoke-static {v4}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 2320012
    iget-object v4, v0, LX/G6f;->a:LX/1Ck;

    const-string v5, "change_privacy"

    iget-object p1, v0, LX/G6f;->c:LX/0tX;

    invoke-virtual {p1, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    invoke-virtual {v4, v5, v3, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2320013
    iget-object v0, p0, LX/G6N;->c:LX/G6O;

    iget-object v0, v0, LX/G6O;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2320014
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 2320015
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 2320016
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2320017
    iget-object v0, p0, LX/G6N;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x106000b

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2320018
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 2320019
    return-void
.end method
