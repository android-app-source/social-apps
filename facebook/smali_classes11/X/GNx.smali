.class public final LX/GNx;
.super LX/GNw;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GNw",
        "<",
        "Lcom/facebook/adspayments/protocol/CvvPrepayData;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/model/CreditCard;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/adspayments/activity/AddPaymentCardActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/AddPaymentCardActivity;Lcom/facebook/payments/paymentmethods/model/CreditCard;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2346670
    iput-object p1, p0, LX/GNx;->c:Lcom/facebook/adspayments/activity/AddPaymentCardActivity;

    iput-object p2, p0, LX/GNx;->a:Lcom/facebook/payments/paymentmethods/model/CreditCard;

    iput-object p3, p0, LX/GNx;->b:Ljava/lang/String;

    invoke-direct {p0, p1}, LX/GNw;-><init>(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;)V

    return-void
.end method

.method private a(Lcom/facebook/adspayments/protocol/CvvPrepayData;)V
    .locals 6

    .prologue
    .line 2346671
    iget-object v0, p0, LX/GNx;->c:Lcom/facebook/adspayments/activity/AddPaymentCardActivity;

    invoke-virtual {v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->u()V

    .line 2346672
    invoke-virtual {p1}, Lcom/facebook/adspayments/protocol/CvvPrepayData;->c()LX/50M;

    move-result-object v4

    .line 2346673
    new-instance v0, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;

    iget-object v1, p0, LX/GNx;->a:Lcom/facebook/payments/paymentmethods/model/CreditCard;

    iget-object v2, p0, LX/GNx;->b:Ljava/lang/String;

    invoke-virtual {v4}, LX/50M;->d()Ljava/lang/Comparable;

    move-result-object v3

    check-cast v3, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {v4}, LX/50M;->g()Ljava/lang/Comparable;

    move-result-object v4

    check-cast v4, Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2346674
    iget-object v5, p1, Lcom/facebook/common/util/Quartet;->a:Ljava/lang/Object;

    check-cast v5, Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v5, v5

    .line 2346675
    invoke-direct/range {v0 .. v5}, Lcom/facebook/adspayments/model/CvvPrepayCreditCard;-><init>(Lcom/facebook/payments/paymentmethods/model/CreditCard;Ljava/lang/String;Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/payments/currency/CurrencyAmount;)V

    .line 2346676
    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v1, v1

    .line 2346677
    if-eqz v1, :cond_0

    .line 2346678
    iget-object v1, p0, LX/GNx;->c:Lcom/facebook/adspayments/activity/AddPaymentCardActivity;

    .line 2346679
    invoke-static {v1, v0}, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->a$redex0(Lcom/facebook/adspayments/activity/AddPaymentCardActivity;Lcom/facebook/adspayments/model/CvvPrepayCreditCard;)V

    .line 2346680
    :goto_0
    return-void

    .line 2346681
    :cond_0
    iget-object v1, p0, LX/GNx;->c:Lcom/facebook/adspayments/activity/AddPaymentCardActivity;

    .line 2346682
    invoke-static {v1, v0}, Lcom/facebook/adspayments/activity/AddPaymentCardActivity;->b$redex0(Lcom/facebook/adspayments/activity/AddPaymentCardActivity;Lcom/facebook/adspayments/model/CvvPrepayCreditCard;)V

    .line 2346683
    goto :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2346684
    iget-object v0, p0, LX/GNx;->c:Lcom/facebook/adspayments/activity/AddPaymentCardActivity;

    invoke-virtual {v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->u()V

    .line 2346685
    invoke-super {p0, p1}, LX/GNw;->onNonCancellationFailure(Ljava/lang/Throwable;)V

    .line 2346686
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2346687
    check-cast p1, Lcom/facebook/adspayments/protocol/CvvPrepayData;

    invoke-direct {p0, p1}, LX/GNx;->a(Lcom/facebook/adspayments/protocol/CvvPrepayData;)V

    return-void
.end method
