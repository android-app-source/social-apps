.class public final LX/Gva;
.super LX/4Ae;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/katana/gdp/PlatformDialogActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/gdp/PlatformDialogActivity;)V
    .locals 0

    .prologue
    .line 2405647
    iput-object p1, p0, LX/Gva;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    invoke-direct {p0}, LX/4Ae;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailed(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 5

    .prologue
    .line 2405648
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 2405649
    sget-object v1, LX/1nY;->API_ERROR:LX/1nY;

    if-ne v0, v1, :cond_0

    .line 2405650
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/ServiceException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    check-cast v0, LX/2Oo;

    .line 2405651
    invoke-virtual {v0}, LX/2Oo;->a()Lcom/facebook/http/protocol/ApiErrorResult;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/http/protocol/ApiErrorResult;->a()I

    move-result v0

    .line 2405652
    const/16 v1, 0xbe

    if-ne v0, v1, :cond_0

    .line 2405653
    iget-object v0, p0, LX/Gva;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    invoke-static {v0}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;)Lcom/facebook/katana/service/AppSession;

    move-result-object v0

    .line 2405654
    iget-object v1, p0, LX/Gva;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    iget-object v1, v1, Lcom/facebook/katana/gdp/PlatformDialogActivity;->D:LX/278;

    invoke-virtual {v0, v1}, Lcom/facebook/katana/service/AppSession;->a(LX/278;)V

    .line 2405655
    iget-object v1, p0, LX/Gva;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    sget-object v2, LX/279;->FORCED_ERROR_INVALID_SESSION:LX/279;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;LX/279;)V

    .line 2405656
    :goto_0
    return-void

    .line 2405657
    :cond_0
    iget-object v0, p0, LX/Gva;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    invoke-static {v0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->u(Lcom/facebook/katana/gdp/PlatformDialogActivity;)V

    .line 2405658
    sget-object v0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->s:Ljava/lang/Class;

    const-string v1, "Login failed due to error: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/facebook/fbservice/service/ServiceException;->getMessage()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2405659
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2405660
    const-string v1, "error"

    invoke-virtual {p1}, Lcom/facebook/fbservice/service/ServiceException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405661
    const-string v1, "error_code"

    .line 2405662
    iget-object v2, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v2, v2

    .line 2405663
    invoke-virtual {v2}, LX/1nY;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405664
    iget-object v1, p0, LX/Gva;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    invoke-virtual {v1, v0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->e(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final onSucceeded(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 4

    .prologue
    .line 2405665
    iget-object v0, p0, LX/Gva;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    .line 2405666
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/katana/service/AppSession;->b(Landroid/content/Context;Z)Lcom/facebook/katana/service/AppSession;

    move-result-object v1

    .line 2405667
    if-nez v1, :cond_0

    .line 2405668
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->e(Landroid/os/Bundle;)V

    .line 2405669
    :goto_0
    return-void

    .line 2405670
    :cond_0
    iget-object v1, v0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->q:Ljava/lang/String;

    iput-object v1, v0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->v:Ljava/lang/String;

    .line 2405671
    iget-object v1, v0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->v:Ljava/lang/String;

    invoke-static {v1}, LX/FC4;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2405672
    new-instance v2, LX/0Yj;

    const v3, 0x380007

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->r(Lcom/facebook/katana/gdp/PlatformDialogActivity;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, LX/0Yj;-><init>(ILjava/lang/String;)V

    const-string v1, "e2e"

    invoke-virtual {v0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string p0, "e2e"

    invoke-virtual {v3, p0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0Yj;->a(Ljava/lang/String;Ljava/lang/String;)LX/0Yj;

    move-result-object v1

    .line 2405673
    iget-object v2, v0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->r:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v2, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 2405674
    iget-object v1, v0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->q:Ljava/lang/String;

    invoke-static {v0, v1}, LX/FBg;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->q:Ljava/lang/String;

    .line 2405675
    invoke-virtual {v0}, Lcom/facebook/base/activity/FbFragmentActivity;->iz_()LX/0QA;

    move-result-object v1

    invoke-static {v1}, LX/48V;->b(LX/0QB;)LX/48V;

    move-result-object v1

    check-cast v1, LX/48V;

    iget-object v2, v0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->w:Lcom/facebook/webview/BasicWebView;

    iget-object v3, v0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->q:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, LX/48V;->a(Landroid/webkit/WebView;Ljava/lang/String;)V

    goto :goto_0
.end method
