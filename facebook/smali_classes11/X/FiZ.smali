.class public final LX/FiZ;
.super LX/0xh;
.source ""


# instance fields
.field public final synthetic a:F

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:LX/0wS;


# direct methods
.method public constructor <init>(LX/0wS;FLandroid/view/View;)V
    .locals 0

    .prologue
    .line 2275503
    iput-object p1, p0, LX/FiZ;->c:LX/0wS;

    iput p2, p0, LX/FiZ;->a:F

    iput-object p3, p0, LX/FiZ;->b:Landroid/view/View;

    invoke-direct {p0}, LX/0xh;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0wd;)V
    .locals 2

    .prologue
    .line 2275508
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 2275509
    iget-object v1, p0, LX/FiZ;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 2275510
    iget-object v1, p0, LX/FiZ;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleX(F)V

    .line 2275511
    iget-object v1, p0, LX/FiZ;->b:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleY(F)V

    .line 2275512
    return-void
.end method

.method public final b(LX/0wd;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2275504
    invoke-virtual {p1}, LX/0wd;->d()D

    move-result-wide v0

    double-to-float v0, v0

    .line 2275505
    iget v1, p0, LX/FiZ;->a:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    cmpl-float v0, v0, v2

    if-nez v0, :cond_0

    .line 2275506
    iget-object v0, p0, LX/FiZ;->b:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2275507
    :cond_0
    return-void
.end method
