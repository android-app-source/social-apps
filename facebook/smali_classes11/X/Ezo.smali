.class public final LX/Ezo;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/3Fw;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Ezp;


# direct methods
.method public constructor <init>(LX/Ezp;)V
    .locals 0

    .prologue
    .line 2187663
    iput-object p1, p0, LX/Ezo;->a:LX/Ezp;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2187686
    iget-object v0, p0, LX/Ezo;->a:LX/Ezp;

    iget-object v0, v0, LX/Ezp;->c:LX/Ezl;

    if-eqz v0, :cond_0

    .line 2187687
    iget-object v0, p0, LX/Ezo;->a:LX/Ezp;

    iget-object v0, v0, LX/Ezp;->c:LX/Ezl;

    .line 2187688
    sget-object p0, LX/Ezk;->ERROR:LX/Ezk;

    iput-object p0, v0, LX/Ezl;->d:LX/Ezk;

    .line 2187689
    iget-object p0, v0, LX/Ezl;->e:LX/Ezg;

    if-eqz p0, :cond_0

    .line 2187690
    iget-object p0, v0, LX/Ezl;->e:LX/Ezg;

    invoke-interface {p0}, LX/Ezg;->d()V

    .line 2187691
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 2187664
    check-cast p1, LX/3Fw;

    .line 2187665
    iget-object v0, p0, LX/Ezo;->a:LX/Ezp;

    iget-object v0, v0, LX/Ezp;->c:LX/Ezl;

    if-eqz v0, :cond_2

    .line 2187666
    iget-object v0, p0, LX/Ezo;->a:LX/Ezp;

    iget-object v0, v0, LX/Ezp;->c:LX/Ezl;

    .line 2187667
    sget-object v1, LX/Ezk;->IDLE:LX/Ezk;

    iput-object v1, v0, LX/Ezl;->d:LX/Ezk;

    .line 2187668
    iget-object v1, v0, LX/Ezl;->a:LX/Ezn;

    iget-object v2, p1, LX/3Fw;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2187669
    iput-object v2, v1, LX/Ezn;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2187670
    iget-object v1, v0, LX/Ezl;->c:LX/Ezm;

    iget-object v2, p1, LX/3Fw;->a:LX/0Px;

    .line 2187671
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2187672
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/friends/model/PersonYouMayKnow;

    .line 2187673
    iget-object v6, v1, LX/Ezm;->a:Ljava/util/Set;

    invoke-virtual {v3}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 2187674
    iget-object v6, v1, LX/Ezm;->a:Ljava/util/Set;

    invoke-virtual {v3}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2187675
    iget-boolean v6, v3, Lcom/facebook/friends/model/PersonYouMayKnow;->g:Z

    move v6, v6

    .line 2187676
    if-nez v6, :cond_0

    invoke-virtual {v3}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v6, v7, :cond_0

    .line 2187677
    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2187678
    :cond_1
    move-object v1, v4

    .line 2187679
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2187680
    :cond_2
    :goto_1
    return-void

    .line 2187681
    :cond_3
    iget-object v2, v0, LX/Ezl;->a:LX/Ezn;

    .line 2187682
    iget-object v3, v2, LX/Ezn;->a:Ljava/util/List;

    move-object v2, v3

    .line 2187683
    invoke-interface {v2, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2187684
    iget-object v1, v0, LX/Ezl;->e:LX/Ezg;

    if-eqz v1, :cond_2

    .line 2187685
    iget-object v1, v0, LX/Ezl;->e:LX/Ezg;

    invoke-interface {v1}, LX/Ezg;->d()V

    goto :goto_1
.end method
