.class public final enum LX/FRw;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/6vZ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FRw;",
        ">;",
        "LX/6vZ;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FRw;

.field public static final enum ADS_MANAGER:LX/FRw;

.field public static final enum DOUBLE_ROW_DIVIDER:LX/FRw;

.field public static final enum FACEBOOK_GAMES:LX/FRw;

.field public static final enum ORDER_INFORMATION:LX/FRw;

.field public static final enum PAYMENT_HISTORY:LX/FRw;

.field public static final enum PAYMENT_METHODS:LX/FRw;

.field public static final enum SECURITY:LX/FRw;

.field public static final enum SUPPORT:LX/FRw;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2241121
    new-instance v0, LX/FRw;

    const-string v1, "ADS_MANAGER"

    invoke-direct {v0, v1, v3}, LX/FRw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FRw;->ADS_MANAGER:LX/FRw;

    .line 2241122
    new-instance v0, LX/FRw;

    const-string v1, "DOUBLE_ROW_DIVIDER"

    invoke-direct {v0, v1, v4}, LX/FRw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FRw;->DOUBLE_ROW_DIVIDER:LX/FRw;

    .line 2241123
    new-instance v0, LX/FRw;

    const-string v1, "FACEBOOK_GAMES"

    invoke-direct {v0, v1, v5}, LX/FRw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FRw;->FACEBOOK_GAMES:LX/FRw;

    .line 2241124
    new-instance v0, LX/FRw;

    const-string v1, "PAYMENT_METHODS"

    invoke-direct {v0, v1, v6}, LX/FRw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FRw;->PAYMENT_METHODS:LX/FRw;

    .line 2241125
    new-instance v0, LX/FRw;

    const-string v1, "PAYMENT_HISTORY"

    invoke-direct {v0, v1, v7}, LX/FRw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FRw;->PAYMENT_HISTORY:LX/FRw;

    .line 2241126
    new-instance v0, LX/FRw;

    const-string v1, "SECURITY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/FRw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FRw;->SECURITY:LX/FRw;

    .line 2241127
    new-instance v0, LX/FRw;

    const-string v1, "ORDER_INFORMATION"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/FRw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FRw;->ORDER_INFORMATION:LX/FRw;

    .line 2241128
    new-instance v0, LX/FRw;

    const-string v1, "SUPPORT"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/FRw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FRw;->SUPPORT:LX/FRw;

    .line 2241129
    const/16 v0, 0x8

    new-array v0, v0, [LX/FRw;

    sget-object v1, LX/FRw;->ADS_MANAGER:LX/FRw;

    aput-object v1, v0, v3

    sget-object v1, LX/FRw;->DOUBLE_ROW_DIVIDER:LX/FRw;

    aput-object v1, v0, v4

    sget-object v1, LX/FRw;->FACEBOOK_GAMES:LX/FRw;

    aput-object v1, v0, v5

    sget-object v1, LX/FRw;->PAYMENT_METHODS:LX/FRw;

    aput-object v1, v0, v6

    sget-object v1, LX/FRw;->PAYMENT_HISTORY:LX/FRw;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/FRw;->SECURITY:LX/FRw;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/FRw;->ORDER_INFORMATION:LX/FRw;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/FRw;->SUPPORT:LX/FRw;

    aput-object v2, v0, v1

    sput-object v0, LX/FRw;->$VALUES:[LX/FRw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2241130
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FRw;
    .locals 1

    .prologue
    .line 2241131
    const-class v0, LX/FRw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FRw;

    return-object v0
.end method

.method public static values()[LX/FRw;
    .locals 1

    .prologue
    .line 2241120
    sget-object v0, LX/FRw;->$VALUES:[LX/FRw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FRw;

    return-object v0
.end method
