.class public final LX/F9V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/F9W;


# direct methods
.method public constructor <init>(LX/F9W;)V
    .locals 0

    .prologue
    .line 2205517
    iput-object p1, p0, LX/F9V;->a:LX/F9W;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2205518
    iget-object v0, p0, LX/F9V;->a:LX/F9W;

    iget-object v0, v0, LX/F9W;->c:LX/0kL;

    new-instance v1, LX/27k;

    const-string v2, "NUX status reset complete, but server fetch failed. Log out and back in to see NUX."

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2205519
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2205520
    iget-object v0, p0, LX/F9V;->a:LX/F9W;

    iget-object v0, v0, LX/F9W;->c:LX/0kL;

    new-instance v1, LX/27k;

    const-string v2, "NUX status fetched. Choose the Launch option below or log out and back in to see NUX."

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2205521
    return-void
.end method
