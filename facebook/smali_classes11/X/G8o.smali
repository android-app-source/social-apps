.class public final enum LX/G8o;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/G8o;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/G8o;

.field public static final enum AZTEC:LX/G8o;

.field public static final enum CODABAR:LX/G8o;

.field public static final enum CODE_128:LX/G8o;

.field public static final enum CODE_39:LX/G8o;

.field public static final enum CODE_93:LX/G8o;

.field public static final enum DATA_MATRIX:LX/G8o;

.field public static final enum EAN_13:LX/G8o;

.field public static final enum EAN_8:LX/G8o;

.field public static final enum ITF:LX/G8o;

.field public static final enum MAXICODE:LX/G8o;

.field public static final enum PDF_417:LX/G8o;

.field public static final enum QR_CODE:LX/G8o;

.field public static final enum RSS_14:LX/G8o;

.field public static final enum RSS_EXPANDED:LX/G8o;

.field public static final enum UPC_A:LX/G8o;

.field public static final enum UPC_E:LX/G8o;

.field public static final enum UPC_EAN_EXTENSION:LX/G8o;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2321227
    new-instance v0, LX/G8o;

    const-string v1, "AZTEC"

    invoke-direct {v0, v1, v3}, LX/G8o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8o;->AZTEC:LX/G8o;

    .line 2321228
    new-instance v0, LX/G8o;

    const-string v1, "CODABAR"

    invoke-direct {v0, v1, v4}, LX/G8o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8o;->CODABAR:LX/G8o;

    .line 2321229
    new-instance v0, LX/G8o;

    const-string v1, "CODE_39"

    invoke-direct {v0, v1, v5}, LX/G8o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8o;->CODE_39:LX/G8o;

    .line 2321230
    new-instance v0, LX/G8o;

    const-string v1, "CODE_93"

    invoke-direct {v0, v1, v6}, LX/G8o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8o;->CODE_93:LX/G8o;

    .line 2321231
    new-instance v0, LX/G8o;

    const-string v1, "CODE_128"

    invoke-direct {v0, v1, v7}, LX/G8o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8o;->CODE_128:LX/G8o;

    .line 2321232
    new-instance v0, LX/G8o;

    const-string v1, "DATA_MATRIX"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/G8o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8o;->DATA_MATRIX:LX/G8o;

    .line 2321233
    new-instance v0, LX/G8o;

    const-string v1, "EAN_8"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/G8o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8o;->EAN_8:LX/G8o;

    .line 2321234
    new-instance v0, LX/G8o;

    const-string v1, "EAN_13"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/G8o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8o;->EAN_13:LX/G8o;

    .line 2321235
    new-instance v0, LX/G8o;

    const-string v1, "ITF"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/G8o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8o;->ITF:LX/G8o;

    .line 2321236
    new-instance v0, LX/G8o;

    const-string v1, "MAXICODE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/G8o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8o;->MAXICODE:LX/G8o;

    .line 2321237
    new-instance v0, LX/G8o;

    const-string v1, "PDF_417"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/G8o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8o;->PDF_417:LX/G8o;

    .line 2321238
    new-instance v0, LX/G8o;

    const-string v1, "QR_CODE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, LX/G8o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8o;->QR_CODE:LX/G8o;

    .line 2321239
    new-instance v0, LX/G8o;

    const-string v1, "RSS_14"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, LX/G8o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8o;->RSS_14:LX/G8o;

    .line 2321240
    new-instance v0, LX/G8o;

    const-string v1, "RSS_EXPANDED"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, LX/G8o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8o;->RSS_EXPANDED:LX/G8o;

    .line 2321241
    new-instance v0, LX/G8o;

    const-string v1, "UPC_A"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, LX/G8o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8o;->UPC_A:LX/G8o;

    .line 2321242
    new-instance v0, LX/G8o;

    const-string v1, "UPC_E"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, LX/G8o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8o;->UPC_E:LX/G8o;

    .line 2321243
    new-instance v0, LX/G8o;

    const-string v1, "UPC_EAN_EXTENSION"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, LX/G8o;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G8o;->UPC_EAN_EXTENSION:LX/G8o;

    .line 2321244
    const/16 v0, 0x11

    new-array v0, v0, [LX/G8o;

    sget-object v1, LX/G8o;->AZTEC:LX/G8o;

    aput-object v1, v0, v3

    sget-object v1, LX/G8o;->CODABAR:LX/G8o;

    aput-object v1, v0, v4

    sget-object v1, LX/G8o;->CODE_39:LX/G8o;

    aput-object v1, v0, v5

    sget-object v1, LX/G8o;->CODE_93:LX/G8o;

    aput-object v1, v0, v6

    sget-object v1, LX/G8o;->CODE_128:LX/G8o;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/G8o;->DATA_MATRIX:LX/G8o;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/G8o;->EAN_8:LX/G8o;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/G8o;->EAN_13:LX/G8o;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/G8o;->ITF:LX/G8o;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/G8o;->MAXICODE:LX/G8o;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/G8o;->PDF_417:LX/G8o;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/G8o;->QR_CODE:LX/G8o;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/G8o;->RSS_14:LX/G8o;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/G8o;->RSS_EXPANDED:LX/G8o;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/G8o;->UPC_A:LX/G8o;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/G8o;->UPC_E:LX/G8o;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/G8o;->UPC_EAN_EXTENSION:LX/G8o;

    aput-object v2, v0, v1

    sput-object v0, LX/G8o;->$VALUES:[LX/G8o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2321245
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/G8o;
    .locals 1

    .prologue
    .line 2321226
    const-class v0, LX/G8o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/G8o;

    return-object v0
.end method

.method public static values()[LX/G8o;
    .locals 1

    .prologue
    .line 2321225
    sget-object v0, LX/G8o;->$VALUES:[LX/G8o;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/G8o;

    return-object v0
.end method
