.class public final LX/Gxj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/view/LayoutInflater;

.field public final synthetic b:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;Landroid/view/LayoutInflater;)V
    .locals 0

    .prologue
    .line 2408411
    iput-object p1, p0, LX/Gxj;->b:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iput-object p2, p0, LX/Gxj;->a:Landroid/view/LayoutInflater;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, -0x68ac530e

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2408412
    iget-object v1, p0, LX/Gxj;->b:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iget-object v1, v1, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->e:LX/27g;

    invoke-virtual {v1}, LX/27g;->b()LX/27h;

    move-result-object v1

    .line 2408413
    invoke-virtual {v1}, LX/27h;->a()[Ljava/lang/String;

    move-result-object v2

    .line 2408414
    new-instance v3, LX/31Y;

    iget-object v4, p0, LX/Gxj;->b:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, LX/27h;->b()[Ljava/lang/String;

    move-result-object v1

    new-instance v4, LX/Gxi;

    invoke-direct {v4, p0, v2}, LX/Gxi;-><init>(LX/Gxj;[Ljava/lang/String;)V

    invoke-virtual {v3, v1, v4}, LX/0ju;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f0835ca

    new-instance v3, LX/Gxh;

    invoke-direct {v3, p0}, LX/Gxh;-><init>(LX/Gxj;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->c(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    invoke-virtual {v1}, LX/2EJ;->show()V

    .line 2408415
    iget-object v1, p0, LX/Gxj;->b:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iget-object v1, v1, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->b:LX/Gxv;

    .line 2408416
    iget-object v2, v1, LX/Gxv;->a:LX/0Zb;

    sget-object v3, LX/Gxu;->OTHER_LANGUAGES_CLICKED:LX/Gxu;

    invoke-static {v3}, LX/Gxv;->a(LX/Gxu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2408417
    const v1, -0x7c12a2ce

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
