.class public LX/H22;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0po;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:[Ljava/lang/String;

.field private static volatile i:LX/H22;


# instance fields
.field private final c:I

.field private final d:I

.field private final e:LX/0SG;

.field public final f:LX/H1w;

.field private final g:LX/H1p;

.field public h:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2416262
    const-class v0, LX/H22;

    sput-object v0, LX/H22;->a:Ljava/lang/Class;

    .line 2416263
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, LX/H1x;->a:LX/0U1;

    .line 2416264
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2416265
    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/H1x;->h:LX/0U1;

    .line 2416266
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2416267
    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/H1x;->j:LX/0U1;

    .line 2416268
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2416269
    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/H1x;->b:LX/0U1;

    .line 2416270
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2416271
    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/H1x;->c:LX/0U1;

    .line 2416272
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2416273
    aput-object v2, v0, v1

    sput-object v0, LX/H22;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/H1w;LX/H1p;LX/0pq;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2416120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2416121
    const/16 v0, 0x64

    iput v0, p0, LX/H22;->c:I

    .line 2416122
    const/16 v0, 0xa

    iput v0, p0, LX/H22;->d:I

    .line 2416123
    iput-object p1, p0, LX/H22;->e:LX/0SG;

    .line 2416124
    iput-object p2, p0, LX/H22;->f:LX/H1w;

    .line 2416125
    iput-object p3, p0, LX/H22;->g:LX/H1p;

    .line 2416126
    invoke-virtual {p4, p0}, LX/0pq;->a(LX/0po;)V

    .line 2416127
    return-void
.end method

.method public static a(LX/0QB;)LX/H22;
    .locals 7

    .prologue
    .line 2416249
    sget-object v0, LX/H22;->i:LX/H22;

    if-nez v0, :cond_1

    .line 2416250
    const-class v1, LX/H22;

    monitor-enter v1

    .line 2416251
    :try_start_0
    sget-object v0, LX/H22;->i:LX/H22;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2416252
    if-eqz v2, :cond_0

    .line 2416253
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2416254
    new-instance p0, LX/H22;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    invoke-static {v0}, LX/H1w;->a(LX/0QB;)LX/H1w;

    move-result-object v4

    check-cast v4, LX/H1w;

    invoke-static {v0}, LX/H1p;->a(LX/0QB;)LX/H1p;

    move-result-object v5

    check-cast v5, LX/H1p;

    invoke-static {v0}, LX/0pq;->a(LX/0QB;)LX/0pq;

    move-result-object v6

    check-cast v6, LX/0pq;

    invoke-direct {p0, v3, v4, v5, v6}, LX/H22;-><init>(LX/0SG;LX/H1w;LX/H1p;LX/0pq;)V

    .line 2416255
    move-object v0, p0

    .line 2416256
    sput-object v0, LX/H22;->i:LX/H22;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2416257
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2416258
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2416259
    :cond_1
    sget-object v0, LX/H22;->i:LX/H22;

    return-object v0

    .line 2416260
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2416261
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/H22;I)V
    .locals 3

    .prologue
    .line 2416239
    invoke-static {p0}, LX/H22;->f(LX/H22;)V

    .line 2416240
    iget-object v0, p0, LX/H22;->f:LX/H1w;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2416241
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DELETE FROM nearby_tiles WHERE "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, LX/H1x;->a:LX/0U1;

    .line 2416242
    iget-object p0, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, p0

    .line 2416243
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " NOT IN (SELECT "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/H1x;->a:LX/0U1;

    .line 2416244
    iget-object p0, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, p0

    .line 2416245
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " FROM nearby_tiles"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ORDER BY "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/H1x;->j:LX/0U1;

    .line 2416246
    iget-object p0, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, p0

    .line 2416247
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " DESC LIMIT "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const v2, 0x35f1fa38

    invoke-static {v2}, LX/03h;->a(I)V

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x7bef9e3

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2416248
    return-void
.end method

.method private static e(LX/H22;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 2416230
    iget-object v0, p0, LX/H22;->h:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2416231
    :goto_0
    return-void

    .line 2416232
    :cond_0
    iget-object v0, p0, LX/H22;->f:LX/H1w;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2416233
    const-string v1, "nearby_tiles_version"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    sget-object v5, LX/H1z;->a:LX/0U1;

    .line 2416234
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 2416235
    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2416236
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2416237
    sget-object v0, LX/H1z;->a:LX/0U1;

    invoke-virtual {v0, v1}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/H22;->h:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2416238
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static f(LX/H22;)V
    .locals 4

    .prologue
    .line 2416222
    iget-object v0, p0, LX/H22;->f:LX/H1w;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2416223
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/H1x;->i:LX/0U1;

    .line 2416224
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2416225
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " + "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, LX/H1x;->j:LX/0U1;

    .line 2416226
    iget-object v3, v2, LX/0U1;->d:Ljava/lang/String;

    move-object v2, v3

    .line 2416227
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/H22;->e:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2416228
    const-string v2, "nearby_tiles"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2416229
    return-void
.end method


# virtual methods
.method public final U_()V
    .locals 1

    .prologue
    .line 2416220
    const/16 v0, 0xa

    invoke-static {p0, v0}, LX/H22;->b(LX/H22;I)V

    .line 2416221
    return-void
.end method

.method public final a(Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;)V
    .locals 12

    .prologue
    .line 2416158
    iget-object v0, p1, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2416159
    invoke-static {p0}, LX/H22;->e(LX/H22;)V

    .line 2416160
    iget-object v1, p0, LX/H22;->h:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/H22;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2416161
    :cond_0
    iput-object v0, p0, LX/H22;->h:Ljava/lang/String;

    .line 2416162
    iget-object v0, p0, LX/H22;->f:LX/H1w;

    invoke-virtual {v0}, LX/0Tr;->f()V

    .line 2416163
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2416164
    sget-object v1, LX/H1z;->a:LX/0U1;

    .line 2416165
    iget-object v2, v1, LX/0U1;->d:Ljava/lang/String;

    move-object v1, v2

    .line 2416166
    iget-object v2, p0, LX/H22;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2416167
    iget-object v1, p0, LX/H22;->f:LX/H1w;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2416168
    const-string v2, "nearby_tiles_version"

    const/4 v3, 0x0

    const v4, -0x72a1d7bc

    invoke-static {v4}, LX/03h;->a(I)V

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, 0x36543362

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2416169
    :cond_1
    iget-object v0, p0, LX/H22;->f:LX/H1w;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 2416170
    iget-object v0, p0, LX/H22;->e:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 2416171
    iget-object v0, p1, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->b:Ljava/util/List;

    move-object v0, v0

    .line 2416172
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/model/MapTile;

    .line 2416173
    iget-object v5, p1, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->d:LX/0Rf;

    move-object v6, v5

    .line 2416174
    invoke-static {v6}, LX/H1p;->c(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v5

    .line 2416175
    iget-object v6, p1, Lcom/facebook/nearby/protocol/NearbyTilesWithLayoutsResult;->e:LX/0Rf;

    move-object v7, v6

    .line 2416176
    invoke-static {v7}, LX/H1p;->c(Ljava/util/Set;)Ljava/lang/String;

    move-result-object v6

    .line 2416177
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 2416178
    sget-object v8, LX/H1x;->a:LX/0U1;

    .line 2416179
    iget-object v9, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, v9

    .line 2416180
    iget-object v9, v0, Lcom/facebook/nearby/model/MapTile;->id:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2416181
    sget-object v8, LX/H1x;->b:LX/0U1;

    .line 2416182
    iget-object v9, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, v9

    .line 2416183
    invoke-virtual {v7, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2416184
    sget-object v8, LX/H1x;->c:LX/0U1;

    .line 2416185
    iget-object v9, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, v9

    .line 2416186
    invoke-virtual {v7, v8, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2416187
    sget-object v8, LX/H1x;->d:LX/0U1;

    .line 2416188
    iget-object v9, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, v9

    .line 2416189
    iget-object v9, v0, Lcom/facebook/nearby/model/MapTile;->bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->j()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2416190
    sget-object v8, LX/H1x;->e:LX/0U1;

    .line 2416191
    iget-object v9, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, v9

    .line 2416192
    iget-object v9, v0, Lcom/facebook/nearby/model/MapTile;->bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->k()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2416193
    sget-object v8, LX/H1x;->f:LX/0U1;

    .line 2416194
    iget-object v9, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, v9

    .line 2416195
    iget-object v9, v0, Lcom/facebook/nearby/model/MapTile;->bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->a()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2416196
    sget-object v8, LX/H1x;->g:LX/0U1;

    .line 2416197
    iget-object v9, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, v9

    .line 2416198
    iget-object v9, v0, Lcom/facebook/nearby/model/MapTile;->bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->l()D

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2416199
    sget-object v8, LX/H1x;->i:LX/0U1;

    .line 2416200
    iget-object v9, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, v9

    .line 2416201
    invoke-virtual {v0}, Lcom/facebook/nearby/model/MapTile;->a()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2416202
    sget-object v8, LX/H1x;->j:LX/0U1;

    .line 2416203
    iget-object v9, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, v9

    .line 2416204
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2416205
    sget-object v8, LX/H1x;->h:LX/0U1;

    .line 2416206
    iget-object v9, v8, LX/0U1;->d:Ljava/lang/String;

    move-object v8, v9

    .line 2416207
    iget-object v9, p0, LX/H22;->g:LX/H1p;

    .line 2416208
    iget-object v10, v9, LX/H1p;->a:LX/0lC;

    invoke-virtual {v10, v0}, LX/0lC;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    move-object v9, v10

    .line 2416209
    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2416210
    const-string v8, "nearby_tiles"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, LX/H1x;->a:LX/0U1;

    .line 2416211
    iget-object v11, v10, LX/0U1;->d:Ljava/lang/String;

    move-object v10, v11

    .line 2416212
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " = ? AND "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, LX/H1x;->b:LX/0U1;

    .line 2416213
    iget-object v11, v10, LX/0U1;->d:Ljava/lang/String;

    move-object v10, v11

    .line 2416214
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " = ? AND "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, LX/H1x;->c:LX/0U1;

    .line 2416215
    iget-object v11, v10, LX/0U1;->d:Ljava/lang/String;

    move-object v10, v11

    .line 2416216
    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " = ? "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    iget-object v0, v0, Lcom/facebook/nearby/model/MapTile;->id:Ljava/lang/String;

    aput-object v0, v10, v11

    const/4 v0, 0x1

    aput-object v5, v10, v0

    const/4 v0, 0x2

    aput-object v6, v10, v0

    invoke-virtual {v1, v8, v7, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 2416217
    if-nez v0, :cond_2

    .line 2416218
    const-string v0, "nearby_tiles"

    const/4 v5, 0x0

    const v6, 0x6806293c

    invoke-static {v6}, LX/03h;->a(I)V

    invoke-virtual {v1, v0, v5, v7}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    const v0, -0x1e49a3ad

    invoke-static {v0}, LX/03h;->a(I)V

    goto/16 :goto_0

    .line 2416219
    :cond_3
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2416156
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/H22;->b(LX/H22;I)V

    .line 2416157
    return-void
.end method

.method public final declared-synchronized c()LX/H1v;
    .locals 14

    .prologue
    .line 2416128
    monitor-enter p0

    :try_start_0
    invoke-static {p0}, LX/H22;->e(LX/H22;)V

    .line 2416129
    invoke-static {p0}, LX/H22;->f(LX/H22;)V

    .line 2416130
    const/16 v0, 0x64

    invoke-static {p0, v0}, LX/H22;->b(LX/H22;I)V

    .line 2416131
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v8

    .line 2416132
    iget-object v0, p0, LX/H22;->f:LX/H1w;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 2416133
    const-string v1, "nearby_tiles"

    sget-object v2, LX/H22;->b:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 2416134
    :try_start_1
    sget-object v0, LX/H1x;->h:LX/0U1;

    invoke-virtual {v0, v6}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v7

    .line 2416135
    sget-object v0, LX/H1x;->b:LX/0U1;

    invoke-virtual {v0, v6}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v9

    .line 2416136
    sget-object v0, LX/H1x;->c:LX/0U1;

    invoke-virtual {v0, v6}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v10

    .line 2416137
    sget-object v0, LX/H1x;->j:LX/0U1;

    invoke-virtual {v0, v6}, LX/0U1;->a(Landroid/database/Cursor;)I

    move-result v11

    .line 2416138
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eqz v0, :cond_0

    .line 2416139
    :try_start_2
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2416140
    invoke-interface {v6, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2416141
    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2416142
    invoke-interface {v6, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 2416143
    iget-object v1, p0, LX/H22;->g:LX/H1p;

    .line 2416144
    iget-object v12, v1, LX/H1p;->a:LX/0lC;

    const-class v13, Lcom/facebook/nearby/model/MapTile;

    invoke-virtual {v12, v0, v13}, LX/0lC;->a(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/facebook/nearby/model/MapTile;

    move-object v1, v12

    .line 2416145
    invoke-static {v4}, LX/H1p;->d(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v4

    .line 2416146
    invoke-static {v5}, LX/H1p;->e(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v5

    .line 2416147
    new-instance v0, LX/H29;

    invoke-direct/range {v0 .. v5}, LX/H29;-><init>(Lcom/facebook/nearby/model/MapTile;JLjava/util/Set;Ljava/util/Set;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    .line 2416148
    :catch_0
    :try_start_3
    sget-object v0, LX/H22;->a:Ljava/lang/Class;

    const-string v1, "There is corrupt tiles JSON data"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 2416149
    :catch_1
    move-exception v0

    .line 2416150
    :try_start_4
    sget-object v1, LX/H22;->a:Ljava/lang/Class;

    const-string v2, "Nearby Tiles database corrupted"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2416151
    :try_start_5
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2416152
    :goto_1
    new-instance v0, LX/H1v;

    iget-object v1, p0, LX/H22;->h:Ljava/lang/String;

    invoke-direct {v0, v1, v8}, LX/H1v;-><init>(Ljava/lang/String;Ljava/util/List;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    monitor-exit p0

    return-object v0

    .line 2416153
    :cond_0
    :try_start_6
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_1

    .line 2416154
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2416155
    :catchall_1
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method
