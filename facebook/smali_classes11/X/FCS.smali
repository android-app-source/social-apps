.class public final LX/FCS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/os/MessageQueue$IdleHandler;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;)V
    .locals 0

    .prologue
    .line 2210324
    iput-object p1, p0, LX/FCS;->a:Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final queueIdle()Z
    .locals 8

    .prologue
    const v7, 0x540003

    const/16 v6, 0x54

    const/16 v5, 0xc

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2210325
    iget-object v2, p0, LX/FCS;->a:Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;

    iget-object v2, v2, Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;->a:LX/2Mq;

    .line 2210326
    iget-object v3, v2, LX/2Mq;->b:LX/0Sg;

    .line 2210327
    iget-object v4, v3, LX/0Sg;->g:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0Uo;

    invoke-virtual {v4}, LX/0Uo;->h()Z

    move-result v4

    move v3, v4

    .line 2210328
    if-eqz v3, :cond_2

    iget-object v3, v2, LX/2Mq;->b:LX/0Sg;

    invoke-virtual {v3}, LX/0Sg;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v2, LX/2Mq;->b:LX/0Sg;

    invoke-virtual {v3}, LX/0Sg;->b()Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v3, 0x1

    :goto_0
    move v2, v3

    .line 2210329
    if-eqz v2, :cond_1

    .line 2210330
    iget-object v2, p0, LX/FCS;->a:Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;

    iget-object v2, v2, Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;->a:LX/2Mq;

    iget-object v2, v2, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const-string v3, "caught_on_idle"

    invoke-interface {v2, v6, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(SLjava/lang/String;)V

    .line 2210331
    iget-object v2, p0, LX/FCS;->a:Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;

    iget-object v2, v2, Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;->a:LX/2Mq;

    const v3, 0x540002

    const-string v4, "ui_idle"

    invoke-static {v2, v3, v4}, LX/2Mq;->b(LX/2Mq;ILjava/lang/String;)V

    .line 2210332
    iget-object v2, p0, LX/FCS;->a:Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;

    iget-object v2, v2, Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;->a:LX/2Mq;

    const v3, 0x540004

    const-string v4, "ui_idle"

    invoke-static {v2, v3, v4}, LX/2Mq;->b(LX/2Mq;ILjava/lang/String;)V

    .line 2210333
    iget-object v2, p0, LX/FCS;->a:Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;

    iget-object v2, v2, Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;->a:LX/2Mq;

    iget-object v2, v2, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v2, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->f(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2210334
    iget-object v2, p0, LX/FCS;->a:Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;

    iget-object v2, v2, Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;->a:LX/2Mq;

    .line 2210335
    iput-boolean v1, v2, LX/2Mq;->l:Z

    .line 2210336
    iget-object v1, p0, LX/FCS;->a:Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;

    iget-object v1, v1, Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;->a:LX/2Mq;

    iget-object v1, v1, LX/2Mq;->f:LX/0Xp;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "background_started"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0Xp;->a(Landroid/content/Intent;)Z

    .line 2210337
    iget-object v1, p0, LX/FCS;->a:Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;

    iget-object v1, v1, Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;->a:LX/2Mq;

    iget-object v1, v1, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 2210338
    iget-object v1, p0, LX/FCS;->a:Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;

    iget-object v1, v1, Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;->a:LX/2Mq;

    iget-object v1, v1, LX/2Mq;->d:LX/2Mr;

    .line 2210339
    const/4 v2, 0x0

    iput-boolean v2, v1, LX/2Mr;->b:Z

    .line 2210340
    invoke-static {}, LX/1gM;->loadIsNotColdStartRunMarker()V

    .line 2210341
    :cond_0
    iget-object v1, p0, LX/FCS;->a:Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;

    iget-object v1, v1, Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;->a:LX/2Mq;

    iget-object v1, v1, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x540001

    const v3, 0x927c0

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->i(II)V

    .line 2210342
    iget-object v1, p0, LX/FCS;->a:Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;

    iget-object v1, v1, Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;->a:LX/2Mq;

    iget-object v1, v1, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x540034

    const v3, 0xea60

    invoke-interface {v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->i(II)V

    .line 2210343
    iget-object v1, p0, LX/FCS;->a:Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;

    iget-object v1, v1, Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;->a:LX/2Mq;

    iget-object v1, v1, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x54000a

    iget-object v3, p0, LX/FCS;->a:Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;

    iget-object v3, v3, Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;->a:LX/2Mq;

    iget v3, v3, LX/2Mq;->i:I

    invoke-interface {v1, v2, v3, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IIS)V

    .line 2210344
    iget-object v1, p0, LX/FCS;->a:Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;

    iget-object v1, v1, Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;->a:LX/2Mq;

    iget-object v1, v1, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v2, 0x540008

    invoke-interface {v1, v2, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2210345
    iget-object v1, p0, LX/FCS;->a:Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;

    iget-object v1, v1, Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;->a:LX/2Mq;

    iget-object v1, v1, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(S)V

    .line 2210346
    iget-object v1, p0, LX/FCS;->a:Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;

    iget-object v1, v1, Lcom/facebook/messaging/analytics/perf/MessagingPerformanceLogger$1;->a:LX/2Mq;

    .line 2210347
    iput-boolean v0, v1, LX/2Mq;->h:Z

    .line 2210348
    :goto_1
    return v0

    :cond_1
    move v0, v1

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto/16 :goto_0
.end method
