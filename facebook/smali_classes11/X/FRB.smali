.class public LX/FRB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6w1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6w1",
        "<",
        "Lcom/facebook/payments/history/picker/PaymentHistoryPickerRunTimeData;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/1Ck;

.field private final b:LX/FRR;

.field public c:LX/70k;


# direct methods
.method public constructor <init>(LX/1Ck;LX/FRR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2240083
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2240084
    iput-object p1, p0, LX/FRB;->a:LX/1Ck;

    .line 2240085
    iput-object p2, p0, LX/FRB;->b:LX/FRR;

    .line 2240086
    return-void
.end method

.method public static a$redex0(LX/FRB;Ljava/lang/Long;LX/6zj;Lcom/facebook/payments/history/model/PaymentTransactions;)V
    .locals 4
    .param p0    # LX/FRB;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/6zj;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2240087
    new-instance v0, LX/FRA;

    invoke-direct {v0, p0, p2, p3, p1}, LX/FRA;-><init>(LX/FRB;LX/6zj;Lcom/facebook/payments/history/model/PaymentTransactions;Ljava/lang/Long;)V

    .line 2240088
    iget-object v1, p0, LX/FRB;->b:LX/FRR;

    invoke-static {}, Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;->newBuilder()LX/FRO;

    move-result-object v2

    .line 2240089
    iput-object p1, v2, LX/FRO;->a:Ljava/lang/Long;

    .line 2240090
    move-object v2, v2

    .line 2240091
    const/16 v3, 0x32

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 2240092
    iput-object v3, v2, LX/FRO;->b:Ljava/lang/Integer;

    .line 2240093
    move-object v2, v2

    .line 2240094
    invoke-virtual {v2}, LX/FRO;->c()Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/FRR;->a(Lcom/facebook/payments/history/protocol/GetPaymentHistoryParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2240095
    iget-object v2, p0, LX/FRB;->a:LX/1Ck;

    const-string v3, "fetch_payment_history_key"

    invoke-virtual {v2, v3, v1, v0}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2240096
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2240097
    iget-object v0, p0, LX/FRB;->a:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2240098
    return-void
.end method

.method public final a(LX/6zj;Lcom/facebook/payments/picker/model/PickerRunTimeData;)V
    .locals 2

    .prologue
    .line 2240099
    const/4 v1, 0x0

    .line 2240100
    iget-object v0, p0, LX/FRB;->c:LX/70k;

    invoke-virtual {v0}, LX/70k;->a()V

    .line 2240101
    invoke-static {p0, v1, p1, v1}, LX/FRB;->a$redex0(LX/FRB;Ljava/lang/Long;LX/6zj;Lcom/facebook/payments/history/model/PaymentTransactions;)V

    .line 2240102
    return-void
.end method

.method public final a(LX/70k;)V
    .locals 0

    .prologue
    .line 2240103
    iput-object p1, p0, LX/FRB;->c:LX/70k;

    .line 2240104
    return-void
.end method

.method public final b(LX/6zj;Lcom/facebook/payments/picker/model/PickerRunTimeData;)V
    .locals 2

    .prologue
    .line 2240105
    check-cast p2, Lcom/facebook/payments/history/picker/PaymentHistoryPickerRunTimeData;

    .line 2240106
    iget-object v0, p2, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v0, v0

    .line 2240107
    check-cast v0, Lcom/facebook/payments/history/picker/PaymentHistoryCoreClientData;

    iget-object v0, v0, Lcom/facebook/payments/history/picker/PaymentHistoryCoreClientData;->a:Lcom/facebook/payments/history/model/PaymentTransactions;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/history/model/PaymentTransactions;

    .line 2240108
    invoke-interface {v0}, Lcom/facebook/payments/history/model/PaymentTransactions;->c()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2240109
    :goto_0
    return-void

    .line 2240110
    :cond_0
    invoke-interface {v0}, Lcom/facebook/payments/history/model/PaymentTransactions;->b()Ljava/lang/Long;

    move-result-object v1

    invoke-static {p0, v1, p1, v0}, LX/FRB;->a$redex0(LX/FRB;Ljava/lang/Long;LX/6zj;Lcom/facebook/payments/history/model/PaymentTransactions;)V

    goto :goto_0
.end method
