.class public LX/GTU;
.super Landroid/preference/PreferenceCategory;
.source ""


# instance fields
.field public a:LX/GTL;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2355549
    invoke-direct {p0, p1}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 2355550
    invoke-virtual {p0}, Landroid/preference/PreferenceGroup;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/GTU;

    invoke-static {v0}, LX/GTL;->a(LX/0QB;)LX/GTL;

    move-result-object v0

    check-cast v0, LX/GTL;

    iput-object v0, p0, LX/GTU;->a:LX/GTL;

    .line 2355551
    return-void
.end method


# virtual methods
.method public final onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V
    .locals 2

    .prologue
    .line 2355542
    invoke-super {p0, p1}, Landroid/preference/PreferenceCategory;->onAttachedToHierarchy(Landroid/preference/PreferenceManager;)V

    .line 2355543
    const-string v0, "Reauth - internal"

    invoke-virtual {p0, v0}, LX/GTU;->setTitle(Ljava/lang/CharSequence;)V

    .line 2355544
    new-instance v0, Landroid/preference/Preference;

    invoke-virtual {p0}, LX/GTU;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 2355545
    const-string v1, "Get Reauth Token"

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 2355546
    new-instance v1, LX/GTT;

    invoke-direct {v1, p0}, LX/GTT;-><init>(LX/GTU;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 2355547
    invoke-virtual {p0, v0}, LX/GTU;->addPreference(Landroid/preference/Preference;)Z

    .line 2355548
    return-void
.end method
