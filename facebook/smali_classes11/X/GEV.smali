.class public final LX/GEV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/2Tw;


# direct methods
.method public constructor <init>(LX/2Tw;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2331818
    iput-object p1, p0, LX/GEV;->b:LX/2Tw;

    iput-object p2, p0, LX/GEV;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2331819
    iget-object v0, p0, LX/GEV;->b:LX/2Tw;

    iget-object v0, v0, LX/2Tw;->e:LX/2U3;

    sget-object v1, LX/2Tw;->a:Ljava/lang/Class;

    const-string v2, "Failed to retrieve top posts"

    invoke-virtual {v0, v1, v2, p1}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2331820
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2331821
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2331822
    if-eqz p1, :cond_0

    .line 2331823
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331824
    if-eqz v0, :cond_0

    .line 2331825
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331826
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel;->a()Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2331827
    :cond_0
    iget-object v0, p0, LX/GEV;->b:LX/2Tw;

    iget-object v0, v0, LX/2Tw;->e:LX/2U3;

    sget-object v1, LX/2Tw;->a:Ljava/lang/Class;

    const-string v2, "Null result when retrieving top posts"

    invoke-virtual {v0, v1, v2}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2331828
    :cond_1
    return-void

    .line 2331829
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331830
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel;->a()Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2331831
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331832
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel;->a()Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2331833
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331834
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel;->a()Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    .line 2331835
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 2331836
    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;

    .line 2331837
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    .line 2331838
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;->k()Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;

    move-result-object v4

    if-eqz v4, :cond_5

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;->k()Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    if-eqz v4, :cond_5

    const-string v4, "Story"

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;->k()Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "Video"

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;->k()Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel$ShareableModel;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_3
    const/4 v4, 0x1

    :goto_1
    move v4, v4

    .line 2331839
    if-eqz v4, :cond_4

    .line 2331840
    iget-object v4, p0, LX/GEV;->b:LX/2Tw;

    iget-object v5, p0, LX/GEV;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchTopPagePostsQueryModels$FetchTopPagePostsQueryModel$TimelineStoriesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v0

    .line 2331841
    const-string v6, "PrefetchTopPostsPromotionInfo"

    invoke-static {v6}, LX/2U5;->a(Ljava/lang/String;)LX/0v6;

    move-result-object v6

    .line 2331842
    iget-object v7, v4, LX/2Tw;->g:LX/2U4;

    sget-object v8, LX/8wL;->BOOST_POST:LX/8wL;

    invoke-virtual {v7, v5, v0, v8}, LX/2U4;->a(Ljava/lang/String;Ljava/lang/String;LX/8wL;)LX/0zO;

    move-result-object v7

    sget-object v8, LX/0zS;->e:LX/0zS;

    invoke-virtual {v7, v8}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v7

    .line 2331843
    iget-object v8, v4, LX/2Tw;->g:LX/2U4;

    sget-object v9, LX/8wL;->BOOST_POST:LX/8wL;

    const/4 p1, 0x1

    invoke-virtual {v8, v0, v9, p1}, LX/2U4;->a(Ljava/lang/String;LX/8wL;Z)LX/0zO;

    move-result-object v8

    sget-object v9, LX/0zS;->e:LX/0zS;

    invoke-virtual {v8, v9}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v8

    .line 2331844
    invoke-virtual {v6, v7}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2331845
    invoke-virtual {v6, v8}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2331846
    iget-object v7, v4, LX/2Tw;->f:LX/0tX;

    iget-object v8, v4, LX/2Tw;->i:Ljava/util/concurrent/ExecutorService;

    invoke-virtual {v7, v6, v8}, LX/0tX;->a(LX/0v6;Ljava/util/concurrent/ExecutorService;)V

    .line 2331847
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    :cond_5
    const/4 v4, 0x0

    goto :goto_1
.end method
