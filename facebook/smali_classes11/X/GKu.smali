.class public final LX/GKu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/GKv;


# direct methods
.method public constructor <init>(LX/GKv;)V
    .locals 0

    .prologue
    .line 2341531
    iput-object p1, p0, LX/GKu;->a:LX/GKv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    .prologue
    .line 2341533
    iget-object v0, p0, LX/GKu;->a:LX/GKv;

    iget-object v0, v0, LX/GKv;->i:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v1, p0, LX/GKu;->a:LX/GKv;

    iget-object v1, v1, LX/GKv;->a:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2341534
    iget-object v0, p0, LX/GKu;->a:LX/GKv;

    iget-object v0, v0, LX/GKv;->i:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v1, p0, LX/GKu;->a:LX/GKv;

    iget-object v1, v1, LX/GKv;->a:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/widget/text/BetterEditTextView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2341535
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    .line 2341536
    iget-object v0, p0, LX/GKu;->a:LX/GKv;

    iget-object v0, v0, LX/GKv;->j:LX/GCE;

    sget-object v1, LX/GG8;->PHONE_NUMBER:LX/GG8;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/GCE;->a(LX/GG8;Z)V

    .line 2341537
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2341532
    return-void
.end method
