.class public LX/FW5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0gc;

.field public final b:I

.field public c:LX/FW7;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0gc;Ljava/lang/Integer;)V
    .locals 3
    .param p1    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Integer;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2251542
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2251543
    iput-object p1, p0, LX/FW5;->a:LX/0gc;

    .line 2251544
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, LX/FW5;->b:I

    .line 2251545
    iget-object v0, p0, LX/FW5;->a:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v1

    .line 2251546
    invoke-static {}, LX/FW7;->values()[LX/FW7;

    move-result-object v2

    array-length p1, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_1

    aget-object p2, v2, v0

    .line 2251547
    invoke-static {p0, p2}, LX/FW5;->c(LX/FW5;LX/FW7;)Lcom/facebook/base/fragment/FbFragment;

    move-result-object p2

    .line 2251548
    if-eqz p2, :cond_0

    .line 2251549
    invoke-virtual {v1, p2}, LX/0hH;->b(Landroid/support/v4/app/Fragment;)LX/0hH;

    .line 2251550
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2251551
    :cond_1
    invoke-virtual {v1}, LX/0hH;->c()I

    .line 2251552
    iget-object v0, p0, LX/FW5;->a:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 2251553
    return-void
.end method

.method public static c(LX/FW5;LX/FW7;)Lcom/facebook/base/fragment/FbFragment;
    .locals 2

    .prologue
    .line 2251538
    iget-object v0, p0, LX/FW5;->a:LX/0gc;

    invoke-virtual {p1}, LX/FW7;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/support/v4/app/Fragment;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2251539
    iget-object v0, p0, LX/FW5;->c:LX/FW7;

    if-nez v0, :cond_0

    .line 2251540
    const/4 v0, 0x0

    .line 2251541
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/FW5;->a:LX/0gc;

    iget-object v1, p0, LX/FW5;->c:LX/FW7;

    invoke-virtual {v1}, LX/FW7;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    goto :goto_0
.end method
