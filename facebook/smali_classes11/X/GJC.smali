.class public LX/GJC;
.super LX/GIw;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GIw",
        "<",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/GDg;

.field public final b:LX/GDm;

.field public c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public d:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;


# direct methods
.method public constructor <init>(LX/GK4;LX/GDg;LX/GDm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2337895
    invoke-direct {p0, p1}, LX/GIw;-><init>(LX/GK4;)V

    .line 2337896
    iput-object p2, p0, LX/GJC;->a:LX/GDg;

    .line 2337897
    iput-object p3, p0, LX/GJC;->b:LX/GDm;

    .line 2337898
    return-void
.end method

.method public static a(LX/0QB;)LX/GJC;
    .locals 4

    .prologue
    .line 2337892
    new-instance v3, LX/GJC;

    invoke-static {p0}, LX/GK4;->a(LX/0QB;)LX/GK4;

    move-result-object v0

    check-cast v0, LX/GK4;

    invoke-static {p0}, LX/GDg;->a(LX/0QB;)LX/GDg;

    move-result-object v1

    check-cast v1, LX/GDg;

    invoke-static {p0}, LX/GDm;->a(LX/0QB;)LX/GDm;

    move-result-object v2

    check-cast v2, LX/GDm;

    invoke-direct {v3, v0, v1, v2}, LX/GJC;-><init>(LX/GK4;LX/GDg;LX/GDm;)V

    .line 2337893
    move-object v0, v3

    .line 2337894
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2337890
    const/4 v0, 0x0

    iput-object v0, p0, LX/GJC;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    .line 2337891
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2337849
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {p0, p1, p2}, LX/GIw;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2337887
    invoke-super {p0, p1}, LX/GIw;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2337888
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iput-object p1, p0, LX/GJC;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2337889
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2337884
    invoke-super {p0, p1, p2}, LX/GIw;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2337885
    iput-object p1, p0, LX/GJC;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    .line 2337886
    return-void
.end method

.method public final b()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2337883
    new-instance v0, LX/GJ9;

    invoke-direct {v0, p0}, LX/GJ9;-><init>(LX/GJC;)V

    return-object v0
.end method

.method public final d()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2337882
    new-instance v0, LX/GJA;

    invoke-direct {v0, p0}, LX/GJA;-><init>(LX/GJC;)V

    return-object v0
.end method

.method public final e()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 2337850
    sget-object v0, LX/GJB;->a:[I

    iget-object v1, p0, LX/GJC;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v1

    invoke-virtual {v1}, LX/GGB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2337851
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337852
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setCreateAdButtonVisibility(I)V

    .line 2337853
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337854
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAddBudgetButtonVisibility(I)V

    .line 2337855
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337856
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setResumeAdButtonVisibility(I)V

    .line 2337857
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337858
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setPauseAdButtonVisibility(I)V

    .line 2337859
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337860
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setDeleteAdButtonVisibility(I)V

    .line 2337861
    :goto_0
    return-void

    .line 2337862
    :pswitch_0
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337863
    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setCreateAdButtonVisibility(I)V

    .line 2337864
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337865
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAddBudgetButtonVisibility(I)V

    .line 2337866
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337867
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setResumeAdButtonVisibility(I)V

    .line 2337868
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337869
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setPauseAdButtonVisibility(I)V

    .line 2337870
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337871
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setDeleteAdButtonVisibility(I)V

    goto :goto_0

    .line 2337872
    :pswitch_1
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337873
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setCreateAdButtonVisibility(I)V

    .line 2337874
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337875
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAddBudgetButtonVisibility(I)V

    .line 2337876
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337877
    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setResumeAdButtonVisibility(I)V

    .line 2337878
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337879
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setPauseAdButtonVisibility(I)V

    .line 2337880
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337881
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setDeleteAdButtonVisibility(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
