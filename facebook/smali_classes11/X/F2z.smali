.class public final LX/F2z;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/F30;


# direct methods
.method public constructor <init>(LX/F30;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2193522
    iput-object p1, p0, LX/F2z;->c:LX/F30;

    iput-object p2, p0, LX/F2z;->a:Ljava/lang/String;

    iput-object p3, p0, LX/F2z;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x3748927c

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2193523
    new-instance v1, LX/F3U;

    iget-object v2, p0, LX/F2z;->a:Ljava/lang/String;

    iget-object v3, p0, LX/F2z;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, LX/F3U;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2193524
    iget-object v2, p0, LX/F2z;->c:LX/F30;

    iget-object v2, v2, LX/F30;->b:LX/F3H;

    .line 2193525
    iget-object v3, v2, LX/F3H;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    .line 2193526
    iget-object v5, v3, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->l:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v5}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokens()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    const/4 v6, 0x5

    if-ne v5, v6, :cond_2

    .line 2193527
    iget-object v5, v3, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->a:Landroid/content/res/Resources;

    const v6, 0x7f083199

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v3, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->a:Landroid/content/res/Resources;

    const v7, 0x7f08319a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v5, v6}, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->a(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 2193528
    const/4 v5, 0x1

    .line 2193529
    :goto_0
    move v3, v5

    .line 2193530
    if-nez v3, :cond_0

    iget-object v3, v2, LX/F3H;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    const/4 v6, 0x0

    .line 2193531
    iget-object v5, v3, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->l:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v5}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->getPickedTokens()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result p0

    move v7, v6

    :goto_1
    if-ge v7, p0, :cond_4

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/8QK;

    .line 2193532
    check-cast v5, LX/F3U;

    .line 2193533
    iget-object p1, v5, LX/F3U;->f:Ljava/lang/String;

    move-object v5, p1

    .line 2193534
    iget-object p1, v1, LX/F3U;->f:Ljava/lang/String;

    move-object p1, p1

    .line 2193535
    invoke-virtual {v5, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2193536
    iget-object v5, v3, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->a:Landroid/content/res/Resources;

    const v6, 0x7f08319b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v3, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->a:Landroid/content/res/Resources;

    const v7, 0x7f08319c

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v5, v6}, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->a(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 2193537
    const/4 v5, 0x1

    .line 2193538
    :goto_2
    move v3, v5

    .line 2193539
    if-eqz v3, :cond_1

    .line 2193540
    :cond_0
    :goto_3
    const v1, 0x63e6d2b1

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2193541
    :cond_1
    iget-object v3, v2, LX/F3H;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    iget-object v3, v3, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->m:Landroid/widget/ListView;

    const/16 v5, 0x8

    invoke-virtual {v3, v5}, Landroid/widget/ListView;->setVisibility(I)V

    .line 2193542
    iget-object v3, v2, LX/F3H;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    iget-object v3, v3, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->l:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v3}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a()V

    .line 2193543
    iget-object v3, v2, LX/F3H;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    iget-object v3, v3, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->l:Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;

    invoke-virtual {v3, v1}, Lcom/facebook/widget/tokenizedtypeahead/TokenizedAutoCompleteTextView;->a(LX/8QK;)V

    .line 2193544
    iget-object v3, v2, LX/F3H;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    const/4 v5, 0x1

    .line 2193545
    iput-boolean v5, v3, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->r:Z

    .line 2193546
    goto :goto_3

    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    .line 2193547
    :cond_3
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto :goto_1

    :cond_4
    move v5, v6

    .line 2193548
    goto :goto_2
.end method
