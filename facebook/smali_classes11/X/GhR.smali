.class public final enum LX/GhR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GhR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GhR;

.field public static final enum LEAGUE:LX/GhR;

.field public static final enum MATCH:LX/GhR;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2384172
    new-instance v0, LX/GhR;

    const-string v1, "MATCH"

    invoke-direct {v0, v1, v2}, LX/GhR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GhR;->MATCH:LX/GhR;

    .line 2384173
    new-instance v0, LX/GhR;

    const-string v1, "LEAGUE"

    invoke-direct {v0, v1, v3}, LX/GhR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GhR;->LEAGUE:LX/GhR;

    .line 2384174
    const/4 v0, 0x2

    new-array v0, v0, [LX/GhR;

    sget-object v1, LX/GhR;->MATCH:LX/GhR;

    aput-object v1, v0, v2

    sget-object v1, LX/GhR;->LEAGUE:LX/GhR;

    aput-object v1, v0, v3

    sput-object v0, LX/GhR;->$VALUES:[LX/GhR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2384175
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GhR;
    .locals 1

    .prologue
    .line 2384171
    const-class v0, LX/GhR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GhR;

    return-object v0
.end method

.method public static values()[LX/GhR;
    .locals 1

    .prologue
    .line 2384170
    sget-object v0, LX/GhR;->$VALUES:[LX/GhR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GhR;

    return-object v0
.end method
