.class public LX/GeH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1Uf;


# direct methods
.method public constructor <init>(LX/1Uf;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2375452
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2375453
    iput-object p1, p0, LX/GeH;->a:LX/1Uf;

    .line 2375454
    return-void
.end method

.method public static b(LX/0QB;)LX/GeH;
    .locals 2

    .prologue
    .line 2375455
    new-instance v1, LX/GeH;

    invoke-static {p0}, LX/1Uf;->a(LX/0QB;)LX/1Uf;

    move-result-object v0

    check-cast v0, LX/1Uf;

    invoke-direct {v1, v0}, LX/GeH;-><init>(LX/1Uf;)V

    .line 2375456
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;LX/25E;)Landroid/text/Spannable;
    .locals 5

    .prologue
    .line 2375457
    invoke-interface {p2}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-static {v0}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object v0

    .line 2375458
    invoke-static {p2, p1}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v1

    .line 2375459
    if-nez v0, :cond_0

    .line 2375460
    const/4 v0, 0x0

    .line 2375461
    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, LX/GeH;->a:LX/1Uf;

    sget v3, LX/1Uf;->a:I

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v1, v3, v4}, LX/1Uf;->a(LX/1y5;LX/0lF;IZ)Landroid/text/SpannableStringBuilder;

    move-result-object v0

    goto :goto_0
.end method
