.class public LX/FS6;
.super LX/0Tv;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile b:LX/FS6;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2241237
    const-class v0, LX/FS6;

    sput-object v0, LX/FS6;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2241250
    const-string v0, "localphototags"

    const/4 v1, 0x4

    new-instance v2, LX/FS4;

    invoke-direct {v2}, LX/FS4;-><init>()V

    new-instance v3, LX/FS5;

    invoke-direct {v3}, LX/FS5;-><init>()V

    new-instance v4, LX/FS3;

    invoke-direct {v4}, LX/FS3;-><init>()V

    invoke-static {v2, v3, v4}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, LX/0Tv;-><init>(Ljava/lang/String;ILX/0Px;)V

    .line 2241251
    return-void
.end method

.method public static a(LX/0QB;)LX/FS6;
    .locals 3

    .prologue
    .line 2241238
    sget-object v0, LX/FS6;->b:LX/FS6;

    if-nez v0, :cond_1

    .line 2241239
    const-class v1, LX/FS6;

    monitor-enter v1

    .line 2241240
    :try_start_0
    sget-object v0, LX/FS6;->b:LX/FS6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2241241
    if-eqz v2, :cond_0

    .line 2241242
    :try_start_1
    new-instance v0, LX/FS6;

    invoke-direct {v0}, LX/FS6;-><init>()V

    .line 2241243
    move-object v0, v0

    .line 2241244
    sput-object v0, LX/FS6;->b:LX/FS6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2241245
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2241246
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2241247
    :cond_1
    sget-object v0, LX/FS6;->b:LX/FS6;

    return-object v0

    .line 2241248
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2241249
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
