.class public final enum LX/FDr;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FDr;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FDr;

.field public static final enum THREAD_MARKED_READ:LX/FDr;

.field public static final enum THREAD_NOT_FOUND:LX/FDr;

.field public static final enum THREAD_NOT_MARKED_READ:LX/FDr;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2213602
    new-instance v0, LX/FDr;

    const-string v1, "THREAD_MARKED_READ"

    invoke-direct {v0, v1, v2}, LX/FDr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FDr;->THREAD_MARKED_READ:LX/FDr;

    .line 2213603
    new-instance v0, LX/FDr;

    const-string v1, "THREAD_NOT_MARKED_READ"

    invoke-direct {v0, v1, v3}, LX/FDr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FDr;->THREAD_NOT_MARKED_READ:LX/FDr;

    .line 2213604
    new-instance v0, LX/FDr;

    const-string v1, "THREAD_NOT_FOUND"

    invoke-direct {v0, v1, v4}, LX/FDr;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FDr;->THREAD_NOT_FOUND:LX/FDr;

    .line 2213605
    const/4 v0, 0x3

    new-array v0, v0, [LX/FDr;

    sget-object v1, LX/FDr;->THREAD_MARKED_READ:LX/FDr;

    aput-object v1, v0, v2

    sget-object v1, LX/FDr;->THREAD_NOT_MARKED_READ:LX/FDr;

    aput-object v1, v0, v3

    sget-object v1, LX/FDr;->THREAD_NOT_FOUND:LX/FDr;

    aput-object v1, v0, v4

    sput-object v0, LX/FDr;->$VALUES:[LX/FDr;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2213599
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FDr;
    .locals 1

    .prologue
    .line 2213601
    const-class v0, LX/FDr;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FDr;

    return-object v0
.end method

.method public static values()[LX/FDr;
    .locals 1

    .prologue
    .line 2213600
    sget-object v0, LX/FDr;->$VALUES:[LX/FDr;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FDr;

    return-object v0
.end method
