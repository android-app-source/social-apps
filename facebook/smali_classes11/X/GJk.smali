.class public LX/GJk;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field private c:Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;

.field private d:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2339513
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2339514
    const-string v0, "RSVP"

    const-string v1, "TICKET"

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/GJk;->a:LX/0Px;

    .line 2339515
    iput-object p1, p0, LX/GJk;->d:Landroid/content/res/Resources;

    .line 2339516
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/EventBoostType;
        .end annotation
    .end param

    .prologue
    .line 2339508
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2339509
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown boost types"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2339510
    :sswitch_0
    const-string v1, "RSVP"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string v1, "TICKET"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 2339511
    :pswitch_0
    iget-object v0, p0, LX/GJk;->d:Landroid/content/res/Resources;

    const v1, 0x7f080b39

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2339512
    :goto_1
    return-object v0

    :pswitch_1
    iget-object v0, p0, LX/GJk;->d:Landroid/content/res/Resources;

    const v1, 0x7f080b3a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6c84a0f4 -> :sswitch_1
        0x2688bb -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/EventBoostType;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2339500
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2339501
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown boost types"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2339502
    :sswitch_0
    const-string v3, "RSVP"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v3, "TICKET"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v0, v2

    goto :goto_0

    .line 2339503
    :pswitch_0
    iget-object v0, p0, LX/GJk;->d:Landroid/content/res/Resources;

    const v1, 0x7f080b3b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2339504
    :goto_1
    return-object v0

    :pswitch_1
    iget-object v0, p0, LX/GJk;->d:Landroid/content/res/Resources;

    const v3, 0x7f080b3c

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v4, p0, LX/GJk;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2339505
    iget-object p0, v4, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->d:Lcom/facebook/adinterfaces/model/EventSpecModel;

    move-object v4, p0

    .line 2339506
    iget-object p0, v4, Lcom/facebook/adinterfaces/model/EventSpecModel;->d:Ljava/lang/String;

    move-object v4, p0

    .line 2339507
    aput-object v4, v2, v1

    invoke-virtual {v0, v3, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6c84a0f4 -> :sswitch_1
        0x2688bb -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2339495
    invoke-super {p0, p1}, LX/GHg;->a(Landroid/os/Bundle;)V

    .line 2339496
    const-string v0, "BOOST_EVENT_TYPE"

    iget-object v1, p0, LX/GJk;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;

    .line 2339497
    iget p0, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->c:I

    move v1, p0

    .line 2339498
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2339499
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 4

    .prologue
    .line 2339478
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;

    .line 2339479
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2339480
    iput-object p1, p0, LX/GJk;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;

    .line 2339481
    iget-object v0, p0, LX/GJk;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;

    iget-object v1, p0, LX/GJk;->a:LX/0Px;

    iget-object v2, p0, LX/GJk;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2339482
    iget-object v3, v2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->d:Lcom/facebook/adinterfaces/model/EventSpecModel;

    move-object v2, v3

    .line 2339483
    iget-object v3, v2, Lcom/facebook/adinterfaces/model/EventSpecModel;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2339484
    invoke-virtual {v1, v2}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->setSelected(I)V

    .line 2339485
    const v0, 0x7f080b3e

    invoke-virtual {p2, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setHeaderTitleResource(I)V

    .line 2339486
    iget-object v0, p0, LX/GJk;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;

    new-instance v1, LX/GJj;

    invoke-direct {v1, p0}, LX/GJj;-><init>(LX/GJk;)V

    .line 2339487
    iput-object v1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->b:LX/GJ4;

    .line 2339488
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/GJk;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 2339489
    iget-object v0, p0, LX/GJk;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2339490
    iget-object v2, p0, LX/GJk;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;

    invoke-virtual {v2, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->d(I)Lcom/facebook/fbui/widget/contentview/CheckedContentView;

    move-result-object v2

    .line 2339491
    invoke-direct {p0, v0}, LX/GJk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2339492
    invoke-direct {p0, v0}, LX/GJk;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2339493
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2339494
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2339475
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2339476
    iput-object p1, p0, LX/GJk;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2339477
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2339470
    invoke-super {p0, p1}, LX/GHg;->b(Landroid/os/Bundle;)V

    .line 2339471
    if-eqz p1, :cond_0

    .line 2339472
    iget-object v0, p0, LX/GJk;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;

    const-string v1, "BOOST_EVENT_TYPE"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 2339473
    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBoostTypeRadioGroupView;->setSelected(I)V

    .line 2339474
    :cond_0
    return-void
.end method
