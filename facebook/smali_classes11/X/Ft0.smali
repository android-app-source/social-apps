.class public LX/Ft0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1LV;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0hB;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BQ9;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/G0n;

.field private final f:LX/BQB;

.field public final g:LX/0QR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QR",
            "<",
            "LX/195;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0QR;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QR",
            "<",
            "LX/23N;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/Fv9;

.field private final j:J

.field private final k:LX/BQG;

.field private final l:LX/BPC;

.field private m:I

.field public n:Z

.field private o:Z

.field public p:LX/9lQ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2297842
    const-class v0, LX/Ft0;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Ft0;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(JLX/0g8;LX/G0n;LX/BQG;LX/BQB;LX/0QR;LX/Fv9;Ljava/lang/String;LX/BPC;LX/0Or;LX/0Or;LX/193;LX/0Or;)V
    .locals 9
    .param p1    # J
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0g8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/G0n;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/BQG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/BQB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/0QR;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/Fv9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "LX/0g8;",
            "LX/G0n;",
            "LX/BQG;",
            "LX/BQB;",
            "LX/0QR",
            "<",
            "LX/Fw0;",
            ">;",
            "LX/Fv9;",
            "Ljava/lang/String;",
            "LX/BPC;",
            "LX/0Or",
            "<",
            "LX/1LV;",
            ">;",
            "LX/0Or",
            "<",
            "LX/FyO;",
            ">;",
            "LX/193;",
            "LX/0Or",
            "<",
            "LX/23N;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2297843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2297844
    const/4 v2, 0x0

    iput v2, p0, LX/Ft0;->m:I

    .line 2297845
    const/4 v2, 0x0

    iput-boolean v2, p0, LX/Ft0;->n:Z

    .line 2297846
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/Ft0;->o:Z

    .line 2297847
    sget-object v2, LX/9lQ;->UNKNOWN_RELATIONSHIP:LX/9lQ;

    iput-object v2, p0, LX/Ft0;->p:LX/9lQ;

    .line 2297848
    iput-wide p1, p0, LX/Ft0;->j:J

    .line 2297849
    iput-object p4, p0, LX/Ft0;->e:LX/G0n;

    .line 2297850
    iput-object p5, p0, LX/Ft0;->k:LX/BQG;

    .line 2297851
    iput-object p6, p0, LX/Ft0;->f:LX/BQB;

    .line 2297852
    move-object/from16 v0, p8

    iput-object v0, p0, LX/Ft0;->i:LX/Fv9;

    .line 2297853
    move-object/from16 v0, p10

    iput-object v0, p0, LX/Ft0;->l:LX/BPC;

    .line 2297854
    new-instance v2, LX/Fsx;

    move-object v3, p0

    move-object/from16 v4, p14

    move-object/from16 v5, p11

    move-object/from16 v6, p7

    move-object/from16 v7, p12

    invoke-direct/range {v2 .. v7}, LX/Fsx;-><init>(LX/Ft0;LX/0Or;LX/0Or;LX/0QR;LX/0Or;)V

    invoke-static {v2}, LX/0Wf;->memoize(LX/0QR;)LX/0QR;

    move-result-object v2

    iput-object v2, p0, LX/Ft0;->h:LX/0QR;

    .line 2297855
    new-instance v2, LX/Fsy;

    move-object/from16 v0, p13

    move-object/from16 v1, p9

    invoke-direct {v2, p0, v0, v1}, LX/Fsy;-><init>(LX/Ft0;LX/193;Ljava/lang/String;)V

    invoke-static {v2}, LX/0Wf;->memoize(LX/0QR;)LX/0QR;

    move-result-object v2

    iput-object v2, p0, LX/Ft0;->g:LX/0QR;

    .line 2297856
    iget-object v2, p0, LX/Ft0;->e:LX/G0n;

    new-instance v3, LX/Fsz;

    invoke-direct {v3, p0, p3}, LX/Fsz;-><init>(LX/Ft0;LX/0g8;)V

    invoke-virtual {v2, v3}, LX/G0n;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2297857
    iget-object v2, p0, LX/Ft0;->k:LX/BQG;

    invoke-virtual {v2}, LX/BQG;->a()Z

    move-result v2

    iput-boolean v2, p0, LX/Ft0;->n:Z

    .line 2297858
    return-void
.end method

.method public static a(LX/Ft0;LX/0Or;LX/0Or;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Ft0;",
            "LX/0Or",
            "<",
            "LX/1LV;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0hB;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BQ9;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2297859
    iput-object p1, p0, LX/Ft0;->a:LX/0Or;

    iput-object p2, p0, LX/Ft0;->b:LX/0Or;

    iput-object p3, p0, LX/Ft0;->c:LX/0Or;

    return-void
.end method

.method public static a(LX/Ft0;LX/1LV;)V
    .locals 6

    .prologue
    .line 2297860
    iget-object v0, p0, LX/Ft0;->l:LX/BPC;

    .line 2297861
    iget-boolean v1, v0, LX/BPC;->c:Z

    move v0, v1

    .line 2297862
    if-eqz v0, :cond_0

    const-string v5, "1"

    .line 2297863
    :goto_0
    const-string v0, "vpv_profile_id"

    iget-wide v2, p0, LX/Ft0;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    const-string v2, "profile_relationship_type"

    iget-object v3, p0, LX/Ft0;->p:LX/9lQ;

    invoke-virtual {v3}, LX/9lQ;->getValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, "is_profile_cached"

    invoke-static/range {v0 .. v5}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    .line 2297864
    invoke-virtual {p1, v0}, LX/1LV;->a(LX/0P1;)V

    .line 2297865
    return-void

    .line 2297866
    :cond_0
    const-string v5, "0"

    goto :goto_0
.end method

.method public static a$redex0(LX/Ft0;Landroid/view/View;LX/0g8;)V
    .locals 3

    .prologue
    .line 2297867
    if-nez p1, :cond_0

    .line 2297868
    :goto_0
    return-void

    .line 2297869
    :cond_0
    iget-object v0, p0, LX/Ft0;->i:LX/Fv9;

    invoke-interface {p2}, LX/0g8;->d()I

    move-result v1

    invoke-virtual {v0, p1, v1}, LX/Fv9;->a(Landroid/view/View;I)Z

    move-result v0

    .line 2297870
    iget-object v1, p0, LX/Ft0;->i:LX/Fv9;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, LX/Fv9;->a(IZ)V

    goto :goto_0
.end method

.method public static b$redex0(LX/Ft0;Landroid/view/View;LX/0g8;)V
    .locals 3

    .prologue
    .line 2297871
    if-nez p1, :cond_0

    .line 2297872
    :goto_0
    return-void

    .line 2297873
    :cond_0
    iget-object v0, p0, LX/Ft0;->i:LX/Fv9;

    invoke-interface {p2}, LX/0g8;->d()I

    move-result v1

    invoke-virtual {v0, p1, v1}, LX/Fv9;->a(Landroid/view/View;I)Z

    move-result v0

    .line 2297874
    iget-object v1, p0, LX/Ft0;->i:LX/Fv9;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, LX/Fv9;->a(IZ)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 5

    .prologue
    .line 2297875
    if-nez p2, :cond_0

    .line 2297876
    iget-object v0, p0, LX/Ft0;->h:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23N;

    invoke-virtual {v0, p1}, LX/1Kt;->b(LX/0g8;)V

    .line 2297877
    :cond_0
    iget v0, p0, LX/Ft0;->m:I

    if-nez v0, :cond_3

    .line 2297878
    iget-boolean v0, p0, LX/Ft0;->n:Z

    if-eqz v0, :cond_1

    .line 2297879
    iget-object v0, p0, LX/Ft0;->g:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/195;

    invoke-virtual {v0}, LX/195;->a()V

    .line 2297880
    :cond_1
    :goto_0
    iget-boolean v0, p0, LX/Ft0;->o:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    if-ne p2, v0, :cond_2

    .line 2297881
    iget-object v0, p0, LX/Ft0;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQ9;

    .line 2297882
    iget-wide v2, p0, LX/Ft0;->j:J

    const-string v1, "0"

    iget-object v4, p0, LX/Ft0;->p:LX/9lQ;

    invoke-virtual {v0, v2, v3, v1, v4}, LX/BQ9;->c(JLjava/lang/String;LX/9lQ;)V

    .line 2297883
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Ft0;->o:Z

    .line 2297884
    :cond_2
    iput p2, p0, LX/Ft0;->m:I

    .line 2297885
    return-void

    .line 2297886
    :cond_3
    if-nez p2, :cond_1

    .line 2297887
    iget-boolean v0, p0, LX/Ft0;->n:Z

    if-eqz v0, :cond_1

    .line 2297888
    iget-object v0, p0, LX/Ft0;->g:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/195;

    invoke-virtual {v0}, LX/195;->b()V

    goto :goto_0
.end method

.method public final a(LX/0g8;III)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2297889
    iget-object v0, p0, LX/Ft0;->h:LX/0QR;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/23N;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/1Kt;->a(LX/0g8;III)V

    .line 2297890
    if-lez p3, :cond_2

    const/4 v0, -0x1

    if-eq p2, v0, :cond_2

    .line 2297891
    iget-object v0, p0, LX/Ft0;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hB;

    .line 2297892
    invoke-interface {p1, v2}, LX/0g8;->e(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    neg-int v3, v3

    int-to-double v4, v3

    invoke-virtual {v0}, LX/0hB;->d()I

    move-result v0

    int-to-double v6, v0

    div-double/2addr v4, v6

    .line 2297893
    const-wide v6, 0x3fc999999999999aL    # 0.2

    cmpl-double v0, v4, v6

    if-lez v0, :cond_8

    move v3, v1

    .line 2297894
    :goto_0
    const-wide/16 v6, 0x0

    cmpl-double v0, v4, v6

    if-lez v0, :cond_9

    add-int v0, p2, p3

    if-ne v0, p4, :cond_9

    move v0, v1

    .line 2297895
    :goto_1
    if-nez v3, :cond_0

    if-eqz v0, :cond_2

    .line 2297896
    :cond_0
    iget-object v0, p0, LX/Ft0;->f:LX/BQB;

    .line 2297897
    iget-boolean v1, v0, LX/BQB;->D:Z

    if-nez v1, :cond_1

    .line 2297898
    iget-object v1, v0, LX/BQB;->c:LX/BQD;

    const-string v2, "TimelineUserScrolled"

    invoke-virtual {v1, v2}, LX/BQD;->f(Ljava/lang/String;)V

    .line 2297899
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/BQB;->D:Z

    .line 2297900
    :cond_1
    const-string v1, "user_scrolled"

    invoke-static {v0, v1}, LX/BQB;->f(LX/BQB;Ljava/lang/String;)V

    .line 2297901
    const-string v1, "user_scrolled"

    invoke-static {v0, v1}, LX/BQB;->g(LX/BQB;Ljava/lang/String;)V

    .line 2297902
    :cond_2
    add-int v0, p2, p3

    invoke-interface {p1}, LX/0g8;->s()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2297903
    :goto_2
    if-ge p2, v1, :cond_7

    .line 2297904
    invoke-interface {p1, p2}, LX/0g8;->f(I)Ljava/lang/Object;

    move-result-object v2

    .line 2297905
    invoke-interface {p1, p2}, LX/0g8;->c(I)Landroid/view/View;

    move-result-object v0

    .line 2297906
    sget-object v3, LX/FvU;->COVER_PHOTO:LX/FvU;

    if-ne v2, v3, :cond_4

    instance-of v3, v0, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    if-eqz v3, :cond_4

    .line 2297907
    iget-object v2, p0, LX/Ft0;->i:LX/Fv9;

    invoke-interface {p1}, LX/0g8;->d()I

    move-result v3

    invoke-virtual {v2, v0, v3}, LX/Fv9;->a(Landroid/view/View;I)Z

    move-result v0

    .line 2297908
    iget-object v2, p0, LX/Ft0;->i:LX/Fv9;

    invoke-virtual {v2, v0}, LX/Fv9;->a(Z)V

    .line 2297909
    :cond_3
    :goto_3
    add-int/lit8 p2, p2, 0x1

    goto :goto_2

    .line 2297910
    :cond_4
    sget-object v3, LX/FvC;->FULL_HEADER:LX/FvC;

    if-ne v2, v3, :cond_5

    instance-of v3, v0, LX/Fw6;

    if-eqz v3, :cond_5

    .line 2297911
    check-cast v0, LX/Fw6;

    .line 2297912
    new-instance v2, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$4;

    invoke-direct {v2, p0, v0}, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$4;-><init>(LX/Ft0;LX/Fw6;)V

    invoke-virtual {v0, v2}, LX/Fw6;->post(Ljava/lang/Runnable;)Z

    goto :goto_3

    .line 2297913
    :cond_5
    sget-object v3, LX/Fvh;->INTRO_CARD:LX/Fvh;

    if-ne v2, v3, :cond_6

    instance-of v3, v0, Lcom/facebook/timeline/header/TimelineIntroCardView;

    if-eqz v3, :cond_6

    .line 2297914
    check-cast v0, Lcom/facebook/timeline/header/TimelineIntroCardView;

    .line 2297915
    new-instance v2, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$5;

    invoke-direct {v2, p0, v0, p1}, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$5;-><init>(LX/Ft0;Lcom/facebook/timeline/header/TimelineIntroCardView;LX/0g8;)V

    invoke-virtual {v0, v2}, Lcom/facebook/timeline/header/TimelineIntroCardView;->post(Ljava/lang/Runnable;)Z

    goto :goto_3

    .line 2297916
    :cond_6
    instance-of v3, v0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;

    if-eqz v3, :cond_3

    .line 2297917
    check-cast v0, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;

    .line 2297918
    new-instance v3, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;

    invoke-direct {v3, p0, v2, v0, p1}, Lcom/facebook/timeline/delegate/TimelineFragmentScrollDelegate$6;-><init>(LX/Ft0;Ljava/lang/Object;Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;LX/0g8;)V

    invoke-virtual {v0, v3}, Lcom/facebook/timeline/header/TimelineIntroCardFavPhotosView;->post(Ljava/lang/Runnable;)Z

    goto :goto_3

    .line 2297919
    :cond_7
    return-void

    :cond_8
    move v3, v2

    .line 2297920
    goto/16 :goto_0

    :cond_9
    move v0, v2

    .line 2297921
    goto/16 :goto_1
.end method
