.class public LX/GlL;
.super LX/Gkp;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2390727
    invoke-direct {p0, p1}, LX/Gkp;-><init>(Landroid/content/res/Resources;)V

    .line 2390728
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/Gkq;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "LX/Gkq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2390729
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkq;

    .line 2390730
    iget-boolean v3, v0, LX/Gkq;->g:Z

    move v3, v3

    .line 2390731
    if-eqz v3, :cond_0

    .line 2390732
    iget-boolean v3, v0, LX/Gkq;->j:Z

    move v3, v3

    .line 2390733
    if-nez v3, :cond_1

    .line 2390734
    :cond_0
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 2390735
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2390736
    :cond_2
    return-object p1
.end method

.method public final b(I)LX/GlC;
    .locals 3

    .prologue
    .line 2390737
    invoke-super {p0, p1}, LX/Gkp;->b(I)LX/GlC;

    move-result-object v0

    .line 2390738
    const-string v1, "has_favorited"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2390739
    const-string v1, "order"

    const-string v2, "app_landing"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2390740
    return-object v0
.end method

.method public final synthetic c()Ljava/lang/Enum;
    .locals 1

    .prologue
    .line 2390741
    invoke-virtual {p0}, LX/Gkp;->i()LX/Gkn;

    move-result-object v0

    return-object v0
.end method

.method public final i()LX/Gkn;
    .locals 1

    .prologue
    .line 2390742
    sget-object v0, LX/Gkn;->FAVORITES_SECTION:LX/Gkn;

    return-object v0
.end method
