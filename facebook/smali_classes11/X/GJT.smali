.class public final LX/GJT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;Ljava/util/List;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2338857
    iput-object p1, p0, LX/GJT;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iput-object p2, p0, LX/GJT;->a:Ljava/util/List;

    iput-object p3, p0, LX/GJT;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    const v0, 0x30816c31

    invoke-static {v5, v6, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2338858
    iget-object v1, p0, LX/GJT;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v1, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->q:Landroid/animation/ValueAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/GJT;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v1, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->q:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->isRunning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2338859
    iget-object v1, p0, LX/GJT;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v1, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->q:Landroid/animation/ValueAnimator;

    invoke-virtual {v1}, Landroid/animation/ValueAnimator;->end()V

    .line 2338860
    :cond_0
    iget-object v1, p0, LX/GJT;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v1, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->s:LX/0ad;

    sget-short v2, LX/GDK;->E:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    .line 2338861
    iget-object v1, p0, LX/GJT;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v1, v1, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->r:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    .line 2338862
    new-instance v2, LX/0Tn;

    sget-object v3, LX/0Tm;->g:LX/0Tn;

    const-string v4, "adinterfaces_walkthrough_nux"

    invoke-direct {v2, v3, v4}, LX/0Tn;-><init>(LX/0To;Ljava/lang/String;)V

    invoke-interface {v1, v2, v6}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    .line 2338863
    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2338864
    iget-object v1, p0, LX/GJT;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    new-instance v2, LX/GJS;

    invoke-direct {v2, p0}, LX/GJS;-><init>(LX/GJT;)V

    invoke-static {v1, v2}, LX/GMo;->a(Landroid/view/View;LX/GJR;)V

    .line 2338865
    const v1, 0x389cf367

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
