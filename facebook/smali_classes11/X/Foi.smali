.class public final LX/Foi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V
    .locals 0

    .prologue
    .line 2290073
    iput-object p1, p0, LX/Foi;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 3

    .prologue
    .line 2290074
    add-int v0, p2, p3

    add-int/lit8 v1, p4, -0x3

    if-le v0, v1, :cond_0

    iget-object v0, p0, LX/Foi;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->a:LX/Foq;

    .line 2290075
    iget-boolean v1, v0, LX/Foq;->h:Z

    move v0, v1

    .line 2290076
    if-eqz v0, :cond_0

    .line 2290077
    iget-object v0, p0, LX/Foi;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->q:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    if-eqz v0, :cond_1

    .line 2290078
    iget-object v0, p0, LX/Foi;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->b:LX/Foy;

    iget-object v1, p0, LX/Foi;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->q:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    iget-object v2, p0, LX/Foi;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v2, v2, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->s:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/Foy;->a(Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;Ljava/lang/String;)V

    .line 2290079
    :cond_0
    :goto_0
    return-void

    .line 2290080
    :cond_1
    iget-object v0, p0, LX/Foi;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v0}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2290081
    iget-object v0, p0, LX/Foi;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->b:LX/Foy;

    iget-object v1, p0, LX/Foi;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v1}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Foi;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v2, v2, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->s:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/Foy;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2290082
    :cond_2
    iget-object v0, p0, LX/Foi;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->b:LX/Foy;

    iget-object v1, p0, LX/Foi;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Foy;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 2290083
    return-void
.end method
