.class public final LX/Gwr;
.super Landroid/webkit/WebViewClient;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Ljava/lang/String;

.field public final synthetic c:LX/Gws;

.field private d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private e:LX/03R;


# direct methods
.method public constructor <init>(LX/Gws;Landroid/content/Context;Ljava/lang/String;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/03R;)V
    .locals 1
    .param p5    # LX/03R;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param

    .prologue
    .line 2407463
    iput-object p1, p0, LX/Gwr;->c:LX/Gws;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    .line 2407464
    iput-object p2, p0, LX/Gwr;->a:Landroid/content/Context;

    .line 2407465
    iput-object p4, p0, LX/Gwr;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2407466
    iput-object p5, p0, LX/Gwr;->e:LX/03R;

    .line 2407467
    invoke-static {p3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2407468
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Gwr;->b:Ljava/lang/String;

    .line 2407469
    return-void
.end method


# virtual methods
.method public final onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2407470
    iget-object v0, p0, LX/Gwr;->c:LX/Gws;

    iget-object v0, v0, LX/Gws;->g:LX/44G;

    invoke-virtual {v0, p2}, LX/44G;->a(Ljava/lang/String;)V

    .line 2407471
    return-void
.end method

.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2407472
    if-eqz p2, :cond_0

    .line 2407473
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2407474
    invoke-static {v2}, LX/2yp;->b(Landroid/net/Uri;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/Gwr;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2407475
    iget-object v1, p0, LX/Gwr;->c:LX/Gws;

    iput-boolean v0, v1, LX/Gws;->a:Z

    move v1, v0

    .line 2407476
    :cond_0
    const-class v2, LX/Gws;

    monitor-enter v2

    .line 2407477
    :try_start_0
    iget-object v0, p0, LX/Gwr;->c:LX/Gws;

    const/4 v3, 0x0

    iput-boolean v3, v0, LX/Gws;->b:Z

    .line 2407478
    iget-object v0, p0, LX/Gwr;->c:LX/Gws;

    iget-object v0, v0, LX/Gws;->c:Ljava/util/Set;

    .line 2407479
    iget-object v3, p0, LX/Gwr;->c:LX/Gws;

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iput-object v4, v3, LX/Gws;->c:Ljava/util/Set;

    .line 2407480
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2407481
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BWV;

    .line 2407482
    if-eqz v1, :cond_1

    .line 2407483
    invoke-interface {v0}, LX/BWV;->a()V

    goto :goto_0

    .line 2407484
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2407485
    :cond_1
    invoke-interface {v0}, LX/BWV;->b()V

    goto :goto_0

    .line 2407486
    :cond_2
    iget-object v0, p0, LX/Gwr;->c:LX/Gws;

    iget-object v0, v0, LX/Gws;->f:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->destroy()V

    .line 2407487
    iget-object v0, p0, LX/Gwr;->c:LX/Gws;

    const/4 v1, 0x0

    .line 2407488
    iput-object v1, v0, LX/Gws;->f:Landroid/webkit/WebView;

    .line 2407489
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2407490
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "authentication error: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2407491
    iget-object v1, p0, LX/Gwr;->c:LX/Gws;

    monitor-enter v1

    .line 2407492
    :try_start_0
    iget-object v0, p0, LX/Gwr;->c:LX/Gws;

    const/4 v2, 0x0

    iput-boolean v2, v0, LX/Gws;->b:Z

    .line 2407493
    iget-object v0, p0, LX/Gwr;->c:LX/Gws;

    iget-object v0, v0, LX/Gws;->c:Ljava/util/Set;

    .line 2407494
    iget-object v2, p0, LX/Gwr;->c:LX/Gws;

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    iput-object v3, v2, LX/Gws;->c:Ljava/util/Set;

    .line 2407495
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2407496
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BWV;

    .line 2407497
    invoke-interface {v0}, LX/BWV;->c()V

    goto :goto_0

    .line 2407498
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2407499
    :cond_0
    return-void
.end method

.method public final onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 3

    .prologue
    .line 2407500
    iget-object v0, p0, LX/Gwr;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0dU;->j:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 2407501
    if-nez v0, :cond_0

    .line 2407502
    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->proceed()V

    .line 2407503
    :goto_0
    return-void

    .line 2407504
    :cond_0
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 2407505
    if-nez v0, :cond_1

    sget-object v0, LX/03R;->YES:LX/03R;

    iget-object v1, p0, LX/Gwr;->e:LX/03R;

    if-ne v0, v1, :cond_2

    .line 2407506
    :cond_1
    iget-object v0, p0, LX/Gwr;->a:Landroid/content/Context;

    const v1, 0x7f082715

    invoke-static {v0, v1}, LX/0kL;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 2407507
    :cond_2
    iget-object v0, p0, LX/Gwr;->a:Landroid/content/Context;

    const v1, 0x7f082714

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    goto :goto_0
.end method
