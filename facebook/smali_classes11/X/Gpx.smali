.class public LX/Gpx;
.super LX/Cod;
.source ""

# interfaces
.implements LX/CnG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/GpQ;",
        ">;",
        "Lcom/facebook/instantshopping/view/block/ButtonBlockView;"
    }
.end annotation


# instance fields
.field public a:LX/Go0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/GnF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/CIh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Go7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final e:Landroid/widget/FrameLayout;

.field public final f:Lcom/facebook/richdocument/view/widget/RichTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2396021
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 2396022
    const v0, 0x7f0d180e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, LX/Gpx;->f:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2396023
    const v0, 0x7f0d180d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/Gpx;->e:Landroid/widget/FrameLayout;

    .line 2396024
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/Gpx;

    invoke-static {v0}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object v2

    check-cast v2, LX/Go0;

    invoke-static {v0}, LX/GnF;->a(LX/0QB;)LX/GnF;

    move-result-object v3

    check-cast v3, LX/GnF;

    invoke-static {v0}, LX/CIh;->b(LX/0QB;)LX/CIh;

    move-result-object p1

    check-cast p1, LX/CIh;

    invoke-static {v0}, LX/Go7;->a(LX/0QB;)LX/Go7;

    move-result-object v0

    check-cast v0, LX/Go7;

    iput-object v2, p0, LX/Gpx;->a:LX/Go0;

    iput-object v3, p0, LX/Gpx;->b:LX/GnF;

    iput-object p1, p0, LX/Gpx;->c:LX/CIh;

    iput-object v0, p0, LX/Gpx;->d:LX/Go7;

    .line 2396025
    return-void
.end method

.method public static a(Landroid/content/Context;LX/0Px;I)Landroid/graphics/drawable/GradientDrawable;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;",
            ">;I)",
            "Landroid/graphics/drawable/GradientDrawable;"
        }
    .end annotation

    .prologue
    .line 2396026
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 2396027
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1bf2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2396028
    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    .line 2396029
    if-eqz p1, :cond_1

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->BUTTON_OUTLINE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-virtual {p1, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->BUTTON_COMPACT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-virtual {p1, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2396030
    :cond_0
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {p0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    invoke-virtual {v0, v1, p2}, Landroid/graphics/drawable/GradientDrawable;->setStroke(II)V

    .line 2396031
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 2396032
    :goto_0
    return-object v0

    .line 2396033
    :cond_1
    invoke-virtual {v0, p2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2396034
    invoke-super {p0, p1}, LX/Cod;->a(Landroid/os/Bundle;)V

    .line 2396035
    iget-object v0, p0, LX/Gpx;->f:Lcom/facebook/richdocument/view/widget/RichTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/RichTextView;->setVisibility(I)V

    .line 2396036
    iget-object v0, p0, LX/Gpx;->f:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2396037
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 2396038
    invoke-virtual {v0}, LX/CtG;->a()V

    .line 2396039
    return-void
.end method
