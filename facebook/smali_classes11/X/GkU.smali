.class public final LX/GkU;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/mutations/protocol/GroupArchiveStatusMutationModels$GroupArchiveMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GkV;


# direct methods
.method public constructor <init>(LX/GkV;)V
    .locals 0

    .prologue
    .line 2389109
    iput-object p1, p0, LX/GkU;->a:LX/GkV;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2389110
    iget-object v0, p0, LX/GkU;->a:LX/GkV;

    iget-object v0, v0, LX/GkV;->b:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    const v1, 0x7f083049

    invoke-static {v0, v1}, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->a$redex0(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;I)V

    .line 2389111
    iget-object v0, p0, LX/GkU;->a:LX/GkV;

    iget-object v0, v0, LX/GkV;->b:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->C:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->D:Ljava/lang/String;

    const-string v2, "Group archive action failed."

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2389112
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2389113
    iget-object v0, p0, LX/GkU;->a:LX/GkV;

    iget-object v0, v0, LX/GkV;->b:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    const v1, 0x7f083047

    invoke-static {v0, v1}, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->a$redex0(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;I)V

    .line 2389114
    iget-object v0, p0, LX/GkU;->a:LX/GkV;

    iget-object v0, v0, LX/GkV;->b:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->o:LX/Gkk;

    check-cast v0, LX/Gl6;

    invoke-virtual {v0}, LX/Gl6;->b()V

    .line 2389115
    return-void
.end method
