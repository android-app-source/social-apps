.class public final LX/GtH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/InternSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/InternSettingsActivity;)V
    .locals 0

    .prologue
    .line 2400495
    iput-object p1, p0, LX/GtH;->a:Lcom/facebook/katana/InternSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2400496
    const-string v0, "webview/?url=https%3A%2F%2Fwww.facebook.com"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2400497
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2400498
    iget-object v0, p0, LX/GtH;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v0, v0, Lcom/facebook/katana/InternSettingsActivity;->E:LX/D39;

    iget-object v2, p0, LX/GtH;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v2, v2, Lcom/facebook/katana/InternSettingsActivity;->u:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2400499
    if-nez v0, :cond_0

    .line 2400500
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2400501
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2400502
    :cond_0
    :try_start_0
    invoke-static {v1}, LX/32x;->b(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "force_external_browser"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2400503
    :cond_1
    iget-object v1, p0, LX/GtH;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v1, v1, Lcom/facebook/katana/InternSettingsActivity;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/GtH;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v2, v2, Lcom/facebook/katana/InternSettingsActivity;->u:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2400504
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 2400505
    :cond_2
    iget-object v1, p0, LX/GtH;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v1, v1, Lcom/facebook/katana/InternSettingsActivity;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/GtH;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v2, v2, Lcom/facebook/katana/InternSettingsActivity;->u:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2400506
    :catch_0
    iget-object v0, p0, LX/GtH;->a:Lcom/facebook/katana/InternSettingsActivity;

    invoke-virtual {v0}, Lcom/facebook/katana/InternSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "Couldn\'t start activity"

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
