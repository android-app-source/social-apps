.class public final LX/FJb;
.super LX/3hi;
.source ""


# instance fields
.field public final synthetic b:Landroid/graphics/Bitmap;

.field public final synthetic c:I

.field public final synthetic d:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;Landroid/graphics/Bitmap;I)V
    .locals 0

    .prologue
    .line 2223693
    iput-object p1, p0, LX/FJb;->d:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    iput-object p2, p0, LX/FJb;->b:Landroid/graphics/Bitmap;

    iput p3, p0, LX/FJb;->c:I

    invoke-direct {p0}, LX/3hi;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 9

    .prologue
    .line 2223694
    iget-object v0, p0, LX/FJb;->d:Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;

    iget-object v1, p0, LX/FJb;->b:Landroid/graphics/Bitmap;

    iget v2, p0, LX/FJb;->c:I

    const/4 v4, 0x0

    const/4 p0, 0x0

    .line 2223695
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2223696
    invoke-virtual {v3, p2, v4, v4, p0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 2223697
    iget-object v4, v0, Lcom/facebook/messaging/notify/util/MessagingNotificationUtil;->b:Landroid/content/Context;

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v4, v5}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v4

    .line 2223698
    new-instance v5, Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    sub-int v6, v2, v6

    sub-int/2addr v6, v4

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    sub-int v7, v2, v7

    sub-int/2addr v7, v4

    sub-int v8, v2, v4

    sub-int v4, v2, v4

    invoke-direct {v5, v6, v7, v8, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2223699
    invoke-virtual {v3, v1, p0, v5, p0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 2223700
    return-void
.end method
