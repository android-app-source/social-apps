.class public final LX/GVV;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/captcha/fragment/CaptchaFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/captcha/fragment/CaptchaFragment;)V
    .locals 0

    .prologue
    .line 2359516
    iput-object p1, p0, LX/GVV;->a:Lcom/facebook/captcha/fragment/CaptchaFragment;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2359517
    iget-object v0, p0, LX/GVV;->a:Lcom/facebook/captcha/fragment/CaptchaFragment;

    .line 2359518
    iget-object v1, v0, Lcom/facebook/captcha/fragment/CaptchaFragment;->h:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->a()V

    .line 2359519
    invoke-static {v0}, Lcom/facebook/captcha/fragment/CaptchaFragment;->k(Lcom/facebook/captcha/fragment/CaptchaFragment;)V

    .line 2359520
    iget-object v1, v0, Lcom/facebook/captcha/fragment/CaptchaFragment;->k:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2359521
    iget-object v1, v0, Lcom/facebook/captcha/fragment/CaptchaFragment;->k:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2359522
    iget-object v1, v0, Lcom/facebook/captcha/fragment/CaptchaFragment;->o:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/captcha/fragment/CaptchaFragment;->o:Ljava/lang/Integer;

    .line 2359523
    iget-object v1, v0, Lcom/facebook/captcha/fragment/CaptchaFragment;->c:LX/GVe;

    iget-object v2, v0, Lcom/facebook/captcha/fragment/CaptchaFragment;->o:Ljava/lang/Integer;

    .line 2359524
    iget-object p0, v1, LX/GVe;->a:LX/0Zb;

    new-instance p1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v0, LX/GVd;->TRY_ANOTHER_CAPTCHA_CLICKED:LX/GVd;

    invoke-virtual {v0}, LX/GVd;->getEventName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v0, "captcha"

    .line 2359525
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2359526
    move-object p1, p1

    .line 2359527
    const-string v0, "numTryAnotherClicks"

    invoke-virtual {p1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const/4 v0, 0x1

    invoke-interface {p0, p1, v0}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2359528
    return-void
.end method
