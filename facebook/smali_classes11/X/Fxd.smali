.class public final LX/Fxd;
.super Landroid/view/animation/Animation;
.source ""


# instance fields
.field public final synthetic a:LX/Fxf;

.field public final b:Landroid/view/View;

.field public c:Z


# direct methods
.method public constructor <init>(LX/Fxf;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2305585
    iput-object p1, p0, LX/Fxd;->a:LX/Fxf;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    .line 2305586
    iput-object p2, p0, LX/Fxd;->b:Landroid/view/View;

    .line 2305587
    iget-object v0, p1, LX/Fxf;->p:Landroid/view/animation/Interpolator;

    invoke-virtual {p0, v0}, LX/Fxd;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 2305588
    iget v0, p1, LX/Fxf;->o:I

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, LX/Fxd;->setDuration(J)V

    .line 2305589
    new-instance v0, LX/Fxc;

    invoke-direct {v0, p0, p1}, LX/Fxc;-><init>(LX/Fxd;LX/Fxf;)V

    invoke-virtual {p0, v0}, LX/Fxd;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 2305590
    return-void
.end method


# virtual methods
.method public final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 5

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 2305591
    iget-object v3, p0, LX/Fxd;->b:Landroid/view/View;

    iget-boolean v0, p0, LX/Fxd;->c:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iget-boolean v4, p0, LX/Fxd;->c:Z

    if-nez v4, :cond_1

    .line 2305592
    :goto_1
    sub-float v2, v1, v0

    mul-float/2addr v2, p1

    add-float/2addr v2, v0

    move v0, v2

    .line 2305593
    invoke-virtual {v3, v0}, Landroid/view/View;->setAlpha(F)V

    .line 2305594
    return-void

    :cond_0
    move v0, v2

    .line 2305595
    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method
