.class public LX/F6M;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/F6L;


# instance fields
.field private final a:LX/193;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/195;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/concurrent/ExecutorService;

.field private d:Ljava/util/concurrent/Future;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/193;Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2200358
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2200359
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/F6M;->b:Ljava/util/Map;

    .line 2200360
    const-string v0, ""

    iput-object v0, p0, LX/F6M;->e:Ljava/lang/String;

    .line 2200361
    iput-object p2, p0, LX/F6M;->c:Ljava/util/concurrent/ExecutorService;

    .line 2200362
    iput-object p1, p0, LX/F6M;->a:LX/193;

    .line 2200363
    return-void
.end method

.method public static c(LX/F6M;Ljava/lang/String;)LX/195;
    .locals 5

    .prologue
    .line 2200364
    iput-object p1, p0, LX/F6M;->e:Ljava/lang/String;

    .line 2200365
    iget-object v0, p0, LX/F6M;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2200366
    iget-object v0, p0, LX/F6M;->b:Ljava/util/Map;

    iget-object v1, p0, LX/F6M;->a:LX/193;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GROUPS_SCROLL_PERF_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2200367
    :cond_0
    iget-object v0, p0, LX/F6M;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/195;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2200368
    iget-object v0, p0, LX/F6M;->d:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/F6M;->d:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/F6M;->d:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2200369
    iget-object v0, p0, LX/F6M;->d:Ljava/util/concurrent/Future;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 2200370
    :cond_0
    invoke-static {p0, p1}, LX/F6M;->c(LX/F6M;Ljava/lang/String;)LX/195;

    move-result-object v0

    invoke-virtual {v0}, LX/195;->a()V

    .line 2200371
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2200372
    iget-object v0, p0, LX/F6M;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/facebook/groups/react/GroupsReactFpsLogger$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/groups/react/GroupsReactFpsLogger$1;-><init>(LX/F6M;Ljava/lang/String;)V

    const/16 v2, 0x28

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0xb678353

    invoke-static {v0, v1, v2, v3}, LX/03X;->a(Ljava/util/concurrent/ExecutorService;Ljava/lang/Runnable;Ljava/lang/Object;I)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, LX/F6M;->d:Ljava/util/concurrent/Future;

    .line 2200373
    return-void
.end method
