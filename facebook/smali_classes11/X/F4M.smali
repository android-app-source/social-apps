.class public final LX/F4M;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "Landroid/widget/LinearLayout;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Landroid/view/View$OnClickListener;

.field public final synthetic c:Ljava/lang/CharSequence;

.field public final synthetic d:LX/F4Q;


# direct methods
.method public constructor <init>(LX/F4Q;LX/DML;Ljava/lang/String;Landroid/view/View$OnClickListener;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 2196871
    iput-object p1, p0, LX/F4M;->d:LX/F4Q;

    iput-object p3, p0, LX/F4M;->a:Ljava/lang/String;

    iput-object p4, p0, LX/F4M;->b:Landroid/view/View$OnClickListener;

    iput-object p5, p0, LX/F4M;->c:Ljava/lang/CharSequence;

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2196872
    check-cast p1, Landroid/widget/LinearLayout;

    .line 2196873
    const v0, 0x7f0d1554

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    .line 2196874
    iget-object v1, p0, LX/F4M;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2196875
    iget-object v1, p0, LX/F4M;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2196876
    iget-object v1, p0, LX/F4M;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2196877
    const v0, 0x7f0d1553

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/editsettings/view/SimpleAccessibleTextLayoutView;

    .line 2196878
    iget-object v1, p0, LX/F4M;->c:Ljava/lang/CharSequence;

    if-eqz v1, :cond_0

    .line 2196879
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/groups/editsettings/view/SimpleAccessibleTextLayoutView;->setVisibility(I)V

    .line 2196880
    new-instance v1, LX/1nq;

    invoke-direct {v1}, LX/1nq;-><init>()V

    iget-object v2, p0, LX/F4M;->d:LX/F4Q;

    iget-object v2, v2, LX/F4Q;->a:Landroid/content/res/Resources;

    const v3, 0x7f0a05ef

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, LX/1nq;->c(I)LX/1nq;

    move-result-object v1

    iget-object v2, p0, LX/F4M;->c:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, LX/1nq;->a(Ljava/lang/CharSequence;)LX/1nq;

    move-result-object v1

    iget-object v2, p0, LX/F4M;->d:LX/F4Q;

    iget-object v2, v2, LX/F4Q;->a:Landroid/content/res/Resources;

    const v3, 0x7f0b0050

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2}, LX/1nq;->b(I)LX/1nq;

    move-result-object v1

    .line 2196881
    iput-object v1, v0, Lcom/facebook/groups/editsettings/view/SimpleAccessibleTextLayoutView;->a:LX/1nq;

    .line 2196882
    :goto_0
    return-void

    .line 2196883
    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/groups/editsettings/view/SimpleAccessibleTextLayoutView;->setVisibility(I)V

    goto :goto_0
.end method
