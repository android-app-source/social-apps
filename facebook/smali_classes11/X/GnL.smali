.class public LX/GnL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/Chv;


# direct methods
.method public constructor <init>(LX/Chv;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2393689
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2393690
    iput-object p1, p0, LX/GnL;->a:LX/Chv;

    .line 2393691
    return-void
.end method

.method public static a(LX/0QB;)LX/GnL;
    .locals 4

    .prologue
    .line 2393678
    const-class v1, LX/GnL;

    monitor-enter v1

    .line 2393679
    :try_start_0
    sget-object v0, LX/GnL;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2393680
    sput-object v2, LX/GnL;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2393681
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2393682
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2393683
    new-instance p0, LX/GnL;

    invoke-static {v0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v3

    check-cast v3, LX/Chv;

    invoke-direct {p0, v3}, LX/GnL;-><init>(LX/Chv;)V

    .line 2393684
    move-object v0, p0

    .line 2393685
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2393686
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GnL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2393687
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2393688
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
