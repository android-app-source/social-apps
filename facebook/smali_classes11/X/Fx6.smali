.class public LX/Fx6;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/G6g;


# instance fields
.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/Fx4;

.field public final d:LX/1BK;

.field public final e:LX/EQL;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x6

    const/4 v1, 0x0

    .line 2304274
    new-instance v0, LX/G6g;

    invoke-direct {v0, v1, v1, v2, v2}, LX/G6g;-><init>(IIII)V

    sput-object v0, LX/Fx6;->a:LX/G6g;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/Fx4;LX/1BK;LX/EQL;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;",
            "LX/Fx4;",
            "LX/1BK;",
            "LX/EQL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2304275
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2304276
    iput-object p1, p0, LX/Fx6;->b:LX/0Or;

    .line 2304277
    iput-object p2, p0, LX/Fx6;->c:LX/Fx4;

    .line 2304278
    iput-object p3, p0, LX/Fx6;->d:LX/1BK;

    .line 2304279
    iput-object p4, p0, LX/Fx6;->e:LX/EQL;

    .line 2304280
    return-void
.end method

.method public static a(LX/5vn;)LX/G6g;
    .locals 6

    .prologue
    .line 2304281
    if-nez p0, :cond_0

    .line 2304282
    sget-object v0, LX/Fx6;->a:LX/G6g;

    .line 2304283
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/G6g;

    invoke-interface {p0}, LX/5vn;->c()D

    move-result-wide v2

    double-to-int v1, v2

    invoke-interface {p0}, LX/5vn;->d()D

    move-result-wide v2

    double-to-int v2, v2

    invoke-interface {p0}, LX/5vn;->b()D

    move-result-wide v4

    double-to-int v3, v4

    invoke-interface {p0}, LX/5vn;->a()D

    move-result-wide v4

    double-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, LX/G6g;-><init>(IIII)V

    goto :goto_0
.end method

.method public static a(LX/1U8;)Landroid/graphics/PointF;
    .locals 4

    .prologue
    const/high16 v1, 0x3f000000    # 0.5f

    .line 2304284
    if-eqz p0, :cond_0

    invoke-interface {p0}, LX/1U8;->c()LX/1f8;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2304285
    :cond_0
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v1, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 2304286
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Landroid/graphics/PointF;

    invoke-interface {p0}, LX/1U8;->c()LX/1f8;

    move-result-object v1

    invoke-interface {v1}, LX/1f8;->a()D

    move-result-wide v2

    double-to-float v1, v2

    invoke-interface {p0}, LX/1U8;->c()LX/1f8;

    move-result-object v2

    invoke-interface {v2}, LX/1f8;->b()D

    move-result-wide v2

    double-to-float v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/1aZ;Ljava/lang/String;Landroid/graphics/PointF;)Lcom/facebook/drawee/fbpipeline/FbDraweeView;
    .locals 5
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2304287
    new-instance v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-direct {v0, p1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;-><init>(Landroid/content/Context;)V

    .line 2304288
    new-instance v1, LX/1Uo;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2}, LX/1Uo;-><init>(Landroid/content/res/Resources;)V

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a054c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, LX/1Uo;->g(Landroid/graphics/drawable/Drawable;)LX/1Uo;

    move-result-object v1

    .line 2304289
    iget-object v2, p0, LX/Fx6;->e:LX/EQL;

    const v3, 0x7f0a0168

    invoke-virtual {v2, p3, v3}, LX/EQL;->a(Ljava/lang/String;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    move-object v2, v2

    .line 2304290
    iput-object v2, v1, LX/1Uo;->f:Landroid/graphics/drawable/Drawable;

    .line 2304291
    move-object v1, v1

    .line 2304292
    sget-object v2, LX/1Up;->h:LX/1Up;

    invoke-virtual {v1, v2}, LX/1Uo;->e(LX/1Up;)LX/1Uo;

    move-result-object v1

    .line 2304293
    iput-object p4, v1, LX/1Uo;->p:Landroid/graphics/PointF;

    .line 2304294
    move-object v1, v1

    .line 2304295
    invoke-virtual {v1}, LX/1Uo;->u()LX/1af;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/DraweeView;->setHierarchy(LX/1aY;)V

    .line 2304296
    invoke-virtual {v0, p2}, Lcom/facebook/drawee/view/DraweeView;->setController(LX/1aZ;)V

    .line 2304297
    return-object v0
.end method

.method public final a(Ljava/lang/String;LX/1aZ;LX/1bf;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # LX/1bf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2304298
    if-eqz p1, :cond_0

    if-eqz p3, :cond_0

    .line 2304299
    iget-object v0, p3, LX/1bf;->b:Landroid/net/Uri;

    move-object v0, v0

    .line 2304300
    if-nez v0, :cond_1

    .line 2304301
    :cond_0
    :goto_0
    return-void

    .line 2304302
    :cond_1
    iget-object v0, p0, LX/Fx6;->d:LX/1BK;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "featured_photos_unit"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2, v1, p3}, LX/1BL;->a(LX/1aZ;Ljava/lang/String;LX/1bf;)V

    goto :goto_0
.end method
