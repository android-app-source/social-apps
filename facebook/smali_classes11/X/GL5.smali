.class public final LX/GL5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GDi;


# instance fields
.field public final synthetic a:LX/GL6;


# direct methods
.method public constructor <init>(LX/GL6;)V
    .locals 0

    .prologue
    .line 2342074
    iput-object p1, p0, LX/GL5;->a:LX/GL6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;)V
    .locals 3

    .prologue
    .line 2342075
    iget-object v0, p0, LX/GL5;->a:LX/GL6;

    iget-object v0, v0, LX/GL6;->a:LX/GL7;

    iget-object v0, v0, LX/GL7;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;)V

    .line 2342076
    iget-object v0, p0, LX/GL5;->a:LX/GL6;

    iget-object v0, v0, LX/GL6;->a:LX/GL7;

    iget-object v0, v0, LX/GL7;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 2342077
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->e:Ljava/lang/String;

    .line 2342078
    iget-object v0, p0, LX/GL5;->a:LX/GL6;

    iget-object v0, v0, LX/GL6;->a:LX/GL7;

    iget-object v0, v0, LX/GL7;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v1

    .line 2342079
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->f:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2342080
    iget-object v0, p0, LX/GL5;->a:LX/GL6;

    iget-object v0, v0, LX/GL6;->a:LX/GL7;

    iget-object v0, v0, LX/GL7;->c:LX/GDm;

    iget-object v1, p0, LX/GL5;->a:LX/GL6;

    iget-object v1, v1, LX/GL6;->a:LX/GL7;

    iget-object v1, v1, LX/GL7;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v2, p0, LX/GL5;->a:LX/GL6;

    iget-object v2, v2, LX/GL6;->a:LX/GL7;

    iget-object v2, v2, LX/GL7;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2342081
    iput-object v2, v0, LX/GDm;->f:Landroid/content/Context;

    .line 2342082
    const p0, 0x7f080af2

    const/4 p1, 0x0

    invoke-virtual {v0, v1, v2, p0, p1}, LX/GDY;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;IZ)V

    .line 2342083
    return-void
.end method
