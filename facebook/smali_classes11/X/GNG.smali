.class public final LX/GNG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public final synthetic b:LX/GCE;

.field public final synthetic c:LX/GFR;

.field public final synthetic d:LX/GNI;

.field public final synthetic e:LX/GDm;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;LX/GCE;LX/GFR;LX/GNI;LX/GDm;)V
    .locals 0

    .prologue
    .line 2345592
    iput-object p1, p0, LX/GNG;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iput-object p2, p0, LX/GNG;->b:LX/GCE;

    iput-object p3, p0, LX/GNG;->c:LX/GFR;

    iput-object p4, p0, LX/GNG;->d:LX/GNI;

    iput-object p5, p0, LX/GNG;->e:LX/GDm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x1cac4308

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2345593
    iget-object v1, p0, LX/GNG;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v2, p0, LX/GNG;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v1

    .line 2345594
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->s()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2345595
    iget-object v2, p0, LX/GNG;->b:LX/GCE;

    sget v3, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->y:I

    iget-object v4, p0, LX/GNG;->c:LX/GFR;

    invoke-virtual {v2, v3, v4}, LX/GCE;->a(ILX/GFR;)V

    .line 2345596
    iget-object v2, p0, LX/GNG;->d:LX/GNI;

    iget-object v3, p0, LX/GNG;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v4, p0, LX/GNG;->b:LX/GCE;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v2, v1, v3, v4, v5}, LX/GNI;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;LX/GCE;Landroid/content/Context;)V

    .line 2345597
    const v1, -0x3837e448

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2345598
    :goto_0
    return-void

    .line 2345599
    :cond_0
    iget-object v1, p0, LX/GNG;->e:LX/GDm;

    iget-object v2, p0, LX/GNG;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, LX/A8v;->UPDATE_BUDGET:LX/A8v;

    invoke-virtual {v1, v2, v3, v4}, LX/GDm;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;LX/A8v;)V

    .line 2345600
    const v1, -0x8c1d5fa

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0
.end method
