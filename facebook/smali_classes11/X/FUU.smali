.class public final enum LX/FUU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FUU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FUU;

.field public static final enum SCAN:LX/FUU;

.field public static final enum SHOW:LX/FUU;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2248300
    new-instance v0, LX/FUU;

    const-string v1, "SHOW"

    invoke-direct {v0, v1, v2}, LX/FUU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FUU;->SHOW:LX/FUU;

    new-instance v0, LX/FUU;

    const-string v1, "SCAN"

    invoke-direct {v0, v1, v3}, LX/FUU;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FUU;->SCAN:LX/FUU;

    .line 2248301
    const/4 v0, 0x2

    new-array v0, v0, [LX/FUU;

    sget-object v1, LX/FUU;->SHOW:LX/FUU;

    aput-object v1, v0, v2

    sget-object v1, LX/FUU;->SCAN:LX/FUU;

    aput-object v1, v0, v3

    sput-object v0, LX/FUU;->$VALUES:[LX/FUU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2248302
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FUU;
    .locals 1

    .prologue
    .line 2248303
    const-class v0, LX/FUU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FUU;

    return-object v0
.end method

.method public static values()[LX/FUU;
    .locals 1

    .prologue
    .line 2248304
    sget-object v0, LX/FUU;->$VALUES:[LX/FUU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FUU;

    return-object v0
.end method
