.class public abstract LX/Fwg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<NuxController::",
        "LX/0i1;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:LX/0iA;

.field public b:Z


# direct methods
.method public constructor <init>(LX/0iA;)V
    .locals 1

    .prologue
    .line 2303439
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2303440
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Fwg;->b:Z

    .line 2303441
    iput-object p1, p0, LX/Fwg;->a:LX/0iA;

    .line 2303442
    return-void
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public abstract b()Lcom/facebook/interstitial/manager/InterstitialTrigger;
.end method

.method public abstract c()Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<TNuxController;>;"
        }
    .end annotation
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2303443
    iget-object v0, p0, LX/Fwg;->a:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    invoke-virtual {p0}, LX/Fwg;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2303444
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2303445
    iget-object v0, p0, LX/Fwg;->a:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    invoke-virtual {p0}, LX/Fwg;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->d(Ljava/lang/String;)V

    .line 2303446
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fwg;->b:Z

    .line 2303447
    return-void
.end method

.method public final g()LX/0i1;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TNuxController;"
        }
    .end annotation

    .prologue
    .line 2303448
    iget-object v0, p0, LX/Fwg;->a:LX/0iA;

    invoke-virtual {p0}, LX/Fwg;->b()Lcom/facebook/interstitial/manager/InterstitialTrigger;

    move-result-object v1

    invoke-virtual {p0}, LX/Fwg;->c()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    return-object v0
.end method
