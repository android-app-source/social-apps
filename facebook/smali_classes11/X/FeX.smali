.class public final LX/FeX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ve;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6Ve",
        "<",
        "LX/EJ3;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FeY;


# direct methods
.method public constructor <init>(LX/FeY;)V
    .locals 0

    .prologue
    .line 2265913
    iput-object p1, p0, LX/FeX;->a:LX/FeY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;II)V
    .locals 10

    .prologue
    .line 2265914
    iget-object v0, p0, LX/FeX;->a:LX/FeY;

    iget-object v7, v0, LX/FeY;->f:LX/CvY;

    iget-object v0, p0, LX/FeX;->a:LX/FeY;

    iget-object v8, v0, LX/FeY;->h:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    if-eqz p3, :cond_0

    sget-object v0, LX/8ch;->REACTED:LX/8ch;

    move-object v6, v0

    :goto_0
    iget-object v0, p0, LX/FeX;->a:LX/FeY;

    iget-object v0, v0, LX/FeY;->e:LX/CzE;

    invoke-virtual {v0, p1}, LX/CzE;->c(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CvY;->a(Ljava/lang/String;)LX/CvV;

    move-result-object v9

    iget-object v0, p0, LX/FeX;->a:LX/FeY;

    iget-object v0, v0, LX/FeY;->h:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz p3, :cond_1

    sget-object v3, LX/8ch;->REACTED:LX/8ch;

    :goto_1
    iget-object v1, p0, LX/FeX;->a:LX/FeY;

    iget-object v1, v1, LX/FeY;->e:LX/CzE;

    invoke-virtual {v1, p1}, LX/CzE;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v4

    iget-object v1, p0, LX/FeX;->a:LX/FeY;

    iget-object v1, v1, LX/FeY;->e:LX/CzE;

    invoke-virtual {v1, p1}, LX/CzE;->b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v5

    move v1, p2

    invoke-static/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILjava/lang/String;LX/8ch;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    move-object v0, v7

    move-object v1, v8

    move-object v2, v6

    move v3, p2

    move-object v4, v9

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2265915
    return-void

    .line 2265916
    :cond_0
    sget-object v0, LX/8ch;->UNREACTED:LX/8ch;

    move-object v6, v0

    goto :goto_0

    :cond_1
    sget-object v3, LX/8ch;->UNREACTED:LX/8ch;

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2265917
    check-cast p1, LX/EJ3;

    .line 2265918
    iget-object v0, p1, LX/EJ3;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 2265919
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 2265920
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2265921
    iget-object v1, p0, LX/FeX;->a:LX/FeY;

    iget-object v1, v1, LX/FeY;->k:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/20h;

    .line 2265922
    iget-object v2, p1, LX/EJ3;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2265923
    iget-object v3, p1, LX/EJ3;->c:LX/1zt;

    move-object v3, v3

    .line 2265924
    invoke-virtual {v1, v0, v2, v3}, LX/20h;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;LX/1zt;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v3

    .line 2265925
    iget-object v1, p0, LX/FeX;->a:LX/FeY;

    iget-object v1, v1, LX/FeY;->e:LX/CzE;

    invoke-virtual {v1, v0}, LX/CzE;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/0am;

    move-result-object v2

    .line 2265926
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2265927
    iget-object v1, p1, LX/EJ3;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 2265928
    invoke-static {v1}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2265929
    if-eqz v1, :cond_1

    iget-object v2, p0, LX/FeX;->a:LX/FeY;

    iget-object v2, v2, LX/FeY;->e:LX/CzE;

    invoke-virtual {v2, v1}, LX/CzE;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/0am;

    move-result-object v1

    move-object v2, v1

    .line 2265930
    :goto_0
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;

    invoke-interface {v1, v3}, Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;

    move-result-object v1

    move-object v3, v1

    .line 2265931
    :cond_0
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    .line 2265932
    :goto_1
    iget-object v2, p0, LX/FeX;->a:LX/FeY;

    iget-object v2, v2, LX/FeY;->e:LX/CzE;

    invoke-virtual {v2, v1}, LX/CzE;->b(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v2

    .line 2265933
    iget-object v4, p1, LX/EJ3;->c:LX/1zt;

    move-object v4, v4

    .line 2265934
    iget p1, v4, LX/1zt;->e:I

    move v4, p1

    .line 2265935
    invoke-direct {p0, v0, v2, v4}, LX/FeX;->a(Lcom/facebook/graphql/model/GraphQLStory;II)V

    .line 2265936
    iget-object v0, p0, LX/FeX;->a:LX/FeY;

    invoke-static {v0, v1, v3}, LX/FeY;->a$redex0(LX/FeY;Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2265937
    :goto_2
    return-void

    .line 2265938
    :cond_1
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    move-object v2, v1

    goto :goto_0

    :cond_2
    move-object v1, v0

    .line 2265939
    goto :goto_1

    .line 2265940
    :cond_3
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;

    invoke-interface {v1, v3}, Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;

    move-result-object v3

    .line 2265941
    iget-object v1, p0, LX/FeX;->a:LX/FeY;

    iget-object v4, v1, LX/FeY;->e:LX/CzE;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v4, v1}, LX/CzE;->b(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v1

    .line 2265942
    iget-object v4, p1, LX/EJ3;->c:LX/1zt;

    move-object v4, v4

    .line 2265943
    iget p1, v4, LX/1zt;->e:I

    move v4, p1

    .line 2265944
    invoke-direct {p0, v0, v1, v4}, LX/FeX;->a(Lcom/facebook/graphql/model/GraphQLStory;II)V

    .line 2265945
    iget-object v1, p0, LX/FeX;->a:LX/FeY;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v1, v0, v3}, LX/FeY;->a$redex0(LX/FeY;Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)V

    goto :goto_2
.end method
