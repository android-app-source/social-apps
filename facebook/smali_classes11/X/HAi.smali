.class public final LX/HAi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;

.field public final synthetic b:Landroid/view/ViewGroup;

.field public final synthetic c:LX/HAj;


# direct methods
.method public constructor <init>(LX/HAj;Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2435852
    iput-object p1, p0, LX/HAi;->c:LX/HAj;

    iput-object p2, p0, LX/HAi;->a:Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;

    iput-object p3, p0, LX/HAi;->b:Landroid/view/ViewGroup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x46631a57

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2435853
    iget-object v1, p0, LX/HAi;->c:LX/HAj;

    iget-object v1, v1, LX/HAj;->a:LX/CSL;

    iget-object v2, p0, LX/HAi;->a:Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;

    invoke-virtual {v2}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v4, p0, LX/HAi;->a:Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/HAi;->a:Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;

    invoke-virtual {v5}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel$NodesModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, LX/CSL;->b(JLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2435854
    iget-object v2, p0, LX/HAi;->c:LX/HAj;

    iget-object v2, v2, LX/HAj;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/HAi;->b:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2435855
    const v1, 0x5e9d34d0

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
