.class public final LX/GTl;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V
    .locals 0

    .prologue
    .line 2355806
    iput-object p1, p0, LX/GTl;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2355807
    iget-object v0, p0, LX/GTl;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    .line 2355808
    iput-object p1, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->U:Ljava/lang/String;

    .line 2355809
    const-string v0, "redirect_dashboard"

    iget-object v1, p0, LX/GTl;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->U:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2355810
    iget-object v0, p0, LX/GTl;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-static {v0, v3}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->e(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;Z)V

    .line 2355811
    :goto_0
    return-void

    .line 2355812
    :cond_0
    const-string v0, "invite_notification"

    iget-object v1, p0, LX/GTl;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->U:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2355813
    iget-object v0, p0, LX/GTl;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v1, p0, LX/GTl;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-virtual {v1}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "source"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    aget-object v1, v1, v3

    .line 2355814
    invoke-static {v0, v1}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->b$redex0(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;Ljava/lang/String;)V

    .line 2355815
    goto :goto_0

    .line 2355816
    :cond_1
    iget-object v0, p0, LX/GTl;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-static {v0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->n(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V

    .line 2355817
    iget-object v0, p0, LX/GTl;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-static {v0}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->l(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V

    goto :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2355818
    sget-object v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->B:Ljava/lang/Class;

    const-string v1, "failed to get nux type to show"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2355819
    iget-object v0, p0, LX/GTl;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->w:LX/03V;

    const-string v1, "background_location_nux_type_to_show_fetch_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2355820
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2355821
    check-cast p1, Ljava/lang/String;

    invoke-direct {p0, p1}, LX/GTl;->a(Ljava/lang/String;)V

    return-void
.end method
