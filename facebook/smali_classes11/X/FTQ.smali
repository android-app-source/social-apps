.class public final LX/FTQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 21

    .prologue
    .line 2245665
    const/16 v17, 0x0

    .line 2245666
    const/16 v16, 0x0

    .line 2245667
    const/4 v15, 0x0

    .line 2245668
    const/4 v14, 0x0

    .line 2245669
    const/4 v13, 0x0

    .line 2245670
    const/4 v12, 0x0

    .line 2245671
    const/4 v11, 0x0

    .line 2245672
    const/4 v10, 0x0

    .line 2245673
    const/4 v9, 0x0

    .line 2245674
    const/4 v8, 0x0

    .line 2245675
    const/4 v7, 0x0

    .line 2245676
    const/4 v6, 0x0

    .line 2245677
    const/4 v5, 0x0

    .line 2245678
    const/4 v4, 0x0

    .line 2245679
    const/4 v3, 0x0

    .line 2245680
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_1

    .line 2245681
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2245682
    const/4 v3, 0x0

    .line 2245683
    :goto_0
    return v3

    .line 2245684
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2245685
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v18

    sget-object v19, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_d

    .line 2245686
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v18

    .line 2245687
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2245688
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v19

    sget-object v20, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    if-eq v0, v1, :cond_1

    if-eqz v18, :cond_1

    .line 2245689
    const-string v19, "alternate_name"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 2245690
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v17

    goto :goto_1

    .line 2245691
    :cond_2
    const-string v19, "cover_photo"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 2245692
    invoke-static/range {p0 .. p1}, LX/FTN;->a(LX/15w;LX/186;)I

    move-result v16

    goto :goto_1

    .line 2245693
    :cond_3
    const-string v19, "id"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_4

    .line 2245694
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto :goto_1

    .line 2245695
    :cond_4
    const-string v19, "is_verified"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 2245696
    const/4 v5, 0x1

    .line 2245697
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v14

    goto :goto_1

    .line 2245698
    :cond_5
    const-string v19, "is_work_user"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 2245699
    const/4 v4, 0x1

    .line 2245700
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v13

    goto :goto_1

    .line 2245701
    :cond_6
    const-string v19, "name"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 2245702
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 2245703
    :cond_7
    const-string v19, "preliminaryProfilePicture"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 2245704
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v11

    goto/16 :goto_1

    .line 2245705
    :cond_8
    const-string v19, "profile_photo"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 2245706
    invoke-static/range {p0 .. p1}, LX/FTS;->a(LX/15w;LX/186;)I

    move-result v10

    goto/16 :goto_1

    .line 2245707
    :cond_9
    const-string v19, "profile_picture"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_a

    .line 2245708
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 2245709
    :cond_a
    const-string v19, "profile_picture_is_silhouette"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_b

    .line 2245710
    const/4 v3, 0x1

    .line 2245711
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto/16 :goto_1

    .line 2245712
    :cond_b
    const-string v19, "profile_video"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_c

    .line 2245713
    invoke-static/range {p0 .. p1}, LX/FTP;->a(LX/15w;LX/186;)I

    move-result v7

    goto/16 :goto_1

    .line 2245714
    :cond_c
    const-string v19, "structured_name"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_0

    .line 2245715
    invoke-static/range {p0 .. p1}, LX/3EF;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 2245716
    :cond_d
    const/16 v18, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2245717
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2245718
    const/16 v17, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2245719
    const/16 v16, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 2245720
    if-eqz v5, :cond_e

    .line 2245721
    const/4 v5, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v14}, LX/186;->a(IZ)V

    .line 2245722
    :cond_e
    if-eqz v4, :cond_f

    .line 2245723
    const/4 v4, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v13}, LX/186;->a(IZ)V

    .line 2245724
    :cond_f
    const/4 v4, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v12}, LX/186;->b(II)V

    .line 2245725
    const/4 v4, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v11}, LX/186;->b(II)V

    .line 2245726
    const/4 v4, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v10}, LX/186;->b(II)V

    .line 2245727
    const/16 v4, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v9}, LX/186;->b(II)V

    .line 2245728
    if-eqz v3, :cond_10

    .line 2245729
    const/16 v3, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->a(IZ)V

    .line 2245730
    :cond_10
    const/16 v3, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v7}, LX/186;->b(II)V

    .line 2245731
    const/16 v3, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, LX/186;->b(II)V

    .line 2245732
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v3

    goto/16 :goto_0
.end method
