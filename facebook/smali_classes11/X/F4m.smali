.class public final LX/F4m;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;)V
    .locals 0

    .prologue
    .line 2197238
    iput-object p1, p0, LX/F4m;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 10

    .prologue
    .line 2197239
    iget-object v0, p0, LX/F4m;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2197240
    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->u:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v1}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    .line 2197241
    new-instance v2, LX/4Fi;

    invoke-direct {v2}, LX/4Fi;-><init>()V

    .line 2197242
    const-string v3, "name"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2197243
    move-object v1, v2

    .line 2197244
    iget-object v2, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197245
    iget-object v3, v2, LX/F53;->h:Ljava/lang/String;

    move-object v2, v3

    .line 2197246
    const-string v3, "visibility_setting"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2197247
    move-object v1, v1

    .line 2197248
    const-string v2, "create_flow"

    .line 2197249
    const-string v3, "source"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2197250
    move-object v2, v1

    .line 2197251
    new-array v1, v5, [Ljava/lang/CharSequence;

    iget-object v3, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197252
    iget-object v6, v3, LX/F53;->o:Ljava/lang/String;

    move-object v3, v6

    .line 2197253
    aput-object v3, v1, v4

    invoke-static {v1}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2197254
    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197255
    iget-object v3, v1, LX/F53;->o:Ljava/lang/String;

    move-object v1, v3

    .line 2197256
    const-string v3, "referrer"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2197257
    :cond_0
    new-array v1, v5, [Ljava/lang/CharSequence;

    iget-object v3, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197258
    iget-object v6, v3, LX/F53;->j:Ljava/lang/String;

    move-object v3, v6

    .line 2197259
    aput-object v3, v1, v4

    invoke-static {v1}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2197260
    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197261
    iget-object v3, v1, LX/F53;->j:Ljava/lang/String;

    move-object v1, v3

    .line 2197262
    const-string v3, "group_purpose"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2197263
    :cond_1
    new-array v1, v5, [Ljava/lang/CharSequence;

    iget-object v3, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->D:Ljava/lang/String;

    aput-object v3, v1, v4

    invoke-static {v1}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2197264
    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->D:Ljava/lang/String;

    .line 2197265
    const-string v3, "suggestion_category"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2197266
    :cond_2
    new-array v1, v5, [Ljava/lang/CharSequence;

    iget-object v3, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->E:Ljava/lang/String;

    aput-object v3, v1, v4

    invoke-static {v1}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2197267
    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->E:Ljava/lang/String;

    .line 2197268
    const-string v3, "suggestion_identifier"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2197269
    :cond_3
    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197270
    iget-object v3, v1, LX/F53;->r:Ljava/lang/String;

    move-object v1, v3

    .line 2197271
    if-eqz v1, :cond_4

    .line 2197272
    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197273
    iget-object v3, v1, LX/F53;->r:Ljava/lang/String;

    move-object v1, v3

    .line 2197274
    const-string v3, "actor_id"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2197275
    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197276
    iget-object v3, v1, LX/F53;->o:Ljava/lang/String;

    move-object v1, v3

    .line 2197277
    if-nez v1, :cond_4

    .line 2197278
    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    const-string v3, "PAGE_CREATE_FLOW"

    invoke-virtual {v1, v3}, LX/F53;->c(Ljava/lang/String;)V

    .line 2197279
    :cond_4
    const-string v1, "-1"

    iget-object v3, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    invoke-virtual {v3}, LX/F53;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2197280
    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    invoke-virtual {v1}, LX/F53;->l()Ljava/lang/String;

    move-result-object v1

    .line 2197281
    const-string v3, "group_parent"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2197282
    :cond_5
    const/4 v4, 0x0

    .line 2197283
    const v1, 0x7f082f53

    const/4 v3, 0x1

    .line 2197284
    new-instance v5, Lcom/facebook/groups/create/GroupCreationProgressDialogFragment;

    invoke-direct {v5}, Lcom/facebook/groups/create/GroupCreationProgressDialogFragment;-><init>()V

    .line 2197285
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2197286
    const-string v7, "message_res_id"

    invoke-virtual {v6, v7, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2197287
    const-string v7, "is_indeterminate"

    invoke-virtual {v6, v7, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2197288
    const-string v7, "is_cancelable"

    invoke-virtual {v6, v7, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2197289
    const-string v7, "dismiss_on_pause"

    invoke-virtual {v6, v7, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2197290
    invoke-virtual {v5, v6}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2197291
    move-object v1, v5

    .line 2197292
    iput-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->B:Lcom/facebook/groups/create/GroupCreationProgressDialogFragment;

    .line 2197293
    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->B:Lcom/facebook/groups/create/GroupCreationProgressDialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()LX/0gc;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2197294
    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->i:LX/1g8;

    .line 2197295
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "creation_create_requested"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "group_creation"

    .line 2197296
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2197297
    move-object v3, v3

    .line 2197298
    iget-object v4, v1, LX/1g8;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2197299
    invoke-static {v1, v3}, LX/1g8;->a(LX/1g8;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2197300
    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197301
    iget-object v3, v1, LX/F53;->q:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v8, v3

    .line 2197302
    if-nez v8, :cond_6

    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->g:LX/DKa;

    iget-object v3, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    invoke-virtual {v3}, LX/F53;->d()LX/0Px;

    move-result-object v3

    .line 2197303
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 2197304
    iget-object v5, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->M:LX/F4e;

    iget-object v6, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->y:Landroid/net/Uri;

    iget-object v7, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197305
    iget-object v8, v7, LX/F53;->i:Ljava/lang/String;

    move-object v7, v8

    .line 2197306
    invoke-virtual/range {v1 .. v7}, LX/DKa;->a(LX/4Fi;LX/0Px;LX/0Px;LX/F4e;Landroid/net/Uri;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2197307
    :goto_0
    iget-object v2, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->h:LX/1Ck;

    sget-object v3, LX/F4n;->TASK_CREATE_GROUP:LX/F4n;

    new-instance v4, LX/F4k;

    invoke-direct {v4, v0}, LX/F4k;-><init>(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;)V

    invoke-virtual {v2, v3, v1, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2197308
    return-void

    .line 2197309
    :cond_6
    iget-object v1, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->g:LX/DKa;

    iget-object v3, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    invoke-virtual {v3}, LX/F53;->d()LX/0Px;

    move-result-object v3

    .line 2197310
    sget-object v4, LX/0Q7;->a:LX/0Px;

    move-object v4, v4

    .line 2197311
    iget-object v5, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->M:LX/F4e;

    iget-object v6, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->y:Landroid/net/Uri;

    iget-object v7, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateFragment;->o:LX/F53;

    .line 2197312
    iget-object v9, v7, LX/F53;->i:Ljava/lang/String;

    move-object v7, v9

    .line 2197313
    invoke-virtual/range {v1 .. v8}, LX/DKa;->a(LX/4Fi;LX/0Px;LX/0Px;LX/F4e;Landroid/net/Uri;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    goto :goto_0
.end method
