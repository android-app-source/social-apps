.class public final LX/Gcm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/devicebasedlogin/ui/PinCodeView;


# direct methods
.method public constructor <init>(Lcom/facebook/devicebasedlogin/ui/PinCodeView;)V
    .locals 0

    .prologue
    .line 2372562
    iput-object p1, p0, LX/Gcm;->a:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(ILandroid/text/Editable;Landroid/widget/TextView;)V
    .locals 2

    .prologue
    .line 2372545
    invoke-interface {p2}, Landroid/text/Editable;->length()I

    move-result v0

    .line 2372546
    if-le v0, p1, :cond_2

    .line 2372547
    add-int/lit8 v1, p1, 0x1

    invoke-interface {p2, p1, v1}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2372548
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setActivated(Z)V

    .line 2372549
    iget-object v1, p0, LX/Gcm;->a:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    iget-boolean v1, v1, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->h:Z

    if-eqz v1, :cond_0

    .line 2372550
    iget-object v1, p0, LX/Gcm;->a:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->g:Landroid/text/method/PasswordTransformationMethod;

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2372551
    :cond_0
    if-ne v0, p1, :cond_1

    .line 2372552
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setActivated(Z)V

    .line 2372553
    :cond_1
    return-void

    .line 2372554
    :cond_2
    const-string v1, ""

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 2372555
    const/4 v0, 0x0

    iget-object v1, p0, LX/Gcm;->a:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->a:Lcom/facebook/resources/ui/FbEditText;

    invoke-direct {p0, v0, p1, v1}, LX/Gcm;->a(ILandroid/text/Editable;Landroid/widget/TextView;)V

    .line 2372556
    const/4 v0, 0x1

    iget-object v1, p0, LX/Gcm;->a:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->b:Lcom/facebook/resources/ui/FbEditText;

    invoke-direct {p0, v0, p1, v1}, LX/Gcm;->a(ILandroid/text/Editable;Landroid/widget/TextView;)V

    .line 2372557
    const/4 v0, 0x2

    iget-object v1, p0, LX/Gcm;->a:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-direct {p0, v0, p1, v1}, LX/Gcm;->a(ILandroid/text/Editable;Landroid/widget/TextView;)V

    .line 2372558
    const/4 v0, 0x3

    iget-object v1, p0, LX/Gcm;->a:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->d:Lcom/facebook/resources/ui/FbEditText;

    invoke-direct {p0, v0, p1, v1}, LX/Gcm;->a(ILandroid/text/Editable;Landroid/widget/TextView;)V

    .line 2372559
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 2372560
    iget-object v0, p0, LX/Gcm;->a:Lcom/facebook/devicebasedlogin/ui/PinCodeView;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/PinCodeView;->f:LX/GbD;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, LX/GbD;->b(Ljava/lang/String;)V

    .line 2372561
    :cond_0
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2372544
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2372543
    return-void
.end method
