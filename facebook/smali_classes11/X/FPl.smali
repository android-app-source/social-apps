.class public final LX/FPl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Pz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Pz",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2237900
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2237901
    :try_start_0
    invoke-static {p1}, LX/FPm;->valueOf(Ljava/lang/String;)LX/FPm;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2237902
    iget-object v0, p0, LX/FPl;->a:LX/0Pz;

    if-nez v0, :cond_0

    .line 2237903
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    iput-object v0, p0, LX/FPl;->a:LX/0Pz;

    .line 2237904
    :cond_0
    iget-object v0, p0, LX/FPl;->a:LX/0Pz;

    invoke-virtual {v0, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2237905
    return-void

    .line 2237906
    :catch_0
    const-string v0, "Invalid priceCategoryString passed to addPriceCategory. Must be one of [%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, ", "

    invoke-static {}, LX/FPm;->values()[LX/FPm;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2237907
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
