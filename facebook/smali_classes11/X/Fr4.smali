.class public LX/Fr4;
.super LX/Fr3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>(LX/Fsr;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2294979
    invoke-direct {p0, p1}, LX/Fr3;-><init>(LX/Fsr;)V

    .line 2294980
    return-void
.end method

.method public static a(LX/0QB;)LX/Fr4;
    .locals 4

    .prologue
    .line 2294981
    const-class v1, LX/Fr4;

    monitor-enter v1

    .line 2294982
    :try_start_0
    sget-object v0, LX/Fr4;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2294983
    sput-object v2, LX/Fr4;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2294984
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2294985
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2294986
    new-instance p0, LX/Fr4;

    invoke-static {v0}, LX/Fsr;->a(LX/0QB;)LX/Fsr;

    move-result-object v3

    check-cast v3, LX/Fsr;

    invoke-direct {p0, v3}, LX/Fr4;-><init>(LX/Fsr;)V

    .line 2294987
    move-object v0, p0

    .line 2294988
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2294989
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fr4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2294990
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2294991
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;LX/Fr7;)Z
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 2294992
    iget-object v0, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    if-nez v0, :cond_0

    .line 2294993
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setVisibility(I)V

    .line 2294994
    :goto_0
    return v2

    .line 2294995
    :cond_0
    iput-object p0, p1, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->b:LX/D3l;

    .line 2294996
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 2294997
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0da8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2294998
    iget-object v0, p2, LX/Fr7;->b:LX/G4n;

    .line 2294999
    sget-object v3, LX/G4n;->BEGIN:LX/G4n;

    if-ne v0, v3, :cond_2

    const v3, 0x7f0b0daa

    invoke-virtual {v7, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    :goto_1
    move v0, v3

    .line 2295000
    iget-object v3, p2, LX/Fr7;->b:LX/G4n;

    .line 2295001
    sget-object v4, LX/G4n;->END:LX/G4n;

    if-ne v3, v4, :cond_3

    const v4, 0x7f0b0dab

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    :goto_2
    move v3, v4

    .line 2295002
    add-int v4, v1, v0

    add-int/2addr v4, v3

    invoke-virtual {p1, v4}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setMinimumHeight(I)V

    .line 2295003
    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getPaddingLeft()I

    move-result v4

    invoke-virtual {p1, v4}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setThumbnailPadding(I)V

    .line 2295004
    const v4, 0x7f0b0dac

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {p1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->getPaddingRight()I

    move-result v5

    invoke-virtual {p1, v4, v0, v5, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->setPadding(IIII)V

    .line 2295005
    iget-object v0, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    .line 2295006
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->cf_()LX/1Fb;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->cf_()LX/1Fb;

    move-result-object v3

    invoke-interface {v3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 2295007
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->cf_()LX/1Fb;

    move-result-object v3

    invoke-interface {v3}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2295008
    :goto_3
    move-object v4, v3

    .line 2295009
    const-string v5, "timeline"

    move-object v0, p1

    move v3, v2

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(IIILandroid/net/Uri;Ljava/lang/String;)V

    .line 2295010
    iget-object v0, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->d()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;->d()Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/protocol/IntroCommonGraphQLModels$ContextListItemCoreFieldsModel$TitleModel;->a()Ljava/lang/String;

    move-result-object v0

    :goto_4
    const/4 v1, 0x2

    const v3, 0x7f0b0050

    invoke-static {v7, v3}, LX/0tP;->c(Landroid/content/res/Resources;I)I

    move-result v3

    invoke-virtual {p1, v0, v1, v3}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/CharSequence;II)V

    .line 2295011
    invoke-virtual {p1, v6, v2, v2}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/String;II)V

    .line 2295012
    iget-object v0, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    invoke-static {v0}, LX/Fr3;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(Ljava/lang/Integer;)V

    .line 2295013
    iput-boolean v2, p1, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->l:Z

    .line 2295014
    iget-object v0, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    invoke-static {v0}, LX/Fr3;->b(Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;)Z

    move-result v0

    iget-object v1, p2, LX/Fr7;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextListItemFieldsModel;

    invoke-virtual {p1, v0, v1}, Lcom/facebook/uicontrib/contextitem/PlutoniumContextualItemView;->a(ZLjava/lang/Object;)V

    .line 2295015
    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_1
    move-object v0, v6

    .line 2295016
    goto :goto_4

    :cond_2
    const v3, 0x7f0b0da9

    invoke-virtual {v7, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    goto/16 :goto_1

    :cond_3
    const v4, 0x7f0b0da9

    invoke-virtual {v7, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    goto/16 :goto_2

    :cond_4
    const/4 v3, 0x0

    goto :goto_3
.end method
