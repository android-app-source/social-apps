.class public final LX/GOu;
.super LX/GOr;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GOr",
        "<",
        "Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;",
        "LX/GQF;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;Landroid/content/Context;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 2348038
    iput-object p1, p0, LX/GOu;->a:Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;

    invoke-direct {p0, p2, p3}, LX/GOr;-><init>(Landroid/content/Context;Ljava/util/List;)V

    return-void
.end method

.method private a(LX/GQF;Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;)V
    .locals 2

    .prologue
    .line 2348039
    iget-object v0, p1, LX/GQF;->b:Landroid/widget/TextView;

    .line 2348040
    iget-object v1, p2, Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2348041
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2348042
    iget-object v0, p1, LX/GQF;->a:Lcom/facebook/drawee/view/SimpleDraweeView;

    .line 2348043
    iget-object v1, p2, Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;->b:Landroid/net/Uri;

    move-object v1, v1

    .line 2348044
    invoke-virtual {v0, v1}, Lcom/facebook/drawee/view/SimpleDraweeView;->setImageURI(Landroid/net/Uri;)V

    .line 2348045
    new-instance v0, LX/GOt;

    invoke-direct {v0, p0, p2}, LX/GOt;-><init>(LX/GOu;Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;)V

    invoke-virtual {p1, v0}, LX/GQF;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2348046
    return-void
.end method

.method private b()LX/GQF;
    .locals 2

    .prologue
    .line 2348047
    new-instance v0, LX/GQF;

    iget-object v1, p0, LX/GOu;->a:Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;

    invoke-direct {v0, v1}, LX/GQF;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final synthetic a()Landroid/view/View;
    .locals 1

    .prologue
    .line 2348048
    invoke-direct {p0}, LX/GOu;->b()LX/GQF;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Landroid/view/View;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2348049
    check-cast p1, LX/GQF;

    check-cast p2, Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;

    invoke-direct {p0, p1, p2}, LX/GOu;->a(LX/GQF;Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;)V

    return-void
.end method
