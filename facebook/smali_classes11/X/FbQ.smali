.class public LX/FbQ;
.super LX/FbD;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FbD",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FbQ;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2260196
    invoke-direct {p0}, LX/FbD;-><init>()V

    .line 2260197
    return-void
.end method

.method public static a(LX/0QB;)LX/FbQ;
    .locals 3

    .prologue
    .line 2260215
    sget-object v0, LX/FbQ;->a:LX/FbQ;

    if-nez v0, :cond_1

    .line 2260216
    const-class v1, LX/FbQ;

    monitor-enter v1

    .line 2260217
    :try_start_0
    sget-object v0, LX/FbQ;->a:LX/FbQ;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2260218
    if-eqz v2, :cond_0

    .line 2260219
    :try_start_1
    new-instance v0, LX/FbQ;

    invoke-direct {v0}, LX/FbQ;-><init>()V

    .line 2260220
    move-object v0, v0

    .line 2260221
    sput-object v0, LX/FbQ;->a:LX/FbQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2260222
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2260223
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2260224
    :cond_1
    sget-object v0, LX/FbQ;->a:LX/FbQ;

    return-object v0

    .line 2260225
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2260226
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
    .locals 9
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260198
    const/4 v3, 0x0

    .line 2260199
    invoke-static {p1}, LX/Fbf;->b(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v2

    .line 2260200
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->iW()Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2260201
    :cond_0
    new-instance v0, Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;

    const-string v1, "Join the conversation..."

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v3, v3, v2}, Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2260202
    :goto_0
    return-object v0

    .line 2260203
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->iW()Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->o()J

    move-result-wide v0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->iW()Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLSportsDataMatchData;->p()Ljava/lang/String;

    move-result-object v3

    .line 2260204
    const-wide/16 v5, 0x3e8

    mul-long/2addr v5, v0

    .line 2260205
    sget-object v7, LX/0SF;->a:LX/0SF;

    move-object v7, v7

    .line 2260206
    invoke-virtual {v7}, LX/0SF;->a()J

    move-result-wide v7

    cmp-long v5, v5, v7

    if-lez v5, :cond_3

    .line 2260207
    sget-object v5, LX/Cz0;->PREGAME:LX/Cz0;

    .line 2260208
    :goto_1
    move-object v0, v5

    .line 2260209
    sget-object v1, LX/Cz0;->PREGAME:LX/Cz0;

    if-ne v0, v1, :cond_2

    const-string v0, "Who are you rooting for?"

    .line 2260210
    :goto_2
    new-instance v1, Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->e()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v0, v3, v2, v4}, Lcom/facebook/search/results/model/unit/SearchResultsComposerUnit;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    .line 2260211
    :cond_2
    const-string v0, "Join the conversation..."

    goto :goto_2

    .line 2260212
    :cond_3
    const-string v5, "closed"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2260213
    sget-object v5, LX/Cz0;->POSTGAME:LX/Cz0;

    goto :goto_1

    .line 2260214
    :cond_4
    sget-object v5, LX/Cz0;->ONGOING:LX/Cz0;

    goto :goto_1
.end method
