.class public final LX/Fd3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:LX/Fd6;


# direct methods
.method public constructor <init>(LX/Fd6;)V
    .locals 0

    .prologue
    .line 2263060
    iput-object p1, p0, LX/Fd3;->a:LX/Fd6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 2263061
    if-eqz p2, :cond_2

    .line 2263062
    iget-object v0, p0, LX/Fd3;->a:LX/Fd6;

    iget-object v0, v0, LX/Fd6;->t:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2263063
    iget-object v0, p0, LX/Fd3;->a:LX/Fd6;

    invoke-static {v0}, LX/Fd6;->s(LX/Fd6;)V

    .line 2263064
    :cond_0
    iget-object v0, p0, LX/Fd3;->a:LX/Fd6;

    const/4 p2, 0x0

    .line 2263065
    invoke-virtual {v0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string p1, "input_method"

    invoke-virtual {v1, p1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 2263066
    iget-object p1, v0, LX/Fd6;->t:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1, p1, p2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    move-result p1

    if-nez p1, :cond_1

    .line 2263067
    invoke-static {v0}, LX/Fd6;->r(LX/Fd6;)V

    .line 2263068
    invoke-virtual {v1, p2, p2}, Landroid/view/inputmethod/InputMethodManager;->toggleSoftInput(II)V

    .line 2263069
    iget-object p1, v0, LX/Fd6;->t:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1, p1, p2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 2263070
    :cond_1
    iget-object v0, p0, LX/Fd3;->a:LX/Fd6;

    .line 2263071
    iget-object v1, v0, LX/Fd6;->v:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    const/4 p0, 0x4

    invoke-virtual {v1, p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 2263072
    iget-object v1, v0, LX/Fd6;->v:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iget-object p0, v0, LX/Fd6;->n:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2263073
    :goto_0
    return-void

    .line 2263074
    :cond_2
    iget-object v0, p0, LX/Fd3;->a:LX/Fd6;

    invoke-static {v0}, LX/Fd6;->r(LX/Fd6;)V

    .line 2263075
    iget-object v0, p0, LX/Fd3;->a:LX/Fd6;

    invoke-static {v0}, LX/Fd6;->u(LX/Fd6;)V

    .line 2263076
    iget-object v0, p0, LX/Fd3;->a:LX/Fd6;

    .line 2263077
    iget-object v1, v0, LX/Fd6;->v:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    const/4 p0, 0x2

    invoke-virtual {v1, p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDividerThickness(I)V

    .line 2263078
    iget-object v1, v0, LX/Fd6;->v:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iget-object p0, v0, LX/Fd6;->o:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p0}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->setSegmentedDivider(Landroid/graphics/drawable/Drawable;)V

    .line 2263079
    goto :goto_0
.end method
