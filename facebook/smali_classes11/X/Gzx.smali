.class public final LX/Gzx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<+",
        "Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLInterfaces$PageCTAGetQuoteFormBuilderQuery;",
        ">;",
        "Lcom/facebook/messaging/professionalservices/getquote/model/FormData;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Gzy;


# direct methods
.method public constructor <init>(LX/Gzy;)V
    .locals 0

    .prologue
    .line 2412630
    iput-object p1, p0, LX/Gzx;->a:LX/Gzy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2412631
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v3, 0x0

    .line 2412632
    if-eqz p1, :cond_0

    .line 2412633
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2412634
    if-nez v0, :cond_1

    :cond_0
    move-object v0, v3

    .line 2412635
    :goto_0
    return-object v0

    .line 2412636
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2412637
    check-cast v0, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel;->c()Ljava/lang/String;

    move-result-object v1

    .line 2412638
    iget-object v0, p0, LX/Gzx;->a:LX/Gzy;

    const/4 v6, 0x0

    .line 2412639
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2412640
    invoke-static {p1}, LX/Gzy;->f(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel;

    move-result-object v2

    .line 2412641
    invoke-static {p1}, LX/Gzy;->e(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel;

    move-result-object v9

    .line 2412642
    invoke-virtual {v2}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel;->a()LX/0Px;

    move-result-object v10

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    move v7, v6

    :goto_1
    if-ge v7, v11, :cond_6

    invoke-virtual {v10, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel;

    .line 2412643
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->FULL_NAME:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    invoke-virtual {v2}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2412644
    sget-object v4, LX/H01;->a:LX/0Ri;

    invoke-virtual {v2}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    move-result-object v5

    invoke-interface {v4, v5}, LX/0Ri;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v5, v6

    .line 2412645
    :goto_2
    if-eqz v9, :cond_8

    invoke-virtual {v9}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel;->d()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_8

    .line 2412646
    invoke-virtual {v9}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel;->d()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    invoke-virtual {v2}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    move-result-object v12

    invoke-virtual {v4, v12}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2412647
    const/4 v4, 0x1

    move v5, v4

    .line 2412648
    :goto_3
    invoke-virtual {v2}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel;->b()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    .line 2412649
    iget-object v4, v0, LX/Gzy;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/03V;

    const-string v12, "GetQuoteFetcher"

    const-string v13, "Null AllUserInfoFields label data"

    invoke-virtual {v4, v12, v13}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2412650
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel;->b()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_5

    const-string v4, ""

    .line 2412651
    :goto_4
    new-instance v12, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;

    sget-object v13, LX/H01;->a:LX/0Ri;

    invoke-virtual {v2}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel;->a()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    move-result-object v2

    invoke-interface {v13, v2}, LX/0Ri;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    invoke-direct {v12, v4, v2, v5}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;-><init>(Ljava/lang/String;Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;Z)V

    invoke-interface {v8, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2412652
    :cond_3
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_1

    .line 2412653
    :cond_4
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_2

    .line 2412654
    :cond_5
    invoke-virtual {v2}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel$AllUserInfoFieldsModel;->b()Ljava/lang/String;

    move-result-object v4

    goto :goto_4

    .line 2412655
    :cond_6
    move-object v4, v8

    .line 2412656
    iget-object v0, p0, LX/Gzx;->a:LX/Gzy;

    .line 2412657
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2412658
    invoke-static {p1}, LX/Gzy;->f(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel;

    move-result-object v2

    .line 2412659
    invoke-static {p1}, LX/Gzy;->e(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel;

    move-result-object v5

    .line 2412660
    invoke-virtual {v2}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$ComponentFlowAppsModel$ConfigInfoModel;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    move-object v2, v6

    .line 2412661
    :goto_5
    move-object v5, v2

    .line 2412662
    invoke-static {p1}, LX/Gzy;->e(Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel;

    move-result-object v0

    .line 2412663
    if-eqz v0, :cond_7

    .line 2412664
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 2412665
    invoke-virtual {v0}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 2412666
    :goto_6
    new-instance v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    goto/16 :goto_0

    :cond_7
    move-object v2, v3

    goto :goto_6

    :cond_8
    move v5, v6

    goto :goto_3

    .line 2412667
    :cond_9
    if-eqz v5, :cond_d

    .line 2412668
    invoke-virtual {v5}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel;->a()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    .line 2412669
    const/4 v2, 0x0

    move v7, v2

    :goto_7
    if-ge v7, v9, :cond_d

    invoke-virtual {v8, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel$CustomizedQuestionsModel;

    .line 2412670
    sget-object v5, LX/H01;->b:LX/0Ri;

    invoke-virtual {v2}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel$CustomizedQuestionsModel;->a()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    move-result-object v10

    invoke-interface {v5, v10}, LX/0Ri;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 2412671
    invoke-virtual {v2}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel$CustomizedQuestionsModel;->b()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_a

    .line 2412672
    iget-object v5, v0, LX/Gzy;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/03V;

    const-string v10, "GetQuoteFetcher"

    const-string v11, "Null form question label data"

    invoke-virtual {v5, v10, v11}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2412673
    :cond_a
    invoke-virtual {v2}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel$CustomizedQuestionsModel;->b()Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_c

    const-string v5, ""

    .line 2412674
    :goto_8
    new-instance v10, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;

    sget-object v11, LX/H01;->b:LX/0Ri;

    invoke-virtual {v2}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel$CustomizedQuestionsModel;->a()Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    move-result-object v2

    invoke-interface {v11, v2}, LX/0Ri;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    invoke-direct {v10, v5, v2}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;-><init>(Ljava/lang/String;Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;)V

    invoke-interface {v6, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2412675
    :cond_b
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_7

    .line 2412676
    :cond_c
    invoke-virtual {v2}, Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteFormBuilderQueryModel$PageCallToActionModel$ComponentFlowServiceConfigModel$SpecificFlowConfigModel$DataModel$CustomizedQuestionsModel;->b()Ljava/lang/String;

    move-result-object v5

    goto :goto_8

    .line 2412677
    :cond_d
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 2412678
    new-instance v2, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;

    const-string v5, ""

    sget-object v7, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;->TEXT:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    invoke-direct {v2, v5, v7}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;-><init>(Ljava/lang/String;Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;)V

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_e
    move-object v2, v6

    .line 2412679
    goto/16 :goto_5
.end method
