.class public LX/Fhs;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/widget/ProgressBar;",
            ">;"
        }
    .end annotation
.end field

.field public b:Landroid/view/View;

.field public c:LX/0g8;


# direct methods
.method public constructor <init>(LX/0zw;LX/0g8;Landroid/view/View;)V
    .locals 0
    .param p1    # LX/0zw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0g8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zw",
            "<",
            "Landroid/widget/ProgressBar;",
            ">;",
            "LX/0g8;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2273845
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2273846
    iput-object p1, p0, LX/Fhs;->a:LX/0zw;

    .line 2273847
    iput-object p2, p0, LX/Fhs;->c:LX/0g8;

    .line 2273848
    iput-object p3, p0, LX/Fhs;->b:Landroid/view/View;

    .line 2273849
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2273842
    iput-object v0, p0, LX/Fhs;->a:LX/0zw;

    .line 2273843
    iput-object v0, p0, LX/Fhs;->c:LX/0g8;

    .line 2273844
    return-void
.end method

.method public final a(LX/7BE;Z)V
    .locals 4

    .prologue
    .line 2273850
    sget-object v0, LX/Fhr;->a:[I

    invoke-virtual {p1}, LX/7BE;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2273851
    invoke-virtual {p0}, LX/Fhs;->a()V

    .line 2273852
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 2273853
    :pswitch_0
    iget-object v0, p0, LX/Fhs;->a:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2273854
    iget-object v0, p0, LX/Fhs;->c:LX/0g8;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, LX/0g8;->a(I)V

    .line 2273855
    :goto_0
    return-void

    .line 2273856
    :pswitch_1
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2273857
    iget-object v0, p0, LX/Fhs;->c:LX/0g8;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fhs;->a:LX/0zw;

    if-nez v0, :cond_1

    .line 2273858
    :cond_0
    :goto_1
    goto :goto_0

    .line 2273859
    :cond_1
    iget-object v0, p0, LX/Fhs;->b:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 2273860
    iget-object v3, p0, LX/Fhs;->b:Landroid/view/View;

    if-eqz p2, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 2273861
    iget-object v0, p0, LX/Fhs;->c:LX/0g8;

    if-eqz p2, :cond_2

    move v1, v2

    :cond_2
    invoke-interface {v0, v1}, LX/0g8;->a(I)V

    .line 2273862
    :goto_3
    sget-object v0, LX/7BE;->READY:LX/7BE;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, LX/Fhs;->a:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2273863
    iget-object v0, p0, LX/Fhs;->a:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1

    :cond_3
    move v0, v2

    .line 2273864
    goto :goto_2

    .line 2273865
    :cond_4
    iget-object v0, p0, LX/Fhs;->c:LX/0g8;

    invoke-interface {v0, v1}, LX/0g8;->a(I)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
