.class public final LX/GhT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/GiR;

.field public final b:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
    .end annotation
.end field

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Ljava/lang/Object;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/GiR;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2384189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2384190
    iput-object p1, p0, LX/GhT;->a:LX/GiR;

    .line 2384191
    iput-object p2, p0, LX/GhT;->b:Ljava/lang/String;

    .line 2384192
    iput-object p3, p0, LX/GhT;->c:Ljava/lang/String;

    .line 2384193
    iput-object v0, p0, LX/GhT;->d:Ljava/lang/String;

    .line 2384194
    iput-object v0, p0, LX/GhT;->e:Ljava/lang/String;

    .line 2384195
    iput-object v0, p0, LX/GhT;->f:Ljava/lang/String;

    .line 2384196
    iput-object v0, p0, LX/GhT;->g:Ljava/lang/Object;

    .line 2384197
    return-void
.end method

.method public constructor <init>(LX/GiR;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p2    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2384198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2384199
    iput-object p1, p0, LX/GhT;->a:LX/GiR;

    .line 2384200
    iput-object p2, p0, LX/GhT;->b:Ljava/lang/String;

    .line 2384201
    iput-object p3, p0, LX/GhT;->c:Ljava/lang/String;

    .line 2384202
    iput-object p4, p0, LX/GhT;->d:Ljava/lang/String;

    .line 2384203
    iput-object p5, p0, LX/GhT;->e:Ljava/lang/String;

    .line 2384204
    iput-object p6, p0, LX/GhT;->f:Ljava/lang/String;

    .line 2384205
    const/4 v0, 0x0

    iput-object v0, p0, LX/GhT;->g:Ljava/lang/Object;

    .line 2384206
    return-void
.end method
