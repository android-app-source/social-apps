.class public final LX/GTj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V
    .locals 0

    .prologue
    .line 2355787
    iput-object p1, p0, LX/GTj;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x3991261d

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2355788
    iget-object v1, p0, LX/GTj;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v2, p0, LX/GTj;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v2, v2, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->K:Lcom/facebook/fbui/popover/PopoverSpinner;

    invoke-virtual {v2}, Lcom/facebook/fbui/popover/PopoverSpinner;->getSelectedItemPosition()I

    move-result v2

    invoke-static {v1, v2}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->b(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;I)Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    move-result-object v1

    .line 2355789
    if-nez v1, :cond_0

    .line 2355790
    iget-object v1, p0, LX/GTj;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->w:LX/03V;

    const-string v2, "background_location_one_page_nux_select_privacy_failure"

    const-string v3, "selected privacy option is empty"

    invoke-virtual {v1, v2, v3}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2355791
    const v1, 0x2067dbe1

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2355792
    :goto_0
    return-void

    .line 2355793
    :cond_0
    iget-object v2, p0, LX/GTj;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    .line 2355794
    invoke-static {v2, v1}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->a$redex0(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)V

    .line 2355795
    iget-object v2, p0, LX/GTj;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v2, v2, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->s:LX/GTt;

    .line 2355796
    const-string v3, "informational"

    iget-object v4, v2, LX/GTt;->e:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "nearby_friends_informational_ok"

    .line 2355797
    :goto_1
    iget-object v4, v2, LX/GTt;->a:LX/0Zb;

    invoke-static {v2, v3}, LX/GTt;->a(LX/GTt;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p0, "privacy_type"

    invoke-virtual {v1}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;->b()LX/1Fd;

    move-result-object p1

    invoke-interface {p1}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2355798
    const v1, -0x6d16f4e7

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 2355799
    :cond_1
    const-string v3, "nearby_friends_now_nux_turn_on"

    goto :goto_1
.end method
