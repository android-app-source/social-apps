.class public LX/FLA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2226159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2226160
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 3

    .prologue
    .line 2226139
    check-cast p1, Ljava/lang/String;

    .line 2226140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2226141
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "audience_mode"

    invoke-direct {v1, v2, p1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2226142
    new-instance v1, LX/14O;

    invoke-direct {v1}, LX/14O;-><init>()V

    const-string v2, "updateMontageAudienceMode"

    .line 2226143
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2226144
    move-object v1, v1

    .line 2226145
    const-string v2, "POST"

    .line 2226146
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2226147
    move-object v1, v1

    .line 2226148
    const-string v2, "me/montage_preferences"

    .line 2226149
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2226150
    move-object v1, v1

    .line 2226151
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2226152
    move-object v0, v1

    .line 2226153
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    sget-object v1, LX/14S;->STRING:LX/14S;

    .line 2226154
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2226155
    move-object v0, v0

    .line 2226156
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2226157
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2226158
    const/4 v0, 0x0

    return-object v0
.end method
