.class public LX/Grc;
.super LX/Cts;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/widget/ImageView;

.field private final b:Landroid/view/Display;

.field private final c:Landroid/graphics/Point;

.field private d:LX/Grb;

.field private e:LX/Ctg;


# direct methods
.method public constructor <init>(LX/Ctg;)V
    .locals 2

    .prologue
    .line 2398447
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 2398448
    iput-object p1, p0, LX/Grc;->e:LX/Ctg;

    .line 2398449
    iget-object v0, p0, LX/Grc;->e:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->a()Landroid/view/ViewGroup;

    move-result-object v0

    const v1, 0x7f0d182a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/Grc;->a:Landroid/widget/ImageView;

    .line 2398450
    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 2398451
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, LX/Grc;->b:Landroid/view/Display;

    .line 2398452
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, LX/Grc;->c:Landroid/graphics/Point;

    .line 2398453
    iget-object v0, p0, LX/Grc;->b:Landroid/view/Display;

    iget-object v1, p0, LX/Grc;->c:Landroid/graphics/Point;

    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2398454
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2398443
    iget-object v0, p0, LX/Grc;->a:Landroid/widget/ImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 2398444
    iget-object v0, p0, LX/Grc;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 2398445
    iget-object v0, p0, LX/Grc;->a:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2398446
    return-void
.end method

.method public final a(LX/Cqw;)V
    .locals 2

    .prologue
    .line 2398437
    iget-object v0, p0, LX/Grc;->a:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Grc;->d:LX/Grb;

    sget-object v1, LX/Grb;->HIDDEN:LX/Grb;

    invoke-virtual {v0, v1}, LX/Grb;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2398438
    :cond_0
    :goto_0
    return-void

    .line 2398439
    :cond_1
    sget-object v0, LX/Cqw;->a:LX/Cqw;

    invoke-virtual {v0, p1}, LX/Cqw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2398440
    sget-object v0, LX/Grb;->EXPANDABLE:LX/Grb;

    invoke-virtual {p0, v0}, LX/Grc;->a(LX/Grb;)V

    goto :goto_0

    .line 2398441
    :cond_2
    sget-object v0, LX/Cqw;->b:LX/Cqw;

    invoke-virtual {v0, p1}, LX/Cqw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2398442
    sget-object v0, LX/Grb;->TILT_TO_PAN:LX/Grb;

    invoke-virtual {p0, v0}, LX/Grc;->a(LX/Grb;)V

    goto :goto_0
.end method

.method public final a(LX/CrS;)V
    .locals 5

    .prologue
    .line 2398430
    iget-object v0, p0, LX/Grc;->a:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 2398431
    :goto_0
    return-void

    .line 2398432
    :cond_0
    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x41000000    # 8.0f

    invoke-static {v0, v1}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    .line 2398433
    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v1

    const/high16 v2, 0x41200000    # 10.0f

    invoke-static {v1, v2}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    .line 2398434
    iget-object v2, p0, LX/Grc;->c:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget-object v3, p0, LX/Grc;->a:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v3

    sub-int/2addr v2, v3

    sub-int v1, v2, v1

    .line 2398435
    new-instance v2, Landroid/graphics/Rect;

    iget-object v3, p0, LX/Grc;->a:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, LX/Grc;->a:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-direct {v2, v1, v0, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2398436
    iget-object v0, p0, LX/Grc;->e:LX/Ctg;

    iget-object v1, p0, LX/Grc;->a:Landroid/widget/ImageView;

    invoke-interface {v0, v1, v2}, LX/Ctg;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method public final a(LX/Grb;)V
    .locals 6

    .prologue
    .line 2398414
    iget-object v0, p0, LX/Grc;->a:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 2398415
    :goto_0
    return-void

    .line 2398416
    :cond_0
    sget-object v0, LX/Gra;->a:[I

    invoke-virtual {p1}, LX/Grb;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2398417
    invoke-virtual {p0}, LX/Grc;->a()V

    goto :goto_0

    .line 2398418
    :pswitch_0
    iget-object v0, p0, LX/Grc;->a:Landroid/widget/ImageView;

    const v1, 0x7f020dd1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2398419
    sget-object v0, LX/Grb;->TILT_TO_PAN:LX/Grb;

    iput-object v0, p0, LX/Grc;->d:LX/Grb;

    .line 2398420
    :goto_1
    iget-object v2, p0, LX/Grc;->a:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 2398421
    iget-object v2, p0, LX/Grc;->a:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2398422
    iget-object v2, p0, LX/Grc;->a:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 2398423
    goto :goto_0

    .line 2398424
    :pswitch_1
    iget-object v0, p0, LX/Grc;->a:Landroid/widget/ImageView;

    const v1, 0x7f020dce

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2398425
    sget-object v0, LX/Grb;->EXPANDABLE:LX/Grb;

    iput-object v0, p0, LX/Grc;->d:LX/Grb;

    goto :goto_1

    .line 2398426
    :pswitch_2
    iget-object v0, p0, LX/Grc;->a:Landroid/widget/ImageView;

    const v1, 0x7f020dd0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2398427
    sget-object v0, LX/Grb;->LINK:LX/Grb;

    iput-object v0, p0, LX/Grc;->d:LX/Grb;

    goto :goto_1

    .line 2398428
    :pswitch_3
    invoke-virtual {p0}, LX/Grc;->a()V

    .line 2398429
    sget-object v0, LX/Grb;->HIDDEN:LX/Grb;

    iput-object v0, p0, LX/Grc;->d:LX/Grb;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
