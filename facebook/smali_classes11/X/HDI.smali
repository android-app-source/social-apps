.class public LX/HDI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPageActionType;",
            "LX/HDF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/HDG;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2441087
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2441088
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLPageActionType;->TAB_ABOUT:Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-object v0, v0

    .line 2441089
    invoke-static {v0, p1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v0

    iput-object v0, p0, LX/HDI;->a:LX/0P1;

    .line 2441090
    return-void
.end method

.method public static a(LX/0QB;)LX/HDI;
    .locals 5

    .prologue
    .line 2441091
    const-class v1, LX/HDI;

    monitor-enter v1

    .line 2441092
    :try_start_0
    sget-object v0, LX/HDI;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2441093
    sput-object v2, LX/HDI;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2441094
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2441095
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2441096
    new-instance v4, LX/HDI;

    .line 2441097
    new-instance p0, LX/HDG;

    invoke-static {v0}, LX/HC0;->b(LX/0QB;)LX/HC0;

    move-result-object v3

    check-cast v3, LX/HC0;

    invoke-direct {p0, v3}, LX/HDG;-><init>(LX/HC0;)V

    .line 2441098
    move-object v3, p0

    .line 2441099
    check-cast v3, LX/HDG;

    invoke-direct {v4, v3}, LX/HDI;-><init>(LX/HDG;)V

    .line 2441100
    move-object v0, v4

    .line 2441101
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2441102
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/HDI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2441103
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2441104
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
