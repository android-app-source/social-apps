.class public final LX/Fab;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$UserSearchAwarenessSuggestionSubscriptionModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Fac;


# direct methods
.method public constructor <init>(LX/Fac;)V
    .locals 0

    .prologue
    .line 2258953
    iput-object p1, p0, LX/Fab;->a:LX/Fac;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2258941
    sget-object v0, LX/Fac;->a:Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2258942
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2258943
    check-cast p1, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$UserSearchAwarenessSuggestionSubscriptionModel;

    .line 2258944
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$UserSearchAwarenessSuggestionSubscriptionModel;->a()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2258945
    :cond_0
    :goto_0
    return-void

    .line 2258946
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$UserSearchAwarenessSuggestionSubscriptionModel;->a()Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;

    move-result-object v0

    .line 2258947
    iget-object v1, p0, LX/Fab;->a:LX/Fac;

    iget-object v1, v1, LX/Fac;->d:LX/0x9;

    invoke-virtual {v1, v0}, LX/0x9;->a(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2258948
    iget-object v1, p0, LX/Fab;->a:LX/Fac;

    iget-object v1, v1, LX/Fac;->d:LX/0x9;

    invoke-virtual {v1, v0}, LX/0x9;->b(Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;)V

    .line 2258949
    sget-object v1, LX/Faa;->a:[I

    invoke-virtual {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->e()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2258950
    sget-object v0, LX/Fac;->a:Ljava/lang/Class;

    const-string v1, "Unsupported suggestion template"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    goto :goto_0

    .line 2258951
    :pswitch_0
    iget-object v1, p0, LX/Fab;->a:LX/Fac;

    iget-object v1, v1, LX/Fac;->d:LX/0x9;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->e()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0x9;->a(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2258952
    iget-object v1, p0, LX/Fab;->a:LX/Fac;

    iget-object v1, v1, LX/Fac;->d:LX/0x9;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$SearchAwarenessRootSuggestionFieldsFragmentModel;->e()Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0x9;->b(Lcom/facebook/graphql/enums/GraphQLSearchAwarenessTemplatesEnum;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
