.class public LX/GaP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# instance fields
.field public a:Landroid/os/CountDownTimer;

.field public b:J

.field public c:J

.field public d:LX/GAx;


# direct methods
.method public constructor <init>(Ljava/lang/Long;Ljava/lang/Long;)V
    .locals 2
    .param p1    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2368248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2368249
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, LX/GaP;->b:J

    .line 2368250
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, LX/GaP;->c:J

    .line 2368251
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 6

    .prologue
    .line 2368252
    invoke-virtual {p0}, LX/GaP;->d()V

    .line 2368253
    new-instance v0, LX/GaO;

    iget-wide v2, p0, LX/GaP;->b:J

    iget-wide v4, p0, LX/GaP;->c:J

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, LX/GaO;-><init>(LX/GaP;JJ)V

    invoke-virtual {v0}, LX/GaO;->start()Landroid/os/CountDownTimer;

    move-result-object v0

    iput-object v0, p0, LX/GaP;->a:Landroid/os/CountDownTimer;

    .line 2368254
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2368255
    iget-object v0, p0, LX/GaP;->a:Landroid/os/CountDownTimer;

    if-eqz v0, :cond_0

    .line 2368256
    iget-object v0, p0, LX/GaP;->a:Landroid/os/CountDownTimer;

    invoke-virtual {v0}, Landroid/os/CountDownTimer;->cancel()V

    .line 2368257
    const/4 v0, 0x0

    iput-object v0, p0, LX/GaP;->a:Landroid/os/CountDownTimer;

    .line 2368258
    :cond_0
    return-void
.end method
