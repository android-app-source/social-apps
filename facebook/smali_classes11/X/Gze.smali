.class public LX/Gze;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method private constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2412194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2412195
    iput-object p1, p0, LX/Gze;->a:LX/0Zb;

    .line 2412196
    return-void
.end method

.method public static a(LX/0QB;)LX/Gze;
    .locals 1

    .prologue
    .line 2412193
    invoke-static {p0}, LX/Gze;->b(LX/0QB;)LX/Gze;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 2412189
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "pages_public_view"

    .line 2412190
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2412191
    move-object v0, v0

    .line 2412192
    const-string v1, "page_id"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/Gze;
    .locals 2

    .prologue
    .line 2412173
    new-instance v1, LX/Gze;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/Gze;-><init>(LX/0Zb;)V

    .line 2412174
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 2412185
    if-eqz p2, :cond_0

    .line 2412186
    iget-object v0, p0, LX/Gze;->a:LX/0Zb;

    const-string v1, "get_quote_cta_admin_enter_form_builder"

    invoke-static {v1, p1}, LX/Gze;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "refer"

    sget-object v3, LX/Gzc;->EDIT:LX/Gzc;

    invoke-virtual {v3}, LX/Gzc;->getLogValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2412187
    :goto_0
    return-void

    .line 2412188
    :cond_0
    iget-object v0, p0, LX/Gze;->a:LX/0Zb;

    const-string v1, "get_quote_cta_admin_enter_form_builder"

    invoke-static {v1, p1}, LX/Gze;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "refer"

    sget-object v3, LX/Gzc;->CREATE:LX/Gzc;

    invoke-virtual {v3}, LX/Gzc;->getLogValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2412183
    iget-object v0, p0, LX/Gze;->a:LX/0Zb;

    const-string v1, "get_quote_cta_admin_tap_save"

    invoke-static {v1, p1}, LX/Gze;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2412184
    return-void
.end method

.method public final b(Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 2412179
    if-eqz p2, :cond_0

    .line 2412180
    iget-object v0, p0, LX/Gze;->a:LX/0Zb;

    const-string v1, "get_quote_cta_admin_tap_edit_form"

    invoke-static {v1, p1}, LX/Gze;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "refer"

    sget-object v3, LX/Gzc;->EDIT:LX/Gzc;

    invoke-virtual {v3}, LX/Gzc;->getLogValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2412181
    :goto_0
    return-void

    .line 2412182
    :cond_0
    iget-object v0, p0, LX/Gze;->a:LX/0Zb;

    const-string v1, "get_quote_cta_admin_tap_edit_form"

    invoke-static {v1, p1}, LX/Gze;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "refer"

    sget-object v3, LX/Gzc;->CREATE:LX/Gzc;

    invoke-virtual {v3}, LX/Gzc;->getLogValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2412177
    iget-object v0, p0, LX/Gze;->a:LX/0Zb;

    const-string v1, "get_quote_cta_admin_save_success"

    invoke-static {v1, p1}, LX/Gze;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2412178
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2412175
    iget-object v0, p0, LX/Gze;->a:LX/0Zb;

    const-string v1, "get_quote_cta_admin_save_failure"

    invoke-static {v1, p1}, LX/Gze;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2412176
    return-void
.end method
