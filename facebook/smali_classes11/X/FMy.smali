.class public LX/FMy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0TD;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Mb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/FMu;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0V8;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2231754
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2231755
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2231756
    iput-object v0, p0, LX/FMy;->a:LX/0Ot;

    .line 2231757
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2231758
    iput-object v0, p0, LX/FMy;->b:LX/0Ot;

    .line 2231759
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2231760
    iput-object v0, p0, LX/FMy;->c:LX/0Ot;

    .line 2231761
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2231762
    iput-object v0, p0, LX/FMy;->e:LX/0Ot;

    .line 2231763
    return-void
.end method

.method private static b(LX/FMy;Lcom/facebook/ui/media/attachments/MediaResource;I)Z
    .locals 11

    .prologue
    .line 2231730
    iget-object v0, p0, LX/FMy;->d:LX/FMu;

    invoke-virtual {v0, p2}, LX/FMu;->b(I)I

    move-result v0

    int-to-long v2, v0

    .line 2231731
    iget-object v0, p0, LX/FMy;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Mb;

    invoke-virtual {v0, p1}, LX/2Mb;->a(Lcom/facebook/ui/media/attachments/MediaResource;)I

    move-result v0

    int-to-long v0, v0

    .line 2231732
    cmp-long v2, v0, v2

    if-gez v2, :cond_3

    const/4 v5, 0x0

    const-wide/16 v8, 0x0

    .line 2231733
    cmp-long v4, v0, v8

    if-lez v4, :cond_2

    iget-object v4, p0, LX/FMy;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0V8;

    sget-object v6, LX/0VA;->EXTERNAL:LX/0VA;

    invoke-virtual {v4, v6}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v6

    cmp-long v4, v6, v8

    if-gtz v4, :cond_0

    iget-object v4, p0, LX/FMy;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0V8;

    sget-object v6, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v4, v6}, LX/0V8;->c(LX/0VA;)J

    move-result-wide v6

    cmp-long v4, v6, v8

    if-lez v4, :cond_2

    .line 2231734
    :cond_0
    iget-object v4, p0, LX/FMy;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0V8;

    sget-object v6, LX/0VA;->EXTERNAL:LX/0VA;

    invoke-virtual {v4, v6, v0, v1}, LX/0V8;->a(LX/0VA;J)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, LX/FMy;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0V8;

    sget-object v6, LX/0VA;->INTERNAL:LX/0VA;

    invoke-virtual {v4, v6, v0, v1}, LX/0V8;->a(LX/0VA;J)Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_1
    const/4 v4, 0x1

    :goto_0
    move v5, v4

    .line 2231735
    :cond_2
    move v0, v5

    .line 2231736
    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    move v4, v5

    .line 2231737
    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/ui/media/attachments/MediaResource;I)Landroid/net/Uri;
    .locals 8

    .prologue
    .line 2231738
    invoke-static {}, Lcom/facebook/ui/media/attachments/MediaResource;->a()LX/5zn;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    move-result-object v0

    sget-object v1, LX/5zj;->VIDEO_MMS:LX/5zj;

    .line 2231739
    iput-object v1, v0, LX/5zn;->d:LX/5zj;

    .line 2231740
    move-object v0, v0

    .line 2231741
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 2231742
    invoke-static {p0, v0, p2}, LX/FMy;->b(LX/FMy;Lcom/facebook/ui/media/attachments/MediaResource;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2231743
    const-wide/16 v2, 0x7530

    .line 2231744
    :try_start_0
    iget-object v4, p0, LX/FMy;->b:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0TD;

    new-instance v5, Lcom/facebook/messaging/sms/defaultapp/send/MmsVideoAttachmentHelper$1;

    invoke-direct {v5, p0, v0}, Lcom/facebook/messaging/sms/defaultapp/send/MmsVideoAttachmentHelper$1;-><init>(LX/FMy;Lcom/facebook/ui/media/attachments/MediaResource;)V

    invoke-interface {v4, v5}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2231745
    sget-object v5, LX/0Vg;->a:LX/0Vj;

    invoke-static {v4, v5}, LX/0Vg;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/0Vj;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v4, v5

    .line 2231746
    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    const v6, -0x2ff00907

    invoke-static {v4, v2, v3, v5, v6}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/fbservice/service/OperationResult;

    .line 2231747
    invoke-virtual {v4}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2231748
    if-eqz v4, :cond_1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2231749
    :goto_0
    move-object v0, v4

    .line 2231750
    :cond_0
    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->c:Landroid/net/Uri;

    return-object v0

    .line 2231751
    :catch_0
    move-exception v4

    .line 2231752
    const-string v5, "MmsVideoAttachmentHelper"

    const-string v6, "Failed in compressing video resource for mms"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v5, v4, v6, v7}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_1
    move-object v4, v0

    .line 2231753
    goto :goto_0
.end method
