.class public final enum LX/FPf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FPf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FPf;

.field public static final enum HAS_DELIVERY:LX/FPf;

.field public static final enum HAS_PARKING:LX/FPf;

.field public static final enum HAS_TAKEOUT:LX/FPf;

.field public static final enum HAS_WIFI:LX/FPf;

.field public static final enum OUTDOOR_SEATING:LX/FPf;

.field public static final enum POPULAR_WITH_GROUPS:LX/FPf;

.field public static final enum TAKES_CREDIT_CARDS:LX/FPf;

.field public static final enum TAKES_RESERVATIONS:LX/FPf;

.field public static final enum VISITED_BY_FRIENDS:LX/FPf;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2237741
    new-instance v0, LX/FPf;

    const-string v1, "HAS_DELIVERY"

    invoke-direct {v0, v1, v3}, LX/FPf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPf;->HAS_DELIVERY:LX/FPf;

    .line 2237742
    new-instance v0, LX/FPf;

    const-string v1, "HAS_PARKING"

    invoke-direct {v0, v1, v4}, LX/FPf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPf;->HAS_PARKING:LX/FPf;

    .line 2237743
    new-instance v0, LX/FPf;

    const-string v1, "HAS_TAKEOUT"

    invoke-direct {v0, v1, v5}, LX/FPf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPf;->HAS_TAKEOUT:LX/FPf;

    .line 2237744
    new-instance v0, LX/FPf;

    const-string v1, "HAS_WIFI"

    invoke-direct {v0, v1, v6}, LX/FPf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPf;->HAS_WIFI:LX/FPf;

    .line 2237745
    new-instance v0, LX/FPf;

    const-string v1, "OUTDOOR_SEATING"

    invoke-direct {v0, v1, v7}, LX/FPf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPf;->OUTDOOR_SEATING:LX/FPf;

    .line 2237746
    new-instance v0, LX/FPf;

    const-string v1, "POPULAR_WITH_GROUPS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/FPf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPf;->POPULAR_WITH_GROUPS:LX/FPf;

    .line 2237747
    new-instance v0, LX/FPf;

    const-string v1, "TAKES_CREDIT_CARDS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/FPf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPf;->TAKES_CREDIT_CARDS:LX/FPf;

    .line 2237748
    new-instance v0, LX/FPf;

    const-string v1, "TAKES_RESERVATIONS"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/FPf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPf;->TAKES_RESERVATIONS:LX/FPf;

    .line 2237749
    new-instance v0, LX/FPf;

    const-string v1, "VISITED_BY_FRIENDS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/FPf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FPf;->VISITED_BY_FRIENDS:LX/FPf;

    .line 2237750
    const/16 v0, 0x9

    new-array v0, v0, [LX/FPf;

    sget-object v1, LX/FPf;->HAS_DELIVERY:LX/FPf;

    aput-object v1, v0, v3

    sget-object v1, LX/FPf;->HAS_PARKING:LX/FPf;

    aput-object v1, v0, v4

    sget-object v1, LX/FPf;->HAS_TAKEOUT:LX/FPf;

    aput-object v1, v0, v5

    sget-object v1, LX/FPf;->HAS_WIFI:LX/FPf;

    aput-object v1, v0, v6

    sget-object v1, LX/FPf;->OUTDOOR_SEATING:LX/FPf;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/FPf;->POPULAR_WITH_GROUPS:LX/FPf;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/FPf;->TAKES_CREDIT_CARDS:LX/FPf;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/FPf;->TAKES_RESERVATIONS:LX/FPf;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/FPf;->VISITED_BY_FRIENDS:LX/FPf;

    aput-object v2, v0, v1

    sput-object v0, LX/FPf;->$VALUES:[LX/FPf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2237751
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FPf;
    .locals 1

    .prologue
    .line 2237752
    const-class v0, LX/FPf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FPf;

    return-object v0
.end method

.method public static values()[LX/FPf;
    .locals 1

    .prologue
    .line 2237753
    sget-object v0, LX/FPf;->$VALUES:[LX/FPf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FPf;

    return-object v0
.end method
