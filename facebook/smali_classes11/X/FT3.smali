.class public final LX/FT3;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public A:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public B:Lcom/facebook/graphql/enums/GraphQLSavedState;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Z

.field public m:Z

.field public n:Z

.field public o:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfilePictureModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public u:Z

.field public v:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public w:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public y:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public z:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2242709
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;
    .locals 21

    .prologue
    .line 2242654
    new-instance v1, LX/186;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    .line 2242655
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FT3;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    .line 2242656
    move-object/from16 v0, p0

    iget-object v3, v0, LX/FT3;->h:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$CoverPhotoModel;

    invoke-static {v1, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2242657
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FT3;->i:Lcom/facebook/timeline/widget/actionbar/protocol/TimelineHeaderActionFieldsGraphQLModels$NonSelfActionFieldsModel$FriendsModel;

    invoke-static {v1, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2242658
    move-object/from16 v0, p0

    iget-object v5, v0, LX/FT3;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v1, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 2242659
    move-object/from16 v0, p0

    iget-object v6, v0, LX/FT3;->k:Ljava/lang/String;

    invoke-virtual {v1, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2242660
    move-object/from16 v0, p0

    iget-object v7, v0, LX/FT3;->o:Ljava/lang/String;

    invoke-virtual {v1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    .line 2242661
    move-object/from16 v0, p0

    iget-object v8, v0, LX/FT3;->p:Lcom/facebook/ipc/composer/intent/graphql/FetchComposerTargetDataPrivacyScopeModels$ComposerTargetDataPrivacyScopeFieldsModel;

    invoke-static {v1, v8}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v8

    .line 2242662
    move-object/from16 v0, p0

    iget-object v9, v0, LX/FT3;->q:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-static {v1, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2242663
    move-object/from16 v0, p0

    iget-object v10, v0, LX/FT3;->r:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfileIntroCardModel;

    invoke-static {v1, v10}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v10

    .line 2242664
    move-object/from16 v0, p0

    iget-object v11, v0, LX/FT3;->s:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryClickablePhotoFieldsModel;

    invoke-static {v1, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 2242665
    move-object/from16 v0, p0

    iget-object v12, v0, LX/FT3;->t:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel$ProfilePictureModel;

    invoke-static {v1, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 2242666
    move-object/from16 v0, p0

    iget-object v13, v0, LX/FT3;->v:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryCardHeaderFieldsModel$ProfileVideoModel;

    invoke-static {v1, v13}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v13

    .line 2242667
    move-object/from16 v0, p0

    iget-object v14, v0, LX/FT3;->w:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    invoke-virtual {v1, v14}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v14

    .line 2242668
    move-object/from16 v0, p0

    iget-object v15, v0, LX/FT3;->x:Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultNameFieldsModel;

    invoke-static {v1, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 2242669
    move-object/from16 v0, p0

    iget-object v0, v0, LX/FT3;->y:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v16

    .line 2242670
    move-object/from16 v0, p0

    iget-object v0, v0, LX/FT3;->z:Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$DiscoveryMutualityFieldsModel$TimelineContextItemsModel;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v1, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 2242671
    move-object/from16 v0, p0

    iget-object v0, v0, LX/FT3;->A:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v1, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 2242672
    move-object/from16 v0, p0

    iget-object v0, v0, LX/FT3;->B:Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v1, v0}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v19

    .line 2242673
    const/16 v20, 0x1c

    move/from16 v0, v20

    invoke-virtual {v1, v0}, LX/186;->c(I)V

    .line 2242674
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v1, v0, v2}, LX/186;->b(II)V

    .line 2242675
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/FT3;->b:Z

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-virtual {v1, v2, v0}, LX/186;->a(IZ)V

    .line 2242676
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/FT3;->c:Z

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-virtual {v1, v2, v0}, LX/186;->a(IZ)V

    .line 2242677
    const/4 v2, 0x3

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/FT3;->d:Z

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-virtual {v1, v2, v0}, LX/186;->a(IZ)V

    .line 2242678
    const/4 v2, 0x4

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/FT3;->e:Z

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-virtual {v1, v2, v0}, LX/186;->a(IZ)V

    .line 2242679
    const/4 v2, 0x5

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/FT3;->f:Z

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-virtual {v1, v2, v0}, LX/186;->a(IZ)V

    .line 2242680
    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/FT3;->g:Z

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-virtual {v1, v2, v0}, LX/186;->a(IZ)V

    .line 2242681
    const/4 v2, 0x7

    invoke-virtual {v1, v2, v3}, LX/186;->b(II)V

    .line 2242682
    const/16 v2, 0x8

    invoke-virtual {v1, v2, v4}, LX/186;->b(II)V

    .line 2242683
    const/16 v2, 0x9

    invoke-virtual {v1, v2, v5}, LX/186;->b(II)V

    .line 2242684
    const/16 v2, 0xa

    invoke-virtual {v1, v2, v6}, LX/186;->b(II)V

    .line 2242685
    const/16 v2, 0xb

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/FT3;->l:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 2242686
    const/16 v2, 0xc

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/FT3;->m:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 2242687
    const/16 v2, 0xd

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/FT3;->n:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 2242688
    const/16 v2, 0xe

    invoke-virtual {v1, v2, v7}, LX/186;->b(II)V

    .line 2242689
    const/16 v2, 0xf

    invoke-virtual {v1, v2, v8}, LX/186;->b(II)V

    .line 2242690
    const/16 v2, 0x10

    invoke-virtual {v1, v2, v9}, LX/186;->b(II)V

    .line 2242691
    const/16 v2, 0x11

    invoke-virtual {v1, v2, v10}, LX/186;->b(II)V

    .line 2242692
    const/16 v2, 0x12

    invoke-virtual {v1, v2, v11}, LX/186;->b(II)V

    .line 2242693
    const/16 v2, 0x13

    invoke-virtual {v1, v2, v12}, LX/186;->b(II)V

    .line 2242694
    const/16 v2, 0x14

    move-object/from16 v0, p0

    iget-boolean v3, v0, LX/FT3;->u:Z

    invoke-virtual {v1, v2, v3}, LX/186;->a(IZ)V

    .line 2242695
    const/16 v2, 0x15

    invoke-virtual {v1, v2, v13}, LX/186;->b(II)V

    .line 2242696
    const/16 v2, 0x16

    invoke-virtual {v1, v2, v14}, LX/186;->b(II)V

    .line 2242697
    const/16 v2, 0x17

    invoke-virtual {v1, v2, v15}, LX/186;->b(II)V

    .line 2242698
    const/16 v2, 0x18

    move/from16 v0, v16

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 2242699
    const/16 v2, 0x19

    move/from16 v0, v17

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 2242700
    const/16 v2, 0x1a

    move/from16 v0, v18

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 2242701
    const/16 v2, 0x1b

    move/from16 v0, v19

    invoke-virtual {v1, v2, v0}, LX/186;->b(II)V

    .line 2242702
    invoke-virtual {v1}, LX/186;->d()I

    move-result v2

    .line 2242703
    invoke-virtual {v1, v2}, LX/186;->d(I)V

    .line 2242704
    invoke-virtual {v1}, LX/186;->e()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 2242705
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2242706
    new-instance v1, LX/15i;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2242707
    new-instance v2, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;

    invoke-direct {v2, v1}, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$BucketItemModel$PersonModel;-><init>(LX/15i;)V

    .line 2242708
    return-object v2
.end method
