.class public final LX/Gxr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:I

.field public final synthetic c:I

.field public final synthetic d:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;III)V
    .locals 0

    .prologue
    .line 2408503
    iput-object p1, p0, LX/Gxr;->d:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iput p2, p0, LX/Gxr;->a:I

    iput p3, p0, LX/Gxr;->b:I

    iput p4, p0, LX/Gxr;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f000000    # 0.5f

    .line 2408504
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 2408505
    iget-object v0, p0, LX/Gxr;->d:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iget-object v0, v0, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 2408506
    iget v2, p0, LX/Gxr;->a:I

    int-to-float v2, v2

    mul-float/2addr v2, v1

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    .line 2408507
    iget v2, p0, LX/Gxr;->b:I

    int-to-float v2, v2

    mul-float/2addr v2, v1

    add-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 2408508
    iget v2, p0, LX/Gxr;->c:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    add-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 2408509
    iget-object v1, p0, LX/Gxr;->d:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iget-object v1, v1, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->l:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2408510
    return-void
.end method
