.class public final enum LX/GSv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GSv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GSv;

.field public static final enum FETCH_BLOCKED_APPS:LX/GSv;

.field public static final enum FETCH_BLOCKED_USERS:LX/GSv;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2354920
    new-instance v0, LX/GSv;

    const-string v1, "FETCH_BLOCKED_APPS"

    invoke-direct {v0, v1, v2}, LX/GSv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GSv;->FETCH_BLOCKED_APPS:LX/GSv;

    .line 2354921
    new-instance v0, LX/GSv;

    const-string v1, "FETCH_BLOCKED_USERS"

    invoke-direct {v0, v1, v3}, LX/GSv;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GSv;->FETCH_BLOCKED_USERS:LX/GSv;

    .line 2354922
    const/4 v0, 0x2

    new-array v0, v0, [LX/GSv;

    sget-object v1, LX/GSv;->FETCH_BLOCKED_APPS:LX/GSv;

    aput-object v1, v0, v2

    sget-object v1, LX/GSv;->FETCH_BLOCKED_USERS:LX/GSv;

    aput-object v1, v0, v3

    sput-object v0, LX/GSv;->$VALUES:[LX/GSv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2354923
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GSv;
    .locals 1

    .prologue
    .line 2354924
    const-class v0, LX/GSv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GSv;

    return-object v0
.end method

.method public static values()[LX/GSv;
    .locals 1

    .prologue
    .line 2354925
    sget-object v0, LX/GSv;->$VALUES:[LX/GSv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GSv;

    return-object v0
.end method
