.class public LX/Fbd;
.super LX/FbI;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2260759
    invoke-direct {p0}, LX/FbI;-><init>()V

    .line 2260760
    return-void
.end method

.method public static a(LX/0QB;)LX/Fbd;
    .locals 3

    .prologue
    .line 2260748
    const-class v1, LX/Fbd;

    monitor-enter v1

    .line 2260749
    :try_start_0
    sget-object v0, LX/Fbd;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2260750
    sput-object v2, LX/Fbd;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2260751
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2260752
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2260753
    new-instance v0, LX/Fbd;

    invoke-direct {v0}, LX/Fbd;-><init>()V

    .line 2260754
    move-object v0, v0

    .line 2260755
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2260756
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fbd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2260757
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2260758
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;)Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;
    .locals 11
    .param p0    # Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260635
    if-nez p0, :cond_0

    .line 2260636
    const/4 v0, 0x0

    .line 2260637
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/5C5;

    invoke-direct {v0}, LX/5C5;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;->a()LX/0Px;

    move-result-object v1

    .line 2260638
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2260639
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2260640
    :goto_1
    move-object v1, v2

    .line 2260641
    iput-object v1, v0, LX/5C5;->a:LX/0Px;

    .line 2260642
    move-object v0, v0

    .line 2260643
    invoke-virtual {v0}, LX/5C5;->a()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v0

    goto :goto_0

    .line 2260644
    :cond_1
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2260645
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_2

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;

    .line 2260646
    new-instance v6, LX/5C6;

    invoke-direct {v6}, LX/5C6;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->j()J

    move-result-wide v8

    .line 2260647
    iput-wide v8, v6, LX/5C6;->b:J

    .line 2260648
    move-object v6, v6

    .line 2260649
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->a()I

    move-result v7

    .line 2260650
    iput v7, v6, LX/5C6;->a:I

    .line 2260651
    move-object v6, v6

    .line 2260652
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideoGuidedTourKeyframe;->k()I

    move-result v7

    .line 2260653
    iput v7, v6, LX/5C6;->c:I

    .line 2260654
    move-object v6, v6

    .line 2260655
    invoke-virtual {v6}, LX/5C6;->a()Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel$KeyframesModel;

    move-result-object v6

    move-object v2, v6

    .line 2260656
    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2260657
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 2260658
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;)LX/8dV;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2260659
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 2260660
    if-nez v1, :cond_1

    .line 2260661
    :cond_0
    :goto_0
    return-object v0

    .line 2260662
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->bG()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 2260663
    if-eqz v2, :cond_0

    .line 2260664
    new-instance v0, LX/8dV;

    invoke-direct {v0}, LX/8dV;-><init>()V

    new-instance v3, LX/8dX;

    invoke-direct {v3}, LX/8dX;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v4

    .line 2260665
    iput-object v4, v3, LX/8dX;->L:Ljava/lang/String;

    .line 2260666
    move-object v3, v3

    .line 2260667
    new-instance v4, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v5, 0x581d640b

    invoke-direct {v4, v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2260668
    iput-object v4, v3, LX/8dX;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2260669
    move-object v3, v3

    .line 2260670
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->bF()J

    move-result-wide v4

    .line 2260671
    iput-wide v4, v3, LX/8dX;->r:J

    .line 2260672
    move-object v3, v3

    .line 2260673
    iput-object v2, v3, LX/8dX;->s:Lcom/facebook/graphql/model/GraphQLStory;

    .line 2260674
    move-object v2, v3

    .line 2260675
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dN()Z

    move-result v3

    .line 2260676
    iput-boolean v3, v2, LX/8dX;->I:Z

    .line 2260677
    move-object v2, v2

    .line 2260678
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dR()I

    move-result v3

    .line 2260679
    iput v3, v2, LX/8dX;->J:I

    .line 2260680
    move-object v2, v2

    .line 2260681
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->ez()Z

    move-result v3

    .line 2260682
    iput-boolean v3, v2, LX/8dX;->T:Z

    .line 2260683
    move-object v2, v2

    .line 2260684
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fh()I

    move-result v3

    .line 2260685
    iput v3, v2, LX/8dX;->Z:I

    .line 2260686
    move-object v2, v2

    .line 2260687
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fY()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v3

    .line 2260688
    if-nez v3, :cond_2

    .line 2260689
    const/4 v4, 0x0

    .line 2260690
    :goto_1
    move-object v3, v4

    .line 2260691
    iput-object v3, v2, LX/8dX;->ah:Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    .line 2260692
    move-object v2, v2

    .line 2260693
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->gC()I

    move-result v3

    .line 2260694
    iput v3, v2, LX/8dX;->ar:I

    .line 2260695
    move-object v2, v2

    .line 2260696
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->gE()I

    move-result v3

    .line 2260697
    iput v3, v2, LX/8dX;->as:I

    .line 2260698
    move-object v2, v2

    .line 2260699
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->fv()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v3

    invoke-static {v3}, LX/FbH;->a(Lcom/facebook/graphql/model/GraphQLTextWithEntities;)Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    move-result-object v3

    .line 2260700
    iput-object v3, v2, LX/8dX;->ab:Lcom/facebook/graphql/querybuilder/common/TextWithEntitiesGraphQLModels$DefaultTextWithEntitiesLongFieldsModel;

    .line 2260701
    move-object v2, v2

    .line 2260702
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->lf()I

    move-result v3

    .line 2260703
    iput v3, v2, LX/8dX;->bh:I

    .line 2260704
    move-object v2, v2

    .line 2260705
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->eJ()Z

    move-result v3

    .line 2260706
    iput-boolean v3, v2, LX/8dX;->V:Z

    .line 2260707
    move-object v2, v2

    .line 2260708
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->hr()Ljava/lang/String;

    move-result-object v3

    .line 2260709
    iput-object v3, v2, LX/8dX;->ax:Ljava/lang/String;

    .line 2260710
    move-object v2, v2

    .line 2260711
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->ee()I

    move-result v3

    .line 2260712
    iput v3, v2, LX/8dX;->O:I

    .line 2260713
    move-object v2, v2

    .line 2260714
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->ef()I

    move-result v3

    .line 2260715
    iput v3, v2, LX/8dX;->P:I

    .line 2260716
    move-object v2, v2

    .line 2260717
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->eg()I

    move-result v3

    .line 2260718
    iput v3, v2, LX/8dX;->Q:I

    .line 2260719
    move-object v2, v2

    .line 2260720
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->iR()Ljava/lang/String;

    move-result-object v3

    .line 2260721
    iput-object v3, v2, LX/8dX;->aK:Ljava/lang/String;

    .line 2260722
    move-object v2, v2

    .line 2260723
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->iQ()Ljava/lang/String;

    move-result-object v3

    .line 2260724
    iput-object v3, v2, LX/8dX;->aJ:Ljava/lang/String;

    .line 2260725
    move-object v2, v2

    .line 2260726
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->iS()I

    move-result v3

    .line 2260727
    iput v3, v2, LX/8dX;->aL:I

    .line 2260728
    move-object v2, v2

    .line 2260729
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->iP()D

    move-result-wide v4

    .line 2260730
    iput-wide v4, v2, LX/8dX;->aI:D

    .line 2260731
    move-object v2, v2

    .line 2260732
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->iO()D

    move-result-wide v4

    .line 2260733
    iput-wide v4, v2, LX/8dX;->aH:D

    .line 2260734
    move-object v2, v2

    .line 2260735
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->dJ()Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;

    move-result-object v1

    invoke-static {v1}, LX/Fbd;->a(Lcom/facebook/graphql/model/GraphQLVideoGuidedTour;)Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    move-result-object v1

    .line 2260736
    iput-object v1, v2, LX/8dX;->H:Lcom/facebook/api/graphql/media/NewsFeedMediaGraphQLModels$SphericalMetadataModel$GuidedTourModel;

    .line 2260737
    move-object v1, v2

    .line 2260738
    invoke-virtual {v1}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v1

    .line 2260739
    iput-object v1, v0, LX/8dV;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2260740
    move-object v0, v0

    .line 2260741
    goto/16 :goto_0

    :cond_2
    new-instance v4, LX/A4w;

    invoke-direct {v4}, LX/A4w;-><init>()V

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->R()Z

    move-result v5

    .line 2260742
    iput-boolean v5, v4, LX/A4w;->b:Z

    .line 2260743
    move-object v4, v4

    .line 2260744
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object v5

    .line 2260745
    iput-object v5, v4, LX/A4w;->c:Ljava/lang/String;

    .line 2260746
    move-object v4, v4

    .line 2260747
    invoke-virtual {v4}, LX/A4w;->a()Lcom/facebook/search/results/protocol/video/SearchResultsVideoModels$SearchResultsVideoModel$OwnerModel;

    move-result-object v4

    goto/16 :goto_1
.end method
