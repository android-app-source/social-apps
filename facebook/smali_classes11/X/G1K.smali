.class public LX/G1K;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/timeline/protiles/model/ProtileModel;

.field public final b:LX/G1J;

.field public final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/protiles/model/ProtileModel;LX/G1J;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/protiles/model/ProtileModel;",
            "LX/G1J;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2311060
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2311061
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/protiles/model/ProtileModel;

    iput-object v0, p0, LX/G1K;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 2311062
    iput-object p2, p0, LX/G1K;->b:LX/G1J;

    .line 2311063
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, LX/G1K;->c:Ljava/util/List;

    .line 2311064
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2311065
    return-void

    .line 2311066
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
