.class public final LX/F6f;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/F6f;


# instance fields
.field private final a:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Uh;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Uh;)V
    .locals 0
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2200727
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2200728
    iput-object p1, p0, LX/F6f;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2200729
    iput-object p3, p0, LX/F6f;->c:LX/0Uh;

    .line 2200730
    iput-object p2, p0, LX/F6f;->b:LX/0Or;

    .line 2200731
    return-void
.end method

.method private a(LX/0Tn;)LX/0Tn;
    .locals 1

    .prologue
    .line 2200736
    iget-object v0, p0, LX/F6f;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2200737
    if-nez v0, :cond_0

    .line 2200738
    const-string v0, "0"

    .line 2200739
    :cond_0
    invoke-virtual {p1, v0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    return-object v0
.end method

.method public static a(LX/0QB;)LX/F6f;
    .locals 6

    .prologue
    .line 2200740
    sget-object v0, LX/F6f;->d:LX/F6f;

    if-nez v0, :cond_1

    .line 2200741
    const-class v1, LX/F6f;

    monitor-enter v1

    .line 2200742
    :try_start_0
    sget-object v0, LX/F6f;->d:LX/F6f;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2200743
    if-eqz v2, :cond_0

    .line 2200744
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2200745
    new-instance v5, LX/F6f;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v4, 0x15e7

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {v5, v3, p0, v4}, LX/F6f;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/0Uh;)V

    .line 2200746
    move-object v0, v5

    .line 2200747
    sput-object v0, LX/F6f;->d:LX/F6f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2200748
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2200749
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2200750
    :cond_1
    sget-object v0, LX/F6f;->d:LX/F6f;

    return-object v0

    .line 2200751
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2200752
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2200734
    iget-object v0, p0, LX/F6f;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/1nR;->k:LX/0Tn;

    invoke-direct {p0, v1}, LX/F6f;->a(LX/0Tn;)LX/0Tn;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2200735
    return-void
.end method

.method public final b()Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2200732
    sget-object v2, LX/03R;->NO:LX/03R;

    iget-object v3, p0, LX/F6f;->c:LX/0Uh;

    const/16 v4, 0x31

    invoke-virtual {v3, v4}, LX/0Uh;->a(I)LX/03R;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/03R;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2200733
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, LX/F6f;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v3, LX/1nR;->k:LX/0Tn;

    invoke-direct {p0, v3}, LX/F6f;->a(LX/0Tn;)LX/0Tn;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method
