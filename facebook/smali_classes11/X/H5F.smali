.class public final LX/H5F;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/H5G;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/ReactionActionsGraphQLInterfaces$ReactionStoryAttachmentActionFragment;",
            ">;"
        }
    .end annotation
.end field

.field public b:Z

.field public c:Z

.field public d:LX/2km;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public e:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public f:Ljava/lang/String;

.field public final synthetic g:LX/H5G;


# direct methods
.method public constructor <init>(LX/H5G;)V
    .locals 1

    .prologue
    .line 2423912
    iput-object p1, p0, LX/H5F;->g:LX/H5G;

    .line 2423913
    move-object v0, p1

    .line 2423914
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2423915
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2423916
    const-string v0, "NotificationsActionListComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2423917
    if-ne p0, p1, :cond_1

    .line 2423918
    :cond_0
    :goto_0
    return v0

    .line 2423919
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2423920
    goto :goto_0

    .line 2423921
    :cond_3
    check-cast p1, LX/H5F;

    .line 2423922
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2423923
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2423924
    if-eq v2, v3, :cond_0

    .line 2423925
    iget-object v2, p0, LX/H5F;->a:LX/0Px;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/H5F;->a:LX/0Px;

    iget-object v3, p1, LX/H5F;->a:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2423926
    goto :goto_0

    .line 2423927
    :cond_5
    iget-object v2, p1, LX/H5F;->a:LX/0Px;

    if-nez v2, :cond_4

    .line 2423928
    :cond_6
    iget-boolean v2, p0, LX/H5F;->b:Z

    iget-boolean v3, p1, LX/H5F;->b:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2423929
    goto :goto_0

    .line 2423930
    :cond_7
    iget-boolean v2, p0, LX/H5F;->c:Z

    iget-boolean v3, p1, LX/H5F;->c:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 2423931
    goto :goto_0

    .line 2423932
    :cond_8
    iget-object v2, p0, LX/H5F;->d:LX/2km;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/H5F;->d:LX/2km;

    iget-object v3, p1, LX/H5F;->d:LX/2km;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_9
    move v0, v1

    .line 2423933
    goto :goto_0

    .line 2423934
    :cond_a
    iget-object v2, p1, LX/H5F;->d:LX/2km;

    if-nez v2, :cond_9

    .line 2423935
    :cond_b
    iget-object v2, p0, LX/H5F;->e:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-eqz v2, :cond_d

    iget-object v2, p0, LX/H5F;->e:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v3, p1, LX/H5F;->e:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    :cond_c
    move v0, v1

    .line 2423936
    goto :goto_0

    .line 2423937
    :cond_d
    iget-object v2, p1, LX/H5F;->e:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    if-nez v2, :cond_c

    .line 2423938
    :cond_e
    iget-object v2, p0, LX/H5F;->f:Ljava/lang/String;

    if-eqz v2, :cond_f

    iget-object v2, p0, LX/H5F;->f:Ljava/lang/String;

    iget-object v3, p1, LX/H5F;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2423939
    goto :goto_0

    .line 2423940
    :cond_f
    iget-object v2, p1, LX/H5F;->f:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
