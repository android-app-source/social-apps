.class public final LX/GtI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/InternSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/InternSettingsActivity;)V
    .locals 0

    .prologue
    .line 2400507
    iput-object p1, p0, LX/GtI;->a:Lcom/facebook/katana/InternSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2400508
    instance-of v0, p2, Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2400509
    const/4 v0, 0x0

    .line 2400510
    :goto_0
    return v0

    .line 2400511
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2400512
    const-string v1, "extra_instant_articles_id"

    check-cast p2, Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2400513
    const-string v1, "extra_instant_articles_referrer"

    const-string v2, "settings"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2400514
    iget-object v1, p0, LX/GtI;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v1, v1, Lcom/facebook/katana/InternSettingsActivity;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/GtI;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v2, v2, Lcom/facebook/katana/InternSettingsActivity;->u:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2400515
    const/4 v0, 0x1

    goto :goto_0
.end method
