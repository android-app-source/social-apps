.class public final LX/G9I;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/G9I;

.field public static final b:LX/G9I;

.field public static final c:LX/G9I;

.field public static final d:LX/G9I;

.field public static final e:LX/G9I;

.field public static final f:LX/G9I;

.field public static final g:LX/G9I;

.field public static final h:LX/G9I;


# instance fields
.field private final i:[I

.field private final j:[I

.field public final k:LX/G9J;

.field public final l:LX/G9J;

.field public final m:I

.field private final n:I

.field public final o:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0x100

    const/4 v3, 0x1

    .line 2322067
    new-instance v0, LX/G9I;

    const/16 v1, 0x1069

    const/16 v2, 0x1000

    invoke-direct {v0, v1, v2, v3}, LX/G9I;-><init>(III)V

    sput-object v0, LX/G9I;->a:LX/G9I;

    .line 2322068
    new-instance v0, LX/G9I;

    const/16 v1, 0x409

    const/16 v2, 0x400

    invoke-direct {v0, v1, v2, v3}, LX/G9I;-><init>(III)V

    sput-object v0, LX/G9I;->b:LX/G9I;

    .line 2322069
    new-instance v0, LX/G9I;

    const/16 v1, 0x43

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2, v3}, LX/G9I;-><init>(III)V

    sput-object v0, LX/G9I;->c:LX/G9I;

    .line 2322070
    new-instance v0, LX/G9I;

    const/16 v1, 0x13

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2, v3}, LX/G9I;-><init>(III)V

    sput-object v0, LX/G9I;->d:LX/G9I;

    .line 2322071
    new-instance v0, LX/G9I;

    const/16 v1, 0x11d

    const/4 v2, 0x0

    invoke-direct {v0, v1, v4, v2}, LX/G9I;-><init>(III)V

    sput-object v0, LX/G9I;->e:LX/G9I;

    .line 2322072
    new-instance v0, LX/G9I;

    const/16 v1, 0x12d

    invoke-direct {v0, v1, v4, v3}, LX/G9I;-><init>(III)V

    .line 2322073
    sput-object v0, LX/G9I;->f:LX/G9I;

    sput-object v0, LX/G9I;->g:LX/G9I;

    .line 2322074
    sget-object v0, LX/G9I;->c:LX/G9I;

    sput-object v0, LX/G9I;->h:LX/G9I;

    return-void
.end method

.method private constructor <init>(III)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 2322075
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2322076
    iput p1, p0, LX/G9I;->n:I

    .line 2322077
    iput p2, p0, LX/G9I;->m:I

    .line 2322078
    iput p3, p0, LX/G9I;->o:I

    .line 2322079
    new-array v0, p2, [I

    iput-object v0, p0, LX/G9I;->i:[I

    .line 2322080
    new-array v0, p2, [I

    iput-object v0, p0, LX/G9I;->j:[I

    move v2, v1

    move v0, v3

    .line 2322081
    :goto_0
    if-ge v2, p2, :cond_1

    .line 2322082
    iget-object v4, p0, LX/G9I;->i:[I

    aput v0, v4, v2

    .line 2322083
    mul-int/lit8 v0, v0, 0x2

    .line 2322084
    if-lt v0, p2, :cond_0

    .line 2322085
    xor-int/2addr v0, p1

    .line 2322086
    add-int/lit8 v4, p2, -0x1

    and-int/2addr v0, v4

    .line 2322087
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 2322088
    :goto_1
    add-int/lit8 v2, p2, -0x1

    if-ge v0, v2, :cond_2

    .line 2322089
    iget-object v2, p0, LX/G9I;->j:[I

    iget-object v4, p0, LX/G9I;->i:[I

    aget v4, v4, v0

    aput v0, v2, v4

    .line 2322090
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2322091
    :cond_2
    new-instance v0, LX/G9J;

    new-array v2, v3, [I

    aput v1, v2, v1

    invoke-direct {v0, p0, v2}, LX/G9J;-><init>(LX/G9I;[I)V

    iput-object v0, p0, LX/G9I;->k:LX/G9J;

    .line 2322092
    new-instance v0, LX/G9J;

    new-array v2, v3, [I

    aput v3, v2, v1

    invoke-direct {v0, p0, v2}, LX/G9J;-><init>(LX/G9I;[I)V

    iput-object v0, p0, LX/G9I;->l:LX/G9J;

    .line 2322093
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 2322094
    iget-object v0, p0, LX/G9I;->i:[I

    aget v0, v0, p1

    return v0
.end method

.method public final a(II)LX/G9J;
    .locals 2

    .prologue
    .line 2322095
    if-gez p1, :cond_0

    .line 2322096
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2322097
    :cond_0
    if-nez p2, :cond_1

    .line 2322098
    iget-object v0, p0, LX/G9I;->k:LX/G9J;

    .line 2322099
    :goto_0
    return-object v0

    .line 2322100
    :cond_1
    add-int/lit8 v0, p1, 0x1

    new-array v1, v0, [I

    .line 2322101
    const/4 v0, 0x0

    aput p2, v1, v0

    .line 2322102
    new-instance v0, LX/G9J;

    invoke-direct {v0, p0, v1}, LX/G9J;-><init>(LX/G9I;[I)V

    goto :goto_0
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 2322103
    if-nez p1, :cond_0

    .line 2322104
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2322105
    :cond_0
    iget-object v0, p0, LX/G9I;->j:[I

    aget v0, v0, p1

    return v0
.end method

.method public final c(I)I
    .locals 3

    .prologue
    .line 2322106
    if-nez p1, :cond_0

    .line 2322107
    new-instance v0, Ljava/lang/ArithmeticException;

    invoke-direct {v0}, Ljava/lang/ArithmeticException;-><init>()V

    throw v0

    .line 2322108
    :cond_0
    iget-object v0, p0, LX/G9I;->i:[I

    iget v1, p0, LX/G9I;->m:I

    iget-object v2, p0, LX/G9I;->j:[I

    aget v2, v2, p1

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    return v0
.end method

.method public final c(II)I
    .locals 3

    .prologue
    .line 2322109
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 2322110
    :cond_0
    const/4 v0, 0x0

    .line 2322111
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, LX/G9I;->i:[I

    iget-object v1, p0, LX/G9I;->j:[I

    aget v1, v1, p1

    iget-object v2, p0, LX/G9I;->j:[I

    aget v2, v2, p2

    add-int/2addr v1, v2

    iget v2, p0, LX/G9I;->m:I

    add-int/lit8 v2, v2, -0x1

    rem-int/2addr v1, v2

    aget v0, v0, v1

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2322112
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GF(0x"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, LX/G9I;->n:I

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, LX/G9I;->m:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
