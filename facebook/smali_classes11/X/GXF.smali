.class public final LX/GXF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/concurrent/Callable;

.field public final synthetic b:LX/GXH;


# direct methods
.method public constructor <init>(LX/GXH;Ljava/util/concurrent/Callable;)V
    .locals 0

    .prologue
    .line 2363577
    iput-object p1, p0, LX/GXF;->b:LX/GXH;

    iput-object p2, p0, LX/GXF;->a:Ljava/util/concurrent/Callable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2363570
    const-string v0, "ProductGroupUserInteractionsViewControllerImpl"

    const-string v1, "Couldn\'t toggle like."

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2363571
    iget-object v0, p0, LX/GXF;->b:LX/GXH;

    iget-object v0, v0, LX/GXH;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0814c5

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2363572
    :try_start_0
    iget-object v0, p0, LX/GXF;->a:Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2363573
    :goto_0
    return-void

    .line 2363574
    :catch_0
    move-exception v0

    .line 2363575
    const-string v1, "ProductGroupUserInteractionsViewControllerImpl"

    const-string v2, "Couldn\'t call onFailureCallback"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2363576
    return-void
.end method
