.class public final LX/HAm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<+",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<*>;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;)V
    .locals 0

    .prologue
    .line 2435910
    iput-object p1, p0, LX/HAm;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2435911
    iget-object v0, p0, LX/HAm;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->e:LX/0tX;

    iget-object v1, p0, LX/HAm;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    .line 2435912
    iget-object v2, v1, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->k:Landroid/location/Location;

    .line 2435913
    if-nez v2, :cond_0

    .line 2435914
    iget-object v3, v1, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->g:LX/0y2;

    const-wide/32 v4, 0x1b7740

    invoke-virtual {v3, v4, v5}, LX/0y2;->a(J)Lcom/facebook/location/ImmutableLocation;

    move-result-object v3

    .line 2435915
    if-eqz v3, :cond_0

    .line 2435916
    invoke-virtual {v3}, Lcom/facebook/location/ImmutableLocation;->l()Landroid/location/Location;

    move-result-object v2

    .line 2435917
    :cond_0
    if-eqz v2, :cond_1

    .line 2435918
    new-instance v6, LX/HAY;

    invoke-direct {v6}, LX/HAY;-><init>()V

    move-object v6, v6

    .line 2435919
    const-string v7, "page_id"

    iget-object v8, v1, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->i:Ljava/lang/String;

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string v7, "user_image_size"

    iget v8, v1, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->m:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v7, "viewer_latitude"

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v7, "viewer_longitude"

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    const-string v7, "max_child_locations"

    iget v8, v1, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->l:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v6

    check-cast v6, LX/HAY;

    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    const-string v7, "GraphQlPageCardTag"

    invoke-static {v7}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v7

    .line 2435920
    iput-object v7, v6, LX/0zO;->d:Ljava/util/Set;

    .line 2435921
    move-object v6, v6

    .line 2435922
    sget-object v7, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v6, v7}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v6

    move-object v2, v6

    .line 2435923
    :goto_0
    move-object v1, v2

    .line 2435924
    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0

    .line 2435925
    :cond_1
    new-instance v2, LX/HAZ;

    invoke-direct {v2}, LX/HAZ;-><init>()V

    move-object v2, v2

    .line 2435926
    const-string v3, "page_id"

    iget-object v4, v1, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->i:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "user_image_size"

    iget v4, v1, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->m:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "max_child_locations"

    iget v4, v1, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->l:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    check-cast v2, LX/HAZ;

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    const-string v3, "GraphQlPageCardTag"

    invoke-static {v3}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v3

    .line 2435927
    iput-object v3, v2, LX/0zO;->d:Ljava/util/Set;

    .line 2435928
    move-object v2, v2

    .line 2435929
    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v2, v3}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v2

    move-object v2, v2

    .line 2435930
    goto :goto_0
.end method
