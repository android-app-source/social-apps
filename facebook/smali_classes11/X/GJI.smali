.class public abstract LX/GJI;
.super LX/GJH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/facebook/adinterfaces/ui/BudgetOptionsView;",
        "D::",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">",
        "LX/GJH",
        "<TT;TD;>;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public b:Landroid/content/res/Resources;

.field public c:I

.field private g:LX/GMU;

.field private h:LX/1Ck;

.field private i:LX/GE2;

.field public j:Lcom/facebook/quicklog/QuickPerformanceLogger;

.field private k:LX/GLN;

.field public l:LX/0tX;

.field private m:LX/GLM;

.field public final n:LX/2U3;

.field public final o:LX/0Sh;

.field public p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

.field public q:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

.field public r:Z

.field public s:Z

.field private t:LX/0ad;


# direct methods
.method public constructor <init>(Landroid/view/inputmethod/InputMethodManager;LX/GMU;LX/1Ck;LX/GE2;LX/GLN;LX/0Sh;LX/0tX;LX/2U3;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2338145
    invoke-direct {p0, p1}, LX/GJH;-><init>(Landroid/view/inputmethod/InputMethodManager;)V

    .line 2338146
    iput v1, p0, LX/GJI;->c:I

    .line 2338147
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GJI;->r:Z

    .line 2338148
    iput-boolean v1, p0, LX/GJI;->s:Z

    .line 2338149
    iput-object p2, p0, LX/GJI;->g:LX/GMU;

    .line 2338150
    iput-object p3, p0, LX/GJI;->h:LX/1Ck;

    .line 2338151
    iput-object p4, p0, LX/GJI;->i:LX/GE2;

    .line 2338152
    iput-object p9, p0, LX/GJI;->j:Lcom/facebook/quicklog/QuickPerformanceLogger;

    .line 2338153
    iput-object p5, p0, LX/GJI;->k:LX/GLN;

    .line 2338154
    iput-object p6, p0, LX/GJI;->o:LX/0Sh;

    .line 2338155
    iput-object p7, p0, LX/GJI;->l:LX/0tX;

    .line 2338156
    iput-object p8, p0, LX/GJI;->n:LX/2U3;

    .line 2338157
    iput-object p10, p0, LX/GJI;->t:LX/0ad;

    .line 2338158
    return-void
.end method

.method public static A(LX/GJI;)V
    .locals 3

    .prologue
    .line 2338262
    invoke-virtual {p0}, LX/GJI;->u()V

    .line 2338263
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->getTextEditText()Ljava/lang/String;

    move-result-object v0

    .line 2338264
    invoke-virtual {p0, v0}, LX/GJH;->a(Ljava/lang/String;)LX/GMB;

    move-result-object v1

    .line 2338265
    sget-object v2, LX/GMB;->VALID:LX/GMB;

    if-ne v1, v2, :cond_0

    .line 2338266
    new-instance v1, Ljava/math/BigDecimal;

    invoke-direct {v1, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/math/BigDecimal;

    iget-object v2, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v2}, LX/GG6;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)I

    move-result v2

    invoke-direct {v0, v2}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2338267
    iget-object v1, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0, v1}, LX/GMU;->a(Ljava/math/BigDecimal;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    .line 2338268
    invoke-static {p0, v0}, LX/GJI;->d(LX/GJI;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)V

    .line 2338269
    :cond_0
    return-void
.end method

.method public static F(LX/GJI;)V
    .locals 12

    .prologue
    .line 2338270
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    .line 2338271
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->p()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2338272
    :cond_0
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->setSpinnerVisibility(Z)V

    .line 2338273
    :goto_0
    return-void

    .line 2338274
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2338275
    iget-object v2, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, LX/5fw;->a(Landroid/content/Context;)LX/0Px;

    move-result-object v2

    .line 2338276
    iget-object v3, p0, LX/GJI;->m:LX/GLM;

    if-nez v3, :cond_2

    .line 2338277
    iget-object v3, p0, LX/GJI;->k:LX/GLN;

    new-instance v4, LX/GLr;

    invoke-direct {v4, p0}, LX/GLr;-><init>(LX/GJI;)V

    invoke-static {v2, v4}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/GLN;->a(Ljava/util/List;)LX/GLM;

    move-result-object v3

    iput-object v3, p0, LX/GJI;->m:LX/GLM;

    .line 2338278
    :cond_2
    iget-object v3, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    iget-object v4, p0, LX/GJI;->m:LX/GLM;

    iget-object v5, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    .line 2338279
    new-instance v6, LX/GLu;

    move-object v7, p0

    move-object v8, v2

    move-object v9, v1

    move-object v10, v5

    move-object v11, v0

    invoke-direct/range {v6 .. v11}, LX/GLu;-><init>(LX/GJI;LX/0Px;Ljava/lang/String;Lcom/facebook/adinterfaces/ui/BudgetOptionsView;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)V

    move-object v0, v6

    .line 2338280
    invoke-virtual {v3, v4, v0}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->a(LX/GLM;Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 2338281
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {v2, v1}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->setSpinnerSelected(I)V

    goto :goto_0
.end method

.method public static G(LX/GJI;)I
    .locals 2

    .prologue
    .line 2338282
    sget-object v0, LX/GLw;->b:[I

    iget-object v1, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v1

    invoke-virtual {v1}, LX/8wL;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2338283
    const v0, 0x7f080bb3

    :goto_0
    return v0

    .line 2338284
    :pswitch_0
    const v0, 0x7f080baf

    goto :goto_0

    .line 2338285
    :pswitch_1
    const v0, 0x7f080bad

    goto :goto_0

    .line 2338286
    :pswitch_2
    const v0, 0x7f080bb1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public static H(LX/GJI;)I
    .locals 2

    .prologue
    .line 2338287
    sget-object v0, LX/GLw;->b:[I

    iget-object v1, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v1

    invoke-virtual {v1}, LX/8wL;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2338288
    const v0, 0x7f080bb4

    :goto_0
    return v0

    .line 2338289
    :pswitch_0
    const v0, 0x7f080bb0

    goto :goto_0

    .line 2338290
    :pswitch_1
    const v0, 0x7f080bae

    goto :goto_0

    .line 2338291
    :pswitch_2
    const v0, 0x7f080bb2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private a(Landroid/text/Spanned;)V
    .locals 2

    .prologue
    .line 2338292
    iget-object v0, p0, LX/GJI;->q:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a()Z

    move-result v0

    .line 2338293
    iget-object v1, p0, LX/GJI;->q:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v1, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    .line 2338294
    if-eqz p1, :cond_0

    if-nez v0, :cond_0

    .line 2338295
    iget-object v0, p0, LX/GJI;->q:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-static {v0}, LX/GMo;->a(Landroid/view/View;)V

    .line 2338296
    :cond_0
    return-void
.end method

.method public static b(LX/GJI;Z)V
    .locals 1

    .prologue
    .line 2338297
    iget-object v0, p0, LX/GJI;->q:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Z)V

    .line 2338298
    return-void
.end method

.method public static c(LX/GJI;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;)V
    .locals 3

    .prologue
    .line 2338299
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2338300
    :cond_0
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setTextSuffixTextView(Ljava/lang/CharSequence;)V

    .line 2338301
    :goto_0
    return-void

    .line 2338302
    :cond_1
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {p0, p1}, LX/GJI;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setTextSuffixTextView(Ljava/lang/CharSequence;)V

    .line 2338303
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {p0, p1}, LX/GJI;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setContentDescriptionSuffixTextView(Ljava/lang/CharSequence;)V

    .line 2338304
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v2}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v2

    invoke-virtual {v2}, Ljava/text/NumberFormat;->getCurrency()Ljava/util/Currency;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Currency;->getSymbol()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->getTextEditText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setContentDescriptionPrefixTextView(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public static d(LX/GJI;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)V
    .locals 14

    .prologue
    const/4 v7, 0x0

    const v3, 0x5a0008

    .line 2338305
    invoke-static {p0, v7}, LX/GJI;->c(LX/GJI;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;)V

    .line 2338306
    iget-object v0, p0, LX/GJI;->j:Lcom/facebook/quicklog/QuickPerformanceLogger;

    sget-object v1, LX/GE1;->FETCH_SINGLE_BUDGET_RECOMMENDATION:LX/GE1;

    invoke-virtual {v1}, LX/GE1;->hashCode()I

    move-result v1

    invoke-interface {v0, v3, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 2338307
    iget-object v0, p0, LX/GJI;->j:Lcom/facebook/quicklog/QuickPerformanceLogger;

    sget-object v1, LX/GE1;->FETCH_SINGLE_BUDGET_RECOMMENDATION:LX/GE1;

    invoke-virtual {v1}, LX/GE1;->hashCode()I

    move-result v1

    iget-object v2, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v2

    invoke-virtual {v2}, LX/8wL;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 2338308
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/GJI;->b(LX/GJI;Z)V

    .line 2338309
    iget-object v12, p0, LX/GJI;->h:LX/1Ck;

    sget-object v13, LX/GE1;->FETCH_SINGLE_BUDGET_RECOMMENDATION:LX/GE1;

    iget-object v0, p0, LX/GJI;->i:LX/GE2;

    iget-object v1, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v2

    invoke-virtual {v2}, LX/8wL;->getComponentAppEnum()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v3}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v4}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v5}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->i()I

    move-result v5

    invoke-static {v5}, LX/GG6;->b(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v6

    iget-object v8, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v8}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v8

    .line 2338310
    iget-object v9, v8, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v8, v9

    .line 2338311
    if-nez v8, :cond_0

    :goto_0
    iget-object v8, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v8}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v8

    .line 2338312
    iget-object v9, v8, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v8, v9

    .line 2338313
    iget-object v9, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v9}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v9

    sget-object v10, LX/GE2;->a:Ljava/lang/Integer;

    iget-object v11, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    check-cast v11, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v11}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->E()Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    move-result-object v11

    invoke-virtual/range {v0 .. v11}, LX/GE2;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;Ljava/lang/Integer;Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/GM5;

    invoke-direct {v1, p0}, LX/GM5;-><init>(LX/GJI;)V

    invoke-virtual {v12, v13, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2338314
    return-void

    .line 2338315
    :cond_0
    iget-object v7, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v7}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v7

    .line 2338316
    iget-object v8, v7, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v7, v8

    .line 2338317
    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->name()Ljava/lang/String;

    move-result-object v7

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;)Landroid/text/Spanned;
.end method

.method public abstract a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/lang/CharSequence;
.end method

.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2338318
    invoke-super {p0}, LX/GJH;->a()V

    .line 2338319
    iget-object v0, p0, LX/GJI;->h:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2338320
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {v0, v1}, LX/GJD;->setOnCheckChangedListener(LX/Bc9;)V

    .line 2338321
    iput-object v1, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    .line 2338322
    iput-object v1, p0, LX/GJI;->q:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2338323
    return-void
.end method

.method public abstract a(LX/GGB;)V
.end method

.method public bridge synthetic a(LX/GJD;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2338354
    check-cast p1, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {p0, p1, p2}, LX/GJI;->a(Lcom/facebook/adinterfaces/ui/BudgetOptionsView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2338351
    const-string v0, "selected_budget"

    iget-object v1, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2338352
    const-string v0, "selected_interval"

    iget-object v1, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->g()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v1

    invoke-static {p1, v0, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2338353
    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2338350
    check-cast p1, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {p0, p1, p2}, LX/GJI;->a(Lcom/facebook/adinterfaces/ui/BudgetOptionsView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)V"
        }
    .end annotation

    .prologue
    .line 2338348
    iput-object p1, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2338349
    return-void
.end method

.method public a(Lcom/facebook/adinterfaces/ui/BudgetOptionsView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2338331
    invoke-super {p0, p1, p2}, LX/GJH;->a(LX/GJD;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2338332
    iput-object p1, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    .line 2338333
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/BudgetOptionsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/GJI;->b:Landroid/content/res/Resources;

    .line 2338334
    iput-object p2, p0, LX/GJI;->q:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2338335
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/GJI;->a(LX/GGB;)V

    .line 2338336
    invoke-static {p0}, LX/GJI;->F(LX/GJI;)V

    .line 2338337
    invoke-virtual {p0}, LX/GJI;->c()V

    .line 2338338
    invoke-virtual {p0}, LX/GJI;->e()V

    .line 2338339
    invoke-virtual {p0}, LX/GJI;->j()V

    .line 2338340
    invoke-virtual {p0}, LX/GJH;->z()V

    .line 2338341
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2338342
    sget-object v1, LX/GG8;->INVALID_BUDGET:LX/GG8;

    invoke-virtual {p0}, LX/GJI;->s()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/GCE;->a(LX/GG8;Z)V

    .line 2338343
    invoke-virtual {p0}, LX/GJI;->t()V

    .line 2338344
    invoke-virtual {p0}, LX/GJI;->p()V

    .line 2338345
    iget-object v0, p0, LX/GJI;->q:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080a4a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2338346
    iget-object v1, p0, LX/GJI;->q:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const v2, 0x7f021962

    invoke-virtual {v1, v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Ljava/lang/String;I)V

    .line 2338347
    return-void
.end method

.method public abstract b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;)Landroid/text/Spanned;
.end method

.method public b()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2338324
    iget-object v1, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {v1}, LX/GJD;->getSelectedIndex()I

    move-result v1

    .line 2338325
    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    .line 2338326
    :cond_0
    :goto_0
    return-object v0

    .line 2338327
    :cond_1
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->getTag()Ljava/lang/Object;

    move-result-object v3

    if-ne v2, v3, :cond_2

    .line 2338328
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->f()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    goto :goto_0

    .line 2338329
    :cond_2
    iget-object v2, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->a()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 2338330
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->j()LX/0Px;

    move-result-object v0

    iget-object v1, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {v1}, LX/GJD;->getSelectedIndex()I

    move-result v1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    goto :goto_0
.end method

.method public abstract b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/lang/CharSequence;
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2338159
    if-nez p1, :cond_0

    .line 2338160
    invoke-virtual {p0}, LX/GJI;->u()V

    .line 2338161
    :goto_0
    return-void

    .line 2338162
    :cond_0
    iget-object v2, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    const-string v0, "selected_budget"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    const-string v1, "selected_interval"

    invoke-static {p1, v1}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    invoke-interface {v2, v0, v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V

    goto :goto_0
.end method

.method public abstract c(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;
.end method

.method public c()V
    .locals 5

    .prologue
    .line 2338163
    invoke-virtual {p0}, LX/GJI;->d()V

    .line 2338164
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2338165
    iget-object v0, p0, LX/GJI;->n:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "No budget recommendation"

    invoke-virtual {v0, v1, v2}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2338166
    :goto_0
    return-void

    .line 2338167
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->a()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2338168
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {v0, v1}, LX/GJD;->e(I)V

    .line 2338169
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;

    move-result-object v0

    .line 2338170
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/GJI;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/lang/CharSequence;

    move-result-object v2

    .line 2338171
    if-nez v2, :cond_1

    .line 2338172
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {v0, v1}, LX/GJD;->d(I)V

    .line 2338173
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2338174
    :cond_1
    iget-object v3, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {p0, v0}, LX/GJI;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v3, v2, v4, v1}, LX/GJD;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    .line 2338175
    iget-object v2, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {p0, v0}, LX/GJI;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, LX/GJD;->a(Ljava/lang/CharSequence;I)V

    .line 2338176
    iget-object v2, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/GJI;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2338177
    if-ltz v1, :cond_2

    iget-object v3, v2, LX/GJD;->c:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-lt v1, v3, :cond_4

    .line 2338178
    :cond_2
    :goto_3
    goto :goto_2

    .line 2338179
    :cond_3
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    .line 2338180
    new-instance v1, LX/GM2;

    invoke-direct {v1, p0}, LX/GM2;-><init>(LX/GJI;)V

    move-object v1, v1

    .line 2338181
    invoke-virtual {v0, v1}, LX/GJD;->setOnCheckChangedListener(LX/Bc9;)V

    goto :goto_0

    .line 2338182
    :cond_4
    iget-object v3, v2, LX/GJD;->c:LX/0Px;

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;

    invoke-virtual {v3, v0}, Lcom/facebook/adinterfaces/ui/FbCustomRadioButton;->setContentDescriptionTextViewStart(Ljava/lang/CharSequence;)V

    goto :goto_3
.end method

.method public d()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2338183
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {v0}, LX/GJD;->e()V

    .line 2338184
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2338185
    :goto_0
    return-void

    .line 2338186
    :cond_0
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2338187
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    .line 2338188
    iget-object v1, v0, LX/GJD;->b:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    move-object v0, v1

    .line 2338189
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setChecked(Z)V

    goto :goto_0

    :cond_1
    move v1, v2

    .line 2338190
    :goto_1
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->a()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2338191
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;

    .line 2338192
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2338193
    iget-object v2, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V

    .line 2338194
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    iget v2, p0, LX/GJI;->c:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, LX/GJD;->f(I)V

    goto :goto_0

    .line 2338195
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2338196
    :cond_3
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {v0, v2}, LX/GJD;->f(I)V

    goto :goto_0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 2338197
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2338198
    new-instance v1, LX/GLx;

    invoke-direct {v1, p0}, LX/GLx;-><init>(LX/GJI;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2338199
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2338200
    new-instance v1, LX/GLy;

    invoke-direct {v1, p0}, LX/GLy;-><init>(LX/GJI;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2338201
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2338202
    new-instance v1, LX/GLz;

    invoke-direct {v1, p0}, LX/GLz;-><init>(LX/GJI;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2338203
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2338204
    new-instance v1, LX/GM0;

    invoke-direct {v1, p0}, LX/GM0;-><init>(LX/GJI;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2338205
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2338206
    new-instance v1, LX/GM1;

    invoke-direct {v1, p0}, LX/GM1;-><init>(LX/GJI;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2338207
    return-void
.end method

.method public abstract g()Ljava/lang/String;
.end method

.method public abstract h()LX/GMT;
.end method

.method public final h_(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2338208
    iget-object v0, p0, LX/GJI;->t:LX/0ad;

    sget-short v1, LX/GDK;->u:S

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 2338209
    if-eqz v0, :cond_0

    .line 2338210
    iput-boolean p1, p0, LX/GJI;->r:Z

    .line 2338211
    :cond_0
    if-eqz v0, :cond_1

    .line 2338212
    iget-boolean v0, p0, LX/GHg;->a:Z

    move v0, v0

    .line 2338213
    if-eqz v0, :cond_1

    .line 2338214
    if-eqz p1, :cond_1

    iget-boolean v0, p0, LX/GJI;->s:Z

    if-eqz v0, :cond_1

    .line 2338215
    iput-boolean v2, p0, LX/GJI;->s:Z

    .line 2338216
    invoke-virtual {p0}, LX/GJI;->u()V

    .line 2338217
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->getTextEditText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2338218
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->getTextEditText()Ljava/lang/String;

    move-result-object v0

    .line 2338219
    new-instance v1, Ljava/math/BigDecimal;

    invoke-direct {v1, v0}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/math/BigDecimal;

    iget-object p1, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {p1}, LX/GG6;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)I

    move-result p1

    invoke-direct {v2, p1}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v1, v2}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 2338220
    iget-object v2, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v1, v2}, LX/GMU;->a(Ljava/math/BigDecimal;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    .line 2338221
    invoke-static {p0, v1}, LX/GJI;->d(LX/GJI;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)V

    .line 2338222
    :cond_1
    return-void
.end method

.method public abstract i()Landroid/text/Spanned;
.end method

.method public j()V
    .locals 2

    .prologue
    .line 2338223
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    iget-object v1, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v1}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v1

    invoke-virtual {v1}, Ljava/text/NumberFormat;->getCurrency()Ljava/util/Currency;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Currency;->getSymbol()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setTextPrefixTextView(Ljava/lang/CharSequence;)V

    .line 2338224
    return-void
.end method

.method public abstract o()Landroid/text/Spanned;
.end method

.method public final p()V
    .locals 3

    .prologue
    .line 2338225
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2338226
    new-instance v1, LX/GFp;

    invoke-direct {v1}, LX/GFp;-><init>()V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2338227
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOSTED_COMPONENT_EDIT_DURATION_BUDGET:LX/8wL;

    if-ne v0, v1, :cond_1

    .line 2338228
    :cond_0
    :goto_0
    return-void

    .line 2338229
    :cond_1
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    instance-of v0, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v0, :cond_0

    .line 2338230
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v0

    .line 2338231
    sget-object v2, LX/GG8;->UNEDITED_DATA:LX/GG8;

    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Z

    move-result v0

    invoke-virtual {v1, v2, v0}, LX/GCE;->a(LX/GG8;Z)V

    goto :goto_0
.end method

.method public final s()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2338232
    invoke-virtual {p0}, LX/GJI;->b()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2338233
    :cond_0
    :goto_0
    return v0

    .line 2338234
    :cond_1
    invoke-virtual {p0}, LX/GJI;->h()LX/GMT;

    move-result-object v1

    .line 2338235
    sget-object v2, LX/GMT;->MAX:LX/GMT;

    if-eq v1, v2, :cond_0

    sget-object v2, LX/GMT;->MIN:LX/GMT;

    if-eq v1, v2, :cond_0

    .line 2338236
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final t()V
    .locals 2

    .prologue
    .line 2338237
    sget-object v0, LX/GLw;->a:[I

    invoke-virtual {p0}, LX/GJI;->h()LX/GMT;

    move-result-object v1

    invoke-virtual {v1}, LX/GMT;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2338238
    :goto_0
    return-void

    .line 2338239
    :pswitch_0
    invoke-virtual {p0}, LX/GJI;->o()Landroid/text/Spanned;

    move-result-object v0

    invoke-direct {p0, v0}, LX/GJI;->a(Landroid/text/Spanned;)V

    goto :goto_0

    .line 2338240
    :pswitch_1
    invoke-virtual {p0}, LX/GJI;->i()Landroid/text/Spanned;

    move-result-object v0

    invoke-direct {p0, v0}, LX/GJI;->a(Landroid/text/Spanned;)V

    goto :goto_0

    .line 2338241
    :pswitch_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/GJI;->a(Landroid/text/Spanned;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final u()V
    .locals 14

    .prologue
    const/4 v7, 0x0

    const v3, 0x5a0008

    .line 2338242
    iget-object v0, p0, LX/GJI;->j:Lcom/facebook/quicklog/QuickPerformanceLogger;

    sget-object v1, LX/GE1;->FETCH_BUDGET_RECOMMENDATION:LX/GE1;

    invoke-virtual {v1}, LX/GE1;->hashCode()I

    move-result v1

    invoke-interface {v0, v3, v1}, Lcom/facebook/quicklog/QuickPerformanceLogger;->e(II)V

    .line 2338243
    iget-object v0, p0, LX/GJI;->j:Lcom/facebook/quicklog/QuickPerformanceLogger;

    sget-object v1, LX/GE1;->FETCH_BUDGET_RECOMMENDATION:LX/GE1;

    invoke-virtual {v1}, LX/GE1;->hashCode()I

    move-result v1

    iget-object v2, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v2

    invoke-virtual {v2}, LX/8wL;->name()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v3, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IILjava/lang/String;)V

    .line 2338244
    const/4 v0, 0x1

    invoke-static {p0, v0}, LX/GJI;->b(LX/GJI;Z)V

    .line 2338245
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2338246
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    invoke-virtual {v0}, LX/GJD;->getSelectedIndex()I

    move-result v0

    .line 2338247
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->getTag()Ljava/lang/Object;

    move-result-object v1

    if-ne v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2338248
    if-nez v0, :cond_2

    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->INACTIVE:LX/GGB;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    if-ne v0, v1, :cond_2

    .line 2338249
    :cond_0
    iget-object v0, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-static {v0}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    .line 2338250
    :goto_1
    iget-object v12, p0, LX/GJI;->h:LX/1Ck;

    sget-object v13, LX/GE1;->FETCH_BUDGET_RECOMMENDATION:LX/GE1;

    iget-object v0, p0, LX/GJI;->i:LX/GE2;

    iget-object v1, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v2

    invoke-virtual {v2}, LX/8wL;->getComponentAppEnum()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v3}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v4}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v5}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->i()I

    move-result v5

    invoke-static {v5}, LX/GG6;->b(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    iget-object v8, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v8}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v8

    .line 2338251
    iget-object v9, v8, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v8, v9

    .line 2338252
    if-nez v8, :cond_1

    :goto_2
    iget-object v8, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v8}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v8

    .line 2338253
    iget-object v9, v8, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v8, v9

    .line 2338254
    iget-object v9, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v9}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v9

    sget-object v10, LX/GE2;->b:Ljava/lang/Integer;

    iget-object v11, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    check-cast v11, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v11}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->E()Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    move-result-object v11

    invoke-virtual/range {v0 .. v11}, LX/GE2;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;Ljava/lang/Integer;Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/GM3;

    invoke-direct {v1, p0}, LX/GM3;-><init>(LX/GJI;)V

    invoke-virtual {v12, v13, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2338255
    return-void

    .line 2338256
    :cond_1
    iget-object v7, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v7}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v7

    .line 2338257
    iget-object v8, v7, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v7, v8

    .line 2338258
    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->name()Ljava/lang/String;

    move-result-object v7

    goto :goto_2

    :cond_2
    move-object v6, v7

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final v()Landroid/text/TextWatcher;
    .locals 1

    .prologue
    .line 2338259
    new-instance v0, LX/GM4;

    invoke-direct {v0, p0}, LX/GM4;-><init>(LX/GJI;)V

    return-object v0
.end method

.method public final w()Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 2338260
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/GJI;->b:Landroid/content/res/Resources;

    const v2, 0x7f080a51

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final x()Ljava/lang/CharSequence;
    .locals 6

    .prologue
    .line 2338261
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, LX/GJI;->b:Landroid/content/res/Resources;

    const v2, 0x7f080a52

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {p0}, LX/GJI;->g()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
