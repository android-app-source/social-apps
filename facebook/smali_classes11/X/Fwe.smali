.class public LX/Fwe;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/graphics/Paint;

.field public b:I


# direct methods
.method public constructor <init>(IILandroid/content/res/Resources;)V
    .locals 2
    .param p1    # I
        .annotation build Landroid/support/annotation/ColorRes;
        .end annotation

        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2303431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2303432
    iput p2, p0, LX/Fwe;->b:I

    .line 2303433
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/Fwe;->a:Landroid/graphics/Paint;

    .line 2303434
    iget-object v0, p0, LX/Fwe;->a:Landroid/graphics/Paint;

    invoke-virtual {p3, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2303435
    iget-object v0, p0, LX/Fwe;->a:Landroid/graphics/Paint;

    iget v1, p0, LX/Fwe;->b:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2303436
    return-void
.end method
