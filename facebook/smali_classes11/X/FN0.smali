.class public LX/FN0;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "LX/FMM;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/FMw;

.field public c:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/FMB;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/2Or;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/2Uq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMu;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2231815
    sget-object v0, LX/FMM;->NO_CONNECTION:LX/FMM;

    const/4 v1, 0x5

    new-array v1, v1, [LX/FMM;

    const/4 v2, 0x0

    sget-object v3, LX/FMM;->CONNECTION_ERROR:LX/FMM;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, LX/FMM;->SERVER_ERROR:LX/FMM;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, LX/FMM;->IO_ERROR:LX/FMM;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    sget-object v3, LX/FMM;->STICKER_FAIL:LX/FMM;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    sget-object v3, LX/FMM;->GENERIC:LX/FMM;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;[Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, LX/FN0;->a:Ljava/util/EnumSet;

    return-void
.end method

.method public constructor <init>(LX/FMw;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2231829
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2231830
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2231831
    iput-object v0, p0, LX/FN0;->h:LX/0Ot;

    .line 2231832
    iput-object p1, p0, LX/FN0;->b:LX/FMw;

    .line 2231833
    return-void
.end method

.method private static a(LX/FN0;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2231834
    iget-object v0, p1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2231835
    invoke-static {v0}, LX/FMQ;->d(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2231836
    :cond_0
    return-void

    .line 2231837
    :cond_1
    invoke-static {v0}, LX/FMQ;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2231838
    iget-object v0, p0, LX/FN0;->d:LX/FMB;

    invoke-virtual {v0, v3}, LX/FMB;->a(Landroid/net/Uri;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v4

    .line 2231839
    if-eqz v4, :cond_0

    .line 2231840
    iget-object v0, v4, Lcom/facebook/messaging/model/messages/Message;->L:Lcom/facebook/messaging/model/mms/MmsData;

    iget-object v5, v0, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v2, v1

    :goto_0
    if-ge v2, v6, :cond_5

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2231841
    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v7, LX/2MK;->PHOTO:LX/2MK;

    if-ne v0, v7, :cond_3

    .line 2231842
    const/4 v0, 0x1

    .line 2231843
    :goto_1
    if-eqz v0, :cond_0

    .line 2231844
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 2231845
    :try_start_0
    iget-object v0, p0, LX/FN0;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMu;

    iget v1, v4, Lcom/facebook/messaging/model/messages/Message;->V:I

    invoke-virtual {v0, v1}, LX/FMu;->b(I)I

    move-result v0

    const v1, 0x4b000

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2231846
    iget-object v1, p0, LX/FN0;->e:Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;

    invoke-virtual {v1, v4, v0, v2}, Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;->a(Lcom/facebook/messaging/model/messages/Message;ILjava/util/HashSet;)LX/EdY;

    move-result-object v1

    .line 2231847
    iget-object v0, p0, LX/FN0;->c:Landroid/content/Context;

    invoke-static {v0}, LX/Edh;->a(Landroid/content/Context;)LX/Edh;

    move-result-object v4

    .line 2231848
    invoke-virtual {v4, v3}, LX/Edh;->a(Landroid/net/Uri;)LX/EdM;

    move-result-object v0

    check-cast v0, LX/Edn;

    .line 2231849
    iput-object v1, v0, LX/EdV;->b:LX/EdY;

    .line 2231850
    const/4 v0, 0x0

    invoke-virtual {v4, v3, v1, v0}, LX/Edh;->a(Landroid/net/Uri;LX/EdY;Ljava/util/Map;)V

    .line 2231851
    iget-object v0, p0, LX/FN0;->d:LX/FMB;

    invoke-virtual {v0, v3}, LX/FMB;->a(Landroid/net/Uri;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    .line 2231852
    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/facebook/messaging/model/messages/Message;->L:Lcom/facebook/messaging/model/mms/MmsData;

    if-eqz v1, :cond_2

    .line 2231853
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->L:Lcom/facebook/messaging/model/mms/MmsData;

    iget-wide v0, v0, Lcom/facebook/messaging/model/mms/MmsData;->c:J

    long-to-int v0, v0

    .line 2231854
    iput v0, p1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->d:I
    :try_end_0
    .catch LX/EdT; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2231855
    :cond_2
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 2231856
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_2

    .line 2231857
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2231858
    :catch_0
    move-exception v0

    .line 2231859
    :try_start_1
    const-string v1, "SendRetryController"

    const-string v3, "Queue mms failed."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v3, v4}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2231860
    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 2231861
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_3

    .line 2231862
    :catchall_0
    move-exception v0

    move-object v1, v0

    invoke-virtual {v2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 2231863
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_4

    .line 2231864
    :cond_4
    throw v1

    :cond_5
    move v0, v1

    goto/16 :goto_1
.end method


# virtual methods
.method public final a(LX/FMM;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)Z
    .locals 2
    .param p2    # Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2231822
    if-eqz p2, :cond_1

    sget-object v0, LX/FN0;->a:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2231823
    iget v0, p2, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->e:I

    move v0, v0

    .line 2231824
    iget-object v1, p0, LX/FN0;->g:LX/2Uq;

    const/4 p0, 0x1

    .line 2231825
    const-string p1, "android_messenger_sms_takeover_retry_on_failure"

    invoke-static {v1, p1}, LX/2Uq;->a(LX/2Uq;Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2231826
    iget-object p1, v1, LX/2Uq;->a:LX/0ad;

    sget p2, LX/6jD;->aa:I

    invoke-interface {p1, p2, p0}, LX/0ad;->a(II)I

    move-result p0

    .line 2231827
    :cond_0
    move v1, p0

    .line 2231828
    if-ge v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(LX/FMM;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V
    .locals 4

    .prologue
    .line 2231816
    sget-object v0, LX/FMM;->IO_ERROR:LX/FMM;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, LX/FN0;->f:LX/2Or;

    .line 2231817
    iget-object v1, v0, LX/2Or;->a:LX/0Uh;

    const/16 v2, 0x632

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    move v0, v1

    .line 2231818
    if-eqz v0, :cond_1

    sget-object v0, LX/FMM;->SERVER_ERROR:LX/FMM;

    if-ne p1, v0, :cond_1

    .line 2231819
    :cond_0
    invoke-static {p0, p2}, LX/FN0;->a(LX/FN0;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V

    .line 2231820
    :cond_1
    iget-object v0, p0, LX/FN0;->b:LX/FMw;

    invoke-virtual {v0, p2}, LX/FMw;->a(Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V

    .line 2231821
    return-void
.end method
