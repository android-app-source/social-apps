.class public LX/GRy;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/GRy;


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2351874
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2351875
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GRy;->a:Z

    .line 2351876
    return-void
.end method

.method public static a(LX/0QB;)LX/GRy;
    .locals 3

    .prologue
    .line 2351877
    sget-object v0, LX/GRy;->b:LX/GRy;

    if-nez v0, :cond_1

    .line 2351878
    const-class v1, LX/GRy;

    monitor-enter v1

    .line 2351879
    :try_start_0
    sget-object v0, LX/GRy;->b:LX/GRy;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2351880
    if-eqz v2, :cond_0

    .line 2351881
    :try_start_1
    new-instance v0, LX/GRy;

    invoke-direct {v0}, LX/GRy;-><init>()V

    .line 2351882
    move-object v0, v0

    .line 2351883
    sput-object v0, LX/GRy;->b:LX/GRy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2351884
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2351885
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2351886
    :cond_1
    sget-object v0, LX/GRy;->b:LX/GRy;

    return-object v0

    .line 2351887
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2351888
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
