.class public final enum LX/FQU;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FQU;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FQU;

.field public static final enum UPDATE_AUTO_APPROVAL:LX/FQU;

.field public static final enum UPDATE_EXPLICIT_APPROVAL:LX/FQU;

.field public static final enum UPDATE_NO_APPROVAL:LX/FQU;


# instance fields
.field private final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2239394
    new-instance v0, LX/FQU;

    const-string v1, "UPDATE_EXPLICIT_APPROVAL"

    invoke-direct {v0, v1, v4, v2}, LX/FQU;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/FQU;->UPDATE_EXPLICIT_APPROVAL:LX/FQU;

    .line 2239395
    new-instance v0, LX/FQU;

    const-string v1, "UPDATE_AUTO_APPROVAL"

    invoke-direct {v0, v1, v2, v3}, LX/FQU;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/FQU;->UPDATE_AUTO_APPROVAL:LX/FQU;

    .line 2239396
    new-instance v0, LX/FQU;

    const-string v1, "UPDATE_NO_APPROVAL"

    invoke-direct {v0, v1, v3, v5}, LX/FQU;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/FQU;->UPDATE_NO_APPROVAL:LX/FQU;

    .line 2239397
    new-array v0, v5, [LX/FQU;

    sget-object v1, LX/FQU;->UPDATE_EXPLICIT_APPROVAL:LX/FQU;

    aput-object v1, v0, v4

    sget-object v1, LX/FQU;->UPDATE_AUTO_APPROVAL:LX/FQU;

    aput-object v1, v0, v2

    sget-object v1, LX/FQU;->UPDATE_NO_APPROVAL:LX/FQU;

    aput-object v1, v0, v3

    sput-object v0, LX/FQU;->$VALUES:[LX/FQU;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2239398
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2239399
    iput p3, p0, LX/FQU;->value:I

    .line 2239400
    return-void
.end method

.method public static fromInt(I)LX/FQU;
    .locals 5

    .prologue
    .line 2239388
    invoke-static {}, LX/FQU;->values()[LX/FQU;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2239389
    iget v4, v0, LX/FQU;->value:I

    if-ne v4, p0, :cond_0

    .line 2239390
    :goto_1
    return-object v0

    .line 2239391
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2239392
    :cond_1
    invoke-static {}, LX/FQU;->getDefault()LX/FQU;

    move-result-object v0

    goto :goto_1
.end method

.method public static getDefault()LX/FQU;
    .locals 1

    .prologue
    .line 2239393
    sget-object v0, LX/FQU;->UPDATE_EXPLICIT_APPROVAL:LX/FQU;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/FQU;
    .locals 1

    .prologue
    .line 2239387
    const-class v0, LX/FQU;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FQU;

    return-object v0
.end method

.method public static values()[LX/FQU;
    .locals 1

    .prologue
    .line 2239386
    sget-object v0, LX/FQU;->$VALUES:[LX/FQU;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FQU;

    return-object v0
.end method


# virtual methods
.method public final asInt()I
    .locals 1

    .prologue
    .line 2239385
    iget v0, p0, LX/FQU;->value:I

    return v0
.end method
