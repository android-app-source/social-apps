.class public final LX/FS3;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2241219
    sget-object v0, LX/6Yx;->b:LX/0U1;

    sget-object v1, LX/6Yx;->d:LX/0U1;

    sget-object v2, LX/6Yx;->f:LX/0U1;

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/FS3;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2241220
    const-string v0, "localphotometadata"

    sget-object v1, LX/FS3;->a:LX/0Px;

    invoke-direct {p0, v0, v1}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 2241221
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 2241222
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2241223
    const-string v0, "localphotometadata"

    const-string v1, "local_photo_metadata_photo_hash_idx"

    sget-object v2, LX/6Yx;->b:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x3e7ee97e

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, 0x32982a9

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2241224
    return-void
.end method
