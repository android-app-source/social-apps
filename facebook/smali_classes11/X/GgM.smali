.class public LX/GgM;
.super Landroid/view/View;
.source ""


# static fields
.field private static a:Landroid/util/SparseIntArray;


# instance fields
.field private b:Landroid/graphics/drawable/Drawable;

.field private c:I

.field private d:I

.field private final e:Landroid/graphics/Paint;

.field public f:Ljava/lang/String;

.field private final g:Landroid/graphics/Rect;

.field private h:I

.field private i:I

.field private final j:Landroid/graphics/Paint;

.field private k:Ljava/lang/String;

.field private l:I

.field private m:I

.field private n:Z

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private final s:Landroid/graphics/Rect;

.field private final t:Landroid/graphics/Rect;

.field private u:I

.field private v:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2379066
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 2379067
    sput-object v0, LX/GgM;->a:Landroid/util/SparseIntArray;

    const/4 v1, 0x1

    const v2, 0x7f0e0660

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 2379068
    sget-object v0, LX/GgM;->a:Landroid/util/SparseIntArray;

    const/4 v1, 0x2

    const v2, 0x7f0e0661

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 2379069
    sget-object v0, LX/GgM;->a:Landroid/util/SparseIntArray;

    const/4 v1, 0x4

    const v2, 0x7f0e0662

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 2379070
    sget-object v0, LX/GgM;->a:Landroid/util/SparseIntArray;

    const/16 v1, 0x10

    const v2, 0x7f0e0663

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 2379071
    sget-object v0, LX/GgM;->a:Landroid/util/SparseIntArray;

    const/16 v1, 0x20

    const v2, 0x7f0e0664

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 2379072
    sget-object v0, LX/GgM;->a:Landroid/util/SparseIntArray;

    const/16 v1, 0x40

    const v2, 0x7f0e0665

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 2379073
    return-void
.end method

.method private static a(II)I
    .locals 2

    .prologue
    .line 2379074
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 2379075
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 2379076
    sparse-switch v1, :sswitch_data_0

    .line 2379077
    :goto_0
    :sswitch_0
    return p0

    .line 2379078
    :sswitch_1
    invoke-static {p0, v0}, Ljava/lang/Math;->min(II)I

    move-result p0

    goto :goto_0

    :sswitch_2
    move p0, v0

    .line 2379079
    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_2
    .end sparse-switch
.end method

.method private a(I[I)V
    .locals 13
    .param p1    # I
        .annotation build Landroid/support/annotation/StyleRes;
        .end annotation
    .end param
    .param p2    # [I
        .annotation build Landroid/support/annotation/StyleableRes;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/CallSuper;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2379086
    invoke-virtual {p0}, LX/GgM;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Landroid/content/Context;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v6

    .line 2379087
    const/4 v4, 0x0

    .line 2379088
    :try_start_0
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->getIndexCount()I

    move-result v7

    move v5, v0

    move v1, v0

    move v2, v0

    move v3, v0

    .line 2379089
    :goto_0
    if-ge v5, v7, :cond_b

    .line 2379090
    invoke-virtual {v6, v5}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v8

    .line 2379091
    const/16 v9, 0x6

    if-ne v8, v9, :cond_1

    .line 2379092
    invoke-virtual {v6, v8}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-static {v8}, LX/1d5;->c(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    iput-object v8, p0, LX/GgM;->b:Landroid/graphics/drawable/Drawable;

    .line 2379093
    :cond_0
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 2379094
    :cond_1
    const/16 v9, 0xa

    if-ne v8, v9, :cond_2

    .line 2379095
    const/4 v4, 0x0

    invoke-virtual {v6, v8, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    goto :goto_1

    .line 2379096
    :cond_2
    const/16 v9, 0x2

    if-ne v8, v9, :cond_3

    .line 2379097
    const/4 v9, 0x0

    invoke-virtual {v6, v8, v9}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v8

    .line 2379098
    iget-object v9, p0, LX/GgM;->e:Landroid/graphics/Paint;

    invoke-virtual {v9, v8}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2379099
    iget-object v9, p0, LX/GgM;->j:Landroid/graphics/Paint;

    invoke-virtual {v9, v8}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2379100
    invoke-static {}, LX/0zj;->a()Landroid/graphics/Rect;

    move-result-object v8

    .line 2379101
    iget-object v9, p0, LX/GgM;->k:Ljava/lang/String;

    invoke-static {v9}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v9

    .line 2379102
    iget-object v10, p0, LX/GgM;->j:Landroid/graphics/Paint;

    iget-object v11, p0, LX/GgM;->k:Ljava/lang/String;

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12, v9, v8}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 2379103
    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    iput v8, p0, LX/GgM;->o:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2379104
    :catchall_0
    move-exception v0

    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    throw v0

    .line 2379105
    :cond_3
    :try_start_1
    const/16 v9, 0x7

    if-ne v8, v9, :cond_4

    .line 2379106
    const/4 v9, 0x0

    invoke-virtual {v6, v8, v9}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v8

    iput v8, p0, LX/GgM;->c:I

    goto :goto_1

    .line 2379107
    :cond_4
    const/16 v9, 0x8

    if-ne v8, v9, :cond_5

    .line 2379108
    const/4 v9, 0x0

    invoke-virtual {v6, v8, v9}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v8

    iput v8, p0, LX/GgM;->d:I

    goto :goto_1

    .line 2379109
    :cond_5
    const/16 v9, 0x9

    if-ne v8, v9, :cond_6

    .line 2379110
    const/4 v9, 0x0

    invoke-virtual {v6, v8, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v8

    iput-boolean v8, p0, LX/GgM;->n:Z

    goto :goto_1

    .line 2379111
    :cond_6
    const/16 v9, 0x5

    if-ne v8, v9, :cond_7

    .line 2379112
    iget-object v9, p0, LX/GgM;->j:Landroid/graphics/Paint;

    const/4 v10, 0x0

    invoke-virtual {v6, v8, v10}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v8

    invoke-virtual {v9, v8}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_1

    .line 2379113
    :cond_7
    const/16 v9, 0x0

    if-ne v8, v9, :cond_8

    .line 2379114
    const/4 v3, 0x0

    invoke-virtual {v6, v8, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    goto :goto_1

    .line 2379115
    :cond_8
    const/16 v9, 0x3

    if-ne v8, v9, :cond_9

    .line 2379116
    const/4 v2, 0x0

    invoke-virtual {v6, v8, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    goto/16 :goto_1

    .line 2379117
    :cond_9
    const/16 v9, 0x1

    if-ne v8, v9, :cond_a

    .line 2379118
    const/4 v1, 0x0

    invoke-virtual {v6, v8, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    goto/16 :goto_1

    .line 2379119
    :cond_a
    const/16 v9, 0x4

    if-ne v8, v9, :cond_0

    .line 2379120
    const/4 v0, 0x0

    invoke-virtual {v6, v8, v0}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    goto/16 :goto_1

    .line 2379121
    :cond_b
    if-nez v3, :cond_c

    if-nez v2, :cond_c

    if-nez v1, :cond_c

    if-eqz v0, :cond_d

    .line 2379122
    :cond_c
    invoke-static {p0, v3, v2, v1, v0}, LX/0vv;->b(Landroid/view/View;IIII)V

    .line 2379123
    :cond_d
    if-eqz v4, :cond_e

    .line 2379124
    iget-object v0, p0, LX/GgM;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, LX/1d5;->a(Landroid/graphics/drawable/Drawable;I)V

    .line 2379125
    iget-object v0, p0, LX/GgM;->e:Landroid/graphics/Paint;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2379126
    :cond_e
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    .line 2379127
    return-void
.end method

.method private b(I)V
    .locals 5

    .prologue
    .line 2379080
    sparse-switch p1, :sswitch_data_0

    .line 2379081
    const/4 v0, 0x0

    :goto_0
    move v0, v0

    .line 2379082
    if-nez v0, :cond_0

    .line 2379083
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "%s doesn\'t support the supplied type: 0x%X"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2379084
    :cond_0
    return-void

    .line 2379085
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_0
        0x12 -> :sswitch_0
        0x14 -> :sswitch_0
        0x21 -> :sswitch_0
        0x22 -> :sswitch_0
        0x24 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2379128
    iget-object v0, p0, LX/GgM;->f:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2379129
    invoke-virtual {p0}, LX/GgM;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2379130
    iget v1, p0, LX/GgM;->u:I

    const/16 v2, 0x42

    if-ne v1, v2, :cond_0

    .line 2379131
    const v1, 0x7f0814de

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/GgM;->f:Ljava/lang/String;

    aput-object v3, v2, v4

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2379132
    :goto_0
    return-object v0

    .line 2379133
    :cond_0
    const v1, 0x7f0814dd

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, LX/GgM;->f:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2379134
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 2379057
    iget v0, p0, LX/GgM;->u:I

    return v0
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 2379058
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2379059
    iget-object v0, p0, LX/GgM;->t:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget-object v1, p0, LX/GgM;->t:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2379060
    iget-object v0, p0, LX/GgM;->f:Ljava/lang/String;

    iget v1, p0, LX/GgM;->h:I

    int-to-float v1, v1

    iget v2, p0, LX/GgM;->i:I

    int-to-float v2, v2

    iget-object v3, p0, LX/GgM;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2379061
    iget-object v0, p0, LX/GgM;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2379062
    iget-boolean v0, p0, LX/GgM;->n:Z

    if-eqz v0, :cond_0

    .line 2379063
    iget-object v0, p0, LX/GgM;->k:Ljava/lang/String;

    iget v1, p0, LX/GgM;->l:I

    int-to-float v1, v1

    iget v2, p0, LX/GgM;->m:I

    int-to-float v2, v2

    iget-object v3, p0, LX/GgM;->j:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2379064
    :cond_0
    iget-object v0, p0, LX/GgM;->t:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    neg-int v0, v0

    int-to-float v0, v0

    iget-object v1, p0, LX/GgM;->t:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2379065
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2379043
    iget-object v0, p0, LX/GgM;->g:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    .line 2379044
    iget-object v0, p0, LX/GgM;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    .line 2379045
    iget-boolean v0, p0, LX/GgM;->v:Z

    if-eqz v0, :cond_1

    invoke-static {p0}, LX/0vv;->n(Landroid/view/View;)I

    move-result v0

    :goto_0
    iput v0, p0, LX/GgM;->h:I

    .line 2379046
    iget-boolean v0, p0, LX/GgM;->v:Z

    if-eqz v0, :cond_2

    iget v0, p0, LX/GgM;->h:I

    add-int/2addr v0, v1

    .line 2379047
    :goto_1
    iget-boolean v1, p0, LX/GgM;->n:Z

    if-eqz v1, :cond_0

    .line 2379048
    iget-boolean v1, p0, LX/GgM;->v:Z

    if-eqz v1, :cond_3

    add-int v1, v0, v2

    iget v2, p0, LX/GgM;->d:I

    add-int/2addr v1, v2

    :goto_2
    iput v1, p0, LX/GgM;->l:I

    .line 2379049
    :cond_0
    iget v1, p0, LX/GgM;->q:I

    invoke-virtual {p0}, LX/GgM;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, p0, LX/GgM;->i:I

    iput v1, p0, LX/GgM;->m:I

    .line 2379050
    iget-object v1, p0, LX/GgM;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, LX/GgM;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, LX/GgM;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {p0}, LX/GgM;->getPaddingTop()I

    move-result v4

    iget-object v5, p0, LX/GgM;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2379051
    iget-object v0, p0, LX/GgM;->s:Landroid/graphics/Rect;

    invoke-virtual {p0}, LX/GgM;->getWidth()I

    move-result v1

    invoke-virtual {p0}, LX/GgM;->getHeight()I

    move-result v2

    invoke-virtual {v0, v6, v6, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 2379052
    iget v0, p0, LX/GgM;->r:I

    iget v1, p0, LX/GgM;->p:I

    iget v2, p0, LX/GgM;->q:I

    iget-object v3, p0, LX/GgM;->s:Landroid/graphics/Rect;

    iget-object v4, p0, LX/GgM;->t:Landroid/graphics/Rect;

    invoke-static {v0, v1, v2, v3, v4}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 2379053
    return-void

    .line 2379054
    :cond_1
    iget v0, p0, LX/GgM;->p:I

    invoke-static {p0}, LX/0vv;->n(Landroid/view/View;)I

    move-result v3

    sub-int/2addr v0, v3

    goto :goto_0

    .line 2379055
    :cond_2
    iget v0, p0, LX/GgM;->h:I

    sub-int/2addr v0, v1

    sub-int/2addr v0, v2

    goto :goto_1

    .line 2379056
    :cond_3
    iget v1, p0, LX/GgM;->d:I

    sub-int v1, v0, v1

    goto :goto_2
.end method

.method public final onMeasure(II)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2379036
    iget-object v0, p0, LX/GgM;->f:Ljava/lang/String;

    invoke-static {v0}, LX/1z7;->a(Ljava/lang/String;)I

    move-result v0

    .line 2379037
    iget-object v2, p0, LX/GgM;->e:Landroid/graphics/Paint;

    iget-object v3, p0, LX/GgM;->f:Ljava/lang/String;

    iget-object v4, p0, LX/GgM;->g:Landroid/graphics/Rect;

    invoke-virtual {v2, v3, v1, v0, v4}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 2379038
    invoke-static {p0}, LX/0vv;->n(Landroid/view/View;)I

    move-result v0

    iget-object v2, p0, LX/GgM;->g:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    add-int/2addr v0, v2

    iget v2, p0, LX/GgM;->c:I

    add-int/2addr v0, v2

    iget-object v2, p0, LX/GgM;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    add-int/2addr v2, v0

    iget-boolean v0, p0, LX/GgM;->n:Z

    if-eqz v0, :cond_1

    iget v0, p0, LX/GgM;->d:I

    :goto_0
    add-int/2addr v0, v2

    iget-boolean v2, p0, LX/GgM;->n:Z

    if-eqz v2, :cond_0

    iget v1, p0, LX/GgM;->o:I

    :cond_0
    add-int/2addr v0, v1

    invoke-static {p0}, LX/0vv;->o(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LX/GgM;->p:I

    .line 2379039
    invoke-virtual {p0}, LX/GgM;->getPaddingTop()I

    move-result v0

    iget-object v1, p0, LX/GgM;->g:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0}, LX/GgM;->getPaddingBottom()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, LX/GgM;->q:I

    .line 2379040
    iget v0, p0, LX/GgM;->p:I

    invoke-static {v0, p1}, LX/GgM;->a(II)I

    move-result v0

    iget v1, p0, LX/GgM;->q:I

    invoke-static {v1, p2}, LX/GgM;->a(II)I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/GgM;->setMeasuredDimension(II)V

    .line 2379041
    return-void

    :cond_1
    move v0, v1

    .line 2379042
    goto :goto_0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 2379032
    check-cast p1, Lcom/facebook/fig/starrating/FigCondensedStarRating$SavedState;

    .line 2379033
    invoke-virtual {p1}, Lcom/facebook/fig/starrating/FigCondensedStarRating$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/View;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 2379034
    iget-object v0, p1, Lcom/facebook/fig/starrating/FigCondensedStarRating$SavedState;->a:Ljava/lang/String;

    iput-object v0, p0, LX/GgM;->f:Ljava/lang/String;

    .line 2379035
    return-void
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 2379027
    invoke-super {p0}, Landroid/view/View;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 2379028
    new-instance v1, Lcom/facebook/fig/starrating/FigCondensedStarRating$SavedState;

    invoke-direct {v1, v0}, Lcom/facebook/fig/starrating/FigCondensedStarRating$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 2379029
    iget-object v0, p0, LX/GgM;->f:Ljava/lang/String;

    .line 2379030
    iput-object v0, v1, Lcom/facebook/fig/starrating/FigCondensedStarRating$SavedState;->a:Ljava/lang/String;

    .line 2379031
    return-object v1
.end method

.method public setRating(F)V
    .locals 4

    .prologue
    const/high16 v3, 0x41200000    # 10.0f

    const/high16 v1, 0x40a00000    # 5.0f

    const/high16 v0, 0x3f800000    # 1.0f

    .line 2379014
    cmpg-float v2, p1, v0

    if-gez v2, :cond_1

    move p1, v0

    .line 2379015
    :cond_0
    :goto_0
    mul-float v1, p1, v3

    .line 2379016
    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    .line 2379017
    div-float/2addr v1, v3

    .line 2379018
    rem-float v0, v1, v0

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-nez v0, :cond_2

    .line 2379019
    float-to-int v0, v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 2379020
    :goto_1
    iput-object v0, p0, LX/GgM;->f:Ljava/lang/String;

    .line 2379021
    invoke-virtual {p0}, LX/GgM;->invalidate()V

    .line 2379022
    invoke-virtual {p0}, LX/GgM;->requestLayout()V

    .line 2379023
    return-void

    .line 2379024
    :cond_1
    cmpl-float v2, p1, v1

    if-lez v2, :cond_0

    move p1, v1

    .line 2379025
    goto :goto_0

    .line 2379026
    :cond_2
    const-string v0, "%.1f"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public setType(I)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 2379009
    invoke-direct {p0, p1}, LX/GgM;->b(I)V

    .line 2379010
    iput p1, p0, LX/GgM;->u:I

    .line 2379011
    sget-object v0, LX/GgM;->a:Landroid/util/SparseIntArray;

    and-int/lit8 v1, p1, 0xf

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    sget-object v1, LX/03r;->FigCondensedStarRatingInternal:[I

    invoke-direct {p0, v0, v1}, LX/GgM;->a(I[I)V

    .line 2379012
    sget-object v0, LX/GgM;->a:Landroid/util/SparseIntArray;

    and-int/lit16 v1, p1, 0xf0

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    sget-object v1, LX/03r;->FigCondensedStarRatingInternal:[I

    invoke-direct {p0, v0, v1}, LX/GgM;->a(I[I)V

    .line 2379013
    return-void
.end method
