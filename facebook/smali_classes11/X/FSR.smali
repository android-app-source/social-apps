.class public LX/FSR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pf;


# instance fields
.field private final a:LX/1QP;

.field private final b:LX/1QM;

.field private final c:LX/1Q7;

.field private final d:LX/1QQ;

.field public final e:LX/FSN;

.field private final f:LX/1QF;

.field private final g:LX/FSQ;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;LX/1PT;LX/1Q6;LX/1Q5;LX/1Q7;LX/1QA;LX/FSN;LX/1QF;LX/FSQ;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2241981
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2241982
    invoke-static {p1}, LX/1Q6;->a(Landroid/content/Context;)LX/1QP;

    move-result-object v0

    iput-object v0, p0, LX/FSR;->a:LX/1QP;

    .line 2241983
    invoke-virtual {p5, p2}, LX/1Q5;->a(Ljava/lang/String;)LX/1QM;

    move-result-object v0

    iput-object v0, p0, LX/FSR;->b:LX/1QM;

    .line 2241984
    iput-object p6, p0, LX/FSR;->c:LX/1Q7;

    .line 2241985
    invoke-static {p3}, LX/1QA;->a(LX/1PT;)LX/1QQ;

    move-result-object v0

    iput-object v0, p0, LX/FSR;->d:LX/1QQ;

    .line 2241986
    iput-object p8, p0, LX/FSR;->e:LX/FSN;

    .line 2241987
    iput-object p9, p0, LX/FSR;->f:LX/1QF;

    .line 2241988
    iput-object p10, p0, LX/FSR;->g:LX/FSQ;

    .line 2241989
    return-void
.end method


# virtual methods
.method public final a()LX/1Q9;
    .locals 1

    .prologue
    .line 2241990
    iget-object v0, p0, LX/FSR;->c:LX/1Q7;

    invoke-virtual {v0}, LX/1Q7;->a()LX/1Q9;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)LX/5Oh;
    .locals 6

    .prologue
    .line 2241991
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/FSQ;->a(Ljava/lang/String;Ljava/lang/String;LX/2h7;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;LX/5P5;)LX/5Oh;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1KL;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 2241992
    iget-object v0, p0, LX/FSR;->f:LX/1QF;

    invoke-virtual {v0, p1}, LX/1QF;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1KL;LX/0jW;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;",
            "LX/0jW;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 2241993
    iget-object v0, p0, LX/FSR;->f:LX/1QF;

    invoke-virtual {v0, p1, p2}, LX/1QF;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1R6;)V
    .locals 1

    .prologue
    .line 2241994
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->a(LX/1R6;)V

    .line 2241995
    return-void
.end method

.method public final a(LX/1Rb;)V
    .locals 1

    .prologue
    .line 2241996
    iget-object v0, p0, LX/FSR;->e:LX/FSN;

    invoke-virtual {v0, p1}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->a(LX/1Rb;)V

    .line 2241997
    return-void
.end method

.method public final a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 2241998
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/FSQ;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2241999
    return-void
.end method

.method public final a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 2242000
    iget-object v0, p0, LX/FSR;->e:LX/FSN;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2242001
    return-void
.end method

.method public final a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 2242002
    iget-object v0, p0, LX/FSR;->e:LX/FSN;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2242003
    return-void
.end method

.method public final a(LX/22C;)V
    .locals 1

    .prologue
    .line 2242004
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->a(LX/22C;)V

    .line 2242005
    return-void
.end method

.method public final a(LX/34p;)V
    .locals 1

    .prologue
    .line 2242006
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->a(LX/34p;)V

    .line 2242007
    return-void
.end method

.method public final a(LX/5Oj;)V
    .locals 1

    .prologue
    .line 2242008
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->a(LX/5Oj;)V

    .line 2242009
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2242010
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/FSQ;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2242011
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2242012
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/FSQ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2242013
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 1

    .prologue
    .line 2242014
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2242015
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2242016
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0, p1, p2}, LX/FSQ;->a(Lcom/facebook/graphql/model/GraphQLStoryAttachment;Landroid/view/View;)V

    .line 2242017
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2242018
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->a(Ljava/lang/String;)V

    .line 2242019
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2241960
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0, p1, p2}, LX/FSQ;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2241961
    return-void
.end method

.method public final a([Lcom/facebook/feed/rows/core/props/FeedProps;)V
    .locals 1

    .prologue
    .line 2242020
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->a([Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2242021
    return-void
.end method

.method public final a([Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2242022
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->a([Ljava/lang/Object;)V

    .line 2242023
    return-void
.end method

.method public final a(LX/1KL;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;TT;)Z"
        }
    .end annotation

    .prologue
    .line 2242024
    iget-object v0, p0, LX/FSR;->f:LX/1QF;

    invoke-virtual {v0, p1, p2}, LX/1QF;->a(LX/1KL;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(LX/1R6;)V
    .locals 1

    .prologue
    .line 2242025
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->b(LX/1R6;)V

    .line 2242026
    return-void
.end method

.method public final b(LX/22C;)V
    .locals 1

    .prologue
    .line 2242027
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->b(LX/22C;)V

    .line 2242028
    return-void
.end method

.method public final b(LX/5Oj;)V
    .locals 1

    .prologue
    .line 2242029
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->b(LX/5Oj;)V

    .line 2242030
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V
    .locals 1

    .prologue
    .line 2241979
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->b(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2241980
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2242031
    iget-object v0, p0, LX/FSR;->f:LX/1QF;

    invoke-virtual {v0, p1}, LX/1QF;->b(Ljava/lang/String;)V

    .line 2242032
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2241951
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0, p1, p2}, LX/FSQ;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2241952
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2241958
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->b(Z)V

    .line 2241959
    return-void
.end method

.method public final c()LX/1PT;
    .locals 1

    .prologue
    .line 2241957
    iget-object v0, p0, LX/FSR;->d:LX/1QQ;

    invoke-virtual {v0}, LX/1QQ;->c()LX/1PT;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z
    .locals 1

    .prologue
    .line 2241956
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->c(Lcom/facebook/graphql/model/GraphQLStoryAttachment;)Z

    move-result v0

    return v0
.end method

.method public final e()LX/1SX;
    .locals 1

    .prologue
    .line 2241955
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0}, LX/FSQ;->e()LX/1SX;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 2241954
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0}, LX/FSQ;->f()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public getAnalyticsModule()Ljava/lang/String;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "analyticsModule"
        mode = .enum LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;
    .end annotation

    .prologue
    .line 2241953
    iget-object v0, p0, LX/FSR;->b:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getAnalyticsModule()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 2241950
    iget-object v0, p0, LX/FSR;->a:LX/1QP;

    invoke-virtual {v0}, LX/1QP;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getFontFoundry()LX/1QO;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "fontFoundry"
        mode = .enum LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;
    .end annotation

    .prologue
    .line 2241949
    iget-object v0, p0, LX/FSR;->b:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getFontFoundry()LX/1QO;

    move-result-object v0

    return-object v0
.end method

.method public getNavigator()LX/5KM;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "navigator"
        mode = .enum LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;
    .end annotation

    .prologue
    .line 2241948
    iget-object v0, p0, LX/FSR;->b:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getNavigator()LX/5KM;

    move-result-object v0

    return-object v0
.end method

.method public getTraitCollection()LX/1QN;
    .locals 1
    .annotation runtime Lcom/facebook/java2js/annotation/JSExport;
        as = "traitCollection"
        mode = .enum LX/5SV;->READONLY_PROPERTY_GETTER:LX/5SV;
    .end annotation

    .prologue
    .line 2241947
    iget-object v0, p0, LX/FSR;->b:LX/1QM;

    invoke-virtual {v0}, LX/1QM;->getTraitCollection()LX/1QN;

    move-result-object v0

    return-object v0
.end method

.method public final h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 2241946
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0}, LX/FSQ;->h()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2241962
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0}, LX/FSQ;->i()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;
    .locals 1

    .prologue
    .line 2241963
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0}, LX/FSQ;->iM_()Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    move-result-object v0

    return-object v0
.end method

.method public final iN_()V
    .locals 1

    .prologue
    .line 2241964
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0}, LX/FSQ;->iN_()V

    .line 2241965
    return-void
.end method

.method public final iO_()Z
    .locals 1

    .prologue
    .line 2241966
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0}, LX/FSQ;->iO_()Z

    move-result v0

    return v0
.end method

.method public final j()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2241967
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0}, LX/FSQ;->j()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 2241968
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0}, LX/FSQ;->k()V

    .line 2241969
    return-void
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2241970
    iget-object v0, p0, LX/FSR;->e:LX/FSN;

    invoke-virtual {v0}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->l()Z

    move-result v0

    return v0
.end method

.method public final m()LX/1f9;
    .locals 1

    .prologue
    .line 2241971
    iget-object v0, p0, LX/FSR;->e:LX/FSN;

    invoke-virtual {v0}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->m()LX/1f9;

    move-result-object v0

    return-object v0
.end method

.method public final m_(Z)V
    .locals 1

    .prologue
    .line 2241972
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0, p1}, LX/FSQ;->m_(Z)V

    .line 2241973
    return-void
.end method

.method public final o()LX/1Rb;
    .locals 1

    .prologue
    .line 2241974
    iget-object v0, p0, LX/FSR;->e:LX/FSN;

    invoke-virtual {v0}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->o()LX/1Rb;

    move-result-object v0

    return-object v0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 2241975
    iget-object v0, p0, LX/FSR;->e:LX/FSN;

    invoke-virtual {v0}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->p()V

    .line 2241976
    return-void
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 2241977
    iget-object v0, p0, LX/FSR;->e:LX/FSN;

    invoke-virtual {v0}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->q()Z

    move-result v0

    return v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 2241978
    iget-object v0, p0, LX/FSR;->g:LX/FSQ;

    invoke-virtual {v0}, LX/FSQ;->r()Z

    move-result v0

    return v0
.end method
