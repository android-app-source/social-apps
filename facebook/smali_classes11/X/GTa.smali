.class public final enum LX/GTa;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GTa;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GTa;

.field public static final enum EMPTY_SERVER_DATA:LX/GTa;

.field public static final enum LOCATION_UNAVAILABLE:LX/GTa;

.field public static final enum NOT_ALLOWED_IN_APP:LX/GTa;

.field public static final enum WRONG_NUX_STEP:LX/GTa;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2355616
    new-instance v0, LX/GTa;

    const-string v1, "NOT_ALLOWED_IN_APP"

    invoke-direct {v0, v1, v2}, LX/GTa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GTa;->NOT_ALLOWED_IN_APP:LX/GTa;

    .line 2355617
    new-instance v0, LX/GTa;

    const-string v1, "LOCATION_UNAVAILABLE"

    invoke-direct {v0, v1, v3}, LX/GTa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GTa;->LOCATION_UNAVAILABLE:LX/GTa;

    .line 2355618
    new-instance v0, LX/GTa;

    const-string v1, "EMPTY_SERVER_DATA"

    invoke-direct {v0, v1, v4}, LX/GTa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GTa;->EMPTY_SERVER_DATA:LX/GTa;

    .line 2355619
    new-instance v0, LX/GTa;

    const-string v1, "WRONG_NUX_STEP"

    invoke-direct {v0, v1, v5}, LX/GTa;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GTa;->WRONG_NUX_STEP:LX/GTa;

    .line 2355620
    const/4 v0, 0x4

    new-array v0, v0, [LX/GTa;

    sget-object v1, LX/GTa;->NOT_ALLOWED_IN_APP:LX/GTa;

    aput-object v1, v0, v2

    sget-object v1, LX/GTa;->LOCATION_UNAVAILABLE:LX/GTa;

    aput-object v1, v0, v3

    sget-object v1, LX/GTa;->EMPTY_SERVER_DATA:LX/GTa;

    aput-object v1, v0, v4

    sget-object v1, LX/GTa;->WRONG_NUX_STEP:LX/GTa;

    aput-object v1, v0, v5

    sput-object v0, LX/GTa;->$VALUES:[LX/GTa;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2355613
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GTa;
    .locals 1

    .prologue
    .line 2355615
    const-class v0, LX/GTa;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GTa;

    return-object v0
.end method

.method public static values()[LX/GTa;
    .locals 1

    .prologue
    .line 2355614
    sget-object v0, LX/GTa;->$VALUES:[LX/GTa;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GTa;

    return-object v0
.end method
