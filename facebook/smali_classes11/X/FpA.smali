.class public final LX/FpA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:[Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2291057
    iput-object p1, p0, LX/FpA;->b:Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

    iput-object p2, p0, LX/FpA;->a:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x7ccf5e9b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2291058
    new-instance v1, LX/0ju;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    const v2, 0x7f08331d

    invoke-virtual {v1, v2}, LX/0ju;->a(I)LX/0ju;

    move-result-object v1

    const v2, 0x7f080017

    new-instance v3, LX/Fp9;

    invoke-direct {v3, p0}, LX/Fp9;-><init>(LX/FpA;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    iget-object v2, p0, LX/FpA;->a:[Ljava/lang/String;

    new-instance v3, LX/Fp8;

    invoke-direct {v3, p0}, LX/Fp8;-><init>(LX/FpA;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->a()LX/2EJ;

    move-result-object v1

    .line 2291059
    invoke-virtual {v1}, LX/2EJ;->a()Landroid/widget/ListView;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 2291060
    invoke-virtual {v1}, LX/2EJ;->show()V

    .line 2291061
    const/4 v2, -0x2

    invoke-virtual {v1, v2}, LX/2EJ;->a(I)Landroid/widget/Button;

    move-result-object v1

    iget-object v2, p0, LX/FpA;->b:Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

    invoke-virtual {v2}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00d1

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setTextColor(I)V

    .line 2291062
    const v1, 0x2e317b33

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
