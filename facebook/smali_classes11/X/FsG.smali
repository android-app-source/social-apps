.class public LX/FsG;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/FsG;


# instance fields
.field public final a:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2296685
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2296686
    iput-object p1, p0, LX/FsG;->a:LX/0tX;

    .line 2296687
    return-void
.end method

.method public static a(LX/0QB;)LX/FsG;
    .locals 4

    .prologue
    .line 2296672
    sget-object v0, LX/FsG;->b:LX/FsG;

    if-nez v0, :cond_1

    .line 2296673
    const-class v1, LX/FsG;

    monitor-enter v1

    .line 2296674
    :try_start_0
    sget-object v0, LX/FsG;->b:LX/FsG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2296675
    if-eqz v2, :cond_0

    .line 2296676
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2296677
    new-instance p0, LX/FsG;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-direct {p0, v3}, LX/FsG;-><init>(LX/0tX;)V

    .line 2296678
    move-object v0, p0

    .line 2296679
    sput-object v0, LX/FsG;->b:LX/FsG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2296680
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2296681
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2296682
    :cond_1
    sget-object v0, LX/FsG;->b:LX/FsG;

    return-object v0

    .line 2296683
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2296684
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
