.class public final LX/Gjv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/Gk2;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:I

.field public final synthetic d:I

.field public final synthetic e:LX/0wC;

.field public final synthetic f:Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;


# direct methods
.method public constructor <init>(Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/String;IILX/0wC;)V
    .locals 0

    .prologue
    .line 2388621
    iput-object p1, p0, LX/Gjv;->f:Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

    iput-object p2, p0, LX/Gjv;->a:Lcom/google/common/util/concurrent/SettableFuture;

    iput-object p3, p0, LX/Gjv;->b:Ljava/lang/String;

    iput p4, p0, LX/Gjv;->c:I

    iput p5, p0, LX/Gjv;->d:I

    iput-object p6, p0, LX/Gjv;->e:LX/0wC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 7

    .prologue
    .line 2388622
    instance-of v0, p1, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_0

    .line 2388623
    iget-object v0, p0, LX/Gjv;->f:Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

    iget-object v1, p0, LX/Gjv;->b:Ljava/lang/String;

    iget v2, p0, LX/Gjv;->c:I

    iget v3, p0, LX/Gjv;->d:I

    iget-object v4, p0, LX/Gjv;->e:LX/0wC;

    sget-object v5, LX/0zS;->d:LX/0zS;

    .line 2388624
    invoke-static/range {v0 .. v5}, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->a$redex0(Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;Ljava/lang/String;IILX/0wC;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v0, v6

    .line 2388625
    iget-object v1, p0, LX/Gjv;->f:Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

    iget-object v2, p0, LX/Gjv;->a:Lcom/google/common/util/concurrent/SettableFuture;

    .line 2388626
    new-instance v3, LX/Gjw;

    invoke-direct {v3, v1, v2}, LX/Gjw;-><init>(Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;Lcom/google/common/util/concurrent/SettableFuture;)V

    iget-object v4, v1, Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;->e:LX/0TD;

    invoke-static {v0, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2388627
    :goto_0
    return-void

    .line 2388628
    :cond_0
    iget-object v0, p0, LX/Gjv;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    goto :goto_0
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2388629
    check-cast p1, LX/Gk2;

    .line 2388630
    iget-object v0, p0, LX/Gjv;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, 0x5e1bb272

    invoke-static {v0, p1, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2388631
    return-void
.end method
