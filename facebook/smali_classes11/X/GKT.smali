.class public LX/GKT;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;

.field public b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

.field private c:LX/GN0;


# direct methods
.method public constructor <init>(LX/GN0;)V
    .locals 0
    .param p1    # LX/GN0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2340957
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2340958
    iput-object p1, p0, LX/GKT;->c:LX/GN0;

    .line 2340959
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2340960
    iget-object v0, p0, LX/GKT;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->setVisibility(I)V

    .line 2340961
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    .line 2340942
    iget-object v0, p0, LX/GKT;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;

    iget-object v1, p0, LX/GKT;->c:LX/GN0;

    iget-object v2, p0, LX/GKT;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2340943
    iget-object v3, p0, LX/GHg;->b:LX/GCE;

    move-object v3, v3

    .line 2340944
    iget-object v4, p0, LX/GKT;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1, v2, v3, v4}, LX/GN0;->c(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->setResumeBoostButtonListener(Landroid/view/View$OnClickListener;)V

    .line 2340945
    iget-object v0, p0, LX/GKT;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;

    iget-object v1, p0, LX/GKT;->c:LX/GN0;

    iget-object v2, p0, LX/GKT;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2340946
    iget-object v3, p0, LX/GHg;->b:LX/GCE;

    move-object v3, v3

    .line 2340947
    iget-object v4, p0, LX/GKT;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1, v2, v3, v4}, LX/GN0;->b(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->setPauseBoostButtonListener(Landroid/view/View$OnClickListener;)V

    .line 2340948
    iget-object v0, p0, LX/GKT;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;

    invoke-direct {p0}, LX/GKT;->d()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->setIncreaseBudgetButtonListener(Landroid/view/View$OnClickListener;)V

    .line 2340949
    iget-object v0, p0, LX/GKT;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;

    iget-object v1, p0, LX/GKT;->c:LX/GN0;

    iget-object v2, p0, LX/GKT;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2340950
    iget-object v3, p0, LX/GHg;->b:LX/GCE;

    move-object v3, v3

    .line 2340951
    iget-object v4, p0, LX/GKT;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1, v2, v3, v4}, LX/GN0;->e(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->setBoostAgainButtonListener(Landroid/view/View$OnClickListener;)V

    .line 2340952
    iget-object v0, p0, LX/GKT;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;

    iget-object v1, p0, LX/GKT;->c:LX/GN0;

    .line 2340953
    iget-object v2, p0, LX/GHg;->b:LX/GCE;

    move-object v2, v2

    .line 2340954
    iget-object v3, p0, LX/GKT;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1, v2, v3}, LX/GN0;->b(LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;->setDeleteBoostButtonListener(Landroid/view/View$OnClickListener;)V

    .line 2340955
    return-void
.end method

.method private d()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2340956
    new-instance v0, LX/GKS;

    invoke-direct {v0, p0}, LX/GKS;-><init>(LX/GKT;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2340939
    invoke-super {p0}, LX/GHg;->a()V

    .line 2340940
    const/4 v0, 0x0

    iput-object v0, p0, LX/GKT;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;

    .line 2340941
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2340931
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;

    invoke-virtual {p0, p1, p2}, LX/GKT;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2340937
    iput-object p1, p0, LX/GKT;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2340938
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2340932
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2340933
    iput-object p1, p0, LX/GKT;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesOverviewFooterView;

    .line 2340934
    invoke-direct {p0}, LX/GKT;->b()V

    .line 2340935
    invoke-direct {p0}, LX/GKT;->c()V

    .line 2340936
    return-void
.end method
