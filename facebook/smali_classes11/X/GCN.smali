.class public final LX/GCN;
.super LX/GCM;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)V
    .locals 0

    .prologue
    .line 2328280
    iput-object p1, p0, LX/GCN;->a:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    invoke-direct {p0}, LX/GCM;-><init>()V

    return-void
.end method

.method private a(LX/GFS;)V
    .locals 4

    .prologue
    .line 2328281
    iget-object v0, p1, LX/GFS;->a:Landroid/content/Intent;

    move-object v0, v0

    .line 2328282
    iget-boolean v1, p1, LX/GFS;->d:Z

    move v1, v1

    .line 2328283
    if-eqz v1, :cond_0

    .line 2328284
    iget-object v0, p0, LX/GCN;->a:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    const/4 v1, -0x1

    .line 2328285
    iget-object v2, p1, LX/GFS;->a:Landroid/content/Intent;

    move-object v2, v2

    .line 2328286
    invoke-virtual {v0, v1, v2}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->setResult(ILandroid/content/Intent;)V

    .line 2328287
    iget-object v0, p0, LX/GCN;->a:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->finish()V

    .line 2328288
    :goto_0
    return-void

    .line 2328289
    :cond_0
    iget-boolean v1, p1, LX/GFS;->c:Z

    move v1, v1

    .line 2328290
    if-eqz v1, :cond_1

    .line 2328291
    const-string v1, "flow_id"

    iget-object v2, p0, LX/GCN;->a:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    iget-object v2, v2, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->V:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2328292
    :cond_1
    iget-object v1, p1, LX/GFS;->b:Ljava/lang/Integer;

    move-object v1, v1

    .line 2328293
    if-eqz v1, :cond_2

    .line 2328294
    iget-boolean v1, p1, LX/GFS;->c:Z

    move v1, v1

    .line 2328295
    if-eqz v1, :cond_2

    .line 2328296
    iget-object v1, p0, LX/GCN;->a:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    iget-object v1, v1, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->r:Lcom/facebook/content/SecureContextHelper;

    .line 2328297
    iget-object v2, p1, LX/GFS;->b:Ljava/lang/Integer;

    move-object v2, v2

    .line 2328298
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, LX/GCN;->a:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0

    .line 2328299
    :cond_2
    iget-object v1, p1, LX/GFS;->b:Ljava/lang/Integer;

    move-object v1, v1

    .line 2328300
    if-eqz v1, :cond_3

    .line 2328301
    iget-boolean v1, p1, LX/GFS;->c:Z

    move v1, v1

    .line 2328302
    if-nez v1, :cond_3

    .line 2328303
    iget-object v1, p0, LX/GCN;->a:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    iget-object v1, v1, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->r:Lcom/facebook/content/SecureContextHelper;

    .line 2328304
    iget-object v2, p1, LX/GFS;->b:Ljava/lang/Integer;

    move-object v2, v2

    .line 2328305
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, LX/GCN;->a:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    invoke-interface {v1, v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0

    .line 2328306
    :cond_3
    iget-boolean v1, p1, LX/GFS;->c:Z

    move v1, v1

    .line 2328307
    if-eqz v1, :cond_4

    .line 2328308
    iget-object v1, p0, LX/GCN;->a:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    iget-object v1, v1, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->r:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/GCN;->a:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0

    .line 2328309
    :cond_4
    iget-object v1, p0, LX/GCN;->a:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    iget-object v1, v1, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->r:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/GCN;->a:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 2328310
    check-cast p1, LX/GFS;

    invoke-direct {p0, p1}, LX/GCN;->a(LX/GFS;)V

    return-void
.end method
