.class public LX/G0v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/G0u;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/G0v;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2310511
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2310512
    return-void
.end method

.method public static a(LX/0QB;)LX/G0v;
    .locals 3

    .prologue
    .line 2310513
    sget-object v0, LX/G0v;->a:LX/G0v;

    if-nez v0, :cond_1

    .line 2310514
    const-class v1, LX/G0v;

    monitor-enter v1

    .line 2310515
    :try_start_0
    sget-object v0, LX/G0v;->a:LX/G0v;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2310516
    if-eqz v2, :cond_0

    .line 2310517
    :try_start_1
    new-instance v0, LX/G0v;

    invoke-direct {v0}, LX/G0v;-><init>()V

    .line 2310518
    move-object v0, v0

    .line 2310519
    sput-object v0, LX/G0v;->a:LX/G0v;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2310520
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2310521
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2310522
    :cond_1
    sget-object v0, LX/G0v;->a:LX/G0v;

    return-object v0

    .line 2310523
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2310524
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)V
    .locals 0

    .prologue
    .line 2310510
    return-void
.end method
