.class public LX/FVD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FV3;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/FV3",
        "<",
        "LX/FVH;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/0hy;

.field private final c:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0hy;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2250495
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2250496
    iput-object p1, p0, LX/FVD;->a:Landroid/content/res/Resources;

    .line 2250497
    iput-object p2, p0, LX/FVD;->b:LX/0hy;

    .line 2250498
    iput-object p3, p0, LX/FVD;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2250499
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/FVH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2250494
    const-class v0, LX/FVH;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2250489
    check-cast p1, LX/FVH;

    .line 2250490
    invoke-interface {p1}, LX/FVH;->h()Ljava/lang/String;

    move-result-object v0

    .line 2250491
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2250492
    const/4 v0, 0x0

    .line 2250493
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/FVD;->a:Landroid/content/res/Resources;

    const v2, 0x7f081ae0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Landroid/support/v4/app/Fragment;)Z
    .locals 3

    .prologue
    .line 2250480
    check-cast p1, LX/FVH;

    .line 2250481
    invoke-interface {p1}, LX/FVH;->g()Ljava/lang/String;

    move-result-object v0

    .line 2250482
    new-instance v1, LX/89k;

    invoke-direct {v1}, LX/89k;-><init>()V

    .line 2250483
    iput-object v0, v1, LX/89k;->b:Ljava/lang/String;

    .line 2250484
    move-object v0, v1

    .line 2250485
    invoke-virtual {v0}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v0

    .line 2250486
    iget-object v1, p0, LX/FVD;->b:LX/0hy;

    invoke-interface {v1, v0}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v0

    .line 2250487
    iget-object v1, p0, LX/FVD;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p2}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2250488
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2250479
    iget-object v0, p0, LX/FVD;->a:Landroid/content/res/Resources;

    const v1, 0x7f081acc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2250477
    check-cast p1, LX/FVH;

    .line 2250478
    invoke-interface {p1}, LX/FVH;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation

    .prologue
    .line 2250476
    const-string v0, "view_post"

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2250475
    const v0, 0x7f0217a5

    return v0
.end method
