.class public LX/Fv5;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static r:LX/0Xm;


# instance fields
.field private final a:LX/0ad;

.field private final b:LX/FvH;

.field private final c:LX/FvW;

.field private final d:LX/FvL;

.field private final e:LX/FvE;

.field private final f:LX/Fvx;

.field private final g:LX/Fvj;

.field private final h:LX/Fvr;

.field private final i:LX/Fvv;

.field private final j:LX/8p8;

.field private final k:Z

.field private l:LX/BP0;

.field private m:LX/BQ1;

.field private n:LX/Fvi;

.field private o:LX/FvG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private p:LX/FvD;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private q:LX/Fvw;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0ad;LX/FvH;LX/FvW;LX/FvL;LX/FvE;LX/Fvx;LX/Fvj;LX/Fvr;LX/Fvv;LX/8p8;Ljava/lang/Boolean;)V
    .locals 1
    .param p11    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2300823
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2300824
    iput-object p1, p0, LX/Fv5;->a:LX/0ad;

    .line 2300825
    iput-object p2, p0, LX/Fv5;->b:LX/FvH;

    .line 2300826
    iput-object p3, p0, LX/Fv5;->c:LX/FvW;

    .line 2300827
    iput-object p4, p0, LX/Fv5;->d:LX/FvL;

    .line 2300828
    iput-object p5, p0, LX/Fv5;->e:LX/FvE;

    .line 2300829
    iput-object p6, p0, LX/Fv5;->f:LX/Fvx;

    .line 2300830
    iput-object p7, p0, LX/Fv5;->g:LX/Fvj;

    .line 2300831
    iput-object p8, p0, LX/Fv5;->h:LX/Fvr;

    .line 2300832
    iput-object p9, p0, LX/Fv5;->i:LX/Fvv;

    .line 2300833
    iput-object p10, p0, LX/Fv5;->j:LX/8p8;

    .line 2300834
    invoke-virtual {p11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/Fv5;->k:Z

    .line 2300835
    return-void
.end method

.method public static a(LX/0QB;)LX/Fv5;
    .locals 15

    .prologue
    .line 2300796
    const-class v1, LX/Fv5;

    monitor-enter v1

    .line 2300797
    :try_start_0
    sget-object v0, LX/Fv5;->r:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2300798
    sput-object v2, LX/Fv5;->r:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2300799
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2300800
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2300801
    new-instance v3, LX/Fv5;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    const-class v5, LX/FvH;

    invoke-interface {v0, v5}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v5

    check-cast v5, LX/FvH;

    const-class v6, LX/FvW;

    invoke-interface {v0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/FvW;

    const-class v7, LX/FvL;

    invoke-interface {v0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/FvL;

    const-class v8, LX/FvE;

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/FvE;

    const-class v9, LX/Fvx;

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/Fvx;

    const-class v10, LX/Fvj;

    invoke-interface {v0, v10}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/Fvj;

    const-class v11, LX/Fvr;

    invoke-interface {v0, v11}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/Fvr;

    const-class v12, LX/Fvv;

    invoke-interface {v0, v12}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/Fvv;

    invoke-static {v0}, LX/8p8;->b(LX/0QB;)LX/8p8;

    move-result-object v13

    check-cast v13, LX/8p8;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v14

    check-cast v14, Ljava/lang/Boolean;

    invoke-direct/range {v3 .. v14}, LX/Fv5;-><init>(LX/0ad;LX/FvH;LX/FvW;LX/FvL;LX/FvE;LX/Fvx;LX/Fvj;LX/Fvr;LX/Fvv;LX/8p8;Ljava/lang/Boolean;)V

    .line 2300802
    move-object v0, v3

    .line 2300803
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2300804
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fv5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2300805
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2300806
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/content/Context;LX/BQ1;LX/G4m;LX/BP0;ZLX/Fv9;)LX/Fvw;
    .locals 8

    .prologue
    .line 2300836
    iget-object v0, p0, LX/Fv5;->g:LX/Fvj;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, LX/Fvj;->a(Landroid/content/Context;LX/BQ1;LX/G4m;LX/BP0;ZLX/Fv9;)LX/Fvi;

    move-result-object v0

    iput-object v0, p0, LX/Fv5;->n:LX/Fvi;

    .line 2300837
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2300838
    iget-object v0, p0, LX/Fv5;->n:LX/Fvi;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2300839
    iget-object v0, p0, LX/Fv5;->j:LX/8p8;

    invoke-virtual {v0}, LX/8p8;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2300840
    iget-object v0, p0, LX/Fv5;->i:LX/Fvv;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, LX/Fvv;->a(Landroid/content/Context;LX/BQ1;LX/G4m;LX/BP0;ZLX/Fv9;)LX/Fvu;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2300841
    :cond_0
    iget-boolean v0, p0, LX/Fv5;->k:Z

    if-nez v0, :cond_1

    .line 2300842
    iget-object v0, p0, LX/Fv5;->h:LX/Fvr;

    invoke-virtual {v0, p1, p2, p4, p5}, LX/Fvr;->a(Landroid/content/Context;LX/BQ1;LX/BP0;Z)LX/Fvq;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2300843
    :cond_1
    iget-object v0, p0, LX/Fv5;->j:LX/8p8;

    invoke-virtual {v0}, LX/8p8;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2300844
    iget-object v0, p0, LX/Fv5;->i:LX/Fvv;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, LX/Fvv;->a(Landroid/content/Context;LX/BQ1;LX/G4m;LX/BP0;ZLX/Fv9;)LX/Fvu;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2300845
    :cond_2
    iget-object v0, p0, LX/Fv5;->n:LX/Fvi;

    .line 2300846
    new-instance v1, LX/Fvg;

    invoke-direct {v1, v0}, LX/Fvg;-><init>(LX/Fvi;)V

    move-object v0, v1

    .line 2300847
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2300848
    iget-object v0, p0, LX/Fv5;->f:LX/Fvx;

    invoke-virtual {v0, v7, p6, p2, p4}, LX/Fvx;->a(Ljava/util/List;LX/Fv9;LX/BQ1;LX/5SB;)LX/Fvw;

    move-result-object v0

    iput-object v0, p0, LX/Fv5;->q:LX/Fvw;

    .line 2300849
    iget-object v0, p0, LX/Fv5;->n:LX/Fvi;

    iget-object v1, p0, LX/Fv5;->q:LX/Fvw;

    .line 2300850
    iput-object v1, v0, LX/Fvi;->t:LX/Fvw;

    .line 2300851
    iget-object v0, p0, LX/Fv5;->q:LX/Fvw;

    return-object v0
.end method

.method private a()Z
    .locals 3

    .prologue
    .line 2300822
    iget-object v0, p0, LX/Fv5;->a:LX/0ad;

    sget-short v1, LX/0wf;->C:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/BP0;LX/FrH;LX/BQ1;LX/G4m;LX/Fv9;Ljava/lang/Boolean;Z)LX/FvG;
    .locals 11

    .prologue
    .line 2300807
    iput-object p2, p0, LX/Fv5;->l:LX/BP0;

    .line 2300808
    iput-object p4, p0, LX/Fv5;->m:LX/BQ1;

    .line 2300809
    iget-object v1, p0, LX/Fv5;->c:LX/FvW;

    move/from16 v0, p8

    invoke-virtual {v1, p1, p2, p4, v0}, LX/FvW;->a(Landroid/content/Context;LX/BP0;LX/BQ1;Z)LX/FvV;

    move-result-object v9

    .line 2300810
    iget-object v1, p0, LX/Fv5;->e:LX/FvE;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p4

    move-object v5, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-virtual/range {v1 .. v8}, LX/FvE;->a(Landroid/content/Context;LX/BP0;LX/BQ1;LX/FrH;LX/G4m;LX/Fv9;Ljava/lang/Boolean;)LX/FvD;

    move-result-object v1

    iput-object v1, p0, LX/Fv5;->p:LX/FvD;

    .line 2300811
    iget-object v1, p0, LX/Fv5;->d:LX/FvL;

    invoke-virtual {v1, p1, p2, p4}, LX/FvL;->a(Landroid/content/Context;LX/BP0;LX/BQ1;)LX/FvK;

    move-result-object v8

    .line 2300812
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 2300813
    invoke-interface {v10, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2300814
    invoke-direct {p0}, LX/Fv5;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2300815
    invoke-interface {v10, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2300816
    :cond_0
    iget-object v1, p0, LX/Fv5;->p:LX/FvD;

    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, p0

    move-object v2, p1

    move-object v3, p4

    move-object/from16 v4, p5

    move-object v5, p2

    move/from16 v6, p8

    move-object/from16 v7, p6

    .line 2300817
    invoke-direct/range {v1 .. v7}, LX/Fv5;->a(Landroid/content/Context;LX/BQ1;LX/G4m;LX/BP0;ZLX/Fv9;)LX/Fvw;

    move-result-object v1

    invoke-interface {v10, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2300818
    invoke-direct {p0}, LX/Fv5;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2300819
    invoke-interface {v10, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2300820
    :cond_1
    invoke-static {p4, v9, v10}, LX/FvH;->a(LX/BQ1;LX/FvV;Ljava/util/List;)LX/FvG;

    move-result-object v1

    iput-object v1, p0, LX/Fv5;->o:LX/FvG;

    .line 2300821
    iget-object v1, p0, LX/Fv5;->o:LX/FvG;

    return-object v1
.end method
