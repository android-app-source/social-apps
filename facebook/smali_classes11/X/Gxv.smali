.class public LX/Gxv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field public final b:Landroid/telephony/TelephonyManager;


# direct methods
.method public constructor <init>(LX/0Zb;Landroid/telephony/TelephonyManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2408685
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2408686
    iput-object p1, p0, LX/Gxv;->a:LX/0Zb;

    .line 2408687
    iput-object p2, p0, LX/Gxv;->b:Landroid/telephony/TelephonyManager;

    .line 2408688
    return-void
.end method

.method public static a(LX/Gxu;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 2408689
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-virtual {p0}, LX/Gxu;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "language_switcher_activity"

    .line 2408690
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2408691
    move-object v0, v0

    .line 2408692
    return-object v0
.end method


# virtual methods
.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2408693
    iget-object v0, p0, LX/Gxv;->a:LX/0Zb;

    sget-object v1, LX/Gxu;->OTHER_LANGUAGES_SELECTED:LX/Gxu;

    invoke-static {v1}, LX/Gxv;->a(LX/Gxu;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const-string v2, "selected_locale"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2408694
    return-void
.end method
