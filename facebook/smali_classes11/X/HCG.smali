.class public final LX/HCG;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/HCg;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/editpage/EditTabOrderFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/editpage/EditTabOrderFragment;)V
    .locals 0

    .prologue
    .line 2438412
    iput-object p1, p0, LX/HCG;->a:Lcom/facebook/pages/common/editpage/EditTabOrderFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2438403
    iget-object v0, p0, LX/HCG;->a:Lcom/facebook/pages/common/editpage/EditTabOrderFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "fail to get reorder tab data when fetched directly in the reorder fragment"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2438404
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2438405
    check-cast p1, LX/HCg;

    .line 2438406
    if-nez p1, :cond_0

    .line 2438407
    iget-object v0, p0, LX/HCG;->a:Lcom/facebook/pages/common/editpage/EditTabOrderFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Fethcing directly in the reorder fragment gets null result"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2438408
    :goto_0
    return-void

    .line 2438409
    :cond_0
    iget-object v0, p0, LX/HCG;->a:Lcom/facebook/pages/common/editpage/EditTabOrderFragment;

    .line 2438410
    iput-object p1, v0, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->h:LX/HCg;

    .line 2438411
    iget-object v0, p0, LX/HCG;->a:Lcom/facebook/pages/common/editpage/EditTabOrderFragment;

    invoke-static {v0}, Lcom/facebook/pages/common/editpage/EditTabOrderFragment;->c(Lcom/facebook/pages/common/editpage/EditTabOrderFragment;)V

    goto :goto_0
.end method
