.class public LX/Fe6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/EQF;
.implements LX/0fu;


# instance fields
.field public final a:LX/Fe5;

.field public final b:LX/Cvq;

.field private final c:LX/CvY;

.field public final d:LX/1Db;

.field public e:LX/Fje;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/EQG;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private g:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field public h:LX/1Cw;

.field public i:Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;

.field public j:LX/0fu;

.field public k:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

.field private l:I

.field private m:Z

.field public n:Z


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/1Cw;LX/Fe5;LX/Cvq;LX/CvY;LX/1Db;)V
    .locals 1
    .param p1    # Lcom/facebook/search/results/model/SearchResultsMutableContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1Cw;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2265171
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2265172
    iput-boolean v0, p0, LX/Fe6;->m:Z

    .line 2265173
    iput-boolean v0, p0, LX/Fe6;->n:Z

    .line 2265174
    iput-object p1, p0, LX/Fe6;->g:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2265175
    iput-object p2, p0, LX/Fe6;->h:LX/1Cw;

    .line 2265176
    iput-object p3, p0, LX/Fe6;->a:LX/Fe5;

    .line 2265177
    iput-object p4, p0, LX/Fe6;->b:LX/Cvq;

    .line 2265178
    iput-object p5, p0, LX/Fe6;->c:LX/CvY;

    .line 2265179
    iput-object p6, p0, LX/Fe6;->d:LX/1Db;

    .line 2265180
    return-void
.end method

.method public static a(LX/Fe6;LX/EQG;)V
    .locals 1

    .prologue
    .line 2265167
    iput-object p1, p0, LX/Fe6;->f:LX/EQG;

    .line 2265168
    iget-object v0, p0, LX/Fe6;->e:LX/Fje;

    if-eqz v0, :cond_0

    .line 2265169
    iget-object v0, p0, LX/Fe6;->e:LX/Fje;

    invoke-virtual {v0, p1}, LX/Fje;->setState(LX/EQG;)V

    .line 2265170
    :cond_0
    return-void
.end method

.method public static a(LX/Fe6;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2265162
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fe6;->n:Z

    .line 2265163
    iget-object v0, p0, LX/Fe6;->a:LX/Fe5;

    iget-object v1, p0, LX/Fe6;->g:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2265164
    new-instance v2, LX/Fe3;

    invoke-direct {v2, v0, p1, p0}, LX/Fe3;-><init>(LX/Fe5;Ljava/lang/String;LX/Fe6;)V

    .line 2265165
    new-instance v3, LX/Fe4;

    invoke-direct {v3, v0, v1, p1}, LX/Fe4;-><init>(LX/Fe5;Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;)V

    invoke-static {v0, v3, v2}, LX/Fe5;->a(LX/Fe5;Ljava/util/concurrent/Callable;LX/2h0;)V

    .line 2265166
    return-void
.end method

.method public static h(LX/Fe6;)V
    .locals 1

    .prologue
    .line 2265159
    iget-object v0, p0, LX/Fe6;->j:LX/0fu;

    if-eqz v0, :cond_0

    .line 2265160
    :goto_0
    return-void

    .line 2265161
    :cond_0
    iget-object v0, p0, LX/Fe6;->b:LX/Cvq;

    invoke-virtual {v0}, LX/Cvq;->e()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 2265152
    iget-boolean v0, p0, LX/Fe6;->m:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/Fe6;->n:Z

    if-eqz v0, :cond_1

    .line 2265153
    :cond_0
    :goto_0
    return-void

    .line 2265154
    :cond_1
    iget-object v0, p0, LX/Fe6;->k:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Fe6;->k:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->n()Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Fe6;->k:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->n()Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Fe6;->k:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->n()Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v0

    :goto_1
    move-object v0, v0

    .line 2265155
    if-eqz v0, :cond_0

    .line 2265156
    iget-object v1, p0, LX/Fe6;->b:LX/Cvq;

    iget-object v2, p0, LX/Fe6;->k:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->o()LX/0Px;

    move-result-object v2

    .line 2265157
    sget-object v3, LX/Cvx;->a:LX/Cvv;

    const/4 v4, 0x0

    invoke-static {v1, v3, v2, v4}, LX/Cvq;->a(LX/Cvq;LX/0Pq;LX/0Px;Z)V

    .line 2265158
    invoke-static {p0, v0}, LX/Fe6;->a(LX/Fe6;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;Z)V
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 2265119
    iput-boolean v4, p0, LX/Fe6;->n:Z

    .line 2265120
    iput-object p1, p0, LX/Fe6;->k:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    .line 2265121
    iget-object v0, p0, LX/Fe6;->g:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    iget-object v1, p0, LX/Fe6;->k:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;)V

    .line 2265122
    iget-object v0, p0, LX/Fe6;->k:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->q()Ljava/lang/String;

    move-result-object v0

    .line 2265123
    iget-object v1, p0, LX/Fe6;->g:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v1, v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->c(Ljava/lang/String;)V

    .line 2265124
    iget-object v0, p0, LX/Fe6;->k:Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->k()Ljava/lang/String;

    move-result-object v0

    .line 2265125
    iget-object v1, p0, LX/Fe6;->g:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v1, v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e(Ljava/lang/String;)V

    .line 2265126
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->n()Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->n()Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->n()Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    .line 2265127
    :goto_0
    if-lez v3, :cond_3

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, LX/Fe6;->m:Z

    .line 2265128
    iget-object v8, p0, LX/Fe6;->c:LX/CvY;

    iget-object v9, p0, LX/Fe6;->g:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->r()Ljava/lang/String;

    move-result-object v10

    iget-object v0, p0, LX/Fe6;->c:LX/CvY;

    iget-object v1, p0, LX/Fe6;->g:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    iget v2, p0, LX/Fe6;->l:I

    add-int/lit8 v6, v2, 0x1

    iput v6, p0, LX/Fe6;->l:I

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;IIILX/0Px;LX/8cg;LX/8cf;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-virtual {v8, v9, v10, v4, v0}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;ILcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2265129
    iget-object v0, p0, LX/Fe6;->i:Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;

    if-eqz v0, :cond_1

    .line 2265130
    iget-object v0, p0, LX/Fe6;->i:Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;

    .line 2265131
    iget-object v1, v0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->s:LX/Fe6;

    iget-object v2, v0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->n:LX/Cvq;

    .line 2265132
    iput-object v2, v1, LX/Fe6;->j:LX/0fu;

    .line 2265133
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->n()Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLGraphSearchQuery;->n()Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsConnection;->a()LX/0Px;

    move-result-object v1

    .line 2265134
    :goto_2
    iget-object v2, v0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->l:LX/CzB;

    .line 2265135
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v6

    const/4 v4, 0x0

    move v5, v4

    :goto_3
    if-ge v5, v6, :cond_0

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLGraphSearchResultsEdge;

    .line 2265136
    iget-object p1, v2, LX/CzB;->a:Ljava/util/List;

    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2265137
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_3

    .line 2265138
    :cond_0
    iget-object v1, v0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->t:LX/1Qq;

    invoke-interface {v1}, LX/1Cw;->notifyDataSetChanged()V

    .line 2265139
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;->w:Z

    .line 2265140
    :cond_1
    if-lez v3, :cond_4

    .line 2265141
    sget-object v0, LX/EQG;->LOADING_MORE:LX/EQG;

    .line 2265142
    :goto_4
    invoke-static {p0, v0}, LX/Fe6;->a(LX/Fe6;LX/EQG;)V

    .line 2265143
    invoke-static {p0}, LX/Fe6;->h(LX/Fe6;)V

    .line 2265144
    return-void

    :cond_2
    move v3, v4

    .line 2265145
    goto :goto_0

    :cond_3
    move v0, v4

    .line 2265146
    goto :goto_1

    .line 2265147
    :cond_4
    if-nez p2, :cond_5

    .line 2265148
    sget-object v0, LX/EQG;->LOADING_FINISHED:LX/EQG;

    goto :goto_4

    .line 2265149
    :cond_5
    sget-object v0, LX/EQG;->EMPTY:LX/EQG;

    goto :goto_4

    .line 2265150
    :cond_6
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2265151
    goto :goto_2
.end method

.method public final ko_()Z
    .locals 1

    .prologue
    .line 2265116
    iget-object v0, p0, LX/Fe6;->j:LX/0fu;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fe6;->j:LX/0fu;

    invoke-interface {v0}, LX/0fu;->ko_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2265117
    const/4 v0, 0x0

    iput-object v0, p0, LX/Fe6;->j:LX/0fu;

    .line 2265118
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
