.class public final LX/GvP;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/katana/gdp/PermissionItem;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/GvU;

.field public final c:LX/0if;

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>(LX/0if;LX/GvU;Landroid/view/View;)V
    .locals 1
    .param p3    # Landroid/view/View;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2405237
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2405238
    invoke-virtual {p2}, LX/GvU;->d()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, LX/GvP;->a:Ljava/util/List;

    .line 2405239
    iput-object p1, p0, LX/GvP;->c:LX/0if;

    .line 2405240
    iput-object p2, p0, LX/GvP;->b:LX/GvU;

    .line 2405241
    iput-object p3, p0, LX/GvP;->d:Landroid/view/View;

    .line 2405242
    return-void
.end method

.method public static e(LX/GvP;I)I
    .locals 1

    .prologue
    .line 2405225
    iget-object v0, p0, LX/GvP;->d:Landroid/view/View;

    if-nez v0, :cond_0

    :goto_0
    return p1

    :cond_0
    add-int/lit8 p1, p1, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2405230
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 2405231
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030794

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2405232
    new-instance v0, LX/GvO;

    invoke-direct {v0, v1}, LX/GvO;-><init>(Landroid/view/View;)V

    .line 2405233
    new-instance v1, LX/GvL;

    invoke-direct {v1, p0, v0}, LX/GvL;-><init>(LX/GvP;LX/GvO;)V

    .line 2405234
    iput-object v1, v0, LX/GvO;->q:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    .line 2405235
    iget-object v1, v0, LX/GvO;->p:Landroid/view/View;

    new-instance v2, LX/GvM;

    invoke-direct {v2, p0, v0}, LX/GvM;-><init>(LX/GvP;LX/GvO;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2405236
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/GvN;

    iget-object v1, p0, LX/GvP;->d:Landroid/view/View;

    invoke-direct {v0, v1}, LX/GvN;-><init>(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final a(LX/1a1;I)V
    .locals 7

    .prologue
    const/16 v2, 0x8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2405243
    invoke-virtual {p0, p2}, LX/GvP;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_0

    .line 2405244
    :goto_0
    return-void

    .line 2405245
    :cond_0
    invoke-static {p0, p2}, LX/GvP;->e(LX/GvP;I)I

    move-result v0

    .line 2405246
    check-cast p1, LX/GvO;

    .line 2405247
    iget-object v1, p0, LX/GvP;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/gdp/PermissionItem;

    .line 2405248
    iget-object v1, p1, LX/GvO;->l:Landroid/widget/TextView;

    .line 2405249
    iget-object v5, v0, Lcom/facebook/katana/gdp/PermissionItem;->c:Ljava/lang/String;

    move-object v5, v5

    .line 2405250
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2405251
    iget-object v1, p1, LX/GvO;->m:Landroid/widget/TextView;

    .line 2405252
    iget-object v5, v0, Lcom/facebook/katana/gdp/PermissionItem;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2405253
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2405254
    iget-boolean v1, v0, Lcom/facebook/katana/gdp/PermissionItem;->a:Z

    move v5, v1

    .line 2405255
    iget-object v6, p1, LX/GvO;->o:Landroid/widget/CompoundButton;

    if-eqz v5, :cond_2

    move v1, v2

    :goto_1
    invoke-virtual {v6, v1}, Landroid/widget/CompoundButton;->setVisibility(I)V

    .line 2405256
    iget-object v1, p1, LX/GvO;->n:Landroid/widget/TextView;

    if-eqz v5, :cond_1

    move v2, v3

    :cond_1
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2405257
    iget-object v2, p1, LX/GvO;->p:Landroid/view/View;

    if-nez v5, :cond_3

    move v1, v4

    :goto_2
    invoke-virtual {v2, v1}, Landroid/view/View;->setClickable(Z)V

    .line 2405258
    iget-object v2, p1, LX/GvO;->p:Landroid/view/View;

    if-nez v5, :cond_4

    move v1, v4

    :goto_3
    invoke-virtual {v2, v1}, Landroid/view/View;->setFocusable(Z)V

    .line 2405259
    iget-object v1, p1, LX/GvO;->p:Landroid/view/View;

    if-nez v5, :cond_5

    :goto_4
    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 2405260
    iget-object v1, p1, LX/GvO;->o:Landroid/widget/CompoundButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2405261
    iget-object v1, p1, LX/GvO;->o:Landroid/widget/CompoundButton;

    .line 2405262
    iget-boolean v2, v0, Lcom/facebook/katana/gdp/PermissionItem;->f:Z

    move v0, v2

    .line 2405263
    invoke-virtual {v1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 2405264
    iget-object v0, p1, LX/GvO;->o:Landroid/widget/CompoundButton;

    .line 2405265
    iget-object v1, p1, LX/GvO;->q:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    move-object v1, v1

    .line 2405266
    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_0

    :cond_2
    move v1, v3

    .line 2405267
    goto :goto_1

    :cond_3
    move v1, v3

    .line 2405268
    goto :goto_2

    :cond_4
    move v1, v3

    .line 2405269
    goto :goto_3

    :cond_5
    move v4, v3

    .line 2405270
    goto :goto_4
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2405227
    iget-object v1, p0, LX/GvP;->d:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 2405228
    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 2405229
    :cond_0
    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2405226
    iget-object v0, p0, LX/GvP;->d:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GvP;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/GvP;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method
