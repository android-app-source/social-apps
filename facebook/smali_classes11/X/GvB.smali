.class public LX/GvB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;

.field private final b:LX/275;

.field private final c:Landroid/content/ComponentName;

.field private final d:LX/2CQ;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0fW;


# direct methods
.method public constructor <init>(Lcom/facebook/content/SecureContextHelper;LX/275;Landroid/content/ComponentName;LX/2CQ;LX/0Or;LX/0fW;)V
    .locals 0
    .param p3    # Landroid/content/ComponentName;
        .annotation runtime Lcom/facebook/katana/login/LogoutActivityComponent;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/275;",
            "Landroid/content/ComponentName;",
            "LX/2CQ;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0fW;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2404842
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2404843
    iput-object p1, p0, LX/GvB;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2404844
    iput-object p2, p0, LX/GvB;->b:LX/275;

    .line 2404845
    iput-object p3, p0, LX/GvB;->c:Landroid/content/ComponentName;

    .line 2404846
    iput-object p4, p0, LX/GvB;->d:LX/2CQ;

    .line 2404847
    iput-object p5, p0, LX/GvB;->e:LX/0Or;

    .line 2404848
    iput-object p6, p0, LX/GvB;->f:LX/0fW;

    .line 2404849
    return-void
.end method

.method public static a(LX/0QB;)LX/GvB;
    .locals 1

    .prologue
    .line 2404850
    invoke-static {p0}, LX/GvB;->b(LX/0QB;)LX/GvB;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/GvB;
    .locals 7

    .prologue
    .line 2404840
    new-instance v0, LX/GvB;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v1

    check-cast v1, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/275;->b(LX/0QB;)LX/275;

    move-result-object v2

    check-cast v2, LX/275;

    invoke-static {p0}, LX/2lQ;->b(LX/0QB;)Landroid/content/ComponentName;

    move-result-object v3

    check-cast v3, Landroid/content/ComponentName;

    invoke-static {p0}, LX/2CQ;->b(LX/0QB;)LX/2CQ;

    move-result-object v4

    check-cast v4, LX/2CQ;

    const/16 v5, 0x12cb

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p0}, LX/0fW;->a(LX/0QB;)LX/0fW;

    move-result-object v6

    check-cast v6, LX/0fW;

    invoke-direct/range {v0 .. v6}, LX/GvB;-><init>(Lcom/facebook/content/SecureContextHelper;LX/275;Landroid/content/ComponentName;LX/2CQ;LX/0Or;LX/0fW;)V

    .line 2404841
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 2404851
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/GvB;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 2404852
    return-void
.end method

.method public final a(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2404838
    iget-object v0, p0, LX/GvB;->b:LX/275;

    invoke-virtual {v0, p1, p2}, LX/275;->a(Landroid/app/Activity;Landroid/os/Bundle;)V

    .line 2404839
    return-void
.end method

.method public final a(Landroid/app/Activity;Z)V
    .locals 3

    .prologue
    .line 2404829
    if-eqz p2, :cond_1

    .line 2404830
    invoke-static {p1}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;)Lcom/facebook/katana/service/AppSession;

    move-result-object v0

    .line 2404831
    iget-object v1, v0, Lcom/facebook/katana/service/AppSession;->e:LX/2A1;

    move-object v1, v1

    .line 2404832
    sget-object v2, LX/2A1;->STATUS_LOGGED_OUT:LX/2A1;

    if-ne v1, v2, :cond_0

    .line 2404833
    invoke-virtual {p0, p1}, LX/GvB;->b(Landroid/app/Activity;)V

    .line 2404834
    :goto_0
    return-void

    .line 2404835
    :cond_0
    new-instance v1, LX/GvA;

    invoke-direct {v1, p0, v0, p1}, LX/GvA;-><init>(LX/GvB;Lcom/facebook/katana/service/AppSession;Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Lcom/facebook/katana/service/AppSession;->a(LX/278;)V

    .line 2404836
    sget-object v1, LX/279;->FORCED_ERROR_INVALID_SESSION:LX/279;

    invoke-virtual {v0, p1, v1}, Lcom/facebook/katana/service/AppSession;->a(Landroid/content/Context;LX/279;)V

    goto :goto_0

    .line 2404837
    :cond_1
    invoke-virtual {p0, p1}, LX/GvB;->b(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2404820
    iget-object v0, p0, LX/GvB;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2404821
    iget-object v1, p0, LX/GvB;->f:LX/0fW;

    iget-object v0, p0, LX/GvB;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2404822
    iget-object v2, v0, Lcom/facebook/user/model/User;->a:Ljava/lang/String;

    move-object v0, v2

    .line 2404823
    invoke-virtual {v1, v0}, LX/0fW;->c(Ljava/lang/String;)V

    .line 2404824
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-object v1, p0, LX/GvB;->c:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 2404825
    iget-object v1, p0, LX/GvB;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2404826
    return-void
.end method

.method public final b(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 2404827
    iget-object v0, p0, LX/GvB;->b:LX/275;

    invoke-virtual {v0, p1}, LX/275;->b(Landroid/app/Activity;)V

    .line 2404828
    return-void
.end method
