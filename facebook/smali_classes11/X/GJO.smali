.class public LX/GJO;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;",
        "Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLCallToActionType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/0ad;

.field private c:Landroid/content/Context;

.field public d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

.field public e:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

.field public f:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

.field public g:Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

.field public h:LX/GCE;

.field private i:LX/GFG;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    .line 2338802
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_DIRECTIONS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CALL_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SAVE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->MESSAGE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->LEARN_MORE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-static/range {v0 .. v5}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/GJO;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2338752
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2338753
    iput-object p1, p0, LX/GJO;->b:LX/0ad;

    .line 2338754
    return-void
.end method

.method public static a(LX/0QB;)LX/GJO;
    .locals 1

    .prologue
    .line 2338803
    invoke-static {p0}, LX/GJO;->b(LX/0QB;)LX/GJO;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;)V
    .locals 1

    .prologue
    .line 2338839
    iput-object p1, p0, LX/GJO;->g:Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    .line 2338840
    iget-object v0, p0, LX/GJO;->g:Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    .line 2338841
    iget-object p0, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v0, p0

    .line 2338842
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2338843
    return-void
.end method

.method private a(Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 2

    .prologue
    .line 2338804
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2338805
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/GJO;->c:Landroid/content/Context;

    .line 2338806
    iput-object p1, p0, LX/GJO;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    .line 2338807
    iget-object v0, p0, LX/GJO;->g:Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    .line 2338808
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v0, v1

    .line 2338809
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->i:Ljava/lang/String;

    move-object v0, v1

    .line 2338810
    if-nez v0, :cond_0

    .line 2338811
    invoke-static {p0}, LX/GJO;->b(LX/GJO;)V

    .line 2338812
    :goto_0
    iput-object p2, p0, LX/GJO;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2338813
    new-instance v0, LX/GJL;

    invoke-direct {v0, p0}, LX/GJL;-><init>(LX/GJO;)V

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->setOnCheckedChangeListener(LX/Bc9;)V

    .line 2338814
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2338815
    iput-object v0, p0, LX/GJO;->h:LX/GCE;

    .line 2338816
    iget-object v0, p0, LX/GJO;->g:Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    .line 2338817
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v0, v1

    .line 2338818
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->i:Ljava/lang/String;

    move-object v0, v1

    .line 2338819
    if-nez v0, :cond_1

    .line 2338820
    iget-object v0, p0, LX/GJO;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    iget-object v1, p0, LX/GJO;->g:Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    .line 2338821
    iget-object p1, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v1, p1

    .line 2338822
    iget-object p1, v1, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-object v1, p1

    .line 2338823
    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->setCallToActionType(Lcom/facebook/graphql/enums/GraphQLCallToActionType;)V

    .line 2338824
    :goto_1
    iget-object v0, p0, LX/GJO;->h:LX/GCE;

    new-instance v1, LX/GJM;

    invoke-direct {v1, p0}, LX/GJM;-><init>(LX/GJO;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2338825
    iget-object v0, p0, LX/GJO;->h:LX/GCE;

    new-instance v1, LX/GJN;

    invoke-direct {v1, p0}, LX/GJN;-><init>(LX/GJO;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2338826
    return-void

    .line 2338827
    :cond_0
    iget-object v0, p0, LX/GJO;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_OFFER:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->a(Lcom/facebook/graphql/enums/GraphQLCallToActionType;)V

    goto :goto_0

    .line 2338828
    :cond_1
    iget-object v0, p0, LX/GJO;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->setCallToActionTypeIndex(I)V

    goto :goto_1
.end method

.method public static a(LX/GCE;Lcom/facebook/graphql/enums/GraphQLCallToActionType;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2338829
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CALL_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne p1, v2, :cond_0

    invoke-virtual {p3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v2

    .line 2338830
    iget v3, v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->d:I

    move v2, v3

    .line 2338831
    const/16 v3, 0x12

    if-ge v2, v3, :cond_0

    .line 2338832
    sget-object v1, LX/GG8;->CALL_NOW_MIN_AGE:LX/GG8;

    invoke-virtual {p0, v1, v0}, LX/GCE;->a(LX/GG8;Z)V

    .line 2338833
    invoke-virtual {p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080b13

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    .line 2338834
    :goto_0
    return v0

    .line 2338835
    :cond_0
    sget-object v0, LX/GG8;->CALL_NOW_MIN_AGE:LX/GG8;

    invoke-virtual {p0, v0, v1}, LX/GCE;->a(LX/GG8;Z)V

    .line 2338836
    if-eqz p2, :cond_1

    .line 2338837
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    :cond_1
    move v0, v1

    .line 2338838
    goto :goto_0
.end method

.method public static a$redex0(LX/GJO;Lcom/facebook/graphql/enums/GraphQLCallToActionType;)V
    .locals 4

    .prologue
    .line 2338777
    iput-object p1, p0, LX/GJO;->f:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2338778
    iget-object v0, p0, LX/GJO;->h:LX/GCE;

    new-instance v1, LX/GFE;

    invoke-direct {v1, p1}, LX/GFE;-><init>(Lcom/facebook/graphql/enums/GraphQLCallToActionType;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2338779
    iget-object v0, p0, LX/GJO;->h:LX/GCE;

    iget-object v1, p0, LX/GJO;->f:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    iget-object v2, p0, LX/GJO;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v3, p0, LX/GJO;->g:Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    invoke-static {v0, v1, v2, v3}, LX/GJO;->a(LX/GCE;Lcom/facebook/graphql/enums/GraphQLCallToActionType;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Z

    move-result v0

    .line 2338780
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GJO;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    if-nez v0, :cond_1

    .line 2338781
    :cond_0
    :goto_0
    return-void

    .line 2338782
    :cond_1
    invoke-static {p1}, LX/GCi;->fromGraphQLTypeCallToAction(Lcom/facebook/graphql/enums/GraphQLCallToActionType;)LX/GCi;

    move-result-object v0

    .line 2338783
    iget-object v1, p0, LX/GJO;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/GCi;->getDescriptionText(Landroid/content/Context;)Landroid/text/Spanned;

    move-result-object v2

    .line 2338784
    iget-object v1, p0, LX/GJO;->g:Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    .line 2338785
    iget-object v3, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v3, v3

    .line 2338786
    iget-object v1, p0, LX/GJO;->g:Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    invoke-virtual {v0, v1}, LX/GCi;->getUri(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Ljava/lang/String;

    move-result-object v1

    .line 2338787
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->LEARN_MORE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne p1, v0, :cond_2

    const/4 v0, 0x1

    .line 2338788
    :goto_1
    iput-object v1, v3, Lcom/facebook/adinterfaces/model/CreativeAdModel;->l:Ljava/lang/String;

    .line 2338789
    iput-object p1, v3, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2338790
    if-eqz v0, :cond_3

    .line 2338791
    :goto_2
    iput-object v1, v3, Lcom/facebook/adinterfaces/model/CreativeAdModel;->d:Ljava/lang/String;

    .line 2338792
    iget-object v1, p0, LX/GJO;->h:LX/GCE;

    new-instance v3, LX/GFz;

    invoke-direct {v3, v0}, LX/GFz;-><init>(Z)V

    invoke-virtual {v1, v3}, LX/GCE;->a(LX/8wN;)V

    .line 2338793
    new-instance v0, LX/GFG;

    invoke-direct {v0}, LX/GFG;-><init>()V

    iput-object v0, p0, LX/GJO;->i:LX/GFG;

    .line 2338794
    iget-object v0, p0, LX/GJO;->h:LX/GCE;

    iget-object v1, p0, LX/GJO;->i:LX/GFG;

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2338795
    iget-object v0, p0, LX/GJO;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    goto :goto_0

    .line 2338796
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2338797
    :cond_3
    iget-object v1, p0, LX/GJO;->g:Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    .line 2338798
    iget-object p1, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->o:Ljava/lang/String;

    move-object v1, p1

    .line 2338799
    goto :goto_2
.end method

.method private static b(LX/0QB;)LX/GJO;
    .locals 2

    .prologue
    .line 2338800
    new-instance v1, LX/GJO;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-direct {v1, v0}, LX/GJO;-><init>(LX/0ad;)V

    .line 2338801
    return-object v1
.end method

.method public static b(LX/GJO;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2338770
    sget-object v0, LX/GJO;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_2

    sget-object v0, LX/GJO;->a:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2338771
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->SAVE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v0, v1, :cond_1

    iget-object v1, p0, LX/GJO;->b:LX/0ad;

    sget-short v5, LX/GDK;->x:S

    invoke-interface {v1, v5, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 2338772
    :goto_1
    invoke-static {v0}, LX/GCi;->fromGraphQLTypeCallToAction(Lcom/facebook/graphql/enums/GraphQLCallToActionType;)LX/GCi;

    move-result-object v5

    iget-object v6, p0, LX/GJO;->g:Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    invoke-virtual {v5, v6}, LX/GCi;->isAvailable(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Z

    move-result v5

    if-eqz v5, :cond_0

    if-nez v1, :cond_0

    .line 2338773
    iget-object v1, p0, LX/GJO;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->a(Lcom/facebook/graphql/enums/GraphQLCallToActionType;)V

    .line 2338774
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 2338775
    goto :goto_1

    .line 2338776
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2338764
    invoke-super {p0}, LX/GHg;->a()V

    .line 2338765
    iput-object v0, p0, LX/GJO;->c:Landroid/content/Context;

    .line 2338766
    iput-object v0, p0, LX/GJO;->d:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2338767
    iput-object v0, p0, LX/GJO;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    .line 2338768
    iput-object v0, p0, LX/GJO;->h:LX/GCE;

    .line 2338769
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2338762
    const-string v0, "cta"

    iget-object v1, p0, LX/GJO;->f:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2338763
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2338761
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    invoke-direct {p0, p1, p2}, LX/GJO;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2338760
    check-cast p1, Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    invoke-direct {p0, p1}, LX/GJO;->a(Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;)V

    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2338755
    if-nez p1, :cond_1

    .line 2338756
    :cond_0
    :goto_0
    return-void

    .line 2338757
    :cond_1
    const-string v0, "cta"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    .line 2338758
    if-eqz v0, :cond_0

    .line 2338759
    iget-object v1, p0, LX/GJO;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCallToActionView;->setCallToActionType(Lcom/facebook/graphql/enums/GraphQLCallToActionType;)V

    goto :goto_0
.end method
