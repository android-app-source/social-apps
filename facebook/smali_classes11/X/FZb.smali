.class public final LX/FZb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/search/fragment/GraphSearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/fragment/GraphSearchFragment;)V
    .locals 0

    .prologue
    .line 2257434
    iput-object p1, p0, LX/FZb;->a:Lcom/facebook/search/fragment/GraphSearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)V
    .locals 1

    .prologue
    .line 2257422
    iget-object v0, p0, LX/FZb;->a:Lcom/facebook/search/fragment/GraphSearchFragment;

    iget-object v0, v0, Lcom/facebook/search/fragment/GraphSearchFragment;->p:LX/FZe;

    invoke-virtual {v0, p1}, LX/FZe;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)V

    .line 2257423
    return-void
.end method

.method public final a(Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;)V
    .locals 3

    .prologue
    .line 2257429
    iget-object v0, p0, LX/FZb;->a:Lcom/facebook/search/fragment/GraphSearchFragment;

    iget-object v0, v0, Lcom/facebook/search/fragment/GraphSearchFragment;->p:LX/FZe;

    .line 2257430
    invoke-virtual {p1}, Lcom/facebook/search/model/PlaceTipsTypeaheadUnit;->k()Ljava/lang/String;

    move-result-object v1

    .line 2257431
    sget-object v2, LX/0ax;->aE:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2257432
    iget-object p0, v0, LX/FZe;->b:LX/17W;

    iget-object p1, v0, LX/FZe;->a:Landroid/content/Context;

    invoke-virtual {p0, p1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2257433
    return-void
.end method

.method public final a(Lcom/facebook/search/model/SeeMoreResultPageUnit;)V
    .locals 2

    .prologue
    .line 2257424
    iget-object v0, p0, LX/FZb;->a:Lcom/facebook/search/fragment/GraphSearchFragment;

    iget-object v0, v0, Lcom/facebook/search/fragment/GraphSearchFragment;->p:LX/FZe;

    .line 2257425
    const/4 v1, 0x0

    .line 2257426
    iget-object p0, p1, Lcom/facebook/search/model/SeeMoreResultPageUnit;->a:Lcom/facebook/search/model/EntityTypeaheadUnit;

    move-object p0, p0

    .line 2257427
    invoke-virtual {v0, v1, p0}, LX/FZe;->a(Lcom/facebook/search/api/GraphSearchQuery;Lcom/facebook/search/model/EntityTypeaheadUnit;)V

    .line 2257428
    return-void
.end method
