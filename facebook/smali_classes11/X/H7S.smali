.class public final LX/H7S;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:I

.field public e:I

.field public f:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:I

.field public h:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Z

.field public j:I

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "videoThumbnails"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "videoThumbnails"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2430495
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2430496
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;
    .locals 13

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v12, 0x0

    .line 2430497
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2430498
    iget-object v1, p0, LX/H7S;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    .line 2430499
    iget-object v3, p0, LX/H7S;->f:Ljava/lang/String;

    invoke-virtual {v0, v3}, LX/186;->b(Ljava/lang/String;)I

    move-result v3

    .line 2430500
    iget-object v5, p0, LX/H7S;->h:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/186;->b(Ljava/lang/String;)I

    move-result v5

    .line 2430501
    iget-object v6, p0, LX/H7S;->k:Ljava/lang/String;

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2430502
    iget-object v7, p0, LX/H7S;->l:Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel$TitleModel;

    invoke-static {v0, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2430503
    iget-object v8, p0, LX/H7S;->m:Ljava/lang/String;

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    .line 2430504
    sget-object v9, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v9

    :try_start_0
    iget-object v10, p0, LX/H7S;->n:LX/15i;

    iget v11, p0, LX/H7S;->o:I

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v9, 0x25ccafee

    invoke-static {v10, v11, v9}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v9

    invoke-static {v0, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2430505
    const/16 v10, 0xf

    invoke-virtual {v0, v10}, LX/186;->c(I)V

    .line 2430506
    iget v10, p0, LX/H7S;->a:I

    invoke-virtual {v0, v12, v10, v12}, LX/186;->a(III)V

    .line 2430507
    iget v10, p0, LX/H7S;->b:I

    invoke-virtual {v0, v4, v10, v12}, LX/186;->a(III)V

    .line 2430508
    const/4 v10, 0x2

    invoke-virtual {v0, v10, v1}, LX/186;->b(II)V

    .line 2430509
    const/4 v1, 0x3

    iget v10, p0, LX/H7S;->d:I

    invoke-virtual {v0, v1, v10, v12}, LX/186;->a(III)V

    .line 2430510
    const/4 v1, 0x4

    iget v10, p0, LX/H7S;->e:I

    invoke-virtual {v0, v1, v10, v12}, LX/186;->a(III)V

    .line 2430511
    const/4 v1, 0x5

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 2430512
    const/4 v1, 0x6

    iget v3, p0, LX/H7S;->g:I

    invoke-virtual {v0, v1, v3, v12}, LX/186;->a(III)V

    .line 2430513
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 2430514
    const/16 v1, 0x8

    iget-boolean v3, p0, LX/H7S;->i:Z

    invoke-virtual {v0, v1, v3}, LX/186;->a(IZ)V

    .line 2430515
    const/16 v1, 0x9

    iget v3, p0, LX/H7S;->j:I

    invoke-virtual {v0, v1, v3, v12}, LX/186;->a(III)V

    .line 2430516
    const/16 v1, 0xa

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2430517
    const/16 v1, 0xb

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 2430518
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v8}, LX/186;->b(II)V

    .line 2430519
    const/16 v1, 0xd

    invoke-virtual {v0, v1, v9}, LX/186;->b(II)V

    .line 2430520
    const/16 v1, 0xe

    iget v3, p0, LX/H7S;->p:I

    invoke-virtual {v0, v1, v3, v12}, LX/186;->a(III)V

    .line 2430521
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 2430522
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2430523
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 2430524
    invoke-virtual {v1, v12}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2430525
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2430526
    new-instance v1, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;

    invoke-direct {v1, v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;-><init>(LX/15i;)V

    .line 2430527
    return-object v1

    .line 2430528
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
