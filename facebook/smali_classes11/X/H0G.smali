.class public final LX/H0G;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/professionalservices/getquote/protocol/GetQuoteGraphQLModels$PageCTAGetQuoteCreateFormMutationModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

.field public final synthetic e:LX/H0I;


# direct methods
.method public constructor <init>(LX/H0I;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V
    .locals 0

    .prologue
    .line 2413700
    iput-object p1, p0, LX/H0G;->e:LX/H0I;

    iput-object p2, p0, LX/H0G;->a:Ljava/lang/String;

    iput-object p3, p0, LX/H0G;->b:Ljava/lang/String;

    iput-object p4, p0, LX/H0G;->c:Ljava/lang/String;

    iput-object p5, p0, LX/H0G;->d:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 12

    .prologue
    .line 2413701
    iget-object v0, p0, LX/H0G;->a:Ljava/lang/String;

    iget-object v1, p0, LX/H0G;->b:Ljava/lang/String;

    iget-object v2, p0, LX/H0G;->c:Ljava/lang/String;

    iget-object v3, p0, LX/H0G;->d:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    .line 2413702
    new-instance v4, LX/4Db;

    invoke-direct {v4}, LX/4Db;-><init>()V

    .line 2413703
    const-string v5, "client_mutation_id"

    invoke-virtual {v4, v5, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2413704
    move-object v4, v4

    .line 2413705
    const-string v5, "actor_id"

    invoke-virtual {v4, v5, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2413706
    move-object v4, v4

    .line 2413707
    const-string v5, "page_id"

    invoke-virtual {v4, v5, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2413708
    move-object v4, v4

    .line 2413709
    const-string v5, "NATIVE_LEAD_GEN_GET_QUOTE"

    .line 2413710
    const-string v6, "flow_type"

    invoke-virtual {v4, v6, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2413711
    move-object v5, v4

    .line 2413712
    iget-object v4, v3, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->b:Ljava/lang/String;

    move-object v4, v4

    .line 2413713
    const-string v6, "form_name"

    invoke-virtual {v5, v6, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2413714
    move-object v4, v5

    .line 2413715
    iget-object v6, v3, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->c:Ljava/lang/String;

    move-object v6, v6

    .line 2413716
    const-string v7, "form_description"

    invoke-virtual {v4, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2413717
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2413718
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->FULL_NAME:Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2413719
    iget-object v4, v3, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->d:Ljava/util/List;

    move-object v4, v4

    .line 2413720
    if-eqz v4, :cond_1

    .line 2413721
    iget-object v4, v3, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->d:Ljava/util/List;

    move-object v4, v4

    .line 2413722
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;

    .line 2413723
    sget-object v8, LX/H01;->a:LX/0Ri;

    invoke-interface {v8}, LX/0Ri;->a_()LX/0Ri;

    move-result-object v8

    .line 2413724
    iget-object v9, v4, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    move-object v9, v9

    .line 2413725
    invoke-interface {v8, v9}, LX/0Ri;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    invoke-static {v8}, LX/0PB;->checkState(Z)V

    .line 2413726
    iget-boolean v8, v4, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->c:Z

    move v8, v8

    .line 2413727
    if-eqz v8, :cond_0

    .line 2413728
    sget-object v8, LX/H01;->a:LX/0Ri;

    invoke-interface {v8}, LX/0Ri;->a_()LX/0Ri;

    move-result-object v8

    .line 2413729
    iget-object v9, v4, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField$FieldType;

    move-object v4, v9

    .line 2413730
    invoke-interface {v8, v4}, LX/0Ri;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoField;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2413731
    :cond_1
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2413732
    iget-object v4, v3, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->e:Ljava/util/List;

    move-object v4, v4

    .line 2413733
    if-eqz v4, :cond_3

    .line 2413734
    iget-object v4, v3, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->e:Ljava/util/List;

    move-object v4, v4

    .line 2413735
    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;

    .line 2413736
    iget-object v9, v4, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->a:Ljava/lang/String;

    move-object v9, v9

    .line 2413737
    invoke-static {v9}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_2

    .line 2413738
    new-instance v9, LX/4Dc;

    invoke-direct {v9}, LX/4Dc;-><init>()V

    .line 2413739
    iget-object v10, v4, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->a:Ljava/lang/String;

    move-object v10, v10

    .line 2413740
    const-string v11, "question_label"

    invoke-virtual {v9, v11, v10}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2413741
    sget-object v10, LX/H01;->b:LX/0Ri;

    invoke-interface {v10}, LX/0Ri;->a_()LX/0Ri;

    move-result-object v10

    .line 2413742
    iget-object v11, v4, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    move-object v11, v11

    .line 2413743
    invoke-interface {v10, v11}, LX/0Ri;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    invoke-static {v10}, LX/0PB;->checkState(Z)V

    .line 2413744
    sget-object v10, LX/H01;->b:LX/0Ri;

    invoke-interface {v10}, LX/0Ri;->a_()LX/0Ri;

    move-result-object v10

    .line 2413745
    iget-object v11, v4, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    move-object v4, v11

    .line 2413746
    invoke-interface {v10, v4}, LX/0Ri;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLLeadGenInfoFieldInputType;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2413747
    const-string v10, "input_type"

    invoke-virtual {v9, v10, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2413748
    invoke-interface {v7, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2413749
    :cond_3
    const-string v4, "user_info_fields"

    invoke-virtual {v5, v4, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2413750
    const-string v4, "customized_questions"

    invoke-virtual {v5, v4, v7}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2413751
    move-object v0, v5

    .line 2413752
    new-instance v1, LX/Gzz;

    invoke-direct {v1}, LX/Gzz;-><init>()V

    move-object v1, v1

    .line 2413753
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2413754
    iget-object v0, p0, LX/H0G;->e:LX/H0I;

    iget-object v0, v0, LX/H0I;->a:LX/0tX;

    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
