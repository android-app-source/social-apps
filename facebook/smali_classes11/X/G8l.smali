.class public final enum LX/G8l;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/G8l;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum eA:LX/G8l;

.field public static final enum eB:LX/G8l;

.field public static final enum eC:LX/G8l;

.field public static final enum eD:LX/G8l;

.field public static final enum eE:LX/G8l;

.field public static final enum eF:LX/G8l;

.field public static final enum eG:LX/G8l;

.field public static final enum eH:LX/G8l;

.field public static final enum eI:LX/G8l;

.field public static final enum eJ:LX/G8l;

.field public static final enum eK:LX/G8l;

.field public static final enum eL:LX/G8l;

.field public static final enum eM:LX/G8l;

.field public static final enum eN:LX/G8l;

.field public static final enum eO:LX/G8l;

.field public static final enum eP:LX/G8l;

.field public static final enum eQ:LX/G8l;

.field public static final enum eR:LX/G8l;

.field public static final enum eS:LX/G8l;

.field public static final enum eT:LX/G8l;

.field public static final enum eU:LX/G8l;

.field public static final enum eV:LX/G8l;

.field public static final enum eW:LX/G8l;

.field public static final enum eX:LX/G8l;

.field public static final enum eY:LX/G8l;

.field public static final enum eZ:LX/G8l;

.field public static final enum et:LX/G8l;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum eu:LX/G8l;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum ev:LX/G8l;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final enum ew:LX/G8l;

.field public static final enum ex:LX/G8l;

.field public static final enum ey:LX/G8l;

.field public static final enum ez:LX/G8l;

.field public static final enum fa:LX/G8l;

.field public static final enum fb:LX/G8l;

.field public static final enum fc:LX/G8l;

.field public static final enum fd:LX/G8l;

.field public static final enum fe:LX/G8l;

.field public static final enum ff:LX/G8l;

.field public static final enum fg:LX/G8l;

.field public static final enum fh:LX/G8l;

.field public static final enum fi:LX/G8l;

.field public static final enum fj:LX/G8l;

.field public static final enum fk:LX/G8l;

.field public static final enum fl:LX/G8l;

.field public static final enum fm:LX/G8l;

.field public static final enum fn:LX/G8l;

.field public static final enum fo:LX/G8l;

.field public static final enum fp:LX/G8l;

.field public static final enum fq:LX/G8l;

.field public static final enum fr:LX/G8l;

.field private static final synthetic ft:[LX/G8l;


# instance fields
.field private final fs:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, LX/G8l;

    const-string v1, "CLIENT_LOGIN_DISABLED"

    const-string v2, "ClientLoginDisabled"

    invoke-direct {v0, v1, v4, v2}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->et:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "DEVICE_MANAGEMENT_REQUIRED"

    const-string v2, "DeviceManagementRequiredOrSyncDisabled"

    invoke-direct {v0, v1, v5, v2}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eu:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "SOCKET_TIMEOUT"

    const-string v2, "SocketTimeout"

    invoke-direct {v0, v1, v6, v2}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->ev:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "SUCCESS"

    const-string v2, "Ok"

    invoke-direct {v0, v1, v7, v2}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->ew:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "UNKNOWN_ERROR"

    const-string v2, "UNKNOWN_ERR"

    invoke-direct {v0, v1, v8, v2}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->ex:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "NETWORK_ERROR"

    const/4 v2, 0x5

    const-string v3, "NetworkError"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->ey:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "SERVICE_UNAVAILABLE"

    const/4 v2, 0x6

    const-string v3, "ServiceUnavailable"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->ez:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "INTNERNAL_ERROR"

    const/4 v2, 0x7

    const-string v3, "InternalError"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eA:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "BAD_AUTHENTICATION"

    const/16 v2, 0x8

    const-string v3, "BadAuthentication"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eB:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "EMPTY_CONSUMER_PKG_OR_SIG"

    const/16 v2, 0x9

    const-string v3, "EmptyConsumerPackageOrSig"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eC:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "NEEDS_2F"

    const/16 v2, 0xa

    const-string v3, "InvalidSecondFactor"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eD:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "NEEDS_POST_SIGN_IN_FLOW"

    const/16 v2, 0xb

    const-string v3, "PostSignInFlowRequired"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eE:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "NEEDS_BROWSER"

    const/16 v2, 0xc

    const-string v3, "NeedsBrowser"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eF:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "UNKNOWN"

    const/16 v2, 0xd

    const-string v3, "Unknown"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eG:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "NOT_VERIFIED"

    const/16 v2, 0xe

    const-string v3, "NotVerified"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eH:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "TERMS_NOT_AGREED"

    const/16 v2, 0xf

    const-string v3, "TermsNotAgreed"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eI:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "ACCOUNT_DISABLED"

    const/16 v2, 0x10

    const-string v3, "AccountDisabled"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eJ:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "CAPTCHA"

    const/16 v2, 0x11

    const-string v3, "CaptchaRequired"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eK:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "ACCOUNT_DELETED"

    const/16 v2, 0x12

    const-string v3, "AccountDeleted"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eL:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "SERVICE_DISABLED"

    const/16 v2, 0x13

    const-string v3, "ServiceDisabled"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eM:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "NEED_PERMISSION"

    const/16 v2, 0x14

    const-string v3, "NeedPermission"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eN:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "INVALID_SCOPE"

    const/16 v2, 0x15

    const-string v3, "INVALID_SCOPE"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eO:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "USER_CANCEL"

    const/16 v2, 0x16

    const-string v3, "UserCancel"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eP:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "PERMISSION_DENIED"

    const/16 v2, 0x17

    const-string v3, "PermissionDenied"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eQ:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "INVALID_AUDIENCE"

    const/16 v2, 0x18

    const-string v3, "INVALID_AUDIENCE"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eR:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "UNREGISTERED_ON_API_CONSOLE"

    const/16 v2, 0x19

    const-string v3, "UNREGISTERED_ON_API_CONSOLE"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eS:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "THIRD_PARTY_DEVICE_MANAGEMENT_REQUIRED"

    const/16 v2, 0x1a

    const-string v3, "ThirdPartyDeviceManagementRequired"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eT:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "DM_INTERNAL_ERROR"

    const/16 v2, 0x1b

    const-string v3, "DeviceManagementInternalError"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eU:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "DM_SYNC_DISABLED"

    const/16 v2, 0x1c

    const-string v3, "DeviceManagementSyncDisabled"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eV:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "DM_ADMIN_BLOCKED"

    const/16 v2, 0x1d

    const-string v3, "DeviceManagementAdminBlocked"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eW:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "DM_ADMIN_PENDING_APPROVAL"

    const/16 v2, 0x1e

    const-string v3, "DeviceManagementAdminPendingApproval"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eX:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "DM_STALE_SYNC_REQUIRED"

    const/16 v2, 0x1f

    const-string v3, "DeviceManagementStaleSyncRequired"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eY:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "DM_DEACTIVATED"

    const/16 v2, 0x20

    const-string v3, "DeviceManagementDeactivated"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->eZ:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "DM_REQUIRED"

    const/16 v2, 0x21

    const-string v3, "DeviceManagementRequired"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->fa:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "ALREADY_HAS_GMAIL"

    const/16 v2, 0x22

    const-string v3, "ALREADY_HAS_GMAIL"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->fb:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "BAD_PASSWORD"

    const/16 v2, 0x23

    const-string v3, "WeakPassword"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->fc:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "BAD_REQUEST"

    const/16 v2, 0x24

    const-string v3, "BadRequest"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->fd:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "BAD_USERNAME"

    const/16 v2, 0x25

    const-string v3, "BadUsername"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->fe:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "DELETED_GMAIL"

    const/16 v2, 0x26

    const-string v3, "DeletedGmail"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->ff:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "EXISTING_USERNAME"

    const/16 v2, 0x27

    const-string v3, "ExistingUsername"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->fg:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "LOGIN_FAIL"

    const/16 v2, 0x28

    const-string v3, "LoginFail"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->fh:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "NOT_LOGGED_IN"

    const/16 v2, 0x29

    const-string v3, "NotLoggedIn"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->fi:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "NO_GMAIL"

    const/16 v2, 0x2a

    const-string v3, "NoGmail"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->fj:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "REQUEST_DENIED"

    const/16 v2, 0x2b

    const-string v3, "RequestDenied"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->fk:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "SERVER_ERROR"

    const/16 v2, 0x2c

    const-string v3, "ServerError"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->fl:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "USERNAME_UNAVAILABLE"

    const/16 v2, 0x2d

    const-string v3, "UsernameUnavailable"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->fm:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "GPLUS_OTHER"

    const/16 v2, 0x2e

    const-string v3, "GPlusOther"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->fn:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "GPLUS_NICKNAME"

    const/16 v2, 0x2f

    const-string v3, "GPlusNickname"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->fo:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "GPLUS_INVALID_CHAR"

    const/16 v2, 0x30

    const-string v3, "GPlusInvalidChar"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->fp:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "GPLUS_INTERSTITIAL"

    const/16 v2, 0x31

    const-string v3, "GPlusInterstitial"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->fq:LX/G8l;

    new-instance v0, LX/G8l;

    const-string v1, "GPLUS_PROFILE_ERROR"

    const/16 v2, 0x32

    const-string v3, "ProfileUpgradeError"

    invoke-direct {v0, v1, v2, v3}, LX/G8l;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/G8l;->fr:LX/G8l;

    const/16 v0, 0x33

    new-array v0, v0, [LX/G8l;

    sget-object v1, LX/G8l;->et:LX/G8l;

    aput-object v1, v0, v4

    sget-object v1, LX/G8l;->eu:LX/G8l;

    aput-object v1, v0, v5

    sget-object v1, LX/G8l;->ev:LX/G8l;

    aput-object v1, v0, v6

    sget-object v1, LX/G8l;->ew:LX/G8l;

    aput-object v1, v0, v7

    sget-object v1, LX/G8l;->ex:LX/G8l;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/G8l;->ey:LX/G8l;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/G8l;->ez:LX/G8l;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/G8l;->eA:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/G8l;->eB:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/G8l;->eC:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/G8l;->eD:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/G8l;->eE:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/G8l;->eF:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/G8l;->eG:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/G8l;->eH:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/G8l;->eI:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/G8l;->eJ:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/G8l;->eK:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/G8l;->eL:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/G8l;->eM:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/G8l;->eN:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/G8l;->eO:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/G8l;->eP:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/G8l;->eQ:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/G8l;->eR:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/G8l;->eS:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/G8l;->eT:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/G8l;->eU:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/G8l;->eV:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/G8l;->eW:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/G8l;->eX:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/G8l;->eY:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/G8l;->eZ:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/G8l;->fa:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/G8l;->fb:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/G8l;->fc:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/G8l;->fd:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, LX/G8l;->fe:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, LX/G8l;->ff:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, LX/G8l;->fg:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, LX/G8l;->fh:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, LX/G8l;->fi:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, LX/G8l;->fj:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, LX/G8l;->fk:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, LX/G8l;->fl:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, LX/G8l;->fm:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, LX/G8l;->fn:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, LX/G8l;->fo:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, LX/G8l;->fp:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, LX/G8l;->fq:LX/G8l;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, LX/G8l;->fr:LX/G8l;

    aput-object v2, v0, v1

    sput-object v0, LX/G8l;->ft:[LX/G8l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, LX/G8l;->fs:Ljava/lang/String;

    return-void
.end method

.method public static values()[LX/G8l;
    .locals 1

    sget-object v0, LX/G8l;->ft:[LX/G8l;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/G8l;

    return-object v0
.end method

.method public static zza(LX/G8l;)Z
    .locals 1

    sget-object v0, LX/G8l;->eB:LX/G8l;

    invoke-virtual {v0, p0}, LX/G8l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/G8l;->eK:LX/G8l;

    invoke-virtual {v0, p0}, LX/G8l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/G8l;->eN:LX/G8l;

    invoke-virtual {v0, p0}, LX/G8l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/G8l;->eF:LX/G8l;

    invoke-virtual {v0, p0}, LX/G8l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/G8l;->eP:LX/G8l;

    invoke-virtual {v0, p0}, LX/G8l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/G8l;->eu:LX/G8l;

    invoke-virtual {v0, p0}, LX/G8l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/G8l;->eU:LX/G8l;

    invoke-virtual {v0, p0}, LX/G8l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/G8l;->eV:LX/G8l;

    invoke-virtual {v0, p0}, LX/G8l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/G8l;->eW:LX/G8l;

    invoke-virtual {v0, p0}, LX/G8l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/G8l;->eX:LX/G8l;

    invoke-virtual {v0, p0}, LX/G8l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/G8l;->eY:LX/G8l;

    invoke-virtual {v0, p0}, LX/G8l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/G8l;->eZ:LX/G8l;

    invoke-virtual {v0, p0}, LX/G8l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/G8l;->fa:LX/G8l;

    invoke-virtual {v0, p0}, LX/G8l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/G8l;->eT:LX/G8l;

    invoke-virtual {v0, p0}, LX/G8l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static zzb(LX/G8l;)Z
    .locals 1

    sget-object v0, LX/G8l;->ey:LX/G8l;

    invoke-virtual {v0, p0}, LX/G8l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/G8l;->ez:LX/G8l;

    invoke-virtual {v0, p0}, LX/G8l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final zzfx(Ljava/lang/String;)LX/G8l;
    .locals 6

    const/4 v1, 0x0

    invoke-static {}, LX/G8l;->values()[LX/G8l;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    iget-object v5, v0, LX/G8l;->fs:Ljava/lang/String;

    invoke-virtual {v5, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_0

    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method
