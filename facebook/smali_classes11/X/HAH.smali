.class public LX/HAH;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/CYI;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/fig/button/FigButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2434978
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2434979
    const-class p1, LX/HAH;

    invoke-static {p1, p0}, LX/HAH;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2434980
    const p1, 0x7f030e53

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2434981
    const p1, 0x7f0d22f2

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/fig/button/FigButton;

    iput-object p1, p0, LX/HAH;->b:Lcom/facebook/fig/button/FigButton;

    .line 2434982
    return-void
.end method

.method private static a(LX/HAH;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2434983
    iget-object v0, p0, LX/HAH;->b:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, p1}, Lcom/facebook/fig/button/FigButton;->setText(Ljava/lang/CharSequence;)V

    .line 2434984
    iget-object v0, p0, LX/HAH;->b:Lcom/facebook/fig/button/FigButton;

    new-instance v1, LX/HAG;

    invoke-direct {v1, p0}, LX/HAG;-><init>(LX/HAH;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2434985
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/HAH;

    invoke-static {p0}, LX/CYI;->b(LX/0QB;)LX/CYI;

    move-result-object p0

    check-cast p0, LX/CYI;

    iput-object p0, p1, LX/HAH;->a:LX/CYI;

    return-void
.end method


# virtual methods
.method public final a(LX/CY7;)V
    .locals 1

    .prologue
    .line 2434986
    iget-object v0, p0, LX/HAH;->a:LX/CYI;

    invoke-virtual {v0, p1}, LX/CYI;->a(LX/CY7;)LX/CY8;

    .line 2434987
    iget-object v0, p1, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/CY7;->h:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/CY7;->h:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2434988
    iget-object v0, p1, LX/CY7;->h:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageAdminCallToActionGraphQLModels$CallToActionAdminConfigModel;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/HAH;->a(LX/HAH;Ljava/lang/String;)V

    .line 2434989
    :goto_0
    return-void

    .line 2434990
    :cond_0
    iget-object v0, p1, LX/CY7;->d:Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;

    invoke-virtual {v0}, Lcom/facebook/pages/common/surface/calltoaction/graphql/PageCallToActionButtonModels$PageCallToActionButtonDataModel;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/HAH;->a(LX/HAH;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setButtonRes(I)V
    .locals 2

    .prologue
    .line 2434991
    if-lez p1, :cond_0

    .line 2434992
    iget-object v0, p0, LX/HAH;->b:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, p1}, Lcom/facebook/fig/button/FigButton;->setGlyph(I)V

    .line 2434993
    :goto_0
    return-void

    .line 2434994
    :cond_0
    iget-object v0, p0, LX/HAH;->b:Lcom/facebook/fig/button/FigButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/FigButton;->setGlyph(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public setButtonType(I)V
    .locals 1

    .prologue
    .line 2434995
    iget-object v0, p0, LX/HAH;->b:Lcom/facebook/fig/button/FigButton;

    invoke-virtual {v0, p1}, Lcom/facebook/fig/button/FigButton;->setType(I)V

    .line 2434996
    return-void
.end method

.method public setLoggingUuid(Landroid/os/ParcelUuid;)V
    .locals 1

    .prologue
    .line 2434997
    iget-object v0, p0, LX/HAH;->a:LX/CYI;

    .line 2434998
    iput-object p1, v0, LX/CYI;->i:Landroid/os/ParcelUuid;

    .line 2434999
    return-void
.end method
