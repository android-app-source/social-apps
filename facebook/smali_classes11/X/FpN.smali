.class public LX/FpN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public a:Landroid/app/Activity;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThreadImmediate;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

.field public e:Landroid/util/DisplayMetrics;

.field public f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2291509
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2291510
    return-void
.end method

.method public static a(LX/0QB;)LX/FpN;
    .locals 6

    .prologue
    .line 2291511
    const-class v1, LX/FpN;

    monitor-enter v1

    .line 2291512
    :try_start_0
    sget-object v0, LX/FpN;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2291513
    sput-object v2, LX/FpN;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2291514
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2291515
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2291516
    new-instance p0, LX/FpN;

    invoke-direct {p0}, LX/FpN;-><init>()V

    .line 2291517
    invoke-static {v0}, LX/0kU;->b(LX/0QB;)Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0W1;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    .line 2291518
    iput-object v3, p0, LX/FpN;->a:Landroid/app/Activity;

    iput-object v4, p0, LX/FpN;->b:LX/0tX;

    iput-object v5, p0, LX/FpN;->c:Ljava/util/concurrent/ExecutorService;

    .line 2291519
    move-object v0, p0

    .line 2291520
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2291521
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FpN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2291522
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2291523
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2291524
    new-instance v0, LX/FoC;

    invoke-direct {v0}, LX/FoC;-><init>()V

    move-object v0, v0

    .line 2291525
    const-string v1, "charity_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "width"

    iget-object v2, p0, LX/FpN;->e:Landroid/util/DisplayMetrics;

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/FoC;

    .line 2291526
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2291527
    iget-object v1, p0, LX/FpN;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/FpM;

    invoke-direct {v1, p0}, LX/FpM;-><init>(LX/FpN;)V

    iget-object v2, p0, LX/FpN;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2291528
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2291529
    new-instance v0, LX/FoB;

    invoke-direct {v0}, LX/FoB;-><init>()V

    move-object v0, v0

    .line 2291530
    const-string v1, "charity_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/FoB;

    .line 2291531
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2291532
    iget-object v1, p0, LX/FpN;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v1, LX/FpL;

    invoke-direct {v1, p0}, LX/FpL;-><init>(LX/FpN;)V

    iget-object v2, p0, LX/FpN;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2291533
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2291534
    iget-object v0, p0, LX/FpN;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FpN;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
