.class public LX/FzB;
.super LX/8Sc;
.source ""

# interfaces
.implements Landroid/widget/SpinnerAdapter;


# instance fields
.field private final e:Landroid/content/res/Resources;

.field public final f:LX/03V;

.field private final g:LX/8Sa;

.field private final h:LX/0wM;

.field public i:LX/8QJ;

.field private j:LX/3R3;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;Landroid/view/LayoutInflater;LX/03V;LX/8Sa;LX/0wM;LX/3R3;)V
    .locals 0

    .prologue
    .line 2307694
    invoke-direct {p0, p1, p3}, LX/8Sc;-><init>(Landroid/content/Context;Landroid/view/LayoutInflater;)V

    .line 2307695
    iput-object p2, p0, LX/FzB;->e:Landroid/content/res/Resources;

    .line 2307696
    iput-object p4, p0, LX/FzB;->f:LX/03V;

    .line 2307697
    iput-object p5, p0, LX/FzB;->g:LX/8Sa;

    .line 2307698
    iput-object p6, p0, LX/FzB;->h:LX/0wM;

    .line 2307699
    iput-object p7, p0, LX/FzB;->j:LX/3R3;

    .line 2307700
    return-void
.end method

.method public static b(LX/FzB;Lcom/facebook/graphql/model/GraphQLPrivacyOption;)I
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 2307701
    const/4 v0, 0x0

    .line 2307702
    iget-object v2, p0, LX/8Sc;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v2

    .line 2307703
    :goto_0
    invoke-interface {v2}, Ljava/util/ListIterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2307704
    add-int/lit8 v3, v0, 0x1

    .line 2307705
    invoke-interface {v2}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/621;

    invoke-interface {v0}, LX/621;->b()Ljava/util/List;

    move-result-object v0

    .line 2307706
    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    .line 2307707
    if-eq v4, v1, :cond_0

    .line 2307708
    add-int v0, v3, v4

    .line 2307709
    :goto_1
    return v0

    .line 2307710
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v0, v3

    .line 2307711
    goto :goto_0

    :cond_1
    move v0, v1

    .line 2307712
    goto :goto_1
.end method


# virtual methods
.method public final a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 2307713
    if-nez p4, :cond_0

    .line 2307714
    iget-object v0, p0, LX/8Sc;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f0308cd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p4

    .line 2307715
    :cond_0
    invoke-virtual {p0, p1, p2}, LX/8Sc;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2307716
    const v1, 0x7f0d16de

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 2307717
    iget-object v2, p0, LX/FzB;->h:LX/0wM;

    iget-object v3, p0, LX/FzB;->g:LX/8Sa;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v4

    sget-object v5, LX/8SZ;->GLYPH:LX/8SZ;

    invoke-virtual {v3, v4, v5}, LX/8Sa;->a(Lcom/facebook/graphql/model/GraphQLImage;LX/8SZ;)I

    move-result v3

    const v4, -0x958e80

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2307718
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2307719
    return-object p4
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2307720
    if-nez p2, :cond_0

    .line 2307721
    iget-object v0, p0, LX/8Sc;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f0308cd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 2307722
    :cond_0
    return-object p2
.end method

.method public final a(LX/8QJ;)V
    .locals 0

    .prologue
    .line 2307723
    iput-object p1, p0, LX/FzB;->i:LX/8QJ;

    .line 2307724
    invoke-super {p0, p1}, LX/8Sc;->a(LX/8QJ;)V

    .line 2307725
    return-void
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 2307726
    if-nez p2, :cond_1

    .line 2307727
    iget-object v0, p0, LX/8Sc;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f0308cc

    invoke-virtual {v0, v1, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 2307728
    :goto_0
    invoke-virtual {p0, p1}, LX/3Tf;->d(I)[I

    move-result-object v0

    .line 2307729
    invoke-virtual {p0, p1}, LX/FzB;->getItemViewType(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 2307730
    aget v0, v0, v3

    invoke-virtual {p0, v0}, LX/8Sc;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/623;

    move-object v1, v2

    .line 2307731
    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, LX/623;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object v0, v2

    .line 2307732
    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v6, v6, v6, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2307733
    :goto_1
    return-object v2

    .line 2307734
    :cond_0
    aget v1, v0, v3

    const/4 v3, 0x1

    aget v0, v0, v3

    invoke-virtual {p0, v1, v0}, LX/8Sc;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-object v1, v2

    .line 2307735
    check-cast v1, Landroid/widget/TextView;

    .line 2307736
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2307737
    iget-object v3, p0, LX/FzB;->h:LX/0wM;

    iget-object v4, p0, LX/FzB;->g:LX/8Sa;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPrivacyOption;->n()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v0

    sget-object v5, LX/8SZ;->GLYPH:LX/8SZ;

    invoke-virtual {v4, v0, v5}, LX/8Sa;->a(Lcom/facebook/graphql/model/GraphQLImage;LX/8SZ;)I

    move-result v0

    invoke-virtual {v1}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v4

    invoke-virtual {v3, v0, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2307738
    invoke-virtual {v1, v0, v6, v6, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_1
    move-object v2, p2

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2307739
    const/4 v0, 0x1

    return v0
.end method
