.class public LX/FFB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FEd;


# instance fields
.field private final a:LX/FF2;

.field public final b:LX/FF4;

.field public final c:LX/FF3;

.field private final d:Landroid/support/v7/widget/RecyclerView;

.field public final e:LX/FEX;


# direct methods
.method public constructor <init>(LX/FF2;LX/FF4;LX/FF3;Landroid/support/v7/widget/RecyclerView;LX/FEX;)V
    .locals 2
    .param p4    # Landroid/support/v7/widget/RecyclerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/FEX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2216928
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2216929
    iput-object p1, p0, LX/FFB;->a:LX/FF2;

    .line 2216930
    iput-object p2, p0, LX/FFB;->b:LX/FF4;

    .line 2216931
    iput-object p3, p0, LX/FFB;->c:LX/FF3;

    .line 2216932
    iput-object p4, p0, LX/FFB;->d:Landroid/support/v7/widget/RecyclerView;

    .line 2216933
    iput-object p5, p0, LX/FFB;->e:LX/FEX;

    .line 2216934
    iget-object v0, p0, LX/FFB;->d:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/FFB;->b:LX/FF4;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2216935
    iget-object v0, p0, LX/FFB;->b:LX/FF4;

    new-instance v1, LX/FF9;

    invoke-direct {v1, p0}, LX/FF9;-><init>(LX/FFB;)V

    .line 2216936
    iput-object v1, v0, LX/FF4;->c:LX/FF9;

    .line 2216937
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2216938
    iget-object v0, p0, LX/FFB;->a:LX/FF2;

    .line 2216939
    iget-object v1, v0, LX/FF2;->b:LX/1Ck;

    const-string p2, "rich_games_list_query"

    invoke-virtual {v1, p2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2216940
    iget-object v0, p0, LX/FFB;->a:LX/FF2;

    new-instance v1, LX/FFA;

    invoke-direct {v1, p0}, LX/FFA;-><init>(LX/FFB;)V

    .line 2216941
    new-instance v2, LX/DeO;

    invoke-direct {v2}, LX/DeO;-><init>()V

    move-object v2, v2

    .line 2216942
    new-instance v3, LX/4GZ;

    invoke-direct {v3}, LX/4GZ;-><init>()V

    .line 2216943
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2216944
    new-instance v4, LX/4Ga;

    invoke-direct {v4}, LX/4Ga;-><init>()V

    .line 2216945
    invoke-virtual {v4, p1}, LX/4Ga;->a(Ljava/lang/String;)LX/4Ga;

    .line 2216946
    invoke-virtual {v3, v4}, LX/4GZ;->a(LX/4Ga;)LX/4GZ;

    .line 2216947
    :cond_0
    new-instance v4, LX/4Gb;

    invoke-direct {v4}, LX/4Gb;-><init>()V

    .line 2216948
    const-string v5, "COMPOSER_LIST"

    invoke-virtual {v4, v5}, LX/4Gb;->a(Ljava/lang/String;)LX/4Gb;

    .line 2216949
    invoke-virtual {v3, v4}, LX/4GZ;->a(LX/4Gb;)LX/4GZ;

    .line 2216950
    const-string v4, "input"

    invoke-virtual {v2, v4, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2216951
    move-object v2, v2

    .line 2216952
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->a:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v4, v5}, LX/0zO;->a(J)LX/0zO;

    move-result-object v2

    .line 2216953
    iget-object v3, v0, LX/FF2;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 2216954
    new-instance v3, LX/FF1;

    invoke-direct {v3, v0, v1}, LX/FF1;-><init>(LX/FF2;LX/FFA;)V

    .line 2216955
    iget-object v4, v0, LX/FF2;->b:LX/1Ck;

    const-string v5, "rich_games_list_query"

    invoke-virtual {v4, v5, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2216956
    return-void
.end method
