.class public final LX/GBi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V
    .locals 0

    .prologue
    .line 2327461
    iput-object p1, p0, LX/GBi;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2327462
    iget-object v0, p0, LX/GBi;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->A:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2327463
    iget-object v0, p0, LX/GBi;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-boolean v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->H:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GBi;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->j:LX/2Dt;

    invoke-virtual {v0}, LX/2Dt;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2327464
    :cond_0
    iget-object v0, p0, LX/GBi;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->r:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->a()V

    .line 2327465
    :cond_1
    iget-object v0, p0, LX/GBi;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->r:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->b()Z

    .line 2327466
    iget-object v0, p0, LX/GBi;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->u:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 2327467
    iget-object v0, p0, LX/GBi;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2327468
    iget-object v0, p0, LX/GBi;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->y:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2327469
    iget-object v0, p0, LX/GBi;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->w:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2327470
    iget-object v0, p0, LX/GBi;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->b:LX/GCA;

    invoke-virtual {v0}, LX/GCA;->a()V

    .line 2327471
    :goto_0
    return-void

    .line 2327472
    :cond_2
    iget-object v0, p0, LX/GBi;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    const-string v1, ""

    .line 2327473
    iput-object v1, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->A:Ljava/lang/String;

    .line 2327474
    iget-object v0, p0, LX/GBi;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->q:LX/2Nh;

    iget-object v1, p0, LX/GBi;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->z:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/2Nh;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
