.class public LX/G2a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0g1;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0g1",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private final a:LX/0W3;

.field public final b:LX/G2f;


# direct methods
.method public constructor <init>(LX/0W3;LX/G2f;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2314309
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2314310
    iput-object p1, p0, LX/G2a;->a:LX/0W3;

    .line 2314311
    iput-object p2, p0, LX/G2a;->b:LX/G2f;

    .line 2314312
    return-void
.end method

.method public static a(LX/0QB;)LX/G2a;
    .locals 5

    .prologue
    .line 2314313
    const-class v1, LX/G2a;

    monitor-enter v1

    .line 2314314
    :try_start_0
    sget-object v0, LX/G2a;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2314315
    sput-object v2, LX/G2a;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2314316
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2314317
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2314318
    new-instance p0, LX/G2a;

    invoke-static {v0}, LX/0W2;->a(LX/0QB;)LX/0W3;

    move-result-object v3

    check-cast v3, LX/0W3;

    .line 2314319
    new-instance v4, LX/G2f;

    invoke-direct {v4}, LX/G2f;-><init>()V

    .line 2314320
    move-object v4, v4

    .line 2314321
    move-object v4, v4

    .line 2314322
    check-cast v4, LX/G2f;

    invoke-direct {p0, v3, v4}, LX/G2a;-><init>(LX/0W3;LX/G2f;)V

    .line 2314323
    move-object v0, p0

    .line 2314324
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2314325
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/G2a;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2314326
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2314327
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2314328
    iget-object v2, p0, LX/G2a;->b:LX/G2f;

    .line 2314329
    iget-boolean v3, v2, LX/G2f;->a:Z

    move v2, v3

    .line 2314330
    if-eqz v2, :cond_3

    .line 2314331
    iget-object v2, p0, LX/G2a;->b:LX/G2f;

    .line 2314332
    iget-boolean v3, v2, LX/G2f;->b:Z

    move v2, v3

    .line 2314333
    if-eqz v2, :cond_2

    .line 2314334
    iget-object v2, p0, LX/G2a;->a:LX/0W3;

    sget-wide v4, LX/0X5;->gX:J

    invoke-interface {v2, v4, v5, v1}, LX/0W4;->a(JZ)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2314335
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 2314336
    goto :goto_0

    .line 2314337
    :cond_2
    iget-object v2, p0, LX/G2a;->a:LX/0W3;

    sget-wide v4, LX/0X5;->gY:J

    invoke-interface {v2, v4, v5, v1}, LX/0W4;->a(JZ)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2314338
    goto :goto_0
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2314339
    if-nez p1, :cond_0

    invoke-direct {p0}, LX/G2a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2314340
    iget-object v0, p0, LX/G2a;->b:LX/G2f;

    return-object v0

    .line 2314341
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final size()I
    .locals 2

    .prologue
    .line 2314342
    const/4 v0, 0x0

    .line 2314343
    invoke-direct {p0}, LX/G2a;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2314344
    const/4 v0, 0x1

    .line 2314345
    :cond_0
    return v0
.end method
