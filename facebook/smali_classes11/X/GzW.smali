.class public final LX/GzW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceNotificationSubscriptionModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2t5;


# direct methods
.method public constructor <init>(LX/2t5;)V
    .locals 0

    .prologue
    .line 2412102
    iput-object p1, p0, LX/GzW;->a:LX/2t5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2412103
    iget-object v0, p0, LX/GzW;->a:LX/2t5;

    iget-object v0, v0, LX/2t5;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, LX/2t5;->a:Ljava/lang/String;

    const-string v2, "Marketplace notif subscription query failed"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2412104
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2412105
    check-cast p1, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceNotificationSubscriptionModel;

    .line 2412106
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceNotificationSubscriptionModel;->a()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2412107
    :cond_0
    iget-object v0, p0, LX/GzW;->a:LX/2t5;

    invoke-static {v0}, LX/2t5;->h(LX/2t5;)V

    .line 2412108
    :goto_0
    return-void

    .line 2412109
    :cond_1
    iget-object v0, p0, LX/GzW;->a:LX/2t5;

    invoke-virtual {p1}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceNotificationSubscriptionModel;->a()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;->a()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$MarketplaceBadgeCountModel;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceNotificationSubscriptionModel;->a()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel;->j()Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$NotifReadnessModel;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, LX/2t5;->a$redex0(LX/2t5;Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$MarketplaceBadgeCountModel;Lcom/facebook/marketplace/badge/MarketplaceBadgeCountQueryModels$MarketplaceBadgeCountFragmentModel$NotifReadnessModel;Z)V

    goto :goto_0
.end method
