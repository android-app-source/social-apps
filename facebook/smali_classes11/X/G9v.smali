.class public final LX/G9v;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2324188
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2324189
    return-void
.end method

.method public static a(LX/G9s;Z)I
    .locals 12

    .prologue
    const/4 v10, 0x5

    const/4 v5, 0x0

    .line 2324161
    if-eqz p1, :cond_0

    .line 2324162
    iget v0, p0, LX/G9s;->c:I

    move v0, v0

    .line 2324163
    move v8, v0

    .line 2324164
    :goto_0
    if-eqz p1, :cond_1

    .line 2324165
    iget v0, p0, LX/G9s;->b:I

    move v0, v0

    .line 2324166
    :goto_1
    iget-object v1, p0, LX/G9s;->a:[[B

    move-object v9, v1

    .line 2324167
    move v7, v5

    move v1, v5

    .line 2324168
    :goto_2
    if-ge v7, v8, :cond_7

    .line 2324169
    const/4 v2, -0x1

    move v4, v5

    move v6, v5

    .line 2324170
    :goto_3
    if-ge v4, v0, :cond_5

    .line 2324171
    if-eqz p1, :cond_2

    aget-object v3, v9, v7

    aget-byte v3, v3, v4

    .line 2324172
    :goto_4
    if-ne v3, v2, :cond_3

    .line 2324173
    add-int/lit8 v3, v6, 0x1

    move v11, v2

    move v2, v3

    move v3, v1

    move v1, v11

    .line 2324174
    :goto_5
    add-int/lit8 v4, v4, 0x1

    move v6, v2

    move v2, v1

    move v1, v3

    goto :goto_3

    .line 2324175
    :cond_0
    iget v0, p0, LX/G9s;->b:I

    move v0, v0

    .line 2324176
    move v8, v0

    goto :goto_0

    .line 2324177
    :cond_1
    iget v0, p0, LX/G9s;->c:I

    move v0, v0

    .line 2324178
    goto :goto_1

    .line 2324179
    :cond_2
    aget-object v3, v9, v4

    aget-byte v3, v3, v7

    goto :goto_4

    .line 2324180
    :cond_3
    if-lt v6, v10, :cond_4

    .line 2324181
    add-int/lit8 v2, v6, -0x5

    add-int/lit8 v2, v2, 0x3

    add-int/2addr v1, v2

    .line 2324182
    :cond_4
    const/4 v2, 0x1

    move v11, v3

    move v3, v1

    move v1, v11

    .line 2324183
    goto :goto_5

    .line 2324184
    :cond_5
    if-lt v6, v10, :cond_6

    .line 2324185
    add-int/lit8 v2, v6, -0x5

    add-int/lit8 v2, v2, 0x3

    add-int/2addr v1, v2

    .line 2324186
    :cond_6
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_2

    .line 2324187
    :cond_7
    return v1
.end method

.method public static a(III)Z
    .locals 3

    .prologue
    .line 2324131
    packed-switch p0, :pswitch_data_0

    .line 2324132
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid mask pattern: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2324133
    :pswitch_0
    add-int v0, p2, p1

    and-int/lit8 v0, v0, 0x1

    .line 2324134
    :goto_0
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    .line 2324135
    :pswitch_1
    and-int/lit8 v0, p2, 0x1

    .line 2324136
    goto :goto_0

    .line 2324137
    :pswitch_2
    rem-int/lit8 v0, p1, 0x3

    goto :goto_0

    .line 2324138
    :pswitch_3
    add-int v0, p2, p1

    rem-int/lit8 v0, v0, 0x3

    goto :goto_0

    .line 2324139
    :pswitch_4
    div-int/lit8 v0, p2, 0x2

    div-int/lit8 v1, p1, 0x3

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x1

    .line 2324140
    goto :goto_0

    .line 2324141
    :pswitch_5
    mul-int v0, p2, p1

    .line 2324142
    and-int/lit8 v1, v0, 0x1

    rem-int/lit8 v0, v0, 0x3

    add-int/2addr v0, v1

    .line 2324143
    goto :goto_0

    .line 2324144
    :pswitch_6
    mul-int v0, p2, p1

    .line 2324145
    and-int/lit8 v1, v0, 0x1

    rem-int/lit8 v0, v0, 0x3

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x1

    .line 2324146
    goto :goto_0

    .line 2324147
    :pswitch_7
    mul-int v0, p2, p1

    .line 2324148
    rem-int/lit8 v0, v0, 0x3

    add-int v1, p2, p1

    and-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    and-int/lit8 v0, v0, 0x1

    .line 2324149
    goto :goto_0

    .line 2324150
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public static a([BII)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2324156
    :goto_0
    if-ge p1, p2, :cond_0

    .line 2324157
    if-ltz p1, :cond_1

    array-length v1, p0

    if-ge p1, v1, :cond_1

    aget-byte v1, p0, p1

    if-ne v1, v0, :cond_1

    .line 2324158
    const/4 v0, 0x0

    .line 2324159
    :cond_0
    return v0

    .line 2324160
    :cond_1
    add-int/lit8 p1, p1, 0x1

    goto :goto_0
.end method

.method public static a([[BIII)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2324151
    :goto_0
    if-ge p2, p3, :cond_0

    .line 2324152
    if-ltz p2, :cond_1

    array-length v1, p0

    if-ge p2, v1, :cond_1

    aget-object v1, p0, p2

    aget-byte v1, v1, p1

    if-ne v1, v0, :cond_1

    .line 2324153
    const/4 v0, 0x0

    .line 2324154
    :cond_0
    return v0

    .line 2324155
    :cond_1
    add-int/lit8 p2, p2, 0x1

    goto :goto_0
.end method
