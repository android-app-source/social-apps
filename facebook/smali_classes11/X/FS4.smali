.class public final LX/FS4;
.super LX/0Tz;
.source ""


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/0U1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    .line 2241225
    sget-object v0, LX/6Yy;->a:LX/0U1;

    sget-object v1, LX/6Yy;->b:LX/0U1;

    sget-object v2, LX/6Yy;->c:LX/0U1;

    sget-object v3, LX/6Yy;->d:LX/0U1;

    sget-object v4, LX/6Yy;->e:LX/0U1;

    sget-object v5, LX/6Yy;->f:LX/0U1;

    sget-object v6, LX/6Yy;->g:LX/0U1;

    sget-object v7, LX/6Yy;->h:LX/0U1;

    sget-object v8, LX/6Yy;->i:LX/0U1;

    sget-object v9, LX/6Yy;->j:LX/0U1;

    sget-object v10, LX/6Yy;->k:LX/0U1;

    invoke-static/range {v0 .. v10}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/FS4;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2241226
    const-string v0, "localphototags"

    sget-object v1, LX/FS4;->a:LX/0Px;

    invoke-direct {p0, v0, v1}, LX/0Tz;-><init>(Ljava/lang/String;LX/0Px;)V

    .line 2241227
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 3

    .prologue
    .line 2241228
    invoke-super {p0, p1}, LX/0Tz;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 2241229
    const-string v0, "localphototags"

    const-string v1, "local_tag_image_hash_idx"

    sget-object v2, LX/6Yy;->k:LX/0U1;

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/0Tz;->a(Ljava/lang/String;Ljava/lang/String;LX/0Px;)Ljava/lang/String;

    move-result-object v0

    const v1, 0x21b533c9

    invoke-static {v1}, LX/03h;->a(I)V

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    const v0, -0x302ea710

    invoke-static {v0}, LX/03h;->a(I)V

    .line 2241230
    return-void
.end method
