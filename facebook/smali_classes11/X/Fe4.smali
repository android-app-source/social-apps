.class public final LX/Fe4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/Fe5;


# direct methods
.method public constructor <init>(LX/Fe5;Lcom/facebook/search/results/model/SearchResultsMutableContext;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2265072
    iput-object p1, p0, LX/Fe4;->c:LX/Fe5;

    iput-object p2, p0, LX/Fe4;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    iput-object p3, p0, LX/Fe4;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2265073
    iget-object v0, p0, LX/Fe4;->c:LX/Fe5;

    iget-object v0, v0, LX/Fe5;->b:LX/FZr;

    iget-object v1, p0, LX/Fe4;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    const/16 v2, 0xa

    iget-object v3, p0, LX/Fe4;->b:Ljava/lang/String;

    .line 2265074
    new-instance v4, LX/9zB;

    invoke-direct {v4}, LX/9zB;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v5

    .line 2265075
    iput-object v5, v4, LX/9zB;->a:Ljava/lang/String;

    .line 2265076
    move-object v4, v4

    .line 2265077
    iput v2, v4, LX/9zB;->d:I

    .line 2265078
    move-object v4, v4

    .line 2265079
    iget-object v5, v1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v5, v5

    .line 2265080
    iget-object v5, v5, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    .line 2265081
    iput-object v5, v4, LX/9zB;->c:Ljava/lang/String;

    .line 2265082
    move-object v4, v4

    .line 2265083
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2265084
    iput-object v3, v4, LX/9zB;->b:Ljava/lang/String;

    .line 2265085
    :cond_0
    const-string v5, "graph_search_query_result_data"

    const-string p0, "FetchGraphSearchResultDataParams"

    .line 2265086
    new-instance v1, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;

    invoke-direct {v1, v4}, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;-><init>(LX/9zB;)V

    move-object v4, v1

    .line 2265087
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2265088
    invoke-virtual {v1, p0, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2265089
    iget-object v2, v0, LX/FZr;->a:LX/0aG;

    const v3, -0x3132e299

    invoke-static {v2, v5, v1, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v1

    invoke-interface {v1}, LX/1MF;->start()LX/1ML;

    move-result-object v1

    move-object v4, v1

    .line 2265090
    move-object v0, v4

    .line 2265091
    return-object v0
.end method
