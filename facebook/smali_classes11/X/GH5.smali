.class public LX/GH5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GGc;
.implements LX/GGk;


# instance fields
.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/GDm;

.field private final k:LX/GCB;

.field public l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;


# direct methods
.method public constructor <init>(LX/GDm;LX/GCB;LX/GMK;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2334703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2334704
    iput-object p1, p0, LX/GH5;->j:LX/GDm;

    .line 2334705
    iput-object p2, p0, LX/GH5;->k:LX/GCB;

    .line 2334706
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    new-instance v1, LX/GEZ;

    const v2, 0x7f03004e

    sget-object v3, LX/GH5;->a:LX/GGX;

    sget-object v4, LX/8wK;->BUDGET:LX/8wK;

    invoke-direct {v1, v2, p3, v3, v4}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/GH5;->i:LX/0Px;

    .line 2334707
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/content/Intent;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2334708
    invoke-static {p1}, LX/GMU;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f080ad6

    .line 2334709
    :goto_0
    sget-object v2, LX/8wL;->BOOSTED_COMPONENT_EDIT_BUDGET:LX/8wL;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p0, v2, v0, v1}, LX/8wJ;->a(Landroid/content/Context;LX/8wL;Ljava/lang/Integer;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    move-object v0, p1

    .line 2334710
    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    move-result-object v3

    .line 2334711
    new-instance v4, LX/A93;

    invoke-direct {v4}, LX/A93;-><init>()V

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v0

    .line 2334712
    :goto_1
    iput-object v0, v4, LX/A93;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    .line 2334713
    move-object v0, v4

    .line 2334714
    new-instance v1, LX/A92;

    invoke-direct {v1}, LX/A92;-><init>()V

    invoke-static {p1}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v4

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    .line 2334715
    iput-object v4, v1, LX/A92;->a:LX/0Px;

    .line 2334716
    move-object v1, v1

    .line 2334717
    invoke-virtual {v1}, LX/A92;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    move-result-object v1

    .line 2334718
    iput-object v1, v0, LX/A93;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountsModel;

    .line 2334719
    move-object v0, v0

    .line 2334720
    invoke-virtual {v0}, LX/A93;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;)V

    .line 2334721
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->i()I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(I)V

    .line 2334722
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    .line 2334723
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->g()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V

    .line 2334724
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b(Ljava/lang/String;)V

    .line 2334725
    const-string v0, "data"

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2334726
    return-object v2

    .line 2334727
    :cond_0
    const v0, 0x7f080ac1

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 2334728
    goto :goto_1
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2334729
    iget-object v0, p0, LX/GH5;->i:LX/0Px;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;LX/GCY;)V
    .locals 1

    .prologue
    .line 2334730
    const-string v0, "data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iput-object v0, p0, LX/GH5;->l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2334731
    iget-object v0, p0, LX/GH5;->l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-interface {p2, v0}, LX/GCY;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2334732
    return-void
.end method

.method public final b()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;
    .locals 1

    .prologue
    .line 2334733
    iget-object v0, p0, LX/GH5;->k:LX/GCB;

    invoke-virtual {v0}, LX/GCB;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/GGh;
    .locals 1

    .prologue
    .line 2334734
    new-instance v0, LX/GH4;

    invoke-direct {v0, p0}, LX/GH4;-><init>(LX/GH5;)V

    return-object v0
.end method
