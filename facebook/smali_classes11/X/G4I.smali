.class public final LX/G4I;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2317390
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2317391
    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2317392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2317393
    const-string v0, "com.facebook.katana.profile.id"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, LX/G4I;->a:J

    .line 2317394
    const-string v0, "profile_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/G4I;->b:Ljava/lang/String;

    .line 2317395
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, LX/G4I;->d:Landroid/os/Bundle;

    .line 2317396
    iget-object v0, p0, LX/G4I;->d:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/G4I;->d:Landroid/os/Bundle;

    const-string v1, "timeline_context_item_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, LX/G4I;->c:Ljava/lang/String;

    .line 2317397
    return-void

    .line 2317398
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/G4J;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2317399
    iget-wide v2, p0, LX/G4I;->a:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2317400
    new-instance v0, LX/G4J;

    invoke-direct {v0, p0}, LX/G4J;-><init>(LX/G4I;)V

    return-object v0

    :cond_0
    move v0, v1

    .line 2317401
    goto :goto_0
.end method
