.class public final LX/GJG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Bc9;


# instance fields
.field public final synthetic a:LX/GJK;


# direct methods
.method public constructor <init>(LX/GJK;)V
    .locals 0

    .prologue
    .line 2338078
    iput-object p1, p0, LX/GJG;->a:LX/GJK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/common/ui/radiobutton/EditableRadioGroup;I)V
    .locals 4

    .prologue
    .line 2338063
    iget-object v0, p0, LX/GJG;->a:LX/GJK;

    iget-object v0, v0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    iget-object v1, p0, LX/GJG;->a:LX/GJK;

    invoke-virtual {v1}, LX/GJK;->b()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    iget-object v2, p0, LX/GJG;->a:LX/GJK;

    iget-object v2, v2, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->g()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V

    .line 2338064
    iget-object v0, p0, LX/GJG;->a:LX/GJK;

    iget-object v0, v0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    iget-object v1, p0, LX/GJG;->a:LX/GJK;

    invoke-static {v1}, LX/GJK;->F(LX/GJK;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(I)V

    .line 2338065
    iget-object v0, p0, LX/GJG;->a:LX/GJK;

    iget-object v0, v0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    iget-object v1, p0, LX/GJG;->a:LX/GJK;

    invoke-static {v1}, LX/GJK;->E(LX/GJK;)I

    move-result v1

    .line 2338066
    iput v1, v0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->k:I

    .line 2338067
    iget-object v0, p0, LX/GJG;->a:LX/GJK;

    .line 2338068
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2338069
    sget-object v1, LX/GG8;->INVALID_BUDGET:LX/GG8;

    iget-object v2, p0, LX/GJG;->a:LX/GJK;

    invoke-virtual {v2}, LX/GJI;->s()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/GCE;->a(LX/GG8;Z)V

    .line 2338070
    iget-object v0, p0, LX/GJG;->a:LX/GJK;

    invoke-virtual {v0}, LX/GJI;->p()V

    .line 2338071
    iget-object v0, p0, LX/GJG;->a:LX/GJK;

    invoke-virtual {v0}, LX/GJI;->t()V

    .line 2338072
    iget-object v0, p0, LX/GJG;->a:LX/GJK;

    invoke-static {v0}, LX/GJK;->B(LX/GJK;)V

    .line 2338073
    iget-object v0, p0, LX/GJG;->a:LX/GJK;

    iget-object v0, v0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    iget-object v1, p0, LX/GJG;->a:LX/GJK;

    iget-object v1, v1, LX/GJK;->l:LX/GK4;

    invoke-virtual {v1}, LX/GK4;->a()Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->a(Landroid/text/Spanned;)V

    .line 2338074
    iget-object v0, p0, LX/GJG;->a:LX/GJK;

    iget-object v0, v0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    iget-object v1, p0, LX/GJG;->a:LX/GJK;

    iget-object v1, v1, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    check-cast v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v2, p0, LX/GJG;->a:LX/GJK;

    iget-object v2, v2, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v2, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v2, p0, LX/GJG;->a:LX/GJK;

    iget-object v2, v2, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v2, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1, v3, v2}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;Landroid/content/res/Resources;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->setSummaryText(Landroid/text/Spanned;)V

    .line 2338075
    iget-object v1, p0, LX/GJG;->a:LX/GJK;

    iget-object v0, p0, LX/GJG;->a:LX/GJK;

    iget-object v0, v0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {v0}, LX/GJD;->getSelectedIndex()I

    move-result v0

    .line 2338076
    iput v0, v1, LX/GJK;->s:I

    .line 2338077
    return-void
.end method
