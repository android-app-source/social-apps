.class public final LX/GBr;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V
    .locals 0

    .prologue
    .line 2327551
    iput-object p1, p0, LX/GBr;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2327546
    iget-object v0, p0, LX/GBr;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->b:LX/GCA;

    invoke-virtual {v0, p3}, LX/GCA;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GB8;

    .line 2327547
    sget-object v1, LX/GBl;->b:[I

    invoke-interface {v0}, LX/GB8;->l()LX/GBB;

    move-result-object v0

    invoke-virtual {v0}, LX/GBB;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2327548
    :goto_0
    return-void

    .line 2327549
    :pswitch_0
    iget-object v0, p0, LX/GBr;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v1, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->q:LX/2Nh;

    iget-object v0, p0, LX/GBr;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->b:LX/GCA;

    invoke-virtual {v0, p3}, LX/GCA;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/account/recovery/common/model/AccountCandidateModel;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, LX/2Nh;->a(Lcom/facebook/account/recovery/common/model/AccountCandidateModel;Z)V

    goto :goto_0

    .line 2327550
    :pswitch_1
    iget-object v0, p0, LX/GBr;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->q:LX/2Nh;

    iget-object v1, p0, LX/GBr;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->z:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/2Nh;->b(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
