.class public final LX/GKs;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/GKv;


# direct methods
.method public constructor <init>(LX/GKv;)V
    .locals 0

    .prologue
    .line 2341505
    iput-object p1, p0, LX/GKs;->a:LX/GKv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2341506
    iget-object v0, p0, LX/GKs;->a:LX/GKv;

    iget-object v0, v0, LX/GKv;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Z)V

    .line 2341507
    iget-object v0, p0, LX/GKs;->a:LX/GKv;

    iget-object v1, p0, LX/GKs;->a:LX/GKv;

    iget-object v1, v1, LX/GKv;->e:LX/3Lz;

    invoke-virtual {v1, p1}, LX/3Lz;->getCountryCodeForRegion(Ljava/lang/String;)I

    move-result v1

    .line 2341508
    iput v1, v0, LX/GKv;->l:I

    .line 2341509
    iget-object v0, p0, LX/GKs;->a:LX/GKv;

    iget-object v0, v0, LX/GKv;->m:Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;

    iget-object v1, p0, LX/GKs;->a:LX/GKv;

    iget v1, v1, LX/GKv;->l:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;->setCountryCode(Ljava/lang/CharSequence;)V

    .line 2341510
    iget-object v0, p0, LX/GKs;->a:LX/GKv;

    iget-object v0, v0, LX/GKv;->m:Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;

    iget-object v1, p0, LX/GKs;->a:LX/GKv;

    iget-object v1, v1, LX/GKv;->a:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesPhoneNumberView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2341511
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2341512
    iget-object v0, p0, LX/GKs;->a:LX/GKv;

    iget-object v0, v0, LX/GKv;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Z)V

    .line 2341513
    iget-object v0, p0, LX/GKs;->a:LX/GKv;

    iget-boolean v0, v0, LX/GKv;->k:Z

    if-eqz v0, :cond_0

    .line 2341514
    iget-object v0, p0, LX/GKs;->a:LX/GKv;

    iget-object v0, v0, LX/GKv;->j:LX/GCE;

    new-instance v1, LX/GFO;

    invoke-direct {v1, p1}, LX/GFO;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2341515
    :cond_0
    return-void
.end method
