.class public LX/FrH;
.super LX/BPB;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/BPB",
        "<",
        "LX/Fr5;",
        ">;"
    }
.end annotation


# instance fields
.field public final f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2295349
    invoke-direct {p0}, LX/BPB;-><init>()V

    .line 2295350
    const/16 v0, 0x32

    iput v0, p0, LX/FrH;->f:I

    .line 2295351
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 2295346
    invoke-direct {p0}, LX/BPB;-><init>()V

    .line 2295347
    iput p1, p0, LX/FrH;->f:I

    .line 2295348
    return-void
.end method


# virtual methods
.method public final a(LX/Fr5;I)V
    .locals 2

    .prologue
    .line 2295352
    invoke-super {p0, p1, p2}, LX/BPB;->a(Ljava/lang/Object;I)V

    .line 2295353
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, LX/BPB;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BPB;->b:Ljava/lang/Object;

    check-cast v0, LX/Fr5;

    .line 2295354
    iget-object v1, v0, LX/Fr5;->a:LX/0Px;

    move-object v0, v1

    .line 2295355
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iget v1, p0, LX/FrH;->f:I

    if-gt v0, v1, :cond_0

    iget-object v0, p0, LX/BPB;->b:Ljava/lang/Object;

    check-cast v0, LX/Fr5;

    .line 2295356
    iget-object v1, v0, LX/Fr5;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextItemsPageInfoModel;

    move-object v0, v1

    .line 2295357
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/BPB;->b:Ljava/lang/Object;

    check-cast v0, LX/Fr5;

    .line 2295358
    iget-object v1, v0, LX/Fr5;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextItemsPageInfoModel;

    move-object v0, v1

    .line 2295359
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineContextItemsPageInfoModel;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2295360
    const/4 v0, 0x2

    iput v0, p0, LX/FrH;->d:I

    .line 2295361
    :cond_0
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 2295345
    check-cast p1, LX/Fr5;

    invoke-virtual {p0, p1, p2}, LX/FrH;->a(LX/Fr5;I)V

    return-void
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 2295342
    iget v0, p0, LX/BPB;->c:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/BPB;->b:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/BPB;->b:Ljava/lang/Object;

    check-cast v0, LX/Fr5;

    .line 2295343
    iget-object v1, v0, LX/Fr5;->a:LX/0Px;

    move-object v0, v1

    .line 2295344
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
