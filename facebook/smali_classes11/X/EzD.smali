.class public final LX/EzD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic b:Lcom/facebook/friends/model/PersonYouMayKnow;

.field public final synthetic c:LX/2iS;


# direct methods
.method public constructor <init>(LX/2iS;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/friends/model/PersonYouMayKnow;)V
    .locals 0

    .prologue
    .line 2186948
    iput-object p1, p0, LX/EzD;->c:LX/2iS;

    iput-object p2, p0, LX/EzD;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object p3, p0, LX/EzD;->b:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x6632ddf6

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2186949
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iget-object v2, p0, LX/EzD;->a:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v1, v2}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2186950
    iget-object v1, p0, LX/EzD;->c:LX/2iS;

    iget-object v1, v1, LX/2iS;->g:LX/2hb;

    iget-object v2, p0, LX/EzD;->b:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v1, v2}, LX/2hb;->b(Lcom/facebook/friends/model/PersonYouMayKnow;)V

    .line 2186951
    :cond_0
    iget-object v1, p0, LX/EzD;->c:LX/2iS;

    iget-object v2, p0, LX/EzD;->b:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-static {v1, v2}, LX/2iS;->c$redex0(LX/2iS;Lcom/facebook/friends/model/PersonYouMayKnow;)V

    .line 2186952
    const v1, -0x6477bce5

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
