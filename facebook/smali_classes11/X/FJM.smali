.class public final LX/FJM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;)V
    .locals 0

    .prologue
    .line 2223372
    iput-object p1, p0, LX/FJM;->a:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x2

    const v0, 0x3bfe880b

    invoke-static {v2, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2223362
    iget-object v0, p0, LX/FJM;->a:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;

    invoke-static {v0}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->c(Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;)Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;

    move-result-object v3

    .line 2223363
    if-nez v3, :cond_0

    .line 2223364
    iget-object v0, p0, LX/FJM;->a:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;

    iget-object v0, v0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIz;

    iget-object v1, p0, LX/FJM;->a:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;

    iget-object v1, v1, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i:LX/6gC;

    .line 2223365
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "moments_invite_clicked_bad_link"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2223366
    invoke-static {v1, v3}, LX/FIz;->a(LX/6gC;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2223367
    iget-object v4, v0, LX/FIz;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2223368
    const v0, 0x115954dc

    invoke-static {v2, v2, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2223369
    :goto_0
    return-void

    .line 2223370
    :cond_0
    iget-object v0, p0, LX/FJM;->a:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;

    iget-object v0, v0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FJT;

    iget-object v1, p0, LX/FJM;->a:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;

    invoke-virtual {v1}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/FJM;->a:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;

    iget-object v2, v2, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i:LX/6gC;

    new-instance v5, LX/FJL;

    invoke-direct {v5, p0, p1}, LX/FJL;-><init>(LX/FJM;Landroid/view/View;)V

    invoke-virtual/range {v0 .. v5}, LX/FJT;->a(Landroid/content/Context;LX/6gC;Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;ZLX/FJK;)V

    .line 2223371
    const v0, 0x56b80878

    invoke-static {v0, v6}, LX/02F;->a(II)V

    goto :goto_0
.end method
