.class public LX/FCw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/FCw;


# instance fields
.field private final a:Landroid/media/AudioManager;

.field public b:I


# direct methods
.method public constructor <init>(Landroid/media/AudioManager;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2210799
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2210800
    const/high16 v0, -0x80000000

    iput v0, p0, LX/FCw;->b:I

    .line 2210801
    iput-object p1, p0, LX/FCw;->a:Landroid/media/AudioManager;

    .line 2210802
    return-void
.end method

.method public static a(LX/0QB;)LX/FCw;
    .locals 4

    .prologue
    .line 2210803
    sget-object v0, LX/FCw;->c:LX/FCw;

    if-nez v0, :cond_1

    .line 2210804
    const-class v1, LX/FCw;

    monitor-enter v1

    .line 2210805
    :try_start_0
    sget-object v0, LX/FCw;->c:LX/FCw;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2210806
    if-eqz v2, :cond_0

    .line 2210807
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2210808
    new-instance p0, LX/FCw;

    invoke-static {v0}, LX/19T;->b(LX/0QB;)Landroid/media/AudioManager;

    move-result-object v3

    check-cast v3, Landroid/media/AudioManager;

    invoke-direct {p0, v3}, LX/FCw;-><init>(Landroid/media/AudioManager;)V

    .line 2210809
    move-object v0, p0

    .line 2210810
    sput-object v0, LX/FCw;->c:LX/FCw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2210811
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2210812
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2210813
    :cond_1
    sget-object v0, LX/FCw;->c:LX/FCw;

    return-object v0

    .line 2210814
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2210815
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
