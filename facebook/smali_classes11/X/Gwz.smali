.class public LX/Gwz;
.super LX/2Zg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2Zg",
        "<",
        "LX/Gwt;",
        "LX/Gww;",
        ">;"
    }
.end annotation


# instance fields
.field public l:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Gwt;",
            "Ljava/util/List",
            "<",
            "LX/Gwu;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2407643
    new-instance v0, LX/Gwy;

    invoke-direct {v0, p1}, LX/Gwy;-><init>(Landroid/content/Context;)V

    sget-object v1, LX/2Zh;->SINGLE_REQUEST_BY_KEY:LX/2Zh;

    invoke-direct {p0, v0, v1, p1}, LX/2Zg;-><init>(LX/2Zb;LX/2Zh;Landroid/content/Context;)V

    .line 2407644
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Gwz;->l:Ljava/util/Map;

    .line 2407645
    return-void
.end method


# virtual methods
.method public final a(LX/Gwt;LX/Gwu;)LX/Gww;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2407626
    monitor-enter p0

    .line 2407627
    :try_start_0
    iget-object v0, p0, LX/Gwz;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2407628
    if-eqz v0, :cond_0

    .line 2407629
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2407630
    monitor-exit p0

    move-object v0, v1

    .line 2407631
    :goto_0
    return-object v0

    .line 2407632
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2407633
    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2407634
    iget-object v2, p0, LX/Gwz;->l:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2407635
    invoke-super {p0, p1}, LX/2Zg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gww;

    .line 2407636
    if-eqz v0, :cond_2

    .line 2407637
    iget-object v1, p0, LX/Gwz;->l:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2407638
    invoke-virtual {v0}, LX/Gww;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, LX/Gww;->a()LX/44w;

    move-result-object v1

    iget-object v1, v1, LX/44w;->a:Ljava/lang/Object;

    sget-object v2, LX/Gwv;->DESERIALIZATION_ERROR:LX/Gwv;

    if-ne v1, v2, :cond_1

    .line 2407639
    sget-object v1, LX/FCH;->CLEAR_DISK:LX/FCH;

    invoke-super {p0, v1}, LX/2Zg;->a(LX/FCH;)V

    .line 2407640
    :cond_1
    monitor-exit p0

    goto :goto_0

    .line 2407641
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    .line 2407642
    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2407606
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Don\'t call me directly"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;ZLX/Gwt;Ljava/lang/String;LX/Gww;)V
    .locals 6

    .prologue
    .line 2407608
    const/4 v3, 0x0

    .line 2407609
    if-eqz p2, :cond_0

    .line 2407610
    invoke-static {p5}, LX/0sL;->a(Ljava/lang/Object;)V

    .line 2407611
    invoke-virtual {p5}, LX/Gww;->b()Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;

    move-result-object v0

    invoke-static {v0}, LX/0sL;->a(Ljava/lang/Object;)V

    .line 2407612
    new-instance v3, LX/Gwt;

    invoke-virtual {p3}, LX/Gwt;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p5}, LX/Gww;->b()Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v0, v1}, LX/Gwt;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2407613
    invoke-virtual {v3}, LX/Gwt;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, LX/GxG;->a(Landroid/content/Context;Ljava/lang/String;)V

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p4

    move-object v5, p5

    .line 2407614
    invoke-super/range {v0 .. v5}, LX/2Zg;->a(Landroid/content/Context;ZLjava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2407615
    monitor-enter p0

    .line 2407616
    :try_start_0
    iget-object v0, p0, LX/Gwz;->l:Ljava/util/Map;

    invoke-interface {v0, p3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2407617
    monitor-exit p0

    .line 2407618
    if-nez v0, :cond_2

    .line 2407619
    :cond_1
    return-void

    .line 2407620
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 2407621
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gwu;

    .line 2407622
    if-eqz p2, :cond_3

    .line 2407623
    invoke-virtual {p5}, LX/Gww;->b()Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;

    move-result-object v1

    invoke-interface {v0, v1}, LX/Gwu;->a(Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;)V

    goto :goto_0

    .line 2407624
    :cond_3
    invoke-virtual {p5}, LX/Gww;->a()LX/44w;

    move-result-object v2

    .line 2407625
    iget-object v1, v2, LX/44w;->a:Ljava/lang/Object;

    check-cast v1, LX/Gwv;

    iget-object v2, v2, LX/44w;->b:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/Gwu;->a(LX/Gwv;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Landroid/content/Context;ZLjava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2407607
    move-object v3, p3

    check-cast v3, LX/Gwt;

    move-object v5, p5

    check-cast v5, LX/Gww;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, LX/Gwz;->a(Landroid/content/Context;ZLX/Gwt;Ljava/lang/String;LX/Gww;)V

    return-void
.end method
