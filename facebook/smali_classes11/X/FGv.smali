.class public final LX/FGv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "LX/6eR;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final synthetic b:LX/FHx;

.field public final synthetic c:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final synthetic d:Lcom/facebook/messaging/media/photoquality/PhotoQuality;

.field public final synthetic e:Z

.field public final synthetic f:Z

.field public final synthetic g:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;LX/FHx;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/messaging/media/photoquality/PhotoQuality;ZZ)V
    .locals 0

    .prologue
    .line 2219610
    iput-object p1, p0, LX/FGv;->g:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iput-object p2, p0, LX/FGv;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object p3, p0, LX/FGv;->b:LX/FHx;

    iput-object p4, p0, LX/FGv;->c:Lcom/facebook/ui/media/attachments/MediaResource;

    iput-object p5, p0, LX/FGv;->d:Lcom/facebook/messaging/media/photoquality/PhotoQuality;

    iput-boolean p6, p0, LX/FGv;->e:Z

    iput-boolean p7, p0, LX/FGv;->f:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2219611
    check-cast p1, LX/6eR;

    .line 2219612
    sget-object v0, LX/6eR;->VALID:LX/6eR;

    if-ne p1, v0, :cond_0

    .line 2219613
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2219614
    :goto_0
    return-object v0

    .line 2219615
    :cond_0
    iget-object v0, p0, LX/FGv;->g:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, v0, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->h:LX/2MO;

    iget-object v2, p0, LX/FGv;->a:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, p0, LX/FGv;->b:LX/FHx;

    sget-object v3, LX/FHx;->PHASE_TWO:LX/FHx;

    if-ne v0, v3, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v2, v0}, LX/2MO;->a(Lcom/facebook/ui/media/attachments/MediaResource;Z)V

    .line 2219616
    iget-object v0, p0, LX/FGv;->g:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v1, p0, LX/FGv;->c:Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v2, p0, LX/FGv;->d:Lcom/facebook/messaging/media/photoquality/PhotoQuality;

    iget-object v3, p0, LX/FGv;->b:LX/FHx;

    iget-boolean v4, p0, LX/FGv;->e:Z

    iget-boolean v5, p0, LX/FGv;->f:Z

    .line 2219617
    invoke-static/range {v0 .. v5}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->a$redex0(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Lcom/facebook/messaging/media/photoquality/PhotoQuality;LX/FHx;ZZ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v0, v6

    .line 2219618
    goto :goto_0

    .line 2219619
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
