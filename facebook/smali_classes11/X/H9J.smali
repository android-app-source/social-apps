.class public final LX/H9J;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/H9L;


# direct methods
.method public constructor <init>(LX/H9L;Z)V
    .locals 0

    .prologue
    .line 2434240
    iput-object p1, p0, LX/H9J;->b:LX/H9L;

    iput-boolean p2, p0, LX/H9J;->a:Z

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2434241
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 2434242
    sget-object v3, LX/1nY;->CANCELLED:LX/1nY;

    if-ne v0, v3, :cond_0

    .line 2434243
    :goto_0
    return-void

    .line 2434244
    :cond_0
    iget-object v0, p0, LX/H9J;->b:LX/H9L;

    iget-object v3, v0, LX/H9L;->d:LX/H8W;

    iget-boolean v0, p0, LX/H9J;->a:Z

    if-eqz v0, :cond_1

    sget-object v0, LX/9XA;->EVENT_LIKE_ERROR:LX/9XA;

    :goto_1
    iget-object v4, p0, LX/H9J;->b:LX/H9L;

    iget-object v4, v4, LX/H9L;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2434245
    iget-object v3, p0, LX/H9J;->b:LX/H9L;

    iget-boolean v0, p0, LX/H9J;->a:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v3, v0}, LX/H9L;->a$redex0(LX/H9L;Z)V

    .line 2434246
    iget-object v0, p0, LX/H9J;->b:LX/H9L;

    iget-object v0, v0, LX/H9L;->d:LX/H8W;

    iget-boolean v3, p0, LX/H9J;->a:Z

    if-nez v3, :cond_3

    .line 2434247
    :goto_3
    if-eqz v1, :cond_4

    .line 2434248
    const v3, 0x7f0817c8

    .line 2434249
    const-string v2, "page_identity_unlike_fail"

    move v4, v3

    move-object v3, v2

    .line 2434250
    :goto_4
    iget-object v2, v0, LX/H8W;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0kL;

    new-instance p0, LX/27k;

    invoke-direct {p0, v4}, LX/27k;-><init>(I)V

    invoke-virtual {v2, p0}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2434251
    iget-object v2, v0, LX/H8W;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    invoke-virtual {v2, v3, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2434252
    goto :goto_0

    .line 2434253
    :cond_1
    sget-object v0, LX/9XA;->EVENT_UNLIKE_ERROR:LX/9XA;

    goto :goto_1

    :cond_2
    move v0, v2

    .line 2434254
    goto :goto_2

    :cond_3
    move v1, v2

    .line 2434255
    goto :goto_3

    .line 2434256
    :cond_4
    const v3, 0x7f0817c7

    .line 2434257
    const-string v2, "page_identity_like_fail"

    move v4, v3

    move-object v3, v2

    goto :goto_4
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2434258
    iget-object v0, p0, LX/H9J;->b:LX/H9L;

    iget-object v1, v0, LX/H9L;->d:LX/H8W;

    iget-boolean v0, p0, LX/H9J;->a:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/9XB;->EVENT_LIKE_SUCCESS:LX/9XB;

    :goto_0
    iget-object v2, p0, LX/H9J;->b:LX/H9L;

    iget-object v2, v2, LX/H9L;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2434259
    return-void

    .line 2434260
    :cond_0
    sget-object v0, LX/9XB;->EVENT_UNLIKE_SUCCESS:LX/9XB;

    goto :goto_0
.end method
