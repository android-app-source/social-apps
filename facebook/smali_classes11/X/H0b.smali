.class public LX/H0b;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/H0Z;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/content/Context;

.field public c:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2414027
    new-instance v0, LX/H0Z;

    sget-object v1, LX/H0a;->HEADER:LX/H0a;

    invoke-direct {v0, v1}, LX/H0Z;-><init>(LX/H0a;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/H0b;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2414048
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2414049
    iput-object p1, p0, LX/H0b;->b:Landroid/content/Context;

    .line 2414050
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2414044
    sget-object v0, LX/H0a;->HEADER:LX/H0a;

    invoke-virtual {v0}, LX/H0a;->toInt()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2414045
    iget-object v0, p0, LX/H0b;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307a5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2414046
    new-instance v1, LX/H0Y;

    invoke-direct {v1, v0}, LX/H0Y;-><init>(Landroid/view/View;)V

    return-object v1

    .line 2414047
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid viewType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    .line 2414032
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2414033
    sget-object v1, LX/H0a;->HEADER:LX/H0a;

    invoke-virtual {v1}, LX/H0a;->toInt()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 2414034
    check-cast p1, LX/H0Y;

    .line 2414035
    iget-object v0, p0, LX/H0b;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081514

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2414036
    iget-object v1, p0, LX/H0b;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081515

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2414037
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/H0b;->c:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    .line 2414038
    iget-object p0, v4, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->a:Ljava/lang/String;

    move-object v4, p0

    .line 2414039
    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2414040
    iget-object v2, p1, LX/H0Y;->l:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2414041
    iget-object v2, p1, LX/H0Y;->m:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2414042
    return-void

    .line 2414043
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid viewType "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2414031
    sget-object v0, LX/H0b;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H0Z;

    iget-object v0, v0, LX/H0Z;->a:LX/H0a;

    invoke-virtual {v0}, LX/H0a;->toInt()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2414028
    iget-object v0, p0, LX/H0b;->c:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    if-nez v0, :cond_0

    .line 2414029
    const/4 v0, 0x0

    .line 2414030
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/H0b;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method
