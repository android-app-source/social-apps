.class public final LX/Gx9;
.super LX/BWM;
.source ""


# instance fields
.field public final synthetic b:LX/GxF;


# direct methods
.method public constructor <init>(LX/GxF;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 2407729
    iput-object p1, p0, LX/Gx9;->b:LX/GxF;

    .line 2407730
    invoke-direct {p0, p2}, LX/BWM;-><init>(Landroid/os/Handler;)V

    .line 2407731
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/webview/FacebookWebView;LX/BWN;)V
    .locals 5

    .prologue
    .line 2407732
    iget-object v0, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2407733
    const-string v1, "script"

    invoke-interface {p2, v0, v1}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2407734
    iget-object v1, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v1, v1

    .line 2407735
    const-string v2, "delay"

    invoke-interface {p2, v1, v2}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2407736
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 2407737
    invoke-virtual {p1}, Lcom/facebook/webview/FacebookWebView;->getUrl()Ljava/lang/String;

    move-result-object v2

    .line 2407738
    if-eqz v2, :cond_0

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, LX/2yp;->d(Landroid/net/Uri;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2407739
    :cond_0
    sget-object v0, LX/GxF;->z:Ljava/lang/Class;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Page with Non-facebook URL ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/facebook/webview/FacebookWebView;->getUrl()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") attempting to invoke broadcastScript"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2407740
    :goto_0
    return-void

    .line 2407741
    :cond_1
    iget-object v2, p0, LX/BWM;->a:Landroid/os/Handler;

    new-instance v3, Lcom/facebook/katana/webview/FacewebWebView$BroadcastScriptHandler$1;

    invoke-direct {v3, p0, v0}, Lcom/facebook/katana/webview/FacewebWebView$BroadcastScriptHandler$1;-><init>(LX/Gx9;Ljava/lang/String;)V

    int-to-long v0, v1

    const v4, -0x1ea72c0c

    invoke-static {v2, v3, v0, v1, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_0
.end method
