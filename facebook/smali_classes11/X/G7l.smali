.class public final LX/G7l;
.super LX/2wH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2wH",
        "<",
        "LX/G7p;",
        ">;"
    }
.end annotation


# instance fields
.field public final d:LX/G7Q;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;LX/2wA;LX/G7Q;LX/1qf;LX/1qg;)V
    .locals 7

    const/16 v3, 0x44

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, LX/2wH;-><init>(Landroid/content/Context;Landroid/os/Looper;ILX/2wA;LX/1qf;LX/1qg;)V

    iput-object p4, p0, LX/G7l;->d:LX/G7Q;

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    move-object v0, v0

    return-object v0

    :cond_0
    const-string v0, "com.google.android.gms.auth.api.credentials.internal.ICredentialsService"

    invoke-interface {p1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of p0, v0, LX/G7p;

    if-eqz p0, :cond_1

    check-cast v0, LX/G7p;

    goto :goto_0

    :cond_1
    new-instance v0, LX/G7q;

    invoke-direct {v0, p1}, LX/G7q;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.auth.api.credentials.service.START"

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.auth.api.credentials.internal.ICredentialsService"

    return-object v0
.end method

.method public final j()Landroid/os/Bundle;
    .locals 2

    iget-object v0, p0, LX/G7l;->d:LX/G7Q;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    const/4 p0, 0x0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "consumer_package"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "password_specification"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    move-object v0, v0

    goto :goto_0
.end method
