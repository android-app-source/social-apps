.class public final LX/GLc;
.super LX/GFy;
.source ""


# instance fields
.field public final synthetic a:LX/GLd;


# direct methods
.method public constructor <init>(LX/GLd;)V
    .locals 0

    .prologue
    .line 2343356
    iput-object p1, p0, LX/GLc;->a:LX/GLd;

    invoke-direct {p0}, LX/GFy;-><init>()V

    return-void
.end method

.method private a(LX/GFx;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2343357
    iget-object v0, p1, LX/GFx;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-object v3, v0

    .line 2343358
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 2343359
    :goto_0
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-ne v4, v5, :cond_3

    .line 2343360
    :goto_1
    iget-object v4, p0, LX/GLc;->a:LX/GLd;

    .line 2343361
    invoke-static {v4, v0, v1}, LX/GLd;->a$redex0(LX/GLd;ZZ)V

    .line 2343362
    iget-object v0, p0, LX/GLc;->a:LX/GLd;

    iget-object v0, v0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    instance-of v0, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v0, :cond_0

    .line 2343363
    iget-object v0, p0, LX/GLc;->a:LX/GLd;

    iget-object v0, v0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 2343364
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->e:Ljava/lang/String;

    .line 2343365
    iget-object v0, p0, LX/GLc;->a:LX/GLd;

    iget-object v0, v0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v1

    .line 2343366
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->f:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2343367
    :cond_0
    iget-object v0, p0, LX/GLc;->a:LX/GLd;

    iget-object v0, v0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->a(Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;)V

    .line 2343368
    iget-object v0, p0, LX/GLc;->a:LX/GLd;

    iget-object v0, v0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 2343369
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    .line 2343370
    iget-object v0, p0, LX/GLc;->a:LX/GLd;

    iget-object v0, v0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->o()LX/0Px;

    move-result-object v1

    .line 2343371
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->l:LX/0Px;

    .line 2343372
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2343373
    iget-object v0, p0, LX/GLc;->a:LX/GLd;

    iget-object v0, v0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    iget-object v1, p0, LX/GLc;->a:LX/GLd;

    invoke-virtual {v1}, LX/GIr;->c()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    .line 2343374
    :cond_1
    iget-object v0, p0, LX/GLc;->a:LX/GLd;

    .line 2343375
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2343376
    new-instance v1, LX/GFr;

    iget-object v3, p0, LX/GLc;->a:LX/GLd;

    iget-object v3, v3, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v3}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v3

    invoke-direct {v1, v3}, LX/GFr;-><init>(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2343377
    iget-object v0, p0, LX/GLc;->a:LX/GLd;

    .line 2343378
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2343379
    new-instance v1, LX/GFp;

    invoke-direct {v1}, LX/GFp;-><init>()V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2343380
    iget-object v0, p0, LX/GLc;->a:LX/GLd;

    invoke-virtual {v0, v2}, LX/GIr;->f(Z)V

    .line 2343381
    return-void

    :cond_2
    move v0, v2

    .line 2343382
    goto/16 :goto_0

    :cond_3
    move v1, v2

    .line 2343383
    goto/16 :goto_1
.end method


# virtual methods
.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 2343384
    check-cast p1, LX/GFx;

    invoke-direct {p0, p1}, LX/GLc;->a(LX/GFx;)V

    return-void
.end method
