.class public LX/G4L;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/G4K;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/G4K;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field

.field public final c:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2317468
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2317469
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/G4L;->a:Ljava/util/Set;

    .line 2317470
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/G4L;->b:Ljava/util/Set;

    .line 2317471
    const-string v0, "get_notified"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, LX/G4L;->c:Landroid/content/SharedPreferences;

    .line 2317472
    invoke-direct {p0}, LX/G4L;->e()V

    .line 2317473
    return-void
.end method

.method public static a(LX/0QB;)LX/G4L;
    .locals 2

    .prologue
    .line 2317465
    new-instance v1, LX/G4L;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/G4L;-><init>(Landroid/content/Context;)V

    .line 2317466
    move-object v0, v1

    .line 2317467
    return-object v0
.end method

.method private static a(LX/G4L;Ljava/util/Set;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "LX/G4K;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2317450
    iget-object v0, p0, LX/G4L;->c:Landroid/content/SharedPreferences;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0, p2, v1}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 2317451
    new-instance v1, LX/G4I;

    invoke-direct {v1}, LX/G4I;-><init>()V

    .line 2317452
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2317453
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2317454
    iput-wide v4, v1, LX/G4I;->a:J

    .line 2317455
    move-object v0, v1

    .line 2317456
    iget-object v3, p0, LX/G4L;->c:Landroid/content/SharedPreferences;

    invoke-static {v4, v5}, LX/G4L;->c(J)Ljava/lang/String;

    move-result-object v6

    const-string p2, ""

    invoke-interface {v3, v6, p2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v3, v3

    .line 2317457
    iput-object v3, v0, LX/G4I;->b:Ljava/lang/String;

    .line 2317458
    move-object v0, v0

    .line 2317459
    invoke-virtual {v0}, LX/G4I;->a()LX/G4J;

    move-result-object v0

    .line 2317460
    new-instance v3, LX/G4K;

    invoke-direct {v3, v0}, LX/G4K;-><init>(LX/G4J;)V

    .line 2317461
    iget-object v0, p0, LX/G4L;->c:Landroid/content/SharedPreferences;

    invoke-static {v4, v5}, LX/G4L;->d(J)Ljava/lang/String;

    move-result-object v6

    const/4 p2, 0x0

    invoke-interface {v0, v6, p2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    move v0, v0

    .line 2317462
    iput v0, v3, LX/G4K;->b:I

    .line 2317463
    invoke-interface {p1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2317464
    :cond_0
    return-void
.end method

.method public static c(J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2317449
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "profile_name_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2317448
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "retry_count_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized e()V
    .locals 2

    .prologue
    .line 2317444
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/G4L;->a:Ljava/util/Set;

    const-string v1, "in_flight_profiles"

    invoke-static {p0, v0, v1}, LX/G4L;->a(LX/G4L;Ljava/util/Set;Ljava/lang/String;)V

    .line 2317445
    iget-object v0, p0, LX/G4L;->b:Ljava/util/Set;

    const-string v1, "retry_queue"

    invoke-static {p0, v0, v1}, LX/G4L;->a(LX/G4L;Ljava/util/Set;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2317446
    monitor-exit p0

    return-void

    .line 2317447
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static f(LX/G4L;)Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2317440
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 2317441
    iget-object v0, p0, LX/G4L;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G4K;

    .line 2317442
    invoke-virtual {v0}, LX/G4K;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2317443
    :cond_0
    return-object v1
.end method

.method private static g(LX/G4L;)Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2317474
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 2317475
    iget-object v0, p0, LX/G4L;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G4K;

    .line 2317476
    invoke-virtual {v0}, LX/G4K;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2317477
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final declared-synchronized a(LX/G4K;)V
    .locals 4

    .prologue
    .line 2317433
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/G4L;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2317434
    iget-object v0, p0, LX/G4L;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2317435
    iget-object v0, p0, LX/G4L;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "in_flight_profiles"

    invoke-static {p0}, LX/G4L;->f(LX/G4L;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "retry_queue"

    invoke-static {p0}, LX/G4L;->g(LX/G4L;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p1}, LX/G4K;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, LX/G4L;->c(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, LX/G4K;->b()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p1}, LX/G4K;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, LX/G4L;->d(J)Ljava/lang/String;

    move-result-object v1

    .line 2317436
    iget v2, p1, LX/G4K;->b:I

    move v2, v2

    .line 2317437
    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2317438
    monitor-exit p0

    return-void

    .line 2317439
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 2317432
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/G4L;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, LX/G4L;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/G4K;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2317420
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/G4L;->a:Ljava/util/Set;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/G4K;)V
    .locals 4

    .prologue
    .line 2317428
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/G4L;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2317429
    iget-object v0, p0, LX/G4L;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "in_flight_profiles"

    invoke-static {p0}, LX/G4L;->f(LX/G4L;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p1}, LX/G4K;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, LX/G4L;->c(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-virtual {p1}, LX/G4K;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, LX/G4L;->d(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2317430
    monitor-exit p0

    return-void

    .line 2317431
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/G4K;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2317427
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/G4L;->b:Ljava/util/Set;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(LX/G4K;)V
    .locals 3

    .prologue
    .line 2317422
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/G4L;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2317423
    iget-object v0, p0, LX/G4L;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2317424
    iget-object v0, p0, LX/G4L;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "in_flight_profiles"

    invoke-static {p0}, LX/G4L;->f(LX/G4L;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "retry_queue"

    invoke-static {p0}, LX/G4L;->g(LX/G4L;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2317425
    monitor-exit p0

    return-void

    .line 2317426
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d(LX/G4K;)Z
    .locals 1

    .prologue
    .line 2317421
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/G4L;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
