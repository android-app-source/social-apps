.class public LX/GIw;
.super LX/GHg;
.source ""


# annotations
.annotation build Lcom/facebook/annotations/OkToExtend;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D::",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">",
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;",
        "TD;>;"
    }
.end annotation


# instance fields
.field private final a:LX/GK4;

.field public b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TD;"
        }
    .end annotation
.end field

.field public c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;


# direct methods
.method public constructor <init>(LX/GK4;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2337424
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2337425
    iput-object p1, p0, LX/GIw;->a:LX/GK4;

    .line 2337426
    return-void
.end method

.method private b(Ljava/lang/String;)Landroid/text/Spanned;
    .locals 4

    .prologue
    .line 2337427
    invoke-direct {p0, p1}, LX/GIw;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2337428
    iget-object v0, p0, LX/GIw;->a:LX/GK4;

    invoke-virtual {v0}, LX/GK4;->a()Landroid/text/Spanned;

    move-result-object v0

    .line 2337429
    :goto_0
    return-object v0

    .line 2337430
    :cond_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2337431
    iget-object v1, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080a59

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2337432
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2337433
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    iget-object v2, p0, LX/GIw;->a:LX/GK4;

    invoke-virtual {v2}, LX/GK4;->a()Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2337434
    if-eqz p1, :cond_0

    iget-object v1, p0, LX/GIw;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v1}, LX/GMU;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2337435
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2337436
    iget-object v2, v1, LX/GCE;->e:LX/0ad;

    move-object v1, v2

    .line 2337437
    sget-short v2, LX/GDK;->f:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 2337438
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2337439
    new-instance v1, LX/GJl;

    invoke-direct {v1, p0}, LX/GJl;-><init>(LX/GIw;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2337440
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2337441
    new-instance v1, LX/GJm;

    invoke-direct {v1, p0}, LX/GJm;-><init>(LX/GIw;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2337442
    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 2337449
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    const/4 v1, 0x0

    invoke-direct {p0, v1}, LX/GIw;->b(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setLegalDisclaimerContent(Landroid/text/Spanned;)V

    .line 2337450
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setLegalDisclaimerMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2337451
    return-void
.end method

.method private o()V
    .locals 2

    .prologue
    .line 2337443
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {p0}, LX/GIw;->b()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setCreateAdButtonListener(Landroid/view/View$OnClickListener;)V

    .line 2337444
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {p0}, LX/GIw;->c()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAddBudgetButtonListener(Landroid/view/View$OnClickListener;)V

    .line 2337445
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {p0}, LX/GIw;->d()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setResumeAdButtonListener(Landroid/view/View$OnClickListener;)V

    .line 2337446
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {p0}, LX/GIw;->h()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setPauseAdButtonListener(Landroid/view/View$OnClickListener;)V

    .line 2337447
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {p0}, LX/GIw;->f()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setDeleteAdButtonListener(Landroid/view/View$OnClickListener;)V

    .line 2337448
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2337455
    if-eqz p1, :cond_0

    invoke-direct {p0, p1}, LX/GIw;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2337456
    :cond_0
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080b1b

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2337457
    :goto_0
    return-object v0

    .line 2337458
    :cond_1
    iget-object v0, p0, LX/GIw;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2337459
    invoke-static {v0}, LX/GMU;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2337460
    iget-object v0, p0, LX/GIw;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2337461
    instance-of v0, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v0, :cond_2

    .line 2337462
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080aed

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2337463
    :cond_2
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080aee

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 2337452
    invoke-super {p0}, LX/GHg;->a()V

    .line 2337453
    const/4 v0, 0x0

    iput-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    .line 2337454
    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2337421
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {p0, p1, p2}, LX/GIw;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)V"
        }
    .end annotation

    .prologue
    .line 2337422
    iput-object p1, p0, LX/GIw;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2337423
    return-void
.end method

.method public a(Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2337403
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2337404
    iput-object p1, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    .line 2337405
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2337406
    iget-object v1, v0, LX/GCE;->e:LX/0ad;

    move-object v0, v1

    .line 2337407
    iget-object v1, p0, LX/GIw;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    sget-object v2, LX/GGB;->PAUSED:LX/GGB;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/GIw;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v1

    sget-object v2, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/GIw;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v1

    sget-object v2, LX/8wL;->BOOST_POST:LX/8wL;

    if-eq v1, v2, :cond_0

    sget-short v1, LX/GDK;->A:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2337408
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setVisibility(I)V

    .line 2337409
    :goto_0
    return-void

    .line 2337410
    :cond_0
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setVisibility(I)V

    .line 2337411
    invoke-direct {p0}, LX/GIw;->m()V

    .line 2337412
    invoke-direct {p0}, LX/GIw;->n()V

    .line 2337413
    iget-object v0, p0, LX/GIw;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/GIw;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)V

    .line 2337414
    invoke-virtual {p0}, LX/GIw;->g()V

    .line 2337415
    invoke-direct {p0}, LX/GIw;->o()V

    .line 2337416
    invoke-virtual {p0}, LX/GIw;->e()V

    .line 2337417
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2337418
    invoke-virtual {v0}, LX/GCE;->e()Z

    move-result v0

    .line 2337419
    iget-object v1, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setCreateAdButtonEnabled(Z)V

    .line 2337420
    iget-object v1, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAddBudgetButtonEnabled(Z)V

    goto :goto_0
.end method

.method public b()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2337402
    const/4 v0, 0x0

    return-object v0
.end method

.method public b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2337394
    if-nez p1, :cond_0

    .line 2337395
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {p0, v2}, LX/GIw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setCreateAdButtonText(Ljava/lang/String;)V

    .line 2337396
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-direct {p0, v2}, LX/GIw;->b(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setLegalDisclaimerContent(Landroid/text/Spanned;)V

    .line 2337397
    :goto_0
    return-void

    .line 2337398
    :cond_0
    invoke-static {p1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v0

    .line 2337399
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v2

    iget-object v3, p0, LX/GIw;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v3}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v3

    invoke-static {v2, v0, v1, v3}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v0

    .line 2337400
    iget-object v1, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-direct {p0, v0}, LX/GIw;->b(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setLegalDisclaimerContent(Landroid/text/Spanned;)V

    .line 2337401
    iget-object v1, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {p0, v0}, LX/GIw;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setCreateAdButtonText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public c()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2337393
    const/4 v0, 0x0

    return-object v0
.end method

.method public d()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2337392
    const/4 v0, 0x0

    return-object v0
.end method

.method public e()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 2337386
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setCreateAdButtonVisibility(I)V

    .line 2337387
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAddBudgetButtonVisibility(I)V

    .line 2337388
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setResumeAdButtonVisibility(I)V

    .line 2337389
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setPauseAdButtonVisibility(I)V

    .line 2337390
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setDeleteAdButtonVisibility(I)V

    .line 2337391
    return-void
.end method

.method public f()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2337385
    const/4 v0, 0x0

    return-object v0
.end method

.method public g()V
    .locals 0

    .prologue
    .line 2337384
    return-void
.end method

.method public h()Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2337383
    const/4 v0, 0x0

    return-object v0
.end method
