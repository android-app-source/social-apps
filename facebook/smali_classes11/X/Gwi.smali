.class public LX/Gwi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0lp;


# direct methods
.method public constructor <init>(LX/0lp;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2407303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2407304
    iput-object p1, p0, LX/Gwi;->a:LX/0lp;

    .line 2407305
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 7

    .prologue
    .line 2407306
    new-instance v0, LX/14N;

    const-string v1, "fetchFwComponents"

    const-string v2, "GET"

    const-string v3, "method/fwcomponents.get"

    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "format"

    const-string v6, "json"

    invoke-direct {v4, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v4}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    sget-object v5, LX/14S;->STRING:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2407307
    invoke-virtual {p2}, LX/1pN;->c()Ljava/lang/String;

    move-result-object v0

    .line 2407308
    iget-object v1, p0, LX/Gwi;->a:LX/0lp;

    invoke-static {v0, v1}, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;->a(Ljava/lang/String;LX/0lp;)Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;

    move-result-object v0

    return-object v0
.end method
