.class public LX/FHa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/io/Serializable;


# instance fields
.field public final mFbid:Ljava/lang/String;

.field public mMediaResourceAsBytes:[B

.field public retryCount:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2220623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2220624
    iput-object p2, p0, LX/FHa;->mFbid:Ljava/lang/String;

    .line 2220625
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, LX/FHa;->retryCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 2220626
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    .line 2220627
    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/facebook/ui/media/attachments/MediaResource;->writeToParcel(Landroid/os/Parcel;I)V

    .line 2220628
    invoke-virtual {v0}, Landroid/os/Parcel;->marshall()[B

    move-result-object v1

    iput-object v1, p0, LX/FHa;->mMediaResourceAsBytes:[B

    .line 2220629
    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    .line 2220630
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2220631
    if-ne p0, p1, :cond_1

    .line 2220632
    :cond_0
    :goto_0
    return v0

    .line 2220633
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2220634
    goto :goto_0

    .line 2220635
    :cond_3
    check-cast p1, LX/FHa;

    .line 2220636
    iget-object v2, p1, LX/FHa;->mFbid:Ljava/lang/String;

    iget-object v3, p0, LX/FHa;->mFbid:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p1, LX/FHa;->mMediaResourceAsBytes:[B

    iget-object v3, p0, LX/FHa;->mMediaResourceAsBytes:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2220622
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/FHa;->mFbid:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/FHa;->mMediaResourceAsBytes:[B

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
