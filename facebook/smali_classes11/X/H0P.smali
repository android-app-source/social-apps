.class public LX/H0P;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/1OM;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/1OM;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 4

    .prologue
    .line 2413883
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2413884
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/H0P;->b:Ljava/util/Map;

    .line 2413885
    iput-object p1, p0, LX/H0P;->c:Landroid/content/Context;

    .line 2413886
    iget-object v0, p0, LX/H0P;->c:Landroid/content/Context;

    .line 2413887
    if-eqz p2, :cond_0

    .line 2413888
    new-instance v1, LX/H0i;

    invoke-direct {v1, v0}, LX/H0i;-><init>(Landroid/content/Context;)V

    new-instance v2, LX/H0X;

    invoke-direct {v2, v0}, LX/H0X;-><init>(Landroid/content/Context;)V

    new-instance v3, LX/H0s;

    invoke-direct {v3, v0}, LX/H0s;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2413889
    :goto_0
    move-object v0, v1

    .line 2413890
    iput-object v0, p0, LX/H0P;->a:LX/0Px;

    .line 2413891
    return-void

    :cond_0
    new-instance v1, LX/H0b;

    invoke-direct {v1, v0}, LX/H0b;-><init>(Landroid/content/Context;)V

    new-instance v2, LX/H0i;

    invoke-direct {v2, v0}, LX/H0i;-><init>(Landroid/content/Context;)V

    new-instance v3, LX/H0X;

    invoke-direct {v3, v0}, LX/H0X;-><init>(Landroid/content/Context;)V

    new-instance p1, LX/H0s;

    invoke-direct {p1, v0}, LX/H0s;-><init>(Landroid/content/Context;)V

    invoke-static {v1, v2, v3, p1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    goto :goto_0
.end method

.method private static a(LX/0Px;I)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/1OM;",
            ">;I)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2413892
    move v1, v0

    move v2, v0

    .line 2413893
    :goto_0
    if-ge v1, p1, :cond_0

    .line 2413894
    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OM;

    .line 2413895
    invoke-static {v0}, LX/H0P;->a(LX/1OM;)I

    move-result v0

    add-int/2addr v2, v0

    .line 2413896
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2413897
    :cond_0
    return v2
.end method

.method private static a(LX/1OM;)I
    .locals 1

    .prologue
    .line 2413906
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2413898
    sget-object v0, LX/H0O;->SECTION_GAP:LX/H0O;

    invoke-virtual {v0}, LX/H0O;->toInt()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2413899
    iget-object v0, p0, LX/H0P;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307a7

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2413900
    new-instance v0, LX/H0N;

    invoke-direct {v0, v1}, LX/H0N;-><init>(Landroid/view/View;)V

    .line 2413901
    :goto_0
    return-object v0

    .line 2413902
    :cond_0
    iget-object v0, p0, LX/H0P;->b:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OM;

    .line 2413903
    if-eqz v0, :cond_1

    .line 2413904
    invoke-virtual {v0, p1, p2}, LX/1OM;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    goto :goto_0

    .line 2413905
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid viewType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2413874
    iget-object v1, p0, LX/H0P;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v3, :cond_3

    iget-object v0, p0, LX/H0P;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OM;

    .line 2413875
    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v4

    add-int/2addr v4, v2

    if-ge p2, v4, :cond_1

    .line 2413876
    sub-int v1, p2, v2

    .line 2413877
    invoke-virtual {v0, p1, v1}, LX/1OM;->a(LX/1a1;I)V

    .line 2413878
    :cond_0
    return-void

    .line 2413879
    :cond_1
    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v4

    add-int/2addr v4, v2

    if-lt p2, v4, :cond_2

    invoke-static {v0}, LX/H0P;->a(LX/1OM;)I

    move-result v4

    add-int/2addr v4, v2

    if-lt p2, v4, :cond_0

    .line 2413880
    :cond_2
    invoke-static {v0}, LX/H0P;->a(LX/1OM;)I

    move-result v0

    add-int/2addr v2, v0

    .line 2413881
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2413882
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V
    .locals 4

    .prologue
    .line 2413853
    iget-object v0, p0, LX/H0P;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_4

    iget-object v0, p0, LX/H0P;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OM;

    .line 2413854
    instance-of v3, v0, LX/H0b;

    if-eqz v3, :cond_1

    .line 2413855
    check-cast v0, LX/H0b;

    .line 2413856
    iput-object p1, v0, LX/H0b;->c:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    .line 2413857
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2413858
    :cond_1
    instance-of v3, v0, LX/H0i;

    if-eqz v3, :cond_2

    .line 2413859
    check-cast v0, LX/H0i;

    .line 2413860
    iput-object p1, v0, LX/H0i;->d:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    .line 2413861
    new-instance v3, LX/H0J;

    invoke-direct {v3, p0}, LX/H0J;-><init>(LX/H0P;)V

    .line 2413862
    iput-object v3, v0, LX/H0i;->e:LX/H0J;

    .line 2413863
    goto :goto_1

    .line 2413864
    :cond_2
    instance-of v3, v0, LX/H0X;

    if-eqz v3, :cond_3

    .line 2413865
    check-cast v0, LX/H0X;

    .line 2413866
    invoke-virtual {v0, p1}, LX/H0X;->a(Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V

    goto :goto_1

    .line 2413867
    :cond_3
    instance-of v3, v0, LX/H0s;

    if-eqz v3, :cond_0

    .line 2413868
    check-cast v0, LX/H0s;

    .line 2413869
    invoke-virtual {v0, p1}, LX/H0s;->a(Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V

    .line 2413870
    new-instance v3, LX/H0K;

    invoke-direct {v3, p0}, LX/H0K;-><init>(LX/H0P;)V

    .line 2413871
    iput-object v3, v0, LX/H0s;->e:LX/H0K;

    .line 2413872
    goto :goto_1

    .line 2413873
    :cond_4
    return-void
.end method

.method public final b(Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V
    .locals 4

    .prologue
    .line 2413802
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2413803
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/H0P;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2413804
    iget-object v0, p0, LX/H0P;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OM;

    .line 2413805
    instance-of v3, v0, LX/H0b;

    if-nez v3, :cond_0

    .line 2413806
    instance-of v3, v0, LX/H0i;

    if-eqz v3, :cond_1

    .line 2413807
    check-cast v0, LX/H0i;

    .line 2413808
    :try_start_0
    invoke-virtual {v0, p1}, LX/H0i;->b(Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V
    :try_end_0
    .catch LX/Gzr; {:try_start_0 .. :try_end_0} :catch_0

    .line 2413809
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2413810
    :catch_0
    iget-object v0, p0, LX/H0P;->a:LX/0Px;

    invoke-static {v0, v1}, LX/H0P;->a(LX/0Px;I)I

    move-result v0

    .line 2413811
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2413812
    :cond_1
    instance-of v3, v0, LX/H0X;

    if-nez v3, :cond_0

    .line 2413813
    instance-of v3, v0, LX/H0s;

    if-eqz v3, :cond_0

    .line 2413814
    check-cast v0, LX/H0s;

    .line 2413815
    :try_start_1
    invoke-virtual {v0, p1}, LX/H0s;->b(Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V
    :try_end_1
    .catch LX/Gzr; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 2413816
    :catch_1
    iget-object v0, p0, LX/H0P;->a:LX/0Px;

    invoke-static {v0, v1}, LX/H0P;->a(LX/0Px;I)I

    move-result v0

    .line 2413817
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2413818
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2413819
    new-instance v0, LX/H0L;

    const-string v1, "We have empty required fields"

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/H0L;-><init>(Ljava/lang/String;LX/0Px;)V

    throw v0

    .line 2413820
    :cond_3
    return-void
.end method

.method public final c(Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V
    .locals 4

    .prologue
    .line 2413836
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2413837
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/H0P;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2413838
    iget-object v0, p0, LX/H0P;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OM;

    .line 2413839
    instance-of v3, v0, LX/H0b;

    if-nez v3, :cond_0

    .line 2413840
    instance-of v3, v0, LX/H0i;

    if-eqz v3, :cond_1

    .line 2413841
    :try_start_0
    invoke-static {p1}, LX/H0i;->c(Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V
    :try_end_0
    .catch LX/Gzs; {:try_start_0 .. :try_end_0} :catch_0

    .line 2413842
    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2413843
    new-instance v0, LX/H0M;

    const-string v1, "We have too long sentences"

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/H0M;-><init>(Ljava/lang/String;LX/0Px;)V

    throw v0

    .line 2413844
    :catch_0
    iget-object v0, p0, LX/H0P;->a:LX/0Px;

    invoke-static {v0, v1}, LX/H0P;->a(LX/0Px;I)I

    move-result v0

    .line 2413845
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2413846
    :cond_1
    instance-of v3, v0, LX/H0X;

    if-nez v3, :cond_0

    .line 2413847
    instance-of v0, v0, LX/H0s;

    if-eqz v0, :cond_0

    .line 2413848
    :try_start_1
    invoke-static {p1}, LX/H0s;->c(Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V
    :try_end_1
    .catch LX/Gzs; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 2413849
    :catch_1
    iget-object v0, p0, LX/H0P;->a:LX/0Px;

    invoke-static {v0, v1}, LX/H0P;->a(LX/0Px;I)I

    move-result v0

    .line 2413850
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2413851
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2413852
    :cond_3
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2413825
    iget-object v1, p0, LX/H0P;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v3, :cond_2

    iget-object v0, p0, LX/H0P;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OM;

    .line 2413826
    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v4

    add-int/2addr v4, v2

    if-ge p1, v4, :cond_0

    .line 2413827
    sub-int v1, p1, v2

    .line 2413828
    invoke-virtual {v0, v1}, LX/1OM;->getItemViewType(I)I

    move-result v1

    .line 2413829
    iget-object v2, p0, LX/H0P;->b:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    .line 2413830
    :goto_1
    return v0

    .line 2413831
    :cond_0
    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v4

    add-int/2addr v4, v2

    if-lt p1, v4, :cond_1

    invoke-static {v0}, LX/H0P;->a(LX/1OM;)I

    move-result v4

    add-int/2addr v4, v2

    if-ge p1, v4, :cond_1

    .line 2413832
    sget-object v0, LX/H0O;->SECTION_GAP:LX/H0O;

    invoke-virtual {v0}, LX/H0O;->toInt()I

    move-result v0

    goto :goto_1

    .line 2413833
    :cond_1
    invoke-static {v0}, LX/H0P;->a(LX/1OM;)I

    move-result v0

    add-int/2addr v2, v0

    .line 2413834
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2413835
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final ij_()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2413821
    iget-object v1, p0, LX/H0P;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LX/H0P;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1OM;

    .line 2413822
    invoke-static {v0}, LX/H0P;->a(LX/1OM;)I

    move-result v0

    add-int/2addr v2, v0

    .line 2413823
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2413824
    :cond_0
    return v2
.end method
