.class public final LX/GKC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GDA;


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;)V
    .locals 0

    .prologue
    .line 2340502
    iput-object p1, p0, LX/GKC;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;)V
    .locals 6
    .param p1    # Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2340495
    if-nez p1, :cond_0

    .line 2340496
    :goto_0
    return-void

    .line 2340497
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;->a()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;->b()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, LX/6aA;->a(DD)Landroid/location/Location;

    move-result-object v0

    .line 2340498
    iget-object v1, p0, LX/GKC;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;->c()D

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->a(Landroid/location/Location;D)V

    .line 2340499
    iget-object v1, p0, LX/GKC;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    .line 2340500
    iget-object v2, v1, LX/GHg;->b:LX/GCE;

    move-object v1, v2

    .line 2340501
    new-instance v2, LX/GFa;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;->c()D

    move-result-wide v4

    invoke-direct {v2, v0, v4, v5}, LX/GFa;-><init>(Landroid/location/Location;D)V

    invoke-virtual {v1, v2}, LX/GCE;->a(LX/8wN;)V

    goto :goto_0
.end method
