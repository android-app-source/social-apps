.class public final LX/GlH;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 32

    .prologue
    .line 2390482
    const-wide/16 v24, 0x0

    .line 2390483
    const/16 v23, 0x0

    .line 2390484
    const/16 v22, 0x0

    .line 2390485
    const/16 v21, 0x0

    .line 2390486
    const/16 v20, 0x0

    .line 2390487
    const/16 v19, 0x0

    .line 2390488
    const/16 v18, 0x0

    .line 2390489
    const/4 v15, 0x0

    .line 2390490
    const-wide/16 v16, 0x0

    .line 2390491
    const/4 v14, 0x0

    .line 2390492
    const/4 v13, 0x0

    .line 2390493
    const/4 v12, 0x0

    .line 2390494
    const-wide/16 v10, 0x0

    .line 2390495
    const/4 v9, 0x0

    .line 2390496
    const/4 v8, 0x0

    .line 2390497
    const/4 v7, 0x0

    .line 2390498
    const/4 v6, 0x0

    .line 2390499
    const/4 v5, 0x0

    .line 2390500
    const/4 v4, 0x0

    .line 2390501
    const/4 v3, 0x0

    .line 2390502
    const/4 v2, 0x0

    .line 2390503
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v26

    sget-object v27, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v26

    move-object/from16 v1, v27

    if-eq v0, v1, :cond_17

    .line 2390504
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2390505
    const/4 v2, 0x0

    .line 2390506
    :goto_0
    return v2

    .line 2390507
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v6, :cond_10

    .line 2390508
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2390509
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2390510
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_0

    if-eqz v2, :cond_0

    .line 2390511
    const-string v6, "archived_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2390512
    const/4 v2, 0x1

    .line 2390513
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    move v3, v2

    goto :goto_1

    .line 2390514
    :cond_1
    const-string v6, "community_category"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2390515
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupCategory;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupCategory;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v28, v2

    goto :goto_1

    .line 2390516
    :cond_2
    const-string v6, "groupItemCoverPhoto"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2390517
    invoke-static/range {p0 .. p1}, LX/Dab;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto :goto_1

    .line 2390518
    :cond_3
    const-string v6, "group_feed"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2390519
    invoke-static/range {p0 .. p1}, LX/Daa;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v26, v2

    goto :goto_1

    .line 2390520
    :cond_4
    const-string v6, "has_viewer_favorited"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    .line 2390521
    const/4 v2, 0x1

    .line 2390522
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v12, v2

    move/from16 v25, v6

    goto :goto_1

    .line 2390523
    :cond_5
    const-string v6, "has_viewer_hidden"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 2390524
    const/4 v2, 0x1

    .line 2390525
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v11, v2

    move/from16 v24, v6

    goto :goto_1

    .line 2390526
    :cond_6
    const-string v6, "id"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 2390527
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v23, v2

    goto/16 :goto_1

    .line 2390528
    :cond_7
    const-string v6, "is_viewer_newly_added"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2390529
    const/4 v2, 0x1

    .line 2390530
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move v10, v2

    move/from16 v22, v6

    goto/16 :goto_1

    .line 2390531
    :cond_8
    const-string v6, "last_activity_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    .line 2390532
    const/4 v2, 0x1

    .line 2390533
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v9, v2

    move-wide/from16 v20, v6

    goto/16 :goto_1

    .line 2390534
    :cond_9
    const-string v6, "name"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 2390535
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v19, v2

    goto/16 :goto_1

    .line 2390536
    :cond_a
    const-string v6, "viewer_admin_type"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_b

    .line 2390537
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupAdminType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupAdminType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_1

    .line 2390538
    :cond_b
    const-string v6, "viewer_join_state"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_c

    .line 2390539
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v15, v2

    goto/16 :goto_1

    .line 2390540
    :cond_c
    const-string v6, "viewer_last_visited_time"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_d

    .line 2390541
    const/4 v2, 0x1

    .line 2390542
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    move v8, v2

    move-wide/from16 v16, v6

    goto/16 :goto_1

    .line 2390543
    :cond_d
    const-string v6, "viewer_leave_scenario"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_e

    .line 2390544
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v14, v2

    goto/16 :goto_1

    .line 2390545
    :cond_e
    const-string v6, "viewer_subscription_level"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 2390546
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move v13, v2

    goto/16 :goto_1

    .line 2390547
    :cond_f
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2390548
    :cond_10
    const/16 v2, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2390549
    if-eqz v3, :cond_11

    .line 2390550
    const/4 v3, 0x0

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2390551
    :cond_11
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2390552
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2390553
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2390554
    if-eqz v12, :cond_12

    .line 2390555
    const/4 v2, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2390556
    :cond_12
    if-eqz v11, :cond_13

    .line 2390557
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2390558
    :cond_13
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2390559
    if-eqz v10, :cond_14

    .line 2390560
    const/4 v2, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v22

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2390561
    :cond_14
    if-eqz v9, :cond_15

    .line 2390562
    const/16 v3, 0x8

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v20

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2390563
    :cond_15
    const/16 v2, 0x9

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2390564
    const/16 v2, 0xa

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2390565
    const/16 v2, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v15}, LX/186;->b(II)V

    .line 2390566
    if-eqz v8, :cond_16

    .line 2390567
    const/16 v3, 0xc

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v16

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2390568
    :cond_16
    const/16 v2, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v14}, LX/186;->b(II)V

    .line 2390569
    const/16 v2, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v13}, LX/186;->b(II)V

    .line 2390570
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_17
    move/from16 v26, v21

    move/from16 v27, v22

    move/from16 v28, v23

    move/from16 v22, v15

    move/from16 v23, v18

    move v15, v12

    move/from16 v18, v13

    move v13, v8

    move v12, v6

    move v8, v2

    move/from16 v29, v9

    move v9, v3

    move v3, v7

    move-wide/from16 v30, v16

    move-wide/from16 v16, v10

    move v11, v5

    move v10, v4

    move-wide/from16 v4, v24

    move/from16 v25, v20

    move/from16 v24, v19

    move-wide/from16 v20, v30

    move/from16 v19, v14

    move/from16 v14, v29

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 9

    .prologue
    const/16 v8, 0xd

    const/16 v7, 0xb

    const/16 v6, 0xa

    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    .line 2390571
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2390572
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2390573
    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    .line 2390574
    const-string v2, "archived_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2390575
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2390576
    :cond_0
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2390577
    if-eqz v0, :cond_1

    .line 2390578
    const-string v0, "community_category"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2390579
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2390580
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2390581
    if-eqz v0, :cond_2

    .line 2390582
    const-string v1, "groupItemCoverPhoto"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2390583
    invoke-static {p0, v0, p2, p3}, LX/Dab;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2390584
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2390585
    if-eqz v0, :cond_3

    .line 2390586
    const-string v1, "group_feed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2390587
    invoke-static {p0, v0, p2}, LX/Daa;->a(LX/15i;ILX/0nX;)V

    .line 2390588
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2390589
    if-eqz v0, :cond_4

    .line 2390590
    const-string v1, "has_viewer_favorited"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2390591
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2390592
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2390593
    if-eqz v0, :cond_5

    .line 2390594
    const-string v1, "has_viewer_hidden"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2390595
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2390596
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2390597
    if-eqz v0, :cond_6

    .line 2390598
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2390599
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2390600
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2390601
    if-eqz v0, :cond_7

    .line 2390602
    const-string v1, "is_viewer_newly_added"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2390603
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2390604
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2390605
    cmp-long v2, v0, v4

    if-eqz v2, :cond_8

    .line 2390606
    const-string v2, "last_activity_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2390607
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2390608
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2390609
    if-eqz v0, :cond_9

    .line 2390610
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2390611
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2390612
    :cond_9
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 2390613
    if-eqz v0, :cond_a

    .line 2390614
    const-string v0, "viewer_admin_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2390615
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2390616
    :cond_a
    invoke-virtual {p0, p1, v7}, LX/15i;->g(II)I

    move-result v0

    .line 2390617
    if-eqz v0, :cond_b

    .line 2390618
    const-string v0, "viewer_join_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2390619
    invoke-virtual {p0, p1, v7}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2390620
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2390621
    cmp-long v2, v0, v4

    if-eqz v2, :cond_c

    .line 2390622
    const-string v2, "viewer_last_visited_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2390623
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2390624
    :cond_c
    invoke-virtual {p0, p1, v8}, LX/15i;->g(II)I

    move-result v0

    .line 2390625
    if-eqz v0, :cond_d

    .line 2390626
    const-string v0, "viewer_leave_scenario"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2390627
    invoke-virtual {p0, p1, v8}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2390628
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2390629
    if-eqz v0, :cond_e

    .line 2390630
    const-string v0, "viewer_subscription_level"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2390631
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2390632
    :cond_e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2390633
    return-void
.end method
