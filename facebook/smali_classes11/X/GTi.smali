.class public final LX/GTi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field public final synthetic a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V
    .locals 0

    .prologue
    .line 2355786
    iput-object p1, p0, LX/GTi;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2355781
    iget-object v0, p0, LX/GTi;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-static {v0, p3}, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->b(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;I)Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    move-result-object v0

    .line 2355782
    if-eqz v0, :cond_0

    .line 2355783
    iget-object v1, p0, LX/GTi;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->s:LX/GTt;

    .line 2355784
    iget-object p0, v1, LX/GTt;->a:LX/0Zb;

    const-string p1, "nearby_friends_now_nux_privacy_changed"

    invoke-static {v1, p1}, LX/GTt;->a(LX/GTt;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string p2, "privacy_type"

    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;->b()LX/1Fd;

    move-result-object p3

    invoke-interface {p3}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    invoke-interface {p0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2355785
    :cond_0
    return-void
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2355780
    return-void
.end method
