.class public LX/GrE;
.super LX/Ci8;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static v:LX/0Xm;


# instance fields
.field public a:LX/Go0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/3kp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Go7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Landroid/content/Context;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/Gn1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:Landroid/view/View;

.field public i:Landroid/view/View;

.field private j:Landroid/widget/FrameLayout;

.field public k:Landroid/view/View;

.field public l:Ljava/lang/String;

.field public m:I

.field public n:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;",
            ">;"
        }
    .end annotation
.end field

.field public o:Z

.field public p:I

.field public q:I

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:Lcom/facebook/widget/PhotoToggleButton;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2397682
    invoke-direct {p0}, LX/Ci8;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/GrE;
    .locals 10

    .prologue
    .line 2397669
    const-class v1, LX/GrE;

    monitor-enter v1

    .line 2397670
    :try_start_0
    sget-object v0, LX/GrE;->v:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2397671
    sput-object v2, LX/GrE;->v:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2397672
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2397673
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2397674
    new-instance v3, LX/GrE;

    invoke-direct {v3}, LX/GrE;-><init>()V

    .line 2397675
    invoke-static {v0}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object v4

    check-cast v4, LX/Go0;

    invoke-static {v0}, LX/3kp;->b(LX/0QB;)LX/3kp;

    move-result-object v5

    check-cast v5, LX/3kp;

    invoke-static {v0}, LX/Go7;->a(LX/0QB;)LX/Go7;

    move-result-object v6

    check-cast v6, LX/Go7;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const-class v8, Landroid/content/Context;

    invoke-interface {v0, v8}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/Context;

    invoke-static {v0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v9

    check-cast v9, LX/Chv;

    invoke-static {v0}, LX/Gn1;->b(LX/0QB;)LX/Gn1;

    move-result-object p0

    check-cast p0, LX/Gn1;

    .line 2397676
    iput-object v4, v3, LX/GrE;->a:LX/Go0;

    iput-object v5, v3, LX/GrE;->b:LX/3kp;

    iput-object v6, v3, LX/GrE;->c:LX/Go7;

    iput-object v7, v3, LX/GrE;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iput-object v8, v3, LX/GrE;->e:Landroid/content/Context;

    iput-object v9, v3, LX/GrE;->f:LX/Chv;

    iput-object p0, v3, LX/GrE;->g:LX/Gn1;

    .line 2397677
    move-object v0, v3

    .line 2397678
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2397679
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GrE;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2397680
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2397681
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/GrE;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2397667
    iget-object v0, p0, LX/GrE;->a:LX/Go0;

    new-instance v1, LX/GrC;

    invoke-direct {v1, p0, p2}, LX/GrC;-><init>(LX/GrE;Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, LX/Go0;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2397668
    return-void
.end method

.method public static b(Lcom/facebook/widget/PhotoToggleButton;)V
    .locals 14

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 2397660
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 2397661
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v6, 0x64

    add-long/2addr v2, v6

    move v6, v5

    move v7, v4

    .line 2397662
    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v6

    .line 2397663
    invoke-virtual {p0, v6}, Lcom/facebook/widget/PhotoToggleButton;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2397664
    const/4 v10, 0x1

    move-wide v6, v0

    move-wide v8, v2

    move v11, v5

    move v12, v5

    move v13, v4

    invoke-static/range {v6 .. v13}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 2397665
    invoke-virtual {p0, v0}, Lcom/facebook/widget/PhotoToggleButton;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 2397666
    return-void
.end method

.method public static e(LX/GrE;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2397592
    iput-boolean v0, p0, LX/GrE;->s:Z

    .line 2397593
    iput v0, p0, LX/GrE;->p:I

    .line 2397594
    iget-object v0, p0, LX/GrE;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->y(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 2397595
    iget-object v0, p0, LX/GrE;->i:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2397596
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 2

    .prologue
    .line 2397654
    iget-boolean v0, p0, LX/GrE;->o:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0d1878

    .line 2397655
    :goto_0
    iget-object v1, p0, LX/GrE;->k:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/GrE;->j:Landroid/widget/FrameLayout;

    .line 2397656
    iget-object v0, p0, LX/GrE;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2397657
    iget-object v0, p0, LX/GrE;->j:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2397658
    return-void

    .line 2397659
    :cond_0
    const v0, 0x7f0d187c

    goto :goto_0
.end method

.method public final b()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2397597
    iget-boolean v0, p0, LX/GrE;->o:Z

    if-eqz v0, :cond_3

    const v0, 0x7f0d187a

    .line 2397598
    :goto_0
    iget-boolean v1, p0, LX/GrE;->o:Z

    if-eqz v1, :cond_4

    const v1, 0x7f0d187b

    .line 2397599
    :goto_1
    iget-object v4, p0, LX/GrE;->k:Landroid/view/View;

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 2397600
    iget-object v4, p0, LX/GrE;->k:Landroid/view/View;

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/PhotoToggleButton;

    iput-object v1, p0, LX/GrE;->u:Lcom/facebook/widget/PhotoToggleButton;

    .line 2397601
    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2397602
    iget-object v1, p0, LX/GrE;->u:Lcom/facebook/widget/PhotoToggleButton;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/PhotoToggleButton;->setClickable(Z)V

    .line 2397603
    iget-object v1, p0, LX/GrE;->u:Lcom/facebook/widget/PhotoToggleButton;

    new-instance v4, LX/GrD;

    invoke-direct {v4, p0}, LX/GrD;-><init>(LX/GrE;)V

    .line 2397604
    iput-object v4, v1, Lcom/facebook/widget/PhotoToggleButton;->e:LX/4oG;

    .line 2397605
    new-instance v1, LX/GrA;

    invoke-direct {v1, p0}, LX/GrA;-><init>(LX/GrE;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2397606
    iget-object v0, p0, LX/GrE;->n:LX/0Px;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->AUDIO_MUTED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    invoke-virtual {v0, v1}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    move v1, v2

    .line 2397607
    :goto_2
    iget-object v4, p0, LX/GrE;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v0, LX/Gn0;->f:LX/0Tn;

    iget-object v5, p0, LX/GrE;->l:Ljava/lang/String;

    invoke-virtual {v0, v5}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    invoke-interface {v4, v0, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 2397608
    if-eqz v0, :cond_0

    move v1, v0

    .line 2397609
    :cond_0
    iget-object v0, p0, LX/GrE;->g:LX/Gn1;

    invoke-virtual {v0}, LX/Chj;->b()LX/ChL;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2397610
    iget-object v0, p0, LX/GrE;->g:LX/Gn1;

    invoke-virtual {v0}, LX/Chj;->b()LX/ChL;

    move-result-object v0

    invoke-interface {v0}, LX/ChL;->k()LX/ChZ;

    move-result-object v0

    sget-object v4, LX/ChZ;->ON:LX/ChZ;

    if-ne v0, v4, :cond_6

    move v1, v2

    .line 2397611
    :cond_1
    :goto_3
    iput-boolean v1, p0, LX/GrE;->t:Z

    .line 2397612
    const-string v0, "instant_shopping_audio_button_on_enter"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v0, v2}, LX/GrE;->a$redex0(LX/GrE;Ljava/lang/String;Ljava/lang/String;)V

    .line 2397613
    iget-object v0, p0, LX/GrE;->u:Lcom/facebook/widget/PhotoToggleButton;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/PhotoToggleButton;->setChecked(Z)V

    .line 2397614
    iget-object v0, p0, LX/GrE;->u:Lcom/facebook/widget/PhotoToggleButton;

    const/high16 v6, 0x40400000    # 3.0f

    .line 2397615
    iget-object v1, p0, LX/GrE;->b:LX/3kp;

    invoke-virtual {v1}, LX/3kp;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/GrE;->n:LX/0Px;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->AUDIO_CONTROL_FLOATING:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    invoke-virtual {v1, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2397616
    :cond_2
    :goto_4
    return-void

    .line 2397617
    :cond_3
    const v0, 0x7f0d187e

    goto/16 :goto_0

    .line 2397618
    :cond_4
    const v1, 0x7f0d187f

    goto/16 :goto_1

    :cond_5
    move v1, v3

    .line 2397619
    goto :goto_2

    .line 2397620
    :cond_6
    iget-object v0, p0, LX/GrE;->g:LX/Gn1;

    invoke-virtual {v0}, LX/Chj;->b()LX/ChL;

    move-result-object v0

    invoke-interface {v0}, LX/ChL;->k()LX/ChZ;

    move-result-object v0

    sget-object v2, LX/ChZ;->OFF:LX/ChZ;

    if-ne v0, v2, :cond_1

    move v1, v3

    .line 2397621
    goto :goto_3

    .line 2397622
    :cond_7
    iget-object v1, p0, LX/GrE;->e:Landroid/content/Context;

    const v2, 0x7f0828d1

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2397623
    iget-object v2, p0, LX/GrE;->e:Landroid/content/Context;

    const v3, 0x7f0828d2

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2397624
    new-instance v3, LX/0hs;

    iget-object v4, p0, LX/GrE;->e:Landroid/content/Context;

    const/4 v5, 0x2

    invoke-direct {v3, v4, v5}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2397625
    invoke-virtual {v3, v1}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 2397626
    invoke-virtual {v3, v2}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2397627
    iget-object v1, p0, LX/GrE;->e:Landroid/content/Context;

    invoke-static {v1, v6}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    .line 2397628
    iput v1, v3, LX/0ht;->z:I

    .line 2397629
    iget-object v1, p0, LX/GrE;->e:Landroid/content/Context;

    invoke-static {v1, v6}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v1

    .line 2397630
    iput v1, v3, LX/0ht;->y:I

    .line 2397631
    sget-object v1, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v3, v1}, LX/0ht;->a(LX/3AV;)V

    .line 2397632
    invoke-virtual {v3, v0}, LX/0ht;->a(Landroid/view/View;)V

    .line 2397633
    const/16 v1, 0x1388

    .line 2397634
    iput v1, v3, LX/0hs;->t:I

    .line 2397635
    invoke-virtual {v3}, LX/0ht;->d()V

    .line 2397636
    iget-object v1, p0, LX/GrE;->b:LX/3kp;

    invoke-virtual {v1}, LX/3kp;->a()V

    goto :goto_4
.end method

.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 2397637
    check-cast p1, LX/Cic;

    .line 2397638
    iget-boolean v0, p0, LX/GrE;->r:Z

    if-nez v0, :cond_1

    .line 2397639
    :cond_0
    :goto_0
    return-void

    .line 2397640
    :cond_1
    iget v0, p0, LX/GrE;->p:I

    .line 2397641
    iget v1, p1, LX/Cic;->b:I

    move v1, v1

    .line 2397642
    add-int/2addr v0, v1

    iput v0, p0, LX/GrE;->p:I

    .line 2397643
    iget v0, p0, LX/GrE;->q:I

    iget v1, p0, LX/GrE;->p:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, LX/GrE;->p:I

    .line 2397644
    const/4 v0, 0x0

    iget v1, p0, LX/GrE;->p:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, LX/GrE;->p:I

    .line 2397645
    iget-boolean v0, p0, LX/GrE;->s:Z

    if-nez v0, :cond_2

    iget v0, p0, LX/GrE;->p:I

    iget v1, p0, LX/GrE;->q:I

    if-ne v0, v1, :cond_2

    .line 2397646
    const/4 v2, 0x1

    iput-boolean v2, p0, LX/GrE;->s:Z

    .line 2397647
    iget v2, p0, LX/GrE;->q:I

    iput v2, p0, LX/GrE;->p:I

    .line 2397648
    iget-object v2, p0, LX/GrE;->h:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    iget v3, p0, LX/GrE;->q:I

    neg-int v3, v3

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->y(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 2397649
    iget-object v2, p0, LX/GrE;->i:Landroid/view/View;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2397650
    :cond_2
    iget-boolean v0, p0, LX/GrE;->s:Z

    if-eqz v0, :cond_0

    .line 2397651
    iget v0, p1, LX/Cic;->b:I

    move v0, v0

    .line 2397652
    if-gez v0, :cond_0

    .line 2397653
    invoke-static {p0}, LX/GrE;->e(LX/GrE;)V

    goto :goto_0
.end method
