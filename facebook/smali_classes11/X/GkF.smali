.class public final LX/GkF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;)V
    .locals 0

    .prologue
    .line 2388947
    iput-object p1, p0, LX/GkF;->a:Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 2388948
    iget-object v0, p0, LX/GkF;->a:Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;

    iget-object v0, v0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->c:LX/Gkk;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/GkF;->a:Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;

    const/4 v1, 0x0

    .line 2388949
    iget-object p1, v0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->h:LX/GkA;

    invoke-virtual {p1}, LX/GkA;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_3

    .line 2388950
    :cond_0
    :goto_0
    move v0, v1

    .line 2388951
    if-eqz v0, :cond_2

    .line 2388952
    iget-object v0, p0, LX/GkF;->a:Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;

    .line 2388953
    iget-object v1, v0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->i:LX/GkK;

    if-nez v1, :cond_1

    .line 2388954
    new-instance v1, LX/GkL;

    invoke-direct {v1, v0}, LX/GkL;-><init>(Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;)V

    iput-object v1, v0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->i:LX/GkK;

    .line 2388955
    :cond_1
    iget-object v1, v0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->c:LX/Gkk;

    const/16 p0, 0xa

    iget-object p1, v0, Lcom/facebook/groups/editfavorites/fragment/GroupsEditFavoritesFragment;->i:LX/GkK;

    invoke-interface {v1, p0, p1}, LX/Gkk;->a(ILX/GkK;)V

    .line 2388956
    :cond_2
    return-void

    .line 2388957
    :cond_3
    if-lez p3, :cond_0

    if-lez p4, :cond_0

    .line 2388958
    add-int p1, p2, p3

    add-int/lit8 p1, p1, 0x3

    if-le p1, p4, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 2388946
    return-void
.end method
