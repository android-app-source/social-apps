.class public final enum LX/GGB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GGB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GGB;

.field public static final enum ACTIVE:LX/GGB;

.field public static final enum CREATING:LX/GGB;

.field public static final enum ERROR:LX/GGB;

.field public static final enum EXTENDABLE:LX/GGB;

.field public static final enum FINISHED:LX/GGB;

.field public static final enum INACTIVE:LX/GGB;

.field public static final enum NEVER_BOOSTED:LX/GGB;

.field public static final enum PAUSED:LX/GGB;

.field public static final enum PENDING:LX/GGB;

.field public static final enum REJECTED:LX/GGB;

.field public static final enum UNSET_OR_UNRECOGNIZED_ENUM_VALUE:LX/GGB;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2333363
    new-instance v0, LX/GGB;

    const-string v1, "NEVER_BOOSTED"

    invoke-direct {v0, v1, v3}, LX/GGB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    .line 2333364
    new-instance v0, LX/GGB;

    const-string v1, "INACTIVE"

    invoke-direct {v0, v1, v4}, LX/GGB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GGB;->INACTIVE:LX/GGB;

    .line 2333365
    new-instance v0, LX/GGB;

    const-string v1, "ACTIVE"

    invoke-direct {v0, v1, v5}, LX/GGB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GGB;->ACTIVE:LX/GGB;

    .line 2333366
    new-instance v0, LX/GGB;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v6}, LX/GGB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GGB;->PAUSED:LX/GGB;

    .line 2333367
    new-instance v0, LX/GGB;

    const-string v1, "FINISHED"

    invoke-direct {v0, v1, v7}, LX/GGB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GGB;->FINISHED:LX/GGB;

    .line 2333368
    new-instance v0, LX/GGB;

    const-string v1, "EXTENDABLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/GGB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GGB;->EXTENDABLE:LX/GGB;

    .line 2333369
    new-instance v0, LX/GGB;

    const-string v1, "REJECTED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/GGB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GGB;->REJECTED:LX/GGB;

    .line 2333370
    new-instance v0, LX/GGB;

    const-string v1, "PENDING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/GGB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GGB;->PENDING:LX/GGB;

    .line 2333371
    new-instance v0, LX/GGB;

    const-string v1, "ERROR"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/GGB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GGB;->ERROR:LX/GGB;

    .line 2333372
    new-instance v0, LX/GGB;

    const-string v1, "CREATING"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/GGB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GGB;->CREATING:LX/GGB;

    .line 2333373
    new-instance v0, LX/GGB;

    const-string v1, "UNSET_OR_UNRECOGNIZED_ENUM_VALUE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/GGB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GGB;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:LX/GGB;

    .line 2333374
    const/16 v0, 0xb

    new-array v0, v0, [LX/GGB;

    sget-object v1, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    aput-object v1, v0, v3

    sget-object v1, LX/GGB;->INACTIVE:LX/GGB;

    aput-object v1, v0, v4

    sget-object v1, LX/GGB;->ACTIVE:LX/GGB;

    aput-object v1, v0, v5

    sget-object v1, LX/GGB;->PAUSED:LX/GGB;

    aput-object v1, v0, v6

    sget-object v1, LX/GGB;->FINISHED:LX/GGB;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/GGB;->EXTENDABLE:LX/GGB;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/GGB;->REJECTED:LX/GGB;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/GGB;->PENDING:LX/GGB;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/GGB;->ERROR:LX/GGB;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/GGB;->CREATING:LX/GGB;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/GGB;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:LX/GGB;

    aput-object v2, v0, v1

    sput-object v0, LX/GGB;->$VALUES:[LX/GGB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2333375
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GGB;
    .locals 1

    .prologue
    .line 2333376
    const-class v0, LX/GGB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GGB;

    return-object v0
.end method

.method public static values()[LX/GGB;
    .locals 1

    .prologue
    .line 2333377
    sget-object v0, LX/GGB;->$VALUES:[LX/GGB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GGB;

    return-object v0
.end method
