.class public final LX/HBq;
.super LX/1a1;
.source ""


# instance fields
.field public final synthetic l:Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;

.field public final m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

.field public final n:Lcom/facebook/resources/ui/FbTextView;

.field public final o:Lcom/facebook/resources/ui/FbTextView;

.field public final p:Lcom/facebook/resources/ui/FbTextView;

.field public final q:Lcom/facebook/resources/ui/FbTextView;

.field public final r:I


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;Landroid/view/View;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2437912
    iput-object p1, p0, LX/HBq;->l:Lcom/facebook/pages/common/editpage/AllTemplatesRecyclerViewAdapter;

    .line 2437913
    invoke-direct {p0, p2}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2437914
    iput p3, p0, LX/HBq;->r:I

    .line 2437915
    sget-object v0, LX/HBp;->HEADER:LX/HBp;

    invoke-virtual {v0}, LX/HBp;->ordinal()I

    move-result v0

    if-ne p3, v0, :cond_0

    .line 2437916
    iput-object v1, p0, LX/HBq;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2437917
    iput-object v1, p0, LX/HBq;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 2437918
    iput-object v1, p0, LX/HBq;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 2437919
    iput-object v1, p0, LX/HBq;->p:Lcom/facebook/resources/ui/FbTextView;

    .line 2437920
    iput-object v1, p0, LX/HBq;->q:Lcom/facebook/resources/ui/FbTextView;

    .line 2437921
    :goto_0
    return-void

    .line 2437922
    :cond_0
    const v0, 0x7f0d2eb9

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    iput-object v0, p0, LX/HBq;->m:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2437923
    const v0, 0x7f0d2eba

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/HBq;->n:Lcom/facebook/resources/ui/FbTextView;

    .line 2437924
    const v0, 0x7f0d2ebb

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/HBq;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 2437925
    const v0, 0x7f0d2ebc

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/HBq;->p:Lcom/facebook/resources/ui/FbTextView;

    .line 2437926
    const v0, 0x7f0d2ebd

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/HBq;->q:Lcom/facebook/resources/ui/FbTextView;

    goto :goto_0
.end method
