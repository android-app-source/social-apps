.class public final LX/G6m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4lG;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;


# direct methods
.method public constructor <init>(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;)V
    .locals 0

    .prologue
    .line 2320750
    iput-object p1, p0, LX/G6m;->a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2320751
    iget-object v0, p0, LX/G6m;->a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    iget v0, v0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->f:I

    if-eqz v0, :cond_0

    .line 2320752
    iget-object v0, p0, LX/G6m;->a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    iget-object v0, v0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->d:Ljava/util/ArrayList;

    iget-object v1, p0, LX/G6m;->a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    iget v1, v1, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->f:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/DigitEditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/DigitEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2320753
    iget-object v0, p0, LX/G6m;->a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    iget-object v0, v0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->d:Ljava/util/ArrayList;

    iget-object v1, p0, LX/G6m;->a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    iget v1, v1, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->f:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/DigitEditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/DigitEditText;->setFocusableInTouchMode(Z)V

    .line 2320754
    iget-object v0, p0, LX/G6m;->a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    iget-object v0, v0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->d:Ljava/util/ArrayList;

    iget-object v1, p0, LX/G6m;->a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    iget v1, v1, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->f:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/DigitEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/DigitEditText;->setFocusableInTouchMode(Z)V

    .line 2320755
    iget-object v0, p0, LX/G6m;->a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    iget-object v0, v0, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->d:Ljava/util/ArrayList;

    iget-object v1, p0, LX/G6m;->a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    iget v1, v1, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->f:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/DigitEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/DigitEditText;->requestFocus()Z

    .line 2320756
    iget-object v0, p0, LX/G6m;->a:Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;

    invoke-static {v0}, Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;->e(Lcom/facebook/widget/splitinput/SplitFieldCodeInputView;)I

    .line 2320757
    :cond_0
    return-void
.end method
