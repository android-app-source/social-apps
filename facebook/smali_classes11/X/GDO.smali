.class public final LX/GDO;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$AdInterfacesGeocodeQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GDQ;

.field public final synthetic b:LX/GDS;


# direct methods
.method public constructor <init>(LX/GDS;LX/GDQ;)V
    .locals 0

    .prologue
    .line 2329774
    iput-object p1, p0, LX/GDO;->b:LX/GDS;

    iput-object p2, p0, LX/GDO;->a:LX/GDQ;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2329801
    iget-object v0, p0, LX/GDO;->a:LX/GDQ;

    invoke-interface {v0, p1}, LX/GDQ;->a(Ljava/lang/Throwable;)V

    .line 2329802
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2329775
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v7, 0x4

    const/4 v6, 0x3

    const v4, -0x49251d43

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2329776
    if-eqz p1, :cond_0

    .line 2329777
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2329778
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_5

    .line 2329779
    iget-object v0, p0, LX/GDO;->a:LX/GDQ;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "Location not found"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/GDQ;->a(Ljava/lang/Throwable;)V

    .line 2329780
    :goto_2
    return-void

    .line 2329781
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2329782
    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$AdInterfacesGeocodeQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$AdInterfacesGeocodeQueryModel;->a()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2329783
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0

    .line 2329784
    :cond_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2329785
    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$AdInterfacesGeocodeQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$AdInterfacesGeocodeQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2329786
    if-eqz v0, :cond_4

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    goto :goto_1

    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_3

    .line 2329787
    :cond_5
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2329788
    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$AdInterfacesGeocodeQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesGeocodeQueryModels$AdInterfacesGeocodeQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, v4}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_4
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2329789
    if-nez v4, :cond_7

    move v0, v1

    :goto_5
    if-eqz v0, :cond_9

    .line 2329790
    iget-object v0, p0, LX/GDO;->a:LX/GDQ;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "Location not found"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/GDQ;->a(Ljava/lang/Throwable;)V

    goto :goto_2

    .line 2329791
    :cond_6
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_4

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2329792
    :cond_7
    invoke-virtual {v3, v4, v1}, LX/15i;->g(II)I

    move-result v0

    if-nez v0, :cond_8

    move v0, v1

    goto :goto_5

    :cond_8
    move v0, v2

    goto :goto_5

    .line 2329793
    :cond_9
    invoke-virtual {v3, v4, v1}, LX/15i;->g(II)I

    move-result v0

    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2329794
    invoke-virtual {v3, v0, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_a

    invoke-virtual {v3, v0, v7}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 2329795
    :cond_a
    iget-object v0, p0, LX/GDO;->a:LX/GDQ;

    new-instance v1, Ljava/lang/Throwable;

    const-string v2, "Invalid location data"

    invoke-direct {v1, v2}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, LX/GDQ;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_2

    .line 2329796
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 2329797
    :cond_b
    :try_start_4
    iget-object v1, p0, LX/GDO;->a:LX/GDQ;

    const/4 v2, 0x3

    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    const/4 v2, 0x4

    invoke-virtual {v3, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    invoke-interface {v1, v4, v5, v2, v3}, LX/GDQ;->a(DD)V
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_2

    .line 2329798
    :catch_0
    move-exception v0

    .line 2329799
    iget-object v1, p0, LX/GDO;->b:LX/GDS;

    invoke-static {v1, v0}, LX/GDS;->a$redex0(LX/GDS;Ljava/lang/Throwable;)V

    .line 2329800
    iget-object v1, p0, LX/GDO;->a:LX/GDQ;

    invoke-interface {v1, v0}, LX/GDQ;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_2
.end method
