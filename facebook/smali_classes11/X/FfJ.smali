.class public LX/FfJ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/FfN;

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Cw8;",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/SeeMoreResultPageUnit;",
            ">;>;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Cw8;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/CwL;",
            ">;>;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Cw8;",
            "LX/CwL;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Cw8;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/Cw8;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/Cw8;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/Cw8;",
            ">;"
        }
    .end annotation
.end field

.field public i:I

.field public j:LX/FfG;

.field public k:LX/Cvm;


# direct methods
.method public constructor <init>(LX/FfG;LX/FfN;)V
    .locals 1
    .param p1    # LX/FfG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2267986
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2267987
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/FfJ;->b:Ljava/util/Map;

    .line 2267988
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/FfJ;->c:Ljava/util/Map;

    .line 2267989
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/FfJ;->d:Ljava/util/Map;

    .line 2267990
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/FfJ;->e:Ljava/util/Map;

    .line 2267991
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/FfJ;->f:Ljava/util/Set;

    .line 2267992
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/FfJ;->g:Ljava/util/Set;

    .line 2267993
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/FfJ;->h:Ljava/util/Set;

    .line 2267994
    const/16 v0, 0xa

    iput v0, p0, LX/FfJ;->i:I

    .line 2267995
    iput-object p1, p0, LX/FfJ;->j:LX/FfG;

    .line 2267996
    iput-object p2, p0, LX/FfJ;->a:LX/FfN;

    .line 2267997
    return-void
.end method

.method public static e(LX/FfJ;LX/Cw8;)V
    .locals 1

    .prologue
    .line 2267907
    iget-object v0, p0, LX/FfJ;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2267908
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;LX/Cw8;)V
    .locals 1

    .prologue
    .line 2267909
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/FfJ;->a(Ljava/lang/String;LX/Cw8;LX/CwL;)V

    .line 2267910
    return-void
.end method

.method public final a(Ljava/lang/String;LX/Cw8;LX/CwL;)V
    .locals 11
    .param p3    # LX/CwL;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2267911
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267912
    iget-object v0, p0, LX/FfJ;->h:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2267913
    sget-object v0, LX/Cw8;->People:LX/Cw8;

    if-ne p2, v0, :cond_5

    .line 2267914
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267915
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 2267916
    iget-object v0, p0, LX/FfJ;->c:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2267917
    iget-object v0, p0, LX/FfJ;->c:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2267918
    :cond_0
    if-eqz p3, :cond_3

    .line 2267919
    iget-object v0, p0, LX/FfJ;->d:Ljava/util/Map;

    invoke-interface {v0, p2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267920
    iget-object v0, p3, LX/CwL;->b:Ljava/lang/String;

    move-object v0, v0

    .line 2267921
    invoke-interface {v1, v0, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267922
    iget-object v0, p0, LX/FfJ;->k:LX/Cvm;

    if-eqz v0, :cond_3

    .line 2267923
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2267924
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CwL;

    .line 2267925
    iget-object v4, v0, LX/CwL;->e:Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-object v4, v4

    .line 2267926
    if-eqz v4, :cond_1

    .line 2267927
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2267928
    :cond_2
    iget-object v0, p0, LX/FfJ;->k:LX/Cvm;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2267929
    iget-object v3, v0, LX/Cvm;->f:Ljava/lang/String;

    if-nez v3, :cond_6

    .line 2267930
    :cond_3
    :goto_1
    iget-object v0, p0, LX/FfJ;->a:LX/FfN;

    iget v2, p0, LX/FfJ;->i:I

    iget-object v3, p0, LX/FfJ;->e:Ljava/util/Map;

    invoke-interface {v3, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v5

    new-instance v6, LX/FfI;

    invoke-direct {v6, p0, p2}, LX/FfI;-><init>(LX/FfJ;LX/Cw8;)V

    move-object v1, p1

    move-object v3, p2

    .line 2267931
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267932
    sget-object v7, LX/Cw8;->People:LX/Cw8;

    if-ne v3, v7, :cond_7

    const/4 v7, 0x1

    :goto_2
    invoke-static {v7}, LX/0PB;->checkState(Z)V

    .line 2267933
    iget-object v7, v0, LX/FfN;->e:LX/1Ck;

    new-instance p0, Ljava/lang/StringBuilder;

    const-string p1, "simple_search_loader_key"

    invoke-direct {p0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 2267934
    new-instance p1, LX/9yb;

    invoke-direct {p1}, LX/9yb;-><init>()V

    const-string p2, "query"

    new-instance p3, Ljava/lang/StringBuilder;

    const-string v3, "users-named("

    invoke-direct {p3, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p3

    const-string v3, ")"

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p1

    const-string p2, "count"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object p1

    const-string p2, "after"

    invoke-virtual {p1, p2, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p1

    const-string p2, "device"

    const-string p3, "android"

    invoke-virtual {p1, p2, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p1

    const-string p2, "filter"

    .line 2267935
    if-eqz v5, :cond_4

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result p3

    if-eqz p3, :cond_8

    .line 2267936
    :cond_4
    const/4 p3, 0x0

    .line 2267937
    :goto_3
    move-object p3, p3

    .line 2267938
    invoke-virtual {p1, p2, p3}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object p1

    const-string p2, "profile_picture_size"

    iget-object p3, v0, LX/FfN;->b:Landroid/content/res/Resources;

    const v3, 0x7f0b008d

    invoke-virtual {p3, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p3

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p1

    .line 2267939
    iget-object p2, p1, LX/0gW;->e:LX/0w7;

    move-object p1, p2

    .line 2267940
    iget-object p2, v0, LX/FfN;->a:LX/0tX;

    .line 2267941
    new-instance p3, LX/9yb;

    invoke-direct {p3}, LX/9yb;-><init>()V

    move-object p3, p3

    .line 2267942
    invoke-static {p3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object p3

    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {p3, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object p3

    invoke-virtual {p3, p1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object p1

    invoke-virtual {p2, p1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object p1

    .line 2267943
    move-object p1, p1

    .line 2267944
    new-instance p2, LX/FfM;

    invoke-direct {p2, v0, v6}, LX/FfM;-><init>(LX/FfN;LX/FfI;)V

    invoke-virtual {v7, p0, p1, p2}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2267945
    :goto_4
    return-void

    .line 2267946
    :cond_5
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267947
    iget-object v0, p0, LX/FfJ;->a:LX/FfN;

    iget v2, p0, LX/FfJ;->i:I

    iget-object v1, p0, LX/FfJ;->e:Ljava/util/Map;

    invoke-interface {v1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    new-instance v5, LX/FfI;

    invoke-direct {v5, p0, p2}, LX/FfI;-><init>(LX/FfJ;LX/Cw8;)V

    iget-object v1, p0, LX/FfJ;->k:LX/Cvm;

    .line 2267948
    iget-object v3, v1, LX/Cvm;->f:Ljava/lang/String;

    move-object v6, v3

    .line 2267949
    move-object v1, p1

    move-object v3, p2

    .line 2267950
    if-eqz v1, :cond_b

    const/4 v7, 0x1

    :goto_5
    invoke-static {v7}, LX/0PB;->checkState(Z)V

    .line 2267951
    iget-object p1, v0, LX/FfN;->e:LX/1Ck;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "simple_search_loader_key"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    move-object v7, v0

    move-object v8, v1

    move v9, v2

    move-object v10, v4

    move-object p3, v3

    move-object p0, v6

    .line 2267952
    new-instance v1, LX/9zs;

    invoke-direct {v1}, LX/9zs;-><init>()V

    const-string v2, "query"

    invoke-virtual {v1, v2, v8}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "count"

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "after"

    invoke-virtual {v1, v2, v10}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "type"

    invoke-virtual {p3}, LX/Cw8;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "profile_picture_size"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v7, LX/FfN;->b:Landroid/content/res/Resources;

    const v6, 0x7f0b008d

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "session_id"

    invoke-virtual {v1, v2, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    .line 2267953
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2267954
    iget-object v2, v7, LX/FfN;->a:LX/0tX;

    .line 2267955
    new-instance v3, LX/9zs;

    invoke-direct {v3}, LX/9zs;-><init>()V

    move-object v3, v3

    .line 2267956
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->c:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    invoke-virtual {v3, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 2267957
    move-object v7, v1

    .line 2267958
    new-instance v8, LX/FfL;

    invoke-direct {v8, v0, v5}, LX/FfL;-><init>(LX/FfN;LX/FfI;)V

    invoke-virtual {p1, p2, v7, v8}, LX/1Ck;->b(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2267959
    goto/16 :goto_4

    .line 2267960
    :cond_6
    const-string v3, "see_more_impression"

    invoke-static {v0, v3}, LX/Cvm;->a(LX/Cvm;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "filter_type"

    invoke-virtual {p2}, LX/Cw8;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "query"

    invoke-virtual {v3, v4, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "advanced_filters"

    invoke-virtual {v2}, LX/0Px;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2267961
    iget-object v4, v0, LX/Cvm;->b:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2267962
    invoke-virtual {v3}, Lcom/facebook/analytics/HoneyAnalyticsEvent;->g()Ljava/lang/String;

    goto/16 :goto_1

    .line 2267963
    :cond_7
    const/4 v7, 0x0

    goto/16 :goto_2

    .line 2267964
    :cond_8
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2267965
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v2

    const/4 p3, 0x0

    move v3, p3

    :goto_6
    if-ge v3, v2, :cond_a

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, LX/CwL;

    .line 2267966
    iget-object v4, p3, LX/CwL;->e:Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-object v4, v4

    .line 2267967
    if-eqz v4, :cond_9

    .line 2267968
    invoke-static {p3}, LX/FfN;->a(LX/CwL;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {v1, p3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2267969
    :cond_9
    add-int/lit8 p3, v3, 0x1

    move v3, p3

    goto :goto_6

    .line 2267970
    :cond_a
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object p3

    goto/16 :goto_3

    .line 2267971
    :cond_b
    const/4 v7, 0x0

    goto/16 :goto_5
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2267972
    iget-object v0, p0, LX/FfJ;->a:LX/FfN;

    .line 2267973
    iget-object p0, v0, LX/FfN;->e:LX/1Ck;

    invoke-virtual {p0}, LX/1Ck;->c()V

    .line 2267974
    return-void
.end method

.method public final b(LX/Cw8;)Z
    .locals 1

    .prologue
    .line 2267975
    iget-object v0, p0, LX/FfJ;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c(LX/Cw8;)LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Cw8;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/SeeMoreResultPageUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2267976
    iget-object v0, p0, LX/FfJ;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final d(LX/Cw8;)V
    .locals 4

    .prologue
    .line 2267977
    iget-object v0, p0, LX/FfJ;->a:LX/FfN;

    .line 2267978
    iget-object v1, v0, LX/FfN;->e:LX/1Ck;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "simple_search_loader_key"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2267979
    iget-object v0, p0, LX/FfJ;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267980
    iget-object v0, p0, LX/FfJ;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267981
    iget-object v0, p0, LX/FfJ;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267982
    iget-object v0, p0, LX/FfJ;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2267983
    iget-object v0, p0, LX/FfJ;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2267984
    iget-object v0, p0, LX/FfJ;->h:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2267985
    return-void
.end method
