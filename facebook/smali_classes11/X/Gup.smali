.class public final LX/Gup;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:LX/Gui;


# direct methods
.method public constructor <init>(LX/Gui;)V
    .locals 0

    .prologue
    .line 2403968
    iput-object p1, p0, LX/Gup;->a:LX/Gui;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 2

    .prologue
    .line 2403969
    iget-object v0, p0, LX/Gup;->a:LX/Gui;

    iget-object v0, v0, LX/Gui;->e:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ah:Landroid/view/View;

    if-nez v0, :cond_1

    .line 2403970
    :cond_0
    :goto_0
    return-void

    .line 2403971
    :cond_1
    iget-object v0, p0, LX/Gup;->a:LX/Gui;

    iget-object v0, v0, LX/Gui;->e:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ah:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v1, p0, LX/Gup;->a:LX/Gui;

    iget-object v1, v1, LX/Gui;->e:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v1, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ah:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_0

    .line 2403972
    iget-object v0, p0, LX/Gup;->a:LX/Gui;

    iget-object v0, v0, LX/Gui;->e:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/GxF;->pageDown(Z)Z

    .line 2403973
    iget-object v0, p0, LX/Gup;->a:LX/Gui;

    iget-object v0, v0, LX/Gui;->e:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ah:Landroid/view/View;

    invoke-static {v0, p0}, LX/8He;->b(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method
