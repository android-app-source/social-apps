.class public final enum LX/GbF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GbF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GbF;

.field public static final enum AUTOSAVE:LX/GbF;

.field public static final enum DEFAULT:LX/GbF;

.field public static final enum FREQUENT_LOGIN_LOGOUT:LX/GbF;

.field public static final enum SHARED_DEVICE:LX/GbF;

.field public static final enum TROUBLE_LOGGING_IN:LX/GbF;


# instance fields
.field private final mNuxType:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2369280
    new-instance v0, LX/GbF;

    const-string v1, "DEFAULT"

    const-string v2, "default"

    invoke-direct {v0, v1, v3, v2}, LX/GbF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GbF;->DEFAULT:LX/GbF;

    .line 2369281
    new-instance v0, LX/GbF;

    const-string v1, "AUTOSAVE"

    const-string v2, "autosave"

    invoke-direct {v0, v1, v4, v2}, LX/GbF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GbF;->AUTOSAVE:LX/GbF;

    .line 2369282
    new-instance v0, LX/GbF;

    const-string v1, "FREQUENT_LOGIN_LOGOUT"

    const-string v2, "login_logout"

    invoke-direct {v0, v1, v5, v2}, LX/GbF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GbF;->FREQUENT_LOGIN_LOGOUT:LX/GbF;

    .line 2369283
    new-instance v0, LX/GbF;

    const-string v1, "TROUBLE_LOGGING_IN"

    const-string v2, "trouble_logging_in"

    invoke-direct {v0, v1, v6, v2}, LX/GbF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GbF;->TROUBLE_LOGGING_IN:LX/GbF;

    .line 2369284
    new-instance v0, LX/GbF;

    const-string v1, "SHARED_DEVICE"

    const-string v2, "shared_device"

    invoke-direct {v0, v1, v7, v2}, LX/GbF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GbF;->SHARED_DEVICE:LX/GbF;

    .line 2369285
    const/4 v0, 0x5

    new-array v0, v0, [LX/GbF;

    sget-object v1, LX/GbF;->DEFAULT:LX/GbF;

    aput-object v1, v0, v3

    sget-object v1, LX/GbF;->AUTOSAVE:LX/GbF;

    aput-object v1, v0, v4

    sget-object v1, LX/GbF;->FREQUENT_LOGIN_LOGOUT:LX/GbF;

    aput-object v1, v0, v5

    sget-object v1, LX/GbF;->TROUBLE_LOGGING_IN:LX/GbF;

    aput-object v1, v0, v6

    sget-object v1, LX/GbF;->SHARED_DEVICE:LX/GbF;

    aput-object v1, v0, v7

    sput-object v0, LX/GbF;->$VALUES:[LX/GbF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2369277
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2369278
    iput-object p3, p0, LX/GbF;->mNuxType:Ljava/lang/String;

    .line 2369279
    return-void
.end method

.method public static fromString(Ljava/lang/String;)LX/GbF;
    .locals 5

    .prologue
    .line 2369268
    if-eqz p0, :cond_1

    .line 2369269
    invoke-static {}, LX/GbF;->values()[LX/GbF;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2369270
    invoke-virtual {v0}, LX/GbF;->getTypeString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2369271
    :goto_1
    return-object v0

    .line 2369272
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2369273
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)LX/GbF;
    .locals 1

    .prologue
    .line 2369276
    const-class v0, LX/GbF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GbF;

    return-object v0
.end method

.method public static values()[LX/GbF;
    .locals 1

    .prologue
    .line 2369275
    sget-object v0, LX/GbF;->$VALUES:[LX/GbF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GbF;

    return-object v0
.end method


# virtual methods
.method public final getTypeString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2369274
    iget-object v0, p0, LX/GbF;->mNuxType:Ljava/lang/String;

    return-object v0
.end method
