.class public LX/F5U;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/F5T;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2198838
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2198839
    return-void
.end method


# virtual methods
.method public final a(LX/5pY;)LX/F5T;
    .locals 21

    .prologue
    .line 2198840
    new-instance v1, LX/F5T;

    invoke-static/range {p0 .. p0}, LX/33u;->a(LX/0QB;)LX/33u;

    move-result-object v3

    check-cast v3, LX/33u;

    invoke-static/range {p0 .. p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v4

    check-cast v4, LX/0Sh;

    invoke-static/range {p0 .. p0}, Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;->a(LX/0QB;)Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;

    move-result-object v5

    check-cast v5, Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;

    const-class v2, LX/F5X;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/F5X;

    invoke-static/range {p0 .. p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v7

    check-cast v7, LX/0Uh;

    const-class v2, LX/F6E;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/F6E;

    const-class v2, LX/F6P;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/F6P;

    const-class v2, LX/F6R;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v10

    check-cast v10, LX/F6R;

    const-class v2, LX/F68;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v11

    check-cast v11, LX/F68;

    const-class v2, LX/F63;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v12

    check-cast v12, LX/F63;

    invoke-static/range {p0 .. p0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v13

    check-cast v13, LX/0bH;

    invoke-static/range {p0 .. p0}, LX/1nF;->a(LX/0QB;)LX/1nF;

    move-result-object v14

    check-cast v14, LX/1nF;

    invoke-static/range {p0 .. p0}, LX/DKO;->a(LX/0QB;)LX/DKO;

    move-result-object v15

    check-cast v15, LX/DKO;

    invoke-static/range {p0 .. p0}, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;->a(LX/0QB;)Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    move-result-object v16

    check-cast v16, Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v17

    check-cast v17, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, LX/F2N;->a(LX/0QB;)LX/F2N;

    move-result-object v18

    check-cast v18, LX/F2N;

    invoke-static/range {p0 .. p0}, LX/DYZ;->a(LX/0QB;)LX/DYZ;

    move-result-object v19

    check-cast v19, LX/DYZ;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v20

    check-cast v20, LX/0ad;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v20}, LX/F5T;-><init>(LX/5pY;LX/33u;LX/0Sh;Lcom/facebook/groups/fb4a/react/GroupPostSearchLauncher;LX/F5X;LX/0Uh;LX/F6E;LX/F6P;LX/F6R;LX/F68;LX/F63;LX/0bH;LX/1nF;LX/DKO;Lcom/facebook/api/feedcache/mutator/FeedUnitCacheMutator;Lcom/facebook/content/SecureContextHelper;LX/F2N;LX/DYZ;LX/0ad;)V

    .line 2198841
    return-object v1
.end method
