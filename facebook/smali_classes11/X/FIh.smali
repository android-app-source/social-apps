.class public LX/FIh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/FIh;


# instance fields
.field private final a:LX/2Ul;

.field private final b:LX/2Uh;

.field public final c:LX/1qI;

.field public final d:LX/FIK;

.field private final e:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private f:Ljava/net/DatagramSocket;


# direct methods
.method public constructor <init>(LX/2Ul;LX/2Uh;LX/1qI;LX/FIK;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2222515
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2222516
    iput-object p1, p0, LX/FIh;->a:LX/2Ul;

    .line 2222517
    iput-object p2, p0, LX/FIh;->b:LX/2Uh;

    .line 2222518
    iput-object p3, p0, LX/FIh;->c:LX/1qI;

    .line 2222519
    iput-object p4, p0, LX/FIh;->d:LX/FIK;

    .line 2222520
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/FIh;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2222521
    return-void
.end method

.method public static a(LX/0QB;)LX/FIh;
    .locals 7

    .prologue
    .line 2222522
    sget-object v0, LX/FIh;->g:LX/FIh;

    if-nez v0, :cond_1

    .line 2222523
    const-class v1, LX/FIh;

    monitor-enter v1

    .line 2222524
    :try_start_0
    sget-object v0, LX/FIh;->g:LX/FIh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2222525
    if-eqz v2, :cond_0

    .line 2222526
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2222527
    new-instance p0, LX/FIh;

    invoke-static {v0}, LX/2Ul;->a(LX/0QB;)LX/2Ul;

    move-result-object v3

    check-cast v3, LX/2Ul;

    invoke-static {v0}, LX/2Uh;->a(LX/0QB;)LX/2Uh;

    move-result-object v4

    check-cast v4, LX/2Uh;

    invoke-static {v0}, LX/1qI;->a(LX/0QB;)LX/1qI;

    move-result-object v5

    check-cast v5, LX/1qI;

    invoke-static {v0}, LX/FIK;->a(LX/0QB;)LX/FIK;

    move-result-object v6

    check-cast v6, LX/FIK;

    invoke-direct {p0, v3, v4, v5, v6}, LX/FIh;-><init>(LX/2Ul;LX/2Uh;LX/1qI;LX/FIK;)V

    .line 2222528
    move-object v0, p0

    .line 2222529
    sput-object v0, LX/FIh;->g:LX/FIh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2222530
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2222531
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2222532
    :cond_1
    sget-object v0, LX/FIh;->g:LX/FIh;

    return-object v0

    .line 2222533
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2222534
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/FIh;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 2222535
    iget-object v0, p0, LX/FIh;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2222536
    new-instance v0, Ljava/net/DatagramSocket;

    invoke-direct {v0}, Ljava/net/DatagramSocket;-><init>()V

    iput-object v0, p0, LX/FIh;->f:Ljava/net/DatagramSocket;

    .line 2222537
    iget-object v0, p0, LX/FIh;->f:Ljava/net/DatagramSocket;

    const/16 v1, 0x4e20

    invoke-virtual {v0, v1}, Ljava/net/DatagramSocket;->setSoTimeout(I)V

    .line 2222538
    iget-object v0, p0, LX/FIh;->a:LX/2Ul;

    iget-object v1, p0, LX/FIh;->f:Ljava/net/DatagramSocket;

    .line 2222539
    iput-object v1, v0, LX/2Ul;->c:Ljava/net/DatagramSocket;

    .line 2222540
    iget-object v0, p0, LX/FIh;->b:LX/2Uh;

    iget-object v1, p0, LX/FIh;->f:Ljava/net/DatagramSocket;

    .line 2222541
    iput-object v1, v0, LX/2Uh;->f:Ljava/net/DatagramSocket;

    .line 2222542
    :cond_0
    iget-object v0, p0, LX/FIh;->a:LX/2Ul;

    .line 2222543
    new-instance p0, Ljava/net/InetSocketAddress;

    invoke-direct {p0, p1, p2}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    .line 2222544
    iget-object v1, v0, LX/2Ul;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, v0, LX/2Ul;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/net/SocketAddress;

    invoke-virtual {v1, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2222545
    :cond_1
    iget-object v1, v0, LX/2Ul;->d:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1, p0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 2222546
    :cond_2
    return-void
.end method
