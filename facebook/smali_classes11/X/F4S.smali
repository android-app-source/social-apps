.class public final LX/F4S;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;)V
    .locals 0

    .prologue
    .line 2196937
    iput-object p1, p0, LX/F4S;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel;)V
    .locals 9
    .param p1    # Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2196938
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel;->a()Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel;->a()Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel;->a()Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2196939
    :cond_0
    :goto_0
    return-void

    .line 2196940
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel;->a()Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel;->a()Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel;->a()LX/0Px;

    move-result-object v4

    .line 2196941
    iget-object v0, p0, LX/F4S;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2196942
    iget-object v0, p0, LX/F4S;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->c:LX/F53;

    .line 2196943
    iget-object v1, v0, LX/F53;->l:Ljava/lang/String;

    move-object v5, v1

    .line 2196944
    invoke-static {v5}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 2196945
    :goto_1
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    :goto_2
    if-ge v3, v6, :cond_4

    invoke-virtual {v4, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;

    .line 2196946
    if-nez v1, :cond_3

    .line 2196947
    iget-object v7, p0, LX/F4S;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;

    .line 2196948
    invoke-static {v7, v0, v2}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->a$redex0(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;Z)V

    .line 2196949
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_2
    move v1, v2

    .line 2196950
    goto :goto_1

    .line 2196951
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    .line 2196952
    iget-object v8, p0, LX/F4S;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;

    .line 2196953
    invoke-static {v8, v0, v7}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->a$redex0(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel$ActorModel$CommunitiesModel$NodesModel;Z)V

    .line 2196954
    goto :goto_3

    .line 2196955
    :cond_4
    iget-object v0, p0, LX/F4S;->a:Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;

    .line 2196956
    invoke-static {v0}, Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;->b$redex0(Lcom/facebook/groups/fb4a/create/FB4AGroupsCreateCommunityPickerFragment;)V

    .line 2196957
    goto :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2196958
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2196959
    check-cast p1, Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel;

    invoke-direct {p0, p1}, LX/F4S;->a(Lcom/facebook/groups/fb4a/create/protocol/FetchViewerCommunitiesGraphQLModels$FetchViewerCommunitiesQueryModel;)V

    return-void
.end method
