.class public final LX/FIx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/FIy;

.field private final b:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "LX/FIs;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/graphics/Paint;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/FIy;)V
    .locals 3

    .prologue
    .line 2222800
    iput-object p1, p0, LX/FIx;->a:LX/FIy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2222801
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 2222802
    iget-object v0, p1, LX/FIy;->d:LX/FIs;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 2222803
    iget-object v0, p1, LX/FIy;->e:LX/FIs;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 2222804
    iget-object v0, p1, LX/FIy;->a:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextBoolean()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, LX/FIy;->d:LX/FIs;

    :goto_0
    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 2222805
    invoke-static {v1}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 2222806
    new-instance v0, Ljava/util/LinkedList;

    iget-object v2, p1, LX/FIy;->f:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    .line 2222807
    invoke-static {v0}, Ljava/util/Collections;->shuffle(Ljava/util/List;)V

    .line 2222808
    iput-object v1, p0, LX/FIx;->b:Ljava/util/Queue;

    .line 2222809
    iput-object v0, p0, LX/FIx;->c:Ljava/util/Queue;

    .line 2222810
    return-void

    .line 2222811
    :cond_0
    iget-object v0, p1, LX/FIy;->e:LX/FIs;

    goto :goto_0
.end method


# virtual methods
.method public final a(FF)LX/FIp;
    .locals 3

    .prologue
    .line 2222812
    invoke-static {}, LX/FIq;->a()LX/FIp;

    move-result-object v0

    iget-object v1, p0, LX/FIx;->a:LX/FIy;

    invoke-static {v1, p1}, LX/FIy;->a$redex0(LX/FIy;F)F

    move-result v1

    iget-object v2, p0, LX/FIx;->a:LX/FIy;

    invoke-static {v2, p2}, LX/FIy;->a$redex0(LX/FIy;F)F

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/FIp;->a(FF)LX/FIp;

    move-result-object v1

    iget-object v0, p0, LX/FIx;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIs;

    .line 2222813
    iput-object v0, v1, LX/FIp;->f:LX/FIs;

    .line 2222814
    move-object v1, v1

    .line 2222815
    iget-object v0, p0, LX/FIx;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Paint;

    .line 2222816
    iput-object v0, v1, LX/FIp;->g:Landroid/graphics/Paint;

    .line 2222817
    move-object v0, v1

    .line 2222818
    iget-object v1, p0, LX/FIx;->a:LX/FIy;

    iget-object v1, v1, LX/FIy;->a:Ljava/util/Random;

    const/16 v2, 0x168

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    int-to-float v1, v1

    .line 2222819
    iput v1, v0, LX/FIp;->d:F

    .line 2222820
    move-object v0, v0

    .line 2222821
    return-object v0
.end method

.method public final b(FF)LX/FIp;
    .locals 3

    .prologue
    .line 2222822
    invoke-static {}, LX/FIq;->a()LX/FIp;

    move-result-object v0

    iget-object v1, p0, LX/FIx;->a:LX/FIy;

    invoke-static {v1, p1}, LX/FIy;->a$redex0(LX/FIy;F)F

    move-result v1

    iget-object v2, p0, LX/FIx;->a:LX/FIy;

    invoke-static {v2, p2}, LX/FIy;->a$redex0(LX/FIy;F)F

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/FIp;->a(FF)LX/FIp;

    move-result-object v0

    iget-object v1, p0, LX/FIx;->a:LX/FIy;

    iget-object v1, v1, LX/FIy;->c:LX/FIs;

    .line 2222823
    iput-object v1, v0, LX/FIp;->f:LX/FIs;

    .line 2222824
    move-object v0, v0

    .line 2222825
    return-object v0
.end method
