.class public LX/EzP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/EzP;


# instance fields
.field private final a:LX/EnT;

.field private final b:LX/EnR;


# direct methods
.method public constructor <init>(LX/EnT;LX/EnR;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2187144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2187145
    iput-object p1, p0, LX/EzP;->a:LX/EnT;

    .line 2187146
    iput-object p2, p0, LX/EzP;->b:LX/EnR;

    .line 2187147
    return-void
.end method

.method public static a(LX/0QB;)LX/EzP;
    .locals 5

    .prologue
    .line 2187048
    sget-object v0, LX/EzP;->c:LX/EzP;

    if-nez v0, :cond_1

    .line 2187049
    const-class v1, LX/EzP;

    monitor-enter v1

    .line 2187050
    :try_start_0
    sget-object v0, LX/EzP;->c:LX/EzP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2187051
    if-eqz v2, :cond_0

    .line 2187052
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2187053
    new-instance p0, LX/EzP;

    invoke-static {v0}, LX/EnT;->a(LX/0QB;)LX/EnT;

    move-result-object v3

    check-cast v3, LX/EnT;

    invoke-static {v0}, LX/EnR;->a(LX/0QB;)LX/EnR;

    move-result-object v4

    check-cast v4, LX/EnR;

    invoke-direct {p0, v3, v4}, LX/EzP;-><init>(LX/EnT;LX/EnR;)V

    .line 2187054
    move-object v0, p0

    .line 2187055
    sput-object v0, LX/EzP;->c:LX/EzP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2187056
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2187057
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2187058
    :cond_1
    sget-object v0, LX/EzP;->c:LX/EzP;

    return-object v0

    .line 2187059
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2187060
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Lcom/facebook/friends/model/FriendRequest;)Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;
    .locals 9

    .prologue
    .line 2187104
    invoke-virtual {p0}, Lcom/facebook/friends/model/FriendRequest;->k()Z

    move-result v0

    .line 2187105
    sget-object v1, LX/EzO;->a:[I

    .line 2187106
    iget-object v2, p0, Lcom/facebook/friends/model/FriendRequest;->j:LX/2lu;

    move-object v2, v2

    .line 2187107
    invoke-virtual {v2}, LX/2lu;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2187108
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected friend request state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2187109
    iget-object v2, p0, Lcom/facebook/friends/model/FriendRequest;->j:LX/2lu;

    move-object v2, v2

    .line 2187110
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2187111
    :pswitch_0
    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2187112
    :goto_0
    new-instance v1, LX/Eov;

    invoke-direct {v1}, LX/Eov;-><init>()V

    .line 2187113
    iget-object v2, p0, Lcom/facebook/friends/model/FriendRequest;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2187114
    iput-object v2, v1, LX/Eov;->k:Ljava/lang/String;

    .line 2187115
    move-object v1, v1

    .line 2187116
    iput-object v0, v1, LX/Eov;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2187117
    move-object v0, v1

    .line 2187118
    invoke-virtual {p0}, Lcom/facebook/friends/model/FriendRequest;->b()Ljava/lang/String;

    move-result-object v1

    .line 2187119
    iput-object v1, v0, LX/Eov;->p:Ljava/lang/String;

    .line 2187120
    move-object v0, v0

    .line 2187121
    new-instance v1, LX/Eox;

    invoke-direct {v1}, LX/Eox;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/friends/model/FriendRequest;->d()Ljava/lang/String;

    move-result-object v2

    .line 2187122
    iput-object v2, v1, LX/Eox;->b:Ljava/lang/String;

    .line 2187123
    move-object v1, v1

    .line 2187124
    const/4 v7, 0x1

    const/4 v5, 0x0

    const/4 v8, 0x0

    .line 2187125
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2187126
    iget-object v4, v1, LX/Eox;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    .line 2187127
    const/4 v6, 0x3

    invoke-virtual {v3, v6}, LX/186;->c(I)V

    .line 2187128
    iget v6, v1, LX/Eox;->a:I

    invoke-virtual {v3, v8, v6, v8}, LX/186;->a(III)V

    .line 2187129
    invoke-virtual {v3, v7, v4}, LX/186;->b(II)V

    .line 2187130
    const/4 v4, 0x2

    iget v6, v1, LX/Eox;->c:I

    invoke-virtual {v3, v4, v6, v8}, LX/186;->a(III)V

    .line 2187131
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 2187132
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 2187133
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2187134
    invoke-virtual {v4, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2187135
    new-instance v3, LX/15i;

    move-object v6, v5

    move-object v8, v5

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2187136
    new-instance v4, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;

    invoke-direct {v4, v3}, Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;-><init>(LX/15i;)V

    .line 2187137
    move-object v1, v4

    .line 2187138
    iput-object v1, v0, LX/Eov;->s:Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel$ProfilePictureModel;

    .line 2187139
    move-object v0, v0

    .line 2187140
    invoke-virtual {v0}, LX/Eov;->a()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    move-result-object v0

    return-object v0

    .line 2187141
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_0

    .line 2187142
    :pswitch_1
    if-eqz v0, :cond_1

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_0

    .line 2187143
    :pswitch_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Lcom/facebook/friends/model/PersonYouMayKnow;)Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;
    .locals 4

    .prologue
    .line 2187088
    new-instance v0, LX/Eov;

    invoke-direct {v0}, LX/Eov;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/friends/model/PersonYouMayKnow;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    .line 2187089
    iput-object v1, v0, LX/Eov;->j:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2187090
    move-object v0, v0

    .line 2187091
    invoke-virtual {p0}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 2187092
    iput-object v1, v0, LX/Eov;->k:Ljava/lang/String;

    .line 2187093
    move-object v0, v0

    .line 2187094
    invoke-virtual {p0}, Lcom/facebook/friends/model/PersonYouMayKnow;->b()Ljava/lang/String;

    move-result-object v1

    .line 2187095
    iput-object v1, v0, LX/Eov;->p:Ljava/lang/String;

    .line 2187096
    move-object v0, v0

    .line 2187097
    new-instance v1, LX/4aM;

    invoke-direct {v1}, LX/4aM;-><init>()V

    invoke-virtual {p0}, Lcom/facebook/friends/model/PersonYouMayKnow;->d()Ljava/lang/String;

    move-result-object v2

    .line 2187098
    iput-object v2, v1, LX/4aM;->b:Ljava/lang/String;

    .line 2187099
    move-object v1, v1

    .line 2187100
    invoke-virtual {v1}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v1

    .line 2187101
    iput-object v1, v0, LX/Eov;->r:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2187102
    move-object v0, v0

    .line 2187103
    invoke-virtual {v0}, LX/Eov;->a()Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/EzP;Landroid/app/Activity;LX/0Px;Ljava/lang/String;Ljava/lang/String;LX/0am;Ljava/util/List;LX/2h7;LX/5P2;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 10
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/2h7;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;",
            ">;",
            "LX/2h7;",
            "LX/5P2;",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2187080
    invoke-virtual {p5}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object/from16 v0, p7

    invoke-static {p4, v1, v0}, LX/EzT;->a(Ljava/lang/String;Ljava/lang/String;LX/2h7;)Landroid/os/Bundle;

    move-result-object v8

    .line 2187081
    const-string v1, "preliminary_entities"

    invoke-static/range {p6 .. p6}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v8, v1, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    .line 2187082
    const-string v1, "extra_friending_location_name"

    invoke-virtual/range {p7 .. p7}, LX/2h7;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2187083
    const-string v2, "extra_friend_request_make_ref"

    if-eqz p8, :cond_0

    invoke-virtual/range {p8 .. p8}, LX/5P2;->name()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v8, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2187084
    iget-object v1, p0, LX/EzP;->a:LX/EnT;

    const-string v3, "friending"

    invoke-static {p4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v4, 0x0

    :goto_1
    move-object v2, p1

    move-object v5, p2

    move-object v6, p3

    move-object/from16 v7, p9

    move-object/from16 v9, p10

    invoke-virtual/range {v1 .. v9}, LX/EnT;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/Integer;)V

    .line 2187085
    return-void

    .line 2187086
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object v4, p4

    .line 2187087
    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/app/Activity;LX/2iL;Ljava/lang/String;LX/0am;LX/2h7;Ljava/lang/String;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "LX/2iL;",
            "Ljava/lang/String;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/2h7;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2187071
    invoke-interface {p2}, LX/2iL;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0, p3}, LX/EnR;->a(LX/0Px;Ljava/lang/String;)LX/EnQ;

    move-result-object v1

    .line 2187072
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 2187073
    iget v0, v1, LX/EnQ;->d:I

    :goto_0
    iget v2, v1, LX/EnQ;->e:I

    if-gt v0, v2, :cond_0

    .line 2187074
    invoke-interface {p2, v0}, LX/2iL;->a(I)Lcom/facebook/friends/model/FriendRequest;

    move-result-object v2

    .line 2187075
    invoke-static {v2}, LX/EzP;->a(Lcom/facebook/friends/model/FriendRequest;)Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    move-result-object v2

    .line 2187076
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2187077
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2187078
    :cond_0
    iget-object v2, v1, LX/EnQ;->a:LX/0Px;

    const-string v4, "friend_requests"

    sget-object v8, LX/5P2;->JEWEL:LX/5P2;

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v5, p4

    move-object/from16 v7, p5

    move-object/from16 v9, p6

    invoke-static/range {v0 .. v10}, LX/EzP;->a(LX/EzP;Landroid/app/Activity;LX/0Px;Ljava/lang/String;Ljava/lang/String;LX/0am;Ljava/util/List;LX/2h7;LX/5P2;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2187079
    return-void
.end method

.method public final a(Landroid/app/Activity;LX/2iL;Ljava/lang/String;LX/0am;LX/2h7;Ljava/lang/String;I)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "LX/2iL;",
            "Ljava/lang/String;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/2h7;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 2187062
    invoke-interface {p2}, LX/2iL;->b()LX/0Px;

    move-result-object v0

    invoke-static {v0, p3}, LX/EnR;->a(LX/0Px;Ljava/lang/String;)LX/EnQ;

    move-result-object v1

    .line 2187063
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v6

    .line 2187064
    iget v0, v1, LX/EnQ;->d:I

    :goto_0
    iget v2, v1, LX/EnQ;->e:I

    if-gt v0, v2, :cond_0

    .line 2187065
    invoke-interface {p2, v0}, LX/2iL;->b(I)Lcom/facebook/friends/model/PersonYouMayKnow;

    move-result-object v2

    .line 2187066
    invoke-static {v2}, LX/EzP;->a(Lcom/facebook/friends/model/PersonYouMayKnow;)Lcom/facebook/entitycardsplugins/person/protocol/PersonCardGraphQLModels$PersonCardModel;

    move-result-object v2

    .line 2187067
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2187068
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2187069
    :cond_0
    iget-object v2, v1, LX/EnQ;->a:LX/0Px;

    const-string v4, "pymk"

    sget-object v8, LX/5P2;->PYMK_JEWEL:LX/5P2;

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v5, p4

    move-object/from16 v7, p5

    move-object/from16 v9, p6

    invoke-static/range {v0 .. v10}, LX/EzP;->a(LX/EzP;Landroid/app/Activity;LX/0Px;Ljava/lang/String;Ljava/lang/String;LX/0am;Ljava/util/List;LX/2h7;LX/5P2;Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2187070
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 2187061
    iget-object v0, p0, LX/EzP;->a:LX/EnT;

    const-string v1, "friending"

    invoke-virtual {v0, v1}, LX/EnT;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
