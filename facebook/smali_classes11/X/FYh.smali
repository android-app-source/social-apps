.class public LX/FYh;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/16H;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hy;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/19v;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FYx;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FYy;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/16H;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/16H;",
            "LX/0Ot",
            "<",
            "LX/0hy;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/video/downloadmanager/db/OfflineVideoCache;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/19v;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FYx;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FYy;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2256425
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2256426
    iput-object p1, p0, LX/FYh;->a:LX/16H;

    .line 2256427
    iput-object p2, p0, LX/FYh;->b:LX/0Ot;

    .line 2256428
    iput-object p3, p0, LX/FYh;->c:LX/0Ot;

    .line 2256429
    iput-object p4, p0, LX/FYh;->d:LX/0Ot;

    .line 2256430
    iput-object p5, p0, LX/FYh;->e:LX/0Ot;

    .line 2256431
    iput-object p6, p0, LX/FYh;->f:LX/0Ot;

    .line 2256432
    iput-object p7, p0, LX/FYh;->g:LX/0Ot;

    .line 2256433
    iput-object p8, p0, LX/FYh;->h:LX/0Ot;

    .line 2256434
    return-void
.end method

.method public static a(LX/FYh;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 2256421
    iget-object v0, p0, LX/FYh;->a:LX/16H;

    const/4 v11, 0x0

    .line 2256422
    iget-object v9, v0, LX/16H;->b:LX/0gh;

    const-string v10, "saved_dashboard"

    const-string v1, "action_name"

    const-string v2, "saved_dashboard_item_clicked"

    const-string v3, "object_id"

    invoke-static {p1}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "section_type"

    const-string v7, "event_id"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v8

    move-object v6, p2

    invoke-static/range {v1 .. v8}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v1

    invoke-virtual {v9, v10, v11, v11, v1}, LX/0gh;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 2256423
    iget-object v1, v0, LX/16H;->b:LX/0gh;

    const-string v2, "tap_saved_dashboard_item"

    invoke-virtual {v1, v2}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2256424
    return-void
.end method
