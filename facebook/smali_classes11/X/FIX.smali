.class public LX/FIX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# instance fields
.field public final a:LX/18V;

.field public final b:LX/FIg;

.field public final c:LX/FIW;

.field public final d:LX/14U;


# direct methods
.method public constructor <init>(LX/18V;LX/FIg;LX/FIW;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2222258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2222259
    iput-object p1, p0, LX/FIX;->a:LX/18V;

    .line 2222260
    iput-object p2, p0, LX/FIX;->b:LX/FIg;

    .line 2222261
    iput-object p3, p0, LX/FIX;->c:LX/FIW;

    .line 2222262
    new-instance v0, LX/14U;

    invoke-direct {v0}, LX/14U;-><init>()V

    iput-object v0, p0, LX/FIX;->d:LX/14U;

    .line 2222263
    return-void
.end method

.method public static b(LX/0QB;)LX/FIX;
    .locals 4

    .prologue
    .line 2222232
    new-instance v3, LX/FIX;

    invoke-static {p0}, LX/18V;->a(LX/0QB;)LX/18V;

    move-result-object v0

    check-cast v0, LX/18V;

    .line 2222233
    new-instance v1, LX/FIg;

    invoke-direct {v1}, LX/FIg;-><init>()V

    .line 2222234
    move-object v1, v1

    .line 2222235
    move-object v1, v1

    .line 2222236
    check-cast v1, LX/FIg;

    invoke-static {p0}, LX/FIW;->a(LX/0QB;)LX/FIW;

    move-result-object v2

    check-cast v2, LX/FIW;

    invoke-direct {v3, v0, v1, v2}, LX/FIX;-><init>(LX/18V;LX/FIg;LX/FIW;)V

    .line 2222237
    return-object v3
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2222238
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2222239
    const-string v1, "udp_connection_refresh_server_config"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2222240
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2222241
    const-string v1, "udp_parcel_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/upload/udp/UDPConnectionMethod$UDPConnectionParams;

    .line 2222242
    :try_start_0
    iget-object v1, p0, LX/FIX;->a:LX/18V;

    iget-object v2, p0, LX/FIX;->c:LX/FIW;

    iget-object v3, p0, LX/FIX;->d:LX/14U;

    invoke-virtual {v1, v2, v0, v3}, LX/18V;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;

    .line 2222243
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2222244
    :goto_0
    move-object v0, v0

    .line 2222245
    :goto_1
    return-object v0

    .line 2222246
    :cond_0
    const-string v1, "udp_connection_upload_metadata"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2222247
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2222248
    const-string v1, "udp_parcel_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/upload/udp/UDPMetadataUploadMethod$UDPUploadParams;

    .line 2222249
    :try_start_1
    iget-object v1, p0, LX/FIX;->a:LX/18V;

    iget-object v2, p0, LX/FIX;->b:LX/FIg;

    iget-object v3, p0, LX/FIX;->d:LX/14U;

    invoke-virtual {v1, v2, v0, v3}, LX/18V;->a(LX/0e6;Ljava/lang/Object;LX/14U;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/media/upload/udp/UDPServerConfig;

    .line 2222250
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 2222251
    :goto_2
    move-object v0, v0

    .line 2222252
    goto :goto_1

    .line 2222253
    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2222254
    :catch_0
    move-exception v0

    .line 2222255
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2222256
    :catch_1
    move-exception v0

    .line 2222257
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_2
.end method
