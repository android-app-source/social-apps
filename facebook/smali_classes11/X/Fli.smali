.class public LX/Fli;
.super LX/2s5;
.source ""


# instance fields
.field private final a:Landroid/content/Context;

.field public final b:[Lcom/facebook/base/fragment/FbFragment;

.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/Flu;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0gc;Landroid/content/Context;Ljava/util/List;Ljava/lang/String;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gc;",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "LX/Flu;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2281355
    invoke-direct {p0, p1}, LX/2s5;-><init>(LX/0gc;)V

    .line 2281356
    iput-object p2, p0, LX/Fli;->a:Landroid/content/Context;

    .line 2281357
    iput-object p3, p0, LX/Fli;->c:Ljava/util/List;

    .line 2281358
    iput-object p4, p0, LX/Fli;->e:Ljava/lang/String;

    .line 2281359
    iget-object v0, p0, LX/Fli;->c:Ljava/util/List;

    .line 2281360
    new-instance p2, LX/0Pz;

    invoke-direct {p2}, LX/0Pz;-><init>()V

    .line 2281361
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/Flu;

    .line 2281362
    sget-object p4, LX/Flh;->a:[I

    invoke-virtual {p1}, LX/Flu;->ordinal()I

    move-result p1

    aget p1, p4, p1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 2281363
    :pswitch_0
    const p1, 0x7f08333d

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p2, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2281364
    :pswitch_1
    const p1, 0x7f08333c

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p2, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2281365
    :pswitch_2
    const p1, 0x7f08333e

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p2, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2281366
    :pswitch_3
    const p1, 0x7f08333f

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p2, p1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2281367
    :cond_0
    invoke-virtual {p2}, LX/0Pz;->b()LX/0Px;

    move-result-object p1

    move-object v0, p1

    .line 2281368
    iput-object v0, p0, LX/Fli;->d:LX/0Px;

    .line 2281369
    iget-object v0, p0, LX/Fli;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/facebook/base/fragment/FbFragment;

    iput-object v0, p0, LX/Fli;->b:[Lcom/facebook/base/fragment/FbFragment;

    .line 2281370
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 2281371
    iget-object v0, p0, LX/Fli;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, LX/Fli;->d:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 2281372
    iget-object v0, p0, LX/Fli;->b:[Lcom/facebook/base/fragment/FbFragment;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2281373
    iget-object v0, p0, LX/Fli;->b:[Lcom/facebook/base/fragment/FbFragment;

    array-length v0, v0

    return v0
.end method

.method public final f()LX/0Rf;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2281374
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 2281375
    iget-object v3, p0, LX/Fli;->b:[Lcom/facebook/base/fragment/FbFragment;

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v0, v3, v1

    .line 2281376
    if-eqz v0, :cond_0

    instance-of v5, v0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;

    if-eqz v5, :cond_0

    .line 2281377
    check-cast v0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;

    .line 2281378
    iget-object v5, v0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFragment;->i:Ljava/util/Set;

    invoke-static {v5}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v5

    move-object v0, v5

    .line 2281379
    invoke-interface {v2, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 2281380
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2281381
    :cond_1
    invoke-static {v2}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
