.class public LX/FKU;
.super LX/4Zv;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4Zv",
        "<",
        "Lcom/facebook/messaging/service/model/ModifyThreadParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/FFR;


# direct methods
.method public constructor <init>(LX/0sO;LX/FFR;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2225121
    invoke-direct {p0, p1}, LX/4Zv;-><init>(LX/0sO;)V

    .line 2225122
    iput-object p2, p0, LX/FKU;->b:LX/FFR;

    .line 2225123
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2225124
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2225125
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2225126
    const/4 v0, 0x0

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 5

    .prologue
    .line 2225127
    check-cast p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;

    .line 2225128
    iget-object v0, p0, LX/FKU;->b:LX/FFR;

    .line 2225129
    iget-object v1, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2225130
    iget-object v2, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->s:Ljava/lang/String;

    move-object v2, v2

    .line 2225131
    new-instance v4, LX/4HD;

    invoke-direct {v4}, LX/4HD;-><init>()V

    .line 2225132
    new-instance p0, LX/4Jm;

    invoke-direct {p0}, LX/4Jm;-><init>()V

    .line 2225133
    const-string v3, "description"

    invoke-virtual {p0, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2225134
    const-string v3, "thread_fbid"

    invoke-virtual {v4, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2225135
    move-object p1, v4

    .line 2225136
    iget-object v3, v0, LX/FFR;->a:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2225137
    const-string v0, "actor_id"

    invoke-virtual {p1, v0, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2225138
    move-object v3, p1

    .line 2225139
    const-string p1, "thread_settings"

    invoke-virtual {v3, p1, p0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2225140
    move-object v0, v4

    .line 2225141
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2225142
    const-string v2, "client_mutation_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2225143
    move-object v0, v0

    .line 2225144
    new-instance v1, LX/5d6;

    invoke-direct {v1}, LX/5d6;-><init>()V

    move-object v1, v1

    .line 2225145
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    return-object v0
.end method
