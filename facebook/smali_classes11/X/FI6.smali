.class public final LX/FI6;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:Ljava/lang/String;

.field public final d:LX/FI7;

.field public final e:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/FI7;Ljava/io/File;JLjava/lang/String;)V
    .locals 2

    .prologue
    .line 2221456
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2221457
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Stream Id is empty"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2221458
    const-string v0, "Object Fileis null"

    invoke-static {p3, v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221459
    iput-object p1, p0, LX/FI6;->a:Ljava/lang/String;

    .line 2221460
    iput-object p2, p0, LX/FI6;->d:LX/FI7;

    .line 2221461
    iput-object p3, p0, LX/FI6;->e:Ljava/io/File;

    .line 2221462
    iput-wide p4, p0, LX/FI6;->b:J

    .line 2221463
    iput-object p6, p0, LX/FI6;->c:Ljava/lang/String;

    .line 2221464
    return-void

    .line 2221465
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
