.class public LX/H83;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/H72;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:LX/H70;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/2pa;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/H72;LX/H70;Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;)V
    .locals 0
    .param p1    # LX/H72;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # LX/H70;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2432747
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2432748
    iput-object p1, p0, LX/H83;->a:LX/H72;

    .line 2432749
    iput-object p2, p0, LX/H83;->b:LX/H70;

    .line 2432750
    iput-object p3, p0, LX/H83;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    .line 2432751
    return-void
.end method

.method private S()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2432752
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2432753
    invoke-virtual {p0}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 2432754
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/H83;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->b()Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLCouponClaimLocation;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private declared-synchronized T()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/2pa;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2432755
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/H83;->d:Ljava/util/List;

    if-nez v0, :cond_0

    .line 2432756
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/H83;->d:Ljava/util/List;

    .line 2432757
    :cond_0
    iget-object v0, p0, LX/H83;->d:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 2432758
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static a(LX/H70;)LX/H83;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "fromOfferClaim"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2432759
    new-instance v0, LX/H83;

    invoke-direct {v0, v1, p0, v1}, LX/H83;-><init>(LX/H72;LX/H70;Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;)V

    return-object v0
.end method

.method public static c(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;)LX/H83;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2432760
    new-instance v0, LX/H83;

    invoke-static {p0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;->a(LX/H72;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferViewDataModel;

    move-result-object v1

    invoke-direct {v0, v1, v2, v2}, LX/H83;-><init>(LX/H72;LX/H70;Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;)V

    return-object v0
.end method


# virtual methods
.method public final A()D
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2432761
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->r()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2432762
    invoke-virtual {p0}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->r()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    .line 2432763
    invoke-virtual {p0}, LX/H83;->N()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2432764
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->mj_()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel$EdgesModel;->a()LX/H74;

    move-result-object v0

    invoke-interface {v0}, LX/H74;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v3}, LX/15i;->l(II)D

    move-result-wide v0

    .line 2432765
    :goto_0
    return-wide v0

    .line 2432766
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->j()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_1

    .line 2432767
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v3}, LX/15i;->l(II)D

    move-result-wide v0

    goto :goto_0

    .line 2432768
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final B()D
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2432769
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->r()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2432770
    invoke-virtual {p0}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->r()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    .line 2432771
    invoke-virtual {p0}, LX/H83;->N()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2432772
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->mj_()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel$EdgesModel;->a()LX/H74;

    move-result-object v0

    invoke-interface {v0}, LX/H74;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v3}, LX/15i;->l(II)D

    move-result-wide v0

    .line 2432773
    :goto_0
    return-wide v0

    .line 2432774
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->j()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_1

    .line 2432775
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v3}, LX/15i;->l(II)D

    move-result-wide v0

    goto :goto_0

    .line 2432776
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final C()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 2432777
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->r()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2432778
    invoke-virtual {p0}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->r()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    .line 2432779
    invoke-virtual {p0}, LX/H83;->N()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2432780
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->mj_()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel$EdgesModel;->a()LX/H74;

    move-result-object v0

    invoke-interface {v0}, LX/H74;->mi_()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2432781
    :goto_0
    return-object v0

    .line 2432782
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->mi_()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2432783
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final E()LX/1vs;
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAppData"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2432784
    invoke-virtual {p0}, LX/H83;->w()LX/H72;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2432785
    invoke-virtual {p0}, LX/H83;->w()LX/H72;

    move-result-object v0

    invoke-interface {v0}, LX/H72;->F()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    .line 2432786
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    goto :goto_0
.end method

.method public final F()Z
    .locals 1

    .prologue
    .line 2432787
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2432788
    invoke-virtual {p0}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->n()Z

    move-result v0

    .line 2432789
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final G()Z
    .locals 2

    .prologue
    .line 2432744
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2432745
    const-string v0, "offline"

    invoke-direct {p0}, LX/H83;->S()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2432746
    :goto_0
    return v0

    :cond_0
    const-string v0, "instore_only"

    invoke-direct {p0}, LX/H83;->S()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final H()Z
    .locals 2

    .prologue
    .line 2432790
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2432791
    const-string v0, "online"

    invoke-direct {p0}, LX/H83;->S()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2432792
    :goto_0
    return v0

    :cond_0
    const-string v0, "instore_only"

    invoke-direct {p0}, LX/H83;->S()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final I()Z
    .locals 2

    .prologue
    .line 2432793
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2432794
    const-string v0, "both"

    invoke-direct {p0}, LX/H83;->S()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2432795
    :goto_0
    return v0

    :cond_0
    const-string v0, "instore_only"

    invoke-direct {p0}, LX/H83;->S()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final J()Z
    .locals 1

    .prologue
    .line 2432796
    iget-object v0, p0, LX/H83;->a:LX/H72;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/H83;->b:LX/H70;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final K()Z
    .locals 1

    .prologue
    .line 2432797
    iget-object v0, p0, LX/H83;->a:LX/H72;

    if-nez v0, :cond_0

    iget-object v0, p0, LX/H83;->b:LX/H70;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/H83;->b:LX/H70;

    invoke-interface {v0}, LX/H70;->D()LX/H72;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final L()Z
    .locals 1

    .prologue
    .line 2432798
    iget-object v0, p0, LX/H83;->b:LX/H70;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final M()Z
    .locals 1

    .prologue
    .line 2432799
    iget-object v0, p0, LX/H83;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final N()Z
    .locals 1

    .prologue
    .line 2432800
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->r()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->r()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->mj_()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel$ChildLocationsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final P()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2432801
    invoke-virtual {p0}, LX/H83;->e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->d()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2432802
    invoke-virtual {p0}, LX/H83;->e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2432803
    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    .line 2432804
    invoke-virtual {p0}, LX/H83;->e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;->l()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2432805
    if-eqz v0, :cond_2

    :goto_1
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public final a()LX/1vs;
    .locals 3
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getImage"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2432806
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2432807
    invoke-virtual {p0}, LX/H83;->w()LX/H72;

    move-result-object v0

    invoke-interface {v0}, LX/H72;->H()LX/0Px;

    move-result-object v0

    .line 2432808
    if-eqz v0, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2432809
    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    .line 2432810
    :goto_0
    return-object v0

    .line 2432811
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0, v2}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    goto :goto_0

    .line 2432812
    :cond_1
    iget-object v0, p0, LX/H83;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->s()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-static {v1, v0}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    goto :goto_0
.end method

.method public final declared-synchronized a(IZ)LX/2pa;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2432813
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/H83;->T()Ljava/util/List;

    move-result-object v0

    .line 2432814
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_9

    .line 2432815
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2432816
    invoke-virtual {p0}, LX/H83;->w()LX/H72;

    move-result-object v0

    invoke-interface {v0}, LX/H72;->J()LX/0Px;

    move-result-object v0

    .line 2432817
    :goto_0
    move-object v0, v0

    .line 2432818
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;

    .line 2432819
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->ml_()Ljava/lang/String;

    move-result-object v4

    .line 2432820
    invoke-static {}, Lcom/facebook/video/engine/VideoDataSource;->newBuilder()LX/2oE;

    move-result-object v5

    sget-object v6, LX/097;->FROM_STREAM:LX/097;

    .line 2432821
    iput-object v6, v5, LX/2oE;->e:LX/097;

    .line 2432822
    move-object v5, v5

    .line 2432823
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->n()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 2432824
    iput-object v6, v5, LX/2oE;->a:Landroid/net/Uri;

    .line 2432825
    move-object v5, v5

    .line 2432826
    if-eqz v4, :cond_0

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2432827
    :cond_0
    iput-object v1, v5, LX/2oE;->b:Landroid/net/Uri;

    .line 2432828
    move-object v1, v5

    .line 2432829
    invoke-virtual {v1}, LX/2oE;->h()Lcom/facebook/video/engine/VideoDataSource;

    move-result-object v1

    .line 2432830
    invoke-static {}, Lcom/facebook/video/engine/VideoPlayerParams;->newBuilder()LX/2oH;

    move-result-object v4

    invoke-virtual {v4, v1}, LX/2oH;->a(Lcom/facebook/video/engine/VideoDataSource;)LX/2oH;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->k()Ljava/lang/String;

    move-result-object v4

    .line 2432831
    iput-object v4, v1, LX/2oH;->b:Ljava/lang/String;

    .line 2432832
    move-object v1, v1

    .line 2432833
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->m()I

    move-result v4

    .line 2432834
    iput v4, v1, LX/2oH;->c:I

    .line 2432835
    move-object v1, v1

    .line 2432836
    const/4 v4, 0x0

    .line 2432837
    iput-boolean v4, v1, LX/2oH;->f:Z

    .line 2432838
    move-object v1, v1

    .line 2432839
    const/4 v4, 0x0

    .line 2432840
    iput-boolean v4, v1, LX/2oH;->g:Z

    .line 2432841
    move-object v1, v1

    .line 2432842
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->c()I

    move-result v4

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->mk_()I

    move-result v5

    invoke-virtual {v1, v4, v5}, LX/2oH;->a(II)LX/2oH;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->b()I

    move-result v4

    .line 2432843
    iput v4, v1, LX/2oH;->l:I

    .line 2432844
    move-object v1, v1

    .line 2432845
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->l()Z

    move-result v4

    .line 2432846
    iput-boolean v4, v1, LX/2oH;->h:Z

    .line 2432847
    move-object v1, v1

    .line 2432848
    iput-boolean p2, v1, LX/2oH;->o:Z

    .line 2432849
    move-object v1, v1

    .line 2432850
    new-instance v4, LX/2pZ;

    invoke-direct {v4}, LX/2pZ;-><init>()V

    .line 2432851
    invoke-virtual {v1}, LX/2oH;->n()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v1

    .line 2432852
    iput-object v1, v4, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 2432853
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->r()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_4

    .line 2432854
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->r()LX/1vs;

    move-result-object v1

    iget-object v5, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const/4 v6, 0x0

    const v7, -0x48514013

    invoke-static {v5, v1, v6, v7}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v1

    .line 2432855
    if-eqz v1, :cond_2

    invoke-static {v1}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v1

    :goto_1
    if-eqz v1, :cond_3

    move v1, v2

    :goto_2
    if-eqz v1, :cond_7

    .line 2432856
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->r()LX/1vs;

    move-result-object v1

    iget-object v5, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const/4 v6, 0x0

    const v7, -0x48514013

    invoke-static {v5, v1, v6, v7}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v1

    .line 2432857
    if-eqz v1, :cond_5

    invoke-static {v1}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v1

    :goto_3
    invoke-virtual {v1}, LX/39O;->c()I

    move-result v1

    if-lez v1, :cond_6

    :goto_4
    if-eqz v2, :cond_1

    .line 2432858
    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$VideoDataModel;->r()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x0

    const v3, -0x48514013

    invoke-static {v1, v0, v2, v3}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_5
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v2, v0, LX/1vs;->b:I

    const/4 v0, 0x0

    invoke-virtual {v1, v2, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2432859
    const-string v2, "CoverImageParamsKey"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v0

    invoke-virtual {v4, v2, v0}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    .line 2432860
    :cond_1
    const-string v0, "IsAutoplayKey"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    .line 2432861
    const-string v0, "GraphQLStoryProps"

    const/4 v1, 0x0

    invoke-static {v1}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, LX/2pZ;->a(Ljava/lang/String;Ljava/lang/Object;)LX/2pZ;

    .line 2432862
    invoke-virtual {v4}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    .line 2432863
    iget-object v1, p0, LX/H83;->d:Ljava/util/List;

    invoke-interface {v1, p1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2432864
    :goto_6
    monitor-exit p0

    return-object v0

    .line 2432865
    :cond_2
    :try_start_1
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v1

    goto :goto_1

    :cond_3
    move v1, v3

    goto :goto_2

    :cond_4
    move v1, v3

    goto :goto_2

    :cond_5
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v1

    goto :goto_3

    :cond_6
    move v2, v3

    goto :goto_4

    :cond_7
    move v2, v3

    goto :goto_4

    .line 2432866
    :cond_8
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_5

    .line 2432867
    :cond_9
    iget-object v0, p0, LX/H83;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2pa;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_6

    .line 2432868
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_a
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final b()Ljava/util/List;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getImages"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/offers/graphql/OfferQueriesInterfaces$PhotoData$;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2432741
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2432742
    invoke-virtual {p0}, LX/H83;->w()LX/H72;

    move-result-object v0

    invoke-interface {v0}, LX/H72;->H()LX/0Px;

    move-result-object v0

    .line 2432743
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 2432652
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2432653
    invoke-virtual {p0}, LX/H83;->w()LX/H72;

    move-result-object v0

    invoke-interface {v0}, LX/H72;->H()LX/0Px;

    move-result-object v0

    .line 2432654
    if-eqz v0, :cond_0

    .line 2432655
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2432656
    :goto_0
    return v0

    .line 2432657
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2432658
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2432659
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2432660
    invoke-virtual {p0}, LX/H83;->w()LX/H72;

    move-result-object v0

    invoke-interface {v0}, LX/H72;->J()LX/0Px;

    move-result-object v0

    .line 2432661
    if-eqz v0, :cond_0

    .line 2432662
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    .line 2432663
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOwner"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2432664
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2432665
    invoke-virtual {p0}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->r()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    .line 2432666
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/H83;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->t()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerModel;

    move-result-object v0

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2432667
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2432668
    invoke-virtual {p0}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->o()Ljava/lang/String;

    move-result-object v0

    .line 2432669
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/H83;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->q()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2432670
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2432671
    invoke-virtual {p0}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->mg_()Ljava/lang/String;

    move-result-object v0

    .line 2432672
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/H83;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->l()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final h()J
    .locals 2

    .prologue
    .line 2432673
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2432674
    invoke-virtual {p0}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->l()J

    move-result-wide v0

    .line 2432675
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, LX/H83;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->c()J

    move-result-wide v0

    goto :goto_0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2432676
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2432677
    invoke-virtual {p0}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->mh_()Ljava/lang/String;

    move-result-object v0

    .line 2432678
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/H83;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->n()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final j()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2432679
    invoke-virtual {p0}, LX/H83;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2432680
    iget-object v0, p0, LX/H83;->b:LX/H70;

    invoke-interface {v0}, LX/H6y;->B()Ljava/lang/String;

    move-result-object v0

    .line 2432681
    :goto_0
    return-object v0

    .line 2432682
    :cond_0
    invoke-virtual {p0}, LX/H83;->M()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2432683
    iget-object v0, p0, LX/H83;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->m()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2432684
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2432685
    invoke-virtual {p0}, LX/H83;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2432686
    iget-object v0, p0, LX/H83;->b:LX/H70;

    invoke-interface {v0}, LX/H6y;->x()Ljava/lang/String;

    move-result-object v0

    .line 2432687
    :goto_0
    return-object v0

    .line 2432688
    :cond_0
    invoke-virtual {p0}, LX/H83;->M()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2432689
    iget-object v0, p0, LX/H83;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->m()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2432690
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2432691
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2432692
    invoke-virtual {p0}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->e()Ljava/lang/String;

    move-result-object v0

    .line 2432693
    :goto_0
    return-object v0

    .line 2432694
    :cond_0
    iget-object v0, p0, LX/H83;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->m()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2432695
    const-string v0, "no-code"

    goto :goto_0

    .line 2432696
    :cond_1
    const-string v0, "single-code"

    goto :goto_0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2432697
    iget-object v0, p0, LX/H83;->a:LX/H72;

    if-eqz v0, :cond_0

    .line 2432698
    iget-object v0, p0, LX/H83;->a:LX/H72;

    invoke-interface {v0}, LX/H71;->me_()Ljava/lang/String;

    move-result-object v0

    .line 2432699
    :goto_0
    return-object v0

    .line 2432700
    :cond_0
    iget-object v0, p0, LX/H83;->b:LX/H70;

    if-eqz v0, :cond_1

    .line 2432701
    iget-object v0, p0, LX/H83;->b:LX/H70;

    invoke-interface {v0}, LX/H6y;->me_()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2432702
    :cond_1
    iget-object v0, p0, LX/H83;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->me_()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final n()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2432703
    iget-object v0, p0, LX/H83;->b:LX/H70;

    if-eqz v0, :cond_0

    .line 2432704
    iget-object v0, p0, LX/H83;->b:LX/H70;

    invoke-interface {v0}, LX/H6y;->me_()Ljava/lang/String;

    move-result-object v0

    .line 2432705
    :goto_0
    return-object v0

    .line 2432706
    :cond_0
    invoke-virtual {p0}, LX/H83;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2432707
    iget-object v0, p0, LX/H83;->a:LX/H72;

    invoke-interface {v0}, LX/H72;->K()LX/H6z;

    move-result-object v0

    invoke-interface {v0}, LX/H6x;->me_()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2432708
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2432649
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2432650
    invoke-virtual {p0}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->p()Ljava/lang/String;

    move-result-object v0

    .line 2432651
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/H83;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->o()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final p()I
    .locals 1

    .prologue
    .line 2432709
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2432710
    invoke-virtual {p0}, LX/H83;->v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->b()I

    move-result v0

    .line 2432711
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/H83;->c:Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->d()I

    move-result v0

    goto :goto_0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 2432712
    invoke-virtual {p0}, LX/H83;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2432713
    iget-object v0, p0, LX/H83;->b:LX/H70;

    invoke-interface {v0}, LX/H6y;->y()Z

    move-result v0

    .line 2432714
    :goto_0
    return v0

    .line 2432715
    :cond_0
    invoke-virtual {p0}, LX/H83;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2432716
    iget-object v0, p0, LX/H83;->a:LX/H72;

    invoke-interface {v0}, LX/H72;->K()LX/H6z;

    move-result-object v0

    invoke-interface {v0}, LX/H6x;->y()Z

    move-result v0

    goto :goto_0

    .line 2432717
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 2432718
    invoke-virtual {p0}, LX/H83;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2432719
    iget-object v0, p0, LX/H83;->b:LX/H70;

    invoke-interface {v0}, LX/H6y;->A()Z

    move-result v0

    .line 2432720
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final u()Z
    .locals 1

    .prologue
    .line 2432721
    invoke-virtual {p0}, LX/H83;->L()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, LX/H83;->K()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/H83;->a:LX/H72;

    invoke-interface {v0}, LX/H72;->K()LX/H6z;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final v()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOffer"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2432722
    invoke-virtual {p0}, LX/H83;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/H83;->w()LX/H72;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2432723
    invoke-virtual {p0}, LX/H83;->w()LX/H72;

    move-result-object v0

    invoke-interface {v0}, LX/H72;->G()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    .line 2432724
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final w()LX/H72;
    .locals 1
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOfferView"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2432725
    iget-object v0, p0, LX/H83;->a:LX/H72;

    if-eqz v0, :cond_0

    .line 2432726
    iget-object v0, p0, LX/H83;->a:LX/H72;

    .line 2432727
    :goto_0
    return-object v0

    .line 2432728
    :cond_0
    iget-object v0, p0, LX/H83;->b:LX/H70;

    if-eqz v0, :cond_1

    .line 2432729
    iget-object v0, p0, LX/H83;->b:LX/H70;

    invoke-interface {v0}, LX/H70;->D()LX/H72;

    move-result-object v0

    goto :goto_0

    .line 2432730
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final x()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2432731
    invoke-virtual {p0}, LX/H83;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/H83;->b:LX/H70;

    invoke-interface {v0}, LX/H70;->C()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2432732
    iget-object v0, p0, LX/H83;->b:LX/H70;

    invoke-interface {v0}, LX/H70;->C()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2432733
    :goto_0
    return-object v0

    .line 2432734
    :cond_0
    invoke-virtual {p0}, LX/H83;->K()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/H83;->w()LX/H72;

    move-result-object v0

    invoke-interface {v0}, LX/H72;->G()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->q()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2432735
    invoke-virtual {p0}, LX/H83;->w()LX/H72;

    move-result-object v0

    invoke-interface {v0}, LX/H72;->G()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;->q()Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$PhotoDataModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2432736
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final z()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2432737
    invoke-virtual {p0}, LX/H83;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2432738
    iget-object v0, p0, LX/H83;->b:LX/H70;

    move-object v0, v0

    .line 2432739
    invoke-interface {v0}, LX/H6y;->w()Ljava/lang/String;

    move-result-object v0

    .line 2432740
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
