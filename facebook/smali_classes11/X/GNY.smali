.class public LX/GNY;
.super LX/3HM;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0sa;LX/0rq;LX/0se;LX/0sO;LX/0SG;LX/0jU;LX/3HO;LX/0sf;LX/0tE;LX/0tG;LX/0tI;LX/0wo;LX/0ad;LX/3HE;LX/0sU;LX/0wp;LX/0sX;LX/0tQ;)V
    .locals 20
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2346267
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p14

    move-object/from16 v14, p15

    move-object/from16 v15, p16

    move-object/from16 v16, p13

    move-object/from16 v17, p17

    move-object/from16 v18, p18

    move-object/from16 v19, p19

    invoke-direct/range {v0 .. v19}, LX/3HM;-><init>(Landroid/content/res/Resources;LX/0sa;LX/0rq;LX/0se;LX/0sO;LX/0SG;LX/0jU;LX/3HO;LX/0sf;LX/0tE;LX/0tG;LX/0tI;LX/0ad;LX/3HE;LX/0sU;LX/0wo;LX/0wp;LX/0sX;LX/0tQ;)V

    .line 2346268
    return-void
.end method

.method public static c(LX/0QB;)LX/GNY;
    .locals 20

    .prologue
    .line 2346269
    new-instance v0, LX/GNY;

    invoke-static/range {p0 .. p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static/range {p0 .. p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v2

    check-cast v2, LX/0sa;

    invoke-static/range {p0 .. p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v3

    check-cast v3, LX/0rq;

    invoke-static/range {p0 .. p0}, LX/0se;->a(LX/0QB;)LX/0se;

    move-result-object v4

    check-cast v4, LX/0se;

    invoke-static/range {p0 .. p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v5

    check-cast v5, LX/0sO;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0jU;->a(LX/0QB;)LX/0jU;

    move-result-object v7

    check-cast v7, LX/0jU;

    invoke-static/range {p0 .. p0}, LX/3HO;->a(LX/0QB;)LX/3HO;

    move-result-object v8

    check-cast v8, LX/3HO;

    invoke-static/range {p0 .. p0}, LX/0sf;->a(LX/0QB;)LX/0sf;

    move-result-object v9

    check-cast v9, LX/0sf;

    invoke-static/range {p0 .. p0}, LX/0tE;->a(LX/0QB;)LX/0tE;

    move-result-object v10

    check-cast v10, LX/0tE;

    invoke-static/range {p0 .. p0}, LX/0tG;->a(LX/0QB;)LX/0tG;

    move-result-object v11

    check-cast v11, LX/0tG;

    invoke-static/range {p0 .. p0}, LX/0tI;->a(LX/0QB;)LX/0tI;

    move-result-object v12

    check-cast v12, LX/0tI;

    invoke-static/range {p0 .. p0}, LX/0wo;->a(LX/0QB;)LX/0wo;

    move-result-object v13

    check-cast v13, LX/0wo;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v14

    check-cast v14, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/3HE;->a(LX/0QB;)LX/3HE;

    move-result-object v15

    check-cast v15, LX/3HE;

    invoke-static/range {p0 .. p0}, LX/0sU;->a(LX/0QB;)LX/0sU;

    move-result-object v16

    check-cast v16, LX/0sU;

    invoke-static/range {p0 .. p0}, LX/0wp;->a(LX/0QB;)LX/0wp;

    move-result-object v17

    check-cast v17, LX/0wp;

    invoke-static/range {p0 .. p0}, LX/0sX;->a(LX/0QB;)LX/0sX;

    move-result-object v18

    check-cast v18, LX/0sX;

    invoke-static/range {p0 .. p0}, LX/0tQ;->a(LX/0QB;)LX/0tQ;

    move-result-object v19

    check-cast v19, LX/0tQ;

    invoke-direct/range {v0 .. v19}, LX/GNY;-><init>(Landroid/content/res/Resources;LX/0sa;LX/0rq;LX/0se;LX/0sO;LX/0SG;LX/0jU;LX/3HO;LX/0sf;LX/0tE;LX/0tG;LX/0tI;LX/0wo;LX/0ad;LX/3HE;LX/0sU;LX/0wp;LX/0sX;LX/0tQ;)V

    .line 2346270
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/api/story/FetchSingleStoryParams;Ljava/lang/String;)LX/0gW;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/api/story/FetchSingleStoryParams;",
            "Ljava/lang/String;",
            ")",
            "LX/0gW",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2346261
    new-instance v0, LX/GNW;

    invoke-direct {v0}, LX/GNW;-><init>()V

    move-object v0, v0

    .line 2346262
    const/4 v1, 0x1

    .line 2346263
    iput-boolean v1, v0, LX/0gW;->l:Z

    .line 2346264
    invoke-virtual {p0, p1, v0}, LX/3HN;->a(Lcom/facebook/api/story/FetchSingleStoryParams;LX/0gW;)LX/0gW;

    .line 2346265
    const-string v1, "preview_id"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2346266
    return-object v0
.end method
