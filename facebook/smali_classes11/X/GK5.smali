.class public final LX/GK5;
.super LX/GFb;
.source ""


# instance fields
.field public final synthetic a:LX/GK8;


# direct methods
.method public constructor <init>(LX/GK8;)V
    .locals 0

    .prologue
    .line 2340316
    iput-object p1, p0, LX/GK5;->a:LX/GK8;

    invoke-direct {p0}, LX/GFb;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 8

    .prologue
    .line 2340317
    check-cast p1, LX/GFa;

    .line 2340318
    iget-object v0, p1, LX/GFa;->a:Landroid/location/Location;

    move-object v0, v0

    .line 2340319
    iget-object v1, p0, LX/GK5;->a:LX/GK8;

    iget-object v1, v1, LX/GK8;->u:Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    .line 2340320
    new-instance v2, LX/A9E;

    invoke-direct {v2}, LX/A9E;-><init>()V

    iget-object v3, p0, LX/GK5;->a:LX/GK8;

    iget-object v3, v3, LX/GK8;->t:LX/0W9;

    invoke-static {v3}, LX/GNL;->a(LX/0W9;)Ljava/lang/String;

    move-result-object v3

    .line 2340321
    iput-object v3, v2, LX/A9E;->d:Ljava/lang/String;

    .line 2340322
    move-object v2, v2

    .line 2340323
    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v4

    .line 2340324
    iput-wide v4, v2, LX/A9E;->f:D

    .line 2340325
    move-object v2, v2

    .line 2340326
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;->CUSTOM_LOCATION:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    .line 2340327
    iput-object v3, v2, LX/A9E;->g:Lcom/facebook/graphql/enums/GraphQLAdGeoLocationType;

    .line 2340328
    move-object v2, v2

    .line 2340329
    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    .line 2340330
    iput-wide v4, v2, LX/A9E;->h:D

    .line 2340331
    move-object v0, v2

    .line 2340332
    iget-wide v6, p1, LX/GFa;->b:D

    move-wide v2, v6

    .line 2340333
    iput-wide v2, v0, LX/A9E;->j:D

    .line 2340334
    move-object v0, v0

    .line 2340335
    invoke-virtual {v0}, LX/A9E;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v0

    .line 2340336
    iget-object v2, p0, LX/GK5;->a:LX/GK8;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 2340337
    iput-object v3, v2, LX/GK8;->v:LX/0Px;

    .line 2340338
    iget-object v2, p0, LX/GK5;->a:LX/GK8;

    iget-object v2, v2, LX/GK8;->v:LX/0Px;

    .line 2340339
    iput-object v2, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    .line 2340340
    iput-object v0, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2340341
    iget-object v0, p0, LX/GK5;->a:LX/GK8;

    invoke-static {v0}, LX/GK8;->h(LX/GK8;)V

    .line 2340342
    iget-object v0, p0, LX/GK5;->a:LX/GK8;

    invoke-virtual {v0}, LX/GIr;->b()V

    .line 2340343
    return-void
.end method
