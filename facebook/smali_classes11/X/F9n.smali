.class public final LX/F9n;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/facebook/growth/model/DeviceOwnerData;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/F9o;


# direct methods
.method public constructor <init>(LX/F9o;)V
    .locals 0

    .prologue
    .line 2205952
    iput-object p1, p0, LX/F9n;->a:LX/F9o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 15

    .prologue
    .line 2205953
    iget-object v0, p0, LX/F9n;->a:LX/F9o;

    const/4 v2, 0x0

    .line 2205954
    iget-object v1, v0, LX/F9o;->b:LX/F9m;

    .line 2205955
    iget-object v3, v1, LX/F9m;->a:Landroid/content/Context;

    invoke-static {v3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    const-string v4, "com.google"

    invoke-virtual {v3, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    .line 2205956
    invoke-static {v3}, LX/F9m;->a([Landroid/accounts/Account;)Ljava/util/List;

    move-result-object v3

    move-object v1, v3

    .line 2205957
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2205958
    iget-object v4, v0, LX/F9o;->g:Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-virtual {v4, v1}, Lcom/facebook/growth/model/DeviceOwnerData;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2205959
    :cond_0
    iget-object v1, v0, LX/F9o;->b:LX/F9m;

    .line 2205960
    iget-object v3, v1, LX/F9m;->a:Landroid/content/Context;

    invoke-static {v3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v3

    .line 2205961
    invoke-static {v3}, LX/F9m;->a([Landroid/accounts/Account;)Ljava/util/List;

    move-result-object v3

    move-object v1, v3

    .line 2205962
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2205963
    iget-object v4, v0, LX/F9o;->g:Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-virtual {v4, v1}, Lcom/facebook/growth/model/DeviceOwnerData;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 2205964
    :cond_1
    iget-object v1, v0, LX/F9o;->b:LX/F9m;

    invoke-virtual {v1}, LX/F9m;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, LX/F9o;->a(LX/F9o;Ljava/lang/String;)V

    .line 2205965
    :try_start_0
    iget-object v1, v0, LX/F9o;->c:LX/F9p;

    invoke-virtual {v1}, LX/F9p;->a()Lcom/facebook/growth/model/DeviceOwnerData;

    move-result-object v1

    .line 2205966
    invoke-static {v0, v1}, LX/F9o;->a(LX/F9o;Lcom/facebook/growth/model/DeviceOwnerData;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 2205967
    :goto_2
    new-instance v4, Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-direct {v4}, Lcom/facebook/growth/model/DeviceOwnerData;-><init>()V

    .line 2205968
    :try_start_1
    iget-object v1, v0, LX/F9o;->g:Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-virtual {v1}, Lcom/facebook/growth/model/DeviceOwnerData;->c()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    :goto_3
    if-ge v3, v6, :cond_2

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2205969
    iget-object v7, v0, LX/F9o;->c:LX/F9p;

    const/4 v14, 0x1

    const/4 v13, 0x0

    .line 2205970
    sget-object v9, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    new-array v10, v14, [Ljava/lang/String;

    const-string v8, "contact_id"

    aput-object v8, v10, v13

    const-string v11, "data1 LIKE ? AND mimetype = ?"

    const/4 v8, 0x2

    new-array v12, v8, [Ljava/lang/String;

    aput-object v1, v12, v13

    const-string v8, "vnd.android.cursor.item/email_v2"

    aput-object v8, v12, v14

    const/4 v13, 0x0

    move-object v8, v7

    invoke-static/range {v8 .. v13}, LX/F9p;->a(LX/F9p;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    move-object v1, v8

    .line 2205971
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/growth/model/DeviceOwnerData;

    .line 2205972
    invoke-virtual {v4, v1}, Lcom/facebook/growth/model/DeviceOwnerData;->a(Lcom/facebook/growth/model/DeviceOwnerData;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4

    .line 2205973
    :catch_0
    :cond_2
    :try_start_2
    iget-object v1, v0, LX/F9o;->g:Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-virtual {v1}, Lcom/facebook/growth/model/DeviceOwnerData;->d()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    :goto_5
    if-ge v2, v5, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2205974
    iget-object v6, v0, LX/F9o;->c:LX/F9p;

    const/4 v11, 0x0

    .line 2205975
    sget-object v8, Landroid/provider/ContactsContract$PhoneLookup;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    const/4 v8, 0x1

    new-array v10, v8, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v12, "_id"

    aput-object v12, v10, v8

    move-object v8, v6

    move-object v12, v11

    move-object v13, v11

    invoke-static/range {v8 .. v13}, LX/F9p;->a(LX/F9p;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    move-object v1, v8

    .line 2205976
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_6
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/growth/model/DeviceOwnerData;

    .line 2205977
    invoke-virtual {v4, v1}, Lcom/facebook/growth/model/DeviceOwnerData;->a(Lcom/facebook/growth/model/DeviceOwnerData;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_6

    .line 2205978
    :catch_1
    :cond_3
    invoke-static {v0, v4}, LX/F9o;->a(LX/F9o;Lcom/facebook/growth/model/DeviceOwnerData;)V

    .line 2205979
    iget-object v1, v0, LX/F9o;->g:Lcom/facebook/growth/model/DeviceOwnerData;

    invoke-virtual {v1}, Lcom/facebook/growth/model/DeviceOwnerData;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2205980
    iget-object v1, v0, LX/F9o;->g:Lcom/facebook/growth/model/DeviceOwnerData;

    iget-object v2, v0, LX/F9o;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/growth/model/DeviceOwnerData;->c(Ljava/lang/String;)V

    .line 2205981
    :cond_4
    iget-object v1, v0, LX/F9o;->g:Lcom/facebook/growth/model/DeviceOwnerData;

    move-object v0, v1

    .line 2205982
    return-object v0

    .line 2205983
    :cond_5
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_3

    .line 2205984
    :cond_6
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_5

    :catch_2
    goto/16 :goto_2
.end method
