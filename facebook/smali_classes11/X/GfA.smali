.class public LX/GfA;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pj;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/GfF;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/GfA",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/GfF;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2376913
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2376914
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/GfA;->b:LX/0Zi;

    .line 2376915
    iput-object p1, p0, LX/GfA;->a:LX/0Ot;

    .line 2376916
    return-void
.end method

.method public static a(LX/0QB;)LX/GfA;
    .locals 4

    .prologue
    .line 2376917
    const-class v1, LX/GfA;

    monitor-enter v1

    .line 2376918
    :try_start_0
    sget-object v0, LX/GfA;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2376919
    sput-object v2, LX/GfA;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2376920
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2376921
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2376922
    new-instance v3, LX/GfA;

    const/16 p0, 0x2111

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/GfA;-><init>(LX/0Ot;)V

    .line 2376923
    move-object v0, v3

    .line 2376924
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2376925
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GfA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2376926
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2376927
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2376928
    check-cast p2, LX/Gf9;

    .line 2376929
    iget-object v0, p0, LX/GfA;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GfF;

    iget-object v2, p2, LX/Gf9;->a:LX/1Pp;

    iget-object v3, p2, LX/Gf9;->b:LX/99i;

    iget v4, p2, LX/Gf9;->c:I

    iget-boolean v5, p2, LX/Gf9;->d:Z

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, LX/GfF;->a(LX/1De;LX/1Pp;LX/99i;IZ)LX/1Dg;

    move-result-object v0

    .line 2376930
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2376931
    invoke-static {}, LX/1dS;->b()V

    .line 2376932
    const/4 v0, 0x0

    return-object v0
.end method
