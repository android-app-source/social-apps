.class public LX/Fsf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FrR;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/Fsf;


# instance fields
.field private final a:LX/8p9;

.field private final b:LX/Fse;

.field private final c:LX/0tX;

.field private final d:LX/Fsj;

.field private final e:LX/0ad;


# direct methods
.method public constructor <init>(LX/8p9;LX/Fse;LX/0tX;LX/Fsj;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2297339
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2297340
    iput-object p1, p0, LX/Fsf;->a:LX/8p9;

    .line 2297341
    iput-object p2, p0, LX/Fsf;->b:LX/Fse;

    .line 2297342
    iput-object p3, p0, LX/Fsf;->c:LX/0tX;

    .line 2297343
    iput-object p4, p0, LX/Fsf;->d:LX/Fsj;

    .line 2297344
    iput-object p5, p0, LX/Fsf;->e:LX/0ad;

    .line 2297345
    return-void
.end method

.method public static a(LX/Fsf;LX/0v6;LX/G12;ILcom/facebook/common/callercontext/CallerContext;LX/0zS;)LX/FsJ;
    .locals 19
    .param p0    # LX/Fsf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2297375
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Fsf;->b:LX/Fse;

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p5

    move/from16 v8, p3

    move-object/from16 v9, p4

    invoke-virtual/range {v4 .. v9}, LX/Fse;->a(LX/0v6;LX/G12;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zX;

    move-result-object v17

    .line 2297376
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Fsf;->b:LX/Fse;

    move-object/from16 v0, p1

    move-object/from16 v1, p5

    move/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v4, v0, v1, v2, v3}, LX/Fse;->a(LX/0v6;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zX;

    move-result-object v18

    .line 2297377
    const/4 v6, 0x0

    .line 2297378
    const/4 v5, 0x0

    .line 2297379
    const/4 v10, 0x0

    .line 2297380
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Fsf;->e:LX/0ad;

    sget-short v7, LX/0wf;->aO:S

    const/4 v8, 0x0

    invoke-interface {v4, v7, v8}, LX/0ad;->a(SZ)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2297381
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Fsf;->b:LX/Fse;

    move-object/from16 v0, p2

    move-object/from16 v1, p5

    move/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v4, v0, v1, v2, v3}, LX/Fse;->a(LX/G12;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v6

    .line 2297382
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Fsf;->b:LX/Fse;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v6}, LX/Fse;->a(LX/0v6;LX/0zO;)LX/0zX;

    move-result-object v11

    .line 2297383
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Fsf;->b:LX/Fse;

    move-object/from16 v5, p1

    move-object/from16 v7, p5

    move/from16 v8, p3

    move-object/from16 v9, p4

    invoke-virtual/range {v4 .. v9}, LX/Fse;->a(LX/0v6;LX/0zO;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zX;

    move-result-object v4

    move-object v14, v10

    move-object v15, v4

    move-object/from16 v16, v11

    .line 2297384
    :goto_0
    move-object/from16 v0, p0

    iget-object v5, v0, LX/Fsf;->c:LX/0tX;

    invoke-virtual/range {p2 .. p2}, LX/G12;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v4, v0, LX/Fsf;->e:LX/0ad;

    move-object/from16 v0, p5

    invoke-static {v4, v0}, LX/Fsl;->a(LX/0ad;LX/0zS;)LX/0zS;

    move-result-object v9

    const-wide/32 v10, 0x93a80

    move-object/from16 v4, p1

    move/from16 v7, p3

    move-object/from16 v8, p4

    invoke-static/range {v4 .. v11}, LX/Fsl;->a(LX/0v6;LX/0tX;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;LX/0zS;J)LX/0zX;

    move-result-object v10

    .line 2297385
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Fsf;->b:LX/Fse;

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v7, p5

    move/from16 v8, p3

    move-object/from16 v9, p4

    invoke-virtual/range {v4 .. v9}, LX/Fse;->b(LX/0v6;LX/G12;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zX;

    move-result-object v11

    .line 2297386
    const/4 v13, 0x0

    .line 2297387
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Fsf;->a:LX/8p9;

    invoke-virtual {v4}, LX/8p9;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2297388
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Fsf;->b:LX/Fse;

    move-object/from16 v0, p1

    move/from16 v1, p3

    move-object/from16 v2, p4

    invoke-virtual {v4, v0, v1, v2}, LX/Fse;->a(LX/0v6;ILcom/facebook/common/callercontext/CallerContext;)LX/0zX;

    move-result-object v13

    .line 2297389
    :cond_0
    new-instance v4, LX/FsJ;

    const/4 v12, 0x0

    move-object/from16 v5, v16

    move-object v6, v15

    move-object v7, v14

    move-object v8, v10

    move-object/from16 v9, v17

    move-object/from16 v10, v18

    invoke-direct/range {v4 .. v13}, LX/FsJ;-><init>(LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;)V

    return-object v4

    .line 2297390
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, LX/Fsf;->b:LX/Fse;

    move-object/from16 v0, p2

    move-object/from16 v1, p5

    move/from16 v2, p3

    move-object/from16 v3, p4

    invoke-virtual {v4, v0, v1, v2, v3}, LX/Fse;->b(LX/G12;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v4

    .line 2297391
    move-object/from16 v0, p0

    iget-object v7, v0, LX/Fsf;->d:LX/Fsj;

    move-object/from16 v0, p1

    invoke-virtual {v7, v0, v4}, LX/Fsj;->a(LX/0v6;LX/0zO;)LX/0zX;

    move-result-object v4

    move-object v14, v4

    move-object v15, v5

    move-object/from16 v16, v6

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/Fsf;
    .locals 3

    .prologue
    .line 2297365
    sget-object v0, LX/Fsf;->f:LX/Fsf;

    if-nez v0, :cond_1

    .line 2297366
    const-class v1, LX/Fsf;

    monitor-enter v1

    .line 2297367
    :try_start_0
    sget-object v0, LX/Fsf;->f:LX/Fsf;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2297368
    if-eqz v2, :cond_0

    .line 2297369
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/Fsf;->b(LX/0QB;)LX/Fsf;

    move-result-object v0

    sput-object v0, LX/Fsf;->f:LX/Fsf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2297370
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2297371
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2297372
    :cond_1
    sget-object v0, LX/Fsf;->f:LX/Fsf;

    return-object v0

    .line 2297373
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2297374
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/Fsf;
    .locals 14

    .prologue
    .line 2297392
    new-instance v0, LX/Fsf;

    invoke-static {p0}, LX/8p9;->a(LX/0QB;)LX/8p9;

    move-result-object v1

    check-cast v1, LX/8p9;

    .line 2297393
    new-instance v6, LX/Fse;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v7

    check-cast v7, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v8

    check-cast v8, LX/0tX;

    invoke-static {p0}, LX/FsI;->a(LX/0QB;)LX/FsI;

    move-result-object v9

    check-cast v9, LX/FsI;

    invoke-static {p0}, LX/Fsg;->a(LX/0QB;)LX/Fsg;

    move-result-object v10

    check-cast v10, LX/Fsg;

    invoke-static {p0}, LX/FsX;->b(LX/0QB;)LX/FsX;

    move-result-object v11

    check-cast v11, LX/FsX;

    invoke-static {p0}, LX/G2W;->a(LX/0QB;)LX/G2W;

    move-result-object v12

    check-cast v12, LX/G2W;

    invoke-static {p0}, LX/2dk;->b(LX/0QB;)LX/2dk;

    move-result-object v13

    check-cast v13, LX/2dk;

    invoke-direct/range {v6 .. v13}, LX/Fse;-><init>(Landroid/content/res/Resources;LX/0tX;LX/FsI;LX/Fsg;LX/FsX;LX/G2W;LX/2dk;)V

    .line 2297394
    move-object v2, v6

    .line 2297395
    check-cast v2, LX/Fse;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/Fsj;->a(LX/0QB;)LX/Fsj;

    move-result-object v4

    check-cast v4, LX/Fsj;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct/range {v0 .. v5}, LX/Fsf;-><init>(LX/8p9;LX/Fse;LX/0tX;LX/Fsj;LX/0ad;)V

    .line 2297396
    return-object v0
.end method


# virtual methods
.method public final a(LX/0v6;IZLX/G12;Lcom/facebook/common/callercontext/CallerContext;)LX/FsJ;
    .locals 10

    .prologue
    .line 2297350
    iget-object v0, p0, LX/Fsf;->e:LX/0ad;

    sget-short v1, LX/0wf;->aq:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2297351
    if-eqz p3, :cond_2

    .line 2297352
    sget-object v8, LX/0zS;->d:LX/0zS;

    move-object v3, p0

    move-object v4, p1

    move-object v5, p4

    move v6, p2

    move-object v7, p5

    invoke-static/range {v3 .. v8}, LX/Fsf;->a(LX/Fsf;LX/0v6;LX/G12;ILcom/facebook/common/callercontext/CallerContext;LX/0zS;)LX/FsJ;

    move-result-object v3

    .line 2297353
    :goto_0
    move-object v0, v3

    .line 2297354
    :goto_1
    return-object v0

    .line 2297355
    :cond_0
    if-eqz p3, :cond_3

    sget-object v8, LX/0zS;->d:LX/0zS;

    :goto_2
    move-object v3, p0

    move-object v4, p1

    move-object v5, p4

    move v6, p2

    move-object v7, p5

    .line 2297356
    invoke-static/range {v3 .. v8}, LX/Fsf;->a(LX/Fsf;LX/0v6;LX/G12;ILcom/facebook/common/callercontext/CallerContext;LX/0zS;)LX/FsJ;

    move-result-object v3

    .line 2297357
    if-nez p3, :cond_1

    .line 2297358
    invoke-static {p4, p5, v3, p0}, LX/Fs8;->a(LX/G12;Lcom/facebook/common/callercontext/CallerContext;LX/FsJ;LX/FrR;)LX/FsJ;

    move-result-object v3

    .line 2297359
    :cond_1
    move-object v0, v3

    .line 2297360
    goto :goto_1

    .line 2297361
    :cond_2
    const/4 v4, 0x0

    sget-object v8, LX/0zS;->b:LX/0zS;

    move-object v3, p0

    move-object v5, p4

    move v6, p2

    move-object v7, p5

    invoke-static/range {v3 .. v8}, LX/Fsf;->a(LX/Fsf;LX/0v6;LX/G12;ILcom/facebook/common/callercontext/CallerContext;LX/0zS;)LX/FsJ;

    move-result-object v9

    .line 2297362
    sget-object v8, LX/0zS;->d:LX/0zS;

    move-object v3, p0

    move-object v4, p1

    move-object v5, p4

    move v6, p2

    move-object v7, p5

    invoke-static/range {v3 .. v8}, LX/Fsf;->a(LX/Fsf;LX/0v6;LX/G12;ILcom/facebook/common/callercontext/CallerContext;LX/0zS;)LX/FsJ;

    move-result-object v3

    .line 2297363
    invoke-static {v9, v3}, LX/Fs5;->a(LX/FsJ;LX/FsJ;)LX/FsJ;

    move-result-object v3

    goto :goto_0

    .line 2297364
    :cond_3
    sget-object v8, LX/0zS;->a:LX/0zS;

    goto :goto_2
.end method

.method public final a(ZLX/G12;Lcom/facebook/common/callercontext/CallerContext;)LX/FsJ;
    .locals 6

    .prologue
    .line 2297346
    new-instance v1, LX/0v6;

    const-string v0, "TimelineSelfFirstUnits"

    invoke-direct {v1, v0}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 2297347
    const/4 v2, 0x0

    move-object v0, p0

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, LX/Fsf;->a(LX/0v6;IZLX/G12;Lcom/facebook/common/callercontext/CallerContext;)LX/FsJ;

    move-result-object v0

    .line 2297348
    iget-object v2, p0, LX/Fsf;->c:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0v6;)V

    .line 2297349
    return-object v0
.end method
