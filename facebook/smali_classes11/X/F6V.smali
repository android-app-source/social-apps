.class public final LX/F6V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field public a:LX/A8g;

.field public final synthetic b:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;)V
    .locals 0

    .prologue
    .line 2200434
    iput-object p1, p0, LX/F6V;->b:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2200435
    iget-object v0, p0, LX/F6V;->b:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    iget-object v0, v0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->x:Lcom/facebook/widget/countryspinner/CountrySpinner;

    invoke-virtual {v0}, Lcom/facebook/widget/countryspinner/CountrySpinner;->getSelectedCountryIsoCode()Ljava/lang/String;

    move-result-object v0

    .line 2200436
    iget-object v1, p0, LX/F6V;->b:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    iget-object v1, v1, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->w:Landroid/widget/EditText;

    iget-object v2, p0, LX/F6V;->a:LX/A8g;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2200437
    new-instance v1, LX/A8g;

    iget-object v2, p0, LX/F6V;->b:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    invoke-direct {v1, v0, v2}, LX/A8g;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    iput-object v1, p0, LX/F6V;->a:LX/A8g;

    .line 2200438
    iget-object v1, p0, LX/F6V;->b:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    iget-object v1, v1, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->w:Landroid/widget/EditText;

    iget-object v2, p0, LX/F6V;->a:LX/A8g;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2200439
    iget-object v1, p0, LX/F6V;->b:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    invoke-static {v1, v0}, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->c(Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;Ljava/lang/String;)V

    .line 2200440
    return-void
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2200441
    return-void
.end method
