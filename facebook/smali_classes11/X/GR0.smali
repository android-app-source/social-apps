.class public final enum LX/GR0;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GR0;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GR0;

.field public static final enum NO_CONNECTIVITY:LX/GR0;

.field public static final enum SERVER_ERROR:LX/GR0;

.field public static final enum SUCCESS:LX/GR0;

.field public static final enum UNKNOWN_ERROR:LX/GR0;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2350992
    new-instance v0, LX/GR0;

    const-string v1, "SUCCESS"

    invoke-direct {v0, v1, v2}, LX/GR0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GR0;->SUCCESS:LX/GR0;

    .line 2350993
    new-instance v0, LX/GR0;

    const-string v1, "SERVER_ERROR"

    invoke-direct {v0, v1, v3}, LX/GR0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GR0;->SERVER_ERROR:LX/GR0;

    .line 2350994
    new-instance v0, LX/GR0;

    const-string v1, "NO_CONNECTIVITY"

    invoke-direct {v0, v1, v4}, LX/GR0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GR0;->NO_CONNECTIVITY:LX/GR0;

    .line 2350995
    new-instance v0, LX/GR0;

    const-string v1, "UNKNOWN_ERROR"

    invoke-direct {v0, v1, v5}, LX/GR0;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GR0;->UNKNOWN_ERROR:LX/GR0;

    .line 2350996
    const/4 v0, 0x4

    new-array v0, v0, [LX/GR0;

    sget-object v1, LX/GR0;->SUCCESS:LX/GR0;

    aput-object v1, v0, v2

    sget-object v1, LX/GR0;->SERVER_ERROR:LX/GR0;

    aput-object v1, v0, v3

    sget-object v1, LX/GR0;->NO_CONNECTIVITY:LX/GR0;

    aput-object v1, v0, v4

    sget-object v1, LX/GR0;->UNKNOWN_ERROR:LX/GR0;

    aput-object v1, v0, v5

    sput-object v0, LX/GR0;->$VALUES:[LX/GR0;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2350998
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GR0;
    .locals 1

    .prologue
    .line 2350999
    const-class v0, LX/GR0;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GR0;

    return-object v0
.end method

.method public static values()[LX/GR0;
    .locals 1

    .prologue
    .line 2350997
    sget-object v0, LX/GR0;->$VALUES:[LX/GR0;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GR0;

    return-object v0
.end method
