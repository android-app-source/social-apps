.class public LX/HDL;
.super LX/1a1;
.source ""


# instance fields
.field private final l:LX/H8V;

.field private final m:LX/23P;

.field private final n:Lcom/facebook/fig/listitem/FigListItem;

.field private final o:Lcom/facebook/resources/ui/FbTextView;

.field private final p:Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;

.field private final q:Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;


# direct methods
.method public constructor <init>(LX/H8V;LX/23P;Landroid/view/View;)V
    .locals 2
    .param p3    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2441129
    invoke-direct {p0, p3}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2441130
    iput-object p1, p0, LX/HDL;->l:LX/H8V;

    .line 2441131
    iput-object p2, p0, LX/HDL;->m:LX/23P;

    .line 2441132
    const v0, 0x7f0d0d1e

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    iput-object v0, p0, LX/HDL;->n:Lcom/facebook/fig/listitem/FigListItem;

    .line 2441133
    const v0, 0x7f0d0d1f

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/HDL;->o:Lcom/facebook/resources/ui/FbTextView;

    .line 2441134
    iget-object v0, p0, LX/HDL;->o:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/HDL;->m:LX/23P;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2441135
    const v0, 0x7f0d0d20

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;

    iput-object v0, p0, LX/HDL;->p:Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;

    .line 2441136
    const v0, 0x7f0d0d21

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;

    iput-object v0, p0, LX/HDL;->q:Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;

    .line 2441137
    return-void
.end method


# virtual methods
.method public final a(JLcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;Landroid/view/View$OnClickListener;)V
    .locals 9
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindData"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2441138
    invoke-virtual {p3}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->e()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;

    move-result-object v0

    .line 2441139
    invoke-virtual {p3}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->b()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;

    move-result-object v8

    .line 2441140
    iget-object v1, p0, LX/HDL;->n:Lcom/facebook/fig/listitem/FigListItem;

    const v2, 0x7f0836c5

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(I)V

    .line 2441141
    iget-object v1, p0, LX/HDL;->n:Lcom/facebook/fig/listitem/FigListItem;

    const v2, 0x7f0836c6

    invoke-virtual {v1, v2}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(I)V

    .line 2441142
    invoke-virtual {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;->b()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v8}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2441143
    :cond_0
    iget-object v1, p0, LX/HDL;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v7}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2441144
    :goto_0
    iget-object v1, p0, LX/HDL;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, p5}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2441145
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2441146
    invoke-virtual {v0}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$PrimaryButtonsChannelModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v7

    :goto_1
    if-ge v1, v4, :cond_3

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    .line 2441147
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 2441148
    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2441149
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2441150
    :cond_2
    iget-object v1, p0, LX/HDL;->o:Lcom/facebook/resources/ui/FbTextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_0

    .line 2441151
    :cond_3
    iget-object v1, p0, LX/HDL;->p:Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;->NONE:Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;

    move-wide v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/facebook/pages/common/actionchannel/primarybuttons/PagesDualCallToActionContainer;->a(JLX/0Px;Lcom/facebook/graphql/enums/GraphQLPageCallToActionRef;Z)V

    .line 2441152
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2441153
    invoke-virtual {v8}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$ActionBarChannelModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v7

    :goto_2
    if-ge v1, v4, :cond_4

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    .line 2441154
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2441155
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2441156
    :cond_4
    iget-object v0, p0, LX/HDL;->l:LX/H8V;

    iget-object v1, p0, LX/HDL;->q:Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;

    invoke-virtual {v0, v1}, LX/H8V;->a(Lcom/facebook/pages/common/actionchannel/actionbar/ui/BetterActionBarView;)LX/H8U;

    move-result-object v1

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    move-wide v2, p1

    move-object v4, p4

    invoke-virtual/range {v1 .. v6}, LX/H8U;->a(JLcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;LX/0Px;Z)V

    .line 2441157
    return-void
.end method
