.class public LX/GaN;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:Lcom/facebook/common/patch/core/BsdiffNativeLibrary;


# direct methods
.method public constructor <init>(Lcom/facebook/common/patch/core/BsdiffNativeLibrary;)V
    .locals 0

    .prologue
    .line 2368230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2368231
    iput-object p1, p0, LX/GaN;->a:Lcom/facebook/common/patch/core/BsdiffNativeLibrary;

    .line 2368232
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/File;Ljava/io/File;Ljava/io/File;)V
    .locals 3

    .prologue
    .line 2368233
    iget-object v0, p0, LX/GaN;->a:Lcom/facebook/common/patch/core/BsdiffNativeLibrary;

    invoke-virtual {v0}, LX/03m;->b()V

    .line 2368234
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/patch/core/BsdiffNativeLibrary;->patch(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2368235
    return-void

    .line 2368236
    :catch_0
    move-exception v0

    .line 2368237
    new-instance v1, Lcom/facebook/common/patch/core/PatchException;

    const-string v2, "Error applying patch."

    invoke-direct {v1, v2, v0}, Lcom/facebook/common/patch/core/PatchException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
