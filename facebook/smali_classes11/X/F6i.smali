.class public LX/F6i;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0aG;

.field private final b:Ljava/lang/String;

.field private final c:LX/89v;


# direct methods
.method public constructor <init>(LX/89v;LX/0aG;Ljava/lang/String;)V
    .locals 0
    .param p1    # LX/89v;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/common/hardware/StrictPhoneIsoCountryCode;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2200784
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2200785
    iput-object p1, p0, LX/F6i;->c:LX/89v;

    .line 2200786
    iput-object p2, p0, LX/F6i;->a:LX/0aG;

    .line 2200787
    iput-object p3, p0, LX/F6i;->b:Ljava/lang/String;

    .line 2200788
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;ZLcom/facebook/common/callercontext/CallerContext;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;Z",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2200789
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2200790
    :goto_0
    return-void

    .line 2200791
    :cond_0
    new-instance v0, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;

    iget-object v2, p0, LX/F6i;->b:Ljava/lang/String;

    iget-object v3, p0, LX/F6i;->c:LX/89v;

    const/4 v5, 0x0

    move-object v1, p1

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;-><init>(Ljava/util/List;Ljava/lang/String;LX/89v;ZZ)V

    .line 2200792
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2200793
    const-string v1, "growthUsersInviteParamsKey"

    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2200794
    iget-object v0, p0, LX/F6i;->a:LX/0aG;

    const-string v1, "growth_users_invite"

    sget-object v3, LX/1ME;->BY_EXCEPTION:LX/1ME;

    const v5, -0x19248b29

    move-object v4, p3

    invoke-static/range {v0 .. v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    goto :goto_0
.end method
