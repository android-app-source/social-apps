.class public LX/GIz;
.super LX/GIw;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GIw",
        "<",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/GMU;

.field private b:LX/GN0;

.field private c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;


# direct methods
.method public constructor <init>(LX/GN0;LX/GMU;LX/GK4;)V
    .locals 0
    .param p1    # LX/GN0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2337614
    invoke-direct {p0, p3}, LX/GIw;-><init>(LX/GK4;)V

    .line 2337615
    iput-object p2, p0, LX/GIz;->a:LX/GMU;

    .line 2337616
    iput-object p1, p0, LX/GIz;->b:LX/GN0;

    .line 2337617
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2337602
    if-nez p1, :cond_0

    .line 2337603
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337604
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080abc

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2337605
    :goto_0
    return-object v0

    .line 2337606
    :cond_0
    iget-object v0, p0, LX/GIz;->a:LX/GMU;

    iget-object v1, p0, LX/GIz;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->y()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2337607
    if-nez v0, :cond_1

    .line 2337608
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337609
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080abc

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2337610
    :cond_1
    iget-object v1, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v1, v1

    .line 2337611
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f080a91

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v2

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v4

    .line 2337612
    iget-object v0, p0, LX/GIw;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2337613
    invoke-static {v0}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v0

    invoke-static {v2, v4, v5, v0}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2337593
    iget-object v0, p0, LX/GIz;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-ne v0, v1, :cond_0

    .line 2337594
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337595
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080acc

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2337596
    :goto_0
    return-object v0

    .line 2337597
    :cond_0
    if-nez p1, :cond_1

    .line 2337598
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337599
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080146

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2337600
    :cond_1
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337601
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f080a90

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2337592
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    invoke-virtual {p0, p1, p2}, LX/GIw;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2337589
    invoke-super {p0, p1}, LX/GIw;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2337590
    iput-object p1, p0, LX/GIz;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2337591
    return-void
.end method

.method public final b()Landroid/view/View$OnClickListener;
    .locals 4

    .prologue
    .line 2337584
    iget-object v0, p0, LX/GIz;->b:LX/GN0;

    .line 2337585
    iget-object v1, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v1, v1

    .line 2337586
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2337587
    iget-object v2, p0, LX/GHg;->b:LX/GCE;

    move-object v2, v2

    .line 2337588
    iget-object v3, p0, LX/GIz;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0, v1, v2, v3}, LX/GN0;->e(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)V
    .locals 2

    .prologue
    .line 2337618
    iget-object v0, p0, LX/GIw;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2337619
    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->INACTIVE:LX/GGB;

    if-eq v0, v1, :cond_0

    .line 2337620
    iget-object v0, p0, LX/GIw;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2337621
    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    if-ne v0, v1, :cond_1

    .line 2337622
    :cond_0
    invoke-super {p0, p1}, LX/GIw;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)V

    .line 2337623
    :goto_0
    return-void

    .line 2337624
    :cond_1
    if-nez p1, :cond_2

    .line 2337625
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337626
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, LX/GIz;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAddBudgetButtonText(Ljava/lang/String;)V

    goto :goto_0

    .line 2337627
    :cond_2
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337628
    invoke-virtual {p0, p1}, LX/GIz;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAddBudgetButtonText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c()Landroid/view/View$OnClickListener;
    .locals 4

    .prologue
    .line 2337579
    iget-object v0, p0, LX/GIz;->b:LX/GN0;

    .line 2337580
    iget-object v1, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v1, v1

    .line 2337581
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2337582
    iget-object v2, p0, LX/GHg;->b:LX/GCE;

    move-object v2, v2

    .line 2337583
    iget-object v3, p0, LX/GIz;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0, v1, v2, v3}, LX/GN0;->d(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method public final d()Landroid/view/View$OnClickListener;
    .locals 4

    .prologue
    .line 2337574
    iget-object v0, p0, LX/GIz;->b:LX/GN0;

    .line 2337575
    iget-object v1, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v1, v1

    .line 2337576
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2337577
    iget-object v2, p0, LX/GHg;->b:LX/GCE;

    move-object v2, v2

    .line 2337578
    iget-object v3, p0, LX/GIz;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0, v1, v2, v3}, LX/GN0;->c(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method public final e()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 2337505
    sget-object v0, LX/GIy;->a:[I

    .line 2337506
    iget-object v1, p0, LX/GIw;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v1, v1

    .line 2337507
    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    invoke-virtual {v1}, LX/GGB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2337508
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337509
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setCreateAdButtonVisibility(I)V

    .line 2337510
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337511
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAddBudgetButtonVisibility(I)V

    .line 2337512
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337513
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setResumeAdButtonVisibility(I)V

    .line 2337514
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337515
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setPauseAdButtonVisibility(I)V

    .line 2337516
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337517
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setDeleteAdButtonVisibility(I)V

    .line 2337518
    :goto_0
    return-void

    .line 2337519
    :pswitch_0
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337520
    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setCreateAdButtonVisibility(I)V

    .line 2337521
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337522
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAddBudgetButtonVisibility(I)V

    .line 2337523
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337524
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setResumeAdButtonVisibility(I)V

    .line 2337525
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337526
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setPauseAdButtonVisibility(I)V

    .line 2337527
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337528
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setDeleteAdButtonVisibility(I)V

    goto :goto_0

    .line 2337529
    :pswitch_1
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337530
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setCreateAdButtonVisibility(I)V

    .line 2337531
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337532
    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAddBudgetButtonVisibility(I)V

    .line 2337533
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337534
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setResumeAdButtonVisibility(I)V

    .line 2337535
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337536
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setPauseAdButtonVisibility(I)V

    .line 2337537
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337538
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setDeleteAdButtonVisibility(I)V

    goto :goto_0

    .line 2337539
    :pswitch_2
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337540
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setCreateAdButtonVisibility(I)V

    .line 2337541
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337542
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAddBudgetButtonVisibility(I)V

    .line 2337543
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337544
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setResumeAdButtonVisibility(I)V

    .line 2337545
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337546
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setPauseAdButtonVisibility(I)V

    .line 2337547
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337548
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setDeleteAdButtonVisibility(I)V

    goto :goto_0

    .line 2337549
    :pswitch_3
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337550
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setCreateAdButtonVisibility(I)V

    .line 2337551
    iget-object v0, p0, LX/GIz;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_POST:LX/8wL;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/GIz;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-ne v0, v1, :cond_1

    .line 2337552
    :cond_0
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337553
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAddBudgetButtonVisibility(I)V

    .line 2337554
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337555
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setLegalDisclaimerVisibility(I)V

    .line 2337556
    :goto_1
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337557
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setResumeAdButtonVisibility(I)V

    .line 2337558
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337559
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setDeleteAdButtonVisibility(I)V

    .line 2337560
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337561
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setPauseAdButtonVisibility(I)V

    goto/16 :goto_0

    .line 2337562
    :cond_1
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337563
    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAddBudgetButtonVisibility(I)V

    goto :goto_1

    .line 2337564
    :pswitch_4
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337565
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setCreateAdButtonVisibility(I)V

    .line 2337566
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337567
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setAddBudgetButtonVisibility(I)V

    .line 2337568
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337569
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setResumeAdButtonVisibility(I)V

    .line 2337570
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337571
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setPauseAdButtonVisibility(I)V

    .line 2337572
    iget-object v0, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v0, v0

    .line 2337573
    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setDeleteAdButtonVisibility(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final f()Landroid/view/View$OnClickListener;
    .locals 3

    .prologue
    .line 2337502
    iget-object v0, p0, LX/GIz;->b:LX/GN0;

    .line 2337503
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2337504
    iget-object v2, p0, LX/GIz;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0, v1, v2}, LX/GN0;->b(LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 2337490
    iget-object v0, p0, LX/GIz;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 2337491
    :goto_0
    iget-object v1, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v2, v1

    .line 2337492
    if-eqz v0, :cond_1

    const v1, 0x7f080aca

    :goto_1
    invoke-virtual {v2, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setPauseAdButtonText(I)V

    .line 2337493
    iget-object v1, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v2, v1

    .line 2337494
    if-eqz v0, :cond_2

    const v1, 0x7f080acb

    :goto_2
    invoke-virtual {v2, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setResumeAdButtonText(I)V

    .line 2337495
    iget-object v1, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v1, v1

    .line 2337496
    if-eqz v0, :cond_3

    const v0, 0x7f080ac9

    :goto_3
    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setDeleteAdButtonText(I)V

    .line 2337497
    return-void

    .line 2337498
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2337499
    :cond_1
    const v1, 0x7f080ac7

    goto :goto_1

    .line 2337500
    :cond_2
    const v1, 0x7f080ac8

    goto :goto_2

    .line 2337501
    :cond_3
    const v0, 0x7f080ad3

    goto :goto_3
.end method

.method public final h()Landroid/view/View$OnClickListener;
    .locals 4

    .prologue
    .line 2337485
    iget-object v0, p0, LX/GIz;->b:LX/GN0;

    .line 2337486
    iget-object v1, p0, LX/GIw;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v1, v1

    .line 2337487
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2337488
    iget-object v2, p0, LX/GHg;->b:LX/GCE;

    move-object v2, v2

    .line 2337489
    iget-object v3, p0, LX/GIz;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0, v1, v2, v3}, LX/GN0;->b(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method
