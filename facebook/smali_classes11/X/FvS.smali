.class public LX/FvS;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# instance fields
.field public final b:Landroid/content/Context;

.field public final c:LX/0iA;

.field private final d:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final e:Z

.field public final f:Landroid/os/Handler;

.field public final g:LX/Fsr;

.field public final h:LX/D2D;

.field public i:I

.field public j:Z

.field public k:Z

.field public l:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2301525
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->TIMELINE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/FvS;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/0iA;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/lang/Boolean;Landroid/os/Handler;LX/Fsr;LX/D2D;)V
    .locals 1
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .param p5    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2301510
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2301511
    iput-boolean v0, p0, LX/FvS;->j:Z

    .line 2301512
    iput-boolean v0, p0, LX/FvS;->k:Z

    .line 2301513
    iput-boolean v0, p0, LX/FvS;->l:Z

    .line 2301514
    iput-object p1, p0, LX/FvS;->b:Landroid/content/Context;

    .line 2301515
    iput-object p2, p0, LX/FvS;->c:LX/0iA;

    .line 2301516
    iput-object p3, p0, LX/FvS;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2301517
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/FvS;->e:Z

    .line 2301518
    iput-object p5, p0, LX/FvS;->f:Landroid/os/Handler;

    .line 2301519
    iput-object p6, p0, LX/FvS;->g:LX/Fsr;

    .line 2301520
    iput-object p7, p0, LX/FvS;->h:LX/D2D;

    .line 2301521
    return-void
.end method

.method public static a(LX/0QB;)LX/FvS;
    .locals 9

    .prologue
    .line 2301522
    new-instance v1, LX/FvS;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {p0}, LX/0iA;->a(LX/0QB;)LX/0iA;

    move-result-object v3

    check-cast v3, LX/0iA;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-static {p0}, LX/0Ss;->b(LX/0QB;)Landroid/os/Handler;

    move-result-object v6

    check-cast v6, Landroid/os/Handler;

    invoke-static {p0}, LX/Fsr;->a(LX/0QB;)LX/Fsr;

    move-result-object v7

    check-cast v7, LX/Fsr;

    invoke-static {p0}, LX/D2D;->a(LX/0QB;)LX/D2D;

    move-result-object v8

    check-cast v8, LX/D2D;

    invoke-direct/range {v1 .. v8}, LX/FvS;-><init>(Landroid/content/Context;LX/0iA;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/lang/Boolean;Landroid/os/Handler;LX/Fsr;LX/D2D;)V

    .line 2301523
    move-object v0, v1

    .line 2301524
    return-object v0
.end method

.method private static a(LX/FvS;ILandroid/view/View;Ljava/lang/String;)V
    .locals 3
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2301429
    new-instance v0, LX/0hs;

    iget-object v1, p0, LX/FvS;->b:Landroid/content/Context;

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, LX/0hs;-><init>(Landroid/content/Context;I)V

    .line 2301430
    sget-object v1, LX/3AV;->ABOVE:LX/3AV;

    invoke-virtual {v0, v1}, LX/0ht;->a(LX/3AV;)V

    .line 2301431
    const/4 v1, -0x1

    .line 2301432
    iput v1, v0, LX/0hs;->t:I

    .line 2301433
    invoke-virtual {v0, p2}, LX/0ht;->a(Landroid/view/View;)V

    .line 2301434
    invoke-virtual {v0, p1}, LX/0hs;->a(I)V

    .line 2301435
    invoke-virtual {v0}, LX/0ht;->d()V

    .line 2301436
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/FvS;->k:Z

    .line 2301437
    if-eqz p3, :cond_0

    .line 2301438
    iget-object v0, p0, LX/FvS;->c:LX/0iA;

    invoke-virtual {v0}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2301439
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/5SB;LX/BQ1;Landroid/view/View;)V
    .locals 11

    .prologue
    .line 2301440
    iget-boolean v0, p0, LX/FvS;->k:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/FvS;->l:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/FvS;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/BQO;->b:LX/0Tn;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, LX/0i9;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, LX/5SB;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, LX/BPy;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2301441
    :cond_0
    :goto_0
    return-void

    .line 2301442
    :cond_1
    iget-object v0, p0, LX/FvS;->c:LX/0iA;

    const-string v1, "3972"

    const-class v2, LX/3ks;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3ks;

    .line 2301443
    const/4 v1, 0x1

    .line 2301444
    iput-boolean v1, v0, LX/3ks;->b:Z

    .line 2301445
    iget-object v1, p0, LX/FvS;->c:LX/0iA;

    sget-object v2, LX/FvS;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class p1, LX/3ks;

    invoke-virtual {v1, v2, p1}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    check-cast v1, LX/3ks;

    .line 2301446
    const/4 v2, 0x0

    .line 2301447
    iput-boolean v2, v0, LX/3ks;->b:Z

    .line 2301448
    move-object v0, v1

    .line 2301449
    if-eqz v0, :cond_3

    .line 2301450
    invoke-virtual {v0}, LX/3ks;->b()Ljava/lang/String;

    move-result-object v0

    const/4 p1, 0x1

    const/4 v1, 0x0

    const/4 p3, 0x2

    .line 2301451
    iput-boolean p1, p0, LX/FvS;->l:Z

    .line 2301452
    new-array v2, p3, [I

    const v3, 0x7f021521

    aput v3, v2, v1

    const v3, 0x7f021520

    aput v3, v2, p1

    .line 2301453
    iput p3, p0, LX/FvS;->i:I

    .line 2301454
    new-instance v3, LX/FvQ;

    invoke-direct {v3, p0, v0}, LX/FvQ;-><init>(LX/FvS;Ljava/lang/String;)V

    .line 2301455
    :goto_1
    if-ge v1, p3, :cond_2

    aget p1, v2, v1

    .line 2301456
    iget-object p2, p0, LX/FvS;->b:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-static {p2, p1, v3}, LX/99C;->a(Landroid/content/res/Resources;ILX/99A;)V

    .line 2301457
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2301458
    :cond_2
    goto :goto_0

    .line 2301459
    :cond_3
    invoke-virtual {p2}, LX/BQ1;->w()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2301460
    iget-object v0, p2, LX/BQ1;->l:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;

    move-object v0, v0

    .line 2301461
    if-eqz v0, :cond_4

    .line 2301462
    iget-object v0, p2, LX/BQ1;->l:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;

    move-object v0, v0

    .line 2301463
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineNuxFieldsModel$ProfileWizardNuxModel;->c()Z

    move-result v0

    if-nez v0, :cond_6

    .line 2301464
    :cond_4
    iget-boolean v0, p0, LX/FvS;->e:Z

    if-eqz v0, :cond_5

    const v0, 0x7f081574

    .line 2301465
    :goto_2
    const/4 v1, 0x0

    invoke-static {p0, v0, p3, v1}, LX/FvS;->a(LX/FvS;ILandroid/view/View;Ljava/lang/String;)V

    goto :goto_0

    .line 2301466
    :cond_5
    const v0, 0x7f081573

    goto :goto_2

    .line 2301467
    :cond_6
    iget-object v0, p0, LX/FvS;->c:LX/0iA;

    const-string v1, "4101"

    const-class v2, LX/Fux;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/Fux;

    .line 2301468
    const/4 v1, 0x1

    .line 2301469
    iput-boolean v1, v0, LX/Fux;->a:Z

    .line 2301470
    iget-object v1, p0, LX/FvS;->c:LX/0iA;

    sget-object v2, LX/FvS;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v3, LX/Fux;

    invoke-virtual {v1, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    check-cast v1, LX/Fux;

    .line 2301471
    const/4 v2, 0x0

    .line 2301472
    iput-boolean v2, v0, LX/Fux;->a:Z

    .line 2301473
    move-object v0, v1

    .line 2301474
    if-eqz v0, :cond_7

    .line 2301475
    const v1, 0x7f0815d9

    invoke-virtual {v0}, LX/Fux;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, p3, v0}, LX/FvS;->a(LX/FvS;ILandroid/view/View;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2301476
    :cond_7
    invoke-virtual {p2}, LX/BQ1;->x()Z

    move-result v0

    if-nez v0, :cond_8

    .line 2301477
    iget-object v0, p0, LX/FvS;->c:LX/0iA;

    const-string v1, "3876"

    const-class v2, LX/3km;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3km;

    .line 2301478
    const/4 v1, 0x1

    .line 2301479
    iput-boolean v1, v0, LX/3km;->a:Z

    .line 2301480
    iget-object v1, p0, LX/FvS;->c:LX/0iA;

    sget-object v2, LX/FvS;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v3, LX/3km;

    invoke-virtual {v1, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    check-cast v1, LX/3km;

    .line 2301481
    const/4 v2, 0x0

    .line 2301482
    iput-boolean v2, v0, LX/3km;->a:Z

    .line 2301483
    move-object v0, v1

    .line 2301484
    if-eqz v0, :cond_8

    .line 2301485
    const v1, 0x7f08161a

    invoke-virtual {v0}, LX/3km;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, p3, v0}, LX/FvS;->a(LX/FvS;ILandroid/view/View;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2301486
    :cond_8
    iget-object v0, p0, LX/FvS;->c:LX/0iA;

    const-string v1, "3887"

    const-class v2, LX/3kq;

    invoke-virtual {v0, v1, v2}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v0

    check-cast v0, LX/3kq;

    .line 2301487
    const/4 v1, 0x1

    .line 2301488
    iput-boolean v1, v0, LX/3kq;->a:Z

    .line 2301489
    iget-object v1, p0, LX/FvS;->c:LX/0iA;

    sget-object v2, LX/FvS;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v3, LX/3kq;

    invoke-virtual {v1, v2, v3}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    check-cast v1, LX/3kq;

    .line 2301490
    const/4 v2, 0x0

    .line 2301491
    iput-boolean v2, v0, LX/3kq;->a:Z

    .line 2301492
    move-object v0, v1

    .line 2301493
    if-eqz v0, :cond_9

    .line 2301494
    const v1, 0x7f08161d

    invoke-virtual {v0}, LX/3kq;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, p3, v0}, LX/FvS;->a(LX/FvS;ILandroid/view/View;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2301495
    :cond_9
    iget-object v3, p0, LX/FvS;->c:LX/0iA;

    const-string v4, "3336"

    const-class v5, LX/3kb;

    invoke-virtual {v3, v4, v5}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v3

    check-cast v3, LX/3kb;

    .line 2301496
    iget-object v7, p2, LX/BQ1;->f:LX/BQ0;

    .line 2301497
    iget-object v9, v7, LX/BQ0;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfilePhotoFieldsModel;

    if-eqz v9, :cond_a

    iget-object v9, v7, LX/BQ0;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfilePhotoFieldsModel;

    invoke-virtual {v9}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfilePhotoFieldsModel;->d()J

    move-result-wide v9

    :goto_3
    move-wide v7, v9

    .line 2301498
    move-wide v5, v7

    .line 2301499
    iput-wide v5, v3, LX/3kb;->a:J

    .line 2301500
    const/4 v4, 0x1

    .line 2301501
    iput-boolean v4, v3, LX/3kb;->c:Z

    .line 2301502
    iget-object v4, p0, LX/FvS;->c:LX/0iA;

    sget-object v5, LX/FvS;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class v6, LX/3kb;

    invoke-virtual {v4, v5, v6}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v4

    check-cast v4, LX/3kb;

    .line 2301503
    const/4 v5, 0x0

    .line 2301504
    iput-boolean v5, v3, LX/3kb;->c:Z

    .line 2301505
    const-wide/16 v5, 0x0

    .line 2301506
    iput-wide v5, v3, LX/3kb;->a:J

    .line 2301507
    move-object v0, v4

    .line 2301508
    if-eqz v0, :cond_0

    .line 2301509
    const v1, 0x7f081575

    invoke-virtual {v0}, LX/3kb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v1, p3, v0}, LX/FvS;->a(LX/FvS;ILandroid/view/View;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    const-wide/16 v9, 0x0

    goto :goto_3
.end method
