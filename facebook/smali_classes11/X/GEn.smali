.class public LX/GEn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GEW",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;",
        "Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/GJo;


# direct methods
.method public constructor <init>(LX/GJo;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2332079
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2332080
    iput-object p1, p0, LX/GEn;->a:LX/GJo;

    .line 2332081
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2332082
    const v0, 0x7f030063

    return v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 2

    .prologue
    .line 2332083
    check-cast p1, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    .line 2332084
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v0

    .line 2332085
    sget-object v1, LX/GGB;->ACTIVE:LX/GGB;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/GGB;->PENDING:LX/GGB;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/GHg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesViewController",
            "<",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesIncreaseBudgetView;",
            "Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2332086
    iget-object v0, p0, LX/GEn;->a:LX/GJo;

    return-object v0
.end method

.method public final c()LX/8wK;
    .locals 1

    .prologue
    .line 2332087
    sget-object v0, LX/8wK;->BUDGET:LX/8wK;

    return-object v0
.end method
