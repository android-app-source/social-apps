.class public LX/GER;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/2U3;

.field private final b:LX/0tX;

.field private final c:LX/1Ck;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/2U3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2331681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2331682
    iput-object p1, p0, LX/GER;->b:LX/0tX;

    .line 2331683
    iput-object p2, p0, LX/GER;->c:LX/1Ck;

    .line 2331684
    iput-object p3, p0, LX/GER;->a:LX/2U3;

    .line 2331685
    return-void
.end method

.method public static a(LX/0QB;)LX/GER;
    .locals 6

    .prologue
    .line 2331686
    const-class v1, LX/GER;

    monitor-enter v1

    .line 2331687
    :try_start_0
    sget-object v0, LX/GER;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2331688
    sput-object v2, LX/GER;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2331689
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2331690
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2331691
    new-instance p0, LX/GER;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {v0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v5

    check-cast v5, LX/2U3;

    invoke-direct {p0, v3, v4, v5}, LX/GER;-><init>(LX/0tX;LX/1Ck;LX/2U3;)V

    .line 2331692
    move-object v0, p0

    .line 2331693
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2331694
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GER;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2331695
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2331696
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;IILX/GDc;)V
    .locals 4

    .prologue
    .line 2331697
    new-instance v0, LX/ACO;

    invoke-direct {v0}, LX/ACO;-><init>()V

    move-object v0, v0

    .line 2331698
    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "page_id"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "duration"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "total_budget"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/ACO;

    .line 2331699
    iget-object v1, p0, LX/GER;->c:LX/1Ck;

    sget-object v2, LX/GEQ;->SHOW_CHECKOUT:LX/GEQ;

    iget-object v3, p0, LX/GER;->b:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v3, LX/GEP;

    invoke-direct {v3, p0, p5}, LX/GEP;-><init>(LX/GER;LX/GDc;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2331700
    return-void
.end method
