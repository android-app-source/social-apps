.class public LX/Go0;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/Go0;


# instance fields
.field public a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/GoE;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0ad;

.field private final c:LX/0Zb;

.field public d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0lF;

.field public g:LX/0SG;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0SG;LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2393962
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2393963
    iput-object p1, p0, LX/Go0;->c:LX/0Zb;

    .line 2393964
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Go0;->d:Ljava/util/Map;

    .line 2393965
    iput-object p2, p0, LX/Go0;->g:LX/0SG;

    .line 2393966
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Go0;->a:Ljava/util/Map;

    .line 2393967
    iput-object p3, p0, LX/Go0;->b:LX/0ad;

    .line 2393968
    return-void
.end method

.method public static a(LX/0QB;)LX/Go0;
    .locals 6

    .prologue
    .line 2393912
    sget-object v0, LX/Go0;->h:LX/Go0;

    if-nez v0, :cond_1

    .line 2393913
    const-class v1, LX/Go0;

    monitor-enter v1

    .line 2393914
    :try_start_0
    sget-object v0, LX/Go0;->h:LX/Go0;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2393915
    if-eqz v2, :cond_0

    .line 2393916
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2393917
    new-instance p0, LX/Go0;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v5

    check-cast v5, LX/0ad;

    invoke-direct {p0, v3, v4, v5}, LX/Go0;-><init>(LX/0Zb;LX/0SG;LX/0ad;)V

    .line 2393918
    move-object v0, p0

    .line 2393919
    sput-object v0, LX/Go0;->h:LX/Go0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2393920
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2393921
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2393922
    :cond_1
    sget-object v0, LX/Go0;->h:LX/Go0;

    return-object v0

    .line 2393923
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2393924
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/String;Ljava/util/Map;Z)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2393948
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2393949
    :goto_0
    return-void

    .line 2393950
    :cond_0
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2393951
    const-string v1, "instant_shopping"

    .line 2393952
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2393953
    invoke-virtual {v0, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2393954
    iget-object v1, p0, LX/Go0;->d:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2393955
    iget-object v1, p0, LX/Go0;->e:Ljava/util/Map;

    if-eqz v1, :cond_1

    .line 2393956
    iget-object v1, p0, LX/Go0;->e:Ljava/util/Map;

    invoke-virtual {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2393957
    :cond_1
    iget-object v1, p0, LX/Go0;->f:LX/0lF;

    if-eqz v1, :cond_2

    .line 2393958
    const-string v1, "tracking"

    iget-object v2, p0, LX/Go0;->f:LX/0lF;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2393959
    :cond_2
    if-eqz p2, :cond_3

    .line 2393960
    invoke-virtual {v0, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/util/Map;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2393961
    :cond_3
    iget-object v1, p0, LX/Go0;->c:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/GoE;)V
    .locals 10

    .prologue
    .line 2393931
    iget-object v4, p0, LX/Go0;->a:Ljava/util/Map;

    invoke-interface {v4, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2393932
    iget-object v4, p0, LX/Go0;->a:Ljava/util/Map;

    invoke-interface {v4, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 2393933
    iget-object v5, p0, LX/Go0;->b:LX/0ad;

    sget v6, LX/CHN;->i:I

    const/16 v7, 0x3c

    invoke-interface {v5, v6, v7}, LX/0ad;->a(II)I

    move-result v5

    int-to-long v6, v5

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    .line 2393934
    iget-object v5, p0, LX/Go0;->g:LX/0SG;

    invoke-interface {v5}, LX/0SG;->a()J

    move-result-wide v8

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long v4, v8, v4

    cmp-long v4, v4, v6

    if-gez v4, :cond_1

    .line 2393935
    const/4 v4, 0x1

    .line 2393936
    :goto_0
    move v0, v4

    .line 2393937
    if-eqz v0, :cond_0

    .line 2393938
    :goto_1
    return-void

    .line 2393939
    :cond_0
    iget-object v0, p0, LX/Go0;->a:Ljava/util/Map;

    iget-object v1, p0, LX/Go0;->g:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2393940
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2393941
    const-string v1, "element_type"

    .line 2393942
    iget-object v2, p1, LX/GoE;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2393943
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2393944
    const-string v1, "logging_token"

    .line 2393945
    iget-object v2, p1, LX/GoE;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2393946
    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2393947
    const-string v1, "instant_shopping_element_impression"

    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, LX/Go0;->a(Ljava/lang/String;Ljava/util/Map;Z)V

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final a(LX/GoE;Ljava/util/Map;)V
    .locals 2
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/GoE;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2393929
    const-string v0, "instant_shopping_element_click"

    new-instance v1, LX/Gnz;

    invoke-direct {v1, p0, p1, p2}, LX/Gnz;-><init>(LX/Go0;LX/GoE;Ljava/util/Map;)V

    invoke-virtual {p0, v0, v1}, LX/Go0;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2393930
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2393927
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, LX/Go0;->a(Ljava/lang/String;Ljava/util/Map;Z)V

    .line 2393928
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/util/Map;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2393925
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, LX/Go0;->a(Ljava/lang/String;Ljava/util/Map;Z)V

    .line 2393926
    return-void
.end method
