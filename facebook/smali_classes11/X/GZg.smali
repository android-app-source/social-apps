.class public LX/GZg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# instance fields
.field public a:LX/GZj;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/01T;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2367076
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2367077
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 11

    .prologue
    const-wide/16 v4, -0x1

    const/4 v3, 0x0

    .line 2367078
    iget-object v0, p0, LX/GZg;->a:LX/GZj;

    invoke-virtual {v0}, LX/GZj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GZg;->b:LX/01T;

    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne v0, v1, :cond_0

    .line 2367079
    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {p1, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    const-string v2, "arg_init_product_id"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "product_ref_type"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v4, "product_ref_id"

    invoke-virtual {p1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v4, "1"

    const-string v7, "hide_page_header"

    invoke-virtual {p1, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    move v4, v3

    invoke-static/range {v0 .. v7}, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->a(JLjava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Z)Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    move-result-object v0

    .line 2367080
    :goto_0
    return-object v0

    .line 2367081
    :cond_0
    const-string v0, "com.facebook.katana.profile.id"

    invoke-virtual {p1, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 2367082
    const-string v0, "arg_init_product_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2367083
    const-string v0, "extra_finish_on_launch_edit_shop"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 2367084
    const-string v0, "product_ref_type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2367085
    instance-of v1, v0, LX/7iP;

    if-eqz v1, :cond_1

    .line 2367086
    check-cast v0, LX/7iP;

    .line 2367087
    :goto_1
    move-object v9, v0

    .line 2367088
    :try_start_0
    const-string v0, "product_ref_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    :goto_2
    move v8, v3

    .line 2367089
    invoke-static/range {v4 .. v10}, Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;->a(JLjava/lang/String;ZZLX/7iP;Ljava/lang/Long;)Lcom/facebook/commerce/storefront/fragments/StorefrontFragment;

    move-result-object v0

    goto :goto_0

    .line 2367090
    :catch_0
    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    goto :goto_2

    .line 2367091
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 2367092
    invoke-static {v1}, LX/7iR;->a(I)LX/7iP;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_1

    .line 2367093
    :catch_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/7iP;->getFromValue(Ljava/lang/String;)LX/7iP;

    move-result-object v0

    goto :goto_1
.end method
