.class public final enum LX/Fyz;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Fyz;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Fyz;

.field public static final enum COLLAPSED_ITEM:LX/Fyz;

.field public static final enum EXPANDED_PROFILE_QUESTION_ITEM:LX/Fyz;

.field public static final enum OVERFLOW_LINK:LX/Fyz;

.field public static final enum UNKNOWN:LX/Fyz;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2307326
    new-instance v0, LX/Fyz;

    const-string v1, "COLLAPSED_ITEM"

    invoke-direct {v0, v1, v2}, LX/Fyz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fyz;->COLLAPSED_ITEM:LX/Fyz;

    .line 2307327
    new-instance v0, LX/Fyz;

    const-string v1, "EXPANDED_PROFILE_QUESTION_ITEM"

    invoke-direct {v0, v1, v3}, LX/Fyz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fyz;->EXPANDED_PROFILE_QUESTION_ITEM:LX/Fyz;

    .line 2307328
    new-instance v0, LX/Fyz;

    const-string v1, "OVERFLOW_LINK"

    invoke-direct {v0, v1, v4}, LX/Fyz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fyz;->OVERFLOW_LINK:LX/Fyz;

    .line 2307329
    new-instance v0, LX/Fyz;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v5}, LX/Fyz;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fyz;->UNKNOWN:LX/Fyz;

    .line 2307330
    const/4 v0, 0x4

    new-array v0, v0, [LX/Fyz;

    sget-object v1, LX/Fyz;->COLLAPSED_ITEM:LX/Fyz;

    aput-object v1, v0, v2

    sget-object v1, LX/Fyz;->EXPANDED_PROFILE_QUESTION_ITEM:LX/Fyz;

    aput-object v1, v0, v3

    sget-object v1, LX/Fyz;->OVERFLOW_LINK:LX/Fyz;

    aput-object v1, v0, v4

    sget-object v1, LX/Fyz;->UNKNOWN:LX/Fyz;

    aput-object v1, v0, v5

    sput-object v0, LX/Fyz;->$VALUES:[LX/Fyz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2307331
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Fyz;
    .locals 1

    .prologue
    .line 2307332
    const-class v0, LX/Fyz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Fyz;

    return-object v0
.end method

.method public static values()[LX/Fyz;
    .locals 1

    .prologue
    .line 2307333
    sget-object v0, LX/Fyz;->$VALUES:[LX/Fyz;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Fyz;

    return-object v0
.end method
