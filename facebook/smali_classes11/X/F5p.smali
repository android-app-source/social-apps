.class public final LX/F5p;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2199203
    const-class v1, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel;

    const v0, -0x265fe98c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "FetchGroupCreationSuggestions"

    const-string v6, "7c9949603b01f0bdd39be06244008e2e"

    const-string v7, "viewer"

    const-string v8, "10155252904846729"

    const-string v9, "10155259087521729"

    .line 2199204
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2199205
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2199206
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2199211
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2199212
    sparse-switch v0, :sswitch_data_0

    .line 2199213
    :goto_0
    return-object p1

    .line 2199214
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2199215
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x5ad4e5a4 -> :sswitch_0
        0x6771e9f5 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2199207
    const/4 v1, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_1

    .line 2199208
    :goto_1
    return v0

    .line 2199209
    :pswitch_0
    const-string v2, "1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v0

    goto :goto_0

    .line 2199210
    :pswitch_1
    invoke-static {p2}, LX/0wE;->a(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x31
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch
.end method
