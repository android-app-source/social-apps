.class public final LX/Gj1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/GjL;

.field public final synthetic b:I

.field public final synthetic c:Landroid/view/ViewGroup;

.field public final synthetic d:I

.field public final synthetic e:Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;LX/GjL;ILandroid/view/ViewGroup;I)V
    .locals 0

    .prologue
    .line 2386848
    iput-object p1, p0, LX/Gj1;->e:Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;

    iput-object p2, p0, LX/Gj1;->a:LX/GjL;

    iput p3, p0, LX/Gj1;->b:I

    iput-object p4, p0, LX/Gj1;->c:Landroid/view/ViewGroup;

    iput p5, p0, LX/Gj1;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2386849
    iget-object v0, p0, LX/Gj1;->e:Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;

    iget-object v1, p0, LX/Gj1;->a:LX/GjL;

    .line 2386850
    iput-object v1, v0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->A:LX/Gik;

    .line 2386851
    iget-object v0, p0, LX/Gj1;->e:Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;

    iget-object v1, p0, LX/Gj1;->a:LX/GjL;

    .line 2386852
    invoke-static {v0, v1}, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->a$redex0(Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;Landroid/view/View;)V

    .line 2386853
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 2386837
    iget-object v0, p0, LX/Gj1;->a:LX/GjL;

    invoke-virtual {v0}, LX/GjL;->b()V

    .line 2386838
    iget v0, p0, LX/Gj1;->b:I

    if-lez v0, :cond_0

    iget-object v0, p0, LX/Gj1;->e:Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;

    iget-object v0, v0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->x:Ljava/util/ArrayList;

    iget v1, p0, LX/Gj1;->b:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GjL;

    invoke-virtual {v0}, LX/GjL;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2386839
    iget-object v0, p0, LX/Gj1;->c:Landroid/view/ViewGroup;

    iget v1, p0, LX/Gj1;->d:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2386840
    iget-object v0, p0, LX/Gj1;->a:LX/GjL;

    invoke-virtual {v0, v3}, LX/GjL;->setVisibility(I)V

    .line 2386841
    :cond_0
    iget v0, p0, LX/Gj1;->b:I

    const/4 v1, 0x7

    if-ge v0, v1, :cond_1

    .line 2386842
    iget-object v0, p0, LX/Gj1;->e:Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;

    iget-object v0, v0, Lcom/facebook/greetingcards/create/GreetingCardEditorActivity;->x:Ljava/util/ArrayList;

    iget v1, p0, LX/Gj1;->b:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GjL;

    .line 2386843
    invoke-virtual {v0}, LX/GjL;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2386844
    invoke-virtual {v0}, LX/GjL;->b()V

    .line 2386845
    iget-object v1, p0, LX/Gj1;->c:Landroid/view/ViewGroup;

    iget v2, p0, LX/Gj1;->d:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2386846
    invoke-virtual {v0, v3}, LX/GjL;->setVisibility(I)V

    .line 2386847
    :cond_1
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2386833
    iget v0, p0, LX/Gj1;->b:I

    const/4 v1, 0x7

    if-ge v0, v1, :cond_0

    .line 2386834
    iget-object v0, p0, LX/Gj1;->c:Landroid/view/ViewGroup;

    iget v1, p0, LX/Gj1;->d:I

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2386835
    iget-object v0, p0, LX/Gj1;->c:Landroid/view/ViewGroup;

    iget v1, p0, LX/Gj1;->d:I

    add-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2386836
    :cond_0
    return-void
.end method
