.class public final LX/Fg1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Fee;


# instance fields
.field public final synthetic a:LX/Fg3;


# direct methods
.method public constructor <init>(LX/Fg3;)V
    .locals 0

    .prologue
    .line 2269208
    iput-object p1, p0, LX/Fg1;->a:LX/Fg3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0Px;LX/0am;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/0am",
            "<",
            "LX/0us;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2269209
    invoke-virtual {p2}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2269210
    iget-object v1, p0, LX/Fg1;->a:LX/Fg3;

    invoke-virtual {p2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0us;

    invoke-interface {v0}, LX/0us;->p_()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 2269211
    iput-object v0, v1, LX/Fg3;->l:LX/0am;

    .line 2269212
    :cond_0
    invoke-virtual {p1}, LX/0Px;->size()I

    .line 2269213
    iget-object v0, p0, LX/Fg1;->a:LX/Fg3;

    iget-object v0, v0, LX/Fg3;->i:LX/Fee;

    invoke-virtual {p1}, LX/0Px;->reverse()LX/0Px;

    move-result-object v1

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, LX/Fee;->a(LX/0Px;LX/0am;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2269214
    return-void
.end method

.method public final a(LX/7C4;)V
    .locals 1

    .prologue
    .line 2269215
    iget-object v0, p0, LX/Fg1;->a:LX/Fg3;

    iget-object v0, v0, LX/Fg3;->i:LX/Fee;

    invoke-interface {v0, p1}, LX/Fee;->a(LX/7C4;)V

    .line 2269216
    return-void
.end method

.method public final b(LX/0Px;LX/0am;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "LX/0am",
            "<",
            "LX/0us;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2269217
    invoke-virtual {p1}, LX/0Px;->size()I

    .line 2269218
    iget-object v0, p0, LX/Fg1;->a:LX/Fg3;

    iget-object v0, v0, LX/Fg3;->i:LX/Fee;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, LX/Fee;->b(LX/0Px;LX/0am;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2269219
    return-void
.end method
