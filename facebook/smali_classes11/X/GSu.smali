.class public final LX/GSu;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GSw;


# direct methods
.method public constructor <init>(LX/GSw;)V
    .locals 0

    .prologue
    .line 2354916
    iput-object p1, p0, LX/GSu;->a:LX/GSw;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2354917
    sget-object v0, LX/GSw;->b:Ljava/lang/Class;

    const-string v1, "Fetch block list failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2354918
    iget-object v0, p0, LX/GSu;->a:LX/GSw;

    iget-object v0, v0, LX/GSw;->f:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p0, LX/GSu;->a:LX/GSw;

    invoke-virtual {v1}, LX/GSw;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08006f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/GSt;

    invoke-direct {v2, p0}, LX/GSt;-><init>(LX/GSu;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 2354919
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2354892
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 2354893
    iget-object v0, p0, LX/GSu;->a:LX/GSw;

    iget-object v0, v0, LX/GSw;->i:LX/GRS;

    .line 2354894
    iget-object v1, v0, LX/GRS;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 2354895
    iget-object v0, p0, LX/GSu;->a:LX/GSw;

    iget v0, v0, LX/GSw;->h:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 2354896
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2354897
    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel;

    .line 2354898
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2354899
    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const v3, -0x3171f5c1

    invoke-static {v1, v0, v2, v3}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2354900
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2354901
    const-class v4, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel$ApplicationRequestBlockedApplicationsModel$EdgesModel$NodeModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel$ApplicationRequestBlockedApplicationsModel$EdgesModel$NodeModel;

    .line 2354902
    iget-object v3, p0, LX/GSu;->a:LX/GSw;

    iget-object v3, v3, LX/GSw;->i:LX/GRS;

    new-instance v4, LX/GRR;

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel$ApplicationRequestBlockedApplicationsModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel$ApplicationRequestBlockedApplicationsModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedAppsQueryModel$ApplicationRequestBlockedApplicationsModel$EdgesModel$NodeModel;->d()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v5, v6, v0, v9}, LX/GRR;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v3, v4}, LX/GRS;->a(LX/GRR;)V

    goto :goto_1

    .line 2354903
    :cond_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    .line 2354904
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2354905
    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedUsersQueryModel;

    .line 2354906
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2354907
    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedUsersQueryModel;->a()Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedUsersQueryModel$ApplicationRequestBlockedUsersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedUsersQueryModel$ApplicationRequestBlockedUsersModel;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v1, v2

    .line 2354908
    :goto_2
    if-ge v1, v4, :cond_2

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedUsersQueryModel$ApplicationRequestBlockedUsersModel$EdgesModel;

    .line 2354909
    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedUsersQueryModel$ApplicationRequestBlockedUsersModel$EdgesModel;->a()Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedUsersQueryModel$ApplicationRequestBlockedUsersModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 2354910
    iget-object v5, p0, LX/GSu;->a:LX/GSw;

    iget-object v5, v5, LX/GSw;->i:LX/GRS;

    new-instance v6, LX/GRR;

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedUsersQueryModel$ApplicationRequestBlockedUsersModel$EdgesModel$NodeModel;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedUsersQueryModel$ApplicationRequestBlockedUsersModel$EdgesModel$NodeModel;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesBlockedListQueriesModels$AppInviteBlockedUsersQueryModel$ApplicationRequestBlockedUsersModel$EdgesModel$NodeModel;->d()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v7, v8, v0, v9}, LX/GRR;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v5, v6}, LX/GRS;->a(LX/GRR;)V

    .line 2354911
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2354912
    :cond_2
    iget-object v0, p0, LX/GSu;->a:LX/GSw;

    iget-object v0, v0, LX/GSw;->j:LX/GRP;

    const v1, 0x28a6fd7a

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2354913
    iget-object v0, p0, LX/GSu;->a:LX/GSw;

    iget-object v0, v0, LX/GSw;->f:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2354914
    iget-object v0, p0, LX/GSu;->a:LX/GSw;

    iget-object v0, v0, LX/GSw;->g:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    .line 2354915
    return-void
.end method
