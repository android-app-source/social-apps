.class public LX/FBl;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2209712
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(JLandroid/app/Activity;LX/0ad;Landroid/content/ComponentName;Landroid/content/ComponentName;LX/0Uh;LX/17X;LX/2lA;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 2209713
    const-wide v0, 0x8bb78869L

    cmp-long v0, v0, p0

    if-nez v0, :cond_0

    .line 2209714
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    sget-object v1, LX/0c0;->Live:LX/0c0;

    sget-short v2, LX/347;->C:S

    const/4 v3, 0x0

    invoke-interface {p3, v1, v2, v3}, LX/0ad;->a(LX/0c0;SZ)Z

    move-result v1

    if-eqz v1, :cond_5

    :goto_0
    invoke-virtual {v0, p4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 2209715
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->EVENTS_DASHBOARD_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2209716
    move-object v0, v0

    .line 2209717
    :goto_1
    return-object v0

    .line 2209718
    :cond_0
    const-wide v0, 0x21531ffed86f8L

    cmp-long v0, v0, p0

    if-nez v0, :cond_2

    .line 2209719
    const/16 v0, 0x21c

    const/4 v1, 0x0

    invoke-virtual {p6, v0, v1}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2209720
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, p5}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    .line 2209721
    invoke-virtual {p5}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2209722
    const-string v1, "target_fragment"

    sget-object v2, LX/0cQ;->SAVED_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2209723
    const-string v1, "extra_referer"

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->MOBILE_SAVED_BOOKMARK:Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLCollectionCurationReferrerTag;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2209724
    invoke-static {p6}, LX/79u;->a(LX/0Uh;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2209725
    const-string v1, "inflate_fragment_before_animation"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2209726
    :cond_1
    :goto_2
    move-object v0, v0

    .line 2209727
    goto :goto_1

    .line 2209728
    :cond_2
    const-wide v0, 0x195709b26L

    cmp-long v0, v0, p0

    if-nez v0, :cond_4

    .line 2209729
    invoke-virtual {p8}, LX/2lA;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2209730
    const-string v0, "fb-ama://?ref=fb_bm_android"

    move-object v0, v0

    .line 2209731
    invoke-static {v0}, LX/2lA;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2209732
    :cond_3
    const/4 v0, 0x0

    .line 2209733
    invoke-virtual {p8}, LX/2lA;->d()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2209734
    :goto_3
    move v0, v0

    .line 2209735
    if-eqz v0, :cond_4

    .line 2209736
    sget-object v0, LX/0ax;->t:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "BOOKMARK"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "/"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2209737
    invoke-virtual {p7, p2, v0}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2209738
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_5
    move-object p4, p5

    .line 2209739
    goto/16 :goto_0

    .line 2209740
    :cond_6
    const-string v0, "https://m.facebook.com/saved"

    invoke-virtual {p7, p2, v0}, LX/17X;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_2

    :cond_7
    iget-object v1, p8, LX/2lA;->a:LX/0ad;

    sget-short v2, LX/ADP;->b:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_3
.end method
