.class public final LX/Fl5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserLeaveMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;)V
    .locals 0

    .prologue
    .line 2279389
    iput-object p1, p0, LX/Fl5;->a:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2279392
    iget-object v0, p0, LX/Fl5;->a:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->l:LX/0Zb;

    iget-object v1, p0, LX/Fl5;->a:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->I:Ljava/lang/String;

    .line 2279393
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "fundraiser_leave_mutation_failure"

    invoke-direct {p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "social_good"

    .line 2279394
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2279395
    move-object p0, p0

    .line 2279396
    const-string p1, "fundraiser_campaign_id"

    invoke-virtual {p0, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p0

    move-object v1, p0

    .line 2279397
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2279398
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2279390
    iget-object v0, p0, LX/Fl5;->a:Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;

    invoke-virtual {v0}, Lcom/facebook/socialgood/fundraiserpage/FundraiserPageFragment;->e()V

    .line 2279391
    return-void
.end method
