.class public final LX/GEC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;",
        ">;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GEF;


# direct methods
.method public constructor <init>(LX/GEF;)V
    .locals 0

    .prologue
    .line 2331175
    iput-object p1, p0, LX/GEC;->a:LX/GEF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2331176
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v6, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 2331177
    if-eqz p1, :cond_0

    .line 2331178
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331179
    if-eqz v0, :cond_0

    .line 2331180
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331181
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_0
    move-object v1, v3

    .line 2331182
    :goto_0
    if-nez p1, :cond_3

    move-object v2, v3

    .line 2331183
    :goto_1
    if-eqz p1, :cond_1

    .line 2331184
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331185
    if-eqz v0, :cond_1

    .line 2331186
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331187
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    if-nez v0, :cond_4

    .line 2331188
    :cond_1
    :goto_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331189
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 2331190
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331191
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;->o()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v4, v0

    .line 2331192
    :goto_3
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331193
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;->n()Ljava/lang/String;

    move-result-object v5

    .line 2331194
    const-string v8, ""

    .line 2331195
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331196
    if-eqz v0, :cond_7

    .line 2331197
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331198
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;->k()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 2331199
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331200
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;->k()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-nez v0, :cond_7

    .line 2331201
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331202
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;->k()LX/2uF;

    move-result-object v0

    invoke-virtual {v0, v7}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v9, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2331203
    invoke-virtual {v9, v0, v7}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_6

    move v0, v6

    :goto_4
    if-eqz v0, :cond_8

    .line 2331204
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331205
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;->k()LX/2uF;

    move-result-object v0

    invoke-virtual {v0, v7}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v8, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v8, v0, v7}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v8, v0, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    .line 2331206
    :goto_5
    new-instance v0, LX/GGO;

    invoke-direct/range {v0 .. v6}, LX/GGO;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 2331207
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331208
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v1

    goto/16 :goto_0

    .line 2331209
    :cond_3
    iget-object v2, p0, LX/GEC;->a:LX/GEF;

    .line 2331210
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331211
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;

    invoke-static {v2, v0}, LX/GEF;->a$redex0(LX/GEF;Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-result-object v2

    goto/16 :goto_1

    .line 2331212
    :cond_4
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331213
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchAdminInfoQueryModels$FetchAdminInfoQueryModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    goto/16 :goto_2

    .line 2331214
    :cond_5
    const-string v4, ""

    goto/16 :goto_3

    :cond_6
    move v0, v7

    .line 2331215
    goto :goto_4

    :cond_7
    move v0, v7

    goto :goto_4

    :cond_8
    move-object v6, v8

    goto :goto_5
.end method
