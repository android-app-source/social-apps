.class public LX/Fbp;
.super LX/FbI;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2261169
    invoke-direct {p0}, LX/FbI;-><init>()V

    .line 2261170
    return-void
.end method

.method public static a(LX/0QB;)LX/Fbp;
    .locals 3

    .prologue
    .line 2261104
    const-class v1, LX/Fbp;

    monitor-enter v1

    .line 2261105
    :try_start_0
    sget-object v0, LX/Fbp;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2261106
    sput-object v2, LX/Fbp;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2261107
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2261108
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2261109
    new-instance v0, LX/Fbp;

    invoke-direct {v0}, LX/Fbp;-><init>()V

    .line 2261110
    move-object v0, v0

    .line 2261111
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2261112
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fbp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2261113
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2261114
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;)LX/8dV;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261115
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2261116
    if-nez v0, :cond_0

    .line 2261117
    const/4 v0, 0x0

    .line 2261118
    :goto_0
    return-object v0

    .line 2261119
    :cond_0
    invoke-static {v0}, LX/A0T;->a(Lcom/facebook/graphql/model/GraphQLNode;)LX/8d0;

    move-result-object v1

    .line 2261120
    new-instance v2, LX/8dX;

    invoke-direct {v2}, LX/8dX;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v3

    .line 2261121
    iput-object v3, v2, LX/8dX;->L:Ljava/lang/String;

    .line 2261122
    move-object v2, v2

    .line 2261123
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->fJ()Ljava/lang/String;

    move-result-object v3

    .line 2261124
    iput-object v3, v2, LX/8dX;->ad:Ljava/lang/String;

    .line 2261125
    move-object v2, v2

    .line 2261126
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->eM()Z

    move-result v3

    .line 2261127
    iput-boolean v3, v2, LX/8dX;->W:Z

    .line 2261128
    move-object v2, v2

    .line 2261129
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->cc()Z

    move-result v3

    .line 2261130
    iput-boolean v3, v2, LX/8dX;->u:Z

    .line 2261131
    move-object v2, v2

    .line 2261132
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aY()Z

    move-result v3

    .line 2261133
    iput-boolean v3, v2, LX/8dX;->l:Z

    .line 2261134
    move-object v2, v2

    .line 2261135
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->aW()Z

    move-result v3

    .line 2261136
    iput-boolean v3, v2, LX/8dX;->j:Z

    .line 2261137
    move-object v2, v2

    .line 2261138
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->lo()Z

    move-result v3

    .line 2261139
    iput-boolean v3, v2, LX/8dX;->aX:Z

    .line 2261140
    move-object v2, v2

    .line 2261141
    invoke-interface {v1}, LX/8d0;->e()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->a(Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v3

    .line 2261142
    iput-object v3, v2, LX/8dX;->aw:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    .line 2261143
    move-object v2, v2

    .line 2261144
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->bj()LX/0Px;

    move-result-object v3

    .line 2261145
    iput-object v3, v2, LX/8dX;->m:LX/0Px;

    .line 2261146
    move-object v2, v2

    .line 2261147
    invoke-interface {v1}, LX/8d0;->y()Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;->a(Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    move-result-object v3

    .line 2261148
    iput-object v3, v2, LX/8dX;->aj:Lcom/facebook/search/results/protocol/entity/SearchResultsPageModels$SearchResultsPageModel$PageLikersModel;

    .line 2261149
    move-object v2, v2

    .line 2261150
    invoke-interface {v1}, LX/8d0;->b()Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;->a(Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;)Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    move-result-object v3

    .line 2261151
    iput-object v3, v2, LX/8dX;->q:Lcom/facebook/search/results/protocol/common/SearchResultsSimpleCoverPhotoModels$SearchResultsSimpleCoverPhotoModel$CoverPhotoModel;

    .line 2261152
    move-object v2, v2

    .line 2261153
    invoke-interface {v1}, LX/8d0;->x()Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-result-object v1

    invoke-static {v1}, Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;->a(Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;)Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    move-result-object v1

    .line 2261154
    iput-object v1, v2, LX/8dX;->ai:Lcom/facebook/search/results/protocol/entity/SearchResultsPageCtaModels$SearchResultsPageCtaModel;

    .line 2261155
    move-object v1, v2

    .line 2261156
    new-instance v2, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v3, 0x2a731e0d

    invoke-direct {v2, v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2261157
    iput-object v2, v1, LX/8dX;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2261158
    move-object v1, v1

    .line 2261159
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->ku()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v0

    .line 2261160
    iput-object v0, v1, LX/8dX;->aW:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 2261161
    move-object v0, v1

    .line 2261162
    invoke-virtual {v0}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2261163
    new-instance v1, LX/8dV;

    invoke-direct {v1}, LX/8dV;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->fN_()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;

    move-result-object v2

    invoke-static {v2}, LX/FbX;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;

    move-result-object v2

    .line 2261164
    iput-object v2, v1, LX/8dV;->d:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;

    .line 2261165
    move-object v1, v1

    .line 2261166
    iput-object v0, v1, LX/8dV;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2261167
    move-object v0, v1

    .line 2261168
    goto/16 :goto_0
.end method
