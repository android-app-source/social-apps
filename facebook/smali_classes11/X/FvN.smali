.class public LX/FvN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FvM;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FvN;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2301252
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2301253
    return-void
.end method

.method public static a(LX/0QB;)LX/FvN;
    .locals 3

    .prologue
    .line 2301254
    sget-object v0, LX/FvN;->a:LX/FvN;

    if-nez v0, :cond_1

    .line 2301255
    const-class v1, LX/FvN;

    monitor-enter v1

    .line 2301256
    :try_start_0
    sget-object v0, LX/FvN;->a:LX/FvN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2301257
    if-eqz v2, :cond_0

    .line 2301258
    :try_start_1
    new-instance v0, LX/FvN;

    invoke-direct {v0}, LX/FvN;-><init>()V

    .line 2301259
    move-object v0, v0

    .line 2301260
    sput-object v0, LX/FvN;->a:LX/FvN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2301261
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2301262
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2301263
    :cond_1
    sget-object v0, LX/FvN;->a:LX/FvN;

    return-object v0

    .line 2301264
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2301265
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/BPA;)V
    .locals 0

    .prologue
    .line 2301248
    return-void
.end method

.method public final a(LX/BQ1;LX/G4q;)V
    .locals 0
    .param p1    # LX/BQ1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2301251
    return-void
.end method

.method public final a(Lcom/facebook/timeline/TimelineFragment;)V
    .locals 0
    .param p1    # Lcom/facebook/timeline/TimelineFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2301250
    return-void
.end method

.method public final b(LX/BPA;)V
    .locals 0

    .prologue
    .line 2301249
    return-void
.end method
