.class public LX/Ghx;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Ghv;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/Ghx;


# instance fields
.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ghz;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2385106
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Ghx;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Ghz;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2385107
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2385108
    iput-object p1, p0, LX/Ghx;->b:LX/0Ot;

    .line 2385109
    return-void
.end method

.method public static a(LX/0QB;)LX/Ghx;
    .locals 4

    .prologue
    .line 2385110
    sget-object v0, LX/Ghx;->c:LX/Ghx;

    if-nez v0, :cond_1

    .line 2385111
    const-class v1, LX/Ghx;

    monitor-enter v1

    .line 2385112
    :try_start_0
    sget-object v0, LX/Ghx;->c:LX/Ghx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2385113
    if-eqz v2, :cond_0

    .line 2385114
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2385115
    new-instance v3, LX/Ghx;

    const/16 p0, 0x2310

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Ghx;-><init>(LX/0Ot;)V

    .line 2385116
    move-object v0, v3

    .line 2385117
    sput-object v0, LX/Ghx;->c:LX/Ghx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2385118
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2385119
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2385120
    :cond_1
    sget-object v0, LX/Ghx;->c:LX/Ghx;

    return-object v0

    .line 2385121
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2385122
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2385123
    check-cast p2, LX/Ghw;

    .line 2385124
    iget-object v0, p0, LX/Ghx;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v1, p2, LX/Ghw;->a:LX/0Px;

    iget-object v0, p2, LX/Ghw;->b:[I

    check-cast v0, [I

    iget v2, p2, LX/Ghw;->c:I

    iget v3, p2, LX/Ghw;->d:I

    const/4 p2, 0x7

    const/4 p0, 0x2

    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 2385125
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v9}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const v5, 0x7f0a00d5

    invoke-interface {v4, v5}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v7

    move v5, v6

    .line 2385126
    :goto_0
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_2

    .line 2385127
    if-nez v5, :cond_0

    .line 2385128
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v8, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v9}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-interface {v4, v8}, LX/1Di;->a(F)LX/1Di;

    move-result-object v4

    invoke-interface {v7, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const v8, 0x7f0b22e0

    invoke-interface {v4, p2, v8}, LX/1Dh;->q(II)LX/1Dh;

    move-result-object v4

    const v8, 0x7f0b161d

    invoke-interface {v4, v6, v8}, LX/1Dh;->u(II)LX/1Dh;

    .line 2385129
    :goto_1
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 2385130
    :cond_0
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v5, v4, :cond_1

    .line 2385131
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v8, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v9}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v4, v8}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    aget v8, v0, v5

    invoke-interface {v4, v8}, LX/1Di;->g(I)LX/1Di;

    move-result-object v4

    const v8, 0x7f0b22df

    invoke-interface {v4, v6, v8}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v7, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const v8, 0x7f0b22e0

    invoke-interface {v4, p2, v8}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const v8, 0x7f0b161d

    invoke-interface {v4, p0, v8}, LX/1Dh;->u(II)LX/1Dh;

    goto :goto_1

    .line 2385132
    :cond_1
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    invoke-virtual {v1, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-virtual {v8, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v9}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/1ne;->q(I)LX/1ne;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/1ne;->n(I)LX/1ne;

    move-result-object v4

    sget-object v8, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    invoke-virtual {v4, v8}, LX/1ne;->a(Landroid/text/Layout$Alignment;)LX/1ne;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    aget v8, v0, v5

    invoke-interface {v4, v8}, LX/1Di;->g(I)LX/1Di;

    move-result-object v4

    const v8, 0x7f0b22e0

    invoke-interface {v4, p2, v8}, LX/1Di;->g(II)LX/1Di;

    move-result-object v4

    const v8, 0x7f0b22df

    invoke-interface {v4, v6, v8}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-interface {v7, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    goto/16 :goto_1

    .line 2385133
    :cond_2
    invoke-interface {v7}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2385134
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2385135
    invoke-static {}, LX/1dS;->b()V

    .line 2385136
    const/4 v0, 0x0

    return-object v0
.end method
