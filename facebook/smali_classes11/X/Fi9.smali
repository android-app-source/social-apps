.class public LX/Fi9;
.super LX/Fi8;
.source ""


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:LX/2Sf;

.field private final c:LX/FgS;

.field private final d:LX/0W9;

.field private e:LX/Cwb;

.field private f:LX/Cwb;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/2Sf;LX/FgS;LX/0W9;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2274297
    invoke-direct {p0}, LX/Fi8;-><init>()V

    .line 2274298
    sget-object v0, LX/Cwb;->NO_GROUP:LX/Cwb;

    iput-object v0, p0, LX/Fi9;->e:LX/Cwb;

    .line 2274299
    iput-object p1, p0, LX/Fi9;->a:Landroid/content/res/Resources;

    .line 2274300
    iput-object p2, p0, LX/Fi9;->b:LX/2Sf;

    .line 2274301
    iput-object p3, p0, LX/Fi9;->c:LX/FgS;

    .line 2274302
    iput-object p4, p0, LX/Fi9;->d:LX/0W9;

    .line 2274303
    return-void
.end method

.method private a(LX/CwI;Ljava/lang/String;)Lcom/facebook/search/model/KeywordTypeaheadUnit;
    .locals 3

    .prologue
    .line 2274304
    new-instance v0, LX/CwH;

    invoke-direct {v0}, LX/CwH;-><init>()V

    .line 2274305
    iput-object p2, v0, LX/CwH;->b:Ljava/lang/String;

    .line 2274306
    move-object v0, v0

    .line 2274307
    iget-object v1, p0, LX/Fi9;->a:Landroid/content/res/Resources;

    const v2, 0x7f08232d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2274308
    iput-object v1, v0, LX/CwH;->t:Ljava/lang/String;

    .line 2274309
    move-object v0, v0

    .line 2274310
    iput-object p2, v0, LX/CwH;->d:Ljava/lang/String;

    .line 2274311
    move-object v0, v0

    .line 2274312
    const-string v1, "content"

    .line 2274313
    iput-object v1, v0, LX/CwH;->e:Ljava/lang/String;

    .line 2274314
    move-object v0, v0

    .line 2274315
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2274316
    iput-object v1, v0, LX/CwH;->f:Ljava/lang/Boolean;

    .line 2274317
    move-object v0, v0

    .line 2274318
    iput-object p1, v0, LX/CwH;->l:LX/CwI;

    .line 2274319
    move-object v0, v0

    .line 2274320
    invoke-static {p2}, LX/7BG;->r(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2274321
    iput-object v1, v0, LX/CwH;->c:Ljava/lang/String;

    .line 2274322
    move-object v0, v0

    .line 2274323
    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/api/GraphSearchQuery;LX/7Hc;Lcom/facebook/search/model/TypeaheadUnit;LX/7HZ;)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            "LX/7HZ;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2274253
    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4}, Ljava/util/LinkedHashMap;-><init>()V

    .line 2274254
    iget-object v1, p0, LX/Fi9;->c:LX/FgS;

    invoke-virtual {v1}, LX/FgS;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Fi9;->d:LX/0W9;

    invoke-virtual {v2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    .line 2274255
    iget-object v1, p0, LX/Fi9;->f:LX/Cwb;

    if-eqz v1, :cond_0

    if-eqz p3, :cond_0

    iget-object v1, p0, LX/Fi9;->f:LX/Cwb;

    sget-object v2, LX/Cwb;->ENTITY:LX/Cwb;

    invoke-virtual {v1, v2}, LX/Cwb;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p3}, Lcom/facebook/search/model/TypeaheadUnit;->l()LX/Cwb;

    move-result-object v1

    sget-object v2, LX/Cwb;->KEYWORD:LX/Cwb;

    invoke-virtual {v1, v2}, LX/Cwb;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2274256
    invoke-virtual {p3}, Lcom/facebook/search/model/TypeaheadUnit;->l()LX/Cwb;

    move-result-object v1

    iput-object v1, p0, LX/Fi9;->e:LX/Cwb;

    .line 2274257
    :cond_0
    iget-object v1, p0, LX/Fi9;->e:LX/Cwb;

    sget-object v2, LX/Cwb;->NO_GROUP:LX/Cwb;

    invoke-virtual {v1, v2}, LX/Cwb;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2274258
    iget-object v1, p0, LX/Fi9;->e:LX/Cwb;

    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    invoke-interface {v4, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2274259
    :cond_1
    iget-object v1, p2, LX/7Hc;->b:LX/0Px;

    move-object v6, v1

    .line 2274260
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v3, v0

    move v2, v0

    :goto_0
    if-ge v3, v7, :cond_6

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/TypeaheadUnit;

    .line 2274261
    invoke-virtual {v0}, Lcom/facebook/search/model/TypeaheadUnit;->E()Z

    move-result v1

    if-nez v1, :cond_4

    .line 2274262
    invoke-virtual {v0}, Lcom/facebook/search/model/TypeaheadUnit;->l()LX/Cwb;

    move-result-object v8

    .line 2274263
    invoke-interface {v4, v8}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2274264
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    invoke-interface {v4, v8, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2274265
    :cond_2
    if-nez v2, :cond_5

    sget-object v1, LX/Cwb;->KEYWORD:LX/Cwb;

    invoke-virtual {v8, v1}, LX/Cwb;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    move-object v1, v0

    check-cast v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-virtual {v1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2274266
    const/4 v1, 0x1

    move v2, v1

    .line 2274267
    :cond_3
    :goto_1
    invoke-interface {v4, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Pz;

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2274268
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 2274269
    :cond_5
    if-eqz v2, :cond_3

    sget-object v1, LX/Cwb;->KEYWORD:LX/Cwb;

    invoke-virtual {v8, v1}, LX/Cwb;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object v1, v0

    check-cast v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-virtual {v1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object v1, v0

    check-cast v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-virtual {v1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->jB_()LX/CwF;

    move-result-object v1

    sget-object v9, LX/CwF;->keyword:LX/CwF;

    invoke-virtual {v1, v9}, LX/CwF;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_1

    .line 2274270
    :cond_6
    if-nez v2, :cond_8

    .line 2274271
    sget-object v0, LX/Cwb;->KEYWORD:LX/Cwb;

    invoke-interface {v4, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2274272
    sget-object v0, LX/Cwb;->KEYWORD:LX/Cwb;

    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2274273
    :cond_7
    sget-object v0, LX/Cwb;->KEYWORD:LX/Cwb;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pz;

    sget-object v1, LX/CwI;->ECHO:LX/CwI;

    invoke-direct {p0, v1, v5}, LX/Fi9;->a(LX/CwI;Ljava/lang/String;)Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2274274
    :cond_8
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2274275
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2274276
    :cond_9
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2274277
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cwb;

    .line 2274278
    new-instance v3, LX/Cwa;

    invoke-direct {v3}, LX/Cwa;-><init>()V

    .line 2274279
    iput-object v0, v3, LX/Cwa;->a:LX/Cwb;

    .line 2274280
    move-object v3, v3

    .line 2274281
    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Pz;

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2274282
    iput-object v0, v3, LX/Cwa;->b:LX/0Px;

    .line 2274283
    move-object v0, v3

    .line 2274284
    invoke-virtual {v0}, LX/Cwa;->a()LX/Cwc;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2274285
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2274286
    new-instance v0, LX/Cwa;

    invoke-direct {v0}, LX/Cwa;-><init>()V

    sget-object v3, LX/Cwb;->NO_GROUP:LX/Cwb;

    .line 2274287
    iput-object v3, v0, LX/Cwa;->a:LX/Cwb;

    .line 2274288
    move-object v0, v0

    .line 2274289
    sget-object v3, Lcom/facebook/search/model/DividerTypeaheadUnit;->a:Lcom/facebook/search/model/DividerTypeaheadUnit;

    move-object v3, v3

    .line 2274290
    invoke-static {v3}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    .line 2274291
    iput-object v3, v0, LX/Cwa;->b:LX/0Px;

    .line 2274292
    move-object v0, v0

    .line 2274293
    invoke-virtual {v0}, LX/Cwa;->a()LX/Cwc;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_2

    .line 2274294
    :cond_a
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 2274295
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cwb;

    iput-object v0, p0, LX/Fi9;->f:LX/Cwb;

    .line 2274296
    :cond_b
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/2Sf;->a(Ljava/util/List;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/facebook/search/model/TypeaheadUnit;
    .locals 2

    .prologue
    .line 2274252
    sget-object v0, LX/CwI;->SEARCH_BUTTON:LX/CwI;

    iget-object v1, p0, LX/Fi9;->c:LX/FgS;

    invoke-virtual {v1}, LX/FgS;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/Fi9;->a(LX/CwI;Ljava/lang/String;)Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    return-object v0
.end method
