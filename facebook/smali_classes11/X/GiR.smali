.class public final enum LX/GiR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GiR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GiR;

.field public static final enum HEAD_LOAD:LX/GiR;

.field public static final enum TAIL_LOAD:LX/GiR;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2385797
    new-instance v0, LX/GiR;

    const-string v1, "HEAD_LOAD"

    invoke-direct {v0, v1, v2}, LX/GiR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GiR;->HEAD_LOAD:LX/GiR;

    .line 2385798
    new-instance v0, LX/GiR;

    const-string v1, "TAIL_LOAD"

    invoke-direct {v0, v1, v3}, LX/GiR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GiR;->TAIL_LOAD:LX/GiR;

    .line 2385799
    const/4 v0, 0x2

    new-array v0, v0, [LX/GiR;

    sget-object v1, LX/GiR;->HEAD_LOAD:LX/GiR;

    aput-object v1, v0, v2

    sget-object v1, LX/GiR;->TAIL_LOAD:LX/GiR;

    aput-object v1, v0, v3

    sput-object v0, LX/GiR;->$VALUES:[LX/GiR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2385800
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GiR;
    .locals 1

    .prologue
    .line 2385801
    const-class v0, LX/GiR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GiR;

    return-object v0
.end method

.method public static values()[LX/GiR;
    .locals 1

    .prologue
    .line 2385802
    sget-object v0, LX/GiR;->$VALUES:[LX/GiR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GiR;

    return-object v0
.end method
