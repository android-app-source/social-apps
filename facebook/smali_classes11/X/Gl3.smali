.class public final LX/Gl3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GkK;

.field public final synthetic b:LX/Gkp;

.field public final synthetic c:I

.field public final synthetic d:LX/Gl6;


# direct methods
.method public constructor <init>(LX/Gl6;LX/GkK;LX/Gkp;I)V
    .locals 0

    .prologue
    .line 2389777
    iput-object p1, p0, LX/Gl3;->d:LX/Gl6;

    iput-object p2, p0, LX/Gl3;->a:LX/GkK;

    iput-object p3, p0, LX/Gl3;->b:LX/Gkp;

    iput p4, p0, LX/Gl3;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2389778
    const/4 v3, 0x1

    .line 2389779
    iget-object v0, p0, LX/Gl3;->a:LX/GkK;

    iget-object v1, p0, LX/Gl3;->b:LX/Gkp;

    invoke-virtual {v1}, LX/Gkp;->i()LX/Gkn;

    move-result-object v1

    invoke-interface {v0, v1, v3}, LX/GkK;->a(LX/Gkn;Z)V

    .line 2389780
    iget-object v0, p0, LX/Gl3;->d:LX/Gl6;

    iget-object v0, v0, LX/Gl6;->l:LX/0tX;

    iget-object v1, p0, LX/Gl3;->b:LX/Gkp;

    iget v2, p0, LX/Gl3;->c:I

    invoke-virtual {v1, v2}, LX/Gkp;->b(I)LX/GlC;

    move-result-object v1

    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->d:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    .line 2389781
    iput-boolean v3, v1, LX/0zO;->p:Z

    .line 2389782
    move-object v1, v1

    .line 2389783
    const-wide/16 v2, 0x708

    invoke-virtual {v1, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
