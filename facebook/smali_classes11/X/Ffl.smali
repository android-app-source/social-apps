.class public LX/Ffl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:Z

.field private final b:LX/0ad;

.field private final c:LX/7Aw;

.field private final d:LX/1vx;


# direct methods
.method public constructor <init>(Ljava/lang/Boolean;LX/0ad;LX/7Aw;LX/1vx;)V
    .locals 1
    .param p1    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2268840
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2268841
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/Ffl;->a:Z

    .line 2268842
    iput-object p2, p0, LX/Ffl;->b:LX/0ad;

    .line 2268843
    iput-object p3, p0, LX/Ffl;->c:LX/7Aw;

    .line 2268844
    iput-object p4, p0, LX/Ffl;->d:LX/1vx;

    .line 2268845
    return-void
.end method

.method public static a(LX/0QB;)LX/Ffl;
    .locals 7

    .prologue
    .line 2268846
    const-class v1, LX/Ffl;

    monitor-enter v1

    .line 2268847
    :try_start_0
    sget-object v0, LX/Ffl;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2268848
    sput-object v2, LX/Ffl;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2268849
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2268850
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2268851
    new-instance p0, LX/Ffl;

    invoke-static {v0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/7Aw;->a(LX/0QB;)LX/7Aw;

    move-result-object v5

    check-cast v5, LX/7Aw;

    invoke-static {v0}, LX/1vx;->a(LX/0QB;)LX/1vx;

    move-result-object v6

    check-cast v6, LX/1vx;

    invoke-direct {p0, v3, v4, v5, v6}, LX/Ffl;-><init>(Ljava/lang/Boolean;LX/0ad;LX/7Aw;LX/1vx;)V

    .line 2268852
    move-object v0, p0

    .line 2268853
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2268854
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ffl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2268855
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2268856
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(LX/CwB;)Z
    .locals 2

    .prologue
    .line 2268857
    invoke-interface {p0}, LX/CwB;->n()Lcom/facebook/search/model/ReactionSearchData;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, LX/CwB;->n()Lcom/facebook/search/model/ReactionSearchData;

    move-result-object v0

    .line 2268858
    iget-object v1, v0, Lcom/facebook/search/model/ReactionSearchData;->e:Ljava/lang/String;

    move-object v0, v1

    .line 2268859
    invoke-static {v0}, LX/2s8;->j(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, LX/CwB;->n()Lcom/facebook/search/model/ReactionSearchData;

    move-result-object v0

    .line 2268860
    iget-object v1, v0, Lcom/facebook/search/model/ReactionSearchData;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2268861
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, LX/CwB;->n()Lcom/facebook/search/model/ReactionSearchData;

    move-result-object v0

    .line 2268862
    iget-object v1, v0, Lcom/facebook/search/model/ReactionSearchData;->c:Ljava/lang/String;

    move-object v0, v1

    .line 2268863
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/CwB;)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/CwB;",
            ")",
            "LX/0Px",
            "<",
            "LX/CyI;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2268864
    iget-boolean v0, p0, LX/Ffl;->a:Z

    if-eqz v0, :cond_0

    .line 2268865
    sget-object v0, LX/CyI;->WORK_TABS:LX/0Px;

    .line 2268866
    :goto_0
    return-object v0

    .line 2268867
    :cond_0
    invoke-static {p1}, LX/CwC;->a(LX/CwB;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2268868
    sget-object v0, LX/CyI;->VIDEO_HOME_TABS:LX/0Px;

    goto :goto_0

    .line 2268869
    :cond_1
    iget-object v0, p0, LX/Ffl;->b:LX/0ad;

    sget-short v1, LX/100;->M:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2268870
    sget-object v0, LX/CyI;->I18N_TOP_ENTITIES_TABS:LX/0Px;

    goto :goto_0

    .line 2268871
    :cond_2
    iget-object v0, p0, LX/Ffl;->b:LX/0ad;

    sget-short v1, LX/100;->K:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2268872
    sget-object v0, LX/CyI;->I18N_TABS:LX/0Px;

    goto :goto_0

    .line 2268873
    :cond_3
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2268874
    invoke-static {p1}, LX/Ffl;->b(LX/CwB;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, LX/CyI;->TOP_REACTION:LX/CyI;

    :goto_1
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2268875
    iget-object v0, p0, LX/Ffl;->b:LX/0ad;

    sget-short v1, LX/100;->bW:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_a

    sget-object v0, LX/CyI;->SECONDARY_PREFER_ENTITY_TABS:LX/0Px;

    move-object v1, v0

    .line 2268876
    :goto_2
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v5

    move v2, v3

    :goto_3
    if-ge v2, v5, :cond_b

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CyI;

    .line 2268877
    sget-object v6, LX/CyI;->MARKETPLACE:LX/CyI;

    if-ne v0, v6, :cond_4

    iget-object v6, p0, LX/Ffl;->c:LX/7Aw;

    .line 2268878
    iget-object v7, v6, LX/7Aw;->b:LX/0Uh;

    sget v8, LX/2SU;->s:I

    const/4 p1, 0x0

    invoke-virtual {v7, v8, p1}, LX/0Uh;->a(IZ)Z

    move-result v7

    move v6, v7

    .line 2268879
    if-eqz v6, :cond_8

    .line 2268880
    :cond_4
    sget-object v6, LX/CyI;->VIDEOS:LX/CyI;

    if-ne v0, v6, :cond_5

    iget-object v6, p0, LX/Ffl;->d:LX/1vx;

    .line 2268881
    iget-object v7, v6, LX/1vx;->a:LX/0Uh;

    sget v8, LX/2SU;->O:I

    const/4 p1, 0x0

    invoke-virtual {v7, v8, p1}, LX/0Uh;->a(IZ)Z

    move-result v7

    move v6, v7

    .line 2268882
    if-eqz v6, :cond_8

    .line 2268883
    :cond_5
    sget-object v6, LX/CyI;->BLENDED_POSTS:LX/CyI;

    if-ne v0, v6, :cond_6

    iget-object v6, p0, LX/Ffl;->b:LX/0ad;

    sget-short v7, LX/100;->aH:S

    invoke-interface {v6, v7, v3}, LX/0ad;->a(SZ)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2268884
    :cond_6
    sget-object v6, LX/CyI;->LATEST:LX/CyI;

    if-ne v0, v6, :cond_7

    iget-object v6, p0, LX/Ffl;->b:LX/0ad;

    sget-short v7, LX/100;->R:S

    invoke-interface {v6, v7, v3}, LX/0ad;->a(SZ)Z

    move-result v6

    if-nez v6, :cond_8

    .line 2268885
    :cond_7
    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2268886
    :cond_8
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 2268887
    :cond_9
    sget-object v0, LX/CyI;->TOP:LX/CyI;

    goto :goto_1

    .line 2268888
    :cond_a
    sget-object v0, LX/CyI;->SECONDARY_TABS:LX/0Px;

    move-object v1, v0

    goto :goto_2

    .line 2268889
    :cond_b
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto/16 :goto_0
.end method
