.class public final LX/GB1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/accounts/Account;

.field public final synthetic b:LX/4gy;

.field public final synthetic c:LX/GB2;


# direct methods
.method public constructor <init>(LX/GB2;Landroid/accounts/Account;LX/4gy;)V
    .locals 0

    .prologue
    .line 2326298
    iput-object p1, p0, LX/GB1;->c:LX/GB2;

    iput-object p2, p0, LX/GB1;->a:Landroid/accounts/Account;

    iput-object p3, p0, LX/GB1;->b:LX/4gy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2326299
    sget-object v0, LX/GB2;->a:Ljava/lang/Class;

    const-string v1, "Account Recovery Open ID token fetch failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2326300
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2326301
    check-cast p1, Ljava/lang/String;

    .line 2326302
    if-nez p1, :cond_0

    .line 2326303
    :goto_0
    return-void

    .line 2326304
    :cond_0
    iget-object v0, p0, LX/GB1;->c:LX/GB2;

    iget-object v0, v0, LX/GB2;->d:Lcom/facebook/account/recovery/common/model/AccountRecoveryData;

    new-instance v1, Lcom/facebook/openidconnect/model/OpenIDCredential;

    iget-object v2, p0, LX/GB1;->a:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v3, p0, LX/GB1;->b:LX/4gy;

    invoke-direct {v1, v2, v3, p1}, Lcom/facebook/openidconnect/model/OpenIDCredential;-><init>(Ljava/lang/String;LX/4gy;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/facebook/account/recovery/common/model/AccountRecoveryData;->a(Lcom/facebook/openidconnect/model/OpenIDCredential;)V

    goto :goto_0
.end method
