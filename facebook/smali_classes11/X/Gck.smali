.class public LX/Gck;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "LX/Gcj;",
        "Ljava/lang/Void;",
        "[B>;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:LX/7yL;

.field private c:LX/Gcg;

.field public d:LX/1FZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2372337
    const-class v0, LX/Gck;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Gck;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/7yL;LX/Gcg;LX/1FZ;)V
    .locals 0

    .prologue
    .line 2372338
    invoke-direct {p0}, LX/3nE;-><init>()V

    .line 2372339
    iput-object p1, p0, LX/Gck;->b:LX/7yL;

    .line 2372340
    iput-object p2, p0, LX/Gck;->c:LX/Gcg;

    .line 2372341
    iput-object p3, p0, LX/Gck;->d:LX/1FZ;

    .line 2372342
    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 2372343
    check-cast p1, [LX/Gcj;

    .line 2372344
    const/4 v0, 0x0

    aget-object v0, p1, v0

    .line 2372345
    :try_start_0
    iget-object v1, v0, LX/Gcj;->a:Ljava/nio/ByteBuffer;

    iget v2, v0, LX/Gcj;->d:I

    iget v3, v0, LX/Gcj;->e:I

    iget v4, v0, LX/Gcj;->c:I

    const/4 v7, 0x0

    .line 2372346
    mul-int v5, v2, v3

    new-array v6, v5, [I

    .line 2372347
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v5

    new-array v8, v5, [B

    .line 2372348
    invoke-virtual {v1, v8}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    move v5, v7

    .line 2372349
    :goto_0
    mul-int v9, v2, v3

    if-ge v5, v9, :cond_0

    .line 2372350
    aget-byte v9, v8, v5

    and-int/lit16 v9, v9, 0xff

    .line 2372351
    const/high16 v10, -0x1000000

    shl-int/lit8 v11, v9, 0x10

    or-int/2addr v10, v11

    shl-int/lit8 v11, v9, 0x8

    or-int/2addr v10, v11

    or-int/2addr v9, v10

    aput v9, v6, v5

    .line 2372352
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 2372353
    :cond_0
    iget-object v5, p0, LX/Gck;->d:LX/1FZ;

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v5, v6, v2, v3, v8}, LX/1FZ;->a([IIILandroid/graphics/Bitmap$Config;)LX/1FJ;

    move-result-object v5

    invoke-virtual {v5}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Bitmap;

    .line 2372354
    new-instance v11, Landroid/graphics/Matrix;

    invoke-direct {v11}, Landroid/graphics/Matrix;-><init>()V

    .line 2372355
    int-to-float v5, v4

    invoke-virtual {v11, v5}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 2372356
    iget-object v5, p0, LX/Gck;->d:LX/1FZ;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    const/4 v12, 0x1

    move v8, v7

    invoke-virtual/range {v5 .. v12}, LX/1FZ;->a(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)LX/1FJ;

    move-result-object v5

    invoke-virtual {v5}, LX/1FJ;->a()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/Bitmap;

    .line 2372357
    move-object v1, v5

    .line 2372358
    iget-object v2, p0, LX/Gck;->b:LX/7yL;

    iget-object v3, v0, LX/Gcj;->b:LX/7yR;

    iget-object v0, v0, LX/Gcj;->f:LX/7yQ;

    invoke-virtual {v2, v3, v1, v0}, LX/7yL;->a(LX/7yR;Landroid/graphics/Bitmap;LX/7yQ;)[B
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2372359
    :goto_1
    return-object v0

    :catch_0
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2372360
    check-cast p1, [B

    .line 2372361
    iget-object v0, p0, LX/Gck;->c:LX/Gcg;

    .line 2372362
    if-nez p1, :cond_0

    .line 2372363
    :goto_0
    return-void

    .line 2372364
    :cond_0
    iget-object v1, v0, LX/Gcg;->w:Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;

    iget-object v2, v0, LX/Gcg;->A:Ljava/lang/String;

    new-instance v3, LX/Gce;

    invoke-direct {v3, v0}, LX/Gce;-><init>(LX/Gcg;)V

    .line 2372365
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2372366
    const-string v4, "account_id"

    invoke-virtual {v6, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2372367
    const-string v4, "face_crop_data"

    invoke-virtual {v6, v4, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 2372368
    iget-object v4, v1, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->f:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0aG;

    const-string v5, "dbl_face_rec"

    sget-object v7, LX/1ME;->BY_ERROR_CODE:LX/1ME;

    sget-object v8, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->e:Lcom/facebook/common/callercontext/CallerContext;

    const v9, -0x6e603717

    invoke-static/range {v4 .. v9}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;LX/1ME;Lcom/facebook/common/callercontext/CallerContext;I)LX/1MF;

    move-result-object v4

    .line 2372369
    invoke-interface {v4}, LX/1MF;->start()LX/1ML;

    move-result-object v4

    .line 2372370
    if-eqz v3, :cond_1

    .line 2372371
    iget-object v5, v1, Lcom/facebook/devicebasedlogin/protocol/DBLRequestHelper;->g:Ljava/util/concurrent/ExecutorService;

    invoke-static {v4, v3, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2372372
    :cond_1
    goto :goto_0
.end method
