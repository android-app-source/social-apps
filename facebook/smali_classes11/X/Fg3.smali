.class public LX/Fg3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Fg2;


# static fields
.field public static final b:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final c:Landroid/os/Handler;

.field public final d:LX/0kb;

.field public final e:LX/0SG;

.field public final f:LX/Ffw;

.field public g:LX/Ffx;

.field public h:LX/Ffz;

.field public i:LX/Fee;

.field public j:Z

.field public k:LX/Ffv;

.field public l:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2269280
    const-class v0, LX/Fg3;

    sput-object v0, LX/Fg3;->b:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/Ffw;Landroid/os/Handler;LX/0kb;)V
    .locals 1
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2269272
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2269273
    sget-object v0, LX/Fg3;->a:LX/Fee;

    iput-object v0, p0, LX/Fg3;->i:LX/Fee;

    .line 2269274
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Fg3;->j:Z

    .line 2269275
    iput-object p1, p0, LX/Fg3;->e:LX/0SG;

    .line 2269276
    iput-object p3, p0, LX/Fg3;->c:Landroid/os/Handler;

    .line 2269277
    iput-object p2, p0, LX/Fg3;->f:LX/Ffw;

    .line 2269278
    iput-object p4, p0, LX/Fg3;->d:LX/0kb;

    .line 2269279
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2269234
    iget-boolean v0, p0, LX/Fg3;->j:Z

    if-nez v0, :cond_0

    .line 2269235
    :goto_0
    return-void

    .line 2269236
    :cond_0
    iget-object v0, p0, LX/Fg3;->h:LX/Ffz;

    invoke-virtual {v0}, LX/Ffz;->b()V

    goto :goto_0
.end method

.method public final a(LX/Fee;)V
    .locals 0
    .param p1    # LX/Fee;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2269269
    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, LX/Fg3;->i:LX/Fee;

    .line 2269270
    return-void

    .line 2269271
    :cond_0
    sget-object p1, LX/Fg3;->a:LX/Fee;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/Ffx;LX/0am;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/results/model/contract/SearchResultsContext;",
            "Lcom/facebook/search/results/livefeed/loader/LiveFeedRefreshConfig;",
            "LX/0am",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2269243
    const/4 v0, 0x0

    .line 2269244
    invoke-virtual {p1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    .line 2269245
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[Refresh TypeaheadConfig Values] RenderInterval: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2269246
    const/16 v2, 0x3a98

    move v2, v2

    .line 2269247
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", TimeDelta: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2269248
    const/16 v2, 0x59d8

    move v2, v2

    .line 2269249
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", FetchInterval: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2269250
    const/16 v2, 0x3a98

    move v2, v2

    .line 2269251
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", FetchNumResults: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2269252
    const/16 v2, 0x14

    move v2, v2

    .line 2269253
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ExcludeIdQueueLength: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2269254
    const/16 v2, 0x14

    move v2, v2

    .line 2269255
    mul-int/lit8 v2, v2, 0x3

    move v2, v2

    .line 2269256
    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    .line 2269257
    iput-object p2, p0, LX/Fg3;->g:LX/Ffx;

    .line 2269258
    iget-object v1, p0, LX/Fg3;->f:LX/Ffw;

    .line 2269259
    new-instance v2, LX/Fg1;

    invoke-direct {v2, p0}, LX/Fg1;-><init>(LX/Fg3;)V

    move-object v2, v2

    .line 2269260
    invoke-virtual {v1, p1, v2}, LX/Ffw;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/Fee;)LX/Ffv;

    move-result-object v1

    iput-object v1, p0, LX/Fg3;->k:LX/Ffv;

    .line 2269261
    iget-object v1, p0, LX/Fg3;->h:LX/Ffz;

    if-eqz v1, :cond_0

    .line 2269262
    iget-object v1, p0, LX/Fg3;->h:LX/Ffz;

    invoke-virtual {v1}, LX/Ffz;->c()V

    .line 2269263
    :cond_0
    new-instance v1, LX/Fg0;

    iget-object v3, p0, LX/Fg3;->e:LX/0SG;

    iget-object v4, p0, LX/Fg3;->c:Landroid/os/Handler;

    .line 2269264
    const/16 v5, 0x3a98

    move v5, v5

    .line 2269265
    move-object v2, p0

    move v6, v0

    invoke-direct/range {v1 .. v6}, LX/Fg0;-><init>(LX/Fg3;LX/0SG;Landroid/os/Handler;IZ)V

    iput-object v1, p0, LX/Fg3;->h:LX/Ffz;

    .line 2269266
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/Fg3;->j:Z

    .line 2269267
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    iput-object v1, p0, LX/Fg3;->l:LX/0am;

    .line 2269268
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2269281
    iget-boolean v0, p0, LX/Fg3;->j:Z

    if-nez v0, :cond_0

    .line 2269282
    :goto_0
    return-void

    .line 2269283
    :cond_0
    iget-object v0, p0, LX/Fg3;->k:LX/Ffv;

    const/16 v1, 0xa

    .line 2269284
    iget-object v2, v0, LX/Ffv;->b:LX/1Ck;

    const-string v3, "fetch_live_conversations_tailload"

    new-instance v4, LX/Fft;

    invoke-direct {v4, v0, p1, v1}, LX/Fft;-><init>(LX/Ffv;Ljava/lang/String;I)V

    new-instance p0, LX/Ffu;

    invoke-direct {p0, v0}, LX/Ffu;-><init>(LX/Ffv;)V

    invoke-virtual {v2, v3, v4, p0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2269285
    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2269239
    iget-boolean v0, p0, LX/Fg3;->j:Z

    if-nez v0, :cond_0

    .line 2269240
    :goto_0
    return-void

    .line 2269241
    :cond_0
    iget-object v0, p0, LX/Fg3;->h:LX/Ffz;

    invoke-virtual {v0}, LX/Ffz;->c()V

    .line 2269242
    iget-object v0, p0, LX/Fg3;->k:LX/Ffv;

    invoke-virtual {v0}, LX/Ffv;->b()V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2269237
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/Fg3;->a(Ljava/lang/String;)V

    .line 2269238
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2269226
    iget-boolean v0, p0, LX/Fg3;->j:Z

    if-nez v0, :cond_0

    .line 2269227
    :goto_0
    return-void

    .line 2269228
    :cond_0
    iget-object v0, p0, LX/Fg3;->h:LX/Ffz;

    invoke-virtual {v0}, LX/Ffz;->c()V

    .line 2269229
    iget-object v0, p0, LX/Fg3;->h:LX/Ffz;

    const/4 v1, 0x0

    .line 2269230
    iput-boolean v1, v0, LX/Ffz;->g:Z

    .line 2269231
    iget-object v0, p0, LX/Fg3;->h:LX/Ffz;

    invoke-virtual {v0}, LX/Ffz;->a()V

    .line 2269232
    iget-object v0, p0, LX/Fg3;->h:LX/Ffz;

    invoke-virtual {v0}, LX/Ffz;->b()V

    .line 2269233
    goto :goto_0
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 2269221
    iget-boolean v0, p0, LX/Fg3;->j:Z

    if-nez v0, :cond_0

    .line 2269222
    const/4 v0, 0x0

    .line 2269223
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/Fg3;->k:LX/Ffv;

    .line 2269224
    iget-object v1, v0, LX/Ffv;->b:LX/1Ck;

    const-string p0, "fetch_live_conversations_tailload"

    invoke-virtual {v1, p0}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v1

    move v0, v1

    .line 2269225
    goto :goto_0
.end method
