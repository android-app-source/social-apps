.class public LX/Gpi;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/instantshopping/view/block/InstantShoppingSlideshowBlockView;",
        "Lcom/facebook/instantshopping/model/data/InstantShoppingSlideshowBlockData;",
        ">;"
    }
.end annotation


# instance fields
.field public d:LX/Go7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Go4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Chi;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:LX/GoE;

.field private h:Landroid/os/Bundle;

.field private final i:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/GqX;)V
    .locals 4

    .prologue
    .line 2395631
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2395632
    const-string v0, "last_visited_slide"

    iput-object v0, p0, LX/Gpi;->i:Ljava/lang/String;

    .line 2395633
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p1

    check-cast p0, LX/Gpi;

    invoke-static {p1}, LX/Go7;->a(LX/0QB;)LX/Go7;

    move-result-object v2

    check-cast v2, LX/Go7;

    invoke-static {p1}, LX/Go4;->a(LX/0QB;)LX/Go4;

    move-result-object v3

    check-cast v3, LX/Go4;

    const/16 v0, 0x31dc

    invoke-static {p1, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p1

    iput-object v2, p0, LX/Gpi;->d:LX/Go7;

    iput-object v3, p0, LX/Gpi;->e:LX/Go4;

    iput-object p1, p0, LX/Gpi;->f:LX/0Ot;

    .line 2395634
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 7

    .prologue
    .line 2395594
    check-cast p1, LX/GpF;

    .line 2395595
    invoke-static {p1}, LX/Co1;->a(LX/Clu;)Landroid/os/Bundle;

    move-result-object v1

    .line 2395596
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395597
    check-cast v0, LX/GqX;

    .line 2395598
    invoke-interface {p1}, LX/Gol;->M()Z

    move-result v2

    .line 2395599
    if-eqz v2, :cond_0

    .line 2395600
    const-string v2, "strategyType"

    sget-object v3, LX/CrN;->NON_ADJUSTED_FIT_TO_WIDTH_SLIDE:LX/CrN;

    invoke-virtual {v3}, LX/CrN;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2395601
    :cond_0
    invoke-interface {p1}, LX/God;->lB_()Z

    move-result v2

    invoke-virtual {v0, v2}, LX/GqX;->b(Z)V

    .line 2395602
    invoke-interface {p1}, LX/Goc;->p()Z

    move-result v2

    .line 2395603
    iput-boolean v2, v0, LX/GqX;->m:Z

    .line 2395604
    invoke-interface {p1}, LX/Clr;->o()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, LX/Gpi;->h:Landroid/os/Bundle;

    .line 2395605
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395606
    check-cast v0, LX/GqX;

    invoke-interface {v0, v1}, LX/CnG;->a(Landroid/os/Bundle;)V

    .line 2395607
    invoke-interface {p1}, LX/GoY;->D()LX/GoE;

    move-result-object v0

    iput-object v0, p0, LX/Gpi;->g:LX/GoE;

    .line 2395608
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395609
    check-cast v0, LX/GqX;

    invoke-interface {p1}, LX/Cm3;->a()LX/Clo;

    move-result-object v1

    invoke-interface {p1}, LX/Clp;->m()Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;

    move-result-object v2

    invoke-static {v2}, LX/CoV;->a(Lcom/facebook/graphql/enums/GraphQLDocumentMediaPresentationStyle;)LX/Cqw;

    move-result-object v2

    .line 2395610
    invoke-virtual {v0, v2}, LX/Cos;->a(LX/Cqw;)V

    .line 2395611
    iget-object v3, v0, LX/GqX;->l:LX/Grs;

    invoke-virtual {v1}, LX/Clo;->d()I

    move-result v4

    const/4 v6, 0x0

    .line 2395612
    iget-object v2, v3, LX/Grs;->a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    const/4 v5, 0x1

    if-le v4, v5, :cond_2

    move v5, v4

    :goto_0
    invoke-virtual {v2, v5}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setCount(I)V

    .line 2395613
    iget-object v5, v3, LX/Grs;->a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    .line 2395614
    iput v4, v5, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->c:I

    .line 2395615
    iget-object v5, v3, LX/Grs;->a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    invoke-virtual {v5, v6}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setCurrentItem(I)V

    .line 2395616
    invoke-virtual {v0}, LX/Cot;->f()Lcom/facebook/richdocument/view/widget/SlideshowView;

    move-result-object v3

    check-cast v3, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;

    .line 2395617
    invoke-virtual {v3, v1}, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->setInstantShoppingSlides(LX/Clo;)V

    .line 2395618
    iget v0, p1, LX/GpF;->b:I

    move v1, v0

    .line 2395619
    const/4 v0, -0x1

    if-eq v1, v0, :cond_1

    .line 2395620
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395621
    check-cast v0, LX/GqX;

    .line 2395622
    invoke-virtual {v0}, LX/Cot;->f()Lcom/facebook/richdocument/view/widget/SlideshowView;

    move-result-object v2

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v3, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Lcom/facebook/richdocument/view/widget/SlideshowView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2395623
    iget-object v2, v0, LX/Cos;->a:LX/Ctg;

    move-object v2, v2

    .line 2395624
    invoke-interface {v2, v1}, LX/Ctg;->setOverlayBackgroundColor(I)V

    .line 2395625
    :cond_1
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v1, v0

    .line 2395626
    iget-object v0, p0, LX/Gpi;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chi;

    .line 2395627
    iget-object v2, v0, LX/Chi;->k:Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;

    move-object v0, v2

    .line 2395628
    invoke-interface {p1}, LX/Clr;->o()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v1, p1, v0, v2}, LX/Co1;->a(LX/CnG;LX/Clq;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentStyleModel;Landroid/os/Bundle;)V

    .line 2395629
    return-void

    :cond_2
    move v5, v6

    .line 2395630
    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2395635
    invoke-super {p0, p1}, LX/CnT;->a(Landroid/os/Bundle;)V

    .line 2395636
    iget-object v0, p0, LX/Gpi;->g:LX/GoE;

    if-eqz v0, :cond_0

    .line 2395637
    iget-object v0, p0, LX/Gpi;->d:LX/Go7;

    const-string v1, "slideshow_element_start"

    iget-object v2, p0, LX/Gpi;->g:LX/GoE;

    invoke-virtual {v2}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2395638
    iget-object v0, p0, LX/Gpi;->e:LX/Go4;

    iget-object v1, p0, LX/Gpi;->g:LX/GoE;

    invoke-virtual {v1}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Go4;->a(Ljava/lang/String;)V

    .line 2395639
    :cond_0
    iget-object v0, p0, LX/Gpi;->h:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    .line 2395640
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395641
    if-eqz v0, :cond_1

    .line 2395642
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395643
    check-cast v0, LX/GqX;

    iget-object v1, p0, LX/Gpi;->h:Landroid/os/Bundle;

    const-string v2, "last_visited_slide"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 2395644
    invoke-virtual {v0}, LX/Cot;->f()Lcom/facebook/richdocument/view/widget/SlideshowView;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2395645
    invoke-virtual {v0}, LX/Cot;->f()Lcom/facebook/richdocument/view/widget/SlideshowView;

    move-result-object v2

    .line 2395646
    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView;->g_(I)V

    .line 2395647
    invoke-virtual {v0}, LX/Cot;->f()Lcom/facebook/richdocument/view/widget/SlideshowView;

    move-result-object v2

    check-cast v2, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;

    .line 2395648
    iput v1, v2, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->t:I

    .line 2395649
    iget-object v2, v0, LX/GqX;->l:LX/Grs;

    .line 2395650
    iget-object v0, v2, LX/Grs;->a:Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/viewpageindicator/HScrollCirclePageIndicator;->setCurrentItem(I)V

    .line 2395651
    :cond_1
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2395579
    invoke-super {p0, p1}, LX/CnT;->b(Landroid/os/Bundle;)V

    .line 2395580
    iget-object v0, p0, LX/Gpi;->g:LX/GoE;

    if-eqz v0, :cond_0

    .line 2395581
    iget-object v0, p0, LX/Gpi;->d:LX/Go7;

    const-string v1, "slideshow_element_end"

    iget-object v2, p0, LX/Gpi;->g:LX/GoE;

    invoke-virtual {v2}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2395582
    iget-object v0, p0, LX/Gpi;->e:LX/Go4;

    iget-object v1, p0, LX/Gpi;->g:LX/GoE;

    invoke-virtual {v1}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Go4;->b(Ljava/lang/String;)V

    .line 2395583
    :cond_0
    iget-object v0, p0, LX/Gpi;->h:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    .line 2395584
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395585
    if-eqz v0, :cond_1

    .line 2395586
    iget-object v1, p0, LX/Gpi;->h:Landroid/os/Bundle;

    const-string v2, "last_visited_slide"

    .line 2395587
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395588
    check-cast v0, LX/GqX;

    .line 2395589
    invoke-virtual {v0}, LX/Cot;->f()Lcom/facebook/richdocument/view/widget/SlideshowView;

    move-result-object p0

    if-eqz p0, :cond_2

    invoke-virtual {v0}, LX/Cot;->f()Lcom/facebook/richdocument/view/widget/SlideshowView;

    move-result-object p0

    check-cast p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;

    .line 2395590
    iget v0, p0, Lcom/facebook/instantshopping/view/widget/InstantShoppingSlideshowView;->t:I

    move p0, v0

    .line 2395591
    :goto_0
    move v0, p0

    .line 2395592
    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2395593
    :cond_1
    return-void

    :cond_2
    const/4 p0, 0x0

    goto :goto_0
.end method
