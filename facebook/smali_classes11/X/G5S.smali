.class public LX/G5S;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/7Ba;

.field private final c:Lcom/facebook/performancelogger/PerformanceLogger;


# direct methods
.method public constructor <init>(LX/0Or;LX/7Ba;Lcom/facebook/performancelogger/PerformanceLogger;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/7Ba;",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2318766
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2318767
    iput-object p1, p0, LX/G5S;->a:LX/0Or;

    .line 2318768
    iput-object p2, p0, LX/G5S;->b:LX/7Ba;

    .line 2318769
    iput-object p3, p0, LX/G5S;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    .line 2318770
    return-void
.end method

.method public static a(LX/0QB;)LX/G5S;
    .locals 6

    .prologue
    .line 2318771
    const-class v1, LX/G5S;

    monitor-enter v1

    .line 2318772
    :try_start_0
    sget-object v0, LX/G5S;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2318773
    sput-object v2, LX/G5S;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2318774
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2318775
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2318776
    new-instance v5, LX/G5S;

    const/16 v3, 0xb83

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/7Ba;->b(LX/0QB;)LX/7Ba;

    move-result-object v3

    check-cast v3, LX/7Ba;

    invoke-static {v0}, LX/0XW;->a(LX/0QB;)LX/0XW;

    move-result-object v4

    check-cast v4, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-direct {v5, p0, v3, v4}, LX/G5S;-><init>(LX/0Or;LX/7Ba;Lcom/facebook/performancelogger/PerformanceLogger;)V

    .line 2318777
    move-object v0, v5

    .line 2318778
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2318779
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/G5S;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2318780
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2318781
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(LX/1qK;ILjava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2318782
    iget-object v0, p0, LX/G5S;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, p2, p3}, Lcom/facebook/performancelogger/PerformanceLogger;->b(ILjava/lang/String;)V

    .line 2318783
    iget-object v0, p0, LX/G5S;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, p2, p3}, Lcom/facebook/performancelogger/PerformanceLogger;->d(ILjava/lang/String;)V

    .line 2318784
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2318785
    const-string v1, "fetchQueryResultParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/api/protocol/FetchSearchTypeaheadResultParams;

    .line 2318786
    iget-object v1, p0, LX/G5S;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    .line 2318787
    :try_start_0
    iget-object v2, p0, LX/G5S;->b:LX/7Ba;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/7Hc;

    .line 2318788
    iget-object v1, v0, LX/7Hc;->b:LX/0Px;

    move-object v0, v1

    .line 2318789
    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2318790
    iget-object v1, p0, LX/G5S;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v1, p2, p3}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 2318791
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/util/ArrayList;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 2318792
    :catch_0
    move-exception v0

    .line 2318793
    iget-object v1, p0, LX/G5S;->c:Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v1, p2, p3}, Lcom/facebook/performancelogger/PerformanceLogger;->f(ILjava/lang/String;)V

    .line 2318794
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 2318795
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2318796
    const-string v1, "fetch_uberbar_result"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2318797
    const-string v0, "SearchRemoteFetch"

    .line 2318798
    const v1, 0x1b0004

    invoke-direct {p0, p1, v1, v0}, LX/G5S;->a(LX/1qK;ILjava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 2318799
    :cond_0
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Unknown type"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
.end method
