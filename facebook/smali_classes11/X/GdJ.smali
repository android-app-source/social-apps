.class public LX/GdJ;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RKI18n"
.end annotation


# instance fields
.field private final a:I


# direct methods
.method public constructor <init>(LX/5pY;I)V
    .locals 0

    .prologue
    .line 2373562
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2373563
    iput p2, p0, LX/GdJ;->a:I

    .line 2373564
    return-void
.end method

.method public static a(Landroid/content/Context;I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2373550
    const/4 v1, 0x0

    .line 2373551
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    .line 2373552
    new-instance v0, Ljava/lang/String;

    invoke-static {v1}, LX/0aP;->a(Ljava/io/InputStream;)[B

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>([B)V
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2373553
    if-eqz v1, :cond_0

    .line 2373554
    :try_start_1
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2373555
    :cond_0
    :goto_0
    return-object v0

    .line 2373556
    :catch_0
    move-exception v0

    .line 2373557
    :goto_1
    :try_start_2
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Could not read localized JSON file from resources"

    invoke-direct {v2, v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2373558
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 2373559
    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 2373560
    :cond_1
    :goto_2
    throw v0

    :catch_1
    goto :goto_0

    :catch_2
    goto :goto_2

    .line 2373561
    :catch_3
    move-exception v0

    goto :goto_1
.end method

.method private a(Ljava/lang/Boolean;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2373565
    iget-object v0, p0, LX/5pb;->a:LX/5pY;

    move-object v0, v0

    .line 2373566
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const-string v2, "number_format_config"

    const-string v3, "raw"

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2373567
    const v1, 0x7f070041

    invoke-static {v0, v1}, LX/GdJ;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 2373568
    :goto_0
    return-object v0

    .line 2373569
    :cond_0
    new-instance v1, Ljava/io/StringWriter;

    invoke-direct {v1}, Ljava/io/StringWriter;-><init>()V

    .line 2373570
    invoke-static {}, Ljava/text/DecimalFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v0

    check-cast v0, Ljava/text/DecimalFormat;

    .line 2373571
    invoke-static {}, Ljava/text/DecimalFormatSymbols;->getInstance()Ljava/text/DecimalFormatSymbols;

    move-result-object v2

    .line 2373572
    :try_start_0
    new-instance v3, LX/5pP;

    invoke-direct {v3, v1}, LX/5pP;-><init>(Ljava/io/Writer;)V

    .line 2373573
    invoke-virtual {v3}, LX/5pP;->a()LX/5pP;

    .line 2373574
    const-string v4, "decimalSeparator"

    invoke-virtual {v3, v4}, LX/5pP;->a(Ljava/lang/String;)LX/5pP;

    move-result-object v4

    invoke-virtual {v2}, Ljava/text/DecimalFormatSymbols;->getDecimalSeparator()C

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/5pP;->b(Ljava/lang/String;)LX/5pP;

    .line 2373575
    const-string v4, "numberDelimiter"

    invoke-virtual {v3, v4}, LX/5pP;->a(Ljava/lang/String;)LX/5pP;

    move-result-object v4

    invoke-virtual {v2}, Ljava/text/DecimalFormatSymbols;->getGroupingSeparator()C

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/5pP;->b(Ljava/lang/String;)LX/5pP;

    .line 2373576
    const-string v2, "minDigitsForThousandsSeparator"

    invoke-virtual {v3, v2}, LX/5pP;->a(Ljava/lang/String;)LX/5pP;

    move-result-object v2

    invoke-virtual {v0}, Ljava/text/DecimalFormat;->getGroupingSize()I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {v2, v4, v5}, LX/5pP;->a(J)LX/5pP;

    .line 2373577
    invoke-virtual {v3}, LX/5pP;->b()LX/5pP;

    .line 2373578
    invoke-virtual {v3}, LX/5pP;->close()V

    .line 2373579
    invoke-virtual {v1}, Ljava/io/StringWriter;->getBuffer()Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 2373580
    :catch_0
    move-exception v0

    .line 2373581
    const-string v1, "React"

    const-string v2, "Unable to serialize NumberFormatConfig from system values"

    invoke-static {v1, v2, v0}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2373582
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/util/HashMap;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2373540
    const-string v0, "fbLocaleIdentifier"

    .line 2373541
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2373542
    invoke-static {v1}, LX/GdI;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2373543
    const-string v0, "CurrencyFormatConfig"

    .line 2373544
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2373545
    const v2, 0x7f07001c

    invoke-static {v1, v2}, LX/GdJ;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2373546
    const-string v0, "DateFormatConfig"

    .line 2373547
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2373548
    const v2, 0x7f07001f

    invoke-static {v1, v2}, LX/GdJ;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2373549
    return-void
.end method


# virtual methods
.method public final b()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2373528
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2373529
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2373530
    invoke-virtual {v1}, LX/5pY;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    .line 2373531
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 2373532
    invoke-direct {p0, v0}, LX/GdJ;->a(Ljava/util/HashMap;)V

    .line 2373533
    const-string v3, "NumberFormatConfig"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {p0, v4}, LX/GdJ;->a(Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2373534
    const-string v3, "FallbackNumberFormatConfig"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {p0, v4}, LX/GdJ;->a(Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2373535
    const-string v3, "localeIdentifier"

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2373536
    const-string v3, "localeCountryCode"

    invoke-virtual {v2}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2373537
    const-string v2, "translationsDictionary"

    iget v3, p0, LX/GdJ;->a:I

    invoke-static {v1, v3}, LX/GdJ;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2373538
    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2373539
    const-string v0, "RKI18n"

    return-object v0
.end method
