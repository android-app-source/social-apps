.class public LX/FjC;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2SY;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Sr;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2276426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2276427
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2276428
    iput-object v0, p0, LX/FjC;->a:LX/0Ot;

    .line 2276429
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2276430
    iput-object v0, p0, LX/FjC;->b:LX/0Ot;

    .line 2276431
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2276432
    iput-object v0, p0, LX/FjC;->c:LX/0Ot;

    .line 2276433
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2276434
    iput-object v0, p0, LX/FjC;->d:LX/0Ot;

    .line 2276435
    return-void
.end method

.method public static b(LX/0QB;)LX/FjC;
    .locals 5

    .prologue
    .line 2276436
    new-instance v0, LX/FjC;

    invoke-direct {v0}, LX/FjC;-><init>()V

    .line 2276437
    const/16 v1, 0x11b8

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x11b9

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x15e7

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2eb

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    .line 2276438
    iput-object v1, v0, LX/FjC;->a:LX/0Ot;

    iput-object v2, v0, LX/FjC;->b:LX/0Ot;

    iput-object v3, v0, LX/FjC;->c:LX/0Ot;

    iput-object v4, v0, LX/FjC;->d:LX/0Ot;

    .line 2276439
    return-object v0
.end method


# virtual methods
.method public final a(LX/Cwb;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 2276440
    new-instance v0, LX/FjB;

    invoke-direct {v0, p0, p1}, LX/FjB;-><init>(LX/FjC;LX/Cwb;)V

    return-object v0
.end method
