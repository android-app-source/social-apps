.class public final LX/F5e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/F5h;


# direct methods
.method public constructor <init>(LX/F5h;)V
    .locals 0

    .prologue
    .line 2199080
    iput-object p1, p0, LX/F5e;->a:LX/F5h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2199077
    new-instance v0, LX/F5p;

    invoke-direct {v0}, LX/F5p;-><init>()V

    move-object v0, v0

    .line 2199078
    const-string v1, "is_work_build"

    iget-object v2, p0, LX/F5e;->a:LX/F5h;

    iget-boolean v2, v2, LX/F5h;->d:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v0

    const-string v1, "member_profile_size"

    iget-object v2, p0, LX/F5e;->a:LX/F5h;

    iget-object v2, v2, LX/F5h;->c:Landroid/content/res/Resources;

    const v3, 0x7f0b208b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/F5p;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 2199079
    iget-object v1, p0, LX/F5e;->a:LX/F5h;

    iget-object v1, v1, LX/F5h;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
