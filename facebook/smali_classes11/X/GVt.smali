.class public final LX/GVt;
.super LX/AQ9;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<ModelData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDataProvider;",
        "DerivedData::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginDerivedDataProvider;",
        "Mutation::",
        "Lcom/facebook/ipc/composer/plugin/ComposerPluginMutation",
        "<TMutation;>;>",
        "LX/AQ9",
        "<TModelData;TDerivedData;TMutation;>;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;


# direct methods
.method public constructor <init>(LX/B5j;Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;Landroid/content/Context;)V
    .locals 0
    .param p1    # LX/B5j;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/B5j",
            "<TModelData;TDerivedData;TMutation;>;",
            "Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2360163
    invoke-direct {p0, p3, p1}, LX/AQ9;-><init>(Landroid/content/Context;LX/B5j;)V

    .line 2360164
    iput-object p2, p0, LX/GVt;->a:Lcom/facebook/civicengagement/composer/CivicEngagementComposerPluginConfig;

    .line 2360165
    return-void
.end method


# virtual methods
.method public final aB()LX/AQ4;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/AQ4",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2360167
    new-instance v0, LX/GVr;

    invoke-direct {v0, p0}, LX/GVr;-><init>(LX/GVt;)V

    return-object v0
.end method

.method public final aI()LX/ARN;
    .locals 1

    .prologue
    .line 2360168
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method

.method public final aa()LX/ARN;
    .locals 1
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 2360166
    sget-object v0, LX/ARN;->b:LX/ARN;

    return-object v0
.end method
