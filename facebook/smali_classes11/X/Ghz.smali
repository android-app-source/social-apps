.class public LX/Ghz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:I

.field public static final b:I

.field private static volatile c:LX/Ghz;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2385157
    const v0, 0x7f0a00d6

    sput v0, LX/Ghz;->a:I

    .line 2385158
    const v0, 0x7f0b004e

    sput v0, LX/Ghz;->b:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2385143
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2385144
    return-void
.end method

.method public static a(LX/0QB;)LX/Ghz;
    .locals 3

    .prologue
    .line 2385145
    sget-object v0, LX/Ghz;->c:LX/Ghz;

    if-nez v0, :cond_1

    .line 2385146
    const-class v1, LX/Ghz;

    monitor-enter v1

    .line 2385147
    :try_start_0
    sget-object v0, LX/Ghz;->c:LX/Ghz;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2385148
    if-eqz v2, :cond_0

    .line 2385149
    :try_start_1
    new-instance v0, LX/Ghz;

    invoke-direct {v0}, LX/Ghz;-><init>()V

    .line 2385150
    move-object v0, v0

    .line 2385151
    sput-object v0, LX/Ghz;->c:LX/Ghz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2385152
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2385153
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2385154
    :cond_1
    sget-object v0, LX/Ghz;->c:LX/Ghz;

    return-object v0

    .line 2385155
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2385156
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
