.class public final LX/FrK;
.super LX/BPt;
.source ""


# instance fields
.field public final synthetic a:LX/FrL;


# direct methods
.method public constructor <init>(LX/FrL;Landroid/os/ParcelUuid;)V
    .locals 0

    .prologue
    .line 2295402
    iput-object p1, p0, LX/FrK;->a:LX/FrL;

    invoke-direct {p0, p2}, LX/BPt;-><init>(Landroid/os/ParcelUuid;)V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 5

    .prologue
    .line 2295403
    check-cast p1, LX/BPr;

    .line 2295404
    iget-object v0, p0, LX/FrK;->a:LX/FrL;

    iget-object v0, v0, LX/FrL;->b:LX/G1I;

    .line 2295405
    iget-object v1, p1, LX/BPr;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2295406
    iget-object v2, p0, LX/FrK;->a:LX/FrL;

    iget-object v2, v2, LX/FrL;->e:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 2295407
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    invoke-static {v0, v4}, LX/G1I;->b(LX/G1I;Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;)Lcom/facebook/timeline/protiles/model/ProtileModel;

    move-result-object v4

    .line 2295408
    if-eqz v4, :cond_0

    .line 2295409
    invoke-static {v4, v1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->b(Lcom/facebook/timeline/protiles/model/ProtileModel;Ljava/lang/String;)Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    move-result-object p1

    .line 2295410
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->q()I

    move-result v0

    if-gtz v0, :cond_1

    .line 2295411
    :cond_0
    :goto_0
    iget-object v0, p0, LX/FrK;->a:LX/FrL;

    iget-object v0, v0, LX/FrL;->d:LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->E()V

    .line 2295412
    return-void

    .line 2295413
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->k()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->n()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object p1

    invoke-static {v4, v1, v0, p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->a(Lcom/facebook/timeline/protiles/model/ProtileModel;Ljava/lang/String;ILcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2295414
    invoke-virtual {v4, v2, v3}, Lcom/facebook/graphql/model/BaseFeedUnit;->a(J)V

    goto :goto_0
.end method
