.class public LX/Fi6;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/Fi6;


# instance fields
.field private final a:LX/8ht;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2SO;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fi1;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fhx;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2So;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fi5;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/8ht;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/8ht;",
            "LX/0Ot",
            "<",
            "LX/2SO;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fi1;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fhx;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2So;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Fi5;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2274211
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2274212
    iput-object p1, p0, LX/Fi6;->a:LX/8ht;

    .line 2274213
    iput-object p2, p0, LX/Fi6;->b:LX/0Ot;

    .line 2274214
    iput-object p3, p0, LX/Fi6;->c:LX/0Ot;

    .line 2274215
    iput-object p4, p0, LX/Fi6;->d:LX/0Ot;

    .line 2274216
    iput-object p5, p0, LX/Fi6;->e:LX/0Ot;

    .line 2274217
    iput-object p6, p0, LX/Fi6;->f:LX/0Ot;

    .line 2274218
    return-void
.end method

.method public static a(LX/0QB;)LX/Fi6;
    .locals 10

    .prologue
    .line 2274219
    sget-object v0, LX/Fi6;->g:LX/Fi6;

    if-nez v0, :cond_1

    .line 2274220
    const-class v1, LX/Fi6;

    monitor-enter v1

    .line 2274221
    :try_start_0
    sget-object v0, LX/Fi6;->g:LX/Fi6;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2274222
    if-eqz v2, :cond_0

    .line 2274223
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2274224
    new-instance v3, LX/Fi6;

    invoke-static {v0}, LX/8ht;->a(LX/0QB;)LX/8ht;

    move-result-object v4

    check-cast v4, LX/8ht;

    const/16 v5, 0x11ae

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x34cf

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x34cd

    invoke-static {v0, v7}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x11b4

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x34d1

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, LX/Fi6;-><init>(LX/8ht;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2274225
    move-object v0, v3

    .line 2274226
    sput-object v0, LX/Fi6;->g:LX/Fi6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2274227
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2274228
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2274229
    :cond_1
    sget-object v0, LX/Fi6;->g:LX/Fi6;

    return-object v0

    .line 2274230
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2274231
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)LX/2SP;
    .locals 2

    .prologue
    .line 2274232
    invoke-static {p1}, LX/8ht;->b(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2274233
    iget-object v0, p0, LX/Fi6;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    .line 2274234
    :goto_0
    return-object v0

    .line 2274235
    :cond_0
    iget-object v0, p0, LX/Fi6;->a:LX/8ht;

    invoke-virtual {v0, p1}, LX/8ht;->h(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2274236
    iget-object v0, p0, LX/Fi6;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fi5;

    .line 2274237
    invoke-virtual {v0, p1}, LX/2SP;->a(Lcom/facebook/search/api/GraphSearchQuery;)V

    goto :goto_0

    .line 2274238
    :cond_1
    iget-object v0, p0, LX/Fi6;->a:LX/8ht;

    invoke-virtual {v0, p1}, LX/8ht;->e(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2274239
    iget-object v0, p0, LX/Fi6;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    goto :goto_0

    .line 2274240
    :cond_2
    if-eqz p1, :cond_4

    .line 2274241
    iget-object v0, p1, Lcom/facebook/search/api/GraphSearchQuery;->j:LX/7B5;

    move-object v0, v0

    .line 2274242
    sget-object v1, LX/7B5;->TAB:LX/7B5;

    if-ne v0, v1, :cond_4

    .line 2274243
    iget-boolean v0, p1, Lcom/facebook/search/api/GraphSearchQuery;->k:Z

    move v0, v0

    .line 2274244
    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    move v0, v0

    .line 2274245
    if-eqz v0, :cond_3

    .line 2274246
    iget-object v0, p0, LX/Fi6;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    goto :goto_0

    .line 2274247
    :cond_3
    iget-object v0, p0, LX/Fi6;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SP;

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method
