.class public final LX/HC6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HCC;


# direct methods
.method public constructor <init>(LX/HCC;)V
    .locals 0

    .prologue
    .line 2438187
    iput-object p1, p0, LX/HC6;->a:LX/HCC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x5c50f86e

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2438188
    iget-object v1, p0, LX/HC6;->a:LX/HCC;

    .line 2438189
    sget-object v3, LX/9X7;->EDIT_TAP_REORDER_TABS:LX/9X7;

    invoke-static {v1, v3}, LX/HCC;->a$redex0(LX/HCC;LX/9X7;)V

    .line 2438190
    sget-object v3, LX/8Dq;->e:Ljava/lang/String;

    iget-wide v5, v1, LX/HCC;->k:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 2438191
    iget-object v3, v1, LX/HCC;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/17Y;

    iget-object v5, v1, LX/HCC;->l:Landroid/content/Context;

    invoke-interface {v3, v5, v4}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 2438192
    const-string v3, "profile_name"

    iget-object v4, v1, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2438193
    const-string v3, "extra_reorder_tabs_data"

    iget-object v4, v1, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-static {v5, v3, v4}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2438194
    iget-object v3, v1, LX/HCC;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    const/16 v6, 0x2784

    iget-object v4, v1, LX/HCC;->l:Landroid/content/Context;

    check-cast v4, Landroid/app/Activity;

    invoke-interface {v3, v5, v6, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2438195
    const v1, -0xa49a0c1

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
