.class public final LX/G7T;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Z

.field public b:[Ljava/lang/String;

.field public c:Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;

.field public d:Lcom/google/android/gms/auth/api/credentials/CredentialPickerConfig;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/auth/api/credentials/CredentialRequest;
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, LX/G7T;->b:[Ljava/lang/String;

    if-nez v0, :cond_0

    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, LX/G7T;->b:[Ljava/lang/String;

    :cond_0
    iget-boolean v0, p0, LX/G7T;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, LX/G7T;->b:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "At least one authentication method must be specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Lcom/google/android/gms/auth/api/credentials/CredentialRequest;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/api/credentials/CredentialRequest;-><init>(LX/G7T;)V

    return-object v0
.end method
