.class public abstract LX/G9A;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:LX/G9A;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2321747
    new-instance v0, LX/G9B;

    invoke-direct {v0}, LX/G9B;-><init>()V

    sput-object v0, LX/G9A;->a:LX/G9A;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2321746
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/G96;[F)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v8, 0x0

    const/4 v7, -0x1

    const/4 v3, 0x1

    .line 2321708
    iget v0, p0, LX/G96;->a:I

    move v4, v0

    .line 2321709
    iget v0, p0, LX/G96;->b:I

    move v5, v0

    .line 2321710
    move v0, v1

    move v2, v3

    .line 2321711
    :goto_0
    array-length v6, p1

    if-ge v0, v6, :cond_5

    if-eqz v2, :cond_5

    .line 2321712
    aget v2, p1, v0

    float-to-int v2, v2

    .line 2321713
    add-int/lit8 v6, v0, 0x1

    aget v6, p1, v6

    float-to-int v6, v6

    .line 2321714
    if-lt v2, v7, :cond_0

    if-gt v2, v4, :cond_0

    if-lt v6, v7, :cond_0

    if-le v6, v5, :cond_1

    .line 2321715
    :cond_0
    sget-object v0, LX/G8x;->c:LX/G8x;

    move-object v0, v0

    .line 2321716
    throw v0

    .line 2321717
    :cond_1
    if-ne v2, v7, :cond_3

    .line 2321718
    aput v8, p1, v0

    move v2, v3

    .line 2321719
    :goto_1
    if-ne v6, v7, :cond_4

    .line 2321720
    add-int/lit8 v2, v0, 0x1

    aput v8, p1, v2

    move v2, v3

    .line 2321721
    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 2321722
    :cond_3
    if-ne v2, v4, :cond_d

    .line 2321723
    add-int/lit8 v2, v4, -0x1

    int-to-float v2, v2

    aput v2, p1, v0

    move v2, v3

    .line 2321724
    goto :goto_1

    .line 2321725
    :cond_4
    if-ne v6, v5, :cond_2

    .line 2321726
    add-int/lit8 v2, v0, 0x1

    add-int/lit8 v6, v5, -0x1

    int-to-float v6, v6

    aput v6, p1, v2

    move v2, v3

    .line 2321727
    goto :goto_2

    .line 2321728
    :cond_5
    array-length v0, p1

    add-int/lit8 v0, v0, -0x2

    move v2, v0

    move v0, v3

    :goto_3
    if-ltz v2, :cond_b

    if-eqz v0, :cond_b

    .line 2321729
    aget v0, p1, v2

    float-to-int v0, v0

    .line 2321730
    add-int/lit8 v6, v2, 0x1

    aget v6, p1, v6

    float-to-int v6, v6

    .line 2321731
    if-lt v0, v7, :cond_6

    if-gt v0, v4, :cond_6

    if-lt v6, v7, :cond_6

    if-le v6, v5, :cond_7

    .line 2321732
    :cond_6
    sget-object v0, LX/G8x;->c:LX/G8x;

    move-object v0, v0

    .line 2321733
    throw v0

    .line 2321734
    :cond_7
    if-ne v0, v7, :cond_9

    .line 2321735
    aput v8, p1, v2

    move v0, v3

    .line 2321736
    :goto_4
    if-ne v6, v7, :cond_a

    .line 2321737
    add-int/lit8 v0, v2, 0x1

    aput v8, p1, v0

    move v0, v3

    .line 2321738
    :cond_8
    :goto_5
    add-int/lit8 v2, v2, -0x2

    goto :goto_3

    .line 2321739
    :cond_9
    if-ne v0, v4, :cond_c

    .line 2321740
    add-int/lit8 v0, v4, -0x1

    int-to-float v0, v0

    aput v0, p1, v2

    move v0, v3

    .line 2321741
    goto :goto_4

    .line 2321742
    :cond_a
    if-ne v6, v5, :cond_8

    .line 2321743
    add-int/lit8 v0, v2, 0x1

    add-int/lit8 v6, v5, -0x1

    int-to-float v6, v6

    aput v6, p1, v0

    move v0, v3

    .line 2321744
    goto :goto_5

    .line 2321745
    :cond_b
    return-void

    :cond_c
    move v0, v1

    goto :goto_4

    :cond_d
    move v2, v1

    goto :goto_1
.end method


# virtual methods
.method public abstract a(LX/G96;IILX/G9F;)LX/G96;
.end method
