.class public LX/Gn1;
.super LX/Chj;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Gn1;


# direct methods
.method public constructor <init>(LX/8bL;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2393299
    invoke-direct {p0, p1}, LX/Chj;-><init>(LX/8bL;)V

    .line 2393300
    return-void
.end method

.method public static b(LX/0QB;)LX/Gn1;
    .locals 4

    .prologue
    .line 2393301
    sget-object v0, LX/Gn1;->a:LX/Gn1;

    if-nez v0, :cond_1

    .line 2393302
    const-class v1, LX/Gn1;

    monitor-enter v1

    .line 2393303
    :try_start_0
    sget-object v0, LX/Gn1;->a:LX/Gn1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2393304
    if-eqz v2, :cond_0

    .line 2393305
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2393306
    new-instance p0, LX/Gn1;

    invoke-static {v0}, LX/8bL;->b(LX/0QB;)LX/8bL;

    move-result-object v3

    check-cast v3, LX/8bL;

    invoke-direct {p0, v3}, LX/Gn1;-><init>(LX/8bL;)V

    .line 2393307
    move-object v0, p0

    .line 2393308
    sput-object v0, LX/Gn1;->a:LX/Gn1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2393309
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2393310
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2393311
    :cond_1
    sget-object v0, LX/Gn1;->a:LX/Gn1;

    return-object v0

    .line 2393312
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2393313
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b(LX/ChL;)V
    .locals 2

    .prologue
    .line 2393314
    invoke-super {p0, p1}, LX/Chj;->b(LX/ChL;)V

    .line 2393315
    iget-object v0, p0, LX/Chj;->a:Ljava/util/Stack;

    move-object v0, v0

    .line 2393316
    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2393317
    iget-object v0, p0, LX/Chj;->a:Ljava/util/Stack;

    move-object v0, v0

    .line 2393318
    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/ChL;

    invoke-interface {p1}, LX/ChL;->k()LX/ChZ;

    move-result-object v1

    invoke-interface {v0, v1}, LX/ChL;->a(LX/ChZ;)V

    .line 2393319
    :cond_0
    return-void
.end method
