.class public LX/G0t;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public a:LX/Fsr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/23P;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:Ljava/lang/Boolean;
    .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:Landroid/graphics/Paint;

.field public f:Landroid/graphics/Paint;

.field public final g:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2310481
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2310482
    new-instance v0, LX/G0s;

    invoke-direct {v0, p0}, LX/G0s;-><init>(LX/G0t;)V

    iput-object v0, p0, LX/G0t;->g:Landroid/view/View$OnClickListener;

    .line 2310483
    const/4 p1, 0x1

    .line 2310484
    const-class v0, LX/G0t;

    invoke-static {v0, p0}, LX/G0t;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2310485
    const v0, 0x7f03134e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2310486
    invoke-static {p0}, LX/G0t;->b(LX/G0t;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2310487
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, p1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/G0t;->e:Landroid/graphics/Paint;

    .line 2310488
    iget-object v0, p0, LX/G0t;->e:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/G0t;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2310489
    iget-object v0, p0, LX/G0t;->e:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/G0t;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0034

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2310490
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, p1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, LX/G0t;->f:Landroid/graphics/Paint;

    .line 2310491
    iget-object v0, p0, LX/G0t;->f:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/G0t;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a00e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2310492
    iget-object v0, p0, LX/G0t;->f:Landroid/graphics/Paint;

    invoke-virtual {p0}, LX/G0t;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0034

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 2310493
    :cond_0
    const v0, 0x7f0d00eb

    invoke-virtual {p0, v0}, LX/G0t;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f08156d

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->ABOUT:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    iget-object p1, p0, LX/G0t;->g:Landroid/view/View$OnClickListener;

    invoke-static {p0, v0, v1, v2, p1}, LX/G0t;->a(LX/G0t;Landroid/view/View;ILcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;Landroid/view/View$OnClickListener;)V

    .line 2310494
    const v0, 0x7f0d00ec

    invoke-virtual {p0, v0}, LX/G0t;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f08156e

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    iget-object p1, p0, LX/G0t;->g:Landroid/view/View$OnClickListener;

    invoke-static {p0, v0, v1, v2, p1}, LX/G0t;->a(LX/G0t;Landroid/view/View;ILcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;Landroid/view/View$OnClickListener;)V

    .line 2310495
    iget-object v0, p0, LX/G0t;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2310496
    const v0, 0x7f0d00ed

    invoke-virtual {p0, v0}, LX/G0t;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f081570

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->SUBSCRIBERS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    iget-object p1, p0, LX/G0t;->g:Landroid/view/View$OnClickListener;

    invoke-static {p0, v0, v1, v2, p1}, LX/G0t;->a(LX/G0t;Landroid/view/View;ILcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;Landroid/view/View$OnClickListener;)V

    .line 2310497
    :goto_0
    return-void

    .line 2310498
    :cond_1
    const v0, 0x7f0d00ed

    invoke-virtual {p0, v0}, LX/G0t;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f08156f

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    iget-object p1, p0, LX/G0t;->g:Landroid/view/View$OnClickListener;

    invoke-static {p0, v0, v1, v2, p1}, LX/G0t;->a(LX/G0t;Landroid/view/View;ILcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public static a(LX/G0t;Landroid/view/View;ILcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;Landroid/view/View$OnClickListener;)V
    .locals 3

    .prologue
    .line 2310499
    move-object v0, p1

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, LX/G0t;->b:LX/23P;

    invoke-virtual {p0}, LX/G0t;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2310500
    invoke-virtual {p1, p3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 2310501
    invoke-virtual {p1, p4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2310502
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/G0t;

    invoke-static {p0}, LX/Fsr;->a(LX/0QB;)LX/Fsr;

    move-result-object v1

    check-cast v1, LX/Fsr;

    invoke-static {p0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v2

    check-cast v2, LX/23P;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p0

    check-cast p0, LX/0ad;

    iput-object v1, p1, LX/G0t;->a:LX/Fsr;

    iput-object v2, p1, LX/G0t;->b:LX/23P;

    iput-object v3, p1, LX/G0t;->c:Ljava/lang/Boolean;

    iput-object p0, p1, LX/G0t;->d:LX/0ad;

    return-void
.end method

.method public static b(LX/G0t;)Z
    .locals 3

    .prologue
    .line 2310503
    iget-object v0, p0, LX/G0t;->d:LX/0ad;

    sget-short v1, LX/0wf;->C:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2310504
    invoke-super {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 2310505
    invoke-static {p0}, LX/G0t;->b(LX/G0t;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2310506
    :goto_0
    return-void

    .line 2310507
    :cond_0
    invoke-virtual {p0}, LX/G0t;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0dde

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 2310508
    invoke-virtual {p0}, LX/G0t;->getWidth()I

    move-result v0

    int-to-float v3, v0

    iget-object v5, p0, LX/G0t;->e:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2310509
    int-to-float v2, v6

    invoke-virtual {p0}, LX/G0t;->getWidth()I

    move-result v0

    int-to-float v3, v0

    int-to-float v4, v6

    iget-object v5, p0, LX/G0t;->f:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method
