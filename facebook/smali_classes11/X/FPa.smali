.class public LX/FPa;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final e:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "LX/FPo;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:LX/0Zb;

.field public b:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

.field public c:LX/FOz;

.field public d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2237644
    new-instance v0, LX/0P2;

    invoke-direct {v0}, LX/0P2;-><init>()V

    sget-object v1, LX/FPo;->RELEVANCE:LX/FPo;

    const-string v2, "FBPlacesSortingToggleStateRelevance"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/FPo;->DISTANCE:LX/FPo;

    const-string v2, "FBPlacesSortingToggleStateDistance"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/FPo;->RATING:LX/FPo;

    const-string v2, "FBPlacesSortingToggleStateRating"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    sget-object v1, LX/FPo;->POPULARITY:LX/FPo;

    const-string v2, "FBPlacesSortingToggleStatePopularity"

    invoke-virtual {v0, v1, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v0

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    sput-object v0, LX/FPa;->e:LX/0P1;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;LX/FOz;Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;LX/0Zb;)V
    .locals 0
    .param p1    # Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/FOz;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2237564
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2237565
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2237566
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2237567
    iput-object p1, p0, LX/FPa;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2237568
    iput-object p2, p0, LX/FPa;->c:LX/FOz;

    .line 2237569
    iput-object p3, p0, LX/FPa;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2237570
    iput-object p4, p0, LX/FPa;->a:LX/0Zb;

    .line 2237571
    return-void
.end method

.method public static a(LX/FPa;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 4

    .prologue
    .line 2237593
    iget-object v0, p0, LX/FPa;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2237594
    iget-object v1, v0, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    move-object v0, v1

    .line 2237595
    iget-object v1, v0, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->a:LX/CQB;

    move-object v0, v1

    .line 2237596
    sget-object v1, LX/CQB;->SEARCH_SUGGESTION:LX/CQB;

    if-ne v1, v0, :cond_4

    const-string v0, "android_local_set_search_module"

    .line 2237597
    :goto_0
    iget-object v1, p0, LX/FPa;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2237598
    iget-object v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    move-object v1, v2

    .line 2237599
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2237600
    iget-object v3, v1, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->c:Ljava/lang/String;

    move-object v3, v3

    .line 2237601
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 2237602
    move-object v2, v2

    .line 2237603
    iput-object v0, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2237604
    move-object v2, v2

    .line 2237605
    iget-object v3, v1, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->c:Ljava/lang/String;

    move-object v1, v3

    .line 2237606
    iput-object v1, v2, Lcom/facebook/analytics/HoneyAnalyticsEvent;->h:Ljava/lang/String;

    .line 2237607
    iget-object v1, p0, LX/FPa;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2237608
    iget-object v3, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    move-object v1, v3

    .line 2237609
    const-string v3, "session_id"

    .line 2237610
    iget-object p1, v1, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->c:Ljava/lang/String;

    move-object p1, p1

    .line 2237611
    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p1, "user_has_location_services"

    iget-object v0, p0, LX/FPa;->c:LX/FOz;

    invoke-interface {v0}, LX/FOz;->c()Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->f()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p1, "ref"

    .line 2237612
    iget-object v0, v1, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->a:LX/CQB;

    move-object v0, v0

    .line 2237613
    invoke-virtual {v0}, LX/CQB;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p1, "search_impression_source"

    .line 2237614
    iget-object v0, v1, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->b:LX/CQC;

    move-object v1, v0

    .line 2237615
    invoke-virtual {v1}, LX/CQC;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2237616
    iget-object v1, p0, LX/FPa;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2237617
    iget-object v3, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    move-object v1, v3

    .line 2237618
    iget-object v3, v1, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->d:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v3, v3

    .line 2237619
    if-eqz v3, :cond_2

    .line 2237620
    invoke-virtual {v3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v1

    .line 2237621
    if-nez v1, :cond_5

    const/4 v1, 0x0

    .line 2237622
    :goto_1
    const-string p1, "search_semantic"

    invoke-virtual {v2, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2237623
    iget-object v1, v3, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b:LX/8ci;

    move-object v1, v1

    .line 2237624
    const-string p1, "search_source_type"

    if-nez v1, :cond_0

    sget-object v1, LX/8ci;->K:LX/8ci;

    :cond_0
    invoke-virtual {v1}, LX/8ci;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2237625
    instance-of v1, v3, Lcom/facebook/search/results/model/SearchResultsMutableContext;

    if-eqz v1, :cond_1

    move-object v1, v3

    .line 2237626
    check-cast v1, Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2237627
    const-string p1, "search_query_string"

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, p1, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object p1

    const-string v0, "search_results_vertical"

    invoke-virtual {v1}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->jA_()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2237628
    :cond_1
    iget-object v1, v3, Lcom/facebook/search/results/model/SearchResultsMutableContext;->e:Lcom/facebook/search/logging/api/SearchTypeaheadSession;

    move-object v1, v1

    .line 2237629
    if-eqz v1, :cond_2

    .line 2237630
    const-string v3, "search_candidate_session_id"

    iget-object p1, v1, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p1, "search_typeahead_session_id"

    iget-object v1, v1, Lcom/facebook/search/logging/api/SearchTypeaheadSession;->b:Ljava/lang/String;

    invoke-virtual {v3, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2237631
    :cond_2
    iget-object v1, p0, LX/FPa;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    if-eqz v1, :cond_3

    .line 2237632
    iget-object v1, p0, LX/FPa;->d:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;

    .line 2237633
    iget-object v3, v1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListModel;->a:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;

    move-object v1, v3

    .line 2237634
    if-eqz v1, :cond_3

    .line 2237635
    const-string v3, "result_list_id"

    .line 2237636
    iget-object p1, v1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->m:Ljava/lang/String;

    move-object p1, p1

    .line 2237637
    invoke-virtual {v2, v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p1, "result_list_search_type"

    .line 2237638
    iget-object p0, v1, Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListData;->e:LX/FPn;

    move-object v1, p0

    .line 2237639
    invoke-virtual {v3, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2237640
    :cond_3
    move-object v0, v2

    .line 2237641
    return-object v0

    .line 2237642
    :cond_4
    const-string v0, "nearby_places_module"

    goto/16 :goto_0

    .line 2237643
    :cond_5
    const-string p1, " "

    const-string v0, "+"

    invoke-virtual {v1, p1, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private static a(LX/FPa;Ljava/lang/String;Ljava/lang/String;IIZLX/FPZ;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Z)V
    .locals 4
    .param p6    # LX/FPZ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2237584
    invoke-static {p0, p1}, LX/FPa;->a(LX/FPa;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2237585
    if-eqz p7, :cond_0

    if-eqz p8, :cond_0

    .line 2237586
    const-string v0, "photo_index"

    invoke-virtual {v1, v0, p7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v2, "photo_results_count"

    invoke-virtual {v0, v2, p8}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2237587
    :cond_0
    invoke-static {p9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2237588
    const-string v0, "results_seen"

    invoke-virtual {v1, v0, p9}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2237589
    :cond_1
    const-string v0, "tap_action"

    invoke-virtual {p6}, LX/FPZ;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "has_friend_context"

    if-eqz p5, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v2, "result_index"

    invoke-virtual {v0, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v2, "results_count"

    invoke-virtual {v0, v2, p4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v2, "page_id"

    invoke-virtual {v0, v2, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "sort_applied"

    if-eqz p10, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2237590
    iget-object v0, p0, LX/FPa;->a:LX/0Zb;

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2237591
    return-void

    .line 2237592
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2237582
    iget-object v0, p0, LX/FPa;->a:LX/0Zb;

    const-string v1, "view_result_list"

    invoke-static {p0, v1}, LX/FPa;->a(LX/FPa;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2237583
    return-void
.end method

.method public final a(ILjava/lang/String;Z)V
    .locals 11
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 2237580
    const-string v1, "places_transition_list_button_was_tapped"

    const-string v2, ""

    sget-object v6, LX/FPZ;->CELL:LX/FPZ;

    move-object v0, p0

    move v4, p1

    move v5, v3

    move-object v8, v7

    move-object v9, p2

    move v10, p3

    invoke-static/range {v0 .. v10}, LX/FPa;->a(LX/FPa;Ljava/lang/String;Ljava/lang/String;IIZLX/FPZ;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Z)V

    .line 2237581
    return-void
.end method

.method public final a(Ljava/lang/String;IIZLX/FPZ;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Z)V
    .locals 11
    .param p6    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2237578
    const-string v1, "tap_result_in_list"

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    invoke-static/range {v0 .. v10}, LX/FPa;->a(LX/FPa;Ljava/lang/String;Ljava/lang/String;IIZLX/FPZ;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Z)V

    .line 2237579
    return-void
.end method

.method public final b(ILjava/lang/String;Z)V
    .locals 11
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x0

    .line 2237576
    const-string v1, "places_transition_map_button_was_tapped"

    const-string v2, ""

    sget-object v6, LX/FPZ;->CELL:LX/FPZ;

    move-object v0, p0

    move v4, p1

    move v5, v3

    move-object v8, v7

    move-object v9, p2

    move v10, p3

    invoke-static/range {v0 .. v10}, LX/FPa;->a(LX/FPa;Ljava/lang/String;Ljava/lang/String;IIZLX/FPZ;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Z)V

    .line 2237577
    return-void
.end method

.method public final b(Ljava/lang/String;IIZLX/FPZ;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Z)V
    .locals 11
    .param p6    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2237574
    const-string v1, "nearby_places_map_result_pin_tapped"

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    invoke-static/range {v0 .. v10}, LX/FPa;->a(LX/FPa;Ljava/lang/String;Ljava/lang/String;IIZLX/FPZ;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Z)V

    .line 2237575
    return-void
.end method

.method public final c(Ljava/lang/String;IIZLX/FPZ;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Z)V
    .locals 11
    .param p6    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2237572
    const-string v1, "nearby_places_map_result_card_tapped"

    move-object v0, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move/from16 v10, p9

    invoke-static/range {v0 .. v10}, LX/FPa;->a(LX/FPa;Ljava/lang/String;Ljava/lang/String;IIZLX/FPZ;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Z)V

    .line 2237573
    return-void
.end method
