.class public LX/Ffd;
.super LX/FfQ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FfQ",
        "<",
        "Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Ffd;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2268693
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->PLACES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const v1, 0x7f082317

    invoke-direct {p0, p1, v0, v1}, LX/FfQ;-><init>(Landroid/content/res/Resources;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;I)V

    .line 2268694
    return-void
.end method

.method public static a(LX/0QB;)LX/Ffd;
    .locals 4

    .prologue
    .line 2268696
    sget-object v0, LX/Ffd;->a:LX/Ffd;

    if-nez v0, :cond_1

    .line 2268697
    const-class v1, LX/Ffd;

    monitor-enter v1

    .line 2268698
    :try_start_0
    sget-object v0, LX/Ffd;->a:LX/Ffd;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2268699
    if-eqz v2, :cond_0

    .line 2268700
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2268701
    new-instance p0, LX/Ffd;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/Ffd;-><init>(Landroid/content/res/Resources;)V

    .line 2268702
    move-object v0, p0

    .line 2268703
    sput-object v0, LX/Ffd;->a:LX/Ffd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2268704
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2268705
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2268706
    :cond_1
    sget-object v0, LX/Ffd;->a:LX/Ffd;

    return-object v0

    .line 2268707
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2268708
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()LX/Fdv;
    .locals 1

    .prologue
    .line 2268695
    new-instance v0, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;

    invoke-direct {v0}, Lcom/facebook/search/results/fragment/places/SearchResultsPlacesFragment;-><init>()V

    return-object v0
.end method
