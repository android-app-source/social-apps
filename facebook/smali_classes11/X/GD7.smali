.class public final LX/GD7;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/68J;


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/MapAreaPickerActivity;)V
    .locals 0

    .prologue
    .line 2329429
    iput-object p1, p0, LX/GD7;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/680;)V
    .locals 5

    .prologue
    .line 2329430
    iget-object v0, p0, LX/GD7;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget-object v0, v0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->z:Lcom/facebook/adinterfaces/ui/MapSpinnerView;

    sget-object v1, LX/GMY;->IDLE:LX/GMY;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/MapSpinnerView;->setState(LX/GMY;)V

    .line 2329431
    invoke-virtual {p1}, LX/680;->c()LX/692;

    move-result-object v0

    iget-object v0, v0, LX/692;->a:Lcom/facebook/android/maps/model/LatLng;

    .line 2329432
    iget-object v1, p0, LX/GD7;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget-wide v2, v0, Lcom/facebook/android/maps/model/LatLng;->a:D

    .line 2329433
    iput-wide v2, v1, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->G:D

    .line 2329434
    iget-object v1, p0, LX/GD7;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    iget-wide v2, v0, Lcom/facebook/android/maps/model/LatLng;->b:D

    .line 2329435
    iput-wide v2, v1, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->H:D

    .line 2329436
    iget-object v0, p0, LX/GD7;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    invoke-virtual {p1}, LX/680;->c()LX/692;

    move-result-object v1

    iget v1, v1, LX/692;->b:F

    .line 2329437
    iput v1, v0, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->F:F

    .line 2329438
    iget-object v0, p0, LX/GD7;->a:Lcom/facebook/adinterfaces/MapAreaPickerActivity;

    invoke-static {v0, p1}, Lcom/facebook/adinterfaces/MapAreaPickerActivity;->b(Lcom/facebook/adinterfaces/MapAreaPickerActivity;LX/680;)V

    .line 2329439
    return-void
.end method
