.class public LX/GXM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/GXL;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;LX/0Px;LX/0Px;LX/0am;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<",
            "LX/GXL;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2364021
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2364022
    iput-object p1, p0, LX/GXM;->a:Ljava/lang/String;

    .line 2364023
    iput-object p2, p0, LX/GXM;->b:LX/0Px;

    .line 2364024
    iput-object p3, p0, LX/GXM;->c:LX/0Px;

    .line 2364025
    iput-object p4, p0, LX/GXM;->d:LX/0am;

    .line 2364026
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2364027
    if-eqz p1, :cond_0

    instance-of v2, p1, LX/GXM;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 2364028
    :cond_1
    :goto_0
    return v0

    .line 2364029
    :cond_2
    if-eq p0, p1, :cond_1

    .line 2364030
    check-cast p1, LX/GXM;

    .line 2364031
    iget-object v2, p0, LX/GXM;->a:Ljava/lang/String;

    .line 2364032
    iget-object v3, p1, LX/GXM;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2364033
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GXM;->b:LX/0Px;

    .line 2364034
    iget-object v3, p1, LX/GXM;->b:LX/0Px;

    move-object v3, v3

    .line 2364035
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GXM;->c:LX/0Px;

    .line 2364036
    iget-object v3, p1, LX/GXM;->c:LX/0Px;

    move-object v3, v3

    .line 2364037
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/GXM;->d:LX/0am;

    .line 2364038
    iget-object v3, p1, LX/GXM;->d:LX/0am;

    move-object v3, v3

    .line 2364039
    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2364040
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/GXM;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/GXM;->b:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/GXM;->c:LX/0Px;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/GXM;->d:LX/0am;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
