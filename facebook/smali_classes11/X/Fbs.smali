.class public LX/Fbs;
.super LX/FbD;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FbD",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Fbs;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2261288
    invoke-direct {p0}, LX/FbD;-><init>()V

    .line 2261289
    return-void
.end method

.method public static a(LX/0QB;)LX/Fbs;
    .locals 3

    .prologue
    .line 2261252
    sget-object v0, LX/Fbs;->a:LX/Fbs;

    if-nez v0, :cond_1

    .line 2261253
    const-class v1, LX/Fbs;

    monitor-enter v1

    .line 2261254
    :try_start_0
    sget-object v0, LX/Fbs;->a:LX/Fbs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2261255
    if-eqz v2, :cond_0

    .line 2261256
    :try_start_1
    new-instance v0, LX/Fbs;

    invoke-direct {v0}, LX/Fbs;-><init>()V

    .line 2261257
    move-object v0, v0

    .line 2261258
    sput-object v0, LX/Fbs;->a:LX/Fbs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2261259
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2261260
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2261261
    :cond_1
    sget-object v0, LX/Fbs;->a:LX/Fbs;

    return-object v0

    .line 2261262
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2261263
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
    .locals 11
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261264
    const/4 v3, 0x0

    const/4 v6, 0x0

    .line 2261265
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->o()LX/8dH;

    move-result-object v10

    .line 2261266
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->q()Ljava/lang/String;

    move-result-object v2

    .line 2261267
    if-eqz v10, :cond_6

    .line 2261268
    invoke-interface {v10}, LX/8dH;->j()LX/8dG;

    move-result-object v0

    .line 2261269
    if-eqz v0, :cond_6

    .line 2261270
    invoke-interface {v0}, LX/8dG;->a()Ljava/lang/String;

    move-result-object v0

    .line 2261271
    :goto_0
    move-object v4, v0

    .line 2261272
    if-eqz v10, :cond_0

    invoke-interface {v10}, LX/8dH;->c()Ljava/lang/String;

    move-result-object v5

    .line 2261273
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->n()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    .line 2261274
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2261275
    :goto_2
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->o()LX/8dH;

    move-result-object v0

    invoke-interface {v0}, LX/8dH;->fn_()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    .line 2261276
    if-eqz v10, :cond_2

    invoke-interface {v10}, LX/8dH;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v9

    .line 2261277
    :goto_3
    invoke-interface {v10}, LX/8dH;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-interface {v10}, LX/8dH;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFiltersFragmentModel;->a()LX/0Px;

    move-result-object v6

    .line 2261278
    :goto_4
    invoke-interface {v10}, LX/8dH;->fo_()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-interface {v10}, LX/8dH;->fo_()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-interface {v10}, LX/8dH;->fo_()Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/results/protocol/explore/SearchResultsSerpTabsModuleModels$SearchResultsSerpTabsModuleModel;->a()LX/0Px;

    move-result-object v10

    .line 2261279
    :goto_5
    if-eqz v1, :cond_5

    if-eqz v5, :cond_5

    if-eqz v2, :cond_5

    new-instance v0, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-direct/range {v0 .. v10}, Lcom/facebook/search/results/model/unit/SearchResultsSeeMorePostsUnit;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0Px;)V

    :goto_6
    return-object v0

    :cond_0
    move-object v5, v3

    .line 2261280
    goto :goto_1

    .line 2261281
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-object v7, v0

    goto :goto_2

    :cond_2
    move-object v9, v3

    .line 2261282
    goto :goto_3

    .line 2261283
    :cond_3
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v6, v0

    .line 2261284
    goto :goto_4

    .line 2261285
    :cond_4
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v10, v0

    .line 2261286
    goto :goto_5

    :cond_5
    move-object v0, v3

    .line 2261287
    goto :goto_6

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0
.end method
