.class public final LX/Fwj;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;)V
    .locals 0

    .prologue
    .line 2303592
    iput-object p1, p0, LX/Fwj;->a:Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 4

    .prologue
    .line 2303593
    iget-object v0, p0, LX/Fwj;->a:Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;

    .line 2303594
    new-instance v1, LX/4Ii;

    invoke-direct {v1}, LX/4Ii;-><init>()V

    .line 2303595
    iget-object v2, v0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->o:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 2303596
    const-string v3, "bio"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2303597
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 2303598
    const-string p0, "publish_bio_feed_story"

    invoke-virtual {v1, p0, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2303599
    new-instance v3, LX/5yC;

    invoke-direct {v3}, LX/5yC;-><init>()V

    move-object v3, v3

    .line 2303600
    const-string p0, "input"

    invoke-virtual {v3, p0, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2303601
    invoke-static {v3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    .line 2303602
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    const/4 p0, 0x0

    const p1, 0x7f080ffb

    invoke-virtual {v0, p1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p1

    const/4 p2, 0x1

    invoke-static {v3, p0, p1, p2}, LX/4BY;->a(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)LX/4BY;

    move-result-object v3

    .line 2303603
    iget-object p0, v0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->f:LX/1mR;

    sget-object p1, Lcom/facebook/timeline/header/intro/edit/BioQueryExecutor;->a:Ljava/util/Set;

    invoke-virtual {p0, p1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2303604
    iget-object p0, v0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->a:LX/0tX;

    invoke-virtual {p0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2303605
    new-instance p0, LX/Fwm;

    invoke-direct {p0, v0, v2, v3}, LX/Fwm;-><init>(Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;Ljava/lang/String;LX/4BY;)V

    iget-object v2, v0, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->b:Ljava/util/concurrent/Executor;

    invoke-static {v1, p0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2303606
    return-void
.end method
