.class public final LX/G09;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/friends/model/FriendRequest;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Z

.field public final synthetic c:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;ZZ)V
    .locals 0

    .prologue
    .line 2308524
    iput-object p1, p0, LX/G09;->c:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    iput-boolean p2, p0, LX/G09;->a:Z

    iput-boolean p3, p0, LX/G09;->b:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/friends/model/FriendRequest;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2308525
    iget-boolean v0, p0, LX/G09;->a:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/G09;->b:Z

    if-eqz v0, :cond_1

    .line 2308526
    :cond_0
    iget-object v0, p0, LX/G09;->c:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2308527
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/FriendRequest;

    .line 2308528
    new-instance v1, LX/Ewh;

    invoke-direct {v1}, LX/Ewh;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/friends/model/FriendRequest;->a()J

    move-result-wide v4

    .line 2308529
    iput-wide v4, v1, LX/Euq;->a:J

    .line 2308530
    move-object v1, v1

    .line 2308531
    check-cast v1, LX/Ewh;

    invoke-virtual {v0}, Lcom/facebook/friends/model/FriendRequest;->b()Ljava/lang/String;

    move-result-object v4

    .line 2308532
    iput-object v4, v1, LX/Euq;->d:Ljava/lang/String;

    .line 2308533
    move-object v1, v1

    .line 2308534
    check-cast v1, LX/Ewh;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2308535
    iput-object v4, v1, LX/Euq;->h:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2308536
    move-object v1, v1

    .line 2308537
    check-cast v1, LX/Ewh;

    invoke-virtual {v0}, Lcom/facebook/friends/model/FriendRequest;->d()Ljava/lang/String;

    move-result-object v0

    .line 2308538
    iput-object v0, v1, LX/Euq;->c:Ljava/lang/String;

    .line 2308539
    move-object v0, v1

    .line 2308540
    check-cast v0, LX/Ewh;

    const/4 v1, 0x1

    .line 2308541
    iput-boolean v1, v0, LX/Eur;->a:Z

    .line 2308542
    move-object v0, v0

    .line 2308543
    check-cast v0, LX/Ewh;

    iget-object v1, p0, LX/G09;->c:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    iget-object v1, v1, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->r:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2308544
    iput-wide v4, v0, LX/Euq;->b:J

    .line 2308545
    move-object v0, v0

    .line 2308546
    check-cast v0, LX/Ewh;

    invoke-virtual {v0}, LX/Ewh;->c()LX/Ewi;

    move-result-object v0

    .line 2308547
    iget-object v1, p0, LX/G09;->c:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    iget-object v1, v1, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->z:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2308548
    :cond_2
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v1, v0

    .line 2308549
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2308550
    iget-object v0, p0, LX/G09;->c:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    iget-object v7, v0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->u:LX/EwG;

    iget-object v0, p0, LX/G09;->c:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->z:Ljava/util/List;

    const/4 v2, 0x0

    const/4 v6, -0x1

    move v4, v3

    move v5, v3

    invoke-static/range {v0 .. v6}, LX/Ewd;->a(Ljava/util/List;Ljava/util/List;LX/Ewf;ZZZI)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v7, v0}, LX/EwG;->a(Ljava/util/List;)V

    .line 2308551
    :cond_3
    iget-object v0, p0, LX/G09;->c:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2308552
    iget-object v0, p0, LX/G09;->c:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    invoke-static {v0}, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->n(Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;)V

    .line 2308553
    :goto_1
    iget-object v0, p0, LX/G09;->c:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->s:LX/2dj;

    invoke-virtual {v0}, LX/2dj;->c()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2308554
    iget-object v0, p0, LX/G09;->c:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->u:LX/EwG;

    invoke-virtual {v0, v3}, LX/EwG;->a(Z)V

    .line 2308555
    :cond_4
    iget-object v0, p0, LX/G09;->c:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->x:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {v0}, LX/62l;->f()V

    .line 2308556
    return-void

    .line 2308557
    :cond_5
    iget-object v0, p0, LX/G09;->c:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    invoke-static {v0}, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->o(Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;)V

    goto :goto_1
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2308520
    iget-object v0, p0, LX/G09;->c:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->x:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {v0}, LX/62l;->g()V

    .line 2308521
    iget-object v0, p0, LX/G09;->c:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->u:LX/EwG;

    invoke-virtual {v0, v4}, LX/EwG;->a(Z)V

    .line 2308522
    const-class v0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    const-string v1, "Could not fetch data for FBID %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/G09;->c:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    iget-object v3, v3, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->r:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2308523
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2308519
    check-cast p1, Ljava/util/List;

    invoke-direct {p0, p1}, LX/G09;->a(Ljava/util/List;)V

    return-void
.end method
