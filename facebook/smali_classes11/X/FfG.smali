.class public LX/FfG;
.super LX/0gG;
.source ""


# instance fields
.field public a:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Landroid/content/Context;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Ff8;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/FfJ;

.field public final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Cw8;",
            "LX/Ff8;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Cw8;",
            "LX/Fje;",
            ">;"
        }
    .end annotation
.end field

.field public final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Cw8;",
            "Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Cw8;",
            "Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0gc;

.field public j:LX/Cvm;

.field private k:LX/D0Q;

.field private l:LX/Ff9;

.field public m:Landroid/support/v4/view/ViewPager;

.field private n:LX/FgS;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Or;LX/FfK;LX/0gc;Landroid/support/v4/view/ViewPager;LX/Ff9;LX/Cvm;LX/FgS;LX/D0Q;)V
    .locals 1
    .param p4    # LX/0gc;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Landroid/support/v4/view/ViewPager;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/Ff9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # LX/Cvm;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Or",
            "<",
            "LX/Ff8;",
            ">;",
            "LX/FfK;",
            "LX/0gc;",
            "Landroid/support/v4/view/ViewPager;",
            "Lcom/facebook/search/results/fragment/pps/SeeMoreResultsPagerAdapter$Listener;",
            "LX/Cvm;",
            "LX/FgS;",
            "LX/D0Q;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2267801
    invoke-direct {p0}, LX/0gG;-><init>()V

    .line 2267802
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/FfG;->e:Ljava/util/Map;

    .line 2267803
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/FfG;->f:Ljava/util/Map;

    .line 2267804
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/FfG;->g:Ljava/util/Map;

    .line 2267805
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/FfG;->h:Ljava/util/Map;

    .line 2267806
    iput-object p1, p0, LX/FfG;->b:Landroid/content/Context;

    .line 2267807
    iput-object p2, p0, LX/FfG;->c:LX/0Or;

    .line 2267808
    invoke-virtual {p3, p0}, LX/FfK;->a(LX/FfG;)LX/FfJ;

    move-result-object v0

    iput-object v0, p0, LX/FfG;->d:LX/FfJ;

    .line 2267809
    iput-object p4, p0, LX/FfG;->i:LX/0gc;

    .line 2267810
    iput-object p5, p0, LX/FfG;->m:Landroid/support/v4/view/ViewPager;

    .line 2267811
    iput-object p6, p0, LX/FfG;->l:LX/Ff9;

    .line 2267812
    iget-object v0, p0, LX/FfG;->d:LX/FfJ;

    .line 2267813
    iput-object p7, v0, LX/FfJ;->k:LX/Cvm;

    .line 2267814
    iput-object p7, p0, LX/FfG;->j:LX/Cvm;

    .line 2267815
    iput-object p8, p0, LX/FfG;->n:LX/FgS;

    .line 2267816
    iput-object p9, p0, LX/FfG;->k:LX/D0Q;

    .line 2267817
    return-void
.end method

.method public static a$redex0(LX/FfG;LX/Cw8;LX/Ff8;)V
    .locals 6

    .prologue
    .line 2267772
    iget-object v0, p0, LX/FfG;->d:LX/FfJ;

    invoke-virtual {v0, p1}, LX/FfJ;->c(LX/Cw8;)LX/0Px;

    move-result-object v0

    .line 2267773
    const/4 v5, 0x0

    .line 2267774
    iget-object v1, p0, LX/FfG;->f:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Fje;

    .line 2267775
    if-eqz v1, :cond_2

    .line 2267776
    :goto_0
    move-object v1, v1

    .line 2267777
    invoke-virtual {p2, v0}, LX/Ff8;->a(LX/0Px;)V

    .line 2267778
    iget-object v2, v1, LX/Fje;->q:LX/0g8;

    move-object v2, v2

    .line 2267779
    if-nez v0, :cond_1

    .line 2267780
    sget-object v0, LX/EQG;->LOADING:LX/EQG;

    invoke-virtual {v1, v0}, LX/Fje;->setState(LX/EQG;)V

    .line 2267781
    :cond_0
    :goto_1
    invoke-interface {v2, p2}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2267782
    new-instance v0, LX/FfB;

    invoke-direct {v0, p0, p1}, LX/FfB;-><init>(LX/FfG;LX/Cw8;)V

    invoke-interface {v2, v0}, LX/0g8;->a(LX/2ii;)V

    .line 2267783
    const v0, -0x24708407

    invoke-static {p2, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2267784
    return-void

    .line 2267785
    :cond_1
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2267786
    sget-object v0, LX/EQG;->EMPTY:LX/EQG;

    invoke-virtual {v1, v0}, LX/Fje;->setState(LX/EQG;)V

    goto :goto_1

    .line 2267787
    :cond_2
    new-instance v3, LX/Fje;

    iget-object v1, p0, LX/FfG;->b:Landroid/content/Context;

    invoke-static {v1, v5}, LX/D0Q;->a(Landroid/content/Context;Landroid/os/Bundle;)Landroid/view/ContextThemeWrapper;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v3, v1, p1, v2}, LX/Fje;-><init>(Landroid/content/Context;LX/Cw8;Z)V

    .line 2267788
    sget-object v1, LX/Cw8;->People:LX/Cw8;

    if-ne p1, v1, :cond_3

    .line 2267789
    invoke-virtual {v3}, LX/Fje;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 2267790
    const v1, 0x7f0312b9

    invoke-virtual {v2, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;

    .line 2267791
    const v4, 0x7f0312b9

    invoke-virtual {v2, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;

    .line 2267792
    new-instance v4, LX/FfD;

    invoke-direct {v4, p0, p1}, LX/FfD;-><init>(LX/FfG;LX/Cw8;)V

    invoke-virtual {v1, v4}, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->setOnFilterSelectedListener(LX/Fd4;)V

    .line 2267793
    new-instance v4, LX/FfE;

    invoke-direct {v4, p0, p1}, LX/FfE;-><init>(LX/FfG;LX/Cw8;)V

    invoke-virtual {v2, v4}, Lcom/facebook/search/results/filters/ui/SearchResultsPageFilter;->setOnFilterSelectedListener(LX/Fd4;)V

    .line 2267794
    invoke-virtual {v3, v1}, LX/Fje;->a(Landroid/view/View;)V

    .line 2267795
    iget-object v4, v3, LX/Fje;->o:Landroid/widget/LinearLayout;

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 2267796
    iget-object v4, p0, LX/FfG;->g:Ljava/util/Map;

    invoke-interface {v4, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267797
    iget-object v1, p0, LX/FfG;->h:Ljava/util/Map;

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267798
    :cond_3
    invoke-static {p0}, LX/FfG;->i(LX/FfG;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/Fje;->setTextViewQueryString(Ljava/lang/String;)V

    .line 2267799
    iget-object v1, p0, LX/FfG;->f:Ljava/util/Map;

    invoke-interface {v1, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v3

    .line 2267800
    goto/16 :goto_0
.end method

.method public static a$redex0(LX/FfG;Lcom/facebook/search/model/SeeMoreResultPageUnit;LX/Cw8;)V
    .locals 8

    .prologue
    .line 2267757
    iget-object v0, p0, LX/FfG;->l:LX/Ff9;

    if-nez v0, :cond_0

    .line 2267758
    :goto_0
    return-void

    .line 2267759
    :cond_0
    iget-object v0, p0, LX/FfG;->l:LX/Ff9;

    invoke-direct {p0, p2}, LX/FfG;->e(LX/Cw8;)LX/Ff8;

    move-result-object v1

    .line 2267760
    iget-object p0, v1, LX/Ff8;->e:LX/0Px;

    move-object v1, p0

    .line 2267761
    iget-object v2, v0, LX/Ff9;->a:Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;

    iget-object v2, v2, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->c:LX/Cvm;

    iget-object v3, v0, LX/Ff9;->a:Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;

    invoke-static {v3}, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->c(Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;)Ljava/lang/String;

    move-result-object v3

    .line 2267762
    iget-object v4, v2, LX/Cvm;->f:Ljava/lang/String;

    invoke-static {v4}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2267763
    invoke-virtual {v2, v1}, LX/CvH;->a(Ljava/util/List;)LX/162;

    move-result-object v4

    .line 2267764
    const-string v5, "click"

    invoke-static {v2, v5}, LX/Cvm;->a(LX/Cvm;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "action"

    const-string v7, "entity_selected"

    invoke-virtual {v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "filter_type"

    invoke-virtual {p2}, LX/Cw8;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "result_type"

    invoke-virtual {p1}, Lcom/facebook/search/model/SeeMoreResultPageUnit;->o()Ljava/lang/String;

    move-result-object v7

    sget-object p0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, p0}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "object_id"

    invoke-virtual {p1}, Lcom/facebook/search/model/SeeMoreResultPageUnit;->n()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "query"

    invoke-virtual {v5, v6, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "selected_position"

    invoke-static {p1, v1}, LX/CvH;->a(Lcom/facebook/search/model/TypeaheadUnit;LX/0Px;)I

    move-result v7

    invoke-virtual {v5, v6, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    const-string v6, "end_session_candidate_results"

    invoke-virtual {v5, v6, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v4

    .line 2267765
    iget-object v5, v2, LX/Cvm;->b:LX/0Zb;

    invoke-interface {v5, v4}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2267766
    iget v4, v2, LX/Cvm;->h:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v2, LX/Cvm;->h:I

    .line 2267767
    iget-object v2, v0, LX/Ff9;->a:Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;

    iget-object v2, v2, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->d:LX/EQ6;

    const/4 v3, 0x0

    iget-object v4, v0, LX/Ff9;->a:Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;

    invoke-static {v4}, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->c(Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, p1, v4}, LX/EQ6;->a(Lcom/facebook/search/api/GraphSearchQuery;Lcom/facebook/search/model/TypeaheadUnit;Ljava/lang/String;)V

    .line 2267768
    iget-object v2, v0, LX/Ff9;->a:Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;

    iget-object v2, v2, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->e:LX/2SY;

    invoke-virtual {v2, p1}, LX/2SY;->a(Lcom/facebook/search/model/SeeMoreResultPageUnit;)V

    .line 2267769
    iget-object v2, v0, LX/Ff9;->a:Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;

    iget-object v2, v2, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->f:LX/FZb;

    if-eqz v2, :cond_1

    .line 2267770
    iget-object v2, v0, LX/Ff9;->a:Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;

    iget-object v2, v2, Lcom/facebook/search/results/fragment/pps/SeeMoreResultsListFragment;->f:LX/FZb;

    invoke-virtual {v2, p1}, LX/FZb;->a(Lcom/facebook/search/model/SeeMoreResultPageUnit;)V

    .line 2267771
    :cond_1
    goto/16 :goto_0
.end method

.method public static b(LX/FfG;LX/Cw8;)V
    .locals 2

    .prologue
    .line 2267750
    iget-object v0, p0, LX/FfG;->d:LX/FfJ;

    invoke-virtual {v0, p1}, LX/FfJ;->c(LX/Cw8;)LX/0Px;

    move-result-object v0

    .line 2267751
    if-nez v0, :cond_1

    .line 2267752
    iget-object v0, p0, LX/FfG;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fje;

    .line 2267753
    if-eqz v0, :cond_0

    .line 2267754
    sget-object v1, LX/EQG;->LOADING:LX/EQG;

    invoke-virtual {v0, v1}, LX/Fje;->setState(LX/EQG;)V

    .line 2267755
    :cond_0
    iget-object v0, p0, LX/FfG;->d:LX/FfJ;

    invoke-static {p0}, LX/FfG;->i(LX/FfG;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/FfJ;->a(Ljava/lang/String;LX/Cw8;)V

    .line 2267756
    :cond_1
    return-void
.end method

.method private e(LX/Cw8;)LX/Ff8;
    .locals 2

    .prologue
    .line 2267745
    iget-object v0, p0, LX/FfG;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2267746
    iget-object v0, p0, LX/FfG;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ff8;

    .line 2267747
    :goto_0
    return-object v0

    .line 2267748
    :cond_0
    iget-object v0, p0, LX/FfG;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ff8;

    .line 2267749
    iget-object v1, p0, LX/FfG;->e:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public static i(LX/FfG;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2267716
    iget-object v0, p0, LX/FfG;->n:LX/FgS;

    .line 2267717
    iget-object p0, v0, LX/FgS;->a:Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;

    move-object v0, p0

    .line 2267718
    iget-object p0, v0, Lcom/facebook/search/titlebar/GraphSearchTitleSearchBox;->g:Lcom/facebook/ui/search/SearchEditText;

    move-object v0, p0

    .line 2267719
    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final G_(I)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    .line 2267734
    invoke-static {p1}, LX/Cw8;->getCoreFilterTypeAt(I)LX/Cw8;

    move-result-object v0

    .line 2267735
    sget-object v1, LX/FfF;->a:[I

    invoke-virtual {v0}, LX/Cw8;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2267736
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unimplemented PPS tab of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2267737
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2267738
    :pswitch_0
    const v0, 0x7f082315

    .line 2267739
    :goto_0
    iget-object v1, p0, LX/FfG;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2267740
    :pswitch_1
    const v0, 0x7f082314

    goto :goto_0

    .line 2267741
    :pswitch_2
    const v0, 0x7f082313

    goto :goto_0

    .line 2267742
    :pswitch_3
    const v0, 0x7f082312

    goto :goto_0

    .line 2267743
    :pswitch_4
    const v0, 0x7f082311

    goto :goto_0

    .line 2267744
    :pswitch_5
    const v0, 0x7f082319

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2267728
    invoke-static {p2}, LX/Cw8;->getCoreFilterTypeAt(I)LX/Cw8;

    move-result-object v1

    .line 2267729
    invoke-direct {p0, v1}, LX/FfG;->e(LX/Cw8;)LX/Ff8;

    move-result-object v0

    .line 2267730
    invoke-static {p0, v1, v0}, LX/FfG;->a$redex0(LX/FfG;LX/Cw8;LX/Ff8;)V

    .line 2267731
    iget-object v0, p0, LX/FfG;->f:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2267732
    invoke-static {p0, v1}, LX/FfG;->b(LX/FfG;LX/Cw8;)V

    .line 2267733
    iget-object v0, p0, LX/FfG;->f:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 2267725
    check-cast p3, LX/Fje;

    .line 2267726
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2267727
    return-void
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2267724
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2267723
    invoke-static {}, LX/Cw8;->getCoreFilterTypeLength()I

    move-result v0

    return v0
.end method

.method public final f()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/SeeMoreResultPageUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2267720
    iget-object v0, p0, LX/FfG;->m:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    invoke-static {v0}, LX/Cw8;->getCoreFilterTypeAt(I)LX/Cw8;

    move-result-object v0

    invoke-direct {p0, v0}, LX/FfG;->e(LX/Cw8;)LX/Ff8;

    move-result-object v0

    .line 2267721
    iget-object p0, v0, LX/Ff8;->e:LX/0Px;

    move-object v0, p0

    .line 2267722
    return-object v0
.end method
