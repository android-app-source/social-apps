.class public final LX/GjZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

.field public b:Landroid/view/View;

.field public c:Landroid/view/Window;

.field public d:Landroid/graphics/drawable/Drawable;

.field private e:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Lcom/facebook/greetingcards/render/FoldingPopoverFragment;Landroid/view/View;Landroid/view/Window;)V
    .locals 1

    .prologue
    .line 2387859
    iput-object p1, p0, LX/GjZ;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2387860
    iput-object p2, p0, LX/GjZ;->b:Landroid/view/View;

    .line 2387861
    if-eqz p3, :cond_0

    .line 2387862
    iput-object p3, p0, LX/GjZ;->c:Landroid/view/Window;

    .line 2387863
    iget-object v0, p0, LX/GjZ;->c:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/GjZ;->d:Landroid/graphics/drawable/Drawable;

    .line 2387864
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 2387865
    iget-object v0, p0, LX/GjZ;->e:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 2387866
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget-object v1, p0, LX/GjZ;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, LX/GjZ;->e:Landroid/graphics/drawable/Drawable;

    .line 2387867
    iget-object v0, p0, LX/GjZ;->e:Landroid/graphics/drawable/Drawable;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 2387868
    :cond_0
    iget-object v0, p0, LX/GjZ;->e:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2387869
    iget-object v0, p0, LX/GjZ;->c:Landroid/view/Window;

    if-eqz v0, :cond_0

    .line 2387870
    iget-object v0, p0, LX/GjZ;->c:Landroid/view/Window;

    const v1, 0x106000c

    invoke-virtual {v0, v1}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 2387871
    :cond_0
    return-void
.end method
