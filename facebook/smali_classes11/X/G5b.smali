.class public abstract LX/G5b;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/3fr;

.field private final b:LX/0TD;

.field public final c:LX/2RQ;


# direct methods
.method public constructor <init>(LX/3fr;LX/0TD;LX/2RQ;)V
    .locals 0

    .prologue
    .line 2319001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2319002
    iput-object p1, p0, LX/G5b;->a:LX/3fr;

    .line 2319003
    iput-object p2, p0, LX/G5b;->b:LX/0TD;

    .line 2319004
    iput-object p3, p0, LX/G5b;->c:LX/2RQ;

    .line 2319005
    return-void
.end method


# virtual methods
.method public abstract a()LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QK",
            "<",
            "LX/6N1;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;>;"
        }
    .end annotation
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2319006
    iget-object v0, p0, LX/G5b;->b:LX/0TD;

    new-instance v1, LX/G5d;

    invoke-direct {v1, p0, p1}, LX/G5d;-><init>(LX/G5b;Lcom/facebook/search/api/GraphSearchQuery;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public abstract b()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/3Oq;",
            ">;"
        }
    .end annotation
.end method
