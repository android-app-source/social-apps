.class public LX/GgG;
.super Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;
.source ""

# interfaces
.implements LX/2eZ;


# instance fields
.field public final a:LX/GgF;

.field public b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2378848
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/GgG;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2378849
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2378850
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2378851
    const v0, 0x7f030469

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2378852
    new-instance v0, LX/GgF;

    invoke-direct {v0, p0}, LX/GgF;-><init>(LX/GgG;)V

    iput-object v0, p0, LX/GgG;->a:LX/GgF;

    .line 2378853
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 2378854
    iget-boolean v0, p0, LX/GgG;->b:Z

    return v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x732dd6d1

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2378855
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onAttachedToWindow()V

    .line 2378856
    const/4 v1, 0x1

    .line 2378857
    iput-boolean v1, p0, LX/GgG;->b:Z

    .line 2378858
    const/16 v1, 0x2d

    const v2, 0x56a91469

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x37908bae

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2378859
    invoke-super {p0}, Lcom/facebook/widget/pageritemwrapper/PagerItemWrapperLayout;->onDetachedFromWindow()V

    .line 2378860
    const/4 v1, 0x0

    .line 2378861
    iput-boolean v1, p0, LX/GgG;->b:Z

    .line 2378862
    const/16 v1, 0x2d

    const v2, 0x55fb52ec

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
