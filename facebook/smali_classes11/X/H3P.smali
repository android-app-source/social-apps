.class public LX/H3P;
.super Landroid/widget/BaseAdapter;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/03V;

.field public final c:Landroid/content/Context;

.field private final d:Landroid/view/LayoutInflater;

.field private final e:LX/BVS;

.field private final f:LX/H3b;

.field private g:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/common/SearchSuggestion;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;",
            ">;"
        }
    .end annotation
.end field

.field private i:Landroid/location/Location;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2420972
    const-class v0, LX/H3P;

    sput-object v0, LX/H3P;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/H3b;LX/BVS;LX/03V;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2420961
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2420962
    iput-object p3, p0, LX/H3P;->b:LX/03V;

    .line 2420963
    iput-object p4, p0, LX/H3P;->c:Landroid/content/Context;

    .line 2420964
    iput-object p2, p0, LX/H3P;->e:LX/BVS;

    .line 2420965
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2420966
    iput-object v0, p0, LX/H3P;->g:LX/0Px;

    .line 2420967
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2420968
    iput-object v0, p0, LX/H3P;->h:LX/0Px;

    .line 2420969
    iget-object v0, p0, LX/H3P;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/H3P;->d:Landroid/view/LayoutInflater;

    .line 2420970
    iput-object p1, p0, LX/H3P;->f:LX/H3b;

    .line 2420971
    return-void
.end method

.method private a(LX/H3O;)I
    .locals 3

    .prologue
    .line 2420957
    sget-object v0, LX/H3N;->a:[I

    invoke-virtual {p1}, LX/H3O;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2420958
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected RowType type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2420959
    :pswitch_0
    const/4 v0, 0x0

    .line 2420960
    :goto_0
    return v0

    :pswitch_1
    iget-object v0, p0, LX/H3P;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private a(I)LX/H3O;
    .locals 2

    .prologue
    .line 2420950
    iget-object v0, p0, LX/H3P;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2420951
    sget-object v0, LX/H3O;->SEARCH_SUGGESTION:LX/H3O;

    .line 2420952
    :goto_0
    return-object v0

    .line 2420953
    :cond_0
    iget-object v0, p0, LX/H3P;->h:LX/0Px;

    iget-object v1, p0, LX/H3P;->g:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;

    .line 2420954
    iget-object v0, v0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->layout:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2420955
    sget-object v0, LX/H3O;->PLACE_INFO:LX/H3O;

    goto :goto_0

    .line 2420956
    :cond_1
    sget-object v0, LX/H3O;->EXPERIMENTAL_PLACE_INFO:LX/H3O;

    goto :goto_0
.end method

.method private a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    .line 2420943
    if-eqz p2, :cond_0

    .line 2420944
    :goto_0
    return-object p2

    .line 2420945
    :cond_0
    invoke-virtual {p0, p1}, LX/H3P;->getItem(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;

    .line 2420946
    :try_start_0
    iget-object v0, p0, LX/H3P;->e:LX/BVS;

    iget-object v1, v5, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->layout:Ljava/lang/String;

    iget-object v2, p0, LX/H3P;->c:Landroid/content/Context;

    invoke-virtual {v0, v1, p3, v2}, LX/BVS;->a(Ljava/lang/String;Landroid/view/ViewGroup;Landroid/content/Context;)Landroid/view/View;

    move-result-object v4

    .line 2420947
    invoke-static {p0, p1}, LX/H3P;->b(LX/H3P;I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/view/View;->setBackgroundResource(I)V

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move v3, p1

    .line 2420948
    invoke-static/range {v0 .. v5}, LX/H3P;->a(LX/H3P;Landroid/view/View;Landroid/view/ViewGroup;ILandroid/view/View;Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;)Landroid/view/View;
    :try_end_0
    .catch LX/BVR; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p2

    goto :goto_0

    .line 2420949
    :catch_0
    invoke-static {p0, p2, p3, p1}, LX/H3P;->a(LX/H3P;Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p2

    goto :goto_0
.end method

.method private static a(LX/H3P;Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;
    .locals 4

    .prologue
    .line 2420938
    iget-object v0, p0, LX/H3P;->b:LX/03V;

    const-string v1, "EXPERIMENTAL_INFLATED_VIEWS"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "inflation failed for item id = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    const/4 v2, 0x0

    .line 2420939
    iput-boolean v2, v1, LX/0VK;->d:Z

    .line 2420940
    move-object v1, v1

    .line 2420941
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2420942
    invoke-direct {p0, p3, p1, p2}, LX/H3P;->b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/H3P;Landroid/view/View;Landroid/view/ViewGroup;ILandroid/view/View;Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;)Landroid/view/View;
    .locals 6

    .prologue
    .line 2420918
    iget-object v0, p0, LX/H3P;->i:Landroid/location/Location;

    if-nez v0, :cond_0

    .line 2420919
    :goto_0
    return-object p4

    .line 2420920
    :cond_0
    :try_start_0
    iget-object v0, p0, LX/H3P;->f:LX/H3b;

    iget-object v1, p0, LX/H3P;->e:LX/BVS;

    iget-object v2, p5, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->typeaheadPlace:Lcom/facebook/nearby/model/TypeaheadPlace;

    iget-object v2, v2, Lcom/facebook/nearby/model/TypeaheadPlace;->e:Lcom/facebook/graphql/model/GraphQLLocation;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLLocation;->a()D

    move-result-wide v2

    iget-object v4, p5, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->typeaheadPlace:Lcom/facebook/nearby/model/TypeaheadPlace;

    iget-object v4, v4, Lcom/facebook/nearby/model/TypeaheadPlace;->e:Lcom/facebook/graphql/model/GraphQLLocation;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLLocation;->b()D

    move-result-wide v4

    invoke-static {v2, v3, v4, v5}, LX/6aA;->a(DD)Landroid/location/Location;

    move-result-object v2

    iget-object v3, p0, LX/H3P;->i:Landroid/location/Location;

    invoke-virtual {v0, v1, p4, v2, v3}, LX/H3b;->a(LX/BVS;Landroid/view/View;Landroid/location/Location;Landroid/location/Location;)V
    :try_end_0
    .catch LX/H3a; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2420921
    :catch_0
    invoke-static {p0, p1, p2, p3}, LX/H3P;->a(LX/H3P;Landroid/view/View;Landroid/view/ViewGroup;I)Landroid/view/View;

    move-result-object p4

    goto :goto_0
.end method

.method private static b(LX/H3P;I)I
    .locals 1

    .prologue
    .line 2420932
    if-nez p1, :cond_0

    .line 2420933
    const v0, 0x7f0210ca

    .line 2420934
    :goto_0
    return v0

    .line 2420935
    :cond_0
    invoke-virtual {p0}, LX/H3P;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    .line 2420936
    const v0, 0x7f0210c9

    goto :goto_0

    .line 2420937
    :cond_1
    const v0, 0x7f0210c8

    goto :goto_0
.end method

.method private b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2420973
    if-eqz p2, :cond_0

    instance-of v0, p2, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;

    if-eqz v0, :cond_0

    .line 2420974
    check-cast p2, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;

    .line 2420975
    :goto_0
    invoke-virtual {p0, p1}, LX/H3P;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;

    iget-object v0, v0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->typeaheadPlace:Lcom/facebook/nearby/model/TypeaheadPlace;

    .line 2420976
    iget-object v1, p0, LX/H3P;->i:Landroid/location/Location;

    invoke-virtual {p2, v0, v1}, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->a(Lcom/facebook/nearby/model/TypeaheadPlace;Landroid/location/Location;)V

    .line 2420977
    invoke-static {p0, p1}, LX/H3P;->b(LX/H3P;I)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;->setBackgroundResource(I)V

    .line 2420978
    return-object p2

    .line 2420979
    :cond_0
    iget-object v0, p0, LX/H3P;->d:Landroid/view/LayoutInflater;

    const v1, 0x7f030bb9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/places/NearbyPlaceDetailsView;

    move-object p2, v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/common/SearchSuggestion;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2420927
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/H3P;->g:LX/0Px;

    .line 2420928
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2420929
    iput-object v0, p0, LX/H3P;->h:LX/0Px;

    .line 2420930
    const v0, 0x24868349

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2420931
    return-void
.end method

.method public final a(Ljava/util/List;Ljava/util/List;Landroid/location/Location;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/common/SearchSuggestion;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;",
            ">;",
            "Landroid/location/Location;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2420922
    invoke-static {p1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/H3P;->g:LX/0Px;

    .line 2420923
    invoke-static {p2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/H3P;->h:LX/0Px;

    .line 2420924
    iput-object p3, p0, LX/H3P;->i:Landroid/location/Location;

    .line 2420925
    const v0, -0x7152b3ee

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2420926
    return-void
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 2420917
    iget-object v0, p0, LX/H3P;->g:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    iget-object v1, p0, LX/H3P;->h:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2420911
    invoke-direct {p0, p1}, LX/H3P;->a(I)LX/H3O;

    move-result-object v0

    .line 2420912
    invoke-direct {p0, v0}, LX/H3P;->a(LX/H3O;)I

    move-result v1

    .line 2420913
    sget-object v2, LX/H3N;->a:[I

    invoke-virtual {v0}, LX/H3O;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2420914
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected RowType type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2420915
    :pswitch_0
    iget-object v0, p0, LX/H3P;->g:LX/0Px;

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 2420916
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, LX/H3P;->h:LX/0Px;

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final getItemId(I)J
    .locals 4

    .prologue
    .line 2420906
    invoke-direct {p0, p1}, LX/H3P;->a(I)LX/H3O;

    move-result-object v0

    .line 2420907
    sget-object v1, LX/H3N;->a:[I

    invoke-virtual {v0}, LX/H3O;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2420908
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown row type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2420909
    :pswitch_0
    int-to-long v0, p1

    .line 2420910
    :goto_0
    return-wide v0

    :pswitch_1
    invoke-direct {p0, v0}, LX/H3P;->a(LX/H3O;)I

    move-result v0

    int-to-long v2, v0

    invoke-virtual {p0, p1}, LX/H3P;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;

    iget-object v0, v0, Lcom/facebook/nearby/model/TypeaheadPlaceWithLayout;->typeaheadPlace:Lcom/facebook/nearby/model/TypeaheadPlace;

    iget-object v0, v0, Lcom/facebook/nearby/model/TypeaheadPlace;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    add-long/2addr v0, v2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2420905
    invoke-direct {p0, p1}, LX/H3P;->a(I)LX/H3O;

    move-result-object v0

    invoke-virtual {v0}, LX/H3O;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2420893
    invoke-direct {p0, p1}, LX/H3P;->a(I)LX/H3O;

    move-result-object v0

    .line 2420894
    sget-object v1, LX/H3N;->a:[I

    invoke-virtual {v0}, LX/H3O;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2420895
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unexpected type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2420896
    :pswitch_0
    if-eqz p2, :cond_0

    .line 2420897
    check-cast p2, LX/H3Z;

    .line 2420898
    :goto_0
    move-object v1, p2

    .line 2420899
    invoke-virtual {p0, p1}, LX/H3P;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/common/SearchSuggestion;

    .line 2420900
    iget-object p2, v1, LX/H3Z;->a:Landroid/widget/TextView;

    iget-object p3, v0, Lcom/facebook/nearby/common/SearchSuggestion;->b:Ljava/lang/String;

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2420901
    invoke-static {p0, p1}, LX/H3P;->b(LX/H3P;I)I

    move-result v0

    invoke-virtual {v1, v0}, LX/H3Z;->setBackgroundResource(I)V

    move-object v0, v1

    .line 2420902
    :goto_1
    return-object v0

    .line 2420903
    :pswitch_1
    invoke-direct {p0, p1, p2, p3}, LX/H3P;->b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    .line 2420904
    :pswitch_2
    invoke-direct {p0, p1, p2, p3}, LX/H3P;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_1

    :cond_0
    new-instance p2, LX/H3Z;

    iget-object v0, p0, LX/H3P;->c:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/H3Z;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2420892
    invoke-static {}, LX/H3O;->values()[LX/H3O;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
