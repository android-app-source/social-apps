.class public final LX/GLU;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GLV;


# direct methods
.method public constructor <init>(LX/GLV;)V
    .locals 0

    .prologue
    .line 2343004
    iput-object p1, p0, LX/GLU;->a:LX/GLV;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(LX/0Px;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2343005
    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2343006
    :goto_0
    return-void

    .line 2343007
    :cond_0
    iget-object v0, p0, LX/GLU;->a:LX/GLV;

    iget-object v0, v0, LX/GLV;->b:LX/GLb;

    iget-object v0, v0, LX/GLb;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->getMoreOptionsOffset()I

    move-result v3

    .line 2343008
    iget-object v0, p0, LX/GLU;->a:LX/GLV;

    iget-object v0, v0, LX/GLV;->b:LX/GLb;

    iget-object v0, v0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2343009
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v4, v2

    .line 2343010
    iget-object v0, p0, LX/GLU;->a:LX/GLV;

    iget-object v0, v0, LX/GLV;->b:LX/GLb;

    iget-object v0, v0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2343011
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v5, v2

    .line 2343012
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v6

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_3

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    .line 2343013
    iget-object v7, p0, LX/GLU;->a:LX/GLV;

    iget-object v7, v7, LX/GLV;->b:LX/GLb;

    invoke-static {v7}, LX/GLb;->g(LX/GLb;)I

    .line 2343014
    if-eqz v0, :cond_1

    .line 2343015
    iget-object v7, p0, LX/GLU;->a:LX/GLV;

    iget-object v7, v7, LX/GLV;->b:LX/GLb;

    add-int/lit8 v8, v3, 0x1

    invoke-static {v7, v0, v8}, LX/GLb;->a(LX/GLb;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;I)Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;

    move-result-object v7

    .line 2343016
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v0

    if-ne v4, v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v7, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesRadioButtonWithDetails;->setChecked(Z)V

    .line 2343017
    iget-object v0, p0, LX/GLU;->a:LX/GLV;

    iget-object v0, v0, LX/GLV;->b:LX/GLb;

    iget-object v0, v0, LX/GLb;->h:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2343018
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 2343019
    goto :goto_2

    .line 2343020
    :cond_3
    iget-object v0, p0, LX/GLU;->a:LX/GLV;

    iget-object v0, v0, LX/GLV;->b:LX/GLb;

    iget-object v0, v0, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    iget-object v2, p0, LX/GLU;->a:LX/GLV;

    iget-object v2, v2, LX/GLV;->b:LX/GLb;

    iget-object v2, v2, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->v()LX/0Px;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a(LX/0Px;)V

    .line 2343021
    iget-object v0, p0, LX/GLU;->a:LX/GLV;

    iget-object v0, v0, LX/GLV;->b:LX/GLb;

    iget-object v1, p0, LX/GLU;->a:LX/GLV;

    iget-object v1, v1, LX/GLV;->b:LX/GLb;

    iget-object v1, v1, LX/GLb;->c:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->v()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    .line 2343022
    iput v1, v0, LX/GLb;->j:I

    .line 2343023
    iget-object v0, p0, LX/GLU;->a:LX/GLV;

    iget-object v0, v0, LX/GLV;->b:LX/GLb;

    invoke-static {v0}, LX/GLb;->g(LX/GLb;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2343024
    iget-object v0, p0, LX/GLU;->a:LX/GLV;

    iget-object v0, v0, LX/GLV;->b:LX/GLb;

    iget-object v0, v0, LX/GLb;->d:LX/2U3;

    const-class v1, LX/GLb;

    const-string v2, "Error fetching more audiences for Unified Audience"

    invoke-virtual {v0, v1, v2, p1}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2343025
    iget-object v0, p0, LX/GLU;->a:LX/GLV;

    iget-object v0, v0, LX/GLV;->b:LX/GLb;

    iget-object v0, v0, LX/GLb;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->setMoreOptionsViewVisibility(I)V

    .line 2343026
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2343027
    check-cast p1, LX/0Px;

    invoke-direct {p0, p1}, LX/GLU;->a(LX/0Px;)V

    return-void
.end method
