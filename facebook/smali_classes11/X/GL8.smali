.class public final LX/GL8;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GL9;


# direct methods
.method public constructor <init>(LX/GL9;)V
    .locals 0

    .prologue
    .line 2342119
    iput-object p1, p0, LX/GL8;->a:LX/GL9;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2342135
    iget-object v0, p0, LX/GL8;->a:LX/GL9;

    iget-object v0, v0, LX/GL9;->b:LX/GLB;

    .line 2342136
    iget-boolean v2, v0, LX/GHg;->a:Z

    move v0, v2

    .line 2342137
    if-nez v0, :cond_0

    .line 2342138
    :goto_0
    return-void

    .line 2342139
    :cond_0
    iget-object v0, p0, LX/GL8;->a:LX/GL9;

    iget-object v0, v0, LX/GL9;->b:LX/GLB;

    iget-object v0, v0, LX/GLB;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Z)V

    .line 2342140
    iget-object v0, p0, LX/GL8;->a:LX/GL9;

    iget-object v0, v0, LX/GL9;->b:LX/GLB;

    iget-object v0, v0, LX/GLB;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2342141
    iget-object v0, p0, LX/GL8;->a:LX/GL9;

    iget-object v0, v0, LX/GL9;->b:LX/GLB;

    iget-object v0, v0, LX/GLB;->e:LX/2U3;

    const-class v1, LX/GLB;

    const-string v2, "Error Fetching Saved Audiences"

    invoke-virtual {v0, v1, v2, p1}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2342120
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2342121
    iget-object v0, p0, LX/GL8;->a:LX/GL9;

    iget-object v0, v0, LX/GL9;->b:LX/GLB;

    .line 2342122
    iget-boolean v2, v0, LX/GHg;->a:Z

    move v0, v2

    .line 2342123
    if-nez v0, :cond_1

    .line 2342124
    :cond_0
    :goto_0
    return-void

    .line 2342125
    :cond_1
    iget-object v0, p0, LX/GL8;->a:LX/GL9;

    iget-object v0, v0, LX/GL9;->b:LX/GLB;

    iget-object v0, v0, LX/GLB;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Z)V

    .line 2342126
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2342127
    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel;

    .line 2342128
    if-eqz v0, :cond_0

    .line 2342129
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    :goto_1
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel$NodesModel;

    .line 2342130
    iget-object v4, p0, LX/GL8;->a:LX/GL9;

    iget-object v4, v4, LX/GL9;->b:LX/GLB;

    iget-object v4, v4, LX/GLB;->g:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2342131
    iget-object v4, p0, LX/GL8;->a:LX/GL9;

    iget-object v4, v4, LX/GL9;->b:LX/GLB;

    iget-object v4, v4, LX/GLB;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel$NodesModel;->j()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0}, LX/GLB;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountAudiencesModel$AudiencesModel$NodesModel;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v4, v5, v6, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAudienceOptionsView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/facebook/adinterfaces/ui/AdInterfacesSavedAudienceRadioButton;

    move-result-object v0

    .line 2342132
    iget-object v4, p0, LX/GL8;->a:LX/GL9;

    iget-object v4, v4, LX/GL9;->b:LX/GLB;

    iget-object v4, v4, LX/GLB;->f:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2342133
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2342134
    :cond_2
    iget-object v0, p0, LX/GL8;->a:LX/GL9;

    iget-object v0, v0, LX/GL9;->b:LX/GLB;

    invoke-static {v0}, LX/GLB;->b(LX/GLB;)V

    goto :goto_0
.end method
