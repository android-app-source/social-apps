.class public LX/FSC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/FSB;


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final E:Ljava/lang/Object;

.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final b:Ljava/lang/String;


# instance fields
.field private final A:LX/0pJ;

.field private B:Lcom/facebook/api/feedtype/FeedType;

.field public C:I

.field public D:I

.field private final c:LX/FSI;

.field private final d:LX/FSH;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0TD;

.field private final g:LX/0qz;

.field public final h:LX/0SG;

.field public final i:LX/0pm;

.field public final j:LX/187;

.field public final k:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final l:LX/0pn;

.field public final m:LX/1fN;

.field private final n:LX/0qX;

.field private final o:LX/0Zb;

.field private final p:LX/0kb;

.field public final q:LX/FSc;

.field public final r:Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;

.field public final s:LX/0oy;

.field private final t:LX/1fJ;

.field private final u:LX/FSZ;

.field private final v:LX/0Ym;

.field public final w:LX/0ad;

.field private final x:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/16u;",
            ">;"
        }
    .end annotation
.end field

.field private final y:LX/FSE;

.field private final z:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2241652
    const-class v0, LX/FSC;

    sput-object v0, LX/FSC;->a:Ljava/lang/Class;

    .line 2241653
    const/4 v0, 0x0

    sput-object v0, LX/FSC;->b:Ljava/lang/String;

    .line 2241654
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/FSC;->E:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/FSI;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/FSH;LX/0TD;LX/0qz;LX/0SG;LX/0pm;LX/187;LX/0pn;LX/1fN;LX/0qX;LX/0Zb;LX/0kb;LX/FSc;Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;LX/0oy;LX/1fJ;LX/FSZ;LX/0Ym;LX/0ad;LX/0Ot;LX/FSE;LX/0pJ;)V
    .locals 2
    .param p2    # LX/0Or;
        .annotation runtime Lcom/facebook/feed/annotations/IsFlatBufferFromServerEnabled;
        .end annotation
    .end param
    .param p5    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FSI;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/FSH;",
            "LX/0TD;",
            "LX/0qz;",
            "LX/0SG;",
            "LX/0pm;",
            "LX/187;",
            "LX/0pn;",
            "LX/1fN;",
            "LX/0qX;",
            "LX/0Zb;",
            "LX/0kb;",
            "LX/FSc;",
            "Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;",
            "LX/0oy;",
            "LX/1fJ;",
            "LX/FSZ;",
            "LX/0Ym;",
            "LX/0ad;",
            "LX/0Ot",
            "<",
            "LX/16u;",
            ">;",
            "LX/FSE;",
            "LX/0pJ;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2241623
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2241624
    const/4 v1, 0x0

    iput v1, p0, LX/FSC;->C:I

    .line 2241625
    const/4 v1, 0x0

    iput v1, p0, LX/FSC;->D:I

    .line 2241626
    iput-object p1, p0, LX/FSC;->c:LX/FSI;

    .line 2241627
    iput-object p2, p0, LX/FSC;->e:LX/0Or;

    .line 2241628
    iput-object p3, p0, LX/FSC;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2241629
    iput-object p4, p0, LX/FSC;->d:LX/FSH;

    .line 2241630
    iput-object p5, p0, LX/FSC;->f:LX/0TD;

    .line 2241631
    iput-object p6, p0, LX/FSC;->g:LX/0qz;

    .line 2241632
    iput-object p7, p0, LX/FSC;->h:LX/0SG;

    .line 2241633
    iput-object p8, p0, LX/FSC;->i:LX/0pm;

    .line 2241634
    iput-object p9, p0, LX/FSC;->j:LX/187;

    .line 2241635
    iput-object p10, p0, LX/FSC;->l:LX/0pn;

    .line 2241636
    iput-object p11, p0, LX/FSC;->m:LX/1fN;

    .line 2241637
    iput-object p12, p0, LX/FSC;->n:LX/0qX;

    .line 2241638
    iput-object p13, p0, LX/FSC;->o:LX/0Zb;

    .line 2241639
    move-object/from16 v0, p14

    iput-object v0, p0, LX/FSC;->p:LX/0kb;

    .line 2241640
    move-object/from16 v0, p15

    iput-object v0, p0, LX/FSC;->q:LX/FSc;

    .line 2241641
    move-object/from16 v0, p16

    iput-object v0, p0, LX/FSC;->r:Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;

    .line 2241642
    move-object/from16 v0, p17

    iput-object v0, p0, LX/FSC;->s:LX/0oy;

    .line 2241643
    move-object/from16 v0, p18

    iput-object v0, p0, LX/FSC;->t:LX/1fJ;

    .line 2241644
    move-object/from16 v0, p19

    iput-object v0, p0, LX/FSC;->u:LX/FSZ;

    .line 2241645
    move-object/from16 v0, p20

    iput-object v0, p0, LX/FSC;->v:LX/0Ym;

    .line 2241646
    move-object/from16 v0, p21

    iput-object v0, p0, LX/FSC;->w:LX/0ad;

    .line 2241647
    move-object/from16 v0, p22

    iput-object v0, p0, LX/FSC;->x:LX/0Ot;

    .line 2241648
    move-object/from16 v0, p23

    iput-object v0, p0, LX/FSC;->y:LX/FSE;

    .line 2241649
    new-instance v1, LX/0UE;

    invoke-direct {v1}, LX/0UE;-><init>()V

    iput-object v1, p0, LX/FSC;->z:Ljava/util/Set;

    .line 2241650
    move-object/from16 v0, p24

    iput-object v0, p0, LX/FSC;->A:LX/0pJ;

    .line 2241651
    return-void
.end method

.method public static a(LX/0QB;)LX/FSC;
    .locals 7

    .prologue
    .line 2241596
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2241597
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2241598
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2241599
    if-nez v1, :cond_0

    .line 2241600
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2241601
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2241602
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2241603
    sget-object v1, LX/FSC;->E:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2241604
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2241605
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2241606
    :cond_1
    if-nez v1, :cond_4

    .line 2241607
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2241608
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2241609
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/FSC;->b(LX/0QB;)LX/FSC;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2241610
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2241611
    if-nez v1, :cond_2

    .line 2241612
    sget-object v0, LX/FSC;->E:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FSC;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2241613
    :goto_1
    if-eqz v0, :cond_3

    .line 2241614
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2241615
    :goto_3
    check-cast v0, LX/FSC;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2241616
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2241617
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2241618
    :catchall_1
    move-exception v0

    .line 2241619
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2241620
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2241621
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2241622
    :cond_2
    :try_start_8
    sget-object v0, LX/FSC;->E:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FSC;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private a(I)Lcom/facebook/api/feed/FetchFeedParams;
    .locals 8
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 2241440
    iget-object v0, p0, LX/FSC;->l:LX/0pn;

    invoke-static {p0}, LX/FSC;->e(LX/FSC;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0pn;->c(Lcom/facebook/api/feedtype/FeedType;)Ljava/lang/String;

    move-result-object v2

    .line 2241441
    iget-object v0, p0, LX/FSC;->g:LX/0qz;

    invoke-static {p0}, LX/FSC;->e(LX/FSC;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v1

    sget-object v6, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    sget-object v7, LX/0gf;->PREFETCH:LX/0gf;

    move v3, p1

    move v5, v4

    invoke-virtual/range {v0 .. v7}, LX/0qz;->a(Lcom/facebook/api/feedtype/FeedType;Ljava/lang/String;IZZLX/0rS;LX/0gf;)Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v0

    .line 2241442
    iget-object v1, v0, Lcom/facebook/api/feed/FetchFeedParams;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2241443
    if-eqz v1, :cond_0

    const-string v2, "cold_start_cursor"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2241444
    new-instance v1, LX/0rT;

    invoke-direct {v1}, LX/0rT;-><init>()V

    invoke-virtual {v1, v0}, LX/0rT;->a(Lcom/facebook/api/feed/FetchFeedParams;)LX/0rT;

    move-result-object v0

    sget-object v1, LX/FSC;->b:Ljava/lang/String;

    .line 2241445
    iput-object v1, v0, LX/0rT;->g:Ljava/lang/String;

    .line 2241446
    move-object v0, v0

    .line 2241447
    invoke-virtual {v0}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v0

    .line 2241448
    :cond_0
    return-object v0
.end method

.method public static a$redex0(LX/FSC;LX/0Px;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2241567
    iget-object v0, p0, LX/FSC;->w:LX/0ad;

    sget v1, LX/0fe;->K:I

    const/16 v2, 0xa

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    move v0, v0

    .line 2241568
    if-eqz p2, :cond_7

    .line 2241569
    iget-object v1, p0, LX/FSC;->w:LX/0ad;

    sget v2, LX/0fe;->L:I

    const/16 v4, 0x14

    invoke-interface {v1, v2, v4}, LX/0ad;->a(II)I

    move-result v1

    move v1, v1

    .line 2241570
    add-int/2addr v0, v1

    move v1, v0

    .line 2241571
    :goto_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2241572
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    move v2, v3

    :goto_1
    if-ge v4, v6, :cond_1

    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 2241573
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v7

    .line 2241574
    if-eqz v7, :cond_6

    iget-object v8, p0, LX/FSC;->z:Ljava/util/Set;

    invoke-interface {v7}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_6

    invoke-virtual {v0}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->A()I

    move-result v0

    const/4 v8, 0x1

    if-eq v0, v8, :cond_6

    .line 2241575
    invoke-virtual {v5, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2241576
    add-int/lit8 v0, v2, 0x1

    .line 2241577
    if-ge v0, v1, :cond_0

    .line 2241578
    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v0

    goto :goto_1

    :cond_0
    move v2, v0

    .line 2241579
    :cond_1
    if-ge v2, v1, :cond_2

    .line 2241580
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v6

    move v4, v3

    :goto_3
    if-ge v4, v6, :cond_2

    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 2241581
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v7

    .line 2241582
    if-eqz v7, :cond_5

    iget-object v8, p0, LX/FSC;->z:Ljava/util/Set;

    invoke-interface {v7}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    invoke-virtual {v0}, Lcom/facebook/feed/model/ClientFeedUnitEdge;->A()I

    move-result v0

    if-eqz v0, :cond_5

    .line 2241583
    invoke-virtual {v5, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2241584
    add-int/lit8 v0, v2, 0x1

    .line 2241585
    if-ge v0, v1, :cond_2

    .line 2241586
    :goto_4
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v0

    goto :goto_3

    .line 2241587
    :cond_2
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    move v1, v3

    :goto_5
    if-ge v1, v2, :cond_3

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 2241588
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v4, v0}, LX/FSC;->a$redex0(LX/FSC;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V

    .line 2241589
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 2241590
    :cond_3
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2241591
    iget-object v0, p0, LX/FSC;->y:LX/FSE;

    invoke-virtual {v0, v2}, LX/FSE;->a(Ljava/util/Collection;)V

    .line 2241592
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    move v1, v3

    :goto_6
    if-ge v1, v4, :cond_4

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 2241593
    iget-object v3, p0, LX/FSC;->z:Ljava/util/Set;

    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2241594
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 2241595
    :cond_4
    return-void

    :cond_5
    move v0, v2

    goto :goto_4

    :cond_6
    move v0, v2

    goto :goto_2

    :cond_7
    move v1, v0

    goto/16 :goto_0
.end method

.method public static a$redex0(LX/FSC;Lcom/facebook/api/feed/FetchFeedParams;IIZ)V
    .locals 10

    .prologue
    .line 2241541
    iget-object v0, p0, LX/FSC;->h:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 2241542
    iget-object v8, p0, LX/FSC;->c:LX/FSI;

    const-string v9, "async-feed-prefetch"

    new-instance v0, LX/FSA;

    move-object v1, p0

    move-object v4, p1

    move v5, p4

    move v6, p3

    move v7, p2

    invoke-direct/range {v0 .. v7}, LX/FSA;-><init>(LX/FSC;JLcom/facebook/api/feed/FetchFeedParams;ZII)V

    const/4 p0, 0x5

    const/4 v7, 0x1

    .line 2241543
    new-instance v2, LX/0v6;

    invoke-direct {v2, v9}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 2241544
    iget-object v1, v8, LX/FSI;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 2241545
    iput-boolean v1, v2, LX/0v6;->g:Z

    .line 2241546
    iget v1, p1, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v1, v1

    .line 2241547
    new-instance v3, LX/0rT;

    invoke-direct {v3}, LX/0rT;-><init>()V

    invoke-virtual {v3, p1}, LX/0rT;->a(Lcom/facebook/api/feed/FetchFeedParams;)LX/0rT;

    move-result-object v3

    .line 2241548
    iput v7, v3, LX/0rT;->c:I

    .line 2241549
    move-object v3, v3

    .line 2241550
    invoke-virtual {v3}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v3

    .line 2241551
    iget-object v4, v8, LX/FSI;->c:LX/0rm;

    invoke-virtual {v4, v3}, LX/0rm;->e(Lcom/facebook/api/feed/FetchFeedParams;)LX/0gW;

    move-result-object v3

    .line 2241552
    iget-object v4, v3, LX/0gW;->e:LX/0w7;

    move-object v4, v4

    .line 2241553
    const-string v5, "param"

    const-string v6, "after_home_story_param"

    invoke-virtual {v4, v5, v6}, LX/0w7;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0w7;

    .line 2241554
    const-string v5, "import"

    const-string v6, "end_cursor"

    invoke-virtual {v4, v5, v6}, LX/0w7;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0w7;

    .line 2241555
    if-lez v1, :cond_0

    add-int/lit8 v1, v1, -0x1

    .line 2241556
    :goto_0
    const-string v5, "max_runs"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, LX/0w7;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0w7;

    .line 2241557
    const-string v1, "storyset_first_fetch_size"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v1, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2241558
    const-string v1, "pyml_first_fetch_size"

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v1, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2241559
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-virtual {v1, v4}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v1

    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {v1, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    .line 2241560
    iput-boolean v7, v1, LX/0zO;->p:Z

    .line 2241561
    move-object v1, v1

    .line 2241562
    invoke-virtual {v2, v1}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v1

    .line 2241563
    invoke-virtual {v1, v0}, LX/0zX;->a(LX/0rl;)LX/0zi;

    .line 2241564
    iget-object v1, v8, LX/FSI;->b:LX/0tX;

    invoke-virtual {v1, v2}, LX/0tX;->a(LX/0v6;)V

    .line 2241565
    return-void

    .line 2241566
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/FSC;Lcom/facebook/graphql/model/FeedUnit;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2241655
    iget-object v0, p0, LX/FSC;->w:LX/0ad;

    sget-short v1, LX/0fe;->Q:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v0, v0

    .line 2241656
    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 2241657
    invoke-static {p1}, LX/16u;->a(Lcom/facebook/graphql/model/FeedUnit;)Ljava/lang/String;

    move-result-object v1

    .line 2241658
    if-eqz v1, :cond_0

    .line 2241659
    iget-object v0, p0, LX/FSC;->x:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/16u;

    invoke-virtual {v0, v1, p2}, LX/16u;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2241660
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/FSC;Ljava/lang/String;JJILjava/lang/String;)V
    .locals 4

    .prologue
    .line 2241529
    iget-object v0, p0, LX/FSC;->o:LX/0Zb;

    const-string v1, "android_async_feed_prefetch"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2241530
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2241531
    const-string v1, "stop_reason"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2241532
    const-string v1, "network_type"

    iget-object v2, p0, LX/FSC;->p:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2241533
    const-string v1, "network_subtype"

    iget-object v2, p0, LX/FSC;->p:LX/0kb;

    invoke-virtual {v2}, LX/0kb;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2241534
    const-string v1, "total_time"

    iget-object v2, p0, LX/FSC;->h:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    sub-long/2addr v2, p2

    invoke-virtual {v0, v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 2241535
    const-string v1, "total_stories_count"

    invoke-virtual {v0, v1, p4, p5}, LX/0oG;->a(Ljava/lang/String;J)LX/0oG;

    .line 2241536
    const-string v1, "stories_to_fetch"

    invoke-virtual {v0, v1, p6}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2241537
    const-string v1, "predictive_enabled"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Z)LX/0oG;

    .line 2241538
    const-string v1, "extra_field"

    invoke-virtual {v0, v1, p7}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2241539
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2241540
    :cond_0
    return-void
.end method

.method private static b(LX/0QB;)LX/FSC;
    .locals 27

    .prologue
    .line 2241527
    new-instance v2, LX/FSC;

    invoke-static/range {p0 .. p0}, LX/FSI;->a(LX/0QB;)LX/FSI;

    move-result-object v3

    check-cast v3, LX/FSI;

    const/16 v4, 0x149a

    move-object/from16 v0, p0

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static/range {p0 .. p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v5

    check-cast v5, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {p0 .. p0}, LX/FSH;->a(LX/0QB;)LX/FSH;

    move-result-object v6

    check-cast v6, LX/FSH;

    invoke-static/range {p0 .. p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v7

    check-cast v7, LX/0TD;

    invoke-static/range {p0 .. p0}, LX/0qz;->a(LX/0QB;)LX/0qz;

    move-result-object v8

    check-cast v8, LX/0qz;

    invoke-static/range {p0 .. p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v9

    check-cast v9, LX/0SG;

    invoke-static/range {p0 .. p0}, LX/0pm;->a(LX/0QB;)LX/0pm;

    move-result-object v10

    check-cast v10, LX/0pm;

    invoke-static/range {p0 .. p0}, LX/187;->a(LX/0QB;)LX/187;

    move-result-object v11

    check-cast v11, LX/187;

    invoke-static/range {p0 .. p0}, LX/0pn;->a(LX/0QB;)LX/0pn;

    move-result-object v12

    check-cast v12, LX/0pn;

    invoke-static/range {p0 .. p0}, LX/1fN;->a(LX/0QB;)LX/1fN;

    move-result-object v13

    check-cast v13, LX/1fN;

    invoke-static/range {p0 .. p0}, LX/0qX;->a(LX/0QB;)LX/0qX;

    move-result-object v14

    check-cast v14, LX/0qX;

    invoke-static/range {p0 .. p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v15

    check-cast v15, LX/0Zb;

    invoke-static/range {p0 .. p0}, LX/0kb;->a(LX/0QB;)LX/0kb;

    move-result-object v16

    check-cast v16, LX/0kb;

    invoke-static/range {p0 .. p0}, LX/FSc;->a(LX/0QB;)LX/FSc;

    move-result-object v17

    check-cast v17, LX/FSc;

    invoke-static/range {p0 .. p0}, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;->a(LX/0QB;)Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;

    move-result-object v18

    check-cast v18, Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;

    invoke-static/range {p0 .. p0}, LX/0oy;->a(LX/0QB;)LX/0oy;

    move-result-object v19

    check-cast v19, LX/0oy;

    invoke-static/range {p0 .. p0}, LX/1fJ;->a(LX/0QB;)LX/1fJ;

    move-result-object v20

    check-cast v20, LX/1fJ;

    invoke-static/range {p0 .. p0}, LX/FSZ;->a(LX/0QB;)LX/FSZ;

    move-result-object v21

    check-cast v21, LX/FSZ;

    invoke-static/range {p0 .. p0}, LX/0Ym;->a(LX/0QB;)LX/0Ym;

    move-result-object v22

    check-cast v22, LX/0Ym;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v23

    check-cast v23, LX/0ad;

    const/16 v24, 0x695

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v24

    invoke-static/range {p0 .. p0}, LX/FSE;->a(LX/0QB;)LX/FSE;

    move-result-object v25

    check-cast v25, LX/FSE;

    invoke-static/range {p0 .. p0}, LX/0pJ;->a(LX/0QB;)LX/0pJ;

    move-result-object v26

    check-cast v26, LX/0pJ;

    invoke-direct/range {v2 .. v26}, LX/FSC;-><init>(LX/FSI;LX/0Or;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/FSH;LX/0TD;LX/0qz;LX/0SG;LX/0pm;LX/187;LX/0pn;LX/1fN;LX/0qX;LX/0Zb;LX/0kb;LX/FSc;Lcom/facebook/feed/ui/feedprefetch/FeedPrefetchLoader;LX/0oy;LX/1fJ;LX/FSZ;LX/0Ym;LX/0ad;LX/0Ot;LX/FSE;LX/0pJ;)V

    .line 2241528
    return-object v2
.end method

.method public static e(LX/FSC;)Lcom/facebook/api/feedtype/FeedType;
    .locals 1

    .prologue
    .line 2241524
    iget-object v0, p0, LX/FSC;->B:Lcom/facebook/api/feedtype/FeedType;

    if-nez v0, :cond_0

    .line 2241525
    iget-object v0, p0, LX/FSC;->v:LX/0Ym;

    invoke-virtual {v0}, LX/0Ym;->a()Lcom/facebook/api/feedtype/FeedType;

    move-result-object v0

    iput-object v0, p0, LX/FSC;->B:Lcom/facebook/api/feedtype/FeedType;

    .line 2241526
    :cond_0
    iget-object v0, p0, LX/FSC;->B:Lcom/facebook/api/feedtype/FeedType;

    return-object v0
.end method


# virtual methods
.method public final a(ILX/0Px;Ljava/lang/String;)Lcom/facebook/api/feed/FetchFeedParams;
    .locals 7
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "LX/0Px",
            "<",
            "Lcom/facebook/feed/model/ClientFeedUnitEdge;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/facebook/api/feed/FetchFeedParams;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2241503
    const/4 v3, 0x0

    .line 2241504
    const/4 v2, -0x1

    .line 2241505
    if-eqz p3, :cond_7

    .line 2241506
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_7

    .line 2241507
    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {p3, v0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2241508
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v2, v0

    move-object v1, v3

    :goto_2
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 2241509
    if-eqz v1, :cond_5

    .line 2241510
    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v3

    .line 2241511
    :cond_0
    if-nez v1, :cond_1

    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2241512
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v1

    .line 2241513
    :cond_1
    new-instance v0, LX/44w;

    invoke-direct {v0, v1, v3}, LX/44w;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v3, v0

    .line 2241514
    if-eqz v3, :cond_2

    iget-object v0, v3, LX/44w;->a:Ljava/lang/Object;

    if-nez v0, :cond_3

    .line 2241515
    :cond_2
    const/4 v0, 0x0

    .line 2241516
    :goto_3
    return-object v0

    .line 2241517
    :cond_3
    iget-object v0, p0, LX/FSC;->g:LX/0qz;

    invoke-static {p0}, LX/FSC;->e(LX/FSC;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v1

    iget-object v2, v3, LX/44w;->a:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v3, v3, LX/44w;->b:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    sget-object v5, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    sget-object v6, LX/0gf;->PREFETCH:LX/0gf;

    move v4, p1

    invoke-virtual/range {v0 .. v6}, LX/0qz;->a(Lcom/facebook/api/feedtype/FeedType;Ljava/lang/String;Ljava/lang/String;ILX/0rS;LX/0gf;)Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v0

    goto :goto_3

    .line 2241518
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2241519
    :cond_5
    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    .line 2241520
    iget-boolean p3, v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;->E:Z

    move v0, p3

    .line 2241521
    if-eqz v0, :cond_6

    .line 2241522
    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feed/model/ClientFeedUnitEdge;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->g()Ljava/lang/String;

    move-result-object v0

    .line 2241523
    :goto_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-object v1, v0

    goto :goto_2

    :cond_6
    move-object v0, v1

    goto :goto_4

    :cond_7
    move v1, v2

    goto :goto_1
.end method

.method public final a()Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 2241481
    iget-object v0, p0, LX/FSC;->d:LX/FSH;

    .line 2241482
    iget-object v1, v0, LX/FSH;->b:LX/0WJ;

    invoke-virtual {v1}, LX/0WJ;->b()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2241483
    sget-object v1, LX/FSG;->NOT_LOGGED_IN:LX/FSG;

    .line 2241484
    :goto_0
    move-object v0, v1

    .line 2241485
    iput v6, p0, LX/FSC;->C:I

    .line 2241486
    iput v6, p0, LX/FSC;->D:I

    .line 2241487
    sget-object v1, LX/FSG;->SUCCESS:LX/FSG;

    if-eq v0, v1, :cond_0

    .line 2241488
    const-string v1, "failure_conditions"

    iget-object v2, p0, LX/FSC;->h:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    invoke-virtual {v0}, LX/FSG;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v0, p0

    invoke-static/range {v0 .. v7}, LX/FSC;->a$redex0(LX/FSC;Ljava/lang/String;JJILjava/lang/String;)V

    .line 2241489
    iget-object v0, p0, LX/FSC;->q:LX/FSc;

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v2, v3}, LX/FSc;->a(J)V

    .line 2241490
    new-instance v0, LX/FSV;

    const-string v1, "Prefetch conditions not satisfied"

    invoke-direct {v0, v1}, LX/FSV;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2241491
    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, LX/FSC;->f:LX/0TD;

    new-instance v1, Lcom/facebook/prefetch/feed/AsyncNewsFeedPrefetchHelper$1;

    invoke-direct {v1, p0}, Lcom/facebook/prefetch/feed/AsyncNewsFeedPrefetchHelper$1;-><init>(LX/FSC;)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_1

    .line 2241492
    :cond_1
    iget-object v1, v0, LX/FSH;->a:LX/0Uo;

    invoke-virtual {v1}, LX/0Uo;->j()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2241493
    sget-object v1, LX/FSG;->APP_NOT_IN_BACKGROUND:LX/FSG;

    goto :goto_0

    .line 2241494
    :cond_2
    const/4 v1, 0x1

    .line 2241495
    invoke-static {v0}, LX/FSH;->c(LX/FSH;)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-static {v0}, LX/FSH;->d(LX/FSH;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2241496
    :cond_3
    :goto_2
    move v1, v1

    .line 2241497
    if-nez v1, :cond_4

    .line 2241498
    sget-object v1, LX/FSG;->NOT_CONNECTED:LX/FSG;

    goto :goto_0

    .line 2241499
    :cond_4
    sget-object v1, LX/FSG;->SUCCESS:LX/FSG;

    goto :goto_0

    .line 2241500
    :cond_5
    invoke-static {v0}, LX/FSH;->d(LX/FSH;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2241501
    iget-object v2, v0, LX/FSH;->d:LX/AjI;

    invoke-virtual {v2}, LX/AjI;->a()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, v0, LX/FSH;->c:LX/0qj;

    invoke-virtual {v2}, LX/0qj;->a()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_6
    const/4 v1, 0x0

    goto :goto_2

    .line 2241502
    :cond_7
    iget-object v1, v0, LX/FSH;->c:LX/0qj;

    invoke-virtual {v1}, LX/0qj;->a()Z

    move-result v1

    goto :goto_2
.end method

.method public final b()V
    .locals 9
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2241449
    iget-object v0, p0, LX/FSC;->A:LX/0pJ;

    const/4 v1, 0x0

    .line 2241450
    sget-wide v5, LX/0X5;->dA:J

    const/16 v7, 0x8

    invoke-virtual {v0, v5, v6, v7, v1}, LX/0pK;->a(JIZ)Z

    move-result v5

    move v0, v5

    .line 2241451
    if-eqz v0, :cond_0

    .line 2241452
    :goto_0
    return-void

    .line 2241453
    :cond_0
    iget-object v0, p0, LX/FSC;->u:LX/FSZ;

    invoke-virtual {v0}, LX/FSZ;->c()LX/FSd;

    move-result-object v1

    .line 2241454
    iget-wide v5, v1, LX/FSd;->a:J

    move-wide v2, v5

    .line 2241455
    long-to-int v0, v2

    .line 2241456
    if-nez v0, :cond_1

    .line 2241457
    iget-object v0, p0, LX/FSC;->n:LX/0qX;

    const/16 v2, 0x32

    .line 2241458
    sget-wide v5, LX/0X5;->ch:J

    invoke-static {v0, v5, v6, v2}, LX/0qX;->a(LX/0qX;JI)I

    move-result v5

    move v0, v5

    .line 2241459
    :cond_1
    iget-object v2, p0, LX/FSC;->t:LX/1fJ;

    invoke-virtual {v2}, LX/1fJ;->init()V

    .line 2241460
    iget-object v2, p0, LX/FSC;->d:LX/FSH;

    iget-object v3, p0, LX/FSC;->p:LX/0kb;

    .line 2241461
    iget v4, v1, LX/FSd;->b:I

    move v4, v4

    .line 2241462
    const/4 v5, 0x1

    .line 2241463
    packed-switch v4, :pswitch_data_0

    .line 2241464
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2241465
    invoke-static {v2}, LX/FSH;->c(LX/FSH;)Z

    move-result v4

    if-nez v4, :cond_5

    invoke-static {v2}, LX/FSH;->d(LX/FSH;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 2241466
    :cond_2
    :goto_1
    move v5, v5

    .line 2241467
    :cond_3
    :goto_2
    :pswitch_0
    move v2, v5

    .line 2241468
    iget v3, v1, LX/FSd;->b:I

    move v3, v3

    .line 2241469
    const/4 v4, 0x3

    if-ne v3, v4, :cond_4

    .line 2241470
    sget-object v5, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    iget-object v6, p0, LX/FSC;->s:LX/0oy;

    invoke-virtual {v6}, LX/0oy;->i()I

    move-result v6

    int-to-long v7, v6

    invoke-virtual {v5, v7, v8}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v5

    .line 2241471
    iget-object v7, p0, LX/FSC;->l:LX/0pn;

    invoke-static {p0}, LX/FSC;->e(LX/FSC;)Lcom/facebook/api/feedtype/FeedType;

    move-result-object v8

    invoke-virtual {v7, v8, v5, v6}, LX/0pn;->a(Lcom/facebook/api/feedtype/FeedType;J)LX/0Px;

    move-result-object v5

    .line 2241472
    invoke-static {p0, v5, v2}, LX/FSC;->a$redex0(LX/FSC;LX/0Px;Z)V

    .line 2241473
    iget-object v0, p0, LX/FSC;->q:LX/FSc;

    invoke-virtual {v0}, LX/FSc;->a()V

    goto :goto_0

    .line 2241474
    :cond_4
    invoke-direct {p0, v0}, LX/FSC;->a(I)Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v3

    .line 2241475
    iget v4, v1, LX/FSd;->b:I

    move v1, v4

    .line 2241476
    invoke-static {p0, v3, v1, v0, v2}, LX/FSC;->a$redex0(LX/FSC;Lcom/facebook/api/feed/FetchFeedParams;IIZ)V

    goto :goto_0

    .line 2241477
    :pswitch_1
    invoke-virtual {v3}, LX/0kb;->h()Z

    move-result v6

    if-eqz v6, :cond_3

    const/4 v5, 0x0

    goto :goto_2

    .line 2241478
    :cond_5
    invoke-static {v2}, LX/FSH;->d(LX/FSH;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2241479
    iget-object v4, v2, LX/FSH;->d:LX/AjI;

    invoke-virtual {v4}, LX/AjI;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, LX/0kb;->h()Z

    move-result v4

    if-nez v4, :cond_2

    move v5, v6

    goto :goto_1

    .line 2241480
    :cond_6
    invoke-virtual {v3}, LX/0kb;->h()Z

    move-result v4

    if-nez v4, :cond_2

    move v5, v6

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
