.class public LX/G63;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/0tX;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/1Ck;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2319642
    const-class v0, LX/G63;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/G63;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0tX;LX/1Ck;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0tX;",
            "LX/1Ck;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2319643
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2319644
    iput-object p2, p0, LX/G63;->b:LX/0tX;

    .line 2319645
    iput-object p1, p0, LX/G63;->c:LX/0Or;

    .line 2319646
    iput-object p3, p0, LX/G63;->d:LX/1Ck;

    .line 2319647
    return-void
.end method

.method public static b(LX/0QB;)LX/G63;
    .locals 4

    .prologue
    .line 2319648
    new-instance v2, LX/G63;

    const/16 v0, 0x15e7

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-direct {v2, v3, v0, v1}, LX/G63;-><init>(LX/0Or;LX/0tX;LX/1Ck;)V

    .line 2319649
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/0Ve;)V
    .locals 4

    .prologue
    .line 2319650
    new-instance v1, LX/4Jz;

    invoke-direct {v1}, LX/4Jz;-><init>()V

    iget-object v0, p0, LX/G63;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2319651
    const-string v2, "actor_id"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2319652
    move-object v0, v1

    .line 2319653
    const-string v1, "mentee_id"

    invoke-virtual {v0, v1, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2319654
    move-object v0, v0

    .line 2319655
    const-string v1, "suggestions"

    invoke-virtual {v0, v1, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2319656
    move-object v0, v0

    .line 2319657
    new-instance v1, LX/G64;

    invoke-direct {v1}, LX/G64;-><init>()V

    move-object v1, v1

    .line 2319658
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2319659
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2319660
    iget-object v1, p0, LX/G63;->d:LX/1Ck;

    sget-object v2, LX/G63;->a:Ljava/lang/String;

    iget-object v3, p0, LX/G63;->b:LX/0tX;

    invoke-virtual {v3, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-virtual {v1, v2, v0, p3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2319661
    return-void
.end method
