.class public LX/FWU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/FWU;


# instance fields
.field public a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/facebook/saved/launcher/SavedActivityLauncherWithResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/saved/launcher/SavedActivityLauncherWithResult;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2252173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2252174
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/FWU;->a:Ljava/util/Map;

    .line 2252175
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FWX;

    .line 2252176
    iget-object v2, p0, LX/FWU;->a:Ljava/util/Map;

    .line 2252177
    sget-object v3, LX/79w;->REVIEW_ITEM:LX/79w;

    move-object v3, v3

    .line 2252178
    invoke-virtual {v3}, LX/79w;->ordinal()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2252179
    :cond_0
    return-void
.end method

.method public static a(LX/0QB;)LX/FWU;
    .locals 6

    .prologue
    .line 2252180
    sget-object v0, LX/FWU;->b:LX/FWU;

    if-nez v0, :cond_1

    .line 2252181
    const-class v1, LX/FWU;

    monitor-enter v1

    .line 2252182
    :try_start_0
    sget-object v0, LX/FWU;->b:LX/FWU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2252183
    if-eqz v2, :cond_0

    .line 2252184
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2252185
    new-instance v3, LX/FWU;

    .line 2252186
    new-instance v4, LX/0U8;

    invoke-interface {v0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v5

    new-instance p0, LX/FWT;

    invoke-direct {p0, v0}, LX/FWT;-><init>(LX/0QB;)V

    invoke-direct {v4, v5, p0}, LX/0U8;-><init>(LX/0QC;LX/0Sq;)V

    move-object v4, v4

    .line 2252187
    invoke-direct {v3, v4}, LX/FWU;-><init>(Ljava/util/Set;)V

    .line 2252188
    move-object v0, v3

    .line 2252189
    sput-object v0, LX/FWU;->b:LX/FWU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2252190
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2252191
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2252192
    :cond_1
    sget-object v0, LX/FWU;->b:LX/FWU;

    return-object v0

    .line 2252193
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2252194
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
