.class public final LX/HEi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/HEk;


# direct methods
.method public constructor <init>(LX/HEk;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2442672
    iput-object p1, p0, LX/HEi;->b:LX/HEk;

    iput-object p2, p0, LX/HEi;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x34dc46

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2442673
    iget-object v1, p0, LX/HEi;->b:LX/HEk;

    iget-object v1, v1, LX/HEk;->d:LX/HEU;

    iget-object v2, p0, LX/HEi;->a:Ljava/lang/String;

    .line 2442674
    iget-object v3, v1, LX/HEU;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/17W;

    iget-object v5, v1, LX/HEU;->a:Landroid/content/Context;

    invoke-virtual {v3, v5, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2442675
    new-instance v3, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v3, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v5, 0x10000000

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v3

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v5

    .line 2442676
    :try_start_0
    iget-object v3, v1, LX/HEU;->f:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    iget-object v6, v1, LX/HEU;->a:Landroid/content/Context;

    invoke-interface {v3, v5, v6}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2442677
    :cond_0
    :goto_0
    iget-object v1, p0, LX/HEi;->b:LX/HEk;

    iget-object v1, v1, LX/HEk;->d:LX/HEU;

    iget-object v2, p0, LX/HEi;->b:LX/HEk;

    iget-object v2, v2, LX/HEk;->a:LX/HEh;

    .line 2442678
    iget-object v3, v2, LX/HEh;->c:Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;

    move-object v2, v3

    .line 2442679
    invoke-virtual {v2}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel$TipsModel;->m()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/HEi;->b:LX/HEk;

    iget-object v3, v3, LX/HEk;->a:LX/HEh;

    .line 2442680
    iget-object v5, v3, LX/HEh;->b:Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel;

    move-object v3, v5

    .line 2442681
    invoke-virtual {v3}, Lcom/facebook/pages/common/megaphone/graphql/FetchPageAYMTMegaphoneGraphQLModels$FetchPageAYMTMegaphoneQueryModel$AymtMegaphoneChannelModel;->j()Ljava/lang/String;

    move-result-object v3

    .line 2442682
    const-string v5, "CLICK"

    invoke-static {v1, v2, v3, v5}, LX/HEU;->a(LX/HEU;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2442683
    const v1, -0x718b7710

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2442684
    :catch_0
    move-exception v3

    move-object v5, v3

    .line 2442685
    iget-object v3, v1, LX/HEU;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/03V;

    const-string v6, "page_admin_megaphone"

    const-string p1, "Failed to launch activity."

    invoke-virtual {v3, v6, p1, v5}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
