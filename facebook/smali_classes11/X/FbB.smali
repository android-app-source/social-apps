.class public LX/FbB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Xu;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Xu",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "LX/D0G;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/Fb9;",
            "LX/0RL",
            "<+",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0RL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0RL",
            "<+",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Rf;LX/0RL;)V
    .locals 7
    .param p1    # LX/0Rf;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0RL;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "LX/CzO;",
            ">;",
            "LX/0RL",
            "<+",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2259668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2259669
    iput-object p2, p0, LX/FbB;->c:LX/0RL;

    .line 2259670
    new-instance v2, LX/4yA;

    invoke-direct {v2}, LX/4yA;-><init>()V

    .line 2259671
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/FbB;->b:Ljava/util/Map;

    .line 2259672
    invoke-virtual {p1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CzO;

    .line 2259673
    iget-object v1, v0, LX/CzO;->b:LX/0Rf;

    invoke-virtual {v1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 2259674
    iget-object v5, v0, LX/CzO;->c:LX/D0G;

    invoke-virtual {v2, v1, v5}, LX/4yA;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/4yA;

    .line 2259675
    new-instance v5, LX/Fb9;

    iget-object v6, v0, LX/CzO;->c:LX/D0G;

    invoke-direct {v5, v1, v6}, LX/Fb9;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0Rl;)V

    .line 2259676
    iget-object v1, p0, LX/FbB;->b:Ljava/util/Map;

    iget-object v6, v0, LX/CzO;->d:LX/0RL;

    invoke-interface {v1, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2259677
    :cond_1
    invoke-virtual {v2}, LX/4yA;->a()LX/3CE;

    move-result-object v0

    iput-object v0, p0, LX/FbB;->a:LX/0Xu;

    .line 2259678
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 2259679
    if-eqz v0, :cond_2

    .line 2259680
    invoke-direct {p0}, LX/FbB;->a()V

    .line 2259681
    :cond_2
    return-void
.end method

.method private a()V
    .locals 7

    .prologue
    .line 2259657
    iget-object v0, p0, LX/FbB;->a:LX/0Xu;

    invoke-interface {v0}, LX/0Xu;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 2259658
    iget-object v1, p0, LX/FbB;->a:LX/0Xu;

    invoke-interface {v1, v0}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v6

    .line 2259659
    const/4 v1, 0x0

    move v3, v1

    :goto_0
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v1

    if-ge v3, v1, :cond_0

    .line 2259660
    add-int/lit8 v1, v3, 0x1

    move v4, v1

    :goto_1
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v1

    if-ge v4, v1, :cond_2

    .line 2259661
    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/D0G;

    .line 2259662
    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/D0G;

    .line 2259663
    invoke-interface {v1, v2}, LX/D0G;->a(LX/D0G;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2259664
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Overlapping display style matchers for role: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2259665
    :cond_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    .line 2259666
    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 2259667
    :cond_3
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)LX/0RL;
    .locals 3
    .param p2    # Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;",
            "Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;",
            ")",
            "LX/0RL",
            "<+",
            "Lcom/facebook/search/model/SearchResultsBaseFeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2259652
    iget-object v0, p0, LX/FbB;->a:LX/0Xu;

    invoke-interface {v0, p1}, LX/0Xu;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Rl;

    .line 2259653
    invoke-interface {v0, p2}, LX/0Rl;->apply(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2259654
    new-instance v1, LX/Fb9;

    invoke-direct {v1, p1, v0}, LX/Fb9;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;LX/0Rl;)V

    .line 2259655
    iget-object v0, p0, LX/FbB;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0RL;

    .line 2259656
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, LX/FbB;->c:LX/0RL;

    goto :goto_0
.end method
