.class public final enum LX/Fjy;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Fjy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Fjy;

.field public static final enum DEFAULT:LX/Fjy;

.field public static final enum MOBILE:LX/Fjy;

.field public static final enum WIFI:LX/Fjy;


# instance fields
.field public final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 2278025
    new-instance v0, LX/Fjy;

    const-string v1, "MOBILE"

    invoke-direct {v0, v1, v5, v3}, LX/Fjy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Fjy;->MOBILE:LX/Fjy;

    new-instance v0, LX/Fjy;

    const-string v1, "WIFI"

    invoke-direct {v0, v1, v3, v4}, LX/Fjy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Fjy;->WIFI:LX/Fjy;

    new-instance v0, LX/Fjy;

    const-string v1, "DEFAULT"

    sget-object v2, LX/Fjy;->WIFI:LX/Fjy;

    iget v2, v2, LX/Fjy;->value:I

    invoke-direct {v0, v1, v4, v2}, LX/Fjy;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/Fjy;->DEFAULT:LX/Fjy;

    .line 2278026
    const/4 v0, 0x3

    new-array v0, v0, [LX/Fjy;

    sget-object v1, LX/Fjy;->MOBILE:LX/Fjy;

    aput-object v1, v0, v5

    sget-object v1, LX/Fjy;->WIFI:LX/Fjy;

    aput-object v1, v0, v3

    sget-object v1, LX/Fjy;->DEFAULT:LX/Fjy;

    aput-object v1, v0, v4

    sput-object v0, LX/Fjy;->$VALUES:[LX/Fjy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2278027
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, LX/Fjy;->value:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Fjy;
    .locals 1

    .prologue
    .line 2278028
    const-class v0, LX/Fjy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Fjy;

    return-object v0
.end method

.method public static values()[LX/Fjy;
    .locals 1

    .prologue
    .line 2278029
    sget-object v0, LX/Fjy;->$VALUES:[LX/Fjy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Fjy;

    return-object v0
.end method
