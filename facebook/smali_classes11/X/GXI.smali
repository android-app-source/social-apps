.class public final LX/GXI;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/7iu;",
            ">;"
        }
    .end annotation
.end field

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/lang/String;

.field public h:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public j:Z

.field public k:Z

.field public l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/GXM;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public n:Z

.field public o:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel$ProductItemsEdgeModel$NodesModel;",
            ">;"
        }
    .end annotation
.end field

.field public p:LX/GXJ;

.field public q:Ljava/lang/String;

.field public r:Z

.field public s:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/GX0;",
            ">;"
        }
    .end annotation
.end field

.field public t:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2363925
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2363926
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/GXI;->a:LX/0am;

    .line 2363927
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2363928
    iput-object v0, p0, LX/GXI;->f:LX/0Px;

    .line 2363929
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/GXI;->h:LX/0am;

    .line 2363930
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/GXI;->i:LX/0am;

    .line 2363931
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2363932
    iput-object v0, p0, LX/GXI;->l:LX/0Px;

    .line 2363933
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/GXI;->m:LX/0am;

    .line 2363934
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2363935
    iput-object v0, p0, LX/GXI;->o:LX/0Px;

    .line 2363936
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    iput-object v0, p0, LX/GXI;->s:LX/0am;

    return-void
.end method


# virtual methods
.method public final a()LX/GXK;
    .locals 23

    .prologue
    .line 2363937
    new-instance v1, LX/GXK;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/GXI;->a:LX/0am;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/GXI;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/GXI;->c:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/GXI;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v6, v0, LX/GXI;->e:Z

    move-object/from16 v0, p0

    iget-object v7, v0, LX/GXI;->f:LX/0Px;

    move-object/from16 v0, p0

    iget-object v8, v0, LX/GXI;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, LX/GXI;->h:LX/0am;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/GXI;->i:LX/0am;

    move-object/from16 v0, p0

    iget-boolean v11, v0, LX/GXI;->j:Z

    move-object/from16 v0, p0

    iget-boolean v12, v0, LX/GXI;->k:Z

    move-object/from16 v0, p0

    iget-object v13, v0, LX/GXI;->l:LX/0Px;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/GXI;->m:LX/0am;

    move-object/from16 v0, p0

    iget-boolean v15, v0, LX/GXI;->n:Z

    move-object/from16 v0, p0

    iget-object v0, v0, LX/GXI;->o:LX/0Px;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/GXI;->p:LX/GXJ;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/GXI;->q:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, LX/GXI;->r:Z

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/GXI;->s:LX/0am;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, LX/GXI;->t:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-direct/range {v1 .. v22}, LX/GXK;-><init>(LX/0am;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLX/0Px;Ljava/lang/String;LX/0am;LX/0am;ZZLX/0Px;LX/0am;ZLX/0Px;LX/GXJ;Ljava/lang/String;ZLX/0am;Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;B)V

    return-object v1
.end method
