.class public final LX/FpM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FpN;


# direct methods
.method public constructor <init>(LX/FpN;)V
    .locals 0

    .prologue
    .line 2291508
    iput-object p1, p0, LX/FpM;->a:LX/FpN;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2291506
    iget-object v0, p0, LX/FpM;->a:LX/FpN;

    iget-object v0, v0, LX/FpN;->d:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    invoke-virtual {v0}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->b()V

    .line 2291507
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2291452
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2291453
    iget-object v0, p0, LX/FpM;->a:LX/FpN;

    iget-object v0, v0, LX/FpN;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    .line 2291454
    iget-object v0, p0, LX/FpM;->a:LX/FpN;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2291455
    iput-object v1, v0, LX/FpN;->f:Ljava/util/ArrayList;

    .line 2291456
    :goto_0
    if-eqz p1, :cond_0

    .line 2291457
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2291458
    if-eqz v0, :cond_0

    .line 2291459
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2291460
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel;->j()LX/2uF;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2291461
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2291462
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel;->j()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2291463
    :cond_0
    iget-object v0, p0, LX/FpM;->a:LX/FpN;

    iget-object v0, v0, LX/FpN;->d:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    invoke-virtual {v0}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->b()V

    .line 2291464
    :goto_1
    return-void

    .line 2291465
    :cond_1
    iget-object v0, p0, LX/FpM;->a:LX/FpN;

    iget-object v0, v0, LX/FpN;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    .line 2291466
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2291467
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel;->j()LX/2uF;

    move-result-object v0

    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v1

    :cond_3
    :goto_2
    invoke-interface {v1}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v3, v0, LX/1vs;->b:I

    .line 2291468
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 2291469
    if-eqz v3, :cond_9

    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;

    invoke-virtual {v2, v3, v4, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    if-eqz v0, :cond_9

    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;

    invoke-virtual {v2, v3, v4, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 2291470
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;

    invoke-virtual {v2, v3, v4, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2291471
    if-eqz v0, :cond_8

    move v0, v4

    :goto_3
    if-eqz v0, :cond_b

    .line 2291472
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;

    invoke-virtual {v2, v3, v4, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;->k()LX/1vs;

    move-result-object v0

    iget-object v6, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2291473
    invoke-virtual {v6, v0, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    move v0, v4

    :goto_4
    if-eqz v0, :cond_c

    .line 2291474
    :goto_5
    move v0, v4

    .line 2291475
    if-eqz v0, :cond_3

    .line 2291476
    iget-object v0, p0, LX/FpM;->a:LX/FpN;

    iget-object v4, v0, LX/FpN;->f:Ljava/util/ArrayList;

    new-instance v5, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;

    invoke-virtual {v2, v3, v8, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;

    .line 2291477
    if-eqz v0, :cond_d

    .line 2291478
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;->j()Ljava/lang/String;

    move-result-object v6

    .line 2291479
    :goto_6
    move-object v6, v6

    .line 2291480
    const-class v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;

    invoke-virtual {v2, v3, v8, v0}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;

    const/4 v3, 0x1

    const/4 v9, 0x0

    .line 2291481
    if-eqz v0, :cond_f

    .line 2291482
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;->k()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2291483
    if-eqz v2, :cond_e

    move v2, v3

    :goto_7
    if-eqz v2, :cond_11

    .line 2291484
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;->k()LX/1vs;

    move-result-object v2

    iget-object p1, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2291485
    invoke-virtual {p1, v2, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_10

    :goto_8
    if-eqz v3, :cond_12

    .line 2291486
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserSuggestedCoverPhotosQueryModels$FundraiserSuggestedCoverPhotosFragmentModel$SuggestedCoverPhotosModel$PhotoModel;->k()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v3, v2, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2291487
    :goto_9
    move-object v0, v2

    .line 2291488
    invoke-direct {v5, v6, v0}, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 2291489
    :cond_4
    iget-object v0, p0, LX/FpM;->a:LX/FpN;

    iget-object v0, v0, LX/FpN;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2291490
    iget-object v0, p0, LX/FpM;->a:LX/FpN;

    iget-object v0, v0, LX/FpN;->d:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    invoke-virtual {v0}, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->b()V

    goto/16 :goto_1

    .line 2291491
    :cond_5
    iget-object v0, p0, LX/FpM;->a:LX/FpN;

    iget-object v1, v0, LX/FpN;->d:Lcom/facebook/socialgood/ui/FundraiserCreationFragment;

    iget-object v0, p0, LX/FpM;->a:LX/FpN;

    iget-object v0, v0, LX/FpN;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    .line 2291492
    iget-object v2, v0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2291493
    iget-object v0, p0, LX/FpM;->a:LX/FpN;

    iget-object v0, v0, LX/FpN;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->b()Landroid/net/Uri;

    move-result-object v0

    .line 2291494
    iget-object v3, v1, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    if-eqz v3, :cond_6

    iget-object v3, v1, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    .line 2291495
    iget-boolean v4, v3, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;->d:Z

    move v3, v4

    .line 2291496
    if-nez v3, :cond_7

    .line 2291497
    :cond_6
    new-instance v3, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    invoke-direct {v3, v2, v0}, Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iput-object v3, v1, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    .line 2291498
    iget-object v3, v1, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->v:Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

    iget-object v4, v1, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->D:Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;

    sget-object v5, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->a(Lcom/facebook/socialgood/ui/create/coverphoto/FundraiserCoverPhotoModel;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2291499
    :cond_7
    iget-object v3, v1, Lcom/facebook/socialgood/ui/FundraiserCreationFragment;->v:Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;

    const/4 v4, 0x1

    .line 2291500
    iput-boolean v4, v3, Lcom/facebook/socialgood/ui/create/FundraiserCreationCoverPhotoPickerView;->i:Z

    .line 2291501
    goto/16 :goto_1

    :cond_8
    move v0, v5

    .line 2291502
    goto/16 :goto_3

    :cond_9
    move v0, v5

    goto/16 :goto_3

    :cond_a
    move v0, v5

    goto/16 :goto_4

    :cond_b
    move v0, v5

    goto/16 :goto_4

    :cond_c
    move v4, v5

    .line 2291503
    goto/16 :goto_5

    :cond_d
    const/4 v6, 0x0

    goto/16 :goto_6

    :cond_e
    move v2, v9

    .line 2291504
    goto/16 :goto_7

    :cond_f
    move v2, v9

    goto/16 :goto_7

    :cond_10
    move v3, v9

    goto/16 :goto_8

    :cond_11
    move v3, v9

    goto/16 :goto_8

    .line 2291505
    :cond_12
    const/4 v2, 0x0

    goto :goto_9
.end method
