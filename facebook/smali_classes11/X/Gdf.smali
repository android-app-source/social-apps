.class public final LX/Gdf;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/location/ImmutableLocation;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GDA;

.field public final synthetic b:Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;LX/GDA;)V
    .locals 0

    .prologue
    .line 2374401
    iput-object p1, p0, LX/Gdf;->b:Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;

    iput-object p2, p0, LX/Gdf;->a:LX/GDA;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2374402
    iget-object v0, p0, LX/Gdf;->a:LX/GDA;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/GDA;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;)V

    .line 2374403
    iget-object v0, p0, LX/Gdf;->b:Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;

    iget-object v0, v0, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->f:LX/03V;

    sget-object v1, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->b:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to get location"

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2374404
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2374405
    check-cast p1, Lcom/facebook/location/ImmutableLocation;

    .line 2374406
    if-eqz p1, :cond_0

    .line 2374407
    iget-object v1, p0, LX/Gdf;->b:Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->a()D

    move-result-wide v2

    invoke-virtual {p1}, Lcom/facebook/location/ImmutableLocation;->b()D

    move-result-wide v4

    iget-object v6, p0, LX/Gdf;->a:LX/GDA;

    .line 2374408
    invoke-static/range {v1 .. v6}, Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;->a$redex0(Lcom/facebook/feedplugins/localad/AdInterfacesLocationFetcher;DDLX/GDA;)V

    .line 2374409
    :goto_0
    return-void

    .line 2374410
    :cond_0
    iget-object v0, p0, LX/Gdf;->a:LX/GDA;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/GDA;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdGeoCircleModel;)V

    goto :goto_0
.end method
