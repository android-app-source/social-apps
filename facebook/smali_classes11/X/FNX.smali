.class public LX/FNX;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field private static volatile h:LX/FNX;


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6jL;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FM9;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMI;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FM8;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Rc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2232680
    const-wide/16 v0, -0x65

    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    sput-object v0, LX/FNX;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2232681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2232682
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2232683
    iput-object v0, p0, LX/FNX;->b:LX/0Ot;

    .line 2232684
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2232685
    iput-object v0, p0, LX/FNX;->c:LX/0Ot;

    .line 2232686
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2232687
    iput-object v0, p0, LX/FNX;->d:LX/0Ot;

    .line 2232688
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2232689
    iput-object v0, p0, LX/FNX;->e:LX/0Ot;

    .line 2232690
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2232691
    iput-object v0, p0, LX/FNX;->f:LX/0Ot;

    .line 2232692
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2232693
    iput-object v0, p0, LX/FNX;->g:LX/0Ot;

    .line 2232694
    return-void
.end method

.method public static a(LX/0QB;)LX/FNX;
    .locals 9

    .prologue
    .line 2232695
    sget-object v0, LX/FNX;->h:LX/FNX;

    if-nez v0, :cond_1

    .line 2232696
    const-class v1, LX/FNX;

    monitor-enter v1

    .line 2232697
    :try_start_0
    sget-object v0, LX/FNX;->h:LX/FNX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2232698
    if-eqz v2, :cond_0

    .line 2232699
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2232700
    new-instance v3, LX/FNX;

    invoke-direct {v3}, LX/FNX;-><init>()V

    .line 2232701
    const/16 v4, 0x2987

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x297b

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2982

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x297a

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const-class v8, Landroid/content/Context;

    invoke-interface {v0, v8}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v8

    const/16 p0, 0xdaa

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2232702
    iput-object v4, v3, LX/FNX;->b:LX/0Ot;

    iput-object v5, v3, LX/FNX;->c:LX/0Ot;

    iput-object v6, v3, LX/FNX;->d:LX/0Ot;

    iput-object v7, v3, LX/FNX;->e:LX/0Ot;

    iput-object v8, v3, LX/FNX;->f:LX/0Ot;

    iput-object p0, v3, LX/FNX;->g:LX/0Ot;

    .line 2232703
    move-object v0, v3

    .line 2232704
    sput-object v0, LX/FNX;->h:LX/FNX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2232705
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2232706
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2232707
    :cond_1
    sget-object v0, LX/FNX;->h:LX/FNX;

    return-object v0

    .line 2232708
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2232709
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2232710
    invoke-virtual {p0}, LX/FNX;->b()LX/0Rf;

    move-result-object v0

    .line 2232711
    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2232712
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v0, v0

    .line 2232713
    :goto_0
    return-object v0

    .line 2232714
    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 2232715
    iget-object v0, p0, LX/FNX;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FM9;

    invoke-virtual {v0}, LX/FM9;->a()LX/0Rf;

    move-result-object v0

    .line 2232716
    invoke-interface {v1, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 2232717
    invoke-static {v1}, LX/0Rf;->copyOf(Ljava/util/Collection;)LX/0Rf;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2232718
    iget-object v0, p0, LX/FNX;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6jL;

    invoke-virtual {v0}, LX/6jL;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2232719
    iget-object v0, p0, LX/FNX;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMI;

    iget-object v1, p0, LX/FNX;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6jL;

    invoke-virtual {v1}, LX/6jL;->a()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/FMI;->a(Ljava/util/Set;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
