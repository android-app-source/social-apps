.class public abstract LX/Gcw;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""

# interfaces
.implements LX/Gcv;


# instance fields
.field public a:LX/1nQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Landroid/view/View$OnClickListener;

.field public c:Landroid/widget/ProgressBar;

.field public d:Lcom/facebook/widget/ListViewFriendlyViewPager;

.field public e:Lcom/facebook/widget/text/BetterTextView;

.field public f:Lcom/facebook/resources/ui/FbTextView;

.field public g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2372795
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2372796
    const-class v0, LX/Gcw;

    invoke-static {v0, p0}, LX/Gcw;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2372797
    const v0, 0x7f03058c

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2372798
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/Gcw;->g:Ljava/util/Set;

    .line 2372799
    const v0, 0x7f0d0f4e

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/Gcw;->c:Landroid/widget/ProgressBar;

    .line 2372800
    const v0, 0x7f0d0ed1

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/Gcw;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2372801
    const v0, 0x7f0d0f50

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Gcw;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2372802
    const v0, 0x7f0d0f51

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/ListViewFriendlyViewPager;

    iput-object v0, p0, LX/Gcw;->d:Lcom/facebook/widget/ListViewFriendlyViewPager;

    .line 2372803
    iget-object v0, p0, LX/Gcw;->d:Lcom/facebook/widget/ListViewFriendlyViewPager;

    invoke-virtual {p0}, LX/Gcw;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const p1, 0x7f0b1539

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    .line 2372804
    iget-object v0, p0, LX/Gcw;->d:Lcom/facebook/widget/ListViewFriendlyViewPager;

    new-instance v1, LX/Gcu;

    invoke-direct {v1, p0}, LX/Gcu;-><init>(LX/Gcw;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(LX/0hc;)V

    .line 2372805
    invoke-static {p0}, LX/Gcw;->c(LX/Gcw;)V

    .line 2372806
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Gcw;

    invoke-static {p0}, LX/1nQ;->b(LX/0QB;)LX/1nQ;

    move-result-object p0

    check-cast p0, LX/1nQ;

    iput-object p0, p1, LX/Gcw;->a:LX/1nQ;

    return-void
.end method

.method public static c(LX/Gcw;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2372817
    invoke-virtual {p0}, LX/Gcw;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    .line 2372818
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2372819
    const v0, 0x7f0d0ece

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    .line 2372820
    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2372821
    invoke-virtual {p0}, LX/Gcw;->getViewAllText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 2372822
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2372823
    iget-object v1, p0, LX/Gcw;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2372824
    iget-object v1, p0, LX/Gcw;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2372825
    iget-object v0, p0, LX/Gcw;->e:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2372826
    iget-object v0, p0, LX/Gcw;->e:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, LX/Gcw;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2372827
    iget-object v0, p0, LX/Gcw;->f:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/Gcw;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2372828
    :goto_0
    return-void

    .line 2372829
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Dashboard cards must have a title."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2372830
    :cond_1
    iget-object v0, p0, LX/Gcw;->e:Lcom/facebook/widget/text/BetterTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2372831
    iget-object v0, p0, LX/Gcw;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2372832
    iget-object v0, p0, LX/Gcw;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0, v2}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public static d(LX/Gcw;I)V
    .locals 10

    .prologue
    .line 2372807
    invoke-virtual {p0, p1}, LX/Gcw;->c(I)Ljava/lang/String;

    move-result-object v0

    .line 2372808
    invoke-virtual {p0}, LX/Gcw;->getEventAnalyticsParams()Lcom/facebook/events/common/EventAnalyticsParams;

    move-result-object v1

    .line 2372809
    iget-object v2, p0, LX/Gcw;->g:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    if-nez v1, :cond_1

    .line 2372810
    :cond_0
    :goto_0
    return-void

    .line 2372811
    :cond_1
    iget-object v1, p0, LX/Gcw;->g:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2372812
    iget-object v1, p0, LX/Gcw;->a:LX/1nQ;

    invoke-virtual {p0}, LX/Gcw;->getModule()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, LX/Gcw;->getEventAnalyticsParams()Lcom/facebook/events/common/EventAnalyticsParams;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/events/common/EventAnalyticsParams;->b:Lcom/facebook/events/common/EventActionContext;

    .line 2372813
    iget-object p0, v3, Lcom/facebook/events/common/EventActionContext;->e:Lcom/facebook/events/common/ActionSource;

    move-object v3, p0

    .line 2372814
    invoke-virtual {v3}, Lcom/facebook/events/common/ActionSource;->getParamValue()I

    move-result v3

    .line 2372815
    const-string v5, "view"

    move-object v4, v1

    move-object v6, v0

    move-object v7, v2

    move v8, p1

    move v9, v3

    invoke-static/range {v4 .. v9}, LX/1nQ;->a(LX/1nQ;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 2372816
    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 2372782
    iget-object v0, p0, LX/Gcw;->e:Lcom/facebook/widget/text/BetterTextView;

    iget-object v1, p0, LX/Gcw;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {p0}, LX/Gcw;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/high16 v3, 0x10e0000

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, LX/Gd1;->a(Landroid/view/View;Landroid/view/View;J)V

    .line 2372783
    return-void
.end method

.method public final a(III)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 2372784
    invoke-virtual {p0}, LX/Gcw;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 2372785
    invoke-virtual {p0}, LX/Gcw;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 2372786
    sget-object v0, Landroid/graphics/PorterDuff$Mode;->SRC_ATOP:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v1, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(ILandroid/graphics/PorterDuff$Mode;)V

    .line 2372787
    const v0, 0x7f0d0f52

    invoke-virtual {p0, v0}, LX/Gcw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/ImageWithTextView;

    .line 2372788
    invoke-virtual {v0, p1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setText(I)V

    .line 2372789
    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setTextColor(I)V

    .line 2372790
    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2372791
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/text/ImageWithTextView;->setVisibility(I)V

    .line 2372792
    const v0, 0x7f0d0f4d

    invoke-virtual {p0, v0}, LX/Gcw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2372793
    iget-object v0, p0, LX/Gcw;->e:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, v3}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2372794
    return-void
.end method

.method public abstract c(I)Ljava/lang/String;
.end method

.method public abstract getEventAnalyticsParams()Lcom/facebook/events/common/EventAnalyticsParams;
.end method

.method public abstract getModule()Ljava/lang/String;
.end method

.method public abstract getTitle()Ljava/lang/CharSequence;
.end method

.method public abstract getViewAllText()Ljava/lang/CharSequence;
.end method

.method public setPagerAdapter(LX/Gcz;)V
    .locals 3
    .param p1    # LX/Gcz;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2372772
    invoke-virtual {p1}, LX/0gG;->b()I

    move-result v0

    if-nez v0, :cond_0

    .line 2372773
    iget-object v0, p0, LX/Gcw;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2372774
    const v0, 0x7f0d0f4f

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2372775
    :goto_0
    return-void

    .line 2372776
    :cond_0
    iget-object v0, p0, LX/Gcw;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2372777
    invoke-static {p0}, LX/Gcw;->c(LX/Gcw;)V

    .line 2372778
    iget-object v0, p0, LX/Gcw;->d:Lcom/facebook/widget/ListViewFriendlyViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setAdapter(LX/0gG;)V

    .line 2372779
    iput-object p0, p1, LX/Gcz;->j:LX/Gcv;

    .line 2372780
    iget-object v0, p0, LX/Gcw;->d:Lcom/facebook/widget/ListViewFriendlyViewPager;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/ListViewFriendlyViewPager;->setVisibility(I)V

    .line 2372781
    invoke-static {p0, v1}, LX/Gcw;->d(LX/Gcw;I)V

    goto :goto_0
.end method
