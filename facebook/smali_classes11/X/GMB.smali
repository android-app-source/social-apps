.class public final enum LX/GMB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GMB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GMB;

.field public static final enum EMPTY_SELECTED:LX/GMB;

.field public static final enum EMPTY_UNSELECTED:LX/GMB;

.field public static final enum HIDDEN:LX/GMB;

.field public static final enum INVALID:LX/GMB;

.field public static final enum VALID:LX/GMB;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2344067
    new-instance v0, LX/GMB;

    const-string v1, "EMPTY_UNSELECTED"

    invoke-direct {v0, v1, v2}, LX/GMB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GMB;->EMPTY_UNSELECTED:LX/GMB;

    .line 2344068
    new-instance v0, LX/GMB;

    const-string v1, "EMPTY_SELECTED"

    invoke-direct {v0, v1, v3}, LX/GMB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GMB;->EMPTY_SELECTED:LX/GMB;

    .line 2344069
    new-instance v0, LX/GMB;

    const-string v1, "VALID"

    invoke-direct {v0, v1, v4}, LX/GMB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GMB;->VALID:LX/GMB;

    .line 2344070
    new-instance v0, LX/GMB;

    const-string v1, "INVALID"

    invoke-direct {v0, v1, v5}, LX/GMB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GMB;->INVALID:LX/GMB;

    .line 2344071
    new-instance v0, LX/GMB;

    const-string v1, "HIDDEN"

    invoke-direct {v0, v1, v6}, LX/GMB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GMB;->HIDDEN:LX/GMB;

    .line 2344072
    const/4 v0, 0x5

    new-array v0, v0, [LX/GMB;

    sget-object v1, LX/GMB;->EMPTY_UNSELECTED:LX/GMB;

    aput-object v1, v0, v2

    sget-object v1, LX/GMB;->EMPTY_SELECTED:LX/GMB;

    aput-object v1, v0, v3

    sget-object v1, LX/GMB;->VALID:LX/GMB;

    aput-object v1, v0, v4

    sget-object v1, LX/GMB;->INVALID:LX/GMB;

    aput-object v1, v0, v5

    sget-object v1, LX/GMB;->HIDDEN:LX/GMB;

    aput-object v1, v0, v6

    sput-object v0, LX/GMB;->$VALUES:[LX/GMB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2344066
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GMB;
    .locals 1

    .prologue
    .line 2344064
    const-class v0, LX/GMB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GMB;

    return-object v0
.end method

.method public static values()[LX/GMB;
    .locals 1

    .prologue
    .line 2344065
    sget-object v0, LX/GMB;->$VALUES:[LX/GMB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GMB;

    return-object v0
.end method
