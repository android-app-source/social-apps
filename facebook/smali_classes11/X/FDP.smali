.class public LX/FDP;
.super LX/Dny;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public final b:LX/2OQ;

.field private final c:LX/FDI;

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;"
        }
    .end annotation
.end field

.field private e:LX/3fv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:LX/6Po;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private g:LX/2N9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private h:LX/6bg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:LX/DdG;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/2Ow;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private k:LX/2Mq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Di5;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private m:LX/2Oi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStoreIncremental;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3QU;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/deliveryreceipt/SendDeliveryReceiptManager;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8t1;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private r:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private s:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2212002
    const-class v0, LX/FDP;

    sput-object v0, LX/FDP;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;LX/2OQ;LX/FDI;LX/0Ot;)V
    .locals 1
    .param p1    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/2OQ;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/FDI;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0Ot;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/2OQ;",
            "LX/FDI;",
            "LX/0Ot",
            "<",
            "LX/2Oe;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2212003
    invoke-direct {p0, p1}, LX/Dny;-><init>(Ljava/lang/String;)V

    .line 2212004
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2212005
    iput-object v0, p0, LX/FDP;->n:LX/0Ot;

    .line 2212006
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2212007
    iput-object v0, p0, LX/FDP;->o:LX/0Ot;

    .line 2212008
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2212009
    iput-object v0, p0, LX/FDP;->p:LX/0Ot;

    .line 2212010
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2212011
    iput-object v0, p0, LX/FDP;->q:LX/0Ot;

    .line 2212012
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2212013
    iput-object v0, p0, LX/FDP;->s:LX/0Ot;

    .line 2212014
    iput-object p2, p0, LX/FDP;->b:LX/2OQ;

    .line 2212015
    iput-object p3, p0, LX/FDP;->c:LX/FDI;

    .line 2212016
    iput-object p4, p0, LX/FDP;->d:LX/0Ot;

    .line 2212017
    return-void
.end method

.method public static a(LX/FDP;LX/3fv;LX/6Po;LX/2N9;LX/6bg;LX/DdG;LX/2Ow;LX/2Mq;LX/0Or;LX/2Oi;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Or;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FDP;",
            "LX/3fv;",
            "LX/6Po;",
            "LX/2N9;",
            "LX/6bg;",
            "LX/DdG;",
            "LX/2Ow;",
            "LX/2Mq;",
            "LX/0Or",
            "<",
            "LX/Di5;",
            ">;",
            "LX/2Oi;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/auth/datastore/LoggedInUserAuthDataStoreIncremental;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3QU;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/deliveryreceipt/SendDeliveryReceiptManager;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8t1;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2212018
    iput-object p1, p0, LX/FDP;->e:LX/3fv;

    iput-object p2, p0, LX/FDP;->f:LX/6Po;

    iput-object p3, p0, LX/FDP;->g:LX/2N9;

    iput-object p4, p0, LX/FDP;->h:LX/6bg;

    iput-object p5, p0, LX/FDP;->i:LX/DdG;

    iput-object p6, p0, LX/FDP;->j:LX/2Ow;

    iput-object p7, p0, LX/FDP;->k:LX/2Mq;

    iput-object p8, p0, LX/FDP;->l:LX/0Or;

    iput-object p9, p0, LX/FDP;->m:LX/2Oi;

    iput-object p10, p0, LX/FDP;->n:LX/0Ot;

    iput-object p11, p0, LX/FDP;->o:LX/0Ot;

    iput-object p12, p0, LX/FDP;->p:LX/0Ot;

    iput-object p13, p0, LX/FDP;->q:LX/0Ot;

    iput-object p14, p0, LX/FDP;->r:LX/0Or;

    iput-object p15, p0, LX/FDP;->s:LX/0Ot;

    return-void
.end method

.method private a(Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;)V
    .locals 4

    .prologue
    .line 2212019
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->c:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p1, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->d:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 2212020
    :goto_0
    if-nez v0, :cond_0

    .line 2212021
    iget-object v0, p0, LX/FDP;->j:LX/2Ow;

    iget-wide v2, p1, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->e:J

    invoke-virtual {v0, v2, v3}, LX/2Ow;->a(J)V

    .line 2212022
    :cond_0
    return-void

    .line 2212023
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final B(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 2212024
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2212025
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    .line 2212026
    new-instance v1, LX/3hB;

    invoke-direct {v1, v0}, LX/3hB;-><init>(Lcom/facebook/contacts/graphql/Contact;)V

    const/4 v0, 0x1

    .line 2212027
    iput-boolean v0, v1, LX/3hB;->z:Z

    .line 2212028
    move-object v0, v1

    .line 2212029
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;->CONNECTED:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 2212030
    iput-object v1, v0, LX/3hB;->P:Lcom/facebook/graphql/enums/GraphQLContactConnectionStatus;

    .line 2212031
    move-object v0, v0

    .line 2212032
    invoke-virtual {v0}, LX/3hB;->O()Lcom/facebook/contacts/graphql/Contact;

    move-result-object v0

    .line 2212033
    iget-object v1, p0, LX/FDP;->e:LX/3fv;

    invoke-virtual {v1, v0}, LX/3fv;->a(Lcom/facebook/contacts/graphql/Contact;)V

    .line 2212034
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final D(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 13

    .prologue
    .line 2212035
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2212036
    if-nez v0, :cond_0

    .line 2212037
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "operationParams.getBundle() is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2212038
    :goto_0
    return-object v0

    .line 2212039
    :cond_0
    const-string v1, "createLocalAdminMessageParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/CreateLocalAdminMessageParams;

    .line 2212040
    if-eqz v0, :cond_1

    .line 2212041
    iget-object v1, v0, Lcom/facebook/messaging/service/model/CreateLocalAdminMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v1, v1

    .line 2212042
    if-nez v1, :cond_2

    .line 2212043
    :cond_1
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "adminMessage is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forException(Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2212044
    :cond_2
    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 2212045
    iget-object v7, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v10, v7

    .line 2212046
    if-eqz v10, :cond_6

    move v7, v9

    :goto_1
    invoke-static {v7}, LX/0PB;->checkArgument(Z)V

    .line 2212047
    const-string v7, "createLocalAdminMessageParams"

    invoke-virtual {v10, v7}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v7

    check-cast v7, Lcom/facebook/messaging/service/model/CreateLocalAdminMessageParams;

    .line 2212048
    if-eqz v7, :cond_3

    .line 2212049
    iget-object v10, v7, Lcom/facebook/messaging/service/model/CreateLocalAdminMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v10, v10

    .line 2212050
    if-eqz v10, :cond_3

    move v8, v9

    :cond_3
    invoke-static {v8}, LX/0PB;->checkArgument(Z)V

    .line 2212051
    iget-object v8, v7, Lcom/facebook/messaging/service/model/CreateLocalAdminMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v8, v8

    .line 2212052
    iget-object v11, v8, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2212053
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadParams;->newBuilder()LX/6iM;

    move-result-object v8

    invoke-static {v11}, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-result-object v10

    .line 2212054
    iput-object v10, v8, LX/6iM;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    .line 2212055
    move-object v8, v8

    .line 2212056
    sget-object v10, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    .line 2212057
    iput-object v10, v8, LX/6iM;->b:LX/0rS;

    .line 2212058
    move-object v8, v8

    .line 2212059
    const/16 v10, 0x14

    .line 2212060
    iput v10, v8, LX/6iM;->g:I

    .line 2212061
    move-object v8, v8

    .line 2212062
    invoke-virtual {v8}, LX/6iM;->j()Lcom/facebook/messaging/service/model/FetchThreadParams;

    move-result-object v8

    .line 2212063
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 2212064
    const-string v12, "fetchThreadParams"

    invoke-virtual {v10, v12, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2212065
    invoke-static {}, LX/1qK;->builder()LX/4BI;

    move-result-object v8

    invoke-virtual {v8, p1}, LX/4BI;->from(LX/1qK;)LX/4BI;

    move-result-object v8

    const-string v12, "fetch_thread"

    .line 2212066
    iput-object v12, v8, LX/4BI;->mType:Ljava/lang/String;

    .line 2212067
    move-object v8, v8

    .line 2212068
    iput-object v10, v8, LX/4BI;->mBundle:Landroid/os/Bundle;

    .line 2212069
    move-object v8, v8

    .line 2212070
    invoke-virtual {v8}, LX/4BI;->build()LX/1qK;

    move-result-object v8

    .line 2212071
    invoke-virtual {p0, v8, p2}, LX/FDP;->c(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v10

    .line 2212072
    iget-boolean v8, v10, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v8, v8

    .line 2212073
    if-eqz v8, :cond_7

    .line 2212074
    iget-boolean v8, v7, Lcom/facebook/messaging/service/model/CreateLocalAdminMessageParams;->b:Z

    move v8, v8

    .line 2212075
    if-eqz v8, :cond_7

    .line 2212076
    invoke-virtual {v10}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2212077
    iget-object v8, v8, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-nez v8, :cond_7

    .line 2212078
    new-instance v8, Lcom/facebook/user/model/UserFbidIdentifier;

    iget-wide v11, v11, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-static {v11, v12}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v10}, Lcom/facebook/user/model/UserFbidIdentifier;-><init>(Ljava/lang/String;)V

    .line 2212079
    new-instance v10, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;

    const/4 v11, 0x0

    .line 2212080
    iget-object v12, v7, Lcom/facebook/messaging/service/model/CreateLocalAdminMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v7, v12

    .line 2212081
    invoke-static {v8}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-direct {v10, v11, v7, v8}, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;-><init>(Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;Ljava/util/List;)V

    .line 2212082
    iput-boolean v9, v10, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->d:Z

    .line 2212083
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 2212084
    const-string v8, "createThreadParams"

    invoke-virtual {v7, v8, v10}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2212085
    invoke-static {}, LX/1qK;->builder()LX/4BI;

    move-result-object v8

    invoke-virtual {v8, p1}, LX/4BI;->from(LX/1qK;)LX/4BI;

    move-result-object v8

    const-string v9, "create_thread"

    .line 2212086
    iput-object v9, v8, LX/4BI;->mType:Ljava/lang/String;

    .line 2212087
    move-object v8, v8

    .line 2212088
    iput-object v7, v8, LX/4BI;->mBundle:Landroid/os/Bundle;

    .line 2212089
    move-object v7, v8

    .line 2212090
    invoke-virtual {v7}, LX/4BI;->build()LX/1qK;

    move-result-object v7

    .line 2212091
    invoke-interface {p2, v7}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v9

    .line 2212092
    invoke-virtual {v9}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2212093
    iget-object v8, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/2Oe;

    invoke-virtual {v8, v7}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;)V

    move-object v7, v9

    .line 2212094
    :goto_2
    move-object v1, v7

    .line 2212095
    iget-boolean v2, v1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v2, v2

    .line 2212096
    if-eqz v2, :cond_5

    .line 2212097
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2212098
    iget-object v1, v1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v1, :cond_4

    .line 2212099
    iget-object v1, v0, Lcom/facebook/messaging/service/model/CreateLocalAdminMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v2, v1

    .line 2212100
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2212101
    iget-object v1, p0, LX/FDP;->b:LX/2OQ;

    const/4 v3, 0x0

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual/range {v1 .. v6}, LX/2OQ;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;JLjava/lang/Boolean;)V

    .line 2212102
    iget-object v1, p0, LX/FDP;->j:LX/2Ow;

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1, v2}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    goto/16 :goto_0

    .line 2212103
    :cond_4
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    const-string v1, "empty thread"

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    move-object v0, v1

    goto/16 :goto_0

    :cond_6
    move v7, v8

    .line 2212104
    goto/16 :goto_1

    :cond_7
    move-object v7, v10

    goto :goto_2
.end method

.method public final E(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 9

    .prologue
    .line 2212105
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v3, v0

    .line 2212106
    const-string v0, "message"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/messages/Message;

    .line 2212107
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v6

    .line 2212108
    invoke-virtual {v6}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2212109
    if-eqz v0, :cond_1

    .line 2212110
    iget-object v2, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Oe;

    invoke-virtual {v2, v0}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/NewMessageResult;)V

    .line 2212111
    const-string v0, "delete_msg_id"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2212112
    if-eqz v0, :cond_0

    .line 2212113
    iget-object v2, p0, LX/FDP;->b:LX/2OQ;

    invoke-virtual {v2, v0}, LX/2OQ;->b(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    .line 2212114
    if-eqz v2, :cond_0

    .line 2212115
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2212116
    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    .line 2212117
    const-string v5, "deleteMessagesParams"

    new-instance v7, Lcom/facebook/messaging/service/model/DeleteMessagesParams;

    sget-object v8, LX/6hi;->CLIENT_ONLY:LX/6hi;

    iget-object v2, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct {v7, v0, v8, v2}, Lcom/facebook/messaging/service/model/DeleteMessagesParams;-><init>(LX/0Rf;LX/6hi;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    invoke-virtual {v4, v5, v7}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2212118
    invoke-static {}, LX/1qK;->builder()LX/4BI;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/4BI;->from(LX/1qK;)LX/4BI;

    move-result-object v0

    const-string v2, "delete_messages"

    .line 2212119
    iput-object v2, v0, LX/4BI;->mType:Ljava/lang/String;

    .line 2212120
    move-object v0, v0

    .line 2212121
    iput-object v4, v0, LX/4BI;->mBundle:Landroid/os/Bundle;

    .line 2212122
    move-object v0, v0

    .line 2212123
    invoke-virtual {v0}, LX/4BI;->build()LX/1qK;

    move-result-object v0

    .line 2212124
    invoke-virtual {p0, v0, p2}, LX/FDP;->n(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;

    .line 2212125
    :cond_0
    iget-object v0, p0, LX/FDP;->j:LX/2Ow;

    iget-object v2, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v2}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2212126
    const-string v0, "should_show_notification"

    const/4 v2, 0x1

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2212127
    const-string v0, "only_notify_from_chathead"

    const/4 v2, 0x0

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v4, Lcom/facebook/push/PushProperty;

    sget-object v0, LX/3B4;->SMS_READONLY_MODE:LX/3B4;

    invoke-direct {v4, v0}, Lcom/facebook/push/PushProperty;-><init>(LX/3B4;)V

    .line 2212128
    :goto_0
    iget-object v0, p0, LX/FDP;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3QU;

    iget-object v2, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    sget-object v3, Lcom/facebook/messaging/model/threads/ThreadCustomization;->a:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    sget-object v5, LX/03R;->UNSET:LX/03R;

    invoke-virtual/range {v0 .. v5}, LX/3QU;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/threads/ThreadCustomization;Lcom/facebook/push/PushProperty;LX/03R;)Lcom/facebook/messaging/notify/NewMessageNotification;

    move-result-object v1

    .line 2212129
    iget-object v0, p0, LX/FDP;->l:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Di5;

    invoke-virtual {v0, v1}, LX/Di5;->a(Lcom/facebook/messaging/notify/NewMessageNotification;)V

    .line 2212130
    :cond_1
    return-object v6

    .line 2212131
    :cond_2
    new-instance v4, Lcom/facebook/push/PushProperty;

    sget-object v0, LX/3B4;->SMS_DEFAULT_APP:LX/3B4;

    invoke-direct {v4, v0}, Lcom/facebook/push/PushProperty;-><init>(LX/3B4;)V

    goto :goto_0
.end method

.method public final G(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2212132
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v3

    .line 2212133
    if-eqz v3, :cond_0

    .line 2212134
    iget-boolean v0, v3, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2212135
    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 2212136
    :goto_0
    return-object v0

    .line 2212137
    :cond_1
    invoke-virtual {v3}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/UpdateFolderCountsResult;

    .line 2212138
    if-nez v0, :cond_2

    move-object v0, v1

    .line 2212139
    goto :goto_0

    .line 2212140
    :cond_2
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 2212141
    const-string v2, "updateFolderCountsParams"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;

    .line 2212142
    iget-object v2, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Oe;

    iget-object v1, v1, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;->a:LX/6ek;

    iget-object v0, v0, Lcom/facebook/messaging/service/model/UpdateFolderCountsResult;->a:Lcom/facebook/messaging/model/folders/FolderCounts;

    invoke-virtual {v2, v1, v0}, LX/2Oe;->a(LX/6ek;Lcom/facebook/messaging/model/folders/FolderCounts;)V

    move-object v0, v3

    .line 2212143
    goto :goto_0
.end method

.method public final K(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2212144
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2212145
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/EditUsernameResult;

    .line 2212146
    if-eqz v0, :cond_0

    .line 2212147
    new-instance v3, LX/0XI;

    invoke-direct {v3}, LX/0XI;-><init>()V

    iget-object v1, p0, LX/FDP;->r:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/User;

    invoke-virtual {v3, v1}, LX/0XI;->a(Lcom/facebook/user/model/User;)LX/0XI;

    move-result-object v1

    iget-object v0, v0, Lcom/facebook/messaging/service/model/EditUsernameResult;->a:Ljava/lang/String;

    .line 2212148
    iput-object v0, v1, LX/0XI;->l:Ljava/lang/String;

    .line 2212149
    move-object v0, v1

    .line 2212150
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v1

    .line 2212151
    iget-object v0, p0, LX/FDP;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0WJ;

    invoke-virtual {v0, v1}, LX/0WJ;->a(Lcom/facebook/user/model/User;)V

    .line 2212152
    iget-object v0, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oe;

    invoke-virtual {v0, v1}, LX/2Oe;->a(Lcom/facebook/user/model/User;)V

    .line 2212153
    iget-object v0, p0, LX/FDP;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8t1;

    .line 2212154
    iget-object v3, v1, Lcom/facebook/user/model/User;->ah:Lcom/facebook/user/model/UserKey;

    move-object v1, v3

    .line 2212155
    new-instance v3, Landroid/content/Intent;

    const-string p0, "com.facebook.user.broadcast.ACTION_USERNAME_UPDATED"

    invoke-direct {v3, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2212156
    const-string p0, "updated_user"

    invoke-virtual {v3, p0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2212157
    iget-object p0, v0, LX/8t1;->a:LX/0Xl;

    invoke-interface {p0, v3}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 2212158
    :cond_0
    return-object v2
.end method

.method public final L(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 2212159
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2212160
    sget-object v1, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledParams;

    .line 2212161
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 2212162
    iget-object v2, p0, LX/FDP;->b:LX/2OQ;

    .line 2212163
    iget-object v3, v0, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v3

    .line 2212164
    invoke-virtual {v2, v0}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    .line 2212165
    if-eqz v2, :cond_0

    .line 2212166
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledResult;

    .line 2212167
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2212168
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v3

    .line 2212169
    iget-boolean v4, v0, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledResult;->a:Z

    move v0, v4

    .line 2212170
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    .line 2212171
    iput-object v0, v3, LX/6g6;->S:LX/03R;

    .line 2212172
    move-object v0, v3

    .line 2212173
    iget-object v3, p0, LX/FDP;->b:LX/2OQ;

    invoke-virtual {v0}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    .line 2212174
    sget-object v4, LX/0SF;->a:LX/0SF;

    move-object v4, v4

    .line 2212175
    invoke-virtual {v4}, LX/0SF;->a()J

    move-result-wide v4

    invoke-virtual {v3, v0, v4, v5}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2212176
    iget-object v0, p0, LX/FDP;->j:LX/2Ow;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0, v2}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2212177
    :cond_0
    return-object v1
.end method

.method public final M(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2212178
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2212179
    sget-object v1, Lcom/facebook/messaging/service/model/FetchEventRemindersMembersParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchEventRemindersMembersParams;

    .line 2212180
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2212181
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/service/model/FetchEventRemindersMembersResult;

    .line 2212182
    if-eqz v1, :cond_0

    .line 2212183
    iget-object v3, v1, Lcom/facebook/messaging/service/model/FetchEventRemindersMembersResult;->a:LX/0Px;

    move-object v3, v3

    .line 2212184
    if-eqz v3, :cond_0

    .line 2212185
    iget-object v3, p0, LX/FDP;->b:LX/2OQ;

    .line 2212186
    iget-object p1, v1, Lcom/facebook/messaging/service/model/FetchEventRemindersMembersResult;->a:LX/0Px;

    move-object v1, p1

    .line 2212187
    invoke-virtual {v3, v1}, LX/2OQ;->a(LX/0Px;)V

    .line 2212188
    iget-object v1, p0, LX/FDP;->j:LX/2Ow;

    .line 2212189
    iget-object v3, v0, Lcom/facebook/messaging/service/model/FetchEventRemindersMembersParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v3

    .line 2212190
    invoke-virtual {v1, v0}, LX/2Ow;->e(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2212191
    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2212192
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v2

    goto :goto_0
.end method

.method public final O(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2212193
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2212194
    const-string v3, "fetchPinnedThreadsParams"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;

    .line 2212195
    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->b:LX/0rS;

    sget-object v3, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    if-ne v0, v3, :cond_2

    move v0, v1

    .line 2212196
    :goto_0
    iget-object v3, p0, LX/FDP;->b:LX/2OQ;

    invoke-virtual {v3}, LX/2OQ;->g()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, LX/FDP;->b:LX/2OQ;

    invoke-virtual {v3}, LX/2OQ;->h()Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    .line 2212197
    :cond_1
    if-eqz v2, :cond_3

    .line 2212198
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->newBuilder()LX/6hv;

    move-result-object v0

    .line 2212199
    iput-boolean v1, v0, LX/6hv;->b:Z

    .line 2212200
    move-object v0, v0

    .line 2212201
    iget-object v1, p0, LX/FDP;->b:LX/2OQ;

    invoke-virtual {v1}, LX/2OQ;->c()LX/0Px;

    move-result-object v1

    .line 2212202
    iput-object v1, v0, LX/6hv;->a:Ljava/util/List;

    .line 2212203
    move-object v0, v0

    .line 2212204
    invoke-virtual {v0}, LX/6hv;->e()Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2212205
    :goto_1
    return-object v0

    :cond_2
    move v0, v2

    .line 2212206
    goto :goto_0

    .line 2212207
    :cond_3
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2212208
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    .line 2212209
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    .line 2212210
    iget-object v2, v1, LX/2Oe;->a:LX/2OQ;

    iget-object v3, v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->c:LX/0Px;

    .line 2212211
    iget-object v1, v2, LX/2OQ;->p:LX/2Oc;

    invoke-static {v2, v3, v1}, LX/2OQ;->a(LX/2OQ;Ljava/util/List;LX/2Oc;)V

    .line 2212212
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_1
.end method

.method public final P(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2212213
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2212214
    const-string v3, "fetchPinnedThreadsParams"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;

    .line 2212215
    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->b:LX/0rS;

    sget-object v3, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    if-ne v0, v3, :cond_2

    move v0, v1

    .line 2212216
    :goto_0
    iget-object v3, p0, LX/FDP;->b:LX/2OQ;

    invoke-virtual {v3}, LX/2OQ;->i()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, LX/FDP;->b:LX/2OQ;

    invoke-virtual {v3}, LX/2OQ;->j()Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    .line 2212217
    :cond_1
    if-eqz v2, :cond_3

    .line 2212218
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->newBuilder()LX/6hv;

    move-result-object v0

    .line 2212219
    iput-boolean v1, v0, LX/6hv;->b:Z

    .line 2212220
    move-object v0, v0

    .line 2212221
    iget-object v1, p0, LX/FDP;->b:LX/2OQ;

    invoke-virtual {v1}, LX/2OQ;->d()LX/0Px;

    move-result-object v1

    .line 2212222
    iput-object v1, v0, LX/6hv;->a:Ljava/util/List;

    .line 2212223
    move-object v0, v0

    .line 2212224
    invoke-virtual {v0}, LX/6hv;->e()Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2212225
    :goto_1
    return-object v0

    :cond_2
    move v0, v2

    .line 2212226
    goto :goto_0

    .line 2212227
    :cond_3
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2212228
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    .line 2212229
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    .line 2212230
    iget-object v2, v1, LX/2Oe;->a:LX/2OQ;

    iget-object v3, v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->c:LX/0Px;

    .line 2212231
    iget-object v1, v2, LX/2OQ;->q:LX/2Oc;

    invoke-static {v2, v3, v1}, LX/2OQ;->a(LX/2OQ;Ljava/util/List;LX/2Oc;)V

    .line 2212232
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_1
.end method

.method public final Q(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 2212233
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2212234
    const-string v1, "updateAgentSuggestionsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/UpdateAgentSuggestionsParams;

    .line 2212235
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    .line 2212236
    iget-object p0, v1, LX/2Oe;->a:LX/2OQ;

    invoke-virtual {p0, v0}, LX/2OQ;->a(Lcom/facebook/messaging/service/model/UpdateAgentSuggestionsParams;)V

    .line 2212237
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 8

    .prologue
    .line 2212238
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v2, v0

    .line 2212239
    const-string v0, "logger_instance_key"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 2212240
    iget-object v0, p0, LX/FDP;->k:LX/2Mq;

    .line 2212241
    iget-object v1, v0, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v4, 0x540012

    const/16 v5, 0x22

    invoke-interface {v1, v4, v3, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 2212242
    const-string v0, "fetchThreadListParams"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;

    .line 2212243
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v4, v1

    .line 2212244
    iget-object v1, p0, LX/FDP;->f:LX/6Po;

    sget-object v5, LX/FDy;->c:LX/6Pr;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "fetchThreadList (CSH) (folder="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ")"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 2212245
    const-string v1, "logger_instance_key"

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2212246
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "handleFetchThreadList with freshness="

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2212247
    iget-object v5, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->a:LX/0rS;

    move-object v5, v5

    .line 2212248
    invoke-virtual {v5}, LX/0rS;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2212249
    iget-object v1, p0, LX/FDP;->c:LX/FDI;

    invoke-virtual {v1, v0}, LX/FDI;->a(Lcom/facebook/messaging/service/model/FetchThreadListParams;)Lcom/facebook/messaging/service/model/FetchThreadListParams;

    move-result-object v1

    .line 2212250
    if-eq v0, v1, :cond_0

    .line 2212251
    const-string v0, "fetchThreadListParams"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2212252
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "handleFetchThreadList upgraded to "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2212253
    iget-object v2, v1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->a:LX/0rS;

    move-object v2, v2

    .line 2212254
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-object v0, v1

    .line 2212255
    :cond_0
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->a:LX/0rS;

    move-object v0, v1

    .line 2212256
    iget-object v1, p0, LX/FDP;->c:LX/FDI;

    invoke-virtual {v1, v4, v0}, LX/FDI;->a(LX/6ek;LX/0rS;)Z

    move-result v0

    .line 2212257
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleFetchThreadList canServeFromCache="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 2212258
    if-eqz v0, :cond_2

    .line 2212259
    iget-object v0, p0, LX/FDP;->c:LX/FDI;

    invoke-virtual {v0, v4}, LX/FDI;->a(LX/6ek;)Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v0

    .line 2212260
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v1, v0

    .line 2212261
    :goto_0
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;

    .line 2212262
    iget-object v2, p0, LX/FDP;->k:LX/2Mq;

    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    const v6, 0x540012

    .line 2212263
    if-eqz v0, :cond_1

    iget-object v4, v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->dataSource:LX/4B1;

    if-nez v4, :cond_4

    .line 2212264
    :cond_1
    iget-object v4, v2, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v5, 0x21

    invoke-interface {v4, v6, v3, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 2212265
    :goto_1
    return-object v1

    .line 2212266
    :cond_2
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2212267
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;

    .line 2212268
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    iget-object v1, v1, Lcom/facebook/fbservice/results/DataFetchDisposition;->needsInitialFetch:LX/03R;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2212269
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    invoke-virtual {v1, v0}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/FetchThreadListResult;)V

    .line 2212270
    iget-object v1, p0, LX/FDP;->k:LX/2Mq;

    .line 2212271
    iget-object v4, v1, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x540012

    const/16 v6, 0x1d

    invoke-interface {v4, v5, v3, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 2212272
    iget-object v1, p0, LX/FDP;->j:LX/2Ow;

    invoke-virtual {v1}, LX/2Ow;->a()V

    .line 2212273
    iget-object v1, p0, LX/FDP;->l:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Di5;

    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;->g:Lcom/facebook/messaging/model/folders/FolderCounts;

    invoke-virtual {v1, v0}, LX/Di5;->a(Lcom/facebook/messaging/model/folders/FolderCounts;)V

    :cond_3
    move-object v1, v2

    goto :goto_0

    .line 2212274
    :cond_4
    invoke-static {v2, v6, v3, v0}, LX/2Mq;->a(LX/2Mq;IILcom/facebook/fbservice/results/DataFetchDisposition;)V

    .line 2212275
    sget-object v4, LX/FCT;->a:[I

    iget-object v5, v0, Lcom/facebook/fbservice/results/DataFetchDisposition;->dataSource:LX/4B1;

    invoke-virtual {v5}, LX/4B1;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    goto :goto_1

    .line 2212276
    :pswitch_0
    iget-object v4, v2, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v5, 0x19

    invoke-interface {v4, v6, v3, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    goto :goto_1

    .line 2212277
    :pswitch_1
    iget-object v4, v2, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v5, 0x1a

    invoke-interface {v4, v6, v3, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    goto :goto_1

    .line 2212278
    :pswitch_2
    iget-object v4, v2, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v5, 0x1c

    invoke-interface {v4, v6, v3, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    goto :goto_1

    .line 2212279
    :pswitch_3
    iget-object v4, v2, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/16 v5, 0x1b

    invoke-interface {v4, v6, v3, v5}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final b(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 9

    .prologue
    .line 2212280
    iget-object v0, p0, LX/FDP;->f:LX/6Po;

    sget-object v1, LX/FDy;->c:LX/6Pr;

    const-string v2, "fetchMoreThreads (CSH)."

    invoke-virtual {v0, v1, v2}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 2212281
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2212282
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;

    .line 2212283
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    .line 2212284
    iget-object v3, v1, LX/2Oe;->l:LX/2Oi;

    iget-object v4, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->d:LX/0Px;

    invoke-virtual {v3, v4}, LX/2Oi;->a(Ljava/util/Collection;)V

    .line 2212285
    iget-object v3, v1, LX/2Oe;->a:LX/2OQ;

    iget-object v4, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->b:LX/6ek;

    iget-object v5, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    iget-wide v7, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->e:J

    invoke-virtual {v3, v4, v5, v7, v8}, LX/2OQ;->a(LX/6ek;Lcom/facebook/messaging/model/threads/ThreadsCollection;J)V

    .line 2212286
    iget-object v3, v1, LX/2Oe;->b:LX/2Of;

    invoke-virtual {v3}, LX/2Of;->a()V

    .line 2212287
    return-object v2
.end method

.method public final c(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 16

    .prologue
    .line 2211929
    invoke-virtual/range {p1 .. p1}, LX/1qK;->getBundle()Landroid/os/Bundle;

    move-result-object v3

    .line 2211930
    const-string v2, "logger_instance_key"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 2211931
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FDP;->k:LX/2Mq;

    invoke-virtual {v2, v7}, LX/2Mq;->f(I)V

    .line 2211932
    const-string v2, "fetchThreadParams"

    invoke-virtual {v3, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/service/model/FetchThreadParams;

    .line 2211933
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FDP;->f:LX/6Po;

    sget-object v5, LX/FDy;->c:LX/6Pr;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v8, "fetchThread (CSH). "

    invoke-direct {v6, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/facebook/messaging/service/model/FetchThreadParams;->a()Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a()Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 2211934
    invoke-virtual {v2}, Lcom/facebook/messaging/service/model/FetchThreadParams;->i()Z

    move-result v8

    .line 2211935
    if-eqz v8, :cond_0

    .line 2211936
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FDP;->h:LX/6bg;

    invoke-virtual {v4}, LX/6bg;->a()V

    .line 2211937
    :cond_0
    invoke-virtual {v2}, Lcom/facebook/messaging/service/model/FetchThreadParams;->d()LX/0Px;

    move-result-object v4

    .line 2211938
    if-eqz v4, :cond_1

    .line 2211939
    move-object/from16 v0, p0

    iget-object v5, v0, LX/FDP;->m:LX/2Oi;

    const/4 v6, 0x1

    invoke-virtual {v5, v4, v6}, LX/2Oi;->a(Ljava/util/Collection;Z)V

    .line 2211940
    :cond_1
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    .line 2211941
    const-string v4, "fetch_location"

    sget-object v5, LX/6bf;->UNKNOWN:LX/6bf;

    invoke-virtual {v5}, LX/6bf;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v9, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2211942
    invoke-static {}, LX/0SF;->b()LX/0SF;

    move-result-object v4

    invoke-virtual {v4}, LX/0SF;->a()J

    move-result-wide v4

    .line 2211943
    move-object/from16 v0, p0

    iget-object v6, v0, LX/FDP;->c:LX/FDI;

    move-object/from16 v0, p1

    invoke-virtual {v6, v0}, LX/FDI;->a(LX/1qK;)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v6

    .line 2211944
    invoke-static {}, LX/0SF;->b()LX/0SF;

    move-result-object v10

    invoke-virtual {v10}, LX/0SF;->a()J

    move-result-wide v10

    sub-long v4, v10, v4

    .line 2211945
    const-string v10, "thread_cache_duration"

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v9, v10, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2211946
    if-eqz v6, :cond_4

    .line 2211947
    const-string v2, "fetch_location"

    sget-object v3, LX/6bf;->THREAD_CACHE:LX/6bf;

    invoke-virtual {v3}, LX/6bf;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v9, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2211948
    invoke-static {v6}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    move-object v3, v2

    .line 2211949
    :goto_0
    invoke-virtual {v3}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2211950
    iget-object v4, v2, Lcom/facebook/messaging/service/model/FetchThreadResult;->i:Ljava/util/Map;

    .line 2211951
    if-eqz v4, :cond_2

    .line 2211952
    invoke-interface {v9, v4}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2211953
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FDP;->g:LX/2N9;

    invoke-virtual {v4, v9}, LX/2N9;->a(Ljava/util/Map;)V

    .line 2211954
    if-eqz v8, :cond_3

    .line 2211955
    invoke-static {v2}, Lcom/facebook/messaging/service/model/FetchThreadResult;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;)LX/6iO;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, LX/FDP;->h:LX/6bg;

    invoke-virtual {v3}, LX/6bg;->b()Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/6iO;->b(LX/0Px;)LX/6iO;

    move-result-object v2

    invoke-virtual {v2}, LX/6iO;->a()Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v2

    .line 2211956
    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v3

    .line 2211957
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2211958
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FDP;->k:LX/2Mq;

    iget-object v2, v2, Lcom/facebook/messaging/service/model/FetchThreadResult;->c:Lcom/facebook/fbservice/results/DataFetchDisposition;

    invoke-virtual {v4, v7, v2}, LX/2Mq;->b(ILcom/facebook/fbservice/results/DataFetchDisposition;)V

    .line 2211959
    return-object v3

    .line 2211960
    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FDP;->h:LX/6bg;

    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;->a()Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/6bg;->a(Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;)V

    .line 2211961
    const-string v4, "logger_instance_key"

    invoke-virtual {v3, v4, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2211962
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-interface {v0, v1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v6

    .line 2211963
    invoke-virtual {v6}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2211964
    iget-object v4, v3, Lcom/facebook/messaging/service/model/FetchThreadResult;->c:Lcom/facebook/fbservice/results/DataFetchDisposition;

    iget-boolean v4, v4, Lcom/facebook/fbservice/results/DataFetchDisposition;->hasData:Z

    if-eqz v4, :cond_9

    .line 2211965
    iget-object v10, v3, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2211966
    if-eqz v10, :cond_6

    const/4 v4, 0x1

    move v5, v4

    .line 2211967
    :goto_1
    const/4 v4, 0x0

    .line 2211968
    if-eqz v5, :cond_5

    .line 2211969
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FDP;->b:LX/2OQ;

    iget-object v11, v10, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v4, v11}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v4

    .line 2211970
    :cond_5
    if-eqz v5, :cond_7

    if-eqz v4, :cond_7

    iget-wide v12, v10, Lcom/facebook/messaging/model/threads/ThreadSummary;->d:J

    const-wide/16 v14, -0x1

    cmp-long v5, v12, v14

    if-eqz v5, :cond_7

    iget-wide v12, v10, Lcom/facebook/messaging/model/threads/ThreadSummary;->d:J

    iget-wide v14, v4, Lcom/facebook/messaging/model/threads/ThreadSummary;->d:J

    cmp-long v5, v12, v14

    if-gez v5, :cond_7

    .line 2211971
    move-object v3, v6

    goto/16 :goto_0

    .line 2211972
    :cond_6
    const/4 v4, 0x0

    move v5, v4

    goto :goto_1

    .line 2211973
    :cond_7
    if-eqz v10, :cond_8

    iget-object v4, v10, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v4}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, v10, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v4}, LX/FNg;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v4

    if-nez v4, :cond_8

    iget-object v4, v10, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v4}, LX/FNg;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v4

    if-nez v4, :cond_8

    iget-object v4, v10, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v4}, LX/FNg;->c(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v4

    if-nez v4, :cond_8

    iget-object v4, v3, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    if-eqz v4, :cond_8

    iget-object v4, v3, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual {v4}, Lcom/facebook/messaging/model/messages/MessagesCollection;->f()Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2211974
    iget-object v2, v3, Lcom/facebook/messaging/service/model/FetchThreadResult;->f:LX/0Px;

    if-eqz v2, :cond_9

    .line 2211975
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FDP;->m:LX/2Oi;

    iget-object v3, v3, Lcom/facebook/messaging/service/model/FetchThreadResult;->f:LX/0Px;

    invoke-virtual {v2, v3}, LX/2Oi;->a(Ljava/util/Collection;)V

    move-object v3, v6

    goto/16 :goto_0

    .line 2211976
    :cond_8
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FDP;->s:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0Uh;

    const/16 v5, 0x1cb

    const/4 v10, 0x0

    invoke-virtual {v4, v5, v10}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-nez v4, :cond_a

    const/4 v4, 0x1

    move v5, v4

    .line 2211977
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/2Oe;

    invoke-virtual {v2}, Lcom/facebook/messaging/service/model/FetchThreadParams;->g()I

    move-result v2

    invoke-virtual {v4, v2, v3, v5}, LX/2Oe;->a(ILcom/facebook/messaging/service/model/FetchThreadResult;Z)V

    .line 2211978
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FDP;->k:LX/2Mq;

    invoke-virtual {v2, v7}, LX/2Mq;->i(I)V

    :cond_9
    move-object v3, v6

    goto/16 :goto_0

    .line 2211979
    :cond_a
    const/4 v4, 0x0

    move v5, v4

    goto :goto_2
.end method

.method public final d(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 11

    .prologue
    .line 2211980
    iget-object v0, p0, LX/FDP;->c:LX/FDI;

    sget-object v1, LX/6ek;->INBOX:LX/6ek;

    sget-object v2, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    invoke-virtual {v0, v1, v2}, LX/FDI;->a(LX/6ek;LX/0rS;)Z

    move-result v0

    .line 2211981
    if-eqz v0, :cond_2

    .line 2211982
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2211983
    const-string v1, "fetch_thread_with_participants_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;

    .line 2211984
    iget-object v1, p0, LX/FDP;->c:LX/FDI;

    sget-object v2, LX/6ek;->INBOX:LX/6ek;

    invoke-virtual {v1, v2}, LX/FDI;->a(LX/6ek;)Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v1

    iget-object v1, v1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2211985
    iget-object v2, v1, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v1, v2

    .line 2211986
    const/4 v4, 0x0

    .line 2211987
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2211988
    invoke-static {v0, v3}, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;->b(Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;Lcom/facebook/messaging/model/threads/ThreadSummary;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 2211989
    const/4 v6, 0x0

    .line 2211990
    :goto_1
    move v6, v6

    .line 2211991
    if-eqz v6, :cond_3

    .line 2211992
    if-eqz v4, :cond_0

    iget-wide v7, v4, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    iget-wide v9, v3, Lcom/facebook/messaging/model/threads/ThreadSummary;->j:J

    cmp-long v6, v7, v9

    if-gez v6, :cond_3

    :cond_0
    :goto_2
    move-object v4, v3

    .line 2211993
    goto :goto_0

    .line 2211994
    :cond_1
    move-object v0, v4

    .line 2211995
    if-eqz v0, :cond_2

    .line 2211996
    new-instance v1, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsResult;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsResult;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2211997
    :goto_3
    return-object v0

    :cond_2
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_3

    :cond_3
    move-object v3, v4

    goto :goto_2

    .line 2211998
    :cond_4
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 2211999
    iget-object v6, v3, Lcom/facebook/messaging/model/threads/ThreadSummary;->h:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/facebook/messaging/model/threads/ThreadParticipant;

    .line 2212000
    invoke-virtual {v6}, Lcom/facebook/messaging/model/threads/ThreadParticipant;->a()Lcom/facebook/user/model/UserKey;

    move-result-object v6

    invoke-interface {v7, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2212001
    :cond_5
    iget-object v6, v0, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;->b:LX/0Rf;

    invoke-interface {v7, v6}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v6

    goto :goto_1
.end method

.method public final e(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2211707
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2211708
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2211709
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v1, :cond_0

    .line 2211710
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    invoke-virtual {v1, v0}, LX/2Oe;->b(Lcom/facebook/messaging/service/model/FetchThreadResult;)V

    .line 2211711
    :cond_0
    return-object v2
.end method

.method public final f(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2211712
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2211713
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2211714
    const-string v1, "createGroupParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 2211715
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2211716
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2211717
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    .line 2211718
    invoke-virtual {v1, v0}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;)V

    .line 2211719
    :cond_0
    return-object v2
.end method

.method public final g(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2211720
    :try_start_0
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2211721
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2211722
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    invoke-virtual {v1, v0}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;)V
    :try_end_0
    .catch LX/FKG; {:try_start_0 .. :try_end_0} :catch_0

    .line 2211723
    return-object v2

    .line 2211724
    :catch_0
    move-exception v0

    .line 2211725
    iget-object v1, v0, LX/FKG;->failedMessage:Lcom/facebook/messaging/model/messages/Message;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v1, :cond_0

    .line 2211726
    iget-object v1, p0, LX/FDP;->b:LX/2OQ;

    iget-object v2, v0, LX/FKG;->failedMessage:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v1, v2}, LX/2OQ;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2211727
    :cond_0
    throw v0
.end method

.method public final i(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2211728
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2211729
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    .line 2211730
    if-eqz v0, :cond_0

    .line 2211731
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;Z)V

    .line 2211732
    invoke-direct {p0, v0}, LX/FDP;->a(Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;)V

    .line 2211733
    :cond_0
    return-object v2
.end method

.method public final j(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 13

    .prologue
    .line 2211734
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2211735
    const-string v1, "markThreadsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;

    .line 2211736
    invoke-static {}, LX/0vV;->u()LX/0vV;

    move-result-object v4

    .line 2211737
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2211738
    new-instance v6, LX/0P2;

    invoke-direct {v6}, LX/0P2;-><init>()V

    .line 2211739
    iget-object v7, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    const/4 v1, 0x0

    move v3, v1

    :goto_0
    if-ge v3, v8, :cond_2

    invoke-virtual {v7, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/service/model/MarkThreadFields;

    .line 2211740
    iget-object v2, v1, Lcom/facebook/messaging/service/model/MarkThreadFields;->f:LX/6ek;

    .line 2211741
    if-nez v2, :cond_0

    .line 2211742
    sget-object v2, LX/6ek;->INBOX:LX/6ek;

    .line 2211743
    :cond_0
    invoke-virtual {v4, v2, v1}, LX/0Xs;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2211744
    iget-boolean v9, v1, Lcom/facebook/messaging/service/model/MarkThreadFields;->b:Z

    if-eqz v9, :cond_1

    .line 2211745
    iget-object v9, v1, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v5, v9}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2211746
    sget-object v9, LX/6ek;->MONTAGE:LX/6ek;

    if-ne v2, v9, :cond_1

    .line 2211747
    iget-object v2, v1, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v10, v1, Lcom/facebook/messaging/service/model/MarkThreadFields;->e:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v6, v2, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2211748
    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 2211749
    :cond_2
    iget-object v2, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->a:LX/6iW;

    .line 2211750
    invoke-virtual {v4}, LX/0Xt;->p()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, LX/6ek;

    .line 2211751
    iget-object v0, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oe;

    new-instance v7, LX/6ie;

    invoke-direct {v7}, LX/6ie;-><init>()V

    .line 2211752
    iput-object v2, v7, LX/6ie;->a:LX/6iW;

    .line 2211753
    move-object v7, v7

    .line 2211754
    invoke-virtual {v4, v1}, LX/0vW;->a(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v8

    invoke-static {v8}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v8

    .line 2211755
    iget-object v9, v7, LX/6ie;->c:LX/0Pz;

    invoke-virtual {v9, v8}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2211756
    move-object v7, v7

    .line 2211757
    invoke-virtual {v7}, LX/6ie;->a()Lcom/facebook/messaging/service/model/MarkThreadsParams;

    move-result-object v7

    const/4 v8, 0x0

    .line 2211758
    iget-object v9, v7, Lcom/facebook/messaging/service/model/MarkThreadsParams;->a:LX/6iW;

    sget-object v10, LX/6iW;->READ:LX/6iW;

    if-ne v9, v10, :cond_3

    .line 2211759
    iget-object v10, v7, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    invoke-virtual {v10}, LX/0Px;->size()I

    move-result v11

    move v9, v8

    :goto_2
    if-ge v9, v11, :cond_6

    invoke-virtual {v10, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/messaging/service/model/MarkThreadFields;

    .line 2211760
    iget-object v12, v0, LX/2Oe;->a:LX/2OQ;

    invoke-virtual {v12, v8}, LX/2OQ;->a(Lcom/facebook/messaging/service/model/MarkThreadFields;)V

    .line 2211761
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_2

    .line 2211762
    :cond_3
    iget-object v9, v7, Lcom/facebook/messaging/service/model/MarkThreadsParams;->a:LX/6iW;

    sget-object v10, LX/6iW;->ARCHIVED:LX/6iW;

    if-eq v9, v10, :cond_4

    iget-object v9, v7, Lcom/facebook/messaging/service/model/MarkThreadsParams;->a:LX/6iW;

    sget-object v10, LX/6iW;->SPAM:LX/6iW;

    if-ne v9, v10, :cond_6

    .line 2211763
    :cond_4
    new-instance v10, LX/0Pz;

    invoke-direct {v10}, LX/0Pz;-><init>()V

    .line 2211764
    iget-object v11, v7, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    move v9, v8

    :goto_3
    if-ge v9, v12, :cond_5

    invoke-virtual {v11, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/messaging/service/model/MarkThreadFields;

    .line 2211765
    iget-object v8, v8, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v10, v8}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2211766
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_3

    .line 2211767
    :cond_5
    iget-object v8, v0, LX/2Oe;->a:LX/2OQ;

    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    invoke-virtual {v8, v1, v9}, LX/2OQ;->a(LX/6ek;LX/0Px;)V

    .line 2211768
    :cond_6
    goto :goto_1

    .line 2211769
    :cond_7
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2211770
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_8

    .line 2211771
    sget-object v1, LX/6iW;->READ:LX/6iW;

    if-ne v2, v1, :cond_b

    .line 2211772
    iget-object v1, p0, LX/FDP;->j:LX/2Ow;

    invoke-virtual {v1, v0}, LX/2Ow;->b(LX/0Px;)V

    .line 2211773
    :cond_8
    :goto_4
    invoke-virtual {v4}, LX/0Xt;->n()Z

    move-result v0

    if-nez v0, :cond_9

    .line 2211774
    iget-object v0, p0, LX/FDP;->j:LX/2Ow;

    invoke-virtual {v0}, LX/2Ow;->c()V

    .line 2211775
    :cond_9
    invoke-virtual {v6}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 2211776
    invoke-virtual {v0}, LX/0P1;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 2211777
    :cond_a
    :goto_5
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 2211778
    :cond_b
    sget-object v1, LX/6iW;->ARCHIVED:LX/6iW;

    if-eq v2, v1, :cond_c

    sget-object v1, LX/6iW;->SPAM:LX/6iW;

    if-ne v2, v1, :cond_8

    .line 2211779
    :cond_c
    iget-object v1, p0, LX/FDP;->j:LX/2Ow;

    invoke-virtual {v1, v0}, LX/2Ow;->c(LX/0Px;)V

    goto :goto_4

    .line 2211780
    :cond_d
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    invoke-virtual {v1, v0}, LX/2Oe;->a(LX/0P1;)Ljava/util/Set;

    move-result-object v1

    .line 2211781
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    .line 2211782
    iget-object v2, p0, LX/FDP;->j:LX/2Ow;

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/2Ow;->a(LX/0Px;)V

    goto :goto_5
.end method

.method public final l(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 5

    .prologue
    .line 2211783
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2211784
    const-string v1, "deleteThreadsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/DeleteThreadsParams;

    .line 2211785
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2211786
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    sget-object v3, LX/6ek;->INBOX:LX/6ek;

    .line 2211787
    iget-object v4, v0, Lcom/facebook/messaging/service/model/DeleteThreadsParams;->a:LX/0Px;

    move-object v4, v4

    .line 2211788
    invoke-virtual {v1, v3, v4}, LX/2Oe;->a(LX/6ek;LX/0Px;)V

    .line 2211789
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    sget-object v3, LX/6ek;->SMS_BUSINESS:LX/6ek;

    .line 2211790
    iget-object v4, v0, Lcom/facebook/messaging/service/model/DeleteThreadsParams;->a:LX/0Px;

    move-object v4, v4

    .line 2211791
    invoke-virtual {v1, v3, v4}, LX/2Oe;->a(LX/6ek;LX/0Px;)V

    .line 2211792
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    sget-object v3, LX/6ek;->SMS_SPAM:LX/6ek;

    .line 2211793
    iget-object v4, v0, Lcom/facebook/messaging/service/model/DeleteThreadsParams;->a:LX/0Px;

    move-object v0, v4

    .line 2211794
    invoke-virtual {v1, v3, v0}, LX/2Oe;->a(LX/6ek;LX/0Px;)V

    .line 2211795
    return-object v2
.end method

.method public final m(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2211796
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 2211797
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/DeleteAllTincanThreadsResult;

    .line 2211798
    iget-object v0, v0, Lcom/facebook/messaging/service/model/DeleteAllTincanThreadsResult;->a:LX/0Rf;

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    .line 2211799
    iget-object v0, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Oe;

    sget-object v3, LX/6ek;->INBOX:LX/6ek;

    invoke-virtual {v0, v3, v2}, LX/2Oe;->a(LX/6ek;LX/0Px;)V

    .line 2211800
    return-object v1
.end method

.method public final n(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 2211801
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2211802
    const-string v1, "deleteMessagesParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/DeleteMessagesParams;

    .line 2211803
    iget-object v0, v0, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->b:LX/0Rf;

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2211804
    iget-object v2, p0, LX/FDP;->i:LX/DdG;

    .line 2211805
    iget-object v3, v2, LX/DdG;->i:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2211806
    goto :goto_0

    .line 2211807
    :cond_0
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2211808
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/DeleteMessagesResult;

    .line 2211809
    iget-object v3, v0, Lcom/facebook/messaging/service/model/DeleteMessagesResult;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2211810
    if-nez v3, :cond_1

    move-object v0, v2

    .line 2211811
    :goto_1
    return-object v0

    .line 2211812
    :cond_1
    iget-object v1, p0, LX/FDP;->b:LX/2OQ;

    invoke-virtual {v1, v3}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v4

    .line 2211813
    if-nez v4, :cond_2

    move-object v0, v2

    .line 2211814
    goto :goto_1

    .line 2211815
    :cond_2
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    iget-object v4, v4, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    invoke-virtual {v1, v4, v0}, LX/2Oe;->a(LX/6ek;Lcom/facebook/messaging/service/model/DeleteMessagesResult;)V

    .line 2211816
    iget-object v1, p0, LX/FDP;->j:LX/2Ow;

    iget-object v4, v0, Lcom/facebook/messaging/service/model/DeleteMessagesResult;->d:LX/0Rf;

    iget-object v5, v0, Lcom/facebook/messaging/service/model/DeleteMessagesResult;->e:LX/0P1;

    invoke-virtual {v5}, LX/0P1;->values()LX/0Py;

    move-result-object v5

    invoke-virtual {v1, v3, v4, v5}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/Collection;Ljava/util/Collection;)V

    .line 2211817
    iget-boolean v0, v0, Lcom/facebook/messaging/service/model/DeleteMessagesResult;->g:Z

    if-nez v0, :cond_3

    .line 2211818
    iget-object v0, p0, LX/FDP;->j:LX/2Ow;

    invoke-virtual {v0, v3}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    :cond_3
    move-object v0, v2

    .line 2211819
    goto :goto_1
.end method

.method public final o(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2211820
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2211821
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2211822
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v1, :cond_0

    .line 2211823
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    invoke-virtual {v1, v0}, LX/2Oe;->b(Lcom/facebook/messaging/service/model/FetchThreadResult;)V

    .line 2211824
    :cond_0
    return-object v2
.end method

.method public final p(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 2211825
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2211826
    if-eqz v2, :cond_0

    .line 2211827
    iget-boolean v0, v2, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2211828
    if-nez v0, :cond_1

    :cond_0
    move-object v0, v2

    .line 2211829
    :goto_0
    return-object v0

    .line 2211830
    :cond_1
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/MarkFolderSeenResult;

    .line 2211831
    if-nez v0, :cond_2

    .line 2211832
    const/4 v0, 0x0

    goto :goto_0

    .line 2211833
    :cond_2
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    iget-object v3, v0, Lcom/facebook/messaging/service/model/MarkFolderSeenResult;->b:LX/6ek;

    iget-wide v4, v0, Lcom/facebook/messaging/service/model/MarkFolderSeenResult;->a:J

    invoke-virtual {v1, v3, v4, v5}, LX/2Oe;->a(LX/6ek;J)V

    move-object v0, v2

    .line 2211834
    goto :goto_0
.end method

.method public final q(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2211835
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2211836
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2211837
    const-string v1, "saveDraftParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SaveDraftParams;

    .line 2211838
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    iget-object v3, v0, Lcom/facebook/messaging/service/model/SaveDraftParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v0, v0, Lcom/facebook/messaging/service/model/SaveDraftParams;->b:Lcom/facebook/messaging/model/messages/MessageDraft;

    .line 2211839
    iget-object p0, v1, LX/2Oe;->a:LX/2OQ;

    invoke-virtual {p0, v3}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object p0

    .line 2211840
    if-eqz p0, :cond_0

    .line 2211841
    iget-object p1, v1, LX/2Oe;->a:LX/2OQ;

    invoke-virtual {p1, p0, v0}, LX/2OQ;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/messages/MessageDraft;)V

    .line 2211842
    :cond_0
    return-object v2
.end method

.method public final r(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 2211843
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2211844
    const-string v1, "pushProperty"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/push/PushProperty;

    .line 2211845
    iget-object v1, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v1

    .line 2211846
    const-string v2, "message"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/model/messages/Message;

    .line 2211847
    iget-object v2, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2211848
    iget-object v1, p0, LX/FDP;->b:LX/2OQ;

    invoke-virtual {v1, v2}, LX/2OQ;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 2211849
    if-eqz v1, :cond_1

    .line 2211850
    :goto_0
    move-object v3, v1

    .line 2211851
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v4

    .line 2211852
    invoke-virtual {v4}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    .line 2211853
    if-eqz v1, :cond_0

    .line 2211854
    iget-object v2, v1, Lcom/facebook/messaging/service/model/NewMessageResult;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v5, v2

    .line 2211855
    iget-object v2, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/2Oe;

    invoke-virtual {v2, v1}, LX/2Oe;->b(Lcom/facebook/messaging/service/model/NewMessageResult;)V

    .line 2211856
    iget-object v1, p0, LX/FDP;->j:LX/2Ow;

    iget-object v2, v5, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1, v2}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2211857
    iget-object v1, p0, LX/FDP;->p:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/CLl;

    .line 2211858
    if-eqz v3, :cond_0

    iget-object v2, v1, LX/CLl;->g:LX/01T;

    sget-object p0, LX/01T;->MESSENGER:LX/01T;

    if-ne v2, p0, :cond_0

    iget-object v2, v5, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    if-eqz v2, :cond_0

    iget-object v2, v5, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/messages/ParticipantInfo;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, LX/CLl;->e:LX/2Mk;

    invoke-virtual {v2, v5}, LX/2Mk;->s(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2211859
    iget-object v2, v0, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    if-nez v2, :cond_2

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1, v5, v2}, LX/CLl;->a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;)V

    .line 2211860
    :cond_0
    return-object v4

    .line 2211861
    :cond_1
    new-instance v1, LX/6iM;

    invoke-direct {v1}, LX/6iM;-><init>()V

    sget-object v3, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    .line 2211862
    iput-object v3, v1, LX/6iM;->b:LX/0rS;

    .line 2211863
    move-object v1, v1

    .line 2211864
    invoke-static {v2}, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-result-object v2

    .line 2211865
    iput-object v2, v1, LX/6iM;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    .line 2211866
    move-object v1, v1

    .line 2211867
    const/16 v2, 0x14

    .line 2211868
    iput v2, v1, LX/6iM;->g:I

    .line 2211869
    move-object v1, v1

    .line 2211870
    invoke-virtual {v1}, LX/6iM;->j()Lcom/facebook/messaging/service/model/FetchThreadParams;

    move-result-object v1

    .line 2211871
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2211872
    const-string v3, "fetchThreadParams"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2211873
    invoke-static {}, LX/1qK;->builder()LX/4BI;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/4BI;->from(LX/1qK;)LX/4BI;

    move-result-object v1

    const-string v3, "fetch_thread"

    .line 2211874
    iput-object v3, v1, LX/4BI;->mType:Ljava/lang/String;

    .line 2211875
    move-object v1, v1

    .line 2211876
    iput-object v2, v1, LX/4BI;->mBundle:Landroid/os/Bundle;

    .line 2211877
    move-object v1, v1

    .line 2211878
    invoke-virtual {v1}, LX/4BI;->build()LX/1qK;

    move-result-object v1

    .line 2211879
    invoke-virtual {p0, v1, p2}, LX/FDP;->c(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 2211880
    invoke-virtual {v1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2211881
    iget-object v1, v1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    goto/16 :goto_0

    .line 2211882
    :cond_2
    iget-object v2, v0, Lcom/facebook/push/PushProperty;->a:LX/3B4;

    invoke-virtual {v2}, LX/3B4;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public final u(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2211883
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2211884
    const-string v1, "fetchPinnedThreadsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;

    .line 2211885
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->b:LX/0rS;

    sget-object v2, LX/0rS;->PREFER_CACHE_IF_UP_TO_DATE:LX/0rS;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/FDP;->b:LX/2OQ;

    invoke-virtual {v1}, LX/2OQ;->f()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->b:LX/0rS;

    sget-object v1, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    if-ne v0, v1, :cond_2

    .line 2211886
    :cond_1
    iget-object v0, p0, LX/FDP;->c:LX/FDI;

    invoke-virtual {v0}, LX/FDI;->a()Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    move-result-object v0

    .line 2211887
    iget-wide v0, v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 2211888
    iget-object v0, p0, LX/FDP;->c:LX/FDI;

    invoke-virtual {v0}, LX/FDI;->a()Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2211889
    :goto_0
    return-object v0

    .line 2211890
    :cond_2
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2211891
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    .line 2211892
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;Z)V

    .line 2211893
    invoke-direct {p0, v0}, LX/FDP;->a(Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;)V

    move-object v0, v2

    .line 2211894
    goto :goto_0
.end method

.method public final v(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2211895
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2211896
    const-string v1, "updatePinnedThreadsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/UpdatePinnedThreadsParams;

    .line 2211897
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    .line 2211898
    iget-object v2, v1, LX/2Oe;->a:LX/2OQ;

    iget-object v3, v0, Lcom/facebook/messaging/service/model/UpdatePinnedThreadsParams;->a:LX/0Px;

    invoke-virtual {v2, v3}, LX/2OQ;->c(Ljava/util/List;)V

    .line 2211899
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2211900
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    .line 2211901
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;Z)V

    .line 2211902
    iget-object v0, p0, LX/FDP;->j:LX/2Ow;

    invoke-virtual {v0}, LX/2Ow;->d()V

    .line 2211903
    return-object v2
.end method

.method public final w(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2211904
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2211905
    const-string v1, "addPinnedThreadParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/AddPinnedThreadParams;

    .line 2211906
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    .line 2211907
    iget-object v2, v0, Lcom/facebook/messaging/service/model/AddPinnedThreadParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v2, :cond_0

    .line 2211908
    iget-object v2, v1, LX/2Oe;->a:LX/2OQ;

    iget-object v3, v0, Lcom/facebook/messaging/service/model/AddPinnedThreadParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2, v3}, LX/2OQ;->f(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2211909
    :cond_0
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2211910
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    .line 2211911
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;Z)V

    .line 2211912
    iget-object v0, p0, LX/FDP;->j:LX/2Ow;

    invoke-virtual {v0}, LX/2Ow;->d()V

    .line 2211913
    return-object v2
.end method

.method public final x(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2211914
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2211915
    const-string v1, "unpinThreadParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/UnpinThreadParams;

    .line 2211916
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    .line 2211917
    iget-object v2, v1, LX/2Oe;->a:LX/2OQ;

    iget-object v3, v0, Lcom/facebook/messaging/service/model/UnpinThreadParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2, v3}, LX/2OQ;->g(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2211918
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2211919
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    .line 2211920
    iget-object v1, p0, LX/FDP;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2Oe;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, LX/2Oe;->a(Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;Z)V

    .line 2211921
    invoke-direct {p0, v0}, LX/FDP;->a(Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;)V

    .line 2211922
    return-object v2
.end method

.method public final z(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2211923
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2211924
    const-string v1, "updatedMessageSendErrorParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/UpdateMessageSendErrorParams;

    .line 2211925
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 2211926
    iget-object v2, p0, LX/FDP;->b:LX/2OQ;

    invoke-virtual {v2, v0}, LX/2OQ;->a(Lcom/facebook/messaging/service/model/UpdateMessageSendErrorParams;)V

    .line 2211927
    iget-object v2, p0, LX/FDP;->j:LX/2Ow;

    iget-object v0, v0, Lcom/facebook/messaging/service/model/UpdateMessageSendErrorParams;->c:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2, v0}, LX/2Ow;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2211928
    return-object v1
.end method
