.class public LX/G22;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/1Vm;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/1Vm;LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Vm;",
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2313345
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2313346
    iput-object p1, p0, LX/G22;->a:LX/1Vm;

    .line 2313347
    iput-object p2, p0, LX/G22;->b:LX/0Or;

    .line 2313348
    return-void
.end method

.method public static a(LX/0QB;)LX/G22;
    .locals 5

    .prologue
    .line 2313349
    const-class v1, LX/G22;

    monitor-enter v1

    .line 2313350
    :try_start_0
    sget-object v0, LX/G22;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2313351
    sput-object v2, LX/G22;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2313352
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2313353
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2313354
    new-instance v4, LX/G22;

    invoke-static {v0}, LX/1Vm;->a(LX/0QB;)LX/1Vm;

    move-result-object v3

    check-cast v3, LX/1Vm;

    const/16 p0, 0x2eb

    invoke-static {v0, p0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v4, v3, p0}, LX/G22;-><init>(LX/1Vm;LX/0Or;)V

    .line 2313355
    move-object v0, v4

    .line 2313356
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2313357
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/G22;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2313358
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2313359
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/timeline/protiles/model/ProtileModel;)Z
    .locals 1

    .prologue
    .line 2313360
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/model/ProtileModel;->p()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/model/ProtileModel;->p()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/model/ProtileModel;->p()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;->a()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel$DescriptionModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/model/ProtileModel;->p()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;->a()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel$DescriptionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel$DescriptionModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/model/ProtileModel;->p()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel$IconModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/model/ProtileModel;->p()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel$IconModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel$IconModel;->a()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/model/ProtileModel;->p()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
