.class public LX/FQl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation


# instance fields
.field private final a:LX/FQj;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/FQk;",
            ">;>;"
        }
    .end annotation
.end field

.field private final c:LX/0tX;

.field private final d:LX/0Zb;


# direct methods
.method public constructor <init>(LX/FQj;LX/0tX;LX/0Zb;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2239656
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2239657
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, LX/FQl;->b:Ljava/util/Map;

    .line 2239658
    iput-object p1, p0, LX/FQl;->a:LX/FQj;

    .line 2239659
    iput-object p2, p0, LX/FQl;->c:LX/0tX;

    .line 2239660
    iput-object p3, p0, LX/FQl;->d:LX/0Zb;

    .line 2239661
    return-void
.end method
