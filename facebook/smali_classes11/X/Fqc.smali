.class public LX/Fqc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FqT;


# instance fields
.field public A:LX/4BY;

.field private final B:Z

.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G4X;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G3q;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G3G;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fqs;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/Fqp;

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fxv;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FyG;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lcom/facebook/content/SecureContextHelper;

.field private final o:LX/Fsr;

.field private final p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ezi;",
            ">;"
        }
    .end annotation
.end field

.field private final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public final r:Landroid/content/Context;

.field private final s:Ljava/lang/String;

.field public final t:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/BPp;",
            ">;"
        }
    .end annotation
.end field

.field public final u:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2do;",
            ">;"
        }
    .end annotation
.end field

.field public final v:LX/5SB;

.field private final w:LX/BQ1;

.field public final x:LX/Fy4;

.field private final y:Lcom/facebook/timeline/services/ProfileActionClient;

.field private final z:LX/BQ9;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/BQ9;LX/5SB;LX/Fy4;LX/BQ1;Ljava/lang/String;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Or;LX/0Ot;LX/0Or;LX/Fqp;LX/0Or;Lcom/facebook/content/SecureContextHelper;LX/0Ot;LX/0Ot;LX/0Ot;LX/Fsr;Lcom/facebook/timeline/services/ProfileActionClient;LX/0Ot;Ljava/lang/Boolean;LX/0Ot;)V
    .locals 2
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/BQ9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/5SB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/Fy4;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p10    # LX/0Or;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p27    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/BQ9;",
            "LX/5SB;",
            "LX/Fy4;",
            "LX/BQ1;",
            "Ljava/lang/String;",
            "LX/0Or",
            "<",
            "LX/2do;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BPp;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0gh;",
            ">;",
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/0Or",
            "<",
            "LX/G4X;",
            ">;",
            "LX/0Or",
            "<",
            "LX/G3q;",
            ">;",
            "LX/0Or",
            "<",
            "LX/G3G;",
            ">;",
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/2LS;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/Fqp;",
            "LX/0Or",
            "<",
            "LX/Fqs;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Ot",
            "<",
            "LX/Fxv;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FyG;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/Fsr;",
            "Lcom/facebook/timeline/services/ProfileActionClient;",
            "LX/0Ot",
            "<",
            "LX/Ezi;",
            ">;",
            "Ljava/lang/Boolean;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2294119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2294120
    iput-object p1, p0, LX/Fqc;->r:Landroid/content/Context;

    .line 2294121
    move-object/from16 v0, p20

    iput-object v0, p0, LX/Fqc;->n:Lcom/facebook/content/SecureContextHelper;

    .line 2294122
    iput-object p2, p0, LX/Fqc;->z:LX/BQ9;

    .line 2294123
    iput-object p3, p0, LX/Fqc;->v:LX/5SB;

    .line 2294124
    iput-object p4, p0, LX/Fqc;->x:LX/Fy4;

    .line 2294125
    iput-object p5, p0, LX/Fqc;->w:LX/BQ1;

    .line 2294126
    iput-object p6, p0, LX/Fqc;->s:Ljava/lang/String;

    .line 2294127
    iput-object p7, p0, LX/Fqc;->u:LX/0Or;

    .line 2294128
    iput-object p8, p0, LX/Fqc;->t:LX/0Or;

    .line 2294129
    iput-object p9, p0, LX/Fqc;->a:LX/0Or;

    .line 2294130
    iput-object p10, p0, LX/Fqc;->b:LX/0Or;

    .line 2294131
    iput-object p11, p0, LX/Fqc;->c:LX/0Or;

    .line 2294132
    iput-object p12, p0, LX/Fqc;->e:LX/0Or;

    .line 2294133
    iput-object p13, p0, LX/Fqc;->f:LX/0Or;

    .line 2294134
    move-object/from16 v0, p14

    iput-object v0, p0, LX/Fqc;->g:LX/0Or;

    .line 2294135
    move-object/from16 v0, p15

    iput-object v0, p0, LX/Fqc;->d:LX/0Or;

    .line 2294136
    move-object/from16 v0, p17

    iput-object v0, p0, LX/Fqc;->h:LX/0Or;

    .line 2294137
    move-object/from16 v0, p18

    iput-object v0, p0, LX/Fqc;->j:LX/Fqp;

    .line 2294138
    move-object/from16 v0, p19

    iput-object v0, p0, LX/Fqc;->i:LX/0Or;

    .line 2294139
    move-object/from16 v0, p21

    iput-object v0, p0, LX/Fqc;->k:LX/0Ot;

    .line 2294140
    move-object/from16 v0, p22

    iput-object v0, p0, LX/Fqc;->l:LX/0Ot;

    .line 2294141
    move-object/from16 v0, p23

    iput-object v0, p0, LX/Fqc;->m:LX/0Ot;

    .line 2294142
    move-object/from16 v0, p24

    iput-object v0, p0, LX/Fqc;->o:LX/Fsr;

    .line 2294143
    move-object/from16 v0, p25

    iput-object v0, p0, LX/Fqc;->y:Lcom/facebook/timeline/services/ProfileActionClient;

    .line 2294144
    move-object/from16 v0, p26

    iput-object v0, p0, LX/Fqc;->p:LX/0Ot;

    .line 2294145
    invoke-virtual/range {p27 .. p27}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iput-boolean v1, p0, LX/Fqc;->B:Z

    .line 2294146
    move-object/from16 v0, p28

    iput-object v0, p0, LX/Fqc;->q:LX/0Ot;

    .line 2294147
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2294148
    iget-object v0, p0, LX/Fqc;->w:LX/BQ1;

    if-nez v0, :cond_0

    .line 2294149
    :goto_0
    return-void

    .line 2294150
    :cond_0
    invoke-direct {p0}, LX/Fqc;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2294151
    new-instance v0, LX/Fqq;

    iget-object v1, p0, LX/Fqc;->r:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/Fqq;-><init>(Landroid/content/Context;)V

    .line 2294152
    iget-object v1, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->z()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Fqq;->setSeeFirstUserName(Ljava/lang/String;)V

    .line 2294153
    iget-object v1, v0, LX/Fqq;->a:LX/Fql;

    invoke-virtual {v1, p1}, LX/0ht;->a(Landroid/view/View;)V

    .line 2294154
    goto :goto_0

    .line 2294155
    :cond_1
    iget-object v0, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v0, v3, :cond_2

    move v0, v1

    .line 2294156
    :goto_1
    iget-object v3, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v3}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v3, v4, :cond_3

    move v3, v1

    .line 2294157
    :goto_2
    iget-object v4, p0, LX/Fqc;->j:LX/Fqp;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0}, LX/Fqc;->e()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    if-nez v3, :cond_4

    :goto_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v4, p1, v0, v5, v1}, LX/Fqp;->a(Landroid/view/View;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/Boolean;)LX/Fqo;

    move-result-object v1

    .line 2294158
    new-instance v2, LX/FqV;

    invoke-direct {v2, p0, v1}, LX/FqV;-><init>(LX/Fqc;LX/Fqo;)V

    .line 2294159
    iget-object v0, p0, LX/Fqc;->u:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2do;

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2294160
    new-instance v0, LX/FqW;

    invoke-direct {v0, p0, v2}, LX/FqW;-><init>(LX/Fqc;LX/84a;)V

    .line 2294161
    iget-object v2, v1, LX/Fqo;->d:LX/0ht;

    .line 2294162
    iput-object v0, v2, LX/0ht;->H:LX/2dD;

    .line 2294163
    new-instance v0, LX/FqX;

    invoke-direct {v0, p0}, LX/FqX;-><init>(LX/Fqc;)V

    .line 2294164
    iput-object v0, v1, LX/Fqo;->j:LX/EpI;

    .line 2294165
    invoke-virtual {v1}, LX/Fqo;->a()V

    .line 2294166
    invoke-virtual {v1}, LX/Fqo;->e()V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 2294167
    goto :goto_1

    :cond_3
    move v3, v2

    .line 2294168
    goto :goto_2

    :cond_4
    move v1, v2

    .line 2294169
    goto :goto_3
.end method

.method private static a(LX/84Z;)Z
    .locals 2

    .prologue
    .line 2294170
    iget-boolean v0, p0, LX/84Z;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/84Z;->b:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CAN_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v0, v1, :cond_0

    .line 2294171
    const/4 v0, 0x1

    .line 2294172
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static synthetic a(LX/Fqc;LX/84Z;)Z
    .locals 1

    .prologue
    .line 2294173
    invoke-static {p1}, LX/Fqc;->a(LX/84Z;)Z

    move-result v0

    return v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 2294174
    iget-object v0, p0, LX/Fqc;->A:LX/4BY;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fqc;->A:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2294175
    iget-object v0, p0, LX/Fqc;->A:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2294176
    :cond_0
    return-void
.end method

.method private d()Z
    .locals 2

    .prologue
    .line 2294177
    iget-object v0, p0, LX/Fqc;->w:LX/BQ1;

    .line 2294178
    iget-object v1, v0, LX/BQ1;->d:LX/5wQ;

    move-object v0, v1

    .line 2294179
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fqc;->w:LX/BQ1;

    .line 2294180
    iget-object v1, v0, LX/BQ1;->d:LX/5wQ;

    move-object v0, v1

    .line 2294181
    invoke-interface {v0}, LX/5wQ;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Z
    .locals 2

    .prologue
    .line 2294182
    iget-object v0, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->IS_SUBSCRIBED:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v0}, LX/BQ1;->F()Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->SEE_FIRST:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 2294264
    iget-object v0, p0, LX/Fqc;->w:LX/BQ1;

    if-nez v0, :cond_0

    .line 2294265
    :goto_0
    return-void

    .line 2294266
    :cond_0
    iget-object v0, p0, LX/Fqc;->v:LX/5SB;

    iget-object v1, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    iget-object v3, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v3}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/ipc/profile/TimelineFriendParams;->a(LX/5SB;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)Lcom/facebook/ipc/profile/TimelineFriendParams;

    move-result-object v1

    .line 2294267
    iget-object v0, p0, LX/Fqc;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fxv;

    .line 2294268
    iget-object v2, v0, LX/Fxv;->e:Landroid/app/Activity;

    if-eqz v2, :cond_1

    iget-object v2, v0, LX/Fxv;->e:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    if-nez v2, :cond_2

    .line 2294269
    :cond_1
    :goto_1
    goto :goto_0

    .line 2294270
    :cond_2
    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2294271
    iput-object v1, v0, LX/Fxv;->h:Lcom/facebook/ipc/profile/TimelineFriendParams;

    .line 2294272
    new-instance v2, LX/6WS;

    iget-object v3, v0, LX/Fxv;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/6WS;-><init>(Landroid/content/Context;)V

    iput-object v2, v0, LX/Fxv;->i:LX/5OM;

    .line 2294273
    iget-object v2, v0, LX/Fxv;->i:LX/5OM;

    invoke-virtual {v2}, LX/5OM;->c()LX/5OG;

    move-result-object v2

    .line 2294274
    const v3, 0x7f08157c

    invoke-virtual {v2, v3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v3

    iput-object v3, v0, LX/Fxv;->j:Landroid/view/MenuItem;

    .line 2294275
    invoke-static {v0}, LX/Fxv;->e$redex0(LX/Fxv;)V

    .line 2294276
    iget-object v3, v0, LX/Fxv;->j:Landroid/view/MenuItem;

    new-instance p0, LX/Fxp;

    invoke-direct {p0, v0}, LX/Fxp;-><init>(LX/Fxv;)V

    invoke-interface {v3, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2294277
    iget-object v3, v0, LX/Fxv;->j:Landroid/view/MenuItem;

    iget-object p0, v0, LX/Fxv;->h:Lcom/facebook/ipc/profile/TimelineFriendParams;

    iget-object p0, p0, Lcom/facebook/ipc/profile/TimelineFriendParams;->d:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2294278
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->CANNOT_SUBSCRIBE:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    if-eq p0, v1, :cond_4

    const/4 v1, 0x1

    :goto_2
    move p0, v1

    .line 2294279
    invoke-interface {v3, p0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2294280
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iget-object p0, v0, LX/Fxv;->h:Lcom/facebook/ipc/profile/TimelineFriendParams;

    iget-object p0, p0, Lcom/facebook/ipc/profile/TimelineFriendParams;->e:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v3, p0}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2294281
    const v3, 0x7f080fa0

    invoke-virtual {v2, v3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v3

    .line 2294282
    new-instance p0, LX/Fxq;

    invoke-direct {p0, v0}, LX/Fxq;-><init>(LX/Fxv;)V

    invoke-interface {v3, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2294283
    :goto_3
    const v3, 0x7f080fa3

    invoke-virtual {v2, v3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v2

    .line 2294284
    new-instance v3, LX/Fxs;

    invoke-direct {v3, v0}, LX/Fxs;-><init>(LX/Fxv;)V

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2294285
    new-instance v2, LX/1B1;

    invoke-direct {v2}, LX/1B1;-><init>()V

    iput-object v2, v0, LX/Fxv;->k:LX/1B1;

    .line 2294286
    iget-object v2, v0, LX/Fxv;->k:LX/1B1;

    new-instance v3, LX/Fxt;

    invoke-direct {v3, v0}, LX/Fxt;-><init>(LX/Fxv;)V

    invoke-virtual {v2, v3}, LX/1B1;->a(LX/0b2;)Z

    .line 2294287
    iget-object v2, v0, LX/Fxv;->k:LX/1B1;

    iget-object v3, v0, LX/Fxv;->c:LX/BPp;

    invoke-virtual {v2, v3}, LX/1B1;->a(LX/0b4;)V

    .line 2294288
    iget-object v2, v0, LX/Fxv;->i:LX/5OM;

    iget-object v3, v0, LX/Fxv;->f:LX/2dD;

    .line 2294289
    iput-object v3, v2, LX/0ht;->H:LX/2dD;

    .line 2294290
    iget-object v2, v0, LX/Fxv;->i:LX/5OM;

    iget-object v3, v0, LX/Fxv;->g:LX/2yQ;

    .line 2294291
    iput-object v3, v2, LX/0ht;->I:LX/2yQ;

    .line 2294292
    iget-object v2, v0, LX/Fxv;->i:LX/5OM;

    iget-object v3, v0, LX/Fxv;->e:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0ht;->a(Landroid/view/View;)V

    goto/16 :goto_1

    .line 2294293
    :cond_3
    const v3, 0x7f081591

    invoke-virtual {v2, v3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v3

    .line 2294294
    new-instance p0, LX/Fxr;

    invoke-direct {p0, v0}, LX/Fxr;-><init>(LX/Fxv;)V

    invoke-interface {v3, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_3

    :cond_4
    const/4 v1, 0x0

    goto :goto_2
.end method

.method private g()V
    .locals 4

    .prologue
    .line 2294183
    iget-object v0, p0, LX/Fqc;->v:LX/5SB;

    if-nez v0, :cond_0

    .line 2294184
    :goto_0
    return-void

    .line 2294185
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, LX/Fqc;->r:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08153b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/Fqc;->r:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081592

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 2294186
    new-instance v1, LX/0ju;

    iget-object v2, p0, LX/Fqc;->r:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/0ju;-><init>(Landroid/content/Context;)V

    new-instance v2, LX/FqY;

    invoke-direct {v2, p0}, LX/FqY;-><init>(LX/Fqc;)V

    invoke-virtual {v1, v0, v2}, LX/0ju;->a([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    goto :goto_0
.end method

.method private h()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2294187
    iget-object v0, p0, LX/Fqc;->r:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080fa9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/Object;

    iget-object v2, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2294188
    iget-object v0, p0, LX/Fqc;->r:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0314c8

    invoke-virtual {v0, v2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 2294189
    const v0, 0x7f0d2f21

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2294190
    iget-object v3, p0, LX/Fqc;->r:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0815a9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2294191
    const v0, 0x7f0d2f22

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 2294192
    iget-object v3, p0, LX/Fqc;->r:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0815af

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    iget-object v5, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, LX/1fg;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2294193
    new-instance v0, LX/4BY;

    iget-object v3, p0, LX/Fqc;->r:Landroid/content/Context;

    invoke-direct {v0, v3}, LX/4BY;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/Fqc;->A:LX/4BY;

    .line 2294194
    iget-object v0, p0, LX/Fqc;->A:LX/4BY;

    .line 2294195
    iput v6, v0, LX/4BY;->d:I

    .line 2294196
    iget-object v0, p0, LX/Fqc;->A:LX/4BY;

    iget-object v3, p0, LX/Fqc;->r:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0815b0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 2294197
    new-instance v0, LX/0ju;

    iget-object v3, p0, LX/Fqc;->r:Landroid/content/Context;

    invoke-direct {v0, v3}, LX/0ju;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v7}, LX/0ju;->b(Z)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v0

    const v1, 0x7f08154b

    new-instance v2, LX/FqZ;

    invoke-direct {v2, p0}, LX/FqZ;-><init>(LX/Fqc;)V

    invoke-virtual {v0, v1, v2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    const v1, 0x7f0815b3

    invoke-virtual {v0, v1, v8}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->b()LX/2EJ;

    .line 2294198
    return-void
.end method

.method private o(Ljava/lang/String;)V
    .locals 13

    .prologue
    .line 2294199
    iget-object v0, p0, LX/Fqc;->z:LX/BQ9;

    iget-object v1, p0, LX/Fqc;->v:LX/5SB;

    .line 2294200
    iget-wide v6, v1, LX/5SB;->b:J

    move-wide v2, v6

    .line 2294201
    iget-object v1, p0, LX/Fqc;->v:LX/5SB;

    invoke-virtual {v1}, LX/5SB;->i()Z

    move-result v1

    iget-object v4, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v4}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    iget-object v5, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v5

    invoke-static {v1, v4, v5}, LX/9lQ;->getRelationshipType(ZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/9lQ;

    move-result-object v1

    .line 2294202
    const-string v7, "profile_action_bar_item_click"

    const/4 v10, 0x0

    move-object v6, v0

    move-wide v8, v2

    move-object v11, v1

    invoke-static/range {v6 .. v11}, LX/BQ9;->a(LX/BQ9;Ljava/lang/String;JLjava/lang/String;LX/9lQ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 2294203
    const-string v7, "action_bar_target_type"

    invoke-virtual {v6, v7, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2294204
    iget-object v7, v0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v7, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2294205
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2294206
    iget-object v0, p0, LX/Fqc;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G3G;

    const-string v1, "profile_refresher"

    const/4 v3, 0x0

    .line 2294207
    const-string v2, "profile_refresher_entry_point_tapped"

    invoke-static {v0, v1, v3, v3, v2}, LX/G3G;->a(LX/G3G;Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;Ljava/lang/String;)V

    .line 2294208
    iget-object v0, p0, LX/Fqc;->r:Landroid/content/Context;

    const-class v1, Landroid/app/Activity;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 2294209
    if-eqz v0, :cond_1

    .line 2294210
    new-instance v1, LX/G3o;

    invoke-direct {v1}, LX/G3o;-><init>()V

    iget-object v2, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object v2

    .line 2294211
    iput-object v2, v1, LX/G3o;->a:Ljava/lang/String;

    .line 2294212
    move-object v1, v1

    .line 2294213
    iget-object v2, p0, LX/Fqc;->v:LX/5SB;

    .line 2294214
    iget-wide v4, v2, LX/5SB;->b:J

    move-wide v2, v4

    .line 2294215
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    .line 2294216
    iput-object v2, v1, LX/G3o;->b:Ljava/lang/String;

    .line 2294217
    move-object v1, v1

    .line 2294218
    iget-object v2, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->N()Ljava/lang/String;

    move-result-object v2

    .line 2294219
    iput-object v2, v1, LX/G3o;->c:Ljava/lang/String;

    .line 2294220
    move-object v1, v1

    .line 2294221
    iget-object v2, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->V()Ljava/lang/String;

    move-result-object v2

    .line 2294222
    iput-object v2, v1, LX/G3o;->d:Ljava/lang/String;

    .line 2294223
    move-object v1, v1

    .line 2294224
    const/4 v2, 0x1

    .line 2294225
    iput-boolean v2, v1, LX/G3o;->i:Z

    .line 2294226
    move-object v2, v1

    .line 2294227
    iget-object v1, p0, LX/Fqc;->w:LX/BQ1;

    .line 2294228
    iget-object v3, v1, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v1, v3

    .line 2294229
    if-eqz v1, :cond_0

    .line 2294230
    iget-object v1, p0, LX/Fqc;->w:LX/BQ1;

    .line 2294231
    iget-object v3, v1, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v1, v3

    .line 2294232
    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->e()Z

    move-result v1

    .line 2294233
    iput-boolean v1, v2, LX/G3o;->e:Z

    .line 2294234
    move-object v1, v2

    .line 2294235
    iget-object v3, p0, LX/Fqc;->w:LX/BQ1;

    .line 2294236
    iget-object v4, v3, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v3, v4

    .line 2294237
    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->m()Z

    move-result v3

    .line 2294238
    iput-boolean v3, v1, LX/G3o;->f:Z

    .line 2294239
    move-object v1, v1

    .line 2294240
    iget-object v3, p0, LX/Fqc;->w:LX/BQ1;

    .line 2294241
    iget-object v4, v3, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v3, v4

    .line 2294242
    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->ci_()Z

    move-result v3

    .line 2294243
    iput-boolean v3, v1, LX/G3o;->e:Z

    .line 2294244
    move-object v1, v1

    .line 2294245
    iget-object v3, p0, LX/Fqc;->w:LX/BQ1;

    .line 2294246
    iget-object v4, v3, LX/BQ1;->b:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;

    move-object v3, v4

    .line 2294247
    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderProfileIntroCardFieldsModel;->n()Z

    move-result v3

    .line 2294248
    iput-boolean v3, v1, LX/G3o;->f:Z

    .line 2294249
    :cond_0
    iget-object v1, p0, LX/Fqc;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/G3q;

    invoke-virtual {v2}, LX/G3o;->a()Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, LX/G3q;->a(Lcom/facebook/timeline/refresher/launcher/ProfileRefresherConfiguration;Landroid/app/Activity;)V

    .line 2294250
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2294251
    invoke-direct {p0, p1}, LX/Fqc;->o(Ljava/lang/String;)V

    .line 2294252
    sget-object v0, LX/Fqa;->a:[I

    iget-object v1, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2294253
    :goto_0
    return-void

    .line 2294254
    :pswitch_0
    invoke-direct {p0}, LX/Fqc;->f()V

    goto :goto_0

    .line 2294255
    :pswitch_1
    iget-object v0, p0, LX/Fqc;->t:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BPp;

    new-instance v1, LX/BPc;

    iget-object v2, p0, LX/Fqc;->v:LX/5SB;

    .line 2294256
    iget-wide v4, v2, LX/5SB;->b:J

    move-wide v2, v4

    .line 2294257
    invoke-direct {v1, v2, v3}, LX/BPc;-><init>(J)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2294258
    iget-object v0, p0, LX/Fqc;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ezi;

    iget-object v1, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->N()Ljava/lang/String;

    iget-object v1, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->P()Ljava/lang/String;

    invoke-virtual {v0}, LX/Ezi;->a()V

    goto :goto_0

    .line 2294259
    :pswitch_2
    invoke-direct {p0}, LX/Fqc;->f()V

    goto :goto_0

    .line 2294260
    :pswitch_3
    invoke-direct {p0}, LX/Fqc;->g()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2294261
    invoke-direct {p0, p1}, LX/Fqc;->o(Ljava/lang/String;)V

    .line 2294262
    invoke-direct {p0, p2}, LX/Fqc;->a(Landroid/view/View;)V

    .line 2294263
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 2294094
    invoke-direct {p0, p1}, LX/Fqc;->o(Ljava/lang/String;)V

    .line 2294095
    iget-object v0, p0, LX/Fqc;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    invoke-virtual {v0, p2}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2294096
    sget-object v0, LX/0ax;->ai:Ljava/lang/String;

    iget-object v1, p0, LX/Fqc;->v:LX/5SB;

    .line 2294097
    iget-wide v5, v1, LX/5SB;->b:J

    move-wide v2, v5

    .line 2294098
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2294099
    iget-object v0, p0, LX/Fqc;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v2, p0, LX/Fqc;->r:Landroid/content/Context;

    invoke-interface {v0, v2, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2294100
    if-eqz v0, :cond_1

    .line 2294101
    iget-object v1, p0, LX/Fqc;->s:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2294102
    const-string v1, "group_commerce_sell_options_id"

    iget-object v2, p0, LX/Fqc;->s:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2294103
    :cond_0
    const-string v1, "is_from_fb4a"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2294104
    const-string v1, "trigger"

    const-string v2, "timeline_message_button"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2294105
    iget-object v1, p0, LX/Fqc;->n:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/Fqc;->r:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2294106
    :goto_0
    return-void

    .line 2294107
    :cond_1
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2294108
    const-string v0, "is_from_fb4a"

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2294109
    const-string v0, "trigger"

    const-string v3, "timeline_message_button"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2294110
    iget-object v0, p0, LX/Fqc;->s:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2294111
    const-string v0, "group_commerce_sell_options_id"

    iget-object v3, p0, LX/Fqc;->s:Ljava/lang/String;

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2294112
    :cond_2
    iget-object v0, p0, LX/Fqc;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v3, p0, LX/Fqc;->r:Landroid/content/Context;

    invoke-virtual {v0, v3, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Landroid/view/View;)V
    .locals 6

    .prologue
    .line 2294113
    invoke-direct {p0, p1}, LX/Fqc;->o(Ljava/lang/String;)V

    .line 2294114
    iget-object v0, p0, LX/Fqc;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    invoke-virtual {v0, p2}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2294115
    iget-object v0, p0, LX/Fqc;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FyG;

    iget-object v1, p0, LX/Fqc;->v:LX/5SB;

    .line 2294116
    iget-wide v4, v1, LX/5SB;->b:J

    move-wide v2, v4

    .line 2294117
    invoke-virtual {v0, v2, v3, p3}, LX/FyG;->a(JLandroid/view/View;)V

    .line 2294118
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2294014
    invoke-direct {p0}, LX/Fqc;->c()V

    .line 2294015
    iget-object v0, p0, LX/Fqc;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ezi;

    .line 2294016
    iget-object v1, v0, LX/Ezi;->d:LX/Ezl;

    const/4 p0, 0x1

    invoke-virtual {v1, p0}, LX/Ezl;->a(Z)V

    .line 2294017
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2294018
    invoke-direct {p0, p1}, LX/Fqc;->o(Ljava/lang/String;)V

    .line 2294019
    iget-boolean v0, p0, LX/Fqc;->B:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/0ax;->bG:Ljava/lang/String;

    :goto_0
    iget-object v1, p0, LX/Fqc;->v:LX/5SB;

    .line 2294020
    iget-wide v4, v1, LX/5SB;->b:J

    move-wide v2, v4

    .line 2294021
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2294022
    iget-object v0, p0, LX/Fqc;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v2, p0, LX/Fqc;->r:Landroid/content/Context;

    invoke-virtual {v0, v2, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2294023
    return-void

    .line 2294024
    :cond_0
    sget-object v0, LX/0ax;->bF:Ljava/lang/String;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2294025
    invoke-direct {p0, p1}, LX/Fqc;->o(Ljava/lang/String;)V

    .line 2294026
    iget-object v0, p0, LX/Fqc;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    invoke-virtual {v0, p2}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2294027
    sget-object v0, LX/0ax;->bL:Ljava/lang/String;

    iget-object v1, p0, LX/Fqc;->v:LX/5SB;

    .line 2294028
    iget-wide v4, v1, LX/5SB;->b:J

    move-wide v2, v4

    .line 2294029
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2294030
    iget-object v0, p0, LX/Fqc;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v2, p0, LX/Fqc;->r:Landroid/content/Context;

    invoke-virtual {v0, v2, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2294031
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2294032
    invoke-direct {p0, p1}, LX/Fqc;->o(Ljava/lang/String;)V

    .line 2294033
    iget-object v0, p0, LX/Fqc;->o:LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->ll_()V

    .line 2294034
    iget-object v0, p0, LX/Fqc;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v1, p0, LX/Fqc;->r:Landroid/content/Context;

    sget-object v2, LX/0ax;->di:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2294035
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 2294036
    invoke-direct {p0, p1}, LX/Fqc;->o(Ljava/lang/String;)V

    .line 2294037
    iget-object v0, p0, LX/Fqc;->y:Lcom/facebook/timeline/services/ProfileActionClient;

    iget-object v1, p0, LX/Fqc;->v:LX/5SB;

    .line 2294038
    iget-wide v6, v1, LX/5SB;->a:J

    move-wide v2, v6

    .line 2294039
    iget-object v1, p0, LX/Fqc;->v:LX/5SB;

    .line 2294040
    iget-wide v6, v1, LX/5SB;->b:J

    move-wide v4, v6

    .line 2294041
    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/facebook/timeline/services/ProfileActionClient;->a(JJ)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2294042
    iget-object v0, p0, LX/Fqc;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G4X;

    iget-object v2, p0, LX/Fqc;->r:Landroid/content/Context;

    iget-object v3, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v3}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/G4X;->a(Landroid/content/Context;Ljava/lang/String;)LX/0TF;

    move-result-object v2

    iget-object v0, p0, LX/Fqc;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2294043
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 2294044
    invoke-direct {p0, p1}, LX/Fqc;->o(Ljava/lang/String;)V

    .line 2294045
    iget-object v0, p0, LX/Fqc;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v1, p0, LX/Fqc;->r:Landroid/content/Context;

    sget-object v2, LX/0ax;->ep:Ljava/lang/String;

    iget-object v3, p0, LX/Fqc;->v:LX/5SB;

    .line 2294046
    iget-wide v6, v3, LX/5SB;->a:J

    move-wide v4, v6

    .line 2294047
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, p0, LX/Fqc;->v:LX/5SB;

    .line 2294048
    iget-wide v6, v4, LX/5SB;->b:J

    move-wide v4, v6

    .line 2294049
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2294050
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 2294051
    invoke-direct {p0, p1}, LX/Fqc;->o(Ljava/lang/String;)V

    .line 2294052
    iget-object v0, p0, LX/Fqc;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v1, p0, LX/Fqc;->r:Landroid/content/Context;

    sget-object v2, LX/0ax;->eq:Ljava/lang/String;

    iget-object v3, p0, LX/Fqc;->v:LX/5SB;

    .line 2294053
    iget-wide v6, v3, LX/5SB;->b:J

    move-wide v4, v6

    .line 2294054
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v4}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2294055
    return-void
.end method

.method public final g(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 2294056
    invoke-direct {p0, p1}, LX/Fqc;->o(Ljava/lang/String;)V

    .line 2294057
    iget-object v0, p0, LX/Fqc;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v1, p0, LX/Fqc;->r:Landroid/content/Context;

    sget-object v2, LX/0ax;->I:Ljava/lang/String;

    iget-object v3, p0, LX/Fqc;->v:LX/5SB;

    .line 2294058
    iget-wide v6, v3, LX/5SB;->b:J

    move-wide v4, v6

    .line 2294059
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v4, p0, LX/Fqc;->w:LX/BQ1;

    invoke-virtual {v4}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2294060
    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2294061
    invoke-direct {p0, p1}, LX/Fqc;->o(Ljava/lang/String;)V

    .line 2294062
    invoke-direct {p0}, LX/Fqc;->h()V

    .line 2294063
    return-void
.end method

.method public final i(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 2294064
    invoke-direct {p0, p1}, LX/Fqc;->o(Ljava/lang/String;)V

    .line 2294065
    iget-object v0, p0, LX/Fqc;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v1, p0, LX/Fqc;->r:Landroid/content/Context;

    sget-object v2, LX/0ax;->dv:Ljava/lang/String;

    iget-object v3, p0, LX/Fqc;->v:LX/5SB;

    .line 2294066
    iget-wide v6, v3, LX/5SB;->b:J

    move-wide v4, v6

    .line 2294067
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DONT_LIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v5, LX/0wD;->TIMELINE_SOMEONE_ELSE:LX/0wD;

    invoke-virtual {v5}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v3, v4, v5}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2294068
    return-void
.end method

.method public final j(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2294069
    invoke-direct {p0, p1}, LX/Fqc;->o(Ljava/lang/String;)V

    .line 2294070
    iget-object v0, p0, LX/Fqc;->o:LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->p()LX/FwE;

    move-result-object v0

    invoke-interface {v0}, LX/FwE;->b()V

    .line 2294071
    return-void
.end method

.method public final k(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2294072
    invoke-direct {p0, p1}, LX/Fqc;->o(Ljava/lang/String;)V

    .line 2294073
    iget-object v0, p0, LX/Fqc;->o:LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->u()LX/Fw9;

    move-result-object v0

    invoke-interface {v0}, LX/Fw9;->a()V

    .line 2294074
    return-void
.end method

.method public final l(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2294075
    invoke-direct {p0, p1}, LX/Fqc;->o(Ljava/lang/String;)V

    .line 2294076
    iget-object v0, p0, LX/Fqc;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v1, p0, LX/Fqc;->r:Landroid/content/Context;

    sget-object v2, LX/0ax;->dz:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2294077
    return-void
.end method

.method public final m(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2294078
    invoke-direct {p0, p1}, LX/Fqc;->o(Ljava/lang/String;)V

    .line 2294079
    iget-object v0, p0, LX/Fqc;->w:LX/BQ1;

    .line 2294080
    iget-object v1, v0, LX/BQ1;->d:LX/5wQ;

    if-nez v1, :cond_0

    .line 2294081
    const/4 v1, 0x0

    .line 2294082
    :goto_0
    move-object v1, v1

    .line 2294083
    iget-object v0, p0, LX/Fqc;->r:Landroid/content/Context;

    const-string v2, "clipboard"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 2294084
    const-string v2, "Profile URL"

    invoke-static {v2, v1}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 2294085
    iget-object v0, p0, LX/Fqc;->r:Landroid/content/Context;

    const v1, 0x7f08155a

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2294086
    return-void

    :cond_0
    iget-object v1, v0, LX/BQ1;->d:LX/5wQ;

    invoke-interface {v1}, LX/5wQ;->b()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final n(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2294087
    invoke-direct {p0, p1}, LX/Fqc;->o(Ljava/lang/String;)V

    .line 2294088
    sget-object v0, LX/0ax;->ji:Ljava/lang/String;

    iget-object v1, p0, LX/Fqc;->v:LX/5SB;

    .line 2294089
    iget-wide v4, v1, LX/5SB;->b:J

    move-wide v2, v4

    .line 2294090
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2294091
    iget-object v0, p0, LX/Fqc;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    iget-object v2, p0, LX/Fqc;->r:Landroid/content/Context;

    invoke-interface {v0, v2, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2294092
    iget-object v1, p0, LX/Fqc;->n:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/Fqc;->r:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2294093
    return-void
.end method
