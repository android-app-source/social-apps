.class public LX/GEH;
.super Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod",
        "<",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0sa;LX/0rq;LX/0tX;LX/1Ck;LX/GF4;LX/GG6;LX/0ad;)V
    .locals 8
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2331424
    move-object v0, p0

    move-object v1, p3

    move-object v2, p2

    move-object v3, p1

    move-object v4, p5

    move-object v5, p4

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;-><init>(LX/0tX;LX/0rq;LX/0sa;LX/GF4;LX/1Ck;LX/GG6;LX/0ad;)V

    .line 2331425
    return-void
.end method

.method public static b(LX/0QB;)LX/GEH;
    .locals 8

    .prologue
    .line 2331426
    new-instance v0, LX/GEH;

    invoke-static {p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v1

    check-cast v1, LX/0sa;

    invoke-static {p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v2

    check-cast v2, LX/0rq;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {p0}, LX/GF4;->a(LX/0QB;)LX/GF4;

    move-result-object v5

    check-cast v5, LX/GF4;

    invoke-static {p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v6

    check-cast v6, LX/GG6;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-direct/range {v0 .. v7}, LX/GEH;-><init>(LX/0sa;LX/0rq;LX/0tX;LX/1Ck;LX/GF4;LX/GG6;LX/0ad;)V

    .line 2331427
    return-object v0
.end method

.method private static f(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;
    .locals 7
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2331428
    const/4 v1, 0x0

    .line 2331429
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel;->a()LX/0Px;

    move-result-object v4

    .line 2331430
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_1

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel;

    .line 2331431
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    .line 2331432
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;

    .line 2331433
    :goto_1
    return-object v0

    .line 2331434
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;Ljava/lang/String;)Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;
    .locals 3

    .prologue
    .line 2331435
    invoke-static {p1}, LX/GEH;->f(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;

    move-result-object v0

    .line 2331436
    if-nez v0, :cond_0

    .line 2331437
    new-instance v0, LX/GGN;

    invoke-direct {v0}, LX/GGN;-><init>()V

    invoke-virtual {p0, p1}, LX/GEH;->a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-result-object v1

    .line 2331438
    iput-object v1, v0, LX/GGN;->a:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2331439
    move-object v0, v0

    .line 2331440
    invoke-virtual {p0, p1}, LX/GEH;->b(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v1

    .line 2331441
    iput-object v1, v0, LX/GGN;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 2331442
    move-object v0, v0

    .line 2331443
    const-string v1, "boosted_product_mobile"

    .line 2331444
    iput-object v1, v0, LX/GGI;->m:Ljava/lang/String;

    .line 2331445
    move-object v0, v0

    .line 2331446
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v1

    .line 2331447
    iput-object v1, v0, LX/GGI;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    .line 2331448
    move-object v0, v0

    .line 2331449
    iput-object p2, v0, LX/GGI;->c:Ljava/lang/String;

    .line 2331450
    move-object v0, v0

    .line 2331451
    sget-object v1, LX/GGB;->ERROR:LX/GGB;

    .line 2331452
    iput-object v1, v0, LX/GGI;->e:LX/GGB;

    .line 2331453
    move-object v0, v0

    .line 2331454
    sget-object v1, LX/8wL;->PROMOTE_PRODUCT:LX/8wL;

    .line 2331455
    iput-object v1, v0, LX/GGI;->b:LX/8wL;

    .line 2331456
    move-object v0, v0

    .line 2331457
    invoke-virtual {v0}, LX/GGI;->a()Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2331458
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, LX/GGN;

    invoke-direct {v1}, LX/GGN;-><init>()V

    invoke-virtual {p0, p1}, LX/GEH;->a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-result-object v2

    .line 2331459
    iput-object v2, v1, LX/GGN;->a:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2331460
    move-object v1, v1

    .line 2331461
    invoke-virtual {p0, p1}, LX/GEH;->b(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-result-object v2

    .line 2331462
    iput-object v2, v1, LX/GGN;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    .line 2331463
    move-object v1, v1

    .line 2331464
    const-string v2, "boosted_product_mobile"

    .line 2331465
    iput-object v2, v1, LX/GGI;->m:Ljava/lang/String;

    .line 2331466
    move-object v1, v1

    .line 2331467
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v2

    .line 2331468
    iput-object v2, v1, LX/GGI;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    .line 2331469
    move-object v1, v1

    .line 2331470
    iput-object p2, v1, LX/GGI;->c:Ljava/lang/String;

    .line 2331471
    move-object v1, v1

    .line 2331472
    invoke-virtual {p0, p1}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->c(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)LX/GGB;

    move-result-object v2

    .line 2331473
    iput-object v2, v1, LX/GGI;->e:LX/GGB;

    .line 2331474
    move-object v1, v1

    .line 2331475
    sget-object v2, LX/8wL;->PROMOTE_PRODUCT:LX/8wL;

    .line 2331476
    iput-object v2, v1, LX/GGI;->b:LX/8wL;

    .line 2331477
    move-object v1, v1

    .line 2331478
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->k()Ljava/lang/String;

    move-result-object v0

    .line 2331479
    iput-object v0, v1, LX/GGI;->d:Ljava/lang/String;

    .line 2331480
    move-object v0, v1

    .line 2331481
    invoke-virtual {v0}, LX/GGI;->a()Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    goto :goto_0
.end method

.method public final a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/model/CreativeAdModel;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2331482
    invoke-static {p1}, LX/GEH;->f(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;

    move-result-object v1

    .line 2331483
    if-nez v1, :cond_0

    .line 2331484
    const/4 v0, 0x0

    .line 2331485
    :goto_0
    return-object v0

    .line 2331486
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->l()LX/2uF;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v3, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v2, v0, v4, v3}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2331487
    new-instance v2, LX/GGK;

    invoke-direct {v2}, LX/GGK;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->p()Ljava/lang/String;

    move-result-object v3

    .line 2331488
    iput-object v3, v2, LX/GGK;->c:Ljava/lang/String;

    .line 2331489
    move-object v2, v2

    .line 2331490
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->j()Ljava/lang/String;

    move-result-object v3

    .line 2331491
    iput-object v3, v2, LX/GGK;->e:Ljava/lang/String;

    .line 2331492
    move-object v2, v2

    .line 2331493
    const-string v3, "https://www.facebook.com/commerce/products/"

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2331494
    iput-object v1, v2, LX/GGK;->f:Ljava/lang/String;

    .line 2331495
    move-object v1, v2

    .line 2331496
    iput-object v0, v1, LX/GGK;->h:Ljava/lang/String;

    .line 2331497
    move-object v0, v1

    .line 2331498
    invoke-virtual {v0}, LX/GGK;->a()Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-result-object v0

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2331499
    const-string v0, "promote_product_promotion_key"

    return-object v0
.end method

.method public final b(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2331500
    invoke-static {p1}, LX/GEH;->f(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;

    move-result-object v0

    .line 2331501
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-gtz v1, :cond_1

    .line 2331502
    :cond_0
    const/4 v0, 0x0

    .line 2331503
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductInfoModel$OrderedCollectionsModel$NodesModel$CollectionProductItemsModel$CollectionProductItemsNodesModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$PromoteProductAdminInfoModel$ProductPromotionsModel;->a()LX/0Px;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2331504
    const-string v0, "boosted_product_mobile"

    return-object v0
.end method
