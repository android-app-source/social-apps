.class public LX/G92;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:F

.field public final b:F


# direct methods
.method public constructor <init>(FF)V
    .locals 0

    .prologue
    .line 2321474
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2321475
    iput p1, p0, LX/G92;->a:F

    .line 2321476
    iput p2, p0, LX/G92;->b:F

    .line 2321477
    return-void
.end method

.method public static a(LX/G92;LX/G92;)F
    .locals 6

    .prologue
    .line 2321463
    iget v0, p0, LX/G92;->a:F

    iget v1, p0, LX/G92;->b:F

    iget v2, p1, LX/G92;->a:F

    iget v3, p1, LX/G92;->b:F

    .line 2321464
    sub-float v4, v0, v2

    .line 2321465
    sub-float v5, v1, v3

    .line 2321466
    mul-float/2addr v4, v4

    mul-float/2addr v5, v5

    add-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v4, v4

    move v0, v4

    .line 2321467
    return v0
.end method


# virtual methods
.method public final a()F
    .locals 1

    .prologue
    .line 2321473
    iget v0, p0, LX/G92;->a:F

    return v0
.end method

.method public final b()F
    .locals 1

    .prologue
    .line 2321478
    iget v0, p0, LX/G92;->b:F

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2321469
    instance-of v1, p1, LX/G92;

    if-eqz v1, :cond_0

    .line 2321470
    check-cast p1, LX/G92;

    .line 2321471
    iget v1, p0, LX/G92;->a:F

    iget v2, p1, LX/G92;->a:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    iget v1, p0, LX/G92;->b:F

    iget v2, p1, LX/G92;->b:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 2321472
    :cond_0
    return v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2321468
    iget v0, p0, LX/G92;->a:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, LX/G92;->b:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2321456
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x19

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2321457
    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2321458
    iget v1, p0, LX/G92;->a:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 2321459
    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2321460
    iget v1, p0, LX/G92;->b:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 2321461
    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2321462
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
