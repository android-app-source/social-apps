.class public LX/FA8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/FA8;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2206706
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2206707
    iput-object p1, p0, LX/FA8;->a:LX/0Zb;

    .line 2206708
    return-void
.end method

.method public static a(LX/0QB;)LX/FA8;
    .locals 4

    .prologue
    .line 2206709
    sget-object v0, LX/FA8;->b:LX/FA8;

    if-nez v0, :cond_1

    .line 2206710
    const-class v1, LX/FA8;

    monitor-enter v1

    .line 2206711
    :try_start_0
    sget-object v0, LX/FA8;->b:LX/FA8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2206712
    if-eqz v2, :cond_0

    .line 2206713
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2206714
    new-instance p0, LX/FA8;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/FA8;-><init>(LX/0Zb;)V

    .line 2206715
    move-object v0, p0

    .line 2206716
    sput-object v0, LX/FA8;->b:LX/FA8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2206717
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2206718
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2206719
    :cond_1
    sget-object v0, LX/FA8;->b:LX/FA8;

    return-object v0

    .line 2206720
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2206721
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
