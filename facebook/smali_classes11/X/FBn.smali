.class public final LX/FBn;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/Integer;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "type"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation
.end field

.field public final b:Lcom/facebook/bookmark/model/Bookmark;

.field public final c:I
    .annotation build Landroid/support/annotation/DrawableRes;
    .end annotation
.end field

.field public final d:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;ILjava/lang/CharSequence;)V
    .locals 0
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation

    .prologue
    .line 2209770
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2209771
    iput-object p1, p0, LX/FBn;->a:Ljava/lang/Integer;

    .line 2209772
    iput-object p2, p0, LX/FBn;->b:Lcom/facebook/bookmark/model/Bookmark;

    .line 2209773
    iput p3, p0, LX/FBn;->c:I

    .line 2209774
    iput-object p4, p0, LX/FBn;->d:Ljava/lang/CharSequence;

    .line 2209775
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;II)LX/FBn;
    .locals 2
    .param p4    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "createFromContext"
        processor = "com.facebook.thecount.transformer.Transformer"
    .end annotation

    .prologue
    .line 2209776
    new-instance v0, LX/FBn;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, p2, p3, v1}, LX/FBn;-><init>(Ljava/lang/Integer;Lcom/facebook/bookmark/model/Bookmark;ILjava/lang/CharSequence;)V

    return-object v0
.end method
