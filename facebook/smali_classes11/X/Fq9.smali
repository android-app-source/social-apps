.class public LX/Fq9;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/Fq9;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0tf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0tf",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public c:Ljava/lang/String;

.field public d:J

.field public e:LX/1Zp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Zp",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<+",
            "Landroid/os/Parcelable;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2292502
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2292503
    new-instance v0, LX/0tf;

    invoke-direct {v0}, LX/0tf;-><init>()V

    iput-object v0, p0, LX/Fq9;->b:LX/0tf;

    .line 2292504
    iput-object p1, p0, LX/Fq9;->a:LX/0Or;

    .line 2292505
    invoke-interface {p1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, LX/Fq9;->c:Ljava/lang/String;

    .line 2292506
    return-void
.end method

.method public static a(LX/0QB;)LX/Fq9;
    .locals 4

    .prologue
    .line 2292507
    sget-object v0, LX/Fq9;->f:LX/Fq9;

    if-nez v0, :cond_1

    .line 2292508
    const-class v1, LX/Fq9;

    monitor-enter v1

    .line 2292509
    :try_start_0
    sget-object v0, LX/Fq9;->f:LX/Fq9;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2292510
    if-eqz v2, :cond_0

    .line 2292511
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2292512
    new-instance v3, LX/Fq9;

    const/16 p0, 0x15e7

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Fq9;-><init>(LX/0Or;)V

    .line 2292513
    move-object v0, v3

    .line 2292514
    sput-object v0, LX/Fq9;->f:LX/Fq9;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2292515
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2292516
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2292517
    :cond_1
    sget-object v0, LX/Fq9;->f:LX/Fq9;

    return-object v0

    .line 2292518
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2292519
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(J)LX/1Zp;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "LX/1Zp",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<+",
            "Landroid/os/Parcelable;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2292520
    iget-object v5, p0, LX/Fq9;->a:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iget-object v6, p0, LX/Fq9;->c:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2292521
    iget-object v7, p0, LX/Fq9;->b:LX/0tf;

    invoke-virtual {v7}, LX/0tf;->b()V

    .line 2292522
    const-wide/16 v7, 0x0

    iput-wide v7, p0, LX/Fq9;->d:J

    .line 2292523
    iget-object v7, p0, LX/Fq9;->e:LX/1Zp;

    if-eqz v7, :cond_0

    .line 2292524
    iget-object v7, p0, LX/Fq9;->e:LX/1Zp;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, LX/1Zp;->cancel(Z)Z

    .line 2292525
    const/4 v7, 0x0

    iput-object v7, p0, LX/Fq9;->e:LX/1Zp;

    .line 2292526
    :cond_0
    iget-object v5, p0, LX/Fq9;->a:LX/0Or;

    invoke-interface {v5}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    iput-object v5, p0, LX/Fq9;->c:Ljava/lang/String;

    .line 2292527
    :cond_1
    iget-wide v2, p0, LX/Fq9;->d:J

    cmp-long v1, p1, v2

    if-eqz v1, :cond_2

    .line 2292528
    :goto_0
    return-object v0

    .line 2292529
    :cond_2
    iget-object v1, p0, LX/Fq9;->e:LX/1Zp;

    .line 2292530
    const-wide/16 v2, 0x0

    iput-wide v2, p0, LX/Fq9;->d:J

    .line 2292531
    iput-object v0, p0, LX/Fq9;->e:LX/1Zp;

    move-object v0, v1

    .line 2292532
    goto :goto_0
.end method

.method public final a(JI)V
    .locals 3

    .prologue
    .line 2292533
    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    .line 2292534
    iget-object v0, p0, LX/Fq9;->b:LX/0tf;

    .line 2292535
    invoke-virtual {v0, p1, p2}, LX/0tf;->b(J)V

    .line 2292536
    :goto_0
    return-void

    .line 2292537
    :cond_0
    iget-object v0, p0, LX/Fq9;->b:LX/0tf;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, LX/0tf;->b(JLjava/lang/Object;)V

    goto :goto_0
.end method

.method public final b(J)I
    .locals 1

    .prologue
    .line 2292538
    iget-object v0, p0, LX/Fq9;->b:LX/0tf;

    invoke-virtual {v0, p1, p2}, LX/0tf;->a(J)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2292539
    iget-object v0, p0, LX/Fq9;->b:LX/0tf;

    invoke-virtual {v0, p1, p2}, LX/0tf;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2292540
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method
