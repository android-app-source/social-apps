.class public final LX/Gkz;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/groupsgrid/mutations/FavoriteGroupsMutationsModels$GroupsBookmarkAddToFavoritesMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Z

.field public final synthetic c:Z

.field public final synthetic d:LX/Gl6;


# direct methods
.method public constructor <init>(LX/Gl6;Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 2389745
    iput-object p1, p0, LX/Gkz;->d:LX/Gl6;

    iput-object p2, p0, LX/Gkz;->a:Ljava/lang/String;

    iput-boolean p3, p0, LX/Gkz;->b:Z

    iput-boolean p4, p0, LX/Gkz;->c:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2389746
    iget-object v0, p0, LX/Gkz;->d:LX/Gl6;

    iget-object v0, v0, LX/Gl6;->b:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f081b54

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2389747
    iget-object v1, p0, LX/Gkz;->d:LX/Gl6;

    iget-object v0, p0, LX/Gkz;->d:LX/Gl6;

    iget-object v2, v0, LX/Gl6;->g:Ljava/util/HashMap;

    iget-object v3, p0, LX/Gkz;->a:Ljava/lang/String;

    iget-boolean v0, p0, LX/Gkz;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-boolean v4, p0, LX/Gkz;->c:Z

    invoke-static {v2, v3, v0, v4}, LX/Gkr;->a(Ljava/util/HashMap;Ljava/lang/String;ZZ)Ljava/util/HashMap;

    move-result-object v0

    .line 2389748
    iput-object v0, v1, LX/Gl6;->g:Ljava/util/HashMap;

    .line 2389749
    iget-object v0, p0, LX/Gkz;->d:LX/Gl6;

    invoke-static {v0}, LX/Gl6;->g(LX/Gl6;)V

    .line 2389750
    return-void

    .line 2389751
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2389752
    return-void
.end method
