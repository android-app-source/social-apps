.class public LX/FGl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/2ML;


# direct methods
.method public constructor <init>(LX/0Or;LX/2ML;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/media/upload/IsUploadExternalUriEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/2ML;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2219494
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2219495
    iput-object p1, p0, LX/FGl;->a:LX/0Or;

    .line 2219496
    iput-object p2, p0, LX/FGl;->b:LX/2ML;

    .line 2219497
    return-void
.end method

.method public static a(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2219498
    sget-object v0, LX/FGk;->a:[I

    iget-object v1, p0, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    invoke-virtual {v1}, LX/2MK;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2219499
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Unexpected attachment type"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2219500
    :pswitch_0
    invoke-virtual {p0}, Lcom/facebook/ui/media/attachments/MediaResource;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2219501
    const-string v0, "me/message_animated_images"

    .line 2219502
    :goto_0
    return-object v0

    .line 2219503
    :cond_0
    const-string v0, "me/message_images"

    goto :goto_0

    .line 2219504
    :pswitch_1
    const-string v0, "messagevideoattachment"

    goto :goto_0

    .line 2219505
    :pswitch_2
    const-string v0, "me/message_audios"

    goto :goto_0

    .line 2219506
    :pswitch_3
    const-string v0, "me/message_files"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b(LX/0QB;)LX/FGl;
    .locals 3

    .prologue
    .line 2219507
    new-instance v1, LX/FGl;

    const/16 v0, 0x1525

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v2

    invoke-static {p0}, LX/2ML;->a(LX/0QB;)LX/2ML;

    move-result-object v0

    check-cast v0, LX/2ML;

    invoke-direct {v1, v2, v0}, LX/FGl;-><init>(LX/0Or;LX/2ML;)V

    .line 2219508
    return-object v1
.end method


# virtual methods
.method public final b(Lcom/facebook/ui/media/attachments/MediaResource;)Z
    .locals 2

    .prologue
    .line 2219509
    iget-object v0, p0, LX/FGl;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->B:Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->d:LX/2MK;

    sget-object v1, LX/2MK;->PHOTO:LX/2MK;

    if-ne v0, v1, :cond_1

    iget-boolean v0, p1, Lcom/facebook/ui/media/attachments/MediaResource;->C:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/FGl;->b:LX/2ML;

    invoke-virtual {v0, p1}, LX/2ML;->c(Lcom/facebook/ui/media/attachments/MediaResource;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
