.class public final enum LX/FG4;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FG4;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FG4;

.field public static final enum DOWNLOADED:LX/FG4;

.field public static final enum FAILURE:LX/FG4;

.field public static final enum NO_PERMISSION:LX/FG4;

.field public static final enum PRE_EXISTING:LX/FG4;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2218189
    new-instance v0, LX/FG4;

    const-string v1, "DOWNLOADED"

    invoke-direct {v0, v1, v2}, LX/FG4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FG4;->DOWNLOADED:LX/FG4;

    .line 2218190
    new-instance v0, LX/FG4;

    const-string v1, "PRE_EXISTING"

    invoke-direct {v0, v1, v3}, LX/FG4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FG4;->PRE_EXISTING:LX/FG4;

    .line 2218191
    new-instance v0, LX/FG4;

    const-string v1, "FAILURE"

    invoke-direct {v0, v1, v4}, LX/FG4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FG4;->FAILURE:LX/FG4;

    .line 2218192
    new-instance v0, LX/FG4;

    const-string v1, "NO_PERMISSION"

    invoke-direct {v0, v1, v5}, LX/FG4;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FG4;->NO_PERMISSION:LX/FG4;

    .line 2218193
    const/4 v0, 0x4

    new-array v0, v0, [LX/FG4;

    sget-object v1, LX/FG4;->DOWNLOADED:LX/FG4;

    aput-object v1, v0, v2

    sget-object v1, LX/FG4;->PRE_EXISTING:LX/FG4;

    aput-object v1, v0, v3

    sget-object v1, LX/FG4;->FAILURE:LX/FG4;

    aput-object v1, v0, v4

    sget-object v1, LX/FG4;->NO_PERMISSION:LX/FG4;

    aput-object v1, v0, v5

    sput-object v0, LX/FG4;->$VALUES:[LX/FG4;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2218194
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FG4;
    .locals 1

    .prologue
    .line 2218195
    const-class v0, LX/FG4;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FG4;

    return-object v0
.end method

.method public static values()[LX/FG4;
    .locals 1

    .prologue
    .line 2218196
    sget-object v0, LX/FG4;->$VALUES:[LX/FG4;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FG4;

    return-object v0
.end method
