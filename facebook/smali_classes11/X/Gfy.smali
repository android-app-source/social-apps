.class public LX/Gfy;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlFallbackComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Gfy",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlFallbackComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2378291
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2378292
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Gfy;->b:LX/0Zi;

    .line 2378293
    iput-object p1, p0, LX/Gfy;->a:LX/0Ot;

    .line 2378294
    return-void
.end method

.method public static a(LX/0QB;)LX/Gfy;
    .locals 4

    .prologue
    .line 2378295
    const-class v1, LX/Gfy;

    monitor-enter v1

    .line 2378296
    :try_start_0
    sget-object v0, LX/Gfy;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2378297
    sput-object v2, LX/Gfy;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2378298
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2378299
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2378300
    new-instance v3, LX/Gfy;

    const/16 p0, 0x2126

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Gfy;-><init>(LX/0Ot;)V

    .line 2378301
    move-object v0, v3

    .line 2378302
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2378303
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gfy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2378304
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2378305
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2378306
    const v0, -0xf772de3

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2378307
    check-cast p2, LX/Gfx;

    .line 2378308
    iget-object v0, p0, LX/Gfy;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlFallbackComponentSpec;

    iget-object v1, p2, LX/Gfx;->a:LX/GfY;

    iget-object v2, p2, LX/Gfx;->b:LX/1Pp;

    const/4 p2, 0x2

    const/4 p0, 0x1

    .line 2378309
    iget-object v3, v1, LX/GfY;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;

    const/4 v4, 0x0

    .line 2378310
    invoke-interface {v3}, LX/25E;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v5

    .line 2378311
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPage;->u()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPage;->u()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPage;->u()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPhoto;->P()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 2378312
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPage;->u()Lcom/facebook/graphql/model/GraphQLFocusedPhoto;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLFocusedPhoto;->j()Lcom/facebook/graphql/model/GraphQLPhoto;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPhoto;->P()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    invoke-static {v5}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v5

    .line 2378313
    :goto_0
    if-nez v5, :cond_1

    .line 2378314
    invoke-static {v3}, LX/Gdi;->c(LX/25E;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v5

    .line 2378315
    if-eqz v5, :cond_0

    invoke-static {v5}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v4

    .line 2378316
    :cond_0
    :goto_1
    move-object v3, v4

    .line 2378317
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x6

    invoke-interface {v4, v5, p2}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, p0, p2}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlFallbackComponentSpec;->b:LX/1nu;

    invoke-virtual {v5, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v5

    invoke-virtual {v5, v3}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v3

    sget-object v5, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlFallbackComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v5}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v3

    sget-object v5, LX/1Up;->g:LX/1Up;

    invoke-virtual {v3, v5}, LX/1nw;->a(LX/1Up;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/1nw;->a(Z)LX/1nw;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    const v5, 0x7f0b22fa

    invoke-interface {v3, v5}, LX/1Di;->q(I)LX/1Di;

    move-result-object v3

    .line 2378318
    const v5, -0xf772de3

    const/4 p0, 0x0

    invoke-static {p1, v5, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v5

    move-object v5, v5

    .line 2378319
    invoke-interface {v3, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2378320
    return-object v0

    :cond_1
    move-object v4, v5

    goto :goto_1

    :cond_2
    move-object v5, v4

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2378321
    invoke-static {}, LX/1dS;->b()V

    .line 2378322
    iget v0, p1, LX/1dQ;->b:I

    .line 2378323
    packed-switch v0, :pswitch_data_0

    .line 2378324
    :goto_0
    return-object v2

    .line 2378325
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2378326
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2378327
    check-cast v1, LX/Gfx;

    .line 2378328
    iget-object p1, p0, LX/Gfy;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlFallbackComponentSpec;

    iget-object p2, v1, LX/Gfx;->a:LX/GfY;

    .line 2378329
    iget-object p0, p1, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlFallbackComponentSpec;->c:LX/Gfl;

    iget-object v1, p2, LX/GfY;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Gfl;->a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLPage;)V

    .line 2378330
    goto :goto_0

    :pswitch_data_0
    .packed-switch -0xf772de3
        :pswitch_0
    .end packed-switch
.end method
