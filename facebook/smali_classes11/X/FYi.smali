.class public LX/FYi;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "BadMethodUse-java.lang.System.currentTimeMillis"
    }
.end annotation


# instance fields
.field public a:LX/FYr;

.field private b:LX/FYN;

.field private c:LX/FYg;

.field private d:LX/FYR;

.field private e:LX/0tQ;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;LX/FYS;LX/0tQ;)V
    .locals 4
    .param p1    # Landroid/support/v4/app/FragmentActivity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2256435
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2256436
    iput-object p4, p0, LX/FYi;->e:LX/0tQ;

    .line 2256437
    new-instance v1, LX/FYR;

    const-class v0, LX/FYc;

    invoke-interface {p3, v0}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/FYc;

    invoke-direct {v1, p1, p2, v0}, LX/FYR;-><init>(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;LX/FYc;)V

    .line 2256438
    move-object v0, v1

    .line 2256439
    iput-object v0, p0, LX/FYi;->d:LX/FYR;

    .line 2256440
    const-string v0, "PLACES"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, LX/FYQ;

    invoke-direct {v0}, LX/FYQ;-><init>()V

    :goto_0
    iput-object v0, p0, LX/FYi;->b:LX/FYN;

    .line 2256441
    new-instance v0, LX/FYg;

    invoke-direct {v0, p1}, LX/FYg;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/FYi;->c:LX/FYg;

    .line 2256442
    new-instance v0, LX/FYr;

    new-instance v1, LX/G6j;

    iget-object v2, p0, LX/FYi;->c:LX/FYg;

    iget-object v3, p0, LX/FYi;->d:LX/FYR;

    invoke-direct {v1, v2, v3}, LX/G6j;-><init>(LX/1OM;LX/1OM;)V

    invoke-direct {v0, v1, p1}, LX/FYr;-><init>(LX/1OM;Landroid/content/Context;)V

    iput-object v0, p0, LX/FYi;->a:LX/FYr;

    .line 2256443
    return-void

    .line 2256444
    :cond_0
    new-instance v0, LX/FYO;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, p0, LX/FYi;->e:LX/0tQ;

    invoke-direct {v0, p1, v2, v3, v1}, LX/FYO;-><init>(Landroid/content/Context;JLX/0tQ;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;LX/BO1;)V
    .locals 2
    .param p2    # LX/BO1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2256445
    iget-object v0, p0, LX/FYi;->a:LX/FYr;

    const/4 v1, 0x0

    .line 2256446
    iput-boolean v1, v0, LX/FYr;->g:Z

    .line 2256447
    iput-boolean v1, v0, LX/FYr;->h:Z

    .line 2256448
    iget-object v0, p0, LX/FYi;->d:LX/FYR;

    invoke-virtual {v0, p1, p2}, LX/EkO;->a(Landroid/database/Cursor;LX/AU0;)V

    .line 2256449
    if-nez p2, :cond_0

    .line 2256450
    iget-object v0, p0, LX/FYi;->c:LX/FYg;

    .line 2256451
    const/4 v1, 0x0

    iput-object v1, v0, LX/FYg;->c:Ljava/util/ArrayList;

    .line 2256452
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2256453
    :goto_0
    return-void

    .line 2256454
    :cond_0
    iget-object v0, p0, LX/FYi;->c:LX/FYg;

    iget-object v1, p0, LX/FYi;->b:LX/FYN;

    invoke-interface {v1, p2}, LX/FYN;->a(LX/BO1;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2256455
    iput-object v1, v0, LX/FYg;->c:Ljava/util/ArrayList;

    .line 2256456
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2256457
    goto :goto_0
.end method
