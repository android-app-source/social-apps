.class public final LX/H1x;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0U1;

.field public static final b:LX/0U1;

.field public static final c:LX/0U1;

.field public static final d:LX/0U1;

.field public static final e:LX/0U1;

.field public static final f:LX/0U1;

.field public static final g:LX/0U1;

.field public static final h:LX/0U1;

.field public static final i:LX/0U1;

.field public static final j:LX/0U1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2416083
    new-instance v0, LX/0U1;

    const-string v1, "tile_id"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/H1x;->a:LX/0U1;

    .line 2416084
    new-instance v0, LX/0U1;

    const-string v1, "queryFunctions"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/H1x;->b:LX/0U1;

    .line 2416085
    new-instance v0, LX/0U1;

    const-string v1, "topicIds"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/H1x;->c:LX/0U1;

    .line 2416086
    new-instance v0, LX/0U1;

    const-string v1, "northBound"

    const-string v2, "REAL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/H1x;->d:LX/0U1;

    .line 2416087
    new-instance v0, LX/0U1;

    const-string v1, "southBound"

    const-string v2, "REAL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/H1x;->e:LX/0U1;

    .line 2416088
    new-instance v0, LX/0U1;

    const-string v1, "eastBound"

    const-string v2, "REAL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/H1x;->f:LX/0U1;

    .line 2416089
    new-instance v0, LX/0U1;

    const-string v1, "westBound"

    const-string v2, "REAL"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/H1x;->g:LX/0U1;

    .line 2416090
    new-instance v0, LX/0U1;

    const-string v1, "tileJson"

    const-string v2, "TEXT"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/H1x;->h:LX/0U1;

    .line 2416091
    new-instance v0, LX/0U1;

    const-string v1, "tileTtl"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/H1x;->i:LX/0U1;

    .line 2416092
    new-instance v0, LX/0U1;

    const-string v1, "tileFetchedTime"

    const-string v2, "INTEGER"

    invoke-direct {v0, v1, v2}, LX/0U1;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, LX/H1x;->j:LX/0U1;

    return-void
.end method
