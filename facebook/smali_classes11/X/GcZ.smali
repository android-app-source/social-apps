.class public final LX/GcZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/view/LayoutInflater;

.field public final synthetic b:Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;Landroid/view/LayoutInflater;)V
    .locals 0

    .prologue
    .line 2372020
    iput-object p1, p0, LX/GcZ;->b:Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;

    iput-object p2, p0, LX/GcZ;->a:Landroid/view/LayoutInflater;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    const v0, -0x2890c4b4

    invoke-static {v5, v6, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2372021
    iget-object v0, p0, LX/GcZ;->a:Landroid/view/LayoutInflater;

    const v2, 0x7f0303df

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 2372022
    const v0, 0x7f0d0c1a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    .line 2372023
    new-instance v3, LX/31Y;

    iget-object v4, p0, LX/GcZ;->b:Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-direct {v3, v4}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v6}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v2

    const-string v3, "Cancel"

    new-instance v4, LX/GcY;

    invoke-direct {v4, p0}, LX/GcY;-><init>(LX/GcZ;)V

    invoke-virtual {v2, v3, v4}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v2

    const-string v3, "Submit"

    new-instance v4, LX/GcX;

    invoke-direct {v4, p0, v0}, LX/GcX;-><init>(LX/GcZ;Lcom/facebook/resources/ui/FbEditText;)V

    invoke-virtual {v2, v3, v4}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v0

    invoke-virtual {v0}, LX/0ju;->a()LX/2EJ;

    move-result-object v0

    invoke-virtual {v0}, LX/2EJ;->show()V

    .line 2372024
    const v0, 0x1979103a

    invoke-static {v5, v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
