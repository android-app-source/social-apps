.class public final LX/GHb;
.super LX/8wQ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/8wQ",
        "<",
        "LX/8wO;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/service/BoostLiveService;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/service/BoostLiveService;)V
    .locals 0

    .prologue
    .line 2335072
    iput-object p1, p0, LX/GHb;->a:Lcom/facebook/adinterfaces/service/BoostLiveService;

    invoke-direct {p0}, LX/8wQ;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/adinterfaces/service/BoostLiveService;B)V
    .locals 0

    .prologue
    .line 2335081
    invoke-direct {p0, p1}, LX/GHb;-><init>(Lcom/facebook/adinterfaces/service/BoostLiveService;)V

    return-void
.end method

.method private a(LX/8wO;)V
    .locals 4

    .prologue
    .line 2335075
    iget-object v0, p0, LX/GHb;->a:Lcom/facebook/adinterfaces/service/BoostLiveService;

    iget-object v0, v0, Lcom/facebook/adinterfaces/service/BoostLiveService;->f:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GHb;->a:Lcom/facebook/adinterfaces/service/BoostLiveService;

    iget-object v0, v0, Lcom/facebook/adinterfaces/service/BoostLiveService;->f:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    if-ne v0, v1, :cond_0

    .line 2335076
    iget-object v0, p0, LX/GHb;->a:Lcom/facebook/adinterfaces/service/BoostLiveService;

    iget-object v0, v0, Lcom/facebook/adinterfaces/service/BoostLiveService;->f:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2335077
    iget-object v1, p1, LX/8wO;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2335078
    invoke-interface {v0, v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a(Ljava/lang/String;)V

    .line 2335079
    iget-object v0, p0, LX/GHb;->a:Lcom/facebook/adinterfaces/service/BoostLiveService;

    iget-object v1, v0, Lcom/facebook/adinterfaces/service/BoostLiveService;->b:LX/GDg;

    iget-object v0, p0, LX/GHb;->a:Lcom/facebook/adinterfaces/service/BoostLiveService;

    iget-object v2, v0, Lcom/facebook/adinterfaces/service/BoostLiveService;->e:LX/GCE;

    iget-object v0, p0, LX/GHb;->a:Lcom/facebook/adinterfaces/service/BoostLiveService;

    iget-object v0, v0, Lcom/facebook/adinterfaces/service/BoostLiveService;->f:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v3, p0, LX/GHb;->a:Lcom/facebook/adinterfaces/service/BoostLiveService;

    iget-object v3, v3, Lcom/facebook/adinterfaces/service/BoostLiveService;->c:Landroid/content/Context;

    invoke-virtual {v1, v2, v0, v3}, LX/GDg;->a(LX/GCE;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;)V

    .line 2335080
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<",
            "LX/8wO;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2335074
    const-class v0, LX/8wO;

    return-object v0
.end method

.method public final synthetic b(LX/0b7;)V
    .locals 0

    .prologue
    .line 2335073
    check-cast p1, LX/8wO;

    invoke-direct {p0, p1}, LX/GHb;->a(LX/8wO;)V

    return-void
.end method
