.class public LX/Gxx;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;

.field private final b:LX/0W9;


# direct methods
.method public constructor <init>(LX/0Zb;LX/0W9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2408718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2408719
    iput-object p1, p0, LX/Gxx;->a:LX/0Zb;

    .line 2408720
    iput-object p2, p0, LX/Gxx;->b:LX/0W9;

    .line 2408721
    return-void
.end method

.method public static b(LX/0QB;)LX/Gxx;
    .locals 3

    .prologue
    .line 2408706
    new-instance v2, LX/Gxx;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v1

    check-cast v1, LX/0W9;

    invoke-direct {v2, v0, v1}, LX/Gxx;-><init>(LX/0Zb;LX/0W9;)V

    .line 2408707
    return-object v2
.end method

.method public static b(LX/Gxx;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2408714
    const-string v0, "device"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2408715
    invoke-static {}, LX/0W9;->e()Ljava/util/Locale;

    move-result-object v0

    .line 2408716
    :goto_0
    iget-object v1, p0, LX/Gxx;->b:LX/0W9;

    invoke-virtual {v1, v0}, LX/0W9;->a(Ljava/util/Locale;)Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, LX/0sI;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 2408717
    :cond_0
    invoke-static {p1}, LX/0e9;->a(Ljava/lang/String;)Ljava/util/Locale;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2408708
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v1, LX/Gxw;->SWITCH:LX/Gxw;

    invoke-virtual {v1}, LX/Gxw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "language_switcher"

    .line 2408709
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2408710
    move-object v0, v0

    .line 2408711
    const-string v1, "current_app_locale"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "new_app_locale"

    invoke-virtual {v0, v1, p2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "current_fb_locale"

    invoke-static {p0, p1}, LX/Gxx;->b(LX/Gxx;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "new_fb_locale"

    invoke-static {p0, p2}, LX/Gxx;->b(LX/Gxx;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "system_locale"

    invoke-static {}, LX/0W9;->e()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2408712
    iget-object v1, p0, LX/Gxx;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2408713
    return-void
.end method
