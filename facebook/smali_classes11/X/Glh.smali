.class public final enum LX/Glh;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Glh;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Glh;

.field public static final enum EMPTY:LX/Glh;

.field public static final enum FAILED:LX/Glh;

.field public static final enum LOADED:LX/Glh;

.field public static final enum LOADING:LX/Glh;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2391181
    new-instance v0, LX/Glh;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v2}, LX/Glh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Glh;->LOADING:LX/Glh;

    .line 2391182
    new-instance v0, LX/Glh;

    const-string v1, "EMPTY"

    invoke-direct {v0, v1, v3}, LX/Glh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Glh;->EMPTY:LX/Glh;

    .line 2391183
    new-instance v0, LX/Glh;

    const-string v1, "LOADED"

    invoke-direct {v0, v1, v4}, LX/Glh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Glh;->LOADED:LX/Glh;

    .line 2391184
    new-instance v0, LX/Glh;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v5}, LX/Glh;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Glh;->FAILED:LX/Glh;

    .line 2391185
    const/4 v0, 0x4

    new-array v0, v0, [LX/Glh;

    sget-object v1, LX/Glh;->LOADING:LX/Glh;

    aput-object v1, v0, v2

    sget-object v1, LX/Glh;->EMPTY:LX/Glh;

    aput-object v1, v0, v3

    sget-object v1, LX/Glh;->LOADED:LX/Glh;

    aput-object v1, v0, v4

    sget-object v1, LX/Glh;->FAILED:LX/Glh;

    aput-object v1, v0, v5

    sput-object v0, LX/Glh;->$VALUES:[LX/Glh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2391186
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Glh;
    .locals 1

    .prologue
    .line 2391187
    const-class v0, LX/Glh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Glh;

    return-object v0
.end method

.method public static values()[LX/Glh;
    .locals 1

    .prologue
    .line 2391188
    sget-object v0, LX/Glh;->$VALUES:[LX/Glh;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Glh;

    return-object v0
.end method
