.class public LX/FI3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/FI2;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FI3;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2221371
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/FI3;
    .locals 3

    .prologue
    .line 2221399
    sget-object v0, LX/FI3;->a:LX/FI3;

    if-nez v0, :cond_1

    .line 2221400
    const-class v1, LX/FI3;

    monitor-enter v1

    .line 2221401
    :try_start_0
    sget-object v0, LX/FI3;->a:LX/FI3;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2221402
    if-eqz v2, :cond_0

    .line 2221403
    :try_start_1
    new-instance v0, LX/FI3;

    invoke-direct {v0}, LX/FI3;-><init>()V

    .line 2221404
    move-object v0, v0

    .line 2221405
    sput-object v0, LX/FI3;->a:LX/FI3;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2221406
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2221407
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2221408
    :cond_1
    sget-object v0, LX/FI3;->a:LX/FI3;

    return-object v0

    .line 2221409
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2221410
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2221377
    check-cast p1, LX/FI2;

    .line 2221378
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "cancel_stream_upload"

    .line 2221379
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 2221380
    move-object v0, v0

    .line 2221381
    const-string v1, "POST"

    .line 2221382
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 2221383
    move-object v0, v0

    .line 2221384
    new-instance v1, Ljava/lang/StringBuilder;

    const-string p0, "messenger_videos/"

    invoke-direct {v1, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object p0, p1, LX/FI2;->a:Ljava/lang/String;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string p0, "/?phase=cancel"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2221385
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 2221386
    move-object v0, v0

    .line 2221387
    const/4 v1, 0x1

    .line 2221388
    iput-boolean v1, v0, LX/14O;->o:Z

    .line 2221389
    move-object v0, v0

    .line 2221390
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2221391
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "Stream-Id"

    iget-object p0, p1, LX/FI2;->b:Ljava/lang/String;

    invoke-direct {v2, v3, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221392
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 2221393
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2221394
    move-object v0, v0

    .line 2221395
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2221396
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2221397
    move-object v0, v0

    .line 2221398
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2221372
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2221373
    iget v0, p2, LX/1pN;->b:I

    move v0, v0

    .line 2221374
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 2221375
    new-instance v0, Lorg/apache/http/HttpException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Video segment transcoding upload failed. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/HttpException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2221376
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
