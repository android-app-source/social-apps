.class public LX/Gdr;
.super LX/An9;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "LX/An9",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;",
        "TE;>;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Gf0;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Gf0;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2374605
    invoke-direct {p0}, LX/An9;-><init>()V

    .line 2374606
    iput-object p1, p0, LX/Gdr;->a:LX/0Ot;

    .line 2374607
    return-void
.end method

.method public static a(LX/0QB;)LX/Gdr;
    .locals 4

    .prologue
    .line 2374608
    const-class v1, LX/Gdr;

    monitor-enter v1

    .line 2374609
    :try_start_0
    sget-object v0, LX/Gdr;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2374610
    sput-object v2, LX/Gdr;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2374611
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2374612
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2374613
    new-instance v3, LX/Gdr;

    const/16 p0, 0x2109

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Gdr;-><init>(LX/0Ot;)V

    .line 2374614
    move-object v0, v3

    .line 2374615
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2374616
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gdr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2374617
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2374618
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(LX/1De;Lcom/facebook/feed/rows/core/props/FeedProps;LX/3mj;Ljava/lang/Object;Ljava/lang/Object;)LX/1X1;
    .locals 6

    .prologue
    .line 2374619
    move-object v4, p4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;

    move-object v5, p5

    check-cast v5, LX/1Pr;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    .line 2374620
    iget-object p0, v0, LX/Gdr;->a:LX/0Ot;

    invoke-interface {p0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, LX/Gf0;

    const/4 p1, 0x0

    .line 2374621
    new-instance p2, LX/Gez;

    invoke-direct {p2, p0}, LX/Gez;-><init>(LX/Gf0;)V

    .line 2374622
    iget-object v0, p0, LX/Gf0;->b:LX/0Zi;

    invoke-virtual {v0}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gey;

    .line 2374623
    if-nez v0, :cond_0

    .line 2374624
    new-instance v0, LX/Gey;

    invoke-direct {v0, p0}, LX/Gey;-><init>(LX/Gf0;)V

    .line 2374625
    :cond_0
    invoke-static {v0, v1, p1, p1, p2}, LX/Gey;->a$redex0(LX/Gey;LX/1De;IILX/Gez;)V

    .line 2374626
    move-object p2, v0

    .line 2374627
    move-object p1, p2

    .line 2374628
    move-object p0, p1

    .line 2374629
    iget-object p1, p0, LX/Gey;->a:LX/Gez;

    iput-object v4, p1, LX/Gez;->a:Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;

    .line 2374630
    iget-object p1, p0, LX/Gey;->e:Ljava/util/BitSet;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Ljava/util/BitSet;->set(I)V

    .line 2374631
    move-object p0, p0

    .line 2374632
    iget-object p1, p0, LX/Gey;->a:LX/Gez;

    iput-object v2, p1, LX/Gez;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2374633
    iget-object p1, p0, LX/Gey;->e:Ljava/util/BitSet;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Ljava/util/BitSet;->set(I)V

    .line 2374634
    move-object p0, p0

    .line 2374635
    iget-object p1, p0, LX/Gey;->a:LX/Gez;

    iput-object v3, p1, LX/Gez;->c:LX/3mj;

    .line 2374636
    iget-object p1, p0, LX/Gey;->e:Ljava/util/BitSet;

    const/4 p2, 0x2

    invoke-virtual {p1, p2}, Ljava/util/BitSet;->set(I)V

    .line 2374637
    move-object p0, p0

    .line 2374638
    iget-object p1, p0, LX/Gey;->a:LX/Gez;

    iput-object v5, p1, LX/Gez;->d:LX/1Pr;

    .line 2374639
    iget-object p1, p0, LX/Gey;->e:Ljava/util/BitSet;

    const/4 p2, 0x3

    invoke-virtual {p1, p2}, Ljava/util/BitSet;->set(I)V

    .line 2374640
    move-object p0, p0

    .line 2374641
    invoke-virtual {p0}, LX/1X5;->d()LX/1X1;

    move-result-object p0

    move-object v0, p0

    .line 2374642
    return-object v0
.end method

.method public final a()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2374643
    const-class v0, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2374644
    const/16 v0, 0x8

    return v0
.end method
