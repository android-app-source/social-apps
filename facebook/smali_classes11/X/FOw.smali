.class public final enum LX/FOw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FOw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FOw;

.field public static final enum DEVICE_LOCATION_OFF:LX/FOw;

.field public static final enum DEVICE_NON_OPTIMAL_LOCATION_SETTING:LX/FOw;

.field public static final enum LOCATION_PERMISSION_OFF:LX/FOw;

.field public static final enum OKAY:LX/FOw;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2236414
    new-instance v0, LX/FOw;

    const-string v1, "OKAY"

    invoke-direct {v0, v1, v2}, LX/FOw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FOw;->OKAY:LX/FOw;

    .line 2236415
    new-instance v0, LX/FOw;

    const-string v1, "LOCATION_PERMISSION_OFF"

    invoke-direct {v0, v1, v3}, LX/FOw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FOw;->LOCATION_PERMISSION_OFF:LX/FOw;

    .line 2236416
    new-instance v0, LX/FOw;

    const-string v1, "DEVICE_LOCATION_OFF"

    invoke-direct {v0, v1, v4}, LX/FOw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FOw;->DEVICE_LOCATION_OFF:LX/FOw;

    .line 2236417
    new-instance v0, LX/FOw;

    const-string v1, "DEVICE_NON_OPTIMAL_LOCATION_SETTING"

    invoke-direct {v0, v1, v5}, LX/FOw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FOw;->DEVICE_NON_OPTIMAL_LOCATION_SETTING:LX/FOw;

    .line 2236418
    const/4 v0, 0x4

    new-array v0, v0, [LX/FOw;

    sget-object v1, LX/FOw;->OKAY:LX/FOw;

    aput-object v1, v0, v2

    sget-object v1, LX/FOw;->LOCATION_PERMISSION_OFF:LX/FOw;

    aput-object v1, v0, v3

    sget-object v1, LX/FOw;->DEVICE_LOCATION_OFF:LX/FOw;

    aput-object v1, v0, v4

    sget-object v1, LX/FOw;->DEVICE_NON_OPTIMAL_LOCATION_SETTING:LX/FOw;

    aput-object v1, v0, v5

    sput-object v0, LX/FOw;->$VALUES:[LX/FOw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2236419
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FOw;
    .locals 1

    .prologue
    .line 2236420
    const-class v0, LX/FOw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FOw;

    return-object v0
.end method

.method public static values()[LX/FOw;
    .locals 1

    .prologue
    .line 2236421
    sget-object v0, LX/FOw;->$VALUES:[LX/FOw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FOw;

    return-object v0
.end method
