.class public final LX/GNe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;)V
    .locals 0

    .prologue
    .line 2346341
    iput-object p1, p0, LX/GNe;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2346342
    iget-object v0, p0, LX/GNe;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->e:LX/03V;

    sget-object v1, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->k:Ljava/lang/String;

    const-string v2, "Failed to ad experience."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2346343
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2346344
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2346345
    iget-object v1, p0, LX/GNe;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    if-nez p1, :cond_0

    const/4 v0, 0x0

    .line 2346346
    :goto_0
    iput-object v0, v1, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->l:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

    .line 2346347
    iget-object v0, p0, LX/GNe;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->l:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/GNe;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->l:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->m()Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2346348
    iget-object v0, p0, LX/GNe;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->l:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->k()Lcom/facebook/graphql/enums/GraphQLAdsExperienceStatusEnum;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLAdsExperienceStatusEnum;->ACTIVE:Lcom/facebook/graphql/enums/GraphQLAdsExperienceStatusEnum;

    if-ne v0, v1, :cond_1

    .line 2346349
    iget-object v0, p0, LX/GNe;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    invoke-static {v0}, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->e(Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;)V

    .line 2346350
    iget-object v0, p0, LX/GNe;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->m:Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;

    iget-object v1, p0, LX/GNe;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->l:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->m()Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->setConfirmationTextForActiveSharedAds(Ljava/lang/String;)V

    .line 2346351
    :goto_1
    iget-object v0, p0, LX/GNe;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->l:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->m()Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2346352
    iget-object v2, p0, LX/GNe;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v2, v2, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->m:Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->setProfilePicture(Landroid/net/Uri;)V

    .line 2346353
    :goto_2
    return-void

    .line 2346354
    :cond_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2346355
    check-cast v0, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

    goto :goto_0

    .line 2346356
    :cond_1
    iget-object v0, p0, LX/GNe;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->m:Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;

    iget-object v1, p0, LX/GNe;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->l:Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;

    invoke-virtual {v1}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel;->m()Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adsexperiencetool/protocol/FetchAdsExperienceQueryModels$AdsExperienceFragmentModel$PageModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adsexperiencetool/ui/AdsInjectIntroView;->setConfirmationTextForInactiveSharedAds(Ljava/lang/String;)V

    goto :goto_1

    .line 2346357
    :cond_2
    iget-object v0, p0, LX/GNe;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    invoke-static {v0}, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->k(Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;)V

    goto :goto_2
.end method
