.class public LX/FXz;
.super LX/FXy;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FZ3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/FZ3;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2255630
    invoke-direct {p0}, LX/FXy;-><init>()V

    .line 2255631
    iput-object p1, p0, LX/FXz;->a:LX/0Ot;

    .line 2255632
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2255642
    const v0, 0x7f020788

    return v0
.end method

.method public final a(LX/BO1;)LX/FXy;
    .locals 2

    .prologue
    .line 2255637
    invoke-interface {p1}, LX/BO1;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLSavedState;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, LX/BO1;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 2255638
    :goto_0
    iput-boolean v0, p0, LX/FXy;->a:Z

    .line 2255639
    return-object p0

    .line 2255640
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2255641
    const v0, 0x7f081ac9

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1
    .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
    .end annotation

    .prologue
    .line 2255636
    const-string v0, "archive_button"

    return-object v0
.end method

.method public final b(LX/BO1;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 2255633
    iget-object v1, p0, LX/FXz;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FZ3;

    invoke-interface {p1}, LX/AUV;->d()J

    move-result-wide v2

    invoke-interface {p1}, LX/BO1;->f()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, LX/BO1;->W()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p1}, LX/BO1;->X()I

    move-result v6

    if-ne v6, v0, :cond_0

    move v6, v0

    :goto_0
    invoke-virtual {p0}, LX/FXz;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {v1 .. v7}, LX/FZ3;->a(JLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 2255634
    return v0

    .line 2255635
    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method
