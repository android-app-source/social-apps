.class public LX/FMD;
.super LX/Dny;
.source ""


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/34G;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sms/SmsThreadManager;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMB;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FN1;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/30m;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMd;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Lx;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2229244
    const-string v0, "SmsServiceHandler"

    invoke-direct {p0, v0}, LX/Dny;-><init>(Ljava/lang/String;)V

    .line 2229245
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2229246
    iput-object v0, p0, LX/FMD;->b:LX/0Ot;

    .line 2229247
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2229248
    iput-object v0, p0, LX/FMD;->c:LX/0Ot;

    .line 2229249
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2229250
    iput-object v0, p0, LX/FMD;->d:LX/0Ot;

    .line 2229251
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2229252
    iput-object v0, p0, LX/FMD;->e:LX/0Ot;

    .line 2229253
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2229254
    iput-object v0, p0, LX/FMD;->f:LX/0Ot;

    .line 2229255
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2229256
    iput-object v0, p0, LX/FMD;->g:LX/0Ot;

    .line 2229257
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2229258
    iput-object v0, p0, LX/FMD;->h:LX/0Ot;

    .line 2229259
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2229260
    iput-object v0, p0, LX/FMD;->i:LX/0Ot;

    .line 2229261
    return-void
.end method

.method private a(JIJ)Lcom/facebook/messaging/model/messages/MessagesCollection;
    .locals 7

    .prologue
    .line 2229171
    iget-object v0, p0, LX/FMD;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMB;

    move-wide v1, p1

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, LX/FMB;->a(JIJ)LX/0Px;

    move-result-object v1

    .line 2229172
    new-instance v2, Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-static {p1, p2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v3

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    if-ge v0, p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v2, v3, v1, v0}, Lcom/facebook/messaging/model/messages/MessagesCollection;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;Z)V

    return-object v2

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(JI)Lcom/facebook/messaging/service/model/FetchThreadResult;
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 2229173
    const-wide/16 v4, -0x1

    move-object v0, p0

    move-wide v1, p1

    move v3, p3

    invoke-direct/range {v0 .. v5}, LX/FMD;->a(JIJ)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v3

    .line 2229174
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 2229175
    iget-object v0, p0, LX/FMD;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/SmsThreadManager;

    invoke-virtual {v0, p1, p2, v4, v6}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(JLjava/util/Map;LX/6ek;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v2

    .line 2229176
    invoke-virtual {v3}, Lcom/facebook/messaging/model/messages/MessagesCollection;->c()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v5

    .line 2229177
    if-eqz v5, :cond_4

    .line 2229178
    iget-object v1, v5, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    .line 2229179
    invoke-static {v5}, LX/2Mk;->o(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 2229180
    :goto_0
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object v2

    .line 2229181
    iput-object v1, v2, LX/6g6;->o:Ljava/lang/String;

    .line 2229182
    move-object v1, v2

    .line 2229183
    iput-object v0, v1, LX/6g6;->q:Ljava/lang/String;

    .line 2229184
    move-object v0, v1

    .line 2229185
    iget-wide v6, v5, Lcom/facebook/messaging/model/messages/Message;->g:J

    .line 2229186
    iput-wide v6, v0, LX/6g6;->c:J

    .line 2229187
    move-object v0, v0

    .line 2229188
    iget-wide v6, v5, Lcom/facebook/messaging/model/messages/Message;->g:J

    .line 2229189
    iput-wide v6, v0, LX/6g6;->f:J

    .line 2229190
    move-object v0, v0

    .line 2229191
    iget-object v1, v5, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2229192
    iput-object v1, v0, LX/6g6;->p:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2229193
    move-object v1, v0

    .line 2229194
    sget-object v0, Lcom/facebook/messaging/model/send/SendError;->a:Lcom/facebook/messaging/model/send/SendError;

    iget-object v2, v5, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    invoke-virtual {v0, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 2229195
    :goto_1
    iput-boolean v0, v1, LX/6g6;->z:Z

    .line 2229196
    move-object v0, v1

    .line 2229197
    invoke-virtual {v0}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v0

    move-object v1, v0

    .line 2229198
    :goto_2
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadResult;->a()LX/6iO;

    move-result-object v0

    sget-object v2, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_LOCAL_DISK_CACHE_UP_TO_DATE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2229199
    iput-object v2, v0, LX/6iO;->b:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2229200
    move-object v2, v0

    .line 2229201
    iget-object v0, p0, LX/FMD;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    .line 2229202
    iput-wide v6, v2, LX/6iO;->g:J

    .line 2229203
    move-object v0, v2

    .line 2229204
    iput-object v1, v0, LX/6iO;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2229205
    move-object v0, v0

    .line 2229206
    iput-object v3, v0, LX/6iO;->d:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2229207
    move-object v0, v0

    .line 2229208
    invoke-interface {v4}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v1

    .line 2229209
    iput-object v1, v0, LX/6iO;->e:LX/0Px;

    .line 2229210
    move-object v0, v0

    .line 2229211
    invoke-virtual {v0}, LX/6iO;->a()Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    return-object v0

    .line 2229212
    :cond_0
    iget-object v0, p0, LX/FMD;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/34G;

    invoke-virtual {v0, v5}, LX/34G;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2229213
    iget-object v0, p0, LX/FMD;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/34G;

    iget-object v6, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    invoke-virtual {v0, v5, v6}, LX/34G;->a(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2229214
    :cond_1
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2229215
    iget-object v0, p0, LX/FMD;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/34G;

    iget-object v6, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->F:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    invoke-virtual {v0, v5, v6}, LX/34G;->b(Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/threads/ThreadCustomization;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2229216
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move-object v0, v6

    goto/16 :goto_0

    :cond_4
    move-object v1, v2

    goto :goto_2
.end method

.method private a(Lcom/facebook/messaging/model/messages/Message;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    .line 2229217
    iget-object v0, p0, LX/FMD;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/SmsThreadManager;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/facebook/messaging/sms/SmsThreadManager;->b(J)I

    move-result v5

    .line 2229218
    iget-object v0, p0, LX/FMD;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-static {p1}, LX/30m;->a(Lcom/facebook/messaging/model/messages/Message;)Ljava/lang/String;

    move-result-object v3

    .line 2229219
    if-gt v5, v1, :cond_0

    if-eqz v3, :cond_5

    .line 2229220
    :cond_0
    :goto_0
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2229221
    const/4 v7, 0x0

    .line 2229222
    iget-object v2, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v2}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v2

    sget-object v4, LX/0XG;->EMAIL:LX/0XG;

    if-ne v2, v4, :cond_6

    .line 2229223
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->h()Ljava/lang/String;

    move-result-object v6

    .line 2229224
    :cond_1
    :goto_1
    iget-object v0, p0, LX/FMD;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/30m;

    iget-object v2, p1, Lcom/facebook/messaging/model/messages/Message;->L:Lcom/facebook/messaging/model/mms/MmsData;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/mms/MmsData;->a()Z

    move-result v2

    iget-object v4, p1, Lcom/facebook/messaging/model/messages/Message;->L:Lcom/facebook/messaging/model/mms/MmsData;

    iget-object v4, v4, Lcom/facebook/messaging/model/mms/MmsData;->d:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    .line 2229225
    const-string v8, "sms_takeover_message_received"

    invoke-static {v0, v8}, LX/30m;->c(LX/30m;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v8

    .line 2229226
    if-eqz v1, :cond_2

    .line 2229227
    const-string p0, "is_pending_download"

    invoke-virtual {v8, p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2229228
    :cond_2
    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_3

    .line 2229229
    invoke-static {v6}, LX/3MF;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 2229230
    const-string p1, "sender_address"

    invoke-virtual {v8, p1, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2229231
    :cond_3
    invoke-static {v7}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p0

    if-nez p0, :cond_4

    .line 2229232
    const-string p0, "phone_number_type"

    invoke-virtual {v8, p0, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2229233
    :cond_4
    invoke-static {v8, v1, v3, v4, v5}, LX/30m;->a(Lcom/facebook/analytics/logger/HoneyClientEvent;ZLjava/lang/String;II)V

    .line 2229234
    invoke-static {v0, v8}, LX/30m;->a(LX/30m;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2229235
    return-void

    .line 2229236
    :cond_5
    const/4 v1, 0x0

    goto :goto_0

    .line 2229237
    :cond_6
    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->g()Ljava/lang/String;

    move-result-object v6

    .line 2229238
    invoke-static {v6}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2229239
    iget-object v0, p0, LX/FMD;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Lx;

    invoke-virtual {v0, v6}, LX/3Lx;->a(Ljava/lang/String;)LX/4hT;

    move-result-object v2

    .line 2229240
    if-eqz v2, :cond_1

    .line 2229241
    iget-object v0, p0, LX/FMD;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Lx;

    .line 2229242
    iget-object v4, v0, LX/3Lx;->b:LX/3Lz;

    invoke-virtual {v4, v2}, LX/3Lz;->getNumberType(LX/4hT;)LX/4hH;

    move-result-object v4

    invoke-virtual {v4}, LX/4hH;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v7, v4

    .line 2229243
    goto :goto_1
.end method

.method public static b(LX/0QB;)LX/FMD;
    .locals 10

    .prologue
    .line 2229274
    new-instance v0, LX/FMD;

    invoke-direct {v0}, LX/FMD;-><init>()V

    .line 2229275
    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    const/16 v2, 0xd70

    invoke-static {p0, v2}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0xd9c

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x297c

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x299f

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x2e3

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xda0

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x298b

    invoke-static {p0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x123d

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    .line 2229276
    iput-object v1, v0, LX/FMD;->a:Landroid/content/Context;

    iput-object v2, v0, LX/FMD;->b:LX/0Ot;

    iput-object v3, v0, LX/FMD;->c:LX/0Ot;

    iput-object v4, v0, LX/FMD;->d:LX/0Ot;

    iput-object v5, v0, LX/FMD;->e:LX/0Ot;

    iput-object v6, v0, LX/FMD;->f:LX/0Ot;

    iput-object v7, v0, LX/FMD;->g:LX/0Ot;

    iput-object v8, v0, LX/FMD;->h:LX/0Ot;

    iput-object v9, v0, LX/FMD;->i:LX/0Ot;

    .line 2229277
    return-object v0
.end method


# virtual methods
.method public final E(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 12

    .prologue
    .line 2229262
    const-string v2, "SmsServiceHandler.handleReceivedSms"

    const v3, 0x39746739

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2229263
    :try_start_0
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v11, v0

    .line 2229264
    const-string v2, "message"

    invoke-virtual {v11, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    move-object v10, v0

    .line 2229265
    const-string v2, "is_readonly_mode"

    const/4 v3, 0x0

    invoke-virtual {v11, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2229266
    invoke-direct {p0, v10}, LX/FMD;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2229267
    :cond_0
    iget-object v2, v10, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-wide v3, v2, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    const/16 v5, 0x14

    const-wide/16 v6, -0x1

    move-object v2, p0

    invoke-direct/range {v2 .. v7}, LX/FMD;->a(JIJ)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v6

    .line 2229268
    new-instance v3, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v4, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const/4 v7, 0x0

    iget-object v2, p0, LX/FMD;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v8

    move-object v5, v10

    invoke-direct/range {v3 .. v9}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2229269
    const-string v2, "is_class_zero"

    const/4 v4, 0x0

    invoke-virtual {v11, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2229270
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2229271
    iput-object v2, v3, Lcom/facebook/messaging/service/model/NewMessageResult;->d:Ljava/lang/Boolean;

    .line 2229272
    :cond_1
    invoke-static {v3}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 2229273
    const v3, -0x52c40391

    invoke-static {v3}, LX/02m;->a(I)V

    return-object v2

    :catchall_0
    move-exception v2

    const v3, -0xd300b95

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2
.end method

.method public final O(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 7

    .prologue
    .line 2229315
    iget-object v0, p0, LX/FMD;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/SmsThreadManager;

    invoke-virtual {v0}, Lcom/facebook/messaging/sms/SmsThreadManager;->a()LX/0Px;

    move-result-object v1

    .line 2229316
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->newBuilder()LX/6hv;

    move-result-object v0

    const/4 v2, 0x1

    .line 2229317
    iput-boolean v2, v0, LX/6hv;->b:Z

    .line 2229318
    move-object v2, v0

    .line 2229319
    iget-object v0, p0, LX/FMD;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    .line 2229320
    iput-wide v4, v2, LX/6hv;->d:J

    .line 2229321
    move-object v0, v2

    .line 2229322
    iput-object v1, v0, LX/6hv;->a:Ljava/util/List;

    .line 2229323
    move-object v0, v0

    .line 2229324
    invoke-virtual {v0}, LX/6hv;->e()Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    move-result-object v0

    .line 2229325
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 11

    .prologue
    .line 2229278
    const-string v0, "SmsServiceHandler.handleFetchThreadList"

    const v1, -0x76c0272e

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2229279
    :try_start_0
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2229280
    const-string v1, "fetchThreadListParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;

    .line 2229281
    iget-object v1, p0, LX/FMD;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/sms/SmsThreadManager;

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2229282
    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v2, v2

    .line 2229283
    sget-object v3, LX/6ek;->INBOX:LX/6ek;

    if-eq v2, v3, :cond_0

    .line 2229284
    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v2, v2

    .line 2229285
    sget-object v3, LX/6ek;->SMS_SPAM:LX/6ek;

    if-eq v2, v3, :cond_0

    .line 2229286
    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v2, v2

    .line 2229287
    sget-object v3, LX/6ek;->SMS_BUSINESS:LX/6ek;

    if-ne v2, v3, :cond_2

    :cond_0
    move v2, v9

    :goto_0
    const-string v3, "SMS Threads can only belong to inbox or spam or business folder"

    invoke-static {v2, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2229288
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 2229289
    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v3, v2

    .line 2229290
    invoke-virtual {v0}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->g()I

    move-result v4

    const-wide/16 v6, -0x1

    move-object v2, v1

    invoke-static/range {v2 .. v7}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(Lcom/facebook/messaging/sms/SmsThreadManager;LX/6ek;ILjava/util/Map;J)Ljava/util/List;

    move-result-object v2

    .line 2229291
    new-instance v3, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->g()I

    move-result v6

    if-ge v2, v6, :cond_3

    :goto_1
    invoke-direct {v3, v4, v9}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    .line 2229292
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListResult;->newBuilder()LX/6iK;

    move-result-object v2

    .line 2229293
    iput-object v3, v2, LX/6iK;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2229294
    move-object v2, v2

    .line 2229295
    sget-object v3, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_SMS:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2229296
    iput-object v3, v2, LX/6iK;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2229297
    move-object v3, v2

    .line 2229298
    iget-object v2, v1, Lcom/facebook/messaging/sms/SmsThreadManager;->o:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v6

    .line 2229299
    iput-wide v6, v3, LX/6iK;->j:J

    .line 2229300
    move-object v2, v3

    .line 2229301
    iget-object v3, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v3, v3

    .line 2229302
    iput-object v3, v2, LX/6iK;->b:LX/6ek;

    .line 2229303
    move-object v2, v2

    .line 2229304
    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-static {v3}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    .line 2229305
    iput-object v3, v2, LX/6iK;->d:Ljava/util/List;

    .line 2229306
    move-object v2, v2

    .line 2229307
    invoke-virtual {v2}, LX/6iK;->m()Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v3

    .line 2229308
    iget-object v2, v1, Lcom/facebook/messaging/sms/SmsThreadManager;->l:LX/2Oq;

    invoke-virtual {v2}, LX/2Oq;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2229309
    iget-object v2, v1, Lcom/facebook/messaging/sms/SmsThreadManager;->w:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Handler;

    iget-object v4, v1, Lcom/facebook/messaging/sms/SmsThreadManager;->j:Ljava/lang/Runnable;

    const v5, 0x9a51c71

    invoke-static {v2, v4, v5}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2229310
    :cond_1
    move-object v0, v3

    .line 2229311
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2229312
    const v1, -0x3865a8ac

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, 0x3e9999f0    # 0.30000257f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_2
    move v2, v8

    .line 2229313
    goto/16 :goto_0

    :cond_3
    move v9, v8

    .line 2229314
    goto :goto_1
.end method

.method public final b(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 13

    .prologue
    .line 2229139
    const-string v0, "SmsServiceHandler.handleFetchMoreThreads"

    const v1, -0x537e9981

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2229140
    :try_start_0
    iget-object v0, p0, LX/FMD;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/SmsThreadManager;

    const-string v1, "fetchMoreThreadsParams"

    invoke-static {p1, v1}, LX/4BJ;->getParamValue(LX/1qK;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2229141
    iget-object v2, v1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->a:LX/6ek;

    move-object v2, v2

    .line 2229142
    sget-object v3, LX/6ek;->INBOX:LX/6ek;

    if-eq v2, v3, :cond_0

    .line 2229143
    iget-object v2, v1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->a:LX/6ek;

    move-object v2, v2

    .line 2229144
    sget-object v3, LX/6ek;->SMS_SPAM:LX/6ek;

    if-eq v2, v3, :cond_0

    .line 2229145
    iget-object v2, v1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->a:LX/6ek;

    move-object v2, v2

    .line 2229146
    sget-object v3, LX/6ek;->SMS_BUSINESS:LX/6ek;

    if-ne v2, v3, :cond_1

    :cond_0
    move v2, v9

    :goto_0
    const-string v3, "SMS Threads can only belong in the inbox or recent sms spam threads or sms business threads folder"

    invoke-static {v2, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2229147
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 2229148
    iget-object v2, v1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->a:LX/6ek;

    move-object v3, v2

    .line 2229149
    iget v2, v1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->e:I

    move v4, v2

    .line 2229150
    iget-wide v11, v1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->g:J

    move-wide v6, v11

    .line 2229151
    move-object v2, v0

    invoke-static/range {v2 .. v7}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(Lcom/facebook/messaging/sms/SmsThreadManager;LX/6ek;ILjava/util/Map;J)Ljava/util/List;

    move-result-object v2

    .line 2229152
    new-instance v6, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 2229153
    iget v4, v1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->e:I

    move v4, v4

    .line 2229154
    if-eq v2, v4, :cond_2

    :goto_1
    invoke-direct {v6, v3, v9}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    .line 2229155
    new-instance v3, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;

    sget-object v4, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_SMS:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2229156
    iget-object v2, v1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->a:LX/6ek;

    move-object v10, v2

    .line 2229157
    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v7

    iget-object v2, v0, Lcom/facebook/messaging/sms/SmsThreadManager;->o:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v8

    move-object v5, v10

    invoke-direct/range {v3 .. v9}, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;-><init>(Lcom/facebook/fbservice/results/DataFetchDisposition;LX/6ek;Lcom/facebook/messaging/model/threads/ThreadsCollection;LX/0Px;J)V

    move-object v0, v3

    .line 2229158
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2229159
    const v1, 0x74510eb8

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x7e03122a

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_1
    move v2, v8

    .line 2229160
    goto :goto_0

    :cond_2
    move v9, v8

    .line 2229161
    goto :goto_1
.end method

.method public final c(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2229162
    const-string v0, "SmsServiceHandler.handleFetchThread"

    const v1, 0x142c3e44

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2229163
    :try_start_0
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2229164
    const-string v1, "fetchThreadParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadParams;

    .line 2229165
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadParams;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-object v1, v1

    .line 2229166
    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a()Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    .line 2229167
    iget v1, v0, Lcom/facebook/messaging/service/model/FetchThreadParams;->g:I

    move v0, v1

    .line 2229168
    invoke-direct {p0, v2, v3, v0}, LX/FMD;->a(JI)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2229169
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2229170
    const v1, 0x7c898523

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, 0x6e272504

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final d(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 2228939
    const-string v0, "SmsServiceHandler.handleFetchThreadKeyByParticipants"

    const v1, -0x54d6f5dc

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2228940
    :try_start_0
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2228941
    const-string v1, "fetch_thread_with_participants_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;

    .line 2228942
    const/4 v1, 0x1

    .line 2228943
    new-instance v2, Ljava/util/HashSet;

    .line 2228944
    iget-object v3, v0, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;->b:LX/0Rf;

    move-object v3, v3

    .line 2228945
    invoke-virtual {v3}, LX/0Rf;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(I)V

    .line 2228946
    iget-object v3, v0, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;->a:Lcom/facebook/user/model/UserKey;

    move-object v3, v3

    .line 2228947
    iget-object v4, v0, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;->b:LX/0Rf;

    move-object v0, v4

    .line 2228948
    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserKey;

    .line 2228949
    invoke-virtual {v0, v3}, Lcom/facebook/user/model/UserKey;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2228950
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->e()Z

    move-result v5

    if-nez v5, :cond_1

    .line 2228951
    const/4 v0, 0x0

    .line 2228952
    :goto_1
    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2228953
    iget-object v0, p0, LX/FMD;->a:Landroid/content/Context;

    invoke-static {v0, v2}, LX/5e6;->a(Landroid/content/Context;Ljava/util/Set;)J

    move-result-wide v0

    .line 2228954
    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2228955
    new-instance v1, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsResult;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsResult;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2228956
    const v1, 0x207c63b7

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_2
    return-object v0

    .line 2228957
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Lcom/facebook/user/model/UserKey;->i()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2228958
    :catchall_0
    move-exception v0

    const v1, -0x7bab63ef

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2228959
    :cond_2
    :try_start_2
    sget-object v0, LX/1nY;->OTHER:LX/1nY;

    const-string v1, "Unable to get or create thread key"

    invoke-static {v0, v1}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 2228960
    const v1, -0x1330cb8c

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final f(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 2228961
    const-string v0, "SmsServiceHandler.handleCreateGroup"

    const v1, -0x74f91c13

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2228962
    :try_start_0
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2228963
    const-string v1, "createGroupParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/CreateGroupParams;

    .line 2228964
    iget-object v1, v0, Lcom/facebook/messaging/service/model/CreateGroupParams;->c:LX/0Px;

    move-object v2, v1

    .line 2228965
    new-instance v3, Ljava/util/HashSet;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 2228966
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2228967
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2228968
    new-instance v5, Lcom/facebook/user/model/UserFbidIdentifier;

    invoke-direct {v5, v0}, Lcom/facebook/user/model/UserFbidIdentifier;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2228969
    :catchall_0
    move-exception v0

    const v1, 0x74cd50d5

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2228970
    :cond_0
    :try_start_1
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserIdentifier;

    .line 2228971
    invoke-interface {v0}, Lcom/facebook/user/model/UserIdentifier;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2228972
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2228973
    :cond_1
    iget-object v0, p0, LX/FMD;->a:Landroid/content/Context;

    invoke-static {v0, v3}, LX/5e6;->a(Landroid/content/Context;Ljava/util/Set;)J

    move-result-wide v0

    .line 2228974
    const/16 v2, 0x14

    invoke-direct {p0, v0, v1, v2}, LX/FMD;->a(JI)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2228975
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2228976
    const v1, 0x37bcbbb9

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0
.end method

.method public final g(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2228977
    const-string v0, "SmsServiceHandler.handleCreateThread"

    const v2, 0x5d381d9f

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2228978
    :try_start_0
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2228979
    const-string v2, "createThreadParams"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;

    .line 2228980
    new-instance v3, Ljava/util/HashSet;

    .line 2228981
    iget-object v2, v0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->c:LX/0Px;

    move-object v2, v2

    .line 2228982
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-direct {v3, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 2228983
    iget-object v2, v0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->c:LX/0Px;

    move-object v4, v2

    .line 2228984
    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/UserIdentifier;

    .line 2228985
    check-cast v1, Lcom/facebook/user/model/UserSmsIdentifier;

    .line 2228986
    iget-object p1, v1, Lcom/facebook/user/model/UserSmsIdentifier;->b:Ljava/lang/String;

    move-object v1, p1

    .line 2228987
    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2228988
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2228989
    :cond_0
    iget-object v1, p0, LX/FMD;->a:Landroid/content/Context;

    invoke-static {v1, v3}, LX/5e6;->a(Landroid/content/Context;Ljava/util/Set;)J

    move-result-wide v2

    .line 2228990
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v1

    .line 2228991
    iget-object v4, v0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->b:Lcom/facebook/messaging/model/messages/Message;

    move-object v0, v4

    .line 2228992
    invoke-virtual {v1, v0}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v0

    invoke-static {v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v1

    .line 2228993
    iput-object v1, v0, LX/6f7;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2228994
    move-object v0, v0

    .line 2228995
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 2228996
    iget-object v0, p0, LX/FMD;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FN1;

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, LX/FN1;->a(Lcom/facebook/messaging/model/messages/Message;Z)Ljava/lang/String;

    .line 2228997
    const/16 v0, 0x14

    invoke-direct {p0, v2, v3, v0}, LX/FMD;->a(JI)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2228998
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2228999
    const v1, -0x37fdf8b1

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x1f515aba

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final h(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 10

    .prologue
    .line 2229000
    const-string v0, "SmsServiceHandler.handleFetchMoreMessages"

    const v1, -0x7cedd1

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2229001
    :try_start_0
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2229002
    const-string v1, "fetchMoreMessagesParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;

    .line 2229003
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v1, v1

    .line 2229004
    iget-wide v8, v0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->c:J

    move-wide v4, v8

    .line 2229005
    iget v2, v0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->d:I

    move v3, v2

    .line 2229006
    new-instance v6, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;

    sget-object v7, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_LOCAL_DISK_CACHE_UP_TO_DATE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    iget-wide v1, v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, LX/FMD;->a(JIJ)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v1

    iget-object v0, p0, LX/FMD;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    invoke-direct {v6, v7, v1, v2, v3}, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;-><init>(Lcom/facebook/fbservice/results/DataFetchDisposition;Lcom/facebook/messaging/model/messages/MessagesCollection;J)V

    .line 2229007
    invoke-static {v6}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2229008
    const v1, 0x5086bb43

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x4ea4a58f

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final j(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 2229009
    const-string v0, "SmsServiceHandler.handleMarkThreads"

    const v1, -0x74a299b5    # -4.2640008E-32f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2229010
    :try_start_0
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2229011
    const-string v1, "markThreadsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;

    .line 2229012
    sget-object v1, LX/6iW;->READ:LX/6iW;

    iget-object v3, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->a:LX/6iW;

    invoke-virtual {v1, v3}, LX/6iW;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2229013
    iget-object v1, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_2

    .line 2229014
    iget-object v0, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/MarkThreadFields;

    .line 2229015
    iget-boolean v1, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->b:Z

    if-eqz v1, :cond_1

    .line 2229016
    iget-object v1, p0, LX/FMD;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/sms/SmsThreadManager;

    iget-object v0, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/facebook/messaging/sms/SmsThreadManager;->c(J)V

    .line 2229017
    :cond_0
    :goto_0
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2229018
    const v1, -0x2af3c1f2

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    .line 2229019
    :cond_1
    :try_start_1
    iget-object v1, p0, LX/FMD;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/sms/SmsThreadManager;

    iget-object v0, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/messaging/sms/SmsThreadManager;->b(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2229020
    :catchall_0
    move-exception v0

    const v1, 0x140d93ca

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2229021
    :cond_2
    :try_start_2
    new-instance v4, Ljava/util/HashSet;

    iget-object v1, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-direct {v4, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 2229022
    iget-object v5, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_4

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/service/model/MarkThreadFields;

    .line 2229023
    iget-boolean v7, v1, Lcom/facebook/messaging/service/model/MarkThreadFields;->b:Z

    if-eqz v7, :cond_3

    .line 2229024
    iget-object v1, v1, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2229025
    :cond_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 2229026
    :cond_4
    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    .line 2229027
    iget-object v1, p0, LX/FMD;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/sms/SmsThreadManager;

    invoke-virtual {v1, v4}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(Ljava/util/Collection;)V

    .line 2229028
    invoke-interface {v4}, Ljava/util/Set;->clear()V

    .line 2229029
    :cond_5
    iget-object v3, v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;->c:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_2
    if-ge v1, v5, :cond_7

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/MarkThreadFields;

    .line 2229030
    iget-boolean v2, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->b:Z

    if-nez v2, :cond_6

    .line 2229031
    iget-object v0, v0, Lcom/facebook/messaging/service/model/MarkThreadFields;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2229032
    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2229033
    :cond_7
    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2229034
    iget-object v0, p0, LX/FMD;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/SmsThreadManager;

    invoke-virtual {v0, v4}, Lcom/facebook/messaging/sms/SmsThreadManager;->b(Ljava/util/Collection;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public final l(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2229129
    const-string v0, "SmsServiceHandler.handleDeleteThreads"

    const v1, 0x485bd0bf

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2229130
    :try_start_0
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2229131
    const-string v1, "deleteThreadsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/DeleteThreadsParams;

    .line 2229132
    iget-object v1, v0, Lcom/facebook/messaging/service/model/DeleteThreadsParams;->a:LX/0Px;

    move-object v1, v1

    .line 2229133
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2229134
    iget-object v1, v0, Lcom/facebook/messaging/service/model/DeleteThreadsParams;->a:LX/0Px;

    move-object v0, v1

    .line 2229135
    new-instance v1, LX/FMC;

    invoke-direct {v1, p0}, LX/FMC;-><init>(LX/FMD;)V

    invoke-static {v0, v1}, LX/0R9;->a(Ljava/util/List;LX/0QK;)Ljava/util/List;

    move-result-object v1

    .line 2229136
    iget-object v0, p0, LX/FMD;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/SmsThreadManager;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v2}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(Ljava/util/Set;)V

    .line 2229137
    :cond_0
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2229138
    const v1, -0x1a1f461b

    invoke-static {v1}, LX/02m;->a(I)V

    return-object v0

    :catchall_0
    move-exception v0

    const v1, -0x43f767cc

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final n(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 12

    .prologue
    const/4 v4, 0x0

    const/4 v11, 0x1

    const/4 v8, 0x0

    .line 2229035
    const-string v2, "SmsServiceHandler.handleDeleteMessages"

    const v3, -0x21a83754

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2229036
    :try_start_0
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v2, v0

    .line 2229037
    const-string v3, "deleteMessagesParams"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/facebook/messaging/service/model/DeleteMessagesParams;

    move-object v9, v0

    .line 2229038
    iget-object v2, v9, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->b:LX/0Rf;

    invoke-virtual {v2}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2229039
    const-string v3, "sent."

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2229040
    iget-object v3, p0, LX/FMD;->h:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/FMd;

    const/4 v6, 0x5

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, LX/FMd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 2229041
    :goto_1
    iget-object v2, p0, LX/FMD;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/sms/SmsThreadManager;

    const/4 v6, 0x0

    .line 2229042
    invoke-static {v3}, LX/FMQ;->d(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v3}, LX/FMQ;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_0
    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2229043
    invoke-static {v3}, LX/FMQ;->c(Ljava/lang/String;)Z

    move-result v0

    .line 2229044
    if-eqz v0, :cond_7

    invoke-static {v3}, LX/FMQ;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2229045
    :goto_3
    iget-object v1, v2, Lcom/facebook/messaging/sms/SmsThreadManager;->k:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, v6, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2229046
    goto :goto_0

    .line 2229047
    :catchall_0
    move-exception v2

    const v3, 0x11329d56

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2

    .line 2229048
    :cond_1
    :try_start_1
    iget-object v2, v9, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v2, :cond_4

    .line 2229049
    iget-object v2, p0, LX/FMD;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/sms/SmsThreadManager;

    iget-object v3, v9, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v4

    const/4 v3, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v4, v5, v3, v6}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(JLjava/util/Map;LX/6ek;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v10

    .line 2229050
    iget-object v2, p0, LX/FMD;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FMB;

    iget-object v3, v9, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v3

    const/4 v5, 0x1

    const-wide/16 v6, -0x1

    invoke-virtual/range {v2 .. v7}, LX/FMB;->a(JIJ)LX/0Px;

    move-result-object v2

    .line 2229051
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2229052
    iget-object v2, p0, LX/FMD;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/sms/SmsThreadManager;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(Z)V

    move v8, v11

    .line 2229053
    :cond_2
    if-nez v10, :cond_3

    if-eqz v8, :cond_3

    .line 2229054
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v2

    sget-object v3, LX/6ek;->INBOX:LX/6ek;

    .line 2229055
    iput-object v3, v2, LX/6g6;->A:LX/6ek;

    .line 2229056
    move-object v2, v2

    .line 2229057
    iget-object v3, v9, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2229058
    iput-object v3, v2, LX/6g6;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2229059
    move-object v2, v2

    .line 2229060
    invoke-virtual {v2}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v3

    .line 2229061
    :goto_4
    new-instance v2, Lcom/facebook/messaging/service/model/DeleteMessagesResult;

    iget-object v4, v9, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v5, v9, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->b:LX/0Rf;

    new-instance v6, Ljava/util/HashMap;

    const/4 v7, 0x0

    invoke-direct {v6, v7}, Ljava/util/HashMap;-><init>(I)V

    new-instance v7, Ljava/util/HashSet;

    const/4 v9, 0x0

    invoke-direct {v7, v9}, Ljava/util/HashSet;-><init>(I)V

    invoke-direct/range {v2 .. v8}, Lcom/facebook/messaging/service/model/DeleteMessagesResult;-><init>(Lcom/facebook/messaging/model/threads/ThreadSummary;Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/util/Set;Ljava/util/Map;Ljava/util/Set;Z)V

    .line 2229062
    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 2229063
    const v3, 0x5d530f5d

    invoke-static {v3}, LX/02m;->a(I)V

    return-object v2

    :cond_3
    move-object v3, v10

    goto :goto_4

    :cond_4
    move-object v3, v4

    goto :goto_4

    :cond_5
    move-object v3, v2

    goto/16 :goto_1

    .line 2229064
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 2229065
    :cond_7
    invoke-static {v3}, LX/FMQ;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_3
.end method

.method public final o(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 2229066
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v2, v0

    .line 2229067
    const-string v0, "modifyThreadParams"

    invoke-static {p1, v0}, LX/4BJ;->getParamValue(LX/1qK;Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/ModifyThreadParams;

    .line 2229068
    iget-boolean v1, v0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->c:Z

    move v1, v1

    .line 2229069
    if-nez v1, :cond_0

    .line 2229070
    iget-boolean v1, v0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->i:Z

    move v1, v1

    .line 2229071
    if-nez v1, :cond_0

    .line 2229072
    iget-boolean v1, v0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->j:Z

    move v1, v1

    .line 2229073
    if-eqz v1, :cond_4

    .line 2229074
    :cond_0
    iget-object v1, v0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v1, v1

    .line 2229075
    iget-wide v4, v1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    .line 2229076
    iget-boolean v1, v0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->c:Z

    move v1, v1

    .line 2229077
    if-eqz v1, :cond_1

    .line 2229078
    iget-object v1, p0, LX/FMD;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/sms/SmsThreadManager;

    .line 2229079
    iget-object v3, v0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2229080
    iget-object v7, v1, Lcom/facebook/messaging/sms/SmsThreadManager;->s:LX/0Ot;

    invoke-interface {v7}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, LX/3Mo;

    .line 2229081
    new-instance p1, Landroid/content/ContentValues;

    invoke-direct {p1}, Landroid/content/ContentValues;-><init>()V

    .line 2229082
    sget-object p2, LX/3MM;->d:LX/0U1;

    .line 2229083
    iget-object v1, p2, LX/0U1;->d:Ljava/lang/String;

    move-object p2, v1

    .line 2229084
    invoke-virtual {p1, p2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2229085
    invoke-static {v7, v4, v5, p1}, LX/3Mo;->a(LX/3Mo;JLandroid/content/ContentValues;)V

    .line 2229086
    :cond_1
    iget-boolean v1, v0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->i:Z

    move v1, v1

    .line 2229087
    if-nez v1, :cond_2

    .line 2229088
    iget-boolean v1, v0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->j:Z

    move v1, v1

    .line 2229089
    if-eqz v1, :cond_3

    .line 2229090
    :cond_2
    iget-object v1, p0, LX/FMD;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/sms/SmsThreadManager;

    .line 2229091
    iget-object v3, v0, Lcom/facebook/messaging/service/model/ModifyThreadParams;->l:Lcom/facebook/messaging/model/threads/ThreadCustomization;

    move-object v0, v3

    .line 2229092
    iget-object v3, v1, Lcom/facebook/messaging/sms/SmsThreadManager;->s:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/3Mo;

    .line 2229093
    new-instance v7, Landroid/content/ContentValues;

    invoke-direct {v7}, Landroid/content/ContentValues;-><init>()V

    .line 2229094
    sget-object p1, LX/3MM;->f:LX/0U1;

    .line 2229095
    iget-object v1, p1, LX/0U1;->d:Ljava/lang/String;

    move-object p1, v1

    .line 2229096
    iget v1, v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->c:I

    move v1, v1

    .line 2229097
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, p1, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2229098
    sget-object p1, LX/3MM;->g:LX/0U1;

    .line 2229099
    iget-object v1, p1, LX/0U1;->d:Ljava/lang/String;

    move-object p1, v1

    .line 2229100
    iget v1, v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->d:I

    move v1, v1

    .line 2229101
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, p1, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2229102
    sget-object p1, LX/3MM;->h:LX/0U1;

    .line 2229103
    iget-object v1, p1, LX/0U1;->d:Ljava/lang/String;

    move-object p1, v1

    .line 2229104
    iget v1, v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->b:I

    move v1, v1

    .line 2229105
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, p1, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2229106
    sget-object p1, LX/3MM;->i:LX/0U1;

    .line 2229107
    iget-object v1, p1, LX/0U1;->d:Ljava/lang/String;

    move-object p1, v1

    .line 2229108
    iget-object v1, v0, Lcom/facebook/messaging/model/threads/ThreadCustomization;->f:Ljava/lang/String;

    move-object v1, v1

    .line 2229109
    invoke-virtual {v7, p1, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2229110
    invoke-static {v3, v4, v5, v7}, LX/3Mo;->a(LX/3Mo;JLandroid/content/ContentValues;)V

    .line 2229111
    :cond_3
    iget-object v0, p0, LX/FMD;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/SmsThreadManager;

    invoke-virtual {v0, v4, v5, v6, v6}, Lcom/facebook/messaging/sms/SmsThreadManager;->a(JLjava/util/Map;LX/6ek;)Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v1

    .line 2229112
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadResult;->a()LX/6iO;

    move-result-object v0

    sget-object v3, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_LOCAL_DISK_CACHE_UP_TO_DATE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2229113
    iput-object v3, v0, LX/6iO;->b:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2229114
    move-object v3, v0

    .line 2229115
    iget-object v0, p0, LX/FMD;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v4

    .line 2229116
    iput-wide v4, v3, LX/6iO;->g:J

    .line 2229117
    move-object v0, v3

    .line 2229118
    iput-object v1, v0, LX/6iO;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2229119
    move-object v0, v0

    .line 2229120
    iput-object v2, v0, LX/6iO;->e:LX/0Px;

    .line 2229121
    move-object v0, v0

    .line 2229122
    invoke-virtual {v0}, LX/6iO;->a()Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2229123
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2229124
    :goto_0
    return-object v0

    .line 2229125
    :cond_4
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2229126
    goto :goto_0
.end method

.method public final q(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 1

    .prologue
    .line 2229127
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2229128
    return-object v0
.end method
