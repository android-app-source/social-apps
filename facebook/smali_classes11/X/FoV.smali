.class public final LX/FoV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;)V
    .locals 0

    .prologue
    .line 2289549
    iput-object p1, p0, LX/FoV;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 2289550
    add-int v0, p2, p3

    add-int/lit8 v1, p4, -0x3

    if-le v0, v1, :cond_0

    .line 2289551
    iget-object v0, p0, LX/FoV;->a:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerFragment;->c:Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;

    invoke-virtual {v0}, Lcom/facebook/socialgood/triggers/FundraiserCuratedCharityPickerPager;->a()V

    .line 2289552
    :cond_0
    return-void
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 2289553
    return-void
.end method
