.class public final LX/F8V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;)V
    .locals 0

    .prologue
    .line 2204136
    iput-object p1, p0, LX/F8V;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    .prologue
    .line 2204137
    iget-object v0, p0, LX/F8V;->a:Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;

    .line 2204138
    iget-object v1, v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->C:LX/F8S;

    sget-object v2, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->m:Lcom/facebook/common/callercontext/CallerContext;

    iget-object v3, v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    .line 2204139
    iget-object v4, v3, LX/F8M;->a:Ljava/util/List;

    invoke-static {v4}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v4

    move-object v3, v4

    .line 2204140
    iget-object v4, v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->D:LX/89v;

    .line 2204141
    iget-object v5, v1, LX/F8S;->c:LX/F6j;

    invoke-virtual {v5, v4}, LX/F6j;->a(LX/89v;)LX/F6i;

    move-result-object v5

    .line 2204142
    new-instance p2, Ljava/util/ArrayList;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v6

    invoke-direct {p2, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 2204143
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v1

    const/4 v6, 0x0

    move p0, v6

    :goto_0
    if-ge p0, v1, :cond_0

    invoke-virtual {v3, p0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/F8P;

    .line 2204144
    iget-object v4, v6, LX/F8P;->c:Ljava/lang/String;

    move-object v6, v4

    .line 2204145
    invoke-interface {p2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2204146
    add-int/lit8 v6, p0, 0x1

    move p0, v6

    goto :goto_0

    .line 2204147
    :cond_0
    move-object v6, p2

    .line 2204148
    const/4 p0, 0x1

    invoke-virtual {v5, v6, p0, v2}, LX/F6i;->a(Ljava/util/List;ZLcom/facebook/common/callercontext/CallerContext;)V

    .line 2204149
    iget-object v1, v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->B:LX/F8M;

    .line 2204150
    iget-object v2, v1, LX/F8M;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/F8P;

    .line 2204151
    sget-object v4, LX/F8O;->INVITED:LX/F8O;

    .line 2204152
    iput-object v4, v2, LX/F8P;->e:LX/F8O;

    .line 2204153
    goto :goto_1

    .line 2204154
    :cond_1
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 2204155
    iget-object v1, v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->e:LX/9Tk;

    iget-object v2, v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->D:LX/89v;

    iget-object v2, v2, LX/89v;->value:Ljava/lang/String;

    iget-object v3, v0, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsFragment;->n:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    sget-object v4, LX/9Ti;->INVITABLE_CONTACTS_API:LX/9Ti;

    invoke-virtual {v1, v2, v3, v4}, LX/9Tk;->a(Ljava/lang/String;ILX/9Ti;)V

    .line 2204156
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2204157
    return-void
.end method
