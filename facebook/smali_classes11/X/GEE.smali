.class public final LX/GEE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0rl;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0rl",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8wL;

.field public final synthetic b:LX/GEA;

.field public final synthetic c:Z

.field public final synthetic d:LX/GEF;

.field private e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

.field private f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field private g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/GEF;LX/8wL;LX/GEA;Z)V
    .locals 0

    .prologue
    .line 2331266
    iput-object p1, p0, LX/GEE;->d:LX/GEF;

    iput-object p2, p0, LX/GEE;->a:LX/8wL;

    iput-object p3, p0, LX/GEE;->b:LX/GEA;

    iput-boolean p4, p0, LX/GEE;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2331289
    iget-object v0, p0, LX/GEE;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v1, p0, LX/GEE;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;)V

    .line 2331290
    iget-object v0, p0, LX/GEE;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v1, p0, LX/GEE;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2331291
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2331292
    iget-object v0, p0, LX/GEE;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v1, p0, LX/GEE;->h:Ljava/lang/String;

    .line 2331293
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->g:Ljava/lang/String;

    .line 2331294
    iget-object v0, p0, LX/GEE;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v1, p0, LX/GEE;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->h(Ljava/lang/String;)V

    .line 2331295
    iget-object v0, p0, LX/GEE;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v1, p0, LX/GEE;->j:Ljava/lang/String;

    .line 2331296
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->o:Ljava/lang/String;

    .line 2331297
    iget-object v0, p0, LX/GEE;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v1, p0, LX/GEE;->k:Ljava/lang/String;

    .line 2331298
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->n:Ljava/lang/String;

    .line 2331299
    iget-object v0, p0, LX/GEE;->a:LX/8wL;

    sget-object v1, LX/8wL;->BOOST_POST:LX/8wL;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/GEE;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->INACTIVE:LX/GGB;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/GEE;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->aa()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2331300
    iget-object v0, p0, LX/GEE;->d:LX/GEF;

    iget-object v0, v0, LX/GEF;->c:LX/2U4;

    .line 2331301
    iget-object v1, v0, LX/2U4;->b:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget-short v4, LX/GDK;->p:S

    const/4 v5, 0x0

    invoke-interface {v1, v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2331302
    iget-object v1, v0, LX/2U4;->b:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    sget-object v3, LX/0c1;->Off:LX/0c1;

    sget-char v4, LX/GDK;->o:C

    const-string v5, "engagement"

    invoke-interface {v1, v2, v3, v4, v5}, LX/0ad;->a(LX/0c0;LX/0c1;CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2331303
    const-string v2, "clicks"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->LINK_CLICKS:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    .line 2331304
    :goto_0
    move-object v0, v1

    .line 2331305
    if-eqz v0, :cond_0

    .line 2331306
    iget-object v1, p0, LX/GEE;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2331307
    iput-object v0, v1, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->u:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    .line 2331308
    :cond_0
    iget-object v0, p0, LX/GEE;->b:LX/GEA;

    iget-object v1, p0, LX/GEE;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-interface {v0, v1}, LX/GEA;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    .line 2331309
    iget-boolean v0, p0, LX/GEE;->c:Z

    if-eqz v0, :cond_1

    .line 2331310
    iget-object v0, p0, LX/GEE;->d:LX/GEF;

    iget-object v0, v0, LX/GEF;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x970001

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2331311
    :cond_1
    return-void

    .line 2331312
    :cond_2
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;->POST_ENGAGEMENT:Lcom/facebook/graphql/enums/GraphQLBoostedComponentObjective;

    goto :goto_0

    .line 2331313
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2331271
    instance-of v0, p1, LX/GGO;

    if-eqz v0, :cond_1

    .line 2331272
    check-cast p1, LX/GGO;

    .line 2331273
    iget-object v0, p1, LX/GGO;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-object v0, v0

    .line 2331274
    iput-object v0, p0, LX/GEE;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    .line 2331275
    iget-object v0, p1, LX/GGO;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v0, v0

    .line 2331276
    iput-object v0, p0, LX/GEE;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2331277
    iget-object v0, p1, LX/GGO;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-object v0, v0

    .line 2331278
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, LX/GEE;->h:Ljava/lang/String;

    .line 2331279
    iget-object v0, p1, LX/GGO;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2331280
    iput-object v0, p0, LX/GEE;->i:Ljava/lang/String;

    .line 2331281
    iget-object v0, p1, LX/GGO;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2331282
    iput-object v0, p0, LX/GEE;->j:Ljava/lang/String;

    .line 2331283
    iget-object v0, p1, LX/GGO;->f:Ljava/lang/String;

    move-object v0, v0

    .line 2331284
    iput-object v0, p0, LX/GEE;->k:Ljava/lang/String;

    .line 2331285
    :goto_1
    return-void

    .line 2331286
    :cond_0
    iget-object v0, p1, LX/GGO;->c:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-object v0, v0

    .line 2331287
    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2331288
    :cond_1
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iput-object p1, p0, LX/GEE;->f:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    goto :goto_1
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2331267
    iget-boolean v0, p0, LX/GEE;->c:Z

    if-eqz v0, :cond_0

    .line 2331268
    iget-object v0, p0, LX/GEE;->d:LX/GEF;

    iget-object v0, v0, LX/GEF;->f:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x970001

    const/16 v2, 0x57

    invoke-interface {v0, v1, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2331269
    :cond_0
    iget-object v0, p0, LX/GEE;->b:LX/GEA;

    invoke-interface {v0, p1}, LX/GEA;->a(Ljava/lang/Throwable;)V

    .line 2331270
    return-void
.end method
