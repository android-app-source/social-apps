.class public final LX/Gzo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Gzi;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

.field public final synthetic b:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V
    .locals 0

    .prologue
    .line 2412348
    iput-object p1, p0, LX/Gzo;->b:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    iput-object p2, p0, LX/Gzo;->a:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2412349
    iget-object v0, p0, LX/Gzo;->b:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->g:LX/Gze;

    iget-object v1, p0, LX/Gzo;->b:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Gze;->c(Ljava/lang/String;)V

    .line 2412350
    iget-object v0, p0, LX/Gzo;->b:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    invoke-static {v0, v2}, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->a(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;Z)V

    .line 2412351
    iget-object v0, p0, LX/Gzo;->b:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    iget-object v1, p0, LX/Gzo;->a:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    .line 2412352
    iget-object v3, v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->e:LX/H0t;

    const/4 v4, 0x1

    .line 2412353
    iput-boolean v4, v3, LX/H0t;->b:Z

    .line 2412354
    iget-object v3, v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->e:LX/H0t;

    .line 2412355
    iget-object v4, v1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->b:Ljava/lang/String;

    move-object v4, v4

    .line 2412356
    iput-object v4, v3, LX/H0t;->a:Ljava/lang/String;

    .line 2412357
    iget-object v0, p0, LX/Gzo;->b:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2412358
    iget-object v0, p0, LX/Gzo;->b:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    .line 2412359
    iput-boolean v2, v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->m:Z

    .line 2412360
    iget-object v0, p0, LX/Gzo;->b:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->onBackPressed()V

    .line 2412361
    iget-object v0, p0, LX/Gzo;->b:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f081527

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2412362
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2412363
    iget-object v0, p0, LX/Gzo;->b:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    iget-object v0, v0, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->g:LX/Gze;

    iget-object v1, p0, LX/Gzo;->b:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    iget-object v1, v1, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/Gze;->d(Ljava/lang/String;)V

    .line 2412364
    iget-object v0, p0, LX/Gzo;->b:Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;->a(Lcom/facebook/messaging/professionalservices/getquote/fragment/GetQuoteFormBuilderFragment;Z)V

    .line 2412365
    return-void
.end method
