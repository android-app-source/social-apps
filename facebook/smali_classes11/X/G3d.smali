.class public LX/G3d;
.super LX/2s5;
.source ""


# instance fields
.field public final a:LX/0gc;

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;",
            "Landroid/support/v4/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0gc;Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gc;",
            "Ljava/util/List",
            "<",
            "Landroid/support/v4/app/Fragment;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;",
            "Landroid/support/v4/app/Fragment;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2315978
    invoke-direct {p0, p1}, LX/2s5;-><init>(LX/0gc;)V

    .line 2315979
    iput-object p1, p0, LX/G3d;->a:LX/0gc;

    .line 2315980
    iput-object p2, p0, LX/G3d;->b:Ljava/util/List;

    .line 2315981
    iput-object p3, p0, LX/G3d;->c:Ljava/util/Map;

    .line 2315982
    iput-object p4, p0, LX/G3d;->d:Ljava/util/Map;

    .line 2315983
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)I
    .locals 1

    .prologue
    .line 2315977
    iget-object v0, p0, LX/G3d;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final a(I)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 2315976
    iget-object v0, p0, LX/G3d;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 2315975
    iget-object v0, p0, LX/G3d;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 2315972
    invoke-virtual {p0, p1}, LX/G3d;->c(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Z

    move-result v0

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2315973
    iget-object v0, p0, LX/G3d;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method public final c(Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;)Z
    .locals 1

    .prologue
    .line 2315974
    iget-object v0, p0, LX/G3d;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
