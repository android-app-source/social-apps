.class public final enum LX/G98;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/G98;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/G98;

.field public static final enum ASCII:LX/G98;

.field public static final enum Big5:LX/G98;

.field public static final enum Cp1250:LX/G98;

.field public static final enum Cp1251:LX/G98;

.field public static final enum Cp1252:LX/G98;

.field public static final enum Cp1256:LX/G98;

.field public static final enum Cp437:LX/G98;

.field public static final enum EUC_KR:LX/G98;

.field public static final enum GB18030:LX/G98;

.field public static final enum ISO8859_1:LX/G98;

.field public static final enum ISO8859_10:LX/G98;

.field public static final enum ISO8859_11:LX/G98;

.field public static final enum ISO8859_13:LX/G98;

.field public static final enum ISO8859_14:LX/G98;

.field public static final enum ISO8859_15:LX/G98;

.field public static final enum ISO8859_16:LX/G98;

.field public static final enum ISO8859_2:LX/G98;

.field public static final enum ISO8859_3:LX/G98;

.field public static final enum ISO8859_4:LX/G98;

.field public static final enum ISO8859_5:LX/G98;

.field public static final enum ISO8859_6:LX/G98;

.field public static final enum ISO8859_7:LX/G98;

.field public static final enum ISO8859_8:LX/G98;

.field public static final enum ISO8859_9:LX/G98;

.field private static final NAME_TO_ECI:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/G98;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum SJIS:LX/G98;

.field public static final enum UTF8:LX/G98;

.field public static final enum UnicodeBigUnmarked:LX/G98;

.field private static final VALUE_TO_ECI:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/G98;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final otherEncodingNames:[Ljava/lang/String;

.field private final values:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 2321657
    new-instance v0, LX/G98;

    const-string v2, "Cp437"

    new-array v3, v8, [I

    fill-array-data v3, :array_0

    new-array v4, v1, [Ljava/lang/String;

    invoke-direct {v0, v2, v1, v3, v4}, LX/G98;-><init>(Ljava/lang/String;I[I[Ljava/lang/String;)V

    sput-object v0, LX/G98;->Cp437:LX/G98;

    .line 2321658
    new-instance v0, LX/G98;

    const-string v2, "ISO8859_1"

    new-array v3, v8, [I

    fill-array-data v3, :array_1

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "ISO-8859-1"

    aput-object v5, v4, v1

    invoke-direct {v0, v2, v7, v3, v4}, LX/G98;-><init>(Ljava/lang/String;I[I[Ljava/lang/String;)V

    sput-object v0, LX/G98;->ISO8859_1:LX/G98;

    .line 2321659
    new-instance v0, LX/G98;

    const-string v2, "ISO8859_2"

    new-array v3, v7, [Ljava/lang/String;

    const-string v4, "ISO-8859-2"

    aput-object v4, v3, v1

    invoke-direct {v0, v2, v8, v10, v3}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->ISO8859_2:LX/G98;

    .line 2321660
    new-instance v0, LX/G98;

    const-string v2, "ISO8859_3"

    const/4 v3, 0x5

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "ISO-8859-3"

    aput-object v5, v4, v1

    invoke-direct {v0, v2, v9, v3, v4}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->ISO8859_3:LX/G98;

    .line 2321661
    new-instance v0, LX/G98;

    const-string v2, "ISO8859_4"

    const/4 v3, 0x6

    new-array v4, v7, [Ljava/lang/String;

    const-string v5, "ISO-8859-4"

    aput-object v5, v4, v1

    invoke-direct {v0, v2, v10, v3, v4}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->ISO8859_4:LX/G98;

    .line 2321662
    new-instance v0, LX/G98;

    const-string v2, "ISO8859_5"

    const/4 v3, 0x5

    const/4 v4, 0x7

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "ISO-8859-5"

    aput-object v6, v5, v1

    invoke-direct {v0, v2, v3, v4, v5}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->ISO8859_5:LX/G98;

    .line 2321663
    new-instance v0, LX/G98;

    const-string v2, "ISO8859_6"

    const/4 v3, 0x6

    const/16 v4, 0x8

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "ISO-8859-6"

    aput-object v6, v5, v1

    invoke-direct {v0, v2, v3, v4, v5}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->ISO8859_6:LX/G98;

    .line 2321664
    new-instance v0, LX/G98;

    const-string v2, "ISO8859_7"

    const/4 v3, 0x7

    const/16 v4, 0x9

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "ISO-8859-7"

    aput-object v6, v5, v1

    invoke-direct {v0, v2, v3, v4, v5}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->ISO8859_7:LX/G98;

    .line 2321665
    new-instance v0, LX/G98;

    const-string v2, "ISO8859_8"

    const/16 v3, 0x8

    const/16 v4, 0xa

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "ISO-8859-8"

    aput-object v6, v5, v1

    invoke-direct {v0, v2, v3, v4, v5}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->ISO8859_8:LX/G98;

    .line 2321666
    new-instance v0, LX/G98;

    const-string v2, "ISO8859_9"

    const/16 v3, 0x9

    const/16 v4, 0xb

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "ISO-8859-9"

    aput-object v6, v5, v1

    invoke-direct {v0, v2, v3, v4, v5}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->ISO8859_9:LX/G98;

    .line 2321667
    new-instance v0, LX/G98;

    const-string v2, "ISO8859_10"

    const/16 v3, 0xa

    const/16 v4, 0xc

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "ISO-8859-10"

    aput-object v6, v5, v1

    invoke-direct {v0, v2, v3, v4, v5}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->ISO8859_10:LX/G98;

    .line 2321668
    new-instance v0, LX/G98;

    const-string v2, "ISO8859_11"

    const/16 v3, 0xb

    const/16 v4, 0xd

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "ISO-8859-11"

    aput-object v6, v5, v1

    invoke-direct {v0, v2, v3, v4, v5}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->ISO8859_11:LX/G98;

    .line 2321669
    new-instance v0, LX/G98;

    const-string v2, "ISO8859_13"

    const/16 v3, 0xc

    const/16 v4, 0xf

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "ISO-8859-13"

    aput-object v6, v5, v1

    invoke-direct {v0, v2, v3, v4, v5}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->ISO8859_13:LX/G98;

    .line 2321670
    new-instance v0, LX/G98;

    const-string v2, "ISO8859_14"

    const/16 v3, 0xd

    const/16 v4, 0x10

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "ISO-8859-14"

    aput-object v6, v5, v1

    invoke-direct {v0, v2, v3, v4, v5}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->ISO8859_14:LX/G98;

    .line 2321671
    new-instance v0, LX/G98;

    const-string v2, "ISO8859_15"

    const/16 v3, 0xe

    const/16 v4, 0x11

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "ISO-8859-15"

    aput-object v6, v5, v1

    invoke-direct {v0, v2, v3, v4, v5}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->ISO8859_15:LX/G98;

    .line 2321672
    new-instance v0, LX/G98;

    const-string v2, "ISO8859_16"

    const/16 v3, 0xf

    const/16 v4, 0x12

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "ISO-8859-16"

    aput-object v6, v5, v1

    invoke-direct {v0, v2, v3, v4, v5}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->ISO8859_16:LX/G98;

    .line 2321673
    new-instance v0, LX/G98;

    const-string v2, "SJIS"

    const/16 v3, 0x10

    const/16 v4, 0x14

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "Shift_JIS"

    aput-object v6, v5, v1

    invoke-direct {v0, v2, v3, v4, v5}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->SJIS:LX/G98;

    .line 2321674
    new-instance v0, LX/G98;

    const-string v2, "Cp1250"

    const/16 v3, 0x11

    const/16 v4, 0x15

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "windows-1250"

    aput-object v6, v5, v1

    invoke-direct {v0, v2, v3, v4, v5}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->Cp1250:LX/G98;

    .line 2321675
    new-instance v0, LX/G98;

    const-string v2, "Cp1251"

    const/16 v3, 0x12

    const/16 v4, 0x16

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "windows-1251"

    aput-object v6, v5, v1

    invoke-direct {v0, v2, v3, v4, v5}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->Cp1251:LX/G98;

    .line 2321676
    new-instance v0, LX/G98;

    const-string v2, "Cp1252"

    const/16 v3, 0x13

    const/16 v4, 0x17

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "windows-1252"

    aput-object v6, v5, v1

    invoke-direct {v0, v2, v3, v4, v5}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->Cp1252:LX/G98;

    .line 2321677
    new-instance v0, LX/G98;

    const-string v2, "Cp1256"

    const/16 v3, 0x14

    const/16 v4, 0x18

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "windows-1256"

    aput-object v6, v5, v1

    invoke-direct {v0, v2, v3, v4, v5}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->Cp1256:LX/G98;

    .line 2321678
    new-instance v0, LX/G98;

    const-string v2, "UnicodeBigUnmarked"

    const/16 v3, 0x15

    const/16 v4, 0x19

    new-array v5, v8, [Ljava/lang/String;

    const-string v6, "UTF-16BE"

    aput-object v6, v5, v1

    const-string v6, "UnicodeBig"

    aput-object v6, v5, v7

    invoke-direct {v0, v2, v3, v4, v5}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->UnicodeBigUnmarked:LX/G98;

    .line 2321679
    new-instance v0, LX/G98;

    const-string v2, "UTF8"

    const/16 v3, 0x16

    const/16 v4, 0x1a

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "UTF-8"

    aput-object v6, v5, v1

    invoke-direct {v0, v2, v3, v4, v5}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->UTF8:LX/G98;

    .line 2321680
    new-instance v0, LX/G98;

    const-string v2, "ASCII"

    const/16 v3, 0x17

    new-array v4, v8, [I

    fill-array-data v4, :array_2

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "US-ASCII"

    aput-object v6, v5, v1

    invoke-direct {v0, v2, v3, v4, v5}, LX/G98;-><init>(Ljava/lang/String;I[I[Ljava/lang/String;)V

    sput-object v0, LX/G98;->ASCII:LX/G98;

    .line 2321681
    new-instance v0, LX/G98;

    const-string v2, "Big5"

    const/16 v3, 0x18

    const/16 v4, 0x1c

    invoke-direct {v0, v2, v3, v4}, LX/G98;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/G98;->Big5:LX/G98;

    .line 2321682
    new-instance v0, LX/G98;

    const-string v2, "GB18030"

    const/16 v3, 0x19

    const/16 v4, 0x1d

    new-array v5, v9, [Ljava/lang/String;

    const-string v6, "GB2312"

    aput-object v6, v5, v1

    const-string v6, "EUC_CN"

    aput-object v6, v5, v7

    const-string v6, "GBK"

    aput-object v6, v5, v8

    invoke-direct {v0, v2, v3, v4, v5}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->GB18030:LX/G98;

    .line 2321683
    new-instance v0, LX/G98;

    const-string v2, "EUC_KR"

    const/16 v3, 0x1a

    const/16 v4, 0x1e

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "EUC-KR"

    aput-object v6, v5, v1

    invoke-direct {v0, v2, v3, v4, v5}, LX/G98;-><init>(Ljava/lang/String;II[Ljava/lang/String;)V

    sput-object v0, LX/G98;->EUC_KR:LX/G98;

    .line 2321684
    const/16 v0, 0x1b

    new-array v0, v0, [LX/G98;

    sget-object v2, LX/G98;->Cp437:LX/G98;

    aput-object v2, v0, v1

    sget-object v2, LX/G98;->ISO8859_1:LX/G98;

    aput-object v2, v0, v7

    sget-object v2, LX/G98;->ISO8859_2:LX/G98;

    aput-object v2, v0, v8

    sget-object v2, LX/G98;->ISO8859_3:LX/G98;

    aput-object v2, v0, v9

    sget-object v2, LX/G98;->ISO8859_4:LX/G98;

    aput-object v2, v0, v10

    const/4 v2, 0x5

    sget-object v3, LX/G98;->ISO8859_5:LX/G98;

    aput-object v3, v0, v2

    const/4 v2, 0x6

    sget-object v3, LX/G98;->ISO8859_6:LX/G98;

    aput-object v3, v0, v2

    const/4 v2, 0x7

    sget-object v3, LX/G98;->ISO8859_7:LX/G98;

    aput-object v3, v0, v2

    const/16 v2, 0x8

    sget-object v3, LX/G98;->ISO8859_8:LX/G98;

    aput-object v3, v0, v2

    const/16 v2, 0x9

    sget-object v3, LX/G98;->ISO8859_9:LX/G98;

    aput-object v3, v0, v2

    const/16 v2, 0xa

    sget-object v3, LX/G98;->ISO8859_10:LX/G98;

    aput-object v3, v0, v2

    const/16 v2, 0xb

    sget-object v3, LX/G98;->ISO8859_11:LX/G98;

    aput-object v3, v0, v2

    const/16 v2, 0xc

    sget-object v3, LX/G98;->ISO8859_13:LX/G98;

    aput-object v3, v0, v2

    const/16 v2, 0xd

    sget-object v3, LX/G98;->ISO8859_14:LX/G98;

    aput-object v3, v0, v2

    const/16 v2, 0xe

    sget-object v3, LX/G98;->ISO8859_15:LX/G98;

    aput-object v3, v0, v2

    const/16 v2, 0xf

    sget-object v3, LX/G98;->ISO8859_16:LX/G98;

    aput-object v3, v0, v2

    const/16 v2, 0x10

    sget-object v3, LX/G98;->SJIS:LX/G98;

    aput-object v3, v0, v2

    const/16 v2, 0x11

    sget-object v3, LX/G98;->Cp1250:LX/G98;

    aput-object v3, v0, v2

    const/16 v2, 0x12

    sget-object v3, LX/G98;->Cp1251:LX/G98;

    aput-object v3, v0, v2

    const/16 v2, 0x13

    sget-object v3, LX/G98;->Cp1252:LX/G98;

    aput-object v3, v0, v2

    const/16 v2, 0x14

    sget-object v3, LX/G98;->Cp1256:LX/G98;

    aput-object v3, v0, v2

    const/16 v2, 0x15

    sget-object v3, LX/G98;->UnicodeBigUnmarked:LX/G98;

    aput-object v3, v0, v2

    const/16 v2, 0x16

    sget-object v3, LX/G98;->UTF8:LX/G98;

    aput-object v3, v0, v2

    const/16 v2, 0x17

    sget-object v3, LX/G98;->ASCII:LX/G98;

    aput-object v3, v0, v2

    const/16 v2, 0x18

    sget-object v3, LX/G98;->Big5:LX/G98;

    aput-object v3, v0, v2

    const/16 v2, 0x19

    sget-object v3, LX/G98;->GB18030:LX/G98;

    aput-object v3, v0, v2

    const/16 v2, 0x1a

    sget-object v3, LX/G98;->EUC_KR:LX/G98;

    aput-object v3, v0, v2

    sput-object v0, LX/G98;->$VALUES:[LX/G98;

    .line 2321685
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/G98;->VALUE_TO_ECI:Ljava/util/Map;

    .line 2321686
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, LX/G98;->NAME_TO_ECI:Ljava/util/Map;

    .line 2321687
    invoke-static {}, LX/G98;->values()[LX/G98;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 2321688
    iget-object v6, v5, LX/G98;->values:[I

    array-length v7, v6

    move v0, v1

    :goto_1
    if-ge v0, v7, :cond_0

    aget v8, v6, v0

    .line 2321689
    sget-object v9, LX/G98;->VALUE_TO_ECI:Ljava/util/Map;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v9, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2321690
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2321691
    :cond_0
    sget-object v0, LX/G98;->NAME_TO_ECI:Ljava/util/Map;

    invoke-virtual {v5}, LX/G98;->name()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2321692
    iget-object v6, v5, LX/G98;->otherEncodingNames:[Ljava/lang/String;

    array-length v7, v6

    move v0, v1

    :goto_2
    if-ge v0, v7, :cond_1

    aget-object v8, v6, v0

    .line 2321693
    sget-object v9, LX/G98;->NAME_TO_ECI:Ljava/util/Map;

    invoke-interface {v9, v8, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2321694
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2321695
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2321696
    :cond_2
    return-void

    .line 2321697
    :array_0
    .array-data 4
        0x0
        0x2
    .end array-data

    .line 2321698
    :array_1
    .array-data 4
        0x1
        0x3
    .end array-data

    .line 2321699
    :array_2
    .array-data 4
        0x1b
        0xaa
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2321655
    const/4 v0, 0x1

    new-array v0, v0, [I

    aput p3, v0, v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, v0, v1}, LX/G98;-><init>(Ljava/lang/String;I[I[Ljava/lang/String;)V

    .line 2321656
    return-void
.end method

.method private varargs constructor <init>(Ljava/lang/String;II[Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2321640
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2321641
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p3, v0, v1

    iput-object v0, p0, LX/G98;->values:[I

    .line 2321642
    iput-object p4, p0, LX/G98;->otherEncodingNames:[Ljava/lang/String;

    .line 2321643
    return-void
.end method

.method private varargs constructor <init>(Ljava/lang/String;I[I[Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2321651
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2321652
    iput-object p3, p0, LX/G98;->values:[I

    .line 2321653
    iput-object p4, p0, LX/G98;->otherEncodingNames:[Ljava/lang/String;

    .line 2321654
    return-void
.end method

.method public static getCharacterSetECIByName(Ljava/lang/String;)LX/G98;
    .locals 1

    .prologue
    .line 2321650
    sget-object v0, LX/G98;->NAME_TO_ECI:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G98;

    return-object v0
.end method

.method public static getCharacterSetECIByValue(I)LX/G98;
    .locals 2

    .prologue
    .line 2321647
    if-ltz p0, :cond_0

    const/16 v0, 0x384

    if-lt p0, v0, :cond_1

    .line 2321648
    :cond_0
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0

    .line 2321649
    :cond_1
    sget-object v0, LX/G98;->VALUE_TO_ECI:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G98;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/G98;
    .locals 1

    .prologue
    .line 2321646
    const-class v0, LX/G98;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/G98;

    return-object v0
.end method

.method public static values()[LX/G98;
    .locals 1

    .prologue
    .line 2321645
    sget-object v0, LX/G98;->$VALUES:[LX/G98;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/G98;

    return-object v0
.end method


# virtual methods
.method public final getValue()I
    .locals 2

    .prologue
    .line 2321644
    iget-object v0, p0, LX/G98;->values:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method
