.class public LX/Gto;
.super LX/Gtg;
.source ""

# interfaces
.implements LX/7g9;
.implements LX/0f4;
.implements LX/0gr;
.implements LX/0fE;
.implements LX/0fH;
.implements LX/0fI;
.implements LX/63U;


# static fields
.field private static final S:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private A:LX/0gw;

.field private B:Landroid/widget/FrameLayout;

.field private C:Z

.field private D:LX/0cQ;

.field private E:LX/0So;

.field private F:Z

.field private G:Z

.field private H:Z

.field private I:Landroid/support/v4/app/Fragment;

.field public J:LX/0id;

.field private K:LX/11v;

.field private L:LX/12S;

.field private M:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/11u;",
            ">;"
        }
    .end annotation
.end field

.field private N:LX/120;

.field private O:LX/12I;

.field private P:LX/0yH;

.field private Q:LX/0jN;

.field private R:Z

.field private T:LX/0gz;

.field private U:I

.field private V:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/uicontrib/fab/FabView;",
            ">;"
        }
    .end annotation
.end field

.field private W:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field

.field private X:LX/1ih;

.field private Y:Z

.field private Z:Z

.field private aa:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

.field private ab:Z

.field private ac:LX/42f;

.field public ad:Z

.field private ae:LX/0ad;

.field private af:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ja;",
            ">;"
        }
    .end annotation
.end field

.field private ag:LX/0xX;

.field private ah:LX/0go;

.field private ai:LX/03V;

.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0oh;",
            ">;"
        }
    .end annotation
.end field

.field private c:LX/0fU;

.field private d:LX/0gc;

.field private e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/0gn;

.field private g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0fJ;",
            ">;"
        }
    .end annotation
.end field

.field public h:Landroid/os/Handler;

.field private i:LX/0jo;

.field private j:LX/0Yi;

.field private k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            ">;"
        }
    .end annotation
.end field

.field private l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/AnM;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FiT;",
            ">;"
        }
    .end annotation
.end field

.field private n:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Gtt;",
            ">;"
        }
    .end annotation
.end field

.field private o:Z

.field private p:Lcom/facebook/content/SecureContextHelper;

.field private q:LX/0Zb;

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field private s:LX/0jM;

.field private t:LX/0iI;

.field private u:LX/0iL;

.field private v:LX/0iM;

.field private w:LX/0iQ;

.field private x:Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;

.field private y:LX/0hv;

.field private z:LX/0gy;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2402602
    const-class v0, LX/Gto;

    sput-object v0, LX/Gto;->a:Ljava/lang/Class;

    .line 2402603
    const-string v0, "chromeless:content:fragment:tag"

    const-string v1, "MediaGalleryDialogFragment_MEDIA_GALLERY"

    const-string v2, "MediaGalleryDialogFragment_PHOTOS_FEED"

    invoke-static {v0, v1, v2}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/Gto;->S:LX/0Px;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2402595
    invoke-direct {p0}, LX/Gtg;-><init>()V

    .line 2402596
    iput-boolean v1, p0, LX/Gto;->F:Z

    .line 2402597
    iput-boolean v1, p0, LX/Gto;->G:Z

    .line 2402598
    iput-boolean v1, p0, LX/Gto;->H:Z

    .line 2402599
    iput-boolean v1, p0, LX/Gto;->R:Z

    .line 2402600
    const v0, 0x7f0d002f

    iput v0, p0, LX/Gto;->U:I

    .line 2402601
    iput-boolean v1, p0, LX/Gto;->ad:Z

    return-void
.end method

.method private I()V
    .locals 3

    .prologue
    .line 2402585
    iget-object v0, p0, LX/Gto;->W:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    sget-object v1, LX/03R;->YES:LX/03R;

    if-ne v0, v1, :cond_0

    .line 2402586
    iget-object v0, p0, LX/Gto;->M:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11u;

    const v1, 0x7f0d16f9

    invoke-virtual {p0, v1}, LX/42j;->c(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 2402587
    iput-object v1, v0, LX/11u;->e:Landroid/view/ViewStub;

    .line 2402588
    iget-object v1, p0, LX/Gto;->K:LX/11v;

    iget-object v0, p0, LX/Gto;->M:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11u;

    .line 2402589
    iget-object v2, v0, LX/11u;->a:LX/12P;

    move-object v0, v2

    .line 2402590
    iput-object v0, v1, LX/11v;->o:LX/12Q;

    .line 2402591
    iget-object v1, p0, LX/Gto;->N:LX/120;

    const v0, 0x7f0d0c8b

    invoke-virtual {p0, v0}, LX/42j;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v1, v0}, LX/120;->a(Landroid/view/ViewStub;)V

    .line 2402592
    :cond_0
    new-instance v0, LX/Gti;

    invoke-direct {v0, p0}, LX/Gti;-><init>(LX/Gto;)V

    iput-object v0, p0, LX/Gto;->L:LX/12S;

    .line 2402593
    new-instance v0, LX/Gtj;

    invoke-direct {v0, p0}, LX/Gtj;-><init>(LX/Gto;)V

    iput-object v0, p0, LX/Gto;->O:LX/12I;

    .line 2402594
    return-void
.end method

.method public static J(LX/Gto;)V
    .locals 3

    .prologue
    .line 2402572
    iget-object v0, p0, LX/Gto;->q:LX/0Zb;

    const-string v1, "immersive_activity_create"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2402573
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2402574
    const-string v0, "activity_stack_depth"

    iget-object v2, p0, LX/Gto;->c:LX/0fU;

    invoke-virtual {v2}, LX/0fU;->f()I

    move-result v2

    invoke-virtual {v1, v0, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2402575
    iget-object v0, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v0, v0

    .line 2402576
    invoke-virtual {v0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2402577
    invoke-virtual {p0}, LX/Gto;->B()Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2402578
    invoke-virtual {p0}, LX/Gto;->B()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 2402579
    :cond_0
    const-string v2, "fragment_name"

    invoke-virtual {v1, v2, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2402580
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2402581
    :cond_1
    iget-object v0, p0, LX/Gto;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    .line 2402582
    iget-object v1, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v1, v1

    .line 2402583
    invoke-virtual {v0, v1}, LX/0gh;->a(Landroid/app/Activity;)V

    .line 2402584
    return-void
.end method

.method public static K(LX/Gto;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 2402336
    invoke-direct {p0}, LX/Gto;->L()V

    .line 2402337
    iput-boolean v0, p0, LX/Gto;->G:Z

    .line 2402338
    invoke-direct {p0, v0}, LX/Gto;->e(Z)V

    .line 2402339
    return-void
.end method

.method private L()V
    .locals 3

    .prologue
    .line 2402567
    iget-object v0, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v0, v0

    .line 2402568
    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 2402569
    if-eqz v0, :cond_0

    .line 2402570
    invoke-virtual {p0}, LX/42j;->mt_()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2402571
    :cond_0
    return-void
.end method

.method private M()Z
    .locals 1

    .prologue
    .line 2402566
    invoke-direct {p0}, LX/Gto;->P()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/Gto;->N()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private N()Z
    .locals 2

    .prologue
    .line 2402563
    invoke-direct {p0}, LX/Gto;->O()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/facebook/video/videohome/tab/VideoHomeTab;->l:Lcom/facebook/video/videohome/tab/VideoHomeTab;

    iget-object v0, v0, Lcom/facebook/apptab/state/TabTag;->a:Ljava/lang/String;

    iget-object v1, p0, LX/Gto;->c:LX/0fU;

    .line 2402564
    iget-object p0, v1, LX/0fU;->n:Ljava/lang/String;

    move-object v1, p0

    .line 2402565
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private O()Z
    .locals 1

    .prologue
    .line 2402457
    iget-object v0, p0, LX/Gto;->ag:LX/0xX;

    invoke-virtual {v0}, LX/0xX;->a()Z

    move-result v0

    return v0
.end method

.method private P()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2402555
    invoke-virtual {p0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v1

    .line 2402556
    if-nez v1, :cond_1

    .line 2402557
    :cond_0
    :goto_0
    return v0

    .line 2402558
    :cond_1
    const-string v2, "notification_source"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2402559
    if-eqz v1, :cond_0

    sget-object v2, LX/21C;->PUSH:LX/21C;

    invoke-virtual {v2}, LX/21C;->name()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private Q()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2402527
    iget-object v0, p0, LX/Gto;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/AnM;

    .line 2402528
    iget-object v1, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v1, v1

    .line 2402529
    sget-object v2, LX/0ax;->cL:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/398;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2402530
    :try_start_0
    iget-object v0, p0, LX/Gto;->ah:LX/0go;

    iget-object v1, p0, LX/Gto;->c:LX/0fU;

    .line 2402531
    iget-object v3, v1, LX/0fU;->n:Ljava/lang/String;

    move-object v1, v3

    .line 2402532
    invoke-virtual {v0, v1}, LX/0go;->c(Ljava/lang/String;)Lcom/facebook/apptab/state/TabTag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;
    :try_end_0
    .catch LX/0iF; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v1, v0

    .line 2402533
    :goto_0
    const-string v0, "target_tab_name"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2402534
    const-string v0, "jump_to_top"

    invoke-virtual {v2, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2402535
    const-string v0, "is_from_push_notification"

    invoke-direct {p0}, LX/Gto;->P()Z

    move-result v3

    invoke-virtual {v2, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2402536
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 2402537
    const-string v0, "tabbar_target_intent"

    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2402538
    const-string v0, "extra_launch_uri"

    iget-object v2, p0, LX/Gto;->c:LX/0fU;

    .line 2402539
    iget-object v4, v2, LX/0fU;->n:Ljava/lang/String;

    move-object v2, v4

    .line 2402540
    invoke-virtual {v3, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2402541
    const-string v0, "POP_TO_ROOT"

    invoke-virtual {v3, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2402542
    iget-object v0, p0, LX/Gto;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 2402543
    iget-object v0, p0, LX/Gto;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fJ;

    invoke-interface {v0, v3}, LX/0fJ;->a(Landroid/content/Intent;)V

    .line 2402544
    iget-object v0, p0, LX/Gto;->p:Lcom/facebook/content/SecureContextHelper;

    .line 2402545
    iget-object v2, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v2, v2

    .line 2402546
    invoke-interface {v0, v3, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2402547
    sget-object v0, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2402548
    iget-object v0, p0, LX/Gto;->y:LX/0hv;

    invoke-virtual {v0}, LX/0hv;->b()V

    .line 2402549
    iget-object v0, p0, LX/Gto;->j:LX/0Yi;

    invoke-virtual {v0, v5}, LX/0Yi;->d(Z)V

    .line 2402550
    :cond_0
    invoke-virtual {p0}, LX/42j;->q()V

    .line 2402551
    return-void

    .line 2402552
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2402553
    sget-object v0, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    invoke-virtual {v0}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v0

    .line 2402554
    iget-object v3, p0, LX/Gto;->ai:LX/03V;

    const-string v4, "tab manager"

    invoke-virtual {v3, v4, v1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v0

    goto :goto_0
.end method

.method private R()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x1

    .line 2402514
    invoke-virtual {p0}, LX/GVq;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2402515
    iget-object v0, p0, LX/Gto;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "system_triggered"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    move-result-object v0

    const-string v1, "video"

    invoke-virtual {v0, v1, v6}, LX/0gh;->a(Ljava/lang/String;Z)V

    .line 2402516
    :cond_0
    :goto_0
    return-void

    .line 2402517
    :cond_1
    invoke-virtual {p0}, LX/42j;->iC_()LX/0gc;

    move-result-object v4

    .line 2402518
    if-eqz v4, :cond_0

    .line 2402519
    sget-object v0, LX/Gto;->S:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    const/4 v0, 0x0

    move v3, v0

    move-object v1, v2

    :goto_1
    if-ge v3, v5, :cond_2

    sget-object v0, LX/Gto;->S:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2402520
    invoke-virtual {v4, v0}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 2402521
    if-nez v1, :cond_2

    .line 2402522
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 2402523
    :cond_2
    if-eqz v1, :cond_0

    instance-of v0, v1, LX/0f2;

    if-eqz v0, :cond_0

    .line 2402524
    instance-of v0, v1, LX/0f1;

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 2402525
    check-cast v0, LX/0f1;

    invoke-interface {v0}, LX/0f1;->b()Ljava/util/Map;

    move-result-object v0

    move-object v2, v0

    .line 2402526
    :cond_3
    iget-object v0, p0, LX/Gto;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v3, "system_triggered"

    invoke-virtual {v0, v3}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    move-result-object v0

    check-cast v1, LX/0f2;

    invoke-interface {v1}, LX/0f2;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v6, v2}, LX/0gh;->a(Ljava/lang/String;ZLjava/util/Map;)V

    goto :goto_0
.end method

.method private S()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2402511
    const v0, 0x7f0d002f

    invoke-virtual {p0, v0}, LX/42j;->c(I)Landroid/view/View;

    move-result-object v0

    .line 2402512
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    .line 2402513
    return-void
.end method

.method private T()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2402508
    const v0, 0x7f0d002f

    invoke-virtual {p0, v0}, LX/42j;->c(I)Landroid/view/View;

    move-result-object v0

    .line 2402509
    invoke-virtual {p0}, LX/42j;->y()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0429

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v3, v1, v3, v3}, Landroid/view/View;->setPadding(IIII)V

    .line 2402510
    return-void
.end method

.method private U()V
    .locals 2

    .prologue
    .line 2402503
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, LX/42j;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    .line 2402504
    sget-object v1, LX/0zI;->IMMERSIVE:LX/0zI;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->a(LX/0zI;)V

    .line 2402505
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSearchButtonVisible(Z)V

    .line 2402506
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setButtonSpecs(Ljava/util/List;)V

    .line 2402507
    return-void
.end method

.method private a(J)V
    .locals 6

    .prologue
    .line 2402497
    iget-object v0, p0, LX/Gto;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/performancelogger/PerformanceLogger;

    const v1, 0xa0034

    const-string v2, "NNF_PermalinkFromAndroidNotificationColdLoad"

    invoke-interface {v0, v1, v2}, Lcom/facebook/performancelogger/PerformanceLogger;->h(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2402498
    new-instance v0, LX/0Yj;

    const v1, 0xa0031

    const-string v2, "NNF_PermalinkFromAndroidNotificationWarmLoad"

    invoke-direct {v0, v1, v2}, LX/0Yj;-><init>(ILjava/lang/String;)V

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "story_view"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    iget-object v3, v3, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    iget-object v3, v3, Lcom/facebook/apptab/state/TabTag;->e:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, LX/0Yj;->a([Ljava/lang/String;)LX/0Yj;

    move-result-object v1

    .line 2402499
    iput-wide p1, v1, LX/0Yj;->g:J

    .line 2402500
    iget-object v0, p0, LX/Gto;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/performancelogger/PerformanceLogger;

    invoke-interface {v0, v1}, Lcom/facebook/performancelogger/PerformanceLogger;->c(LX/0Yj;)V

    .line 2402501
    :cond_0
    invoke-direct {p0}, LX/Gto;->I()V

    .line 2402502
    return-void
.end method

.method private a(LX/0Ot;LX/0fU;LX/0Zb;LX/0Or;LX/0Yi;LX/0jo;LX/0Ot;LX/0Ot;Landroid/os/Handler;LX/0gn;LX/0Ot;Lcom/facebook/content/SecureContextHelper;LX/0jM;LX/0iI;LX/0iL;LX/0iM;LX/0iQ;Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;LX/0id;LX/0Ot;LX/0hv;LX/0So;LX/0gy;LX/0gw;LX/0Ot;LX/0jN;LX/11v;LX/0Ot;LX/120;LX/0yH;LX/0Or;LX/1ih;LX/0Ot;LX/0ad;LX/0xX;LX/0go;LX/03V;LX/0Ot;)V
    .locals 1
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentBaseActivity;
        .end annotation
    .end param
    .param p9    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p31    # LX/0Or;
        .annotation runtime Lcom/facebook/zero/common/annotations/IsZeroRatingAvailable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/0oh;",
            ">;",
            "LX/0fU;",
            "LX/0Zb;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/0Yi;",
            "LX/0jo;",
            "LX/0Ot",
            "<",
            "LX/AnM;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FiT;",
            ">;",
            "Landroid/os/Handler;",
            "LX/0gn;",
            "LX/0Ot",
            "<",
            "LX/0fJ;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0jM;",
            "LX/0iI;",
            "LX/0iL;",
            "LX/0iM;",
            "LX/0iQ;",
            "Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;",
            "LX/0id;",
            "LX/0Ot",
            "<",
            "LX/0gh;",
            ">;",
            "LX/0hv;",
            "LX/0So;",
            "LX/0gy;",
            "LX/0gw;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/performancelogger/PerformanceLogger;",
            ">;",
            "LX/0jN;",
            "LX/11v;",
            "LX/0Ot",
            "<",
            "LX/11u;",
            ">;",
            "LX/120;",
            "LX/0yH;",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/1ih;",
            "LX/0Ot",
            "<",
            "LX/0ja;",
            ">;",
            "LX/0ad;",
            "LX/0xX;",
            "LX/0go;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/Gtt;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2402458
    iput-object p1, p0, LX/Gto;->b:LX/0Ot;

    .line 2402459
    iput-object p2, p0, LX/Gto;->c:LX/0fU;

    .line 2402460
    iput-object p3, p0, LX/Gto;->q:LX/0Zb;

    .line 2402461
    iput-object p4, p0, LX/Gto;->e:LX/0Or;

    .line 2402462
    iput-object p5, p0, LX/Gto;->j:LX/0Yi;

    .line 2402463
    iput-object p6, p0, LX/Gto;->i:LX/0jo;

    .line 2402464
    iput-object p7, p0, LX/Gto;->l:LX/0Ot;

    .line 2402465
    iput-object p8, p0, LX/Gto;->m:LX/0Ot;

    .line 2402466
    iput-object p9, p0, LX/Gto;->h:Landroid/os/Handler;

    .line 2402467
    iput-object p10, p0, LX/Gto;->f:LX/0gn;

    .line 2402468
    iput-object p11, p0, LX/Gto;->g:LX/0Ot;

    .line 2402469
    iput-object p12, p0, LX/Gto;->p:Lcom/facebook/content/SecureContextHelper;

    .line 2402470
    iput-object p13, p0, LX/Gto;->s:LX/0jM;

    .line 2402471
    iput-object p14, p0, LX/Gto;->t:LX/0iI;

    .line 2402472
    move-object/from16 v0, p15

    iput-object v0, p0, LX/Gto;->u:LX/0iL;

    .line 2402473
    move-object/from16 v0, p16

    iput-object v0, p0, LX/Gto;->v:LX/0iM;

    .line 2402474
    move-object/from16 v0, p17

    iput-object v0, p0, LX/Gto;->w:LX/0iQ;

    .line 2402475
    move-object/from16 v0, p18

    iput-object v0, p0, LX/Gto;->x:Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;

    .line 2402476
    move-object/from16 v0, p19

    iput-object v0, p0, LX/Gto;->J:LX/0id;

    .line 2402477
    move-object/from16 v0, p20

    iput-object v0, p0, LX/Gto;->r:LX/0Ot;

    .line 2402478
    move-object/from16 v0, p21

    iput-object v0, p0, LX/Gto;->y:LX/0hv;

    .line 2402479
    move-object/from16 v0, p22

    iput-object v0, p0, LX/Gto;->E:LX/0So;

    .line 2402480
    move-object/from16 v0, p23

    iput-object v0, p0, LX/Gto;->z:LX/0gy;

    .line 2402481
    move-object/from16 v0, p24

    iput-object v0, p0, LX/Gto;->A:LX/0gw;

    .line 2402482
    move-object/from16 v0, p25

    iput-object v0, p0, LX/Gto;->k:LX/0Ot;

    .line 2402483
    move-object/from16 v0, p26

    iput-object v0, p0, LX/Gto;->Q:LX/0jN;

    .line 2402484
    move-object/from16 v0, p27

    iput-object v0, p0, LX/Gto;->K:LX/11v;

    .line 2402485
    move-object/from16 v0, p28

    iput-object v0, p0, LX/Gto;->M:LX/0Ot;

    .line 2402486
    move-object/from16 v0, p29

    iput-object v0, p0, LX/Gto;->N:LX/120;

    .line 2402487
    move-object/from16 v0, p30

    iput-object v0, p0, LX/Gto;->P:LX/0yH;

    .line 2402488
    move-object/from16 v0, p31

    iput-object v0, p0, LX/Gto;->W:LX/0Or;

    .line 2402489
    move-object/from16 v0, p32

    iput-object v0, p0, LX/Gto;->X:LX/1ih;

    .line 2402490
    move-object/from16 v0, p33

    iput-object v0, p0, LX/Gto;->af:LX/0Ot;

    .line 2402491
    move-object/from16 v0, p34

    iput-object v0, p0, LX/Gto;->ae:LX/0ad;

    .line 2402492
    move-object/from16 v0, p35

    iput-object v0, p0, LX/Gto;->ag:LX/0xX;

    .line 2402493
    move-object/from16 v0, p36

    iput-object v0, p0, LX/Gto;->ah:LX/0go;

    .line 2402494
    move-object/from16 v0, p37

    iput-object v0, p0, LX/Gto;->ai:LX/03V;

    .line 2402495
    move-object/from16 v0, p38

    iput-object v0, p0, LX/Gto;->n:LX/0Ot;

    .line 2402496
    return-void
.end method

.method private a(LX/0gz;)V
    .locals 5

    .prologue
    .line 2402630
    iget-boolean v0, p0, LX/Gto;->C:Z

    if-nez v0, :cond_1

    .line 2402631
    :cond_0
    :goto_0
    return-void

    .line 2402632
    :cond_1
    iget-object v0, p0, LX/Gto;->B:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 2402633
    if-eqz p1, :cond_0

    .line 2402634
    invoke-virtual {p1}, LX/0gz;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2402635
    iget-object v0, p0, LX/Gto;->B:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2402636
    invoke-virtual {p1}, LX/0gz;->g()I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 2402637
    iget-object v0, p0, LX/Gto;->B:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2402638
    iget-object v0, p0, LX/Gto;->B:Landroid/widget/FrameLayout;

    iget-object v1, p0, LX/Gto;->B:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getPaddingLeft()I

    move-result v1

    iget-object v2, p0, LX/Gto;->B:Landroid/widget/FrameLayout;

    invoke-virtual {v2}, Landroid/widget/FrameLayout;->getPaddingTop()I

    move-result v2

    .line 2402639
    iget v3, p1, LX/0gz;->e:I

    move v3, v3

    .line 2402640
    iget-object v4, p0, LX/Gto;->B:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    goto :goto_0

    .line 2402641
    :cond_2
    iget-object v0, p0, LX/Gto;->B:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Landroid/support/v4/app/Fragment;LX/0gz;I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2402779
    const/4 v0, 0x0

    .line 2402780
    instance-of v1, p1, LX/0fv;

    if-eqz v1, :cond_2

    move-object v0, p1

    .line 2402781
    check-cast v0, LX/0fv;

    invoke-interface {v0}, LX/0fw;->k()LX/0g8;

    move-result-object v0

    .line 2402782
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    instance-of v1, p1, LX/0fw;

    if-eqz v1, :cond_1

    .line 2402783
    check-cast p1, LX/0fw;

    invoke-interface {p1}, LX/0fw;->k()LX/0g8;

    move-result-object v0

    .line 2402784
    :cond_1
    if-eqz v0, :cond_3

    .line 2402785
    invoke-virtual {p0}, LX/42j;->y()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1dbf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 2402786
    iget v2, p2, LX/0gz;->c:I

    move v2, v2

    .line 2402787
    iget v3, p2, LX/0gz;->d:I

    move v3, v3

    .line 2402788
    invoke-interface {v0}, LX/0g8;->e()I

    move-result v4

    invoke-interface {v0, v2, v1, v3, v4}, LX/0g8;->a(IIII)V

    .line 2402789
    invoke-interface {v0, v5}, LX/0g8;->a(Z)V

    .line 2402790
    :goto_1
    return-void

    .line 2402791
    :cond_2
    instance-of v1, p1, LX/0fw;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 2402792
    check-cast v0, LX/0fw;

    invoke-interface {v0}, LX/0fw;->k()LX/0g8;

    move-result-object v0

    goto :goto_0

    .line 2402793
    :cond_3
    iget-object v0, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v0, v0

    .line 2402794
    invoke-virtual {v0, p3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 2402795
    invoke-virtual {p0}, LX/42j;->y()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1dbf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 2402796
    iget v2, p2, LX/0gz;->c:I

    move v2, v2

    .line 2402797
    iget v3, p2, LX/0gz;->d:I

    move v3, v3

    .line 2402798
    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/widget/FrameLayout;->setPadding(IIII)V

    .line 2402799
    invoke-virtual {v0, v5}, Landroid/widget/FrameLayout;->setClipToPadding(Z)V

    goto :goto_1
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 42

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v40

    move-object/from16 v2, p0

    check-cast v2, LX/Gto;

    const/16 v3, 0x2a8

    move-object/from16 v0, v40

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-static/range {v40 .. v40}, LX/0fU;->a(LX/0QB;)LX/0fU;

    move-result-object v4

    check-cast v4, LX/0fU;

    invoke-static/range {v40 .. v40}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v5

    check-cast v5, LX/0Zb;

    const/16 v6, 0xb

    move-object/from16 v0, v40

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static/range {v40 .. v40}, LX/0Yi;->a(LX/0QB;)LX/0Yi;

    move-result-object v7

    check-cast v7, LX/0Yi;

    invoke-static/range {v40 .. v40}, LX/0oR;->a(LX/0QB;)LX/0oR;

    move-result-object v8

    check-cast v8, LX/0jo;

    const/16 v9, 0x1d66

    move-object/from16 v0, v40

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x34e9

    move-object/from16 v0, v40

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static/range {v40 .. v40}, LX/0Ss;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v11

    check-cast v11, Landroid/os/Handler;

    invoke-static/range {v40 .. v40}, LX/0gn;->a(LX/0QB;)LX/0gn;

    move-result-object v12

    check-cast v12, LX/0gn;

    const/16 v13, 0xc16

    move-object/from16 v0, v40

    invoke-static {v0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {v40 .. v40}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v14

    check-cast v14, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {v40 .. v40}, LX/0jM;->a(LX/0QB;)LX/0jM;

    move-result-object v15

    check-cast v15, LX/0jM;

    invoke-static/range {v40 .. v40}, LX/0iI;->a(LX/0QB;)LX/0iI;

    move-result-object v16

    check-cast v16, LX/0iI;

    invoke-static/range {v40 .. v40}, LX/0iL;->a(LX/0QB;)LX/0iL;

    move-result-object v17

    check-cast v17, LX/0iL;

    invoke-static/range {v40 .. v40}, LX/0iM;->a(LX/0QB;)LX/0iM;

    move-result-object v18

    check-cast v18, LX/0iM;

    invoke-static/range {v40 .. v40}, LX/0iQ;->a(LX/0QB;)LX/0iQ;

    move-result-object v19

    check-cast v19, LX/0iQ;

    invoke-static/range {v40 .. v40}, Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;->a(LX/0QB;)Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;

    move-result-object v20

    check-cast v20, Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;

    invoke-static/range {v40 .. v40}, LX/0id;->a(LX/0QB;)LX/0id;

    move-result-object v21

    check-cast v21, LX/0id;

    const/16 v22, 0x97

    move-object/from16 v0, v40

    move/from16 v1, v22

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v22

    invoke-static/range {v40 .. v40}, LX/0hv;->a(LX/0QB;)LX/0hv;

    move-result-object v23

    check-cast v23, LX/0hv;

    invoke-static/range {v40 .. v40}, LX/0Sm;->a(LX/0QB;)Lcom/facebook/common/time/AwakeTimeSinceBootClock;

    move-result-object v24

    check-cast v24, LX/0So;

    invoke-static/range {v40 .. v40}, LX/0gy;->a(LX/0QB;)LX/0gy;

    move-result-object v25

    check-cast v25, LX/0gy;

    invoke-static/range {v40 .. v40}, LX/0gw;->a(LX/0QB;)LX/0gw;

    move-result-object v26

    check-cast v26, LX/0gw;

    const/16 v27, 0xf12

    move-object/from16 v0, v40

    move/from16 v1, v27

    invoke-static {v0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v27

    invoke-static/range {v40 .. v40}, LX/0jN;->a(LX/0QB;)LX/0jN;

    move-result-object v28

    check-cast v28, LX/0jN;

    invoke-static/range {v40 .. v40}, LX/11v;->a(LX/0QB;)LX/11v;

    move-result-object v29

    check-cast v29, LX/11v;

    const/16 v30, 0x2e8

    move-object/from16 v0, v40

    move/from16 v1, v30

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v30

    invoke-static/range {v40 .. v40}, LX/120;->a(LX/0QB;)LX/120;

    move-result-object v31

    check-cast v31, LX/120;

    invoke-static/range {v40 .. v40}, LX/0yH;->a(LX/0QB;)LX/0yH;

    move-result-object v32

    check-cast v32, LX/0yH;

    const/16 v33, 0x387

    move-object/from16 v0, v40

    move/from16 v1, v33

    invoke-static {v0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v33

    invoke-static/range {v40 .. v40}, LX/1ih;->a(LX/0QB;)LX/1ih;

    move-result-object v34

    check-cast v34, LX/1ih;

    const/16 v35, 0x12bd

    move-object/from16 v0, v40

    move/from16 v1, v35

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v35

    invoke-static/range {v40 .. v40}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v36

    check-cast v36, LX/0ad;

    invoke-static/range {v40 .. v40}, LX/0xX;->a(LX/0QB;)LX/0xX;

    move-result-object v37

    check-cast v37, LX/0xX;

    invoke-static/range {v40 .. v40}, LX/0go;->a(LX/0QB;)LX/0go;

    move-result-object v38

    check-cast v38, LX/0go;

    invoke-static/range {v40 .. v40}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v39

    check-cast v39, LX/03V;

    const/16 v41, 0x2594

    invoke-static/range {v40 .. v41}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v40

    invoke-direct/range {v2 .. v40}, LX/Gto;->a(LX/0Ot;LX/0fU;LX/0Zb;LX/0Or;LX/0Yi;LX/0jo;LX/0Ot;LX/0Ot;Landroid/os/Handler;LX/0gn;LX/0Ot;Lcom/facebook/content/SecureContextHelper;LX/0jM;LX/0iI;LX/0iL;LX/0iM;LX/0iQ;Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;LX/0id;LX/0Ot;LX/0hv;LX/0So;LX/0gy;LX/0gw;LX/0Ot;LX/0jN;LX/11v;LX/0Ot;LX/120;LX/0yH;LX/0Or;LX/1ih;LX/0Ot;LX/0ad;LX/0xX;LX/0go;LX/03V;LX/0Ot;)V

    return-void
.end method

.method private a(LX/0yY;)Z
    .locals 2

    .prologue
    .line 2402775
    iget-object v0, p0, LX/Gto;->P:LX/0yH;

    invoke-virtual {v0}, LX/0yH;->b()LX/0yY;

    move-result-object v0

    .line 2402776
    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, LX/0yY;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, LX/0yY;->DIALTONE_MANUAL_SWITCHER_MODE:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yY;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, LX/0yY;->FB4A_INDICATOR:LX/0yY;

    invoke-virtual {v0, v1}, LX/0yY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2402777
    :cond_0
    const/4 v0, 0x1

    .line 2402778
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a$redex0(LX/Gto;ILX/0yY;)V
    .locals 2

    .prologue
    .line 2402768
    invoke-direct {p0, p2}, LX/Gto;->a(LX/0yY;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2402769
    invoke-direct {p0, p1, p2}, LX/Gto;->b(ILX/0yY;)V

    .line 2402770
    sget-object v0, LX/0yY;->FB4A_INDICATOR:LX/0yY;

    invoke-virtual {p2, v0}, LX/0yY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2402771
    iget-object v0, p0, LX/Gto;->M:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11u;

    invoke-virtual {v0}, LX/11u;->b()V

    .line 2402772
    :cond_0
    :goto_0
    return-void

    .line 2402773
    :cond_1
    sget-object v0, LX/0yY;->DIALTONE_MANUAL_SWITCHER_MODE:LX/0yY;

    invoke-virtual {p2, v0}, LX/0yY;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2402774
    iget-object v0, p0, LX/Gto;->M:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11u;

    iget-object v1, p0, LX/Gto;->N:LX/120;

    invoke-virtual {v0, v1}, LX/11u;->a(LX/120;)V

    goto :goto_0
.end method

.method private b(Landroid/support/v4/app/Fragment;)LX/0gz;
    .locals 2

    .prologue
    .line 2402761
    const/4 v0, 0x0

    .line 2402762
    instance-of v1, p1, LX/5vN;

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/Gto;->A:LX/0gw;

    invoke-virtual {v1}, LX/0gw;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2402763
    check-cast p1, LX/5vN;

    invoke-interface {p1}, LX/5vN;->kR_()LX/0cQ;

    move-result-object v0

    iput-object v0, p0, LX/Gto;->D:LX/0cQ;

    .line 2402764
    iget-object v0, p0, LX/Gto;->z:LX/0gy;

    invoke-virtual {v0}, LX/0gy;->b()LX/0gz;

    move-result-object v0

    .line 2402765
    :cond_0
    :goto_0
    return-object v0

    .line 2402766
    :cond_1
    iget-object v1, p0, LX/Gto;->A:LX/0gw;

    invoke-virtual {v1}, LX/0gw;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2402767
    iget-object v0, p0, LX/Gto;->z:LX/0gy;

    invoke-virtual {v0}, LX/0gy;->b()LX/0gz;

    move-result-object v0

    goto :goto_0
.end method

.method private b(ILX/0yY;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2402758
    invoke-direct {p0, p2}, LX/Gto;->a(LX/0yY;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2402759
    :goto_0
    return-void

    .line 2402760
    :cond_0
    const v0, 0x7f0d16fb

    invoke-virtual {p0, v0}, LX/42j;->c(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1, p1, v1, v1}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method

.method private b(J)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 2402756
    iget-object v1, p0, LX/Gto;->E:LX/0So;

    invoke-interface {v1}, LX/0So;->now()J

    move-result-wide v2

    sub-long/2addr v2, p1

    .line 2402757
    iget-object v1, p0, LX/Gto;->ae:LX/0ad;

    sget-short v4, LX/0iR;->b:S

    invoke-interface {v1, v4, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Gto;->ae:LX/0ad;

    sget v4, LX/0iR;->a:I

    const v5, 0x7fffffff

    invoke-interface {v1, v4, v5}, LX/0ad;->a(II)I

    move-result v1

    int-to-long v4, v1

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private c(Landroid/support/v4/app/Fragment;)V
    .locals 3

    .prologue
    .line 2402734
    iget-object v0, p0, LX/Gto;->J:LX/0id;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0id;->o(Ljava/lang/String;)V

    .line 2402735
    const/4 v0, 0x0

    iput-object v0, p0, LX/Gto;->I:Landroid/support/v4/app/Fragment;

    .line 2402736
    const v0, 0x7f0d002f

    iput v0, p0, LX/Gto;->U:I

    .line 2402737
    invoke-direct {p0, p1}, LX/Gto;->b(Landroid/support/v4/app/Fragment;)LX/0gz;

    move-result-object v0

    iput-object v0, p0, LX/Gto;->T:LX/0gz;

    .line 2402738
    iget-object v0, p0, LX/Gto;->T:LX/0gz;

    if-eqz v0, :cond_0

    .line 2402739
    const v0, 0x7f0d11ee

    iput v0, p0, LX/Gto;->U:I

    .line 2402740
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Gto;->C:Z

    .line 2402741
    iget-object v0, p0, LX/Gto;->T:LX/0gz;

    invoke-direct {p0, v0}, LX/Gto;->a(LX/0gz;)V

    .line 2402742
    instance-of v0, p1, LX/63S;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 2402743
    check-cast v0, LX/63S;

    invoke-interface {v0}, LX/63S;->h()V

    .line 2402744
    :cond_0
    iget-object v0, p0, LX/Gto;->d:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->a()LX/0hH;

    move-result-object v0

    .line 2402745
    iget v1, p0, LX/Gto;->U:I

    invoke-virtual {v0, v1, p1}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 2402746
    iget-boolean v1, p0, LX/Gto;->C:Z

    if-eqz v1, :cond_1

    .line 2402747
    iget-object v1, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v1, v1

    .line 2402748
    const-class v2, Lcom/facebook/tablet/sideshow/SideshowHost;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 2402749
    const v2, 0x7f0d11ef

    invoke-virtual {v0, v2, v1}, LX/0hH;->b(ILandroid/support/v4/app/Fragment;)LX/0hH;

    .line 2402750
    :cond_1
    invoke-virtual {v0}, LX/0hH;->b()I

    .line 2402751
    iget-object v0, p0, LX/Gto;->d:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->b()Z

    .line 2402752
    iget-boolean v0, p0, LX/Gto;->C:Z

    if-eqz v0, :cond_2

    .line 2402753
    iget-object v0, p0, LX/Gto;->T:LX/0gz;

    iget v1, p0, LX/Gto;->U:I

    invoke-direct {p0, p1, v0, v1}, LX/Gto;->a(Landroid/support/v4/app/Fragment;LX/0gz;I)V

    .line 2402754
    :cond_2
    iget-object v0, p0, LX/Gto;->J:LX/0id;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0id;->p(Ljava/lang/String;)V

    .line 2402755
    return-void
.end method

.method private d(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2402731
    invoke-direct {p0, p1}, LX/Gto;->e(Landroid/content/Intent;)Ljava/lang/Runnable;

    move-result-object v0

    .line 2402732
    invoke-virtual {p0}, LX/Gtg;->G()Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-result-object v1

    new-instance v2, LX/Gtn;

    invoke-direct {v2, p0, v0}, LX/Gtn;-><init>(LX/Gto;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/CustomLinearLayout;->a(LX/10U;)V

    .line 2402733
    return-void
.end method

.method public static synthetic e(LX/Gto;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 2402729
    iget-object v0, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v0, v0

    .line 2402730
    return-object v0
.end method

.method private e(Landroid/content/Intent;)Ljava/lang/Runnable;
    .locals 5
    .param p1    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 2402604
    iget-object v0, p0, LX/Gto;->f:LX/0gn;

    invoke-virtual {v0, p1}, LX/0gn;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v1

    .line 2402605
    iget-object v0, p0, LX/Gto;->f:LX/0gn;

    .line 2402606
    iget-object v2, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v2, v2

    .line 2402607
    invoke-virtual {v0, v2, v1}, LX/0gn;->a(Landroid/app/Activity;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2402608
    const/4 v0, 0x0

    .line 2402609
    :goto_0
    return-object v0

    .line 2402610
    :cond_0
    const-string v0, "target_tab_name"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Gto;->ah:LX/0go;

    const-string v2, "target_tab_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0go;->b(Ljava/lang/String;)Lcom/facebook/apptab/state/TabTag;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2402611
    :try_start_0
    iget-object v0, p0, LX/Gto;->ah:LX/0go;

    const-string v2, "target_tab_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0go;->a(Ljava/lang/String;)Lcom/facebook/apptab/state/TabTag;
    :try_end_0
    .catch LX/0iF; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2402612
    :goto_1
    iget-object v2, p0, LX/Gto;->c:LX/0fU;

    iget-object v3, v0, Lcom/facebook/apptab/state/TabTag;->a:Ljava/lang/String;

    .line 2402613
    iput-object v3, v2, LX/0fU;->n:Ljava/lang/String;

    .line 2402614
    sget-object v2, Lcom/facebook/video/videohome/tab/VideoHomeTab;->l:Lcom/facebook/video/videohome/tab/VideoHomeTab;

    if-ne v0, v2, :cond_1

    .line 2402615
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Gto;->H:Z

    .line 2402616
    :cond_1
    const-string v0, "notification_launch_source"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2402617
    iget-object v0, p0, LX/Gto;->q:LX/0Zb;

    const-string v2, "notification_launched"

    invoke-interface {v0, v2, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2402618
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2402619
    const-string v2, "notification_launch_source"

    const-string v3, "notification_launch_source"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2402620
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2402621
    :cond_2
    const-string v0, "is_from_get_notified_notification"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2402622
    iget-object v0, p0, LX/Gto;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v2, "tap_system_tray_notification"

    invoke-virtual {v0, v2}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2402623
    :cond_3
    const-string v0, "inflate_fragment_before_animation"

    invoke-virtual {v1, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 2402624
    if-nez v0, :cond_4

    invoke-virtual {v1}, Landroid/content/Intent;->getFlags()I

    move-result v0

    const/high16 v2, 0x10000

    and-int/2addr v0, v2

    if-nez v0, :cond_4

    .line 2402625
    new-instance v0, Lcom/facebook/katana/activity/ImmersiveActivityDelegate$8;

    invoke-direct {v0, p0, v1}, Lcom/facebook/katana/activity/ImmersiveActivityDelegate$8;-><init>(LX/Gto;Landroid/content/Intent;)V

    goto :goto_0

    .line 2402626
    :catch_0
    sget-object v0, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    goto :goto_1

    .line 2402627
    :cond_4
    invoke-static {p0, v1}, LX/Gto;->f(LX/Gto;Landroid/content/Intent;)V

    .line 2402628
    invoke-static {p0}, LX/Gto;->J(LX/Gto;)V

    .line 2402629
    new-instance v0, Lcom/facebook/katana/activity/ImmersiveActivityDelegate$9;

    invoke-direct {v0, p0}, Lcom/facebook/katana/activity/ImmersiveActivityDelegate$9;-><init>(LX/Gto;)V

    goto/16 :goto_0
.end method

.method private e(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2402697
    invoke-virtual {p0, p1}, LX/GVq;->d(Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2402698
    :cond_0
    :goto_0
    return-void

    .line 2402699
    :cond_1
    iget-boolean v0, p0, LX/Gto;->Y:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, LX/Gto;->Z:Z

    if-nez v0, :cond_0

    .line 2402700
    :cond_2
    const-string v1, "immersive_system_back_pressed"

    .line 2402701
    iget-object v0, p0, LX/Gto;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v2, "tap_back_button"

    invoke-virtual {v0, v2}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2402702
    iget-boolean v0, p0, LX/Gto;->G:Z

    if-eqz v0, :cond_a

    .line 2402703
    iget-object v0, p0, LX/Gto;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "tap_top_left_nav"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2402704
    const-string v0, "titlebar_back_pressed"

    .line 2402705
    iput-boolean v3, p0, LX/Gto;->G:Z

    .line 2402706
    :goto_1
    iget-object v1, p0, LX/Gto;->q:LX/0Zb;

    invoke-interface {v1, v0, v3}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2402707
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2402708
    const-string v1, "activity_stack_depth"

    iget-object v2, p0, LX/Gto;->c:LX/0fU;

    invoke-virtual {v2}, LX/0fU;->f()I

    move-result v2

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2402709
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2402710
    :cond_3
    invoke-direct {p0}, LX/Gto;->M()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2402711
    iget-object v0, p0, LX/Gto;->c:LX/0fU;

    sget-object v1, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    iget-object v1, v1, Lcom/facebook/apptab/state/TabTag;->a:Ljava/lang/String;

    .line 2402712
    iput-object v1, v0, LX/0fU;->n:Ljava/lang/String;

    .line 2402713
    :cond_4
    invoke-virtual {p0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_5

    .line 2402714
    sget-object v0, LX/Gto;->a:Ljava/lang/Class;

    const-string v1, "Activity with no stored intent being processed as an Immersive Activity."

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2402715
    invoke-direct {p0}, LX/Gto;->Q()V

    .line 2402716
    :cond_5
    iget-boolean v0, p0, LX/Gto;->H:Z

    if-eqz v0, :cond_6

    .line 2402717
    invoke-direct {p0}, LX/Gto;->Q()V

    .line 2402718
    :cond_6
    invoke-virtual {p0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "extra_back_to_home"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 2402719
    invoke-direct {p0}, LX/Gto;->Q()V

    .line 2402720
    :cond_7
    :goto_2
    iget-object v0, p0, LX/Gto;->c:LX/0fU;

    .line 2402721
    iget-object v1, v0, LX/0fU;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_b

    const/4 v1, 0x1

    :goto_3
    move v0, v1

    .line 2402722
    if-eqz v0, :cond_8

    iget-object v0, p0, LX/Gto;->c:LX/0fU;

    .line 2402723
    iget-object v1, v0, LX/0fU;->n:Ljava/lang/String;

    move-object v0, v1

    .line 2402724
    sget-object v1, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    iget-object v1, v1, Lcom/facebook/apptab/state/TabTag;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2402725
    iget-object v0, p0, LX/Gto;->j:LX/0Yi;

    invoke-virtual {v0, v3}, LX/0Yi;->d(Z)V

    .line 2402726
    :cond_8
    invoke-super {p0}, LX/Gtg;->s()V

    goto/16 :goto_0

    .line 2402727
    :cond_9
    invoke-virtual {p0}, LX/42j;->iC_()LX/0gc;

    move-result-object v0

    invoke-virtual {v0}, LX/0gc;->f()I

    move-result v0

    if-nez v0, :cond_7

    iget-boolean v0, p0, LX/Gto;->F:Z

    if-eqz v0, :cond_7

    invoke-virtual {p0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "target_tab_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, LX/Gto;->ah:LX/0go;

    invoke-virtual {p0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "target_tab_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0go;->b(Ljava/lang/String;)Lcom/facebook/apptab/state/TabTag;

    move-result-object v0

    instance-of v0, v0, Lcom/facebook/notifications/tab/NotificationsTab;

    if-eqz v0, :cond_7

    .line 2402728
    invoke-direct {p0}, LX/Gto;->Q()V

    goto :goto_2

    :cond_a
    move-object v0, v1

    goto/16 :goto_1

    :cond_b
    const/4 v1, 0x0

    goto :goto_3
.end method

.method public static f(LX/Gto;Landroid/content/Intent;)V
    .locals 8
    .param p0    # LX/Gto;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v6, 0x1

    const/4 v1, -0x1

    const/4 v5, 0x0

    .line 2402660
    const-string v0, "target_fragment"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2402661
    if-ne v0, v1, :cond_0

    .line 2402662
    sget-object v0, LX/0cQ;->FACEWEB_FRAGMENT:LX/0cQ;

    invoke-virtual {v0}, LX/0cQ;->ordinal()I

    move-result v0

    .line 2402663
    const-string v1, "target_fragment"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_0
    move v2, v0

    .line 2402664
    iget-object v0, p0, LX/Gto;->i:LX/0jo;

    invoke-interface {v0, v2}, LX/0jo;->a(I)LX/0jq;

    move-result-object v1

    .line 2402665
    if-nez v1, :cond_2

    .line 2402666
    invoke-static {v2}, LX/0jj;->a(I)LX/0cQ;

    move-result-object v0

    .line 2402667
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v3, "An IFragmentFactory is not available for fragmentType=%s (id=%d)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    if-nez v0, :cond_1

    const-string v0, "null"

    :goto_0
    aput-object v0, v4, v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    invoke-virtual {v0}, LX/0cQ;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2402668
    :cond_2
    const-class v0, LX/0jp;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 2402669
    check-cast v0, LX/0jp;

    iget-object v3, p0, LX/Gto;->b:LX/0Ot;

    invoke-interface {v0, v3}, LX/0jp;->a(LX/0Ot;)V

    .line 2402670
    :cond_3
    const-string v0, "extra_launch_uri"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2402671
    iget-object v3, p0, LX/Gto;->J:LX/0id;

    .line 2402672
    iget-object v4, v3, LX/0id;->i:Ljava/lang/String;

    if-eqz v4, :cond_8

    if-eqz v0, :cond_8

    iget-object v4, v3, LX/0id;->i:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 2402673
    invoke-virtual {v3}, LX/0id;->a()V

    .line 2402674
    :goto_1
    sget-object v3, LX/0cQ;->FACEWEB_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_4

    if-eqz v0, :cond_4

    .line 2402675
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2402676
    invoke-virtual {v3}, Landroid/net/Uri;->getQueryParameterNames()Ljava/util/Set;

    move-result-object v3

    const-string v4, "dismiss"

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2402677
    invoke-direct {p0}, LX/Gto;->Q()V

    .line 2402678
    :goto_2
    return-void

    .line 2402679
    :cond_4
    sget-object v3, LX/0cQ;->SEARCH_FRAGMENT:LX/0cQ;

    invoke-virtual {v3}, LX/0cQ;->ordinal()I

    move-result v3

    if-ne v2, v3, :cond_5

    .line 2402680
    const-string v2, "config"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2402681
    const-string v3, "awareness_tutorial_nux"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2402682
    const-string v2, "search_awareness_config"

    new-instance v3, Lcom/facebook/search/model/awareness/AwarenessGraphSearchConfiguration;

    sget-object v4, LX/Cwm;->TUTORIAL_NUX:LX/Cwm;

    invoke-direct {v3, v4}, Lcom/facebook/search/model/awareness/AwarenessGraphSearchConfiguration;-><init>(LX/Cwm;)V

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2402683
    :cond_5
    const-string v2, "arg_is_checkpoint"

    invoke-virtual {p1, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, LX/Gto;->Y:Z

    .line 2402684
    const-string v2, "arg_is_blocking_checkpoint"

    invoke-virtual {p1, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, LX/Gto;->Z:Z

    .line 2402685
    invoke-interface {v1, p1}, LX/0jq;->a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 2402686
    if-nez v1, :cond_6

    .line 2402687
    invoke-virtual {p0}, LX/42j;->q()V

    .line 2402688
    iget-object v0, p0, LX/Gto;->J:LX/0id;

    invoke-virtual {v0}, LX/0id;->a()V

    goto :goto_2

    .line 2402689
    :cond_6
    iget-object v2, p0, LX/Gto;->J:LX/0id;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    .line 2402690
    const v3, 0x4c000b

    invoke-static {v2, v3, v0}, LX/0id;->a(LX/0id;ILjava/lang/String;)V

    .line 2402691
    iget-object v0, p0, LX/Gto;->x:Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;

    invoke-virtual {v0}, Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;->a()V

    .line 2402692
    invoke-virtual {v1, v6}, Landroid/support/v4/app/Fragment;->setUserVisibleHint(Z)V

    .line 2402693
    iget-object v0, p0, LX/Gto;->d:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->c()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2402694
    invoke-direct {p0, v1}, LX/Gto;->c(Landroid/support/v4/app/Fragment;)V

    goto :goto_2

    .line 2402695
    :cond_7
    iput-object v1, p0, LX/Gto;->I:Landroid/support/v4/app/Fragment;

    goto :goto_2

    .line 2402696
    :cond_8
    const v7, 0x4c000b

    if-nez v0, :cond_9

    const/4 v4, 0x0

    :goto_3
    invoke-static {v3, v7, v0, v4}, LX/0id;->a(LX/0id;ILjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_9
    invoke-static {v0}, LX/0id;->t(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_3
.end method

.method private f(Z)V
    .locals 2

    .prologue
    .line 2402644
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 2402645
    iput-boolean p1, p0, LX/Gto;->ab:Z

    .line 2402646
    if-eqz p1, :cond_1

    .line 2402647
    invoke-virtual {p0}, LX/42j;->mt_()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x500

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 2402648
    new-instance v0, LX/42f;

    .line 2402649
    iget-object v1, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v1, v1

    .line 2402650
    invoke-direct {v0, v1}, LX/42f;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, LX/Gto;->ac:LX/42f;

    .line 2402651
    iget-object v0, p0, LX/Gto;->ac:LX/42f;

    .line 2402652
    new-instance v1, LX/42e;

    invoke-direct {v1, v0}, LX/42e;-><init>(LX/42f;)V

    iput-object v1, v0, LX/42f;->f:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 2402653
    iget-object v1, v0, LX/42f;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    iget-object p0, v0, LX/42f;->f:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2402654
    iget-object v1, v0, LX/42f;->a:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout$LayoutParams;

    iput-object v1, v0, LX/42f;->c:Landroid/widget/FrameLayout$LayoutParams;

    .line 2402655
    :cond_0
    :goto_0
    return-void

    .line 2402656
    :cond_1
    iget-object v0, p0, LX/Gto;->ac:LX/42f;

    if-eqz v0, :cond_2

    .line 2402657
    iget-object v0, p0, LX/Gto;->ac:LX/42f;

    invoke-virtual {v0}, LX/42f;->b()V

    .line 2402658
    const/4 v0, 0x0

    iput-object v0, p0, LX/Gto;->ac:LX/42f;

    .line 2402659
    :cond_2
    invoke-virtual {p0}, LX/42j;->mt_()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x100

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_0
.end method

.method private g(I)V
    .locals 2

    .prologue
    .line 2402560
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 2402561
    invoke-virtual {p0}, LX/42j;->mt_()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 2402562
    :cond_0
    return-void
.end method

.method private g(Landroid/content/Intent;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2402642
    const-string v2, "force_create_new_activity"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "force_create_new_activity"

    invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2402643
    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string v2, "extra_launch_uri"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "extra_launch_uri"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/CharSequence;

    const-string v3, "extra_launch_uri"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "extra_launch_uri"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v2}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "extra_launch_uri"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v3

    const-string v4, "extra_launch_uri"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public static synthetic h(LX/Gto;)Landroid/app/Activity;
    .locals 1

    .prologue
    .line 2402323
    iget-object v0, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v0, v0

    .line 2402324
    return-object v0
.end method


# virtual methods
.method public final B()Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 2402322
    invoke-virtual {p0}, LX/42j;->iC_()LX/0gc;

    move-result-object v0

    iget v1, p0, LX/Gto;->U:I

    invoke-virtual {v0, v1}, LX/0gc;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public final C()V
    .locals 1

    .prologue
    .line 2402313
    iget-object v0, p0, LX/Gto;->t:LX/0iI;

    .line 2402314
    iput-object v0, p0, LX/GVq;->d:LX/0iI;

    .line 2402315
    iget-object v0, p0, LX/Gto;->u:LX/0iL;

    .line 2402316
    iput-object v0, p0, LX/GVq;->e:LX/0iL;

    .line 2402317
    iget-object v0, p0, LX/Gto;->v:LX/0iM;

    .line 2402318
    iput-object v0, p0, LX/GVq;->f:LX/0iM;

    .line 2402319
    iget-object v0, p0, LX/Gto;->w:LX/0iQ;

    .line 2402320
    iput-object v0, p0, LX/GVq;->g:LX/0iQ;

    .line 2402321
    return-void
.end method

.method public final D()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2402285
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, LX/42j;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    .line 2402286
    iput-object v0, p0, LX/GVq;->h:LX/0h5;

    .line 2402287
    iget-object v1, p0, LX/Gtg;->a:LX/0zF;

    .line 2402288
    iput-object v0, v1, LX/0zF;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2402289
    invoke-virtual {v0, v4}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setHasBackButton(Z)V

    .line 2402290
    new-instance v1, LX/Gtk;

    invoke-direct {v1, p0}, LX/Gtk;-><init>(LX/Gto;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Landroid/view/View$OnClickListener;)V

    .line 2402291
    invoke-virtual {p0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "title_bar_background_color_id"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 2402292
    if-lez v1, :cond_0

    .line 2402293
    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->setBackgroundResource(I)V

    .line 2402294
    :cond_0
    invoke-virtual {p0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "title_bar_primary_button_spec"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2402295
    invoke-virtual {p0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "title_bar_secondary_button_spec"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2402296
    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2402297
    invoke-virtual {v0, v2}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSecondaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2402298
    invoke-virtual {p0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "target_fragment"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    sget-object v2, LX/0cQ;->SEARCH_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    if-ne v1, v2, :cond_2

    .line 2402299
    invoke-virtual {p0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "search_titles_app_diable_animation"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2402300
    invoke-virtual {v0}, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    new-instance v2, LX/Gtl;

    invoke-direct {v2, p0, v0}, LX/Gtl;-><init>(LX/Gto;Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2402301
    :cond_1
    :goto_0
    return-void

    .line 2402302
    :cond_2
    new-instance v1, LX/Gtm;

    invoke-direct {v1, p0}, LX/Gtm;-><init>(LX/Gto;)V

    .line 2402303
    iput-object v1, v0, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->k:Landroid/view/View$OnClickListener;

    .line 2402304
    invoke-virtual {p0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "title_bar_search_button_visible"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 2402305
    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSearchButtonVisible(Z)V

    .line 2402306
    invoke-virtual {p0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v1

    .line 2402307
    const-string v2, "show_next_notification"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    move v1, v2

    .line 2402308
    if-eqz v1, :cond_1

    .line 2402309
    iget-object v1, p0, LX/Gto;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Gtt;

    .line 2402310
    iget-object v2, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v2, v2

    .line 2402311
    invoke-virtual {p0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v1, v2, p0, v0, v3}, LX/Gtt;->a(Landroid/content/Context;LX/Gto;Lcom/facebook/ui/titlebar/Fb4aTitleBar;Landroid/content/Intent;)V

    .line 2402312
    iput-boolean v5, p0, LX/Gto;->o:Z

    goto :goto_0
.end method

.method public final H()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 2402248
    invoke-virtual {p0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v0

    .line 2402249
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_1

    .line 2402250
    :cond_0
    :goto_0
    return-void

    .line 2402251
    :cond_1
    iget-object v1, p0, LX/Gto;->c:LX/0fU;

    invoke-virtual {v1, v6}, LX/0fU;->a(Z)J

    move-result-wide v2

    .line 2402252
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 2402253
    const-string v1, "target_tab_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    invoke-virtual {v1}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v1

    const-string v4, "target_tab_name"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2402254
    iput-boolean v6, p0, LX/Gto;->H:Z

    .line 2402255
    iget-object v0, p0, LX/Gto;->c:LX/0fU;

    iget-object v1, p0, LX/Gto;->ah:LX/0go;

    .line 2402256
    invoke-virtual {v1}, LX/0go;->a()Lcom/facebook/apptab/state/NavigationConfig;

    move-result-object v2

    .line 2402257
    iget-object v3, v2, Lcom/facebook/apptab/state/NavigationConfig;->a:LX/0Px;

    sget-object v4, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    invoke-virtual {v3, v4}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2402258
    sget-object v2, Lcom/facebook/notifications/tab/NotificationsFriendingTab;->l:Lcom/facebook/notifications/tab/NotificationsFriendingTab;

    .line 2402259
    :goto_1
    move-object v1, v2

    .line 2402260
    iget-object v1, v1, Lcom/facebook/apptab/state/TabTag;->a:Ljava/lang/String;

    .line 2402261
    iput-object v1, v0, LX/0fU;->n:Ljava/lang/String;

    .line 2402262
    iget-object v0, p0, LX/Gto;->c:LX/0fU;

    .line 2402263
    iget-object v1, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v1, v1

    .line 2402264
    invoke-virtual {v0, v1}, LX/0fU;->e(Landroid/app/Activity;)V

    goto :goto_0

    .line 2402265
    :cond_2
    const-string v1, "target_tab_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    sget-object v1, Lcom/facebook/video/videohome/tab/VideoHomeTab;->l:Lcom/facebook/video/videohome/tab/VideoHomeTab;

    invoke-virtual {v1}, Lcom/facebook/apptab/state/TabTag;->a()Ljava/lang/String;

    move-result-object v1

    const-string v4, "target_tab_name"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2402266
    iput-boolean v6, p0, LX/Gto;->H:Z

    .line 2402267
    iget-object v0, p0, LX/Gto;->c:LX/0fU;

    sget-object v1, Lcom/facebook/video/videohome/tab/VideoHomeTab;->l:Lcom/facebook/video/videohome/tab/VideoHomeTab;

    iget-object v1, v1, Lcom/facebook/apptab/state/TabTag;->a:Ljava/lang/String;

    .line 2402268
    iput-object v1, v0, LX/0fU;->n:Ljava/lang/String;

    .line 2402269
    iget-object v0, p0, LX/Gto;->c:LX/0fU;

    .line 2402270
    iget-object v1, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v1, v1

    .line 2402271
    invoke-virtual {v0, v1}, LX/0fU;->e(Landroid/app/Activity;)V

    goto :goto_0

    .line 2402272
    :cond_3
    invoke-direct {p0, v2, v3}, LX/Gto;->b(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2402273
    iput-boolean v6, p0, LX/Gto;->H:Z

    .line 2402274
    iget-object v0, p0, LX/Gto;->c:LX/0fU;

    .line 2402275
    iget-object v1, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v1, v1

    .line 2402276
    invoke-virtual {v0, v1}, LX/0fU;->e(Landroid/app/Activity;)V

    .line 2402277
    iget-object v0, p0, LX/Gto;->c:LX/0fU;

    sget-object v1, Lcom/facebook/feed/tab/FeedTab;->l:Lcom/facebook/feed/tab/FeedTab;

    iget-object v1, v1, Lcom/facebook/apptab/state/TabTag;->a:Ljava/lang/String;

    .line 2402278
    iput-object v1, v0, LX/0fU;->n:Ljava/lang/String;

    .line 2402279
    goto/16 :goto_0

    .line 2402280
    :cond_4
    iget-object v3, v2, Lcom/facebook/apptab/state/NavigationConfig;->a:LX/0Px;

    sget-object v4, Lcom/facebook/notifications/tab/NotificationsEastTab;->l:Lcom/facebook/notifications/tab/NotificationsEastTab;

    invoke-virtual {v3, v4}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2402281
    sget-object v2, Lcom/facebook/notifications/tab/NotificationsEastTab;->l:Lcom/facebook/notifications/tab/NotificationsEastTab;

    goto :goto_1

    .line 2402282
    :cond_5
    iget-object v2, v2, Lcom/facebook/apptab/state/NavigationConfig;->a:LX/0Px;

    sget-object v3, Lcom/facebook/notifications/tab/NotificationsJapanTab;->l:Lcom/facebook/notifications/tab/NotificationsJapanTab;

    invoke-virtual {v2, v3}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2402283
    sget-object v2, Lcom/facebook/notifications/tab/NotificationsJapanTab;->l:Lcom/facebook/notifications/tab/NotificationsJapanTab;

    goto :goto_1

    .line 2402284
    :cond_6
    sget-object v2, Lcom/facebook/notifications/tab/NotificationsTab;->m:Lcom/facebook/notifications/tab/NotificationsTab;

    goto :goto_1
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 2402243
    invoke-super {p0, p1, p2, p3}, LX/Gtg;->a(IILandroid/content/Intent;)V

    .line 2402244
    invoke-virtual {p0}, LX/Gto;->B()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2402245
    if-eqz v0, :cond_0

    .line 2402246
    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 2402247
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2402240
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, LX/Gto;->a(J)V

    .line 2402241
    invoke-super {p0, p1}, LX/Gtg;->a(Landroid/content/Intent;)V

    .line 2402242
    return-void
.end method

.method public final a(Landroid/content/Intent;I)V
    .locals 3

    .prologue
    .line 2402228
    iget-object v0, p0, LX/Gto;->J:LX/0id;

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 2402229
    :goto_0
    if-eqz v0, :cond_0

    .line 2402230
    iget-object v1, p0, LX/Gto;->J:LX/0id;

    .line 2402231
    iget-object v2, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v2, v2

    .line 2402232
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0id;->s(Ljava/lang/String;)V

    .line 2402233
    :cond_0
    invoke-super {p0, p1, p2}, LX/Gtg;->a(Landroid/content/Intent;I)V

    .line 2402234
    if-eqz v0, :cond_1

    .line 2402235
    iget-object v0, p0, LX/Gto;->J:LX/0id;

    .line 2402236
    iget-object v1, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v1, v1

    .line 2402237
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/0id;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 2402238
    :cond_1
    return-void

    .line 2402239
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 3

    .prologue
    .line 2402216
    invoke-super {p0, p1}, LX/Gtg;->a(Landroid/content/res/Configuration;)V

    .line 2402217
    iget-boolean v0, p0, LX/Gto;->R:Z

    if-nez v0, :cond_1

    .line 2402218
    invoke-direct {p0}, LX/Gto;->T()V

    .line 2402219
    :goto_0
    invoke-virtual {p0}, LX/Gto;->B()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-direct {p0, v0}, LX/Gto;->b(Landroid/support/v4/app/Fragment;)LX/0gz;

    move-result-object v0

    iput-object v0, p0, LX/Gto;->T:LX/0gz;

    .line 2402220
    iget-object v0, p0, LX/Gto;->T:LX/0gz;

    if-eqz v0, :cond_2

    .line 2402221
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Gto;->C:Z

    .line 2402222
    invoke-virtual {p0}, LX/Gto;->B()Landroid/support/v4/app/Fragment;

    move-result-object v0

    iget-object v1, p0, LX/Gto;->T:LX/0gz;

    const v2, 0x7f0d11ee

    invoke-direct {p0, v0, v1, v2}, LX/Gto;->a(Landroid/support/v4/app/Fragment;LX/0gz;I)V

    .line 2402223
    iget-object v0, p0, LX/Gto;->T:LX/0gz;

    invoke-direct {p0, v0}, LX/Gto;->a(LX/0gz;)V

    .line 2402224
    :cond_0
    :goto_1
    return-void

    .line 2402225
    :cond_1
    invoke-direct {p0}, LX/Gto;->S()V

    goto :goto_0

    .line 2402226
    :cond_2
    iget-object v0, p0, LX/Gto;->B:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 2402227
    iget-object v0, p0, LX/Gto;->B:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_1
.end method

.method public final a(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V
    .locals 2

    .prologue
    .line 2402206
    invoke-virtual {p1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2402207
    iget-object v0, p0, LX/Gto;->V:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2402208
    iget-object v0, p0, LX/Gto;->V:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/fab/FabView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/fab/FabView;->setVisibility(I)V

    .line 2402209
    :cond_0
    :goto_0
    return-void

    .line 2402210
    :cond_1
    iget-object v0, p0, LX/Gto;->V:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/fab/FabView;

    .line 2402211
    const v1, 0x7f020886

    move v1, v1

    .line 2402212
    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/fab/FabView;->setGlyphDrawableID(I)V

    .line 2402213
    iget-object v0, p0, LX/Gto;->V:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/fab/FabView;

    invoke-virtual {p1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->d()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/fab/FabView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2402214
    iget-object v0, p0, LX/Gto;->V:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/fab/FabView;

    invoke-virtual {p1}, Lcom/facebook/friending/jewel/FriendRequestsFragment;->e()Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/fab/FabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2402215
    iget-object v0, p0, LX/Gto;->V:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/fab/FabView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/fab/FabView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a_(Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Intent;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    .line 2402202
    invoke-direct {p0, p1}, LX/Gto;->e(Landroid/content/Intent;)Ljava/lang/Runnable;

    move-result-object v0

    .line 2402203
    if-nez v0, :cond_0

    .line 2402204
    :goto_0
    return-void

    .line 2402205
    :cond_0
    iget-object v1, p0, LX/Gto;->h:Landroid/os/Handler;

    const v2, 0x232d29ee

    invoke-static {v1, v0, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 5
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v4, 0x0

    .line 2402178
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 2402179
    invoke-super {p0, p1}, LX/Gtg;->b(Landroid/os/Bundle;)V

    .line 2402180
    iget-object v1, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v1, v1

    .line 2402181
    invoke-static {p0, v1}, LX/Gto;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2402182
    iget-object v0, p0, LX/Gto;->A:LX/0gw;

    invoke-virtual {v0}, LX/0gw;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Gto;->A:LX/0gw;

    invoke-virtual {v0}, LX/0gw;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2402183
    :cond_0
    const v0, 0x7f0308dc

    invoke-virtual {p0, v0}, LX/42j;->e(I)V

    .line 2402184
    const v0, 0x7f0d11ef

    invoke-virtual {p0, v0}, LX/42j;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/Gto;->B:Landroid/widget/FrameLayout;

    .line 2402185
    :goto_0
    invoke-virtual {p0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "fragment_background_color_id"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2402186
    if-eqz v0, :cond_1

    .line 2402187
    const v1, 0x7f0d002f

    invoke-virtual {p0, v1}, LX/42j;->c(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 2402188
    :cond_1
    invoke-virtual {p0}, LX/42j;->iC_()LX/0gc;

    move-result-object v0

    iput-object v0, p0, LX/Gto;->d:LX/0gc;

    .line 2402189
    if-eqz p1, :cond_4

    .line 2402190
    const-string v0, "last_on_stack"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/Gto;->F:Z

    .line 2402191
    invoke-direct {p0}, LX/Gto;->I()V

    .line 2402192
    :goto_1
    iget-object v0, p0, LX/Gto;->c:LX/0fU;

    invoke-virtual {v0}, LX/0fU;->f()I

    move-result v0

    if-nez v0, :cond_2

    .line 2402193
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Gto;->F:Z

    .line 2402194
    :cond_2
    iget-object v0, p0, LX/Gto;->c:LX/0fU;

    new-instance v1, LX/Gth;

    invoke-direct {v1, p0}, LX/Gth;-><init>(LX/Gto;)V

    .line 2402195
    iput-object v1, v0, LX/0fU;->l:LX/0i3;

    .line 2402196
    new-instance v1, LX/0zw;

    const v0, 0x7f0d03af

    invoke-virtual {p0, v0}, LX/42j;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, LX/Gto;->V:LX/0zw;

    .line 2402197
    iget-object v0, p0, LX/Gto;->J:LX/0id;

    sget-object v1, LX/Gto;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v3}, LX/0id;->b(Ljava/lang/String;J)V

    .line 2402198
    const v0, 0x7f0d00bc

    invoke-virtual {p0, v0}, LX/42j;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    iput-object v0, p0, LX/Gto;->aa:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    .line 2402199
    return-void

    .line 2402200
    :cond_3
    const v0, 0x7f0308db

    invoke-virtual {p0, v0}, LX/42j;->e(I)V

    goto :goto_0

    .line 2402201
    :cond_4
    invoke-direct {p0, v2, v3}, LX/Gto;->a(J)V

    goto :goto_1
.end method

.method public final b(Lcom/facebook/friending/jewel/FriendRequestsFragment;)V
    .locals 2

    .prologue
    .line 2402175
    iget-object v0, p0, LX/Gto;->V:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2402176
    iget-object v0, p0, LX/Gto;->V:LX/0zw;

    invoke-virtual {v0}, LX/0zw;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/uicontrib/fab/FabView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/uicontrib/fab/FabView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2402177
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 2402172
    iget-boolean v0, p0, LX/Gto;->R:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 2402173
    invoke-direct {p0}, LX/Gto;->U()V

    .line 2402174
    :cond_0
    return-void
.end method

.method public final c()Lcom/facebook/base/fragment/FbFragment;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2402170
    invoke-virtual {p0}, LX/Gto;->B()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 2402171
    instance-of v1, v0, Lcom/facebook/base/fragment/FbFragment;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/facebook/base/fragment/FbFragment;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2402131
    iget-object v1, p0, LX/Gto;->J:LX/0id;

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    move v1, v0

    .line 2402132
    :goto_0
    if-eqz v1, :cond_0

    .line 2402133
    iget-object v0, p0, LX/Gto;->J:LX/0id;

    .line 2402134
    iget-object v2, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v2, v2

    .line 2402135
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/0id;->q(Ljava/lang/String;)V

    .line 2402136
    :cond_0
    :try_start_0
    invoke-direct {p0, p1}, LX/Gto;->g(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/Gto;->s:LX/0jM;

    if-eqz v0, :cond_4

    iget-object v0, p0, LX/Gto;->s:LX/0jM;

    .line 2402137
    iget-object v2, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v2, v2

    .line 2402138
    const/4 v3, 0x0

    invoke-virtual {v0, v2, p1, v3}, LX/0jM;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/view/View;)Z

    move-result v3

    move v0, v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2402139
    if-eqz v0, :cond_4

    .line 2402140
    :cond_1
    if-eqz v1, :cond_2

    .line 2402141
    iget-object v0, p0, LX/Gto;->J:LX/0id;

    .line 2402142
    iget-object v1, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v1, v1

    .line 2402143
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0id;->r(Ljava/lang/String;)V

    .line 2402144
    :cond_2
    :goto_1
    return-void

    :cond_3
    move v1, v0

    .line 2402145
    goto :goto_0

    .line 2402146
    :cond_4
    :try_start_1
    iget-object v0, p0, LX/Gto;->x:Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;

    if-eqz v0, :cond_5

    .line 2402147
    iget-object v0, p0, LX/Gto;->x:Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;

    invoke-virtual {v0, p1}, Lcom/facebook/perf/MainActivityToFragmentCreatePerfLogger;->a(Landroid/content/Intent;)V

    .line 2402148
    :cond_5
    iget-object v0, p0, LX/Gto;->Q:LX/0jN;

    if-eqz v0, :cond_6

    if-eqz p1, :cond_6

    .line 2402149
    iget-object v0, p0, LX/Gto;->Q:LX/0jN;

    invoke-virtual {v0, p1}, LX/0jN;->a(Landroid/content/Intent;)V

    .line 2402150
    :cond_6
    invoke-super {p0, p1}, LX/Gtg;->c(Landroid/content/Intent;)V

    .line 2402151
    invoke-virtual {p1}, Landroid/content/Intent;->getFlags()I

    move-result v0

    const/high16 v2, 0x10000

    and-int/2addr v0, v2

    if-eqz v0, :cond_8

    .line 2402152
    iget-object v0, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v0, v0

    .line 2402153
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 2402154
    if-eqz v1, :cond_7

    .line 2402155
    iget-object v0, p0, LX/Gto;->J:LX/0id;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0id;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2402156
    :cond_7
    :goto_2
    if-eqz v1, :cond_2

    .line 2402157
    iget-object v0, p0, LX/Gto;->J:LX/0id;

    .line 2402158
    iget-object v1, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v1, v1

    .line 2402159
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0id;->r(Ljava/lang/String;)V

    goto :goto_1

    .line 2402160
    :cond_8
    :try_start_2
    sget-object v0, LX/0jO;->SLIDE_LEFT_IN:LX/0jO;

    iget v0, v0, LX/0jO;->resource:I

    .line 2402161
    sget-object v2, LX/0jO;->DROP_OUT:LX/0jO;

    iget v2, v2, LX/0jO;->resource:I

    .line 2402162
    iget-object v3, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v3, v3

    .line 2402163
    invoke-virtual {v3, v0, v2}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 2402164
    if-eqz v1, :cond_7

    .line 2402165
    iget-object v2, p0, LX/Gto;->J:LX/0id;

    invoke-virtual {v2, v0}, LX/0id;->a(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 2402166
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_9

    .line 2402167
    iget-object v1, p0, LX/Gto;->J:LX/0id;

    .line 2402168
    iget-object v2, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v2, v2

    .line 2402169
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0id;->r(Ljava/lang/String;)V

    :cond_9
    throw v0
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2402360
    invoke-super {p0, p1}, LX/Gtg;->c(Landroid/os/Bundle;)V

    .line 2402361
    if-nez p1, :cond_0

    .line 2402362
    invoke-virtual {p0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, LX/Gto;->d(Landroid/content/Intent;)V

    .line 2402363
    :cond_0
    return-void
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 2402454
    if-eqz p1, :cond_0

    .line 2402455
    invoke-virtual {p0}, LX/42j;->q()V

    .line 2402456
    :cond_0
    return-void
.end method

.method public final d(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2402450
    iget-boolean v0, p0, LX/Gto;->F:Z

    if-eqz v0, :cond_0

    .line 2402451
    const-string v0, "last_on_stack"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2402452
    :cond_0
    invoke-super {p0, p1}, LX/Gtg;->d(Landroid/os/Bundle;)V

    .line 2402453
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2402441
    iget-object v0, p0, LX/Gto;->J:LX/0id;

    sget-object v1, LX/Gto;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0id;->c(Ljava/lang/String;)V

    .line 2402442
    invoke-super {p0}, LX/Gtg;->e()V

    .line 2402443
    invoke-virtual {p0}, LX/GVq;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2402444
    iget-object v0, p0, LX/GVq;->c:LX/0hE;

    move-object v0, v0

    .line 2402445
    invoke-interface {v0}, LX/0hE;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2402446
    iget-object v0, p0, LX/GVq;->c:LX/0hE;

    move-object v0, v0

    .line 2402447
    invoke-interface {v0}, LX/0hE;->e()V

    .line 2402448
    :cond_0
    iget-object v0, p0, LX/Gto;->J:LX/0id;

    sget-object v1, LX/Gto;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0id;->d(Ljava/lang/String;)V

    .line 2402449
    return-void
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 2402422
    iget-object v0, p0, LX/Gto;->J:LX/0id;

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    .line 2402423
    :goto_0
    if-eqz v0, :cond_0

    .line 2402424
    iget-object v1, p0, LX/Gto;->J:LX/0id;

    .line 2402425
    iget-object v2, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v2, v2

    .line 2402426
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/0id;->i(Ljava/lang/String;)V

    .line 2402427
    :cond_0
    iget-boolean v1, p0, LX/Gto;->Y:Z

    if-eqz v1, :cond_1

    .line 2402428
    iget-object v1, p0, LX/Gto;->X:LX/1ih;

    invoke-virtual {v1}, LX/1ih;->c()V

    .line 2402429
    :cond_1
    invoke-super {p0}, LX/Gtg;->h()V

    .line 2402430
    invoke-virtual {p0}, LX/GVq;->E()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2402431
    iget-object v1, p0, LX/GVq;->c:LX/0hE;

    move-object v1, v1

    .line 2402432
    invoke-interface {v1}, LX/0hE;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2402433
    iget-object v1, p0, LX/GVq;->c:LX/0hE;

    move-object v1, v1

    .line 2402434
    invoke-interface {v1}, LX/0hE;->h()V

    .line 2402435
    :cond_2
    if-eqz v0, :cond_3

    .line 2402436
    iget-object v0, p0, LX/Gto;->J:LX/0id;

    .line 2402437
    iget-object v1, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v1, v1

    .line 2402438
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0id;->j(Ljava/lang/String;)V

    .line 2402439
    :cond_3
    return-void

    .line 2402440
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 2402402
    iget-object v0, p0, LX/Gto;->J:LX/0id;

    .line 2402403
    iget-object v1, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v1, v1

    .line 2402404
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0id;->g(Ljava/lang/String;)V

    .line 2402405
    invoke-super {p0}, LX/Gtg;->j()V

    .line 2402406
    invoke-virtual {p0}, LX/GVq;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2402407
    iget-object v0, p0, LX/GVq;->c:LX/0hE;

    move-object v0, v0

    .line 2402408
    invoke-interface {v0}, LX/0hE;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2402409
    iget-object v0, p0, LX/GVq;->c:LX/0hE;

    move-object v0, v0

    .line 2402410
    invoke-interface {v0}, LX/0hE;->g()V

    .line 2402411
    :cond_0
    iget-object v0, p0, LX/Gto;->K:LX/11v;

    invoke-virtual {v0}, LX/11v;->b()V

    .line 2402412
    iget-object v0, p0, LX/Gto;->K:LX/11v;

    const/4 v1, 0x0

    .line 2402413
    iput-object v1, v0, LX/11v;->p:LX/12S;

    .line 2402414
    iget-object v0, p0, LX/Gto;->N:LX/120;

    invoke-virtual {v0}, LX/120;->b()V

    .line 2402415
    iget-object v0, p0, LX/Gto;->N:LX/120;

    iget-object v1, p0, LX/Gto;->O:LX/12I;

    invoke-virtual {v0, v1}, LX/120;->b(LX/12I;)V

    .line 2402416
    iget-object v0, p0, LX/Gto;->P:LX/0yH;

    invoke-virtual {v0}, LX/0yH;->b()LX/0yY;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2402417
    iget-object v0, p0, LX/Gto;->M:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11u;

    invoke-virtual {v0}, LX/11u;->e()V

    .line 2402418
    :cond_1
    iget-object v0, p0, LX/Gto;->J:LX/0id;

    .line 2402419
    iget-object v1, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v1, v1

    .line 2402420
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0id;->h(Ljava/lang/String;)V

    .line 2402421
    return-void
.end method

.method public final j_(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2402388
    iput-boolean p1, p0, LX/Gto;->R:Z

    .line 2402389
    if-eqz p1, :cond_0

    const v0, 0x7f010293

    .line 2402390
    :goto_0
    iget-boolean v1, p0, LX/Gto;->ad:Z

    if-nez v1, :cond_1

    .line 2402391
    iget-object v1, p0, LX/Gto;->aa:Lcom/facebook/ui/titlebar/search/Fb4aSearchTitleBar;

    invoke-virtual {v1, p1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->a(Z)V

    .line 2402392
    invoke-direct {p0, p1}, LX/Gto;->f(Z)V

    .line 2402393
    iget-object v1, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v1, v1

    .line 2402394
    invoke-static {v1, v0, v2}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v0

    invoke-direct {p0, v0}, LX/Gto;->g(I)V

    .line 2402395
    :goto_1
    if-eqz p1, :cond_2

    .line 2402396
    invoke-direct {p0}, LX/Gto;->S()V

    .line 2402397
    :goto_2
    return-void

    .line 2402398
    :cond_0
    const v0, 0x7f010297

    goto :goto_0

    .line 2402399
    :cond_1
    iget-object v0, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v0, v0

    .line 2402400
    const v1, 0x7f010297

    invoke-static {v0, v1, v2}, LX/0WH;->c(Landroid/content/Context;II)I

    move-result v0

    invoke-direct {p0, v0}, LX/Gto;->g(I)V

    goto :goto_1

    .line 2402401
    :cond_2
    invoke-direct {p0}, LX/Gto;->T()V

    goto :goto_2
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 2402364
    iget-object v0, p0, LX/Gto;->J:LX/0id;

    sget-object v1, LX/Gto;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0id;->e(Ljava/lang/String;)V

    .line 2402365
    invoke-super {p0}, LX/Gtg;->k()V

    .line 2402366
    invoke-virtual {p0}, LX/GVq;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2402367
    iget-object v0, p0, LX/GVq;->c:LX/0hE;

    move-object v0, v0

    .line 2402368
    invoke-interface {v0}, LX/0hE;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2402369
    iget-object v0, p0, LX/GVq;->c:LX/0hE;

    move-object v0, v0

    .line 2402370
    invoke-interface {v0}, LX/0hE;->f()V

    .line 2402371
    :cond_0
    invoke-virtual {p0}, LX/Gto;->B()Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2402372
    invoke-static {p0}, LX/Gto;->J(LX/Gto;)V

    .line 2402373
    :cond_1
    invoke-direct {p0}, LX/Gto;->R()V

    .line 2402374
    iget-object v0, p0, LX/Gto;->I:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Gto;->d:LX/0gc;

    invoke-virtual {v0}, LX/0gc;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2402375
    iget-object v0, p0, LX/Gto;->I:Landroid/support/v4/app/Fragment;

    invoke-direct {p0, v0}, LX/Gto;->c(Landroid/support/v4/app/Fragment;)V

    .line 2402376
    :cond_2
    iget-object v0, p0, LX/Gto;->K:LX/11v;

    iget-object v1, p0, LX/Gto;->L:LX/12S;

    .line 2402377
    iput-object v1, v0, LX/11v;->p:LX/12S;

    .line 2402378
    move-object v0, v0

    .line 2402379
    sget-object v1, LX/0yY;->FB4A_INDICATOR:LX/0yY;

    .line 2402380
    iput-object v1, v0, LX/11v;->q:LX/0yY;

    .line 2402381
    iget-object v0, p0, LX/Gto;->K:LX/11v;

    invoke-virtual {v0}, LX/11v;->a()V

    .line 2402382
    iget-object v0, p0, LX/Gto;->N:LX/120;

    iget-object v1, p0, LX/Gto;->O:LX/12I;

    invoke-virtual {v0, v1}, LX/120;->a(LX/12I;)V

    .line 2402383
    iget-object v0, p0, LX/Gto;->N:LX/120;

    invoke-virtual {v0}, LX/120;->a()V

    .line 2402384
    iget-object v0, p0, LX/Gto;->P:LX/0yH;

    invoke-virtual {v0}, LX/0yH;->b()LX/0yY;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2402385
    iget-object v0, p0, LX/Gto;->M:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11u;

    invoke-virtual {v0}, LX/11u;->c()V

    .line 2402386
    :cond_3
    iget-object v0, p0, LX/Gto;->J:LX/0id;

    sget-object v1, LX/Gto;->a:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0id;->f(Ljava/lang/String;)V

    .line 2402387
    return-void
.end method

.method public final mr_()V
    .locals 0

    .prologue
    .line 2402325
    invoke-virtual {p0}, LX/42j;->q()V

    .line 2402326
    return-void
.end method

.method public final n()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2402349
    iget-object v0, p0, LX/Gto;->c:LX/0fU;

    if-eqz v0, :cond_0

    .line 2402350
    iget-object v0, p0, LX/Gto;->c:LX/0fU;

    invoke-virtual {v0}, LX/0fU;->b()V

    .line 2402351
    :cond_0
    iget-boolean v0, p0, LX/Gto;->ab:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Gto;->ac:LX/42f;

    if-eqz v0, :cond_1

    .line 2402352
    iget-object v0, p0, LX/Gto;->ac:LX/42f;

    invoke-virtual {v0}, LX/42f;->b()V

    .line 2402353
    iput-object v1, p0, LX/Gto;->ac:LX/42f;

    .line 2402354
    :cond_1
    iput-object v1, p0, LX/Gto;->B:Landroid/widget/FrameLayout;

    .line 2402355
    iget-boolean v0, p0, LX/Gto;->o:Z

    if-eqz v0, :cond_2

    .line 2402356
    iget-object v0, p0, LX/Gto;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gtt;

    .line 2402357
    iget-object v1, v0, LX/Gtt;->m:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v1}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->a()LX/2kW;

    move-result-object v1

    iget-object v2, v0, LX/Gtt;->r:LX/95a;

    invoke-virtual {v1, v2}, LX/2kW;->b(LX/1vq;)V

    .line 2402358
    :cond_2
    invoke-super {p0}, LX/Gtg;->n()V

    .line 2402359
    return-void
.end method

.method public final q()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2402342
    invoke-super {p0}, LX/Gtg;->q()V

    .line 2402343
    invoke-virtual {p0}, LX/42j;->mu_()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "search_titles_app_diable_animation"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2402344
    iget-object v0, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v0, v0

    .line 2402345
    invoke-virtual {v0, v2, v2}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 2402346
    :goto_0
    return-void

    .line 2402347
    :cond_0
    iget-object v0, p0, LX/42j;->a:Landroid/app/Activity;

    move-object v0, v0

    .line 2402348
    sget-object v1, LX/0jO;->RISE_IN:LX/0jO;

    iget v1, v1, LX/0jO;->resource:I

    sget-object v2, LX/0jO;->SLIDE_RIGHT_OUT:LX/0jO;

    iget v2, v2, LX/0jO;->resource:I

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->overridePendingTransition(II)V

    goto :goto_0
.end method

.method public final s()V
    .locals 1

    .prologue
    .line 2402340
    const/4 v0, 0x0

    invoke-direct {p0, v0}, LX/Gto;->e(Z)V

    .line 2402341
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2402332
    const-string v0, "No content fragment loaded"

    .line 2402333
    invoke-virtual {p0}, LX/Gto;->B()Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2402334
    invoke-virtual {p0}, LX/Gto;->B()Landroid/support/v4/app/Fragment;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 2402335
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ImmersiveActivity["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final v()V
    .locals 1

    .prologue
    .line 2402329
    iget-object v0, p0, LX/Gto;->af:LX/0Ot;

    if-eqz v0, :cond_0

    .line 2402330
    iget-object v0, p0, LX/Gto;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ja;

    invoke-virtual {v0}, LX/0ja;->a()V

    .line 2402331
    :cond_0
    return-void
.end method

.method public final w()LX/0cQ;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2402328
    iget-object v0, p0, LX/Gto;->D:LX/0cQ;

    return-object v0
.end method

.method public final x()LX/0gz;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2402327
    iget-object v0, p0, LX/Gto;->T:LX/0gz;

    return-object v0
.end method
