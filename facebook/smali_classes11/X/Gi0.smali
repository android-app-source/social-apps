.class public final LX/Gi0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;

.field public final b:[I


# direct methods
.method public constructor <init>(Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;LX/0Px;LX/0Px;Landroid/content/Context;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLInterfaces$ReactionUnitGametimeTableComponentFragment$TypedData;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 2385159
    iput-object p1, p0, LX/Gi0;->a:Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2385160
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 2385161
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0050

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2385162
    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    move v1, v0

    .line 2385163
    :goto_0
    new-array v0, v1, [I

    iput-object v0, p0, LX/Gi0;->b:[I

    move v4, v3

    .line 2385164
    :goto_1
    if-ge v4, v1, :cond_3

    .line 2385165
    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2385166
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;->c:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2385167
    iget-object v2, p0, LX/Gi0;->b:[I

    invoke-virtual {p2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    aput v0, v2, v4

    .line 2385168
    :cond_0
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v6

    move v2, v3

    :goto_2
    if-ge v2, v6, :cond_2

    invoke-virtual {p3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeTableComponentFragmentModel$TypedDataModel;

    .line 2385169
    invoke-virtual {p4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    sget v8, Lcom/facebook/gametime/ui/components/partdefinition/table/GametimeTableUnitComponentPartDefinition;->d:I

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v7

    invoke-virtual {v5, v7}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2385170
    iget-object v7, p0, LX/Gi0;->b:[I

    iget-object v8, p0, LX/Gi0;->b:[I

    aget v8, v8, v4

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeTableComponentFragmentModel$TypedDataModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v0

    float-to-int v0, v0

    invoke-static {v8, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    aput v0, v7, v4

    .line 2385171
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 2385172
    :cond_1
    invoke-virtual {p3, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeTableComponentFragmentModel$TypedDataModel;

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$ReactionUnitGametimeTableComponentFragmentModel$TypedDataModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    move v1, v0

    goto :goto_0

    .line 2385173
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 2385174
    :cond_3
    return-void
.end method
