.class public final LX/FkR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;)V
    .locals 0

    .prologue
    .line 2278565
    iput-object p1, p0, LX/FkR;->a:Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 6

    .prologue
    .line 2278566
    iget-object v0, p0, LX/FkR;->a:Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2278567
    iget v1, v0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->c:I

    iget-object v4, v0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->f:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v4}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v4

    sub-int v4, v1, v4

    .line 2278568
    if-ltz v4, :cond_1

    .line 2278569
    iget-object v1, v0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->d:LX/1ZF;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2278570
    iget-boolean v5, v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    move v5, v5

    .line 2278571
    iget v1, v0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->c:I

    if-eq v4, v1, :cond_2

    move v1, v2

    :goto_0
    xor-int/2addr v1, v5

    if-eqz v1, :cond_0

    .line 2278572
    iget-object v5, v0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    iget v1, v0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->c:I

    if-eq v4, v1, :cond_3

    move v1, v2

    .line 2278573
    :goto_1
    iput-boolean v1, v5, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->u:Z

    .line 2278574
    iget-object v1, v0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->d:LX/1ZF;

    iget-object v5, v0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-interface {v1, v5}, LX/1ZF;->a(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2278575
    :cond_0
    iget-object v1, v0, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiaryOtherInputFragment;->g:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p0, 0x7f0f0165

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v2, v3

    invoke-virtual {v5, p0, v4, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2278576
    :cond_1
    return-void

    :cond_2
    move v1, v3

    .line 2278577
    goto :goto_0

    :cond_3
    move v1, v3

    .line 2278578
    goto :goto_1
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2278579
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2278580
    return-void
.end method
