.class public LX/Ga8;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/GZO;

.field public final b:LX/0TD;

.field private c:Ljava/lang/String;

.field public d:LX/03R;

.field public e:Z

.field public f:LX/Ga6;


# direct methods
.method public constructor <init>(LX/GZO;LX/0TD;)V
    .locals 1
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2367965
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2367966
    sget-object v0, LX/03R;->UNSET:LX/03R;

    iput-object v0, p0, LX/Ga8;->d:LX/03R;

    .line 2367967
    iput-object p1, p0, LX/Ga8;->a:LX/GZO;

    .line 2367968
    iput-object p2, p0, LX/Ga8;->b:LX/0TD;

    .line 2367969
    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 2367987
    iget-object v0, p0, LX/Ga8;->a:LX/GZO;

    iget-object v1, p0, LX/Ga8;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, LX/GZO;->a(Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2367988
    new-instance v1, LX/Ga7;

    invoke-direct {v1, p0, p1}, LX/Ga7;-><init>(LX/Ga8;Z)V

    iget-object v2, p0, LX/Ga8;->b:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2367989
    return-void
.end method

.method public static b(LX/0QB;)LX/Ga8;
    .locals 4

    .prologue
    .line 2367982
    new-instance v2, LX/Ga8;

    .line 2367983
    new-instance v0, LX/GZO;

    const/16 v1, 0x542

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v1

    const/16 v3, 0x1a1

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    invoke-direct {v0, v1, v3}, LX/GZO;-><init>(LX/0Or;LX/0Ot;)V

    .line 2367984
    move-object v0, v0

    .line 2367985
    check-cast v0, LX/GZO;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, LX/0TD;

    invoke-direct {v2, v0, v1}, LX/Ga8;-><init>(LX/GZO;LX/0TD;)V

    .line 2367986
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;ZZ)V
    .locals 2

    .prologue
    .line 2367975
    iput-object p1, p0, LX/Ga8;->c:Ljava/lang/String;

    .line 2367976
    iput-boolean p3, p0, LX/Ga8;->e:Z

    .line 2367977
    iget-object v0, p0, LX/Ga8;->d:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-ne v0, v1, :cond_0

    .line 2367978
    invoke-static {p2}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iput-object v0, p0, LX/Ga8;->d:LX/03R;

    .line 2367979
    :cond_0
    iget-object v0, p0, LX/Ga8;->f:LX/Ga6;

    if-eqz v0, :cond_1

    .line 2367980
    iget-object v0, p0, LX/Ga8;->f:LX/Ga6;

    invoke-interface {v0}, LX/Ga6;->a()V

    .line 2367981
    :cond_1
    return-void
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 2367974
    iget-object v0, p0, LX/Ga8;->c:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Ga8;->d:LX/03R;

    sget-object v1, LX/03R;->UNSET:LX/03R;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 2367973
    iget-object v0, p0, LX/Ga8;->d:LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2367970
    iget-object v0, p0, LX/Ga8;->d:LX/03R;

    sget-object v1, LX/03R;->YES:LX/03R;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, LX/Ga8;->a(Z)V

    .line 2367971
    return-void

    .line 2367972
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
