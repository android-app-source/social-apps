.class public LX/GAM;
.super LX/GAA;
.source ""


# instance fields
.field public final error:LX/GAF;


# direct methods
.method public constructor <init>(LX/GAF;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2325037
    invoke-direct {p0, p2}, LX/GAA;-><init>(Ljava/lang/String;)V

    .line 2325038
    iput-object p1, p0, LX/GAM;->error:LX/GAF;

    .line 2325039
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2325040
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{FacebookServiceException: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "httpResponseCode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/GAM;->error:LX/GAF;

    .line 2325041
    iget v2, v1, LX/GAF;->c:I

    move v1, v2

    .line 2325042
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", facebookErrorCode: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/GAM;->error:LX/GAF;

    .line 2325043
    iget v2, v1, LX/GAF;->d:I

    move v1, v2

    .line 2325044
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", facebookErrorType: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/GAM;->error:LX/GAF;

    .line 2325045
    iget-object v2, v1, LX/GAF;->f:Ljava/lang/String;

    move-object v1, v2

    .line 2325046
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", message: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/GAM;->error:LX/GAF;

    invoke-virtual {v1}, LX/GAF;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
