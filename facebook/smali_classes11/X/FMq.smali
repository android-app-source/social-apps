.class public LX/FMq;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/2uq;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/FMu;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/FMw;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Or;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Uq;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0SG;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1rd;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FN6;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2231068
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2231069
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2231070
    iput-object v0, p0, LX/FMq;->e:LX/0Ot;

    .line 2231071
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2231072
    iput-object v0, p0, LX/FMq;->f:LX/0Ot;

    .line 2231073
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2231074
    iput-object v0, p0, LX/FMq;->g:LX/0Ot;

    .line 2231075
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2231076
    iput-object v0, p0, LX/FMq;->h:LX/0Ot;

    .line 2231077
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2231078
    iput-object v0, p0, LX/FMq;->i:LX/0Ot;

    .line 2231079
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2231080
    iput-object v0, p0, LX/FMq;->j:LX/0Ot;

    .line 2231081
    return-void
.end method

.method private static a(LX/FMq;Landroid/net/Uri;LX/FMM;Ljava/lang/String;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V
    .locals 4
    .param p2    # LX/FMM;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2231082
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.messaging.sms.MESSAGE_SENT"

    iget-object v2, p0, LX/FMq;->a:Landroid/content/Context;

    const-class v3, LX/FMm;

    invoke-direct {v0, v1, p1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 2231083
    invoke-static {p3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2231084
    const-string v1, "mmssms_quickfail_msg"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2231085
    :cond_0
    const-string v1, "mmssms_quickfail_type"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 2231086
    invoke-static {v0, p4}, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a(Landroid/content/Intent;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V

    .line 2231087
    iget-object v1, p0, LX/FMq;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2231088
    return-void
.end method

.method private static a(LX/FMq;LX/FMM;)Z
    .locals 3

    .prologue
    .line 2231089
    iget-object v0, p0, LX/FMq;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Uq;

    .line 2231090
    const-string v1, "android_messenger_sms_takeover_rollout"

    invoke-static {v0, v1}, LX/2Uq;->a(LX/2Uq;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2231091
    iget-object v1, v0, LX/2Uq;->a:LX/0ad;

    sget-char v2, LX/6jD;->ab:C

    const-string p0, "None"

    invoke-interface {v1, v2, p0}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2231092
    :goto_0
    move-object v0, v1

    .line 2231093
    if-eqz v0, :cond_0

    const-string v1, "None"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2231094
    :cond_0
    const/4 v0, 0x0

    .line 2231095
    :goto_1
    return v0

    .line 2231096
    :cond_1
    const-string v1, "All"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2231097
    const/4 v0, 0x1

    goto :goto_1

    .line 2231098
    :cond_2
    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, LX/FMM;->name()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V
    .locals 14

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2231099
    iget-object v0, p1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2231100
    invoke-static {v0}, LX/FMQ;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 2231101
    iget-boolean v0, p1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->f:Z

    move v0, v0

    .line 2231102
    if-eqz v0, :cond_0

    .line 2231103
    sget-object v1, LX/FMM;->EXPIRED_MESSAGE:LX/FMM;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "age: "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, LX/FMq;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v6

    .line 2231104
    iget-wide v12, p1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->c:J

    move-wide v8, v12

    .line 2231105
    sub-long/2addr v6, v8

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v4, v1, v0, p1}, LX/FMq;->a(LX/FMq;Landroid/net/Uri;LX/FMM;Ljava/lang/String;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V

    .line 2231106
    :goto_0
    return-void

    .line 2231107
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-lt v0, v1, :cond_1

    .line 2231108
    iget v0, p1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->e:I

    move v0, v0

    .line 2231109
    if-nez v0, :cond_1

    move v0, v5

    .line 2231110
    :goto_1
    if-nez v0, :cond_2

    iget-object v0, p0, LX/FMq;->a:Landroid/content/Context;

    invoke-static {v0}, LX/FNi;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2231111
    sget-object v0, LX/FMM;->NO_CONNECTION:LX/FMM;

    invoke-static {p0, v4, v0, v3, p1}, LX/FMq;->a(LX/FMq;Landroid/net/Uri;LX/FMM;Ljava/lang/String;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V

    goto :goto_0

    :cond_1
    move v0, v6

    .line 2231112
    goto :goto_1

    .line 2231113
    :cond_2
    iget-object v0, p0, LX/FMq;->a:Landroid/content/Context;

    invoke-static {v0}, LX/Edh;->a(Landroid/content/Context;)LX/Edh;

    move-result-object v2

    .line 2231114
    invoke-virtual {v2, v4}, LX/Edh;->a(Landroid/net/Uri;)LX/EdM;

    move-result-object v0

    check-cast v0, LX/Edn;

    .line 2231115
    if-nez v0, :cond_3

    .line 2231116
    iget-object v0, p0, LX/FMq;->d:LX/FMw;

    .line 2231117
    iget-object v1, p1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2231118
    iget-wide v12, p1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->b:J

    move-wide v2, v12

    .line 2231119
    invoke-virtual {v0, v1, v2, v3}, LX/FMw;->b(Ljava/lang/String;J)V

    goto :goto_0

    .line 2231120
    :cond_3
    iget-object v1, v0, LX/EdV;->b:LX/EdY;

    move-object v7, v1

    .line 2231121
    invoke-virtual {v7, v6}, LX/EdY;->a(I)LX/Edg;

    move-result-object v1

    invoke-virtual {v1}, LX/Edg;->i()[B

    move-result-object v8

    .line 2231122
    if-nez v8, :cond_5

    const-string v1, ""

    .line 2231123
    :goto_2
    invoke-virtual {v7}, LX/EdY;->b()I

    move-result v8

    if-ne v8, v5, :cond_4

    const-string v8, "sticker:"

    invoke-virtual {v1, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2231124
    :try_start_0
    iget-object v0, p0, LX/FMq;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;

    const/16 v8, 0x8

    invoke-virtual {v1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->a(Ljava/lang/String;)LX/Edg;

    move-result-object v0

    .line 2231125
    invoke-virtual {v7, v0}, LX/EdY;->a(LX/Edg;)Z

    .line 2231126
    invoke-static {v4}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v8

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v8, v9, v1}, LX/Edh;->a(LX/Edg;JLjava/util/Map;)Landroid/net/Uri;

    .line 2231127
    invoke-virtual {v2, v4}, LX/Edh;->a(Landroid/net/Uri;)LX/EdM;

    move-result-object v0

    check-cast v0, LX/Edn;
    :try_end_0
    .catch LX/EdT; {:try_start_0 .. :try_end_0} :catch_0

    .line 2231128
    :cond_4
    invoke-virtual {v0}, LX/EdV;->e()J

    move-result-wide v8

    iget-object v1, p0, LX/FMq;->c:LX/FMu;

    .line 2231129
    iget v2, p1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->h:I

    move v2, v2

    .line 2231130
    invoke-virtual {v1, v2}, LX/FMu;->b(I)I

    move-result v1

    int-to-long v10, v1

    cmp-long v1, v8, v10

    if-lez v1, :cond_6

    .line 2231131
    sget-object v1, LX/FMM;->OVERSIZE:LX/FMM;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "size: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/EdV;->e()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v4, v1, v0, p1}, LX/FMq;->a(LX/FMq;Landroid/net/Uri;LX/FMM;Ljava/lang/String;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V

    goto/16 :goto_0

    .line 2231132
    :cond_5
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v8}, Ljava/lang/String;-><init>([B)V

    goto :goto_2

    .line 2231133
    :catch_0
    move-exception v0

    .line 2231134
    sget-object v1, LX/FMM;->STICKER_FAIL:LX/FMM;

    invoke-virtual {v0}, LX/EdT;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v4, v1, v0, p1}, LX/FMq;->a(LX/FMq;Landroid/net/Uri;LX/FMM;Ljava/lang/String;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V

    goto/16 :goto_0

    .line 2231135
    :cond_6
    :try_start_1
    iget-object v1, p0, LX/FMq;->a:Landroid/content/Context;

    invoke-static {v1, v0}, LX/FMY;->a(Landroid/content/Context;LX/EdM;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v2

    .line 2231136
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.messaging.sms.MESSAGE_SENT"

    iget-object v7, p0, LX/FMq;->a:Landroid/content/Context;

    const-class v8, LX/FMm;

    invoke-direct {v0, v1, v4, v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/content/Context;Ljava/lang/Class;)V

    .line 2231137
    const-string v1, "content_uri"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2231138
    invoke-static {v0, p1}, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->a(Landroid/content/Intent;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V

    .line 2231139
    iget-object v1, p0, LX/FMq;->a:Landroid/content/Context;

    const/high16 v4, 0x8000000

    invoke-static {v1, v6, v0, v4}, LX/0nt;->b(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 2231140
    iget-object v0, p0, LX/FMq;->b:LX/2uq;

    invoke-virtual {v0}, LX/2uq;->a()V

    .line 2231141
    iget v0, p1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->e:I

    move v0, v0

    .line 2231142
    if-lez v0, :cond_7

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x17

    if-ge v0, v1, :cond_7

    .line 2231143
    iget-object v0, p1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->g:LX/FMM;

    move-object v0, v0

    .line 2231144
    invoke-static {p0, v0}, LX/FMq;->a(LX/FMq;LX/FMM;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2231145
    :goto_3
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    .line 2231146
    iget-object v0, p0, LX/FMq;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1rd;

    .line 2231147
    iget v1, p1, Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;->h:I

    move v6, v1

    .line 2231148
    iget-object v1, p0, LX/FMq;->j:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FN6;

    invoke-virtual {v1}, LX/FN6;->a()I

    move-result v1

    invoke-virtual {v0, v6, v1}, LX/1rd;->a(II)I

    move-result v0

    iget-object v1, p0, LX/FMq;->a:Landroid/content/Context;

    invoke-static/range {v0 .. v5}, LX/EdD;->a(ILandroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Landroid/app/PendingIntent;Z)V

    goto/16 :goto_0

    .line 2231149
    :catch_1
    move-exception v0

    .line 2231150
    sget-object v1, LX/FMM;->FILE_PROVIDER:LX/FMM;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v4, v1, v0, p1}, LX/FMq;->a(LX/FMq;Landroid/net/Uri;LX/FMM;Ljava/lang/String;Lcom/facebook/messaging/sms/defaultapp/send/PendingSendMessage;)V

    goto/16 :goto_0

    :cond_7
    move v5, v6

    .line 2231151
    goto :goto_3
.end method
