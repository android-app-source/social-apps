.class public LX/GP7;
.super LX/GP6;
.source ""


# instance fields
.field private c:Lcom/facebook/resources/ui/FbEditText;


# direct methods
.method public constructor <init>(LX/6yq;LX/GQC;Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;LX/GOy;LX/GNs;)V
    .locals 0
    .param p4    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2348423
    invoke-direct/range {p0 .. p6}, LX/GP6;-><init>(LX/6yq;LX/GQC;Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;LX/GOy;LX/GNs;)V

    .line 2348424
    return-void
.end method

.method public static b(LX/0QB;)LX/GP7;
    .locals 7

    .prologue
    .line 2348425
    new-instance v0, LX/GP7;

    invoke-static {p0}, LX/6yq;->a(LX/0QB;)LX/6yq;

    move-result-object v1

    check-cast v1, LX/6yq;

    invoke-static {p0}, LX/GQC;->a(LX/0QB;)LX/GQC;

    move-result-object v2

    check-cast v2, LX/GQC;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/GOy;->b(LX/0QB;)LX/GOy;

    move-result-object v5

    check-cast v5, LX/GOy;

    invoke-static {p0}, LX/GNs;->a(LX/0QB;)LX/GNs;

    move-result-object v6

    check-cast v6, LX/GNs;

    invoke-direct/range {v0 .. v6}, LX/GP7;-><init>(LX/6yq;LX/GQC;Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;LX/GOy;LX/GNs;)V

    .line 2348426
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2348427
    const v0, 0x7f0d2480

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, LX/GP7;->c:Lcom/facebook/resources/ui/FbEditText;

    .line 2348428
    invoke-super {p0, p1}, LX/GP6;->a(Landroid/view/View;)V

    .line 2348429
    return-void
.end method

.method public final e()Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;
    .locals 1

    .prologue
    .line 2348430
    iget-object v0, p0, LX/GP7;->c:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6yU;->a(Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v0

    return-object v0
.end method
