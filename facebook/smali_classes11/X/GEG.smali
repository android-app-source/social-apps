.class public LX/GEG;
.super Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod",
        "<",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/0sa;LX/0rq;LX/0tX;LX/1Ck;LX/GF4;LX/GG6;LX/0ad;)V
    .locals 8
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2331420
    move-object v0, p0

    move-object v1, p3

    move-object v2, p2

    move-object v3, p1

    move-object v4, p5

    move-object v5, p4

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;-><init>(LX/0tX;LX/0rq;LX/0sa;LX/GF4;LX/1Ck;LX/GG6;LX/0ad;)V

    .line 2331421
    return-void
.end method

.method public static b(LX/0QB;)LX/GEG;
    .locals 8

    .prologue
    .line 2331422
    new-instance v0, LX/GEG;

    invoke-static {p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v1

    check-cast v1, LX/0sa;

    invoke-static {p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v2

    check-cast v2, LX/0rq;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v4

    check-cast v4, LX/1Ck;

    invoke-static {p0}, LX/GF4;->a(LX/0QB;)LX/GF4;

    move-result-object v5

    check-cast v5, LX/GF4;

    invoke-static {p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v6

    check-cast v6, LX/GG6;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v7

    check-cast v7, LX/0ad;

    invoke-direct/range {v0 .. v7}, LX/GEG;-><init>(LX/0sa;LX/0rq;LX/0tX;LX/1Ck;LX/GF4;LX/GG6;LX/0ad;)V

    .line 2331423
    return-object v0
.end method


# virtual methods
.method public final a(LX/8wL;Ljava/lang/String;Ljava/lang/String;Z)LX/AAj;
    .locals 3

    .prologue
    .line 2331401
    invoke-super {p0, p1, p2, p3, p4}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->a(LX/8wL;Ljava/lang/String;Ljava/lang/String;Z)LX/AAj;

    move-result-object v0

    .line 2331402
    const-string v1, "boosted_post_story_type"

    const-string v2, "LIVE_VIDEO"

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2331403
    const-string v1, "is_boost_live"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2331404
    return-object v0
.end method

.method public final a(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;Ljava/lang/String;)Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;
    .locals 2

    .prologue
    .line 2331405
    new-instance v0, LX/GGN;

    invoke-direct {v0}, LX/GGN;-><init>()V

    const-string v1, "boosted_post_mobile"

    .line 2331406
    iput-object v1, v0, LX/GGI;->m:Ljava/lang/String;

    .line 2331407
    move-object v0, v0

    .line 2331408
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v1

    .line 2331409
    iput-object v1, v0, LX/GGI;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    .line 2331410
    move-object v0, v0

    .line 2331411
    iput-object p2, v0, LX/GGI;->c:Ljava/lang/String;

    .line 2331412
    move-object v0, v0

    .line 2331413
    sget-object v1, LX/GGB;->INACTIVE:LX/GGB;

    .line 2331414
    iput-object v1, v0, LX/GGI;->e:LX/GGB;

    .line 2331415
    move-object v0, v0

    .line 2331416
    sget-object v1, LX/8wL;->BOOST_LIVE:LX/8wL;

    .line 2331417
    iput-object v1, v0, LX/GGI;->b:LX/8wL;

    .line 2331418
    move-object v0, v0

    .line 2331419
    invoke-virtual {v0}, LX/GGI;->a()Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2331400
    const-string v0, "live_promotion_key"

    return-object v0
.end method

.method public final b(Lcom/facebook/adinterfaces/protocol/BoostedComponentDataFetchModels$BoostedComponentDataQueryModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2331399
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2331398
    const-string v0, "boosted_post_mobile"

    return-object v0
.end method
