.class public final LX/FTF;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2245052
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2245053
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2245054
    :goto_0
    return v1

    .line 2245055
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2245056
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2245057
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2245058
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2245059
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2245060
    const-string v4, "focus"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2245061
    invoke-static {p0, p1}, LX/4aC;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2245062
    :cond_2
    const-string v4, "photo"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2245063
    invoke-static {p0, p1}, LX/FTE;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2245064
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2245065
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2245066
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2245067
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2245068
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2245069
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245070
    if-eqz v0, :cond_0

    .line 2245071
    const-string v1, "focus"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245072
    invoke-static {p0, v0, p2}, LX/4aC;->a(LX/15i;ILX/0nX;)V

    .line 2245073
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2245074
    if-eqz v0, :cond_1

    .line 2245075
    const-string v1, "photo"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2245076
    invoke-static {p0, v0, p2, p3}, LX/FTE;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2245077
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2245078
    return-void
.end method
