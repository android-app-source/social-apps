.class public final LX/GUI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V
    .locals 0

    .prologue
    .line 2357343
    iput-object p1, p0, LX/GUI;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x1a20d568

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2357344
    iget-object v1, p0, LX/GUI;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->W:LX/GUi;

    .line 2357345
    iget-object v2, v1, LX/GUi;->a:LX/0Zb;

    const-string v3, "friends_nearby_settings_report_problem"

    invoke-static {v3}, LX/GUi;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2357346
    iget-object v1, p0, LX/GUI;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->U:LX/6G2;

    invoke-static {}, LX/6FY;->newBuilder()LX/6FX;

    move-result-object v2

    iget-object v3, p0, LX/GUI;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    invoke-virtual {v2, v3}, LX/6FX;->a(Landroid/content/Context;)LX/6FX;

    move-result-object v2

    invoke-virtual {v2}, LX/6FX;->a()LX/6FY;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/6G2;->a(LX/6FY;)V

    .line 2357347
    const v1, -0x435f5c42

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
