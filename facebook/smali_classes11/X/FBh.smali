.class public final LX/FBh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/util/Collection",
        "<",
        "Lcom/facebook/bookmark/model/Bookmark;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;)V
    .locals 0

    .prologue
    .line 2209642
    iput-object p1, p0, LX/FBh;->a:Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2209643
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2209644
    check-cast p1, Ljava/util/Collection;

    .line 2209645
    iget-object v0, p0, LX/FBh;->a:Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;

    iget-object v0, v0, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->s:Lcom/facebook/bookmark/model/BookmarksGroup;

    invoke-virtual {v0, p1}, Lcom/facebook/bookmark/model/BookmarksGroup;->a(Ljava/util/Collection;)V

    .line 2209646
    iget-object v0, p0, LX/FBh;->a:Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;

    invoke-virtual {v0}, Lcom/facebook/katana/ui/bookmark/BookmarkGroupFragment;->c()V

    .line 2209647
    return-void
.end method
