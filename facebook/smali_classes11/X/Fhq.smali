.class public LX/Fhq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Jw;


# instance fields
.field private final a:LX/2SV;

.field private final b:LX/03V;


# direct methods
.method public constructor <init>(LX/2SV;LX/03V;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2273830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2273831
    iput-object p1, p0, LX/Fhq;->a:LX/2SV;

    .line 2273832
    iput-object p2, p0, LX/Fhq;->b:LX/03V;

    .line 2273833
    return-void
.end method


# virtual methods
.method public final a(LX/2Jy;)Z
    .locals 3

    .prologue
    .line 2273834
    :try_start_0
    invoke-virtual {p1}, LX/2Jy;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2273835
    iget-object v0, p0, LX/Fhq;->a:LX/2SV;

    const/4 v1, 0x0

    sget-object v2, LX/EPu;->MEMORY:LX/EPu;

    invoke-virtual {v0, v1, v2}, LX/2SP;->a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2273836
    const/4 v0, 0x1

    .line 2273837
    :goto_0
    return v0

    .line 2273838
    :catch_0
    move-exception v0

    .line 2273839
    iget-object v1, p0, LX/Fhq;->b:LX/03V;

    const-string v2, "NullStateSync"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2273840
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
