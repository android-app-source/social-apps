.class public final LX/HC4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HCC;


# direct methods
.method public constructor <init>(LX/HCC;)V
    .locals 0

    .prologue
    .line 2438170
    iput-object p1, p0, LX/HC4;->a:LX/HCC;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x1d3a67a9

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2438171
    iget-object v0, p0, LX/HC4;->a:LX/HCC;

    sget-object v2, LX/9X7;->EDIT_TAP_ADD_TABS:LX/9X7;

    invoke-static {v0, v2}, LX/HCC;->a$redex0(LX/HCC;LX/9X7;)V

    .line 2438172
    iget-object v0, p0, LX/HC4;->a:LX/HCC;

    iget-object v0, v0, LX/HCC;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/HC0;

    iget-object v2, p0, LX/HC4;->a:LX/HCC;

    iget-wide v2, v2, LX/HCC;->k:J

    iget-object v4, p0, LX/HC4;->a:LX/HCC;

    iget-object v4, v4, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->d()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, LX/HC4;->a:LX/HCC;

    iget-object v5, v5, LX/HCC;->p:Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;

    invoke-virtual {v5}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->c()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    move-result-object v5

    .line 2438173
    sget-object v7, LX/8Dq;->g:Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 2438174
    iget-object v8, v0, LX/HC0;->c:LX/17Y;

    iget-object v9, v0, LX/HC0;->a:Landroid/content/Context;

    invoke-interface {v8, v9, v7}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v8

    .line 2438175
    const-string v7, "com.facebook.katana.profile.id"

    invoke-virtual {v8, v7, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 2438176
    const-string v7, "profile_name"

    invoke-virtual {v8, v7, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2438177
    const-string v7, "extra_addable_tabs_channel_data"

    invoke-static {v8, v7, v5}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2438178
    iget-object v9, v0, LX/HC0;->b:Lcom/facebook/content/SecureContextHelper;

    const/16 p0, 0x2788

    iget-object v7, v0, LX/HC0;->a:Landroid/content/Context;

    check-cast v7, Landroid/app/Activity;

    invoke-interface {v9, v8, p0, v7}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2438179
    const v0, 0x7073c6b2

    invoke-static {v6, v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
