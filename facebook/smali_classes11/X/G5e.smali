.class public final LX/G5e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/6N1;",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/search/api/SearchTypeaheadResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/G5f;


# direct methods
.method public constructor <init>(LX/G5f;)V
    .locals 0

    .prologue
    .line 2319040
    iput-object p1, p0, LX/G5e;->a:LX/G5f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2319041
    check-cast p1, LX/6N1;

    .line 2319042
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v1

    .line 2319043
    :goto_0
    invoke-interface {p1}, LX/6N1;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2319044
    invoke-interface {p1}, LX/6N1;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    .line 2319045
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->f()Lcom/facebook/user/model/Name;

    move-result-object v3

    invoke-static {v2, v3}, LX/2Oj;->a(Lcom/facebook/user/model/Name;Lcom/facebook/user/model/Name;)Ljava/lang/String;

    .line 2319046
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const/4 v8, 0x0

    .line 2319047
    invoke-static {}, Lcom/facebook/search/api/SearchTypeaheadResult;->newBuilder()LX/7BL;

    move-result-object v6

    const-string v7, ""

    .line 2319048
    iput-object v7, v6, LX/7BL;->a:Ljava/lang/String;

    .line 2319049
    move-object v6, v6

    .line 2319050
    iput-object v8, v6, LX/7BL;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2319051
    move-object v6, v6

    .line 2319052
    iput-object v8, v6, LX/7BL;->d:Landroid/net/Uri;

    .line 2319053
    move-object v6, v6

    .line 2319054
    iput-object v8, v6, LX/7BL;->e:Landroid/net/Uri;

    .line 2319055
    move-object v6, v6

    .line 2319056
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    .line 2319057
    iput-object v7, v6, LX/7BL;->f:Landroid/net/Uri;

    .line 2319058
    move-object v6, v6

    .line 2319059
    const-string v7, ""

    .line 2319060
    iput-object v7, v6, LX/7BL;->g:Ljava/lang/String;

    .line 2319061
    move-object v6, v6

    .line 2319062
    iput-object v3, v6, LX/7BL;->l:Ljava/lang/String;

    .line 2319063
    move-object v6, v6

    .line 2319064
    sget-object v7, LX/7BK;->PAGE:LX/7BK;

    .line 2319065
    iput-object v7, v6, LX/7BL;->m:LX/7BK;

    .line 2319066
    move-object v6, v6

    .line 2319067
    iput-wide v4, v6, LX/7BL;->n:J

    .line 2319068
    move-object v6, v6

    .line 2319069
    iput-object v8, v6, LX/7BL;->r:Ljava/util/List;

    .line 2319070
    move-object v6, v6

    .line 2319071
    invoke-virtual {v6}, LX/7BL;->a()Lcom/facebook/search/api/SearchTypeaheadResult;

    move-result-object v6

    move-object v0, v6

    .line 2319072
    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2319073
    :cond_0
    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
