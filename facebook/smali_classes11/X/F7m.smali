.class public final LX/F7m;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;)V
    .locals 0

    .prologue
    .line 2202482
    iput-object p1, p0, LX/F7m;->a:Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;B)V
    .locals 0

    .prologue
    .line 2202483
    invoke-direct {p0, p1}, LX/F7m;-><init>(Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 2202484
    iget-object v0, p0, LX/F7m;->a:Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->r:LX/9Tk;

    .line 2202485
    sget-object v1, LX/9Tj;->LEARN_MORE_MANAGE:LX/9Tj;

    invoke-static {v0, v1}, LX/9Tk;->a(LX/9Tk;LX/9Tj;)V

    .line 2202486
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2202487
    const-string v1, "titlebar_with_modal_done"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2202488
    iget-object v1, p0, LX/F7m;->a:Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->q:LX/17W;

    iget-object v2, p0, LX/F7m;->a:Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;

    sget-object v3, LX/0ax;->do:Ljava/lang/String;

    const-string v4, "/invite/history"

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2202489
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2202490
    iget-object v0, p0, LX/F7m;->a:Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;

    iget-boolean v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->v:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/F7m;->a:Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;

    iget-boolean v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->w:Z

    if-nez v0, :cond_0

    .line 2202491
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 2202492
    :goto_0
    return-void

    .line 2202493
    :cond_0
    iget-object v0, p0, LX/F7m;->a:Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;

    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x101009b

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/facebook/growth/friendfinder/FriendFinderLearnMoreActivity;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2202494
    iget v1, p1, Landroid/text/TextPaint;->linkColor:I

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/text/TextPaint;->setColor(I)V

    .line 2202495
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2202496
    invoke-virtual {p1, v3}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    goto :goto_0
.end method
