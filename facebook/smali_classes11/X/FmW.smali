.class public final LX/FmW;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2283044
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v3, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v3, :cond_5

    .line 2283045
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2283046
    :goto_0
    return v1

    .line 2283047
    :cond_0
    const-string v6, "has_next_page"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2283048
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v0

    move v3, v0

    move v0, v2

    .line 2283049
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_3

    .line 2283050
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2283051
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2283052
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 2283053
    const-string v6, "end_cursor"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2283054
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->b(Ljava/lang/String;)I

    move-result v4

    goto :goto_1

    .line 2283055
    :cond_2
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2283056
    :cond_3
    const/4 v5, 0x2

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2283057
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2283058
    if-eqz v0, :cond_4

    .line 2283059
    invoke-virtual {p1, v2, v3}, LX/186;->a(IZ)V

    .line 2283060
    :cond_4
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;)V
    .locals 2

    .prologue
    .line 2283061
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2283062
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2283063
    if-eqz v0, :cond_0

    .line 2283064
    const-string v1, "end_cursor"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2283065
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2283066
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2283067
    if-eqz v0, :cond_1

    .line 2283068
    const-string v1, "has_next_page"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2283069
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2283070
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2283071
    return-void
.end method
