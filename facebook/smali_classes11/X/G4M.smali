.class public LX/G4M;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field private final a:LX/G0r;


# direct methods
.method public constructor <init>(LX/G0r;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2317517
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2317518
    iput-object p1, p0, LX/G4M;->a:LX/G0r;

    .line 2317519
    return-void
.end method

.method public static a(LX/0QB;)LX/G4M;
    .locals 4

    .prologue
    .line 2317520
    const-class v1, LX/G4M;

    monitor-enter v1

    .line 2317521
    :try_start_0
    sget-object v0, LX/G4M;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2317522
    sput-object v2, LX/G4M;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2317523
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2317524
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2317525
    new-instance p0, LX/G4M;

    invoke-static {v0}, LX/G0r;->a(LX/0QB;)LX/G0r;

    move-result-object v3

    check-cast v3, LX/G0r;

    invoke-direct {p0, v3}, LX/G4M;-><init>(LX/G0r;)V

    .line 2317526
    move-object v0, p0

    .line 2317527
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2317528
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/G4M;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2317529
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2317530
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/G4M;Ljava/util/List;LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "LX/0Px",
            "<+",
            "Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLInterfaces$FavoritePhoto;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2317531
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    .line 2317532
    iget-object v3, p0, LX/G4M;->a:LX/G0r;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;->b()LX/5vo;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;->a()LX/5vn;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, LX/G0r;->b(LX/1U8;LX/5vn;)LX/1Fb;

    move-result-object v0

    .line 2317533
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 2317534
    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2317535
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2317536
    :cond_1
    return-void
.end method
