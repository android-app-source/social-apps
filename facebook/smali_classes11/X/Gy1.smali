.class public LX/Gy1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2Jw;


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final c:LX/11H;

.field private final d:LX/Gxz;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2408782
    const-class v0, LX/Gy1;

    sput-object v0, LX/Gy1;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/11H;LX/Gxz;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2408791
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2408792
    iput-object p1, p0, LX/Gy1;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2408793
    iput-object p2, p0, LX/Gy1;->c:LX/11H;

    .line 2408794
    iput-object p3, p0, LX/Gy1;->d:LX/Gxz;

    .line 2408795
    return-void
.end method


# virtual methods
.method public final a(LX/2Jy;)Z
    .locals 4

    .prologue
    .line 2408783
    :try_start_0
    iget-object v0, p0, LX/Gy1;->c:LX/11H;

    iget-object v1, p0, LX/Gy1;->d:LX/Gxz;

    const-string v2, "me"

    invoke-virtual {v0, v1, v2}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2408784
    iget-object v1, p0, LX/Gy1;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0eC;->a:LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2408785
    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2408786
    iget-object v1, p0, LX/Gy1;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/0eC;->a:LX/0Tn;

    invoke-interface {v1, v2, v0}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2408787
    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 2408788
    :catch_0
    move-exception v0

    .line 2408789
    sget-object v1, LX/Gy1;->a:Ljava/lang/Class;

    const-string v2, "Error with Auto Sync Locale"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2408790
    const/4 v0, 0x0

    goto :goto_0
.end method
