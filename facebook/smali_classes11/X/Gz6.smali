.class public final enum LX/Gz6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gz6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gz6;

.field public static final enum DEVICE_AUTH:LX/Gz6;

.field public static final enum NATIVE_ONLY:LX/Gz6;

.field public static final enum NATIVE_WITH_FALLBACK:LX/Gz6;

.field public static final enum WEB_ONLY:LX/Gz6;


# instance fields
.field private final allowsDeviceAuth:Z

.field private final allowsKatanaAuth:Z

.field private final allowsWebViewAuth:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2411146
    new-instance v0, LX/Gz6;

    const-string v1, "NATIVE_WITH_FALLBACK"

    move v4, v3

    move v5, v2

    invoke-direct/range {v0 .. v5}, LX/Gz6;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v0, LX/Gz6;->NATIVE_WITH_FALLBACK:LX/Gz6;

    .line 2411147
    new-instance v4, LX/Gz6;

    const-string v5, "NATIVE_ONLY"

    move v6, v3

    move v7, v3

    move v8, v2

    move v9, v2

    invoke-direct/range {v4 .. v9}, LX/Gz6;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v4, LX/Gz6;->NATIVE_ONLY:LX/Gz6;

    .line 2411148
    new-instance v4, LX/Gz6;

    const-string v5, "WEB_ONLY"

    move v6, v10

    move v7, v2

    move v8, v3

    move v9, v2

    invoke-direct/range {v4 .. v9}, LX/Gz6;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v4, LX/Gz6;->WEB_ONLY:LX/Gz6;

    .line 2411149
    new-instance v4, LX/Gz6;

    const-string v5, "DEVICE_AUTH"

    move v6, v11

    move v7, v2

    move v8, v2

    move v9, v3

    invoke-direct/range {v4 .. v9}, LX/Gz6;-><init>(Ljava/lang/String;IZZZ)V

    sput-object v4, LX/Gz6;->DEVICE_AUTH:LX/Gz6;

    .line 2411150
    const/4 v0, 0x4

    new-array v0, v0, [LX/Gz6;

    sget-object v1, LX/Gz6;->NATIVE_WITH_FALLBACK:LX/Gz6;

    aput-object v1, v0, v2

    sget-object v1, LX/Gz6;->NATIVE_ONLY:LX/Gz6;

    aput-object v1, v0, v3

    sget-object v1, LX/Gz6;->WEB_ONLY:LX/Gz6;

    aput-object v1, v0, v10

    sget-object v1, LX/Gz6;->DEVICE_AUTH:LX/Gz6;

    aput-object v1, v0, v11

    sput-object v0, LX/Gz6;->$VALUES:[LX/Gz6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZZZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZZ)V"
        }
    .end annotation

    .prologue
    .line 2411136
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2411137
    iput-boolean p3, p0, LX/Gz6;->allowsKatanaAuth:Z

    .line 2411138
    iput-boolean p4, p0, LX/Gz6;->allowsWebViewAuth:Z

    .line 2411139
    iput-boolean p5, p0, LX/Gz6;->allowsDeviceAuth:Z

    .line 2411140
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gz6;
    .locals 1

    .prologue
    .line 2411144
    const-class v0, LX/Gz6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gz6;

    return-object v0
.end method

.method public static values()[LX/Gz6;
    .locals 1

    .prologue
    .line 2411145
    sget-object v0, LX/Gz6;->$VALUES:[LX/Gz6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gz6;

    return-object v0
.end method


# virtual methods
.method public final allowsDeviceAuth()Z
    .locals 1

    .prologue
    .line 2411143
    iget-boolean v0, p0, LX/Gz6;->allowsDeviceAuth:Z

    return v0
.end method

.method public final allowsKatanaAuth()Z
    .locals 1

    .prologue
    .line 2411142
    iget-boolean v0, p0, LX/Gz6;->allowsKatanaAuth:Z

    return v0
.end method

.method public final allowsWebViewAuth()Z
    .locals 1

    .prologue
    .line 2411141
    iget-boolean v0, p0, LX/Gz6;->allowsWebViewAuth:Z

    return v0
.end method
