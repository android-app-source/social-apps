.class public final LX/H6j;
.super LX/0zP;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0zP",
        "<",
        "Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimEnableNotificationsMutationModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 12

    .prologue
    .line 2427258
    const-class v1, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimEnableNotificationsMutationModel;

    const v0, -0x270b1d4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "OfferClaimEnableNotificationsMutation"

    const-string v6, "0179214c590426b8b87dbc8c7a588fbe"

    const-string v7, "offer_claim_enable_notifications"

    const-string v8, "0"

    const-string v9, "10155252718116729"

    const/4 v10, 0x0

    .line 2427259
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v11, v0

    .line 2427260
    move-object v0, p0

    invoke-direct/range {v0 .. v11}, LX/0zP;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2427261
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2427262
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2427263
    sparse-switch v0, :sswitch_data_0

    .line 2427264
    :goto_0
    return-object p1

    .line 2427265
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2427266
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2427267
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    .line 2427268
    :sswitch_3
    const-string p1, "3"

    goto :goto_0

    .line 2427269
    :sswitch_4
    const-string p1, "4"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3ede4845 -> :sswitch_1
        -0x3924a673 -> :sswitch_2
        0x5fb57ca -> :sswitch_0
        0x45476ab5 -> :sswitch_3
        0x63a62026 -> :sswitch_4
    .end sparse-switch
.end method
