.class public LX/FDp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final e:[Ljava/lang/String;

.field private static volatile f:LX/FDp;


# instance fields
.field private final a:LX/0SG;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/2P7;

.field private final d:LX/2N8;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2213330
    const/16 v0, 0x25

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, LX/FDo;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/FDo;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/FDo;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, LX/FDo;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, LX/FDo;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, LX/FDo;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/FDo;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/FDo;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/FDo;->i:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/FDo;->j:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/FDo;->k:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, LX/FDo;->l:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, LX/FDo;->m:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, LX/FDo;->n:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, LX/FDo;->o:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, LX/FDo;->p:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, LX/FDo;->q:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, LX/FDo;->r:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, LX/FDo;->s:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, LX/FDo;->t:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, LX/FDo;->u:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, LX/FDo;->v:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, LX/FDo;->w:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, LX/FDo;->x:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, LX/FDo;->y:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, LX/FDo;->z:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, LX/FDo;->A:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, LX/FDo;->B:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, LX/FDo;->C:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, LX/FDo;->D:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, LX/FDo;->E:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, LX/FDo;->F:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, LX/FDo;->G:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, LX/FDo;->H:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, LX/FDo;->I:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, LX/FDo;->J:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, LX/FDo;->K:Ljava/lang/String;

    aput-object v2, v0, v1

    sput-object v0, LX/FDp;->e:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0SG;LX/0Or;LX/2P7;LX/2N8;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;",
            "LX/2P7;",
            "LX/2N8;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2213324
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2213325
    iput-object p1, p0, LX/FDp;->a:LX/0SG;

    .line 2213326
    iput-object p2, p0, LX/FDp;->b:LX/0Or;

    .line 2213327
    iput-object p3, p0, LX/FDp;->c:LX/2P7;

    .line 2213328
    iput-object p4, p0, LX/FDp;->d:LX/2N8;

    .line 2213329
    return-void
.end method

.method public static a(LX/0QB;)LX/FDp;
    .locals 7

    .prologue
    .line 2213311
    sget-object v0, LX/FDp;->f:LX/FDp;

    if-nez v0, :cond_1

    .line 2213312
    const-class v1, LX/FDp;

    monitor-enter v1

    .line 2213313
    :try_start_0
    sget-object v0, LX/FDp;->f:LX/FDp;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2213314
    if-eqz v2, :cond_0

    .line 2213315
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2213316
    new-instance v6, LX/FDp;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v3

    check-cast v3, LX/0SG;

    const/16 v4, 0x274b

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/2P7;->b(LX/0QB;)LX/2P7;

    move-result-object v4

    check-cast v4, LX/2P7;

    invoke-static {v0}, LX/2N8;->a(LX/0QB;)LX/2N8;

    move-result-object v5

    check-cast v5, LX/2N8;

    invoke-direct {v6, v3, p0, v4, v5}, LX/FDp;-><init>(LX/0SG;LX/0Or;LX/2P7;LX/2N8;)V

    .line 2213317
    move-object v0, v6

    .line 2213318
    sput-object v0, LX/FDp;->f:LX/FDp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2213319
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2213320
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2213321
    :cond_1
    sget-object v0, LX/FDp;->f:LX/FDp;

    return-object v0

    .line 2213322
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2213323
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2213224
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/database/Cursor;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2213310
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Landroid/database/Cursor;Ljava/lang/String;)F
    .locals 1

    .prologue
    .line 2213309
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    return v0
.end method

.method private static d(Landroid/database/Cursor;Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 2213308
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)LX/0Px;
    .locals 41
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2213234
    const-string v2, "DbFetchThreadUsersHandler.doThreadUsersQuery"

    const v3, 0x2f78eba7

    invoke-static {v2, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2213235
    :try_start_0
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v10

    .line 2213236
    const/4 v5, 0x0

    .line 2213237
    const/4 v6, 0x0

    .line 2213238
    if-eqz p1, :cond_0

    .line 2213239
    invoke-static/range {p1 .. p1}, Lcom/facebook/user/model/UserKey;->b(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v2

    .line 2213240
    sget-object v3, LX/FDo;->a:Ljava/lang/String;

    invoke-static {v3, v2}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v2

    .line 2213241
    invoke-virtual {v2}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v5

    .line 2213242
    invoke-virtual {v2}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v6

    .line 2213243
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FDp;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/6dQ;

    invoke-virtual {v2}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2213244
    const-string v3, "thread_users"

    sget-object v4, LX/FDp;->e:[Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v11

    .line 2213245
    :goto_0
    :try_start_1
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2213246
    sget-object v2, LX/FDo;->a:Ljava/lang/String;

    invoke-static {v11, v2}, LX/FDp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/user/model/UserKey;->a(Ljava/lang/String;)Lcom/facebook/user/model/UserKey;

    move-result-object v12

    .line 2213247
    new-instance v13, Lcom/facebook/user/model/Name;

    sget-object v2, LX/FDo;->b:Ljava/lang/String;

    invoke-static {v11, v2}, LX/FDp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sget-object v3, LX/FDo;->c:Ljava/lang/String;

    invoke-static {v11, v3}, LX/FDp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/FDo;->d:Ljava/lang/String;

    invoke-static {v11, v4}, LX/FDp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v13, v2, v3, v4}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2213248
    sget-object v2, LX/FDo;->e:Ljava/lang/String;

    invoke-static {v11, v2}, LX/FDp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 2213249
    const/4 v2, 0x0

    .line 2213250
    sget-object v3, LX/FDo;->g:Ljava/lang/String;

    invoke-static {v11, v3}, LX/FDp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2213251
    if-eqz v3, :cond_9

    .line 2213252
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FDp;->d:LX/2N8;

    invoke-virtual {v2, v3}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 2213253
    invoke-static {v2}, LX/2P7;->a(LX/0lF;)Lcom/facebook/user/model/PicSquare;

    move-result-object v2

    move-object v9, v2

    .line 2213254
    :goto_1
    sget-object v2, LX/FDo;->f:Ljava/lang/String;

    invoke-static {v11, v2}, LX/FDp;->b(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v15

    .line 2213255
    sget-object v2, LX/FDo;->i:Ljava/lang/String;

    invoke-static {v11, v2}, LX/FDp;->b(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v16

    .line 2213256
    sget-object v2, LX/FDo;->j:Ljava/lang/String;

    invoke-static {v11, v2}, LX/FDp;->b(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v17

    .line 2213257
    sget-object v2, LX/FDo;->l:Ljava/lang/String;

    invoke-static {v11, v2}, LX/FDp;->b(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v18

    .line 2213258
    sget-object v2, LX/FDo;->m:Ljava/lang/String;

    invoke-static {v11, v2}, LX/FDp;->b(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v19

    .line 2213259
    sget-object v2, LX/FDo;->o:Ljava/lang/String;

    invoke-static {v11, v2}, LX/FDp;->b(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v20

    .line 2213260
    sget-object v2, LX/FDo;->k:Ljava/lang/String;

    invoke-static {v11, v2}, LX/FDp;->c(Landroid/database/Cursor;Ljava/lang/String;)F

    move-result v21

    .line 2213261
    sget-object v2, LX/FDo;->h:Ljava/lang/String;

    invoke-static {v11, v2}, LX/FDp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    .line 2213262
    sget-object v2, LX/FDo;->s:Ljava/lang/String;

    invoke-static {v11, v2}, LX/FDp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 2213263
    sget-object v2, LX/FDo;->t:Ljava/lang/String;

    invoke-static {v11, v2}, LX/FDp;->b(Landroid/database/Cursor;Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v24

    .line 2213264
    const/4 v2, 0x0

    .line 2213265
    :try_start_2
    sget-object v3, LX/FDo;->n:Ljava/lang/String;

    invoke-static {v11, v3}, LX/FDp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2213266
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2213267
    invoke-static {v3}, LX/4nY;->valueOf(Ljava/lang/String;)LX/4nY;
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v2

    :cond_1
    move-object v8, v2

    .line 2213268
    :goto_2
    const/4 v2, 0x0

    .line 2213269
    :try_start_3
    sget-object v3, LX/FDo;->p:Ljava/lang/String;

    invoke-static {v11, v3}, LX/FDp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2213270
    if-eqz v3, :cond_8

    .line 2213271
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FDp;->d:LX/2N8;

    invoke-virtual {v2, v3}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 2213272
    invoke-static {v2}, LX/2P7;->c(LX/0lF;)LX/0Px;

    move-result-object v2

    move-object v3, v2

    .line 2213273
    :goto_3
    sget-object v2, LX/FDo;->x:Ljava/lang/String;

    invoke-static {v11, v2}, LX/FDp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2213274
    if-nez v2, :cond_2

    .line 2213275
    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v2

    move-object v7, v2

    .line 2213276
    :goto_4
    sget-object v2, LX/FDo;->A:Ljava/lang/String;

    invoke-static {v11, v2}, LX/FDp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2213277
    if-nez v2, :cond_3

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v2

    move-object v6, v2

    .line 2213278
    :goto_5
    sget-object v2, LX/FDo;->B:Ljava/lang/String;

    invoke-static {v11, v2}, LX/FDp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2213279
    if-nez v2, :cond_4

    invoke-static {}, LX/0Px;->of()LX/0Px;

    move-result-object v2

    move-object v5, v2

    .line 2213280
    :goto_6
    sget-object v2, LX/FDo;->J:Ljava/lang/String;

    invoke-static {v11, v2}, LX/FDp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2213281
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    const/4 v2, 0x0

    move-object v4, v2

    .line 2213282
    :goto_7
    sget-object v2, LX/FDo;->q:Ljava/lang/String;

    invoke-static {v11, v2}, LX/FDp;->b(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v2

    .line 2213283
    sget-object v25, LX/FDo;->r:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-static {v11, v0}, LX/FDp;->d(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v26

    .line 2213284
    sget-object v25, LX/FDo;->u:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-static {v11, v0}, LX/FDp;->b(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v25

    .line 2213285
    sget-object v28, LX/FDo;->v:Ljava/lang/String;

    move-object/from16 v0, v28

    invoke-static {v11, v0}, LX/FDp;->b(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v28

    .line 2213286
    sget-object v29, LX/FDo;->w:Ljava/lang/String;

    move-object/from16 v0, v29

    invoke-static {v11, v0}, LX/FDp;->b(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v29

    .line 2213287
    sget-object v30, LX/FDo;->y:Ljava/lang/String;

    move-object/from16 v0, v30

    invoke-static {v11, v0}, LX/FDp;->b(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v30

    .line 2213288
    sget-object v31, LX/FDo;->z:Ljava/lang/String;

    move-object/from16 v0, v31

    invoke-static {v11, v0}, LX/FDp;->b(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v31

    .line 2213289
    sget-object v32, LX/FDo;->I:Ljava/lang/String;

    move-object/from16 v0, v32

    invoke-static {v11, v0}, LX/FDp;->b(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v32

    .line 2213290
    sget-object v33, LX/FDo;->E:Ljava/lang/String;

    move-object/from16 v0, v33

    invoke-static {v11, v0}, LX/FDp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v33

    .line 2213291
    sget-object v34, LX/FDo;->F:Ljava/lang/String;

    move-object/from16 v0, v34

    invoke-static {v11, v0}, LX/FDp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v34

    .line 2213292
    sget-object v35, LX/FDo;->G:Ljava/lang/String;

    move-object/from16 v0, v35

    invoke-static {v11, v0}, LX/FDp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 2213293
    sget-object v36, LX/FDo;->H:Ljava/lang/String;

    move-object/from16 v0, v36

    invoke-static {v11, v0}, LX/FDp;->d(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v36

    move-wide/from16 v0, v36

    long-to-int v0, v0

    move/from16 v36, v0

    .line 2213294
    sget-object v37, LX/FDo;->K:Ljava/lang/String;

    move-object/from16 v0, v37

    invoke-static {v11, v0}, LX/FDp;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v37 .. v37}, LX/0XK;->fromDbValue(Ljava/lang/String;)LX/0XK;

    move-result-object v37

    .line 2213295
    new-instance v38, LX/0XI;

    invoke-direct/range {v38 .. v38}, LX/0XI;-><init>()V

    invoke-virtual {v12}, Lcom/facebook/user/model/UserKey;->a()LX/0XG;

    move-result-object v39

    invoke-virtual {v12}, Lcom/facebook/user/model/UserKey;->b()Ljava/lang/String;

    move-result-object v40

    invoke-virtual/range {v38 .. v40}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v38

    move-object/from16 v0, v38

    invoke-virtual {v0, v13}, LX/0XI;->b(Lcom/facebook/user/model/Name;)LX/0XI;

    move-result-object v13

    invoke-virtual {v13, v14}, LX/0XI;->e(Ljava/lang/String;)LX/0XI;

    move-result-object v13

    invoke-virtual {v13, v15}, LX/0XI;->c(Z)LX/0XI;

    move-result-object v13

    invoke-virtual {v13, v9}, LX/0XI;->a(Lcom/facebook/user/model/PicSquare;)LX/0XI;

    move-result-object v9

    move-object/from16 v0, v22

    invoke-virtual {v9, v0}, LX/0XI;->k(Ljava/lang/String;)LX/0XI;

    move-result-object v9

    move/from16 v0, v16

    invoke-virtual {v9, v0}, LX/0XI;->d(Z)LX/0XI;

    move-result-object v9

    move/from16 v0, v17

    invoke-virtual {v9, v0}, LX/0XI;->m(Z)LX/0XI;

    move-result-object v9

    move/from16 v0, v21

    invoke-virtual {v9, v0}, LX/0XI;->a(F)LX/0XI;

    move-result-object v9

    move/from16 v0, v19

    invoke-virtual {v9, v0}, LX/0XI;->f(Z)LX/0XI;

    move-result-object v9

    move/from16 v0, v18

    invoke-virtual {v9, v0}, LX/0XI;->e(Z)LX/0XI;

    move-result-object v9

    invoke-virtual {v9, v8}, LX/0XI;->a(LX/4nY;)LX/0XI;

    move-result-object v8

    move/from16 v0, v20

    invoke-virtual {v8, v0}, LX/0XI;->l(Z)LX/0XI;

    move-result-object v8

    invoke-virtual {v8, v3}, LX/0XI;->b(LX/0Px;)LX/0XI;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0XI;->h(Z)LX/0XI;

    move-result-object v2

    move-wide/from16 v0, v26

    invoke-virtual {v2, v0, v1}, LX/0XI;->c(J)LX/0XI;

    move-result-object v8

    if-eqz v23, :cond_6

    invoke-static/range {v23 .. v23}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    :goto_8
    invoke-virtual {v8, v2, v3}, LX/0XI;->d(J)LX/0XI;

    move-result-object v2

    move/from16 v0, v24

    invoke-virtual {v2, v0}, LX/0XI;->r(Z)LX/0XI;

    move-result-object v2

    move/from16 v0, v25

    invoke-virtual {v2, v0}, LX/0XI;->j(Z)LX/0XI;

    move-result-object v2

    move/from16 v0, v28

    invoke-virtual {v2, v0}, LX/0XI;->k(Z)LX/0XI;

    move-result-object v2

    move/from16 v0, v29

    invoke-virtual {v2, v0}, LX/0XI;->g(Z)LX/0XI;

    move-result-object v2

    invoke-virtual {v2, v7}, LX/0XI;->a(LX/0Px;)LX/0XI;

    move-result-object v2

    move/from16 v0, v30

    invoke-virtual {v2, v0}, LX/0XI;->p(Z)LX/0XI;

    move-result-object v2

    move/from16 v0, v31

    invoke-virtual {v2, v0}, LX/0XI;->q(Z)LX/0XI;

    move-result-object v2

    invoke-virtual {v2, v6}, LX/0XI;->d(LX/0Px;)LX/0XI;

    move-result-object v2

    invoke-virtual {v2, v5}, LX/0XI;->c(LX/0Px;)LX/0XI;

    move-result-object v2

    move-object/from16 v0, v33

    invoke-virtual {v2, v0}, LX/0XI;->l(Ljava/lang/String;)LX/0XI;

    move-result-object v2

    move-object/from16 v0, v34

    invoke-virtual {v2, v0}, LX/0XI;->m(Ljava/lang/String;)LX/0XI;

    move-result-object v2

    move-object/from16 v0, v35

    invoke-virtual {v2, v0}, LX/0XI;->n(Ljava/lang/String;)LX/0XI;

    move-result-object v2

    move/from16 v0, v36

    invoke-virtual {v2, v0}, LX/0XI;->a(I)LX/0XI;

    move-result-object v2

    move/from16 v0, v32

    invoke-virtual {v2, v0}, LX/0XI;->u(Z)LX/0XI;

    move-result-object v2

    invoke-virtual {v2, v4}, LX/0XI;->a(Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;)LX/0XI;

    move-result-object v2

    move-object/from16 v0, v37

    invoke-virtual {v2, v0}, LX/0XI;->a(LX/0XK;)LX/0XI;

    move-result-object v2

    invoke-virtual {v2}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v2

    .line 2213296
    invoke-interface {v10, v12, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    .line 2213297
    :catchall_0
    move-exception v2

    :try_start_4
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2213298
    :catchall_1
    move-exception v2

    const v3, -0x2282105a

    invoke-static {v3}, LX/02m;->a(I)V

    throw v2

    :catch_0
    move-object v8, v2

    goto/16 :goto_2

    .line 2213299
    :cond_2
    :try_start_5
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FDp;->d:LX/2N8;

    invoke-virtual {v4, v2}, LX/2N8;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v2

    .line 2213300
    invoke-static {v2}, LX/2P7;->b(LX/0lF;)LX/0Px;

    move-result-object v2

    move-object v7, v2

    goto/16 :goto_4

    .line 2213301
    :cond_3
    invoke-static {v2}, LX/4gg;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v2

    move-object v6, v2

    goto/16 :goto_5

    .line 2213302
    :cond_4
    invoke-static {v2}, LX/4gg;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v2

    move-object v5, v2

    goto/16 :goto_6

    .line 2213303
    :cond_5
    invoke-static {v2}, LX/4gl;->a(Ljava/lang/String;)Lcom/facebook/messaging/business/messengerextensions/model/MessengerExtensionProperties;

    move-result-object v2

    move-object v4, v2

    goto/16 :goto_7

    .line 2213304
    :cond_6
    const-wide/16 v2, 0x0

    goto/16 :goto_8

    .line 2213305
    :cond_7
    invoke-interface {v10}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v2

    .line 2213306
    :try_start_6
    invoke-interface {v11}, Landroid/database/Cursor;->close()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2213307
    const v3, -0x288c403d

    invoke-static {v3}, LX/02m;->a(I)V

    return-object v2

    :cond_8
    move-object v3, v2

    goto/16 :goto_3

    :cond_9
    move-object v9, v2

    goto/16 :goto_1
.end method

.method public final a(Lcom/facebook/user/model/UserKey;)Lcom/facebook/user/model/User;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2213225
    invoke-static {p1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/FDp;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 2213226
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    move-object v0, v1

    .line 2213227
    :cond_0
    :goto_0
    return-object v0

    .line 2213228
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2213229
    iget-object v2, p0, LX/FDp;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 2213230
    iget-wide v6, v0, Lcom/facebook/user/model/User;->M:J

    move-wide v4, v6

    .line 2213231
    sub-long/2addr v2, v4

    .line 2213232
    const-wide/32 v4, 0x5265c00

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    move-object v0, v1

    .line 2213233
    goto :goto_0
.end method
