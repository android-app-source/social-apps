.class public final LX/H9e;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/8Dz;

.field public final synthetic b:LX/H9g;


# direct methods
.method public constructor <init>(LX/H9g;LX/8Dz;)V
    .locals 0

    .prologue
    .line 2434501
    iput-object p1, p0, LX/H9e;->b:LX/H9g;

    iput-object p2, p0, LX/H9e;->a:LX/8Dz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 9

    .prologue
    .line 2434502
    iget-object v0, p0, LX/H9e;->b:LX/H9g;

    iget-object v0, v0, LX/H9g;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H89;

    iget-object v1, p0, LX/H9e;->a:LX/8Dz;

    iget-object v2, p0, LX/H9e;->b:LX/H9g;

    iget-object v2, v2, LX/H9g;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2434503
    new-instance v4, Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    iget-object v6, v1, LX/8Dz;->mArgVal:Ljava/lang/String;

    const-string v7, "android_timeline_action_menu_report_page"

    const-string v8, "android_timeline_report_dialog"

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/facebook/pages/common/actionbar/blueservice/ReportPlaceParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2434504
    const-string v5, "reportPlaceParams"

    const-string v6, "report_place"

    invoke-static {v0, v4, v5, v6}, LX/H89;->a(LX/H89;Landroid/os/Parcelable;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    move-object v0, v4

    .line 2434505
    return-object v0
.end method
