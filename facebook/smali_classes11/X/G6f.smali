.class public LX/G6f;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1Ck;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0tX;


# direct methods
.method private constructor <init>(LX/1Ck;LX/0tX;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ck;",
            "LX/0tX;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2320621
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2320622
    iput-object p1, p0, LX/G6f;->a:LX/1Ck;

    .line 2320623
    iput-object p2, p0, LX/G6f;->c:LX/0tX;

    .line 2320624
    iput-object p3, p0, LX/G6f;->b:LX/0Or;

    .line 2320625
    return-void
.end method

.method public static b(LX/0QB;)LX/G6f;
    .locals 4

    .prologue
    .line 2320626
    new-instance v2, LX/G6f;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    const/16 v3, 0x15e7

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-direct {v2, v0, v1, v3}, LX/G6f;-><init>(LX/1Ck;LX/0tX;LX/0Or;)V

    .line 2320627
    return-object v2
.end method
