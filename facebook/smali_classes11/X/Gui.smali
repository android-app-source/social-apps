.class public abstract LX/Gui;
.super LX/GuX;
.source ""

# interfaces
.implements LX/BWL;


# instance fields
.field public d:Z

.field public final synthetic e:Lcom/facebook/katana/activity/faceweb/FacewebFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 2403725
    iput-object p1, p0, LX/Gui;->e:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403726
    invoke-direct {p0, p2}, LX/GuX;-><init>(Landroid/os/Handler;)V

    .line 2403727
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Gui;->d:Z

    .line 2403728
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcom/facebook/webview/FacebookWebView;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2403729
    iget-object v0, p0, LX/Gui;->e:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403730
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v1, v1

    .line 2403731
    if-eqz v1, :cond_0

    iget-object v0, p0, LX/GuX;->g:LX/BWN;

    if-nez v0, :cond_1

    .line 2403732
    :cond_0
    :goto_0
    return-void

    .line 2403733
    :cond_1
    const v0, 0x7f0d118a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 2403734
    const v3, 0x7f0d1187

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 2403735
    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2403736
    new-instance v3, LX/Gup;

    invoke-direct {v3, p0}, LX/Gup;-><init>(LX/Gui;)V

    .line 2403737
    iget-object v4, p0, LX/Gui;->e:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v4, v4, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ah:Landroid/view/View;

    invoke-static {v4, v3}, LX/8He;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2403738
    const v3, 0x7f0d1188

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 2403739
    iget-boolean v3, p0, LX/Gui;->d:Z

    if-eqz v3, :cond_4

    .line 2403740
    const v3, 0x7f083152

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setHint(I)V

    .line 2403741
    :goto_1
    new-instance v3, LX/Guq;

    invoke-direct {v3, p0, p2}, LX/Guq;-><init>(LX/Gui;Lcom/facebook/webview/FacebookWebView;)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 2403742
    new-instance v3, LX/Gur;

    invoke-direct {v3, p0}, LX/Gur;-><init>(LX/Gui;)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2403743
    iget-boolean v3, p0, LX/Gui;->d:Z

    if-nez v3, :cond_2

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    if-lez v3, :cond_3

    :cond_2
    const/4 v2, 0x1

    :cond_3
    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2403744
    new-instance v2, LX/Gus;

    invoke-direct {v2, p0, v0, v1}, LX/Gus;-><init>(LX/Gui;Landroid/widget/Button;Landroid/widget/EditText;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2403745
    new-instance v1, LX/Gut;

    invoke-direct {v1, p0, p2}, LX/Gut;-><init>(LX/Gui;Lcom/facebook/webview/FacebookWebView;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2403746
    :cond_4
    const v3, 0x7f083156

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setHint(I)V

    goto :goto_1
.end method

.method public a(Lcom/facebook/webview/FacebookWebView;Landroid/widget/TextView;)V
    .locals 3

    .prologue
    .line 2403747
    iget-object v0, p0, LX/Gui;->e:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p2}, Landroid/widget/TextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2403748
    return-void
.end method

.method public a(Lcom/facebook/webview/FacebookWebView;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 3

    .prologue
    .line 2403749
    iget-object v0, p0, LX/GuX;->f:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment$ShowTextPublisherHandler$6;

    invoke-direct {v1, p0}, Lcom/facebook/katana/activity/faceweb/FacewebFragment$ShowTextPublisherHandler$6;-><init>(LX/Gui;)V

    const v2, -0x6b6cab47

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2403750
    return-void
.end method
