.class public final LX/FoQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;)V
    .locals 0

    .prologue
    .line 2289385
    iput-object p1, p0, LX/FoQ;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2289386
    iget-object v0, p0, LX/FoQ;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    const-string v1, "Failed initial post donation data fetch"

    invoke-static {v0, v1, p1}, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->a$redex0(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2289387
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2289366
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2289367
    if-nez p1, :cond_0

    .line 2289368
    iget-object v0, p0, LX/FoQ;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to render post donation page for campaign ID "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/FoQ;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    iget-object v2, v2, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->a$redex0(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2289369
    :goto_0
    return-void

    .line 2289370
    :cond_0
    iget-object v3, p0, LX/FoQ;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    .line 2289371
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2289372
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    .line 2289373
    iput-object v0, v3, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->m:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    .line 2289374
    iget-object v0, p0, LX/FoQ;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->m:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    if-eqz v0, :cond_4

    .line 2289375
    iget-object v0, p0, LX/FoQ;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to render post donation page for campaign ID "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/FoQ;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    iget-object v2, v2, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->a$redex0(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 2289376
    :cond_1
    iget-object v0, p0, LX/FoQ;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->m:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->m()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2289377
    if-nez v0, :cond_2

    move v0, v1

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_1

    .line 2289378
    :cond_3
    iget-object v0, p0, LX/FoQ;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->m:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->m()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2289379
    invoke-virtual {v3, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_2

    .line 2289380
    :cond_4
    iget-object v0, p0, LX/FoQ;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->m:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    invoke-static {v0}, LX/FoJ;->b(Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, LX/FoQ;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->m:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->n()Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouCampaignFragmentModel$FundraiserPageModel;

    move-result-object v0

    if-nez v0, :cond_5

    .line 2289381
    iget-object v0, p0, LX/FoQ;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->m:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->m()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2289382
    iget-object v3, p0, LX/FoQ;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/FoQ;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    iget-object v2, v2, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->m:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->k()Z

    move-result v2

    invoke-static {v3, v0, v2, v1}, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->a$redex0(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    .line 2289383
    :cond_5
    iget-object v0, p0, LX/FoQ;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->m:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->m()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2289384
    iget-object v4, p0, LX/FoQ;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    invoke-virtual {v3, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/FoQ;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->m:Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;

    invoke-virtual {v1}, Lcom/facebook/socialgood/protocol/FundraiserPostDonationModels$FundraiserThankYouPageQueryModel;->k()Z

    move-result v1

    invoke-static {v4, v0, v1, v2}, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->a$redex0(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;Ljava/lang/String;ZZ)V

    goto/16 :goto_0
.end method
