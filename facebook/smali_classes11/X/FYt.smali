.class public final LX/FYt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:I

.field public final synthetic c:J

.field public final synthetic d:Landroid/content/Context;

.field public final synthetic e:LX/FYx;


# direct methods
.method public constructor <init>(LX/FYx;Ljava/lang/String;IJLandroid/content/Context;)V
    .locals 0

    .prologue
    .line 2256608
    iput-object p1, p0, LX/FYt;->e:LX/FYx;

    iput-object p2, p0, LX/FYt;->a:Ljava/lang/String;

    iput p3, p0, LX/FYt;->b:I

    iput-wide p4, p0, LX/FYt;->c:J

    iput-object p6, p0, LX/FYt;->d:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2256609
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2256610
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2256611
    if-eqz p1, :cond_0

    .line 2256612
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2256613
    if-nez v0, :cond_1

    .line 2256614
    :cond_0
    :goto_0
    return-void

    .line 2256615
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2256616
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2256617
    new-instance v1, LX/3Qv;

    invoke-direct {v1}, LX/3Qv;-><init>()V

    const-string v2, "SAVED_STORY"

    .line 2256618
    iput-object v2, v1, LX/3Qv;->d:Ljava/lang/String;

    .line 2256619
    move-object v1, v1

    .line 2256620
    sget-object v2, LX/04g;->BY_USER:LX/04g;

    .line 2256621
    iput-object v2, v1, LX/3Qv;->h:LX/04g;

    .line 2256622
    move-object v1, v1

    .line 2256623
    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v0

    .line 2256624
    iput-object v0, v1, LX/3Qv;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2256625
    move-object v0, v1

    .line 2256626
    iget-object v1, p0, LX/FYt;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/3Qv;->a(Ljava/lang/String;)LX/3Qv;

    move-result-object v0

    iget v1, p0, LX/FYt;->b:I

    .line 2256627
    iput v1, v0, LX/3Qv;->e:I

    .line 2256628
    move-object v0, v0

    .line 2256629
    sget-object v1, LX/04D;->SAVED_DASHBOARD:LX/04D;

    .line 2256630
    iput-object v1, v0, LX/3Qv;->g:LX/04D;

    .line 2256631
    move-object v0, v0

    .line 2256632
    iget-object v1, p0, LX/FYt;->e:LX/FYx;

    iget-wide v2, p0, LX/FYt;->c:J

    iget-object v4, p0, LX/FYt;->d:Landroid/content/Context;

    invoke-virtual {v0}, LX/3Qv;->a()LX/3Qw;

    move-result-object v0

    invoke-static {v1, v2, v3, v4, v0}, LX/FYx;->a$redex0(LX/FYx;JLandroid/content/Context;LX/3Qw;)V

    goto :goto_0
.end method
