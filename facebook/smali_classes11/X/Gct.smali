.class public LX/Gct;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "Lcom/facebook/events/carousel/EventCardViewBinder;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2372767
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2372768
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/util/List;)Lcom/facebook/events/carousel/EventCardViewBinder;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/events/model/Event;",
            "Lcom/facebook/events/graphql/EventsGraphQLInterfaces$EventCardFragment$CoverPhoto;",
            "Lcom/facebook/events/common/EventAnalyticsParams;",
            "Ljava/util/List",
            "<+",
            "LX/Gco;",
            ">;)",
            "Lcom/facebook/events/carousel/EventCardViewBinder;"
        }
    .end annotation

    .prologue
    .line 2372765
    new-instance v0, Lcom/facebook/events/carousel/EventCardViewBinder;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    const-class v1, LX/38v;

    invoke-interface {p0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/38v;

    invoke-static {p0}, LX/7vW;->b(LX/0QB;)LX/7vW;

    move-result-object v7

    check-cast v7, LX/7vW;

    invoke-static {p0}, LX/7vZ;->b(LX/0QB;)LX/7vZ;

    move-result-object v8

    check-cast v8, LX/7vZ;

    invoke-static {p0}, LX/6RU;->a(LX/0QB;)LX/6RU;

    move-result-object v9

    check-cast v9, LX/6RU;

    invoke-static {p0}, LX/Bl6;->a(LX/0QB;)LX/Bl6;

    move-result-object v10

    check-cast v10, LX/Bl6;

    const/16 v1, 0x509

    invoke-static {p0, v1}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-static {p0}, LX/DB4;->b(LX/0QB;)LX/DB4;

    move-result-object v12

    check-cast v12, LX/DB4;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v13

    check-cast v13, Landroid/content/res/Resources;

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    invoke-direct/range {v0 .. v13}, Lcom/facebook/events/carousel/EventCardViewBinder;-><init>(Lcom/facebook/events/model/Event;Lcom/facebook/events/graphql/EventsGraphQLModels$EventCardFragmentModel$CoverPhotoModel;Lcom/facebook/events/common/EventAnalyticsParams;Ljava/util/List;Landroid/content/Context;LX/38v;LX/7vW;LX/7vZ;LX/6RU;LX/Bl6;LX/0Or;LX/DB4;Landroid/content/res/Resources;)V

    .line 2372766
    return-object v0
.end method
