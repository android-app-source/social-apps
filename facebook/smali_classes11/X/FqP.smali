.class public LX/FqP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/FqP;


# instance fields
.field private final a:LX/0kx;


# direct methods
.method public constructor <init>(LX/0kx;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2293830
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2293831
    iput-object p1, p0, LX/FqP;->a:LX/0kx;

    .line 2293832
    return-void
.end method

.method public static a(LX/0QB;)LX/FqP;
    .locals 4

    .prologue
    .line 2293833
    sget-object v0, LX/FqP;->b:LX/FqP;

    if-nez v0, :cond_1

    .line 2293834
    const-class v1, LX/FqP;

    monitor-enter v1

    .line 2293835
    :try_start_0
    sget-object v0, LX/FqP;->b:LX/FqP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2293836
    if-eqz v2, :cond_0

    .line 2293837
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2293838
    new-instance p0, LX/FqP;

    invoke-static {v0}, LX/0kx;->a(LX/0QB;)LX/0kx;

    move-result-object v3

    check-cast v3, LX/0kx;

    invoke-direct {p0, v3}, LX/FqP;-><init>(LX/0kx;)V

    .line 2293839
    move-object v0, p0

    .line 2293840
    sput-object v0, LX/FqP;->b:LX/FqP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2293841
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2293842
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2293843
    :cond_1
    sget-object v0, LX/FqP;->b:LX/FqP;

    return-object v0

    .line 2293844
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2293845
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static a(LX/36O;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 2293846
    new-instance v0, Lcom/facebook/timeline/TimelineFragment;

    invoke-direct {v0}, Lcom/facebook/timeline/TimelineFragment;-><init>()V

    .line 2293847
    if-nez p2, :cond_0

    .line 2293848
    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    .line 2293849
    :cond_0
    const-string v1, "session_id"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2293850
    const-string v1, "graphql_profile"

    invoke-static {p2, v1, p0}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2293851
    const-string v1, "navigation_source"

    invoke-virtual {p2, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2293852
    const-string v1, "com.facebook.katana.profile.id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2293853
    const-string v1, "com.facebook.katana.profile.id"

    const-wide/16 v2, -0x1

    invoke-virtual {p2, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2293854
    :cond_1
    invoke-virtual {v0, p2}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2293855
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 3

    .prologue
    .line 2293856
    const-string v0, "graphql_profile"

    invoke-static {p1, v0}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/36O;

    move-object v0, v0

    .line 2293857
    iget-object v1, p0, LX/FqP;->a:LX/0kx;

    const-string v2, "unknown"

    invoke-virtual {v1, v2}, LX/0kx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/FqP;->a(LX/36O;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method
