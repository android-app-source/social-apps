.class public LX/FhI;
.super LX/7HT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/7HT",
        "<",
        "Lcom/facebook/search/model/TypeaheadUnit;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/2Sc;

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FhL;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private final c:LX/Fhc;

.field private final d:LX/3Qe;

.field private final e:LX/0Uh;


# direct methods
.method public constructor <init>(LX/1Ck;LX/2Sc;LX/Fhc;LX/3Qe;LX/0Uh;LX/7Hq;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2272640
    invoke-direct {p0, p1, p6}, LX/7HT;-><init>(LX/1Ck;LX/7Hq;)V

    .line 2272641
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2272642
    iput-object v0, p0, LX/FhI;->b:LX/0Ot;

    .line 2272643
    iput-object p2, p0, LX/FhI;->a:LX/2Sc;

    .line 2272644
    iput-object p3, p0, LX/FhI;->c:LX/Fhc;

    .line 2272645
    iput-object p4, p0, LX/FhI;->d:LX/3Qe;

    .line 2272646
    iput-object p5, p0, LX/FhI;->e:LX/0Uh;

    .line 2272647
    return-void
.end method

.method public static b(LX/0QB;)LX/FhI;
    .locals 7

    .prologue
    .line 2272636
    new-instance v0, LX/FhI;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {p0}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v2

    check-cast v2, LX/2Sc;

    invoke-static {p0}, LX/Fhc;->a(LX/0QB;)LX/Fhc;

    move-result-object v3

    check-cast v3, LX/Fhc;

    invoke-static {p0}, LX/3Qe;->a(LX/0QB;)LX/3Qe;

    move-result-object v4

    check-cast v4, LX/3Qe;

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {p0}, LX/FhZ;->a(LX/0QB;)LX/7Hq;

    move-result-object v6

    check-cast v6, LX/7Hq;

    invoke-direct/range {v0 .. v6}, LX/FhI;-><init>(LX/1Ck;LX/2Sc;LX/Fhc;LX/3Qe;LX/0Uh;LX/7Hq;)V

    .line 2272637
    const/16 v1, 0x34ba

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    .line 2272638
    iput-object v1, v0, LX/FhI;->b:LX/0Ot;

    .line 2272639
    return-object v0
.end method


# virtual methods
.method public final a(LX/7B6;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7B6;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2272602
    iget-object v0, p1, LX/7B6;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "#"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FhI;->e:LX/0Uh;

    sget v1, LX/2SU;->e:I

    invoke-virtual {v0, v1}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2272603
    new-instance v0, LX/7Hc;

    .line 2272604
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2272605
    invoke-direct {v0, v1}, LX/7Hc;-><init>(LX/0Px;)V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2272606
    :goto_0
    return-object v0

    .line 2272607
    :cond_0
    iget-object v0, p0, LX/FhI;->c:LX/Fhc;

    .line 2272608
    iget-object v1, v0, LX/Fhc;->a:LX/Fhe;

    invoke-virtual {v1, p1}, LX/Fhd;->c(LX/7B6;)V

    .line 2272609
    iget-object v0, p0, LX/FhI;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FhL;

    .line 2272610
    iget-object v1, v0, LX/FhL;->b:LX/3Qe;

    .line 2272611
    iget v2, v1, LX/3Qe;->g:I

    .line 2272612
    iget v3, v1, LX/3Qe;->g:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, LX/3Qe;->g:I

    .line 2272613
    iget-object v3, v1, LX/3Qe;->h:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 2272614
    :goto_1
    invoke-static {p1}, LX/8ht;->a(LX/7B6;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2272615
    invoke-static {}, Lcom/google/common/util/concurrent/SettableFuture;->create()Lcom/google/common/util/concurrent/SettableFuture;

    move-result-object v1

    .line 2272616
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2272617
    const v3, 0x704473b

    invoke-static {v1, v2, v3}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2272618
    move-object v1, v1

    .line 2272619
    :goto_2
    new-instance v2, LX/FhJ;

    invoke-direct {v2, v0, p1}, LX/FhJ;-><init>(LX/FhL;LX/7B6;)V

    invoke-static {v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 2272620
    goto :goto_0

    .line 2272621
    :cond_1
    const/16 v4, 0xc

    const/4 p0, 0x3

    const/4 v3, 0x0

    .line 2272622
    iget-object v1, v0, LX/FhL;->d:LX/0Uh;

    sget v2, LX/2SU;->e:I

    invoke-virtual {v1, v2}, LX/0Uh;->a(I)LX/03R;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/FhL;->c:LX/0ad;

    sget-short v2, LX/100;->ba:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2272623
    :cond_2
    iget-object v1, v0, LX/FhL;->a:LX/8cT;

    iget-object v2, p1, LX/7B6;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v4}, LX/8cT;->a(Ljava/lang/String;I)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    new-instance v2, LX/FhK;

    invoke-direct {v2, v0}, LX/FhK;-><init>(LX/FhL;)V

    invoke-static {}, LX/0TA;->a()LX/0TD;

    move-result-object v3

    invoke-static {v1, v2, v3}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2272624
    :goto_3
    move-object v1, v1

    .line 2272625
    goto :goto_2

    .line 2272626
    :cond_3
    iget-object v3, v1, LX/3Qe;->h:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2272627
    invoke-static {v1}, LX/3Qe;->k(LX/3Qe;)LX/11o;

    move-result-object v3

    .line 2272628
    const-string v4, "executor_waiting_time"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    const/4 v5, 0x0

    const p0, -0x658d6aa8

    invoke-static {v3, v4, v2, v5, p0}, LX/096;->a(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;I)LX/11o;

    goto :goto_1

    .line 2272629
    :cond_4
    iget-object v1, v0, LX/FhL;->c:LX/0ad;

    sget-short v2, LX/100;->bN:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2272630
    iget-object v1, v0, LX/FhL;->a:LX/8cT;

    iget-object v2, p1, LX/7B6;->b:Ljava/lang/String;

    iget-object v3, v0, LX/FhL;->c:LX/0ad;

    sget v4, LX/100;->bQ:I

    invoke-interface {v3, v4, p0}, LX/0ad;->a(II)I

    move-result v3

    iget-object v4, v0, LX/FhL;->c:LX/0ad;

    sget v5, LX/100;->bR:I

    invoke-interface {v4, v5, p0}, LX/0ad;->a(II)I

    move-result v4

    .line 2272631
    iget-object v5, v1, LX/8cT;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/0TD;

    new-instance p0, LX/8cR;

    invoke-direct {p0, v1, v2, v4, v3}, LX/8cR;-><init>(LX/8cT;Ljava/lang/String;II)V

    invoke-interface {v5, p0}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v1, v5

    .line 2272632
    goto :goto_3

    .line 2272633
    :cond_5
    iget-object v1, v0, LX/FhL;->a:LX/8cT;

    iget-object v2, p1, LX/7B6;->b:Ljava/lang/String;

    .line 2272634
    iget-object v3, v1, LX/8cT;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0TD;

    new-instance v5, LX/8cP;

    invoke-direct {v5, v1, v2, p0, v4}, LX/8cP;-><init>(LX/8cT;Ljava/lang/String;II)V

    invoke-interface {v3, v5}, LX/0TD;->a(Ljava/util/concurrent/Callable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v1, v3

    .line 2272635
    goto :goto_3
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2272601
    const-string v0, "fetch_simple_local_typeahead"

    return-object v0
.end method

.method public final a(LX/0P1;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2272572
    invoke-super {p0, p1}, LX/7HT;->a(LX/0P1;)V

    .line 2272573
    iget-object v0, p0, LX/FhI;->c:LX/Fhc;

    .line 2272574
    iget-object v1, v0, LX/Fhc;->a:LX/Fhe;

    .line 2272575
    const/4 p0, 0x0

    .line 2272576
    iget-object v2, v1, LX/Fhd;->a:LX/11i;

    invoke-virtual {v1}, LX/Fhd;->a()LX/Fhg;

    move-result-object v3

    invoke-interface {v2, v3}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2272577
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    const-string v3, "KW_ONLY_TA_ENABLED"

    iget-object v4, v1, LX/Fhd;->e:LX/0ad;

    sget-short v5, LX/100;->bN:S

    invoke-interface {v4, v5, p0}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    .line 2272578
    if-eqz p1, :cond_0

    invoke-virtual {p1}, LX/0P1;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2272579
    invoke-virtual {v2, p1}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 2272580
    :cond_0
    iget-object v3, v1, LX/Fhd;->a:LX/11i;

    invoke-virtual {v1}, LX/Fhd;->a()LX/Fhg;

    move-result-object v4

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    invoke-interface {v3, v4, v2}, LX/11i;->b(LX/0Pq;LX/0P1;)V

    .line 2272581
    :cond_1
    iget-object v2, v1, LX/Fhd;->f:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 2272582
    iget-object v2, v1, LX/Fhd;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2272583
    iget-object v2, v1, LX/Fhd;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2272584
    iget-object v2, v1, LX/Fhd;->g:Ljava/util/BitSet;

    invoke-virtual {v2}, Ljava/util/BitSet;->clear()V

    .line 2272585
    iget-object v2, v1, LX/Fhd;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2272586
    iput p0, v1, LX/Fhd;->d:I

    .line 2272587
    iget-object v1, v0, LX/Fhc;->e:LX/Fhh;

    .line 2272588
    const/4 p0, 0x0

    .line 2272589
    iget-object v2, v1, LX/Fhd;->a:LX/11i;

    invoke-virtual {v1}, LX/Fhd;->a()LX/Fhg;

    move-result-object v3

    invoke-interface {v2, v3}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2272590
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v2

    const-string v3, "KW_ONLY_TA_ENABLED"

    iget-object v4, v1, LX/Fhd;->e:LX/0ad;

    sget-short v5, LX/100;->bN:S

    invoke-interface {v4, v5, p0}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    .line 2272591
    if-eqz p1, :cond_2

    invoke-virtual {p1}, LX/0P1;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2272592
    invoke-virtual {v2, p1}, LX/0P2;->a(Ljava/util/Map;)LX/0P2;

    .line 2272593
    :cond_2
    iget-object v3, v1, LX/Fhd;->a:LX/11i;

    invoke-virtual {v1}, LX/Fhd;->a()LX/Fhg;

    move-result-object v4

    invoke-virtual {v2}, LX/0P2;->b()LX/0P1;

    move-result-object v2

    invoke-interface {v3, v4, v2}, LX/11i;->b(LX/0Pq;LX/0P1;)V

    .line 2272594
    :cond_3
    iget-object v2, v1, LX/Fhd;->f:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    .line 2272595
    iget-object v2, v1, LX/Fhd;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2272596
    iget-object v2, v1, LX/Fhd;->h:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2272597
    iget-object v2, v1, LX/Fhd;->g:Ljava/util/BitSet;

    invoke-virtual {v2}, Ljava/util/BitSet;->clear()V

    .line 2272598
    iget-object v2, v1, LX/Fhd;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 2272599
    iput p0, v1, LX/Fhd;->d:I

    .line 2272600
    return-void
.end method

.method public final a(LX/7B6;Ljava/lang/Throwable;)V
    .locals 2
    .param p1    # LX/7B6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2272569
    iget-object v0, p0, LX/FhI;->c:LX/Fhc;

    invoke-virtual {v0, p1}, LX/Fhc;->d(LX/7B6;)V

    .line 2272570
    iget-object v0, p0, LX/FhI;->a:LX/2Sc;

    sget-object v1, LX/3Ql;->FETCH_SIMPLE_LOCAL_TYPEAHEAD_SUGGESTION_FAIL:LX/3Ql;

    invoke-virtual {v0, v1, p2}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V

    .line 2272571
    return-void
.end method

.method public final a(LX/7B6;Ljava/util/concurrent/CancellationException;)V
    .locals 2
    .param p1    # LX/7B6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2272566
    iget-object v0, p0, LX/FhI;->c:LX/Fhc;

    invoke-virtual {v0, p1}, LX/Fhc;->d(LX/7B6;)V

    .line 2272567
    iget-object v0, p0, LX/FhI;->a:LX/2Sc;

    sget-object v1, LX/3Ql;->FETCH_SIMPLE_LOCAL_TYPEAHEAD_SUGGESTION_CANCELLED:LX/3Ql;

    invoke-virtual {v0, v1, p2}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V

    .line 2272568
    return-void
.end method

.method public final a(LX/7Hi;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2272555
    iget-object v0, p0, LX/FhI;->c:LX/Fhc;

    .line 2272556
    iget-object v1, p1, LX/7Hi;->b:LX/7Hc;

    move-object v1, v1

    .line 2272557
    iget-object v2, v1, LX/7Hc;->b:LX/0Px;

    move-object v1, v2

    .line 2272558
    iget-object v2, p1, LX/7Hi;->a:LX/7B6;

    move-object v2, v2

    .line 2272559
    iget-object v3, v0, LX/Fhc;->a:LX/Fhe;

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v1, v4}, LX/Fhd;->a(LX/7B6;Ljava/util/List;LX/0P1;)V

    .line 2272560
    iget-object v0, p0, LX/FhI;->d:LX/3Qe;

    .line 2272561
    iget-object v1, p1, LX/7Hi;->a:LX/7B6;

    move-object v1, v1

    .line 2272562
    const-string v2, "ui_thread_waiting_time"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3, v1}, LX/3Qe;->a(LX/3Qe;Ljava/lang/String;Ljava/lang/String;LX/7B6;)V

    .line 2272563
    invoke-super {p0, p1}, LX/7HT;->a(LX/7Hi;)V

    .line 2272564
    return-void
.end method

.method public final b()LX/7HY;
    .locals 1

    .prologue
    .line 2272565
    sget-object v0, LX/7HY;->LOCAL:LX/7HY;

    return-object v0
.end method
