.class public LX/FMb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2229976
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2229977
    iput-object p1, p0, LX/FMb;->a:Landroid/content/Context;

    .line 2229978
    return-void
.end method

.method public static a(I)LX/FMM;
    .locals 1

    .prologue
    .line 2229979
    packed-switch p0, :pswitch_data_0

    .line 2229980
    :pswitch_0
    sget-object v0, LX/FMM;->GENERIC:LX/FMM;

    :goto_0
    return-object v0

    .line 2229981
    :pswitch_1
    sget-object v0, LX/FMM;->NO_ERROR:LX/FMM;

    goto :goto_0

    .line 2229982
    :pswitch_2
    sget-object v0, LX/FMM;->CONNECTION_ERROR:LX/FMM;

    goto :goto_0

    .line 2229983
    :pswitch_3
    sget-object v0, LX/FMM;->NO_CONNECTION:LX/FMM;

    goto :goto_0

    .line 2229984
    :pswitch_4
    sget-object v0, LX/FMM;->CONFIG_ERROR:LX/FMM;

    goto :goto_0

    .line 2229985
    :pswitch_5
    sget-object v0, LX/FMM;->LIMIT_EXCEEDED:LX/FMM;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method public static a(II)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2229986
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "sms:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez p1, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " errorCode:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(IILjava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2229987
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mms:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez p1, :cond_0

    const-string v0, ""

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez p2, :cond_1

    const-string v0, ""

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " http:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, " msg:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static b(I)LX/FMM;
    .locals 1

    .prologue
    .line 2229988
    sparse-switch p0, :sswitch_data_0

    .line 2229989
    sget-object v0, LX/FMM;->GENERIC:LX/FMM;

    :goto_0
    return-object v0

    .line 2229990
    :sswitch_0
    sget-object v0, LX/FMM;->NO_ERROR:LX/FMM;

    goto :goto_0

    .line 2229991
    :sswitch_1
    sget-object v0, LX/FMM;->PROCESSING_ERROR:LX/FMM;

    goto :goto_0

    .line 2229992
    :sswitch_2
    sget-object v0, LX/FMM;->CONNECTION_ERROR:LX/FMM;

    goto :goto_0

    .line 2229993
    :sswitch_3
    sget-object v0, LX/FMM;->SERVER_ERROR:LX/FMM;

    goto :goto_0

    .line 2229994
    :sswitch_4
    sget-object v0, LX/FMM;->APN_FAILURE:LX/FMM;

    goto :goto_0

    .line 2229995
    :sswitch_5
    sget-object v0, LX/FMM;->IO_ERROR:LX/FMM;

    goto :goto_0

    .line 2229996
    :sswitch_6
    sget-object v0, LX/FMM;->CONFIG_ERROR:LX/FMM;

    goto :goto_0

    .line 2229997
    :sswitch_7
    sget-object v0, LX/FMM;->NO_CONNECTION:LX/FMM;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x2 -> :sswitch_4
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_5
        0x7 -> :sswitch_6
        0x8 -> :sswitch_7
        0x64 -> :sswitch_1
    .end sparse-switch
.end method

.method public static b(LX/0QB;)LX/FMb;
    .locals 2

    .prologue
    .line 2229998
    new-instance v1, LX/FMb;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-direct {v1, v0}, LX/FMb;-><init>(Landroid/content/Context;)V

    .line 2229999
    return-object v1
.end method


# virtual methods
.method public final a(LX/FMM;)Lcom/facebook/messaging/model/send/SendError;
    .locals 4

    .prologue
    .line 2230000
    sget-object v0, LX/FMM;->NO_ERROR:LX/FMM;

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2230001
    invoke-static {}, Lcom/facebook/messaging/model/send/SendError;->newBuilder()LX/6fO;

    move-result-object v0

    invoke-virtual {p1}, LX/FMM;->ordinal()I

    move-result v1

    .line 2230002
    iput v1, v0, LX/6fO;->e:I

    .line 2230003
    move-object v0, v0

    .line 2230004
    sget-object v1, LX/6fP;->SMS_SEND_FAILED:LX/6fP;

    .line 2230005
    iput-object v1, v0, LX/6fO;->a:LX/6fP;

    .line 2230006
    move-object v0, v0

    .line 2230007
    sget-object v1, LX/FMa;->a:[I

    invoke-virtual {p1}, LX/FMM;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2230008
    :goto_1
    invoke-virtual {v0}, LX/6fO;->g()Lcom/facebook/messaging/model/send/SendError;

    move-result-object v0

    return-object v0

    .line 2230009
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2230010
    :pswitch_0
    iget-object v1, p0, LX/FMb;->a:Landroid/content/Context;

    const v2, 0x7f080930

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2230011
    iput-object v1, v0, LX/6fO;->b:Ljava/lang/String;

    .line 2230012
    move-object v1, v0

    .line 2230013
    iget-object v2, p0, LX/FMb;->a:Landroid/content/Context;

    const v3, 0x7f080938

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2230014
    iput-object v2, v1, LX/6fO;->f:Ljava/lang/String;

    .line 2230015
    goto :goto_1

    .line 2230016
    :pswitch_1
    iget-object v1, p0, LX/FMb;->a:Landroid/content/Context;

    const v2, 0x7f08092f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2230017
    iput-object v1, v0, LX/6fO;->b:Ljava/lang/String;

    .line 2230018
    move-object v1, v0

    .line 2230019
    iget-object v2, p0, LX/FMb;->a:Landroid/content/Context;

    const v3, 0x7f080937

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2230020
    iput-object v2, v1, LX/6fO;->f:Ljava/lang/String;

    .line 2230021
    goto :goto_1

    .line 2230022
    :pswitch_2
    iget-object v1, p0, LX/FMb;->a:Landroid/content/Context;

    const v2, 0x7f080931

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2230023
    iput-object v1, v0, LX/6fO;->b:Ljava/lang/String;

    .line 2230024
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(LX/FMM;)Lcom/facebook/messaging/model/send/SendError;
    .locals 4

    .prologue
    .line 2230025
    sget-object v0, LX/FMM;->NO_ERROR:LX/FMM;

    if-eq p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2230026
    invoke-static {}, Lcom/facebook/messaging/model/send/SendError;->newBuilder()LX/6fO;

    move-result-object v0

    invoke-virtual {p1}, LX/FMM;->ordinal()I

    move-result v1

    .line 2230027
    iput v1, v0, LX/6fO;->e:I

    .line 2230028
    move-object v0, v0

    .line 2230029
    sget-object v1, LX/6fP;->SMS_SEND_FAILED:LX/6fP;

    .line 2230030
    iput-object v1, v0, LX/6fO;->a:LX/6fP;

    .line 2230031
    move-object v0, v0

    .line 2230032
    sget-object v1, LX/FMa;->a:[I

    invoke-virtual {p1}, LX/FMM;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2230033
    :goto_1
    :pswitch_0
    invoke-virtual {v0}, LX/6fO;->g()Lcom/facebook/messaging/model/send/SendError;

    move-result-object v0

    return-object v0

    .line 2230034
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2230035
    :pswitch_1
    iget-object v1, p0, LX/FMb;->a:Landroid/content/Context;

    const v2, 0x7f080933

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2230036
    iput-object v1, v0, LX/6fO;->b:Ljava/lang/String;

    .line 2230037
    move-object v1, v0

    .line 2230038
    iget-object v2, p0, LX/FMb;->a:Landroid/content/Context;

    const v3, 0x7f080939

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2230039
    iput-object v2, v1, LX/6fO;->f:Ljava/lang/String;

    .line 2230040
    goto :goto_1

    .line 2230041
    :pswitch_2
    iget-object v1, p0, LX/FMb;->a:Landroid/content/Context;

    const v2, 0x7f080934

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2230042
    iput-object v1, v0, LX/6fO;->b:Ljava/lang/String;

    .line 2230043
    move-object v1, v0

    .line 2230044
    iget-object v2, p0, LX/FMb;->a:Landroid/content/Context;

    const v3, 0x7f08093b

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2230045
    iput-object v2, v1, LX/6fO;->f:Ljava/lang/String;

    .line 2230046
    goto :goto_1

    .line 2230047
    :pswitch_3
    iget-object v1, p0, LX/FMb;->a:Landroid/content/Context;

    const v2, 0x7f080932

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2230048
    iput-object v1, v0, LX/6fO;->b:Ljava/lang/String;

    .line 2230049
    move-object v1, v0

    .line 2230050
    iget-object v2, p0, LX/FMb;->a:Landroid/content/Context;

    const v3, 0x7f08093a

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2230051
    iput-object v2, v1, LX/6fO;->f:Ljava/lang/String;

    .line 2230052
    goto :goto_1

    .line 2230053
    :pswitch_4
    iget-object v1, p0, LX/FMb;->a:Landroid/content/Context;

    const v2, 0x7f080936

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2230054
    iput-object v1, v0, LX/6fO;->b:Ljava/lang/String;

    .line 2230055
    move-object v1, v0

    .line 2230056
    iget-object v2, p0, LX/FMb;->a:Landroid/content/Context;

    const v3, 0x7f08093c

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2230057
    iput-object v2, v1, LX/6fO;->f:Ljava/lang/String;

    .line 2230058
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
