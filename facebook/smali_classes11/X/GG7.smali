.class public LX/GG7;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2333285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)LX/03R;
    .locals 1

    .prologue
    .line 2333286
    if-ne p0, p1, :cond_0

    .line 2333287
    sget-object v0, LX/03R;->YES:LX/03R;

    .line 2333288
    :goto_0
    return-object v0

    .line 2333289
    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    .line 2333290
    :cond_1
    sget-object v0, LX/03R;->NO:LX/03R;

    goto :goto_0

    .line 2333291
    :cond_2
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_0
.end method

.method public static a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2333292
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2333293
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->r()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v1, v0

    move v2, v0

    move v3, v0

    :goto_0
    if-ge v1, v6, :cond_0

    invoke-virtual {v5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel$LifetimeOverallStatsModel;

    .line 2333294
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel$LifetimeOverallStatsModel;->j()I

    move-result v7

    add-int/2addr v3, v7

    .line 2333295
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CampaignGroupResultsModel$LifetimeOverallStatsModel;->a()I

    move-result v0

    add-int/2addr v2, v0

    .line 2333296
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2333297
    :cond_0
    invoke-interface {p0, v3}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->c(I)Ljava/lang/String;

    move-result-object v0

    .line 2333298
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2333299
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2333300
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2333301
    return-object v4
.end method

.method public static a(Lcom/facebook/flatbuffers/Flattenable;Lcom/facebook/flatbuffers/Flattenable;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">(TT;TT;)Z"
        }
    .end annotation

    .prologue
    .line 2333302
    invoke-static {p0, p1}, LX/GG7;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/03R;

    move-result-object v0

    .line 2333303
    invoke-virtual {v0}, LX/03R;->isSet()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2333304
    invoke-virtual {v0}, LX/03R;->asBoolean()Z

    move-result v0

    .line 2333305
    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;)[B

    move-result-object v0

    invoke-static {p1}, LX/186;->b(Lcom/facebook/flatbuffers/Flattenable;)[B

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">;",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/flatbuffers/Flattenable;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2333306
    if-eq p0, p1, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v3, v4

    .line 2333307
    :cond_1
    :goto_0
    return v3

    .line 2333308
    :cond_2
    if-eqz p0, :cond_1

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2333309
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_1

    move v2, v3

    .line 2333310
    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 2333311
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/flatbuffers/Flattenable;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/flatbuffers/Flattenable;

    invoke-static {v0, v1}, LX/GG7;->a(Lcom/facebook/flatbuffers/Flattenable;Lcom/facebook/flatbuffers/Flattenable;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2333312
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move v3, v4

    .line 2333313
    goto :goto_0
.end method

.method public static b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;
    .locals 2
    .param p0    # Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2333314
    if-nez p0, :cond_0

    .line 2333315
    const/4 v0, 0x0

    .line 2333316
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, LX/GG7;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method
