.class public final LX/GGE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

.field public b:I

.field public c:I

.field public d:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$InterestModel;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$DetailedTargetingItemModel;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

.field public h:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/GGF;",
            ">;"
        }
    .end annotation
.end field

.field public i:Ljava/lang/String;

.field public j:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

.field public k:LX/GGG;

.field public l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLBoostedComponentAudienceEditableField;",
            ">;"
        }
    .end annotation
.end field

.field public m:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2333415
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2333416
    iput-object v0, p0, LX/GGE;->i:Ljava/lang/String;

    .line 2333417
    iput-object v0, p0, LX/GGE;->j:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2333418
    sget-object v0, LX/GGG;->REGION:LX/GGG;

    iput-object v0, p0, LX/GGE;->k:LX/GGG;

    .line 2333419
    return-void
.end method

.method public constructor <init>(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2333384
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2333385
    iput-object v0, p0, LX/GGE;->i:Ljava/lang/String;

    .line 2333386
    iput-object v0, p0, LX/GGE;->j:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2333387
    sget-object v0, LX/GGG;->REGION:LX/GGG;

    iput-object v0, p0, LX/GGE;->k:LX/GGG;

    .line 2333388
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->g:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    move-object v0, v0

    .line 2333389
    iput-object v0, p0, LX/GGE;->a:Lcom/facebook/graphql/enums/GraphQLAdsTargetingGender;

    .line 2333390
    iget v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->d:I

    move v0, v0

    .line 2333391
    iput v0, p0, LX/GGE;->b:I

    .line 2333392
    iget v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->e:I

    move v0, v0

    .line 2333393
    iput v0, p0, LX/GGE;->c:I

    .line 2333394
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->k:LX/0Px;

    move-object v0, v0

    .line 2333395
    iput-object v0, p0, LX/GGE;->d:LX/0Px;

    .line 2333396
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->m:LX/0Px;

    move-object v0, v0

    .line 2333397
    iput-object v0, p0, LX/GGE;->e:LX/0Px;

    .line 2333398
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->i:LX/0Px;

    move-object v0, v0

    .line 2333399
    iput-object v0, p0, LX/GGE;->f:LX/0Px;

    .line 2333400
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v0, v0

    .line 2333401
    iput-object v0, p0, LX/GGE;->g:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2333402
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->n:LX/0Px;

    move-object v0, v0

    .line 2333403
    iput-object v0, p0, LX/GGE;->h:LX/0Px;

    .line 2333404
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v0, v0

    .line 2333405
    iput-object v0, p0, LX/GGE;->i:Ljava/lang/String;

    .line 2333406
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v0, v0

    .line 2333407
    iput-object v0, p0, LX/GGE;->j:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2333408
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    move-object v0, v0

    .line 2333409
    iput-object v0, p0, LX/GGE;->k:LX/GGG;

    .line 2333410
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->l:LX/0Px;

    move-object v0, v0

    .line 2333411
    iput-object v0, p0, LX/GGE;->l:LX/0Px;

    .line 2333412
    iget-object v0, p1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->q:Ljava/lang/String;

    move-object v0, v0

    .line 2333413
    iput-object v0, p0, LX/GGE;->m:Ljava/lang/String;

    .line 2333414
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;
    .locals 1

    .prologue
    .line 2333383
    new-instance v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    invoke-direct {v0, p0}, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;-><init>(LX/GGE;)V

    return-object v0
.end method
