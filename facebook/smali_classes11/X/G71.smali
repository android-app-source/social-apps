.class public LX/G71;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/63p;


# instance fields
.field private final a:LX/01T;

.field private final b:LX/1hs;


# direct methods
.method public constructor <init>(LX/01T;LX/1hs;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2320967
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2320968
    iput-object p1, p0, LX/G71;->a:LX/01T;

    .line 2320969
    iput-object p2, p0, LX/G71;->b:LX/1hs;

    .line 2320970
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)LX/03R;
    .locals 3

    .prologue
    .line 2320971
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 2320972
    if-eqz v0, :cond_2

    .line 2320973
    invoke-static {v0}, LX/1H1;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2320974
    sget-object v0, LX/03R;->UNSET:LX/03R;

    .line 2320975
    :goto_0
    return-object v0

    .line 2320976
    :cond_0
    iget-object v1, p0, LX/G71;->a:LX/01T;

    sget-object v2, LX/01T;->FB4A:LX/01T;

    if-eq v1, v2, :cond_1

    iget-object v1, p0, LX/G71;->a:LX/01T;

    sget-object v2, LX/01T;->MESSENGER:LX/01T;

    if-ne v1, v2, :cond_2

    :cond_1
    iget-object v1, p0, LX/G71;->b:LX/1hs;

    invoke-virtual {v1, v0}, LX/1hs;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2320977
    sget-object v0, LX/03R;->YES:LX/03R;

    goto :goto_0

    .line 2320978
    :cond_2
    sget-object v0, LX/03R;->UNSET:LX/03R;

    goto :goto_0
.end method
