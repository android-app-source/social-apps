.class public LX/Fe5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Sh;

.field public final b:LX/FZr;

.field private final c:LX/2Sc;

.field public final d:LX/Cvq;

.field public e:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Sh;LX/FZr;LX/2Sc;LX/Cvq;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2265092
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2265093
    iput-object p1, p0, LX/Fe5;->a:LX/0Sh;

    .line 2265094
    iput-object p2, p0, LX/Fe5;->b:LX/FZr;

    .line 2265095
    iput-object p3, p0, LX/Fe5;->c:LX/2Sc;

    .line 2265096
    iput-object p4, p0, LX/Fe5;->d:LX/Cvq;

    .line 2265097
    return-void
.end method

.method public static a(LX/Fe5;LX/3Ql;Ljava/lang/Exception;)V
    .locals 5

    .prologue
    .line 2265098
    iget-object v0, p0, LX/Fe5;->d:LX/Cvq;

    .line 2265099
    sget-object v1, LX/Cvx;->a:LX/Cvv;

    const-string v2, "network_operation"

    .line 2265100
    iget-object v3, v0, LX/Cvq;->a:LX/11i;

    invoke-interface {v3, v1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v3

    .line 2265101
    if-nez v3, :cond_0

    .line 2265102
    :goto_0
    invoke-static {v0}, LX/Cvq;->g(LX/Cvq;)V

    .line 2265103
    const/4 v0, 0x0

    iput-object v0, p0, LX/Fe5;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2265104
    iget-object v0, p0, LX/Fe5;->c:LX/2Sc;

    invoke-virtual {v0, p1, p2}, LX/2Sc;->a(LX/3Ql;Ljava/lang/Throwable;)V

    .line 2265105
    return-void

    .line 2265106
    :cond_0
    const v4, -0x69e1de2

    invoke-static {v3, v2, v4}, LX/096;->c(LX/11o;Ljava/lang/String;I)LX/11o;

    goto :goto_0
.end method

.method public static a(LX/Fe5;Ljava/util/concurrent/Callable;LX/2h0;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;>;",
            "LX/2h0;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2265107
    iget-object v0, p0, LX/Fe5;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_0

    .line 2265108
    iget-object v0, p0, LX/Fe5;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2265109
    :cond_0
    iget-object v0, p0, LX/Fe5;->d:LX/Cvq;

    .line 2265110
    sget-object v1, LX/Cvx;->a:LX/Cvv;

    const-string v2, "network_operation"

    invoke-static {v0, v1, v2}, LX/Cvq;->a(LX/Cvq;LX/0Pq;Ljava/lang/String;)V

    .line 2265111
    :try_start_0
    invoke-interface {p1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object v0, p0, LX/Fe5;->e:Lcom/google/common/util/concurrent/ListenableFuture;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2265112
    iget-object v0, p0, LX/Fe5;->a:LX/0Sh;

    iget-object v1, p0, LX/Fe5;->e:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-virtual {v0, v1, p2}, LX/0Sh;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2265113
    return-void

    .line 2265114
    :catch_0
    move-exception v0

    .line 2265115
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
