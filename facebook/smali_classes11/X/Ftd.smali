.class public final LX/Ftd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;)V
    .locals 0

    .prologue
    .line 2298962
    iput-object p1, p0, LX/Ftd;->a:Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    const v0, -0x1be8b271

    invoke-static {v5, v6, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2298963
    iget-object v0, p0, LX/Ftd;->a:Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;

    iget-object v0, v0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/Ftd;->a:Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2298964
    new-instance v4, LX/8AA;

    sget-object v7, LX/8AB;->FAVORITE_MEDIA_PICKER:LX/8AB;

    invoke-direct {v4, v7}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v4}, LX/8AA;->i()LX/8AA;

    move-result-object v4

    sget-object v7, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v4, v7}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v4

    invoke-virtual {v4}, LX/8AA;->m()LX/8AA;

    move-result-object v4

    .line 2298965
    iget-object v7, v1, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraSectionPartDefinition;->f:LX/0ad;

    sget-short v8, LX/0wf;->A:S

    const/4 p0, 0x0

    invoke-interface {v7, v8, p0}, LX/0ad;->a(SZ)Z

    move-result v7

    move v7, v7

    .line 2298966
    if-nez v7, :cond_0

    .line 2298967
    invoke-virtual {v4}, LX/8AA;->j()LX/8AA;

    .line 2298968
    :cond_0
    invoke-static {v3, v4}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v4

    move-object v3, v4

    .line 2298969
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v4, Landroid/app/Activity;

    invoke-static {v1, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-interface {v0, v3, v6, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2298970
    const v0, 0x5f0f2393

    invoke-static {v5, v5, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
