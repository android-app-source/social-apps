.class public final LX/FUJ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FUH;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Ljava/io/File;

.field public final synthetic c:Lcom/facebook/qrcode/QRCodeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/qrcode/QRCodeFragment;ZLjava/io/File;)V
    .locals 0

    .prologue
    .line 2248108
    iput-object p1, p0, LX/FUJ;->c:Lcom/facebook/qrcode/QRCodeFragment;

    iput-boolean p2, p0, LX/FUJ;->a:Z

    iput-object p3, p0, LX/FUJ;->b:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2248109
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2248110
    iget-boolean v2, p0, LX/FUJ;->a:Z

    if-eqz v2, :cond_0

    .line 2248111
    const-string v2, "android.intent.extra.STREAM"

    iget-object v3, p0, LX/FUJ;->c:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v3, v3, Lcom/facebook/qrcode/QRCodeFragment;->e:Landroid/content/Context;

    iget-object v4, p0, LX/FUJ;->b:Ljava/io/File;

    invoke-static {v3, v4}, Lcom/facebook/content/FbFileProvider;->a(Landroid/content/Context;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2248112
    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2248113
    const-string v2, "image/jpeg"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2248114
    iget-object v2, p0, LX/FUJ;->c:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v2, v2, Lcom/facebook/qrcode/QRCodeFragment;->s:Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/FUJ;->c:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v3, v3, Lcom/facebook/qrcode/QRCodeFragment;->e:Landroid/content/Context;

    invoke-interface {v2, v1, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2248115
    iget-object v1, p0, LX/FUJ;->c:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v1, v1, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    .line 2248116
    iget-object v2, v1, LX/FUf;->a:LX/0if;

    sget-object v3, LX/0ig;->H:LX/0ih;

    const-string v4, "MY_CODE_SHARED_SUCCESS"

    invoke-virtual {v2, v3, v4}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2248117
    iget-object v1, p0, LX/FUJ;->c:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v1, v1, Lcom/facebook/qrcode/QRCodeFragment;->p:LX/FUW;

    iget-object v2, p0, LX/FUJ;->c:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v2, v2, Lcom/facebook/qrcode/QRCodeFragment;->M:LX/FUV;

    sget-object v3, LX/FUV;->VANITY:LX/FUV;

    if-ne v2, v3, :cond_1

    .line 2248118
    :goto_1
    const-string v2, "qrcode_shared"

    const-string v3, "vanity"

    invoke-static {v1, v2, v3, v0}, LX/FUW;->a(LX/FUW;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2248119
    return-void

    .line 2248120
    :cond_0
    const-string v2, "android.intent.extra.STREAM"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "file://"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, LX/FUJ;->b:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_0

    .line 2248121
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 5

    .prologue
    .line 2248122
    iget-object v0, p0, LX/FUJ;->c:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->v:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0832e5

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2248123
    iget-object v0, p0, LX/FUJ;->c:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    .line 2248124
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v1

    const-string v2, "msg"

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v1

    .line 2248125
    iget-object v2, v0, LX/FUf;->a:LX/0if;

    sget-object v3, LX/0ig;->H:LX/0ih;

    const-string v4, "EXCEPTION_ON_SHARE"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, v3, v4, p0, v1}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2248126
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2248127
    iget-object v0, p0, LX/FUJ;->c:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->v:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0832e4

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2248128
    iget-object v0, p0, LX/FUJ;->c:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    .line 2248129
    iget-object v1, v0, LX/FUf;->a:LX/0if;

    sget-object v2, LX/0ig;->H:LX/0ih;

    const-string p0, "ERROR_ON_SHARE"

    invoke-virtual {v1, v2, p0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2248130
    return-void
.end method
