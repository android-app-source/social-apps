.class public LX/FVu;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2251403
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2251404
    return-void
.end method

.method public static final a(LX/FVt;Lcom/facebook/graphql/enums/GraphQLSavedState;Z)LX/FVt;
    .locals 2

    .prologue
    .line 2251405
    new-instance v0, LX/FVr;

    invoke-direct {v0}, LX/FVr;-><init>()V

    invoke-static {p0}, LX/FVr;->a(LX/FVt;)LX/FVr;

    move-result-object v0

    .line 2251406
    iget-object v1, p0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v1, v1

    .line 2251407
    new-instance p0, LX/7A1;

    invoke-direct {p0}, LX/7A1;-><init>()V

    invoke-static {v1}, LX/7A1;->a(Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;)LX/7A1;

    move-result-object p0

    .line 2251408
    iput-object p1, p0, LX/7A1;->C:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2251409
    move-object p0, p0

    .line 2251410
    invoke-virtual {p0}, LX/7A1;->a()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-result-object p0

    .line 2251411
    move-object v1, p0

    .line 2251412
    iput-object v1, v0, LX/FVr;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    .line 2251413
    move-object v0, v0

    .line 2251414
    iput-boolean p2, v0, LX/FVr;->j:Z

    .line 2251415
    move-object v0, v0

    .line 2251416
    invoke-virtual {v0}, LX/FVr;->a()LX/FVt;

    move-result-object v0

    .line 2251417
    return-object v0
.end method

.method public static a(LX/FVt;Z)LX/FVt;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2251392
    if-eqz p1, :cond_0

    new-instance v1, LX/186;

    const/16 v2, 0x400

    invoke-direct {v1, v2}, LX/186;-><init>(I)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/186;->c(I)V

    invoke-virtual {v1, v0, v0, v0}, LX/186;->a(III)V

    const v0, 0x52c4008e

    invoke-static {v1, v0}, LX/1vs;->a(LX/186;I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2251393
    :goto_0
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2251394
    new-instance v2, LX/7A1;

    invoke-direct {v2}, LX/7A1;-><init>()V

    .line 2251395
    iget-object v2, p0, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v2, v2

    .line 2251396
    invoke-static {v2}, LX/7A1;->a(Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;)LX/7A1;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, LX/7A1;->a(LX/15i;I)LX/7A1;

    move-result-object v0

    invoke-virtual {v0}, LX/7A1;->a()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-result-object v0

    .line 2251397
    new-instance v1, LX/FVr;

    invoke-direct {v1}, LX/FVr;-><init>()V

    invoke-static {p0}, LX/FVr;->a(LX/FVt;)LX/FVr;

    move-result-object v1

    .line 2251398
    iput-object v0, v1, LX/FVr;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    .line 2251399
    move-object v0, v1

    .line 2251400
    invoke-virtual {v0}, LX/FVr;->a()LX/FVt;

    move-result-object v0

    return-object v0

    .line 2251401
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 2251402
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a(LX/0QB;)LX/FVu;
    .locals 1

    .prologue
    .line 2251389
    new-instance v0, LX/FVu;

    invoke-direct {v0}, LX/FVu;-><init>()V

    .line 2251390
    move-object v0, v0

    .line 2251391
    return-object v0
.end method
