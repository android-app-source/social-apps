.class public LX/G1A;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/timeline/profileprotocol/SetNotificationPreferenceMethod$Params;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2310763
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2310764
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2310765
    check-cast p1, Lcom/facebook/timeline/profileprotocol/SetNotificationPreferenceMethod$Params;

    .line 2310766
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2310767
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "notifier_id"

    iget-wide v2, p1, Lcom/facebook/timeline/profileprotocol/SetNotificationPreferenceMethod$Params;->a:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2310768
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "enable"

    iget-boolean v2, p1, Lcom/facebook/timeline/profileprotocol/SetNotificationPreferenceMethod$Params;->b:Z

    invoke-static {v2}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2310769
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "format"

    const-string v2, "json"

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2310770
    new-instance v0, LX/14N;

    const-string v1, "subscribeToProfile"

    const-string v2, "POST"

    const-string v3, "me/notify_me"

    sget-object v5, LX/14S;->JSON:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2310771
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2310772
    const/4 v0, 0x0

    return-object v0
.end method
