.class public LX/GYO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2365141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2365142
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 2365143
    const-string v0, "extra_product_item_id_to_fetch"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v8

    .line 2365144
    const-string v0, "com.facebook.katana.profile.id"

    const-wide/16 v2, 0x0

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2365145
    const-string v2, "extra_admin_product_item"

    invoke-static {p1, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/7ja;

    .line 2365146
    const-string v3, "extra_currency"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Ljava/util/Currency;

    .line 2365147
    const-string v4, "extra_featured_products_count"

    invoke-virtual {p1, v4, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 2365148
    const-string v5, "extra_wait_for_mutation_finish"

    invoke-virtual {p1, v5, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 2365149
    const-string v6, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2365150
    const-string v7, "extra_has_empty_catalog"

    invoke-virtual {p1, v7, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 2365151
    const-string v9, "extra_requires_initial_fetch"

    invoke-virtual {p1, v9, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    .line 2365152
    if-eqz v9, :cond_0

    .line 2365153
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2365154
    const-string v2, "com.facebook.katana.profile.id"

    invoke-virtual {v3, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2365155
    const-string v2, "extra_requires_initial_fetch"

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2365156
    const-string v4, "extra_product_item_id_to_fetch"

    invoke-virtual {v8}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2365157
    const-string v2, "extra_wait_for_mutation_finish"

    invoke-virtual {v3, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2365158
    const-string v2, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v3, v2, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2365159
    new-instance v2, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;

    invoke-direct {v2}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;-><init>()V

    .line 2365160
    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2365161
    move-object v0, v2

    .line 2365162
    :goto_0
    return-object v0

    .line 2365163
    :cond_0
    invoke-static {v6}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2365164
    iget-boolean v11, v6, Lcom/facebook/auth/viewercontext/ViewerContext;->d:Z

    move v11, v11

    .line 2365165
    invoke-static {v11}, LX/0PB;->checkState(Z)V

    .line 2365166
    const-wide/16 v11, 0x0

    cmp-long v11, v0, v11

    if-lez v11, :cond_1

    const/4 v11, 0x1

    :goto_1
    invoke-static {v11}, LX/0PB;->checkState(Z)V

    .line 2365167
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2365168
    new-instance v11, Landroid/os/Bundle;

    invoke-direct {v11}, Landroid/os/Bundle;-><init>()V

    .line 2365169
    const-string v12, "com.facebook.katana.profile.id"

    invoke-virtual {v11, v12, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2365170
    const-string v12, "extra_admin_product_item"

    invoke-static {v11, v12, v2}, LX/4By;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2365171
    const-string v12, "extra_currency"

    invoke-virtual {v11, v12, v3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 2365172
    const-string v12, "extra_featured_products_count"

    invoke-virtual {v11, v12, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2365173
    const-string v12, "extra_wait_for_mutation_finish"

    invoke-virtual {v11, v12, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2365174
    const-string v12, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v11, v12, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2365175
    const-string v12, "extra_has_empty_catalog"

    invoke-virtual {v11, v12, v7}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2365176
    new-instance v12, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;

    invoke-direct {v12}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;-><init>()V

    .line 2365177
    invoke-virtual {v12, v11}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2365178
    move-object v0, v12

    .line 2365179
    goto :goto_0

    .line 2365180
    :cond_1
    const/4 v11, 0x0

    goto :goto_1
.end method
