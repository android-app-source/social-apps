.class public final LX/FqA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1CH;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/TimelineFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/TimelineFragment;)V
    .locals 0

    .prologue
    .line 2292541
    iput-object p1, p0, LX/FqA;->a:Lcom/facebook/timeline/TimelineFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    .line 2292542
    iget-object v0, p0, LX/FqA;->a:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2292543
    iget-object v0, p0, LX/FqA;->a:Lcom/facebook/timeline/TimelineFragment;

    const-string v1, "timeline_optimistic_post_failed"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to post to profile "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/FqA;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v3, v3, Lcom/facebook/timeline/TimelineFragment;->aZ:LX/BP0;

    .line 2292544
    iget-wide v6, v3, LX/5SB;->b:J

    move-wide v4, v6

    .line 2292545
    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/timeline/TimelineFragment;->a$redex0(Lcom/facebook/timeline/TimelineFragment;Ljava/lang/String;Ljava/lang/String;)V

    .line 2292546
    iget-object v0, p0, LX/FqA;->a:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/TimelineFragment;->mJ_()V

    .line 2292547
    return-void
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 1

    .prologue
    .line 2292548
    iget-object v0, p0, LX/FqA;->a:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2292549
    return-void
.end method

.method public final a(JLcom/facebook/graphql/model/GraphQLStory;)Z
    .locals 1

    .prologue
    .line 2292550
    iget-object v0, p0, LX/FqA;->a:Lcom/facebook/timeline/TimelineFragment;

    invoke-static {v0, p1, p2, p3}, Lcom/facebook/timeline/TimelineFragment;->a$redex0(Lcom/facebook/timeline/TimelineFragment;JLcom/facebook/graphql/model/GraphQLStory;)Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2292551
    iget-object v0, p0, LX/FqA;->a:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/TimelineFragment;->mJ_()V

    .line 2292552
    return-void
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 7

    .prologue
    .line 2292553
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2292554
    iget-object v0, p0, LX/FqA;->a:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/TimelineFragment;->o()LX/G4x;

    move-result-object v0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v2

    const/4 v4, -0x1

    .line 2292555
    iget-object v3, v0, LX/G4x;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/G59;

    .line 2292556
    invoke-virtual {v3, v1, v2}, LX/G59;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    .line 2292557
    if-eq v3, v4, :cond_0

    .line 2292558
    :goto_0
    move v0, v3

    .line 2292559
    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 2292560
    iget-object v0, p0, LX/FqA;->a:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/TimelineFragment;->o()LX/G4x;

    move-result-object v0

    .line 2292561
    iget-object v1, v0, LX/G4x;->e:LX/0qm;

    move-object v0, v1

    .line 2292562
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v1

    .line 2292563
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2292564
    :cond_1
    :goto_1
    iget-object v0, p0, LX/FqA;->a:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/TimelineFragment;->o()LX/G4x;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/G4x;->a(Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2292565
    invoke-virtual {p0}, LX/FqA;->b()V

    .line 2292566
    :cond_2
    iget-object v0, p0, LX/FqA;->a:Lcom/facebook/timeline/TimelineFragment;

    .line 2292567
    iget-object v1, v0, Lcom/facebook/timeline/TimelineFragment;->bH:LX/1Ar;

    invoke-virtual {v1}, LX/1Ar;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2292568
    invoke-static {p1}, LX/16y;->b(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v1

    .line 2292569
    if-nez v1, :cond_8

    .line 2292570
    :cond_3
    :goto_2
    iget-object v0, p0, LX/FqA;->a:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/TimelineFragment;->G()V

    .line 2292571
    return-void

    :cond_4
    move v3, v4

    goto :goto_0

    .line 2292572
    :cond_5
    iget-object v3, v0, LX/0qm;->b:LX/0qp;

    invoke-virtual {v3}, LX/0qp;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 2292573
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v4

    instance-of v4, v4, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v4, :cond_6

    .line 2292574
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;->c()Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2292575
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2292576
    :goto_3
    move-object v3, v3

    .line 2292577
    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2292578
    invoke-virtual {v0, v3}, LX/0qm;->a(Ljava/lang/String;)V

    .line 2292579
    goto :goto_1

    :cond_7
    const/4 v3, 0x0

    goto :goto_3

    .line 2292580
    :cond_8
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->b()LX/0Px;

    move-result-object v3

    .line 2292581
    if-eqz v3, :cond_3

    .line 2292582
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_4
    if-ge v2, v4, :cond_3

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLEntityAtRange;

    .line 2292583
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntityAtRange;->j()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v1

    .line 2292584
    if-eqz v1, :cond_9

    .line 2292585
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    .line 2292586
    if-eqz v5, :cond_9

    .line 2292587
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLEntity;->d()Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    move-result-object v1

    .line 2292588
    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v5

    const v6, -0x36bd2417

    if-ne v5, v6, :cond_9

    if-eqz v1, :cond_9

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;->NEW_YEARS:Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;

    invoke-virtual {v1, v5}, Lcom/facebook/graphql/enums/GraphQLFormattedTextTypeEnum;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 2292589
    iget-object v1, v0, Lcom/facebook/timeline/TimelineFragment;->bG:Lcom/facebook/delights/floating/DelightsFireworks;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/delights/floating/DelightsFireworks;->a(Landroid/content/Context;)V

    goto/16 :goto_2

    .line 2292590
    :cond_9
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_4
.end method
