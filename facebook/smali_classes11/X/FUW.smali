.class public LX/FUW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6HF;


# instance fields
.field private a:LX/0Zb;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2248716
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2248717
    iput-object p1, p0, LX/FUW;->a:LX/0Zb;

    .line 2248718
    return-void
.end method

.method public static a(LX/0QB;)LX/FUW;
    .locals 1

    .prologue
    .line 2248719
    invoke-static {p0}, LX/FUW;->b(LX/0QB;)LX/FUW;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/FUW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 1

    .prologue
    .line 2248720
    iget-object v0, p0, LX/FUW;->a:LX/0Zb;

    invoke-interface {v0, p1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2248721
    return-void
.end method

.method public static a(LX/FUW;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 2248722
    invoke-static {p0, p1}, LX/FUW;->f(LX/FUW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2248723
    invoke-virtual {v0, p2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2248724
    invoke-static {p0, v0}, LX/FUW;->a(LX/FUW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2248725
    return-void
.end method

.method public static b(LX/0QB;)LX/FUW;
    .locals 2

    .prologue
    .line 2248726
    new-instance v1, LX/FUW;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/FUW;-><init>(LX/0Zb;)V

    .line 2248727
    return-object v1
.end method

.method public static e(LX/FUW;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2248713
    invoke-static {p0, p1}, LX/FUW;->f(LX/FUW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-static {p0, v0}, LX/FUW;->a(LX/FUW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2248714
    return-void
.end method

.method public static f(LX/FUW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 2248728
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2248729
    const-string v1, "qrcode"

    .line 2248730
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2248731
    iget-object v1, p0, LX/FUW;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2248732
    iget-object v1, p0, LX/FUW;->b:Ljava/lang/String;

    .line 2248733
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->f:Ljava/lang/String;

    .line 2248734
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a(IZ)V
    .locals 0

    .prologue
    .line 2248735
    return-void
.end method

.method public final a(LX/6HG;)V
    .locals 0

    .prologue
    .line 2248736
    return-void
.end method

.method public final a(LX/6HG;I)V
    .locals 0

    .prologue
    .line 2248737
    return-void
.end method

.method public final a(LX/6IG;)V
    .locals 0

    .prologue
    .line 2248738
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 2248739
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2248740
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2248741
    return-void
.end method

.method public final a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2248715
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2248688
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 2248689
    const-string v0, "qrcode_camera_exception"

    invoke-static {p0, v0}, LX/FUW;->f(LX/FUW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2248690
    const-string v1, "message"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2248691
    invoke-static {p0, v0}, LX/FUW;->a(LX/FUW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2248692
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 2248693
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 2248694
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2248695
    return-void
.end method

.method public final b(LX/6IG;)V
    .locals 0

    .prologue
    .line 2248696
    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2248697
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2248698
    return-void
.end method

.method public final b(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 2248699
    if-eqz p2, :cond_0

    const-string v0, "qrcode_code_scanned"

    .line 2248700
    :goto_0
    invoke-static {p0, v0}, LX/FUW;->f(LX/FUW;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2248701
    const-string v1, "decoded_string"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2248702
    invoke-static {p0, v0}, LX/FUW;->a(LX/FUW;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2248703
    return-void

    .line 2248704
    :cond_0
    const-string v0, "qrcode_code_imported"

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 2248705
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2248706
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 2248707
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 2248708
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2248709
    const-string v0, "qrcode_import_exception"

    invoke-static {p0, v0}, LX/FUW;->e(LX/FUW;Ljava/lang/String;)V

    .line 2248710
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 2248711
    const-string v0, "qrcode_import_oom"

    invoke-static {p0, v0}, LX/FUW;->e(LX/FUW;Ljava/lang/String;)V

    .line 2248712
    return-void
.end method
