.class public LX/GYo;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/GYq;

.field public final c:Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;

.field public final d:LX/GYx;

.field public final e:LX/GZ0;

.field public final f:LX/GYu;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/GYk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/GYk",
            "<",
            "Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/GYk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/GYk",
            "<",
            "Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductItemModels$FetchAdminCommerceProductItemModel;",
            ">;"
        }
    .end annotation
.end field

.field public final k:LX/GYk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/GYk",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2365795
    const-class v0, LX/GYo;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/GYo;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/GYq;Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;LX/GYx;LX/GZ0;LX/GYu;LX/0Ot;LX/0Ot;)V
    .locals 1
    .param p2    # Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/GYq;",
            "Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragmentController$ViewDelegate;",
            "LX/GYx;",
            "LX/GZ0;",
            "LX/GYu;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2365783
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2365784
    new-instance v0, LX/GYl;

    invoke-direct {v0, p0}, LX/GYl;-><init>(LX/GYo;)V

    iput-object v0, p0, LX/GYo;->i:LX/GYk;

    .line 2365785
    new-instance v0, LX/GYm;

    invoke-direct {v0, p0}, LX/GYm;-><init>(LX/GYo;)V

    iput-object v0, p0, LX/GYo;->j:LX/GYk;

    .line 2365786
    new-instance v0, LX/GYn;

    invoke-direct {v0, p0}, LX/GYn;-><init>(LX/GYo;)V

    iput-object v0, p0, LX/GYo;->k:LX/GYk;

    .line 2365787
    iput-object p1, p0, LX/GYo;->b:LX/GYq;

    .line 2365788
    iput-object p2, p0, LX/GYo;->c:Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;

    .line 2365789
    iput-object p3, p0, LX/GYo;->d:LX/GYx;

    .line 2365790
    iput-object p4, p0, LX/GYo;->e:LX/GZ0;

    .line 2365791
    iput-object p5, p0, LX/GYo;->f:LX/GYu;

    .line 2365792
    iput-object p6, p0, LX/GYo;->g:LX/0Ot;

    .line 2365793
    iput-object p7, p0, LX/GYo;->h:LX/0Ot;

    .line 2365794
    return-void
.end method

.method public static a$redex0(LX/GYo;Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel;)V
    .locals 4
    .param p0    # LX/GYo;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2365758
    if-eqz p1, :cond_4

    .line 2365759
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2365760
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel;->j()Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel$CommerceStoreModel;

    move-result-object v2

    if-nez v2, :cond_5

    :cond_0
    move v2, v0

    :goto_0
    if-eqz v2, :cond_7

    :cond_1
    :goto_1
    if-eqz v0, :cond_8

    .line 2365761
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    .line 2365762
    :goto_2
    move-object v0, v0

    .line 2365763
    const/4 v1, 0x1

    .line 2365764
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel;->j()Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel$CommerceStoreModel;

    move-result-object v2

    if-nez v2, :cond_9

    :cond_2
    :goto_3
    if-eqz v1, :cond_a

    .line 2365765
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    .line 2365766
    :goto_4
    move-object v1, v1

    .line 2365767
    iget-object v2, p0, LX/GYo;->c:Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;

    .line 2365768
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2365769
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Currency;

    iput-object v3, v2, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->w:Ljava/util/Currency;

    .line 2365770
    :cond_3
    invoke-virtual {v1}, LX/0am;->isPresent()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2365771
    invoke-virtual {v1}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, v2, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->z:I

    .line 2365772
    :cond_4
    return-void

    .line 2365773
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel;->j()Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel$CommerceStoreModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel$CommerceStoreModel;->a()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2365774
    if-nez v2, :cond_6

    move v2, v0

    goto :goto_0

    :cond_6
    move v2, v1

    goto :goto_0

    .line 2365775
    :cond_7
    invoke-virtual {p1}, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel;->j()Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel$CommerceStoreModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel$CommerceStoreModel;->a()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2365776
    invoke-virtual {v3, v2, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_1

    .line 2365777
    :cond_8
    invoke-virtual {p1}, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel;->j()Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel$CommerceStoreModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel$CommerceStoreModel;->a()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2365778
    invoke-virtual {v2, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/7j4;->b(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0

    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    goto :goto_2

    .line 2365779
    :cond_9
    invoke-virtual {p1}, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel;->j()Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel$CommerceStoreModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel$CommerceStoreModel;->j()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2365780
    if-eqz v2, :cond_2

    const/4 v1, 0x0

    goto :goto_3

    .line 2365781
    :cond_a
    invoke-virtual {p1}, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel;->j()Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel$CommerceStoreModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel$CommerceStoreModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2365782
    invoke-static {v2, v1}, LX/7jZ;->a(LX/15i;I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    goto/16 :goto_4
.end method

.method public static a$redex0(LX/GYo;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2365796
    if-nez p2, :cond_0

    .line 2365797
    new-instance p2, Ljava/lang/Throwable;

    invoke-direct {p2, p1}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    .line 2365798
    :cond_0
    invoke-virtual {p0}, LX/GYo;->a()V

    .line 2365799
    iget-object v0, p0, LX/GYo;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    sget-object v1, LX/GYo;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1, p2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2365800
    iget-object v0, p0, LX/GYo;->c:Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;

    .line 2365801
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->F:Landroid/widget/ProgressBar;

    const/16 p0, 0x8

    invoke-virtual {v1, p0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2365802
    return-void
.end method

.method public static b(LX/GYo;)V
    .locals 7

    .prologue
    .line 2365656
    iget-object v0, p0, LX/GYo;->c:Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;

    .line 2365657
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 2365658
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->E:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2365659
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->F:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2365660
    const v1, 0x7f0d04d2

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/core/ui/NoticeView;

    iput-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->G:Lcom/facebook/commerce/core/ui/NoticeView;

    .line 2365661
    const v1, 0x7f0d04d3

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->H:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2365662
    const v1, 0x7f0d04d4

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbEditText;

    iput-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->I:Lcom/facebook/resources/ui/FbEditText;

    .line 2365663
    const v1, 0x7f0d04d5

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbEditText;

    iput-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->J:Lcom/facebook/resources/ui/FbEditText;

    .line 2365664
    const v1, 0x7f0d04d6

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbEditText;

    iput-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->K:Lcom/facebook/resources/ui/FbEditText;

    .line 2365665
    const v1, 0x7f0d04d9

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->L:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2365666
    const v1, 0x7f0d04db

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;

    iput-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->M:Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;

    .line 2365667
    const v1, 0x7f0d04da

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->N:Landroid/view/View;

    .line 2365668
    const v1, 0x7f0d04dc

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/core/ui/NoticeView;

    iput-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->O:Lcom/facebook/commerce/core/ui/NoticeView;

    .line 2365669
    const v1, 0x7f0d04dd

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    iput-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->P:Lcom/facebook/widget/text/BetterTextView;

    .line 2365670
    const v1, 0x7f0d04cd

    invoke-virtual {v0, v1}, Lcom/facebook/base/fragment/FbFragment;->b(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 2365671
    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->V:LX/GYQ;

    .line 2365672
    iget-boolean v3, v2, LX/GYQ;->c:Z

    if-eqz v3, :cond_0

    .line 2365673
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v6

    .line 2365674
    const v3, 0x7f0d04cf

    invoke-static {v6, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CompoundButton;

    iput-object v3, v2, LX/GYQ;->a:Landroid/widget/CompoundButton;

    .line 2365675
    const v3, 0x7f0d04d0

    invoke-static {v6, v3}, LX/0jc;->b(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    iput-object v3, v2, LX/GYQ;->b:Landroid/view/View;

    .line 2365676
    iget-object v3, v2, LX/GYQ;->a:Landroid/widget/CompoundButton;

    new-instance v6, LX/GYP;

    invoke-direct {v6, v2}, LX/GYP;-><init>(LX/GYQ;)V

    invoke-virtual {v3, v6}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2365677
    :cond_0
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->G:Lcom/facebook/commerce/core/ui/NoticeView;

    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->Y:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/facebook/commerce/core/ui/NoticeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2365678
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->O:Lcom/facebook/commerce/core/ui/NoticeView;

    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->Y:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/facebook/commerce/core/ui/NoticeView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2365679
    new-instance v1, LX/4At;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f0829bb

    invoke-direct {v1, v2, v3}, LX/4At;-><init>(Landroid/content/Context;I)V

    iput-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->S:LX/4At;

    .line 2365680
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->I:Lcom/facebook/resources/ui/FbEditText;

    new-instance v2, LX/GYg;

    iget-object v3, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->I:Lcom/facebook/resources/ui/FbEditText;

    invoke-direct {v2, v3}, LX/GYg;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2365681
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->J:Lcom/facebook/resources/ui/FbEditText;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->setRawInputType(I)V

    .line 2365682
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->J:Lcom/facebook/resources/ui/FbEditText;

    new-instance v2, LX/GYg;

    iget-object v3, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->J:Lcom/facebook/resources/ui/FbEditText;

    invoke-direct {v2, v3}, LX/GYg;-><init>(Landroid/widget/TextView;)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2365683
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->J:Lcom/facebook/resources/ui/FbEditText;

    new-instance v2, LX/GYW;

    invoke-direct {v2, v0}, LX/GYW;-><init>(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2365684
    new-instance v1, LX/GXS;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, LX/GXS;-><init>(Ljava/util/List;)V

    iput-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->R:LX/GXS;

    .line 2365685
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->o:LX/GZ4;

    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->R:LX/GXS;

    .line 2365686
    iput-object v2, v1, LX/GZ4;->d:LX/GXS;

    .line 2365687
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->R:LX/GXS;

    new-instance v3, LX/GYX;

    invoke-direct {v3, v0}, LX/GYX;-><init>(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V

    .line 2365688
    new-instance v6, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;

    invoke-direct {v6, v1, v2, v3}, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;-><init>(Landroid/content/Context;LX/GXS;Landroid/view/View$OnClickListener;)V

    .line 2365689
    move-object v1, v6

    .line 2365690
    iput-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->Q:Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;

    .line 2365691
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->Q:Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;

    new-instance v2, LX/GYY;

    invoke-direct {v2, v0}, LX/GYY;-><init>(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V

    .line 2365692
    iput-object v2, v1, Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;->g:LX/GYY;

    .line 2365693
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->R:LX/GXS;

    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->Q:Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;

    .line 2365694
    iput-object v2, v1, LX/GXS;->b:LX/1OM;

    .line 2365695
    new-instance v1, LX/1P0;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v4, v4}, LX/1P0;-><init>(Landroid/content/Context;IZ)V

    .line 2365696
    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->H:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2365697
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->H:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->Q:Lcom/facebook/commerce/publishing/adapter/ProductEditImagesAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2365698
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->H:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    new-instance v2, LX/GXm;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0062

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v2, v3}, LX/GXm;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->a(LX/3x6;)V

    .line 2365699
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->L:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v2, LX/GYZ;

    invoke-direct {v2, v0}, LX/GYZ;-><init>(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2365700
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->M:Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;

    const/4 v2, 0x1

    .line 2365701
    invoke-static {v0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->C(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    invoke-interface {v3}, LX/7ja;->b()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2365702
    :cond_1
    :goto_0
    move v2, v2

    .line 2365703
    iput-boolean v2, v1, Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;->a:Z

    .line 2365704
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->N:Landroid/view/View;

    new-instance v2, LX/GYa;

    invoke-direct {v2, v0}, LX/GYa;-><init>(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2365705
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->P:Lcom/facebook/widget/text/BetterTextView;

    new-instance v2, LX/GYd;

    invoke-direct {v2, v0}, LX/GYd;-><init>(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2365706
    invoke-static {v0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->C(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2365707
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->P:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v1, v5}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2365708
    :cond_2
    invoke-static {v0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->C(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    invoke-interface {v1}, LX/7ja;->ew_()LX/0Px;

    move-result-object v1

    if-nez v1, :cond_7

    .line 2365709
    :cond_3
    :goto_1
    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->M:Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;

    invoke-static {v0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->C(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)Z

    move-result v1

    if-nez v1, :cond_a

    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    invoke-interface {v1}, LX/7ja;->b()Z

    move-result v1

    if-eqz v1, :cond_a

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v2, v1}, Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;->setChecked(Z)V

    .line 2365710
    invoke-static {v0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->C(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2365711
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2365712
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->I:Lcom/facebook/resources/ui/FbEditText;

    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    invoke-interface {v2}, LX/7ja;->ev_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2365713
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->K:Lcom/facebook/resources/ui/FbEditText;

    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    invoke-interface {v2}, LX/7ja;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2365714
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    invoke-interface {v1}, LX/7ja;->k()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2365715
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->J:Lcom/facebook/resources/ui/FbEditText;

    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    invoke-interface {v2}, LX/7ja;->k()Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;

    move-result-object v2

    invoke-static {v2}, LX/7j4;->a(Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$ProductItemPriceFieldsModel;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/resources/ui/FbEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2365716
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->R:LX/GXS;

    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->y:LX/0Px;

    .line 2365717
    if-nez v2, :cond_b

    .line 2365718
    const/4 v3, 0x0

    .line 2365719
    :goto_3
    move-object v2, v3

    .line 2365720
    if-eqz v2, :cond_4

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_d

    .line 2365721
    :cond_4
    :goto_4
    invoke-static {v0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->q(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V

    .line 2365722
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 2365723
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    invoke-interface {v1}, LX/7ja;->c()Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    move-result-object v1

    .line 2365724
    invoke-static {v1}, LX/7j9;->a(Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;)LX/7iu;

    move-result-object v2

    .line 2365725
    if-eqz v2, :cond_e

    .line 2365726
    iget-object v3, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->G:Lcom/facebook/commerce/core/ui/NoticeView;

    invoke-virtual {v3, v4}, Lcom/facebook/commerce/core/ui/NoticeView;->setVisibility(I)V

    .line 2365727
    iget-object v3, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->G:Lcom/facebook/commerce/core/ui/NoticeView;

    invoke-virtual {v3, v2}, Lcom/facebook/commerce/core/ui/NoticeView;->setLevel(LX/7iu;)V

    .line 2365728
    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->G:Lcom/facebook/commerce/core/ui/NoticeView;

    iget-object v3, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->i:LX/7j9;

    invoke-virtual {v3, v1}, LX/7j9;->b(Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/commerce/core/ui/NoticeView;->setTitle(Ljava/lang/String;)V

    .line 2365729
    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->G:Lcom/facebook/commerce/core/ui/NoticeView;

    iget-object v3, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->i:LX/7j9;

    invoke-virtual {v3, v1}, LX/7j9;->c(Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/commerce/core/ui/NoticeView;->setMessage(Ljava/lang/String;)V

    .line 2365730
    :goto_5
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;->VISIBLE:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    if-ne v1, v2, :cond_f

    .line 2365731
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->O:Lcom/facebook/commerce/core/ui/NoticeView;

    invoke-virtual {v1, v4}, Lcom/facebook/commerce/core/ui/NoticeView;->setVisibility(I)V

    .line 2365732
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->O:Lcom/facebook/commerce/core/ui/NoticeView;

    sget-object v2, LX/7iu;->NOTIFY:LX/7iu;

    invoke-virtual {v1, v2}, Lcom/facebook/commerce/core/ui/NoticeView;->setLevel(LX/7iu;)V

    .line 2365733
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->O:Lcom/facebook/commerce/core/ui/NoticeView;

    const v2, 0x7f0814c1

    invoke-virtual {v0, v2}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/commerce/core/ui/NoticeView;->setMessage(Ljava/lang/String;)V

    .line 2365734
    :goto_6
    new-instance v1, LX/GYi;

    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->I:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v2}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->J:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v3}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->K:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v4}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->M:Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;

    invoke-virtual {v5}, Lcom/facebook/commerce/publishing/ui/InterceptSwitchCompat;->isChecked()Z

    move-result v5

    invoke-direct {v1, v2, v3, v4, v5}, LX/GYi;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->C:LX/GYi;

    .line 2365735
    invoke-static {v0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->l(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V

    .line 2365736
    return-void

    .line 2365737
    :cond_5
    invoke-static {v0}, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->q(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V

    goto :goto_6

    :cond_6
    iget v3, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->z:I

    const/16 v4, 0xa

    if-lt v3, v4, :cond_1

    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2365738
    :cond_7
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 2365739
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2365740
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    invoke-interface {v1}, LX/7ja;->ew_()LX/0Px;

    move-result-object v5

    .line 2365741
    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v1, 0x0

    move v2, v1

    :goto_7
    if-ge v2, v6, :cond_9

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$OrderedImagesModel;

    .line 2365742
    invoke-virtual {v1}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$OrderedImagesModel;->b()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v3, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_8

    .line 2365743
    invoke-virtual {v1}, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$OrderedImagesModel;->b()Ljava/lang/String;

    move-result-object p0

    invoke-interface {v3, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2365744
    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2365745
    :cond_8
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_7

    .line 2365746
    :cond_9
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->y:LX/0Px;

    goto/16 :goto_1

    .line 2365747
    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_2

    .line 2365748
    :cond_b
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2365749
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_8
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel$OrderedImagesModel;

    .line 2365750
    new-instance v6, LX/GXR;

    invoke-direct {v6, v3}, LX/GXR;-><init>(Ljava/lang/Object;)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_c
    move-object v3, v4

    .line 2365751
    goto/16 :goto_3

    .line 2365752
    :cond_d
    iget-object v3, v1, LX/GXS;->a:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2365753
    iget-object v3, v1, LX/GXS;->b:LX/1OM;

    if-eqz v3, :cond_4

    .line 2365754
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 2365755
    iget-object v4, v1, LX/GXS;->b:LX/1OM;

    invoke-virtual {v1}, LX/GXS;->a()I

    move-result v5

    sub-int/2addr v5, v3

    invoke-virtual {v4, v5, v3}, LX/1OM;->c(II)V

    goto/16 :goto_4

    .line 2365756
    :cond_e
    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->G:Lcom/facebook/commerce/core/ui/NoticeView;

    invoke-virtual {v2, v5}, Lcom/facebook/commerce/core/ui/NoticeView;->setVisibility(I)V

    goto/16 :goto_5

    .line 2365757
    :cond_f
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->O:Lcom/facebook/commerce/core/ui/NoticeView;

    invoke-virtual {v1, v5}, Lcom/facebook/commerce/core/ui/NoticeView;->setVisibility(I)V

    goto/16 :goto_6
.end method

.method public static c(LX/GYo;)V
    .locals 1

    .prologue
    .line 2365651
    iget-object v0, p0, LX/GYo;->b:LX/GYq;

    invoke-virtual {v0}, LX/GYq;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 2365652
    :goto_0
    if-eqz v0, :cond_0

    .line 2365653
    invoke-static {p0}, LX/GYo;->b(LX/GYo;)V

    .line 2365654
    :cond_0
    return-void

    .line 2365655
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2365646
    iget-object v0, p0, LX/GYo;->b:LX/GYq;

    .line 2365647
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/GYq;->b:Z

    .line 2365648
    iget-object v1, v0, LX/GYq;->a:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->clear()V

    .line 2365649
    iget-object v0, p0, LX/GYo;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2365650
    return-void
.end method
