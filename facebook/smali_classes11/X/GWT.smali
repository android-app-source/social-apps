.class public final LX/GWT;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 2361946
    const/4 v12, 0x0

    .line 2361947
    const/4 v11, 0x0

    .line 2361948
    const/4 v10, 0x0

    .line 2361949
    const/4 v9, 0x0

    .line 2361950
    const/4 v8, 0x0

    .line 2361951
    const/4 v7, 0x0

    .line 2361952
    const/4 v6, 0x0

    .line 2361953
    const/4 v5, 0x0

    .line 2361954
    const/4 v4, 0x0

    .line 2361955
    const/4 v3, 0x0

    .line 2361956
    const/4 v2, 0x0

    .line 2361957
    const/4 v1, 0x0

    .line 2361958
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 2361959
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2361960
    const/4 v1, 0x0

    .line 2361961
    :goto_0
    return v1

    .line 2361962
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2361963
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_b

    .line 2361964
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 2361965
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2361966
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 2361967
    const-string v14, "checkout_url"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 2361968
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto :goto_1

    .line 2361969
    :cond_2
    const-string v14, "commerce_inventory"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 2361970
    const/4 v2, 0x1

    .line 2361971
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    goto :goto_1

    .line 2361972
    :cond_3
    const-string v14, "current_product_price"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 2361973
    invoke-static/range {p0 .. p1}, LX/7ir;->a(LX/15w;LX/186;)I

    move-result v10

    goto :goto_1

    .line 2361974
    :cond_4
    const-string v14, "external_url"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 2361975
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 2361976
    :cond_5
    const-string v14, "front_view_image"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 2361977
    invoke-static/range {p0 .. p1}, LX/GWR;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 2361978
    :cond_6
    const-string v14, "id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 2361979
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 2361980
    :cond_7
    const-string v14, "images"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 2361981
    invoke-static/range {p0 .. p1}, LX/GWS;->a(LX/15w;LX/186;)I

    move-result v6

    goto/16 :goto_1

    .line 2361982
    :cond_8
    const-string v14, "is_on_sale"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 2361983
    const/4 v1, 0x1

    .line 2361984
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v5

    goto/16 :goto_1

    .line 2361985
    :cond_9
    const-string v14, "product_item_price"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 2361986
    invoke-static/range {p0 .. p1}, LX/7ir;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 2361987
    :cond_a
    const-string v14, "variant_values"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 2361988
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 2361989
    :cond_b
    const/16 v13, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 2361990
    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 2361991
    if-eqz v2, :cond_c

    .line 2361992
    const/4 v2, 0x1

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11, v12}, LX/186;->a(III)V

    .line 2361993
    :cond_c
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2361994
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2361995
    const/4 v2, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2361996
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v7}, LX/186;->b(II)V

    .line 2361997
    const/4 v2, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v6}, LX/186;->b(II)V

    .line 2361998
    if-eqz v1, :cond_d

    .line 2361999
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->a(IZ)V

    .line 2362000
    :cond_d
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 2362001
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 2362002
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/16 v3, 0x9

    const/4 v2, 0x0

    .line 2362003
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2362004
    invoke-virtual {p0, p1, v2}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2362005
    if-eqz v0, :cond_0

    .line 2362006
    const-string v1, "checkout_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362007
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2362008
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2362009
    if-eqz v0, :cond_1

    .line 2362010
    const-string v1, "commerce_inventory"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362011
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2362012
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2362013
    if-eqz v0, :cond_2

    .line 2362014
    const-string v1, "current_product_price"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362015
    invoke-static {p0, v0, p2}, LX/7ir;->a(LX/15i;ILX/0nX;)V

    .line 2362016
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2362017
    if-eqz v0, :cond_3

    .line 2362018
    const-string v1, "external_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362019
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2362020
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2362021
    if-eqz v0, :cond_5

    .line 2362022
    const-string v1, "front_view_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362023
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2362024
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2362025
    if-eqz v1, :cond_4

    .line 2362026
    const-string v2, "uri"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362027
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2362028
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2362029
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2362030
    if-eqz v0, :cond_6

    .line 2362031
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362032
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2362033
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2362034
    if-eqz v0, :cond_9

    .line 2362035
    const-string v1, "images"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362036
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2362037
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_8

    .line 2362038
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    .line 2362039
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2362040
    const/4 v4, 0x0

    invoke-virtual {p0, v2, v4}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v4

    .line 2362041
    if-eqz v4, :cond_7

    .line 2362042
    const-string p3, "uri"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362043
    invoke-virtual {p2, v4}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2362044
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2362045
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2362046
    :cond_8
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2362047
    :cond_9
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2362048
    if-eqz v0, :cond_a

    .line 2362049
    const-string v1, "is_on_sale"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362050
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2362051
    :cond_a
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2362052
    if-eqz v0, :cond_b

    .line 2362053
    const-string v1, "product_item_price"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362054
    invoke-static {p0, v0, p2}, LX/7ir;->a(LX/15i;ILX/0nX;)V

    .line 2362055
    :cond_b
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2362056
    if-eqz v0, :cond_c

    .line 2362057
    const-string v0, "variant_values"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362058
    invoke-virtual {p0, p1, v3}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2362059
    :cond_c
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2362060
    return-void
.end method
