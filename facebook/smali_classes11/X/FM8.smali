.class public LX/FM8;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/FM8;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMI;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2228413
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2228414
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2228415
    iput-object v0, p0, LX/FM8;->a:LX/0Ot;

    .line 2228416
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2228417
    iput-object v0, p0, LX/FM8;->b:LX/0Ot;

    .line 2228418
    return-void
.end method

.method public static a(LX/0QB;)LX/FM8;
    .locals 5

    .prologue
    .line 2228398
    sget-object v0, LX/FM8;->c:LX/FM8;

    if-nez v0, :cond_1

    .line 2228399
    const-class v1, LX/FM8;

    monitor-enter v1

    .line 2228400
    :try_start_0
    sget-object v0, LX/FM8;->c:LX/FM8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2228401
    if-eqz v2, :cond_0

    .line 2228402
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2228403
    new-instance v3, LX/FM8;

    invoke-direct {v3}, LX/FM8;-><init>()V

    .line 2228404
    const/16 v4, 0x2982

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 p0, 0x29b7

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2228405
    iput-object v4, v3, LX/FM8;->a:LX/0Ot;

    iput-object p0, v3, LX/FM8;->b:LX/0Ot;

    .line 2228406
    move-object v0, v3

    .line 2228407
    sput-object v0, LX/FM8;->c:LX/FM8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2228408
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2228409
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2228410
    :cond_1
    sget-object v0, LX/FM8;->c:LX/FM8;

    return-object v0

    .line 2228411
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2228412
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/model/threads/ThreadParticipant;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2228360
    new-instance v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    new-instance v1, Lcom/facebook/user/model/UserKey;

    sget-object v2, LX/0XG;->EMAIL:LX/0XG;

    const-string v3, ""

    invoke-direct {v1, v2, v3}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    invoke-direct {v0, v1, p1}, Lcom/facebook/messaging/model/messages/ParticipantInfo;-><init>(Lcom/facebook/user/model/UserKey;Ljava/lang/String;)V

    .line 2228361
    new-instance v1, LX/6fz;

    invoke-direct {v1}, LX/6fz;-><init>()V

    .line 2228362
    iput-object v0, v1, LX/6fz;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2228363
    move-object v0, v1

    .line 2228364
    invoke-virtual {v0}, LX/6fz;->g()Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v1

    new-instance v2, LX/6fz;

    invoke-direct {v2}, LX/6fz;-><init>()V

    iget-object v0, p0, LX/FM8;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FNh;

    invoke-virtual {v0}, LX/FNh;->a()Lcom/facebook/messaging/model/messages/ParticipantInfo;

    move-result-object v0

    .line 2228365
    iput-object v0, v2, LX/6fz;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2228366
    move-object v0, v2

    .line 2228367
    invoke-virtual {v0}, LX/6fz;->g()Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v0

    invoke-static {v1, v0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/messaging/model/threads/ThreadsCollection;)LX/FM7;
    .locals 14

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 2228368
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2228369
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2228370
    iget-object v0, p1, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v8, v0

    .line 2228371
    new-instance v9, LX/FM7;

    invoke-direct {v9}, LX/FM7;-><init>()V

    move v1, v2

    move v4, v2

    .line 2228372
    :goto_0
    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v0

    if-ge v4, v0, :cond_1

    .line 2228373
    invoke-virtual {v8, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2228374
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadSummary;->e()Z

    move-result v10

    .line 2228375
    if-eqz v10, :cond_4

    .line 2228376
    add-int/lit8 v3, v1, 0x1

    .line 2228377
    :goto_1
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v1

    const/4 v11, 0x3

    if-ge v1, v11, :cond_0

    .line 2228378
    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v12

    .line 2228379
    iget-object v0, p0, LX/FM8;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMI;

    invoke-virtual {v0, v12, v13}, LX/FMI;->a(J)Ljava/util/List;

    move-result-object v1

    .line 2228380
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_0

    .line 2228381
    iget-object v0, p0, LX/FM8;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FNh;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/FNh;->a(Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/user/model/User;->i()Ljava/lang/String;

    move-result-object v0

    .line 2228382
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2228383
    if-eqz v10, :cond_0

    .line 2228384
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2228385
    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v1, v3

    move v4, v0

    .line 2228386
    goto :goto_0

    .line 2228387
    :cond_1
    const/4 v8, 0x3

    const/4 v4, 0x0

    .line 2228388
    const-string v0, ""

    .line 2228389
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    .line 2228390
    const-string v0, ", "

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3, v8}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-interface {v7, v4, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 2228391
    :cond_2
    :goto_2
    move-object v0, v0

    .line 2228392
    iput-object v0, v9, LX/FM7;->a:Ljava/lang/String;

    .line 2228393
    iput v1, v9, LX/FM7;->c:I

    .line 2228394
    if-lez v1, :cond_3

    move v2, v5

    :cond_3
    iput-boolean v2, v9, LX/FM7;->b:Z

    .line 2228395
    return-object v9

    :cond_4
    move v3, v1

    goto :goto_1

    .line 2228396
    :cond_5
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2228397
    const-string v0, ", "

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3, v8}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-interface {v6, v4, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method
