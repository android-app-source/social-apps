.class public LX/FCR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Tn;

.field private static volatile j:LX/FCR;


# instance fields
.field private final b:LX/0Zb;

.field private final c:LX/1MZ;

.field private final d:LX/0SG;

.field private final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lcom/facebook/common/time/RealtimeSinceBootClock;

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FO4;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/String;",
            "LX/FCQ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2210290
    sget-object v0, LX/0db;->a:LX/0Tn;

    const-string v1, "latencies_serialized"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/FCR;->a:LX/0Tn;

    return-void
.end method

.method public constructor <init>(LX/0Zb;LX/1MZ;LX/0SG;LX/0Or;LX/0Or;Lcom/facebook/common/time/RealtimeSinceBootClock;LX/0Or;)V
    .locals 2
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/analytics/perf/IsClientLatencyLoggerEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/1MZ;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0Or",
            "<",
            "LX/2Og;",
            ">;",
            "Lcom/facebook/common/time/RealtimeSinceBootClock;",
            "LX/0Or",
            "<",
            "LX/FO4;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2210291
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2210292
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, LX/FCR;->i:Ljava/util/LinkedHashMap;

    .line 2210293
    iput-object p1, p0, LX/FCR;->b:LX/0Zb;

    .line 2210294
    iput-object p2, p0, LX/FCR;->c:LX/1MZ;

    .line 2210295
    iput-object p3, p0, LX/FCR;->d:LX/0SG;

    .line 2210296
    iput-object p4, p0, LX/FCR;->e:LX/0Or;

    .line 2210297
    iput-object p5, p0, LX/FCR;->f:LX/0Or;

    .line 2210298
    iput-object p6, p0, LX/FCR;->g:Lcom/facebook/common/time/RealtimeSinceBootClock;

    .line 2210299
    iput-object p7, p0, LX/FCR;->h:LX/0Or;

    .line 2210300
    const/4 v0, 0x1

    const-string v1, "MAX_ENTRIES_TO_KEEP should always be larger than NUM_ENTRIES_TO_SEND_IN_ONE_EVENT"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2210301
    return-void
.end method

.method private static a(JJ)J
    .locals 4

    .prologue
    const-wide/16 v0, -0x1

    .line 2210273
    cmp-long v2, p0, v0

    if-eqz v2, :cond_0

    cmp-long v2, p2, v0

    if-eqz v2, :cond_0

    .line 2210274
    sub-long v0, p0, p2

    .line 2210275
    :cond_0
    return-wide v0
.end method

.method public static a(LX/0QB;)LX/FCR;
    .locals 11

    .prologue
    .line 2210311
    sget-object v0, LX/FCR;->j:LX/FCR;

    if-nez v0, :cond_1

    .line 2210312
    const-class v1, LX/FCR;

    monitor-enter v1

    .line 2210313
    :try_start_0
    sget-object v0, LX/FCR;->j:LX/FCR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2210314
    if-eqz v2, :cond_0

    .line 2210315
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2210316
    new-instance v3, LX/FCR;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    invoke-static {v0}, LX/1MZ;->a(LX/0QB;)LX/1MZ;

    move-result-object v5

    check-cast v5, LX/1MZ;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    const/16 v7, 0x14d8

    invoke-static {v0, v7}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    const/16 v8, 0xce8

    invoke-static {v0, v8}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v8

    invoke-static {v0}, LX/0yE;->a(LX/0QB;)Lcom/facebook/common/time/RealtimeSinceBootClock;

    move-result-object v9

    check-cast v9, Lcom/facebook/common/time/RealtimeSinceBootClock;

    const/16 v10, 0x29f9

    invoke-static {v0, v10}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v10

    invoke-direct/range {v3 .. v10}, LX/FCR;-><init>(LX/0Zb;LX/1MZ;LX/0SG;LX/0Or;LX/0Or;Lcom/facebook/common/time/RealtimeSinceBootClock;LX/0Or;)V

    .line 2210317
    move-object v0, v3

    .line 2210318
    sput-object v0, LX/FCR;->j:LX/FCR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2210319
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2210320
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2210321
    :cond_1
    sget-object v0, LX/FCR;->j:LX/FCR;

    return-object v0

    .line 2210322
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2210323
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a()Ljava/lang/String;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2210302
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FCR;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 2210303
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 2210304
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    const/16 v0, 0xa

    if-ge v2, v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2210305
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2210306
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FCQ;

    invoke-static {v1, v0, v4}, LX/FCR;->a(Ljava/lang/String;LX/FCQ;Ljava/lang/StringBuilder;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2210307
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 2210308
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2210309
    :cond_1
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 2210310
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(LX/FCQ;)V
    .locals 4

    .prologue
    .line 2210276
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FCR;->c:LX/1MZ;

    invoke-virtual {v0}, LX/1MZ;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2210277
    const/4 v0, 0x0

    iput-boolean v0, p1, LX/FCQ;->sameMqttConnection:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2210278
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2210279
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/FCR;->c:LX/1MZ;

    invoke-virtual {v0}, LX/1MZ;->c()J

    move-result-wide v0

    iget-wide v2, p1, LX/FCQ;->sendAttemptDeviceTimestampMs:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 2210280
    const/4 v0, 0x0

    iput-boolean v0, p1, LX/FCQ;->sameMqttConnection:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2210281
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized a(Ljava/lang/String;LX/FCQ;Ljava/lang/StringBuilder;)Z
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 2210282
    const-class v1, LX/FCR;

    monitor-enter v1

    :try_start_0
    iget-wide v2, p1, LX/FCQ;->appPubackMonotonicTimeMs:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    iget-wide v2, p1, LX/FCQ;->appReceiveDrMonotonicTimeMs:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    .line 2210283
    const/4 v0, 0x0

    .line 2210284
    :goto_0
    monitor-exit v1

    return v0

    .line 2210285
    :cond_0
    :try_start_1
    invoke-virtual {p2}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 2210286
    const/16 v0, 0x2c

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2210287
    :cond_1
    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x3d

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p1, LX/FCQ;->appPubackMonotonicTimeMs:J

    iget-wide v4, p1, LX/FCQ;->appSendMonotonicTimeMs:J

    invoke-static {v2, v3, v4, v5}, LX/FCR;->a(JJ)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x3a

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p1, LX/FCQ;->appReceiveDrMonotonicTimeMs:J

    iget-wide v4, p1, LX/FCQ;->appSendMonotonicTimeMs:J

    invoke-static {v2, v3, v4, v5}, LX/FCR;->a(JJ)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x3a

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p1, LX/FCQ;->mqttReceiveDrMonotonicTimeMs:J

    iget-wide v4, p1, LX/FCQ;->mqttSendMonotonicTimeMs:J

    invoke-static {v2, v3, v4, v5}, LX/FCR;->a(JJ)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x3a

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p1, LX/FCQ;->mqttSendMonotonicTimeMs:J

    iget-wide v4, p1, LX/FCQ;->appSendMonotonicTimeMs:J

    invoke-static {v2, v3, v4, v5}, LX/FCR;->a(JJ)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v2, 0x3a

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p1, LX/FCQ;->appReceiveDrMonotonicTimeMs:J

    iget-wide v4, p1, LX/FCQ;->mqttReceiveDrMonotonicTimeMs:J

    invoke-static {v2, v3, v4, v5}, LX/FCR;->a(JJ)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2210288
    const/4 v0, 0x1

    goto :goto_0

    .line 2210289
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private declared-synchronized b()V
    .locals 2

    .prologue
    .line 2210245
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, LX/FCR;->c()V

    .line 2210246
    iget-object v0, p0, LX/FCR;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    const/16 v1, 0xc

    if-ge v0, v1, :cond_0

    .line 2210247
    :goto_0
    monitor-exit p0

    return-void

    .line 2210248
    :cond_0
    :try_start_1
    invoke-direct {p0}, LX/FCR;->a()Ljava/lang/String;

    move-result-object v0

    .line 2210249
    invoke-static {p0, v0}, LX/FCR;->b(LX/FCR;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2210250
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static declared-synchronized b(LX/FCR;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2210224
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2210225
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "msg_latencies"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2210226
    const-string v1, "latencies_map"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2210227
    iget-object v1, p0, LX/FCR;->b:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2210228
    :cond_0
    monitor-exit p0

    return-void

    .line 2210229
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c()V
    .locals 10

    .prologue
    const-wide/32 v8, 0x2932e00

    .line 2210230
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FCR;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 2210231
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2210232
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/FCR;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 2210233
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2210234
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FCQ;

    .line 2210235
    iget-wide v4, v1, LX/FCQ;->appSendMonotonicTimeMs:J

    iget-object v3, p0, LX/FCR;->g:Lcom/facebook/common/time/RealtimeSinceBootClock;

    invoke-virtual {v3}, Lcom/facebook/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v6

    sub-long/2addr v6, v8

    cmp-long v3, v4, v6

    if-gtz v3, :cond_0

    .line 2210236
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2210237
    :goto_1
    iget-wide v4, v1, LX/FCQ;->appSendMonotonicTimeMs:J

    iget-object v6, p0, LX/FCR;->g:Lcom/facebook/common/time/RealtimeSinceBootClock;

    invoke-virtual {v6}, Lcom/facebook/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v6

    sub-long/2addr v6, v8

    cmp-long v4, v4, v6

    if-gez v4, :cond_3

    .line 2210238
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, v1, v3}, LX/FCR;->a(Ljava/lang/String;LX/FCQ;Ljava/lang/StringBuilder;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2210239
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 2210240
    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2210241
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2210242
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FCQ;

    goto :goto_1

    .line 2210243
    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, LX/FCR;->b(LX/FCR;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2210244
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2210251
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v1, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/FCR;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FCR;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p2}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 2210252
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2210253
    :cond_1
    :try_start_1
    new-instance v0, LX/FCQ;

    iget-object v1, p0, LX/FCR;->d:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    iget-object v1, p0, LX/FCR;->g:Lcom/facebook/common/time/RealtimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, LX/FCQ;-><init>(JJ)V

    .line 2210254
    iget-object v1, p0, LX/FCR;->c:LX/1MZ;

    invoke-virtual {v1}, LX/1MZ;->d()Z

    move-result v1

    iput-boolean v1, v0, LX/FCQ;->sameMqttConnection:Z

    .line 2210255
    iget-object v1, p0, LX/FCR;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, p2, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2210256
    invoke-direct {p0}, LX/FCR;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2210257
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2210258
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FCR;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 2210259
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2210260
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/FCR;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FCQ;

    .line 2210261
    if-eqz v0, :cond_0

    iget-wide v2, v0, LX/FCQ;->appPubackMonotonicTimeMs:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 2210262
    iget-object v1, p0, LX/FCR;->g:Lcom/facebook/common/time/RealtimeSinceBootClock;

    invoke-virtual {v1}, Lcom/facebook/common/time/RealtimeSinceBootClock;->now()J

    move-result-wide v2

    iput-wide v2, v0, LX/FCQ;->appPubackMonotonicTimeMs:J

    .line 2210263
    invoke-direct {p0, v0}, LX/FCR;->a(LX/FCQ;)V

    .line 2210264
    invoke-direct {p0}, LX/FCR;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2210265
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;J)V
    .locals 6

    .prologue
    .line 2210266
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FCR;->e:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 2210267
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2210268
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/FCR;->i:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FCQ;

    .line 2210269
    if-eqz v0, :cond_0

    iget-wide v2, v0, LX/FCQ;->appPubackMonotonicTimeMs:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 2210270
    iput-wide p2, v0, LX/FCQ;->mqttSendMonotonicTimeMs:J

    .line 2210271
    invoke-direct {p0}, LX/FCR;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2210272
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
