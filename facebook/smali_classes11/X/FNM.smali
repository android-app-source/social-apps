.class public final LX/FNM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/2YS;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2Us;


# direct methods
.method public constructor <init>(LX/2Us;)V
    .locals 0

    .prologue
    .line 2232490
    iput-object p1, p0, LX/FNM;->a:LX/2Us;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2232491
    const/4 v8, 0x0

    .line 2232492
    :try_start_0
    iget-object v0, p0, LX/FNM;->a:LX/2Us;

    .line 2232493
    iget-object v1, v0, LX/2Us;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FNW;

    .line 2232494
    new-instance v2, LX/FNZ;

    invoke-direct {v2}, LX/FNZ;-><init>()V

    move-object v2, v2

    .line 2232495
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    .line 2232496
    iget-object v3, v1, LX/FNW;->b:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 2232497
    new-instance v3, LX/FNT;

    invoke-direct {v3, v1}, LX/FNT;-><init>(LX/FNW;)V

    .line 2232498
    sget-object v4, LX/131;->INSTANCE:LX/131;

    move-object v4, v4

    .line 2232499
    invoke-static {v2, v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    move-object v1, v2

    .line 2232500
    const v2, 0x3a8c2374

    invoke-static {v1, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Px;

    .line 2232501
    iget-object v2, v0, LX/2Us;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FNW;

    .line 2232502
    new-instance v3, LX/FNY;

    invoke-direct {v3}, LX/FNY;-><init>()V

    move-object v3, v3

    .line 2232503
    const-string v4, "count"

    const/16 v5, 0x64

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/FNY;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->c:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    .line 2232504
    iget-object v4, v2, LX/FNW;->b:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2232505
    new-instance v4, LX/FNV;

    invoke-direct {v4, v2}, LX/FNV;-><init>(LX/FNW;)V

    .line 2232506
    sget-object v5, LX/131;->INSTANCE:LX/131;

    move-object v5, v5

    .line 2232507
    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v2, v3

    .line 2232508
    const v3, -0x142c6ca5

    invoke-static {v2, v3}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Px;

    .line 2232509
    iget-object v3, v0, LX/2Us;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6jL;

    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 2232510
    if-nez v4, :cond_3

    .line 2232511
    iget-object v5, v3, LX/6jL;->c:LX/3MK;

    invoke-virtual {v5}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    const-string v6, "server_spam_list"

    invoke-virtual {v5, v6, v7, v7}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2232512
    iput-object v7, v3, LX/6jL;->b:Ljava/util/Set;

    .line 2232513
    :cond_0
    :goto_0
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 2232514
    iget-object v3, v0, LX/2Us;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6jL;

    invoke-virtual {v3, v1}, LX/6jL;->b(Ljava/util/Collection;)V

    .line 2232515
    :cond_1
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2232516
    iget-object v1, v0, LX/2Us;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6jL;

    invoke-virtual {v1, v2}, LX/6jL;->b(Ljava/util/Collection;)V

    .line 2232517
    :cond_2
    iget-object v0, p0, LX/FNM;->a:LX/2Us;

    iget-object v0, v0, LX/2Us;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v0

    sget-object v1, LX/3Kr;->W:LX/0Tn;

    iget-object v2, p0, LX/FNM;->a:LX/2Us;

    iget-object v2, v2, LX/2Us;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    invoke-interface {v0, v1, v2, v3}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2232518
    new-instance v0, LX/2YS;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, LX/2YS;-><init>(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2232519
    :goto_1
    return-object v0

    .line 2232520
    :catch_0
    move-exception v0

    .line 2232521
    iget-object v1, p0, LX/FNM;->a:LX/2Us;

    iget-object v1, v1, LX/2Us;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v1

    sget-object v2, LX/3Kr;->W:LX/0Tn;

    iget-object v3, p0, LX/FNM;->a:LX/2Us;

    iget-object v3, v3, LX/2Us;->c:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/32 v6, 0x6ddd00

    add-long/2addr v4, v6

    invoke-interface {v1, v2, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2232522
    const-string v1, "FetchSmsSpamNumberBackgroundTask"

    const-string v2, "Failed to sync sms spam numbers from server"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2232523
    new-instance v0, LX/2YS;

    invoke-direct {v0, v8}, LX/2YS;-><init>(Z)V

    goto :goto_1

    .line 2232524
    :cond_3
    sget-object v5, LX/3MO;->a:LX/0U1;

    .line 2232525
    iget-object v6, v5, LX/0U1;->d:Ljava/lang/String;

    move-object v5, v6

    .line 2232526
    invoke-static {v5, v4}, LX/0uu;->a(Ljava/lang/String;Ljava/util/Collection;)LX/0ux;

    move-result-object v5

    .line 2232527
    iget-object v6, v3, LX/6jL;->c:LX/3MK;

    invoke-virtual {v6}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v6

    const-string v7, "server_spam_list"

    invoke-virtual {v5}, LX/0ux;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5}, LX/0ux;->b()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v7, v9, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2232528
    iget-object v5, v3, LX/6jL;->b:Ljava/util/Set;

    if-eqz v5, :cond_0

    .line 2232529
    iget-object v5, v3, LX/6jL;->b:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    goto/16 :goto_0
.end method
