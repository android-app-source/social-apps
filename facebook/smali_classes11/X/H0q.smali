.class public final LX/H0q;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/H0r;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/H0r;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/H0r;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2414292
    sget-object v0, LX/H0r;->TITLE:LX/H0r;

    sget-object v1, LX/H0r;->DESCRIPTION:LX/H0r;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/H0q;->a:LX/0Px;

    .line 2414293
    sget-object v0, LX/H0r;->FIELD_LABEL:LX/H0r;

    sget-object v1, LX/H0r;->FIELD_EDIT_TEXT:LX/H0r;

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/H0q;->b:LX/0Px;

    .line 2414294
    sget-object v0, LX/H0r;->FIELD_BUTTON:LX/H0r;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/H0q;->c:LX/0Px;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2414303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2414304
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2414305
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 2414306
    new-instance v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;

    const-string v1, ""

    sget-object v2, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;->TEXT:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    invoke-direct {v0, v1, v2}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;-><init>(Ljava/lang/String;Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;)V

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2414307
    :cond_0
    invoke-static {p1}, LX/H0q;->b(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2414308
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid FormData questions"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2414309
    :cond_1
    iput-object p1, p0, LX/H0q;->d:Ljava/util/List;

    .line 2414310
    return-void
.end method

.method private static b(Ljava/util/List;)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2414295
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x3

    if-le v0, v2, :cond_0

    move v0, v1

    .line 2414296
    :goto_0
    return v0

    .line 2414297
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;

    .line 2414298
    sget-object v3, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;->TEXT:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    .line 2414299
    iget-object p0, v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    move-object v0, p0

    .line 2414300
    invoke-virtual {v3, v0}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 2414301
    goto :goto_0

    .line 2414302
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static d(LX/H0q;)I
    .locals 2

    .prologue
    .line 2414285
    iget-object v0, p0, LX/H0q;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    sget-object v1, LX/H0q;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    mul-int/2addr v0, v1

    return v0
.end method

.method private static e(LX/H0q;I)Z
    .locals 1

    .prologue
    .line 2414286
    if-lez p1, :cond_0

    iget-object v0, p0, LX/H0q;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 4

    .prologue
    .line 2414287
    sget-object v0, LX/H0q;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-static {p0}, LX/H0q;->d(LX/H0q;)I

    move-result v1

    add-int/2addr v0, v1

    const/4 v1, 0x0

    .line 2414288
    iget-object v2, p0, LX/H0q;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x3

    if-ge v2, v3, :cond_1

    const/4 v2, 0x1

    .line 2414289
    :goto_0
    if-eqz v2, :cond_0

    sget-object v1, LX/H0q;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    :cond_0
    move v1, v1

    .line 2414290
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v2, v1

    .line 2414291
    goto :goto_0
.end method

.method public final a(I)I
    .locals 3

    .prologue
    .line 2414263
    sget-object v0, LX/H0q;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 2414264
    sget-object v0, LX/H0q;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H0r;

    invoke-virtual {v0}, LX/H0r;->toInt()I

    move-result v0

    .line 2414265
    :goto_0
    return v0

    .line 2414266
    :cond_0
    sget-object v0, LX/H0q;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-static {p0}, LX/H0q;->d(LX/H0q;)I

    move-result v1

    add-int/2addr v0, v1

    if-ge p1, v0, :cond_1

    .line 2414267
    sget-object v0, LX/H0q;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    sub-int v0, p1, v0

    .line 2414268
    sget-object v1, LX/H0q;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    rem-int/2addr v0, v1

    .line 2414269
    sget-object v1, LX/H0q;->b:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H0r;

    invoke-virtual {v0}, LX/H0r;->toInt()I

    move-result v0

    goto :goto_0

    .line 2414270
    :cond_1
    invoke-virtual {p0}, LX/H0q;->a()I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 2414271
    sget-object v0, LX/H0q;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    sub-int v0, p1, v0

    invoke-static {p0}, LX/H0q;->d(LX/H0q;)I

    move-result v1

    sub-int/2addr v0, v1

    .line 2414272
    sget-object v1, LX/H0q;->c:LX/0Px;

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H0r;

    invoke-virtual {v0}, LX/H0r;->toInt()I

    move-result v0

    goto :goto_0

    .line 2414273
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(I)Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;
    .locals 3

    .prologue
    .line 2414259
    invoke-static {p0, p1}, LX/H0q;->e(LX/H0q;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2414260
    add-int/lit8 v0, p1, -0x1

    .line 2414261
    iget-object v1, p0, LX/H0q;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;

    return-object v0

    .line 2414262
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid one base question number "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c(I)I
    .locals 3

    .prologue
    .line 2414274
    sget-object v0, LX/H0q;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    sget-object v0, LX/H0q;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-static {p0}, LX/H0q;->d(LX/H0q;)I

    move-result v1

    add-int/2addr v0, v1

    if-ge p1, v0, :cond_0

    .line 2414275
    sget-object v0, LX/H0q;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    sub-int v0, p1, v0

    sget-object v1, LX/H0q;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    div-int/2addr v0, v1

    .line 2414276
    add-int/lit8 v0, v0, 0x1

    return v0

    .line 2414277
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 2414278
    iget-object v0, p0, LX/H0q;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    .line 2414279
    iget-object v0, p0, LX/H0q;->d:Ljava/util/List;

    new-instance v1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;

    const-string v2, ""

    sget-object v3, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;->TEXT:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;

    invoke-direct {v1, v2, v3}, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question;-><init>(Ljava/lang/String;Lcom/facebook/messaging/professionalservices/getquote/model/FormData$Question$QuestionType;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    .line 2414280
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid add operation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final d(I)V
    .locals 3

    .prologue
    .line 2414281
    add-int/lit8 v0, p1, -0x1

    .line 2414282
    iget-object v1, p0, LX/H0q;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_0

    invoke-static {p0, p1}, LX/H0q;->e(LX/H0q;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2414283
    iget-object v1, p0, LX/H0q;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-void

    .line 2414284
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid one base question number "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
