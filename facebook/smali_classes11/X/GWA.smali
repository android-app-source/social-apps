.class public final LX/GWA;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

.field public final synthetic b:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)V
    .locals 0

    .prologue
    .line 2360427
    iput-object p1, p0, LX/GWA;->b:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    iput-object p2, p0, LX/GWA;->a:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 9

    .prologue
    .line 2360428
    iget-object v0, p0, LX/GWA;->b:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    iget-object v0, v0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->j:LX/8Fe;

    .line 2360429
    iget-object v1, v0, LX/8Fe;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v0, v1

    .line 2360430
    if-nez v0, :cond_0

    .line 2360431
    iget-object v0, p0, LX/GWA;->b:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    iget-object v0, v0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->k:LX/03V;

    const-class v1, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Page ViewerContext not available on Edit."

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2360432
    :goto_0
    return-void

    .line 2360433
    :cond_0
    iget-object v0, p0, LX/GWA;->b:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    iget-object v0, v0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7j6;

    iget-object v0, p0, LX/GWA;->a:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->t()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v0, p0, LX/GWA;->a:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v4

    const/4 v5, 0x1

    const/16 v6, 0x2426

    iget-object v7, p0, LX/GWA;->b:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    iget-object v0, p0, LX/GWA;->b:Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;

    iget-object v0, v0, Lcom/facebook/commerce/productdetails/fragments/ProductDetailsFragment;->j:LX/8Fe;

    .line 2360434
    iget-object v8, v0, LX/8Fe;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v8, v8

    .line 2360435
    invoke-virtual {v4}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v4}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2360436
    invoke-static {v1, v2, v3, v0}, LX/7j6;->b(LX/7j6;JLjava/lang/String;)Landroid/content/Intent;

    move-result-object p0

    .line 2360437
    if-eqz p0, :cond_1

    .line 2360438
    const-string p1, "extra_requires_initial_fetch"

    const/4 v4, 0x1

    invoke-virtual {p0, p1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2360439
    const-string p1, "extra_product_item_id_to_fetch"

    invoke-virtual {p0, p1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2360440
    :cond_1
    move-object v0, p0

    .line 2360441
    :goto_1
    if-nez v0, :cond_3

    .line 2360442
    :goto_2
    goto :goto_0

    .line 2360443
    :cond_2
    invoke-virtual {v1, v2, v3}, LX/7j6;->b(J)Landroid/content/Intent;

    move-result-object v0

    goto :goto_1

    .line 2360444
    :cond_3
    const-string p0, "extra_wait_for_mutation_finish"

    invoke-virtual {v0, p0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2360445
    const-string p0, "com.facebook.orca.auth.OVERRIDDEN_VIEWER_CONTEXT"

    invoke-virtual {v0, p0, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2360446
    if-lez v6, :cond_4

    if-eqz v7, :cond_4

    .line 2360447
    iget-object p0, v1, LX/7j6;->b:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p0, v0, v6, v7}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto :goto_2

    .line 2360448
    :cond_4
    iget-object p0, v1, LX/7j6;->b:Lcom/facebook/content/SecureContextHelper;

    iget-object p1, v1, LX/7j6;->a:Landroid/content/Context;

    invoke-interface {p0, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_2
.end method
