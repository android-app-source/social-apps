.class public final enum LX/GcT;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GcT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GcT;

.field public static final enum ADD_PIN:LX/GcT;

.field public static final enum CHANGE_PASSCODE_AFTER_INCORRECT_PASSCODE:LX/GcT;

.field public static final enum CHANGE_PASSCODE_FROM_LOGIN_FLOW:LX/GcT;

.field public static final enum CHANGE_PIN:LX/GcT;

.field public static final enum CHANGE_PIN_USING_PASSWORD:LX/GcT;

.field public static final enum REMOVE_PIN:LX/GcT;

.field public static final enum REMOVE_PIN_USING_PASSWORD:LX/GcT;

.field public static final enum SWITCH_TO_DBL:LX/GcT;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2371859
    new-instance v0, LX/GcT;

    const-string v1, "CHANGE_PIN"

    invoke-direct {v0, v1, v3}, LX/GcT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GcT;->CHANGE_PIN:LX/GcT;

    .line 2371860
    new-instance v0, LX/GcT;

    const-string v1, "ADD_PIN"

    invoke-direct {v0, v1, v4}, LX/GcT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GcT;->ADD_PIN:LX/GcT;

    .line 2371861
    new-instance v0, LX/GcT;

    const-string v1, "REMOVE_PIN"

    invoke-direct {v0, v1, v5}, LX/GcT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GcT;->REMOVE_PIN:LX/GcT;

    .line 2371862
    new-instance v0, LX/GcT;

    const-string v1, "CHANGE_PIN_USING_PASSWORD"

    invoke-direct {v0, v1, v6}, LX/GcT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GcT;->CHANGE_PIN_USING_PASSWORD:LX/GcT;

    .line 2371863
    new-instance v0, LX/GcT;

    const-string v1, "REMOVE_PIN_USING_PASSWORD"

    invoke-direct {v0, v1, v7}, LX/GcT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GcT;->REMOVE_PIN_USING_PASSWORD:LX/GcT;

    .line 2371864
    new-instance v0, LX/GcT;

    const-string v1, "CHANGE_PASSCODE_AFTER_INCORRECT_PASSCODE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/GcT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GcT;->CHANGE_PASSCODE_AFTER_INCORRECT_PASSCODE:LX/GcT;

    .line 2371865
    new-instance v0, LX/GcT;

    const-string v1, "CHANGE_PASSCODE_FROM_LOGIN_FLOW"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/GcT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GcT;->CHANGE_PASSCODE_FROM_LOGIN_FLOW:LX/GcT;

    .line 2371866
    new-instance v0, LX/GcT;

    const-string v1, "SWITCH_TO_DBL"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/GcT;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GcT;->SWITCH_TO_DBL:LX/GcT;

    .line 2371867
    const/16 v0, 0x8

    new-array v0, v0, [LX/GcT;

    sget-object v1, LX/GcT;->CHANGE_PIN:LX/GcT;

    aput-object v1, v0, v3

    sget-object v1, LX/GcT;->ADD_PIN:LX/GcT;

    aput-object v1, v0, v4

    sget-object v1, LX/GcT;->REMOVE_PIN:LX/GcT;

    aput-object v1, v0, v5

    sget-object v1, LX/GcT;->CHANGE_PIN_USING_PASSWORD:LX/GcT;

    aput-object v1, v0, v6

    sget-object v1, LX/GcT;->REMOVE_PIN_USING_PASSWORD:LX/GcT;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/GcT;->CHANGE_PASSCODE_AFTER_INCORRECT_PASSCODE:LX/GcT;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/GcT;->CHANGE_PASSCODE_FROM_LOGIN_FLOW:LX/GcT;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/GcT;->SWITCH_TO_DBL:LX/GcT;

    aput-object v2, v0, v1

    sput-object v0, LX/GcT;->$VALUES:[LX/GcT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2371868
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GcT;
    .locals 1

    .prologue
    .line 2371869
    const-class v0, LX/GcT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GcT;

    return-object v0
.end method

.method public static values()[LX/GcT;
    .locals 1

    .prologue
    .line 2371870
    sget-object v0, LX/GcT;->$VALUES:[LX/GcT;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GcT;

    return-object v0
.end method
