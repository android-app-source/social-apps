.class public LX/FKc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/service/model/FetchGroupInviteLinkParams;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2225329
    check-cast p1, Lcom/facebook/messaging/service/model/FetchGroupInviteLinkParams;

    .line 2225330
    const-string v0, "t_id.%d/invites"

    .line 2225331
    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchGroupInviteLinkParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v1, v1

    .line 2225332
    invoke-virtual {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2225333
    new-instance v1, LX/14O;

    invoke-direct {v1}, LX/14O;-><init>()V

    const-string v2, "fetchGroupInviteLink"

    .line 2225334
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2225335
    move-object v1, v1

    .line 2225336
    const-string v2, "POST"

    .line 2225337
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2225338
    move-object v1, v1

    .line 2225339
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2225340
    iput-object v2, v1, LX/14O;->g:Ljava/util/List;

    .line 2225341
    move-object v1, v1

    .line 2225342
    iput-object v0, v1, LX/14O;->d:Ljava/lang/String;

    .line 2225343
    move-object v0, v1

    .line 2225344
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2225345
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2225346
    move-object v0, v0

    .line 2225347
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2225326
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2225327
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2225328
    const-string v1, "invite_link"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    invoke-static {v0}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
