.class public final LX/GaA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/7j6;

.field private final c:LX/7iO;

.field private final d:LX/0Zb;

.field public e:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

.field public f:LX/GaH;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/7j6;LX/7iO;LX/0Zb;)V
    .locals 0

    .prologue
    .line 2368010
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2368011
    iput-object p1, p0, LX/GaA;->a:Landroid/content/Context;

    .line 2368012
    iput-object p2, p0, LX/GaA;->b:LX/7j6;

    .line 2368013
    iput-object p3, p0, LX/GaA;->c:LX/7iO;

    .line 2368014
    iput-object p4, p0, LX/GaA;->d:LX/0Zb;

    .line 2368015
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x8841b2b

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2367995
    iget-object v1, p0, LX/GaA;->f:LX/GaH;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/GaA;->f:LX/GaH;

    .line 2367996
    iget-object v3, v1, LX/GaH;->a:Ljava/lang/String;

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    :goto_0
    move v1, v3

    .line 2367997
    if-nez v1, :cond_1

    .line 2367998
    :cond_0
    const v1, -0x68b1fbc7

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2367999
    :goto_1
    return-void

    .line 2368000
    :cond_1
    iget-object v1, p0, LX/GaA;->f:LX/GaH;

    .line 2368001
    iget-object v2, v1, LX/GaH;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2368002
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iget-object v3, p0, LX/GaA;->c:LX/7iO;

    invoke-static {v1, v2, v3}, LX/7iS;->a(Ljava/lang/String;Ljava/lang/Boolean;LX/7iO;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    .line 2368003
    iget-object v2, p0, LX/GaA;->d:LX/0Zb;

    invoke-interface {v2, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2368004
    iget-object v1, p0, LX/GaA;->b:LX/7j6;

    iget-object v2, p0, LX/GaA;->f:LX/GaH;

    .line 2368005
    iget-object v3, v2, LX/GaH;->a:Ljava/lang/String;

    move-object v2, v3

    .line 2368006
    iget-object v3, p0, LX/GaA;->e:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    .line 2368007
    iget-object v4, v3, Lcom/facebook/commerce/core/intent/MerchantInfoViewData;->e:Ljava/lang/String;

    move-object v3, v4

    .line 2368008
    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, LX/7j6;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2368009
    const v1, 0x3e7f492d

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method
