.class public final LX/Gxq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field public final synthetic a:Landroid/widget/RadioButton;

.field public final synthetic b:I

.field public final synthetic c:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;Landroid/widget/RadioButton;I)V
    .locals 0

    .prologue
    .line 2408487
    iput-object p1, p0, LX/Gxq;->c:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iput-object p2, p0, LX/Gxq;->a:Landroid/widget/RadioButton;

    iput p3, p0, LX/Gxq;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreDraw()Z
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2408488
    iget-object v0, p0, LX/Gxq;->a:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 2408489
    iget-object v0, p0, LX/Gxq;->a:Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 2408490
    new-array v1, v4, [I

    aput v5, v1, v5

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    aput v0, v1, v6

    invoke-static {v1}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 2408491
    new-instance v1, LX/Gxo;

    invoke-direct {v1, p0}, LX/Gxo;-><init>(LX/Gxq;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 2408492
    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 2408493
    iget v1, p0, LX/Gxq;->b:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 2408494
    new-instance v1, LX/Gxp;

    invoke-direct {v1, p0}, LX/Gxp;-><init>(LX/Gxq;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 2408495
    iget-object v1, p0, LX/Gxq;->c:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iget v1, v1, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->u:I

    const v2, 0xffffff

    and-int/2addr v1, v2

    .line 2408496
    iget-object v2, p0, LX/Gxq;->a:Landroid/widget/RadioButton;

    const-string v3, "textColor"

    new-array v4, v4, [I

    aput v1, v4, v5

    iget-object v1, p0, LX/Gxq;->c:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iget v1, v1, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->u:I

    aput v1, v4, v6

    invoke-static {v2, v3, v4}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 2408497
    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 2408498
    iget v2, p0, LX/Gxq;->b:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 2408499
    new-instance v2, Landroid/animation/ArgbEvaluator;

    invoke-direct {v2}, Landroid/animation/ArgbEvaluator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setEvaluator(Landroid/animation/TypeEvaluator;)V

    .line 2408500
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2408501
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 2408502
    return v6
.end method
