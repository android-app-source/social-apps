.class public final enum LX/G1L;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/G1L;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/G1L;

.field public static final enum COMPLETED:LX/G1L;

.field public static final enum FAILED:LX/G1L;

.field public static final enum IDLE:LX/G1L;

.field public static final enum LOADING:LX/G1L;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2311069
    new-instance v0, LX/G1L;

    const-string v1, "IDLE"

    invoke-direct {v0, v1, v2}, LX/G1L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G1L;->IDLE:LX/G1L;

    .line 2311070
    new-instance v0, LX/G1L;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v3}, LX/G1L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G1L;->LOADING:LX/G1L;

    .line 2311071
    new-instance v0, LX/G1L;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v4}, LX/G1L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G1L;->FAILED:LX/G1L;

    .line 2311072
    new-instance v0, LX/G1L;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v5}, LX/G1L;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G1L;->COMPLETED:LX/G1L;

    .line 2311073
    const/4 v0, 0x4

    new-array v0, v0, [LX/G1L;

    sget-object v1, LX/G1L;->IDLE:LX/G1L;

    aput-object v1, v0, v2

    sget-object v1, LX/G1L;->LOADING:LX/G1L;

    aput-object v1, v0, v3

    sget-object v1, LX/G1L;->FAILED:LX/G1L;

    aput-object v1, v0, v4

    sget-object v1, LX/G1L;->COMPLETED:LX/G1L;

    aput-object v1, v0, v5

    sput-object v0, LX/G1L;->$VALUES:[LX/G1L;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2311074
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/G1L;
    .locals 1

    .prologue
    .line 2311068
    const-class v0, LX/G1L;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/G1L;

    return-object v0
.end method

.method public static values()[LX/G1L;
    .locals 1

    .prologue
    .line 2311067
    sget-object v0, LX/G1L;->$VALUES:[LX/G1L;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/G1L;

    return-object v0
.end method
