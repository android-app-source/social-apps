.class public LX/F5W;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/app/Activity;

.field public final b:Landroid/content/res/Resources;

.field public final c:Lcom/facebook/content/SecureContextHelper;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/17Y;

.field private final f:LX/DTp;

.field public final g:LX/17W;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/content/res/Resources;Lcom/facebook/content/SecureContextHelper;LX/0Or;LX/17Y;LX/DTp;LX/17W;)V
    .locals 0
    .param p1    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/base/activity/FragmentChromeActivity;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Landroid/content/res/Resources;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/0Or",
            "<",
            "Landroid/content/ComponentName;",
            ">;",
            "LX/17Y;",
            "LX/DTp;",
            "LX/17W;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2198871
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2198872
    iput-object p1, p0, LX/F5W;->a:Landroid/app/Activity;

    .line 2198873
    iput-object p2, p0, LX/F5W;->b:Landroid/content/res/Resources;

    .line 2198874
    iput-object p3, p0, LX/F5W;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2198875
    iput-object p4, p0, LX/F5W;->d:LX/0Or;

    .line 2198876
    iput-object p5, p0, LX/F5W;->e:LX/17Y;

    .line 2198877
    iput-object p6, p0, LX/F5W;->f:LX/DTp;

    .line 2198878
    iput-object p7, p0, LX/F5W;->g:LX/17W;

    .line 2198879
    return-void
.end method

.method public static a(LX/F5W;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 2198867
    iget-object v0, p0, LX/F5W;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    .line 2198868
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 2198869
    iget-object v6, p0, LX/F5W;->c:Lcom/facebook/content/SecureContextHelper;

    iget-object v0, p0, LX/F5W;->f:LX/DTp;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/DTp;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, LX/F5W;->a:Landroid/app/Activity;

    invoke-interface {v6, v0, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2198870
    return-void
.end method
