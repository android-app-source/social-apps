.class public LX/GbH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/devicebasedlogin/protocol/DBLFaceRecParams;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2369292
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2369293
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 2369294
    check-cast p1, Lcom/facebook/devicebasedlogin/protocol/DBLFaceRecParams;

    .line 2369295
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2369296
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "method"

    const-string v3, "POST"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2369297
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "uid"

    .line 2369298
    iget-object v3, p1, Lcom/facebook/devicebasedlogin/protocol/DBLFaceRecParams;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2369299
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2369300
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "dbl_face_rec"

    .line 2369301
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2369302
    move-object v1, v1

    .line 2369303
    const-string v2, "POST"

    .line 2369304
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2369305
    move-object v1, v1

    .line 2369306
    const-string v2, "auth/recognize_faces"

    .line 2369307
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2369308
    move-object v1, v1

    .line 2369309
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2369310
    move-object v0, v1

    .line 2369311
    new-instance v1, LX/4cq;

    iget-object v2, p1, Lcom/facebook/devicebasedlogin/protocol/DBLFaceRecParams;->b:[B

    const-string v3, "image/jpeg"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object p0, p1, Lcom/facebook/devicebasedlogin/protocol/DBLFaceRecParams;->a:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string p0, ".jpg"

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, LX/4cq;-><init>([BLjava/lang/String;Ljava/lang/String;)V

    .line 2369312
    new-instance v2, LX/4cQ;

    const-string v3, "file"

    invoke-direct {v2, v3, v1}, LX/4cQ;-><init>(Ljava/lang/String;LX/4cO;)V

    .line 2369313
    const/4 v1, 0x1

    new-array v1, v1, [LX/4cQ;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    invoke-static {v1}, LX/0R9;->a([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v1

    move-object v1, v1

    .line 2369314
    iput-object v1, v0, LX/14O;->l:Ljava/util/List;

    .line 2369315
    move-object v0, v0

    .line 2369316
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2369317
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2369318
    move-object v0, v0

    .line 2369319
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2369320
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2369321
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "success"

    invoke-virtual {v0, v1}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2369322
    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
