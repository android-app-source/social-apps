.class public LX/FiR;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/FiR;


# instance fields
.field private final a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2275287
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2275288
    iput-object p1, p0, LX/FiR;->a:Landroid/content/res/Resources;

    .line 2275289
    return-void
.end method

.method public static a(LX/0QB;)LX/FiR;
    .locals 4

    .prologue
    .line 2275290
    sget-object v0, LX/FiR;->b:LX/FiR;

    if-nez v0, :cond_1

    .line 2275291
    const-class v1, LX/FiR;

    monitor-enter v1

    .line 2275292
    :try_start_0
    sget-object v0, LX/FiR;->b:LX/FiR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2275293
    if-eqz v2, :cond_0

    .line 2275294
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2275295
    new-instance p0, LX/FiR;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/FiR;-><init>(Landroid/content/res/Resources;)V

    .line 2275296
    move-object v0, p0

    .line 2275297
    sput-object v0, LX/FiR;->b:LX/FiR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2275298
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2275299
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2275300
    :cond_1
    sget-object v0, LX/FiR;->b:LX/FiR;

    return-object v0

    .line 2275301
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2275302
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
