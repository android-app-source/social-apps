.class public final enum LX/GXJ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GXJ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GXJ;

.field public static final enum CONTACT_MERCHANT:LX/GXJ;

.field public static final enum OFFSITE:LX/GXJ;

.field public static final enum ONSITE:LX/GXJ;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2363938
    new-instance v0, LX/GXJ;

    const-string v1, "CONTACT_MERCHANT"

    invoke-direct {v0, v1, v2}, LX/GXJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GXJ;->CONTACT_MERCHANT:LX/GXJ;

    .line 2363939
    new-instance v0, LX/GXJ;

    const-string v1, "OFFSITE"

    invoke-direct {v0, v1, v3}, LX/GXJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GXJ;->OFFSITE:LX/GXJ;

    .line 2363940
    new-instance v0, LX/GXJ;

    const-string v1, "ONSITE"

    invoke-direct {v0, v1, v4}, LX/GXJ;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GXJ;->ONSITE:LX/GXJ;

    .line 2363941
    const/4 v0, 0x3

    new-array v0, v0, [LX/GXJ;

    sget-object v1, LX/GXJ;->CONTACT_MERCHANT:LX/GXJ;

    aput-object v1, v0, v2

    sget-object v1, LX/GXJ;->OFFSITE:LX/GXJ;

    aput-object v1, v0, v3

    sget-object v1, LX/GXJ;->ONSITE:LX/GXJ;

    aput-object v1, v0, v4

    sput-object v0, LX/GXJ;->$VALUES:[LX/GXJ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2363942
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GXJ;
    .locals 1

    .prologue
    .line 2363943
    const-class v0, LX/GXJ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GXJ;

    return-object v0
.end method

.method public static values()[LX/GXJ;
    .locals 1

    .prologue
    .line 2363944
    sget-object v0, LX/GXJ;->$VALUES:[LX/GXJ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GXJ;

    return-object v0
.end method
