.class public LX/Gdn;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Class;",
            "LX/Gdm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2374519
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2374520
    invoke-static {}, LX/0PM;->c()Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/Gdn;->a:Ljava/util/Map;

    .line 2374521
    invoke-virtual {p1}, LX/Gdm;->c()Ljava/lang/Class;

    move-result-object v0

    .line 2374522
    iget-object v1, p0, LX/Gdn;->a:Ljava/util/Map;

    invoke-interface {v1, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2374523
    return-void
.end method

.method public static a(LX/0QB;)LX/Gdn;
    .locals 4

    .prologue
    .line 2374524
    const-class v1, LX/Gdn;

    monitor-enter v1

    .line 2374525
    :try_start_0
    sget-object v0, LX/Gdn;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2374526
    sput-object v2, LX/Gdn;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2374527
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2374528
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2374529
    new-instance p0, LX/Gdn;

    invoke-static {v0}, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->a(LX/0QB;)Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;

    invoke-direct {p0, v3}, LX/Gdn;-><init>(Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;)V

    .line 2374530
    move-object v0, p0

    .line 2374531
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2374532
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gdn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2374533
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2374534
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
