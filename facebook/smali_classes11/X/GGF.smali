.class public final enum LX/GGF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GGF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GGF;

.field public static final enum HOME:LX/GGF;

.field public static final enum RECENT:LX/GGF;

.field public static final enum TRAVEL_IN:LX/GGF;


# instance fields
.field public final key:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2333420
    new-instance v0, LX/GGF;

    const-string v1, "HOME"

    const-string v2, "home"

    invoke-direct {v0, v1, v3, v2}, LX/GGF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GGF;->HOME:LX/GGF;

    .line 2333421
    new-instance v0, LX/GGF;

    const-string v1, "RECENT"

    const-string v2, "recent"

    invoke-direct {v0, v1, v4, v2}, LX/GGF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GGF;->RECENT:LX/GGF;

    .line 2333422
    new-instance v0, LX/GGF;

    const-string v1, "TRAVEL_IN"

    const-string v2, "travel_in"

    invoke-direct {v0, v1, v5, v2}, LX/GGF;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/GGF;->TRAVEL_IN:LX/GGF;

    .line 2333423
    const/4 v0, 0x3

    new-array v0, v0, [LX/GGF;

    sget-object v1, LX/GGF;->HOME:LX/GGF;

    aput-object v1, v0, v3

    sget-object v1, LX/GGF;->RECENT:LX/GGF;

    aput-object v1, v0, v4

    sget-object v1, LX/GGF;->TRAVEL_IN:LX/GGF;

    aput-object v1, v0, v5

    sput-object v0, LX/GGF;->$VALUES:[LX/GGF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2333424
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2333425
    iput-object p3, p0, LX/GGF;->key:Ljava/lang/String;

    .line 2333426
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GGF;
    .locals 1

    .prologue
    .line 2333427
    const-class v0, LX/GGF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GGF;

    return-object v0
.end method

.method public static values()[LX/GGF;
    .locals 1

    .prologue
    .line 2333428
    sget-object v0, LX/GGF;->$VALUES:[LX/GGF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GGF;

    return-object v0
.end method
