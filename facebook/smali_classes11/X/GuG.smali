.class public final LX/GuG;
.super LX/BWM;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2403192
    iput-object p1, p0, LX/GuG;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403193
    invoke-direct {p0, p2}, LX/BWM;-><init>(Landroid/os/Handler;)V

    .line 2403194
    iput-object p4, p0, LX/GuG;->c:Ljava/lang/String;

    .line 2403195
    iput-object p3, p0, LX/GuG;->d:Ljava/lang/String;

    .line 2403196
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/webview/FacebookWebView;LX/BWN;)V
    .locals 3

    .prologue
    .line 2403197
    iget-object v0, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2403198
    const-string v1, "target"

    invoke-interface {p2, v0, v1}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2403199
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 2403200
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/GuG;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2403201
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, LX/GuG;->d:Ljava/lang/String;

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2403202
    iget-object v0, p0, LX/GuG;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V

    .line 2403203
    :goto_0
    return-void

    .line 2403204
    :cond_0
    iget-object v0, p0, LX/GuG;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v0}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Empty phone number; text/call skipped."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
