.class public final enum LX/FA5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FA5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FA5;

.field public static final enum ONION:LX/FA5;

.field public static final enum PUBLIC:LX/FA5;

.field public static final enum TOR:LX/FA5;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2206680
    new-instance v0, LX/FA5;

    const-string v1, "PUBLIC"

    invoke-direct {v0, v1, v2}, LX/FA5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FA5;->PUBLIC:LX/FA5;

    .line 2206681
    new-instance v0, LX/FA5;

    const-string v1, "TOR"

    invoke-direct {v0, v1, v3}, LX/FA5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FA5;->TOR:LX/FA5;

    .line 2206682
    new-instance v0, LX/FA5;

    const-string v1, "ONION"

    invoke-direct {v0, v1, v4}, LX/FA5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FA5;->ONION:LX/FA5;

    .line 2206683
    const/4 v0, 0x3

    new-array v0, v0, [LX/FA5;

    sget-object v1, LX/FA5;->PUBLIC:LX/FA5;

    aput-object v1, v0, v2

    sget-object v1, LX/FA5;->TOR:LX/FA5;

    aput-object v1, v0, v3

    sget-object v1, LX/FA5;->ONION:LX/FA5;

    aput-object v1, v0, v4

    sput-object v0, LX/FA5;->$VALUES:[LX/FA5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2206684
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FA5;
    .locals 1

    .prologue
    .line 2206685
    const-class v0, LX/FA5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FA5;

    return-object v0
.end method

.method public static values()[LX/FA5;
    .locals 1

    .prologue
    .line 2206686
    sget-object v0, LX/FA5;->$VALUES:[LX/FA5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FA5;

    return-object v0
.end method
