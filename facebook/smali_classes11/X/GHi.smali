.class public final LX/GHi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GFR;


# instance fields
.field public final synthetic a:LX/GHx;


# direct methods
.method public constructor <init>(LX/GHx;)V
    .locals 0

    .prologue
    .line 2335183
    iput-object p1, p0, LX/GHi;->a:LX/GHx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/content/Intent;)V
    .locals 12

    .prologue
    .line 2335184
    const/4 v0, -0x1

    if-eq p1, v0, :cond_1

    .line 2335185
    if-nez p1, :cond_0

    .line 2335186
    iget-object v0, p0, LX/GHi;->a:LX/GHx;

    .line 2335187
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2335188
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2335189
    iget-object v1, p0, LX/GHi;->a:LX/GHx;

    iget-object v1, v1, LX/GHx;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    const/4 v7, 0x0

    .line 2335190
    const-string v5, "cancel_flow"

    const-string v6, "image_picker"

    const-string v9, "creative_edit"

    const/4 v11, 0x1

    move-object v3, v0

    move-object v4, v1

    move-object v8, v7

    move-object v10, v7

    invoke-static/range {v3 .. v11}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2335191
    invoke-virtual {v0, v1}, LX/GG3;->t(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2335192
    :cond_0
    :goto_0
    return-void

    .line 2335193
    :cond_1
    const-string v0, "photo_category"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2335194
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2335195
    const/4 v0, 0x0

    .line 2335196
    :cond_2
    :goto_1
    move-object v0, v0

    .line 2335197
    iget-object v1, p0, LX/GHi;->a:LX/GHx;

    .line 2335198
    iget-object v2, v1, LX/GHg;->b:LX/GCE;

    move-object v1, v2

    .line 2335199
    iget-object v2, v1, LX/GCE;->f:LX/GG3;

    move-object v1, v2

    .line 2335200
    iget-object v2, p0, LX/GHi;->a:LX/GHx;

    iget-object v2, v2, LX/GHx;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1, v2}, LX/GG3;->t(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2335201
    iget-object v1, p0, LX/GHi;->a:LX/GHx;

    .line 2335202
    iget-object v2, v1, LX/GHg;->b:LX/GCE;

    move-object v1, v2

    .line 2335203
    iget-object v2, v1, LX/GCE;->f:LX/GG3;

    move-object v1, v2

    .line 2335204
    iget-object v2, p0, LX/GHi;->a:LX/GHx;

    iget-object v2, v2, LX/GHx;->n:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    const/4 v8, 0x0

    .line 2335205
    const-string v5, "change_flow_option"

    invoke-static {v2}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v6, "edit"

    :goto_2
    const-string v7, "image_picker"

    const/4 v11, 0x1

    move-object v3, v1

    move-object v4, v2

    move-object v9, v8

    move-object v10, v0

    invoke-static/range {v3 .. v11}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2335206
    iget-object v0, p0, LX/GHi;->a:LX/GHx;

    iget-object v0, v0, LX/GHx;->k:LX/GKy;

    invoke-virtual {v0}, LX/GKy;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2335207
    iget-object v0, p0, LX/GHi;->a:LX/GHx;

    const-string v1, "file_path"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GHx;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2335208
    :cond_3
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 2335209
    iget-object v1, p0, LX/GHi;->a:LX/GHx;

    .line 2335210
    iget-object v2, v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->b:Landroid/net/Uri;

    move-object v0, v2

    .line 2335211
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/GHx;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2335212
    :cond_4
    const-string v1, "AD_IMAGES"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2335213
    const-string v0, "IMAGE_LIBRARY"

    goto :goto_1

    .line 2335214
    :cond_5
    const-string v6, "create"

    goto :goto_2
.end method
