.class public final LX/HA2;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:LX/HA4;


# direct methods
.method public constructor <init>(LX/HA4;)V
    .locals 0

    .prologue
    .line 2434828
    iput-object p1, p0, LX/HA2;->a:LX/HA4;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 4

    .prologue
    .line 2434829
    iget-object v0, p0, LX/HA2;->a:LX/HA4;

    .line 2434830
    new-instance v3, LX/34b;

    iget-object v1, v0, LX/HA4;->e:Landroid/content/Context;

    invoke-direct {v3, v1}, LX/34b;-><init>(Landroid/content/Context;)V

    .line 2434831
    iget-object v1, v0, LX/HA4;->f:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 2434832
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/H8Z;

    .line 2434833
    invoke-interface {v2}, LX/H8Z;->a()LX/HA7;

    move-result-object v2

    .line 2434834
    iget-boolean p1, v2, LX/HA7;->f:Z

    move p1, p1

    .line 2434835
    if-eqz p1, :cond_0

    .line 2434836
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 p1, 0x0

    .line 2434837
    iget p2, v2, LX/HA7;->b:I

    move p2, p2

    .line 2434838
    invoke-virtual {v3, v1, p1, p2}, LX/34c;->a(III)LX/3Ai;

    move-result-object v1

    .line 2434839
    iget-boolean p1, v2, LX/HA7;->f:Z

    move p1, p1

    .line 2434840
    invoke-virtual {v1, p1}, LX/3Ai;->setEnabled(Z)Landroid/view/MenuItem;

    move-result-object p1

    .line 2434841
    iget-boolean p2, v2, LX/HA7;->g:Z

    move p2, p2

    .line 2434842
    invoke-interface {p1, p2}, Landroid/view/MenuItem;->setCheckable(Z)Landroid/view/MenuItem;

    move-result-object p1

    .line 2434843
    iget-boolean p2, v2, LX/HA7;->h:Z

    move p2, p2

    .line 2434844
    invoke-interface {p1, p2}, Landroid/view/MenuItem;->setChecked(Z)Landroid/view/MenuItem;

    move-result-object p1

    invoke-virtual {v2}, LX/HA7;->j()I

    move-result v2

    invoke-interface {p1, v2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2434845
    new-instance v2, LX/HA3;

    invoke-direct {v2, v0}, LX/HA3;-><init>(LX/HA4;)V

    invoke-virtual {v1, v2}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_0

    .line 2434846
    :cond_1
    new-instance v1, LX/3Af;

    iget-object v2, v0, LX/HA4;->e:Landroid/content/Context;

    invoke-direct {v1, v2}, LX/3Af;-><init>(Landroid/content/Context;)V

    .line 2434847
    invoke-virtual {v1, v3}, LX/3Af;->a(LX/1OM;)V

    .line 2434848
    invoke-virtual {v1}, LX/3Af;->show()V

    .line 2434849
    return-void
.end method
