.class public LX/GAV;
.super Landroid/os/AsyncTask;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<",
        "LX/GAY;",
        ">;>;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/net/HttpURLConnection;

.field private final c:LX/GAX;

.field private d:Ljava/lang/Exception;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2325662
    const-class v0, LX/GAV;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/GAV;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/GAX;)V
    .locals 1

    .prologue
    .line 2325660
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, LX/GAV;-><init>(Ljava/net/HttpURLConnection;LX/GAX;)V

    .line 2325661
    return-void
.end method

.method private constructor <init>(Ljava/net/HttpURLConnection;LX/GAX;)V
    .locals 0

    .prologue
    .line 2325629
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 2325630
    iput-object p2, p0, LX/GAV;->c:LX/GAX;

    .line 2325631
    iput-object p1, p0, LX/GAV;->b:Ljava/net/HttpURLConnection;

    .line 2325632
    return-void
.end method


# virtual methods
.method public final doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2325652
    :try_start_0
    iget-object v0, p0, LX/GAV;->b:Ljava/net/HttpURLConnection;

    if-nez v0, :cond_0

    .line 2325653
    iget-object v0, p0, LX/GAV;->c:LX/GAX;

    .line 2325654
    invoke-static {v0}, LX/GAU;->a(LX/GAX;)Ljava/util/List;

    move-result-object v1

    move-object v0, v1

    .line 2325655
    :goto_0
    return-object v0

    .line 2325656
    :cond_0
    iget-object v0, p0, LX/GAV;->b:Ljava/net/HttpURLConnection;

    iget-object v1, p0, LX/GAV;->c:LX/GAX;

    invoke-static {v0, v1}, LX/GAU;->a(Ljava/net/HttpURLConnection;LX/GAX;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 2325657
    :catch_0
    move-exception v0

    .line 2325658
    iput-object v0, p0, LX/GAV;->d:Ljava/lang/Exception;

    .line 2325659
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2325647
    check-cast p1, Ljava/util/List;

    .line 2325648
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 2325649
    iget-object v0, p0, LX/GAV;->d:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    .line 2325650
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/GAV;->d:Ljava/lang/Exception;

    invoke-virtual {v2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 2325651
    :cond_0
    return-void
.end method

.method public final onPreExecute()V
    .locals 2

    .prologue
    .line 2325634
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 2325635
    sget-boolean v0, LX/GAK;->j:Z

    move v0, v0

    .line 2325636
    if-eqz v0, :cond_0

    .line 2325637
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 2325638
    :cond_0
    iget-object v0, p0, LX/GAV;->c:LX/GAX;

    .line 2325639
    iget-object v1, v0, LX/GAX;->b:Landroid/os/Handler;

    move-object v0, v1

    .line 2325640
    if-nez v0, :cond_1

    .line 2325641
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    instance-of v0, v0, Landroid/os/HandlerThread;

    if-eqz v0, :cond_2

    .line 2325642
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 2325643
    :goto_0
    iget-object v1, p0, LX/GAV;->c:LX/GAX;

    .line 2325644
    iput-object v0, v1, LX/GAX;->b:Landroid/os/Handler;

    .line 2325645
    :cond_1
    return-void

    .line 2325646
    :cond_2
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2325633
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{RequestAsyncTask: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " connection: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/GAV;->b:Ljava/net/HttpURLConnection;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", requests: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/GAV;->c:LX/GAX;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
