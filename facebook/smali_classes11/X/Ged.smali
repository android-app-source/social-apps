.class public LX/Ged;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Geb;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Gee;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2375860
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Ged;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Gee;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2375898
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2375899
    iput-object p1, p0, LX/Ged;->b:LX/0Ot;

    .line 2375900
    return-void
.end method

.method public static a(LX/0QB;)LX/Ged;
    .locals 4

    .prologue
    .line 2375887
    const-class v1, LX/Ged;

    monitor-enter v1

    .line 2375888
    :try_start_0
    sget-object v0, LX/Ged;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2375889
    sput-object v2, LX/Ged;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2375890
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2375891
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2375892
    new-instance v3, LX/Ged;

    const/16 p0, 0x2102

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Ged;-><init>(LX/0Ot;)V

    .line 2375893
    move-object v0, v3

    .line 2375894
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2375895
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ged;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2375896
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2375897
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2375901
    const v0, 0x6cfb801

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 3

    .prologue
    .line 2375877
    check-cast p2, LX/Gec;

    .line 2375878
    iget-object v0, p0, LX/Ged;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gee;

    iget-object v1, p2, LX/Gec;->a:Lcom/facebook/java2js/JSValue;

    .line 2375879
    const-string v2, "page"

    invoke-virtual {v1, v2}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    .line 2375880
    const-string p0, "__native_object__"

    invoke-virtual {v2, p0}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v2

    const-class p0, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v2, p0}, Lcom/facebook/java2js/JSValue;->to(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPage;

    .line 2375881
    iget-object p0, v0, LX/Gee;->a:LX/1vg;

    invoke-virtual {p0, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object p0

    const p2, 0x7f020abc

    invoke-virtual {p0, p2}, LX/2xv;->h(I)LX/2xv;

    move-result-object p0

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v2

    if-eqz v2, :cond_0

    const v2, 0x7f0a00d2

    :goto_0
    invoke-virtual {p0, v2}, LX/2xv;->j(I)LX/2xv;

    move-result-object v2

    invoke-virtual {v2}, LX/1n6;->b()LX/1dc;

    move-result-object v2

    .line 2375882
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    const/16 p0, 0x8

    const p2, 0x7f0b107e

    invoke-interface {v2, p0, p2}, LX/1Di;->g(II)LX/1Di;

    move-result-object v2

    const/4 p0, 0x2

    invoke-interface {v2, p0}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    .line 2375883
    const p0, 0x6cfb801

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2375884
    invoke-interface {v2, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2375885
    return-object v0

    .line 2375886
    :cond_0
    const v2, 0x7f0a00e7

    goto :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2375861
    invoke-static {}, LX/1dS;->b()V

    .line 2375862
    iget v0, p1, LX/1dQ;->b:I

    .line 2375863
    packed-switch v0, :pswitch_data_0

    .line 2375864
    :goto_0
    return-object v1

    .line 2375865
    :pswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 2375866
    check-cast v0, LX/Gec;

    .line 2375867
    iget-object v2, p0, LX/Ged;->b:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Gee;

    iget-object v3, v0, LX/Gec;->a:Lcom/facebook/java2js/JSValue;

    .line 2375868
    const-string v4, "page"

    invoke-virtual {v3, v4}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v4

    .line 2375869
    const-string v5, "__native_object__"

    invoke-virtual {v4, v5}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v4

    const-class v5, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v4, v5}, Lcom/facebook/java2js/JSValue;->to(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPage;

    .line 2375870
    const-string v5, "onAction"

    invoke-virtual {v3, v5}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object p1

    .line 2375871
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    .line 2375872
    iget-object v5, v2, LX/Gee;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/GeG;

    const-string v6, "trackingCodes"

    invoke-virtual {v3, v6}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v6

    const-class p0, LX/162;

    invoke-virtual {v6, p0}, Lcom/facebook/java2js/JSValue;->to(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/162;

    const-string p0, "isSponsored"

    invoke-virtual {v3, p0}, Lcom/facebook/java2js/JSValue;->getBooleanProperty(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object p0

    const-string v0, "feed_pyml"

    invoke-virtual {v5, v4, v6, p0, v0}, LX/GeG;->a(Lcom/facebook/graphql/model/GraphQLPage;LX/162;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 2375873
    invoke-virtual {p1}, Lcom/facebook/java2js/JSValue;->isFunction()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2375874
    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "unlike"

    :goto_1
    aput-object v4, v5, v6

    invoke-virtual {p1, v5}, Lcom/facebook/java2js/JSValue;->callAsFunction([Ljava/lang/Object;)Lcom/facebook/java2js/JSValue;

    .line 2375875
    :cond_0
    goto :goto_0

    .line 2375876
    :cond_1
    const-string v4, "like"

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x6cfb801
        :pswitch_0
    .end packed-switch
.end method
