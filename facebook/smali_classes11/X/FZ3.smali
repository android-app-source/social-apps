.class public LX/FZ3;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BUA;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/5up;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Sc;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0hy;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKeyFactory;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3GK;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/0Uh;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2256822
    const-class v0, LX/FZ3;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/FZ3;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Uh;)V
    .locals 0
    .param p4    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/BUA;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/5up;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Sc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Kf;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0hy;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/model/threadkey/ThreadKeyFactory;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3GK;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2256870
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2256871
    iput-object p1, p0, LX/FZ3;->b:LX/0Ot;

    .line 2256872
    iput-object p2, p0, LX/FZ3;->c:LX/0Ot;

    .line 2256873
    iput-object p3, p0, LX/FZ3;->d:LX/0Ot;

    .line 2256874
    iput-object p4, p0, LX/FZ3;->e:LX/0Ot;

    .line 2256875
    iput-object p5, p0, LX/FZ3;->f:LX/0Ot;

    .line 2256876
    iput-object p6, p0, LX/FZ3;->g:LX/0Ot;

    .line 2256877
    iput-object p7, p0, LX/FZ3;->h:LX/0Ot;

    .line 2256878
    iput-object p8, p0, LX/FZ3;->i:LX/0Ot;

    .line 2256879
    iput-object p9, p0, LX/FZ3;->j:LX/0Ot;

    .line 2256880
    iput-object p10, p0, LX/FZ3;->k:LX/0Ot;

    .line 2256881
    iput-object p11, p0, LX/FZ3;->l:LX/0Uh;

    .line 2256882
    return-void
.end method

.method public static a(LX/BO1;)Lcom/facebook/graphql/model/GraphQLEntity;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const v2, 0x4c808d5

    .line 2256854
    invoke-interface {p0}, LX/BO1;->e()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2256855
    :cond_0
    :goto_0
    return-object v0

    .line 2256856
    :cond_1
    invoke-interface {p0}, LX/BO1;->e()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {p0}, LX/BO1;->J()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {p0}, LX/BO1;->K()I

    move-result v1

    const v3, 0x4ed245b

    if-eq v1, v3, :cond_2

    invoke-interface {p0}, LX/BO1;->K()I

    move-result v1

    const v3, 0x25d6af

    if-ne v1, v3, :cond_7

    :cond_2
    const/4 v1, 0x1

    :goto_1
    move v1, v1

    .line 2256857
    if-nez v1, :cond_3

    .line 2256858
    invoke-interface {p0}, LX/BO1;->e()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {p0}, LX/BO1;->J()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {p0}, LX/BO1;->K()I

    move-result v1

    const v3, 0x403827a

    if-ne v1, v3, :cond_8

    invoke-interface {p0}, LX/BO1;->R()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {p0}, LX/BO1;->S()Z

    move-result v1

    if-eqz v1, :cond_8

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 2256859
    if-eqz v1, :cond_4

    .line 2256860
    :cond_3
    invoke-interface {p0}, LX/BO1;->f()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, LX/BO1;->K()I

    move-result v1

    invoke-static {v0, v1}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    goto :goto_0

    .line 2256861
    :cond_4
    invoke-interface {p0}, LX/BO1;->L()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {p0}, LX/BO1;->N()I

    move-result v1

    const v3, 0x1eaef984

    if-ne v1, v3, :cond_9

    const/4 v1, 0x1

    :goto_3
    move v1, v1

    .line 2256862
    if-eqz v1, :cond_5

    .line 2256863
    invoke-interface {p0}, LX/BO1;->M()Ljava/lang/String;

    move-result-object v0

    const v1, 0x1eaef984

    invoke-static {v0, v1}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    goto :goto_0

    .line 2256864
    :cond_5
    invoke-interface {p0}, LX/BO1;->J()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {p0}, LX/BO1;->K()I

    move-result v1

    const v3, 0xa7c5482

    if-ne v1, v3, :cond_a

    invoke-interface {p0}, LX/BO1;->T()Z

    move-result v1

    if-eqz v1, :cond_a

    const/4 v1, 0x1

    :goto_4
    move v1, v1

    .line 2256865
    if-eqz v1, :cond_6

    invoke-interface {p0}, LX/BO1;->v()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2256866
    invoke-interface {p0}, LX/BO1;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    goto/16 :goto_0

    .line 2256867
    :cond_6
    invoke-interface {p0}, LX/BO1;->J()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {p0}, LX/BO1;->K()I

    move-result v1

    const v3, 0x4d5123a8    # 2.19298432E8f

    if-ne v1, v3, :cond_b

    const/4 v1, 0x1

    :goto_5
    move v1, v1

    .line 2256868
    if-eqz v1, :cond_0

    invoke-interface {p0}, LX/BO1;->v()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2256869
    invoke-interface {p0}, LX/BO1;->w()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_8
    const/4 v1, 0x0

    goto :goto_2

    :cond_9
    const/4 v1, 0x0

    goto :goto_3

    :cond_a
    const/4 v1, 0x0

    goto :goto_4

    :cond_b
    const/4 v1, 0x0

    goto :goto_5
.end method

.method public static a$redex0(LX/FZ3;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 4

    .prologue
    .line 2256847
    new-instance v1, LX/FZ0;

    invoke-direct {v1, p0, p1}, LX/FZ0;-><init>(LX/FZ3;Ljava/lang/String;)V

    .line 2256848
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLSavedState;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2256849
    iget-object v0, p0, LX/FZ3;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5up;

    const-string v2, "native_saved_dashboard"

    const-string v3, "delete_button"

    invoke-virtual {v0, p1, v2, v3, v1}, LX/5up;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    .line 2256850
    :goto_0
    return-void

    .line 2256851
    :cond_0
    if-eqz p2, :cond_1

    .line 2256852
    iget-object v0, p0, LX/FZ3;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BUA;

    sget-object v2, LX/7Jb;->USER_INITIATED:LX/7Jb;

    invoke-virtual {v0, p1, v2}, LX/BUA;->a(Ljava/lang/String;LX/7Jb;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2256853
    :cond_1
    iget-object v0, p0, LX/FZ3;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5up;

    const-string v2, "native_saved_dashboard"

    const-string v3, "delete_button"

    invoke-virtual {v0, p1, v2, v3, v1}, LX/5up;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/FZ3;
    .locals 12

    .prologue
    .line 2256845
    new-instance v0, LX/FZ3;

    const/16 v1, 0x37e9

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0x329d

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x12c4

    invoke-static {p0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x140f

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x1132

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x3be

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0xbe1

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x455

    invoke-static {p0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0xd67

    invoke-static {p0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xc1e

    invoke-static {p0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    invoke-static {p0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v11

    check-cast v11, LX/0Uh;

    invoke-direct/range {v0 .. v11}, LX/FZ3;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Uh;)V

    .line 2256846
    return-object v0
.end method

.method public static b(LX/BO1;)Z
    .locals 1

    .prologue
    .line 2256844
    invoke-interface {p0}, LX/BO1;->v()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, LX/BO1;->x()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p0}, LX/BO1;->z()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(JLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 9
    .param p6    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param

    .prologue
    .line 2256842
    iget-object v0, p0, LX/FZ3;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/facebook/saved2/ui/listener/Saved2ItemActionHelper$1;

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p6

    move-object v6, p4

    move v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/facebook/saved2/ui/listener/Saved2ItemActionHelper$1;-><init>(LX/FZ3;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    const v1, 0x769af2cc

    invoke-static {v8, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2256843
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2256827
    if-eqz p2, :cond_2

    .line 2256828
    new-instance v0, LX/89k;

    invoke-direct {v0}, LX/89k;-><init>()V

    .line 2256829
    iput-object p2, v0, LX/89k;->b:Ljava/lang/String;

    .line 2256830
    move-object v0, v0

    .line 2256831
    invoke-virtual {v0}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v1

    .line 2256832
    iget-object v0, p0, LX/FZ3;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0hy;

    invoke-interface {v0, v1}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    .line 2256833
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 2256834
    iget-object v0, p0, LX/FZ3;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, v1, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2256835
    :cond_1
    return-void

    .line 2256836
    :cond_2
    if-eqz p3, :cond_3

    .line 2256837
    iget-object v0, p0, LX/FZ3;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Ec;

    invoke-static {p3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/3Ec;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    move-object v2, v0

    .line 2256838
    :goto_1
    if-eqz v2, :cond_0

    .line 2256839
    iget-object v0, p0, LX/FZ3;->k:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3GK;

    invoke-interface {v0, v2}, LX/3GK;->b(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Landroid/content/Intent;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 2256840
    :cond_3
    if-eqz p4, :cond_4

    .line 2256841
    invoke-static {p4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    move-object v2, v0

    goto :goto_1

    :cond_4
    move-object v2, v1

    goto :goto_1
.end method

.method public final a(Landroid/support/v4/app/FragmentActivity;Lcom/facebook/graphql/model/GraphQLEntity;)V
    .locals 5

    .prologue
    .line 2256825
    iget-object v0, p0, LX/FZ3;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    const/4 v1, 0x0

    sget-object v2, LX/21D;->SAVED_STORIES_DASHBOARD:LX/21D;

    const-string v3, "shareFromSavedDashboard2"

    invoke-static {p2}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v4

    invoke-virtual {v4}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v4

    invoke-static {v2, v3, v4}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3, p1}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2256826
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 2256823
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/facebook/saved2/ui/listener/Saved2ItemActionHelper$3;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/facebook/saved2/ui/listener/Saved2ItemActionHelper$3;-><init>(LX/FZ3;Ljava/lang/String;ZLjava/lang/String;)V

    const v2, 0x3c6c9946

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2256824
    return-void
.end method
