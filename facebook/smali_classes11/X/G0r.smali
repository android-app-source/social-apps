.class public LX/G0r;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/G0r;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public volatile b:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Landroid/content/res/Resources;",
            ">;"
        }
    .end annotation
.end field

.field public volatile c:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0hB;",
            ">;"
        }
    .end annotation
.end field

.field public volatile d:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1Ad;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2310475
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2310476
    return-void
.end method

.method public static a(LX/1U8;III)LX/1Fb;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2310467
    if-ne p1, p2, :cond_0

    invoke-interface {p0}, LX/1U8;->aj_()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2310468
    invoke-interface {p0}, LX/1U8;->aj_()LX/1Fb;

    move-result-object v0

    .line 2310469
    :goto_0
    return-object v0

    .line 2310470
    :cond_0
    int-to-float v0, p1

    int-to-float v1, p2

    div-float/2addr v0, v1

    .line 2310471
    int-to-float v1, p3

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    move v0, v0

    .line 2310472
    invoke-interface {p0}, LX/1U8;->ai_()LX/1Fb;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, LX/1U8;->ai_()LX/1Fb;

    move-result-object v1

    invoke-interface {v1}, LX/1Fb;->c()I

    move-result v1

    if-lt v1, v0, :cond_1

    .line 2310473
    invoke-interface {p0}, LX/1U8;->ai_()LX/1Fb;

    move-result-object v0

    goto :goto_0

    .line 2310474
    :cond_1
    invoke-interface {p0}, LX/1U8;->j()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p0}, LX/1U8;->j()LX/1Fb;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-interface {p0}, LX/1U8;->aj_()LX/1Fb;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/G0r;
    .locals 7

    .prologue
    .line 2310452
    sget-object v0, LX/G0r;->e:LX/G0r;

    if-nez v0, :cond_1

    .line 2310453
    const-class v1, LX/G0r;

    monitor-enter v1

    .line 2310454
    :try_start_0
    sget-object v0, LX/G0r;->e:LX/G0r;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2310455
    if-eqz v2, :cond_0

    .line 2310456
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2310457
    new-instance v3, LX/G0r;

    invoke-direct {v3}, LX/G0r;-><init>()V

    .line 2310458
    const/16 v4, 0x259

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0x1b

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x4be

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 p0, 0x509

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    .line 2310459
    iput-object v4, v3, LX/G0r;->a:LX/0Or;

    iput-object v5, v3, LX/G0r;->b:LX/0Or;

    iput-object v6, v3, LX/G0r;->c:LX/0Or;

    iput-object p0, v3, LX/G0r;->d:LX/0Or;

    .line 2310460
    move-object v0, v3

    .line 2310461
    sput-object v0, LX/G0r;->e:LX/G0r;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2310462
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2310463
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2310464
    :cond_1
    sget-object v0, LX/G0r;->e:LX/G0r;

    return-object v0

    .line 2310465
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2310466
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static b()LX/5vn;
    .locals 7

    .prologue
    const-wide/high16 v4, 0x4018000000000000L    # 6.0

    const-wide/16 v2, 0x0

    .line 2310442
    new-instance v0, LX/5vq;

    invoke-direct {v0}, LX/5vq;-><init>()V

    .line 2310443
    iput-wide v2, v0, LX/5vq;->c:D

    .line 2310444
    move-object v0, v0

    .line 2310445
    iput-wide v2, v0, LX/5vq;->d:D

    .line 2310446
    move-object v0, v0

    .line 2310447
    iput-wide v4, v0, LX/5vq;->b:D

    .line 2310448
    move-object v0, v0

    .line 2310449
    iput-wide v4, v0, LX/5vq;->a:D

    .line 2310450
    move-object v0, v0

    .line 2310451
    invoke-virtual {v0}, LX/5vq;->a()Lcom/facebook/timeline/mediagrid/protocol/ProfileMediaGridPhotoGraphQLModels$CollageLayoutFieldsModel;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 2310440
    iget-object v0, p0, LX/G0r;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    .line 2310441
    const v1, 0x7f0b0e10

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;LX/1bf;)LX/1aZ;
    .locals 1
    .param p2    # LX/1bf;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2310438
    iget-object v0, p0, LX/G0r;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ad;

    .line 2310439
    invoke-virtual {v0, p1}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/1Ae;->c(Ljava/lang/Object;)LX/1Ae;

    move-result-object v0

    check-cast v0, LX/1Ad;

    invoke-virtual {v0}, LX/1Ad;->p()LX/1Ad;

    move-result-object v0

    invoke-virtual {v0}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1U8;LX/5vn;)LX/1bf;
    .locals 1
    .param p2    # LX/5vn;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2310436
    invoke-virtual {p0, p1, p2}, LX/G0r;->b(LX/1U8;LX/5vn;)LX/1Fb;

    move-result-object v0

    .line 2310437
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILX/5vn;LX/6WO;)V
    .locals 6
    .param p2    # LX/5vn;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x6

    const/4 v1, 0x0

    .line 2310429
    if-eqz p2, :cond_0

    .line 2310430
    invoke-interface {p2}, LX/5vn;->c()D

    move-result-wide v0

    double-to-int v0, v0

    invoke-interface {p2}, LX/5vn;->d()D

    move-result-wide v2

    double-to-int v1, v2

    invoke-interface {p2}, LX/5vn;->b()D

    move-result-wide v2

    double-to-int v2, v2

    invoke-interface {p2}, LX/5vn;->a()D

    move-result-wide v4

    double-to-int v3, v4

    invoke-virtual {p3, v0, v1, v2, v3}, LX/6WO;->a(IIII)LX/6WO;

    .line 2310431
    :goto_0
    return-void

    .line 2310432
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 2310433
    invoke-virtual {p3, v1, v1, v2, v2}, LX/6WO;->a(IIII)LX/6WO;

    goto :goto_0

    .line 2310434
    :cond_1
    iget-object v0, p0, LX/G0r;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    .line 2310435
    const-string v1, "profile_media_grid_unexpected_null_collage_layout"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Null collageLayout. Size of collageLayoutList: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(LX/1U8;LX/5vn;)LX/1Fb;
    .locals 4
    .param p2    # LX/5vn;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2310417
    if-nez p1, :cond_0

    .line 2310418
    const/4 v0, 0x0

    .line 2310419
    :goto_0
    return-object v0

    .line 2310420
    :cond_0
    if-nez p2, :cond_1

    .line 2310421
    invoke-interface {p1}, LX/1U8;->aj_()LX/1Fb;

    move-result-object v0

    goto :goto_0

    .line 2310422
    :cond_1
    invoke-interface {p2}, LX/5vn;->b()D

    move-result-wide v0

    double-to-int v0, v0

    invoke-interface {p2}, LX/5vn;->a()D

    move-result-wide v2

    double-to-int v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2310423
    const/4 v1, 0x6

    .line 2310424
    const/4 v2, 0x6

    div-int/2addr v2, v0

    add-int/lit8 v2, v2, -0x1

    .line 2310425
    invoke-virtual {p0}, LX/G0r;->a()I

    move-result v3

    mul-int/2addr v3, v2

    .line 2310426
    iget-object v2, p0, LX/G0r;->c:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0hB;

    .line 2310427
    invoke-virtual {v2}, LX/0hB;->c()I

    move-result v2

    sub-int/2addr v2, v3

    move v2, v2

    .line 2310428
    invoke-static {p1, v0, v1, v2}, LX/G0r;->a(LX/1U8;III)LX/1Fb;

    move-result-object v0

    goto :goto_0
.end method
