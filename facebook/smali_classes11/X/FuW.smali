.class public LX/FuW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FuW;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2300071
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2300072
    return-void
.end method

.method public static a(Ljava/lang/Object;)LX/FuV;
    .locals 3

    .prologue
    .line 2300073
    instance-of v0, p0, Lcom/facebook/graphql/model/HideableUnit;

    if-eqz v0, :cond_2

    move-object v0, p0

    .line 2300074
    check-cast v0, Lcom/facebook/graphql/model/HideableUnit;

    .line 2300075
    invoke-interface {v0}, Lcom/facebook/graphql/model/HideableUnit;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/StoryVisibility;->VISIBLE:Lcom/facebook/graphql/enums/StoryVisibility;

    if-eq v1, v2, :cond_2

    .line 2300076
    invoke-interface {v0}, Lcom/facebook/graphql/model/HideableUnit;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/StoryVisibility;->GONE:Lcom/facebook/graphql/enums/StoryVisibility;

    if-eq v1, v2, :cond_0

    invoke-interface {v0}, Lcom/facebook/graphql/model/HideableUnit;->P_()Lcom/facebook/graphql/enums/StoryVisibility;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/StoryVisibility;->DISAPPEARING:Lcom/facebook/graphql/enums/StoryVisibility;

    if-ne v0, v1, :cond_1

    .line 2300077
    :cond_0
    sget-object v0, LX/FuV;->DELETED_STORY:LX/FuV;

    .line 2300078
    :goto_0
    return-object v0

    .line 2300079
    :cond_1
    sget-object v0, LX/FuV;->HIDDEN_STORY:LX/FuV;

    goto :goto_0

    .line 2300080
    :cond_2
    instance-of v0, p0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v0, :cond_7

    .line 2300081
    check-cast p0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2300082
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->aP()I

    move-result v1

    .line 2300083
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    .line 2300084
    :goto_1
    if-lez v1, :cond_4

    .line 2300085
    sget-object v0, LX/FuV;->STORY_EDGE_NARROW_OR_AGG:LX/FuV;

    goto :goto_0

    .line 2300086
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 2300087
    :cond_4
    if-eqz v0, :cond_6

    .line 2300088
    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2300089
    sget-object v0, LX/FuV;->STORY_EDGE_NARROW_OR_AGG:LX/FuV;

    goto :goto_0

    .line 2300090
    :cond_5
    sget-object v0, LX/FuV;->STORY_EDGE_WIDE:LX/FuV;

    goto :goto_0

    .line 2300091
    :cond_6
    sget-object v0, LX/FuV;->STORY_BASE:LX/FuV;

    goto :goto_0

    .line 2300092
    :cond_7
    instance-of v0, p0, LX/G55;

    if-eqz v0, :cond_8

    move-object v0, p0

    check-cast v0, LX/G55;

    invoke-virtual {v0}, LX/G52;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    move-object v0, p0

    check-cast v0, LX/G55;

    iget-boolean v0, v0, LX/G55;->e:Z

    if-nez v0, :cond_8

    .line 2300093
    sget-object v0, LX/FuV;->LOADING:LX/FuV;

    goto :goto_0

    .line 2300094
    :cond_8
    instance-of v0, p0, LX/G55;

    if-eqz v0, :cond_9

    .line 2300095
    sget-object v0, LX/FuV;->SCRUBBER:LX/FuV;

    goto :goto_0

    .line 2300096
    :cond_9
    instance-of v0, p0, LX/G53;

    if-eqz v0, :cond_a

    .line 2300097
    sget-object v0, LX/FuV;->SEE_MORE:LX/FuV;

    goto :goto_0

    .line 2300098
    :cond_a
    instance-of v0, p0, LX/G4z;

    if-nez v0, :cond_b

    instance-of v0, p0, LX/G51;

    if-eqz v0, :cond_c

    .line 2300099
    :cond_b
    sget-object v0, LX/FuV;->LOADING:LX/FuV;

    goto :goto_0

    .line 2300100
    :cond_c
    instance-of v0, p0, LX/G50;

    if-eqz v0, :cond_d

    .line 2300101
    sget-object v0, LX/FuV;->NO_STORIES:LX/FuV;

    goto :goto_0

    .line 2300102
    :cond_d
    sget-object v0, LX/FuV;->UNKNOWN:LX/FuV;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/FuW;
    .locals 3

    .prologue
    .line 2300103
    sget-object v0, LX/FuW;->a:LX/FuW;

    if-nez v0, :cond_1

    .line 2300104
    const-class v1, LX/FuW;

    monitor-enter v1

    .line 2300105
    :try_start_0
    sget-object v0, LX/FuW;->a:LX/FuW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2300106
    if-eqz v2, :cond_0

    .line 2300107
    :try_start_1
    new-instance v0, LX/FuW;

    invoke-direct {v0}, LX/FuW;-><init>()V

    .line 2300108
    move-object v0, v0

    .line 2300109
    sput-object v0, LX/FuW;->a:LX/FuW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2300110
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2300111
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2300112
    :cond_1
    sget-object v0, LX/FuW;->a:LX/FuW;

    return-object v0

    .line 2300113
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2300114
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
