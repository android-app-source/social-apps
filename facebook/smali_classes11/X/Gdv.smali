.class public final LX/Gdv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;


# direct methods
.method public constructor <init>(Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2374690
    iput-object p1, p0, LX/Gdv;->b:Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;

    iput-object p2, p0, LX/Gdv;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x7170d02b

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2374691
    iget-object v0, p0, LX/Gdv;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2374692
    iget-object v0, p0, LX/Gdv;->b:Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;

    iget-object v0, v0, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->f:LX/03V;

    const-string v2, "PymlEgoProfileSwipeItemController.categoryMissing"

    const-string v3, "Must have category."

    invoke-virtual {v0, v2, v3}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2374693
    sget-object v0, LX/0ax;->bq:Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2374694
    :goto_0
    iget-object v2, p0, LX/Gdv;->b:Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;

    iget-object v2, v2, Lcom/facebook/feedplugins/pyml/controllers/PymlEgoProfileSwipeItemController;->m:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2374695
    const v0, 0x1c64c0f6

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2374696
    :cond_0
    sget-object v0, LX/0ax;->br:Ljava/lang/String;

    iget-object v2, p0, LX/Gdv;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
