.class public final LX/Fkt;
.super LX/1wH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1wH",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/Fku;


# direct methods
.method public constructor <init>(LX/Fku;)V
    .locals 0

    .prologue
    .line 2279102
    iput-object p1, p0, LX/Fkt;->b:LX/Fku;

    invoke-direct {p0, p1}, LX/1wH;-><init>(LX/1SX;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2279103
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2279104
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2279105
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2279106
    iget-object v2, p0, LX/Fkt;->b:LX/Fku;

    invoke-virtual {v2, v0}, LX/1SX;->i(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2279107
    const v2, 0x7f083309    # 1.8104E38f

    invoke-interface {p1, v2}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 2279108
    sget-object v3, LX/Al1;->DELETE:LX/Al1;

    invoke-virtual {v3}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v3

    .line 2279109
    new-instance v4, LX/Fkr;

    invoke-direct {v4, p0, p2, v3, v1}, LX/Fkr;-><init>(LX/Fkt;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Landroid/content/Context;)V

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2279110
    iget-object v4, p0, LX/Fkt;->b:LX/Fku;

    .line 2279111
    const v5, 0x7f020a0b

    move v5, v5

    .line 2279112
    invoke-virtual {v4, v2, v5, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 2279113
    iget-object v4, p0, LX/Fkt;->b:LX/Fku;

    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const/4 v5, 0x0

    .line 2279114
    invoke-virtual {v4, p2, v2, v3, v5}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 2279115
    :cond_0
    invoke-static {v0}, LX/1Sn;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    move v2, v3

    .line 2279116
    if-eqz v2, :cond_1

    .line 2279117
    const v2, 0x7f08330a

    invoke-interface {p1, v2}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 2279118
    new-instance v3, LX/Fks;

    invoke-direct {v3, p0, v0, v1}, LX/Fks;-><init>(LX/Fkt;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2279119
    :cond_1
    iget-object v2, p0, LX/Fkt;->b:LX/Fku;

    .line 2279120
    invoke-virtual {v2, v0}, LX/1SX;->e(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v3

    move v2, v3

    .line 2279121
    if-eqz v2, :cond_2

    .line 2279122
    iget-object v2, p0, LX/Fkt;->b:LX/Fku;

    .line 2279123
    sget-object v3, LX/Al1;->EDIT_PRIVACY:LX/Al1;

    invoke-virtual {v3}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v3

    .line 2279124
    const v4, 0x7f08330e

    invoke-interface {p1, v4}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 2279125
    invoke-interface {v4}, Landroid/view/MenuItem;->getItemId()I

    move-result v5

    const/4 p3, 0x0

    invoke-virtual {v2, p2, v5, v3, p3}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 2279126
    new-instance v5, LX/Fkq;

    invoke-direct {v5, v2, p2, v3, v1}, LX/Fkq;-><init>(LX/Fku;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Landroid/content/Context;)V

    invoke-interface {v4, v5}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2279127
    iget-object v3, v2, LX/Fku;->b:LX/1e4;

    invoke-static {v0}, LX/0sa;->c(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLPrivacyScope;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPrivacyScope;->t()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/1e4;->a(Ljava/lang/String;)I

    move-result v3

    .line 2279128
    invoke-virtual {v2, v4, v3, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 2279129
    :cond_2
    invoke-virtual {p0, p1, p2}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;)V

    .line 2279130
    invoke-virtual {p0, p2}, LX/1wH;->b(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2279131
    invoke-virtual {p0, p1, p2, v1}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/content/Context;)V

    .line 2279132
    :cond_3
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2279133
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2279134
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2279135
    iget-object v1, p0, LX/Fkt;->b:LX/Fku;

    invoke-virtual {v1, v0}, LX/1SX;->i(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2279136
    invoke-static {v0}, LX/1Sn;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result p1

    move v1, p1

    .line 2279137
    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, LX/1wH;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/Fkt;->b:LX/Fku;

    .line 2279138
    invoke-virtual {v1, v0}, LX/1SX;->e(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result p0

    move v0, p0

    .line 2279139
    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
