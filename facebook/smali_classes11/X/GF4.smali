.class public LX/GF4;
.super LX/0b4;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0b4",
        "<",
        "LX/8wQ;",
        "LX/8wN;",
        ">;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/GFR;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/2U3;


# direct methods
.method public constructor <init>(LX/2U3;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2332280
    invoke-direct {p0}, LX/0b4;-><init>()V

    .line 2332281
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, LX/GF4;->a:Landroid/util/SparseArray;

    .line 2332282
    iput-object p1, p0, LX/GF4;->b:LX/2U3;

    .line 2332283
    return-void
.end method

.method public static a(LX/0QB;)LX/GF4;
    .locals 4

    .prologue
    .line 2332267
    const-class v1, LX/GF4;

    monitor-enter v1

    .line 2332268
    :try_start_0
    sget-object v0, LX/GF4;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2332269
    sput-object v2, LX/GF4;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2332270
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2332271
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2332272
    new-instance p0, LX/GF4;

    invoke-static {v0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v3

    check-cast v3, LX/2U3;

    invoke-direct {p0, v3}, LX/GF4;-><init>(LX/2U3;)V

    .line 2332273
    move-object v0, p0

    .line 2332274
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2332275
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GF4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2332276
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2332277
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ILX/GFR;)V
    .locals 1

    .prologue
    .line 2332278
    iget-object v0, p0, LX/GF4;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2332279
    return-void
.end method
