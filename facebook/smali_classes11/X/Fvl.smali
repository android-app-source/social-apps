.class public final LX/Fvl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# instance fields
.field public final synthetic a:Lcom/facebook/fig/button/AnimatableContentFigButton;

.field public final synthetic b:Lcom/facebook/timeline/header/TimelineIntroCardBioView;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/TimelineIntroCardBioView;Lcom/facebook/fig/button/AnimatableContentFigButton;)V
    .locals 0

    .prologue
    .line 2301904
    iput-object p1, p0, LX/Fvl;->b:Lcom/facebook/timeline/header/TimelineIntroCardBioView;

    iput-object p2, p0, LX/Fvl;->a:Lcom/facebook/fig/button/AnimatableContentFigButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    .prologue
    .line 2301905
    iget-object v0, p0, LX/Fvl;->a:Lcom/facebook/fig/button/AnimatableContentFigButton;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v2

    sub-float/2addr v1, v2

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/facebook/fig/button/AnimatableContentFigButton;->a(I)V

    .line 2301906
    return-void
.end method
