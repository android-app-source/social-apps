.class public final LX/Gev;
.super LX/1X5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X5",
        "<",
        "LX/Gex;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/Gew;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Gex",
            "<TE;>.PageYouMay",
            "LikeCardComponentImpl;"
        }
    .end annotation
.end field

.field public final synthetic b:LX/Gex;

.field private c:[Ljava/lang/String;

.field private d:I

.field private e:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(LX/Gex;)V
    .locals 4

    .prologue
    const/4 v3, 0x6

    .line 2376287
    iput-object p1, p0, LX/Gev;->b:LX/Gex;

    invoke-direct {p0}, LX/1X5;-><init>()V

    .line 2376288
    new-array v0, v3, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "environment"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "asyncPrefetchProps"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "feedUnit"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "suggestedItem"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "smallFormatFlag"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "goToNextController"

    aput-object v2, v0, v1

    iput-object v0, p0, LX/Gev;->c:[Ljava/lang/String;

    .line 2376289
    iput v3, p0, LX/Gev;->d:I

    .line 2376290
    new-instance v0, Ljava/util/BitSet;

    iget v1, p0, LX/Gev;->d:I

    invoke-direct {v0, v1}, Ljava/util/BitSet;-><init>(I)V

    iput-object v0, p0, LX/Gev;->e:Ljava/util/BitSet;

    return-void
.end method

.method public static a$redex0(LX/Gev;LX/1De;IILX/Gew;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "II",
            "LX/Gex",
            "<TE;>.PageYouMay",
            "LikeCardComponentImpl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2376283
    invoke-super {p0, p1, p2, p3, p4}, LX/1X5;->a(LX/1De;IILX/1X1;)V

    .line 2376284
    iput-object p4, p0, LX/Gev;->a:LX/Gew;

    .line 2376285
    iget-object v0, p0, LX/Gev;->e:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->clear()V

    .line 2376286
    return-void
.end method


# virtual methods
.method public final a(LX/1Pp;)LX/Gev;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)",
            "LX/Gex",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2376280
    iget-object v0, p0, LX/Gev;->a:LX/Gew;

    iput-object p1, v0, LX/Gew;->a:LX/1Pp;

    .line 2376281
    iget-object v0, p0, LX/Gev;->e:Ljava/util/BitSet;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2376282
    return-object p0
.end method

.method public final a(LX/1f9;)LX/Gev;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1f9;",
            ")",
            "LX/Gex",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2376277
    iget-object v0, p0, LX/Gev;->a:LX/Gew;

    iput-object p1, v0, LX/Gew;->b:LX/1f9;

    .line 2376278
    iget-object v0, p0, LX/Gev;->e:Ljava/util/BitSet;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2376279
    return-object p0
.end method

.method public final a(LX/25E;)LX/Gev;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/25E;",
            ")",
            "LX/Gex",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2376274
    iget-object v0, p0, LX/Gev;->a:LX/Gew;

    iput-object p1, v0, LX/Gew;->d:LX/25E;

    .line 2376275
    iget-object v0, p0, LX/Gev;->e:Ljava/util/BitSet;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2376276
    return-object p0
.end method

.method public final a(LX/2dx;)LX/Gev;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/2dx;",
            ")",
            "LX/Gex",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2376249
    iget-object v0, p0, LX/Gev;->a:LX/Gew;

    iput-object p1, v0, LX/Gew;->g:LX/2dx;

    .line 2376250
    iget-object v0, p0, LX/Gev;->e:Ljava/util/BitSet;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2376251
    return-object p0
.end method

.method public final a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)LX/Gev;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ")",
            "LX/Gex",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2376271
    iget-object v0, p0, LX/Gev;->a:LX/Gew;

    iput-object p1, v0, LX/Gew;->c:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 2376272
    iget-object v0, p0, LX/Gev;->e:Ljava/util/BitSet;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2376273
    return-object p0
.end method

.method public final a(Z)LX/Gev;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/Gex",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2376268
    iget-object v0, p0, LX/Gev;->a:LX/Gew;

    iput-boolean p1, v0, LX/Gew;->e:Z

    .line 2376269
    iget-object v0, p0, LX/Gev;->e:Ljava/util/BitSet;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->set(I)V

    .line 2376270
    return-object p0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 2376264
    invoke-super {p0}, LX/1X5;->a()V

    .line 2376265
    const/4 v0, 0x0

    iput-object v0, p0, LX/Gev;->a:LX/Gew;

    .line 2376266
    iget-object v0, p0, LX/Gev;->b:LX/Gex;

    iget-object v0, v0, LX/Gex;->b:LX/0Zi;

    invoke-virtual {v0, p0}, LX/0Zj;->a(Ljava/lang/Object;)Z

    .line 2376267
    return-void
.end method

.method public final b(Z)LX/Gev;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "LX/Gex",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    .line 2376262
    iget-object v0, p0, LX/Gev;->a:LX/Gew;

    iput-boolean p1, v0, LX/Gew;->f:Z

    .line 2376263
    return-object p0
.end method

.method public final d()LX/1X1;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/1X1",
            "<",
            "LX/Gex;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2376252
    iget-object v1, p0, LX/Gev;->e:Ljava/util/BitSet;

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Gev;->e:Ljava/util/BitSet;

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextClearBit(I)I

    move-result v1

    iget v2, p0, LX/Gev;->d:I

    if-ge v1, v2, :cond_2

    .line 2376253
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2376254
    :goto_0
    iget v2, p0, LX/Gev;->d:I

    if-ge v0, v2, :cond_1

    .line 2376255
    iget-object v2, p0, LX/Gev;->e:Ljava/util/BitSet;

    invoke-virtual {v2, v0}, Ljava/util/BitSet;->get(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2376256
    iget-object v2, p0, LX/Gev;->c:[Ljava/lang/String;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2376257
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2376258
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "The following props are not marked as optional and were not supplied: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2376259
    :cond_2
    iget-object v0, p0, LX/Gev;->a:LX/Gew;

    .line 2376260
    invoke-virtual {p0}, LX/Gev;->a()V

    .line 2376261
    return-object v0
.end method
