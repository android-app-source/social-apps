.class public final enum LX/FQN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FQN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FQN;

.field public static final enum DISTANCE:LX/FQN;

.field public static final enum LIKES:LX/FQN;

.field public static final enum OPEN_NOW:LX/FQN;

.field public static final enum PRICE:LX/FQN;

.field public static final enum REVIEW:LX/FQN;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2239026
    new-instance v0, LX/FQN;

    const-string v1, "REVIEW"

    invoke-direct {v0, v1, v2}, LX/FQN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FQN;->REVIEW:LX/FQN;

    .line 2239027
    new-instance v0, LX/FQN;

    const-string v1, "DISTANCE"

    invoke-direct {v0, v1, v3}, LX/FQN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FQN;->DISTANCE:LX/FQN;

    .line 2239028
    new-instance v0, LX/FQN;

    const-string v1, "PRICE"

    invoke-direct {v0, v1, v4}, LX/FQN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FQN;->PRICE:LX/FQN;

    .line 2239029
    new-instance v0, LX/FQN;

    const-string v1, "OPEN_NOW"

    invoke-direct {v0, v1, v5}, LX/FQN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FQN;->OPEN_NOW:LX/FQN;

    .line 2239030
    new-instance v0, LX/FQN;

    const-string v1, "LIKES"

    invoke-direct {v0, v1, v6}, LX/FQN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FQN;->LIKES:LX/FQN;

    .line 2239031
    const/4 v0, 0x5

    new-array v0, v0, [LX/FQN;

    sget-object v1, LX/FQN;->REVIEW:LX/FQN;

    aput-object v1, v0, v2

    sget-object v1, LX/FQN;->DISTANCE:LX/FQN;

    aput-object v1, v0, v3

    sget-object v1, LX/FQN;->PRICE:LX/FQN;

    aput-object v1, v0, v4

    sget-object v1, LX/FQN;->OPEN_NOW:LX/FQN;

    aput-object v1, v0, v5

    sget-object v1, LX/FQN;->LIKES:LX/FQN;

    aput-object v1, v0, v6

    sput-object v0, LX/FQN;->$VALUES:[LX/FQN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2239033
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FQN;
    .locals 1

    .prologue
    .line 2239034
    const-class v0, LX/FQN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FQN;

    return-object v0
.end method

.method public static values()[LX/FQN;
    .locals 1

    .prologue
    .line 2239032
    sget-object v0, LX/FQN;->$VALUES:[LX/FQN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FQN;

    return-object v0
.end method
