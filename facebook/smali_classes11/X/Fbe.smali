.class public LX/Fbe;
.super LX/FbI;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static a:LX/0Xm;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2260761
    invoke-direct {p0}, LX/FbI;-><init>()V

    .line 2260762
    return-void
.end method

.method public static a(LX/0QB;)LX/Fbe;
    .locals 3

    .prologue
    .line 2260763
    const-class v1, LX/Fbe;

    monitor-enter v1

    .line 2260764
    :try_start_0
    sget-object v0, LX/Fbe;->a:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2260765
    sput-object v2, LX/Fbe;->a:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2260766
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2260767
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    .line 2260768
    new-instance v0, LX/Fbe;

    invoke-direct {v0}, LX/Fbe;-><init>()V

    .line 2260769
    move-object v0, v0

    .line 2260770
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2260771
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fbe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2260772
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2260773
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;)LX/8dV;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260774
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    .line 2260775
    if-nez v0, :cond_0

    .line 2260776
    const/4 v0, 0x0

    .line 2260777
    :goto_0
    return-object v0

    .line 2260778
    :cond_0
    new-instance v1, LX/8dX;

    invoke-direct {v1}, LX/8dX;-><init>()V

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dW()Ljava/lang/String;

    move-result-object v2

    .line 2260779
    iput-object v2, v1, LX/8dX;->L:Ljava/lang/String;

    .line 2260780
    move-object v1, v1

    .line 2260781
    new-instance v2, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v3, -0x5852ec13

    invoke-direct {v2, v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2260782
    iput-object v2, v1, LX/8dX;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2260783
    move-object v1, v1

    .line 2260784
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->lx()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v2

    .line 2260785
    if-nez v2, :cond_1

    .line 2260786
    const/4 v3, 0x0

    .line 2260787
    :goto_1
    move-object v2, v3

    .line 2260788
    iput-object v2, v1, LX/8dX;->an:Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    .line 2260789
    move-object v1, v1

    .line 2260790
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dX()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 2260791
    if-nez v2, :cond_2

    .line 2260792
    const/4 v3, 0x0

    .line 2260793
    :goto_2
    move-object v2, v3

    .line 2260794
    iput-object v2, v1, LX/8dX;->M:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    .line 2260795
    move-object v1, v1

    .line 2260796
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->dY()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v2

    .line 2260797
    if-nez v2, :cond_3

    .line 2260798
    const/4 v3, 0x0

    .line 2260799
    :goto_3
    move-object v2, v3

    .line 2260800
    iput-object v2, v1, LX/8dX;->N:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2260801
    move-object v1, v1

    .line 2260802
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLNode;->l()Ljava/lang/String;

    move-result-object v0

    .line 2260803
    iput-object v0, v1, LX/8dX;->c:Ljava/lang/String;

    .line 2260804
    move-object v0, v1

    .line 2260805
    invoke-virtual {v0}, LX/8dX;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    move-result-object v0

    .line 2260806
    new-instance v1, LX/8dV;

    invoke-direct {v1}, LX/8dV;-><init>()V

    .line 2260807
    iput-object v0, v1, LX/8dV;->c:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel;

    .line 2260808
    move-object v0, v1

    .line 2260809
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->j()Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    .line 2260810
    iput-object v1, v0, LX/8dV;->e:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 2260811
    move-object v0, v0

    .line 2260812
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->fN_()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;

    move-result-object v1

    .line 2260813
    if-nez v1, :cond_4

    .line 2260814
    const/4 v2, 0x0

    .line 2260815
    :goto_4
    move-object v1, v2

    .line 2260816
    iput-object v1, v0, LX/8dV;->d:Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;

    .line 2260817
    move-object v0, v0

    .line 2260818
    goto :goto_0

    :cond_1
    new-instance v3, LX/8hA;

    invoke-direct {v3}, LX/8hA;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->H()Ljava/lang/String;

    move-result-object p0

    .line 2260819
    iput-object p0, v3, LX/8hA;->b:Ljava/lang/String;

    .line 2260820
    move-object v3, v3

    .line 2260821
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLActor;->ab()Ljava/lang/String;

    move-result-object p0

    .line 2260822
    iput-object p0, v3, LX/8hA;->c:Ljava/lang/String;

    .line 2260823
    move-object v3, v3

    .line 2260824
    invoke-virtual {v3}, LX/8hA;->a()Lcom/facebook/search/results/protocol/entity/SearchResultsPhotoModels$SearchResultsPhotoModel$PhotoOwnerModel;

    move-result-object v3

    goto :goto_1

    :cond_2
    new-instance v3, LX/8db;

    invoke-direct {v3}, LX/8db;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->c()I

    move-result p0

    .line 2260825
    iput p0, v3, LX/8db;->c:I

    .line 2260826
    move-object v3, v3

    .line 2260827
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->a()I

    move-result p0

    .line 2260828
    iput p0, v3, LX/8db;->a:I

    .line 2260829
    move-object v3, v3

    .line 2260830
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLImage;->b()Ljava/lang/String;

    move-result-object p0

    .line 2260831
    iput-object p0, v3, LX/8db;->b:Ljava/lang/String;

    .line 2260832
    move-object v3, v3

    .line 2260833
    invoke-virtual {v3}, LX/8db;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$EdgesNodeModel$ImageModel;

    move-result-object v3

    goto :goto_2

    :cond_3
    invoke-static {v2}, LX/9JZ;->a(Lcom/facebook/graphql/model/GraphQLImage;)LX/1Fb;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a(LX/1Fb;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v3

    goto :goto_3

    :cond_4
    new-instance v2, LX/8do;

    invoke-direct {v2}, LX/8do;-><init>()V

    invoke-virtual {v1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;->fK_()Z

    move-result v3

    .line 2260834
    iput-boolean v3, v2, LX/8do;->d:Z

    .line 2260835
    move-object v2, v2

    .line 2260836
    invoke-virtual {v1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2260837
    iput-object v3, v2, LX/8do;->a:Ljava/lang/String;

    .line 2260838
    move-object v2, v2

    .line 2260839
    invoke-virtual {v1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 2260840
    iput-object v3, v2, LX/8do;->b:Ljava/lang/String;

    .line 2260841
    move-object v2, v2

    .line 2260842
    invoke-virtual {v2}, LX/8do;->a()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel$ModuleResultsModel$EdgesModel$ResultDecorationModel;

    move-result-object v2

    goto :goto_4
.end method
