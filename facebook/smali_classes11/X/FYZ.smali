.class public final LX/FYZ;
.super LX/EkQ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EkQ",
        "<",
        "LX/BO1;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/16H;

.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Sc;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FZ3;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/16H;LX/0Ot;LX/0Ot;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/16H;",
            "LX/0Ot",
            "<",
            "LX/1Sc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FZ3;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2256143
    invoke-direct {p0}, LX/EkQ;-><init>()V

    .line 2256144
    iput-object p1, p0, LX/FYZ;->a:Landroid/content/Context;

    .line 2256145
    iput-object p2, p0, LX/FYZ;->b:LX/16H;

    .line 2256146
    iput-object p3, p0, LX/FYZ;->c:LX/0Ot;

    .line 2256147
    iput-object p4, p0, LX/FYZ;->d:LX/0Ot;

    .line 2256148
    iput-object p5, p0, LX/FYZ;->e:Ljava/lang/String;

    .line 2256149
    return-void
.end method


# virtual methods
.method public bridge synthetic onClick(Landroid/view/View;LX/AU0;)V
    .locals 0

    .prologue
    .line 2256150
    check-cast p2, LX/BO1;

    invoke-virtual {p0, p1, p2}, LX/FYZ;->onClick(Landroid/view/View;LX/BO1;)V

    return-void
.end method

.method public onClick(Landroid/view/View;LX/BO1;)V
    .locals 5

    .prologue
    .line 2256151
    iget-object v0, p0, LX/FYZ;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FZ3;

    iget-object v1, p0, LX/FYZ;->a:Landroid/content/Context;

    invoke-interface {p2}, LX/BO1;->w()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p2}, LX/BO1;->y()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2}, LX/BO1;->A()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/FZ3;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2256152
    invoke-interface {p2}, LX/BO1;->F()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2256153
    iget-object v0, p0, LX/FYZ;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Sc;

    invoke-interface {p2}, LX/AUV;->d()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/1Sc;->e(J)V

    .line 2256154
    :cond_0
    iget-object v0, p0, LX/FYZ;->b:LX/16H;

    invoke-interface {p2}, LX/BO1;->f()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/FYZ;->e:Ljava/lang/String;

    .line 2256155
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v4, "click"

    invoke-direct {v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v4, "saved_dashboard"

    .line 2256156
    iput-object v4, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2256157
    move-object v3, v3

    .line 2256158
    const-string v4, "object_id"

    invoke-virtual {v3, v4, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "surface"

    const-string p0, "native_saved_dashboard"

    invoke-virtual {v3, v4, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "mechanism"

    const-string p0, "social_context_row"

    invoke-virtual {v3, v4, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "section_type"

    invoke-virtual {v3, v4, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string v4, "event_id"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v3, v4, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    .line 2256159
    iget-object v4, v0, LX/16H;->a:LX/0Zb;

    invoke-interface {v4, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2256160
    return-void
.end method
