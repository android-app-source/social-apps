.class public LX/FDv;
.super LX/Dny;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final u:Ljava/lang/Object;


# instance fields
.field private final b:LX/FDq;

.field private final c:LX/6cy;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0SI;

.field private final f:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0SG;

.field private final h:LX/2Ow;

.field private final i:LX/2Mq;

.field private final j:LX/6Po;

.field private final k:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field private final l:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FDt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2N4;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FDs;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/deliveryreceipt/SendDeliveryReceiptManager;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Ou;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/media/download/MediaDownloadManager;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public s:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6bg;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public t:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6fD;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2215519
    const-class v0, LX/FDv;

    sput-object v0, LX/FDv;->a:Ljava/lang/Class;

    .line 2215520
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/FDv;->u:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/FDq;LX/6cy;LX/0Or;LX/0SI;LX/0Or;LX/0SG;LX/2Ow;LX/2Mq;LX/6Po;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V
    .locals 1
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/messaging/annotations/IsMessengerSyncEnabled;
        .end annotation
    .end param
    .param p11    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FDq;",
            "LX/6cy;",
            "LX/0Or",
            "<",
            "LX/6dQ;",
            ">;",
            "LX/0SI;",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "LX/0SG;",
            "LX/2Ow;",
            "LX/2Mq;",
            "LX/6Po;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2215521
    const-string v0, "DbServiceHandler"

    invoke-direct {p0, v0}, LX/Dny;-><init>(Ljava/lang/String;)V

    .line 2215522
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2215523
    iput-object v0, p0, LX/FDv;->m:LX/0Ot;

    .line 2215524
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2215525
    iput-object v0, p0, LX/FDv;->n:LX/0Ot;

    .line 2215526
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2215527
    iput-object v0, p0, LX/FDv;->o:LX/0Ot;

    .line 2215528
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2215529
    iput-object v0, p0, LX/FDv;->p:LX/0Ot;

    .line 2215530
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2215531
    iput-object v0, p0, LX/FDv;->q:LX/0Ot;

    .line 2215532
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2215533
    iput-object v0, p0, LX/FDv;->r:LX/0Ot;

    .line 2215534
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2215535
    iput-object v0, p0, LX/FDv;->s:LX/0Ot;

    .line 2215536
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2215537
    iput-object v0, p0, LX/FDv;->t:LX/0Ot;

    .line 2215538
    iput-object p1, p0, LX/FDv;->b:LX/FDq;

    .line 2215539
    iput-object p2, p0, LX/FDv;->c:LX/6cy;

    .line 2215540
    iput-object p3, p0, LX/FDv;->d:LX/0Or;

    .line 2215541
    iput-object p4, p0, LX/FDv;->e:LX/0SI;

    .line 2215542
    iput-object p5, p0, LX/FDv;->f:LX/0Or;

    .line 2215543
    iput-object p6, p0, LX/FDv;->g:LX/0SG;

    .line 2215544
    iput-object p7, p0, LX/FDv;->h:LX/2Ow;

    .line 2215545
    iput-object p8, p0, LX/FDv;->i:LX/2Mq;

    .line 2215546
    iput-object p9, p0, LX/FDv;->j:LX/6Po;

    .line 2215547
    iput-object p10, p0, LX/FDv;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2215548
    iput-object p11, p0, LX/FDv;->l:LX/0Or;

    .line 2215549
    return-void
.end method

.method public static a(LX/0QB;)LX/FDv;
    .locals 7

    .prologue
    .line 2215550
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2215551
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2215552
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2215553
    if-nez v1, :cond_0

    .line 2215554
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2215555
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2215556
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2215557
    sget-object v1, LX/FDv;->u:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2215558
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2215559
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2215560
    :cond_1
    if-nez v1, :cond_4

    .line 2215561
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2215562
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2215563
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/FDv;->b(LX/0QB;)LX/FDv;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v1

    .line 2215564
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2215565
    if-nez v1, :cond_2

    .line 2215566
    sget-object v0, LX/FDv;->u:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FDv;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2215567
    :goto_1
    if-eqz v0, :cond_3

    .line 2215568
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2215569
    :goto_3
    check-cast v0, LX/FDv;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2215570
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2215571
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2215572
    :catchall_1
    move-exception v0

    .line 2215573
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2215574
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2215575
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2215576
    :cond_2
    :try_start_8
    sget-object v0, LX/FDv;->u:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FDv;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private static a(Lcom/facebook/messaging/model/messages/MessagesCollection;)Lcom/facebook/messaging/model/messages/Message;
    .locals 6
    .param p0    # Lcom/facebook/messaging/model/messages/MessagesCollection;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2215577
    if-nez p0, :cond_1

    move-object v0, v1

    .line 2215578
    :cond_0
    :goto_0
    return-object v0

    .line 2215579
    :cond_1
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v3, v0

    .line 2215580
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_2

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2215581
    iget-boolean v5, v0, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-eqz v5, :cond_0

    .line 2215582
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 2215583
    goto :goto_0
.end method

.method private static a(LX/FDv;Lcom/facebook/messaging/service/model/FetchThreadParams;Lcom/facebook/messaging/service/model/FetchThreadResult;LX/1qM;)Lcom/facebook/messaging/service/model/FetchThreadResult;
    .locals 10

    .prologue
    .line 2215584
    iget-object v0, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2215585
    iget-boolean v1, v0, Lcom/facebook/messaging/model/messages/MessagesCollection;->c:Z

    move v0, v1

    .line 2215586
    if-eqz v0, :cond_1

    .line 2215587
    :cond_0
    :goto_0
    return-object p2

    .line 2215588
    :cond_1
    iget-object v7, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2215589
    iget-object v8, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2215590
    iget v0, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->g:I

    move v0, v0

    .line 2215591
    invoke-virtual {v8}, Lcom/facebook/messaging/model/messages/MessagesCollection;->g()I

    move-result v1

    .line 2215592
    sub-int/2addr v0, v1

    .line 2215593
    if-lez v0, :cond_0

    .line 2215594
    invoke-virtual {v8}, Lcom/facebook/messaging/model/messages/MessagesCollection;->d()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v4

    .line 2215595
    new-instance v1, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;

    iget-object v2, v7, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v3, v4, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    iget-wide v4, v4, Lcom/facebook/messaging/model/messages/Message;->c:J

    add-int/lit8 v6, v0, 0x1

    invoke-direct/range {v1 .. v6}, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;Ljava/lang/String;JI)V

    .line 2215596
    iget-object v0, p0, LX/FDv;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6bg;

    sget-object v2, LX/6iC;->NEED_OLDER_MESSAGES:LX/6iC;

    invoke-static {v2}, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;->a(LX/6iC;)Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/6bg;->a(Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;)V

    .line 2215597
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2215598
    const-string v2, "fetchMoreMessagesParams"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2215599
    new-instance v1, LX/1qK;

    const-string v2, "fetch_more_messages"

    invoke-direct {v1, v2, v0}, LX/1qK;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-interface {p3, v1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2215600
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;

    .line 2215601
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    invoke-virtual {v1, p2, v0}, LX/FDs;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;)V

    .line 2215602
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;->c:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2215603
    iget-object v0, p0, LX/FDv;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6fD;

    invoke-virtual {v0, v8, v1}, LX/6fD;->b(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v0

    .line 2215604
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadResult;->a()LX/6iO;

    move-result-object v1

    sget-object v2, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_SERVER:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2215605
    iput-object v2, v1, LX/6iO;->b:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2215606
    move-object v1, v1

    .line 2215607
    iput-object v7, v1, LX/6iO;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2215608
    move-object v1, v1

    .line 2215609
    iput-object v0, v1, LX/6iO;->d:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2215610
    move-object v0, v1

    .line 2215611
    iget-object v1, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->f:LX/0Px;

    .line 2215612
    iput-object v1, v0, LX/6iO;->e:LX/0Px;

    .line 2215613
    move-object v0, v0

    .line 2215614
    iget-object v1, p0, LX/FDv;->g:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 2215615
    iput-wide v2, v0, LX/6iO;->g:J

    .line 2215616
    move-object v0, v0

    .line 2215617
    invoke-virtual {v0}, LX/6iO;->a()Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object p2

    goto/16 :goto_0
.end method

.method private a(Lcom/facebook/messaging/service/model/FetchThreadParams;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/messaging/service/model/FetchThreadResult;LX/1qM;)Lcom/facebook/messaging/service/model/FetchThreadResult;
    .locals 5

    .prologue
    .line 2215618
    iget-object v1, p3, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2215619
    iget-object v0, p3, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2215620
    iget-object v2, p3, Lcom/facebook/messaging/service/model/FetchThreadResult;->i:Ljava/util/Map;

    .line 2215621
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/MessagesCollection;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2215622
    :cond_0
    const/4 p3, 0x0

    .line 2215623
    :cond_1
    :goto_0
    return-object p3

    .line 2215624
    :cond_2
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->b:LX/0rS;

    move-object v0, v0

    .line 2215625
    sget-object v3, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    if-eq v0, v3, :cond_1

    .line 2215626
    :try_start_0
    invoke-static {p0, p1, p2, p3, p4}, LX/FDv;->b(LX/FDv;Lcom/facebook/messaging/service/model/FetchThreadParams;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/messaging/service/model/FetchThreadResult;LX/1qM;)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2215627
    invoke-static {p0, p1, v0, p4}, LX/FDv;->a(LX/FDv;Lcom/facebook/messaging/service/model/FetchThreadParams;Lcom/facebook/messaging/service/model/FetchThreadResult;LX/1qM;)Lcom/facebook/messaging/service/model/FetchThreadResult;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2215628
    iget-object v0, p0, LX/FDv;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2N4;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2215629
    iget v2, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->g:I

    move v2, v2

    .line 2215630
    invoke-virtual {v0, v1, v2}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2215631
    invoke-static {v0}, Lcom/facebook/messaging/service/model/FetchThreadResult;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;)LX/6iO;

    move-result-object v0

    sget-object v1, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_SERVER:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2215632
    iput-object v1, v0, LX/6iO;->b:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2215633
    move-object v0, v0

    .line 2215634
    invoke-virtual {v0}, LX/6iO;->a()Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object p3

    goto :goto_0

    .line 2215635
    :catch_0
    move-exception v0

    .line 2215636
    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->c:LX/0rS;

    move-object v1, v1

    .line 2215637
    sget-object v3, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-eq v1, v3, :cond_3

    .line 2215638
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadResult;->a()LX/6iO;

    move-result-object v0

    sget-object v1, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_CACHE_HAD_SERVER_ERROR:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2215639
    iput-object v1, v0, LX/6iO;->b:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2215640
    move-object v0, v0

    .line 2215641
    iget-object v1, p3, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2215642
    iput-object v1, v0, LX/6iO;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2215643
    move-object v0, v0

    .line 2215644
    iget-object v1, p3, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2215645
    iput-object v1, v0, LX/6iO;->d:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2215646
    move-object v0, v0

    .line 2215647
    iput-object v2, v0, LX/6iO;->f:Ljava/util/Map;

    .line 2215648
    move-object v0, v0

    .line 2215649
    iget-object v1, p3, Lcom/facebook/messaging/service/model/FetchThreadResult;->f:LX/0Px;

    .line 2215650
    iput-object v1, v0, LX/6iO;->e:LX/0Px;

    .line 2215651
    move-object v0, v0

    .line 2215652
    iget-wide v2, p3, Lcom/facebook/messaging/service/model/FetchThreadResult;->g:J

    .line 2215653
    iput-wide v2, v0, LX/6iO;->g:J

    .line 2215654
    move-object v0, v0

    .line 2215655
    invoke-virtual {v0}, LX/6iO;->a()Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object p3

    goto :goto_0

    .line 2215656
    :cond_3
    throw v0
.end method

.method private static a(LX/FDv;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/service/model/FetchThreadResult;)V
    .locals 9
    .param p0    # LX/FDv;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2215657
    iget-object v2, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2215658
    if-nez v2, :cond_1

    .line 2215659
    :cond_0
    :goto_0
    return-void

    .line 2215660
    :cond_1
    iget-object v0, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    if-nez v0, :cond_3

    const/4 v0, 0x0

    move-object v1, v0

    .line 2215661
    :goto_1
    if-eqz v1, :cond_0

    .line 2215662
    iget-object v0, p0, LX/FDv;->e:LX/0SI;

    invoke-interface {v0}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    .line 2215663
    iget-object v3, v0, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v0, v3

    .line 2215664
    new-instance v3, Lcom/facebook/user/model/UserKey;

    sget-object v4, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-direct {v3, v4, v0}, Lcom/facebook/user/model/UserKey;-><init>(LX/0XG;Ljava/lang/String;)V

    .line 2215665
    if-eqz p1, :cond_2

    iget-wide v5, p1, Lcom/facebook/messaging/model/messages/Message;->g:J

    iget-wide v7, v1, Lcom/facebook/messaging/model/messages/Message;->g:J

    cmp-long v5, v5, v7

    if-gez v5, :cond_4

    :cond_2
    const/4 v5, 0x1

    :goto_2
    move v0, v5

    .line 2215666
    if-eqz v0, :cond_0

    .line 2215667
    iget-object v0, v1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/ParticipantInfo;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, v1, Lcom/facebook/messaging/model/messages/Message;->e:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/ParticipantInfo;->b:Lcom/facebook/user/model/UserKey;

    invoke-virtual {v0, v3}, Lcom/facebook/user/model/UserKey;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_3
    move v0, v0

    .line 2215668
    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threads/ThreadSummary;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2215669
    iget-object v0, p0, LX/FDv;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CLl;

    const-string v2, "FETCH_THREAD"

    invoke-virtual {v0, v1, v2}, LX/CLl;->a(Lcom/facebook/messaging/model/messages/Message;Ljava/lang/String;)V

    goto :goto_0

    .line 2215670
    :cond_3
    iget-object v0, p2, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/MessagesCollection;->c()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_4
    const/4 v5, 0x0

    goto :goto_2

    :cond_5
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private a(Lcom/facebook/messaging/service/model/FetchThreadListParams;)Z
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 2215671
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v0, v0

    .line 2215672
    sget-object v1, LX/6ek;->MONTAGE:LX/6ek;

    if-ne v0, v1, :cond_0

    iget-object v1, p0, LX/FDv;->c:LX/6cy;

    invoke-static {v0}, LX/6cx;->a(LX/6ek;)LX/2bA;

    move-result-object v0

    invoke-virtual {v1, v0, v2, v3}, LX/48u;->a(LX/0To;J)J

    move-result-wide v0

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(LX/0QB;)LX/FDv;
    .locals 12

    .prologue
    .line 2215673
    new-instance v0, LX/FDv;

    invoke-static {p0}, LX/FDq;->a(LX/0QB;)LX/FDq;

    move-result-object v1

    check-cast v1, LX/FDq;

    invoke-static {p0}, LX/6cy;->a(LX/0QB;)LX/6cy;

    move-result-object v2

    check-cast v2, LX/6cy;

    const/16 v3, 0x274b

    invoke-static {p0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v4

    check-cast v4, LX/0SI;

    const/16 v5, 0x14ea

    invoke-static {p0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v6

    check-cast v6, LX/0SG;

    invoke-static {p0}, LX/2Ow;->a(LX/0QB;)LX/2Ow;

    move-result-object v7

    check-cast v7, LX/2Ow;

    invoke-static {p0}, LX/2Mq;->a(LX/0QB;)LX/2Mq;

    move-result-object v8

    check-cast v8, LX/2Mq;

    invoke-static {p0}, LX/6Po;->a(LX/0QB;)LX/6Po;

    move-result-object v9

    check-cast v9, LX/6Po;

    invoke-static {p0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v10

    check-cast v10, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v11, 0x12cb

    invoke-static {p0, v11}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v11

    invoke-direct/range {v0 .. v11}, LX/FDv;-><init>(LX/FDq;LX/6cy;LX/0Or;LX/0SI;LX/0Or;LX/0SG;LX/2Ow;LX/2Mq;LX/6Po;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;)V

    .line 2215674
    const/16 v1, 0x273a

    invoke-static {p0, v1}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v1

    const/16 v2, 0xd18

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    const/16 v3, 0x2739

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x2751

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xcee

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x27de

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2659

    invoke-static {p0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x281d

    invoke-static {p0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    .line 2215675
    iput-object v1, v0, LX/FDv;->m:LX/0Ot;

    iput-object v2, v0, LX/FDv;->n:LX/0Ot;

    iput-object v3, v0, LX/FDv;->o:LX/0Ot;

    iput-object v4, v0, LX/FDv;->p:LX/0Ot;

    iput-object v5, v0, LX/FDv;->q:LX/0Ot;

    iput-object v6, v0, LX/FDv;->r:LX/0Ot;

    iput-object v7, v0, LX/FDv;->s:LX/0Ot;

    iput-object v8, v0, LX/FDv;->t:LX/0Ot;

    .line 2215676
    return-object v0
.end method

.method private static b(LX/FDv;Lcom/facebook/messaging/service/model/FetchThreadParams;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/messaging/service/model/FetchThreadResult;LX/1qM;)Lcom/facebook/messaging/service/model/FetchThreadResult;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 2215891
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadParams;->b:LX/0rS;

    move-object v0, v0

    .line 2215892
    sget-object v1, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    if-ne v0, v1, :cond_2

    .line 2215893
    const/4 v1, 0x1

    .line 2215894
    :goto_0
    move v0, v1

    .line 2215895
    if-nez v0, :cond_0

    .line 2215896
    :goto_1
    return-object p3

    .line 2215897
    :cond_0
    iget-object v7, p3, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2215898
    invoke-static {v7}, LX/FDv;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v8

    .line 2215899
    const-wide/16 v0, -0x1

    .line 2215900
    if-eqz v8, :cond_1

    .line 2215901
    iget-wide v0, v8, Lcom/facebook/messaging/model/messages/Message;->g:J

    const-wide/16 v4, 0x1

    sub-long/2addr v0, v4

    .line 2215902
    :cond_1
    new-instance v2, LX/6iM;

    invoke-direct {v2}, LX/6iM;-><init>()V

    invoke-virtual {v2, p1}, LX/6iM;->a(Lcom/facebook/messaging/service/model/FetchThreadParams;)LX/6iM;

    move-result-object v2

    sget-object v4, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 2215903
    iput-object v4, v2, LX/6iM;->b:LX/0rS;

    .line 2215904
    move-object v2, v2

    .line 2215905
    iput-wide v0, v2, LX/6iM;->h:J

    .line 2215906
    move-object v0, v2

    .line 2215907
    invoke-virtual {v0}, LX/6iM;->j()Lcom/facebook/messaging/service/model/FetchThreadParams;

    move-result-object v1

    .line 2215908
    iget-object v0, p0, LX/FDv;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6bg;

    sget-object v2, LX/6iC;->NEED_MORE_RECENT_MESSAGES:LX/6iC;

    invoke-static {v2}, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;->a(LX/6iC;)Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/6bg;->a(Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;)V

    .line 2215909
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2215910
    const-string v0, "fetchThreadParams"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2215911
    new-instance v0, LX/1qK;

    const-string v1, "fetch_thread"

    move-object v4, v3

    move-object v5, p2

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, LX/1qK;-><init>(Ljava/lang/String;Landroid/os/Bundle;Ljava/lang/String;LX/0zW;Lcom/facebook/common/callercontext/CallerContext;LX/1qH;)V

    invoke-interface {p4, v0}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2215912
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2215913
    iget-object v1, p0, LX/FDv;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6dQ;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2215914
    const v1, 0x4858f83d

    invoke-static {v2, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2215915
    :try_start_0
    invoke-static {p0, v8, v0}, LX/FDv;->a(LX/FDv;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/service/model/FetchThreadResult;)V

    .line 2215916
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    invoke-virtual {v1, p3, v0}, LX/FDs;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;Lcom/facebook/messaging/service/model/FetchThreadResult;)V

    .line 2215917
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2215918
    const v1, -0x41c96c3d

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2215919
    iget-object v1, p0, LX/FDv;->t:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6fD;

    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual {v1, v2, v7}, LX/6fD;->b(Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/messages/MessagesCollection;)Lcom/facebook/messaging/model/messages/MessagesCollection;

    move-result-object v1

    .line 2215920
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadResult;->a()LX/6iO;

    move-result-object v2

    sget-object v3, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_SERVER:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2215921
    iput-object v3, v2, LX/6iO;->b:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2215922
    move-object v2, v2

    .line 2215923
    iget-object v3, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2215924
    iput-object v3, v2, LX/6iO;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2215925
    move-object v2, v2

    .line 2215926
    iput-object v1, v2, LX/6iO;->d:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2215927
    move-object v1, v2

    .line 2215928
    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->f:LX/0Px;

    .line 2215929
    iput-object v0, v1, LX/6iO;->e:LX/0Px;

    .line 2215930
    move-object v0, v1

    .line 2215931
    iget-object v1, p0, LX/FDv;->g:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    .line 2215932
    iput-wide v2, v0, LX/6iO;->g:J

    .line 2215933
    move-object v0, v0

    .line 2215934
    invoke-virtual {v0}, LX/6iO;->a()Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object p3

    goto/16 :goto_1

    .line 2215935
    :catchall_0
    move-exception v0

    const v1, -0x4d96eb1c

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    :cond_2
    iget-object v1, p3, Lcom/facebook/messaging/service/model/FetchThreadResult;->c:Lcom/facebook/fbservice/results/DataFetchDisposition;

    iget-object v1, v1, Lcom/facebook/fbservice/results/DataFetchDisposition;->isStaleData:LX/03R;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/03R;->asBoolean(Z)Z

    move-result v1

    goto/16 :goto_0
.end method


# virtual methods
.method public final D(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 2215677
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2215678
    const-string v1, "createLocalAdminMessageParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/CreateLocalAdminMessageParams;

    .line 2215679
    iget-object v1, v0, Lcom/facebook/messaging/service/model/CreateLocalAdminMessageParams;->a:Lcom/facebook/messaging/model/messages/Message;

    move-object v1, v1

    .line 2215680
    iget-object v0, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FDs;

    invoke-virtual {v0, v1}, LX/FDs;->a(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2215681
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2215682
    return-object v0
.end method

.method public final G(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 10

    .prologue
    .line 2215683
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2215684
    const-string v1, "updateFolderCountsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;

    .line 2215685
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    .line 2215686
    iget-object v2, v1, LX/FDs;->e:LX/FDq;

    iget-object v3, v0, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;->a:LX/6ek;

    invoke-virtual {v2, v3}, LX/FDq;->b(LX/6ek;)Lcom/facebook/messaging/model/folders/FolderCounts;

    move-result-object v2

    .line 2215687
    if-nez v2, :cond_0

    .line 2215688
    new-instance v3, Lcom/facebook/messaging/model/folders/FolderCounts;

    iget v4, v0, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;->b:I

    iget v5, v0, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;->c:I

    const-wide/16 v6, 0x0

    const-wide/16 v8, -0x1

    invoke-direct/range {v3 .. v9}, Lcom/facebook/messaging/model/folders/FolderCounts;-><init>(IIJJ)V

    .line 2215689
    :goto_0
    iget-object v2, v0, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;->a:LX/6ek;

    invoke-static {v1, v2, v3}, LX/FDs;->a(LX/FDs;LX/6ek;Lcom/facebook/messaging/model/folders/FolderCounts;)V

    .line 2215690
    move-object v0, v3

    .line 2215691
    new-instance v1, Lcom/facebook/messaging/service/model/UpdateFolderCountsResult;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/service/model/UpdateFolderCountsResult;-><init>(Lcom/facebook/messaging/model/folders/FolderCounts;)V

    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 2215692
    :cond_0
    new-instance v3, Lcom/facebook/messaging/model/folders/FolderCounts;

    iget v4, v0, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;->b:I

    iget v5, v0, Lcom/facebook/messaging/service/model/UpdateFolderCountsParams;->c:I

    iget-wide v6, v2, Lcom/facebook/messaging/model/folders/FolderCounts;->d:J

    iget-wide v8, v2, Lcom/facebook/messaging/model/folders/FolderCounts;->e:J

    invoke-direct/range {v3 .. v9}, Lcom/facebook/messaging/model/folders/FolderCounts;-><init>(IIJJ)V

    goto :goto_0
.end method

.method public final K(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 5

    .prologue
    .line 2215693
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v3

    .line 2215694
    invoke-virtual {v3}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/EditUsernameResult;

    .line 2215695
    if-eqz v0, :cond_0

    .line 2215696
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    new-instance v4, LX/0XI;

    invoke-direct {v4}, LX/0XI;-><init>()V

    iget-object v2, p0, LX/FDv;->l:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/user/model/User;

    invoke-virtual {v4, v2}, LX/0XI;->a(Lcom/facebook/user/model/User;)LX/0XI;

    move-result-object v2

    iget-object v0, v0, Lcom/facebook/messaging/service/model/EditUsernameResult;->a:Ljava/lang/String;

    .line 2215697
    iput-object v0, v2, LX/0XI;->l:Ljava/lang/String;

    .line 2215698
    move-object v0, v2

    .line 2215699
    invoke-virtual {v0}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v0

    .line 2215700
    iget-object v2, v1, LX/FDs;->f:LX/3N0;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/3N0;->a(Ljava/util/List;)V

    .line 2215701
    :cond_0
    return-object v3
.end method

.method public final L(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 2215702
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2215703
    sget-object v1, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledParams;

    .line 2215704
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2215705
    iget-object v1, p0, LX/FDv;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2N4;

    .line 2215706
    iget-object v3, v0, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v0, v3

    .line 2215707
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v1

    .line 2215708
    iget-object v0, v1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v0, :cond_0

    .line 2215709
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledResult;

    .line 2215710
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2215711
    iget-object v3, v1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2215712
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    .line 2215713
    iget-boolean v4, v0, Lcom/facebook/messaging/service/model/FetchIsThreadQueueEnabledResult;->a:Z

    move v0, v4

    .line 2215714
    invoke-static {v0}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v0

    iget-object v4, p0, LX/FDv;->g:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    .line 2215715
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object p0

    invoke-virtual {p0, v3}, LX/6g6;->a(Lcom/facebook/messaging/model/threads/ThreadSummary;)LX/6g6;

    move-result-object p0

    .line 2215716
    iput-object v0, p0, LX/6g6;->S:LX/03R;

    .line 2215717
    move-object p0, p0

    .line 2215718
    invoke-virtual {p0}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object p0

    .line 2215719
    invoke-static {v1, p0, v4, v5}, LX/FDs;->a(LX/FDs;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2215720
    :cond_0
    return-object v2
.end method

.method public final M(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 7

    .prologue
    .line 2215721
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2215722
    sget-object v1, Lcom/facebook/messaging/service/model/FetchEventRemindersMembersParams;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchEventRemindersMembersParams;

    .line 2215723
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchEventRemindersMembersParams;->c:LX/0Px;

    move-object v3, v1

    .line 2215724
    if-nez v3, :cond_0

    .line 2215725
    new-instance v0, Lcom/facebook/messaging/service/model/FetchEventRemindersMembersResult;

    .line 2215726
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2215727
    invoke-direct {v0, v1}, Lcom/facebook/messaging/service/model/FetchEventRemindersMembersResult;-><init>(LX/0Px;)V

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2215728
    :goto_0
    return-object v0

    .line 2215729
    :cond_0
    iget-object v0, p0, LX/FDv;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dQ;

    invoke-virtual {v0}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-static {v0, v3}, LX/6ct;->a(Landroid/database/sqlite/SQLiteDatabase;LX/0Px;)Landroid/database/Cursor;

    move-result-object v0

    .line 2215730
    new-instance v2, LX/6dG;

    invoke-direct {v2, v0}, LX/6dG;-><init>(Landroid/database/Cursor;)V

    .line 2215731
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 2215732
    :try_start_0
    invoke-virtual {v2}, LX/6dG;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6dE;

    .line 2215733
    iget-object v1, v0, LX/6dE;->a:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0P2;

    .line 2215734
    if-nez v1, :cond_1

    .line 2215735
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    .line 2215736
    iget-object v6, v0, LX/6dE;->a:Ljava/lang/String;

    invoke-virtual {v4, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2215737
    :cond_1
    iget-object v6, v0, LX/6dE;->b:Lcom/facebook/user/model/UserKey;

    iget-object v0, v0, LX/6dE;->c:Lcom/facebook/graphql/enums/GraphQLLightweightEventGuestStatus;

    invoke-virtual {v1, v6, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 2215738
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/6dG;->a()V

    throw v0

    :cond_2
    invoke-virtual {v2}, LX/6dG;->a()V

    .line 2215739
    new-instance v5, LX/0Pz;

    invoke-direct {v5}, LX/0Pz;-><init>()V

    .line 2215740
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v6

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v6, :cond_4

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    .line 2215741
    iget-object v1, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2215742
    invoke-virtual {v4, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0P2;

    .line 2215743
    if-nez v1, :cond_3

    .line 2215744
    new-instance v1, LX/6ft;

    invoke-direct {v1, v0}, LX/6ft;-><init>(Lcom/facebook/messaging/model/threads/ThreadEventReminder;)V

    .line 2215745
    sget-object v0, LX/0Rg;->a:LX/0Rg;

    move-object v0, v0

    .line 2215746
    iput-object v0, v1, LX/6ft;->f:LX/0P1;

    .line 2215747
    move-object v0, v1

    .line 2215748
    invoke-virtual {v0}, LX/6ft;->i()Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2215749
    :goto_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 2215750
    :cond_3
    new-instance v1, LX/6ft;

    invoke-direct {v1, v0}, LX/6ft;-><init>(Lcom/facebook/messaging/model/threads/ThreadEventReminder;)V

    .line 2215751
    iget-object p0, v0, Lcom/facebook/messaging/model/threads/ThreadEventReminder;->a:Ljava/lang/String;

    move-object v0, p0

    .line 2215752
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0P2;

    invoke-virtual {v0}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 2215753
    iput-object v0, v1, LX/6ft;->f:LX/0P1;

    .line 2215754
    move-object v0, v1

    .line 2215755
    invoke-virtual {v0}, LX/6ft;->i()Lcom/facebook/messaging/model/threads/ThreadEventReminder;

    move-result-object v0

    invoke-virtual {v5, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 2215756
    :cond_4
    new-instance v0, Lcom/facebook/messaging/service/model/FetchEventRemindersMembersResult;

    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/messaging/service/model/FetchEventRemindersMembersResult;-><init>(LX/0Px;)V

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public final O(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    .line 2215757
    iget-object v0, p0, LX/FDv;->c:LX/6cy;

    sget-object v1, LX/6cx;->i:LX/2bA;

    invoke-virtual {v0, v1, v4, v5}, LX/48u;->a(LX/0To;J)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    move v0, v2

    .line 2215758
    :goto_0
    if-nez v0, :cond_0

    .line 2215759
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2215760
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;

    .line 2215761
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    invoke-virtual {v1, v0}, LX/FDs;->c(Lcom/facebook/messaging/service/model/FetchThreadListResult;)V

    .line 2215762
    :cond_0
    iget-object v0, p0, LX/FDv;->b:LX/FDq;

    invoke-virtual {v0}, LX/FDq;->b()LX/0Px;

    move-result-object v0

    .line 2215763
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->newBuilder()LX/6hv;

    move-result-object v1

    .line 2215764
    iput-boolean v2, v1, LX/6hv;->b:Z

    .line 2215765
    move-object v1, v1

    .line 2215766
    iget-object v2, p0, LX/FDv;->g:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 2215767
    iput-wide v2, v1, LX/6hv;->d:J

    .line 2215768
    move-object v1, v1

    .line 2215769
    iput-object v0, v1, LX/6hv;->a:Ljava/util/List;

    .line 2215770
    move-object v0, v1

    .line 2215771
    invoke-virtual {v0}, LX/6hv;->e()Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    move-result-object v0

    .line 2215772
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 2215773
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final P(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    .line 2215774
    iget-object v0, p0, LX/FDv;->c:LX/6cy;

    sget-object v1, LX/6cx;->j:LX/2bA;

    invoke-virtual {v0, v1, v4, v5}, LX/48u;->a(LX/0To;J)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    move v0, v2

    .line 2215775
    :goto_0
    if-nez v0, :cond_0

    .line 2215776
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2215777
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadListResult;

    .line 2215778
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    invoke-virtual {v1, v0}, LX/FDs;->d(Lcom/facebook/messaging/service/model/FetchThreadListResult;)V

    .line 2215779
    :cond_0
    iget-object v0, p0, LX/FDv;->b:LX/FDq;

    invoke-virtual {v0}, LX/FDq;->c()LX/0Px;

    move-result-object v0

    .line 2215780
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->newBuilder()LX/6hv;

    move-result-object v1

    .line 2215781
    iput-boolean v2, v1, LX/6hv;->b:Z

    .line 2215782
    move-object v1, v1

    .line 2215783
    iget-object v2, p0, LX/FDv;->g:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 2215784
    iput-wide v2, v1, LX/6hv;->d:J

    .line 2215785
    move-object v1, v1

    .line 2215786
    iput-object v0, v1, LX/6hv;->a:Ljava/util/List;

    .line 2215787
    move-object v0, v1

    .line 2215788
    invoke-virtual {v0}, LX/6hv;->e()Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    move-result-object v0

    .line 2215789
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 2215790
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Q(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2215791
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2215792
    const-string v1, "updateAgentSuggestionsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/UpdateAgentSuggestionsParams;

    .line 2215793
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    .line 2215794
    iget-object v2, v1, LX/FDs;->d:LX/2N4;

    iget-object v3, v0, Lcom/facebook/messaging/service/model/UpdateAgentSuggestionsParams;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/2N4;->b(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    .line 2215795
    if-nez v2, :cond_1

    .line 2215796
    sget-object v2, LX/FDs;->a:Ljava/lang/Class;

    const-string v3, "handleUpdateAgentSuggestions: message not found"

    invoke-static {v2, v3}, LX/01m;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2215797
    :cond_0
    :goto_0
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2215798
    return-object v0

    .line 2215799
    :cond_1
    iget-object v3, v2, Lcom/facebook/messaging/model/messages/Message;->R:LX/0P1;

    iget-object p0, v0, Lcom/facebook/messaging/service/model/UpdateAgentSuggestionsParams;->c:Ljava/lang/String;

    iget-object p1, v0, Lcom/facebook/messaging/service/model/UpdateAgentSuggestionsParams;->d:Ljava/lang/String;

    iget-object p2, v0, Lcom/facebook/messaging/service/model/UpdateAgentSuggestionsParams;->e:Ljava/lang/String;

    invoke-static {v3, p0, p1, p2}, LX/5dt;->a(LX/0P1;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/0P1;

    move-result-object v3

    .line 2215800
    if-eqz v3, :cond_0

    .line 2215801
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object p0

    invoke-virtual {p0, v2}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/6f7;->b(Ljava/util/Map;)LX/6f7;

    move-result-object v2

    invoke-virtual {v2}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    .line 2215802
    invoke-static {v1, v2}, LX/FDs;->c(LX/FDs;Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;

    goto :goto_0
.end method

.method public final a(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2215803
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v6, v0

    .line 2215804
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 2215805
    const-string v0, "fetchThreadListParams"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;

    .line 2215806
    iget-object v2, p0, LX/FDv;->j:LX/6Po;

    sget-object v3, LX/FDy;->b:LX/6Pr;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "fetchThreadList (DSH)(folder="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2215807
    iget-object v8, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v8, v8

    .line 2215808
    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v3, v7}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 2215809
    const-string v2, "logger_instance_key"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 2215810
    iget-object v1, p0, LX/FDv;->i:LX/2Mq;

    invoke-virtual {v1, v7}, LX/2Mq;->b(I)V

    .line 2215811
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->a:LX/0rS;

    move-object v2, v1

    .line 2215812
    const-string v1, "DbServiceHandler.handleFetchThreadList"

    const v3, 0x7d0ab5cf

    invoke-static {v1, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2215813
    :try_start_0
    iget-object v1, p0, LX/FDv;->b:LX/FDq;

    invoke-virtual {v1, v0}, LX/FDq;->a(Lcom/facebook/messaging/service/model/FetchThreadListParams;)Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v3

    .line 2215814
    iget-object v1, p0, LX/FDv;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2215815
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v1, v1

    .line 2215816
    invoke-virtual {v1}, LX/6ek;->isMessageRequestFolders()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2215817
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v1, v1

    .line 2215818
    invoke-virtual {v1}, LX/6ek;->isSpamOrArchivedFolder()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2215819
    iget-object v1, p0, LX/FDv;->c:LX/6cy;

    sget-object v8, LX/6cx;->k:LX/2bA;

    invoke-virtual {v1, v8}, LX/48u;->a(LX/0To;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    move v1, v4

    .line 2215820
    :goto_0
    invoke-static {}, Lcom/facebook/fbservice/results/DataFetchDisposition;->newBuilder()LX/4B2;

    move-result-object v8

    sget-object v9, LX/4B1;->LOCAL_DISK_CACHE:LX/4B1;

    .line 2215821
    iput-object v9, v8, LX/4B2;->mDataSource:LX/4B1;

    .line 2215822
    move-object v8, v8

    .line 2215823
    sget-object v9, LX/03R;->YES:LX/03R;

    .line 2215824
    iput-object v9, v8, LX/4B2;->mFromAuthoritativeData:LX/03R;

    .line 2215825
    move-object v8, v8

    .line 2215826
    sget-object v9, LX/03R;->NO:LX/03R;

    .line 2215827
    iput-object v9, v8, LX/4B2;->mIsStaleData:LX/03R;

    .line 2215828
    move-object v8, v8

    .line 2215829
    invoke-static {v1}, LX/03R;->valueOf(Z)LX/03R;

    move-result-object v1

    .line 2215830
    iput-object v1, v8, LX/4B2;->mNeedsInitialFetch:LX/03R;

    .line 2215831
    move-object v1, v8

    .line 2215832
    sget-object v8, LX/03R;->NO:LX/03R;

    .line 2215833
    iput-object v8, v1, LX/4B2;->mWasFetchSynchronous:LX/03R;

    .line 2215834
    move-object v1, v1

    .line 2215835
    invoke-virtual {v1}, LX/4B2;->build()Lcom/facebook/fbservice/results/DataFetchDisposition;

    move-result-object v1

    .line 2215836
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListResult;->newBuilder()LX/6iK;

    move-result-object v8

    invoke-virtual {v8, v3}, LX/6iK;->a(Lcom/facebook/messaging/service/model/FetchThreadListResult;)LX/6iK;

    move-result-object v8

    .line 2215837
    iput-object v1, v8, LX/6iK;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2215838
    move-object v1, v8

    .line 2215839
    iget-object v8, p0, LX/FDv;->g:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v8

    .line 2215840
    iput-wide v8, v1, LX/6iK;->j:J

    .line 2215841
    move-object v1, v1

    .line 2215842
    invoke-virtual {v1}, LX/6iK;->m()Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v1

    .line 2215843
    iget-object v8, p0, LX/FDv;->i:LX/2Mq;

    invoke-virtual {v8, v7}, LX/2Mq;->b(I)V

    .line 2215844
    iget-object v8, v3, Lcom/facebook/messaging/service/model/FetchThreadListResult;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    sget-object v9, Lcom/facebook/fbservice/results/DataFetchDisposition;->NO_DATA:Lcom/facebook/fbservice/results/DataFetchDisposition;

    if-ne v8, v9, :cond_0

    invoke-direct {p0, v0}, LX/FDv;->a(Lcom/facebook/messaging/service/model/FetchThreadListParams;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 2215845
    :cond_0
    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 2215846
    const v1, -0x70fb8e41    # -6.530009E-30f

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_1
    return-object v0

    :cond_1
    move v1, v5

    .line 2215847
    goto :goto_0

    .line 2215848
    :cond_2
    :try_start_1
    iget-object v1, v3, Lcom/facebook/messaging/service/model/FetchThreadListResult;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 2215849
    sget-object v10, LX/FDu;->a:[I

    invoke-virtual {v2}, LX/0rS;->ordinal()I

    move-result p1

    aget v10, v10, p1

    packed-switch v10, :pswitch_data_0

    move v8, v9

    .line 2215850
    :cond_3
    :goto_2
    :pswitch_0
    move v1, v8

    .line 2215851
    if-eqz v1, :cond_4

    .line 2215852
    invoke-static {v3}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 2215853
    :goto_3
    const v1, 0x1c283239

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_1

    .line 2215854
    :cond_4
    :try_start_2
    iget-wide v2, v3, Lcom/facebook/messaging/service/model/FetchThreadListResult;->j:J

    .line 2215855
    const-wide/16 v8, 0x0

    cmp-long v1, v2, v8

    if-lez v1, :cond_5

    .line 2215856
    :goto_4
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->newBuilder()LX/6iI;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/6iI;->a(Lcom/facebook/messaging/service/model/FetchThreadListParams;)LX/6iI;

    move-result-object v1

    sget-object v5, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 2215857
    iput-object v5, v1, LX/6iI;->a:LX/0rS;

    .line 2215858
    move-object v1, v1

    .line 2215859
    iget-object v5, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v5, v5

    .line 2215860
    iput-object v5, v1, LX/6iI;->b:LX/6ek;

    .line 2215861
    move-object v1, v1

    .line 2215862
    if-eqz v4, :cond_6

    .line 2215863
    :goto_5
    iput-wide v2, v1, LX/6iI;->e:J

    .line 2215864
    move-object v1, v1

    .line 2215865
    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchThreadListParams;->h:Lcom/facebook/http/interfaces/RequestPriority;

    move-object v2, v2

    .line 2215866
    iput-object v2, v1, LX/6iI;->g:Lcom/facebook/http/interfaces/RequestPriority;

    .line 2215867
    move-object v1, v1

    .line 2215868
    invoke-virtual {v1}, LX/6iI;->i()Lcom/facebook/messaging/service/model/FetchThreadListParams;

    move-result-object v1

    .line 2215869
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 2215870
    const-string v3, "fetchThreadListParams"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2215871
    new-instance v1, LX/1qK;

    invoke-direct {v1, v6, v2}, LX/1qK;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-interface {p2, v1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v3

    .line 2215872
    invoke-virtual {v3}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/service/model/FetchThreadListResult;

    .line 2215873
    const-string v2, "DbServiceHandler.handleFetchThreadList#insertData"

    const v5, 0x7709456a

    invoke-static {v2, v5}, LX/02m;->a(Ljava/lang/String;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2215874
    :try_start_3
    iget-object v2, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FDs;

    invoke-virtual {v2, v1}, LX/FDs;->b(Lcom/facebook/messaging/service/model/FetchThreadListResult;)V

    .line 2215875
    iget-object v2, p0, LX/FDv;->i:LX/2Mq;

    .line 2215876
    iget-object v5, v2, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v6, 0x540012

    const/16 v8, 0x1e

    invoke-interface {v5, v6, v7, v8}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2215877
    const v2, 0x2334c575

    :try_start_4
    invoke-static {v2}, LX/02m;->a(I)V

    .line 2215878
    if-eqz v4, :cond_7

    .line 2215879
    iget-object v2, p0, LX/FDv;->b:LX/FDq;

    invoke-virtual {v2, v0}, LX/FDq;->a(Lcom/facebook/messaging/service/model/FetchThreadListParams;)Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v0

    .line 2215880
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListResult;->newBuilder()LX/6iK;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/6iK;->a(Lcom/facebook/messaging/service/model/FetchThreadListResult;)LX/6iK;

    move-result-object v0

    iget-object v1, v1, Lcom/facebook/messaging/service/model/FetchThreadListResult;->h:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 2215881
    iput-object v1, v0, LX/6iK;->h:Lcom/facebook/messaging/model/threads/NotificationSetting;

    .line 2215882
    move-object v0, v0

    .line 2215883
    invoke-virtual {v0}, LX/6iK;->m()Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v0

    .line 2215884
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto/16 :goto_3

    :cond_5
    move v4, v5

    .line 2215885
    goto :goto_4

    .line 2215886
    :cond_6
    const-wide/16 v2, -0x1

    goto :goto_5

    .line 2215887
    :catchall_0
    move-exception v0

    const v1, -0x3e5c34b4

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2215888
    :catchall_1
    move-exception v0

    const v1, -0x3e1faf3e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    :cond_7
    move-object v0, v3

    goto/16 :goto_3

    .line 2215889
    :pswitch_1
    iget-boolean v8, v1, Lcom/facebook/fbservice/results/DataFetchDisposition;->hasData:Z

    goto/16 :goto_2

    .line 2215890
    :pswitch_2
    iget-boolean v10, v1, Lcom/facebook/fbservice/results/DataFetchDisposition;->hasData:Z

    if-eqz v10, :cond_8

    iget-object v10, v1, Lcom/facebook/fbservice/results/DataFetchDisposition;->isStaleData:LX/03R;

    invoke-virtual {v10, v9}, LX/03R;->asBoolean(Z)Z

    move-result v10

    if-eqz v10, :cond_3

    :cond_8
    move v8, v9

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final b(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 13

    .prologue
    .line 2215378
    iget-object v0, p0, LX/FDv;->j:LX/6Po;

    sget-object v1, LX/FDy;->b:LX/6Pr;

    const-string v2, "fetchMoreThreads (DSH)."

    invoke-virtual {v0, v1, v2}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 2215379
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2215380
    const-string v1, "fetchMoreThreadsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;

    .line 2215381
    iget-object v1, p0, LX/FDv;->b:LX/FDq;

    .line 2215382
    iget-object v3, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->a:LX/6ek;

    move-object v6, v3

    .line 2215383
    iget-object v3, v1, LX/FDq;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6cy;

    invoke-static {v6}, LX/6cx;->a(LX/6ek;)LX/2bA;

    move-result-object v4

    const-wide/16 v7, -0x1

    invoke-virtual {v3, v4, v7, v8}, LX/48u;->a(LX/0To;J)J

    move-result-wide v9

    .line 2215384
    iget-wide v11, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->c:J

    move-wide v3, v11

    .line 2215385
    iget v5, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->e:I

    move v5, v5

    .line 2215386
    invoke-virtual {v1, v6, v3, v4, v5}, LX/FDq;->a(LX/6ek;JI)Ljava/util/LinkedHashMap;

    move-result-object v4

    .line 2215387
    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->size()I

    move-result v3

    .line 2215388
    iget v5, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->e:I

    move v5, v5

    .line 2215389
    if-ge v3, v5, :cond_2

    invoke-static {v1, v6}, LX/FDq;->c(LX/FDq;LX/6ek;)Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v3, 0x1

    .line 2215390
    :goto_0
    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v5

    .line 2215391
    iget v7, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->e:I

    move v7, v7

    .line 2215392
    invoke-static {v5, v7}, LX/FDq;->a(Ljava/util/Collection;I)LX/0Px;

    move-result-object v5

    .line 2215393
    new-instance v7, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-direct {v7, v5, v3}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    .line 2215394
    invoke-static {v4}, LX/FDq;->a(Ljava/util/LinkedHashMap;)Ljava/util/Set;

    move-result-object v3

    .line 2215395
    iget-object v4, v1, LX/FDq;->b:LX/FDp;

    invoke-virtual {v4, v3}, LX/FDp;->a(Ljava/util/Collection;)LX/0Px;

    move-result-object v8

    .line 2215396
    new-instance v4, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;

    sget-object v5, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_LOCAL_DISK_CACHE_UP_TO_DATE:Lcom/facebook/fbservice/results/DataFetchDisposition;

    invoke-direct/range {v4 .. v10}, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;-><init>(Lcom/facebook/fbservice/results/DataFetchDisposition;LX/6ek;Lcom/facebook/messaging/model/threads/ThreadsCollection;LX/0Px;J)V

    move-object v1, v4

    .line 2215397
    iget-object v2, v1, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2215398
    iget-boolean v3, v2, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d:Z

    move v2, v3

    .line 2215399
    if-nez v2, :cond_0

    iget-object v2, v1, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-virtual {v2}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->e()I

    move-result v2

    .line 2215400
    iget v3, v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->e:I

    move v0, v3

    .line 2215401
    if-ne v2, v0, :cond_1

    .line 2215402
    :cond_0
    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2215403
    :goto_1
    return-object v0

    .line 2215404
    :cond_1
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2215405
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;

    .line 2215406
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    invoke-virtual {v1, v0}, LX/FDs;->a(Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;)V

    move-object v0, v2

    goto :goto_1

    .line 2215407
    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public final c(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 13

    .prologue
    .line 2215408
    iget-object v0, p0, LX/FDv;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FDt;

    invoke-virtual {v0}, LX/FDt;->a()V

    .line 2215409
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2215410
    iget-object v1, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v1

    .line 2215411
    const-string v1, "logger_instance_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 2215412
    iget-object v1, p0, LX/FDv;->i:LX/2Mq;

    .line 2215413
    iget-object v2, v1, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x540011

    const/16 v6, 0x17

    invoke-interface {v2, v5, v4, v6}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 2215414
    const-string v2, "db_thread"

    invoke-static {v1, v2}, LX/2Mq;->c(LX/2Mq;Ljava/lang/String;)V

    .line 2215415
    const-string v1, "fetchThreadParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadParams;

    .line 2215416
    iget-object v1, p0, LX/FDv;->j:LX/6Po;

    sget-object v2, LX/FDy;->b:LX/6Pr;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "fetchThread (DSH). "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2215417
    iget-object v6, v0, Lcom/facebook/messaging/service/model/FetchThreadParams;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-object v6, v6

    .line 2215418
    invoke-virtual {v6}, Lcom/facebook/messaging/model/threads/ThreadCriteria;->a()Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v5}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 2215419
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadParams;->b:LX/0rS;

    move-object v5, v1

    .line 2215420
    iget v1, v0, Lcom/facebook/messaging/service/model/FetchThreadParams;->g:I

    move v6, v1

    .line 2215421
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadParams;->a:Lcom/facebook/messaging/model/threads/ThreadCriteria;

    move-object v2, v1

    .line 2215422
    const-string v1, "DbServiceHandler.handleFetchThread"

    const v7, 0x183d5faa

    invoke-static {v1, v7}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2215423
    :try_start_0
    sget-object v1, LX/0SF;->a:LX/0SF;

    move-object v1, v1

    .line 2215424
    invoke-virtual {v1}, LX/0SF;->a()J

    move-result-wide v8

    .line 2215425
    iget-object v1, p0, LX/FDv;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2N4;

    invoke-virtual {v1, v2, v6}, LX/2N4;->a(Lcom/facebook/messaging/model/threads/ThreadCriteria;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v7

    .line 2215426
    sget-object v1, LX/0SF;->a:LX/0SF;

    move-object v1, v1

    .line 2215427
    invoke-virtual {v1}, LX/0SF;->a()J

    move-result-wide v10

    sub-long v8, v10, v8

    .line 2215428
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 2215429
    const-string v1, "fetch_location"

    sget-object v2, LX/6bf;->THREAD_DB:LX/6bf;

    invoke-virtual {v2}, LX/6bf;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v10, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2215430
    const-string v1, "thread_db_duration"

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v10, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2215431
    iput-object v10, v7, Lcom/facebook/messaging/service/model/FetchThreadResult;->i:Ljava/util/Map;

    .line 2215432
    const/4 v1, 0x0

    .line 2215433
    iget-object v2, v7, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v2, :cond_8

    .line 2215434
    iget-object v1, v7, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-boolean v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->J:Z

    move v2, v1

    .line 2215435
    :goto_0
    iget-object v1, p0, LX/FDv;->f:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v7, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v1, :cond_0

    iget-object v1, v7, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-virtual {v1, v6}, Lcom/facebook/messaging/model/messages/MessagesCollection;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 2215436
    invoke-static {}, Lcom/facebook/fbservice/results/DataFetchDisposition;->newBuilder()LX/4B2;

    move-result-object v0

    sget-object v1, LX/4B1;->LOCAL_DISK_CACHE:LX/4B1;

    .line 2215437
    iput-object v1, v0, LX/4B2;->mDataSource:LX/4B1;

    .line 2215438
    move-object v0, v0

    .line 2215439
    sget-object v1, LX/03R;->YES:LX/03R;

    .line 2215440
    iput-object v1, v0, LX/4B2;->mFromAuthoritativeData:LX/03R;

    .line 2215441
    move-object v0, v0

    .line 2215442
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 2215443
    iput-object v1, v0, LX/4B2;->mIsStaleData:LX/03R;

    .line 2215444
    move-object v0, v0

    .line 2215445
    sget-object v1, LX/03R;->NO:LX/03R;

    .line 2215446
    iput-object v1, v0, LX/4B2;->mWasFetchSynchronous:LX/03R;

    .line 2215447
    move-object v0, v0

    .line 2215448
    invoke-virtual {v0}, LX/4B2;->build()Lcom/facebook/fbservice/results/DataFetchDisposition;

    move-result-object v0

    .line 2215449
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadResult;->a()LX/6iO;

    move-result-object v1

    .line 2215450
    iput-object v0, v1, LX/6iO;->b:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2215451
    move-object v0, v1

    .line 2215452
    iget-object v1, v7, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2215453
    iput-object v1, v0, LX/6iO;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2215454
    move-object v0, v0

    .line 2215455
    iget-object v1, v7, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2215456
    iput-object v1, v0, LX/6iO;->d:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2215457
    move-object v0, v0

    .line 2215458
    iput-object v10, v0, LX/6iO;->f:Ljava/util/Map;

    .line 2215459
    move-object v0, v0

    .line 2215460
    iget-object v1, v7, Lcom/facebook/messaging/service/model/FetchThreadResult;->f:LX/0Px;

    .line 2215461
    iput-object v1, v0, LX/6iO;->e:LX/0Px;

    .line 2215462
    move-object v0, v0

    .line 2215463
    iget-object v1, p0, LX/FDv;->g:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, LX/6iO;->a(J)LX/6iO;

    move-result-object v0

    invoke-virtual {v0}, LX/6iO;->a()Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2215464
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2215465
    const v1, -0x7e6b2604

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_1
    return-object v0

    .line 2215466
    :cond_0
    :try_start_1
    iget-object v1, v7, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v1, :cond_1

    iget-object v1, v7, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v1, v1, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->g(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2215467
    sget-object v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->a:Lcom/facebook/messaging/service/model/FetchThreadResult;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2215468
    const v1, 0x629f21de

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_1

    .line 2215469
    :cond_1
    :try_start_2
    sget-object v1, LX/0rS;->DO_NOT_CHECK_SERVER:LX/0rS;

    if-ne v5, v1, :cond_2

    .line 2215470
    invoke-static {v7}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 2215471
    :goto_2
    const v1, -0x15354a63

    invoke-static {v1}, LX/02m;->a(I)V

    goto :goto_1

    .line 2215472
    :cond_2
    :try_start_3
    invoke-direct {p0, v0, v3, v7, p2}, LX/FDv;->a(Lcom/facebook/messaging/service/model/FetchThreadParams;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/messaging/service/model/FetchThreadResult;LX/1qM;)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2215473
    if-eqz v0, :cond_4

    .line 2215474
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    move-object v12, v0

    move-object v0, v1

    move-object v1, v12

    .line 2215475
    :goto_3
    iget-object v2, v1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v2, :cond_3

    iget-object v2, v1, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    if-eqz v2, :cond_3

    iget-object v2, v1, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->A:LX/6ek;

    sget-object v3, LX/6ek;->INBOX:LX/6ek;

    if-eq v2, v3, :cond_9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2215476
    :cond_3
    :goto_4
    goto :goto_2

    .line 2215477
    :catchall_0
    move-exception v0

    const v1, 0xc88b57e

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2215478
    :cond_4
    :try_start_4
    iget-object v0, p0, LX/FDv;->s:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6bg;

    sget-object v1, LX/6iC;->NOT_MOSTLY_CACHED:LX/6iC;

    invoke-static {v1}, Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;->a(LX/6iC;)Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/6bg;->a(Lcom/facebook/messaging/service/model/FetchThreadHandlerChange;)V

    .line 2215479
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2215480
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2215481
    iget-object v1, p0, LX/FDv;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6dQ;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 2215482
    const v1, -0xf47eb0f

    invoke-static {v2, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 2215483
    :try_start_5
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v1, :cond_5

    .line 2215484
    iget-object v1, v7, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    invoke-static {v1}, LX/FDv;->a(Lcom/facebook/messaging/model/messages/MessagesCollection;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 2215485
    invoke-static {p0, v1, v0}, LX/FDv;->a(LX/FDv;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/service/model/FetchThreadResult;)V

    .line 2215486
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    invoke-virtual {v1, v7, v0}, LX/FDs;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;Lcom/facebook/messaging/service/model/FetchThreadResult;)V

    .line 2215487
    :cond_5
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 2215488
    const v1, -0x105fbe5e

    :try_start_6
    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2215489
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->i:Ljava/util/Map;

    .line 2215490
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2215491
    if-eqz v1, :cond_6

    .line 2215492
    invoke-interface {v2, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 2215493
    :cond_6
    const-string v1, "thread_db_duration"

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2215494
    iput-object v2, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->i:Ljava/util/Map;

    .line 2215495
    iget-object v1, p0, LX/FDv;->i:LX/2Mq;

    .line 2215496
    iget-object v3, v1, LX/2Mq;->e:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v5, 0x540011

    const/16 v7, 0x1e

    invoke-interface {v3, v5, v4, v7}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(IIS)V

    .line 2215497
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v1, :cond_7

    .line 2215498
    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v1, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2215499
    iget-object v0, p0, LX/FDv;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2N4;

    invoke-virtual {v0, v1, v6}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2215500
    invoke-static {v0}, Lcom/facebook/messaging/service/model/FetchThreadResult;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;)LX/6iO;

    move-result-object v0

    .line 2215501
    iput-object v2, v0, LX/6iO;->f:Ljava/util/Map;

    .line 2215502
    move-object v0, v0

    .line 2215503
    sget-object v1, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_SERVER:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2215504
    iput-object v1, v0, LX/6iO;->b:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2215505
    move-object v0, v0

    .line 2215506
    invoke-virtual {v0}, LX/6iO;->a()Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2215507
    :goto_5
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    move-object v12, v0

    move-object v0, v1

    move-object v1, v12

    goto/16 :goto_3

    .line 2215508
    :catchall_1
    move-exception v0

    const v1, 0x2af73a74

    invoke-static {v2, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0

    .line 2215509
    :cond_7
    invoke-static {v0}, Lcom/facebook/messaging/service/model/FetchThreadResult;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;)LX/6iO;

    move-result-object v0

    .line 2215510
    iput-object v2, v0, LX/6iO;->f:Ljava/util/Map;

    .line 2215511
    move-object v0, v0

    .line 2215512
    sget-object v1, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_SERVER:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2215513
    iput-object v1, v0, LX/6iO;->b:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2215514
    move-object v0, v0

    .line 2215515
    invoke-virtual {v0}, LX/6iO;->a()Lcom/facebook/messaging/service/model/FetchThreadResult;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v0

    goto :goto_5

    :cond_8
    move v2, v1

    goto/16 :goto_0

    .line 2215516
    :cond_9
    iget-object v2, p0, LX/FDv;->r:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/media/download/MediaDownloadManager;

    iget-object v3, v1, Lcom/facebook/messaging/service/model/FetchThreadResult;->e:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2215517
    iget-object p0, v3, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v3, p0

    .line 2215518
    invoke-virtual {v2, v3}, Lcom/facebook/messaging/media/download/MediaDownloadManager;->a(LX/0Px;)Z

    goto/16 :goto_4
.end method

.method public final d(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2215095
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2215096
    const-string v1, "fetch_thread_with_participants_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;

    .line 2215097
    iget-object v1, p0, LX/FDv;->b:LX/FDq;

    .line 2215098
    iget-object v2, v0, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;->b:LX/0Rf;

    move-object v2, v2

    .line 2215099
    invoke-virtual {v1, v2}, LX/FDq;->a(LX/0Rf;)LX/0Px;

    move-result-object v1

    .line 2215100
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2215101
    invoke-static {v0, v2}, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;->b(Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsParams;Lcom/facebook/messaging/model/threads/ThreadSummary;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2215102
    :goto_0
    move-object v0, v2

    .line 2215103
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 2215104
    :goto_1
    new-instance v1, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsResult;

    invoke-direct {v1, v0}, Lcom/facebook/messaging/service/model/FetchThreadKeyByParticipantsResult;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    invoke-static {v1}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    .line 2215105
    :cond_1
    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public final e(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2215106
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2215107
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2215108
    if-eqz v0, :cond_0

    .line 2215109
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    invoke-virtual {v1, v0}, LX/FDs;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;)V

    .line 2215110
    :cond_0
    return-object v2
.end method

.method public final f(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2215111
    iget-object v0, p0, LX/FDv;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FDt;

    invoke-virtual {v0}, LX/FDt;->a()V

    .line 2215112
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2215113
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2215114
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    invoke-virtual {v1, v0}, LX/FDs;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;)V

    .line 2215115
    return-object v2
.end method

.method public final g(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 13

    .prologue
    .line 2215116
    :try_start_0
    iget-object v0, p0, LX/FDv;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FDt;

    invoke-virtual {v0}, LX/FDt;->a()V

    .line 2215117
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2215118
    const-string v1, "createThreadParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;

    .line 2215119
    iget-object v1, v0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->b:Lcom/facebook/messaging/model/messages/Message;

    move-object v1, v1

    .line 2215120
    iget-boolean v2, v0, Lcom/facebook/messaging/service/model/SendMessageByRecipientsParams;->d:Z

    move v0, v2

    .line 2215121
    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    iget-object v0, v1, Lcom/facebook/messaging/model/messages/Message;->s:Lcom/facebook/messaging/model/messages/Publicity;

    sget-object v2, Lcom/facebook/messaging/model/messages/Publicity;->b:Lcom/facebook/messaging/model/messages/Publicity;

    if-ne v0, v2, :cond_3

    .line 2215122
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2215123
    if-eqz v1, :cond_0

    iget-object v5, v1, Lcom/facebook/messaging/model/messages/Message;->m:LX/0Px;

    if-eqz v5, :cond_0

    iget-object v5, v1, Lcom/facebook/messaging/model/messages/Message;->m:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v5

    if-le v5, v6, :cond_0

    move v5, v6

    :goto_0
    invoke-static {v5}, LX/0PB;->checkArgument(Z)V

    .line 2215124
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2215125
    iget-object v9, v1, Lcom/facebook/messaging/model/messages/Message;->m:LX/0Px;

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    :goto_1
    if-ge v7, v10, :cond_1

    invoke-virtual {v9, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2215126
    new-instance v11, LX/6fz;

    invoke-direct {v11}, LX/6fz;-><init>()V

    .line 2215127
    iput-object v5, v11, LX/6fz;->a:Lcom/facebook/messaging/model/messages/ParticipantInfo;

    .line 2215128
    move-object v5, v11

    .line 2215129
    invoke-virtual {v5}, LX/6fz;->g()Lcom/facebook/messaging/model/threads/ThreadParticipant;

    move-result-object v5

    invoke-interface {v8, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2215130
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_0
    move v5, v7

    .line 2215131
    goto :goto_0

    .line 2215132
    :cond_1
    invoke-static {}, Lcom/facebook/messaging/model/threads/ThreadSummary;->newBuilder()LX/6g6;

    move-result-object v5

    sget-object v7, LX/6ek;->INBOX:LX/6ek;

    .line 2215133
    iput-object v7, v5, LX/6g6;->A:LX/6ek;

    .line 2215134
    move-object v5, v5

    .line 2215135
    iget-object v7, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2215136
    iput-object v7, v5, LX/6g6;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2215137
    move-object v5, v5

    .line 2215138
    iput-object v8, v5, LX/6g6;->h:Ljava/util/List;

    .line 2215139
    move-object v5, v5

    .line 2215140
    iput-boolean v6, v5, LX/6g6;->u:Z

    .line 2215141
    move-object v5, v5

    .line 2215142
    invoke-virtual {v5}, LX/6g6;->Y()Lcom/facebook/messaging/model/threads/ThreadSummary;

    move-result-object v5

    .line 2215143
    new-instance v7, Lcom/facebook/messaging/model/messages/MessagesCollection;

    iget-object v8, v1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v9

    invoke-direct {v7, v8, v9, v6}, Lcom/facebook/messaging/model/messages/MessagesCollection;-><init>(Lcom/facebook/messaging/model/threadkey/ThreadKey;LX/0Px;Z)V

    .line 2215144
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadResult;->a()LX/6iO;

    move-result-object v6

    sget-object v8, Lcom/facebook/fbservice/results/DataFetchDisposition;->NO_DATA:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2215145
    iput-object v8, v6, LX/6iO;->b:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2215146
    move-object v6, v6

    .line 2215147
    iget-object v8, p0, LX/FDv;->g:LX/0SG;

    invoke-interface {v8}, LX/0SG;->a()J

    move-result-wide v9

    .line 2215148
    iput-wide v9, v6, LX/6iO;->g:J

    .line 2215149
    move-object v6, v6

    .line 2215150
    iput-object v5, v6, LX/6iO;->c:Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2215151
    move-object v5, v6

    .line 2215152
    iput-object v7, v5, LX/6iO;->d:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2215153
    move-object v5, v5

    .line 2215154
    invoke-virtual {v5}, LX/6iO;->a()Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v5

    .line 2215155
    invoke-static {v5}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v5

    move-object v0, v5

    .line 2215156
    move-object v2, v0

    .line 2215157
    :goto_2
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2215158
    if-eqz v0, :cond_2

    .line 2215159
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    invoke-virtual {v1, v0}, LX/FDs;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;)V

    .line 2215160
    :cond_2
    return-object v2

    .line 2215161
    :cond_3
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catch LX/FKG; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v2, v0

    goto :goto_2

    .line 2215162
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2215163
    iget-object v2, v1, LX/FKG;->failedMessage:Lcom/facebook/messaging/model/messages/Message;

    .line 2215164
    iget-object v0, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v0, :cond_5

    iget-object v0, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v0, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v3, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v0, v3, :cond_5

    .line 2215165
    iget-object v0, p0, LX/FDv;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2N4;

    iget-object v3, v2, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;I)Lcom/facebook/messaging/service/model/FetchThreadResult;

    move-result-object v0

    .line 2215166
    if-eqz v0, :cond_5

    iget-object v3, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    if-eqz v3, :cond_5

    iget-object v3, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v3, v3, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v3, :cond_5

    .line 2215167
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v2

    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchThreadResult;->d:Lcom/facebook/messaging/model/threads/ThreadSummary;

    iget-object v0, v0, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2215168
    iput-object v0, v2, LX/6f7;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2215169
    move-object v0, v2

    .line 2215170
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v2

    .line 2215171
    iget-object v0, v2, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iget-object v0, v0, Lcom/facebook/messaging/model/send/SendError;->b:LX/6fP;

    iget-boolean v0, v0, LX/6fP;->shouldNotBeRetried:Z

    if-eqz v0, :cond_4

    .line 2215172
    iget-object v0, p0, LX/FDv;->h:LX/2Ow;

    invoke-virtual {v0, v2}, LX/2Ow;->c(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2215173
    :goto_3
    iget-object v0, p0, LX/FDv;->m:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FDt;

    invoke-virtual {v0, v2}, LX/FDt;->c(Lcom/facebook/messaging/model/messages/Message;)V

    .line 2215174
    new-instance v0, LX/FKG;

    invoke-direct {v0, v1, v2}, LX/FKG;-><init>(Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;)V

    throw v0

    .line 2215175
    :cond_4
    iget-object v0, p0, LX/FDv;->h:LX/2Ow;

    invoke-virtual {v0, v2}, LX/2Ow;->b(Lcom/facebook/messaging/model/messages/Message;)V

    goto :goto_3

    .line 2215176
    :cond_5
    throw v1
.end method

.method public final h(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 9

    .prologue
    .line 2215177
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2215178
    const-string v1, "fetchMoreMessagesParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;

    .line 2215179
    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v1, v1

    .line 2215180
    iget-object v2, p0, LX/FDv;->j:LX/6Po;

    sget-object v3, LX/FDy;->b:LX/6Pr;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "fetchMoreMessages (DSH). "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/6Po;->a(LX/6Pr;Ljava/lang/String;)V

    .line 2215181
    iget-wide v7, v0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->c:J

    move-wide v4, v7

    .line 2215182
    iget v2, v0, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->d:I

    move v6, v2

    .line 2215183
    const-string v0, "DbServiceHandler.handleFetchThread"

    const v2, -0x4b3e6c02

    invoke-static {v0, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2215184
    :try_start_0
    iget-object v0, p0, LX/FDv;->n:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2N4;

    const-wide/16 v2, 0x0

    invoke-virtual/range {v0 .. v6}, LX/2N4;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;JJI)Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;

    move-result-object v3

    .line 2215185
    iget-object v0, v3, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;->c:Lcom/facebook/messaging/model/messages/MessagesCollection;

    .line 2215186
    sget-object v1, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;->a:Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;

    if-eq v3, v1, :cond_1

    .line 2215187
    iget-object v1, v0, Lcom/facebook/messaging/model/messages/MessagesCollection;->b:LX/0Px;

    move-object v1, v1

    .line 2215188
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-eq v1, v6, :cond_0

    .line 2215189
    iget-boolean v1, v0, Lcom/facebook/messaging/model/messages/MessagesCollection;->c:Z

    move v0, v1

    .line 2215190
    if-eqz v0, :cond_1

    .line 2215191
    :cond_0
    invoke-static {v3}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2215192
    const v1, 0x6216bc8

    invoke-static {v1}, LX/02m;->a(I)V

    :goto_0
    return-object v0

    .line 2215193
    :cond_1
    :try_start_1
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2215194
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;

    .line 2215195
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    invoke-virtual {v1, v3, v0}, LX/FDs;->a(Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2215196
    const v0, -0x3ff83681

    invoke-static {v0}, LX/02m;->a(I)V

    move-object v0, v2

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x51c46106

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final i(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 2215197
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2215198
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    .line 2215199
    if-eqz v0, :cond_0

    .line 2215200
    iget-object v1, p0, LX/FDv;->d:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6dQ;

    invoke-virtual {v1}, LX/0Tr;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 2215201
    const v1, 0xa680705

    invoke-static {v3, v1}, LX/03h;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2215202
    :try_start_0
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    iget-object v4, p0, LX/FDv;->g:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v1, v0, v4, v5}, LX/FDs;->a(Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;J)V

    .line 2215203
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2215204
    const v0, 0x3bcfdb18

    invoke-static {v3, v0}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 2215205
    :cond_0
    return-object v2

    .line 2215206
    :catchall_0
    move-exception v0

    const v1, 0x36320f90

    invoke-static {v3, v1}, LX/03h;->b(Landroid/database/sqlite/SQLiteDatabase;I)V

    throw v0
.end method

.method public final j(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 2215207
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2215208
    const-string v1, "markThreadsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/MarkThreadsParams;

    .line 2215209
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    invoke-virtual {v1, v0}, LX/FDs;->a(Lcom/facebook/messaging/service/model/MarkThreadsParams;)V

    .line 2215210
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final l(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2215211
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2215212
    const-string v1, "deleteThreadsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/DeleteThreadsParams;

    .line 2215213
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2215214
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    .line 2215215
    iget-object p0, v0, Lcom/facebook/messaging/service/model/DeleteThreadsParams;->a:LX/0Px;

    move-object v0, p0

    .line 2215216
    invoke-static {v1, v0}, LX/FDs;->b(LX/FDs;LX/0Px;)V

    .line 2215217
    return-object v2
.end method

.method public final n(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 5

    .prologue
    .line 2215218
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2215219
    const-string v1, "deleteMessagesParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/DeleteMessagesParams;

    .line 2215220
    invoke-static {}, LX/0Rf;->builder()LX/0cA;

    move-result-object v2

    .line 2215221
    iget-object v1, v0, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->c:LX/6hi;

    sget-object v3, LX/6hi;->MUST_UPDATE_SERVER:LX/6hi;

    if-ne v1, v3, :cond_1

    .line 2215222
    iget-object v1, v0, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->b:LX/0Rf;

    invoke-virtual {v1}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2215223
    const-string v4, "mid."

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    move v4, v4

    .line 2215224
    if-eqz v4, :cond_0

    .line 2215225
    invoke-virtual {v2, v1}, LX/0cA;->c(Ljava/lang/Object;)LX/0cA;

    goto :goto_0

    .line 2215226
    :cond_1
    invoke-virtual {v2}, LX/0cA;->b()LX/0Rf;

    move-result-object v1

    .line 2215227
    invoke-virtual {v1}, LX/0Rf;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2215228
    new-instance v2, Lcom/facebook/messaging/service/model/DeleteMessagesParams;

    sget-object v3, LX/6hi;->MUST_UPDATE_SERVER:LX/6hi;

    iget-object v4, v0, Lcom/facebook/messaging/service/model/DeleteMessagesParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-direct {v2, v1, v3, v4}, Lcom/facebook/messaging/service/model/DeleteMessagesParams;-><init>(LX/0Rf;LX/6hi;Lcom/facebook/messaging/model/threadkey/ThreadKey;)V

    .line 2215229
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2215230
    const-string v3, "deleteMessagesParams"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2215231
    new-instance v2, LX/1qK;

    const-string v3, "delete_messages"

    invoke-direct {v2, v3, v1}, LX/1qK;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2215232
    invoke-interface {p2, v2}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    .line 2215233
    :cond_2
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    const-wide/16 v2, -0x1

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, LX/FDs;->a(Lcom/facebook/messaging/service/model/DeleteMessagesParams;JZ)Lcom/facebook/messaging/service/model/DeleteMessagesResult;

    move-result-object v0

    .line 2215234
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final o(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 2215235
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2215236
    invoke-virtual {v0}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchThreadResult;

    .line 2215237
    if-eqz v0, :cond_0

    .line 2215238
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    invoke-virtual {v1, v0}, LX/FDs;->a(Lcom/facebook/messaging/service/model/FetchThreadResult;)V

    .line 2215239
    :cond_0
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0
.end method

.method public final p(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 12

    .prologue
    .line 2215240
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2215241
    const-string v1, "folderName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/6ek;->fromDbName(Ljava/lang/String;)LX/6ek;

    move-result-object v1

    .line 2215242
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2215243
    if-eqz v0, :cond_0

    .line 2215244
    iget-boolean v2, v0, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v2, v2

    .line 2215245
    if-nez v2, :cond_1

    .line 2215246
    :cond_0
    :goto_0
    return-object v0

    .line 2215247
    :cond_1
    iget-object v0, p0, LX/FDv;->g:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v2

    .line 2215248
    iget-object v0, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FDs;

    const/4 v7, 0x0

    .line 2215249
    iget-object v4, v0, LX/FDs;->e:LX/FDq;

    invoke-virtual {v4, v1}, LX/FDq;->b(LX/6ek;)Lcom/facebook/messaging/model/folders/FolderCounts;

    move-result-object v4

    .line 2215250
    if-nez v4, :cond_2

    .line 2215251
    :goto_1
    new-instance v0, Lcom/facebook/messaging/service/model/MarkFolderSeenResult;

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/messaging/service/model/MarkFolderSeenResult;-><init>(LX/6ek;J)V

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2215252
    :cond_2
    new-instance v5, Lcom/facebook/messaging/model/folders/FolderCounts;

    iget v6, v4, Lcom/facebook/messaging/model/folders/FolderCounts;->b:I

    iget-wide v10, v4, Lcom/facebook/messaging/model/folders/FolderCounts;->e:J

    move-wide v8, v2

    invoke-direct/range {v5 .. v11}, Lcom/facebook/messaging/model/folders/FolderCounts;-><init>(IIJJ)V

    invoke-static {v0, v1, v5}, LX/FDs;->a(LX/FDs;LX/6ek;Lcom/facebook/messaging/model/folders/FolderCounts;)V

    .line 2215253
    goto :goto_1
.end method

.method public final q(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 3

    .prologue
    .line 2215254
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2215255
    const-string v1, "saveDraftParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SaveDraftParams;

    .line 2215256
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    iget-object v2, v0, Lcom/facebook/messaging/service/model/SaveDraftParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v0, v0, Lcom/facebook/messaging/service/model/SaveDraftParams;->b:Lcom/facebook/messaging/model/messages/MessageDraft;

    invoke-virtual {v1, v2, v0}, LX/FDs;->a(Lcom/facebook/messaging/model/threadkey/ThreadKey;Lcom/facebook/messaging/model/messages/MessageDraft;)V

    .line 2215257
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2215258
    return-object v0
.end method

.method public final r(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    const-wide/16 v8, -0x1

    .line 2215259
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 2215260
    const-string v0, "message"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/Message;

    .line 2215261
    sget-object v2, LX/6ek;->INBOX:LX/6ek;

    .line 2215262
    const-string v3, "prevLastVisibleActionId"

    invoke-virtual {v1, v3, v8, v9}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 2215263
    const/4 v3, 0x0

    .line 2215264
    iget-object v1, p0, LX/FDv;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2N4;

    invoke-virtual {v1, v6, v7}, LX/2N4;->a(J)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v3

    .line 2215265
    :goto_0
    move v1, v1

    .line 2215266
    if-eqz v1, :cond_0

    .line 2215267
    invoke-static {v2}, LX/6cx;->c(LX/6ek;)LX/2bA;

    move-result-object v1

    .line 2215268
    iget-object v2, p0, LX/FDv;->c:LX/6cy;

    invoke-virtual {v2, v1, v5}, LX/48u;->b(LX/0To;Z)V

    .line 2215269
    :cond_0
    cmp-long v1, v6, v8

    if-eqz v1, :cond_1

    iget-object v1, p0, LX/FDv;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2N4;

    invoke-virtual {v1, v6, v7}, LX/2N4;->a(J)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2215270
    :cond_1
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v0

    .line 2215271
    iput-boolean v5, v0, LX/6f7;->o:Z

    .line 2215272
    move-object v0, v0

    .line 2215273
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v3

    .line 2215274
    :goto_1
    new-instance v1, Lcom/facebook/messaging/service/model/NewMessageResult;

    sget-object v2, LX/0ta;->FROM_SERVER:LX/0ta;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    move-object v5, v4

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/NewMessageResult;-><init>(LX/0ta;Lcom/facebook/messaging/model/messages/Message;Lcom/facebook/messaging/model/messages/MessagesCollection;Lcom/facebook/messaging/model/threads/ThreadSummary;J)V

    .line 2215275
    iget-object v0, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FDs;

    invoke-virtual {v0, v1, v8, v9}, LX/FDs;->a(Lcom/facebook/messaging/service/model/NewMessageResult;J)Lcom/facebook/messaging/service/model/NewMessageResult;

    move-result-object v0

    .line 2215276
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    return-object v0

    :cond_2
    move-object v3, v0

    goto :goto_1

    .line 2215277
    :cond_3
    iget-object v1, p0, LX/FDv;->n:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2N4;

    iget-object p1, v0, Lcom/facebook/messaging/model/messages/Message;->a:Ljava/lang/String;

    invoke-virtual {v1, p1}, LX/2N4;->b(Ljava/lang/String;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v1

    .line 2215278
    if-eqz v1, :cond_4

    iget-boolean v1, v1, Lcom/facebook/messaging/model/messages/Message;->o:Z

    if-eqz v1, :cond_5

    :cond_4
    const/4 v1, 0x1

    goto :goto_0

    :cond_5
    move v1, v3

    goto :goto_0
.end method

.method public final s(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 2215279
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v1

    .line 2215280
    iget-boolean v0, v1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2215281
    if-eqz v0, :cond_0

    .line 2215282
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2215283
    const-string v2, "setSettingsParams"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SetSettingsParams;

    .line 2215284
    iget-object v2, v0, Lcom/facebook/messaging/service/model/SetSettingsParams;->b:Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-object v2, v2

    .line 2215285
    if-eqz v2, :cond_0

    .line 2215286
    iget-object v2, p0, LX/FDv;->k:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/0db;->aD:LX/0Tn;

    .line 2215287
    iget-object v4, v0, Lcom/facebook/messaging/service/model/SetSettingsParams;->b:Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-object v0, v4

    .line 2215288
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/NotificationSetting;->a()J

    move-result-wide v4

    invoke-interface {v2, v3, v4, v5}, LX/0hN;->a(LX/0Tn;J)LX/0hN;

    move-result-object v0

    invoke-interface {v0}, LX/0hN;->commit()V

    .line 2215289
    :cond_0
    return-object v1
.end method

.method public final t(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    const-wide/16 v2, 0x0

    .line 2215290
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2215291
    const-string v1, "searchThreadNameAndParticipantsParam"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;

    .line 2215292
    iget-boolean v1, v0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsParams;->c:Z

    move v0, v1

    .line 2215293
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2215294
    iget-object v0, p0, LX/FDv;->c:LX/6cy;

    sget-object v1, LX/6cx;->d:LX/2bA;

    invoke-virtual {v0, v1, v2, v3}, LX/48u;->a(LX/0To;J)J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 2215295
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2215296
    :goto_0
    return-object v0

    .line 2215297
    :cond_0
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2215298
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;

    .line 2215299
    const-string v1, "DbServiceHandler.handleFetchSuggestedGroups#insertData"

    const v3, 0x4f9f26a2

    invoke-static {v1, v3}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2215300
    :try_start_0
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    iget-object v3, p0, LX/FDv;->g:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    .line 2215301
    iget-object v3, v1, LX/FDs;->c:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/6cy;

    sget-object p1, LX/6cx;->d:LX/2bA;

    invoke-virtual {v3, p1, v4, v5}, LX/48u;->b(LX/0To;J)V

    .line 2215302
    iget-object v3, v0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;->a:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-object v3, v3

    .line 2215303
    iget-object p1, v3, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v3, p1

    .line 2215304
    invoke-static {v1, v3, v4, v5}, LX/FDs;->a(LX/FDs;LX/0Px;J)V

    .line 2215305
    iget-object v3, v1, LX/FDs;->f:LX/3N0;

    .line 2215306
    iget-object p1, v0, Lcom/facebook/messaging/service/model/SearchThreadNameAndParticipantsResult;->b:LX/0Px;

    move-object p1, p1

    .line 2215307
    invoke-virtual {v3, p1}, LX/3N0;->a(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2215308
    const v0, -0xa32a67b

    invoke-static {v0}, LX/02m;->a(I)V

    .line 2215309
    iget-object v0, p0, LX/FDv;->h:LX/2Ow;

    .line 2215310
    new-instance v1, Landroid/content/Intent;

    sget-object v3, LX/0aY;->u:Ljava/lang/String;

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2215311
    invoke-static {v0, v1}, LX/2Ow;->a(LX/2Ow;Landroid/content/Intent;)V

    .line 2215312
    invoke-static {v2}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    goto :goto_0

    .line 2215313
    :catchall_0
    move-exception v0

    const v1, 0x5eaa8ba2

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method

.method public final u(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 2215314
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v1, v0

    .line 2215315
    const-string v0, "fetchPinnedThreadsParams"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;

    .line 2215316
    iget-object v4, p0, LX/FDv;->b:LX/FDq;

    invoke-virtual {v4}, LX/FDq;->a()Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    move-result-object v4

    .line 2215317
    iget-object v5, v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->b:LX/0rS;

    sget-object v6, LX/0rS;->STALE_DATA_OKAY:LX/0rS;

    if-ne v5, v6, :cond_0

    iget-wide v6, v4, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->b:J

    cmp-long v5, v6, v2

    if-lez v5, :cond_0

    .line 2215318
    invoke-static {v4}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    .line 2215319
    :goto_0
    return-object v0

    .line 2215320
    :cond_0
    iget-object v5, v4, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->c:LX/0Px;

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2215321
    :goto_1
    new-instance v4, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;

    iget-object v0, v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->b:LX/0rS;

    invoke-direct {v4, v0, v2, v3}, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;-><init>(LX/0rS;J)V

    .line 2215322
    const-string v0, "fetchPinnedThreadsParams"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2215323
    new-instance v0, LX/1qK;

    .line 2215324
    iget-object v2, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v2, v2

    .line 2215325
    invoke-direct {v0, v2, v1}, LX/1qK;-><init>(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-interface {p2, v0}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2215326
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    .line 2215327
    iget-boolean v1, v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->d:Z

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->c:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_2
    move v1, v1

    .line 2215328
    if-nez v1, :cond_1

    .line 2215329
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    iget-object v3, p0, LX/FDv;->g:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v1, v0, v4, v5}, LX/FDs;->a(Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;J)V

    :cond_1
    move-object v0, v2

    .line 2215330
    goto :goto_0

    .line 2215331
    :cond_2
    iget-wide v2, v4, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->e:J

    goto :goto_1

    :cond_3
    const/4 v1, 0x0

    goto :goto_2
.end method

.method public final v(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 6

    .prologue
    .line 2215332
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2215333
    const-string v1, "updatePinnedThreadsParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/UpdatePinnedThreadsParams;

    .line 2215334
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    .line 2215335
    iget-object v2, v0, Lcom/facebook/messaging/service/model/UpdatePinnedThreadsParams;->a:LX/0Px;

    invoke-static {v1, v2}, LX/FDs;->a(LX/FDs;Ljava/util/List;)V

    .line 2215336
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2215337
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    .line 2215338
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    iget-object v3, p0, LX/FDv;->g:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v1, v0, v4, v5}, LX/FDs;->a(Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;J)V

    .line 2215339
    return-object v2
.end method

.method public final w(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 10

    .prologue
    .line 2215340
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2215341
    const-string v1, "addPinnedThreadParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/AddPinnedThreadParams;

    .line 2215342
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    const/4 v2, 0x0

    .line 2215343
    iget-object v3, v1, LX/FDs;->e:LX/FDq;

    invoke-virtual {v3}, LX/FDq;->a()Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    move-result-object v3

    .line 2215344
    iget-object v4, v3, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->c:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, LX/0R9;->a(I)Ljava/util/ArrayList;

    move-result-object v5

    .line 2215345
    iget-object v6, v3, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->c:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v4, v2

    move v3, v2

    :goto_0
    if-ge v4, v7, :cond_1

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2215346
    iget-object v8, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v9, v0, Lcom/facebook/messaging/service/model/AddPinnedThreadParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v8, v9}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 2215347
    const/4 v3, 0x1

    .line 2215348
    :cond_0
    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2215349
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 2215350
    :cond_1
    if-nez v3, :cond_3

    .line 2215351
    iget-object v2, v0, Lcom/facebook/messaging/service/model/AddPinnedThreadParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    if-eqz v2, :cond_2

    .line 2215352
    iget-object v2, v0, Lcom/facebook/messaging/service/model/AddPinnedThreadParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2215353
    :cond_2
    invoke-static {v1, v5}, LX/FDs;->a(LX/FDs;Ljava/util/List;)V

    .line 2215354
    :cond_3
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2215355
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    .line 2215356
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    iget-object v3, p0, LX/FDv;->g:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v1, v0, v4, v5}, LX/FDs;->a(Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;J)V

    .line 2215357
    return-object v2
.end method

.method public final x(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 10

    .prologue
    .line 2215358
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2215359
    const-string v1, "unpinThreadParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/UnpinThreadParams;

    .line 2215360
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    .line 2215361
    iget-object v2, v1, LX/FDs;->e:LX/FDq;

    invoke-virtual {v2}, LX/FDq;->a()Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    move-result-object v4

    .line 2215362
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v5

    .line 2215363
    iget-object v6, v4, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->c:LX/0Px;

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v7, :cond_1

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2215364
    iget-object v8, v0, Lcom/facebook/messaging/service/model/UnpinThreadParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    iget-object v9, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-virtual {v8, v9}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 2215365
    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2215366
    :cond_0
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 2215367
    :cond_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    iget-object v3, v4, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->c:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v3

    if-eq v2, v3, :cond_2

    .line 2215368
    invoke-static {v1, v5}, LX/FDs;->a(LX/FDs;Ljava/util/List;)V

    .line 2215369
    :cond_2
    invoke-interface {p2, p1}, LX/1qM;->handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v2

    .line 2215370
    invoke-virtual {v2}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelableNullOk()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    .line 2215371
    iget-object v1, p0, LX/FDv;->o:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDs;

    iget-object v3, p0, LX/FDv;->g:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    invoke-virtual {v1, v0, v4, v5}, LX/FDs;->a(Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;J)V

    .line 2215372
    return-object v2
.end method

.method public final z(LX/1qK;LX/1qM;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 2

    .prologue
    .line 2215373
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2215374
    const-string v1, "updatedMessageSendErrorParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/service/model/UpdateMessageSendErrorParams;

    .line 2215375
    iget-object v1, p0, LX/FDv;->m:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FDt;

    invoke-virtual {v1, v0}, LX/FDt;->a(Lcom/facebook/messaging/service/model/UpdateMessageSendErrorParams;)V

    .line 2215376
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2215377
    return-object v0
.end method
