.class public final LX/Fhu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel;",
        ">;",
        "LX/Cw5;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/2Sm;


# direct methods
.method public constructor <init>(LX/2Sm;)V
    .locals 0

    .prologue
    .line 2273868
    iput-object p1, p0, LX/Fhu;->a:LX/2Sm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2273869
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2273870
    if-eqz p1, :cond_0

    .line 2273871
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2273872
    if-eqz v0, :cond_0

    .line 2273873
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2273874
    check-cast v0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel;->a()Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2273875
    iget-object v0, p0, LX/Fhu;->a:LX/2Sm;

    iget-object v1, v0, LX/2Sm;->d:LX/2Sn;

    .line 2273876
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2273877
    check-cast v0, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel;->a()Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel;->a()LX/0Px;

    move-result-object v0

    .line 2273878
    if-nez v0, :cond_1

    .line 2273879
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2273880
    :goto_0
    move-object v0, v2

    .line 2273881
    :goto_1
    new-instance v1, LX/Cw5;

    .line 2273882
    iget-wide v5, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v2, v5

    .line 2273883
    iget-object v4, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v4, v4

    .line 2273884
    invoke-direct {v1, v0, v2, v3, v4}, LX/Cw5;-><init>(LX/0Px;JLX/0ta;)V

    return-object v1

    .line 2273885
    :cond_0
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2273886
    goto :goto_1

    .line 2273887
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2273888
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v5

    const/4 v2, 0x0

    move v3, v2

    :goto_2
    if-ge v3, v5, :cond_2

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;

    .line 2273889
    :try_start_0
    invoke-static {v2}, LX/2Sn;->a(Lcom/facebook/search/protocol/FetchTypeaheadPYMKGraphQLModels$FBTypeaheadPYMKQueryModel$PeopleYouMayKnowModel$NodesModel;)Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;
    :try_end_0
    .catch LX/7C4; {:try_start_0 .. :try_end_0} :catch_0

    .line 2273890
    :goto_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 2273891
    :catch_0
    move-exception v2

    .line 2273892
    iget-object p0, v1, LX/2Sn;->a:LX/2Sc;

    invoke-virtual {p0, v2}, LX/2Sc;->a(LX/7C4;)V

    goto :goto_3

    .line 2273893
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    goto :goto_0
.end method
