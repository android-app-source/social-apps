.class public final enum LX/GxE;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GxE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GxE;

.field public static final enum PAGE_STATE_ERROR:LX/GxE;

.field public static final enum PAGE_STATE_SUCCESS:LX/GxE;

.field public static final enum PAGE_STATE_UINITIALIZED:LX/GxE;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2407888
    new-instance v0, LX/GxE;

    const-string v1, "PAGE_STATE_ERROR"

    invoke-direct {v0, v1, v2}, LX/GxE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GxE;->PAGE_STATE_ERROR:LX/GxE;

    new-instance v0, LX/GxE;

    const-string v1, "PAGE_STATE_SUCCESS"

    invoke-direct {v0, v1, v3}, LX/GxE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GxE;->PAGE_STATE_SUCCESS:LX/GxE;

    new-instance v0, LX/GxE;

    const-string v1, "PAGE_STATE_UINITIALIZED"

    invoke-direct {v0, v1, v4}, LX/GxE;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GxE;->PAGE_STATE_UINITIALIZED:LX/GxE;

    const/4 v0, 0x3

    new-array v0, v0, [LX/GxE;

    sget-object v1, LX/GxE;->PAGE_STATE_ERROR:LX/GxE;

    aput-object v1, v0, v2

    sget-object v1, LX/GxE;->PAGE_STATE_SUCCESS:LX/GxE;

    aput-object v1, v0, v3

    sget-object v1, LX/GxE;->PAGE_STATE_UINITIALIZED:LX/GxE;

    aput-object v1, v0, v4

    sput-object v0, LX/GxE;->$VALUES:[LX/GxE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2407887
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GxE;
    .locals 1

    .prologue
    .line 2407886
    const-class v0, LX/GxE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GxE;

    return-object v0
.end method

.method public static values()[LX/GxE;
    .locals 1

    .prologue
    .line 2407885
    sget-object v0, LX/GxE;->$VALUES:[LX/GxE;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GxE;

    return-object v0
.end method
