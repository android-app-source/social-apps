.class public final LX/FUS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/qrcode/QRCodeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/qrcode/QRCodeFragment;)V
    .locals 0

    .prologue
    .line 2248283
    iput-object p1, p0, LX/FUS;->a:Lcom/facebook/qrcode/QRCodeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, -0x6b7e5c8c

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2248284
    iget-object v2, p0, LX/FUS;->a:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v2, v2, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    .line 2248285
    iget-object v3, v2, LX/FUf;->a:LX/0if;

    sget-object v4, LX/0ig;->H:LX/0ih;

    const-string p1, "MY_CODE_CLICKED"

    invoke-virtual {v3, v4, p1}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2248286
    iget-object v2, p0, LX/FUS;->a:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v2, v2, Lcom/facebook/qrcode/QRCodeFragment;->p:LX/FUW;

    iget-object v3, p0, LX/FUS;->a:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v3, v3, Lcom/facebook/qrcode/QRCodeFragment;->M:LX/FUV;

    sget-object v4, LX/FUV;->VANITY:LX/FUV;

    if-ne v3, v4, :cond_1

    .line 2248287
    :goto_0
    const-string v3, "qrcode_code_clicked"

    const-string v4, "vanity"

    invoke-static {v2, v3, v4, v0}, LX/FUW;->a(LX/FUW;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2248288
    iget-object v0, p0, LX/FUS;->a:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->M:LX/FUV;

    sget-object v2, LX/FUV;->STANDARD:LX/FUV;

    if-eq v0, v2, :cond_0

    .line 2248289
    iget-object v0, p0, LX/FUS;->a:Lcom/facebook/qrcode/QRCodeFragment;

    sget-object v2, LX/FUV;->STANDARD:LX/FUV;

    invoke-static {v0, v2}, Lcom/facebook/qrcode/QRCodeFragment;->a$redex0(Lcom/facebook/qrcode/QRCodeFragment;LX/FUV;)V

    .line 2248290
    :cond_0
    const v0, 0x6c0d6912

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2248291
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
