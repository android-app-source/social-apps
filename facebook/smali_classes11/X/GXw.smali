.class public final LX/GXw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "LX/GXx;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/GXy;


# direct methods
.method public constructor <init>(LX/GXy;J)V
    .locals 0

    .prologue
    .line 2364651
    iput-object p1, p0, LX/GXw;->b:LX/GXy;

    iput-wide p2, p0, LX/GXw;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 10

    .prologue
    .line 2364652
    const/4 v2, 0x0

    .line 2364653
    iget-object v0, p0, LX/GXw;->b:LX/GXy;

    iget-object v0, v0, LX/GXy;->c:LX/7j4;

    .line 2364654
    :try_start_0
    iget-object v1, v0, LX/7j4;->b:Ljava/util/Locale;

    invoke-static {v1}, Ljava/util/Currency;->getInstance(Ljava/util/Locale;)Ljava/util/Currency;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2364655
    :goto_0
    move-object v5, v1

    .line 2364656
    iget-object v1, v0, LX/7j4;->c:Landroid/content/Context;

    invoke-static {v1}, LX/5fw;->a(Landroid/content/Context;)LX/0Px;

    move-result-object v3

    .line 2364657
    if-nez v5, :cond_4

    move-object v1, v3

    .line 2364658
    :goto_1
    move-object v3, v1

    .line 2364659
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2364660
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_2
    if-ge v1, v5, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2364661
    iget-object v6, p0, LX/GXw;->b:LX/GXy;

    iget-object v6, v6, LX/GXy;->c:LX/7j4;

    .line 2364662
    :try_start_1
    iget-object v7, v6, LX/7j4;->c:Landroid/content/Context;

    invoke-static {v7, v0}, LX/5fw;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v7

    .line 2364663
    :goto_3
    move-object v6, v7

    .line 2364664
    if-eqz v6, :cond_0

    .line 2364665
    invoke-static {v0, v6}, LX/3rL;->a(Ljava/lang/Object;Ljava/lang/Object;)LX/3rL;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2364666
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2364667
    :cond_1
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2364668
    new-instance v0, LX/7k1;

    invoke-direct {v0}, LX/7k1;-><init>()V

    move-object v0, v0

    .line 2364669
    const-string v1, "page_id"

    iget-wide v4, p0, LX/GXw;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/7k1;

    .line 2364670
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2364671
    iget-object v1, p0, LX/GXw;->b:LX/GXy;

    iget-object v1, v1, LX/GXy;->a:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    const v1, 0x5334d1e2

    invoke-static {v0, v1}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/commerce/publishing/graphql/FetchAdminAddShopQueryModels$FetchAdminAddShopQueryModel;

    .line 2364672
    invoke-virtual {v0}, Lcom/facebook/commerce/publishing/graphql/FetchAdminAddShopQueryModels$FetchAdminAddShopQueryModel;->j()Lcom/facebook/commerce/publishing/graphql/FetchAdminAddShopQueryModels$FetchAdminAddShopQueryModel$CommerceStoreModel;

    move-result-object v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    .line 2364673
    :goto_4
    invoke-virtual {v0}, Lcom/facebook/commerce/publishing/graphql/FetchAdminAddShopQueryModels$FetchAdminAddShopQueryModel;->a()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    if-eqz v4, :cond_2

    .line 2364674
    invoke-virtual {v0}, Lcom/facebook/commerce/publishing/graphql/FetchAdminAddShopQueryModels$FetchAdminAddShopQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2364675
    invoke-virtual {v4, v0, v2}, LX/15i;->h(II)Z

    move-result v2

    .line 2364676
    :cond_2
    new-instance v0, LX/GXx;

    invoke-direct {v0, v3, v1, v2}, LX/GXx;-><init>(LX/0Px;ZZ)V

    return-object v0

    :cond_3
    move v1, v2

    .line 2364677
    goto :goto_4

    .line 2364678
    :cond_4
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2364679
    invoke-virtual {v6, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2364680
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v7

    const/4 v1, 0x0

    move v4, v1

    :goto_5
    if-ge v4, v7, :cond_6

    invoke-virtual {v3, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2364681
    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_5

    .line 2364682
    invoke-virtual {v6, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2364683
    :cond_5
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_5

    .line 2364684
    :cond_6
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto/16 :goto_1

    .line 2364685
    :catch_0
    move-exception v1

    .line 2364686
    sget-object v3, LX/7j4;->a:Ljava/lang/String;

    const-string v4, "locale not supported "

    invoke-static {v3, v4, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2364687
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 2364688
    :catch_1
    move-exception v7

    .line 2364689
    sget-object v8, LX/7j4;->a:Ljava/lang/String;

    const-string v9, "currency not supported"

    invoke-static {v8, v9, v7}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2364690
    const/4 v7, 0x0

    goto/16 :goto_3
.end method
