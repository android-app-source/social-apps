.class public LX/Gdc;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2374365
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2374366
    iput-object p1, p0, LX/Gdc;->a:LX/0Zb;

    .line 2374367
    return-void
.end method

.method public static b(LX/0QB;)LX/Gdc;
    .locals 2

    .prologue
    .line 2374368
    new-instance v1, LX/Gdc;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/Gdc;-><init>(LX/0Zb;)V

    .line 2374369
    return-object v1
.end method


# virtual methods
.method public final a(ILjava/util/List;)V
    .locals 3
    .param p2    # Ljava/util/List;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2374370
    if-ltz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 2374371
    :cond_0
    :goto_0
    return-void

    .line 2374372
    :cond_1
    iget-object v0, p0, LX/Gdc;->a:LX/0Zb;

    const-string v1, "topic_feeds_switcher_render"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2374373
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2374374
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2374375
    const-string p0, "topic_id"

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, p0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2374376
    const-string v1, "index"

    invoke-virtual {v0, v1, p1}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2374377
    const-string v1, "topic_feeds"

    invoke-virtual {v0, v1, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/Object;)LX/0oG;

    .line 2374378
    invoke-virtual {v0}, LX/0oG;->d()V

    goto :goto_0
.end method
