.class public final LX/H9I;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/H9L;


# direct methods
.method public constructor <init>(LX/H9L;Z)V
    .locals 0

    .prologue
    .line 2434220
    iput-object p1, p0, LX/H9I;->b:LX/H9L;

    iput-boolean p2, p0, LX/H9I;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2434221
    iget-object v0, p0, LX/H9I;->b:LX/H9L;

    iget-object v0, v0, LX/H9L;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/25G;

    invoke-static {}, Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;->a()LX/5H8;

    move-result-object v2

    iget-object v1, p0, LX/H9I;->b:LX/H9L;

    iget-object v1, v1, LX/H9L;->g:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/20i;

    invoke-virtual {v1}, LX/20i;->a()Lcom/facebook/graphql/model/GraphQLActor;

    move-result-object v1

    .line 2434222
    iput-object v1, v2, LX/5H8;->c:Lcom/facebook/graphql/model/GraphQLActor;

    .line 2434223
    move-object v1, v2

    .line 2434224
    iget-boolean v2, p0, LX/H9I;->a:Z

    .line 2434225
    iput-boolean v2, v1, LX/5H8;->b:Z

    .line 2434226
    move-object v1, v1

    .line 2434227
    iget-object v2, p0, LX/H9I;->b:LX/H9L;

    iget-object v2, v2, LX/H9L;->h:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    .line 2434228
    iput-object v2, v1, LX/5H8;->a:Ljava/lang/String;

    .line 2434229
    move-object v1, v1

    .line 2434230
    const-string v2, "page_profile"

    .line 2434231
    iput-object v2, v1, LX/5H8;->e:Ljava/lang/String;

    .line 2434232
    move-object v1, v1

    .line 2434233
    new-instance v2, LX/21A;

    invoke-direct {v2}, LX/21A;-><init>()V

    const-string v3, "pages_identity"

    .line 2434234
    iput-object v3, v2, LX/21A;->c:Ljava/lang/String;

    .line 2434235
    move-object v2, v2

    .line 2434236
    invoke-virtual {v2}, LX/21A;->b()Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    move-result-object v2

    .line 2434237
    iput-object v2, v1, LX/5H8;->d:Lcom/facebook/api/ufiservices/common/FeedbackLoggingParams;

    .line 2434238
    move-object v1, v1

    .line 2434239
    invoke-virtual {v1}, LX/5H8;->a()Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/25G;->a(Lcom/facebook/api/ufiservices/common/TogglePageLikeParams;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
