.class public final LX/F7c;
.super LX/79T;
.source ""


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;Z)V
    .locals 0

    .prologue
    .line 2202224
    iput-object p1, p0, LX/F7c;->b:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    iput-boolean p2, p0, LX/F7c;->a:Z

    invoke-direct {p0}, LX/79T;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 2202225
    iget-object v0, p0, LX/F7c;->b:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->c:LX/9Tk;

    iget-object v1, p0, LX/F7c;->b:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    iget-object v1, v1, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->k:LX/89v;

    iget-object v1, v1, LX/89v;->value:Ljava/lang/String;

    iget-object v2, p0, LX/F7c;->b:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    iget-object v2, v2, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->l:Ljava/lang/String;

    .line 2202226
    sget-object v3, LX/9Tj;->LEGAL_GET_STARTED:LX/9Tj;

    const-string v4, "ci_flow"

    const-string v5, "ccu_ref"

    invoke-static {v4, v1, v5, v2}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v4

    invoke-static {v0, v3, v4}, LX/9Tk;->a(LX/9Tk;LX/9Tj;Ljava/util/Map;)V

    .line 2202227
    iget-object v0, p0, LX/F7c;->b:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    iget-object v0, v0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->f:Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;

    iget-boolean v1, p0, LX/F7c;->a:Z

    invoke-virtual {v0, v1}, Lcom/facebook/growth/friendfinder/FriendFinderPreferenceSetter;->b(Z)V

    .line 2202228
    iget-object v0, p0, LX/F7c;->b:Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;

    const/4 v5, 0x0

    .line 2202229
    iget-boolean v1, v0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->m:Z

    if-eqz v1, :cond_1

    .line 2202230
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object v1, v1

    .line 2202231
    check-cast v1, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;

    invoke-virtual {v1}, Lcom/facebook/growth/friendfinder/factory/FriendFinderMainFragment;->b()V

    .line 2202232
    :cond_0
    :goto_0
    return-void

    .line 2202233
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    .line 2202234
    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/facebook/growth/friendfinder/FriendFinderHostingActivity;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2202235
    iget-object v3, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v3, v3

    .line 2202236
    invoke-virtual {v2, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2202237
    iget-object v3, v0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->k:LX/89v;

    sget-object v4, LX/89v;->IORG_INCENTIVE_INVITE:LX/89v;

    if-ne v3, v4, :cond_2

    instance-of v3, v1, LX/F7d;

    if-eqz v3, :cond_2

    .line 2202238
    check-cast v1, LX/F7d;

    invoke-interface {v1}, LX/F7d;->a()V

    goto :goto_0

    .line 2202239
    :cond_2
    instance-of v3, v1, LX/8DG;

    if-eqz v3, :cond_3

    .line 2202240
    iget-object v1, v0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v1, v2, v5, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    goto :goto_0

    .line 2202241
    :cond_3
    iget-object v3, v0, Lcom/facebook/growth/friendfinder/FriendFinderIntroFragment;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, v2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2202242
    iget-object v2, v0, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v2

    .line 2202243
    const-string v3, "FINISH_CONTAINING_ACTIVITY"

    invoke-virtual {v2, v3, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2202244
    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method
