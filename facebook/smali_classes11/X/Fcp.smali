.class public LX/Fcp;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field private final b:Landroid/content/res/Resources;

.field private final c:LX/Cwq;

.field public final d:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/2Sc;

.field public f:LX/Fc4;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0tX;Landroid/content/res/Resources;LX/Cwq;LX/1Ck;LX/2Sc;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2262786
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2262787
    iput-object p1, p0, LX/Fcp;->a:LX/0tX;

    .line 2262788
    iput-object p2, p0, LX/Fcp;->b:Landroid/content/res/Resources;

    .line 2262789
    iput-object p3, p0, LX/Fcp;->c:LX/Cwq;

    .line 2262790
    iput-object p4, p0, LX/Fcp;->d:LX/1Ck;

    .line 2262791
    iput-object p5, p0, LX/Fcp;->e:LX/2Sc;

    .line 2262792
    return-void
.end method

.method public static a(LX/Fcp;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/search/protocol/FB4AGraphSearchUserWithFiltersGraphQLModels$FB4AGraphSearchFilterQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2262793
    new-instance v0, LX/9ya;

    invoke-direct {v0}, LX/9ya;-><init>()V

    const-string v1, "filterID"

    invoke-virtual {v0, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "substring"

    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "count"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "profile_picture_size"

    iget-object v2, p0, LX/Fcp;->b:Landroid/content/res/Resources;

    const v3, 0x7f0b14d5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    .line 2262794
    iget-object v1, v0, LX/0gW;->e:LX/0w7;

    move-object v0, v1

    .line 2262795
    iget-object v1, p0, LX/Fcp;->a:LX/0tX;

    .line 2262796
    new-instance v2, LX/9ya;

    invoke-direct {v2}, LX/9ya;-><init>()V

    move-object v2, v2

    .line 2262797
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->c:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    invoke-virtual {v2, v0}, LX/0zO;->a(LX/0w7;)LX/0zO;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2262798
    return-object v0
.end method
