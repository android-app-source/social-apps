.class public final LX/Gm3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Gm7;


# direct methods
.method public constructor <init>(LX/Gm7;)V
    .locals 0

    .prologue
    .line 2391546
    iput-object p1, p0, LX/Gm3;->a:LX/Gm7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x485dd1f2

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2391547
    iget-object v1, p0, LX/Gm3;->a:LX/Gm7;

    iget-object v1, v1, LX/Gm7;->d:Lcom/facebook/growth/contactinviter/ContactInviterFragment;

    if-eqz v1, :cond_0

    .line 2391548
    iget-object v1, p0, LX/Gm3;->a:LX/Gm7;

    iget-object v1, v1, LX/Gm7;->d:Lcom/facebook/growth/contactinviter/ContactInviterFragment;

    iget-object v2, p0, LX/Gm3;->a:LX/Gm7;

    iget-object v2, v2, LX/Gm7;->c:LX/GmD;

    .line 2391549
    iget-object v4, v1, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->o:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2391550
    sget-object v4, LX/GmC;->PENDING:LX/GmC;

    iput-object v4, v2, LX/GmD;->c:LX/GmC;

    .line 2391551
    iget-object v4, v1, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->a:LX/Glv;

    invoke-virtual {v4}, LX/1OM;->notifyDataSetChanged()V

    .line 2391552
    new-instance v4, Landroid/os/Handler;

    invoke-direct {v4}, Landroid/os/Handler;-><init>()V

    .line 2391553
    new-instance v5, Lcom/facebook/growth/contactinviter/ContactInviterFragment$7;

    invoke-direct {v5, v1, v2}, Lcom/facebook/growth/contactinviter/ContactInviterFragment$7;-><init>(Lcom/facebook/growth/contactinviter/ContactInviterFragment;LX/GmD;)V

    const-wide/16 v6, 0xfa0

    const v8, -0xe047b19

    invoke-static {v4, v5, v6, v7, v8}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    .line 2391554
    sget-object v4, LX/GmG;->INVITE:LX/GmG;

    iget-object v5, v2, LX/GmD;->b:Ljava/lang/String;

    invoke-static {v1, v4, v5}, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->a(Lcom/facebook/growth/contactinviter/ContactInviterFragment;LX/GmG;Ljava/lang/String;)V

    .line 2391555
    iget-object v4, v1, Lcom/facebook/growth/contactinviter/ContactInviterFragment;->g:LX/0if;

    sget-object v5, LX/0ig;->X:LX/0ih;

    const-string v6, "contact_invite_list_item_invite_button_click"

    invoke-virtual {v4, v5, v6}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2391556
    :cond_0
    const v1, -0x2d0c7607

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
