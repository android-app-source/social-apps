.class public LX/GNs;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/GNs;


# instance fields
.field public final a:LX/0ad;


# direct methods
.method public constructor <init>(LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2346625
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2346626
    iput-object p1, p0, LX/GNs;->a:LX/0ad;

    .line 2346627
    return-void
.end method

.method public static a(LX/0QB;)LX/GNs;
    .locals 4

    .prologue
    .line 2346628
    sget-object v0, LX/GNs;->b:LX/GNs;

    if-nez v0, :cond_1

    .line 2346629
    const-class v1, LX/GNs;

    monitor-enter v1

    .line 2346630
    :try_start_0
    sget-object v0, LX/GNs;->b:LX/GNs;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2346631
    if-eqz v2, :cond_0

    .line 2346632
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2346633
    new-instance p0, LX/GNs;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {p0, v3}, LX/GNs;-><init>(LX/0ad;)V

    .line 2346634
    move-object v0, p0

    .line 2346635
    sput-object v0, LX/GNs;->b:LX/GNs;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2346636
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2346637
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2346638
    :cond_1
    sget-object v0, LX/GNs;->b:LX/GNs;

    return-object v0

    .line 2346639
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2346640
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(S)Z
    .locals 4

    .prologue
    .line 2346641
    sget-object v0, LX/0c1;->On:LX/0c1;

    .line 2346642
    iget-object v1, p0, LX/GNs;->a:LX/0ad;

    sget-object v2, LX/0c0;->Live:LX/0c0;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v0, p1, v3}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v1

    move v0, v1

    .line 2346643
    return v0
.end method
