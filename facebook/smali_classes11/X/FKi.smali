.class public LX/FKi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/service/model/IgnoreMessageRequestsParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225478
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2225479
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 5

    .prologue
    .line 2225458
    check-cast p1, Lcom/facebook/messaging/service/model/IgnoreMessageRequestsParams;

    .line 2225459
    const-string v0, "/me/message_ignore_requests"

    .line 2225460
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2225461
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "thread_ids"

    .line 2225462
    iget-object v4, p1, Lcom/facebook/messaging/service/model/IgnoreMessageRequestsParams;->b:LX/0Px;

    move-object v4, v4

    .line 2225463
    invoke-virtual {v4}, LX/0Px;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225464
    new-instance v2, LX/14O;

    invoke-direct {v2}, LX/14O;-><init>()V

    const-string v3, "ignoreMessageRequests"

    .line 2225465
    iput-object v3, v2, LX/14O;->b:Ljava/lang/String;

    .line 2225466
    move-object v2, v2

    .line 2225467
    const-string v3, "POST"

    .line 2225468
    iput-object v3, v2, LX/14O;->c:Ljava/lang/String;

    .line 2225469
    move-object v2, v2

    .line 2225470
    iput-object v0, v2, LX/14O;->d:Ljava/lang/String;

    .line 2225471
    move-object v0, v2

    .line 2225472
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2225473
    move-object v0, v0

    .line 2225474
    sget-object v1, LX/14S;->STRING:LX/14S;

    .line 2225475
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2225476
    move-object v0, v0

    .line 2225477
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2225456
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2225457
    const/4 v0, 0x0

    return-object v0
.end method
