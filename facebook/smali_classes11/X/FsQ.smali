.class public final LX/FsQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel;",
        ">;",
        "Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FsT;


# direct methods
.method public constructor <init>(LX/FsT;)V
    .locals 0

    .prologue
    .line 2296966
    iput-object p1, p0, LX/FsQ;->a:LX/FsT;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2296967
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2296968
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2296969
    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel;

    .line 2296970
    if-nez v0, :cond_0

    .line 2296971
    const/4 v1, 0x0

    .line 2296972
    :goto_0
    move-object v0, v1

    .line 2296973
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->ab()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    move-result-object v0

    return-object v0

    .line 2296974
    :cond_0
    new-instance v1, LX/33O;

    invoke-direct {v1}, LX/33O;-><init>()V

    .line 2296975
    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel;->a()Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;

    move-result-object p0

    .line 2296976
    if-nez p0, :cond_1

    .line 2296977
    const/4 p1, 0x0

    .line 2296978
    :goto_1
    move-object p0, p1

    .line 2296979
    iput-object p0, v1, LX/33O;->bD:Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    .line 2296980
    invoke-virtual {v1}, LX/33O;->a()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v1

    goto :goto_0

    .line 2296981
    :cond_1
    new-instance p1, LX/4ZL;

    invoke-direct {p1}, LX/4ZL;-><init>()V

    .line 2296982
    invoke-virtual {p0}, Lcom/facebook/timeline/protocol/FetchTimelineUnseenSectionQueryModels$TimelineUnseenStoriesQueryModel$UnseenStoriesModel;->a()LX/0Px;

    move-result-object v0

    .line 2296983
    iput-object v0, p1, LX/4ZL;->c:LX/0Px;

    .line 2296984
    invoke-virtual {p1}, LX/4ZL;->a()Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;

    move-result-object p1

    goto :goto_1
.end method
