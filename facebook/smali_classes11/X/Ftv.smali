.class public final LX/Ftv;
.super LX/1OX;
.source ""


# instance fields
.field public final synthetic a:LX/1P1;

.field public final synthetic b:Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;LX/1P1;)V
    .locals 0

    .prologue
    .line 2299302
    iput-object p1, p0, LX/Ftv;->b:Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;

    iput-object p2, p0, LX/Ftv;->a:LX/1P1;

    invoke-direct {p0}, LX/1OX;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 2

    .prologue
    .line 2299303
    iget-object v0, p0, LX/Ftv;->a:LX/1P1;

    invoke-virtual {v0}, LX/1OR;->D()I

    move-result v0

    .line 2299304
    iget-object v1, p0, LX/Ftv;->a:LX/1P1;

    invoke-virtual {v1}, LX/1P1;->n()I

    move-result v1

    .line 2299305
    add-int/lit8 v1, v1, 0x1

    sub-int/2addr v0, v1

    if-gtz v0, :cond_0

    .line 2299306
    iget-object v0, p0, LX/Ftv;->b:Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;

    iget-object v0, v0, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->k:LX/FtF;

    iget-object v1, p0, LX/Ftv;->b:Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;

    iget-object v1, v1, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->k:LX/FtF;

    invoke-virtual {v1}, LX/FtF;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, LX/FtF;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel$PageInfoModel;

    .line 2299307
    invoke-virtual {v0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel$PageInfoModel;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel$PageInfoModel;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2299308
    iget-object v1, p0, LX/Ftv;->b:Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$FavoriteMediaUserModel$ProfileIntroCardModel$FeaturedMediasetsSuggestionsModel$PageInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2299309
    const-string p0, "initial_client_cursor"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2299310
    :cond_0
    :goto_0
    return-void

    .line 2299311
    :cond_1
    iget-object p0, v1, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->b:LX/1Ck;

    new-instance p1, LX/Ftw;

    invoke-direct {p1, v1, v0}, LX/Ftw;-><init>(Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;Ljava/lang/String;)V

    iget-object p2, v1, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerFragment;->n:LX/0Vd;

    invoke-virtual {p0, v0, p1, p2}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method
