.class public LX/GVi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/captcha/protocol/SolveCaptchaMethod$Params;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0dC;


# direct methods
.method public constructor <init>(LX/0dC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2359776
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2359777
    iput-object p1, p0, LX/GVi;->a:LX/0dC;

    .line 2359778
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2359779
    check-cast p1, Lcom/facebook/captcha/protocol/SolveCaptchaMethod$Params;

    .line 2359780
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2359781
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "persist_data"

    .line 2359782
    iget-object v3, p1, Lcom/facebook/captcha/protocol/SolveCaptchaMethod$Params;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2359783
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2359784
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "input"

    .line 2359785
    iget-object v3, p1, Lcom/facebook/captcha/protocol/SolveCaptchaMethod$Params;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2359786
    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2359787
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "device_id"

    iget-object v3, p0, LX/GVi;->a:LX/0dC;

    invoke-virtual {v3}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2359788
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "validateCaptcha"

    .line 2359789
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2359790
    move-object v1, v1

    .line 2359791
    const-string v2, "POST"

    .line 2359792
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2359793
    move-object v1, v1

    .line 2359794
    const-string v2, "captcha/validations"

    .line 2359795
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2359796
    move-object v1, v1

    .line 2359797
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2359798
    move-object v0, v1

    .line 2359799
    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/14O;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/14O;

    move-result-object v0

    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2359800
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2359801
    move-object v0, v0

    .line 2359802
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2359803
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2359804
    invoke-virtual {p2}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    const-string v1, "success"

    invoke-virtual {v0, v1}, LX/0lF;->e(Ljava/lang/String;)LX/0lF;

    move-result-object v0

    .line 2359805
    invoke-static {v0}, LX/16N;->g(LX/0lF;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method
