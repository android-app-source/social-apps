.class public final LX/FWY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel;",
        ">;",
        "LX/FVy;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FWZ;


# direct methods
.method public constructor <init>(LX/FWZ;)V
    .locals 0

    .prologue
    .line 2252256
    iput-object p1, p0, LX/FWY;->a:LX/FWZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 9
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2252257
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2252258
    iget-object v0, p0, LX/FWY;->a:LX/FWZ;

    iget-object v0, v0, LX/FWZ;->c:LX/FWJ;

    const/4 v1, 0x0

    .line 2252259
    if-nez p1, :cond_0

    .line 2252260
    new-instance v2, LX/FVy;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v1}, LX/FVy;-><init>(LX/0am;ZLjava/lang/Long;)V

    move-object v1, v2

    .line 2252261
    :goto_0
    move-object v0, v1

    .line 2252262
    return-object v0

    .line 2252263
    :cond_0
    iget-wide v7, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v3, v7

    .line 2252264
    const-wide/16 v5, 0x0

    cmp-long v2, v3, v5

    if-lez v2, :cond_1

    .line 2252265
    iget-object v1, v0, LX/FWJ;->c:LX/0SG;

    invoke-interface {v1}, LX/0SG;->a()J

    move-result-wide v1

    .line 2252266
    iget-wide v7, p1, Lcom/facebook/fbservice/results/BaseResult;->clientTimeMs:J

    move-wide v3, v7

    .line 2252267
    sub-long/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 2252268
    :cond_1
    new-instance v2, LX/FVy;

    invoke-static {p1}, LX/FWJ;->c(Lcom/facebook/graphql/executor/GraphQLResult;)LX/0Px;

    move-result-object v3

    .line 2252269
    const/4 v5, 0x0

    .line 2252270
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2252271
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/FVt;

    .line 2252272
    iget-object v8, v4, LX/FVt;->d:Ljava/lang/String;

    move-object v8, v8

    .line 2252273
    invoke-static {v8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 2252274
    iget-object v8, v4, LX/FVt;->d:Ljava/lang/String;

    move-object v8, v8

    .line 2252275
    invoke-virtual {v8, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 2252276
    new-instance v5, LX/FVx;

    .line 2252277
    iget-object v8, v4, LX/FVt;->d:Ljava/lang/String;

    move-object v8, v8

    .line 2252278
    invoke-direct {v5, v8}, LX/FVx;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2252279
    iget-object v5, v4, LX/FVt;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2252280
    :cond_3
    invoke-virtual {v6, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2252281
    :cond_4
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v3, v4

    .line 2252282
    invoke-static {v3}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v3

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2252283
    if-eqz p1, :cond_5

    .line 2252284
    iget-object v4, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v4, v4

    .line 2252285
    if-eqz v4, :cond_5

    .line 2252286
    iget-object v4, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v4, v4

    .line 2252287
    check-cast v4, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel;

    invoke-virtual {v4}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel;->a()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel$SavedItemsModel;

    move-result-object v4

    if-nez v4, :cond_6

    :cond_5
    move v4, v6

    :goto_2
    if-eqz v4, :cond_8

    move v4, v5

    .line 2252288
    :goto_3
    move v4, v4

    .line 2252289
    invoke-direct {v2, v3, v4, v1}, LX/FVy;-><init>(LX/0am;ZLjava/lang/Long;)V

    move-object v1, v2

    goto :goto_0

    .line 2252290
    :cond_6
    iget-object v4, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v4, v4

    .line 2252291
    check-cast v4, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel;

    invoke-virtual {v4}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel;->a()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel$SavedItemsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel$SavedItemsModel;->j()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    .line 2252292
    if-nez v4, :cond_7

    move v4, v6

    goto :goto_2

    :cond_7
    move v4, v5

    goto :goto_2

    .line 2252293
    :cond_8
    iget-object v4, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v4, v4

    .line 2252294
    check-cast v4, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel;

    invoke-virtual {v4}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel;->a()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel$SavedItemsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$FetchSavedItemsGraphQLModel$SavedItemsModel;->j()LX/1vs;

    move-result-object v4

    iget-object v6, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    invoke-virtual {v6, v4, v5}, LX/15i;->h(II)Z

    move-result v4

    goto :goto_3
.end method
