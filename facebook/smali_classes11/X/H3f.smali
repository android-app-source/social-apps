.class public final LX/H3f;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Zx;


# instance fields
.field public final synthetic a:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;


# direct methods
.method public constructor <init>(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)V
    .locals 0

    .prologue
    .line 2421515
    iput-object p1, p0, LX/H3f;->a:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2421516
    iget-object v1, p0, LX/H3f;->a:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    iget-object v0, p0, LX/H3f;->a:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1sS;

    iput-object v0, v1, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->b:LX/1sS;

    .line 2421517
    iget-object v0, p0, LX/H3f;->a:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->b:LX/1sS;

    iget-object v1, p0, LX/H3f;->a:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    iget-object v1, v1, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->k:Lcom/facebook/location/FbLocationOperationParams;

    sget-object v2, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->i:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2}, LX/1sS;->a(Lcom/facebook/location/FbLocationOperationParams;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2421518
    iget-object v0, p0, LX/H3f;->a:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->d:LX/1Ck;

    const-string v1, "nearby_places_get_location_task_key"

    iget-object v2, p0, LX/H3f;->a:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    iget-object v2, v2, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->b:LX/1sS;

    new-instance v3, LX/H3e;

    invoke-direct {v3, p0}, LX/H3e;-><init>(LX/H3f;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2421519
    return-void
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2421520
    iget-object v0, p0, LX/H3f;->a:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    sget-object v1, LX/FOw;->LOCATION_PERMISSION_OFF:LX/FOw;

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->a$redex0(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;LX/FOw;)V

    .line 2421521
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2421522
    iget-object v0, p0, LX/H3f;->a:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    sget-object v1, LX/FOw;->LOCATION_PERMISSION_OFF:LX/FOw;

    invoke-static {v0, v1}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->a$redex0(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;LX/FOw;)V

    .line 2421523
    return-void
.end method
