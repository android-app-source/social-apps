.class public LX/GL7;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public b:Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

.field public c:LX/GDm;

.field public d:LX/GDk;

.field public e:Landroid/view/inputmethod/InputMethodManager;


# direct methods
.method public constructor <init>(LX/GDk;LX/GDm;Landroid/view/inputmethod/InputMethodManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2342088
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2342089
    iput-object p1, p0, LX/GL7;->d:LX/GDk;

    .line 2342090
    iput-object p2, p0, LX/GL7;->c:LX/GDm;

    .line 2342091
    iput-object p3, p0, LX/GL7;->e:Landroid/view/inputmethod/InputMethodManager;

    .line 2342092
    return-void
.end method

.method public static b(LX/0QB;)LX/GL7;
    .locals 4

    .prologue
    .line 2342093
    new-instance v3, LX/GL7;

    invoke-static {p0}, LX/GDk;->a(LX/0QB;)LX/GDk;

    move-result-object v0

    check-cast v0, LX/GDk;

    invoke-static {p0}, LX/GDm;->a(LX/0QB;)LX/GDm;

    move-result-object v1

    check-cast v1, LX/GDm;

    invoke-static {p0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    invoke-direct {v3, v0, v1, v2}, LX/GL7;-><init>(LX/GDk;LX/GDm;Landroid/view/inputmethod/InputMethodManager;)V

    .line 2342094
    return-object v3
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2342095
    iget-object v0, p0, LX/GL7;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

    new-instance v1, LX/GL3;

    invoke-direct {v1, p0}, LX/GL3;-><init>(LX/GL7;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;->setSaveAudienceEditTextViewListener(Landroid/text/TextWatcher;)V

    .line 2342096
    iget-object v0, p0, LX/GL7;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

    new-instance v1, LX/GL4;

    invoke-direct {v1, p0}, LX/GL4;-><init>(LX/GL7;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;->setSaveAudienceCheckBoxListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 2342097
    iget-object v0, p0, LX/GL7;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

    new-instance v1, LX/GL6;

    invoke-direct {v1, p0}, LX/GL6;-><init>(LX/GL7;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;->setSaveButtonListener(Landroid/view/View$OnClickListener;)V

    .line 2342098
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2342099
    invoke-super {p0}, LX/GHg;->a()V

    .line 2342100
    const/4 v0, 0x0

    iput-object v0, p0, LX/GL7;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

    .line 2342101
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2342102
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

    invoke-virtual {p0, p1, p2}, LX/GL7;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2342103
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2342104
    iput-object p1, p0, LX/GL7;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2342105
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 3
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2342106
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2342107
    iput-object p1, p0, LX/GL7;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

    .line 2342108
    iget-object v0, p0, LX/GL7;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2342109
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v1

    .line 2342110
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/GL7;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2342111
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v1

    .line 2342112
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-eq v0, v1, :cond_1

    .line 2342113
    :cond_0
    :goto_0
    return-void

    .line 2342114
    :cond_1
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2342115
    iget-object v1, v0, LX/GCE;->e:LX/0ad;

    move-object v0, v1

    .line 2342116
    sget v1, LX/GDK;->k:I

    invoke-interface {v0, v1, v2}, LX/0ad;->a(II)I

    move-result v0

    if-eqz v0, :cond_0

    .line 2342117
    iget-object v0, p0, LX/GL7;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;->setVisibility(I)V

    .line 2342118
    invoke-direct {p0}, LX/GL7;->b()V

    goto :goto_0
.end method
