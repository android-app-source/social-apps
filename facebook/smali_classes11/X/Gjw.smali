.class public final LX/Gjw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;


# direct methods
.method public constructor <init>(Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 2388632
    iput-object p1, p0, LX/Gjw;->b:Lcom/facebook/greetingcards/render/templatefetch/FetchTemplateExecutor;

    iput-object p2, p0, LX/Gjw;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2388633
    iget-object v0, p0, LX/Gjw;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2388634
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2388635
    iget-object v0, p0, LX/Gjw;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, -0x6c62d74b

    invoke-static {v0, p1, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2388636
    return-void
.end method
