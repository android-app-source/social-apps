.class public LX/FOb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final b:Z

.field public final c:LX/FOZ;

.field public final d:LX/FOZ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final e:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/IdentifierLookupCallType;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final f:Ljava/lang/String;
    .annotation build Lcom/facebook/graphql/calls/TimespanCategory;
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/FOc;)V
    .locals 1

    .prologue
    .line 2235385
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2235386
    iget-object v0, p1, LX/FOc;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2235387
    iput-object v0, p0, LX/FOb;->a:Ljava/lang/String;

    .line 2235388
    iget-boolean v0, p1, LX/FOc;->b:Z

    move v0, v0

    .line 2235389
    iput-boolean v0, p0, LX/FOb;->b:Z

    .line 2235390
    iget-object v0, p1, LX/FOc;->c:LX/FOZ;

    move-object v0, v0

    .line 2235391
    iput-object v0, p0, LX/FOb;->c:LX/FOZ;

    .line 2235392
    iget-object v0, p1, LX/FOc;->d:LX/FOZ;

    move-object v0, v0

    .line 2235393
    iput-object v0, p0, LX/FOb;->d:LX/FOZ;

    .line 2235394
    iget-object v0, p1, LX/FOc;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2235395
    iput-object v0, p0, LX/FOb;->e:Ljava/lang/String;

    .line 2235396
    iget-object v0, p1, LX/FOc;->f:Ljava/lang/String;

    move-object v0, v0

    .line 2235397
    iput-object v0, p0, LX/FOb;->f:Ljava/lang/String;

    .line 2235398
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2235380
    instance-of v1, p1, LX/FOb;

    if-nez v1, :cond_1

    .line 2235381
    :cond_0
    :goto_0
    return v0

    .line 2235382
    :cond_1
    check-cast p1, LX/FOb;

    .line 2235383
    iget-object v1, p0, LX/FOb;->a:Ljava/lang/String;

    iget-object v2, p1, LX/FOb;->a:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, LX/FOb;->b:Z

    iget-boolean v2, p1, LX/FOb;->b:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, LX/FOb;->c:LX/FOZ;

    iget-object v2, p1, LX/FOb;->c:LX/FOZ;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/FOb;->e:Ljava/lang/String;

    iget-object v2, p1, LX/FOb;->e:Ljava/lang/String;

    invoke-static {v1, v2}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2235384
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/FOb;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-boolean v2, p0, LX/FOb;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, LX/FOb;->c:LX/FOZ;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, LX/FOb;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
