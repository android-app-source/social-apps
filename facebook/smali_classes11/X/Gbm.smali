.class public final enum LX/Gbm;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gbm;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gbm;

.field public static final enum CATEGORY:LX/Gbm;

.field public static final enum PREFERENCE:LX/Gbm;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2370401
    new-instance v0, LX/Gbm;

    const-string v1, "CATEGORY"

    invoke-direct {v0, v1, v2}, LX/Gbm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gbm;->CATEGORY:LX/Gbm;

    .line 2370402
    new-instance v0, LX/Gbm;

    const-string v1, "PREFERENCE"

    invoke-direct {v0, v1, v3}, LX/Gbm;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gbm;->PREFERENCE:LX/Gbm;

    .line 2370403
    const/4 v0, 0x2

    new-array v0, v0, [LX/Gbm;

    sget-object v1, LX/Gbm;->CATEGORY:LX/Gbm;

    aput-object v1, v0, v2

    sget-object v1, LX/Gbm;->PREFERENCE:LX/Gbm;

    aput-object v1, v0, v3

    sput-object v0, LX/Gbm;->$VALUES:[LX/Gbm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2370404
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gbm;
    .locals 1

    .prologue
    .line 2370405
    const-class v0, LX/Gbm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gbm;

    return-object v0
.end method

.method public static values()[LX/Gbm;
    .locals 1

    .prologue
    .line 2370406
    sget-object v0, LX/Gbm;->$VALUES:[LX/Gbm;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gbm;

    return-object v0
.end method
