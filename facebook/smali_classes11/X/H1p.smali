.class public LX/H1p;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/H1p;


# instance fields
.field public final a:LX/0lC;


# direct methods
.method public constructor <init>(LX/0lC;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2415879
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2415880
    iput-object p1, p0, LX/H1p;->a:LX/0lC;

    .line 2415881
    return-void
.end method

.method public static a(LX/0QB;)LX/H1p;
    .locals 4

    .prologue
    .line 2415882
    sget-object v0, LX/H1p;->b:LX/H1p;

    if-nez v0, :cond_1

    .line 2415883
    const-class v1, LX/H1p;

    monitor-enter v1

    .line 2415884
    :try_start_0
    sget-object v0, LX/H1p;->b:LX/H1p;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2415885
    if-eqz v2, :cond_0

    .line 2415886
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2415887
    new-instance p0, LX/H1p;

    invoke-static {v0}, LX/0l8;->a(LX/0QB;)LX/0lB;

    move-result-object v3

    check-cast v3, LX/0lC;

    invoke-direct {p0, v3}, LX/H1p;-><init>(LX/0lC;)V

    .line 2415888
    move-object v0, p0

    .line 2415889
    sput-object v0, LX/H1p;->b:LX/H1p;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2415890
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2415891
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2415892
    :cond_1
    sget-object v0, LX/H1p;->b:LX/H1p;

    return-object v0

    .line 2415893
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2415894
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static c(Ljava/util/Set;)Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable",
            "<-TT;>;>(",
            "Ljava/util/Set",
            "<TT;>;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 2415895
    const-string v0, ","

    invoke-static {v0}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v0

    .line 2415896
    invoke-static {p0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v1

    .line 2415897
    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 2415898
    invoke-virtual {v0, v1}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(Ljava/lang/String;)Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2415899
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v2

    .line 2415900
    const-string v0, ","

    invoke-static {v0}, LX/2Cb;->on(Ljava/lang/String;)LX/2Cb;

    move-result-object v0

    invoke-virtual {v0, p0}, LX/2Cb;->split(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-static {v0}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v3

    .line 2415901
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2415902
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2415903
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2415904
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2415905
    :cond_1
    return-object v2
.end method

.method public static e(Ljava/lang/String;)Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2415906
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v1

    .line 2415907
    const-string v0, ","

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 2415908
    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 2415909
    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2415910
    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2415911
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2415912
    :cond_1
    return-object v1
.end method
