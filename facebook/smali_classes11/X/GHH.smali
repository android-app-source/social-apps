.class public LX/GHH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GGc;
.implements LX/GGk;


# instance fields
.field public volatile i:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/GDm;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/GCB;

.field public k:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field private l:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/GCB;LX/GKj;)V
    .locals 5
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2334939
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2334940
    iput-object p1, p0, LX/GHH;->j:LX/GCB;

    .line 2334941
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    new-instance v1, LX/GEZ;

    const v2, 0x7f030074

    sget-object v3, LX/GHH;->a:LX/GGX;

    sget-object v4, LX/8wK;->PACING:LX/8wK;

    invoke-direct {v1, v2, p2, v3, v4}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/GHH;->l:LX/0Px;

    .line 2334942
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2334935
    iget-object v0, p0, LX/GHH;->l:LX/0Px;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;LX/GCY;)V
    .locals 1

    .prologue
    .line 2334943
    const-string v0, "data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iput-object v0, p0, LX/GHH;->k:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2334944
    iget-object v0, p0, LX/GHH;->k:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-interface {p2, v0}, LX/GCY;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2334945
    return-void
.end method

.method public final b()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;
    .locals 1

    .prologue
    .line 2334938
    iget-object v0, p0, LX/GHH;->j:LX/GCB;

    invoke-virtual {v0}, LX/GCB;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/GGh;
    .locals 2

    .prologue
    .line 2334936
    iget-object v0, p0, LX/GHH;->i:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GDm;

    .line 2334937
    new-instance v1, LX/GHG;

    invoke-direct {v1, p0, v0}, LX/GHG;-><init>(LX/GHH;LX/GDm;)V

    return-object v1
.end method
