.class public LX/GHN;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GGc;
.implements LX/GGk;


# instance fields
.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/GGX;

.field public k:LX/GGX;

.field public l:LX/GGX;

.field public final m:LX/GDm;

.field private final n:LX/GCB;

.field public o:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field private p:LX/0ad;


# direct methods
.method public constructor <init>(LX/GDm;LX/GCB;LX/GJg;LX/GKo;LX/GLd;LX/GIT;LX/0ad;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2334957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2334958
    new-instance v0, LX/GHI;

    invoke-direct {v0, p0}, LX/GHI;-><init>(LX/GHN;)V

    iput-object v0, p0, LX/GHN;->j:LX/GGX;

    .line 2334959
    new-instance v0, LX/GHJ;

    invoke-direct {v0, p0}, LX/GHJ;-><init>(LX/GHN;)V

    iput-object v0, p0, LX/GHN;->k:LX/GGX;

    .line 2334960
    new-instance v0, LX/GHK;

    invoke-direct {v0, p0}, LX/GHK;-><init>(LX/GHN;)V

    iput-object v0, p0, LX/GHN;->l:LX/GGX;

    .line 2334961
    iput-object p1, p0, LX/GHN;->m:LX/GDm;

    .line 2334962
    iput-object p2, p0, LX/GHN;->n:LX/GCB;

    .line 2334963
    iput-object p7, p0, LX/GHN;->p:LX/0ad;

    .line 2334964
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    new-instance v1, LX/GEZ;

    const v2, 0x7f030067

    sget-object v3, LX/GHN;->a:LX/GGX;

    sget-object v4, LX/8wK;->INFO_CARD:LX/8wK;

    invoke-direct {v1, v2, p3, v3, v4}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/GEZ;

    const v2, 0x7f03008c

    iget-object v3, p0, LX/GHN;->j:LX/GGX;

    sget-object v4, LX/8wK;->TARGETING:LX/8wK;

    invoke-direct {v1, v2, p4, v3, v4}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/GEZ;

    const v2, 0x7f030094

    iget-object v3, p0, LX/GHN;->k:LX/GGX;

    sget-object v4, LX/8wK;->TARGETING:LX/8wK;

    invoke-direct {v1, v2, p5, v3, v4}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/GEZ;

    const v2, 0x7f030085

    iget-object v3, p0, LX/GHN;->l:LX/GGX;

    sget-object v4, LX/8wK;->TARGETING:LX/8wK;

    invoke-direct {v1, v2, p6, v3, v4}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/GHN;->i:LX/0Px;

    .line 2334965
    return-void
.end method

.method public static synthetic a(LX/GHN;LX/8wL;)Z
    .locals 1

    .prologue
    .line 2334966
    sget-object v0, LX/GHM;->a:[I

    invoke-virtual {p1}, LX/8wL;->ordinal()I

    move-result p0

    aget v0, v0, p0

    packed-switch v0, :pswitch_data_0

    .line 2334967
    const/4 v0, 0x0

    :goto_0
    move v0, v0

    .line 2334968
    return v0

    .line 2334969
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static b$redex0(LX/GHN;LX/8wL;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2334970
    sget-object v1, LX/GHM;->a:[I

    invoke-virtual {p1}, LX/8wL;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2334971
    :cond_0
    :goto_0
    return v0

    .line 2334972
    :pswitch_0
    iget-object v1, p0, LX/GHN;->p:LX/0ad;

    sget v2, LX/GDK;->k:I

    invoke-interface {v1, v2, v0}, LX/0ad;->a(II)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2334973
    iget-object v0, p0, LX/GHN;->i:LX/0Px;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;LX/GCY;)V
    .locals 1

    .prologue
    .line 2334974
    const-string v0, "data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iput-object v0, p0, LX/GHN;->o:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2334975
    iget-object v0, p0, LX/GHN;->o:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-interface {p2, v0}, LX/GCY;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2334976
    return-void
.end method

.method public final b()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;
    .locals 1

    .prologue
    .line 2334977
    iget-object v0, p0, LX/GHN;->n:LX/GCB;

    invoke-virtual {v0}, LX/GCB;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/GGh;
    .locals 1

    .prologue
    .line 2334978
    new-instance v0, LX/GHL;

    invoke-direct {v0, p0}, LX/GHL;-><init>(LX/GHN;)V

    return-object v0
.end method
