.class public final LX/F3B;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;)V
    .locals 0

    .prologue
    .line 2193786
    iput-object p1, p0, LX/F3B;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x7fc408c7

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2193787
    iget-object v1, p0, LX/F3B;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    invoke-static {v1}, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->c(Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2193788
    iget-object v1, p0, LX/F3B;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    iget-object v2, p0, LX/F3B;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0831d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/F3B;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0831d1

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->a$redex0(Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2193789
    :goto_0
    const v1, 0xefdaf9c

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2193790
    :cond_0
    iget-object v1, p0, LX/F3B;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2193791
    invoke-static {v1}, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->c(Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;)Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v3

    .line 2193792
    :goto_1
    move v1, v2

    .line 2193793
    if-eqz v1, :cond_1

    .line 2193794
    iget-object v1, p0, LX/F3B;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    iget-object v1, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->c:LX/F2L;

    iget-object v2, p0, LX/F3B;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    iget-object v2, v2, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->f:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->q()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, LX/F3B;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    iget-object v3, v3, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->h:LX/F3T;

    .line 2193795
    iget-object v4, v3, LX/F3T;->b:Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    move-object v3, v4

    .line 2193796
    invoke-virtual {v3}, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/F3B;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    iget-object v4, v4, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->j:LX/F3A;

    .line 2193797
    new-instance v5, LX/4Fk;

    invoke-direct {v5}, LX/4Fk;-><init>()V

    .line 2193798
    invoke-virtual {v5, v3}, LX/4Fk;->j(Ljava/lang/String;)LX/4Fk;

    .line 2193799
    new-instance p0, LX/F2F;

    invoke-direct {p0, v1, v4}, LX/F2F;-><init>(LX/F2L;LX/F3A;)V

    .line 2193800
    invoke-static {v1, v2, v5, p0}, LX/F2L;->a(LX/F2L;Ljava/lang/String;LX/4Fk;LX/0TF;)V

    .line 2193801
    goto :goto_0

    .line 2193802
    :cond_1
    iget-object v1, p0, LX/F3B;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    goto :goto_0

    .line 2193803
    :cond_2
    iget-object v2, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->f:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->p()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-nez v2, :cond_3

    move v2, v4

    :goto_2
    if-eqz v2, :cond_6

    move v2, v4

    :goto_3
    if-eqz v2, :cond_8

    move v2, v4

    .line 2193804
    goto :goto_1

    .line 2193805
    :cond_3
    iget-object v2, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->f:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->p()LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const-class p1, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    invoke-virtual {v5, v2, v3, p1}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v2

    .line 2193806
    if-eqz v2, :cond_4

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    :goto_4
    if-nez v2, :cond_5

    move v2, v4

    goto :goto_2

    .line 2193807
    :cond_4
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2193808
    goto :goto_4

    :cond_5
    move v2, v3

    goto :goto_2

    .line 2193809
    :cond_6
    iget-object v2, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->f:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->p()LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const-class p1, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    invoke-virtual {v5, v2, v3, p1}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v2

    .line 2193810
    if-eqz v2, :cond_7

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    :goto_5
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    goto :goto_3

    .line 2193811
    :cond_7
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2193812
    goto :goto_5

    .line 2193813
    :cond_8
    iget-object v2, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->f:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v2}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->p()LX/1vs;

    move-result-object v2

    iget-object v5, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    const-class p1, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    invoke-virtual {v5, v2, v3, p1}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v2

    if-eqz v2, :cond_9

    invoke-static {v2}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v2

    :goto_6
    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    .line 2193814
    iget-object v5, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->h:LX/F3T;

    .line 2193815
    iget-object p1, v5, LX/F3T;->b:Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;

    move-object v5, p1

    .line 2193816
    invoke-virtual {v5}, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v5

    invoke-virtual {v2}, Lcom/facebook/groups/editsettings/protocol/GroupPurposeFragmentModels$GroupPurposeModel;->j()Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;

    move-result-object v2

    invoke-virtual {v5, v2}, Lcom/facebook/graphql/enums/GraphQLGroupPurposeType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v2, v4

    goto/16 :goto_1

    .line 2193817
    :cond_9
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2193818
    goto :goto_6

    :cond_a
    move v2, v3

    .line 2193819
    goto/16 :goto_1
.end method
