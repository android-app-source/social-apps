.class public LX/FwT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:J

.field public b:Landroid/support/v4/app/Fragment;

.field private c:I

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BQc;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BQS;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;"
        }
    .end annotation
.end field

.field private final i:LX/0ad;

.field private final j:LX/B5y;

.field private final k:LX/03V;

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/BQX;",
            ">;"
        }
    .end annotation
.end field

.field public final m:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/B3r;",
            ">;"
        }
    .end annotation
.end field

.field private final n:LX/2ua;


# direct methods
.method public constructor <init>(Ljava/lang/Long;Landroid/support/v4/app/Fragment;ILX/0ad;LX/0Or;LX/0Or;LX/0Or;LX/0Ot;LX/0Ot;LX/B5y;LX/03V;LX/0Ot;LX/0Or;LX/2ua;)V
    .locals 4
    .param p1    # Ljava/lang/Long;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/support/v4/app/Fragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "Landroid/support/v4/app/Fragment;",
            "I",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/auth/viewercontext/ViewerContext;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BQc;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/BQS;",
            ">;",
            "LX/B5y;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Ot",
            "<",
            "LX/BQX;",
            ">;",
            "LX/0Or",
            "<",
            "LX/B3r;",
            ">;",
            "LX/2ua;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2303216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2303217
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, p0, LX/FwT;->a:J

    .line 2303218
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v4/app/Fragment;

    iput-object v2, p0, LX/FwT;->b:Landroid/support/v4/app/Fragment;

    .line 2303219
    iget-object v2, p0, LX/FwT;->b:Landroid/support/v4/app/Fragment;

    instance-of v2, v2, LX/0fg;

    invoke-static {v2}, LX/0PB;->checkArgument(Z)V

    .line 2303220
    iput p3, p0, LX/FwT;->c:I

    .line 2303221
    iput-object p4, p0, LX/FwT;->i:LX/0ad;

    .line 2303222
    iput-object p5, p0, LX/FwT;->d:LX/0Or;

    .line 2303223
    iput-object p6, p0, LX/FwT;->g:LX/0Or;

    .line 2303224
    iput-object p7, p0, LX/FwT;->h:LX/0Or;

    .line 2303225
    iput-object p8, p0, LX/FwT;->e:LX/0Ot;

    .line 2303226
    iput-object p9, p0, LX/FwT;->f:LX/0Ot;

    .line 2303227
    iput-object p10, p0, LX/FwT;->j:LX/B5y;

    .line 2303228
    iput-object p11, p0, LX/FwT;->k:LX/03V;

    .line 2303229
    move-object/from16 v0, p12

    iput-object v0, p0, LX/FwT;->l:LX/0Ot;

    .line 2303230
    move-object/from16 v0, p13

    iput-object v0, p0, LX/FwT;->m:LX/0Or;

    .line 2303231
    move-object/from16 v0, p14

    iput-object v0, p0, LX/FwT;->n:LX/2ua;

    .line 2303232
    return-void
.end method

.method private a(LX/5SD;IZZZ)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2303195
    iget-object v0, p0, LX/FwT;->i:LX/0ad;

    sget-short v1, LX/0wf;->az:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v4

    .line 2303196
    iget-object v0, p0, LX/FwT;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v1, p0, LX/FwT;->b:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v5

    if-eqz v4, :cond_2

    sget-object v1, LX/0ax;->bY:Ljava/lang/String;

    :goto_0
    invoke-interface {v0, v5, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 2303197
    const-string v0, "extra_photo_tab_mode_params"

    iget-wide v6, p0, LX/FwT;->a:J

    invoke-static {p1, v6, v7}, Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;->a(LX/5SD;J)Lcom/facebook/ipc/profile/TimelinePhotoTabModeParams;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2303198
    const-string v0, "disable_adding_photos_to_albums"

    invoke-virtual {v5, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2303199
    const-string v0, "extra_should_merge_camera_roll"

    invoke-virtual {v5, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2303200
    const-string v6, "extra_simple_picker_launcher_configuration"

    if-nez p5, :cond_3

    move v1, v2

    :goto_1
    if-eqz p5, :cond_4

    sget-object v0, LX/8A9;->NONE:LX/8A9;

    :goto_2
    invoke-static {p5, v1, v0}, LX/BQ2;->a(ZZLX/8A9;)Lcom/facebook/ipc/simplepicker/SimplePickerLauncherConfiguration;

    move-result-object v0

    invoke-virtual {v5, v6, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2303201
    if-nez p5, :cond_0

    if-eqz p3, :cond_0

    .line 2303202
    if-eqz p4, :cond_5

    .line 2303203
    iget-object v0, p0, LX/FwT;->i:LX/0ad;

    sget-short v1, LX/0wf;->aD:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 2303204
    iget-object v0, p0, LX/FwT;->i:LX/0ad;

    sget-short v6, LX/0wf;->aB:S

    invoke-interface {v0, v6, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    .line 2303205
    :goto_3
    const-string v3, "extra_should_show_suggested_photos"

    invoke-virtual {v5, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2303206
    const-string v1, "extra_should_show_suggested_photos_before_camera_roll"

    invoke-virtual {v5, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2303207
    :cond_0
    if-eqz v4, :cond_1

    .line 2303208
    const-string v0, "title"

    iget-object v1, p0, LX/FwT;->b:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v3, 0x7f0815d3

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2303209
    const-string v0, "extra_cancel_button_enabled"

    invoke-virtual {v5, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2303210
    :cond_1
    iget-object v0, p0, LX/FwT;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v1, p0, LX/FwT;->b:Landroid/support/v4/app/Fragment;

    invoke-interface {v0, v5, p2, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2303211
    return-void

    .line 2303212
    :cond_2
    sget-object v1, LX/0ax;->ca:Ljava/lang/String;

    goto :goto_0

    :cond_3
    move v1, v3

    .line 2303213
    goto :goto_1

    :cond_4
    sget-object v0, LX/8A9;->LAUNCH_PROFILE_PIC_EDIT_GALLERY:LX/8A9;

    goto :goto_2

    .line 2303214
    :cond_5
    iget-object v0, p0, LX/FwT;->i:LX/0ad;

    sget-short v1, LX/0wf;->aH:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 2303215
    iget-object v0, p0, LX/FwT;->i:LX/0ad;

    sget-short v6, LX/0wf;->aG:S

    invoke-interface {v0, v6, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    goto :goto_3
.end method

.method private a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;Ljava/lang/String;)V
    .locals 10
    .param p4    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Lcom/facebook/share/model/ComposerAppAttribution;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2303189
    invoke-virtual {p1}, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->d()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->d()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2303190
    invoke-virtual {p1}, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->d()Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDisplayUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    .line 2303191
    :goto_0
    iget-object v0, p0, LX/FwT;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQX;

    invoke-virtual {p1}, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/BQX;->a(Landroid/net/Uri;Ljava/lang/String;)V

    .line 2303192
    iget-object v0, p0, LX/FwT;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQc;

    iget-object v1, p0, LX/FwT;->h:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    invoke-virtual/range {v0 .. v8}, LX/BQc;->a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;Ljava/lang/String;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 2303193
    return-void

    .line 2303194
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a()Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2303186
    iget-object v0, p0, LX/FwT;->b:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, LX/8AA;

    sget-object v2, LX/8AB;->COVERPHOTO:LX/8AB;

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v1}, LX/8AA;->i()LX/8AA;

    move-result-object v1

    invoke-virtual {v1}, LX/8AA;->j()LX/8AA;

    move-result-object v1

    sget-object v2, LX/8A9;->NONE:LX/8A9;

    invoke-virtual {v1, v2}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v1

    .line 2303187
    iget-object v0, p0, LX/FwT;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0xc34

    iget-object v3, p0, LX/FwT;->b:Landroid/support/v4/app/Fragment;

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2303188
    return-void
.end method

.method public final a(J)V
    .locals 6

    .prologue
    .line 2303175
    new-instance v0, LX/8AA;

    sget-object v1, LX/8AB;->PROFILEPIC:LX/8AB;

    invoke-direct {v0, v1}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v0}, LX/8AA;->i()LX/8AA;

    move-result-object v0

    .line 2303176
    iput-wide p1, v0, LX/8AA;->c:J

    .line 2303177
    move-object v0, v0

    .line 2303178
    sget-object v1, LX/8A9;->LAUNCH_PROFILE_PIC_EDIT_GALLERY:LX/8A9;

    invoke-virtual {v0, v1}, LX/8AA;->a(LX/8A9;)LX/8AA;

    move-result-object v0

    .line 2303179
    iget v1, p0, LX/FwT;->c:I

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/FwT;->n:LX/2ua;

    invoke-virtual {v1}, LX/2ua;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2303180
    :cond_0
    invoke-virtual {v0}, LX/8AA;->j()LX/8AA;

    .line 2303181
    :cond_1
    iget v1, p0, LX/FwT;->c:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    iget-object v1, p0, LX/FwT;->i:LX/0ad;

    sget-short v2, LX/0wf;->aE:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2303182
    invoke-virtual {v0}, LX/8AA;->k()LX/8AA;

    .line 2303183
    :cond_2
    iget-object v1, p0, LX/FwT;->b:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v1

    .line 2303184
    iget-object v0, p0, LX/FwT;->g:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v2, 0xc35

    iget-object v3, p0, LX/FwT;->b:Landroid/support/v4/app/Fragment;

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/support/v4/app/Fragment;)V

    .line 2303185
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 12

    .prologue
    const/4 v7, 0x0

    .line 2303173
    iget-object v1, p0, LX/FwT;->j:LX/B5y;

    iget-object v0, p0, LX/FwT;->b:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x3

    const/16 v6, 0x82

    const-wide/16 v10, 0x0

    move-object v8, v7

    move-object v9, v7

    invoke-interface/range {v1 .. v11}, LX/B5y;->a(Landroid/app/Activity;Ljava/lang/String;Landroid/net/Uri;IILX/5QV;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;J)V

    .line 2303174
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    .line 2303151
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "extra_profile_pic_expiration"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2303152
    :cond_0
    :goto_0
    return-void

    .line 2303153
    :cond_1
    const-string v0, "profile_photo_method_extra"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2303154
    iget-object v0, p0, LX/FwT;->k:LX/03V;

    const-string v1, "timeline_logging"

    const-string v2, "profile_photo_method_not_set"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2303155
    :cond_2
    :goto_1
    sget-object v0, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;

    .line 2303156
    const-string v0, "extra_profile_pic_expiration"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2303157
    const-string v0, "extra_profile_pic_caption"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2303158
    const-string v0, "profile_photo_method_extra"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2303159
    const-string v0, "extra_app_attribution"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v6

    check-cast v6, Lcom/facebook/share/model/ComposerAppAttribution;

    .line 2303160
    const-string v0, "extra_msqrd_mask_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2303161
    iget-object v0, v1, Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;->e:Lcom/facebook/photos/creativeediting/model/CreativeEditingData;

    move-object v0, v0

    .line 2303162
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2303163
    const-string p1, "existing"

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_6

    .line 2303164
    :cond_3
    :goto_2
    move v0, v8

    .line 2303165
    if-eqz v0, :cond_5

    .line 2303166
    iget-object v0, p0, LX/FwT;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BQS;

    invoke-virtual {v0, v1, v2, v3}, LX/BQS;->a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;J)V

    goto :goto_0

    .line 2303167
    :cond_4
    const-string v0, "profile_photo_method_extra"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2303168
    iget-object v0, p0, LX/FwT;->k:LX/03V;

    const-string v1, "timeline_logging"

    const-string v2, "profile_photo_method_unknown"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    move-object v0, p0

    .line 2303169
    invoke-direct/range {v0 .. v7}, LX/FwT;->a(Lcom/facebook/ipc/editgallery/EditGalleryIpcBundle;JLjava/lang/String;Ljava/lang/String;Lcom/facebook/share/model/ComposerAppAttribution;Ljava/lang/String;)V

    goto :goto_0

    .line 2303170
    :cond_6
    if-eqz v0, :cond_7

    .line 2303171
    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getStickerParams()LX/0Px;

    move-result-object p1

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getTextParams()LX/0Px;

    move-result-object p1

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/photos/creativeediting/model/CreativeEditingData;->getDoodleParams()LX/0Px;

    move-result-object p1

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-static {v0}, LX/5iB;->e(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result p1

    if-nez p1, :cond_3

    invoke-static {v0}, LX/5iB;->f(Lcom/facebook/photos/creativeediting/model/CreativeEditingData;)Z

    move-result p1

    if-nez p1, :cond_3

    move v8, v9

    goto :goto_2

    :cond_7
    move v8, v9

    .line 2303172
    goto :goto_2
.end method

.method public final a(ZZ)V
    .locals 6

    .prologue
    .line 2303149
    sget-object v1, LX/5SD;->EDIT_COVER_PHOTO:LX/5SD;

    const/16 v2, 0x26bc

    const/4 v5, 0x1

    move-object v0, p0

    move v3, p1

    move v4, p2

    invoke-direct/range {v0 .. v5}, LX/FwT;->a(LX/5SD;IZZZ)V

    .line 2303150
    return-void
.end method

.method public final b(ZZ)V
    .locals 6

    .prologue
    .line 2303147
    sget-object v1, LX/5SD;->EDIT_PROFILE_PIC:LX/5SD;

    const/16 v2, 0x26bb

    const/4 v5, 0x0

    move-object v0, p0

    move v3, p1

    move v4, p2

    invoke-direct/range {v0 .. v5}, LX/FwT;->a(LX/5SD;IZZZ)V

    .line 2303148
    return-void
.end method
