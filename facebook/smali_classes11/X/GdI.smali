.class public LX/GdI;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2373505
    const/4 v0, 0x0

    sput-object v0, LX/GdI;->a:Ljava/util/HashSet;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2373506
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2373507
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v1, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 2373508
    const-string v0, "fil"

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2373509
    new-instance v0, Ljava/util/Locale;

    const-string v2, "tl"

    invoke-virtual {v1}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Ljava/util/Locale;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2373510
    :goto_0
    invoke-static {v0}, LX/0sI;->a(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 2373511
    sget-object v1, LX/GdI;->a:Ljava/util/HashSet;

    if-nez v1, :cond_5

    .line 2373512
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 2373513
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "locale_whitelist"

    const-string v4, "raw"

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 2373514
    if-nez v2, :cond_2

    .line 2373515
    sput-object v1, LX/GdI;->a:Ljava/util/HashSet;

    .line 2373516
    :cond_0
    :goto_1
    move-object v0, v0

    .line 2373517
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0

    .line 2373518
    :cond_2
    invoke-static {p0, v2}, LX/GdJ;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 2373519
    :try_start_0
    new-instance v3, LX/0lp;

    invoke-direct {v3}, LX/0lp;-><init>()V

    invoke-virtual {v3, v2}, LX/0lp;->a(Ljava/lang/String;)LX/15w;

    move-result-object v2

    .line 2373520
    :cond_3
    :goto_2
    invoke-virtual {v2}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_ARRAY:LX/15z;

    if-eq v3, v4, :cond_4

    .line 2373521
    invoke-virtual {v2}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->START_ARRAY:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2373522
    invoke-virtual {v2}, LX/15w;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 2373523
    :catch_0
    move-exception v1

    .line 2373524
    const-string v2, "React"

    const-string v3, "Could not parse locale_whitelist.json"

    invoke-static {v2, v3, v1}, LX/03J;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 2373525
    :cond_4
    :try_start_1
    sput-object v1, LX/GdI;->a:Ljava/util/HashSet;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 2373526
    :cond_5
    sget-object v1, LX/GdI;->a:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, LX/GdI;->a:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2373527
    const-string v0, "en_US"

    goto :goto_1
.end method
