.class public LX/FKv;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/service/model/RemoveMemberParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/FKo;


# direct methods
.method public constructor <init>(LX/0Or;LX/FKo;)V
    .locals 0
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/FKo;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2225677
    iput-object p1, p0, LX/FKv;->a:LX/0Or;

    .line 2225678
    iput-object p2, p0, LX/FKv;->b:LX/FKo;

    .line 2225679
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 8

    .prologue
    .line 2225680
    check-cast p1, Lcom/facebook/messaging/service/model/RemoveMemberParams;

    .line 2225681
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2225682
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "id"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "t_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2225683
    iget-object v3, p1, Lcom/facebook/messaging/service/model/RemoveMemberParams;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v3, v3

    .line 2225684
    invoke-virtual {v3}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225685
    const/4 v0, 0x0

    .line 2225686
    iget-object v1, p1, Lcom/facebook/messaging/service/model/RemoveMemberParams;->c:LX/0Px;

    move-object v1, v1

    .line 2225687
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_2

    .line 2225688
    :goto_0
    move v0, v0

    .line 2225689
    if-nez v0, :cond_1

    .line 2225690
    iget-object v0, p1, Lcom/facebook/messaging/service/model/RemoveMemberParams;->c:LX/0Px;

    move-object v0, v0

    .line 2225691
    new-instance v2, LX/162;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v2, v1}, LX/162;-><init>(LX/0mC;)V

    .line 2225692
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/user/model/UserFbidIdentifier;

    .line 2225693
    invoke-virtual {v1}, Lcom/facebook/user/model/UserFbidIdentifier;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, LX/162;->g(Ljava/lang/String;)LX/162;

    goto :goto_1

    .line 2225694
    :cond_0
    move-object v0, v2

    .line 2225695
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "to"

    invoke-virtual {v0}, LX/0lF;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225696
    :cond_1
    new-instance v0, LX/14N;

    const-string v1, "removeMembers"

    const-string v2, "DELETE"

    const-string v3, "/participants"

    sget-object v5, LX/14S;->STRING:LX/14S;

    invoke-direct/range {v0 .. v5}, LX/14N;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;LX/14S;)V

    return-object v0

    .line 2225697
    :cond_2
    iget-object v1, p1, Lcom/facebook/messaging/service/model/RemoveMemberParams;->c:LX/0Px;

    move-object v1, v1

    .line 2225698
    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserFbidIdentifier;

    invoke-virtual {v0}, Lcom/facebook/user/model/UserFbidIdentifier;->a()Ljava/lang/String;

    move-result-object v0

    .line 2225699
    iget-object v1, p0, LX/FKv;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2225700
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2225701
    const/4 v0, 0x0

    return-object v0
.end method
