.class public LX/FWZ;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0TD;

.field private final b:LX/0tX;

.field public final c:LX/FWJ;

.field private final d:LX/0sU;

.field private final e:I


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0TD;LX/0rq;LX/0tX;LX/FWJ;LX/0sU;)V
    .locals 1
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2252305
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2252306
    iput-object p2, p0, LX/FWZ;->a:LX/0TD;

    .line 2252307
    iput-object p4, p0, LX/FWZ;->b:LX/0tX;

    .line 2252308
    iput-object p5, p0, LX/FWZ;->c:LX/FWJ;

    .line 2252309
    iput-object p6, p0, LX/FWZ;->d:LX/0sU;

    .line 2252310
    const v0, 0x7f0b1159

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/FWZ;->e:I

    .line 2252311
    return-void
.end method

.method private a(LX/0am;LX/0am;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/0zS;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/FVy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2252299
    new-instance v0, LX/79y;

    invoke-direct {v0}, LX/79y;-><init>()V

    move-object v1, v0

    .line 2252300
    const-string v0, "first_count"

    const-string v2, "10"

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "saved_item_pic_width"

    iget v3, p0, LX/FWZ;->e:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v2, "saved_item_pic_height"

    iget v3, p0, LX/FWZ;->e:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "section_type"

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->ALL:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    invoke-virtual {p1, v0}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v2

    const-string v3, "after_cursor"

    invoke-virtual {p2}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2252301
    iget-object v0, p0, LX/FWZ;->d:LX/0sU;

    const-string v2, "SAVED_STORY"

    invoke-virtual {v0, v1, v2}, LX/0sU;->a(LX/0gW;Ljava/lang/String;)V

    .line 2252302
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const-wide/32 v2, 0x278d00

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 2252303
    iget-object v1, p0, LX/FWZ;->b:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2252304
    new-instance v1, LX/FWY;

    invoke-direct {v1, p0}, LX/FWY;-><init>(LX/FWZ;)V

    iget-object v2, p0, LX/FWZ;->a:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/FWZ;
    .locals 7

    .prologue
    .line 2252295
    new-instance v0, LX/FWZ;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, LX/0TD;

    invoke-static {p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v3

    check-cast v3, LX/0rq;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {p0}, LX/FWJ;->a(LX/0QB;)LX/FWJ;

    move-result-object v5

    check-cast v5, LX/FWJ;

    invoke-static {p0}, LX/0sU;->a(LX/0QB;)LX/0sU;

    move-result-object v6

    check-cast v6, LX/0sU;

    invoke-direct/range {v0 .. v6}, LX/FWZ;-><init>(Landroid/content/res/Resources;LX/0TD;LX/0rq;LX/0tX;LX/FWJ;LX/0sU;)V

    .line 2252296
    return-object v0
.end method


# virtual methods
.method public final a(LX/0am;LX/0am;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/FVy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2252298
    sget-object v0, LX/0zS;->d:LX/0zS;

    invoke-direct {p0, p1, p2, v0}, LX/FWZ;->a(LX/0am;LX/0am;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/0am;LX/0am;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;",
            ">;",
            "LX/0am",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/FVy;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2252297
    sget-object v0, LX/0zS;->b:LX/0zS;

    invoke-direct {p0, p1, p2, v0}, LX/FWZ;->a(LX/0am;LX/0am;LX/0zS;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
