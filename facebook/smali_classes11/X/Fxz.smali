.class public final LX/Fxz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:LX/Fy4;


# direct methods
.method public constructor <init>(LX/Fy4;J)V
    .locals 0

    .prologue
    .line 2306184
    iput-object p1, p0, LX/Fxz;->b:LX/Fy4;

    iput-wide p2, p0, LX/Fxz;->a:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2306187
    instance-of v0, p1, Ljava/util/concurrent/CancellationException;

    if-nez v0, :cond_0

    .line 2306188
    iget-object v0, p0, LX/Fxz;->b:LX/Fy4;

    iget-object v0, v0, LX/Fy4;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fsr;

    invoke-virtual {v0}, LX/Fsr;->lk_()V

    .line 2306189
    :cond_0
    iget-object v0, p0, LX/Fxz;->b:LX/Fy4;

    iget-object v0, v0, LX/Fy4;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08159a

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2306190
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2306185
    iget-object v0, p0, LX/Fxz;->b:LX/Fy4;

    iget-wide v2, p0, LX/Fxz;->a:J

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v1, v4}, LX/Fy4;->a$redex0(LX/Fy4;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 2306186
    return-void
.end method
