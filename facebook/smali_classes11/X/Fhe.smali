.class public LX/Fhe;
.super LX/Fhd;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/Fhe;


# instance fields
.field private final e:LX/Fhg;


# direct methods
.method public constructor <init>(LX/11i;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Uh;LX/0ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2273559
    invoke-direct {p0, p1, p2, p4}, LX/Fhd;-><init>(LX/11i;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V

    .line 2273560
    sget v0, LX/2SU;->e:I

    invoke-virtual {p3, v0}, LX/0Uh;->a(I)LX/03R;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2273561
    new-instance v0, LX/Fhg;

    const v1, 0x7001f

    const-string p1, "GraphSearchLocalSuggestionsTypeahead"

    invoke-direct {v0, v1, p1}, LX/Fhg;-><init>(ILjava/lang/String;)V

    move-object v0, v0

    .line 2273562
    :goto_0
    iput-object v0, p0, LX/Fhe;->e:LX/Fhg;

    .line 2273563
    return-void

    .line 2273564
    :cond_0
    new-instance v0, LX/Fhg;

    const v1, 0x70012

    const-string p1, "SimpleSearchLocalSuggestionsTypeahead"

    invoke-direct {v0, v1, p1}, LX/Fhg;-><init>(ILjava/lang/String;)V

    move-object v0, v0

    .line 2273565
    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/Fhe;
    .locals 7

    .prologue
    .line 2273566
    sget-object v0, LX/Fhe;->f:LX/Fhe;

    if-nez v0, :cond_1

    .line 2273567
    const-class v1, LX/Fhe;

    monitor-enter v1

    .line 2273568
    :try_start_0
    sget-object v0, LX/Fhe;->f:LX/Fhe;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2273569
    if-eqz v2, :cond_0

    .line 2273570
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2273571
    new-instance p0, LX/Fhe;

    invoke-static {v0}, LX/11h;->a(LX/0QB;)LX/11h;

    move-result-object v3

    check-cast v3, LX/11i;

    invoke-static {v0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v4

    check-cast v4, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-direct {p0, v3, v4, v5, v6}, LX/Fhe;-><init>(LX/11i;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0Uh;LX/0ad;)V

    .line 2273572
    move-object v0, p0

    .line 2273573
    sput-object v0, LX/Fhe;->f:LX/Fhe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2273574
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2273575
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2273576
    :cond_1
    sget-object v0, LX/Fhe;->f:LX/Fhe;

    return-object v0

    .line 2273577
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2273578
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/Fhg;
    .locals 1

    .prologue
    .line 2273579
    iget-object v0, p0, LX/Fhe;->e:LX/Fhg;

    return-object v0
.end method
