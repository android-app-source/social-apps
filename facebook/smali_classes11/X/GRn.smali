.class public final LX/GRn;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/appinvites/fragment/AppInvitesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/appinvites/fragment/AppInvitesFragment;Z)V
    .locals 0

    .prologue
    .line 2351672
    iput-object p1, p0, LX/GRn;->b:Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    iput-boolean p2, p0, LX/GRn;->a:Z

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2351673
    sget-object v0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->h:Ljava/lang/Class;

    const-string v1, "Fetch invites failed"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2351674
    iget-object v0, p0, LX/GRn;->b:Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    iget-object v0, v0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->o:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 2351675
    iget-object v0, p0, LX/GRn;->b:Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    iget-object v0, v0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->o:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v1, p0, LX/GRn;->b:Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f08006f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/GRm;

    invoke-direct {v2, p0}, LX/GRm;-><init>(LX/GRn;)V

    invoke-virtual {v0, v1, v2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a(Ljava/lang/String;LX/1DI;)V

    .line 2351676
    :goto_0
    return-void

    .line 2351677
    :cond_0
    iget-object v0, p0, LX/GRn;->b:Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    iget-object v0, v0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->m:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {v0}, LX/62l;->g()V

    goto :goto_0
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 14

    .prologue
    .line 2351678
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x0

    .line 2351679
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2351680
    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel;

    .line 2351681
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2351682
    iget-object v1, p0, LX/GRn;->b:Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    iget-object v1, v1, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->l:LX/GRX;

    invoke-virtual {v1}, LX/GRX;->b()V

    .line 2351683
    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel;->a()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel$ApplicationsWithApplicationRequestsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel$ApplicationsWithApplicationRequestsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v2

    .line 2351684
    :goto_0
    if-ge v4, v6, :cond_5

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel$ApplicationsWithApplicationRequestsModel$EdgesModel;

    .line 2351685
    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesListQueryModel$ApplicationsWithApplicationRequestsModel$EdgesModel;->a()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel;->j()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v3, v2

    .line 2351686
    :goto_1
    if-ge v3, v8, :cond_4

    invoke-virtual {v7, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel;

    .line 2351687
    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel;->a()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel;->a()LX/0Px;

    move-result-object v9

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v10

    move v1, v2

    .line 2351688
    :goto_2
    if-ge v1, v10, :cond_3

    invoke-virtual {v9, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel$ApplicationRequestsEdgesModel;

    .line 2351689
    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInvitesApplicationFieldsModel$ApplicationRequestsSendersModel$EdgesModel$ApplicationRequestsModel$ApplicationRequestsEdgesModel;->a()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v0

    .line 2351690
    if-eqz v0, :cond_1

    .line 2351691
    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->j()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;

    move-result-object v11

    if-eqz v11, :cond_8

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->c()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v11

    if-eqz v11, :cond_8

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->c()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->b()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$AndroidAppConfigModel;

    move-result-object v11

    if-eqz v11, :cond_8

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->c()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->b()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$AndroidAppConfigModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel$AndroidAppConfigModel;->a()Ljava/lang/String;

    move-result-object v11

    if-eqz v11, :cond_8

    const/4 v11, 0x1

    :goto_3
    move v11, v11

    .line 2351692
    if-eqz v11, :cond_2

    .line 2351693
    iget-object v11, p0, LX/GRn;->b:Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    iget-object v11, v11, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->l:LX/GRX;

    .line 2351694
    iget-object v12, v11, LX/GRX;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->c()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v13

    invoke-virtual {v13}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->d()Ljava/lang/String;

    move-result-object v13

    .line 2351695
    new-instance p1, LX/GRU;

    invoke-direct {p1, v11, v13}, LX/GRU;-><init>(LX/GRX;Ljava/lang/String;)V

    move-object v13, p1

    .line 2351696
    const/4 p1, 0x0

    invoke-static {v12, v13, p1}, LX/0Ph;->a(Ljava/lang/Iterable;LX/0Rl;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, LX/GRZ;

    .line 2351697
    if-nez v12, :cond_0

    .line 2351698
    new-instance v12, LX/GRZ;

    invoke-direct {v12}, LX/GRZ;-><init>()V

    .line 2351699
    iget-object v13, v11, LX/GRX;->a:Ljava/util/ArrayList;

    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2351700
    :cond_0
    iget-object v13, v12, LX/GRZ;->b:Ljava/util/ArrayList;

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2351701
    :cond_1
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 2351702
    :cond_2
    sget-object v11, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->h:Ljava/lang/Class;

    const-string v12, "Invite %s is missing required fields"

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->mv_()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v13, v2

    invoke-static {v11, v12, v13}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    .line 2351703
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_1

    .line 2351704
    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_0

    .line 2351705
    :cond_5
    iget-object v0, p0, LX/GRn;->b:Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    iget-object v0, v0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->l:LX/GRX;

    invoke-virtual {v0}, LX/GRX;->a()I

    move-result v0

    if-lez v0, :cond_6

    .line 2351706
    iget-object v0, p0, LX/GRn;->b:Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    .line 2351707
    iget-object v1, v0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->e:LX/0iA;

    new-instance v3, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v4, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->APP_INVITE_FEED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v3, v4}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v4, LX/3kZ;

    invoke-virtual {v1, v3, v4}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v1

    check-cast v1, LX/3kZ;

    .line 2351708
    if-eqz v1, :cond_6

    .line 2351709
    iget-object v3, v0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->q:Landroid/view/ViewStub;

    .line 2351710
    if-nez v3, :cond_9

    .line 2351711
    :goto_5
    iget-object v3, v0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->f:LX/GRy;

    .line 2351712
    const/4 v4, 0x1

    iput-boolean v4, v3, LX/GRy;->a:Z

    .line 2351713
    iget-object v3, v0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->e:LX/0iA;

    invoke-virtual {v3}, LX/0iA;->a()Lcom/facebook/interstitial/manager/InterstitialLogger;

    move-result-object v3

    invoke-virtual {v1}, LX/3kZ;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/facebook/interstitial/manager/InterstitialLogger;->a(Ljava/lang/String;)V

    .line 2351714
    :cond_6
    iget-object v0, p0, LX/GRn;->b:Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    iget-object v0, v0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->j:LX/GRM;

    const v1, -0x1abee523

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2351715
    iget-object v0, p0, LX/GRn;->b:Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    iget-object v0, v0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->o:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->setVisibility(I)V

    .line 2351716
    iget-object v0, p0, LX/GRn;->b:Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    iget-object v0, v0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->m:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {v0, v2}, Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;->setVisibility(I)V

    .line 2351717
    iget-object v0, p0, LX/GRn;->b:Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    iget-object v0, v0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->m:Lcom/facebook/widget/refreshableview/RefreshableListViewContainer;

    invoke-virtual {v0}, LX/62l;->f()V

    .line 2351718
    iget-object v0, p0, LX/GRn;->b:Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "invite_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2351719
    if-eqz v0, :cond_7

    iget-boolean v1, p0, LX/GRn;->a:Z

    if-eqz v1, :cond_7

    .line 2351720
    iget-object v1, p0, LX/GRn;->b:Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    iget-object v1, v1, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->n:Landroid/widget/ListView;

    iget-object v2, p0, LX/GRn;->b:Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    iget-object v2, v2, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->l:LX/GRX;

    .line 2351721
    :try_start_0
    iget-object v3, v2, LX/GRX;->a:Ljava/util/ArrayList;

    new-instance v4, LX/GRW;

    invoke-direct {v4, v2, v0}, LX/GRW;-><init>(LX/GRX;Ljava/lang/String;)V

    invoke-static {v3, v4}, LX/0Ph;->f(Ljava/lang/Iterable;LX/0Rl;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/GRZ;

    .line 2351722
    iget-object v4, v2, LX/GRX;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 2351723
    :goto_6
    move v0, v3

    .line 2351724
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setSelection(I)V

    .line 2351725
    :cond_7
    return-void

    :cond_8
    const/4 v11, 0x0

    goto/16 :goto_3

    .line 2351726
    :cond_9
    const v4, 0x7f0300f6

    invoke-virtual {v3, v4}, Landroid/view/ViewStub;->setLayoutResource(I)V

    .line 2351727
    invoke-virtual {v3}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v4

    .line 2351728
    const v5, 0x7f0d057d

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 2351729
    new-instance v6, LX/GRx;

    invoke-direct {v6, v4}, LX/GRx;-><init>(Landroid/view/View;)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_5

    :catch_0
    const/4 v3, 0x0

    goto :goto_6
.end method
