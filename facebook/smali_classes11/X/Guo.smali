.class public final LX/Guo;
.super LX/BWM;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 2403928
    iput-object p1, p0, LX/Guo;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403929
    invoke-direct {p0, p2}, LX/BWM;-><init>(Landroid/os/Handler;)V

    .line 2403930
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/webview/FacebookWebView;LX/BWN;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2403931
    iget-object v0, p0, LX/Guo;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403932
    iget-object v1, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v1, v1

    .line 2403933
    const-string v4, "callback"

    invoke-interface {p2, v1, v4}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2403934
    iput-object v1, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->J:Ljava/lang/String;

    .line 2403935
    iget-object v0, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2403936
    const-string v1, "target"

    const-wide/16 v4, -0x1

    invoke-interface {p2, v0, v1, v4, v5}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;J)J

    move-result-wide v4

    .line 2403937
    iget-object v0, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2403938
    const-string v1, "type"

    invoke-interface {p2, v0, v1, v8}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2403939
    if-eqz v0, :cond_5

    invoke-static {v0}, LX/2rw;->fromString(Ljava/lang/String;)LX/2rw;

    move-result-object v0

    .line 2403940
    :goto_0
    new-instance v6, LX/89I;

    invoke-direct {v6, v4, v5, v0}, LX/89I;-><init>(JLX/2rw;)V

    .line 2403941
    iget-object v0, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2403942
    const-string v1, "acts_as_target"

    const-string v4, "false"

    invoke-interface {p2, v0, v1, v4}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 2403943
    iget-object v0, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2403944
    const-string v1, "actor_profile_pic_uri"

    invoke-interface {p2, v0, v1}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2403945
    if-eqz v4, :cond_0

    .line 2403946
    iput-object v5, v6, LX/89I;->d:Ljava/lang/String;

    .line 2403947
    :cond_0
    iget-object v0, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2403948
    const-string v1, "title"

    invoke-interface {p2, v0, v1}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2403949
    if-nez v1, :cond_6

    const-string v0, ""

    .line 2403950
    :goto_1
    iput-object v0, v6, LX/89I;->c:Ljava/lang/String;

    .line 2403951
    sget-object v0, LX/21D;->FACEWEB:LX/21D;

    const-string v7, "facewebStatusButton"

    invoke-static {v0, v7}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-virtual {v6}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v6

    .line 2403952
    if-eqz v4, :cond_2

    .line 2403953
    invoke-static {}, Lcom/facebook/ipc/composer/intent/ComposerPageData;->newBuilder()Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v0

    if-nez v1, :cond_1

    const-string v1, ""

    :cond_1
    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageName(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPageProfilePicUrl(Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v1

    iget-object v0, p0, LX/Guo;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0SI;

    invoke-interface {v0}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->setPostAsPageViewerContext(Lcom/facebook/auth/viewercontext/ViewerContext;)Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/ipc/composer/intent/ComposerPageData$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerPageData;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialPageData(Lcom/facebook/ipc/composer/intent/ComposerPageData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2403954
    :cond_2
    iget-object v0, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2403955
    const-string v1, "enable_attach_to_album"

    invoke-interface {p2, v0, v1}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2403956
    if-eqz v0, :cond_3

    .line 2403957
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v2

    :goto_2
    invoke-virtual {v6, v0}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableAttachToAlbum(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2403958
    :cond_3
    iget-object v0, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2403959
    const-string v1, "enable_friend_tagging"

    invoke-interface {p2, v0, v1}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2403960
    if-eqz v0, :cond_4

    .line 2403961
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    :goto_3
    invoke-virtual {v6, v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setDisableFriendTagging(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    .line 2403962
    :cond_4
    iget-object v0, p0, LX/Guo;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->Z:LX/1Kf;

    invoke-virtual {v6}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v1

    const/16 v2, 0xa

    iget-object v3, p0, LX/Guo;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v3}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v3

    invoke-interface {v0, v8, v1, v2, v3}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2403963
    return-void

    .line 2403964
    :cond_5
    sget-object v0, LX/2rw;->OTHER:LX/2rw;

    goto/16 :goto_0

    :cond_6
    move-object v0, v1

    .line 2403965
    goto :goto_1

    :cond_7
    move v0, v3

    .line 2403966
    goto :goto_2

    :cond_8
    move v2, v3

    .line 2403967
    goto :goto_3
.end method
