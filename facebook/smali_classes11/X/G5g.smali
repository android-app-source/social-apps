.class public final LX/G5g;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field public final synthetic a:Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;


# direct methods
.method public constructor <init>(Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;)V
    .locals 0

    .prologue
    .line 2319096
    iput-object p1, p0, LX/G5g;->a:Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 2319095
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    .line 2319097
    iget-object v0, p0, LX/G5g;->a:Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;

    invoke-static {v0}, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->a(Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;)I

    .line 2319098
    iget-object v0, p0, LX/G5g;->a:Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;

    iget v0, v0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->n:I

    iget-object v1, p0, LX/G5g;->a:Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;

    iget v1, v1, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->c:I

    if-le v0, v1, :cond_0

    .line 2319099
    iget-object v0, p0, LX/G5g;->a:Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;

    const/4 v1, 0x0

    .line 2319100
    iput v1, v0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->n:I

    .line 2319101
    :cond_0
    iget-object v0, p0, LX/G5g;->a:Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;

    iget-object v0, v0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->i:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2319102
    iget-object v0, p0, LX/G5g;->a:Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;

    iget v0, v0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->n:I

    iget-object v1, p0, LX/G5g;->a:Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;

    iget v1, v1, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->c:I

    if-ne v0, v1, :cond_1

    .line 2319103
    iget-object v0, p0, LX/G5g;->a:Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;

    iget-object v0, v0, Lcom/facebook/uicontrib/loadingindicator/DotProgressIndicator;->j:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 2319104
    :cond_1
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 2319094
    return-void
.end method
