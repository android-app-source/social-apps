.class public LX/GNp;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "AdsPaymentsModule"
.end annotation


# instance fields
.field private final a:Lcom/facebook/content/SecureContextHelper;


# direct methods
.method public constructor <init>(LX/5pY;Lcom/facebook/content/SecureContextHelper;)V
    .locals 0
    .param p1    # LX/5pY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2346589
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2346590
    iput-object p2, p0, LX/GNp;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2346591
    return-void
.end method


# virtual methods
.method public checkoutDone(LX/5pG;)V
    .locals 7
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2346556
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    .line 2346557
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2346558
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2346559
    const-string v2, "checkout_payment_ids"

    const-string v3, "checkout_payment_ids"

    .line 2346560
    invoke-interface {p1, v3}, LX/5pG;->b(Ljava/lang/String;)LX/5pC;

    move-result-object v5

    .line 2346561
    invoke-interface {v5}, LX/5pC;->size()I

    move-result v4

    new-array v6, v4, [Ljava/lang/String;

    .line 2346562
    const/4 v4, 0x0

    :goto_0
    invoke-interface {v5}, LX/5pC;->size()I

    move-result p0

    if-ge v4, p0, :cond_0

    .line 2346563
    invoke-interface {v5, v4}, LX/5pC;->getString(I)Ljava/lang/String;

    move-result-object p0

    aput-object p0, v6, v4

    .line 2346564
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 2346565
    :cond_0
    move-object v3, v6

    .line 2346566
    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2346567
    const-string v2, "campaign_id"

    const-string v3, "campaign_id"

    invoke-interface {p1, v3}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2346568
    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2346569
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2346570
    return-void
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2346588
    const-string v0, "AdsPaymentsModule"

    return-object v0
.end method

.method public prepayFund(LX/5pG;)V
    .locals 7
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2346579
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    .line 2346580
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2346581
    invoke-static {p1}, LX/GQ6;->a(LX/5pG;)Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    move-result-object v1

    .line 2346582
    iget-object v6, p0, LX/GNp;->a:Lcom/facebook/content/SecureContextHelper;

    .line 2346583
    iget-object v2, v1, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->a:Lcom/facebook/payments/currency/CurrencyAmount;

    move-object v2, v2

    .line 2346584
    const/4 v3, 0x0

    .line 2346585
    const-string v4, "country"

    invoke-interface {p1, v4}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/common/locale/Country;->a(Ljava/lang/String;)Lcom/facebook/common/locale/Country;

    move-result-object v4

    move-object v4, v4

    .line 2346586
    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/common/util/Either;Lcom/facebook/common/locale/Country;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v6, v1, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2346587
    return-void
.end method

.method public saveCreditCard(LX/5pG;)V
    .locals 4
    .annotation runtime Lcom/facebook/react/bridge/ReactMethod;
    .end annotation

    .prologue
    .line 2346571
    invoke-virtual {p0}, LX/5pb;->t()Landroid/app/Activity;

    move-result-object v0

    .line 2346572
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2346573
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2346574
    const-string v2, "credential_id"

    const-string v3, "credential_id"

    invoke-interface {p1, v3}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2346575
    const-string v2, "cached_csc_token"

    const-string v3, "cached_csc_token"

    invoke-interface {p1, v3}, LX/5pG;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2346576
    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2346577
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2346578
    return-void
.end method
