.class public final LX/Fd0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:LX/Fd6;


# direct methods
.method public constructor <init>(LX/Fd6;)V
    .locals 0

    .prologue
    .line 2263001
    iput-object p1, p0, LX/Fd0;->a:LX/Fd6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2263002
    iget-object v0, p0, LX/Fd0;->a:LX/Fd6;

    iget-object v0, v0, LX/Fd6;->l:LX/Fc6;

    .line 2263003
    iget-object v1, v0, LX/Fc6;->h:LX/CwL;

    move-object v2, v1

    .line 2263004
    invoke-static {}, Lcom/facebook/search/results/protocol/filters/FilterValue;->g()LX/5ur;

    move-result-object v1

    iget-object v0, p0, LX/Fd0;->a:LX/Fd6;

    iget-object v0, v0, LX/Fd6;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/FilterValue;

    .line 2263005
    iget-object v3, v0, Lcom/facebook/search/results/protocol/filters/FilterValue;->a:Ljava/lang/String;

    move-object v0, v3

    .line 2263006
    iput-object v0, v1, LX/5ur;->a:Ljava/lang/String;

    .line 2263007
    move-object v1, v1

    .line 2263008
    iget-object v0, p0, LX/Fd0;->a:LX/Fd6;

    iget-object v0, v0, LX/Fd6;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/FilterValue;

    .line 2263009
    iget-object v3, v0, Lcom/facebook/search/results/protocol/filters/FilterValue;->b:Ljava/lang/String;

    move-object v0, v3

    .line 2263010
    iput-object v0, v1, LX/5ur;->b:Ljava/lang/String;

    .line 2263011
    move-object v1, v1

    .line 2263012
    iget-object v0, p0, LX/Fd0;->a:LX/Fd6;

    iget-object v0, v0, LX/Fd6;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/FilterValue;

    .line 2263013
    iget-boolean v3, v0, Lcom/facebook/search/results/protocol/filters/FilterValue;->d:Z

    move v0, v3

    .line 2263014
    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 2263015
    :goto_0
    iput-boolean v0, v1, LX/5ur;->d:Z

    .line 2263016
    move-object v1, v1

    .line 2263017
    iget-object v0, p0, LX/Fd0;->a:LX/Fd6;

    iget-object v0, v0, LX/Fd6;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/FilterValue;

    .line 2263018
    iget-boolean v3, v0, Lcom/facebook/search/results/protocol/filters/FilterValue;->f:Z

    move v0, v3

    .line 2263019
    iput-boolean v0, v1, LX/5ur;->f:Z

    .line 2263020
    move-object v1, v1

    .line 2263021
    iget-object v0, p0, LX/Fd0;->a:LX/Fd6;

    iget-object v0, v0, LX/Fd6;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/FilterValue;

    .line 2263022
    iget-object v3, v0, Lcom/facebook/search/results/protocol/filters/FilterValue;->e:Ljava/lang/String;

    move-object v0, v3

    .line 2263023
    iput-object v0, v1, LX/5ur;->e:Ljava/lang/String;

    .line 2263024
    move-object v0, v1

    .line 2263025
    invoke-virtual {v0}, LX/5ur;->f()Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-result-object v1

    .line 2263026
    new-instance v0, LX/CwK;

    invoke-direct {v0}, LX/CwK;-><init>()V

    .line 2263027
    iget-object v3, v2, LX/CwL;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2263028
    iput-object v3, v0, LX/CwK;->a:Ljava/lang/String;

    .line 2263029
    move-object v0, v0

    .line 2263030
    iget-object v3, v2, LX/CwL;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2263031
    iput-object v3, v0, LX/CwK;->b:Ljava/lang/String;

    .line 2263032
    move-object v0, v0

    .line 2263033
    iget-object v3, v2, LX/CwL;->c:Ljava/lang/String;

    move-object v2, v3

    .line 2263034
    iput-object v2, v0, LX/CwK;->c:Ljava/lang/String;

    .line 2263035
    move-object v2, v0

    .line 2263036
    iget-object v0, p0, LX/Fd0;->a:LX/Fd6;

    iget-object v0, v0, LX/Fd6;->a:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/FilterValue;

    .line 2263037
    iget-boolean v3, v0, Lcom/facebook/search/results/protocol/filters/FilterValue;->d:Z

    move v0, v3

    .line 2263038
    if-eqz v0, :cond_1

    const/4 v0, 0x0

    .line 2263039
    :goto_1
    iput-object v0, v2, LX/CwK;->e:Lcom/facebook/search/results/protocol/filters/FilterValue;

    .line 2263040
    move-object v0, v2

    .line 2263041
    invoke-virtual {v0}, LX/CwK;->a()LX/CwL;

    move-result-object v0

    .line 2263042
    iget-object v1, p0, LX/Fd0;->a:LX/Fd6;

    iget-object v1, v1, LX/Fd6;->y:LX/Fd4;

    invoke-interface {v1, v0}, LX/Fd4;->a(LX/CwL;)V

    .line 2263043
    iget-object v0, p0, LX/Fd0;->a:LX/Fd6;

    invoke-virtual {v0}, LX/0ht;->l()V

    .line 2263044
    return-void

    .line 2263045
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 2263046
    goto :goto_1
.end method
