.class public LX/FsP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/FsP;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/FsO;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fqr;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Fqw;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0ad;

.field private final f:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/FsO;LX/0Or;LX/0Or;LX/0ad;)V
    .locals 2
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "LX/FsO;",
            "LX/0Or",
            "<",
            "LX/Fqr;",
            ">;",
            "LX/0Or",
            "<",
            "LX/Fqw;",
            ">;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2296904
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2296905
    const-string v0, "timeline_fetch_header"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/FsP;->f:LX/0Rf;

    .line 2296906
    const-string v0, "timeline_fetch_header"

    const-string v1, "self_profile"

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    iput-object v0, p0, LX/FsP;->g:LX/0Rf;

    .line 2296907
    iput-object p1, p0, LX/FsP;->a:LX/0Or;

    .line 2296908
    iput-object p2, p0, LX/FsP;->b:LX/FsO;

    .line 2296909
    iput-object p3, p0, LX/FsP;->c:LX/0Or;

    .line 2296910
    iput-object p4, p0, LX/FsP;->d:LX/0Or;

    .line 2296911
    iput-object p5, p0, LX/FsP;->e:LX/0ad;

    .line 2296912
    return-void
.end method

.method private a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;)J
    .locals 8

    .prologue
    .line 2296963
    iget-object v0, p0, LX/FsP;->e:LX/0ad;

    sget-wide v2, LX/0wf;->S:J

    .line 2296964
    const-wide/32 v6, 0x24ea00

    move-wide v4, v6

    .line 2296965
    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(LX/0QB;)LX/FsP;
    .locals 9

    .prologue
    .line 2296950
    sget-object v0, LX/FsP;->h:LX/FsP;

    if-nez v0, :cond_1

    .line 2296951
    const-class v1, LX/FsP;

    monitor-enter v1

    .line 2296952
    :try_start_0
    sget-object v0, LX/FsP;->h:LX/FsP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2296953
    if-eqz v2, :cond_0

    .line 2296954
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2296955
    new-instance v3, LX/FsP;

    const/16 v4, 0x15e7

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/FsO;->a(LX/0QB;)LX/FsO;

    move-result-object v5

    check-cast v5, LX/FsO;

    const/16 v6, 0x3621

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x3624

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, LX/FsP;-><init>(LX/0Or;LX/FsO;LX/0Or;LX/0Or;LX/0ad;)V

    .line 2296956
    move-object v0, v3

    .line 2296957
    sput-object v0, LX/FsP;->h:LX/FsP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2296958
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2296959
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2296960
    :cond_1
    sget-object v0, LX/FsP;->h:LX/FsP;

    return-object v0

    .line 2296961
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2296962
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;LX/0zS;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;
    .locals 10
    .param p3    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;",
            "LX/0zS;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "Lcom/facebook/http/interfaces/RequestPriority;",
            ")",
            "LX/0zO",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2296913
    iget-wide v4, p1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->a:J

    move-wide v0, v4

    .line 2296914
    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/FsP;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 2296915
    iget-object v1, p0, LX/FsP;->b:LX/FsO;

    .line 2296916
    iget-object v4, v1, LX/FsO;->b:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    .line 2296917
    iget-wide v8, p1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->a:J

    move-wide v6, v8

    .line 2296918
    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    .line 2296919
    if-eqz v4, :cond_2

    .line 2296920
    new-instance v4, LX/5wI;

    invoke-direct {v4}, LX/5wI;-><init>()V

    move-object v4, v4

    .line 2296921
    const-string v5, "profile_refresher_step_types"

    .line 2296922
    new-instance v6, LX/0Pz;

    invoke-direct {v6}, LX/0Pz;-><init>()V

    .line 2296923
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2296924
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2296925
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->CORE_PROFILE_FIELD:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2296926
    iget-object v7, v1, LX/FsO;->a:LX/0ad;

    sget-short v8, LX/0wf;->aA:S

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, LX/0ad;->a(SZ)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2296927
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2296928
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_FEATURED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2296929
    :cond_0
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    move-object v6, v6

    .line 2296930
    invoke-virtual {v4, v5, v6}, LX/0gW;->b(Ljava/lang/String;Ljava/lang/Object;)LX/0gW;

    move-result-object v5

    const-string v6, "is_self_profile"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2296931
    invoke-static {v1, v4, p1}, LX/FsO;->a(LX/FsO;LX/0gW;Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;)V

    .line 2296932
    move-object v4, v4

    .line 2296933
    :goto_0
    move-object v1, v4

    .line 2296934
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    invoke-direct {p0, p1}, LX/FsP;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v1

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/FsP;->g:LX/0Rf;

    .line 2296935
    :goto_1
    iput-object v0, v1, LX/0zO;->d:Ljava/util/Set;

    .line 2296936
    move-object v1, v1

    .line 2296937
    iget-object v0, p0, LX/FsP;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zT;

    invoke-virtual {v1, v0}, LX/0zO;->a(LX/0zT;)LX/0zO;

    move-result-object v0

    .line 2296938
    iput-object p3, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2296939
    move-object v0, v0

    .line 2296940
    invoke-virtual {v0, p4}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    iget-object v0, p0, LX/FsP;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0sj;

    .line 2296941
    iput-object v0, v1, LX/0zO;->t:LX/0sj;

    .line 2296942
    move-object v0, v1

    .line 2296943
    return-object v0

    .line 2296944
    :cond_1
    iget-object v0, p0, LX/FsP;->f:LX/0Rf;

    goto :goto_1

    .line 2296945
    :cond_2
    new-instance v4, LX/5wH;

    invoke-direct {v4}, LX/5wH;-><init>()V

    move-object v4, v4

    .line 2296946
    const-string v5, "is_self_profile"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2296947
    invoke-static {v1, v4, p1}, LX/FsO;->a(LX/FsO;LX/0gW;Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;)V

    .line 2296948
    move-object v4, v4

    .line 2296949
    goto :goto_0
.end method
