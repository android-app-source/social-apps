.class public final enum LX/FQ9;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FQ9;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FQ9;

.field public static final enum BOOKMARK:LX/FQ9;

.field public static final enum NONE:LX/FQ9;

.field private static sValueArray:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "LX/FQ9;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public mIconResId:I

.field public mSelectedColorResId:I

.field public mUnselectedColorResId:I

.field private mValue:I


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 2238469
    new-instance v0, LX/FQ9;

    const-string v1, "NONE"

    move v3, v2

    move v4, v2

    move v5, v2

    move v6, v2

    invoke-direct/range {v0 .. v6}, LX/FQ9;-><init>(Ljava/lang/String;IIIII)V

    sput-object v0, LX/FQ9;->NONE:LX/FQ9;

    .line 2238470
    new-instance v3, LX/FQ9;

    const-string v4, "BOOKMARK"

    const v7, 0x7f020781

    const v8, 0x7f0a00fc

    const v9, 0x7f0a008a

    move v5, v10

    move v6, v10

    invoke-direct/range {v3 .. v9}, LX/FQ9;-><init>(Ljava/lang/String;IIIII)V

    sput-object v3, LX/FQ9;->BOOKMARK:LX/FQ9;

    .line 2238471
    const/4 v0, 0x2

    new-array v0, v0, [LX/FQ9;

    sget-object v1, LX/FQ9;->NONE:LX/FQ9;

    aput-object v1, v0, v2

    sget-object v1, LX/FQ9;->BOOKMARK:LX/FQ9;

    aput-object v1, v0, v10

    sput-object v0, LX/FQ9;->$VALUES:[LX/FQ9;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIII)V"
        }
    .end annotation

    .prologue
    .line 2238472
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2238473
    iput p3, p0, LX/FQ9;->mValue:I

    .line 2238474
    iput p4, p0, LX/FQ9;->mIconResId:I

    .line 2238475
    iput p5, p0, LX/FQ9;->mUnselectedColorResId:I

    .line 2238476
    iput p6, p0, LX/FQ9;->mSelectedColorResId:I

    .line 2238477
    return-void
.end method

.method public static fromValue(ILX/FQ9;)LX/FQ9;
    .locals 6

    .prologue
    .line 2238478
    sget-object v0, LX/FQ9;->sValueArray:Landroid/util/SparseArray;

    if-nez v0, :cond_0

    .line 2238479
    invoke-static {}, LX/FQ9;->values()[LX/FQ9;

    move-result-object v1

    .line 2238480
    new-instance v0, Landroid/util/SparseArray;

    array-length v2, v1

    invoke-direct {v0, v2}, Landroid/util/SparseArray;-><init>(I)V

    sput-object v0, LX/FQ9;->sValueArray:Landroid/util/SparseArray;

    .line 2238481
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 2238482
    sget-object v4, LX/FQ9;->sValueArray:Landroid/util/SparseArray;

    iget v5, v3, LX/FQ9;->mValue:I

    invoke-virtual {v4, v5, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2238483
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2238484
    :cond_0
    sget-object v0, LX/FQ9;->sValueArray:Landroid/util/SparseArray;

    invoke-virtual {v0, p0, p1}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FQ9;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/FQ9;
    .locals 1

    .prologue
    .line 2238485
    const-class v0, LX/FQ9;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FQ9;

    return-object v0
.end method

.method public static values()[LX/FQ9;
    .locals 1

    .prologue
    .line 2238486
    sget-object v0, LX/FQ9;->$VALUES:[LX/FQ9;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FQ9;

    return-object v0
.end method
