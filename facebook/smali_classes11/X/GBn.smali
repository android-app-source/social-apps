.class public final LX/GBn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7HK;


# instance fields
.field public final synthetic a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V
    .locals 0

    .prologue
    .line 2327520
    iput-object p1, p0, LX/GBn;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 2327521
    iget-object v0, p0, LX/GBn;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->r:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2327522
    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2327523
    :goto_0
    return-void

    .line 2327524
    :cond_0
    iget-object v1, p0, LX/GBn;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->s:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2327525
    iget-object v1, p0, LX/GBn;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->u:Landroid/widget/Button;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 2327526
    iget-object v1, p0, LX/GBn;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->y:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2327527
    iget-object v1, p0, LX/GBn;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->w:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2327528
    iget-object v1, p0, LX/GBn;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    .line 2327529
    iput-object v0, v1, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->z:Ljava/lang/String;

    .line 2327530
    iget-object v0, p0, LX/GBn;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    .line 2327531
    iput-boolean v3, v0, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->H:Z

    .line 2327532
    iget-object v0, p0, LX/GBn;->a:Lcom/facebook/account/recovery/fragment/AccountSearchFragment;

    invoke-static {v0}, Lcom/facebook/account/recovery/fragment/AccountSearchFragment;->m(Lcom/facebook/account/recovery/fragment/AccountSearchFragment;)V

    goto :goto_0
.end method
