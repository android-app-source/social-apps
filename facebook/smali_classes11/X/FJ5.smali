.class public final LX/FJ5;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:LX/FJ6;


# direct methods
.method public constructor <init>(LX/FJ6;)V
    .locals 0

    .prologue
    .line 2223064
    iput-object p1, p0, LX/FJ5;->a:LX/FJ6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(FFFFF)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/high16 v4, 0x44fa0000    # 2000.0f

    const/4 v6, 0x1

    const/high16 v7, 0x3f800000    # 1.0f

    .line 2223065
    sget-object v1, LX/FJ4;->a:[I

    iget-object v2, p0, LX/FJ5;->a:LX/FJ6;

    invoke-virtual {v2}, LX/FJ6;->b()LX/FJ8;

    move-result-object v2

    invoke-virtual {v2}, LX/FJ8;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    move v6, v0

    .line 2223066
    :goto_0
    :pswitch_0
    iget-object v0, p0, LX/FJ5;->a:LX/FJ6;

    iget-object v0, v0, LX/FJ6;->b:LX/FJ7;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v7}, LX/FJ7;->a(FFFFFZF)V

    .line 2223067
    return-void

    .line 2223068
    :pswitch_1
    const/4 v7, 0x0

    .line 2223069
    goto :goto_0

    .line 2223070
    :pswitch_2
    iget-object v0, p0, LX/FJ5;->a:LX/FJ6;

    iget-wide v0, v0, LX/FJ6;->i:D

    double-to-float v0, v0

    div-float v7, v0, v4

    .line 2223071
    goto :goto_0

    .line 2223072
    :pswitch_3
    iget-object v0, p0, LX/FJ5;->a:LX/FJ6;

    iget-wide v0, v0, LX/FJ6;->i:D

    iget-object v2, p0, LX/FJ5;->a:LX/FJ6;

    iget-wide v2, v2, LX/FJ6;->j:D

    sub-double/2addr v0, v2

    double-to-float v0, v0

    div-float/2addr v0, v4

    sub-float/2addr v7, v0

    .line 2223073
    goto :goto_0

    :pswitch_4
    move v6, v0

    .line 2223074
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
