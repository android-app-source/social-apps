.class public LX/GkA;
.super LX/1Cv;
.source ""

# interfaces
.implements LX/61x;
.implements LX/2ht;


# instance fields
.field public final a:LX/Gk6;

.field public b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/Gkn;",
            "Lcom/facebook/groups/groupsections/GroupsSectionInterface;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/content/res/Resources;

.field public d:LX/GkG;


# direct methods
.method public constructor <init>(LX/Gk6;Landroid/content/res/Resources;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2388914
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2388915
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/GkA;->b:Ljava/util/HashMap;

    .line 2388916
    iput-object p1, p0, LX/GkA;->a:LX/Gk6;

    .line 2388917
    iput-object p2, p0, LX/GkA;->c:Landroid/content/res/Resources;

    .line 2388918
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2388909
    invoke-static {}, LX/Gk9;->values()[LX/Gk9;

    move-result-object v0

    aget-object v0, v0, p1

    .line 2388910
    sget-object v1, LX/Gk8;->a:[I

    invoke-virtual {v0}, LX/Gk9;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2388911
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2388912
    :pswitch_0
    new-instance v0, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 2388913
    :pswitch_1
    new-instance v0, LX/GkO;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/GkO;-><init>(Landroid/content/Context;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 2388876
    invoke-static {}, LX/Gk9;->values()[LX/Gk9;

    move-result-object v0

    aget-object v0, v0, p4

    .line 2388877
    sget-object v1, LX/Gk8;->a:[I

    invoke-virtual {v0}, LX/Gk9;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2388878
    :cond_0
    :goto_0
    return-void

    .line 2388879
    :pswitch_0
    check-cast p2, LX/Gkq;

    .line 2388880
    if-eqz p2, :cond_0

    .line 2388881
    check-cast p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;

    .line 2388882
    iget-object v0, p0, LX/GkA;->d:LX/GkG;

    .line 2388883
    iput-object v0, p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->g:LX/GkG;

    .line 2388884
    const/4 p0, 0x0

    .line 2388885
    iget-object v0, p2, LX/Gkq;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2388886
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2388887
    iget-object v0, p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    invoke-virtual {v0, p0}, LX/1af;->b(Landroid/graphics/drawable/Drawable;)V

    .line 2388888
    iget-object v0, p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    .line 2388889
    iget-object v1, p2, LX/Gkq;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2388890
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object p0, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, p0}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2388891
    :goto_1
    iget-object v0, p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->e:Lcom/facebook/widget/text/BetterTextView;

    .line 2388892
    iget-object v1, p2, LX/Gkq;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2388893
    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2388894
    iget-boolean v0, p2, LX/Gkq;->g:Z

    move v0, v0

    .line 2388895
    if-eqz v0, :cond_2

    .line 2388896
    iget-object v0, p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->c:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2388897
    iget-object v0, p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->a:Landroid/content/res/Resources;

    const p0, 0x7f020c42

    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2388898
    iget-object v0, p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->a:Landroid/content/res/Resources;

    const p0, 0x7f0a0945

    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2388899
    iget-object v0, p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->a:Landroid/content/res/Resources;

    const p0, 0x7f08373d

    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2388900
    :goto_2
    iget-object v0, p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/GkP;

    invoke-direct {v1, p3, p2}, LX/GkP;-><init>(Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;LX/Gkq;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2388901
    goto :goto_0

    .line 2388902
    :pswitch_1
    check-cast p3, LX/GkO;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p3, p2}, LX/GkO;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2388903
    :cond_1
    iget-object v0, p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v0}, Lcom/facebook/drawee/view/DraweeView;->getHierarchy()LX/1aY;

    move-result-object v0

    check-cast v0, LX/1af;

    const/4 v1, 0x1

    invoke-static {v1}, LX/Dae;->a(Z)I

    move-result v1

    invoke-virtual {v0, v1}, LX/1af;->b(I)V

    .line 2388904
    iget-object v0, p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->f:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    sget-object v1, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, p0, v1}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    goto :goto_1

    .line 2388905
    :cond_2
    iget-object v0, p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->c:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2388906
    iget-object v0, p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->a:Landroid/content/res/Resources;

    const p0, 0x7f020c41

    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2388907
    iget-object v0, p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->a:Landroid/content/res/Resources;

    const p0, 0x7f0a0944

    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2388908
    iget-object v0, p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->d:Lcom/facebook/fbui/glyph/GlyphView;

    iget-object v1, p3, Lcom/facebook/groups/editfavorites/view/GroupFavoriteRowView;->a:Landroid/content/res/Resources;

    const p0, 0x7f08373c

    invoke-virtual {v1, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 2388871
    const/4 v1, -0x1

    .line 2388872
    iget-object v0, p0, LX/GkA;->b:Ljava/util/HashMap;

    sget-object v2, LX/Gkn;->FAVORITES_SECTION:LX/Gkn;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkp;

    .line 2388873
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/Gk6;->a(LX/Gkp;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 2388874
    const/4 v0, 0x1

    .line 2388875
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final b(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2388867
    if-nez p2, :cond_0

    .line 2388868
    new-instance v1, LX/GkO;

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/GkO;-><init>(Landroid/content/Context;)V

    :goto_0
    move-object v0, v1

    .line 2388869
    check-cast v0, LX/GkO;

    iget-object v2, p0, LX/GkA;->a:LX/Gk6;

    invoke-virtual {v2, p1}, LX/Gk6;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/GkO;->a(Ljava/lang/String;)V

    .line 2388870
    return-object v1

    :cond_0
    move-object v1, p2

    goto :goto_0
.end method

.method public final c()I
    .locals 3

    .prologue
    .line 2388851
    const/4 v1, -0x1

    .line 2388852
    iget-object v0, p0, LX/GkA;->b:Ljava/util/HashMap;

    sget-object v2, LX/Gkn;->FAVORITES_SECTION:LX/Gkn;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkp;

    .line 2388853
    if-eqz v0, :cond_0

    invoke-static {v0}, LX/Gk6;->a(LX/Gkp;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 2388854
    invoke-static {v0}, LX/Gk6;->a(LX/Gkp;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 2388855
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2388866
    const/4 v0, 0x0

    return v0
.end method

.method public final e(I)I
    .locals 2

    .prologue
    .line 2388919
    iget-object v0, p0, LX/GkA;->c:Landroid/content/res/Resources;

    const v1, 0x7f0b237a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method public final f(I)Z
    .locals 1

    .prologue
    .line 2388865
    iget-object v0, p0, LX/GkA;->a:LX/Gk6;

    invoke-virtual {v0, p1}, LX/Gk6;->b(I)Z

    move-result v0

    return v0
.end method

.method public final getCount()I
    .locals 6

    .prologue
    .line 2388856
    iget-object v0, p0, LX/GkA;->a:LX/Gk6;

    const/4 v2, 0x0

    .line 2388857
    iget-object v1, v0, LX/Gk6;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2388858
    :cond_0
    move v0, v2

    .line 2388859
    return v0

    .line 2388860
    :cond_1
    invoke-static {}, LX/Gkn;->values()[LX/Gkn;

    move-result-object v4

    array-length v5, v4

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v1, v4, v3

    .line 2388861
    iget-object p0, v0, LX/Gk6;->b:Ljava/util/HashMap;

    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Gkp;

    .line 2388862
    if-eqz v1, :cond_2

    .line 2388863
    invoke-static {v1}, LX/Gk6;->a(LX/Gkp;)I

    move-result v1

    add-int/2addr v1, v2

    .line 2388864
    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2388823
    iget-object v0, p0, LX/GkA;->a:LX/Gk6;

    invoke-virtual {v0, p1}, LX/Gk6;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2388824
    iget-object v0, p0, LX/GkA;->a:LX/Gk6;

    .line 2388825
    invoke-virtual {v0, p1}, LX/Gk6;->b(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2388826
    invoke-virtual {v0, p1}, LX/Gk6;->c(I)Ljava/lang/String;

    move-result-object v1

    .line 2388827
    :goto_0
    move-object v0, v1

    .line 2388828
    :goto_1
    return-object v0

    .line 2388829
    :cond_0
    iget-object v0, p0, LX/GkA;->a:LX/Gk6;

    invoke-virtual {v0, p1}, LX/Gk6;->a(I)LX/Gkn;

    move-result-object v0

    .line 2388830
    sget-object v1, LX/Gkn;->FAVORITES_SECTION:LX/Gkn;

    if-ne v0, v1, :cond_1

    .line 2388831
    move p1, p1

    .line 2388832
    :cond_1
    iget-object v0, p0, LX/GkA;->a:LX/Gk6;

    const/4 v2, 0x0

    .line 2388833
    invoke-virtual {v0, p1}, LX/Gk6;->b(I)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object v1, v2

    .line 2388834
    :goto_2
    move-object v0, v1

    .line 2388835
    goto :goto_1

    :cond_2
    const-string v1, ""

    goto :goto_0

    .line 2388836
    :cond_3
    invoke-virtual {v0, p1}, LX/Gk6;->a(I)LX/Gkn;

    move-result-object v1

    .line 2388837
    if-nez v1, :cond_4

    move-object v1, v2

    .line 2388838
    goto :goto_2

    .line 2388839
    :cond_4
    iget-object v3, v0, LX/Gk6;->b:Ljava/util/HashMap;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Gkp;

    .line 2388840
    invoke-static {}, LX/Gkn;->values()[LX/Gkn;

    move-result-object v6

    array-length v7, v6

    const/4 v3, 0x0

    move v5, v3

    move v4, p1

    :goto_3
    if-ge v5, v7, :cond_7

    aget-object v3, v6, v5

    .line 2388841
    iget-object p0, v0, LX/Gk6;->b:Ljava/util/HashMap;

    invoke-virtual {p0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Gkp;

    .line 2388842
    invoke-static {v3}, LX/Gk6;->a(LX/Gkp;)I

    move-result v3

    .line 2388843
    if-ge v4, v3, :cond_6

    move v3, v4

    .line 2388844
    :goto_4
    move v3, v3

    .line 2388845
    const/4 v4, -0x1

    if-ne v3, v4, :cond_5

    move-object v1, v2

    .line 2388846
    goto :goto_2

    .line 2388847
    :cond_5
    add-int/lit8 v2, v3, -0x1

    invoke-virtual {v1, v2}, LX/Gkp;->a(I)LX/Gkq;

    move-result-object v1

    goto :goto_2

    .line 2388848
    :cond_6
    sub-int/2addr v4, v3

    .line 2388849
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_3

    .line 2388850
    :cond_7
    const/4 v3, -0x1

    goto :goto_4
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2388822
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2388819
    iget-object v0, p0, LX/GkA;->a:LX/Gk6;

    invoke-virtual {v0, p1}, LX/Gk6;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2388820
    sget-object v0, LX/Gk9;->SECTION_HEADER:LX/Gk9;

    invoke-virtual {v0}, LX/Gk9;->ordinal()I

    move-result v0

    .line 2388821
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/Gk9;->FAVORITE_ROW:LX/Gk9;

    invoke-virtual {v0}, LX/Gk9;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2388818
    invoke-static {}, LX/Gk9;->values()[LX/Gk9;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final o_(I)I
    .locals 1

    .prologue
    .line 2388817
    sget-object v0, LX/Gk9;->SECTION_HEADER:LX/Gk9;

    invoke-virtual {v0}, LX/Gk9;->ordinal()I

    move-result v0

    return v0
.end method
