.class public final LX/GjT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/greetingcards/create/PreviewCardFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/greetingcards/create/PreviewCardFragment;)V
    .locals 0

    .prologue
    .line 2387548
    iput-object p1, p0, LX/GjT;->a:Lcom/facebook/greetingcards/create/PreviewCardFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 2387549
    iget-object v0, p0, LX/GjT;->a:Lcom/facebook/greetingcards/create/PreviewCardFragment;

    .line 2387550
    iget-object p0, v0, Landroid/support/v4/app/Fragment;->mParentFragment:Landroid/support/v4/app/Fragment;

    move-object p0, p0

    .line 2387551
    instance-of p1, p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    if-eqz p1, :cond_1

    .line 2387552
    check-cast p0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    invoke-virtual {p0}, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->b()V

    .line 2387553
    :cond_0
    :goto_0
    return-void

    .line 2387554
    :cond_1
    instance-of p1, p0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;

    if-eqz p1, :cond_0

    .line 2387555
    check-cast p0, Lcom/facebook/greetingcards/render/GreetingCardPopoverFragment;

    invoke-virtual {p0}, Landroid/support/v4/app/DialogFragment;->c()V

    goto :goto_0
.end method
