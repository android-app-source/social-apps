.class public LX/FVO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0fx;


# instance fields
.field public final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/FVN;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/FVM;

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2250715
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2250716
    invoke-static {}, LX/0RA;->a()Ljava/util/HashSet;

    move-result-object v0

    iput-object v0, p0, LX/FVO;->a:Ljava/util/Set;

    .line 2250717
    invoke-virtual {p0}, LX/FVO;->g()V

    .line 2250718
    return-void
.end method

.method public static a(LX/0QB;)LX/FVO;
    .locals 1

    .prologue
    .line 2250719
    new-instance v0, LX/FVO;

    invoke-direct {v0}, LX/FVO;-><init>()V

    .line 2250720
    move-object v0, v0

    .line 2250721
    return-object v0
.end method


# virtual methods
.method public final a(LX/0g8;I)V
    .locals 0

    .prologue
    .line 2250722
    return-void
.end method

.method public final a(LX/0g8;III)V
    .locals 2

    .prologue
    .line 2250723
    iget-object v0, p0, LX/FVO;->b:LX/FVM;

    sget-object v1, LX/FVM;->NOT_LOADING:LX/FVM;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, LX/FVO;->c:Z

    if-nez v0, :cond_1

    .line 2250724
    :cond_0
    return-void

    .line 2250725
    :cond_1
    add-int v0, p2, p3

    .line 2250726
    sub-int v0, p4, v0

    .line 2250727
    const/4 v1, 0x3

    if-gt v0, v1, :cond_0

    .line 2250728
    iget-object v0, p0, LX/FVO;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FVN;

    .line 2250729
    invoke-interface {v0}, LX/FVN;->b()V

    goto :goto_0
.end method

.method public final a(LX/FVN;)V
    .locals 1

    .prologue
    .line 2250730
    iget-object v0, p0, LX/FVO;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2250731
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2250732
    iget-object v0, p0, LX/FVO;->b:LX/FVM;

    .line 2250733
    sget-object v1, LX/FVM;->NOT_LOADING:LX/FVM;

    iput-object v1, p0, LX/FVO;->b:LX/FVM;

    .line 2250734
    sget-object v1, LX/FVM;->LOADING_MORE:LX/FVM;

    if-ne v0, v1, :cond_0

    .line 2250735
    iget-object v0, p0, LX/FVO;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FVN;

    .line 2250736
    invoke-interface {v0}, LX/FVN;->e()V

    goto :goto_0

    .line 2250737
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 2250738
    sget-object v0, LX/FVM;->NOT_LOADING:LX/FVM;

    iput-object v0, p0, LX/FVO;->b:LX/FVM;

    .line 2250739
    const/4 v0, 0x0

    .line 2250740
    iput-boolean v0, p0, LX/FVO;->c:Z

    .line 2250741
    iget-object v0, p0, LX/FVO;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FVN;

    .line 2250742
    invoke-interface {v0}, LX/FVN;->g()V

    goto :goto_0

    .line 2250743
    :cond_0
    return-void
.end method
