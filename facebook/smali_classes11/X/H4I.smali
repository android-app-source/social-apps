.class public LX/H4I;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field private final b:LX/9kE;


# direct methods
.method public constructor <init>(LX/0tX;LX/9kE;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2422567
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2422568
    iput-object p1, p0, LX/H4I;->a:LX/0tX;

    .line 2422569
    iput-object p2, p0, LX/H4I;->b:LX/9kE;

    .line 2422570
    return-void
.end method

.method public static b(LX/0QB;)LX/H4I;
    .locals 3

    .prologue
    .line 2422565
    new-instance v2, LX/H4I;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/9kE;->b(LX/0QB;)LX/9kE;

    move-result-object v1

    check-cast v1, LX/9kE;

    invoke-direct {v2, v0, v1}, LX/H4I;-><init>(LX/0tX;LX/9kE;)V

    .line 2422566
    return-object v2
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2422542
    iget-object v0, p0, LX/H4I;->b:LX/9kE;

    invoke-virtual {v0}, LX/9kE;->c()V

    .line 2422543
    return-void
.end method

.method public final a(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;LX/0TF;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;",
            "LX/0TF",
            "<",
            "Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2422544
    invoke-virtual {p0}, LX/H4I;->a()V

    .line 2422545
    const-wide/16 v7, 0x0

    .line 2422546
    new-instance v3, LX/CRr;

    invoke-direct {v3}, LX/CRr;-><init>()V

    move-object v3, v3

    .line 2422547
    const-string v4, "search_query"

    .line 2422548
    iget-object v5, p1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->c:Ljava/lang/String;

    move-object v5, v5

    .line 2422549
    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2422550
    const-string v4, "scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2422551
    iget-wide v9, p1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->a:D

    move-wide v5, v9

    .line 2422552
    cmpl-double v4, v5, v7

    if-eqz v4, :cond_0

    .line 2422553
    iget-wide v9, p1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->b:D

    move-wide v5, v9

    .line 2422554
    cmpl-double v4, v5, v7

    if-eqz v4, :cond_0

    .line 2422555
    const-string v4, "latitude"

    .line 2422556
    iget-wide v9, p1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->a:D

    move-wide v5, v9

    .line 2422557
    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2422558
    const-string v4, "longitude"

    .line 2422559
    iget-wide v9, p1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->b:D

    move-wide v5, v9

    .line 2422560
    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2422561
    :cond_0
    move-object v0, v3

    .line 2422562
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2422563
    iget-object v1, p0, LX/H4I;->b:LX/9kE;

    iget-object v2, p0, LX/H4I;->a:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/0tX;->a(Lcom/google/common/util/concurrent/ListenableFuture;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v2, LX/H4H;

    invoke-direct {v2, p0, p2, p1}, LX/H4H;-><init>(LX/H4I;LX/0TF;Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;)V

    invoke-virtual {v1, v0, v2}, LX/9kE;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2422564
    return-void
.end method
