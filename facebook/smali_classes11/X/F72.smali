.class public final enum LX/F72;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/F72;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/F72;

.field public static final enum DEFAULT:LX/F72;

.field public static final enum FAILURE:LX/F72;

.field public static final enum LOADING_MORE:LX/F72;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2201077
    new-instance v0, LX/F72;

    const-string v1, "DEFAULT"

    invoke-direct {v0, v1, v2}, LX/F72;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F72;->DEFAULT:LX/F72;

    .line 2201078
    new-instance v0, LX/F72;

    const-string v1, "LOADING_MORE"

    invoke-direct {v0, v1, v3}, LX/F72;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F72;->LOADING_MORE:LX/F72;

    .line 2201079
    new-instance v0, LX/F72;

    const-string v1, "FAILURE"

    invoke-direct {v0, v1, v4}, LX/F72;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F72;->FAILURE:LX/F72;

    .line 2201080
    const/4 v0, 0x3

    new-array v0, v0, [LX/F72;

    sget-object v1, LX/F72;->DEFAULT:LX/F72;

    aput-object v1, v0, v2

    sget-object v1, LX/F72;->LOADING_MORE:LX/F72;

    aput-object v1, v0, v3

    sget-object v1, LX/F72;->FAILURE:LX/F72;

    aput-object v1, v0, v4

    sput-object v0, LX/F72;->$VALUES:[LX/F72;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2201081
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/F72;
    .locals 1

    .prologue
    .line 2201076
    const-class v0, LX/F72;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/F72;

    return-object v0
.end method

.method public static values()[LX/F72;
    .locals 1

    .prologue
    .line 2201075
    sget-object v0, LX/F72;->$VALUES:[LX/F72;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/F72;

    return-object v0
.end method
