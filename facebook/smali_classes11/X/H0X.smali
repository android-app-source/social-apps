.class public LX/H0X;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/1a1;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/H0V;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;

.field private c:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2413981
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2413982
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2413983
    iput-object v0, p0, LX/H0X;->a:LX/0Px;

    .line 2413984
    iput-object p1, p0, LX/H0X;->b:Landroid/content/Context;

    .line 2413985
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2413970
    sget-object v0, LX/H0W;->TITLE:LX/H0W;

    invoke-virtual {v0}, LX/H0W;->toInt()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2413971
    iget-object v0, p0, LX/H0X;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307a8

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2413972
    new-instance v0, LX/H0Q;

    invoke-direct {v0, v1}, LX/H0Q;-><init>(Landroid/view/View;)V

    .line 2413973
    :goto_0
    return-object v0

    .line 2413974
    :cond_0
    sget-object v0, LX/H0W;->DESCRIPTION:LX/H0W;

    invoke-virtual {v0}, LX/H0W;->toInt()I

    move-result v0

    if-ne p2, v0, :cond_1

    .line 2413975
    iget-object v0, p0, LX/H0X;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307a6

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2413976
    new-instance v0, LX/H0R;

    invoke-direct {v0, v1}, LX/H0R;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2413977
    :cond_1
    sget-object v0, LX/H0W;->FIELD_CHECK_BOX:LX/H0W;

    invoke-virtual {v0}, LX/H0W;->toInt()I

    move-result v0

    if-ne p2, v0, :cond_2

    .line 2413978
    iget-object v0, p0, LX/H0X;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307a0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 2413979
    new-instance v0, LX/H0U;

    invoke-direct {v0, v1}, LX/H0U;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2413980
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid viewType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 4

    .prologue
    .line 2413986
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2413987
    sget-object v1, LX/H0W;->TITLE:LX/H0W;

    invoke-virtual {v1}, LX/H0W;->toInt()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 2413988
    check-cast p1, LX/H0Q;

    .line 2413989
    iget-object v0, p0, LX/H0X;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08151c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2413990
    iget-object v1, p0, LX/H0X;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f081524

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2413991
    invoke-virtual {p1, v0, v1}, LX/H0Q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2413992
    :goto_0
    return-void

    .line 2413993
    :cond_0
    sget-object v1, LX/H0W;->DESCRIPTION:LX/H0W;

    invoke-virtual {v1}, LX/H0W;->toInt()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 2413994
    check-cast p1, LX/H0R;

    .line 2413995
    iget-object v0, p0, LX/H0X;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08151d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2413996
    invoke-virtual {p1, v0}, LX/H0R;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2413997
    :cond_1
    sget-object v1, LX/H0W;->FIELD_CHECK_BOX:LX/H0W;

    invoke-virtual {v1}, LX/H0W;->toInt()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 2413998
    check-cast p1, LX/H0U;

    .line 2413999
    iget-object v0, p0, LX/H0X;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H0V;

    iget-object v0, v0, LX/H0V;->b:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;

    .line 2414000
    iput-object v0, p1, LX/H0U;->m:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;

    .line 2414001
    iget-object v1, p1, LX/H0U;->m:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;

    .line 2414002
    iget-object v2, v1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->a:Ljava/lang/String;

    move-object v1, v2

    .line 2414003
    iget-object v2, p1, LX/H0U;->m:Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;

    .line 2414004
    iget-boolean v0, v2, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;->c:Z

    move v2, v0

    .line 2414005
    iget-object v0, p1, LX/H0U;->l:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 2414006
    iget-object v0, p1, LX/H0U;->l:Landroid/widget/CheckBox;

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 2414007
    goto :goto_0

    .line 2414008
    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid viewType "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final a(Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V
    .locals 5

    .prologue
    .line 2413954
    iput-object p1, p0, LX/H0X;->c:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    .line 2413955
    iget-object v0, p0, LX/H0X;->c:Lcom/facebook/messaging/professionalservices/getquote/model/FormData;

    .line 2413956
    if-eqz v0, :cond_0

    .line 2413957
    iget-object v1, v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->d:Ljava/util/List;

    move-object v1, v1

    .line 2413958
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2413959
    :cond_0
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2413960
    :goto_0
    move-object v0, v1

    .line 2413961
    iput-object v0, p0, LX/H0X;->a:LX/0Px;

    .line 2413962
    return-void

    .line 2413963
    :cond_1
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2413964
    new-instance v1, LX/H0V;

    sget-object v3, LX/H0W;->TITLE:LX/H0W;

    invoke-direct {v1, v3}, LX/H0V;-><init>(LX/H0W;)V

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2413965
    new-instance v1, LX/H0V;

    sget-object v3, LX/H0W;->DESCRIPTION:LX/H0W;

    invoke-direct {v1, v3}, LX/H0V;-><init>(LX/H0W;)V

    invoke-virtual {v2, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2413966
    iget-object v1, v0, Lcom/facebook/messaging/professionalservices/getquote/model/FormData;->d:Ljava/util/List;

    move-object v1, v1

    .line 2413967
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;

    .line 2413968
    new-instance v4, LX/H0V;

    sget-object p1, LX/H0W;->FIELD_CHECK_BOX:LX/H0W;

    invoke-direct {v4, p1, v1}, LX/H0V;-><init>(LX/H0W;Lcom/facebook/messaging/professionalservices/getquote/model/FormData$UserInfoField;)V

    invoke-virtual {v2, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2413969
    :cond_2
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2413953
    iget-object v0, p0, LX/H0X;->a:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H0V;

    iget-object v0, v0, LX/H0V;->a:LX/H0W;

    invoke-virtual {v0}, LX/H0W;->toInt()I

    move-result v0

    return v0
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2413952
    iget-object v0, p0, LX/H0X;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
