.class public LX/Fka;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field private a:Landroid/view/LayoutInflater;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/FkY;",
            ">;"
        }
    .end annotation
.end field

.field public c:Landroid/content/Context;

.field public d:Z

.field private final e:LX/FkY;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2278799
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2278800
    new-instance v0, LX/FkY;

    invoke-direct {v0}, LX/FkY;-><init>()V

    iput-object v0, p0, LX/Fka;->e:LX/FkY;

    .line 2278801
    iput-object p1, p0, LX/Fka;->a:Landroid/view/LayoutInflater;

    .line 2278802
    iput-object p2, p0, LX/Fka;->c:Landroid/content/Context;

    .line 2278803
    return-void
.end method

.method private a(I)LX/FkY;
    .locals 1

    .prologue
    .line 2278798
    iget-object v0, p0, LX/Fka;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, p1, :cond_0

    iget-object v0, p0, LX/Fka;->e:LX/FkY;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Fka;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FkY;

    goto :goto_0
.end method

.method public static a(LX/Fka;Landroid/view/View;Landroid/view/ViewGroup;)Lcom/facebook/fig/listitem/FigListItem;
    .locals 3

    .prologue
    .line 2278795
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/facebook/fig/listitem/FigListItem;

    if-nez v0, :cond_1

    .line 2278796
    :cond_0
    iget-object v0, p0, LX/Fka;->a:Landroid/view/LayoutInflater;

    const v1, 0x7f030761

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2278797
    :goto_0
    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    return-object v0

    :cond_1
    move-object v0, p1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/FkY;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2278793
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/Fka;->a(Ljava/util/ArrayList;Z)V

    .line 2278794
    return-void
.end method

.method public final a(Ljava/util/ArrayList;Z)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "LX/FkY;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2278789
    iput-object p1, p0, LX/Fka;->b:Ljava/util/ArrayList;

    .line 2278790
    iput-boolean p2, p0, LX/Fka;->d:Z

    .line 2278791
    const v0, 0x7c845ad1

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2278792
    return-void
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2278737
    iget-boolean v0, p0, LX/Fka;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fka;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/Fka;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2278788
    invoke-direct {p0, p1}, LX/Fka;->a(I)LX/FkY;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2278787
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    .line 2278738
    invoke-direct {p0, p1}, LX/Fka;->a(I)LX/FkY;

    move-result-object v0

    .line 2278739
    iget v1, v0, LX/FkY;->a:I

    move v1, v1

    .line 2278740
    packed-switch v1, :pswitch_data_0

    .line 2278741
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized row type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2278742
    iget v3, v0, LX/FkY;->a:I

    move v0, v3

    .line 2278743
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2278744
    :pswitch_0
    invoke-static {p0, p2, p3}, LX/Fka;->a(LX/Fka;Landroid/view/View;Landroid/view/ViewGroup;)Lcom/facebook/fig/listitem/FigListItem;

    move-result-object v1

    .line 2278745
    const/4 v2, 0x1

    .line 2278746
    iget v3, v0, LX/FkY;->a:I

    if-ne v3, v2, :cond_2

    :goto_0
    invoke-static {v2}, LX/0PB;->checkState(Z)V

    .line 2278747
    iget-object v2, v0, LX/FkY;->b:Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;

    move-object v2, v2

    .line 2278748
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2278749
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->j()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$FundraiserBeneficiaryPickerSubtitleModel;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->j()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$FundraiserBeneficiaryPickerSubtitleModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$FundraiserBeneficiaryPickerSubtitleModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2278750
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->j()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$FundraiserBeneficiaryPickerSubtitleModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$FundraiserBeneficiaryPickerSubtitleModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    .line 2278751
    :goto_1
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->m()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$ProfilePictureModel;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->m()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$ProfilePictureModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2278752
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->m()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$ProfilePictureModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2278753
    :goto_2
    const/4 v2, 0x0

    .line 2278754
    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 2278755
    move-object v0, v1

    .line 2278756
    :goto_3
    return-object v0

    .line 2278757
    :pswitch_1
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2278758
    invoke-static {p0, p2, p3}, LX/Fka;->a(LX/Fka;Landroid/view/View;Landroid/view/ViewGroup;)Lcom/facebook/fig/listitem/FigListItem;

    move-result-object v4

    .line 2278759
    iget v1, v0, LX/FkY;->a:I

    const/4 v5, 0x2

    if-ne v1, v5, :cond_8

    const/4 v1, 0x1

    :goto_4
    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2278760
    iget-object v1, v0, LX/FkY;->c:Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;

    move-object v5, v1

    .line 2278761
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->o()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;

    move-result-object v1

    if-nez v1, :cond_3

    .line 2278762
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2278763
    :goto_5
    const/4 v1, 0x3

    invoke-virtual {v4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setBodyTextAppearenceType(I)V

    .line 2278764
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->k()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_5

    .line 2278765
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->k()LX/1vs;

    move-result-object v1

    iget-object v6, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2278766
    invoke-virtual {v6, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    move v1, v2

    :goto_6
    if-eqz v1, :cond_6

    .line 2278767
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->k()LX/1vs;

    move-result-object v1

    iget-object v6, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    invoke-virtual {v6, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    .line 2278768
    :goto_7
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2278769
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->p()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2278770
    invoke-virtual {v4, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    .line 2278771
    :goto_8
    invoke-virtual {v4, v3}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 2278772
    move-object v0, v4

    .line 2278773
    goto :goto_3

    .line 2278774
    :pswitch_2
    instance-of v0, p2, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    if-eqz v0, :cond_9

    check-cast p2, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2278775
    :goto_9
    invoke-virtual {p2}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2278776
    move-object v0, p2

    .line 2278777
    goto :goto_3

    .line 2278778
    :cond_0
    const-string v3, ""

    invoke-virtual {v1, v3}, Lcom/facebook/fig/listitem/FigListItem;->setMetaText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 2278779
    :cond_1
    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 2278780
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 2278781
    :cond_3
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->o()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;->a()Z

    move-result v6

    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;->o()Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel$PageModel;->j()Z

    move-result v7

    iget-object p1, p0, LX/Fka;->c:Landroid/content/Context;

    invoke-static {v1, v6, v7, p1}, LX/FkG;->a(Ljava/lang/CharSequence;ZZLandroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    :cond_4
    move v1, v3

    .line 2278782
    goto :goto_6

    :cond_5
    move v1, v3

    goto :goto_6

    .line 2278783
    :cond_6
    const-string v1, ""

    invoke-virtual {v4, v1}, Lcom/facebook/fig/listitem/FigListItem;->setBodyText(Ljava/lang/CharSequence;)V

    goto :goto_7

    .line 2278784
    :cond_7
    invoke-virtual {v4, v3}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setShowThumbnail(Z)V

    goto :goto_8

    .line 2278785
    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_4

    .line 2278786
    :cond_9
    new-instance p2, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iget-object v0, p0, LX/Fka;->c:Landroid/content/Context;

    invoke-direct {p2, v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;-><init>(Landroid/content/Context;)V

    goto :goto_9

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
