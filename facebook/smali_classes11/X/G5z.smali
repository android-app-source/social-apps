.class public final LX/G5z;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Landroid/widget/TextView;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/G60;


# direct methods
.method public constructor <init>(LX/G60;JLandroid/widget/TextView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2319590
    iput-object p1, p0, LX/G5z;->d:LX/G60;

    iput-wide p2, p0, LX/G5z;->a:J

    iput-object p4, p0, LX/G5z;->b:Landroid/widget/TextView;

    iput-object p5, p0, LX/G5z;->c:Ljava/lang/String;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2319591
    new-instance v0, Lorg/json/JSONObject;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2319592
    iget-object v1, p0, LX/G5z;->d:LX/G60;

    iget-object v1, v1, LX/G60;->b:LX/G63;

    iget-wide v2, p0, LX/G5z;->a:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/G5y;

    invoke-direct {v3, p0, v0}, LX/G5y;-><init>(LX/G5z;Ljava/lang/String;)V

    invoke-static {v3}, LX/0Vd;->of(LX/0TF;)LX/0Vd;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/G63;->a(Ljava/lang/String;Ljava/lang/String;LX/0Ve;)V

    .line 2319593
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 2319594
    invoke-super {p0, p1}, Landroid/text/style/ClickableSpan;->updateDrawState(Landroid/text/TextPaint;)V

    .line 2319595
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2319596
    iget-object v0, p0, LX/G5z;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x106000b

    invoke-static {v0, v1}, LX/1ED;->b(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2319597
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 2319598
    return-void
.end method
