.class public final LX/GI3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

.field public final synthetic b:LX/GCE;

.field public final synthetic c:LX/GIA;


# direct methods
.method public constructor <init>(LX/GIA;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;LX/GCE;)V
    .locals 0

    .prologue
    .line 2335682
    iput-object p1, p0, LX/GI3;->c:LX/GIA;

    iput-object p2, p0, LX/GI3;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    iput-object p3, p0, LX/GI3;->b:LX/GCE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, -0x4a3213b5

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2335683
    iget-object v1, p0, LX/GI3;->c:LX/GIA;

    iget-object v1, v1, LX/GIA;->f:LX/GNI;

    iget-object v2, p0, LX/GI3;->c:LX/GIA;

    iget-object v2, v2, LX/GIA;->l:Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/GI3;->c:LX/GIA;

    iget-object v3, v3, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    iget-object v4, p0, LX/GI3;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    iget-object v5, p0, LX/GI3;->b:LX/GCE;

    .line 2335684
    invoke-static {v3, v4}, LX/GNI;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    move-result-object v8

    .line 2335685
    iput-object v8, v5, LX/GCE;->k:Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    .line 2335686
    invoke-static {v3, v4}, LX/GNI;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v1, v4}, LX/GNI;->e(LX/GNI;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Lcom/facebook/common/locale/Country;

    move-result-object v11

    const/4 v12, 0x1

    move-object v7, v2

    invoke-static/range {v7 .. v12}, Lcom/facebook/adspayments/activity/PrepayFlowFundingActivity;->a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/payments/currency/CurrencyAmount;Lcom/facebook/common/util/Either;Lcom/facebook/common/locale/Country;Z)Landroid/content/Intent;

    move-result-object v7

    invoke-static {v7, v5}, LX/GNI;->a(Landroid/content/Intent;LX/GCE;)V

    .line 2335687
    const v1, 0x5e5f0b

    invoke-static {v6, v6, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
