.class public final LX/GUS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V
    .locals 0

    .prologue
    .line 2357397
    iput-object p1, p0, LX/GUS;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x27487200

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2357398
    iget-object v1, p0, LX/GUS;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->aj:LX/0yG;

    invoke-static {v1}, LX/1rv;->b(LX/0yG;)Z

    move-result v1

    invoke-static {v1}, LX/0PB;->checkState(Z)V

    .line 2357399
    iget-object v1, p0, LX/GUS;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->aj:LX/0yG;

    sget-object v2, LX/0yG;->LOCATION_DISABLED:LX/0yG;

    if-ne v1, v2, :cond_0

    .line 2357400
    iget-object v1, p0, LX/GUS;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    .line 2357401
    invoke-static {v1}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->u$redex0(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    .line 2357402
    :goto_0
    const v1, -0x1cdbc952

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2357403
    :cond_0
    iget-object v1, p0, LX/GUS;->a:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    invoke-static {v1}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->t(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;)V

    goto :goto_0
.end method
