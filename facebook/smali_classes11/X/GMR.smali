.class public LX/GMR;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public final b:LX/GKy;

.field private final c:LX/GG6;

.field public final d:Landroid/view/View$OnClickListener;

.field public final e:Landroid/view/View$OnClickListener;

.field public f:LX/GFR;

.field private g:Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;

.field private h:LX/15i;

.field private i:I

.field private j:J

.field public k:Ljava/lang/String;

.field public l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field private m:LX/GNC;

.field private n:Landroid/view/View$OnClickListener;

.field private o:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(LX/GKy;LX/GG6;LX/GNC;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2344561
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2344562
    new-instance v0, LX/GMN;

    invoke-direct {v0, p0}, LX/GMN;-><init>(LX/GMR;)V

    iput-object v0, p0, LX/GMR;->d:Landroid/view/View$OnClickListener;

    .line 2344563
    new-instance v0, LX/GMO;

    invoke-direct {v0, p0}, LX/GMO;-><init>(LX/GMR;)V

    iput-object v0, p0, LX/GMR;->e:Landroid/view/View$OnClickListener;

    .line 2344564
    iput-object p1, p0, LX/GMR;->b:LX/GKy;

    .line 2344565
    iput-object p2, p0, LX/GMR;->c:LX/GG6;

    .line 2344566
    iput-object p3, p0, LX/GMR;->m:LX/GNC;

    .line 2344567
    return-void
.end method

.method public static a(LX/0QB;)LX/GMR;
    .locals 1

    .prologue
    .line 2344469
    invoke-static {p0}, LX/GMR;->b(LX/0QB;)LX/GMR;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/GMR;Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2344560
    iget-wide v0, p0, LX/GMR;->j:J

    sget-object v2, LX/GG5;->CONTINUOUS:LX/GG5;

    invoke-virtual {v2}, LX/GG5;->getDuration()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const v0, 0x7f080b0a

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-wide v0, p0, LX/GMR;->j:J

    invoke-static {v0, v1, p1}, LX/GG6;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2344541
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2344542
    invoke-virtual {p1, v4}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setColumnsActive(Z)V

    .line 2344543
    iget-object v0, p0, LX/GMR;->b:LX/GKy;

    invoke-virtual {v0}, LX/GKy;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2344544
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setColumnsActive(Z)V

    .line 2344545
    iget-object v0, p0, LX/GMR;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setSpentText(Ljava/lang/String;)V

    .line 2344546
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, LX/GMR;->h:LX/15i;

    iget v3, p0, LX/GMR;->i:I

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v2, 0x3

    invoke-virtual {v0, v3, v2}, LX/15i;->j(II)I

    move-result v0

    invoke-static {v0, v1}, LX/GG6;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setPaidReach(Ljava/lang/String;)V

    .line 2344547
    :cond_0
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_1
    iget-object v0, p0, LX/GMR;->h:LX/15i;

    iget v3, p0, LX/GMR;->i:I

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const v2, 0x559eed

    invoke-static {v0, v3, v4, v2}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, LX/GMR;->l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v0

    sget-object v2, LX/8wL;->LOCAL_AWARENESS:LX/8wL;

    if-ne v0, v2, :cond_1

    .line 2344548
    invoke-static {p0, p1, v1}, LX/GMR;->b(LX/GMR;Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;Landroid/content/Context;)V

    .line 2344549
    :cond_1
    iget-object v0, p0, LX/GMR;->l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v0

    sget-object v2, LX/8wL;->PAGE_LIKE:LX/8wL;

    if-ne v0, v2, :cond_2

    .line 2344550
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_2
    iget-object v0, p0, LX/GMR;->h:LX/15i;

    iget v3, p0, LX/GMR;->i:I

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2344551
    const v2, 0x7f080b0c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x2

    invoke-virtual {v0, v3, v4}, LX/15i;->j(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->c(Ljava/lang/String;Ljava/lang/String;)LX/GKw;

    .line 2344552
    :cond_2
    iget-object v0, p0, LX/GMR;->l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v0

    sget-object v2, LX/8wL;->PROMOTE_WEBSITE:LX/8wL;

    if-ne v0, v2, :cond_3

    .line 2344553
    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_3
    iget-object v0, p0, LX/GMR;->h:LX/15i;

    iget v3, p0, LX/GMR;->i:I

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 2344554
    const v2, 0x7f080b1c

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v0, v3, v2}, LX/15i;->j(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->c(Ljava/lang/String;Ljava/lang/String;)LX/GKw;

    .line 2344555
    :cond_3
    return-void

    .line 2344556
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 2344557
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    :cond_4
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    .line 2344558
    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    .line 2344559
    :catchall_3
    move-exception v0

    :try_start_7
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v0
.end method

.method public static b(LX/0QB;)LX/GMR;
    .locals 4

    .prologue
    .line 2344539
    new-instance v3, LX/GMR;

    invoke-static {p0}, LX/GKy;->b(LX/0QB;)LX/GKy;

    move-result-object v0

    check-cast v0, LX/GKy;

    invoke-static {p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v1

    check-cast v1, LX/GG6;

    invoke-static {p0}, LX/GNC;->a(LX/0QB;)LX/GNC;

    move-result-object v2

    check-cast v2, LX/GNC;

    invoke-direct {v3, v0, v1, v2}, LX/GMR;-><init>(LX/GKy;LX/GG6;LX/GNC;)V

    .line 2344540
    return-object v3
.end method

.method private static b(LX/GMR;Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2344528
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/GMR;->h:LX/15i;

    iget v2, p0, LX/GMR;->i:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v1, 0x559eed

    invoke-static {v0, v2, v6, v1}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    .line 2344529
    :goto_0
    invoke-virtual {v0, v6}, LX/2uF;->a(I)LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2344530
    invoke-virtual {v0, v6}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2344531
    const-class v4, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-virtual {v3, v0, v7, v4, v5}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    invoke-static {v0}, LX/GCi;->fromGraphQLTypeCallToAction(Lcom/facebook/graphql/enums/GraphQLCallToActionType;)LX/GCi;

    move-result-object v0

    .line 2344532
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_2
    iget-object v4, p0, LX/GMR;->h:LX/15i;

    iget v5, p0, LX/GMR;->i:I

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const/4 v3, 0x3

    invoke-virtual {v4, v5, v3}, LX/15i;->j(II)I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setPaidReach(Ljava/lang/String;)V

    .line 2344533
    const v3, 0x7f080b0b

    invoke-virtual {p2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {v0, p2}, LX/GCi;->getText(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2344534
    invoke-virtual {v2, v1, v6}, LX/15i;->j(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->c(Ljava/lang/String;Ljava/lang/String;)LX/GKw;

    .line 2344535
    return-void

    .line 2344536
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :cond_0
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_0

    .line 2344537
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    .line 2344538
    :catchall_2
    move-exception v0

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2344521
    invoke-super {p0}, LX/GHg;->a()V

    .line 2344522
    const/4 v0, 0x0

    iput-object v0, p0, LX/GMR;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;

    .line 2344523
    iget-object v0, p0, LX/GMR;->m:LX/GNC;

    .line 2344524
    iget-object v1, v0, LX/GNC;->a:LX/GDl;

    invoke-virtual {v1}, LX/GDY;->a()V

    .line 2344525
    iget-object v0, p0, LX/GMR;->m:LX/GNC;

    .line 2344526
    iget-object v1, v0, LX/GNC;->b:LX/GDm;

    invoke-virtual {v1}, LX/GDY;->a()V

    .line 2344527
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 4
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2344482
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;

    const/4 v3, 0x0

    .line 2344483
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2344484
    iput-object p1, p0, LX/GMR;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;

    .line 2344485
    invoke-direct {p0, p1}, LX/GMR;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;)V

    .line 2344486
    iget-object v0, p0, LX/GMR;->b:LX/GKy;

    invoke-virtual {v0}, LX/GKy;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2344487
    const v0, 0x7f080b3d

    invoke-virtual {p2, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setHeaderTitleResource(I)V

    .line 2344488
    :cond_0
    iget-object v0, p0, LX/GMR;->l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v0}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2344489
    new-instance v0, LX/GMP;

    invoke-direct {v0, p0}, LX/GMP;-><init>(LX/GMR;)V

    iput-object v0, p0, LX/GMR;->f:LX/GFR;

    .line 2344490
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2344491
    const/16 v1, 0x8

    iget-object v2, p0, LX/GMR;->f:LX/GFR;

    invoke-virtual {v0, v1, v2}, LX/GCE;->a(ILX/GFR;)V

    .line 2344492
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2344493
    const/4 v1, 0x7

    iget-object v2, p0, LX/GMR;->f:LX/GFR;

    invoke-virtual {v0, v1, v2}, LX/GCE;->a(ILX/GFR;)V

    .line 2344494
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2344495
    iget-object v1, p0, LX/GMR;->l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v1}, LX/GMU;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, LX/GMR;->l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v1}, LX/GMU;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2344496
    const v1, 0x7f080ad6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GMR;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->c(Ljava/lang/String;Ljava/lang/String;)LX/GKw;

    .line 2344497
    :goto_0
    const v1, 0x7f080ad4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0}, LX/GMR;->a(LX/GMR;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, LX/GMR;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v1, v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->a(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 2344498
    :goto_1
    iget-object v0, p0, LX/GMR;->m:LX/GNC;

    .line 2344499
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2344500
    iget-object v2, p0, LX/GMR;->l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0, v1, v2}, LX/GNC;->a(LX/GCE;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, LX/GMR;->n:Landroid/view/View$OnClickListener;

    .line 2344501
    iget-object v0, p0, LX/GMR;->m:LX/GNC;

    iget-object v1, p0, LX/GMR;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->getContext()Landroid/content/Context;

    iget-object v1, p0, LX/GMR;->l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0, v1}, LX/GNC;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Landroid/view/View$OnClickListener;

    move-result-object v0

    iput-object v0, p0, LX/GMR;->o:Landroid/view/View$OnClickListener;

    .line 2344502
    sget-object v0, LX/GMQ;->a:[I

    iget-object v1, p0, LX/GMR;->l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v1

    invoke-virtual {v1}, LX/GGB;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2344503
    :goto_2
    return-void

    .line 2344504
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2344505
    iget-object v1, p0, LX/GMR;->b:LX/GKy;

    invoke-virtual {v1}, LX/GKy;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2344506
    const v1, 0x7f080aa1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GMR;->k:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->c(Ljava/lang/String;Ljava/lang/String;)LX/GKw;

    .line 2344507
    :cond_2
    const v1, 0x7f080ac1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GMR;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->c(Ljava/lang/String;Ljava/lang/String;)LX/GKw;

    .line 2344508
    const v1, 0x7f080ad4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0}, LX/GMR;->a(LX/GMR;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->c(Ljava/lang/String;Ljava/lang/String;)LX/GKw;

    .line 2344509
    goto :goto_1

    .line 2344510
    :pswitch_0
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080ac3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setActionButtonText(Ljava/lang/String;)V

    .line 2344511
    invoke-virtual {p1, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setActionButtonVisibility(I)V

    .line 2344512
    iget-object v0, p0, LX/GMR;->o:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setActionButtonListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 2344513
    :pswitch_1
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080ac6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setActionButtonText(Ljava/lang/String;)V

    .line 2344514
    invoke-virtual {p1, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setActionButtonVisibility(I)V

    .line 2344515
    iget-object v0, p0, LX/GMR;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->setActionButtonListener(Landroid/view/View$OnClickListener;)V

    goto :goto_2

    .line 2344516
    :cond_3
    iget-object v1, p0, LX/GMR;->l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v1}, LX/GMU;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, LX/GMR;->l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v1}, LX/GMU;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2344517
    const v1, 0x7f080ad6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GMR;->a:Ljava/lang/String;

    iget-object p2, p0, LX/GMR;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v1, v2, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->a(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    .line 2344518
    :cond_4
    iget-object v1, p0, LX/GMR;->l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v1}, LX/GMU;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, LX/GMR;->l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-static {v1}, LX/GMU;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2344519
    const v1, 0x7f080ac1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GMR;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->c(Ljava/lang/String;Ljava/lang/String;)LX/GKw;

    goto/16 :goto_0

    .line 2344520
    :cond_5
    const v1, 0x7f080ac1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/GMR;->a:Ljava/lang/String;

    iget-object p2, p0, LX/GMR;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v1, v2, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;->a(Ljava/lang/String;Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 6

    .prologue
    .line 2344470
    move-object v0, p1

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iput-object v0, p0, LX/GMR;->l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2344471
    iget-object v0, p0, LX/GMR;->l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2344472
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v1

    .line 2344473
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    .line 2344474
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v2

    invoke-static {v1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v4

    invoke-static {p1}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v1

    invoke-static {v2, v4, v5, v1}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/GMR;->a:Ljava/lang/String;

    .line 2344475
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->D()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    .line 2344476
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v2

    invoke-static {v1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v4

    invoke-static {p1}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v1

    invoke-static {v2, v4, v5, v1}, LX/GMU;->a(IJLjava/text/NumberFormat;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/GMR;->k:Ljava/lang/String;

    .line 2344477
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->F()J

    move-result-wide v2

    iput-wide v2, p0, LX/GMR;->j:J

    .line 2344478
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->u()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iput-object v1, p0, LX/GMR;->h:LX/15i;

    iput v0, p0, LX/GMR;->i:I

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2344479
    iget-object v0, p0, LX/GMR;->l:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v1, p0, LX/GMR;->c:LX/GG6;

    iget-wide v2, p0, LX/GMR;->j:J

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-virtual {v1, v2, v3}, LX/GG6;->a(J)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(I)V

    .line 2344480
    return-void

    .line 2344481
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
