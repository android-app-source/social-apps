.class public final LX/FVY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2dD;


# instance fields
.field public final synthetic a:LX/FVb;


# direct methods
.method public constructor <init>(LX/FVb;)V
    .locals 0

    .prologue
    .line 2250869
    iput-object p1, p0, LX/FVY;->a:LX/FVb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/0ht;)Z
    .locals 11

    .prologue
    .line 2250870
    iget-object v0, p0, LX/FVY;->a:LX/FVb;

    iget-object v0, v0, LX/FVb;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2250871
    iget-object v0, p0, LX/FVY;->a:LX/FVb;

    iget-object v1, v0, LX/FVb;->g:LX/16H;

    iget-object v0, p0, LX/FVY;->a:LX/FVb;

    iget-object v0, v0, LX/FVb;->b:LX/0am;

    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79Y;

    .line 2250872
    iget-object p0, v0, LX/79Y;->a:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-object v0, p0

    .line 2250873
    const/4 v10, 0x0

    .line 2250874
    iget-object v8, v1, LX/16H;->b:LX/0gh;

    const-string v9, "saved_dashboard"

    const-string v2, "action_name"

    const-string v3, "saved_dashboard_filter_canceled"

    const-string v4, "current_section_type"

    const-string v6, "event_id"

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v7

    move-object v5, v0

    invoke-static/range {v2 .. v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v2

    invoke-virtual {v8, v9, v10, v10, v2}, LX/0gh;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)V

    .line 2250875
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
