.class public final LX/Ghw;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Ghx;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public b:[I

.field public c:I

.field public d:I

.field public final synthetic e:LX/Ghx;


# direct methods
.method public constructor <init>(LX/Ghx;)V
    .locals 1

    .prologue
    .line 2385082
    iput-object p1, p0, LX/Ghw;->e:LX/Ghx;

    .line 2385083
    move-object v0, p1

    .line 2385084
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2385085
    sget v0, LX/Ghz;->a:I

    iput v0, p0, LX/Ghw;->c:I

    .line 2385086
    sget v0, LX/Ghz;->b:I

    iput v0, p0, LX/Ghw;->d:I

    .line 2385087
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2385105
    const-string v0, "GametimeTableRowComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2385088
    if-ne p0, p1, :cond_1

    .line 2385089
    :cond_0
    :goto_0
    return v0

    .line 2385090
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2385091
    goto :goto_0

    .line 2385092
    :cond_3
    check-cast p1, LX/Ghw;

    .line 2385093
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2385094
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2385095
    if-eq v2, v3, :cond_0

    .line 2385096
    iget-object v2, p0, LX/Ghw;->a:LX/0Px;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Ghw;->a:LX/0Px;

    iget-object v3, p1, LX/Ghw;->a:LX/0Px;

    invoke-virtual {v2, v3}, LX/0Px;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2385097
    goto :goto_0

    .line 2385098
    :cond_5
    iget-object v2, p1, LX/Ghw;->a:LX/0Px;

    if-nez v2, :cond_4

    .line 2385099
    :cond_6
    iget-object v2, p0, LX/Ghw;->b:[I

    iget-object v3, p1, LX/Ghw;->b:[I

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 2385100
    goto :goto_0

    .line 2385101
    :cond_7
    iget v2, p0, LX/Ghw;->c:I

    iget v3, p1, LX/Ghw;->c:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 2385102
    goto :goto_0

    .line 2385103
    :cond_8
    iget v2, p0, LX/Ghw;->d:I

    iget v3, p1, LX/Ghw;->d:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2385104
    goto :goto_0
.end method
