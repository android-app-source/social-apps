.class public LX/FW8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# instance fields
.field private final a:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2251656
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2251657
    iput-object p1, p0, LX/FW8;->a:LX/0Uh;

    .line 2251658
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 2251659
    iget-object v0, p0, LX/FW8;->a:LX/0Uh;

    invoke-static {v0}, LX/79u;->a(LX/0Uh;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2251660
    new-instance v0, Lcom/facebook/saved2/ui/Saved2Fragment;

    invoke-direct {v0}, Lcom/facebook/saved2/ui/Saved2Fragment;-><init>()V

    .line 2251661
    :goto_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2251662
    return-object v0

    .line 2251663
    :cond_0
    new-instance v0, Lcom/facebook/saved/fragment/SavedFragment;

    invoke-direct {v0}, Lcom/facebook/saved/fragment/SavedFragment;-><init>()V

    goto :goto_0
.end method
