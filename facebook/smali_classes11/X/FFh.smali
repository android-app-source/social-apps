.class public LX/FFh;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/FFh;


# instance fields
.field public a:LX/0Zb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2217573
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/FFh;
    .locals 4

    .prologue
    .line 2217574
    sget-object v0, LX/FFh;->b:LX/FFh;

    if-nez v0, :cond_1

    .line 2217575
    const-class v1, LX/FFh;

    monitor-enter v1

    .line 2217576
    :try_start_0
    sget-object v0, LX/FFh;->b:LX/FFh;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2217577
    if-eqz v2, :cond_0

    .line 2217578
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2217579
    new-instance p0, LX/FFh;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/FFh;-><init>(LX/0Zb;)V

    .line 2217580
    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    .line 2217581
    iput-object v3, p0, LX/FFh;->a:LX/0Zb;

    .line 2217582
    move-object v0, p0

    .line 2217583
    sput-object v0, LX/FFh;->b:LX/FFh;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2217584
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2217585
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2217586
    :cond_1
    sget-object v0, LX/FFh;->b:LX/FFh;

    return-object v0

    .line 2217587
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2217588
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
