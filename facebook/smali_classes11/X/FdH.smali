.class public LX/FdH;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/FdE;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lcom/facebook/widget/text/BetterTextView;

.field public c:LX/Fcf;

.field public d:LX/Fce;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2263346
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/FdH;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2263347
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2263348
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2263349
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, LX/FdH;->setOrientation(I)V

    .line 2263350
    return-void
.end method


# virtual methods
.method public final a(LX/Fce;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2263351
    invoke-virtual {p0}, LX/FdH;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03129f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/FdH;->b:Lcom/facebook/widget/text/BetterTextView;

    .line 2263352
    iget-object v0, p0, LX/FdH;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2263353
    iget-object v0, p0, LX/FdH;->b:Lcom/facebook/widget/text/BetterTextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setMaxLines(I)V

    .line 2263354
    iget-object v0, p0, LX/FdH;->b:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {p0, v0}, LX/FdH;->addView(Landroid/view/View;)V

    .line 2263355
    iput-object p1, p0, LX/FdH;->d:LX/Fce;

    .line 2263356
    iget-object v0, p0, LX/FdH;->b:Lcom/facebook/widget/text/BetterTextView;

    new-instance v1, LX/FdF;

    invoke-direct {v1, p0}, LX/FdF;-><init>(LX/FdH;)V

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2263357
    return-void
.end method

.method public setOnRadioButtonSelectedListener(LX/Fcf;)V
    .locals 0

    .prologue
    .line 2263358
    iput-object p1, p0, LX/FdH;->c:LX/Fcf;

    .line 2263359
    return-void
.end method

.method public setRadioButtons(LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/FdE;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2263360
    iput-object p1, p0, LX/FdH;->a:LX/0Px;

    .line 2263361
    iget-object v1, p0, LX/FdH;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LX/FdH;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FdE;

    .line 2263362
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, LX/FdE;->setTag(Ljava/lang/Object;)V

    .line 2263363
    invoke-virtual {p0, v0}, LX/FdH;->addView(Landroid/view/View;)V

    .line 2263364
    new-instance v4, LX/FdG;

    invoke-direct {v4, p0}, LX/FdG;-><init>(LX/FdH;)V

    invoke-virtual {v0, v4}, LX/FdE;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2263365
    add-int/lit8 v2, v2, 0x1

    .line 2263366
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2263367
    :cond_0
    return-void
.end method
