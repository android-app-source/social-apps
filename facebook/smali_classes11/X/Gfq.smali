.class public LX/Gfq;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Gfq",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2377991
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2377992
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Gfq;->b:LX/0Zi;

    .line 2377993
    iput-object p1, p0, LX/Gfq;->a:LX/0Ot;

    .line 2377994
    return-void
.end method

.method public static a(LX/0QB;)LX/Gfq;
    .locals 4

    .prologue
    .line 2377995
    const-class v1, LX/Gfq;

    monitor-enter v1

    .line 2377996
    :try_start_0
    sget-object v0, LX/Gfq;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2377997
    sput-object v2, LX/Gfq;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2377998
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2377999
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2378000
    new-instance v3, LX/Gfq;

    const/16 p0, 0x2121

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Gfq;-><init>(LX/0Ot;)V

    .line 2378001
    move-object v0, v3

    .line 2378002
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2378003
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gfq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2378004
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2378005
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static e(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2378006
    const v0, 0x14d62955

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 9

    .prologue
    .line 2378007
    check-cast p2, LX/Gfp;

    .line 2378008
    iget-object v0, p0, LX/Gfq;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;

    iget-object v1, p2, LX/Gfp;->a:LX/GfY;

    iget-object v2, p2, LX/Gfp;->b:LX/1Pp;

    const/4 p2, 0x0

    const/4 v4, 0x0

    .line 2378009
    iget-object v5, v1, LX/GfY;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;

    .line 2378010
    iget-object v3, v1, LX/GfY;->a:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    if-eqz v3, :cond_0

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v3

    if-nez v3, :cond_1

    .line 2378011
    :cond_0
    :goto_0
    move-object v0, v4

    .line 2378012
    return-object v0

    .line 2378013
    :cond_1
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v7

    .line 2378014
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPage;->r()LX/0Px;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPage;->r()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    .line 2378015
    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPage;->r()LX/0Px;

    move-result-object v3

    invoke-virtual {v3, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2378016
    :goto_1
    iget-object v6, v1, LX/GfY;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    if-eqz v6, :cond_5

    iget-object v6, v1, LX/GfY;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 2378017
    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;->w()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v5

    .line 2378018
    :goto_2
    iget-object v6, v1, LX/GfY;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;

    invoke-static {v6}, LX/Gdi;->c(LX/25E;)Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    .line 2378019
    if-nez v6, :cond_6

    move-object v6, v4

    .line 2378020
    :goto_3
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v8

    const/4 p0, 0x2

    invoke-interface {v8, p0}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v8

    if-nez v6, :cond_7

    :goto_4
    invoke-interface {v8, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v6

    const/4 v8, 0x6

    const p0, 0x7f0b0917

    invoke-interface {v6, v8, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v6

    const/4 v8, 0x1

    const p0, 0x7f0b107f

    invoke-interface {v6, v8, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v6

    const/4 v8, 0x3

    const p0, 0x7f0b1080

    invoke-interface {v6, v8, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v6

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v8

    const/4 v1, 0x1

    .line 2378021
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/1ne;->j(I)LX/1ne;

    move-result-object p0

    const p2, 0x7f0b0050

    invoke-virtual {p0, p2}, LX/1ne;->q(I)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v8}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0, v1}, LX/1ne;->t(I)LX/1ne;

    move-result-object p0

    sget-object p2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {p0, p2}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object p0

    invoke-virtual {p0}, LX/1X5;->c()LX/1Di;

    move-result-object p0

    const p2, 0x7f0b0917

    invoke-interface {p0, v1, p2}, LX/1Di;->c(II)LX/1Di;

    move-result-object p0

    invoke-static {p1}, LX/Gfq;->e(LX/1De;)LX/1dQ;

    move-result-object p2

    invoke-interface {p0, p2}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object p0

    move-object v8, p0

    .line 2378022
    invoke-interface {v6, v8}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    if-nez v3, :cond_2

    const-string v3, " "

    .line 2378023
    :cond_2
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v8

    const/4 p0, 0x1

    invoke-virtual {v8, p0}, LX/1ne;->j(I)LX/1ne;

    move-result-object v8

    const p0, 0x7f0b004e

    invoke-virtual {v8, p0}, LX/1ne;->q(I)LX/1ne;

    move-result-object v8

    const p0, 0x7f0a010d

    invoke-virtual {v8, p0}, LX/1ne;->n(I)LX/1ne;

    move-result-object v8

    invoke-virtual {v8, v3}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v8

    sget-object p0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v8, p0}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    invoke-static {p1}, LX/Gfq;->e(LX/1De;)LX/1dQ;

    move-result-object p0

    invoke-interface {v8, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v8

    move-object v3, v8

    .line 2378024
    invoke-interface {v6, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    if-nez v5, :cond_3

    const-string v5, " "

    .line 2378025
    :cond_3
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v6

    const/4 v8, 0x1

    invoke-virtual {v6, v8}, LX/1ne;->j(I)LX/1ne;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v6

    const v8, 0x7f0b004e

    invoke-virtual {v6, v8}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    const v8, 0x7f0a010e

    invoke-virtual {v6, v8}, LX/1ne;->n(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    sget-object v8, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v8}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/4 v8, 0x3

    const p0, 0x7f0b0917

    invoke-interface {v6, v8, p0}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    invoke-static {p1}, LX/Gfq;->e(LX/1De;)LX/1dQ;

    move-result-object v8

    invoke-interface {v6, v8}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v6

    move-object v5, v6

    .line 2378026
    invoke-interface {v3, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v3, v5}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v3

    invoke-interface {v4, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLPage;->w()Z

    move-result v4

    .line 2378027
    iget-object v5, v0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;->b:LX/1vg;

    invoke-virtual {v5, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v6

    if-eqz v4, :cond_8

    const v5, 0x7f021886

    :goto_5
    invoke-virtual {v6, v5}, LX/2xv;->h(I)LX/2xv;

    move-result-object v6

    if-eqz v4, :cond_9

    const v5, 0x7f0a00d2

    :goto_6
    invoke-virtual {v6, v5}, LX/2xv;->j(I)LX/2xv;

    move-result-object v5

    invoke-virtual {v5}, LX/1n6;->b()LX/1dc;

    move-result-object v5

    .line 2378028
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v6

    invoke-virtual {v6, v5}, LX/1o5;->a(LX/1dc;)LX/1o5;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/16 v6, 0x8

    const v7, 0x7f0b010f

    invoke-interface {v5, v6, v7}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    const/4 v6, 0x2

    invoke-interface {v5, v6}, LX/1Di;->b(I)LX/1Di;

    move-result-object v5

    .line 2378029
    const v6, 0x7874efe6

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 2378030
    invoke-interface {v5, v6}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v5

    move-object v4, v5

    .line 2378031
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    goto/16 :goto_0

    :cond_4
    move-object v3, v4

    .line 2378032
    goto/16 :goto_1

    :cond_5
    move-object v5, v4

    .line 2378033
    goto/16 :goto_2

    .line 2378034
    :cond_6
    invoke-static {v6}, LX/1eC;->a(Lcom/facebook/graphql/model/GraphQLImage;)Landroid/net/Uri;

    move-result-object v6

    goto/16 :goto_3

    .line 2378035
    :cond_7
    const/16 v1, 0x2c

    .line 2378036
    iget-object v4, v0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;->d:LX/1nu;

    invoke-virtual {v4, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v4

    invoke-virtual {v4, v6}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v4

    sget-object p0, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v4, p0}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v4

    const/4 p0, 0x1

    invoke-virtual {v4, p0}, LX/1nw;->a(Z)LX/1nw;

    move-result-object v4

    invoke-virtual {v4, v2}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v4

    invoke-virtual {v4}, LX/1X5;->c()LX/1Di;

    move-result-object v4

    const/4 p0, 0x2

    invoke-interface {v4, p0}, LX/1Di;->b(I)LX/1Di;

    move-result-object v4

    invoke-interface {v4, v1}, LX/1Di;->j(I)LX/1Di;

    move-result-object v4

    invoke-interface {v4, v1}, LX/1Di;->r(I)LX/1Di;

    move-result-object v4

    const/4 p0, 0x6

    const v1, 0x7f0b0917

    invoke-interface {v4, p0, v1}, LX/1Di;->c(II)LX/1Di;

    move-result-object v4

    invoke-static {p1}, LX/Gfq;->e(LX/1De;)LX/1dQ;

    move-result-object p0

    invoke-interface {v4, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v4

    move-object v4, v4

    .line 2378037
    goto/16 :goto_4

    .line 2378038
    :cond_8
    const v5, 0x7f021885

    goto/16 :goto_5

    :cond_9
    const v5, 0x7f0a010e

    goto/16 :goto_6
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2378039
    invoke-static {}, LX/1dS;->b()V

    .line 2378040
    iget v0, p1, LX/1dQ;->b:I

    .line 2378041
    sparse-switch v0, :sswitch_data_0

    .line 2378042
    :goto_0
    return-object v2

    .line 2378043
    :sswitch_0
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    .line 2378044
    check-cast v0, LX/Gfp;

    .line 2378045
    iget-object v1, p0, LX/Gfq;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;

    iget-object v3, v0, LX/Gfp;->a:LX/GfY;

    .line 2378046
    iget-object v4, v1, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;->c:LX/Gfl;

    .line 2378047
    iget-object p1, v3, LX/GfY;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object p2

    .line 2378048
    iget-object p0, v4, LX/Gfl;->e:LX/GeG;

    iget-object p1, v3, LX/GfY;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;

    iget-object v0, v3, LX/GfY;->a:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    invoke-static {p1, v0}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v0

    iget-object p1, v3, LX/GfY;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;->x()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    :goto_1
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    const-string v1, "feed_pyml"

    invoke-virtual {p0, p2, v0, p1, v1}, LX/GeG;->a(Lcom/facebook/graphql/model/GraphQLPage;LX/162;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 2378049
    goto :goto_0

    .line 2378050
    :sswitch_1
    check-cast p2, LX/3Ae;

    .line 2378051
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2378052
    check-cast v1, LX/Gfp;

    .line 2378053
    iget-object v3, p0, LX/Gfq;->a:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;

    iget-object v4, v1, LX/Gfp;->a:LX/GfY;

    .line 2378054
    iget-object p0, v3, Lcom/facebook/feedplugins/pyml/rows/paginatedcontentbased/components/PaginatedPymlCardBottomComponentSpec;->c:LX/Gfl;

    iget-object v1, v4, LX/GfY;->b:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/Gfl;->a(Landroid/view/View;Lcom/facebook/graphql/model/GraphQLPage;)V

    .line 2378055
    goto :goto_0

    .line 2378056
    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x14d62955 -> :sswitch_1
        0x7874efe6 -> :sswitch_0
    .end sparse-switch
.end method
