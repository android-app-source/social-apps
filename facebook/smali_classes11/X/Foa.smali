.class public final LX/Foa;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/FoZ;

.field public b:Ljava/lang/String;

.field public c:Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;

.field public d:Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;

.field public e:Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel;


# direct methods
.method public constructor <init>(LX/FoZ;)V
    .locals 0

    .prologue
    .line 2289726
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2289727
    iput-object p1, p0, LX/Foa;->a:LX/FoZ;

    .line 2289728
    return-void
.end method

.method public constructor <init>(Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;)V
    .locals 1

    .prologue
    .line 2289729
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2289730
    sget-object v0, LX/FoZ;->CHARITY:LX/FoZ;

    iput-object v0, p0, LX/Foa;->a:LX/FoZ;

    .line 2289731
    iput-object p1, p0, LX/Foa;->c:Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchResultFragmentModel;

    .line 2289732
    return-void
.end method

.method public constructor <init>(Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel;)V
    .locals 1

    .prologue
    .line 2289733
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2289734
    sget-object v0, LX/FoZ;->HIGHLIGHTED_CHARITY:LX/FoZ;

    iput-object v0, p0, LX/Foa;->a:LX/FoZ;

    .line 2289735
    iput-object p1, p0, LX/Foa;->e:Lcom/facebook/socialgood/protocol/FundraiserCreatePromoModels$FundraiserCreatePromoHighlightedCharityModel;

    .line 2289736
    return-void
.end method

.method public constructor <init>(Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;)V
    .locals 1

    .prologue
    .line 2289737
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2289738
    sget-object v0, LX/FoZ;->DAF_DISCLOSURE:LX/FoZ;

    iput-object v0, p0, LX/Foa;->a:LX/FoZ;

    .line 2289739
    iput-object p1, p0, LX/Foa;->d:Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;

    .line 2289740
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2289741
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2289742
    sget-object v0, LX/FoZ;->SUBTITLE:LX/FoZ;

    iput-object v0, p0, LX/Foa;->a:LX/FoZ;

    .line 2289743
    iput-object p1, p0, LX/Foa;->b:Ljava/lang/String;

    .line 2289744
    return-void
.end method
