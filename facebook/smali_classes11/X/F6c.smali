.class public final LX/F6c;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/F6d;


# direct methods
.method public constructor <init>(LX/F6d;)V
    .locals 0

    .prologue
    .line 2200605
    iput-object p1, p0, LX/F6c;->a:LX/F6d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 2200606
    check-cast p1, Lcom/facebook/ipc/model/FacebookPhonebookContact;

    check-cast p2, Lcom/facebook/ipc/model/FacebookPhonebookContact;

    .line 2200607
    iget-object v0, p1, Lcom/facebook/ipc/model/FacebookPhonebookContact;->name:Ljava/lang/String;

    iget-object v1, p0, LX/F6c;->a:LX/F6d;

    iget-object v1, v1, LX/F6d;->a:LX/F6e;

    iget-object v1, v1, LX/F6e;->r:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p2, Lcom/facebook/ipc/model/FacebookPhonebookContact;->name:Ljava/lang/String;

    iget-object v2, p0, LX/F6c;->a:LX/F6d;

    iget-object v2, v2, LX/F6d;->a:LX/F6e;

    iget-object v2, v2, LX/F6e;->r:LX/0W9;

    invoke-virtual {v2}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
