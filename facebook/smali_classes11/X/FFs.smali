.class public LX/FFs;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2217944
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/messaging/model/messages/Message;)LX/0m9;
    .locals 10

    .prologue
    .line 2217945
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 2217946
    iget-object v1, p0, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    .line 2217947
    if-nez v1, :cond_0

    .line 2217948
    :goto_0
    return-object v0

    .line 2217949
    :cond_0
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->t()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v1

    .line 2217950
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dg()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;

    move-result-object v2

    .line 2217951
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 2217952
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    .line 2217953
    new-instance v3, LX/0m9;

    sget-object v6, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v6}, LX/0m9;-><init>(LX/0mC;)V

    const-string v6, "latitude"

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;->e()D

    move-result-wide v8

    invoke-virtual {v3, v6, v8, v9}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    move-result-object v3

    const-string v6, "longitude"

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;->cS_()D

    move-result-wide v8

    invoke-virtual {v3, v6, v8, v9}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    move-result-object v3

    const-string v6, "altitude"

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;->c()D

    move-result-wide v8

    invoke-virtual {v3, v6, v8, v9}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    move-result-object v3

    const-string v6, "accuracy"

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;->a()D

    move-result-wide v8

    invoke-virtual {v3, v6, v8, v9}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    move-result-object v3

    const-string v6, "altitudeAccuracy"

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;->b()D

    move-result-wide v8

    invoke-virtual {v3, v6, v8, v9}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    move-result-object v3

    const-string v6, "heading"

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;->d()D

    move-result-wide v8

    invoke-virtual {v3, v6, v8, v9}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    move-result-object v3

    const-string v6, "speed"

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;->cT_()D

    move-result-wide v8

    invoke-virtual {v3, v6, v8, v9}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    move-result-object v2

    const-string v3, "timestamp"

    invoke-virtual {v2, v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    move-result-object v2

    .line 2217954
    const-string v3, "coordinates"

    invoke-virtual {v0, v3, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 2217955
    const-string v2, "expiration_time"

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bb()J

    move-result-wide v4

    invoke-virtual {v0, v2, v4, v5}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    goto :goto_0
.end method
