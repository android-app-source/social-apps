.class public LX/H1s;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0SG;


# direct methods
.method public constructor <init>(LX/0SG;)V
    .locals 0

    .prologue
    .line 2416006
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2416007
    iput-object p1, p0, LX/H1s;->a:LX/0SG;

    .line 2416008
    return-void
.end method

.method private static a(Landroid/graphics/RectF;)D
    .locals 2

    .prologue
    .line 2415993
    invoke-virtual {p0}, Landroid/graphics/RectF;->height()F

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/RectF;->width()F

    move-result v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    return-wide v0
.end method

.method public static final a(Landroid/graphics/RectF;Ljava/util/List;)D
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/graphics/RectF;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/nearby/model/MapTile;",
            ">;)D"
        }
    .end annotation

    .prologue
    .line 2415994
    const-wide/16 v0, 0x0

    .line 2415995
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/model/MapTile;

    .line 2415996
    new-instance v1, Landroid/graphics/RectF;

    iget-object v5, v0, Lcom/facebook/nearby/model/MapTile;->bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->l()D

    move-result-wide v6

    double-to-float v5, v6

    iget-object v6, v0, Lcom/facebook/nearby/model/MapTile;->bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->j()D

    move-result-wide v6

    double-to-float v6, v6

    iget-object v7, v0, Lcom/facebook/nearby/model/MapTile;->bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->a()D

    move-result-wide v8

    double-to-float v7, v8

    iget-object v0, v0, Lcom/facebook/nearby/model/MapTile;->bounds:Lcom/facebook/graphql/model/GraphQLGeoRectangle;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->k()D

    move-result-wide v8

    double-to-float v0, v8

    invoke-direct {v1, v5, v6, v7, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 2415997
    invoke-static {p0, v1}, LX/H1s;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    .line 2415998
    invoke-static {v0}, LX/H1s;->a(Landroid/graphics/RectF;)D

    move-result-wide v0

    add-double/2addr v0, v2

    move-wide v2, v0

    .line 2415999
    goto :goto_0

    .line 2416000
    :cond_0
    invoke-static {p0}, LX/H1s;->a(Landroid/graphics/RectF;)D

    move-result-wide v0

    div-double v0, v2, v0

    return-wide v0
.end method

.method private static a(Landroid/graphics/RectF;Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 5
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2416001
    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v1, p0, Landroid/graphics/RectF;->top:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 2416002
    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, Landroid/graphics/RectF;->bottom:F

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 2416003
    iget v2, p1, Landroid/graphics/RectF;->left:F

    iget v3, p0, Landroid/graphics/RectF;->left:F

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 2416004
    iget v3, p1, Landroid/graphics/RectF;->right:F

    iget v4, p0, Landroid/graphics/RectF;->right:F

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 2416005
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v2, v0, v3, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v4
.end method
