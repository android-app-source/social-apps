.class public LX/GzK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/02t;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/2Lt;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/quicklog/QuickPerformanceLogger;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0oz;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kb;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/perftest/PerfTestConfig;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/os/PowerManager;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2411772
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2411773
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2411774
    iput-object v0, p0, LX/GzK;->a:LX/0Ot;

    .line 2411775
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2411776
    iput-object v0, p0, LX/GzK;->b:LX/0Ot;

    .line 2411777
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2411778
    iput-object v0, p0, LX/GzK;->c:LX/0Ot;

    .line 2411779
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2411780
    iput-object v0, p0, LX/GzK;->d:LX/0Ot;

    .line 2411781
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2411782
    iput-object v0, p0, LX/GzK;->e:LX/0Ot;

    .line 2411783
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2411784
    iput-object v0, p0, LX/GzK;->f:LX/0Ot;

    .line 2411785
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2411786
    iput-object v0, p0, LX/GzK;->g:LX/0Ot;

    .line 2411787
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2411788
    iput-object v0, p0, LX/GzK;->h:LX/0Ot;

    .line 2411789
    return-void
.end method

.method public static a(Ljava/util/Map;Ljava/lang/String;I)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 2411769
    invoke-interface {p0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2411770
    const/4 v0, -0x1

    const/16 v1, 0x3b

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    invoke-interface {p0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v7

    move v2, p2

    move-object v6, p1

    invoke-static/range {v0 .. v7}, Lcom/facebook/loom/logger/Logger;->a(IIIIJLjava/lang/String;Ljava/lang/String;)I

    .line 2411771
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2411766
    iget-object v0, p0, LX/GzK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Lt;

    sget-object v1, LX/GzN;->Uploading:LX/GzN;

    invoke-virtual {v0, v1}, LX/2Lt;->a(LX/GzN;)V

    .line 2411767
    iget-object v0, p0, LX/GzK;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x7c0001

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISI)V

    .line 2411768
    return-void
.end method

.method public final a(IIII)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 2411753
    :goto_0
    if-lez p1, :cond_0

    .line 2411754
    iget-object v0, p0, LX/GzK;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x7c0007

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISI)V

    .line 2411755
    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    .line 2411756
    :cond_0
    :goto_1
    if-lez p2, :cond_1

    .line 2411757
    iget-object v0, p0, LX/GzK;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x7c0008

    invoke-interface {v0, v1, v4, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISI)V

    .line 2411758
    add-int/lit8 p2, p2, -0x1

    goto :goto_1

    .line 2411759
    :cond_1
    :goto_2
    if-lez p3, :cond_2

    .line 2411760
    iget-object v0, p0, LX/GzK;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x7c0009

    invoke-interface {v0, v1, v4, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISI)V

    .line 2411761
    add-int/lit8 p3, p3, -0x1

    goto :goto_2

    .line 2411762
    :cond_2
    :goto_3
    if-lez p4, :cond_3

    .line 2411763
    iget-object v0, p0, LX/GzK;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x7c000a

    invoke-interface {v0, v1, v4, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISI)V

    .line 2411764
    add-int/lit8 p4, p4, -0x1

    goto :goto_3

    .line 2411765
    :cond_3
    return-void
.end method

.method public final a(LX/037;)V
    .locals 2

    .prologue
    .line 2411790
    iget-object v0, p0, LX/GzK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Lt;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/2Lt;->a(Z)V

    .line 2411791
    return-void
.end method

.method public final a(Ljava/io/File;)V
    .locals 4

    .prologue
    .line 2411750
    iget-object v0, p0, LX/GzK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Lt;

    sget-object v1, LX/GzN;->Successful:LX/GzN;

    invoke-virtual {v0, v1}, LX/2Lt;->a(LX/GzN;)V

    .line 2411751
    iget-object v0, p0, LX/GzK;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x7c0005

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISI)V

    .line 2411752
    return-void
.end method

.method public final b(LX/037;)V
    .locals 12

    .prologue
    .line 2411704
    const-wide/16 v8, 0x1

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/16 v1, 0x3b

    const/4 v0, -0x1

    .line 2411705
    sget-boolean v2, LX/008;->deoptTaint:Z

    if-nez v2, :cond_4

    const/4 v2, 0x1

    .line 2411706
    :goto_0
    const v10, 0x7c000b

    if-eqz v2, :cond_5

    move-wide v6, v8

    :goto_1
    invoke-static {v0, v1, v10, v6, v7}, Lcom/facebook/loom/logger/Logger;->a(IIIJ)I

    .line 2411707
    iget-object v2, p0, LX/GzK;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {v2}, LX/2FP;->a(Landroid/content/Context;)Ljava/util/Map;

    move-result-object v2

    .line 2411708
    const-string v6, "os_ver"

    const v7, 0x7c0013

    invoke-static {v2, v6, v7}, LX/GzK;->a(Ljava/util/Map;Ljava/lang/String;I)V

    .line 2411709
    const-string v6, "device_type"

    const v7, 0x7c000e

    invoke-static {v2, v6, v7}, LX/GzK;->a(Ljava/util/Map;Ljava/lang/String;I)V

    .line 2411710
    const-string v6, "brand"

    const v7, 0x7c000f

    invoke-static {v2, v6, v7}, LX/GzK;->a(Ljava/util/Map;Ljava/lang/String;I)V

    .line 2411711
    const-string v6, "manufacturer"

    const v7, 0x7c0010

    invoke-static {v2, v6, v7}, LX/GzK;->a(Ljava/util/Map;Ljava/lang/String;I)V

    .line 2411712
    const-string v6, "year_class"

    const v7, 0x7c0011

    invoke-static {v2, v6, v7}, LX/GzK;->a(Ljava/util/Map;Ljava/lang/String;I)V

    .line 2411713
    iget-object v2, p0, LX/GzK;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0oz;

    iget-object v6, p0, LX/GzK;->e:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0kb;

    .line 2411714
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 2411715
    if-eqz v2, :cond_0

    .line 2411716
    const-string v10, "connection_class"

    invoke-virtual {v2}, LX/0oz;->b()LX/0p3;

    move-result-object v11

    invoke-virtual {v11}, LX/0p3;->name()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v7, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2411717
    :cond_0
    if-eqz v6, :cond_1

    .line 2411718
    const-string v10, "network_type"

    invoke-virtual {v6}, LX/0kb;->k()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v7, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2411719
    const-string v10, "network_subtype"

    invoke-virtual {v6}, LX/0kb;->l()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v7, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2411720
    :cond_1
    move-object v2, v7

    .line 2411721
    const-string v6, "connection_class"

    const v7, 0x7c0014

    invoke-static {v2, v6, v7}, LX/GzK;->a(Ljava/util/Map;Ljava/lang/String;I)V

    .line 2411722
    const-string v6, "network_type"

    const v7, 0x7c0015

    invoke-static {v2, v6, v7}, LX/GzK;->a(Ljava/util/Map;Ljava/lang/String;I)V

    .line 2411723
    const-string v6, "network_subtype"

    const v7, 0x7c0016

    invoke-static {v2, v6, v7}, LX/GzK;->a(Ljava/util/Map;Ljava/lang/String;I)V

    .line 2411724
    iget-object v2, p0, LX/GzK;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    .line 2411725
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 2411726
    sget v6, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v10, 0x15

    if-lt v6, v10, :cond_9

    .line 2411727
    invoke-virtual {v2}, Landroid/os/PowerManager;->isPowerSaveMode()Z

    move-result v6

    if-eqz v6, :cond_8

    .line 2411728
    const-string v6, "true"

    .line 2411729
    :goto_2
    const-string v10, "low_power_state"

    invoke-interface {v7, v10, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2411730
    move-object v2, v7

    .line 2411731
    const-string v6, "low_power_state"

    const v7, 0x8d0023

    invoke-static {v2, v6, v7}, LX/GzK;->a(Ljava/util/Map;Ljava/lang/String;I)V

    .line 2411732
    iget-object v2, p0, LX/GzK;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0Uh;

    const/16 v6, 0xf3

    invoke-virtual {v2, v6, v3}, LX/0Uh;->a(IZ)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2411733
    invoke-static {}, LX/0V8;->a()LX/0V8;

    move-result-object v2

    invoke-static {v2}, LX/44B;->a(LX/0V8;)Ljava/util/Map;

    move-result-object v2

    .line 2411734
    const-string v6, "free_disk_percent"

    const v7, 0x7c0021

    invoke-static {v2, v6, v7}, LX/GzK;->a(Ljava/util/Map;Ljava/lang/String;I)V

    .line 2411735
    :cond_2
    iget-object v2, p0, LX/GzK;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    .line 2411736
    sget-object v2, Lcom/facebook/common/perftest/base/PerfTestConfigBase;->v:Ljava/lang/String;

    move-object v7, v2

    .line 2411737
    if-eqz v7, :cond_3

    .line 2411738
    const v2, 0x7c0019

    const-string v6, "PERF_TEST_INFO"

    invoke-static/range {v0 .. v7}, Lcom/facebook/loom/logger/Logger;->a(IIIIJLjava/lang/String;Ljava/lang/String;)I

    .line 2411739
    :cond_3
    new-instance v2, Ljava/io/File;

    const-string v3, "/proc/sys/kernel/perf_event_paranoid"

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2411740
    const v6, 0x7c001a

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_6

    move-wide v2, v8

    :goto_3
    invoke-static {v0, v1, v6, v2, v3}, Lcom/facebook/loom/logger/Logger;->a(IIIJ)I

    .line 2411741
    const v3, 0x7c001e

    iget-object v2, p0, LX/GzK;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static {v2}, LX/03w;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_7

    :goto_4
    invoke-static {v0, v1, v3, v8, v9}, Lcom/facebook/loom/logger/Logger;->a(IIIJ)I

    .line 2411742
    invoke-static {}, Lcom/facebook/loom/provider/NativeEventProvider;->a()V

    .line 2411743
    return-void

    :cond_4
    move v2, v3

    .line 2411744
    goto/16 :goto_0

    :cond_5
    move-wide v6, v4

    .line 2411745
    goto/16 :goto_1

    :cond_6
    move-wide v2, v4

    .line 2411746
    goto :goto_3

    :cond_7
    move-wide v8, v4

    .line 2411747
    goto :goto_4

    .line 2411748
    :cond_8
    const-string v6, "false"

    goto :goto_2

    .line 2411749
    :cond_9
    const-string v6, "unknown"

    goto :goto_2
.end method

.method public final b(Ljava/io/File;)V
    .locals 4

    .prologue
    .line 2411694
    iget-object v0, p0, LX/GzK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Lt;

    sget-object v1, LX/GzN;->Failed:LX/GzN;

    invoke-virtual {v0, v1}, LX/2Lt;->a(LX/GzN;)V

    .line 2411695
    iget-object v0, p0, LX/GzK;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x7c0005

    const/4 v2, 0x3

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISI)V

    .line 2411696
    return-void
.end method

.method public final c(LX/037;)V
    .locals 2

    .prologue
    .line 2411702
    iget-object v0, p0, LX/GzK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Lt;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/2Lt;->a(Z)V

    .line 2411703
    return-void
.end method

.method public final d(LX/037;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2411697
    iget-object v0, p0, LX/GzK;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Lt;

    invoke-virtual {v0, v3}, LX/2Lt;->a(Z)V

    .line 2411698
    iget-object v0, p0, LX/GzK;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/quicklog/QuickPerformanceLogger;

    const v1, 0x7c0002

    .line 2411699
    iget-short v2, p1, LX/037;->i:S

    move v2, v2

    .line 2411700
    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->a(ISI)V

    .line 2411701
    return-void
.end method
