.class public final LX/FqM;
.super LX/1PF;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/TimelineFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/TimelineFragment;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2292680
    iput-object p1, p0, LX/FqM;->a:Lcom/facebook/timeline/TimelineFragment;

    invoke-direct {p0, p2}, LX/1PF;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 2292681
    iget-object v0, p0, LX/FqM;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FqM;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/TimelineFragment;->F:LX/G4x;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FqM;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/TimelineFragment;->aD:LX/1PF;

    if-eq v0, p0, :cond_1

    .line 2292682
    :cond_0
    :goto_0
    return-void

    .line 2292683
    :cond_1
    iget-object v0, p0, LX/FqM;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/TimelineFragment;->R:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kb;

    invoke-virtual {v0}, LX/0kb;->d()Z

    move-result v0

    .line 2292684
    if-eqz v0, :cond_0

    .line 2292685
    iget-object v0, p0, LX/FqM;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-boolean v0, v0, Lcom/facebook/timeline/TimelineFragment;->aT:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/FqM;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/TimelineFragment;->F:LX/G4x;

    invoke-virtual {v0}, LX/G4x;->size()I

    move-result v0

    const/16 v1, 0x14

    if-ge v0, v1, :cond_0

    .line 2292686
    iget-object v0, p0, LX/FqM;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/TimelineFragment;->ba:LX/BQ1;

    .line 2292687
    iget-object v1, v0, LX/BPy;->e:LX/BPx;

    move-object v0, v1

    .line 2292688
    sget-object v1, LX/BPx;->FINAL_DATA:LX/BPx;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, LX/FqM;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/TimelineFragment;->aG:LX/0ta;

    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LX/FqM;->a:Lcom/facebook/timeline/TimelineFragment;

    iget-object v0, v0, Lcom/facebook/timeline/TimelineFragment;->aH:LX/0ta;

    sget-object v1, LX/0ta;->FROM_SERVER:LX/0ta;

    if-eq v0, v1, :cond_0

    .line 2292689
    :cond_2
    iget-object v0, p0, LX/FqM;->a:Lcom/facebook/timeline/TimelineFragment;

    const/4 v1, 0x1

    .line 2292690
    iput-boolean v1, v0, Lcom/facebook/timeline/TimelineFragment;->aT:Z

    .line 2292691
    iget-object v0, p0, LX/FqM;->a:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/TimelineFragment;->mJ_()V

    goto :goto_0
.end method
