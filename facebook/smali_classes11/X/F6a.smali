.class public final LX/F6a;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:LX/F6e;


# direct methods
.method public constructor <init>(LX/F6e;ILandroid/view/View;)V
    .locals 0

    .prologue
    .line 2200581
    iput-object p1, p0, LX/F6a;->c:LX/F6e;

    iput p2, p0, LX/F6a;->a:I

    iput-object p3, p0, LX/F6a;->b:Landroid/view/View;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    .line 2200582
    iget-object v0, p0, LX/F6a;->c:LX/F6e;

    iget v1, p0, LX/F6a;->a:I

    iget-object v2, p0, LX/F6a;->b:Landroid/view/View;

    .line 2200583
    iget-object v3, v0, LX/F6e;->c:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/ipc/model/FacebookPhonebookContact;

    .line 2200584
    invoke-virtual {v0, v3}, LX/F6e;->a(Lcom/facebook/ipc/model/FacebookPhonebookContact;)J

    move-result-wide v5

    .line 2200585
    iget-object v4, v0, LX/F6e;->m:Ljava/util/Set;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2200586
    iget-object v4, v0, LX/F6e;->m:Ljava/util/Set;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2200587
    iget-object v4, v0, LX/F6e;->n:Ljava/util/Hashtable;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2200588
    iget-object v4, v0, LX/F6e;->n:Ljava/util/Hashtable;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/util/Hashtable;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2200589
    if-eqz v4, :cond_0

    .line 2200590
    const/4 v7, 0x1

    invoke-interface {v4, v7}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2200591
    :cond_0
    iget-object v4, v0, LX/F6e;->n:Ljava/util/Hashtable;

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2200592
    :cond_1
    const v4, 0x7f0d271a

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-static {v0, v3, v1, v2}, LX/F6e;->a(LX/F6e;Lcom/facebook/ipc/model/FacebookPhonebookContact;ILandroid/view/View;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2200593
    const v3, 0x7f0d271b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 2200594
    iget-object v3, v0, LX/F6e;->i:LX/9Tk;

    iget-object v4, v0, LX/F6e;->f:LX/89v;

    iget-object v4, v4, LX/89v;->value:Ljava/lang/String;

    sget-object v5, LX/9Ti;->FRIEND_FINDER_API:LX/9Ti;

    invoke-virtual {v3, v4, v5}, LX/9Tk;->b(Ljava/lang/String;LX/9Ti;)V

    .line 2200595
    :cond_2
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 2200596
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2200597
    iget-object v0, p0, LX/F6a;->c:LX/F6e;

    iget-object v0, v0, LX/F6e;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2200598
    return-void
.end method
