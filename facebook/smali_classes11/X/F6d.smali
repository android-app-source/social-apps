.class public final LX/F6d;
.super LX/3nE;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/3nE",
        "<",
        "Ljava/util/List",
        "<+",
        "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
        ">;",
        "Ljava/lang/Void;",
        "Ljava/util/List",
        "<+",
        "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/F6e;


# direct methods
.method public constructor <init>(LX/F6e;)V
    .locals 0

    .prologue
    .line 2200635
    iput-object p1, p0, LX/F6d;->a:LX/F6e;

    invoke-direct {p0}, LX/3nE;-><init>()V

    return-void
.end method


# virtual methods
.method public final a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2200630
    check-cast p1, [Ljava/util/List;

    .line 2200631
    new-instance v0, LX/F6c;

    invoke-direct {v0, p0}, LX/F6c;-><init>(LX/F6d;)V

    .line 2200632
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x0

    aget-object v2, p1, v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2200633
    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2200634
    return-object v1
.end method

.method public final onPostExecute(Ljava/lang/Object;)V
    .locals 10

    .prologue
    .line 2200608
    check-cast p1, Ljava/util/List;

    .line 2200609
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v0, -0x1

    .line 2200610
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 2200611
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v0, v7

    .line 2200612
    :goto_0
    move-object v0, v0

    .line 2200613
    iget-object v1, p0, LX/F6d;->a:LX/F6e;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, v1, LX/F6e;->c:Ljava/util/List;

    .line 2200614
    iget-object v1, p0, LX/F6d;->a:LX/F6e;

    iput-object v0, v1, LX/F6e;->d:Ljava/util/List;

    .line 2200615
    iget-object v0, p0, LX/F6d;->a:LX/F6e;

    iget-object v0, v0, LX/F6e;->m:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 2200616
    iget-object v0, p0, LX/F6d;->a:LX/F6e;

    const v1, 0x465199a

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2200617
    return-void

    .line 2200618
    :cond_0
    const-string v1, ""

    .line 2200619
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v3

    move v5, v0

    move-object v6, v1

    move v1, v0

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;

    .line 2200620
    add-int/lit8 v1, v1, 0x1

    .line 2200621
    iget-object v0, v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->name:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iget-object v9, p0, LX/F6d;->a:LX/F6e;

    iget-object v9, v9, LX/F6e;->r:LX/0W9;

    invoke-virtual {v9}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    .line 2200622
    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 2200623
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 2200624
    goto :goto_1

    .line 2200625
    :cond_1
    if-ltz v5, :cond_2

    .line 2200626
    new-instance v9, LX/F6b;

    invoke-direct {v9, v6, v5, v2}, LX/F6b;-><init>(Ljava/lang/String;II)V

    invoke-interface {v7, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    move v2, v4

    move v5, v1

    move-object v6, v0

    .line 2200627
    goto :goto_1

    .line 2200628
    :cond_3
    new-instance v0, LX/F6b;

    invoke-direct {v0, v6, v5, v2}, LX/F6b;-><init>(Ljava/lang/String;II)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v0, v7

    .line 2200629
    goto :goto_0
.end method
