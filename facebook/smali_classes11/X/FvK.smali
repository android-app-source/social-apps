.class public LX/FvK;
.super LX/Fv4;
.source ""


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/BP0;LX/BQ1;LX/0ad;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/BP0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2301242
    invoke-direct {p0, p1, p4, p2, p3}, LX/Fv4;-><init>(Landroid/content/Context;LX/0ad;LX/BP0;LX/BQ1;)V

    .line 2301243
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2301241
    invoke-static {}, LX/FvJ;->cachedValues()[LX/FvJ;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 2301237
    invoke-static {}, LX/FvJ;->cachedValues()[LX/FvJ;

    move-result-object v0

    aget-object v0, v0, p1

    .line 2301238
    sget-object v1, LX/FvI;->a:[I

    invoke-virtual {v0}, LX/FvJ;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2301239
    invoke-static {p1}, LX/Fv4;->b(I)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2301240
    :pswitch_0
    new-instance v0, LX/G0t;

    iget-object v1, p0, LX/Fv4;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/G0t;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2301236
    invoke-static {}, LX/FvJ;->cachedValues()[LX/FvJ;

    move-result-object v0

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final a([Z)V
    .locals 2

    .prologue
    .line 2301225
    const/4 v0, 0x1

    .line 2301226
    sget-boolean v1, LX/007;->j:Z

    move v1, v1

    .line 2301227
    if-eqz v1, :cond_0

    .line 2301228
    iget-object v0, p0, LX/Fv4;->d:LX/BQ1;

    .line 2301229
    invoke-virtual {v0}, LX/BQ1;->q()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2301230
    iget-object v1, v0, LX/BQ1;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    if-nez v1, :cond_2

    .line 2301231
    const/4 v1, 0x0

    .line 2301232
    :goto_0
    move v1, v1

    .line 2301233
    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 2301234
    :cond_0
    sget-object v1, LX/FvJ;->NAVTILES:LX/FvJ;

    invoke-virtual {v1}, LX/FvJ;->ordinal()I

    move-result v1

    aput-boolean v0, p1, v1

    .line 2301235
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    iget-object v1, v0, LX/BQ1;->a:Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderUserFieldsModel;->r()Z

    move-result v1

    goto :goto_0
.end method

.method public final a(Landroid/view/View;I)Z
    .locals 3

    .prologue
    .line 2301219
    invoke-static {}, LX/FvJ;->cachedValues()[LX/FvJ;

    move-result-object v0

    aget-object v0, v0, p2

    .line 2301220
    sget-object v1, LX/FvJ;->NAVTILES:LX/FvJ;

    if-ne v0, v1, :cond_0

    .line 2301221
    const/4 v0, 0x0

    return v0

    .line 2301222
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown itemViewType "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2301224
    invoke-virtual {p0, p1}, LX/Fv4;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FvJ;

    invoke-virtual {v0}, LX/FvJ;->ordinal()I

    move-result v0

    return v0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2301223
    invoke-static {}, LX/FvJ;->cachedValues()[LX/FvJ;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
