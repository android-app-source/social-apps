.class public final LX/GJs;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/GJt;


# direct methods
.method public constructor <init>(LX/GJt;)V
    .locals 0

    .prologue
    .line 2339882
    iput-object p1, p0, LX/GJs;->a:LX/GJt;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .prologue
    const/4 v8, 0x5

    const/4 v13, 0x0

    const/4 v12, 0x2

    const/4 v7, 0x1

    const v0, -0x21795c6b

    invoke-static {v12, v7, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2339858
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 2339859
    iget-object v0, p0, LX/GJs;->a:LX/GJt;

    iget-wide v0, v0, LX/GJt;->f:J

    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2339860
    new-instance v2, LX/GJr;

    invoke-direct {v2, p0}, LX/GJr;-><init>(LX/GJs;)V

    .line 2339861
    new-instance v0, Landroid/app/DatePickerDialog;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v5, v7}, Ljava/util/Calendar;->get(I)I

    move-result v3

    invoke-virtual {v5, v12}, Ljava/util/Calendar;->get(I)I

    move-result v4

    invoke-virtual {v5, v8}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 2339862
    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->getDatePicker()Landroid/widget/DatePicker;

    move-result-object v1

    .line 2339863
    invoke-virtual {v1, v13}, Landroid/widget/DatePicker;->setCalendarViewShown(Z)V

    .line 2339864
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 2339865
    invoke-virtual {v2, v8, v7}, Ljava/util/Calendar;->add(II)V

    .line 2339866
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 2339867
    iget-object v4, p0, LX/GJs;->a:LX/GJt;

    iget-object v4, v4, LX/GJt;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2339868
    iget-object v5, v4, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v4, v5

    .line 2339869
    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->F()J

    move-result-wide v4

    const-wide/16 v8, 0x3e8

    mul-long/2addr v4, v8

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2339870
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v4

    .line 2339871
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v8

    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v10

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 2339872
    const/16 v2, 0xb

    invoke-virtual {v4, v2, v13}, Ljava/util/Calendar;->set(II)V

    .line 2339873
    const/16 v2, 0xc

    invoke-virtual {v4, v2, v13}, Ljava/util/Calendar;->set(II)V

    .line 2339874
    const/16 v2, 0xd

    invoke-virtual {v4, v2, v7}, Ljava/util/Calendar;->set(II)V

    .line 2339875
    invoke-virtual {v4}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Landroid/widget/DatePicker;->setMinDate(J)V

    .line 2339876
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 2339877
    const/4 v4, 0x6

    const/16 v5, 0x16c

    invoke-virtual {v2, v4, v5}, Ljava/util/Calendar;->add(II)V

    .line 2339878
    invoke-virtual {v3}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    iget-object v3, p0, LX/GJs;->a:LX/GJt;

    iget-object v3, v3, LX/GJt;->b:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->k()I

    move-result v3

    int-to-long v8, v3

    const-wide/32 v10, 0x5265c00

    mul-long/2addr v8, v10

    add-long/2addr v4, v8

    .line 2339879
    invoke-virtual {v2}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/widget/DatePicker;->setMaxDate(J)V

    .line 2339880
    invoke-virtual {v0}, Landroid/app/DatePickerDialog;->show()V

    .line 2339881
    const v0, -0x2253a540

    invoke-static {v12, v12, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
