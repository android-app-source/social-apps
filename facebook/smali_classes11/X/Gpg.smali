.class public LX/Gpg;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;",
        "Lcom/facebook/instantshopping/model/data/InstantShoppingMapBlockData;",
        ">;"
    }
.end annotation


# instance fields
.field public d:LX/Go7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Go4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private f:LX/GoE;


# direct methods
.method public constructor <init>(Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;)V
    .locals 2

    .prologue
    .line 2395447
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2395448
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/Gpg;

    invoke-static {v0}, LX/Go7;->a(LX/0QB;)LX/Go7;

    move-result-object p1

    check-cast p1, LX/Go7;

    invoke-static {v0}, LX/Go4;->a(LX/0QB;)LX/Go4;

    move-result-object v0

    check-cast v0, LX/Go4;

    iput-object p1, p0, LX/Gpg;->d:LX/Go7;

    iput-object v0, p0, LX/Gpg;->e:LX/Go4;

    .line 2395449
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 13

    .prologue
    .line 2395458
    check-cast p1, LX/GpB;

    .line 2395459
    invoke-static {p1}, LX/Co1;->a(LX/Clu;)Landroid/os/Bundle;

    move-result-object v0

    .line 2395460
    iget-object v1, p0, LX/CnT;->d:LX/CnG;

    move-object v1, v1

    .line 2395461
    check-cast v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;

    .line 2395462
    invoke-interface {p1}, LX/GoY;->D()LX/GoE;

    move-result-object v2

    iput-object v2, p0, LX/Gpg;->f:LX/GoE;

    .line 2395463
    iget-object v2, p0, LX/Gpg;->f:LX/GoE;

    .line 2395464
    iget-object v3, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->a:LX/Go0;

    invoke-virtual {v3, v2}, LX/Go0;->a(LX/GoE;)V

    .line 2395465
    invoke-virtual {v1, v0}, LX/Cod;->a(Landroid/os/Bundle;)V

    .line 2395466
    iget-object v0, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->u:LX/Grg;

    invoke-virtual {v1, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 2395467
    iget-object v0, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    invoke-virtual {v0}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2395468
    iget-object v0, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->v:LX/Grj;

    invoke-virtual {v1, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 2395469
    :cond_0
    iget-object v0, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->w:LX/Grn;

    invoke-virtual {v1, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 2395470
    iget-object v0, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->x:LX/Grp;

    invoke-virtual {v1, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 2395471
    iget-object v0, p1, LX/GpB;->p:LX/0Px;

    move-object v0, v0

    .line 2395472
    iget-object v2, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    .line 2395473
    iput-object v0, v2, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->x:LX/0Px;

    .line 2395474
    iget-wide v11, p1, LX/GpB;->f:D

    move-wide v2, v11

    .line 2395475
    iget-wide v11, p1, LX/GpB;->g:D

    move-wide v4, v11

    .line 2395476
    iget-wide v11, p1, LX/GpB;->h:D

    move-wide v6, v11

    .line 2395477
    iget-wide v11, p1, LX/GpB;->i:D

    move-wide v8, v11

    .line 2395478
    iget-object v0, p1, LX/GpB;->e:Ljava/lang/String;

    move-object v10, v0

    .line 2395479
    sget-object v0, LX/Cqw;->a:LX/Cqw;

    invoke-virtual {v1, v0}, LX/Cos;->a(LX/Cqw;)V

    .line 2395480
    iput-object v10, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->q:Ljava/lang/String;

    .line 2395481
    invoke-static {}, LX/697;->a()LX/696;

    move-result-object v0

    new-instance v11, Lcom/facebook/android/maps/model/LatLng;

    invoke-direct {v11, v4, v5, v8, v9}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v0, v11}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    move-result-object v0

    new-instance v11, Lcom/facebook/android/maps/model/LatLng;

    invoke-direct {v11, v6, v7, v2, v3}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v0, v11}, LX/696;->a(Lcom/facebook/android/maps/model/LatLng;)LX/696;

    move-result-object v0

    invoke-virtual {v0}, LX/696;->a()LX/697;

    move-result-object v0

    iput-object v0, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->A:LX/697;

    .line 2395482
    iget-object v0, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    new-instance v11, LX/D1Q;

    invoke-virtual {v1}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-direct {v11, v12}, LX/D1Q;-><init>(Landroid/content/Context;)V

    iget v12, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->n:F

    .line 2395483
    iput v12, v11, LX/D1Q;->j:F

    .line 2395484
    move-object v11, v11

    .line 2395485
    iget-object v12, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->q:Ljava/lang/String;

    .line 2395486
    iput-object v12, v11, LX/D1Q;->c:Ljava/lang/String;

    .line 2395487
    move-object v11, v11

    .line 2395488
    iget-object v12, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->r:Lcom/facebook/storelocator/StoreLocatorRecyclerView;

    .line 2395489
    iput-object v12, v11, LX/D1Q;->h:Lcom/facebook/storelocator/StoreLocatorRecyclerView;

    .line 2395490
    move-object v11, v11

    .line 2395491
    sget-object v12, LX/D1T;->CANVAS_AD:LX/D1T;

    .line 2395492
    iput-object v12, v11, LX/D1Q;->e:LX/D1T;

    .line 2395493
    move-object v11, v11

    .line 2395494
    iget-object v12, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->t:Landroid/view/View;

    .line 2395495
    iput-object v12, v11, LX/D1Q;->g:Landroid/view/View;

    .line 2395496
    move-object v11, v11

    .line 2395497
    iget-object v12, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->A:LX/697;

    .line 2395498
    iput-object v12, v11, LX/D1Q;->f:LX/697;

    .line 2395499
    move-object v11, v11

    .line 2395500
    invoke-virtual {v11}, LX/D1Q;->a()LX/D1R;

    move-result-object v11

    iget-object v12, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->d:LX/D15;

    invoke-virtual {v0, v11, v12}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->a(LX/D1R;LX/D14;)V

    .line 2395501
    iget-object v0, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->m:Lcom/facebook/instantshopping/view/widget/InstantShoppingMapView;

    iget-object v11, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    invoke-virtual {v0, v11}, Lcom/facebook/android/maps/MapView;->a(LX/68J;)V

    .line 2395502
    iget-object v0, p1, LX/GpB;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2395503
    iget-object v2, p1, LX/GpB;->m:Ljava/lang/Integer;

    move-object v2, v2

    .line 2395504
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2395505
    iget-object v3, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    sget-object v5, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->e:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v3, v4, v5}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->a(Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2395506
    iget-object v3, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->l:Lcom/facebook/drawee/fbpipeline/FbDraweeView;

    invoke-virtual {v3, v2}, Lcom/facebook/drawee/fbpipeline/FbDraweeView;->setColorFilter(I)V

    .line 2395507
    iget-object v0, p1, LX/GpB;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2395508
    iget-object v2, p1, LX/GpB;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2395509
    iget-object v3, p1, LX/GpB;->n:Ljava/lang/Integer;

    move-object v3, v3

    .line 2395510
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 2395511
    iget-object v4, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->z:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v4, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2395512
    iget-object v4, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->z:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v4, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2395513
    iget-object v4, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->y:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v4, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2395514
    iget-object v4, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->y:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v4, v3}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    .line 2395515
    iget-object v0, p1, LX/GpB;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2395516
    iget-object v2, p1, LX/GpB;->n:Ljava/lang/Integer;

    move-object v2, v2

    .line 2395517
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2395518
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2395519
    iget-object v3, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->o:Lcom/facebook/resources/ui/FbTextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2395520
    :goto_0
    iget-object v0, p1, LX/GpB;->k:Landroid/graphics/Typeface;

    move-object v0, v0

    .line 2395521
    if-eqz v0, :cond_1

    .line 2395522
    iget-object v0, p1, LX/GpB;->k:Landroid/graphics/Typeface;

    move-object v0, v0

    .line 2395523
    iget-object v2, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->z:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2395524
    iget-object v2, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->y:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2395525
    iget-object v2, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 2395526
    :cond_1
    return-void

    .line 2395527
    :cond_2
    iget-object v3, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->o:Lcom/facebook/resources/ui/FbTextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2395528
    iget-object v3, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v0}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2395529
    iget-object v3, v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingMapBlockViewImpl;->o:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2395454
    invoke-super {p0, p1}, LX/CnT;->a(Landroid/os/Bundle;)V

    .line 2395455
    iget-object v0, p0, LX/Gpg;->d:LX/Go7;

    const-string v1, "storelocator_element_start"

    iget-object v2, p0, LX/Gpg;->f:LX/GoE;

    invoke-virtual {v2}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2395456
    iget-object v0, p0, LX/Gpg;->e:LX/Go4;

    iget-object v1, p0, LX/Gpg;->f:LX/GoE;

    invoke-virtual {v1}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Go4;->a(Ljava/lang/String;)V

    .line 2395457
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2395450
    invoke-super {p0, p1}, LX/CnT;->b(Landroid/os/Bundle;)V

    .line 2395451
    iget-object v0, p0, LX/Gpg;->d:LX/Go7;

    const-string v1, "storelocator_element_end"

    iget-object v2, p0, LX/Gpg;->f:LX/GoE;

    invoke-virtual {v2}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2395452
    iget-object v0, p0, LX/Gpg;->e:LX/Go4;

    iget-object v1, p0, LX/Gpg;->f:LX/GoE;

    invoke-virtual {v1}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Go4;->b(Ljava/lang/String;)V

    .line 2395453
    return-void
.end method
