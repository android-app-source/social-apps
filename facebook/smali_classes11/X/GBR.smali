.class public final LX/GBR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V
    .locals 0

    .prologue
    .line 2326988
    iput-object p1, p0, LX/GBR;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 2326982
    iget-object v0, p0, LX/GBR;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    sget-object v1, LX/GBh;->RESEND_BUTTON:LX/GBh;

    invoke-static {v0, v1}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->b(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;LX/GBh;)V

    .line 2326983
    iget-object v0, p0, LX/GBR;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->j:LX/2Dt;

    invoke-virtual {v0}, LX/2Dt;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2326984
    iget-object v0, p0, LX/GBR;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->c:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/GBR;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    const v3, 0x7f083525

    invoke-virtual {v2, v3}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2326985
    :cond_0
    iget-object v0, p0, LX/GBR;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->y:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/widget/text/BetterEditTextView;->a()V

    .line 2326986
    iget-object v0, p0, LX/GBR;->a:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->y:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->b()Z

    .line 2326987
    return-void
.end method
