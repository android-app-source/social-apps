.class public final LX/GZh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4oV;


# instance fields
.field public final synthetic a:Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;)V
    .locals 0

    .prologue
    .line 2367094
    iput-object p1, p0, LX/GZh;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(III)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2367095
    iget-object v0, p0, LX/GZh;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    iget-object v0, v0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->k:LX/E8s;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GZh;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    iget-boolean v0, v0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->t:Z

    if-nez v0, :cond_0

    .line 2367096
    iget-object v0, p0, LX/GZh;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    iget-object v3, v0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->m:Landroid/view/View;

    iget-object v0, p0, LX/GZh;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    iget-object v4, v0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->c:Lcom/facebook/widget/ScrollingAwareScrollView;

    iget-object v0, p0, LX/GZh;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    iget-object v0, v0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->k:LX/E8s;

    invoke-virtual {v0}, LX/E8s;->getMeasuredHeight()I

    move-result v0

    if-ge p2, v0, :cond_2

    move v0, v1

    :goto_0
    sget-object v5, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->s:[I

    iget-object v6, p0, LX/GZh;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    iget-object v6, v6, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->b:LX/0hB;

    invoke-virtual {v6}, LX/0hB;->d()I

    move-result v6

    invoke-static {v3, v4, v0, v5, v6}, LX/8FX;->a(Landroid/view/View;Landroid/view/ViewGroup;I[II)LX/3rL;

    move-result-object v3

    .line 2367097
    iget-object v4, p0, LX/GZh;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    iget-object v0, v3, LX/3rL;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2367098
    iput-boolean v0, v4, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->t:Z

    .line 2367099
    iget-object v0, p0, LX/GZh;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    iget-boolean v0, v0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->t:Z

    if-eqz v0, :cond_0

    .line 2367100
    iget-object v4, p0, LX/GZh;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    iget-object v0, v3, LX/3rL;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->E_(I)V

    .line 2367101
    :cond_0
    iget-object v0, p0, LX/GZh;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    iget v0, v0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->p:I

    if-eq v0, p2, :cond_1

    iget-object v0, p0, LX/GZh;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    iget-object v0, v0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->n:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/GZh;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    iget-object v0, v0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->k:LX/E8s;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/GZh;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2367102
    iget-object v0, p0, LX/GZh;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    iget-object v0, v0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->k:LX/E8s;

    invoke-virtual {v0}, LX/E8s;->getMeasuredHeight()I

    move-result v0

    if-ge p2, v0, :cond_3

    .line 2367103
    iget-object v0, p0, LX/GZh;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    iget-object v0, v0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->n:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v2, p0, LX/GZh;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    iget-object v2, v2, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->c:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v0, v2, v1}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Landroid/view/ViewGroup;I)V

    .line 2367104
    :goto_1
    iget-object v0, p0, LX/GZh;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    .line 2367105
    iput p2, v0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->p:I

    .line 2367106
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 2367107
    goto :goto_0

    .line 2367108
    :cond_3
    iget-object v0, p0, LX/GZh;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    iget-object v0, v0, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->n:Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;

    iget-object v1, p0, LX/GZh;->a:Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;

    iget-object v1, v1, Lcom/facebook/commerce/storefront/fragments/StorefrontReactNativeFragment;->c:Lcom/facebook/widget/ScrollingAwareScrollView;

    invoke-virtual {v0, v1, v2}, Lcom/facebook/pages/identity/fragments/surface/PagesSurfaceFragment;->a(Landroid/view/ViewGroup;I)V

    goto :goto_1
.end method
