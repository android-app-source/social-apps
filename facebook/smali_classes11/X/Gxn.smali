.class public final LX/Gxn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Landroid/widget/TextView;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Landroid/animation/ObjectAnimator;

.field public final synthetic e:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;ILandroid/widget/TextView;Ljava/lang/String;Landroid/animation/ObjectAnimator;)V
    .locals 0

    .prologue
    .line 2408466
    iput-object p1, p0, LX/Gxn;->e:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iput p2, p0, LX/Gxn;->a:I

    iput-object p3, p0, LX/Gxn;->b:Landroid/widget/TextView;

    iput-object p4, p0, LX/Gxn;->c:Ljava/lang/String;

    iput-object p5, p0, LX/Gxn;->d:Landroid/animation/ObjectAnimator;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 2408467
    return-void
.end method

.method public final onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    .line 2408468
    iget-object v1, p0, LX/Gxn;->e:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    monitor-enter v1

    .line 2408469
    :try_start_0
    iget v0, p0, LX/Gxn;->a:I

    iget-object v2, p0, LX/Gxn;->e:Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;

    iget v2, v2, Lcom/facebook/languages/switcher/fragment/LanguageSwitcherFragment;->p:I

    if-eq v0, v2, :cond_0

    iget-object v0, p0, LX/Gxn;->b:Landroid/widget/TextView;

    instance-of v0, v0, Landroid/widget/RadioButton;

    if-eqz v0, :cond_1

    .line 2408470
    :cond_0
    iget-object v0, p0, LX/Gxn;->b:Landroid/widget/TextView;

    iget-object v2, p0, LX/Gxn;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2408471
    iget-object v0, p0, LX/Gxn;->d:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 2408472
    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 2408473
    return-void
.end method

.method public final onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .prologue
    .line 2408474
    return-void
.end method
