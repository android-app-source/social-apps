.class public final LX/F0y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;)V
    .locals 0

    .prologue
    .line 2191018
    iput-object p1, p0, LX/F0y;->a:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2191019
    iget-object v0, p0, LX/F0y;->a:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;

    const/4 v5, 0x0

    .line 2191020
    iget-object v1, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;->c:Lcom/facebook/goodwill/feed/rows/ThrowbackFriendList;

    .line 2191021
    iget-object v2, v1, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendList;->a:LX/0Px;

    move-object v1, v2

    .line 2191022
    invoke-virtual {v1, p3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;

    .line 2191023
    sget-object v1, LX/0ax;->bE:Ljava/lang/String;

    .line 2191024
    iget-object v2, v4, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2191025
    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 2191026
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2191027
    iget-object v2, v4, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2191028
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2191029
    iget-object v3, v4, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2191030
    iget-object v6, v4, Lcom/facebook/goodwill/feed/rows/ThrowbackFriend;->c:Ljava/lang/String;

    move-object v4, v6

    .line 2191031
    move-object v6, v5

    invoke-static/range {v1 .. v6}, LX/5ve;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/1f8;)V

    .line 2191032
    iget-object v2, v0, Lcom/facebook/goodwill/feed/rows/ThrowbackFriendsFragment;->d:LX/17W;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v7, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2191033
    return-void
.end method
