.class public final LX/GWX;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 16

    .prologue
    .line 2362145
    const/4 v12, 0x0

    .line 2362146
    const/4 v11, 0x0

    .line 2362147
    const/4 v10, 0x0

    .line 2362148
    const/4 v9, 0x0

    .line 2362149
    const/4 v8, 0x0

    .line 2362150
    const/4 v7, 0x0

    .line 2362151
    const/4 v6, 0x0

    .line 2362152
    const/4 v5, 0x0

    .line 2362153
    const/4 v4, 0x0

    .line 2362154
    const/4 v3, 0x0

    .line 2362155
    const/4 v2, 0x0

    .line 2362156
    const/4 v1, 0x0

    .line 2362157
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->START_OBJECT:LX/15z;

    if-eq v13, v14, :cond_1

    .line 2362158
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2362159
    const/4 v1, 0x0

    .line 2362160
    :goto_0
    return v1

    .line 2362161
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2362162
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->END_OBJECT:LX/15z;

    if-eq v13, v14, :cond_b

    .line 2362163
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v13

    .line 2362164
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2362165
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v14

    sget-object v15, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v14, v15, :cond_1

    if-eqz v13, :cond_1

    .line 2362166
    const-string v14, "can_viewer_like"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 2362167
    const/4 v2, 0x1

    .line 2362168
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto :goto_1

    .line 2362169
    :cond_2
    const-string v14, "category_names"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 2362170
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v11

    goto :goto_1

    .line 2362171
    :cond_3
    const-string v14, "category_type"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 2362172
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/facebook/graphql/enums/GraphQLPageCategoryType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLPageCategoryType;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    goto :goto_1

    .line 2362173
    :cond_4
    const-string v14, "commerce_store"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_5

    .line 2362174
    invoke-static/range {p0 .. p1}, LX/GWW;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 2362175
    :cond_5
    const-string v14, "does_viewer_like"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 2362176
    const/4 v1, 0x1

    .line 2362177
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v8

    goto :goto_1

    .line 2362178
    :cond_6
    const-string v14, "id"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_7

    .line 2362179
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_1

    .line 2362180
    :cond_7
    const-string v14, "name"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_8

    .line 2362181
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    goto/16 :goto_1

    .line 2362182
    :cond_8
    const-string v14, "page_likers"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_9

    .line 2362183
    invoke-static/range {p0 .. p1}, LX/7ii;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 2362184
    :cond_9
    const-string v14, "profile_picture"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_a

    .line 2362185
    invoke-static/range {p0 .. p1}, LX/7ij;->a(LX/15w;LX/186;)I

    move-result v4

    goto/16 :goto_1

    .line 2362186
    :cond_a
    const-string v14, "viewer_profile_permissions"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_0

    .line 2362187
    invoke-static/range {p0 .. p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v3

    goto/16 :goto_1

    .line 2362188
    :cond_b
    const/16 v13, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->c(I)V

    .line 2362189
    if-eqz v2, :cond_c

    .line 2362190
    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v12}, LX/186;->a(IZ)V

    .line 2362191
    :cond_c
    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v11}, LX/186;->b(II)V

    .line 2362192
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v10}, LX/186;->b(II)V

    .line 2362193
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v9}, LX/186;->b(II)V

    .line 2362194
    if-eqz v1, :cond_d

    .line 2362195
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v8}, LX/186;->a(IZ)V

    .line 2362196
    :cond_d
    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 2362197
    const/4 v1, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2362198
    const/4 v1, 0x7

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 2362199
    const/16 v1, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v4}, LX/186;->b(II)V

    .line 2362200
    const/16 v1, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v3}, LX/186;->b(II)V

    .line 2362201
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const/16 v4, 0x9

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2362202
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2362203
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2362204
    if-eqz v0, :cond_0

    .line 2362205
    const-string v1, "can_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362206
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2362207
    :cond_0
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2362208
    if-eqz v0, :cond_1

    .line 2362209
    const-string v0, "category_names"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362210
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2362211
    :cond_1
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2362212
    if-eqz v0, :cond_2

    .line 2362213
    const-string v0, "category_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362214
    invoke-virtual {p0, p1, v3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2362215
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2362216
    if-eqz v0, :cond_3

    .line 2362217
    const-string v1, "commerce_store"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362218
    invoke-static {p0, v0, p2}, LX/GWW;->a(LX/15i;ILX/0nX;)V

    .line 2362219
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2362220
    if-eqz v0, :cond_4

    .line 2362221
    const-string v1, "does_viewer_like"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362222
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2362223
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2362224
    if-eqz v0, :cond_5

    .line 2362225
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362226
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2362227
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2362228
    if-eqz v0, :cond_6

    .line 2362229
    const-string v1, "name"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362230
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2362231
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2362232
    if-eqz v0, :cond_7

    .line 2362233
    const-string v1, "page_likers"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362234
    invoke-static {p0, v0, p2}, LX/7ii;->a(LX/15i;ILX/0nX;)V

    .line 2362235
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2362236
    if-eqz v0, :cond_8

    .line 2362237
    const-string v1, "profile_picture"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362238
    invoke-static {p0, v0, p2}, LX/7ij;->a(LX/15i;ILX/0nX;)V

    .line 2362239
    :cond_8
    invoke-virtual {p0, p1, v4}, LX/15i;->g(II)I

    move-result v0

    .line 2362240
    if-eqz v0, :cond_9

    .line 2362241
    const-string v0, "viewer_profile_permissions"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362242
    invoke-virtual {p0, p1, v4}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2362243
    :cond_9
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2362244
    return-void
.end method
