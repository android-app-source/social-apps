.class public LX/F7A;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/23P;

.field public final b:LX/2hX;

.field public final c:LX/2dj;

.field public final d:LX/2do;

.field public final e:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(LX/23P;LX/2hX;LX/2dj;LX/2do;Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2201276
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2201277
    iput-object p1, p0, LX/F7A;->a:LX/23P;

    .line 2201278
    iput-object p2, p0, LX/F7A;->b:LX/2hX;

    .line 2201279
    iput-object p3, p0, LX/F7A;->c:LX/2dj;

    .line 2201280
    iput-object p4, p0, LX/F7A;->d:LX/2do;

    .line 2201281
    iput-object p5, p0, LX/F7A;->e:Landroid/content/res/Resources;

    .line 2201282
    return-void
.end method

.method public static a(LX/F7A;I)Ljava/lang/CharSequence;
    .locals 3
    .param p0    # LX/F7A;
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param

    .prologue
    .line 2201275
    iget-object v0, p0, LX/F7A;->a:LX/23P;

    iget-object v1, p0, LX/F7A;->e:Landroid/content/res/Resources;

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/23P;->getTransformation(Ljava/lang/CharSequence;Landroid/view/View;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/F7A;Lcom/facebook/fbui/widget/contentview/ContentView;LX/83X;)V
    .locals 6

    .prologue
    .line 2201302
    invoke-interface {p2}, LX/2lq;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2201303
    invoke-interface {p2}, LX/2lr;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2201304
    invoke-interface {p2}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v0

    .line 2201305
    invoke-interface {p2}, LX/83X;->c()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    .line 2201306
    const-string v2, ""

    invoke-virtual {p1, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2201307
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v2, :cond_0

    .line 2201308
    const v0, 0x7f080f85

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    .line 2201309
    :goto_0
    const-string v0, "%s %s"

    invoke-virtual {p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->getTitleText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->getSubtitleText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2201310
    return-void

    .line 2201311
    :cond_0
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v2, :cond_1

    if-eq v0, v1, :cond_1

    .line 2201312
    const v0, 0x7f080f8a

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    goto :goto_0

    .line 2201313
    :cond_1
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v2, :cond_5

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v1, v2, :cond_5

    const/4 v2, 0x1

    :goto_1
    move v2, v2

    .line 2201314
    if-eqz v2, :cond_2

    .line 2201315
    const v0, 0x7f080f86

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    goto :goto_0

    .line 2201316
    :cond_2
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v0, v2, :cond_6

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-ne v1, v2, :cond_6

    const/4 v2, 0x1

    :goto_2
    move v0, v2

    .line 2201317
    if-eqz v0, :cond_3

    .line 2201318
    const v0, 0x7f080fa2

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    goto :goto_0

    .line 2201319
    :cond_3
    invoke-interface {p2}, LX/2ls;->h()Ljava/lang/String;

    move-result-object v0

    .line 2201320
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2201321
    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2201322
    :cond_4
    invoke-interface {p2}, LX/2lq;->e()I

    move-result v0

    .line 2201323
    if-lez v0, :cond_7

    iget-object v1, p0, LX/F7A;->e:Landroid/content/res/Resources;

    const v2, 0x7f0f005c

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    move-object v0, v0

    .line 2201324
    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    :cond_6
    const/4 v2, 0x0

    goto :goto_2

    :cond_7
    const-string v0, ""

    goto :goto_3
.end method

.method public static b(LX/0QB;)LX/F7A;
    .locals 6

    .prologue
    .line 2201325
    new-instance v0, LX/F7A;

    invoke-static {p0}, LX/23P;->b(LX/0QB;)LX/23P;

    move-result-object v1

    check-cast v1, LX/23P;

    invoke-static {p0}, LX/2hX;->b(LX/0QB;)LX/2hX;

    move-result-object v2

    check-cast v2, LX/2hX;

    invoke-static {p0}, LX/2dj;->b(LX/0QB;)LX/2dj;

    move-result-object v3

    check-cast v3, LX/2dj;

    invoke-static {p0}, LX/2do;->a(LX/0QB;)LX/2do;

    move-result-object v4

    check-cast v4, LX/2do;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-direct/range {v0 .. v5}, LX/F7A;-><init>(LX/23P;LX/2hX;LX/2dj;LX/2do;Landroid/content/res/Resources;)V

    .line 2201326
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/friending/common/list/FriendListItemView;LX/83X;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2201283
    invoke-static {p0, p1, p2}, LX/F7A;->a(LX/F7A;Lcom/facebook/fbui/widget/contentview/ContentView;LX/83X;)V

    .line 2201284
    invoke-virtual {p1, v3}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setEnableActionButton(Z)V

    .line 2201285
    sget-object v0, LX/F79;->a:[I

    invoke-interface {p2}, LX/2lq;->f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2201286
    :goto_0
    invoke-virtual {p1, v3}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setShowActionButton(Z)V

    .line 2201287
    new-instance v0, LX/F78;

    invoke-direct {v0, p0, p2}, LX/F78;-><init>(LX/F7A;LX/83X;)V

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2201288
    :goto_1
    return-void

    .line 2201289
    :pswitch_0
    sget-object v0, LX/EyR;->PRIMARY:LX/EyR;

    iget-object v1, p0, LX/F7A;->e:Landroid/content/res/Resources;

    const v2, 0x7f020b78

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/friending/common/list/FriendListItemView;->a(LX/EyR;Landroid/graphics/drawable/Drawable;)V

    .line 2201290
    const v0, 0x7f080f7b

    invoke-static {p0, v0}, LX/F7A;->a(LX/F7A;I)Ljava/lang/CharSequence;

    move-result-object v0

    const v1, 0x7f080f7c

    invoke-static {p0, v1}, LX/F7A;->a(LX/F7A;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/friending/common/list/FriendListItemView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2201291
    iget-object v0, p0, LX/F7A;->e:Landroid/content/res/Resources;

    const v1, 0x7f080f9c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2201292
    :pswitch_1
    sget-object v0, LX/EyR;->SECONDARY:LX/EyR;

    iget-object v1, p0, LX/F7A;->e:Landroid/content/res/Resources;

    const v2, 0x7f020b7a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/friending/common/list/FriendListItemView;->a(LX/EyR;Landroid/graphics/drawable/Drawable;)V

    .line 2201293
    const v0, 0x7f080017

    invoke-static {p0, v0}, LX/F7A;->a(LX/F7A;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0, v4}, Lcom/facebook/friending/common/list/FriendListItemView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2201294
    iget-object v0, p0, LX/F7A;->e:Landroid/content/res/Resources;

    const v1, 0x7f080f9e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2201295
    :pswitch_2
    sget-object v0, LX/EyR;->PRIMARY:LX/EyR;

    iget-object v1, p0, LX/F7A;->e:Landroid/content/res/Resources;

    const v2, 0x7f020b78

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/friending/common/list/FriendListItemView;->a(LX/EyR;Landroid/graphics/drawable/Drawable;)V

    .line 2201296
    const v0, 0x7f080f7b

    invoke-static {p0, v0}, LX/F7A;->a(LX/F7A;I)Ljava/lang/CharSequence;

    move-result-object v0

    const v1, 0x7f080f7c

    invoke-static {p0, v1}, LX/F7A;->a(LX/F7A;I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/friending/common/list/FriendListItemView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2201297
    iget-object v0, p0, LX/F7A;->e:Landroid/content/res/Resources;

    const v1, 0x7f080f9f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2201298
    :pswitch_3
    sget-object v0, LX/EyR;->SECONDARY:LX/EyR;

    iget-object v1, p0, LX/F7A;->e:Landroid/content/res/Resources;

    const v2, 0x7f020b7e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/facebook/friending/common/list/FriendListItemView;->a(LX/EyR;Landroid/graphics/drawable/Drawable;)V

    .line 2201299
    const v0, 0x7f080f76

    invoke-static {p0, v0}, LX/F7A;->a(LX/F7A;I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0, v4}, Lcom/facebook/friending/common/list/FriendListItemView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2201300
    iget-object v0, p0, LX/F7A;->e:Landroid/content/res/Resources;

    const v1, 0x7f080f9d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setActionButtonContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 2201301
    :pswitch_4
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;->setShowActionButton(Z)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method
