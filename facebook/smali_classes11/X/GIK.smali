.class public final LX/GIK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/GIL;


# direct methods
.method public constructor <init>(LX/GIL;)V
    .locals 0

    .prologue
    .line 2336359
    iput-object p1, p0, LX/GIK;->a:LX/GIL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 2336366
    iget-object v0, p0, LX/GIK;->a:LX/GIL;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2336367
    iput-object v1, v0, LX/GIL;->e:Ljava/lang/String;

    .line 2336368
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2336364
    iget-object v0, p0, LX/GIK;->a:LX/GIL;

    iget-object v0, v0, LX/GIL;->k:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->a(Z)V

    .line 2336365
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4

    .prologue
    .line 2336360
    iget-object v0, p0, LX/GIK;->a:LX/GIL;

    iget-object v0, v0, LX/GIL;->i:LX/GCE;

    sget-object v1, LX/GG8;->ADDRESS:LX/GG8;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/GCE;->a(LX/GG8;Z)V

    .line 2336361
    iget-object v0, p0, LX/GIK;->a:LX/GIL;

    iget-object v0, v0, LX/GIL;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;

    iget-object v1, p0, LX/GIK;->a:LX/GIL;

    iget-object v1, v1, LX/GIL;->p:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2336362
    iget-object v0, p0, LX/GIK;->a:LX/GIL;

    iget-object v0, v0, LX/GIL;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;

    iget-object v1, p0, LX/GIK;->a:LX/GIL;

    iget-object v1, v1, LX/GIL;->p:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 2336363
    return-void
.end method
