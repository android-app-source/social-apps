.class public LX/FgM;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2270243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2270244
    iput-object p1, p0, LX/FgM;->a:Landroid/content/res/Resources;

    .line 2270245
    return-void
.end method

.method public static a(LX/0QB;)LX/FgM;
    .locals 2

    .prologue
    .line 2270240
    new-instance v1, LX/FgM;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    invoke-direct {v1, v0}, LX/FgM;-><init>(Landroid/content/res/Resources;)V

    .line 2270241
    move-object v0, v1

    .line 2270242
    return-object v0
.end method

.method public static a(LX/3Ai;Ljava/lang/String;Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;)V
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/GroupForSaleStoriesPostKind;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 2270223
    invoke-virtual {p0, v1}, LX/3Ai;->setCheckable(Z)Landroid/view/MenuItem;

    .line 2270224
    iget-boolean v0, p2, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->c:Z

    move v0, v0

    .line 2270225
    if-eqz v0, :cond_0

    .line 2270226
    iget-object v0, p2, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2270227
    if-ne v0, p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, LX/3Ai;->setChecked(Z)Landroid/view/MenuItem;

    .line 2270228
    iput-boolean v1, p0, LX/3Ai;->j:Z

    .line 2270229
    iput-boolean v1, p0, LX/3Ai;->k:Z

    .line 2270230
    return-void

    .line 2270231
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/GroupForSaleStoriesPostKind;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 2270232
    iget-boolean v0, p0, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->c:Z

    move v0, v0

    .line 2270233
    if-eqz v0, :cond_0

    .line 2270234
    iget-object v0, p0, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2270235
    if-eq v0, p1, :cond_1

    :cond_0
    move v0, v1

    .line 2270236
    :goto_0
    iput-boolean v1, p0, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->c:Z

    .line 2270237
    iput-object p1, p0, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->a:Ljava/lang/String;

    .line 2270238
    return v0

    .line 2270239
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
