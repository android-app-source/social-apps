.class public LX/G5c;
.super LX/G5b;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/3Oq;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/G5c;


# instance fields
.field public final b:LX/7BO;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2319007
    sget-object v0, LX/3Oq;->FRIEND:LX/3Oq;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/G5c;->a:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/3fr;LX/0TD;LX/2RQ;LX/7BO;)V
    .locals 0
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/SearchRequestExecutor;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2319008
    invoke-direct {p0, p1, p2, p3}, LX/G5b;-><init>(LX/3fr;LX/0TD;LX/2RQ;)V

    .line 2319009
    iput-object p4, p0, LX/G5c;->b:LX/7BO;

    .line 2319010
    return-void
.end method

.method public static a(LX/0QB;)LX/G5c;
    .locals 7

    .prologue
    .line 2319011
    sget-object v0, LX/G5c;->c:LX/G5c;

    if-nez v0, :cond_1

    .line 2319012
    const-class v1, LX/G5c;

    monitor-enter v1

    .line 2319013
    :try_start_0
    sget-object v0, LX/G5c;->c:LX/G5c;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2319014
    if-eqz v2, :cond_0

    .line 2319015
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2319016
    new-instance p0, LX/G5c;

    invoke-static {v0}, LX/3fr;->a(LX/0QB;)LX/3fr;

    move-result-object v3

    check-cast v3, LX/3fr;

    invoke-static {v0}, LX/44g;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, LX/0TD;

    invoke-static {v0}, LX/2RQ;->b(LX/0QB;)LX/2RQ;

    move-result-object v5

    check-cast v5, LX/2RQ;

    invoke-static {v0}, LX/7BO;->a(LX/0QB;)LX/7BO;

    move-result-object v6

    check-cast v6, LX/7BO;

    invoke-direct {p0, v3, v4, v5, v6}, LX/G5c;-><init>(LX/3fr;LX/0TD;LX/2RQ;LX/7BO;)V

    .line 2319017
    move-object v0, p0

    .line 2319018
    sput-object v0, LX/G5c;->c:LX/G5c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2319019
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2319020
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2319021
    :cond_1
    sget-object v0, LX/G5c;->c:LX/G5c;

    return-object v0

    .line 2319022
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2319023
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()LX/0QK;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0QK",
            "<",
            "LX/6N1;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/api/SearchTypeaheadResult;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2319024
    new-instance v0, LX/G5a;

    invoke-direct {v0, p0}, LX/G5a;-><init>(LX/G5c;)V

    return-object v0
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/3Oq;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2319025
    sget-object v0, LX/G5c;->a:LX/0Px;

    return-object v0
.end method
