.class public final LX/FeC;
.super LX/1LN;
.source ""


# instance fields
.field public final synthetic a:LX/FeD;


# direct methods
.method public constructor <init>(LX/FeD;)V
    .locals 0

    .prologue
    .line 2265241
    iput-object p1, p0, LX/FeC;->a:LX/FeD;

    invoke-direct {p0}, LX/1LN;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 11

    .prologue
    .line 2265242
    check-cast p1, LX/1Zf;

    .line 2265243
    iget-object v0, p0, LX/FeC;->a:LX/FeD;

    iget-object v0, v0, LX/FeD;->a:LX/Cxk;

    iget-object v1, p1, LX/1Zf;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, LX/CxA;->c(Ljava/lang/String;)Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;

    move-result-object v5

    .line 2265244
    if-eqz v5, :cond_0

    invoke-static {v5}, LX/8eM;->k(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2265245
    invoke-static {v5}, LX/8eM;->k(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 2265246
    iget-object v0, p0, LX/FeC;->a:LX/FeD;

    iget-object v0, v0, LX/FeD;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/20h;

    iget-object v1, p1, LX/1Zf;->a:Ljava/lang/String;

    iget-object v3, p1, LX/1Zf;->c:LX/1zt;

    invoke-virtual {v0, v2, v1, v3}, LX/20h;->a(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;LX/1zt;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2265247
    iget-object v1, p0, LX/FeC;->a:LX/FeD;

    iget-object v1, v1, LX/FeD;->a:LX/Cxk;

    invoke-interface {v1, v0}, LX/CxA;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2265248
    iget-object v0, p0, LX/FeC;->a:LX/FeD;

    iget-object v0, v0, LX/FeD;->a:LX/Cxk;

    invoke-interface {v0}, LX/1Pq;->iN_()V

    .line 2265249
    iget-object v0, p0, LX/FeC;->a:LX/FeD;

    iget-object v0, v0, LX/FeD;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, LX/CvY;

    iget-object v0, p0, LX/FeC;->a:LX/FeD;

    iget-object v0, v0, LX/FeD;->a:LX/Cxk;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v8

    iget-object v0, p1, LX/1Zf;->c:LX/1zt;

    .line 2265250
    iget v1, v0, LX/1zt;->e:I

    move v0, v1

    .line 2265251
    if-eqz v0, :cond_1

    sget-object v0, LX/8ch;->REACTED:LX/8ch;

    move-object v7, v0

    :goto_0
    iget-object v0, p0, LX/FeC;->a:LX/FeD;

    iget-object v0, v0, LX/FeD;->a:LX/Cxk;

    invoke-interface {v0, v5}, LX/CxP;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v9

    invoke-static {v5}, LX/CvY;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)LX/CvV;

    move-result-object v10

    iget-object v0, p0, LX/FeC;->a:LX/FeD;

    iget-object v0, v0, LX/FeD;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object v0, p0, LX/FeC;->a:LX/FeD;

    iget-object v0, v0, LX/FeD;->a:LX/Cxk;

    invoke-interface {v0}, LX/CxV;->t()Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-result-object v0

    iget-object v1, p0, LX/FeC;->a:LX/FeD;

    iget-object v1, v1, LX/FeD;->a:LX/Cxk;

    invoke-interface {v1, v5}, LX/CxP;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)I

    move-result v1

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p1, LX/1Zf;->c:LX/1zt;

    .line 2265252
    iget v4, v3, LX/1zt;->e:I

    move v3, v4

    .line 2265253
    if-eqz v3, :cond_2

    sget-object v3, LX/8ch;->REACTED:LX/8ch;

    :goto_1
    invoke-static {v5}, LX/8eM;->a(Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v4

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel;->m()Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/search/results/protocol/SearchResultsEdgeModels$SearchResultsEdgeModel$NodeModel;->ae()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILjava/lang/String;LX/8ch;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    move-object v0, v6

    move-object v1, v8

    move-object v2, v7

    move v3, v9

    move-object v4, v10

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2265254
    :cond_0
    return-void

    .line 2265255
    :cond_1
    sget-object v0, LX/8ch;->UNREACTED:LX/8ch;

    move-object v7, v0

    goto :goto_0

    :cond_2
    sget-object v3, LX/8ch;->UNREACTED:LX/8ch;

    goto :goto_1
.end method
