.class public final LX/H85;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/offers/views/OffersTabbedBar;


# direct methods
.method public constructor <init>(Lcom/facebook/offers/views/OffersTabbedBar;)V
    .locals 0

    .prologue
    .line 2432927
    iput-object p1, p0, LX/H85;->a:Lcom/facebook/offers/views/OffersTabbedBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x2

    const v2, 0x74d07cb4

    invoke-static {v1, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2432928
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v0

    .line 2432929
    :goto_0
    iget-object v3, p0, LX/H85;->a:Lcom/facebook/offers/views/OffersTabbedBar;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H86;

    invoke-virtual {v3, v0}, Lcom/facebook/offers/views/OffersTabbedBar;->setSelected(LX/H86;)V

    .line 2432930
    iget-object v0, p0, LX/H85;->a:Lcom/facebook/offers/views/OffersTabbedBar;

    iget-object v0, v0, Lcom/facebook/offers/views/OffersTabbedBar;->b:LX/H6M;

    if-eqz v0, :cond_0

    .line 2432931
    iget-object v0, p0, LX/H85;->a:Lcom/facebook/offers/views/OffersTabbedBar;

    iget-object v3, v0, Lcom/facebook/offers/views/OffersTabbedBar;->b:LX/H6M;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H86;

    invoke-interface {v3, v0, v1}, LX/H6M;->a(LX/H86;Z)V

    .line 2432932
    :cond_0
    const v0, 0x4d7f74dd    # 2.67865552E8f

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void

    .line 2432933
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0
.end method
