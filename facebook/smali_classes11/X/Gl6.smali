.class public LX/Gl6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Gkk;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/0kL;

.field private final c:LX/0SG;

.field public d:LX/GkH;

.field public e:I

.field private f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/Gkn;",
            ">;"
        }
    .end annotation
.end field

.field public g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/Gkn;",
            "Lcom/facebook/groups/groupsections/GroupsSectionInterface;",
            ">;"
        }
    .end annotation
.end field

.field private h:LX/GlL;

.field public i:LX/GlB;

.field private j:LX/GlM;

.field private k:LX/Gks;

.field public l:LX/0tX;

.field public m:LX/1Ck;

.field public n:LX/3mF;

.field private o:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DPT;",
            ">;"
        }
    .end annotation
.end field

.field private p:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DPY;",
            ">;"
        }
    .end annotation
.end field

.field private q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/settings/GroupSubscriptionController;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/DPL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2389983
    const-class v0, LX/Gl6;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Gl6;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/GlL;LX/GlB;LX/GlM;LX/Gks;LX/0tX;LX/1Ck;LX/0kL;LX/0SG;LX/3mF;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/GlL;",
            "LX/GlB;",
            "LX/GlM;",
            "LX/Gks;",
            "LX/0tX;",
            "LX/1Ck;",
            "LX/0kL;",
            "LX/0SG;",
            "LX/3mF;",
            "LX/0Ot",
            "<",
            "LX/DPT;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DPY;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/groups/settings/GroupSubscriptionController;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/DPL;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2389802
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2389803
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Gl6;->g:Ljava/util/HashMap;

    .line 2389804
    iput-object p1, p0, LX/Gl6;->h:LX/GlL;

    .line 2389805
    iput-object p2, p0, LX/Gl6;->i:LX/GlB;

    .line 2389806
    iput-object p3, p0, LX/Gl6;->j:LX/GlM;

    .line 2389807
    iput-object p4, p0, LX/Gl6;->k:LX/Gks;

    .line 2389808
    iput-object p5, p0, LX/Gl6;->l:LX/0tX;

    .line 2389809
    iput-object p6, p0, LX/Gl6;->m:LX/1Ck;

    .line 2389810
    iput-object p7, p0, LX/Gl6;->b:LX/0kL;

    .line 2389811
    iput-object p8, p0, LX/Gl6;->c:LX/0SG;

    .line 2389812
    iput-object p10, p0, LX/Gl6;->o:LX/0Ot;

    .line 2389813
    iput-object p11, p0, LX/Gl6;->p:LX/0Ot;

    .line 2389814
    iput-object p12, p0, LX/Gl6;->q:LX/0Ot;

    .line 2389815
    iput-object p13, p0, LX/Gl6;->r:LX/0Ot;

    .line 2389816
    iput-object p9, p0, LX/Gl6;->n:LX/3mF;

    .line 2389817
    return-void
.end method

.method public static a(LX/0QB;)LX/Gl6;
    .locals 1

    .prologue
    .line 2389982
    invoke-static {p0}, LX/Gl6;->b(LX/0QB;)LX/Gl6;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 4
    .param p2    # Ljava/lang/Integer;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2389951
    iget-object v0, p0, LX/Gl6;->h:LX/GlL;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Gl6;->h:LX/GlL;

    invoke-virtual {v0}, LX/Gkp;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2389952
    :cond_0
    :goto_0
    return-void

    .line 2389953
    :cond_1
    iget-object v0, p0, LX/Gl6;->h:LX/GlL;

    .line 2389954
    iget-object v1, v0, LX/Gkp;->c:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/Gkp;->g:Ljava/util/HashMap;

    if-eqz v1, :cond_2

    iget-object v1, v0, LX/Gkp;->g:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2389955
    :cond_2
    :goto_1
    invoke-static {p0}, LX/Gl6;->g(LX/Gl6;)V

    .line 2389956
    iget-object v0, p0, LX/Gl6;->h:LX/GlL;

    .line 2389957
    new-instance v3, Ljava/util/ArrayList;

    iget-object v1, v0, LX/Gkp;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2389958
    const/4 v1, 0x0

    move v2, v1

    :goto_2
    iget-object v1, v0, LX/Gkp;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_3

    .line 2389959
    iget-object v1, v0, LX/Gkp;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Gkq;

    .line 2389960
    iget-object p1, v1, LX/Gkq;->a:Ljava/lang/String;

    move-object v1, p1

    .line 2389961
    invoke-virtual {v3, v2, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 2389962
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_2

    .line 2389963
    :cond_3
    move-object v1, v3

    .line 2389964
    iget-object v0, p0, LX/Gl6;->p:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DPY;

    .line 2389965
    new-instance v2, LX/4KS;

    invoke-direct {v2}, LX/4KS;-><init>()V

    iget-object v3, v0, LX/DPY;->b:Ljava/lang/String;

    .line 2389966
    const-string p0, "actor_id"

    invoke-virtual {v2, p0, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2389967
    move-object v2, v2

    .line 2389968
    const-string v3, "group_ids"

    invoke-virtual {v2, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2389969
    move-object v2, v2

    .line 2389970
    new-instance v3, LX/DPU;

    invoke-direct {v3}, LX/DPU;-><init>()V

    move-object v3, v3

    .line 2389971
    const-string p0, "input"

    invoke-virtual {v3, p0, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v2

    check-cast v2, LX/DPU;

    invoke-static {v2}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v2

    .line 2389972
    iget-object v3, v0, LX/DPY;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2389973
    goto :goto_0

    .line 2389974
    :cond_4
    iget-object v1, v0, LX/Gkp;->g:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2389975
    iget-object v1, v0, LX/Gkp;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Gkq;

    .line 2389976
    if-eqz p2, :cond_5

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v3, v2, :cond_2

    .line 2389977
    :cond_5
    iget-object v3, v0, LX/Gkp;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 2389978
    if-eqz p2, :cond_6

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, v0, LX/Gkp;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v2, v3, :cond_7

    .line 2389979
    :cond_6
    iget-object v2, v0, LX/Gkp;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2389980
    :goto_3
    invoke-static {v0}, LX/Gkp;->l(LX/Gkp;)V

    goto/16 :goto_1

    .line 2389981
    :cond_7
    iget-object v2, v0, LX/Gkp;->c:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_3
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 2389949
    iget-object v0, p0, LX/Gl6;->d:LX/GkH;

    iget-object v1, p0, LX/Gl6;->g:Ljava/util/HashMap;

    invoke-direct {p0}, LX/Gl6;->e()I

    move-result v2

    invoke-interface {v0, v1, p1, v2}, LX/GkH;->a(Ljava/util/HashMap;ZI)V

    .line 2389950
    return-void
.end method

.method public static a(LX/Gl6;LX/Gkp;ILX/GkK;)Z
    .locals 4

    .prologue
    .line 2389945
    invoke-virtual {p1}, LX/Gkp;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2389946
    iget-object v0, p0, LX/Gl6;->m:LX/1Ck;

    invoke-virtual {p1}, LX/Gkp;->i()LX/Gkn;

    move-result-object v1

    invoke-virtual {v1}, LX/Gkn;->name()Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/Gl3;

    invoke-direct {v2, p0, p3, p1, p2}, LX/Gl3;-><init>(LX/Gl6;LX/GkK;LX/Gkp;I)V

    new-instance v3, LX/Gl4;

    invoke-direct {v3, p0, p1, p3}, LX/Gl4;-><init>(LX/Gl6;LX/Gkp;LX/GkK;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2389947
    const/4 v0, 0x1

    .line 2389948
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/Gl6;
    .locals 14

    .prologue
    .line 2389931
    new-instance v0, LX/Gl6;

    .line 2389932
    new-instance v2, LX/GlL;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v1

    check-cast v1, Landroid/content/res/Resources;

    invoke-direct {v2, v1}, LX/GlL;-><init>(Landroid/content/res/Resources;)V

    .line 2389933
    move-object v1, v2

    .line 2389934
    check-cast v1, LX/GlL;

    .line 2389935
    new-instance v3, LX/GlB;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-direct {v3, v2}, LX/GlB;-><init>(Landroid/content/res/Resources;)V

    .line 2389936
    move-object v2, v3

    .line 2389937
    check-cast v2, LX/GlB;

    .line 2389938
    new-instance v4, LX/GlM;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {v4, v3}, LX/GlM;-><init>(Landroid/content/res/Resources;)V

    .line 2389939
    move-object v3, v4

    .line 2389940
    check-cast v3, LX/GlM;

    .line 2389941
    new-instance v5, LX/Gks;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-direct {v5, v4}, LX/Gks;-><init>(Landroid/content/res/Resources;)V

    .line 2389942
    move-object v4, v5

    .line 2389943
    check-cast v4, LX/Gks;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v7

    check-cast v7, LX/0kL;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v8

    check-cast v8, LX/0SG;

    invoke-static {p0}, LX/3mF;->b(LX/0QB;)LX/3mF;

    move-result-object v9

    check-cast v9, LX/3mF;

    const/16 v10, 0x2453

    invoke-static {p0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x2454

    invoke-static {p0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x24a8

    invoke-static {p0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x2451

    invoke-static {p0, v13}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v0 .. v13}, LX/Gl6;-><init>(LX/GlL;LX/GlB;LX/GlM;LX/Gks;LX/0tX;LX/1Ck;LX/0kL;LX/0SG;LX/3mF;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2389944
    return-object v0
.end method

.method public static b(LX/Gkp;Lcom/facebook/graphql/executor/GraphQLResult;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Gkp;",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2389914
    if-eqz p1, :cond_0

    .line 2389915
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2389916
    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 2389917
    :goto_0
    return v0

    .line 2389918
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2389919
    check-cast v0, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel;

    invoke-virtual {v0}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel;->a()Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2389920
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2389921
    check-cast v0, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel;

    invoke-virtual {v0}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel;->a()Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel;->a()Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2389922
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2389923
    check-cast v0, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel;

    invoke-virtual {v0}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel;->a()Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel;->a()Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel;

    move-result-object v2

    .line 2389924
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2389925
    invoke-virtual {v2}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    :goto_1
    if-ge v1, v5, :cond_2

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;

    .line 2389926
    new-instance v6, LX/Gkq;

    invoke-direct {v6, v0}, LX/Gkq;-><init>(Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel$NodesModel;)V

    invoke-virtual {v3, v6}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2389927
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2389928
    :cond_2
    invoke-virtual {v2}, Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel$ActorModel$GroupsModel;->j()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {p0, v2, v1, v0}, LX/Gkp;->a(LX/0Px;LX/15i;I)V

    .line 2389929
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 2389930
    goto :goto_0
.end method

.method private d()V
    .locals 4

    .prologue
    .line 2389912
    iget-object v0, p0, LX/Gl6;->m:LX/1Ck;

    sget-object v1, LX/Gl5;->INITIAL_FETCH_TASK:LX/Gl5;

    new-instance v2, LX/Gkw;

    invoke-direct {v2, p0}, LX/Gkw;-><init>(LX/Gl6;)V

    new-instance v3, LX/Gkx;

    invoke-direct {v3, p0}, LX/Gkx;-><init>(LX/Gl6;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2389913
    return-void
.end method

.method private e()I
    .locals 4

    .prologue
    .line 2389906
    const/4 v0, 0x0

    .line 2389907
    iget-object v1, p0, LX/Gl6;->g:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkp;

    .line 2389908
    invoke-virtual {v0}, LX/Gkp;->f()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2389909
    invoke-virtual {v0}, LX/Gkp;->a()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 2389910
    goto :goto_0

    .line 2389911
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private f()Z
    .locals 3

    .prologue
    .line 2389902
    iget-object v0, p0, LX/Gl6;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkp;

    .line 2389903
    instance-of v2, v0, LX/Gkp;

    if-eqz v2, :cond_0

    invoke-virtual {v0}, LX/Gkp;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2389904
    const/4 v0, 0x1

    .line 2389905
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(LX/Gl6;)V
    .locals 1

    .prologue
    .line 2389984
    invoke-direct {p0}, LX/Gl6;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, LX/Gl6;->a(Z)V

    .line 2389985
    return-void

    .line 2389986
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2389897
    iget-object v0, p0, LX/Gl6;->m:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2389898
    iget-object v0, p0, LX/Gl6;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkp;

    .line 2389899
    instance-of v2, v0, LX/Gkp;

    if-eqz v2, :cond_0

    .line 2389900
    check-cast v0, LX/Gkp;

    invoke-virtual {v0}, LX/Gkp;->j()V

    goto :goto_0

    .line 2389901
    :cond_1
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 2389890
    iget-object v0, p0, LX/Gl6;->h:LX/GlL;

    invoke-virtual {v0}, LX/Gkp;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Gl6;->h:LX/GlL;

    invoke-virtual {v0}, LX/Gkp;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2389891
    :cond_0
    :goto_0
    return-void

    .line 2389892
    :cond_1
    iget-object v0, p0, LX/Gl6;->h:LX/GlL;

    invoke-virtual {v0, p1}, LX/Gkp;->a(I)LX/Gkq;

    move-result-object v0

    .line 2389893
    if-nez v0, :cond_2

    .line 2389894
    sget-object v0, LX/Gl6;->a:Ljava/lang/String;

    const-string v1, "Unable to find group model to reorder, favorite group does not exist."

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2389895
    :cond_2
    iget-object v1, v0, LX/Gkq;->a:Ljava/lang/String;

    move-object v0, v1

    .line 2389896
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, LX/Gl6;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method public final a(ILX/GkK;)V
    .locals 5

    .prologue
    .line 2389878
    invoke-direct {p0}, LX/Gl6;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2389879
    :cond_0
    :goto_0
    return-void

    .line 2389880
    :cond_1
    invoke-direct {p0}, LX/Gl6;->e()I

    move-result v0

    add-int v2, v0, p1

    .line 2389881
    iget-object v0, p0, LX/Gl6;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LX/Gl6;->f:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkn;

    .line 2389882
    iget-object v4, p0, LX/Gl6;->g:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkp;

    .line 2389883
    if-eqz v0, :cond_2

    instance-of v4, v0, LX/Gkp;

    if-eqz v4, :cond_2

    .line 2389884
    check-cast v0, LX/Gkp;

    .line 2389885
    invoke-virtual {v0}, LX/Gkp;->d()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2389886
    iget-boolean v4, v0, LX/Gkp;->d:Z

    move v4, v4

    .line 2389887
    if-nez v4, :cond_2

    .line 2389888
    invoke-direct {p0}, LX/Gl6;->e()I

    move-result v1

    sub-int v1, v2, v1

    invoke-static {p0, v0, v1, p2}, LX/Gl6;->a(LX/Gl6;LX/Gkp;ILX/GkK;)Z

    goto :goto_0

    .line 2389889
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public final a(LX/0Px;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "LX/Gkn;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2389867
    iput-object p1, p0, LX/Gl6;->f:LX/0Px;

    .line 2389868
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkn;

    .line 2389869
    sget-object v3, LX/Gkv;->a:[I

    invoke-virtual {v0}, LX/Gkn;->ordinal()I

    move-result v0

    aget v0, v3, v0

    packed-switch v0, :pswitch_data_0

    .line 2389870
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2389871
    :pswitch_0
    iget-object v0, p0, LX/Gl6;->g:Ljava/util/HashMap;

    sget-object v3, LX/Gkn;->FILTERED_GROUPS_SECTION:LX/Gkn;

    iget-object v4, p0, LX/Gl6;->i:LX/GlB;

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2389872
    :pswitch_1
    iget-object v0, p0, LX/Gl6;->g:Ljava/util/HashMap;

    sget-object v3, LX/Gkn;->RECENTLY_JOINED_SECTION:LX/Gkn;

    iget-object v4, p0, LX/Gl6;->k:LX/Gks;

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2389873
    :pswitch_2
    iget-object v0, p0, LX/Gl6;->g:Ljava/util/HashMap;

    sget-object v3, LX/Gkn;->FAVORITES_SECTION:LX/Gkn;

    iget-object v4, p0, LX/Gl6;->h:LX/GlL;

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2389874
    :pswitch_3
    iget-object v0, p0, LX/Gl6;->j:LX/GlM;

    const/4 v3, 0x1

    .line 2389875
    iput-boolean v3, v0, LX/Gkp;->d:Z

    .line 2389876
    iget-object v0, p0, LX/Gl6;->g:Ljava/util/HashMap;

    sget-object v3, LX/Gkn;->HIDDEN_GROUPS_SECTION:LX/Gkn;

    iget-object v4, p0, LX/Gl6;->j:LX/GlM;

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2389877
    :cond_0
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(LX/Gkm;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 2389858
    iget-object v0, p0, LX/Gl6;->i:LX/GlB;

    invoke-virtual {v0}, LX/Gkp;->j()V

    .line 2389859
    invoke-direct {p0, v5}, LX/Gl6;->a(Z)V

    .line 2389860
    iget-object v0, p0, LX/Gl6;->i:LX/GlB;

    .line 2389861
    iput-object p1, v0, LX/GlB;->f:LX/Gkm;

    .line 2389862
    iget-object v0, p0, LX/Gl6;->m:LX/1Ck;

    iget-object v1, p0, LX/Gl6;->i:LX/GlB;

    invoke-virtual {v1}, LX/Gkp;->i()LX/Gkn;

    move-result-object v1

    invoke-virtual {v1}, LX/Gkn;->name()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/Gl6;->l:LX/0tX;

    iget-object v3, p0, LX/Gl6;->i:LX/GlB;

    iget v4, p0, LX/Gl6;->e:I

    invoke-virtual {v3, v4}, LX/Gkp;->b(I)LX/GlC;

    move-result-object v3

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->d:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    .line 2389863
    iput-boolean v5, v3, LX/0zO;->p:Z

    .line 2389864
    move-object v3, v3

    .line 2389865
    const-wide/16 v4, 0x708

    invoke-virtual {v3, v4, v5}, LX/0zO;->a(J)LX/0zO;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    new-instance v3, LX/Gky;

    invoke-direct {v3, p0}, LX/Gky;-><init>(LX/Gl6;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2389866
    return-void
.end method

.method public final a(LX/Gkm;ILX/GkH;)V
    .locals 1

    .prologue
    .line 2389852
    iput-object p3, p0, LX/Gl6;->d:LX/GkH;

    .line 2389853
    iput p2, p0, LX/Gl6;->e:I

    .line 2389854
    iget-object v0, p0, LX/Gl6;->i:LX/GlB;

    .line 2389855
    iput-object p1, v0, LX/GlB;->f:LX/Gkm;

    .line 2389856
    invoke-direct {p0}, LX/Gl6;->d()V

    .line 2389857
    return-void
.end method

.method public final a(LX/Gkn;Z)V
    .locals 2

    .prologue
    .line 2389844
    iget-object v0, p0, LX/Gl6;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkp;

    .line 2389845
    if-eqz v0, :cond_0

    instance-of v1, v0, LX/Gkp;

    if-eqz v1, :cond_0

    .line 2389846
    check-cast v0, LX/Gkp;

    .line 2389847
    iget-boolean v1, v0, LX/Gkp;->d:Z

    move v1, v1

    .line 2389848
    if-eq v1, p2, :cond_0

    .line 2389849
    iput-boolean p2, v0, LX/Gkp;->d:Z

    .line 2389850
    invoke-static {p0}, LX/Gl6;->g(LX/Gl6;)V

    .line 2389851
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;LX/GkS;)V
    .locals 6

    .prologue
    .line 2389835
    iget-object v0, p0, LX/Gl6;->g:Ljava/util/HashMap;

    const/4 v1, 0x0

    iget-object v2, p0, LX/Gl6;->c:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    .line 2389836
    invoke-static {v0, p1}, LX/Gkr;->b(Ljava/util/HashMap;Ljava/lang/String;)LX/Gkq;

    move-result-object v4

    .line 2389837
    if-eqz v4, :cond_0

    .line 2389838
    iput v1, v4, LX/Gkq;->f:I

    .line 2389839
    iput-wide v2, v4, LX/Gkq;->d:J

    .line 2389840
    :cond_0
    move-object v0, v0

    .line 2389841
    iput-object v0, p0, LX/Gl6;->g:Ljava/util/HashMap;

    .line 2389842
    invoke-virtual {p2}, LX/GkS;->a()V

    .line 2389843
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;LX/GkS;)V
    .locals 2

    .prologue
    .line 2389831
    iget-object v0, p0, LX/Gl6;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DZH;

    new-instance v1, LX/Gkt;

    invoke-direct {v1, p0, p1, p3}, LX/Gkt;-><init>(LX/Gl6;Ljava/lang/String;LX/GkS;)V

    .line 2389832
    iput-object v1, v0, LX/DZH;->h:LX/DQQ;

    .line 2389833
    iget-object v0, p0, LX/Gl6;->q:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DZH;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;->OFF:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    invoke-virtual {v0, p1, p2, v1}, LX/DZH;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;)V

    .line 2389834
    return-void
.end method

.method public final a(Ljava/lang/String;ZZ)V
    .locals 4

    .prologue
    .line 2389822
    iget-object v1, p0, LX/Gl6;->g:Ljava/util/HashMap;

    if-eqz p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v1, p1, p2, v0}, LX/Gkr;->a(Ljava/util/HashMap;Ljava/lang/String;ZZ)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, LX/Gl6;->g:Ljava/util/HashMap;

    .line 2389823
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, LX/Gl5;->EDIT_FAVORITES:LX/Gl5;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2389824
    if-eqz p2, :cond_1

    .line 2389825
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/Gl6;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2389826
    iget-object v2, p0, LX/Gl6;->m:LX/1Ck;

    iget-object v0, p0, LX/Gl6;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DPT;

    invoke-virtual {v0, p1}, LX/DPT;->a(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v3, LX/Gkz;

    invoke-direct {v3, p0, p1, p2, p3}, LX/Gkz;-><init>(LX/Gl6;Ljava/lang/String;ZZ)V

    invoke-virtual {v2, v1, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2389827
    :goto_1
    return-void

    :cond_0
    move v0, p3

    .line 2389828
    goto :goto_0

    .line 2389829
    :cond_1
    invoke-static {p0}, LX/Gl6;->g(LX/Gl6;)V

    .line 2389830
    iget-object v2, p0, LX/Gl6;->m:LX/1Ck;

    iget-object v0, p0, LX/Gl6;->o:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/DPT;

    invoke-virtual {v0, p1}, LX/DPT;->b(Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v3, LX/Gl0;

    invoke-direct {v3, p0, p1, p2, p3}, LX/Gl0;-><init>(LX/Gl6;Ljava/lang/String;ZZ)V

    invoke-virtual {v2, v1, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    goto :goto_1
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2389818
    invoke-virtual {p0}, LX/Gl6;->a()V

    .line 2389819
    invoke-static {p0}, LX/Gl6;->g(LX/Gl6;)V

    .line 2389820
    invoke-direct {p0}, LX/Gl6;->d()V

    .line 2389821
    return-void
.end method
