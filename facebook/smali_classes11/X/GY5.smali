.class public final LX/GY5;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "LX/GXx;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;)V
    .locals 0

    .prologue
    .line 2364738
    iput-object p1, p0, LX/GY5;->a:Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2364739
    sget-object v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->g:Ljava/lang/String;

    const-string v1, "failed to load currency data"

    invoke-static {v0, v1, p1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2364740
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2364741
    check-cast p1, LX/GXx;

    .line 2364742
    iget-object v0, p0, LX/GY5;->a:Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;

    .line 2364743
    iget-boolean v1, p1, LX/GXx;->b:Z

    if-nez v1, :cond_3

    .line 2364744
    const/4 v1, 0x0

    .line 2364745
    :goto_0
    move v0, v1

    .line 2364746
    if-eqz v0, :cond_0

    .line 2364747
    :goto_1
    return-void

    .line 2364748
    :cond_0
    iget-object v0, p0, LX/GY5;->a:Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;

    .line 2364749
    iget-boolean v1, p1, LX/GXx;->c:Z

    iput-boolean v1, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->i:Z

    .line 2364750
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->m:Landroid/view/ViewGroup;

    .line 2364751
    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0300b5

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->n:Landroid/view/ViewGroup;

    .line 2364752
    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->n:Landroid/view/ViewGroup;

    const v3, 0x7f0d04e6

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;

    iput-object v2, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->p:Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;

    .line 2364753
    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->p:Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;

    new-instance v3, LX/GY8;

    invoke-direct {v3, v0}, LX/GY8;-><init>(Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/commerce/publishing/adapter/AdminShopCurrencySelectorView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2364754
    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->n:Landroid/view/ViewGroup;

    const v3, 0x7f0d04e4

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/text/BetterButton;

    iput-object v2, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->q:Lcom/facebook/widget/text/BetterButton;

    .line 2364755
    iget-object v3, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->q:Lcom/facebook/widget/text/BetterButton;

    iget-boolean v2, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->i:Z

    if-eqz v2, :cond_4

    const v2, 0x7f0829c5

    :goto_2
    invoke-virtual {v3, v2}, Lcom/facebook/widget/text/BetterButton;->setText(I)V

    .line 2364756
    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->q:Lcom/facebook/widget/text/BetterButton;

    new-instance v3, LX/GY9;

    invoke-direct {v3, v0}, LX/GY9;-><init>(Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;)V

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2364757
    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->n:Landroid/view/ViewGroup;

    const v3, 0x7f0d04e5

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, LX/GYA;

    invoke-direct {v3, v0}, LX/GYA;-><init>(Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2364758
    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->n:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2364759
    iget-object v1, p1, LX/GXx;->a:LX/0Px;

    .line 2364760
    new-instance v2, LX/6WS;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/6WS;-><init>(Landroid/content/Context;)V

    iput-object v2, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->s:LX/6WS;

    .line 2364761
    new-instance v4, LX/5OG;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v4, v2}, LX/5OG;-><init>(Landroid/content/Context;)V

    .line 2364762
    const/4 v2, 0x0

    move v3, v2

    :goto_3
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_1

    .line 2364763
    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/3rL;

    iget-object v2, v2, LX/3rL;->b:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    .line 2364764
    invoke-virtual {v4, v3, v3, v2}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    .line 2364765
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 2364766
    :cond_1
    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->s:LX/6WS;

    invoke-virtual {v2, v4}, LX/5OM;->a(LX/5OG;)V

    .line 2364767
    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->s:LX/6WS;

    new-instance v3, LX/GY6;

    invoke-direct {v3, v0, v1}, LX/GY6;-><init>(Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;LX/0Px;)V

    .line 2364768
    iput-object v3, v2, LX/5OM;->p:LX/5OO;

    .line 2364769
    iget-boolean v2, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->j:Z

    if-nez v2, :cond_2

    .line 2364770
    iget-object v2, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->d:LX/GXt;

    sget-object v3, LX/GXq;->IMP_CURRENCY_SELECTION:LX/GXq;

    iget-wide v4, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->h:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/GXt;->a(LX/GXq;Ljava/lang/String;)V

    .line 2364771
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->j:Z

    .line 2364772
    :cond_2
    goto/16 :goto_1

    .line 2364773
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 2364774
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/7j6;

    iget-wide v3, v0, Lcom/facebook/commerce/publishing/fragments/AdminAddShopFragment;->h:J

    invoke-virtual {v1, v3, v4}, LX/7j6;->a(J)V

    .line 2364775
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 2364776
    :cond_4
    const v2, 0x7f0829c4

    goto/16 :goto_2
.end method
