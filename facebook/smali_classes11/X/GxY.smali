.class public final enum LX/GxY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GxY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GxY;

.field public static final enum CONTENT_STATE_ERROR:LX/GxY;

.field public static final enum CONTENT_STATE_LOADING:LX/GxY;

.field public static final enum CONTENT_STATE_LOAD_COMPLETE:LX/GxY;

.field public static final enum CONTENT_STATE_WEBVIEW:LX/GxY;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2408181
    new-instance v0, LX/GxY;

    const-string v1, "CONTENT_STATE_WEBVIEW"

    invoke-direct {v0, v1, v2}, LX/GxY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GxY;->CONTENT_STATE_WEBVIEW:LX/GxY;

    .line 2408182
    new-instance v0, LX/GxY;

    const-string v1, "CONTENT_STATE_LOADING"

    invoke-direct {v0, v1, v3}, LX/GxY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GxY;->CONTENT_STATE_LOADING:LX/GxY;

    .line 2408183
    new-instance v0, LX/GxY;

    const-string v1, "CONTENT_STATE_ERROR"

    invoke-direct {v0, v1, v4}, LX/GxY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GxY;->CONTENT_STATE_ERROR:LX/GxY;

    .line 2408184
    new-instance v0, LX/GxY;

    const-string v1, "CONTENT_STATE_LOAD_COMPLETE"

    invoke-direct {v0, v1, v5}, LX/GxY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GxY;->CONTENT_STATE_LOAD_COMPLETE:LX/GxY;

    .line 2408185
    const/4 v0, 0x4

    new-array v0, v0, [LX/GxY;

    sget-object v1, LX/GxY;->CONTENT_STATE_WEBVIEW:LX/GxY;

    aput-object v1, v0, v2

    sget-object v1, LX/GxY;->CONTENT_STATE_LOADING:LX/GxY;

    aput-object v1, v0, v3

    sget-object v1, LX/GxY;->CONTENT_STATE_ERROR:LX/GxY;

    aput-object v1, v0, v4

    sget-object v1, LX/GxY;->CONTENT_STATE_LOAD_COMPLETE:LX/GxY;

    aput-object v1, v0, v5

    sput-object v0, LX/GxY;->$VALUES:[LX/GxY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2408180
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GxY;
    .locals 1

    .prologue
    .line 2408186
    const-class v0, LX/GxY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GxY;

    return-object v0
.end method

.method public static values()[LX/GxY;
    .locals 1

    .prologue
    .line 2408179
    sget-object v0, LX/GxY;->$VALUES:[LX/GxY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GxY;

    return-object v0
.end method
