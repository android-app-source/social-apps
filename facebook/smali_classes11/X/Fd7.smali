.class public LX/Fd7;
.super LX/1Cv;
.source ""


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/results/protocol/filters/FilterValue;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/content/Context;

.field private final c:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/results/protocol/filters/FilterValue;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2263173
    invoke-direct {p0}, LX/1Cv;-><init>()V

    .line 2263174
    iput-object p1, p0, LX/Fd7;->b:Landroid/content/Context;

    .line 2263175
    iput-object p2, p0, LX/Fd7;->a:Ljava/util/List;

    .line 2263176
    iget-object v0, p0, LX/Fd7;->b:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, LX/Fd7;->c:Landroid/view/LayoutInflater;

    .line 2263177
    return-void
.end method


# virtual methods
.method public final a(ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2263172
    if-nez p1, :cond_0

    iget-object v0, p0, LX/Fd7;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f0312b8

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Fd7;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f0312b2

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(ILjava/lang/Object;Landroid/view/View;ILandroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 2263162
    check-cast p2, Lcom/facebook/search/results/protocol/filters/FilterValue;

    .line 2263163
    check-cast p3, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2263164
    iget-object v0, p2, Lcom/facebook/search/results/protocol/filters/FilterValue;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2263165
    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2263166
    iget-object v0, p2, Lcom/facebook/search/results/protocol/filters/FilterValue;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2263167
    if-eqz v0, :cond_0

    .line 2263168
    iget-object v0, p2, Lcom/facebook/search/results/protocol/filters/FilterValue;->e:Ljava/lang/String;

    move-object v0, v0

    .line 2263169
    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2263170
    :goto_0
    return-void

    .line 2263171
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p3, v0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 2263161
    const/4 v0, 0x1

    return v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2263160
    iget-object v0, p0, LX/Fd7;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2263159
    iget-object v0, p0, LX/Fd7;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2263178
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2263152
    iget-object v0, p0, LX/Fd7;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/protocol/filters/FilterValue;

    .line 2263153
    iget-boolean p0, v0, Lcom/facebook/search/results/protocol/filters/FilterValue;->d:Z

    move v0, p0

    .line 2263154
    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2263148
    const/4 v0, 0x1

    .line 2263149
    instance-of v1, p2, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    if-nez v1, :cond_4

    instance-of v1, p2, Lcom/facebook/fbui/widget/contentview/ContentView;

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2263150
    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, LX/Fd7;->getItemViewType(I)I

    move-result v1

    if-eq v1, v0, :cond_1

    :cond_0
    instance-of v1, p2, Lcom/facebook/fbui/widget/contentview/ContentViewWithButton;

    if-eqz v1, :cond_3

    invoke-virtual {p0, p1}, LX/Fd7;->getItemViewType(I)I

    move-result v1

    if-nez v1, :cond_3

    :cond_1
    :goto_1
    move v0, v0

    .line 2263151
    if-eqz v0, :cond_2

    :goto_2
    invoke-super {p0, p1, p2, p3}, LX/1Cv;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0

    :cond_2
    const/4 p2, 0x0

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2263158
    const/4 v0, 0x2

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 2263155
    const/4 v0, 0x0

    return v0
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 2263156
    iget-object v0, p0, LX/Fd7;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 2263157
    const/4 v0, 0x1

    return v0
.end method
