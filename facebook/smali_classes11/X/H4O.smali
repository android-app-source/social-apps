.class public final LX/H4O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 2422720
    iput-object p1, p0, LX/H4O;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 2422721
    if-eqz p2, :cond_0

    .line 2422722
    iget-object v0, p0, LX/H4O;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->i:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 2422723
    iget-object v0, p0, LX/H4O;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->l:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    if-ne p1, v0, :cond_1

    .line 2422724
    iget-object v0, p0, LX/H4O;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->g:Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;->b()V

    .line 2422725
    iget-object v0, p0, LX/H4O;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->q:LX/H4a;

    sget-object v1, LX/H4Z;->LOCATIONS:LX/H4Z;

    invoke-virtual {v0, v1}, LX/H4a;->c(LX/H4Z;)V

    .line 2422726
    iget-object v0, p0, LX/H4O;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, LX/H4O;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->n:LX/H4G;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2422727
    iget-object v0, p0, LX/H4O;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, LX/H4O;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->y:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2422728
    iget-object v0, p0, LX/H4O;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->n:LX/H4G;

    const v1, 0x623daf4a

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2422729
    iget-object v0, p0, LX/H4O;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    invoke-static {v0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->q(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;)V

    .line 2422730
    :cond_0
    :goto_0
    return-void

    .line 2422731
    :cond_1
    iget-object v0, p0, LX/H4O;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->k:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    if-ne p1, v0, :cond_0

    .line 2422732
    iget-object v0, p0, LX/H4O;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->g:Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/typeahead/logging/NearbyPlacesTypeaheadLoggerData;->a()V

    .line 2422733
    iget-object v0, p0, LX/H4O;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->q:LX/H4a;

    sget-object v1, LX/H4Z;->PLACES_TOPICS:LX/H4Z;

    invoke-virtual {v0, v1}, LX/H4a;->c(LX/H4Z;)V

    .line 2422734
    iget-object v0, p0, LX/H4O;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, LX/H4O;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->o:LX/H4L;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2422735
    iget-object v0, p0, LX/H4O;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->j:Lcom/facebook/widget/listview/BetterListView;

    iget-object v1, p0, LX/H4O;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->x:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/listview/BetterListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2422736
    iget-object v0, p0, LX/H4O;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->o:LX/H4L;

    const v1, 0x625d2030

    invoke-static {v0, v1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2422737
    iget-object v0, p0, LX/H4O;->a:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    invoke-static {v0}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->p(Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;)V

    goto :goto_0
.end method
