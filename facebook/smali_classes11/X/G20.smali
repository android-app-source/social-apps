.class public LX/G20;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/G22;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/G20",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/G22;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2313252
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2313253
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/G20;->b:LX/0Zi;

    .line 2313254
    iput-object p1, p0, LX/G20;->a:LX/0Ot;

    .line 2313255
    return-void
.end method

.method public static a(LX/0QB;)LX/G20;
    .locals 4

    .prologue
    .line 2313256
    const-class v1, LX/G20;

    monitor-enter v1

    .line 2313257
    :try_start_0
    sget-object v0, LX/G20;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2313258
    sput-object v2, LX/G20;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2313259
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2313260
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2313261
    new-instance v3, LX/G20;

    const/16 p0, 0x36bd

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/G20;-><init>(LX/0Ot;)V

    .line 2313262
    move-object v0, v3

    .line 2313263
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2313264
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/G20;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2313265
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2313266
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2313267
    check-cast p2, LX/G1z;

    .line 2313268
    iget-object v0, p0, LX/G20;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G22;

    iget-object v1, p2, LX/G1z;->a:Lcom/facebook/timeline/protiles/model/ProtileModel;

    iget-object v2, p2, LX/G1z;->b:LX/1Pp;

    .line 2313269
    invoke-static {v1}, LX/G22;->a(Lcom/facebook/timeline/protiles/model/ProtileModel;)Z

    move-result v3

    invoke-static {v3}, LX/0PB;->checkArgument(Z)V

    .line 2313270
    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->p()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    move-result-object v3

    .line 2313271
    if-nez v3, :cond_0

    .line 2313272
    const/4 v4, 0x0

    .line 2313273
    :goto_0
    move-object v3, v4

    .line 2313274
    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->p()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;->d()Ljava/lang/String;

    move-result-object v4

    .line 2313275
    new-instance v5, LX/G21;

    invoke-direct {v5, v0, v4}, LX/G21;-><init>(LX/G22;Ljava/lang/String;)V

    move-object v4, v5

    .line 2313276
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v5, v6}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v6

    const p0, 0x7f0a00e7

    invoke-virtual {v6, p0}, LX/25Q;->i(I)LX/25Q;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/4 p0, 0x1

    invoke-interface {v6, p0}, LX/1Di;->o(I)LX/1Di;

    move-result-object v6

    const/4 p0, 0x3

    const/4 p2, 0x6

    invoke-interface {v6, p0, p2}, LX/1Di;->d(II)LX/1Di;

    move-result-object v6

    invoke-interface {v5, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    iget-object v6, v0, LX/G22;->a:LX/1Vm;

    invoke-virtual {v6, p1}, LX/1Vm;->c(LX/1De;)LX/C2N;

    move-result-object v6

    invoke-virtual {v6, v2}, LX/C2N;->a(LX/1Pp;)LX/C2N;

    move-result-object v6

    invoke-virtual {v6, v3}, LX/C2N;->a(Lcom/facebook/graphql/model/GraphQLStoryActionLink;)LX/C2N;

    move-result-object v3

    invoke-virtual {v1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->p()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel$IconModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel$IconModel;->a()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/C2N;->a(Landroid/net/Uri;)LX/C2N;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/C2N;->a(Landroid/view/View$OnClickListener;)LX/C2N;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/C2N;->b(Landroid/view/View$OnClickListener;)LX/C2N;

    move-result-object v3

    invoke-virtual {v3, v4}, LX/C2N;->c(Landroid/view/View$OnClickListener;)LX/C2N;

    move-result-object v3

    invoke-interface {v5, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2313277
    return-object v0

    .line 2313278
    :cond_0
    new-instance v4, LX/4Ys;

    invoke-direct {v4}, LX/4Ys;-><init>()V

    .line 2313279
    new-instance v5, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v6, -0x4b252c60

    invoke-direct {v5, v6}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    .line 2313280
    iput-object v5, v4, LX/4Ys;->bH:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2313281
    invoke-virtual {v3}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;->a()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel$DescriptionModel;

    move-result-object v5

    .line 2313282
    if-nez v5, :cond_1

    .line 2313283
    const/4 v6, 0x0

    .line 2313284
    :goto_1
    move-object v5, v6

    .line 2313285
    iput-object v5, v4, LX/4Ys;->y:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2313286
    invoke-virtual {v3}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel$IconModel;

    move-result-object v5

    .line 2313287
    if-nez v5, :cond_2

    .line 2313288
    const/4 v6, 0x0

    .line 2313289
    :goto_2
    move-object v5, v6

    .line 2313290
    iput-object v5, v4, LX/4Ys;->Z:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2313291
    invoke-virtual {v3}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 2313292
    iput-object v5, v4, LX/4Ys;->bz:Ljava/lang/String;

    .line 2313293
    invoke-virtual {v3}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel;->d()Ljava/lang/String;

    move-result-object v5

    .line 2313294
    iput-object v5, v4, LX/4Ys;->bD:Ljava/lang/String;

    .line 2313295
    invoke-virtual {v4}, LX/4Ys;->a()Lcom/facebook/graphql/model/GraphQLStoryActionLink;

    move-result-object v4

    goto/16 :goto_0

    .line 2313296
    :cond_1
    new-instance v6, LX/173;

    invoke-direct {v6}, LX/173;-><init>()V

    .line 2313297
    invoke-virtual {v5}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel$DescriptionModel;->a()Ljava/lang/String;

    move-result-object p0

    .line 2313298
    iput-object p0, v6, LX/173;->f:Ljava/lang/String;

    .line 2313299
    invoke-virtual {v6}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    goto :goto_1

    .line 2313300
    :cond_2
    new-instance v6, LX/2dc;

    invoke-direct {v6}, LX/2dc;-><init>()V

    .line 2313301
    invoke-virtual {v5}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileOverlayActionLinkFieldsModel$IconModel;->a()Ljava/lang/String;

    move-result-object p0

    .line 2313302
    iput-object p0, v6, LX/2dc;->h:Ljava/lang/String;

    .line 2313303
    invoke-virtual {v6}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    goto :goto_2
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2313304
    invoke-static {}, LX/1dS;->b()V

    .line 2313305
    const/4 v0, 0x0

    return-object v0
.end method
