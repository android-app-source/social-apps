.class public LX/FNR;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2232637
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/6jM;)D
    .locals 10

    .prologue
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    .line 2232633
    iget v0, p0, LX/6jM;->e:I

    int-to-double v0, v0

    mul-double/2addr v0, v6

    .line 2232634
    iget v2, p0, LX/6jM;->d:I

    int-to-double v2, v2

    mul-double/2addr v2, v8

    .line 2232635
    add-double/2addr v0, v2

    .line 2232636
    const-wide/high16 v4, 0x4030000000000000L    # 16.0

    mul-double/2addr v2, v4

    div-double v0, v2, v0

    invoke-static {v0, v1}, LX/6jP;->a(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    sub-double/2addr v0, v2

    mul-double/2addr v0, v8

    sub-double v0, v6, v0

    return-wide v0
.end method

.method public static a(LX/6jM;J)D
    .locals 9

    .prologue
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide v2, 0x3f847ae147ae147bL    # 0.01

    .line 2232620
    iget v4, p0, LX/6jM;->e:I

    iget v5, p0, LX/6jM;->d:I

    add-int/2addr v4, v5

    const/16 v5, 0xa

    if-gt v4, v5, :cond_1

    .line 2232621
    const-wide/16 v0, 0x0

    .line 2232622
    :cond_0
    :goto_0
    return-wide v0

    .line 2232623
    :cond_1
    iget v4, p0, LX/6jM;->d:I

    if-eqz v4, :cond_0

    .line 2232624
    iget v4, p0, LX/6jM;->d:I

    iget v5, p0, LX/6jM;->e:I

    if-lt v4, v5, :cond_2

    move-wide v0, v2

    .line 2232625
    goto :goto_0

    .line 2232626
    :cond_2
    invoke-static {p0}, LX/FNR;->a(LX/6jM;)D

    move-result-wide v4

    invoke-static {p0, p1, p2}, LX/FNR;->b(LX/6jM;J)D

    move-result-wide v6

    mul-double/2addr v4, v6

    .line 2232627
    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    goto :goto_0
.end method

.method private static b(LX/6jM;J)D
    .locals 11

    .prologue
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 2232628
    iget v0, p0, LX/6jM;->e:I

    iget v1, p0, LX/6jM;->d:I

    add-int/2addr v0, v1

    int-to-double v0, v0

    .line 2232629
    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    div-double/2addr v0, v2

    invoke-static {v0, v1}, LX/6jP;->a(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    sub-double/2addr v0, v2

    mul-double v6, v0, v8

    .line 2232630
    iget-wide v0, p0, LX/6jM;->b:D

    iget-wide v2, p0, LX/6jM;->c:J

    move-wide v4, p1

    invoke-static/range {v0 .. v5}, LX/6jP;->a(DJJ)D

    move-result-wide v0

    .line 2232631
    div-double/2addr v0, v8

    invoke-static {v0, v1}, LX/6jP;->a(D)D

    move-result-wide v0

    mul-double/2addr v0, v8

    .line 2232632
    mul-double/2addr v0, v6

    return-wide v0
.end method
