.class public LX/GmD;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "LX/GmD;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:LX/GmC;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2391649
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2391650
    iput-object p1, p0, LX/GmD;->a:Ljava/lang/String;

    .line 2391651
    iput-object p2, p0, LX/GmD;->b:Ljava/lang/String;

    .line 2391652
    sget-object v0, LX/GmC;->UNINVITED:LX/GmC;

    iput-object v0, p0, LX/GmD;->c:LX/GmC;

    .line 2391653
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2391654
    sget-object v0, LX/1I6;->b:LX/1I6;

    move-object v0, v0

    .line 2391655
    invoke-virtual {p0}, LX/GmD;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1I6;->a([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final compareTo(Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 2391656
    check-cast p1, LX/GmD;

    .line 2391657
    iget-object v0, p0, LX/GmD;->a:Ljava/lang/String;

    iget-object v1, p1, LX/GmD;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2391658
    if-ne p0, p1, :cond_1

    .line 2391659
    :cond_0
    :goto_0
    return v0

    .line 2391660
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2391661
    goto :goto_0

    .line 2391662
    :cond_3
    check-cast p1, LX/GmD;

    .line 2391663
    iget-object v2, p0, LX/GmD;->b:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v0, p0, LX/GmD;->b:Ljava/lang/String;

    iget-object v1, p1, LX/GmD;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_4
    iget-object v2, p1, LX/GmD;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2391664
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/GmD;->a:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/GmD;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
