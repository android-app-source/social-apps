.class public final LX/GvM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/GvO;

.field public final synthetic b:LX/GvP;


# direct methods
.method public constructor <init>(LX/GvP;LX/GvO;)V
    .locals 0

    .prologue
    .line 2405209
    iput-object p1, p0, LX/GvM;->b:LX/GvP;

    iput-object p2, p0, LX/GvM;->a:LX/GvO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x2

    const v2, -0xbf5a8ff

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2405210
    iget-object v0, p0, LX/GvM;->a:LX/GvO;

    iget-object v0, v0, LX/GvO;->o:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v3

    .line 2405211
    iget-object v0, p0, LX/GvM;->b:LX/GvP;

    iget-object v4, v0, LX/GvP;->c:LX/0if;

    sget-object v5, LX/0ig;->aQ:LX/0ih;

    if-eqz v3, :cond_0

    const-string v0, "permission_unchecked"

    :goto_0
    invoke-virtual {v4, v5, v0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2405212
    iget-object v0, p0, LX/GvM;->a:LX/GvO;

    iget-object v4, v0, LX/GvO;->o:Landroid/widget/CompoundButton;

    if-nez v3, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 2405213
    const v0, 0x66172729

    invoke-static {v0, v2}, LX/02F;->a(II)V

    return-void

    .line 2405214
    :cond_0
    const-string v0, "permission_checked"

    goto :goto_0

    .line 2405215
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
