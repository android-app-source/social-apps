.class public LX/FBZ;
.super LX/2g7;
.source ""


# instance fields
.field private final a:LX/0xB;


# direct methods
.method public constructor <init>(LX/0xB;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2209535
    invoke-direct {p0}, LX/2g7;-><init>()V

    .line 2209536
    iput-object p1, p0, LX/FBZ;->a:LX/0xB;

    .line 2209537
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;)Z
    .locals 5
    .param p1    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2209538
    iget-object v2, p2, Lcom/facebook/quickpromotion/model/QuickPromotionDefinition$ContextualFilter;->value:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 2209539
    iget-object v2, p0, LX/FBZ;->a:LX/0xB;

    sget-object v4, LX/12j;->INBOX:LX/12j;

    invoke-virtual {v2, v4}, LX/0xB;->a(LX/12j;)I

    move-result v2

    .line 2209540
    if-lez v2, :cond_0

    move v2, v0

    :goto_0
    if-ne v2, v3, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method
