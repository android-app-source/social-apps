.class public final LX/FHE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/6ed;

.field private final b:Ljava/lang/String;


# direct methods
.method private constructor <init>(LX/6ed;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2220107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2220108
    iput-object p1, p0, LX/FHE;->a:LX/6ed;

    .line 2220109
    iput-object p2, p0, LX/FHE;->b:Ljava/lang/String;

    .line 2220110
    return-void
.end method

.method public static a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/FHE;
    .locals 3

    .prologue
    .line 2220111
    new-instance v0, LX/FHE;

    invoke-static {p0}, LX/6ed;->b(Lcom/facebook/ui/media/attachments/MediaResource;)LX/6ed;

    move-result-object v1

    iget-object v2, p0, Lcom/facebook/ui/media/attachments/MediaResource;->p:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, LX/FHE;-><init>(LX/6ed;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2220112
    if-ne p0, p1, :cond_1

    .line 2220113
    :cond_0
    :goto_0
    return v0

    .line 2220114
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2220115
    goto :goto_0

    .line 2220116
    :cond_3
    check-cast p1, LX/FHE;

    .line 2220117
    iget-object v2, p0, LX/FHE;->a:LX/6ed;

    iget-object v3, p1, LX/FHE;->a:LX/6ed;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, LX/FHE;->b:Ljava/lang/String;

    iget-object v3, p1, LX/FHE;->b:Ljava/lang/String;

    invoke-static {v2, v3}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 2220118
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, LX/FHE;->a:LX/6ed;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, LX/FHE;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, LX/0kk;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
