.class public final LX/HAn;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<*>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;)V
    .locals 0

    .prologue
    .line 2435931
    iput-object p1, p0, LX/HAn;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2435932
    iget-object v0, p0, LX/HAn;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->d:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2435933
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2435934
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2435935
    if-eqz p1, :cond_0

    .line 2435936
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2435937
    if-nez v0, :cond_1

    .line 2435938
    :cond_0
    :goto_0
    return-void

    .line 2435939
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2435940
    instance-of v0, v0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithViewerLocationModel;

    if-eqz v0, :cond_2

    .line 2435941
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2435942
    check-cast v0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithViewerLocationModel;

    .line 2435943
    invoke-virtual {v0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithViewerLocationModel;->a()Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithViewerLocationModel;->a()Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2435944
    iget-object v1, p0, LX/HAn;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->p:LX/HAj;

    invoke-virtual {v1}, LX/HAj;->clear()V

    .line 2435945
    iget-object v1, p0, LX/HAn;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithViewerLocationModel;->a()Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;->a()LX/0Px;

    move-result-object v0

    .line 2435946
    iput-object v0, v1, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->n:LX/0Px;

    .line 2435947
    iget-object v0, p0, LX/HAn;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->p:LX/HAj;

    iget-object v1, p0, LX/HAn;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->n:LX/0Px;

    invoke-virtual {v0, v1}, LX/HAj;->addAll(Ljava/util/Collection;)V

    goto :goto_0

    .line 2435948
    :cond_2
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2435949
    instance-of v0, v0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithoutViewerLocationModel;

    if-eqz v0, :cond_3

    .line 2435950
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2435951
    check-cast v0, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithoutViewerLocationModel;

    .line 2435952
    invoke-virtual {v0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithoutViewerLocationModel;->a()Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithoutViewerLocationModel;->a()Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2435953
    iget-object v1, p0, LX/HAn;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->p:LX/HAj;

    invoke-virtual {v1}, LX/HAj;->clear()V

    .line 2435954
    iget-object v1, p0, LX/HAn;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    invoke-virtual {v0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationQueryWithoutViewerLocationModel;->a()Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/common/childlocations/ChildLocationCardGraphQLModels$ChildLocationConnectionFieldsModel;->a()LX/0Px;

    move-result-object v0

    .line 2435955
    iput-object v0, v1, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->n:LX/0Px;

    .line 2435956
    iget-object v0, p0, LX/HAn;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->p:LX/HAj;

    iget-object v1, p0, LX/HAn;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-object v1, v1, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->n:LX/0Px;

    invoke-virtual {v0, v1}, LX/HAj;->addAll(Ljava/util/Collection;)V

    goto :goto_0

    .line 2435957
    :cond_3
    iget-object v0, p0, LX/HAn;->a:Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/childlocations/PageChildLocationsListFragment;->d:LX/03V;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Unknown returned location model"

    invoke-virtual {v0, v1, v2}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
