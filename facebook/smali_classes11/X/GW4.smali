.class public LX/GW4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0aG;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0SI;

.field public final c:LX/0tX;


# direct methods
.method public constructor <init>(LX/0Or;LX/0SI;LX/0tX;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/0aG;",
            ">;",
            "LX/0SI;",
            "LX/0tX;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2360348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2360349
    iput-object p1, p0, LX/GW4;->a:LX/0Or;

    .line 2360350
    iput-object p2, p0, LX/GW4;->b:LX/0SI;

    .line 2360351
    iput-object p3, p0, LX/GW4;->c:LX/0tX;

    .line 2360352
    return-void
.end method

.method public static a(LX/0QB;)LX/GW4;
    .locals 4

    .prologue
    .line 2360353
    new-instance v2, LX/GW4;

    const/16 v0, 0x542

    invoke-static {p0, v0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    invoke-static {p0}, LX/0WG;->b(LX/0QB;)LX/0SI;

    move-result-object v0

    check-cast v0, LX/0SI;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-direct {v2, v3, v0, v1}, LX/GW4;-><init>(LX/0Or;LX/0SI;LX/0tX;)V

    .line 2360354
    move-object v0, v2

    .line 2360355
    return-object v0
.end method
