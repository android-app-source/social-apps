.class public LX/G2C;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1DZ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "LX/1DZ",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2313700
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2313701
    return-void
.end method

.method public static a(LX/0QB;)LX/G2C;
    .locals 1

    .prologue
    .line 2313697
    new-instance v0, LX/G2C;

    invoke-direct {v0}, LX/G2C;-><init>()V

    .line 2313698
    move-object v0, v0

    .line 2313699
    return-object v0
.end method

.method private static b(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2313685
    instance-of v0, p0, Lcom/facebook/timeline/protiles/model/ProtileModel;

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/facebook/timeline/protiles/model/ProtileModel;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;)Z"
        }
    .end annotation

    .prologue
    .line 2313690
    invoke-static {p1, p2}, LX/G2C;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2313691
    check-cast p1, Lcom/facebook/timeline/protiles/model/ProtileModel;

    check-cast p2, Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 2313692
    if-eq p1, p2, :cond_0

    .line 2313693
    iget-object v0, p1, Lcom/facebook/timeline/protiles/model/ProtileModel;->a:Ljava/lang/String;

    move-object v0, v0

    .line 2313694
    iget-object p0, p2, Lcom/facebook/timeline/protiles/model/ProtileModel;->a:Ljava/lang/String;

    move-object p0, p0

    .line 2313695
    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2313696
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;J)Z
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;TT;J)Z"
        }
    .end annotation

    .prologue
    .line 2313686
    invoke-static {p1, p2}, LX/G2C;->b(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2313687
    check-cast p1, Lcom/facebook/timeline/protiles/model/ProtileModel;

    .line 2313688
    invoke-virtual {p1}, Lcom/facebook/graphql/model/BaseFeedUnit;->F_()J

    move-result-wide v1

    cmp-long v1, v1, p3

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    move v0, v1

    .line 2313689
    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
