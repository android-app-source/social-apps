.class public LX/FKG;
.super Ljava/lang/Exception;
.source ""


# instance fields
.field public final failedMessage:Lcom/facebook/messaging/model/messages/Message;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 1

    .prologue
    .line 2224892
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 2224893
    invoke-static {p2}, LX/FKG;->a(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    iput-object v0, p0, LX/FKG;->failedMessage:Lcom/facebook/messaging/model/messages/Message;

    .line 2224894
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 1

    .prologue
    .line 2224895
    invoke-direct {p0, p1, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2224896
    invoke-static {p3}, LX/FKG;->a(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    iput-object v0, p0, LX/FKG;->failedMessage:Lcom/facebook/messaging/model/messages/Message;

    .line 2224897
    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 1

    .prologue
    .line 2224898
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    .line 2224899
    invoke-static {p2}, LX/FKG;->a(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    iput-object v0, p0, LX/FKG;->failedMessage:Lcom/facebook/messaging/model/messages/Message;

    .line 2224900
    return-void
.end method

.method private static a(Lcom/facebook/messaging/model/messages/Message;)Lcom/facebook/messaging/model/messages/Message;
    .locals 2

    .prologue
    .line 2224901
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2224902
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->l:LX/2uW;

    sget-object v1, LX/2uW;->FAILED_SEND:LX/2uW;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2224903
    return-object p0

    .line 2224904
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final b()LX/6fP;
    .locals 1

    .prologue
    .line 2224905
    iget-object v0, p0, LX/FKG;->failedMessage:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->w:Lcom/facebook/messaging/model/send/SendError;

    iget-object v0, v0, Lcom/facebook/messaging/model/send/SendError;->b:LX/6fP;

    return-object v0
.end method
