.class public LX/Fzo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/Fzo;


# instance fields
.field public a:Ljava/util/concurrent/ExecutorService;

.field public b:LX/Fzy;


# direct methods
.method public constructor <init>(LX/Fzy;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2308258
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2308259
    iput-object p2, p0, LX/Fzo;->a:Ljava/util/concurrent/ExecutorService;

    .line 2308260
    iput-object p1, p0, LX/Fzo;->b:LX/Fzy;

    .line 2308261
    return-void
.end method

.method public static a(LX/0QB;)LX/Fzo;
    .locals 5

    .prologue
    .line 2308262
    sget-object v0, LX/Fzo;->c:LX/Fzo;

    if-nez v0, :cond_1

    .line 2308263
    const-class v1, LX/Fzo;

    monitor-enter v1

    .line 2308264
    :try_start_0
    sget-object v0, LX/Fzo;->c:LX/Fzo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2308265
    if-eqz v2, :cond_0

    .line 2308266
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2308267
    new-instance p0, LX/Fzo;

    invoke-static {v0}, LX/Fzy;->a(LX/0QB;)LX/Fzy;

    move-result-object v3

    check-cast v3, LX/Fzy;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v3, v4}, LX/Fzo;-><init>(LX/Fzy;Ljava/util/concurrent/ExecutorService;)V

    .line 2308268
    move-object v0, p0

    .line 2308269
    sput-object v0, LX/Fzo;->c:LX/Fzo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2308270
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2308271
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2308272
    :cond_1
    sget-object v0, LX/Fzo;->c:LX/Fzo;

    return-object v0

    .line 2308273
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2308274
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
