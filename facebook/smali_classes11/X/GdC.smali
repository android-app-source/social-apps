.class public LX/GdC;
.super LX/0RV;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0RV",
        "<",
        "Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2373305
    invoke-direct {p0}, LX/0RV;-><init>()V

    return-void
.end method

.method public static b(LX/0QB;)Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;
    .locals 5

    .prologue
    .line 2373306
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0sJ;->a(LX/0QB;)LX/0sK;

    move-result-object v1

    check-cast v1, LX/0sK;

    invoke-static {p0}, LX/GdA;->b(LX/0QB;)LX/Gd8;

    move-result-object v2

    check-cast v2, LX/Gd8;

    invoke-static {p0}, LX/GdD;->a(LX/0QB;)LX/Gd5;

    move-result-object v3

    check-cast v3, LX/Gd5;

    invoke-static {p0}, Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;->b(LX/0QB;)Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;

    move-result-object v4

    check-cast v4, Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;

    invoke-static {v0, v1, v2, v3, v4}, LX/27Z;->a(Landroid/content/Context;LX/0sK;LX/Gd8;LX/Gd5;Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;)Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final get()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2373307
    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0sJ;->a(LX/0QB;)LX/0sK;

    move-result-object v1

    check-cast v1, LX/0sK;

    invoke-static {p0}, LX/GdA;->b(LX/0QB;)LX/Gd8;

    move-result-object v2

    check-cast v2, LX/Gd8;

    invoke-static {p0}, LX/GdD;->a(LX/0QB;)LX/Gd5;

    move-result-object v3

    check-cast v3, LX/Gd5;

    invoke-static {p0}, Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;->b(LX/0QB;)Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;

    move-result-object v4

    check-cast v4, Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;

    invoke-static {v0, v1, v2, v3, v4}, LX/27Z;->a(Landroid/content/Context;LX/0sK;LX/Gd8;LX/Gd5;Lcom/facebook/fbreact/autoupdater/fbhttp/UpdaterFbHttpRequests;)Lcom/facebook/fbreact/autoupdater/OverTheAirUpdater;

    move-result-object v0

    return-object v0
.end method
