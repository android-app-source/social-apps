.class public LX/G5r;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Up;


# instance fields
.field private a:LX/00u;


# direct methods
.method public constructor <init>(LX/00u;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2319429
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2319430
    iput-object p1, p0, LX/G5r;->a:LX/00u;

    .line 2319431
    return-void
.end method


# virtual methods
.method public final init()V
    .locals 14

    .prologue
    .line 2319432
    :try_start_0
    iget-object v0, p0, LX/G5r;->a:LX/00u;

    const/4 v3, 0x1

    const/4 v4, 0x0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2319433
    :try_start_1
    invoke-static {v0}, LX/00u;->b(LX/00u;)Ljava/util/List;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    move-result-object v6

    .line 2319434
    const-string v1, "(.*)_(.*)(-dex|\\.zip)"

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v7

    .line 2319435
    new-instance v1, Ljava/io/File;

    iget-object v2, v0, LX/00u;->b:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2319436
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v8

    .line 2319437
    if-nez v8, :cond_1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 2319438
    :cond_0
    :goto_0
    return-void

    .line 2319439
    :catch_0
    move-exception v0

    .line 2319440
    const-string v1, "VoltronModuleCleanUpHandler"

    const-string v2, "Unable to cleanup old voltron modules"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, LX/01m;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 2319441
    :cond_1
    array-length v9, v8

    move v5, v4

    :goto_1
    if-ge v5, v9, :cond_0

    aget-object v10, v8, v5

    .line 2319442
    invoke-virtual {v7, v10}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 2319443
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v2

    const/4 v11, 0x3

    if-ne v2, v11, :cond_2

    .line 2319444
    invoke-virtual {v1, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v11

    .line 2319445
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v12

    .line 2319446
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    move v2, v3

    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Kk;

    .line 2319447
    iget-object p0, v1, LX/0Kk;->b:Ljava/lang/String;

    invoke-virtual {v11, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_5

    iget-object v1, v1, LX/0Kk;->c:Ljava/lang/String;

    invoke-virtual {v12, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v4

    :goto_3
    move v2, v1

    .line 2319448
    goto :goto_2

    :cond_2
    move v2, v4

    .line 2319449
    :cond_3
    if-eqz v2, :cond_4

    .line 2319450
    new-instance v1, Ljava/io/File;

    iget-object v2, v0, LX/00u;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v10}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0, v1}, LX/00u;->a(LX/00u;Ljava/io/File;)V

    .line 2319451
    :cond_4
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    .line 2319452
    :catch_1
    goto :goto_0

    :cond_5
    move v1, v2

    goto :goto_3
.end method
