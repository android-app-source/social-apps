.class public LX/FKe;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;",
        "Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;",
        ">;"
    }
.end annotation


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6hN;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0sO;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2225370
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 2225371
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2225372
    iput-object v0, p0, LX/FKe;->b:LX/0Ot;

    .line 2225373
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2225374
    iput-object v0, p0, LX/FKe;->c:LX/0Ot;

    .line 2225375
    return-void
.end method

.method public static a(LX/0QB;)LX/FKe;
    .locals 3

    .prologue
    .line 2225376
    new-instance v1, LX/FKe;

    invoke-static {p0}, LX/0sO;->a(LX/0QB;)LX/0sO;

    move-result-object v0

    check-cast v0, LX/0sO;

    invoke-direct {v1, v0}, LX/FKe;-><init>(LX/0sO;)V

    .line 2225377
    const/16 v0, 0x2a01

    invoke-static {p0, v0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v0

    const/16 v2, 0x28e3

    invoke-static {p0, v2}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v2

    .line 2225378
    iput-object v0, v1, LX/FKe;->b:LX/0Ot;

    iput-object v2, v1, LX/FKe;->c:LX/0Ot;

    .line 2225379
    move-object v0, v1

    .line 2225380
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2225381
    check-cast p1, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;

    .line 2225382
    const-class v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreMessagesQueryModel;

    invoke-virtual {p3, v0}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreMessagesQueryModel;

    .line 2225383
    iget-object v1, p0, LX/FKe;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    .line 2225384
    iget-object v2, p1, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v2, v2

    .line 2225385
    invoke-virtual {v1, v0, v2}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreMessagesQueryModel;Lcom/facebook/messaging/model/threadkey/ThreadKey;)Lcom/facebook/messaging/service/model/FetchMoreMessagesResult;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2225386
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 6

    .prologue
    .line 2225387
    check-cast p1, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;

    .line 2225388
    invoke-static {}, LX/5ZC;->f()LX/5Z4;

    move-result-object v0

    const-string v1, "thread_id"

    .line 2225389
    iget-object v2, p1, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-object v2, v2

    .line 2225390
    invoke-virtual {v2}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "before_time_ms"

    .line 2225391
    iget-wide v4, p1, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->c:J

    move-wide v2, v4

    .line 2225392
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "msg_count"

    .line 2225393
    iget v2, p1, Lcom/facebook/messaging/service/model/FetchMoreMessagesParams;->d:I

    move v2, v2

    .line 2225394
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "full_screen_height"

    iget-object v0, p0, LX/FKe;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6hN;

    invoke-virtual {v0}, LX/6hN;->e()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "full_screen_width"

    iget-object v0, p0, LX/FKe;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6hN;

    invoke-virtual {v0}, LX/6hN;->d()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    .line 2225395
    iget-object v1, p0, LX/FKe;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6hN;

    invoke-virtual {v1}, LX/6hN;->h()I

    move-result v2

    .line 2225396
    iget-object v1, p0, LX/FKe;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6hN;

    invoke-virtual {v1}, LX/6hN;->g()I

    move-result v3

    .line 2225397
    iget-object v1, p0, LX/FKe;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6hN;

    invoke-virtual {v1}, LX/6hN;->f()I

    move-result v1

    .line 2225398
    const-string v4, "small_preview_width"

    mul-int/lit8 v5, v2, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "small_preview_height"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v4, "medium_preview_width"

    mul-int/lit8 v5, v3, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v4, "medium_preview_height"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "large_preview_width"

    mul-int/lit8 v4, v1, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "large_preview_height"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2225399
    return-object v0
.end method
