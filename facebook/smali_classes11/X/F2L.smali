.class public LX/F2L;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private a:LX/0kL;

.field private b:Ljava/lang/String;

.field private c:Ljava/util/concurrent/ExecutorService;

.field private final d:LX/0tX;

.field public e:Landroid/content/res/Resources;

.field private f:LX/F28;

.field public g:LX/1My;

.field public h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

.field public i:LX/F2f;

.field public j:LX/F39;

.field public final k:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0kL;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;LX/0tX;Landroid/content/res/Resources;LX/1My;LX/F28;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p3    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2193029
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2193030
    new-instance v0, LX/F2C;

    invoke-direct {v0, p0}, LX/F2C;-><init>(LX/F2L;)V

    iput-object v0, p0, LX/F2L;->k:LX/0TF;

    .line 2193031
    iput-object p1, p0, LX/F2L;->a:LX/0kL;

    .line 2193032
    iput-object p2, p0, LX/F2L;->c:Ljava/util/concurrent/ExecutorService;

    .line 2193033
    iput-object p3, p0, LX/F2L;->b:Ljava/lang/String;

    .line 2193034
    iput-object p4, p0, LX/F2L;->d:LX/0tX;

    .line 2193035
    iput-object p5, p0, LX/F2L;->e:Landroid/content/res/Resources;

    .line 2193036
    iput-object p6, p0, LX/F2L;->g:LX/1My;

    .line 2193037
    iput-object p7, p0, LX/F2L;->f:LX/F28;

    .line 2193038
    return-void
.end method

.method public static a(LX/F2L;Ljava/lang/String;LX/4Fk;LX/0TF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/4Fk;",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2193024
    invoke-virtual {p2, p1}, LX/4Fk;->b(Ljava/lang/String;)LX/4Fk;

    move-result-object v0

    iget-object v1, p0, LX/F2L;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/4Fk;->a(Ljava/lang/String;)LX/4Fk;

    move-result-object v0

    const-string v1, "treehouse_group_info"

    invoke-virtual {v0, v1}, LX/4Fk;->i(Ljava/lang/String;)LX/4Fk;

    .line 2193025
    invoke-static {}, LX/F3d;->a()LX/F3c;

    move-result-object v0

    const-string v1, "input"

    invoke-virtual {v0, v1, p2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/F3c;

    invoke-static {v0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v0

    .line 2193026
    iget-object v1, p0, LX/F2L;->d:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2193027
    iget-object v1, p0, LX/F2L;->c:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, p3, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2193028
    return-void
.end method

.method public static a$redex0(LX/F2L;I)V
    .locals 3

    .prologue
    .line 2193019
    iget-object v0, p0, LX/F2L;->a:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/F2L;->e:Landroid/content/res/Resources;

    invoke-virtual {v2, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    const/16 v2, 0x11

    .line 2193020
    iput v2, v1, LX/27k;->b:I

    .line 2193021
    move-object v1, v1

    .line 2193022
    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2193023
    return-void
.end method

.method public static a$redex0(LX/F2L;Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;)V
    .locals 1

    .prologue
    .line 2193012
    iget-object v0, p0, LX/F2L;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    if-eqz v0, :cond_0

    .line 2193013
    iget-object v0, p0, LX/F2L;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-static {v0}, LX/F3s;->a(Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;)LX/F3s;

    move-result-object v0

    .line 2193014
    iput-object p1, v0, LX/F3s;->l:Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;

    .line 2193015
    move-object v0, v0

    .line 2193016
    invoke-virtual {v0}, LX/F3s;->a()Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    move-result-object v0

    iput-object v0, p0, LX/F2L;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    .line 2193017
    invoke-static {p0}, LX/F2L;->b(LX/F2L;)V

    .line 2193018
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/F2L;Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;)V
    .locals 1

    .prologue
    .line 2193005
    iget-object v0, p0, LX/F2L;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    if-eqz v0, :cond_0

    .line 2193006
    iget-object v0, p0, LX/F2L;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-static {v0}, LX/F3s;->a(Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;)LX/F3s;

    move-result-object v0

    .line 2193007
    iput-object p1, v0, LX/F3s;->u:Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;

    .line 2193008
    move-object v0, v0

    .line 2193009
    invoke-virtual {v0}, LX/F3s;->a()Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    move-result-object v0

    iput-object v0, p0, LX/F2L;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    .line 2193010
    invoke-static {p0}, LX/F2L;->b(LX/F2L;)V

    .line 2193011
    :cond_0
    return-void
.end method

.method public static a$redex0(LX/F2L;Z)V
    .locals 1

    .prologue
    .line 2192958
    iget-object v0, p0, LX/F2L;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    if-eqz v0, :cond_0

    .line 2192959
    iget-object v0, p0, LX/F2L;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-static {v0}, LX/F3s;->a(Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;)LX/F3s;

    move-result-object v0

    .line 2192960
    iput-boolean p1, v0, LX/F3s;->x:Z

    .line 2192961
    move-object v0, v0

    .line 2192962
    invoke-virtual {v0}, LX/F3s;->a()Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    move-result-object v0

    iput-object v0, p0, LX/F2L;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    .line 2192963
    invoke-static {p0}, LX/F2L;->b(LX/F2L;)V

    .line 2192964
    :cond_0
    return-void
.end method

.method public static b(LX/0QB;)LX/F2L;
    .locals 8

    .prologue
    .line 2192999
    new-instance v0, LX/F2L;

    invoke-static {p0}, LX/0kL;->b(LX/0QB;)LX/0kL;

    move-result-object v1

    check-cast v1, LX/0kL;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0dG;->b(LX/0QB;)Ljava/lang/String;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {p0}, LX/1My;->b(LX/0QB;)LX/1My;

    move-result-object v6

    check-cast v6, LX/1My;

    .line 2193000
    new-instance v7, LX/F28;

    invoke-direct {v7}, LX/F28;-><init>()V

    .line 2193001
    move-object v7, v7

    .line 2193002
    move-object v7, v7

    .line 2193003
    check-cast v7, LX/F28;

    invoke-direct/range {v0 .. v7}, LX/F2L;-><init>(LX/0kL;Ljava/util/concurrent/ExecutorService;Ljava/lang/String;LX/0tX;Landroid/content/res/Resources;LX/1My;LX/F28;)V

    .line 2193004
    return-object v0
.end method

.method public static b(LX/F2L;)V
    .locals 2

    .prologue
    .line 2192995
    iget-object v0, p0, LX/F2L;->i:LX/F2f;

    if-eqz v0, :cond_0

    .line 2192996
    iget-object v0, p0, LX/F2L;->i:LX/F2f;

    iget-object v1, p0, LX/F2L;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    .line 2192997
    iget-object p0, v0, LX/F2f;->a:LX/F2r;

    invoke-static {p0, v1}, LX/F2r;->b(LX/F2r;Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;)V

    .line 2192998
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2192993
    iget-object v0, p0, LX/F2L;->g:LX/1My;

    invoke-virtual {v0}, LX/1My;->a()V

    .line 2192994
    return-void
.end method

.method public final a(Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;LX/F2f;)V
    .locals 0
    .param p2    # LX/F2f;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2192990
    iput-object p1, p0, LX/F2L;->h:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    .line 2192991
    iput-object p2, p0, LX/F2L;->i:LX/F2f;

    .line 2192992
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;)V
    .locals 3

    .prologue
    .line 2192977
    invoke-static {p0, p2}, LX/F2L;->a$redex0(LX/F2L;Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;)V

    .line 2192978
    new-instance v0, LX/4Fk;

    invoke-direct {v0}, LX/4Fk;-><init>()V

    .line 2192979
    sget-object v1, LX/F2K;->b:[I

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2192980
    const/4 v1, 0x0

    :goto_0
    move-object v1, v1

    .line 2192981
    const-string v2, "join_approval_setting"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2192982
    move-object v0, v0

    .line 2192983
    new-instance v1, LX/F2H;

    invoke-direct {v1, p0, p3}, LX/F2H;-><init>(LX/F2L;Lcom/facebook/graphql/enums/GraphQLGroupJoinApprovalSetting;)V

    .line 2192984
    invoke-static {p0, p1, v0, v1}, LX/F2L;->a(LX/F2L;Ljava/lang/String;LX/4Fk;LX/0TF;)V

    .line 2192985
    return-void

    .line 2192986
    :pswitch_0
    const-string v1, "ADMIN_ONLY"

    goto :goto_0

    .line 2192987
    :pswitch_1
    const-string v1, "ADMIN_ONLY_ADD"

    goto :goto_0

    .line 2192988
    :pswitch_2
    const-string v1, "ANYONE"

    goto :goto_0

    .line 2192989
    :pswitch_3
    const-string v1, "NONE"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;)V
    .locals 3

    .prologue
    .line 2192965
    invoke-static {p0, p2}, LX/F2L;->a$redex0(LX/F2L;Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;)V

    .line 2192966
    new-instance v0, LX/4Fk;

    invoke-direct {v0}, LX/4Fk;-><init>()V

    .line 2192967
    sget-object v1, LX/F2K;->c:[I

    invoke-virtual {p2}, Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2192968
    const/4 v1, 0x0

    :goto_0
    move-object v1, v1

    .line 2192969
    const-string v2, "post_permission_setting"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2192970
    move-object v0, v0

    .line 2192971
    new-instance v1, LX/F2I;

    invoke-direct {v1, p0, p3}, LX/F2I;-><init>(LX/F2L;Lcom/facebook/graphql/enums/GraphQLGroupPostPermissionSetting;)V

    .line 2192972
    invoke-static {p0, p1, v0, v1}, LX/F2L;->a(LX/F2L;Ljava/lang/String;LX/4Fk;LX/0TF;)V

    .line 2192973
    return-void

    .line 2192974
    :pswitch_0
    const-string v1, "ADMIN_ONLY"

    goto :goto_0

    .line 2192975
    :pswitch_1
    const-string v1, "ANYONE"

    goto :goto_0

    .line 2192976
    :pswitch_2
    const-string v1, "NONE"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
