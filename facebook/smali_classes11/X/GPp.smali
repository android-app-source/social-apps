.class public LX/GPp;
.super LX/6u5;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6u5",
        "<",
        "Lcom/facebook/adspayments/protocol/VerifyBrazilianTaxIdParams;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2349589
    invoke-direct {p0, p1}, LX/6u5;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 2349590
    return-void
.end method

.method public static b(LX/0QB;)LX/GPp;
    .locals 2

    .prologue
    .line 2349587
    new-instance v1, LX/GPp;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-direct {v1, v0}, LX/GPp;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 2349588
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 3

    .prologue
    .line 2349566
    check-cast p1, Lcom/facebook/adspayments/protocol/VerifyBrazilianTaxIdParams;

    .line 2349567
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "brazil_tax"

    .line 2349568
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 2349569
    move-object v0, v0

    .line 2349570
    const-string v1, "POST"

    .line 2349571
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 2349572
    move-object v0, v0

    .line 2349573
    const-string v1, "act_%s/brazil_tax"

    .line 2349574
    iget-object v2, p1, Lcom/facebook/adspayments/protocol/VerifyBrazilianTaxIdParams;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2349575
    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2349576
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 2349577
    move-object v0, v0

    .line 2349578
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "tax_id"

    .line 2349579
    iget-object p0, p1, Lcom/facebook/adspayments/protocol/VerifyBrazilianTaxIdParams;->b:Ljava/lang/String;

    move-object p0, p0

    .line 2349580
    invoke-direct {v1, v2, p0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    move-object v1, v1

    .line 2349581
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2349582
    move-object v0, v0

    .line 2349583
    sget-object v1, LX/14S;->STRING:LX/14S;

    .line 2349584
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2349585
    move-object v0, v0

    .line 2349586
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2349565
    const-string v0, "verify_brazilian_tax_id"

    return-object v0
.end method
