.class public LX/Gpq;
.super LX/GpR;
.source ""


# instance fields
.field private final d:LX/15i;

.field private final e:I


# direct methods
.method public constructor <init>(LX/Gpv;LX/15i;I)V
    .locals 2
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "<init>"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2395938
    invoke-direct {p0, p1}, LX/GpR;-><init>(LX/Gpv;)V

    .line 2395939
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iput-object p2, p0, LX/Gpq;->d:LX/15i;

    iput p3, p0, LX/Gpq;->e:I

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final synthetic a(LX/Clr;)V
    .locals 0

    .prologue
    .line 2395940
    invoke-virtual {p0}, LX/GpR;->b()V

    return-void
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 2395941
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395942
    check-cast v0, LX/Gpv;

    invoke-interface {v0}, LX/Gpv;->a()V

    .line 2395943
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, LX/Gpq;->d:LX/15i;

    iget v3, p0, LX/Gpq;->e:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2395944
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395945
    check-cast v0, LX/Gql;

    const/4 v1, 0x4

    invoke-virtual {v2, v3, v1}, LX/15i;->h(II)Z

    move-result v1

    invoke-virtual {v0, v1}, LX/Gql;->a(Z)V

    .line 2395946
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget-object v2, p0, LX/Gpq;->d:LX/15i;

    iget v3, p0, LX/Gpq;->e:I

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 2395947
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395948
    check-cast v0, LX/Gpv;

    const/4 v1, 0x1

    const-class v4, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingActionFragmentModel;

    invoke-virtual {v2, v3, v1, v4}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v1

    check-cast v1, LX/CHX;

    invoke-interface {v0, v1}, LX/Gpv;->a(LX/CHX;)V

    .line 2395949
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_2
    iget-object v0, p0, LX/Gpq;->d:LX/15i;

    iget v2, p0, LX/Gpq;->e:I

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2395950
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_3
    iget-object v3, p0, LX/Gpq;->d:LX/15i;

    iget v4, p0, LX/Gpq;->e:I

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 2395951
    const/4 v1, 0x3

    invoke-virtual {v0, v2, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x2

    const-class v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v0, v2, v5}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/Go7;->a(Ljava/lang/String;Ljava/lang/String;)LX/GoE;

    move-result-object v1

    .line 2395952
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395953
    check-cast v0, LX/Gql;

    .line 2395954
    iput-object v1, v0, LX/Gql;->f:LX/GoE;

    .line 2395955
    return-void

    .line 2395956
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 2395957
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 2395958
    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    .line 2395959
    :catchall_3
    move-exception v0

    :try_start_7
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v0
.end method
