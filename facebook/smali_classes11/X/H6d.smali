.class public final LX/H6d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimEnableNotificationsMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/H70;

.field public final synthetic b:LX/H6e;


# direct methods
.method public constructor <init>(LX/H6e;LX/H70;)V
    .locals 0

    .prologue
    .line 2427084
    iput-object p1, p0, LX/H6d;->b:LX/H6e;

    iput-object p2, p0, LX/H6d;->a:LX/H70;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2427085
    iget-object v0, p0, LX/H6d;->b:LX/H6e;

    iget-object v0, v0, LX/H6e;->a:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OffersWalletFragment;->g:LX/4nT;

    iget-object v1, p0, LX/H6d;->b:LX/H6e;

    iget-object v1, v1, LX/H6e;->a:Lcom/facebook/offers/fragment/OffersWalletFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f081ce7

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/4nT;->a(Ljava/lang/String;I)V

    .line 2427086
    iget-object v0, p0, LX/H6d;->b:LX/H6e;

    iget-object v0, v0, LX/H6e;->a:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OffersWalletFragment;->b:LX/0Sh;

    new-instance v1, Lcom/facebook/offers/fragment/OffersWalletFragment$5$1$2;

    invoke-direct {v1, p0}, Lcom/facebook/offers/fragment/OffersWalletFragment$5$1$2;-><init>(LX/H6d;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2427087
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2427088
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2427089
    if-eqz p1, :cond_0

    .line 2427090
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2427091
    if-eqz v0, :cond_0

    .line 2427092
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2427093
    check-cast v0, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimEnableNotificationsMutationModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferMutationsModels$OfferClaimEnableNotificationsMutationModel;->a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2427094
    iget-object v0, p0, LX/H6d;->b:LX/H6e;

    iget-object v0, v0, LX/H6e;->a:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OffersWalletFragment;->b:LX/0Sh;

    new-instance v1, Lcom/facebook/offers/fragment/OffersWalletFragment$5$1$1;

    invoke-direct {v1, p0}, Lcom/facebook/offers/fragment/OffersWalletFragment$5$1$1;-><init>(LX/H6d;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    .line 2427095
    iget-object v0, p0, LX/H6d;->a:LX/H70;

    invoke-interface {v0}, LX/H6y;->A()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "notifications_enabled"

    .line 2427096
    :goto_0
    iget-object v1, p0, LX/H6d;->b:LX/H6e;

    iget-object v1, v1, LX/H6e;->a:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v1, v1, Lcom/facebook/offers/fragment/OffersWalletFragment;->d:LX/0Zb;

    iget-object v2, p0, LX/H6d;->b:LX/H6e;

    iget-object v2, v2, LX/H6e;->a:Lcom/facebook/offers/fragment/OffersWalletFragment;

    iget-object v2, v2, Lcom/facebook/offers/fragment/OffersWalletFragment;->e:LX/H82;

    iget-object v3, p0, LX/H6d;->a:LX/H70;

    invoke-static {v3}, LX/H83;->a(LX/H70;)LX/H83;

    move-result-object v3

    const-string v4, "details"

    invoke-virtual {v2, v0, v3, v4}, LX/H82;->a(Ljava/lang/String;LX/H83;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2427097
    :cond_0
    return-void

    .line 2427098
    :cond_1
    const-string v0, "notifications_disabled"

    goto :goto_0
.end method
