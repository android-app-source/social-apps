.class public final LX/F1e;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V
    .locals 0

    .prologue
    .line 2192120
    iput-object p1, p0, LX/F1e;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 4

    .prologue
    .line 2192121
    iget-object v0, p0, LX/F1e;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    iget-object v0, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->r:LX/0Uh;

    const/16 v1, 0x4e5

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2192122
    iget-object v0, p0, LX/F1e;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    .line 2192123
    iget-object v1, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->q:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3Af;

    .line 2192124
    new-instance v2, LX/7TY;

    invoke-virtual {v1}, LX/3Af;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/7TY;-><init>(Landroid/content/Context;)V

    .line 2192125
    const p2, -0x808081

    .line 2192126
    const v3, 0x7f08317b

    invoke-virtual {v2, v3}, LX/34c;->e(I)LX/3Ai;

    move-result-object v3

    iget-object p0, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->t:LX/0wM;

    const p1, 0x7f0208b2

    invoke-virtual {p0, p1, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v3, p0}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v3

    new-instance p0, LX/F1m;

    invoke-direct {p0, v0}, LX/F1m;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    invoke-interface {v3, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2192127
    const v3, 0x7f08317c

    invoke-virtual {v2, v3}, LX/34c;->e(I)LX/3Ai;

    move-result-object v3

    iget-object p0, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->t:LX/0wM;

    const p1, 0x7f020906

    invoke-virtual {p0, p1, p2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object p0

    invoke-virtual {v3, p0}, LX/3Ai;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    move-result-object v3

    new-instance p0, LX/F1n;

    invoke-direct {p0, v0}, LX/F1n;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    invoke-interface {v3, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2192128
    invoke-virtual {v1, v2}, LX/3Af;->a(LX/1OM;)V

    .line 2192129
    invoke-virtual {v1}, LX/3Af;->show()V

    .line 2192130
    :goto_0
    return-void

    .line 2192131
    :cond_0
    iget-object v0, p0, LX/F1e;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    iget-object v0, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->g:LX/F0J;

    invoke-virtual {v0}, LX/F0J;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2192132
    iget-object v0, p0, LX/F1e;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    .line 2192133
    iget-object v1, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->q:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3Af;

    .line 2192134
    new-instance v2, LX/7Ta;

    invoke-virtual {v1}, LX/3Af;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, LX/7Ta;-><init>(Landroid/content/Context;)V

    .line 2192135
    const-string v3, ""

    invoke-virtual {v2, v3}, LX/34c;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object v3

    .line 2192136
    iget-object p0, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->g:LX/F0J;

    .line 2192137
    iget-object p1, p0, LX/F0J;->g:LX/F0I;

    move-object p0, p1

    .line 2192138
    sget-object p1, LX/F0I;->STATUS_UNSUBSCRIBED:LX/F0I;

    if-ne p0, p1, :cond_2

    .line 2192139
    const p0, 0x7f08316e

    invoke-virtual {v3, p0}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    .line 2192140
    const p0, 0x7f083171

    invoke-virtual {v3, p0}, LX/3Ai;->setTitle(I)Landroid/view/MenuItem;

    .line 2192141
    :goto_1
    invoke-virtual {v1, v2}, LX/3Af;->a(LX/1OM;)V

    .line 2192142
    invoke-virtual {v1}, LX/3Af;->show()V

    .line 2192143
    goto :goto_0

    .line 2192144
    :cond_1
    iget-object v0, p0, LX/F1e;->a:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->o$redex0(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    goto :goto_0

    .line 2192145
    :cond_2
    iget-object p0, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->g:LX/F0J;

    .line 2192146
    iget-object p1, p0, LX/F0J;->g:LX/F0I;

    move-object p0, p1

    .line 2192147
    sget-object p1, LX/F0I;->STATUS_SUBSCRIBED_HIGHLIGHTS:LX/F0I;

    if-ne p0, p1, :cond_3

    .line 2192148
    const p0, 0x7f083170

    invoke-virtual {v3, p0}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    .line 2192149
    const v3, 0x7f083172

    invoke-virtual {v2, v3}, LX/34c;->e(I)LX/3Ai;

    move-result-object v3

    const p0, 0x7f0208b2

    invoke-virtual {v3, p0}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v3

    new-instance p0, LX/F1T;

    invoke-direct {p0, v0}, LX/F1T;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    invoke-interface {v3, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2192150
    :goto_2
    const v3, 0x7f083173

    invoke-virtual {v2, v3}, LX/34c;->e(I)LX/3Ai;

    move-result-object v3

    const p0, 0x7f0208cf

    invoke-virtual {v3, p0}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    move-result-object v3

    new-instance p0, LX/F1U;

    invoke-direct {p0, v0}, LX/F1U;-><init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    invoke-interface {v3, p0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    goto :goto_1

    .line 2192151
    :cond_3
    const p0, 0x7f08316f

    invoke-virtual {v3, p0}, LX/3Ai;->a(I)Landroid/view/MenuItem;

    goto :goto_2
.end method
