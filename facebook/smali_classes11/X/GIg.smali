.class public final LX/GIg;
.super LX/GFu;
.source ""


# instance fields
.field public final synthetic a:LX/GIr;


# direct methods
.method public constructor <init>(LX/GIr;)V
    .locals 0

    .prologue
    .line 2336954
    iput-object p1, p0, LX/GIg;->a:LX/GIr;

    invoke-direct {p0}, LX/GFu;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 4

    .prologue
    .line 2336955
    check-cast p1, LX/GFt;

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 2336956
    iget-object v0, p0, LX/GIg;->a:LX/GIr;

    iget-object v0, v0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2336957
    iget-object v3, p1, LX/GFt;->a:LX/GGG;

    move-object v3, v3

    .line 2336958
    iput-object v3, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    .line 2336959
    iget-object v0, p1, LX/GFt;->a:LX/GGG;

    move-object v0, v0

    .line 2336960
    sget-object v3, LX/GGG;->ADDRESS:LX/GGG;

    if-ne v0, v3, :cond_0

    .line 2336961
    iget-object v0, p0, LX/GIg;->a:LX/GIr;

    iget-object v0, v0, LX/GIr;->f:LX/GIa;

    invoke-virtual {v0, v2}, LX/GIa;->setInterestsDividerVisibility(I)V

    .line 2336962
    iget-object v0, p0, LX/GIg;->a:LX/GIr;

    iget-object v0, v0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2336963
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->f:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v0, v1

    .line 2336964
    if-nez v0, :cond_1

    .line 2336965
    :goto_0
    return-void

    .line 2336966
    :cond_0
    iget-object v0, p0, LX/GIg;->a:LX/GIr;

    iget-object v0, v0, LX/GIr;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2336967
    iget-object v3, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v0, v3

    .line 2336968
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-ne v0, v3, :cond_2

    const/4 v0, 0x1

    .line 2336969
    :goto_1
    iget-object v3, p0, LX/GIg;->a:LX/GIr;

    iget-object v3, v3, LX/GIr;->f:LX/GIa;

    if-eqz v0, :cond_3

    :goto_2
    invoke-virtual {v3, v1}, LX/GIa;->setInterestsDividerVisibility(I)V

    .line 2336970
    :cond_1
    iget-object v0, p0, LX/GIg;->a:LX/GIr;

    invoke-virtual {v0}, LX/GIr;->b()V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2336971
    goto :goto_1

    :cond_3
    move v1, v2

    .line 2336972
    goto :goto_2
.end method
