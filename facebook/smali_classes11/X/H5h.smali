.class public LX/H5h;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/2kk;",
        ":",
        "LX/1Pr;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/H5L;

.field public final b:LX/2jO;

.field public final c:LX/H5b;


# direct methods
.method public constructor <init>(LX/H5L;LX/2jO;LX/H5b;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2424743
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2424744
    iput-object p1, p0, LX/H5h;->a:LX/H5L;

    .line 2424745
    iput-object p2, p0, LX/H5h;->b:LX/2jO;

    .line 2424746
    iput-object p3, p0, LX/H5h;->c:LX/H5b;

    .line 2424747
    return-void
.end method

.method public static a(LX/0QB;)LX/H5h;
    .locals 6

    .prologue
    .line 2424748
    const-class v1, LX/H5h;

    monitor-enter v1

    .line 2424749
    :try_start_0
    sget-object v0, LX/H5h;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2424750
    sput-object v2, LX/H5h;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2424751
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2424752
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2424753
    new-instance p0, LX/H5h;

    invoke-static {v0}, LX/H5L;->a(LX/0QB;)LX/H5L;

    move-result-object v3

    check-cast v3, LX/H5L;

    invoke-static {v0}, LX/2jO;->a(LX/0QB;)LX/2jO;

    move-result-object v4

    check-cast v4, LX/2jO;

    invoke-static {v0}, LX/H5b;->a(LX/0QB;)LX/H5b;

    move-result-object v5

    check-cast v5, LX/H5b;

    invoke-direct {p0, v3, v4, v5}, LX/H5h;-><init>(LX/H5L;LX/2jO;LX/H5b;)V

    .line 2424754
    move-object v0, p0

    .line 2424755
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2424756
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/H5h;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2424757
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2424758
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
