.class public final LX/GzG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GsB;


# instance fields
.field public final synthetic a:Lcom/facebook/login/LoginClient$Request;

.field public final synthetic b:Lcom/facebook/login/WebViewLoginMethodHandler;


# direct methods
.method public constructor <init>(Lcom/facebook/login/WebViewLoginMethodHandler;Lcom/facebook/login/LoginClient$Request;)V
    .locals 0

    .prologue
    .line 2411497
    iput-object p1, p0, LX/GzG;->b:Lcom/facebook/login/WebViewLoginMethodHandler;

    iput-object p2, p0, LX/GzG;->a:Lcom/facebook/login/LoginClient$Request;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;LX/GAA;)V
    .locals 8

    .prologue
    .line 2411498
    iget-object v0, p0, LX/GzG;->b:Lcom/facebook/login/WebViewLoginMethodHandler;

    iget-object v1, p0, LX/GzG;->a:Lcom/facebook/login/LoginClient$Request;

    const/4 v4, 0x0

    .line 2411499
    if-eqz p1, :cond_2

    .line 2411500
    const-string v2, "e2e"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2411501
    const-string v2, "e2e"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/facebook/login/WebViewLoginMethodHandler;->d:Ljava/lang/String;

    .line 2411502
    :cond_0
    :try_start_0
    iget-object v2, v1, Lcom/facebook/login/LoginClient$Request;->b:Ljava/util/Set;

    move-object v2, v2

    .line 2411503
    sget-object v3, LX/GA9;->WEB_VIEW:LX/GA9;

    .line 2411504
    iget-object v5, v1, Lcom/facebook/login/LoginClient$Request;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2411505
    invoke-static {v2, p1, v3, v5}, Lcom/facebook/login/LoginMethodHandler;->a(Ljava/util/Collection;Landroid/os/Bundle;LX/GA9;Ljava/lang/String;)Lcom/facebook/AccessToken;

    move-result-object v3

    .line 2411506
    iget-object v2, v0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    .line 2411507
    iget-object v5, v2, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    move-object v2, v5

    .line 2411508
    invoke-static {v2, v3}, Lcom/facebook/login/LoginClient$Result;->a(Lcom/facebook/login/LoginClient$Request;Lcom/facebook/AccessToken;)Lcom/facebook/login/LoginClient$Result;

    move-result-object v2

    .line 2411509
    iget-object v5, v0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    invoke-virtual {v5}, Lcom/facebook/login/LoginClient;->b()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-static {v5}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    move-result-object v5

    .line 2411510
    invoke-virtual {v5}, Landroid/webkit/CookieSyncManager;->sync()V

    .line 2411511
    iget-object v5, v3, Lcom/facebook/AccessToken;->h:Ljava/lang/String;

    move-object v3, v5

    .line 2411512
    iget-object v5, v0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    invoke-virtual {v5}, Lcom/facebook/login/LoginClient;->b()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    .line 2411513
    const-string v6, "com.facebook.login.AuthorizationClient.WebViewAuthHandler.TOKEN_STORE_KEY"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    const-string v6, "TOKEN"

    invoke-interface {v5, v6, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catch LX/GAA; {:try_start_0 .. :try_end_0} :catch_0

    .line 2411514
    :goto_0
    iget-object v3, v0, Lcom/facebook/login/WebViewLoginMethodHandler;->d:Ljava/lang/String;

    invoke-static {v3}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2411515
    iget-object v3, v0, Lcom/facebook/login/WebViewLoginMethodHandler;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/facebook/login/LoginMethodHandler;->a(Ljava/lang/String;)V

    .line 2411516
    :cond_1
    iget-object v3, v0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    invoke-virtual {v3, v2}, Lcom/facebook/login/LoginClient;->a(Lcom/facebook/login/LoginClient$Result;)V

    .line 2411517
    return-void

    .line 2411518
    :catch_0
    move-exception v2

    .line 2411519
    iget-object v3, v0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    .line 2411520
    iget-object v5, v3, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    move-object v3, v5

    .line 2411521
    invoke-virtual {v2}, LX/GAA;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v2}, Lcom/facebook/login/LoginClient$Result;->a(Lcom/facebook/login/LoginClient$Request;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/login/LoginClient$Result;

    move-result-object v2

    goto :goto_0

    .line 2411522
    :cond_2
    instance-of v2, p2, LX/GAC;

    if-eqz v2, :cond_3

    .line 2411523
    iget-object v2, v0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    .line 2411524
    iget-object v3, v2, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    move-object v2, v3

    .line 2411525
    const-string v3, "User canceled log in."

    invoke-static {v2, v3}, Lcom/facebook/login/LoginClient$Result;->a(Lcom/facebook/login/LoginClient$Request;Ljava/lang/String;)Lcom/facebook/login/LoginClient$Result;

    move-result-object v2

    goto :goto_0

    .line 2411526
    :cond_3
    iput-object v4, v0, Lcom/facebook/login/WebViewLoginMethodHandler;->d:Ljava/lang/String;

    .line 2411527
    invoke-virtual {p2}, LX/GAA;->getMessage()Ljava/lang/String;

    move-result-object v2

    .line 2411528
    instance-of v3, p2, LX/GAM;

    if-eqz v3, :cond_4

    .line 2411529
    check-cast p2, LX/GAM;

    .line 2411530
    iget-object v2, p2, LX/GAM;->error:LX/GAF;

    move-object v2, v2

    .line 2411531
    sget-object v3, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 2411532
    iget p0, v2, LX/GAF;->d:I

    move p0, p0

    .line 2411533
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    aput-object p0, v6, v7

    invoke-static {v3, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2411534
    invoke-virtual {v2}, LX/GAF;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2411535
    :goto_1
    iget-object v5, v0, Lcom/facebook/login/LoginMethodHandler;->b:Lcom/facebook/login/LoginClient;

    .line 2411536
    iget-object v6, v5, Lcom/facebook/login/LoginClient;->g:Lcom/facebook/login/LoginClient$Request;

    move-object v5, v6

    .line 2411537
    invoke-static {v5, v4, v2, v3}, Lcom/facebook/login/LoginClient$Result;->a(Lcom/facebook/login/LoginClient$Request;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/login/LoginClient$Result;

    move-result-object v2

    goto :goto_0

    :cond_4
    move-object v3, v4

    goto :goto_1
.end method
