.class public final LX/Gf5;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Gf6;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/25E;

.field public c:Z

.field public d:Z

.field public e:LX/1Pk;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic f:LX/Gf6;


# direct methods
.method public constructor <init>(LX/Gf6;)V
    .locals 1

    .prologue
    .line 2376778
    iput-object p1, p0, LX/Gf5;->f:LX/Gf6;

    .line 2376779
    move-object v0, p1

    .line 2376780
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2376781
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Gf5;->d:Z

    .line 2376782
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2376783
    const-string v0, "PageYouMayLikeHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2376784
    if-ne p0, p1, :cond_1

    .line 2376785
    :cond_0
    :goto_0
    return v0

    .line 2376786
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2376787
    goto :goto_0

    .line 2376788
    :cond_3
    check-cast p1, LX/Gf5;

    .line 2376789
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2376790
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2376791
    if-eq v2, v3, :cond_0

    .line 2376792
    iget-object v2, p0, LX/Gf5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Gf5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/Gf5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2376793
    goto :goto_0

    .line 2376794
    :cond_5
    iget-object v2, p1, LX/Gf5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_4

    .line 2376795
    :cond_6
    iget-object v2, p0, LX/Gf5;->b:LX/25E;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Gf5;->b:LX/25E;

    iget-object v3, p1, LX/Gf5;->b:LX/25E;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2376796
    goto :goto_0

    .line 2376797
    :cond_8
    iget-object v2, p1, LX/Gf5;->b:LX/25E;

    if-nez v2, :cond_7

    .line 2376798
    :cond_9
    iget-boolean v2, p0, LX/Gf5;->c:Z

    iget-boolean v3, p1, LX/Gf5;->c:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 2376799
    goto :goto_0

    .line 2376800
    :cond_a
    iget-boolean v2, p0, LX/Gf5;->d:Z

    iget-boolean v3, p1, LX/Gf5;->d:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 2376801
    goto :goto_0

    .line 2376802
    :cond_b
    iget-object v2, p0, LX/Gf5;->e:LX/1Pk;

    if-eqz v2, :cond_c

    iget-object v2, p0, LX/Gf5;->e:LX/1Pk;

    iget-object v3, p1, LX/Gf5;->e:LX/1Pk;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2376803
    goto :goto_0

    .line 2376804
    :cond_c
    iget-object v2, p1, LX/Gf5;->e:LX/1Pk;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
