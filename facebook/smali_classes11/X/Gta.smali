.class public LX/Gta;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 5
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DeprecatedMethod"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/high16 v4, 0x4000000

    .line 2402000
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2402001
    const-string v0, "settings/notifications"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/47N;

    new-instance v2, LX/47Q;

    const-class v3, Lcom/facebook/katana/NotificationSettingsActivity;

    invoke-direct {v2, v3}, LX/47Q;-><init>(Ljava/lang/Class;)V

    invoke-direct {v1, v2, v4}, LX/47N;-><init>(LX/47M;I)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2402002
    const-string v0, "settings/tor"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, LX/47N;

    new-instance v2, LX/47Q;

    const-class v3, Lcom/facebook/katana/FacebookOverTorSettingsActivity;

    invoke-direct {v2, v3}, LX/47Q;-><init>(Ljava/lang/Class;)V

    invoke-direct {v1, v2, v4}, LX/47N;-><init>(LX/47M;I)V

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;LX/47M;)V

    .line 2402003
    return-void
.end method
