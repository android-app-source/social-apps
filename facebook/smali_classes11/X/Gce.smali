.class public final LX/Gce;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Gcg;


# direct methods
.method public constructor <init>(LX/Gcg;)V
    .locals 0

    .prologue
    .line 2372182
    iput-object p1, p0, LX/Gce;->a:LX/Gcg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2372180
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 2372181
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 7
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2372167
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2372168
    if-nez p1, :cond_1

    .line 2372169
    :cond_0
    :goto_0
    return-void

    .line 2372170
    :cond_1
    iget-object v0, p1, Lcom/facebook/fbservice/service/OperationResult;->resultDataString:Ljava/lang/String;

    move-object v0, v0

    .line 2372171
    const-string v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2372172
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2372173
    iget-object v0, p0, LX/Gce;->a:LX/Gcg;

    iget-object v0, v0, LX/Gcg;->y:Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;

    .line 2372174
    iget-boolean v2, v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->l:Z

    if-eqz v2, :cond_2

    .line 2372175
    :goto_1
    goto :goto_0

    .line 2372176
    :cond_2
    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->l:Z

    .line 2372177
    iget-object v2, v0, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;->j:Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;

    invoke-virtual {v2}, Lcom/facebook/devicebasedlogin/ui/FaceRecOverlay;->a()V

    .line 2372178
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    .line 2372179
    new-instance v3, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment$4;

    invoke-direct {v3, v0}, Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment$4;-><init>(Lcom/facebook/devicebasedlogin/ui/DeviceBasedLoginFaceRecognitionFragment;)V

    const-wide/16 v4, 0x1f4

    const v6, 0x5c4aeb31

    invoke-static {v2, v3, v4, v5, v6}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_1
.end method
