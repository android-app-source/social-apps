.class public final LX/GE4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;",
        ">;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/GE6;


# direct methods
.method public constructor <init>(LX/GE6;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2330926
    iput-object p1, p0, LX/GE4;->b:LX/GE6;

    iput-object p2, p0, LX/GE4;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation

    .prologue
    .line 2330927
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2330928
    if-eqz p1, :cond_0

    .line 2330929
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2330930
    if-eqz v0, :cond_0

    .line 2330931
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2330932
    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    move-result-object v0

    if-nez v0, :cond_2

    .line 2330933
    :cond_0
    const/4 v0, 0x0

    .line 2330934
    :cond_1
    :goto_0
    return-object v0

    .line 2330935
    :cond_2
    new-instance v1, LX/GGN;

    invoke-direct {v1}, LX/GGN;-><init>()V

    .line 2330936
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2330937
    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    move-result-object v0

    .line 2330938
    iput-object v0, v1, LX/GGN;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    .line 2330939
    move-object v1, v1

    .line 2330940
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2330941
    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    move-result-object v0

    .line 2330942
    iput-object v0, v1, LX/GGN;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    .line 2330943
    move-object v0, v1

    .line 2330944
    sget-object v1, LX/8wL;->BOOST_POST_INSIGHTS:LX/8wL;

    .line 2330945
    iput-object v1, v0, LX/GGI;->b:LX/8wL;

    .line 2330946
    move-object v0, v0

    .line 2330947
    const-string v1, "boosted_post_mobile"

    .line 2330948
    iput-object v1, v0, LX/GGI;->m:Ljava/lang/String;

    .line 2330949
    move-object v0, v0

    .line 2330950
    invoke-virtual {v0}, LX/GGI;->a()Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2330951
    iget-object v1, p0, LX/GE4;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(Ljava/lang/String;)V

    .line 2330952
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2330953
    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->k()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-eqz v1, :cond_1

    .line 2330954
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2330955
    check-cast v1, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryPromotionInsightsModel;->k()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2330956
    const/4 v3, 0x0

    const-class v4, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-virtual {v2, v1, v3, v4, v5}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-static {v1}, LX/GG6;->a(Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;)LX/GGB;

    move-result-object v1

    .line 2330957
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->f:LX/GGB;

    .line 2330958
    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2330925
    const/4 v0, 0x0

    return v0
.end method
