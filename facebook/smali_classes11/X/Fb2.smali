.class public LX/Fb2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FaB;


# static fields
.field public static final i:Ljava/lang/String;


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/FaN;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13B;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FaY;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Landroid/view/View;

.field public final k:LX/CwT;

.field public final l:LX/0x9;

.field public m:LX/0hs;
    .annotation build Landroid/support/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2259519
    const-string v0, "search"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Fb2;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;LX/CwT;LX/0x9;)V
    .locals 1
    .param p1    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/CwT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/0x9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2259497
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2259498
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259499
    iput-object v0, p0, LX/Fb2;->b:LX/0Ot;

    .line 2259500
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259501
    iput-object v0, p0, LX/Fb2;->c:LX/0Ot;

    .line 2259502
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259503
    iput-object v0, p0, LX/Fb2;->d:LX/0Ot;

    .line 2259504
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259505
    iput-object v0, p0, LX/Fb2;->e:LX/0Ot;

    .line 2259506
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259507
    iput-object v0, p0, LX/Fb2;->f:LX/0Ot;

    .line 2259508
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259509
    iput-object v0, p0, LX/Fb2;->g:LX/0Ot;

    .line 2259510
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2259511
    iput-object v0, p0, LX/Fb2;->h:LX/0Ot;

    .line 2259512
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2259513
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2259514
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2259515
    iput-object p2, p0, LX/Fb2;->k:LX/CwT;

    .line 2259516
    iput-object p3, p0, LX/Fb2;->l:LX/0x9;

    .line 2259517
    iput-object p1, p0, LX/Fb2;->j:Landroid/view/View;

    .line 2259518
    return-void
.end method

.method public static f(LX/Fb2;)Z
    .locals 3

    .prologue
    .line 2259496
    iget-object v0, p0, LX/Fb2;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v1, LX/2SU;->p:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/0P1;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "*>;)V"
        }
    .end annotation

    .prologue
    .line 2259466
    iget-object v0, p0, LX/Fb2;->k:LX/CwT;

    .line 2259467
    iget-object v1, v0, LX/CwT;->a:LX/A0Z;

    move-object v0, v1

    .line 2259468
    invoke-interface {v0}, LX/A0Z;->fQ_()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/Fb2;->k:LX/CwT;

    .line 2259469
    iget-object v2, v1, LX/CwT;->a:LX/A0Z;

    move-object v1, v2

    .line 2259470
    invoke-interface {v1}, LX/A0Z;->d()Ljava/lang/String;

    move-result-object v1

    .line 2259471
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2259472
    iget-object v2, p0, LX/Fb2;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    const-string v3, "SearchAwareness"

    const-string v4, "title and description are empty for formatted tooltip."

    invoke-virtual {v2, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2259473
    const/4 v2, 0x0

    .line 2259474
    :goto_0
    move-object v0, v2

    .line 2259475
    iput-object v0, p0, LX/Fb2;->m:LX/0hs;

    .line 2259476
    iget-object v0, p0, LX/Fb2;->m:LX/0hs;

    if-eqz v0, :cond_0

    .line 2259477
    iget-object v0, p0, LX/Fb2;->m:LX/0hs;

    new-instance v1, LX/Fay;

    invoke-direct {v1, p0}, LX/Fay;-><init>(LX/Fb2;)V

    .line 2259478
    iput-object v1, v0, LX/0ht;->H:LX/2dD;

    .line 2259479
    :cond_0
    iget-object v0, p0, LX/Fb2;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13B;

    iget-object v1, p0, LX/Fb2;->k:LX/CwT;

    invoke-virtual {v0, v1, p1}, LX/13B;->a(LX/CwT;LX/0P1;)V

    .line 2259480
    return-void

    .line 2259481
    :cond_1
    iget-object v2, p0, LX/Fb2;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FaN;

    .line 2259482
    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0hs;->a(Ljava/lang/CharSequence;)V

    .line 2259483
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0hs;->b(Ljava/lang/CharSequence;)V

    .line 2259484
    sget-object v3, LX/3AV;->BELOW:LX/3AV;

    invoke-virtual {v2, v3}, LX/0ht;->a(LX/3AV;)V

    .line 2259485
    const/4 v3, -0x1

    .line 2259486
    iput v3, v2, LX/0hs;->t:I

    .line 2259487
    const v3, 0x3f333333    # 0.7f

    .line 2259488
    iput v3, v2, LX/FaN;->t:F

    .line 2259489
    const/4 v3, 0x0

    .line 2259490
    iput-boolean v3, v2, LX/0ht;->x:Z

    .line 2259491
    new-instance v3, LX/Faz;

    invoke-direct {v3, p0}, LX/Faz;-><init>(LX/Fb2;)V

    .line 2259492
    iget-object v4, v2, LX/FaN;->n:Lcom/facebook/fig/button/FigButton;

    new-instance v0, LX/FaL;

    invoke-direct {v0, v2, v3}, LX/FaL;-><init>(LX/FaN;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v4, v0}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2259493
    new-instance v3, LX/Fb1;

    invoke-direct {v3, p0}, LX/Fb1;-><init>(LX/Fb2;)V

    .line 2259494
    iget-object v4, v2, LX/FaN;->o:Lcom/facebook/fig/button/FigButton;

    new-instance v0, LX/FaM;

    invoke-direct {v0, v2, v3}, LX/FaM;-><init>(LX/FaN;Landroid/view/View$OnClickListener;)V

    invoke-virtual {v4, v0}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2259495
    iget-object v3, p0, LX/Fb2;->j:Landroid/view/View;

    invoke-virtual {v2, v3}, LX/0ht;->f(Landroid/view/View;)V

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 2259463
    iget-object v0, p0, LX/Fb2;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FaY;

    sget-object v1, LX/FaF;->LEARNING_NUX:LX/FaF;

    invoke-virtual {v0, v1}, LX/FaY;->a(LX/FaF;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Fb2;->k:LX/CwT;

    .line 2259464
    iget-boolean v1, v0, LX/CwT;->e:Z

    move v0, v1

    .line 2259465
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 2259456
    iget-object v0, p0, LX/Fb2;->k:LX/CwT;

    const/4 v1, 0x0

    .line 2259457
    iput-boolean v1, v0, LX/CwT;->e:Z

    .line 2259458
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 2259459
    iget-object v0, p0, LX/Fb2;->m:LX/0hs;

    if-eqz v0, :cond_0

    .line 2259460
    iget-object v0, p0, LX/Fb2;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13B;

    iget-object v1, p0, LX/Fb2;->k:LX/CwT;

    invoke-virtual {v0, v1}, LX/13B;->a(LX/CwT;)V

    .line 2259461
    const/4 v0, 0x0

    iput-object v0, p0, LX/Fb2;->m:LX/0hs;

    .line 2259462
    :cond_0
    return-void
.end method
