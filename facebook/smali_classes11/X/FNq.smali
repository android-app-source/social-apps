.class public final LX/FNq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/4oa;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/4oa",
        "<",
        "Landroid/view/View;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FNr;


# direct methods
.method public constructor <init>(LX/FNr;)V
    .locals 0

    .prologue
    .line 2233354
    iput-object p1, p0, LX/FNq;->a:LX/FNr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 2233355
    iget-object v0, p0, LX/FNq;->a:LX/FNr;

    invoke-virtual {v0, v5}, LX/FNr;->requestDisallowInterceptTouchEvent(Z)V

    .line 2233356
    new-instance v0, LX/FNo;

    invoke-direct {v0, p0}, LX/FNo;-><init>(LX/FNq;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 2233357
    iget-object v0, p0, LX/FNq;->a:LX/FNr;

    invoke-virtual {v0}, LX/FNr;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080992

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, LX/FNq;->a:LX/FNr;

    invoke-virtual {v3}, LX/FNr;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080011

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2233358
    const v0, 0x7f0d04b0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;

    .line 2233359
    new-instance v2, LX/FJm;

    invoke-direct {v2}, LX/FJm;-><init>()V

    const v3, 0x7f0212a6

    .line 2233360
    iput v3, v2, LX/FJm;->b:I

    .line 2233361
    move-object v2, v2

    .line 2233362
    iget-object v3, p0, LX/FNq;->a:LX/FNr;

    invoke-virtual {v3}, LX/FNr;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080991

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2233363
    iput-object v3, v2, LX/FJm;->c:Ljava/lang/String;

    .line 2233364
    move-object v2, v2

    .line 2233365
    iput-object v1, v2, LX/FJm;->d:Ljava/lang/String;

    .line 2233366
    move-object v1, v2

    .line 2233367
    iget-object v2, p0, LX/FNq;->a:LX/FNr;

    invoke-virtual {v2}, LX/FNr;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f080993

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2233368
    iput-object v2, v1, LX/FJm;->g:Ljava/lang/String;

    .line 2233369
    move-object v1, v1

    .line 2233370
    new-instance v2, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;

    invoke-direct {v2, v1}, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;-><init>(LX/FJm;)V

    move-object v1, v2

    .line 2233371
    invoke-virtual {v0, v1}, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->setModel(Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxModel;)V

    .line 2233372
    new-instance v1, LX/FNp;

    invoke-direct {v1, p0}, LX/FNp;-><init>(LX/FNq;)V

    .line 2233373
    iput-object v1, v0, Lcom/facebook/messaging/nux/templates/ImageTitleTextNuxView;->j:LX/FJq;

    .line 2233374
    return-void
.end method
