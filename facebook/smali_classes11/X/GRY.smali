.class public final LX/GRY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryInterfaces$AppInviteFields;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GRZ;

.field private b:I


# direct methods
.method public constructor <init>(LX/GRZ;)V
    .locals 1

    .prologue
    .line 2351585
    iput-object p1, p0, LX/GRY;->a:LX/GRZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2351586
    const/4 v0, -0x1

    iput v0, p0, LX/GRY;->b:I

    return-void
.end method


# virtual methods
.method public final hasNext()Z
    .locals 2

    .prologue
    .line 2351587
    iget v0, p0, LX/GRY;->b:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, LX/GRY;->a:LX/GRZ;

    iget-object v1, v1, LX/GRZ;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final next()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2351588
    iget v0, p0, LX/GRY;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, LX/GRY;->b:I

    .line 2351589
    iget-object v0, p0, LX/GRY;->a:LX/GRZ;

    iget-object v0, v0, LX/GRZ;->b:Ljava/util/ArrayList;

    iget v1, p0, LX/GRY;->b:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    return-object v0
.end method

.method public final remove()V
    .locals 4

    .prologue
    .line 2351590
    iget-object v1, p0, LX/GRY;->a:LX/GRZ;

    iget-object v0, p0, LX/GRY;->a:LX/GRZ;

    iget-object v0, v0, LX/GRZ;->b:Ljava/util/ArrayList;

    iget v2, p0, LX/GRY;->b:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    .line 2351591
    iget-object v2, v1, LX/GRZ;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 2351592
    iget v2, v1, LX/GRZ;->a:I

    iget-object v3, v1, LX/GRZ;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 2351593
    iget-object v2, v1, LX/GRZ;->b:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iput v2, v1, LX/GRZ;->a:I

    .line 2351594
    :cond_0
    iget v0, p0, LX/GRY;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, LX/GRY;->b:I

    .line 2351595
    return-void
.end method
