.class public final enum LX/H0r;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/H0r;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/H0r;

.field public static final enum DESCRIPTION:LX/H0r;

.field public static final enum FIELD_BUTTON:LX/H0r;

.field public static final enum FIELD_EDIT_TEXT:LX/H0r;

.field public static final enum FIELD_LABEL:LX/H0r;

.field public static final enum TITLE:LX/H0r;


# instance fields
.field private final viewType:I


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2414317
    new-instance v0, LX/H0r;

    const-string v1, "TITLE"

    const v2, 0x7f0d00ea

    invoke-direct {v0, v1, v3, v2}, LX/H0r;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/H0r;->TITLE:LX/H0r;

    .line 2414318
    new-instance v0, LX/H0r;

    const-string v1, "DESCRIPTION"

    const v2, 0x7f0d00e7

    invoke-direct {v0, v1, v4, v2}, LX/H0r;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/H0r;->DESCRIPTION:LX/H0r;

    .line 2414319
    new-instance v0, LX/H0r;

    const-string v1, "FIELD_LABEL"

    const v2, 0x7f0d00e5

    invoke-direct {v0, v1, v5, v2}, LX/H0r;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/H0r;->FIELD_LABEL:LX/H0r;

    .line 2414320
    new-instance v0, LX/H0r;

    const-string v1, "FIELD_EDIT_TEXT"

    const v2, 0x7f0d00e6

    invoke-direct {v0, v1, v6, v2}, LX/H0r;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/H0r;->FIELD_EDIT_TEXT:LX/H0r;

    .line 2414321
    new-instance v0, LX/H0r;

    const-string v1, "FIELD_BUTTON"

    const v2, 0x7f0d00e2

    invoke-direct {v0, v1, v7, v2}, LX/H0r;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/H0r;->FIELD_BUTTON:LX/H0r;

    .line 2414322
    const/4 v0, 0x5

    new-array v0, v0, [LX/H0r;

    sget-object v1, LX/H0r;->TITLE:LX/H0r;

    aput-object v1, v0, v3

    sget-object v1, LX/H0r;->DESCRIPTION:LX/H0r;

    aput-object v1, v0, v4

    sget-object v1, LX/H0r;->FIELD_LABEL:LX/H0r;

    aput-object v1, v0, v5

    sget-object v1, LX/H0r;->FIELD_EDIT_TEXT:LX/H0r;

    aput-object v1, v0, v6

    sget-object v1, LX/H0r;->FIELD_BUTTON:LX/H0r;

    aput-object v1, v0, v7

    sput-object v0, LX/H0r;->$VALUES:[LX/H0r;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2414311
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2414312
    iput p3, p0, LX/H0r;->viewType:I

    .line 2414313
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/H0r;
    .locals 1

    .prologue
    .line 2414314
    const-class v0, LX/H0r;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/H0r;

    return-object v0
.end method

.method public static values()[LX/H0r;
    .locals 1

    .prologue
    .line 2414315
    sget-object v0, LX/H0r;->$VALUES:[LX/H0r;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/H0r;

    return-object v0
.end method


# virtual methods
.method public final toInt()I
    .locals 1

    .prologue
    .line 2414316
    iget v0, p0, LX/H0r;->viewType:I

    return v0
.end method
