.class public final LX/GC1;
.super LX/6oO;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/account/recovery/fragment/FriendSearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/account/recovery/fragment/FriendSearchFragment;)V
    .locals 0

    .prologue
    .line 2327910
    iput-object p1, p0, LX/GC1;->a:Lcom/facebook/account/recovery/fragment/FriendSearchFragment;

    invoke-direct {p0}, LX/6oO;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 2327911
    iget-object v0, p0, LX/GC1;->a:Lcom/facebook/account/recovery/fragment/FriendSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->b:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v0}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 2327912
    iget-object v0, p0, LX/GC1;->a:Lcom/facebook/account/recovery/fragment/FriendSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->c:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 2327913
    :goto_0
    return-void

    .line 2327914
    :cond_0
    iget-object v0, p0, LX/GC1;->a:Lcom/facebook/account/recovery/fragment/FriendSearchFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/FriendSearchFragment;->c:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method
