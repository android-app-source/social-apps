.class public final LX/Fdu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 2264266
    iput-object p1, p0, LX/Fdu;->a:Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 7

    .prologue
    .line 2264268
    iget-object v0, p0, LX/Fdu;->a:Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;

    iget-object v1, v0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->w:LX/Fdo;

    if-nez p1, :cond_0

    const-string v0, ""

    .line 2264269
    :goto_0
    iget-boolean v2, v1, LX/Fdo;->d:Z

    if-nez v2, :cond_1

    .line 2264270
    :goto_1
    iget-object v0, p0, LX/Fdu;->a:Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/search/results/filters/ui/SearchSpecificFilterTypeaheadFragment;->r:LX/Fc6;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Fc6;->a(Ljava/lang/String;)V

    .line 2264271
    return-void

    .line 2264272
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2264273
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "\""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2264274
    iget-object v3, v1, LX/Fdo;->e:Ljava/lang/String;

    const-string v4, "\\{(.*?)\\}"

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2264275
    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 2264276
    const-string v4, "\""

    add-int/lit8 v5, v3, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v4

    .line 2264277
    new-instance v5, Landroid/text/SpannableString;

    invoke-direct {v5, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    iput-object v5, v1, LX/Fdo;->f:Landroid/text/SpannableString;

    .line 2264278
    iget-object v2, v1, LX/Fdo;->f:Landroid/text/SpannableString;

    new-instance v5, Landroid/text/style/StyleSpan;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Landroid/text/style/StyleSpan;-><init>(I)V

    add-int/lit8 v4, v4, 0x1

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v3, v4, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2264279
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    :goto_2
    iput-object v2, v1, LX/Fdo;->c:Lcom/facebook/search/results/protocol/filters/FilterValue;

    .line 2264280
    goto :goto_1

    .line 2264281
    :cond_2
    invoke-static {}, Lcom/facebook/search/results/protocol/filters/FilterValue;->g()LX/5ur;

    move-result-object v2

    .line 2264282
    iput-object v0, v2, LX/5ur;->a:Ljava/lang/String;

    .line 2264283
    move-object v2, v2

    .line 2264284
    iput-object v0, v2, LX/5ur;->b:Ljava/lang/String;

    .line 2264285
    move-object v2, v2

    .line 2264286
    iput-object v0, v2, LX/5ur;->c:Ljava/lang/String;

    .line 2264287
    move-object v2, v2

    .line 2264288
    invoke-virtual {v2}, LX/5ur;->f()Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-result-object v2

    goto :goto_2
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2264289
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2264267
    return-void
.end method
