.class public LX/Fjf;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:LX/Fjv;

.field public final e:LX/Fjj;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2277423
    const-string v0, "META-INF/MANIFEST.MF"

    const-string v1, "AndroidManifest.xml"

    const-string v2, "classes.dex"

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/Fjf;->a:Ljava/util/Set;

    .line 2277424
    const-string v0, "META-INF/MANIFEST.MF"

    const-string v1, "metadata.txt"

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/Fjf;->b:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/Fjv;LX/Fjj;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2277418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2277419
    iput-object p1, p0, LX/Fjf;->c:Landroid/content/Context;

    .line 2277420
    iput-object p2, p0, LX/Fjf;->d:LX/Fjv;

    .line 2277421
    iput-object p3, p0, LX/Fjf;->e:LX/Fjj;

    .line 2277422
    return-void
.end method

.method public static b(LX/0QB;)LX/Fjf;
    .locals 4

    .prologue
    .line 2277329
    new-instance v3, LX/Fjf;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 2277330
    new-instance v1, LX/Fjv;

    invoke-direct {v1}, LX/Fjv;-><init>()V

    .line 2277331
    move-object v1, v1

    .line 2277332
    move-object v1, v1

    .line 2277333
    check-cast v1, LX/Fjv;

    invoke-static {p0}, LX/Fjj;->b(LX/0QB;)LX/Fjj;

    move-result-object v2

    check-cast v2, LX/Fjj;

    invoke-direct {v3, v0, v1, v2}, LX/Fjf;-><init>(Landroid/content/Context;LX/Fjv;LX/Fjj;)V

    .line 2277334
    return-object v3
.end method


# virtual methods
.method public final a(Ljava/util/jar/JarFile;Ljava/lang/String;)Z
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 2277335
    if-nez p1, :cond_1

    move v0, v1

    .line 2277336
    :cond_0
    :goto_0
    return v0

    .line 2277337
    :cond_1
    const-string v0, "application/java-archive"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2277338
    sget-object v0, LX/Fjf;->b:Ljava/util/Set;

    invoke-static {v0}, LX/0RA;->b(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    move-object v2, v0

    .line 2277339
    :goto_1
    invoke-virtual {p1}, Ljava/util/jar/JarFile;->entries()Ljava/util/Enumeration;

    move-result-object v3

    .line 2277340
    :cond_2
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2277341
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/jar/JarEntry;

    .line 2277342
    invoke-virtual {v0}, Ljava/util/jar/JarEntry;->isDirectory()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2277343
    invoke-virtual {v0}, Ljava/util/jar/JarEntry;->getName()Ljava/lang/String;

    move-result-object v0

    .line 2277344
    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2277345
    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2277346
    :cond_3
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 2277347
    const/4 v0, 0x1

    .line 2277348
    :goto_2
    move v0, v0

    .line 2277349
    if-eqz v0, :cond_0

    .line 2277350
    iget-object v2, p0, LX/Fjf;->c:Landroid/content/Context;

    const/4 v3, 0x0

    .line 2277351
    if-nez v2, :cond_8

    .line 2277352
    :goto_3
    move-object v2, v3

    .line 2277353
    if-eqz v2, :cond_0

    array-length v3, v2

    if-lez v3, :cond_0

    aget-object v1, v2, v1

    .line 2277354
    if-eqz v1, :cond_9

    .line 2277355
    invoke-virtual {v1}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v3

    .line 2277356
    new-instance v4, Ljava/io/ByteArrayInputStream;

    invoke-direct {v4, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 2277357
    :try_start_0
    const-string v3, "X.509"

    invoke-static {v3}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v3

    .line 2277358
    invoke-virtual {v3, v4}, Ljava/security/cert/CertificateFactory;->generateCertificate(Ljava/io/InputStream;)Ljava/security/cert/Certificate;

    move-result-object v3

    .line 2277359
    check-cast v3, Ljava/security/cert/X509Certificate;

    .line 2277360
    invoke-virtual {v3}, Ljava/security/cert/X509Certificate;->getIssuerDN()Ljava/security/Principal;

    move-result-object v3

    invoke-interface {v3}, Ljava/security/Principal;->getName()Ljava/lang/String;
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 2277361
    :goto_4
    move-object v3, v3

    .line 2277362
    const-string v4, "CN=Android Debug"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    move v1, v3

    .line 2277363
    if-nez v1, :cond_0

    .line 2277364
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 2277365
    :try_start_1
    invoke-virtual {p1}, Ljava/util/jar/JarFile;->entries()Ljava/util/Enumeration;

    move-result-object v8

    move-object v6, v1

    .line 2277366
    :cond_4
    :goto_5
    invoke-interface {v8}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_f

    .line 2277367
    invoke-interface {v8}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/jar/JarEntry;

    .line 2277368
    invoke-virtual {v0}, Ljava/util/jar/JarEntry;->isDirectory()Z

    move-result v3

    if-nez v3, :cond_4

    .line 2277369
    invoke-virtual {v0}, Ljava/util/jar/JarEntry;->getName()Ljava/lang/String;

    move-result-object v3

    .line 2277370
    const-string v5, "META-INF/"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 2277371
    invoke-static {p1, v0}, LX/Fjv;->a(Ljava/util/jar/JarFile;Ljava/util/jar/JarEntry;)[Ljava/security/cert/Certificate;

    move-result-object v7

    .line 2277372
    if-nez v7, :cond_a

    .line 2277373
    iget-object v0, p0, LX/Fjf;->e:LX/Fjj;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/util/jar/JarFile;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " has no certificates"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/Fjj;->a(Ljava/lang/String;)V

    move-object v0, v1
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_3

    .line 2277374
    :goto_6
    move-object v0, v0

    .line 2277375
    const/4 v1, 0x0

    .line 2277376
    if-nez v2, :cond_13

    .line 2277377
    if-nez v0, :cond_5

    const/4 v1, 0x1

    .line 2277378
    :cond_5
    :goto_7
    move v0, v1

    .line 2277379
    goto/16 :goto_0

    .line 2277380
    :cond_6
    sget-object v0, LX/Fjf;->a:Ljava/util/Set;

    invoke-static {v0}, LX/0RA;->b(Ljava/lang/Iterable;)Ljava/util/HashSet;

    move-result-object v0

    move-object v2, v0

    goto/16 :goto_1

    .line 2277381
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 2277382
    :cond_8
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    .line 2277383
    :try_start_2
    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    const/16 p2, 0x40

    invoke-virtual {v5, v4, p2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    iget-object v3, v4, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    goto/16 :goto_3

    .line 2277384
    :catch_0
    move-exception v4

    .line 2277385
    iget-object v5, p0, LX/Fjf;->e:LX/Fjj;

    const-string p2, "PackageManager.NameNotFoundException"

    invoke-virtual {v5, p2, v4}, LX/Fjj;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_3

    .line 2277386
    :catch_1
    const-string v3, "Unknown"

    goto/16 :goto_4

    .line 2277387
    :cond_9
    const-string v3, "Unknown"

    goto/16 :goto_4

    .line 2277388
    :cond_a
    :try_start_3
    if-nez v6, :cond_b

    move-object v6, v7

    .line 2277389
    goto :goto_5

    .line 2277390
    :cond_b
    array-length v9, v6

    move v5, v4

    :goto_8
    if-ge v5, v9, :cond_4

    aget-object v10, v6, v5

    .line 2277391
    array-length v11, v7

    move v3, v4

    :goto_9
    if-ge v3, v11, :cond_11

    aget-object p2, v7, v3

    .line 2277392
    if-eqz v10, :cond_d

    invoke-virtual {v10, p2}, Ljava/security/cert/Certificate;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_d

    .line 2277393
    const/4 v3, 0x1

    .line 2277394
    :goto_a
    if-eqz v3, :cond_c

    array-length v3, v6

    array-length v10, v7

    if-eq v3, v10, :cond_e

    .line 2277395
    :cond_c
    iget-object v3, p0, LX/Fjf;->e:LX/Fjj;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Package "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/jar/JarFile;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " has mismatched certificates at entry "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/util/jar/JarEntry;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/Fjj;->a(Ljava/lang/String;)V

    move-object v0, v1

    .line 2277396
    goto :goto_6

    .line 2277397
    :cond_d
    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    .line 2277398
    :cond_e
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_8

    .line 2277399
    :cond_f
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 2277400
    if-eqz v6, :cond_12

    array-length v0, v6

    if-lez v0, :cond_12
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_3

    .line 2277401
    :try_start_4
    array-length v0, v6

    .line 2277402
    new-array v0, v0, [Landroid/content/pm/Signature;

    .line 2277403
    array-length v8, v6

    move v5, v4

    :goto_b
    if-ge v4, v8, :cond_10

    aget-object v9, v6, v4

    .line 2277404
    add-int/lit8 v7, v5, 0x1

    new-instance v10, Landroid/content/pm/Signature;

    invoke-virtual {v9}, Ljava/security/cert/Certificate;->getEncoded()[B

    move-result-object v9

    invoke-direct {v10, v9}, Landroid/content/pm/Signature;-><init>([B)V

    aput-object v10, v0, v5
    :try_end_4
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_3

    .line 2277405
    :try_start_5
    add-int/lit8 v4, v4, 0x1

    move v5, v7

    goto :goto_b

    .line 2277406
    :catch_2
    move-object v0, v3

    .line 2277407
    :cond_10
    :goto_c
    move-object v0, v0
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_3

    .line 2277408
    goto/16 :goto_6

    .line 2277409
    :catch_3
    move-exception v0

    .line 2277410
    iget-object v3, p0, LX/Fjf;->e:LX/Fjj;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "RuntimeException reading "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/util/jar/JarFile;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, LX/Fjj;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    .line 2277411
    goto/16 :goto_6

    :cond_11
    move v3, v4

    goto :goto_a

    :cond_12
    :try_start_6
    move-object v0, v3

    goto :goto_c
    :try_end_6
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_3

    .line 2277412
    :cond_13
    if-eqz v0, :cond_5

    .line 2277413
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 2277414
    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 2277415
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 2277416
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 2277417
    invoke-virtual {v1, v3}, Ljava/util/HashSet;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto/16 :goto_7
.end method
