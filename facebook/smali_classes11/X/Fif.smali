.class public final LX/Fif;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/search/model/SearchSpotlightCollectionUnit;

.field public final synthetic b:Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;Lcom/facebook/search/model/SearchSpotlightCollectionUnit;)V
    .locals 0

    .prologue
    .line 2275604
    iput-object p1, p0, LX/Fif;->b:Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;

    iput-object p2, p0, LX/Fif;->a:Lcom/facebook/search/model/SearchSpotlightCollectionUnit;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 2275605
    iget-object v0, p0, LX/Fif;->a:Lcom/facebook/search/model/SearchSpotlightCollectionUnit;

    invoke-virtual {v0}, Lcom/facebook/search/model/TypeaheadCollectionUnit;->o()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/TypeaheadUnit;

    .line 2275606
    instance-of v4, v0, Lcom/facebook/search/model/SearchSpotlightCardUnit;

    if-eqz v4, :cond_1

    .line 2275607
    iget-object v4, p0, LX/Fif;->b:Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;

    iget-object v4, v4, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->d:Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightCardPagePartDefinition;

    check-cast v0, Lcom/facebook/search/model/SearchSpotlightCardUnit;

    invoke-virtual {p1, v4, v0}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2275608
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2275609
    :cond_1
    instance-of v4, v0, Lcom/facebook/search/model/SearchSpotlightIntroUnit;

    if-eqz v4, :cond_0

    .line 2275610
    iget-object v4, p0, LX/Fif;->b:Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;

    iget-object v4, v4, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->e:Lcom/facebook/search/typeahead/rows/HScrollSearchSpotlightIntroPagePartDefinition;

    check-cast v0, Lcom/facebook/search/model/SearchSpotlightIntroUnit;

    invoke-virtual {p1, v4, v0}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    goto :goto_1

    .line 2275611
    :cond_2
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 2275612
    iget-object v0, p0, LX/Fif;->b:Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;

    iget-object v0, v0, Lcom/facebook/search/typeahead/rows/SearchSpotlightHScrollRecyclerPartDefinition;->f:LX/13B;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, v1}, LX/13B;->b(I)V

    .line 2275613
    return-void
.end method
