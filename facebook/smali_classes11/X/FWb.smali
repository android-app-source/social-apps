.class public LX/FWb;
.super LX/98h;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/98h",
        "<",
        "Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;",
        "LX/FWa;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/FWb;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FWZ;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FWL;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0Uh;


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Uh;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/FWZ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FWL;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2252316
    invoke-direct {p0}, LX/98h;-><init>()V

    .line 2252317
    iput-object p1, p0, LX/FWb;->a:LX/0Ot;

    .line 2252318
    iput-object p2, p0, LX/FWb;->b:LX/0Ot;

    .line 2252319
    iput-object p3, p0, LX/FWb;->c:LX/0Uh;

    .line 2252320
    return-void
.end method

.method public static a(LX/0QB;)LX/FWb;
    .locals 6

    .prologue
    .line 2252321
    sget-object v0, LX/FWb;->d:LX/FWb;

    if-nez v0, :cond_1

    .line 2252322
    const-class v1, LX/FWb;

    monitor-enter v1

    .line 2252323
    :try_start_0
    sget-object v0, LX/FWb;->d:LX/FWb;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2252324
    if-eqz v2, :cond_0

    .line 2252325
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2252326
    new-instance v4, LX/FWb;

    const/16 v3, 0x3296

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v3, 0x3291

    invoke-static {v0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v3

    check-cast v3, LX/0Uh;

    invoke-direct {v4, v5, p0, v3}, LX/FWb;-><init>(LX/0Ot;LX/0Ot;LX/0Uh;)V

    .line 2252327
    move-object v0, v4

    .line 2252328
    sput-object v0, LX/FWb;->d:LX/FWb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2252329
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2252330
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2252331
    :cond_1
    sget-object v0, LX/FWb;->d:LX/FWb;

    return-object v0

    .line 2252332
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2252333
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;)LX/98g;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/Intent;",
            ")",
            "LX/98g",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;",
            "LX/FWa;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2252334
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2252335
    if-nez v0, :cond_0

    .line 2252336
    const/4 v0, 0x0

    .line 2252337
    :goto_0
    return-object v0

    .line 2252338
    :cond_0
    iget-object v1, p0, LX/FWb;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    const-string v1, "extra_section_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/FWL;->a(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-result-object v1

    .line 2252339
    iget-object v0, p0, LX/FWb;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FWZ;

    .line 2252340
    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/FWZ;->a(LX/0am;LX/0am;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v2

    .line 2252341
    invoke-static {v1}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v3

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, LX/FWZ;->b(LX/0am;LX/0am;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    .line 2252342
    new-instance v0, LX/98g;

    new-instance v4, LX/FWa;

    invoke-direct {v4, v2, v3}, LX/FWa;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;Lcom/google/common/util/concurrent/ListenableFuture;)V

    invoke-direct {v0, v1, v4}, LX/98g;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2252343
    check-cast p1, LX/FWa;

    .line 2252344
    if-eqz p1, :cond_0

    .line 2252345
    const/4 v0, 0x1

    .line 2252346
    iget-object p0, p1, LX/FWa;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {p0, v0}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2252347
    iget-object p0, p1, LX/FWa;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {p0, v0}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2252348
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2252349
    iget-object v1, p0, LX/FWb;->c:LX/0Uh;

    const/16 v2, 0x4a3

    invoke-virtual {v1, v2, v0}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method
