.class public LX/FKV;
.super LX/4Zv;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/4Zv",
        "<",
        "Lcom/facebook/messaging/service/model/ModifyThreadParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/FFR;


# direct methods
.method public constructor <init>(LX/0sO;LX/FFR;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2225167
    invoke-direct {p0, p1}, LX/4Zv;-><init>(LX/0sO;)V

    .line 2225168
    iput-object p2, p0, LX/FKV;->b:LX/FFR;

    .line 2225169
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2225170
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2225171
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2225166
    const/4 v0, 0x0

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 4

    .prologue
    .line 2225146
    check-cast p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;

    .line 2225147
    iget-object v0, p0, LX/FKV;->b:LX/FFR;

    .line 2225148
    iget-object v1, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2225149
    iget-object v2, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->r:LX/03R;

    move-object v2, v2

    .line 2225150
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/03R;->asBoolean(Z)Z

    move-result v2

    .line 2225151
    new-instance p0, LX/4HC;

    invoke-direct {p0}, LX/4HC;-><init>()V

    .line 2225152
    const-string v3, "thread_fbid"

    invoke-virtual {p0, v3, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2225153
    move-object p1, p0

    .line 2225154
    iget-object v3, v0, LX/FFR;->a:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2225155
    const-string v0, "actor_id"

    invoke-virtual {p1, v0, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2225156
    if-eqz v2, :cond_0

    const-string v3, "JOINABLE"

    .line 2225157
    :goto_0
    const-string v2, "mode"

    invoke-virtual {p0, v2, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2225158
    move-object v3, p0

    .line 2225159
    move-object v0, v3

    .line 2225160
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2225161
    const-string v2, "client_mutation_id"

    invoke-virtual {v0, v2, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2225162
    move-object v0, v0

    .line 2225163
    new-instance v1, LX/5d5;

    invoke-direct {v1}, LX/5d5;-><init>()V

    move-object v1, v1

    .line 2225164
    const-string v2, "input"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    return-object v0

    .line 2225165
    :cond_0
    const-string v3, "PRIVATE"

    goto :goto_0
.end method
