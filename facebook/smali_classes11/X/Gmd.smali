.class public LX/Gmd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0hc;
.implements LX/IXp;
.implements LX/Che;


# instance fields
.field private final a:LX/ChR;

.field private final b:LX/ChT;

.field private final c:LX/ChV;

.field public d:LX/CqD;

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2392498
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2392499
    new-instance v0, LX/Gma;

    invoke-direct {v0, p0}, LX/Gma;-><init>(LX/Gmd;)V

    iput-object v0, p0, LX/Gmd;->a:LX/ChR;

    .line 2392500
    new-instance v0, LX/Gmb;

    invoke-direct {v0, p0}, LX/Gmb;-><init>(LX/Gmd;)V

    iput-object v0, p0, LX/Gmd;->b:LX/ChT;

    .line 2392501
    new-instance v0, LX/Gmc;

    invoke-direct {v0, p0}, LX/Gmc;-><init>(LX/Gmd;)V

    iput-object v0, p0, LX/Gmd;->c:LX/ChV;

    return-void
.end method

.method private d(I)V
    .locals 2

    .prologue
    .line 2392493
    iget-object v0, p0, LX/Gmd;->d:LX/CqD;

    if-nez v0, :cond_1

    .line 2392494
    :cond_0
    :goto_0
    return-void

    .line 2392495
    :cond_1
    iget-object v0, p0, LX/Gmd;->d:LX/CqD;

    invoke-interface {v0, p1}, LX/CqD;->c(I)Lcom/facebook/richdocument/view/carousel/PageableFragment;

    move-result-object v0

    .line 2392496
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/facebook/instantarticles/InstantArticlesFragment;

    if-eqz v1, :cond_0

    .line 2392497
    check-cast v0, Lcom/facebook/instantarticles/InstantArticlesFragment;

    invoke-virtual {v0}, Lcom/facebook/instantarticles/InstantArticlesFragment;->r()V

    goto :goto_0
.end method


# virtual methods
.method public final B_(I)V
    .locals 0

    .prologue
    .line 2392466
    return-void
.end method

.method public final a(IFI)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 2392484
    iget-boolean v0, p0, LX/Gmd;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_1

    .line 2392485
    :cond_0
    :goto_0
    return-void

    .line 2392486
    :cond_1
    iget-object v0, p0, LX/Gmd;->d:LX/CqD;

    invoke-interface {v0}, LX/CqD;->getActiveFragmentIndex()I

    move-result v0

    .line 2392487
    if-ne p1, v0, :cond_2

    move v0, v1

    .line 2392488
    :goto_1
    if-eqz v0, :cond_3

    .line 2392489
    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0}, LX/Gmd;->d(I)V

    .line 2392490
    :goto_2
    iput-boolean v1, p0, LX/Gmd;->e:Z

    goto :goto_0

    .line 2392491
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2392492
    :cond_3
    invoke-direct {p0, p1}, LX/Gmd;->d(I)V

    goto :goto_2
.end method

.method public final a(LX/0b4;)V
    .locals 1

    .prologue
    .line 2392479
    if-eqz p1, :cond_0

    .line 2392480
    iget-object v0, p0, LX/Gmd;->a:LX/ChR;

    invoke-virtual {p1, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 2392481
    iget-object v0, p0, LX/Gmd;->b:LX/ChT;

    invoke-virtual {p1, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 2392482
    iget-object v0, p0, LX/Gmd;->c:LX/ChV;

    invoke-virtual {p1, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 2392483
    :cond_0
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 2392476
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 2392477
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Gmd;->e:Z

    .line 2392478
    :cond_0
    return-void
.end method

.method public final b(LX/0b4;)V
    .locals 1

    .prologue
    .line 2392471
    if-eqz p1, :cond_0

    .line 2392472
    iget-object v0, p0, LX/Gmd;->a:LX/ChR;

    invoke-virtual {p1, v0}, LX/0b4;->b(LX/0b2;)Z

    .line 2392473
    iget-object v0, p0, LX/Gmd;->b:LX/ChT;

    invoke-virtual {p1, v0}, LX/0b4;->b(LX/0b2;)Z

    .line 2392474
    iget-object v0, p0, LX/Gmd;->c:LX/ChV;

    invoke-virtual {p1, v0}, LX/0b4;->b(LX/0b2;)Z

    .line 2392475
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 0

    .prologue
    .line 2392469
    invoke-direct {p0, p1}, LX/Gmd;->d(I)V

    .line 2392470
    return-void
.end method

.method public final setFragmentPager(LX/CqD;)V
    .locals 0

    .prologue
    .line 2392467
    iput-object p1, p0, LX/Gmd;->d:LX/CqD;

    .line 2392468
    return-void
.end method
