.class public final enum LX/GHw;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GHw;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GHw;

.field public static final enum CHANGED:LX/GHw;

.field public static final enum UNCHANGED:LX/GHw;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2335341
    new-instance v0, LX/GHw;

    const-string v1, "CHANGED"

    invoke-direct {v0, v1, v2}, LX/GHw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GHw;->CHANGED:LX/GHw;

    new-instance v0, LX/GHw;

    const-string v1, "UNCHANGED"

    invoke-direct {v0, v1, v3}, LX/GHw;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GHw;->UNCHANGED:LX/GHw;

    .line 2335342
    const/4 v0, 0x2

    new-array v0, v0, [LX/GHw;

    sget-object v1, LX/GHw;->CHANGED:LX/GHw;

    aput-object v1, v0, v2

    sget-object v1, LX/GHw;->UNCHANGED:LX/GHw;

    aput-object v1, v0, v3

    sput-object v0, LX/GHw;->$VALUES:[LX/GHw;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2335343
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GHw;
    .locals 1

    .prologue
    .line 2335344
    const-class v0, LX/GHw;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GHw;

    return-object v0
.end method

.method public static values()[LX/GHw;
    .locals 1

    .prologue
    .line 2335345
    sget-object v0, LX/GHw;->$VALUES:[LX/GHw;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GHw;

    return-object v0
.end method
