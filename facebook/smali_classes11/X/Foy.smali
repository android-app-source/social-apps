.class public LX/Foy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0Tf;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/1Ck;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public e:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2290504
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2290505
    return-void
.end method

.method private a(LX/0zO;Ljava/lang/String;)Ljava/util/concurrent/Callable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zO",
            "<",
            "Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchModel;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/concurrent/Callable",
            "<",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Fow;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2290503
    new-instance v0, LX/For;

    invoke-direct {v0, p0, p1, p2}, LX/For;-><init>(LX/Foy;LX/0zO;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a$redex0(LX/Foy;Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/socialgood/protocol/FundraiserCharitySearchModels$FundraiserCharitySearchModel;",
            ">;>;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Fow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2290502
    new-instance v0, LX/Fos;

    invoke-direct {v0, p0, p2}, LX/Fos;-><init>(LX/Foy;Ljava/lang/String;)V

    iget-object v1, p0, LX/Foy;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {p1, v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/Foy;
    .locals 5

    .prologue
    .line 2290469
    new-instance v4, LX/Foy;

    invoke-direct {v4}, LX/Foy;-><init>()V

    .line 2290470
    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v1

    check-cast v1, LX/0Tf;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    .line 2290471
    iput-object v0, v4, LX/Foy;->a:Ljava/util/concurrent/ExecutorService;

    iput-object v1, v4, LX/Foy;->b:LX/0Tf;

    iput-object v2, v4, LX/Foy;->c:LX/0tX;

    iput-object v3, v4, LX/Foy;->d:LX/1Ck;

    .line 2290472
    return-object v4
.end method

.method private d()LX/0Ve;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Ve",
            "<",
            "LX/Fow;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2290501
    new-instance v0, LX/Fot;

    invoke-direct {v0, p0}, LX/Fot;-><init>(LX/Foy;)V

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2290499
    iget-object v0, p0, LX/Foy;->d:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->c()V

    .line 2290500
    return-void
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2290491
    iget-object v0, p0, LX/Foy;->d:LX/1Ck;

    sget-object v1, LX/Fox;->FETCH_CATEGORY_CHARITY:LX/Fox;

    .line 2290492
    invoke-static {}, LX/Fmf;->a()LX/Fme;

    move-result-object v2

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 2290493
    new-instance v3, LX/Fme;

    invoke-direct {v3}, LX/Fme;-><init>()V

    const-string v4, "charity_category"

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    const-string v4, "first"

    const/16 v5, 0xa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "is_u2c_eligible"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v3

    const-string v4, "page_cursor"

    invoke-virtual {v3, v4, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    .line 2290494
    iget-object v4, v3, LX/0gW;->e:LX/0w7;

    move-object v3, v4

    .line 2290495
    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2290496
    move-object v2, v2

    .line 2290497
    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, LX/Foy;->a(LX/0zO;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v2

    invoke-direct {p0}, LX/Foy;->d()LX/0Ve;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2290498
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2290483
    iget-object v0, p0, LX/Foy;->d:LX/1Ck;

    sget-object v1, LX/Fox;->FETCH_QUERY_CHARITY:LX/Fox;

    .line 2290484
    invoke-static {}, LX/Fmf;->a()LX/Fme;

    move-result-object v2

    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    .line 2290485
    new-instance v3, LX/Fme;

    invoke-direct {v3}, LX/Fme;-><init>()V

    const-string v4, "first"

    const/16 v5, 0xa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    const-string v4, "is_u2c_eligible"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v3

    const-string v4, "page_cursor"

    invoke-virtual {v3, v4, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v3

    .line 2290486
    iget-object v4, v3, LX/0gW;->e:LX/0w7;

    move-object v3, v4

    .line 2290487
    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2290488
    move-object v2, v2

    .line 2290489
    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, LX/Foy;->a(LX/0zO;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v2

    invoke-direct {p0}, LX/Foy;->d()LX/0Ve;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2290490
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2290473
    invoke-static {}, LX/Fmf;->a()LX/Fme;

    move-result-object v0

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2290474
    new-instance v1, LX/Fme;

    invoke-direct {v1}, LX/Fme;-><init>()V

    const-string v2, "query"

    invoke-virtual {v1, v2, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "first"

    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "is_u2c_eligible"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v1

    const-string v2, "page_cursor"

    invoke-virtual {v1, v2, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    .line 2290475
    iget-object v2, v1, LX/0gW;->e:LX/0w7;

    move-object v1, v2

    .line 2290476
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0w7;)LX/0zO;

    .line 2290477
    move-object v0, v0

    .line 2290478
    if-nez p2, :cond_0

    .line 2290479
    iget-object v1, p0, LX/Foy;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2290480
    iget-object v1, p0, LX/Foy;->d:LX/1Ck;

    sget-object v2, LX/Fox;->FETCH_QUERY_CHARITY:LX/Fox;

    invoke-static {p0, v0, p1}, LX/Foy;->a$redex0(LX/Foy;Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    invoke-direct {p0}, LX/Foy;->d()LX/0Ve;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2290481
    :goto_0
    return-void

    .line 2290482
    :cond_0
    iget-object v1, p0, LX/Foy;->d:LX/1Ck;

    sget-object v2, LX/Fox;->FETCH_QUERY_CHARITY:LX/Fox;

    invoke-direct {p0, v0, p1}, LX/Foy;->a(LX/0zO;Ljava/lang/String;)Ljava/util/concurrent/Callable;

    move-result-object v0

    invoke-direct {p0}, LX/Foy;->d()LX/0Ve;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method
