.class public LX/Gdj;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1nG;

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Q;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lcom/facebook/graphql/model/GraphQLPage;

.field public final d:LX/162;

.field public final e:Z


# direct methods
.method public constructor <init>(Lcom/facebook/java2js/JSValue;Lcom/facebook/java2js/JSValue;LX/1nG;LX/0Ot;)V
    .locals 2
    .param p1    # Lcom/facebook/java2js/JSValue;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/java2js/JSValue;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/java2js/JSValue;",
            "Lcom/facebook/java2js/JSValue;",
            "LX/1nG;",
            "LX/0Ot",
            "<",
            "LX/17Q;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2374503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2374504
    iput-object p3, p0, LX/Gdj;->a:LX/1nG;

    .line 2374505
    iput-object p4, p0, LX/Gdj;->b:LX/0Ot;

    .line 2374506
    const-string v0, "trackingCodes"

    invoke-virtual {p2, v0}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    const-class v1, LX/162;

    invoke-virtual {v0, v1}, Lcom/facebook/java2js/JSValue;->to(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/162;

    iput-object v0, p0, LX/Gdj;->d:LX/162;

    .line 2374507
    const-string v0, "isSponsored"

    invoke-virtual {p1, v0}, Lcom/facebook/java2js/JSValue;->getBooleanProperty(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/Gdj;->e:Z

    .line 2374508
    const-string v0, "page"

    invoke-virtual {p1, v0}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    .line 2374509
    const-string v1, "__native_object__"

    invoke-virtual {v0, v1}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v0

    const-class v1, Lcom/facebook/graphql/model/GraphQLPage;

    invoke-virtual {v0, v1}, Lcom/facebook/java2js/JSValue;->to(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPage;

    iput-object v0, p0, LX/Gdj;->c:Lcom/facebook/graphql/model/GraphQLPage;

    .line 2374510
    return-void
.end method
