.class public final LX/F3F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;)V
    .locals 0

    .prologue
    .line 2193900
    iput-object p1, p0, LX/F3F;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2193901
    new-instance v0, LX/F3q;

    invoke-direct {v0}, LX/F3q;-><init>()V

    move-object v0, v0

    .line 2193902
    const-string v1, "group_id"

    iget-object v2, p0, LX/F3F;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;

    .line 2193903
    iget-object v3, v2, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v2, v3

    .line 2193904
    const-string v3, "group_feed_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "purpose_pog_size"

    iget-object v3, p0, LX/F3F;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b20f8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v2, "purpose_default_cover_photo_size"

    iget-object v3, p0, LX/F3F;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b20f8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2193905
    iget-object v1, p0, LX/F3F;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;

    iget-object v1, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditSettingsFragment;->d:LX/0tX;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v2, LX/0zS;->d:LX/0zS;

    invoke-virtual {v0, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    const/4 v2, 0x1

    .line 2193906
    iput-boolean v2, v0, LX/0zO;->p:Z

    .line 2193907
    move-object v0, v0

    .line 2193908
    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    return-object v0
.end method
