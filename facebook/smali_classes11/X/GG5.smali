.class public final enum LX/GG5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GG5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GG5;

.field public static final enum CONTINUOUS:LX/GG5;

.field public static final enum SPECIFIC_DATE:LX/GG5;


# instance fields
.field private mDuration:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2332663
    new-instance v0, LX/GG5;

    const-string v1, "CONTINUOUS"

    invoke-direct {v0, v1, v3, v3}, LX/GG5;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/GG5;->CONTINUOUS:LX/GG5;

    .line 2332664
    new-instance v0, LX/GG5;

    const-string v1, "SPECIFIC_DATE"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v4, v2}, LX/GG5;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/GG5;->SPECIFIC_DATE:LX/GG5;

    .line 2332665
    const/4 v0, 0x2

    new-array v0, v0, [LX/GG5;

    sget-object v1, LX/GG5;->CONTINUOUS:LX/GG5;

    aput-object v1, v0, v3

    sget-object v1, LX/GG5;->SPECIFIC_DATE:LX/GG5;

    aput-object v1, v0, v4

    sput-object v0, LX/GG5;->$VALUES:[LX/GG5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2332660
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2332661
    iput p3, p0, LX/GG5;->mDuration:I

    .line 2332662
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GG5;
    .locals 1

    .prologue
    .line 2332656
    const-class v0, LX/GG5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GG5;

    return-object v0
.end method

.method public static values()[LX/GG5;
    .locals 1

    .prologue
    .line 2332659
    sget-object v0, LX/GG5;->$VALUES:[LX/GG5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GG5;

    return-object v0
.end method


# virtual methods
.method public final getDuration()I
    .locals 1

    .prologue
    .line 2332658
    iget v0, p0, LX/GG5;->mDuration:I

    return v0
.end method

.method public final isFixedDuration()Z
    .locals 1

    .prologue
    .line 2332657
    iget v0, p0, LX/GG5;->mDuration:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
