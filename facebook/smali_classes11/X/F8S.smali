.class public LX/F8S;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/9Tk;

.field public final b:LX/2dj;

.field public final c:LX/F6j;

.field public final d:LX/1Ck;

.field public final e:LX/0kL;

.field public final f:LX/0Tf;

.field public final g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;>;"
        }
    .end annotation
.end field

.field public final h:LX/89v;

.field private final i:LX/1OM;


# direct methods
.method public constructor <init>(LX/9Tk;LX/2dj;LX/F6j;LX/1Ck;LX/0kL;LX/0Tf;LX/89v;LX/1OM;)V
    .locals 1
    .param p6    # LX/0Tf;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p7    # LX/89v;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # LX/1OM;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2204093
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2204094
    iput-object p1, p0, LX/F8S;->a:LX/9Tk;

    .line 2204095
    iput-object p2, p0, LX/F8S;->b:LX/2dj;

    .line 2204096
    iput-object p3, p0, LX/F8S;->c:LX/F6j;

    .line 2204097
    iput-object p4, p0, LX/F8S;->d:LX/1Ck;

    .line 2204098
    iput-object p5, p0, LX/F8S;->e:LX/0kL;

    .line 2204099
    iput-object p6, p0, LX/F8S;->f:LX/0Tf;

    .line 2204100
    iput-object p7, p0, LX/F8S;->h:LX/89v;

    .line 2204101
    iput-object p8, p0, LX/F8S;->i:LX/1OM;

    .line 2204102
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/F8S;->g:Ljava/util/Map;

    .line 2204103
    return-void
.end method

.method public static a$redex0(LX/F8S;LX/F8P;LX/F8O;)V
    .locals 1

    .prologue
    .line 2204104
    iput-object p2, p1, LX/F8P;->e:LX/F8O;

    .line 2204105
    iget-object v0, p0, LX/F8S;->i:LX/1OM;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2204106
    return-void
.end method


# virtual methods
.method public final a(LX/F8P;)V
    .locals 11

    .prologue
    .line 2204107
    sget-object v0, LX/F8R;->a:[I

    .line 2204108
    iget-object v1, p1, LX/F8P;->e:LX/F8O;

    move-object v1, v1

    .line 2204109
    invoke-virtual {v1}, LX/F8O;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2204110
    :goto_0
    return-void

    .line 2204111
    :pswitch_0
    iget-wide v9, p1, LX/F8P;->a:J

    move-wide v2, v9

    .line 2204112
    iget-object v4, p0, LX/F8S;->g:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2204113
    :goto_1
    sget-object v0, LX/F8O;->PENDING_CAN_UNDO:LX/F8O;

    invoke-static {p0, p1, v0}, LX/F8S;->a$redex0(LX/F8S;LX/F8P;LX/F8O;)V

    goto :goto_0

    .line 2204114
    :pswitch_1
    iget-wide v6, p1, LX/F8P;->a:J

    move-wide v4, v6

    .line 2204115
    iget-object v2, p0, LX/F8S;->g:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2204116
    iget-object v2, p0, LX/F8S;->g:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2204117
    if-eqz v2, :cond_0

    .line 2204118
    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    .line 2204119
    :cond_0
    iget-object v2, p0, LX/F8S;->g:Ljava/util/Map;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2204120
    iget-object v2, p0, LX/F8S;->a:LX/9Tk;

    iget-object v3, p0, LX/F8S;->h:LX/89v;

    iget-object v3, v3, LX/89v;->value:Ljava/lang/String;

    sget-object v4, LX/9Ti;->INVITABLE_CONTACTS_API:LX/9Ti;

    invoke-virtual {v2, v3, v4}, LX/9Tk;->b(Ljava/lang/String;LX/9Ti;)V

    .line 2204121
    :cond_1
    sget-object v2, LX/F8O;->UNINVITED:LX/F8O;

    invoke-static {p0, p1, v2}, LX/F8S;->a$redex0(LX/F8S;LX/F8P;LX/F8O;)V

    .line 2204122
    goto :goto_0

    .line 2204123
    :cond_2
    iget-object v4, p0, LX/F8S;->f:LX/0Tf;

    new-instance v5, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsController$1;

    invoke-direct {v5, p0, v2, v3, p1}, Lcom/facebook/growth/friendfinder/invitablecontacts/InvitableContactsController$1;-><init>(LX/F8S;JLX/F8P;)V

    const-wide/16 v6, 0x4

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v4, v5, v6, v7, v8}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v4

    .line 2204124
    iget-object v5, p0, LX/F8S;->g:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v5, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2204125
    iget-object v2, p0, LX/F8S;->a:LX/9Tk;

    iget-object v3, p0, LX/F8S;->h:LX/89v;

    iget-object v3, v3, LX/89v;->value:Ljava/lang/String;

    sget-object v4, LX/9Ti;->INVITABLE_CONTACTS_API:LX/9Ti;

    invoke-virtual {v2, v3, v4}, LX/9Tk;->a(Ljava/lang/String;LX/9Ti;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
