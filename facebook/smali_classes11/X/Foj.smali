.class public final LX/Foj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V
    .locals 0

    .prologue
    .line 2290087
    iput-object p1, p0, LX/Foj;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .prologue
    .line 2290089
    iget-object v0, p0, LX/Foj;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->t:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 2290090
    iget-object v0, p0, LX/Foj;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->u:Landroid/os/Handler;

    iget-object v1, p0, LX/Foj;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->t:Ljava/lang/Runnable;

    invoke-static {v0, v1}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 2290091
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2290092
    iget-object v0, p0, LX/Foj;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-boolean v0, v0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->d:Z

    if-eqz v0, :cond_2

    .line 2290093
    iget-object v0, p0, LX/Foj;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-static {v0}, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->q(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V

    .line 2290094
    :goto_0
    iget-object v0, p0, LX/Foj;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    const/4 v1, 0x0

    .line 2290095
    iput-boolean v1, v0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->d:Z

    .line 2290096
    :cond_1
    :goto_1
    return-void

    .line 2290097
    :cond_2
    iget-object v0, p0, LX/Foj;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-static {v0}, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->p(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V

    goto :goto_0

    .line 2290098
    :cond_3
    iget-object v0, p0, LX/Foj;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->q:Lcom/facebook/graphql/enums/GraphQLCharityCategoryEnum;

    if-nez v0, :cond_1

    .line 2290099
    iget-object v0, p0, LX/Foj;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-static {v0}, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->o$redex0(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V

    .line 2290100
    iget-object v0, p0, LX/Foj;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    new-instance v1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment$4$1;

    invoke-direct {v1, p0}, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment$4$1;-><init>(LX/Foj;)V

    .line 2290101
    iput-object v1, v0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->t:Ljava/lang/Runnable;

    .line 2290102
    iget-object v0, p0, LX/Foj;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->u:Landroid/os/Handler;

    iget-object v1, p0, LX/Foj;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->t:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    const v4, -0x628bebc4

    invoke-static {v0, v1, v2, v3, v4}, LX/03a;->b(Landroid/os/Handler;Ljava/lang/Runnable;JI)Z

    goto :goto_1
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2290103
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2290088
    return-void
.end method
