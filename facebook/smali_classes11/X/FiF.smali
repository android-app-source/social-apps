.class public LX/FiF;
.super LX/Fi8;
.source ""


# instance fields
.field public final a:LX/FgS;

.field public final b:LX/0W9;

.field public final c:Z

.field public final d:I

.field public final e:I


# direct methods
.method public constructor <init>(LX/FgS;LX/0W9;LX/0ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2274843
    invoke-direct {p0}, LX/Fi8;-><init>()V

    .line 2274844
    iput-object p1, p0, LX/FiF;->a:LX/FgS;

    .line 2274845
    iput-object p2, p0, LX/FiF;->b:LX/0W9;

    .line 2274846
    sget-short v0, LX/100;->bC:S

    const/4 v1, 0x0

    invoke-interface {p3, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    iput-boolean v0, p0, LX/FiF;->c:Z

    .line 2274847
    sget v0, LX/100;->bL:I

    const/4 v1, 0x6

    invoke-interface {p3, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/FiF;->d:I

    .line 2274848
    sget v0, LX/100;->bD:I

    const/4 v1, 0x2

    invoke-interface {p3, v0, v1}, LX/0ad;->a(II)I

    move-result v0

    iput v0, p0, LX/FiF;->e:I

    .line 2274849
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/search/api/GraphSearchQuery;LX/7Hc;Lcom/facebook/search/model/TypeaheadUnit;LX/7HZ;)LX/0Px;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            "LX/7Hc",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            "LX/7HZ;",
            ")",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2274806
    invoke-static {}, LX/4z1;->u()LX/4z1;

    move-result-object v4

    .line 2274807
    invoke-static {}, LX/4z1;->u()LX/4z1;

    move-result-object v5

    .line 2274808
    iget-object v0, p2, LX/7Hc;->b:LX/0Px;

    move-object v6, v0

    .line 2274809
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v3, v2

    :goto_0
    if-ge v3, v7, :cond_3

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/TypeaheadUnit;

    .line 2274810
    instance-of v1, v0, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 2274811
    check-cast v1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-static {v1}, LX/CwH;->a(Lcom/facebook/search/model/KeywordTypeaheadUnit;)LX/CwH;

    move-result-object v1

    invoke-virtual {v1}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v1

    .line 2274812
    check-cast v0, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    invoke-virtual {v0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0, v1}, LX/0Xs;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    .line 2274813
    :cond_0
    :goto_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 2274814
    :cond_1
    instance-of v1, v0, Lcom/facebook/search/model/EntityTypeaheadUnit;

    if-eqz v1, :cond_2

    move-object v1, v0

    check-cast v1, Lcom/facebook/search/model/EntityTypeaheadUnit;

    .line 2274815
    iget-boolean v8, v1, Lcom/facebook/search/model/EntityTypeaheadUnit;->m:Z

    move v1, v8

    .line 2274816
    if-eqz v1, :cond_2

    invoke-virtual {v5}, LX/0Xs;->f()I

    move-result v1

    iget v8, p0, LX/FiF;->e:I

    if-ge v1, v8, :cond_2

    move-object v1, v0

    .line 2274817
    check-cast v1, Lcom/facebook/search/model/EntityTypeaheadUnit;

    .line 2274818
    iget-object v8, v1, Lcom/facebook/search/model/EntityTypeaheadUnit;->b:Ljava/lang/String;

    move-object v1, v8

    .line 2274819
    iget-object v8, p0, LX/FiF;->b:LX/0W9;

    invoke-virtual {v8}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1, v0}, LX/0Xs;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_1

    .line 2274820
    :cond_2
    instance-of v1, v0, Lcom/facebook/search/model/EntityTypeaheadUnit;

    if-eqz v1, :cond_0

    .line 2274821
    check-cast v0, Lcom/facebook/search/model/EntityTypeaheadUnit;

    .line 2274822
    iget-object v1, p0, LX/FiF;->b:LX/0W9;

    invoke-virtual {v1}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v1

    .line 2274823
    iget-object v8, v0, Lcom/facebook/search/model/EntityTypeaheadUnit;->b:Ljava/lang/String;

    move-object v8, v8

    .line 2274824
    invoke-virtual {v8, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    .line 2274825
    invoke-static {v0, v1}, LX/FiB;->a(Lcom/facebook/search/model/EntityTypeaheadUnit;Ljava/util/Locale;)Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    invoke-virtual {v4, v8, v0}, LX/0Xs;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    goto :goto_1

    .line 2274826
    :cond_3
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2274827
    iget-boolean v0, p0, LX/FiF;->c:Z

    if-eqz v0, :cond_4

    .line 2274828
    invoke-virtual {v5}, LX/0Xt;->i()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2274829
    :cond_4
    invoke-virtual {v4}, LX/0Xt;->i()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :cond_5
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    .line 2274830
    iget v2, p0, LX/FiF;->d:I

    invoke-virtual {v5}, LX/0Xt;->i()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->size()I

    move-result v6

    sub-int/2addr v2, v6

    if-eq v1, v2, :cond_6

    .line 2274831
    invoke-virtual {v0}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/0Xs;->f(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2274832
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2274833
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 2274834
    goto :goto_2

    .line 2274835
    :cond_6
    iget-boolean v0, p0, LX/FiF;->c:Z

    if-nez v0, :cond_7

    .line 2274836
    invoke-virtual {v5}, LX/0Xt;->i()Ljava/util/Collection;

    move-result-object v0

    invoke-virtual {v3, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2274837
    :cond_7
    iget-object v0, p0, LX/FiF;->a:LX/FgS;

    invoke-virtual {v0}, LX/FgS;->c()Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/CwI;->ESCAPE:LX/CwI;

    invoke-static {v0, v1}, LX/CwH;->a(Ljava/lang/String;LX/CwI;)LX/CwH;

    move-result-object v0

    sget-object v1, LX/CwF;->escape:LX/CwF;

    .line 2274838
    iput-object v1, v0, LX/CwH;->g:LX/CwF;

    .line 2274839
    move-object v0, v0

    .line 2274840
    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    move-object v0, v0

    .line 2274841
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2274842
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/facebook/search/model/TypeaheadUnit;
    .locals 2

    .prologue
    .line 2274805
    iget-object v0, p0, LX/FiF;->a:LX/FgS;

    invoke-virtual {v0}, LX/FgS;->c()Ljava/lang/String;

    move-result-object v0

    sget-object v1, LX/CwI;->SEARCH_BUTTON:LX/CwI;

    invoke-static {v0, v1}, LX/CwH;->a(Ljava/lang/String;LX/CwI;)LX/CwH;

    move-result-object v0

    invoke-virtual {v0}, LX/CwH;->c()Lcom/facebook/search/model/KeywordTypeaheadUnit;

    move-result-object v0

    return-object v0
.end method
