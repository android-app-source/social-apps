.class public LX/GGl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GGc;
.implements LX/GGk;


# instance fields
.field public i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation
.end field

.field private final j:LX/GGX;

.field public final k:LX/GGX;

.field public final l:LX/GDk;

.field public final m:LX/GDp;

.field private final n:LX/GCB;

.field public o:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public p:LX/GF4;

.field public q:Z


# direct methods
.method public constructor <init>(LX/GF4;LX/GDk;LX/GDp;LX/GCB;LX/GLK;LX/GIT;LX/GIO;LX/GIZ;)V
    .locals 5
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2334528
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2334529
    new-instance v0, LX/GGe;

    invoke-direct {v0, p0}, LX/GGe;-><init>(LX/GGl;)V

    iput-object v0, p0, LX/GGl;->j:LX/GGX;

    .line 2334530
    new-instance v0, LX/GGf;

    invoke-direct {v0, p0}, LX/GGf;-><init>(LX/GGl;)V

    iput-object v0, p0, LX/GGl;->k:LX/GGX;

    .line 2334531
    iput-object p2, p0, LX/GGl;->l:LX/GDk;

    .line 2334532
    iput-object p3, p0, LX/GGl;->m:LX/GDp;

    .line 2334533
    iput-object p4, p0, LX/GGl;->n:LX/GCB;

    .line 2334534
    iput-object p1, p0, LX/GGl;->p:LX/GF4;

    .line 2334535
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    new-instance v1, LX/GEZ;

    const v2, 0x7f03005a

    sget-object v3, LX/GGl;->a:LX/GGX;

    sget-object v4, LX/8wK;->ERROR_CARD:LX/8wK;

    invoke-direct {v1, v2, p8, v3, v4}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/GEZ;

    const v2, 0x7f030067

    iget-object v3, p0, LX/GGl;->j:LX/GGX;

    sget-object v4, LX/8wK;->INFO_CARD:LX/8wK;

    invoke-direct {v1, v2, p7, v3, v4}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/GEZ;

    const v2, 0x7f030085

    iget-object v3, p0, LX/GGl;->k:LX/GGX;

    sget-object v4, LX/8wK;->TARGETING:LX/8wK;

    invoke-direct {v1, v2, p6, v3, v4}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/GEZ;

    const v2, 0x7f030094

    iget-object v3, p0, LX/GGl;->j:LX/GGX;

    sget-object v4, LX/8wK;->TARGETING:LX/8wK;

    invoke-direct {v1, v2, p5, v3, v4}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/GGl;->i:LX/0Px;

    .line 2334536
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Z)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 2334513
    sget-object v0, LX/8wL;->AUDIENCE_MANAGEMENT:LX/8wL;

    const v1, 0x7f080b7b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->n()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v0, v1, v2}, LX/8wJ;->a(Landroid/content/Context;LX/8wL;Ljava/lang/Integer;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2334514
    invoke-static {p1}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    move-result-object v2

    .line 2334515
    new-instance v3, LX/GGE;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    invoke-direct {v3, v0}, LX/GGE;-><init>(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    if-eqz p2, :cond_0

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2334516
    :goto_0
    iput-object v0, v3, LX/GGE;->g:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2334517
    move-object v0, v3

    .line 2334518
    const/4 v3, 0x0

    .line 2334519
    iput-object v3, v0, LX/GGE;->i:Ljava/lang/String;

    .line 2334520
    move-object v3, v0

    .line 2334521
    if-eqz p2, :cond_1

    invoke-static {p1}, LX/GG6;->m(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->o()LX/0Px;

    move-result-object v0

    .line 2334522
    :goto_1
    iput-object v0, v3, LX/GGE;->l:LX/0Px;

    .line 2334523
    move-object v0, v3

    .line 2334524
    invoke-virtual {v0}, LX/GGE;->a()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    .line 2334525
    const-string v0, "data_extra"

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2334526
    return-object v1

    .line 2334527
    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->SAVED_AUDIENCE:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->b:LX/0Px;

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2334537
    iget-object v0, p0, LX/GGl;->i:LX/0Px;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;LX/GCY;)V
    .locals 2

    .prologue
    .line 2334509
    const-string v0, "data_extra"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iput-object v0, p0, LX/GGl;->o:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2334510
    const-string v0, "see_all_extra"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, LX/GGl;->q:Z

    .line 2334511
    iget-object v0, p0, LX/GGl;->o:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-interface {p2, v0}, LX/GCY;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2334512
    return-void
.end method

.method public final b()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;
    .locals 1

    .prologue
    .line 2334506
    iget-boolean v0, p0, LX/GGl;->q:Z

    if-eqz v0, :cond_0

    .line 2334507
    sget-object v0, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2334508
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/GGl;->n:LX/GCB;

    invoke-virtual {v0}, LX/GCB;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()LX/GGh;
    .locals 1

    .prologue
    .line 2334505
    new-instance v0, LX/GGi;

    invoke-direct {v0, p0}, LX/GGi;-><init>(LX/GGl;)V

    return-object v0
.end method

.method public final d()V
    .locals 5

    .prologue
    .line 2334492
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "targeting_data_extra"

    iget-object v2, p0, LX/GGl;->o:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    .line 2334493
    iget-object v1, p0, LX/GGl;->o:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    .line 2334494
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v1, v2

    .line 2334495
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->FANS:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/GGl;->o:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    .line 2334496
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v1, v2

    .line 2334497
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->GROUPER:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, LX/GGl;->o:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    .line 2334498
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v1, v2

    .line 2334499
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->NCPP:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    if-eq v1, v2, :cond_0

    .line 2334500
    const-string v1, "audience_extra"

    iget-object v2, p0, LX/GGl;->o:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->v()LX/0Px;

    move-result-object v2

    iget-object v3, p0, LX/GGl;->o:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v3

    .line 2334501
    iget-object v4, v3, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v3, v4

    .line 2334502
    invoke-static {v2, v3}, LX/GG6;->b(LX/0Px;Ljava/lang/String;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2334503
    :cond_0
    iget-object v1, p0, LX/GGl;->p:LX/GF4;

    new-instance v2, LX/GFS;

    const/4 v3, 0x1

    invoke-direct {v2, v0, v3}, LX/GFS;-><init>(Landroid/content/Intent;Z)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2334504
    return-void
.end method
