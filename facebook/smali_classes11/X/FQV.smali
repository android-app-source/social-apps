.class public final enum LX/FQV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FQV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FQV;

.field public static final enum ALL:LX/FQV;

.field public static final enum MOBILE:LX/FQV;

.field public static final enum WIFI:LX/FQV;


# instance fields
.field private final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 2239401
    new-instance v0, LX/FQV;

    const-string v1, "MOBILE"

    invoke-direct {v0, v1, v4, v2}, LX/FQV;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/FQV;->MOBILE:LX/FQV;

    .line 2239402
    new-instance v0, LX/FQV;

    const-string v1, "WIFI"

    invoke-direct {v0, v1, v2, v3}, LX/FQV;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/FQV;->WIFI:LX/FQV;

    .line 2239403
    new-instance v0, LX/FQV;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v3, v5}, LX/FQV;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/FQV;->ALL:LX/FQV;

    .line 2239404
    new-array v0, v5, [LX/FQV;

    sget-object v1, LX/FQV;->MOBILE:LX/FQV;

    aput-object v1, v0, v4

    sget-object v1, LX/FQV;->WIFI:LX/FQV;

    aput-object v1, v0, v2

    sget-object v1, LX/FQV;->ALL:LX/FQV;

    aput-object v1, v0, v3

    sput-object v0, LX/FQV;->$VALUES:[LX/FQV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2239413
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2239414
    iput p3, p0, LX/FQV;->value:I

    .line 2239415
    return-void
.end method

.method public static fromInt(I)LX/FQV;
    .locals 5

    .prologue
    .line 2239408
    invoke-static {}, LX/FQV;->values()[LX/FQV;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 2239409
    iget v4, v0, LX/FQV;->value:I

    if-ne v4, p0, :cond_0

    .line 2239410
    :goto_1
    return-object v0

    .line 2239411
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2239412
    :cond_1
    sget-object v0, LX/FQV;->WIFI:LX/FQV;

    goto :goto_1
.end method

.method public static getDefault()LX/FQV;
    .locals 1

    .prologue
    .line 2239416
    sget-object v0, LX/FQV;->WIFI:LX/FQV;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/FQV;
    .locals 1

    .prologue
    .line 2239407
    const-class v0, LX/FQV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FQV;

    return-object v0
.end method

.method public static values()[LX/FQV;
    .locals 1

    .prologue
    .line 2239406
    sget-object v0, LX/FQV;->$VALUES:[LX/FQV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FQV;

    return-object v0
.end method


# virtual methods
.method public final asInt()I
    .locals 1

    .prologue
    .line 2239405
    iget v0, p0, LX/FQV;->value:I

    return v0
.end method
