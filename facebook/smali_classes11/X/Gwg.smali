.class public LX/Gwg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/Gwi;

.field private final c:LX/Gwq;


# direct methods
.method public constructor <init>(LX/0Or;LX/Gwi;LX/Gwq;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/Gwi;",
            "LX/Gwq;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2407241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2407242
    iput-object p1, p0, LX/Gwg;->a:LX/0Or;

    .line 2407243
    iput-object p2, p0, LX/Gwg;->b:LX/Gwi;

    .line 2407244
    iput-object p3, p0, LX/Gwg;->c:LX/Gwq;

    .line 2407245
    return-void
.end method

.method public static b(LX/0QB;)LX/Gwg;
    .locals 6

    .prologue
    .line 2407246
    new-instance v2, LX/Gwg;

    const/16 v0, 0xb83

    invoke-static {p0, v0}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v3

    .line 2407247
    new-instance v1, LX/Gwi;

    invoke-static {p0}, LX/0q9;->a(LX/0QB;)LX/0lp;

    move-result-object v0

    check-cast v0, LX/0lp;

    invoke-direct {v1, v0}, LX/Gwi;-><init>(LX/0lp;)V

    .line 2407248
    move-object v0, v1

    .line 2407249
    check-cast v0, LX/Gwi;

    .line 2407250
    new-instance v5, LX/Gwq;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-direct {v5, v1, v4}, LX/Gwq;-><init>(Landroid/content/Context;Ljava/util/concurrent/ExecutorService;)V

    .line 2407251
    move-object v1, v5

    .line 2407252
    check-cast v1, LX/Gwq;

    invoke-direct {v2, v3, v0, v1}, LX/Gwg;-><init>(LX/0Or;LX/Gwi;LX/Gwq;)V

    .line 2407253
    return-object v2
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2407254
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2407255
    const-string v2, "fetchFwComponents"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2407256
    :try_start_0
    iget-object v0, p0, LX/Gwg;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/11H;

    iget-object v2, p0, LX/Gwg;->b:LX/Gwi;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2407257
    :goto_0
    iget-object v1, p0, LX/Gwg;->c:LX/Gwq;

    .line 2407258
    iget-object v2, v1, LX/Gwq;->a:Ljava/util/concurrent/ExecutorService;

    new-instance v3, Lcom/facebook/katana/webview/AsyncFacewebComponentsStoreSerialization$1;

    invoke-direct {v3, v1, v0}, Lcom/facebook/katana/webview/AsyncFacewebComponentsStoreSerialization$1;-><init>(LX/Gwq;Lcom/facebook/katana/features/faceweb/FacewebComponentsStore;)V

    const p0, -0x4086c946

    invoke-static {v2, v3, p0}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2407259
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v1, v0

    .line 2407260
    :cond_0
    return-object v1

    .line 2407261
    :catch_0
    move-object v0, v1

    goto :goto_0
.end method
