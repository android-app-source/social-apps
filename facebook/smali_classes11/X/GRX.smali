.class public LX/GRX;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/GRZ;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2351581
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2351582
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, LX/GRX;->a:Ljava/util/ArrayList;

    .line 2351583
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2351584
    iget-object v0, p0, LX/GRX;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2351576
    iget-object v0, p0, LX/GRX;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, LX/GRX;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GRZ;

    .line 2351577
    invoke-virtual {v0, v2}, LX/GRZ;->b(I)Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->c()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2351578
    iget-object v4, v0, LX/GRZ;->b:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 2351579
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2351580
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2351574
    iget-object v0, p0, LX/GRX;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2351575
    return-void
.end method
