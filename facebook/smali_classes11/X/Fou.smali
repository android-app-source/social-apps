.class public final LX/Fou;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Foy;


# direct methods
.method public constructor <init>(LX/Foy;)V
    .locals 0

    .prologue
    .line 2290440
    iput-object p1, p0, LX/Fou;->a:LX/Foy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2290439
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2290432
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2290433
    if-eqz p1, :cond_0

    .line 2290434
    iget-object v0, p0, LX/Fou;->a:LX/Foy;

    iget-object v1, v0, LX/Foy;->e:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    .line 2290435
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2290436
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;

    .line 2290437
    iget-object p0, v1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->a:LX/Foq;

    invoke-virtual {p0, v0}, LX/Foq;->a(Lcom/facebook/socialgood/protocol/FundraiserCreationContentModels$FundraiserCreationContentQueryModel;)V

    .line 2290438
    :cond_0
    return-void
.end method
