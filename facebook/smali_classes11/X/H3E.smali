.class public final LX/H3E;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 2420611
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_6

    .line 2420612
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2420613
    :goto_0
    return v1

    .line 2420614
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2420615
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_5

    .line 2420616
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2420617
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2420618
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_1

    if-eqz v5, :cond_1

    .line 2420619
    const-string v6, "bounds"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2420620
    invoke-static {p0, p1}, LX/H2X;->a(LX/15w;LX/186;)I

    move-result v4

    goto :goto_1

    .line 2420621
    :cond_2
    const-string v6, "results_title"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2420622
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2420623
    :cond_3
    const-string v6, "suggestion_text"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 2420624
    invoke-static {p0, p1}, LX/4an;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2420625
    :cond_4
    const-string v6, "topic"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2420626
    invoke-static {p0, p1}, LX/H3D;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2420627
    :cond_5
    const/4 v5, 0x4

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2420628
    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2420629
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2420630
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2420631
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2420632
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_6
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    .line 2420633
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2420634
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2420635
    if-eqz v0, :cond_0

    .line 2420636
    const-string v1, "bounds"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2420637
    invoke-static {p0, v0, p2}, LX/H2X;->a(LX/15i;ILX/0nX;)V

    .line 2420638
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2420639
    if-eqz v0, :cond_1

    .line 2420640
    const-string v1, "results_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2420641
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 2420642
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2420643
    if-eqz v0, :cond_2

    .line 2420644
    const-string v1, "suggestion_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2420645
    invoke-static {p0, v0, p2}, LX/4an;->a(LX/15i;ILX/0nX;)V

    .line 2420646
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2420647
    if-eqz v0, :cond_3

    .line 2420648
    const-string v1, "topic"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2420649
    invoke-static {p0, v0, p2, p3}, LX/H3D;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2420650
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2420651
    return-void
.end method
