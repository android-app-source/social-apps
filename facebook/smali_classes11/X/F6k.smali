.class public LX/F6k;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2200799
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2200800
    iput-object p1, p0, LX/F6k;->a:LX/0Zb;

    .line 2200801
    return-void
.end method


# virtual methods
.method public final a(IILjava/lang/String;J)V
    .locals 4

    .prologue
    .line 2200802
    iget-object v0, p0, LX/F6k;->a:LX/0Zb;

    new-instance v1, Lcom/facebook/growth/contactimporter/FindFriendsAnalyticsEvent;

    const-string v2, "invite"

    invoke-direct {v1, v2}, Lcom/facebook/growth/contactimporter/FindFriendsAnalyticsEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "submitted"

    invoke-virtual {v1, v2}, Lcom/facebook/growth/contactimporter/FindFriendsAnalyticsEvent;->m(Ljava/lang/String;)Lcom/facebook/growth/contactimporter/FindFriendsAnalyticsEvent;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, p4

    .line 2200803
    const-string p0, "delta_t"

    invoke-virtual {v1, p0, v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;J)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2200804
    move-object v1, v1

    .line 2200805
    invoke-virtual {v1, p2}, Lcom/facebook/growth/contactimporter/FindFriendsAnalyticsEvent;->a(I)Lcom/facebook/growth/contactimporter/FindFriendsAnalyticsEvent;

    move-result-object v1

    .line 2200806
    const-string v2, "invites_sent"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;I)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2200807
    move-object v1, v1

    .line 2200808
    const-string v2, "submit_type"

    invoke-virtual {v1, v2, p3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2200809
    move-object v1, v1

    .line 2200810
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2200811
    return-void
.end method
