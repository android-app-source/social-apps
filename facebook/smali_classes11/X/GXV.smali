.class public final LX/GXV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;


# direct methods
.method public constructor <init>(Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;)V
    .locals 0

    .prologue
    .line 2364160
    iput-object p1, p0, LX/GXV;->a:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 13

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x44d0f00b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2364161
    iget-object v1, p0, LX/GXV;->a:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    iget-object v1, v1, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->k:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    if-nez v1, :cond_0

    .line 2364162
    const v1, -0x70fdaac2

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2364163
    :goto_0
    return-void

    .line 2364164
    :cond_0
    invoke-static {p1}, LX/1OR;->d(Landroid/view/View;)I

    move-result v1

    .line 2364165
    iget-object v2, p0, LX/GXV;->a:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    iget-object v2, v2, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->f:LX/GXp;

    invoke-virtual {v2, v1}, LX/GXp;->a(I)I

    move-result v2

    .line 2364166
    iget-object v3, p0, LX/GXV;->a:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    iget-object v3, v3, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->f:LX/GXp;

    invoke-virtual {v3, v1}, LX/GXp;->b(I)I

    move-result v1

    .line 2364167
    const/4 v3, 0x3

    if-eq v2, v3, :cond_1

    if-ne v2, v4, :cond_3

    .line 2364168
    :cond_1
    iget-object v1, p0, LX/GXV;->a:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    iget-object v1, v1, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->k:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    .line 2364169
    invoke-static {v1}, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->o(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 2364170
    :cond_2
    :goto_1
    const v1, 0x21e573d2

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto :goto_0

    .line 2364171
    :cond_3
    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    .line 2364172
    iget-object v2, p0, LX/GXV;->a:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    iget-object v2, v2, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->h:LX/GXY;

    invoke-virtual {v2, v1}, LX/GXY;->a(I)Lcom/facebook/commerce/publishing/graphql/CommercePublishingQueryFragmentsModels$AdminCommerceProductItemModel;

    move-result-object v1

    .line 2364173
    if-eqz v1, :cond_2

    iget-object v2, p0, LX/GXV;->a:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    iget-object v2, v2, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->h:LX/GXY;

    invoke-virtual {v2, v1}, LX/GXY;->b(LX/7ja;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2364174
    iget-object v2, p0, LX/GXV;->a:Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;

    iget-object v2, v2, Lcom/facebook/commerce/publishing/adapter/AdminShopAdapter;->k:Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;

    .line 2364175
    invoke-static {v2}, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->o(Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;)Z

    move-result v5

    if-nez v5, :cond_5

    .line 2364176
    :goto_2
    goto :goto_1

    .line 2364177
    :cond_4
    iget-object v6, v1, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->a:LX/7j6;

    iget-wide v7, v1, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->n:J

    iget-object v5, v1, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->q:LX/GXY;

    invoke-virtual {v5}, LX/GXY;->b()Ljava/util/Currency;

    move-result-object v9

    iget-object v5, v1, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->q:LX/GXY;

    invoke-virtual {v5}, LX/GXY;->c()I

    move-result v10

    iget-object v5, v1, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->h:LX/8Fe;

    .line 2364178
    iget-object v11, v5, LX/8Fe;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v11, v11

    .line 2364179
    iget-object v5, v1, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->q:LX/GXY;

    invoke-virtual {v5}, LX/GXY;->g()I

    move-result v12

    invoke-virtual/range {v6 .. v12}, LX/7j6;->a(JLjava/util/Currency;ILcom/facebook/auth/viewercontext/ViewerContext;I)V

    goto :goto_1

    .line 2364180
    :cond_5
    iget-object v6, v2, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->a:LX/7j6;

    iget-wide v7, v2, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->n:J

    iget-object v5, v2, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->q:LX/GXY;

    invoke-virtual {v5}, LX/GXY;->b()Ljava/util/Currency;

    move-result-object v9

    iget-object v5, v2, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->q:LX/GXY;

    invoke-virtual {v5}, LX/GXY;->c()I

    move-result v11

    iget-object v5, v2, Lcom/facebook/commerce/publishing/fragments/AdminEditShopFragment;->h:LX/8Fe;

    .line 2364181
    iget-object v10, v5, LX/8Fe;->f:Lcom/facebook/auth/viewercontext/ViewerContext;

    move-object v12, v10

    .line 2364182
    move-object v10, v1

    invoke-virtual/range {v6 .. v12}, LX/7j6;->a(JLjava/util/Currency;LX/7ja;ILcom/facebook/auth/viewercontext/ViewerContext;)V

    goto :goto_2
.end method
