.class public final LX/G9O;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2322510
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(LX/G9x;III)LX/G96;
    .locals 12

    .prologue
    .line 2322511
    iget-object v0, p0, LX/G9x;->e:LX/G9s;

    move-object v5, v0

    .line 2322512
    if-nez v5, :cond_0

    .line 2322513
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 2322514
    :cond_0
    iget v0, v5, LX/G9s;->b:I

    move v6, v0

    .line 2322515
    iget v0, v5, LX/G9s;->c:I

    move v7, v0

    .line 2322516
    mul-int/lit8 v0, p3, 0x2

    add-int/2addr v0, v6

    .line 2322517
    mul-int/lit8 v1, p3, 0x2

    add-int/2addr v1, v7

    .line 2322518
    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 2322519
    invoke-static {p2, v1}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 2322520
    div-int v0, v2, v0

    div-int v1, v3, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 2322521
    mul-int v0, v6, v8

    sub-int v0, v2, v0

    div-int/lit8 v1, v0, 0x2

    .line 2322522
    mul-int v0, v7, v8

    sub-int v0, v3, v0

    div-int/lit8 v0, v0, 0x2

    .line 2322523
    new-instance v9, LX/G96;

    invoke-direct {v9, v2, v3}, LX/G96;-><init>(II)V

    .line 2322524
    const/4 v2, 0x0

    move v3, v0

    move v4, v2

    :goto_0
    if-ge v4, v7, :cond_3

    .line 2322525
    const/4 v0, 0x0

    move v2, v0

    move v0, v1

    :goto_1
    if-ge v2, v6, :cond_2

    .line 2322526
    invoke-virtual {v5, v2, v4}, LX/G9s;->a(II)B

    move-result v10

    const/4 v11, 0x1

    if-ne v10, v11, :cond_1

    .line 2322527
    invoke-virtual {v9, v0, v3, v8, v8}, LX/G96;->a(IIII)V

    .line 2322528
    :cond_1
    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v8

    goto :goto_1

    .line 2322529
    :cond_2
    add-int/lit8 v2, v4, 0x1

    add-int v0, v3, v8

    move v3, v0

    move v4, v2

    goto :goto_0

    .line 2322530
    :cond_3
    return-object v9
.end method

.method public static a(Ljava/lang/String;LX/G8o;IILjava/util/Map;)LX/G96;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/G8o;",
            "II",
            "Ljava/util/Map",
            "<",
            "LX/G8u;",
            "*>;)",
            "LX/G96;"
        }
    .end annotation

    .prologue
    .line 2322531
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2322532
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Found empty contents"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2322533
    :cond_0
    sget-object v0, LX/G8o;->QR_CODE:LX/G8o;

    if-eq p1, v0, :cond_1

    .line 2322534
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can only encode QR_CODE, but got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2322535
    :cond_1
    if-ltz p2, :cond_2

    if-gez p3, :cond_3

    .line 2322536
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Requested dimensions are too small: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const/16 v2, 0x78

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2322537
    :cond_3
    sget-object v1, LX/G9c;->L:LX/G9c;

    .line 2322538
    const/4 v2, 0x4

    .line 2322539
    if-eqz p4, :cond_9

    .line 2322540
    sget-object v0, LX/G8u;->ERROR_CORRECTION:LX/G8u;

    invoke-interface {p4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G9c;

    .line 2322541
    if-eqz v0, :cond_4

    move-object v1, v0

    .line 2322542
    :cond_4
    sget-object v0, LX/G8u;->MARGIN:LX/G8u;

    invoke-interface {p4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 2322543
    if-eqz v0, :cond_9

    .line 2322544
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2322545
    :goto_0
    if-nez p4, :cond_a

    const/4 v2, 0x0

    .line 2322546
    :goto_1
    if-nez v2, :cond_5

    .line 2322547
    const-string v2, "ISO-8859-1"

    .line 2322548
    :cond_5
    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 2322549
    const-string v4, "Shift_JIS"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 2322550
    const/4 v3, 0x0

    .line 2322551
    :try_start_0
    const-string v4, "Shift_JIS"

    invoke-virtual {p0, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 2322552
    array-length v6, v5

    .line 2322553
    rem-int/lit8 v4, v6, 0x2

    if-eqz v4, :cond_13

    .line 2322554
    :cond_6
    :goto_2
    move v3, v3

    .line 2322555
    if-eqz v3, :cond_c

    sget-object v3, LX/G9e;->KANJI:LX/G9e;

    .line 2322556
    :goto_3
    move-object v3, v3

    .line 2322557
    new-instance v4, LX/G95;

    invoke-direct {v4}, LX/G95;-><init>()V

    .line 2322558
    sget-object v5, LX/G9e;->BYTE:LX/G9e;

    if-ne v3, v5, :cond_7

    const-string v5, "ISO-8859-1"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_7

    .line 2322559
    invoke-static {v2}, LX/G98;->getCharacterSetECIByName(Ljava/lang/String;)LX/G98;

    move-result-object v5

    .line 2322560
    if-eqz v5, :cond_7

    .line 2322561
    sget-object v6, LX/G9e;->ECI:LX/G9e;

    invoke-virtual {v6}, LX/G9e;->getBits()I

    move-result v6

    const/4 p1, 0x4

    invoke-virtual {v4, v6, p1}, LX/G95;->a(II)V

    .line 2322562
    invoke-virtual {v5}, LX/G98;->getValue()I

    move-result v6

    const/16 p1, 0x8

    invoke-virtual {v4, v6, p1}, LX/G95;->a(II)V

    .line 2322563
    :cond_7
    invoke-virtual {v3}, LX/G9e;->getBits()I

    move-result v5

    const/4 v6, 0x4

    invoke-virtual {v4, v5, v6}, LX/G95;->a(II)V

    .line 2322564
    new-instance v5, LX/G95;

    invoke-direct {v5}, LX/G95;-><init>()V

    .line 2322565
    invoke-static {p0, v3, v5, v2}, LX/G9u;->a(Ljava/lang/String;LX/G9e;LX/G95;Ljava/lang/String;)V

    .line 2322566
    iget v2, v4, LX/G95;->b:I

    move v2, v2

    .line 2322567
    const/4 v6, 0x1

    .line 2322568
    invoke-static {v6}, LX/G9i;->b(I)LX/G9i;

    move-result-object v6

    invoke-virtual {v3, v6}, LX/G9e;->getCharacterCountBits(LX/G9i;)I

    move-result v6

    add-int/2addr v2, v6

    .line 2322569
    iget v6, v5, LX/G95;->b:I

    move v6, v6

    .line 2322570
    add-int/2addr v2, v6

    .line 2322571
    invoke-static {v2, v1}, LX/G9u;->a(ILX/G9c;)LX/G9i;

    move-result-object v2

    .line 2322572
    iget v6, v4, LX/G95;->b:I

    move v6, v6

    .line 2322573
    invoke-virtual {v3, v2}, LX/G9e;->getCharacterCountBits(LX/G9i;)I

    move-result v2

    add-int/2addr v2, v6

    .line 2322574
    iget v6, v5, LX/G95;->b:I

    move v6, v6

    .line 2322575
    add-int/2addr v2, v6

    .line 2322576
    invoke-static {v2, v1}, LX/G9u;->a(ILX/G9c;)LX/G9i;

    move-result-object v6

    .line 2322577
    new-instance p1, LX/G95;

    invoke-direct {p1}, LX/G95;-><init>()V

    .line 2322578
    invoke-virtual {p1, v4}, LX/G95;->a(LX/G95;)V

    .line 2322579
    sget-object v2, LX/G9e;->BYTE:LX/G9e;

    if-ne v3, v2, :cond_b

    invoke-virtual {v5}, LX/G95;->b()I

    move-result v2

    .line 2322580
    :goto_4
    invoke-static {v2, v6, v3, p1}, LX/G9u;->a(ILX/G9i;LX/G9e;LX/G95;)V

    .line 2322581
    invoke-virtual {p1, v5}, LX/G95;->a(LX/G95;)V

    .line 2322582
    invoke-virtual {v6, v1}, LX/G9i;->a(LX/G9c;)LX/G9h;

    move-result-object v2

    .line 2322583
    iget v4, v6, LX/G9i;->f:I

    move v4, v4

    .line 2322584
    invoke-virtual {v2}, LX/G9h;->c()I

    move-result v5

    sub-int/2addr v4, v5

    .line 2322585
    invoke-static {v4, p1}, LX/G9u;->a(ILX/G95;)V

    .line 2322586
    iget v5, v6, LX/G9i;->f:I

    move v5, v5

    .line 2322587
    invoke-virtual {v2}, LX/G9h;->b()I

    move-result v2

    .line 2322588
    invoke-static {p1, v5, v4, v2}, LX/G9u;->a(LX/G95;III)LX/G95;

    move-result-object v2

    .line 2322589
    new-instance v4, LX/G9x;

    invoke-direct {v4}, LX/G9x;-><init>()V

    .line 2322590
    iput-object v1, v4, LX/G9x;->b:LX/G9c;

    .line 2322591
    iput-object v3, v4, LX/G9x;->a:LX/G9e;

    .line 2322592
    iput-object v6, v4, LX/G9x;->c:LX/G9i;

    .line 2322593
    invoke-virtual {v6}, LX/G9i;->d()I

    move-result v3

    .line 2322594
    new-instance v5, LX/G9s;

    invoke-direct {v5, v3, v3}, LX/G9s;-><init>(II)V

    .line 2322595
    const p4, 0x7fffffff

    .line 2322596
    const/4 v3, -0x1

    .line 2322597
    const/4 p1, 0x0

    :goto_5
    const/16 p0, 0x8

    if-ge p1, p0, :cond_8

    .line 2322598
    invoke-static {v2, v1, v6, p1, v5}, LX/G9w;->a(LX/G95;LX/G9c;LX/G9i;ILX/G9s;)V

    .line 2322599
    invoke-static {v5}, LX/G9u;->a(LX/G9s;)I

    move-result p0

    .line 2322600
    if-ge p0, p4, :cond_17

    move v3, p1

    .line 2322601
    :goto_6
    add-int/lit8 p1, p1, 0x1

    move p4, p0

    goto :goto_5

    .line 2322602
    :cond_8
    move v3, v3

    .line 2322603
    iput v3, v4, LX/G9x;->d:I

    .line 2322604
    invoke-static {v2, v1, v6, v3, v5}, LX/G9w;->a(LX/G95;LX/G9c;LX/G9i;ILX/G9s;)V

    .line 2322605
    iput-object v5, v4, LX/G9x;->e:LX/G9s;

    .line 2322606
    move-object v1, v4

    .line 2322607
    invoke-static {v1, p2, p3, v0}, LX/G9O;->a(LX/G9x;III)LX/G96;

    move-result-object v0

    return-object v0

    :cond_9
    move v0, v2

    goto/16 :goto_0

    .line 2322608
    :cond_a
    sget-object v2, LX/G8u;->CHARACTER_SET:LX/G8u;

    invoke-interface {p4, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    goto/16 :goto_1

    .line 2322609
    :cond_b
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    goto :goto_4

    .line 2322610
    :cond_c
    sget-object v3, LX/G9e;->BYTE:LX/G9e;

    goto/16 :goto_3

    :cond_d
    move v4, v3

    move v5, v3

    .line 2322611
    :goto_7
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result p1

    if-ge v3, p1, :cond_10

    .line 2322612
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result p1

    .line 2322613
    const/16 p4, 0x30

    if-lt p1, p4, :cond_e

    const/16 p4, 0x39

    if-gt p1, p4, :cond_e

    move v5, v6

    .line 2322614
    :goto_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    .line 2322615
    :cond_e
    invoke-static {p1}, LX/G9u;->a(I)I

    move-result v4

    const/4 p1, -0x1

    if-eq v4, p1, :cond_f

    move v4, v6

    .line 2322616
    goto :goto_8

    .line 2322617
    :cond_f
    sget-object v3, LX/G9e;->BYTE:LX/G9e;

    goto/16 :goto_3

    .line 2322618
    :cond_10
    if-eqz v4, :cond_11

    .line 2322619
    sget-object v3, LX/G9e;->ALPHANUMERIC:LX/G9e;

    goto/16 :goto_3

    .line 2322620
    :cond_11
    if-eqz v5, :cond_12

    .line 2322621
    sget-object v3, LX/G9e;->NUMERIC:LX/G9e;

    goto/16 :goto_3

    .line 2322622
    :cond_12
    sget-object v3, LX/G9e;->BYTE:LX/G9e;

    goto/16 :goto_3

    :cond_13
    move v4, v3

    .line 2322623
    :goto_9
    if-ge v4, v6, :cond_16

    .line 2322624
    aget-byte p1, v5, v4

    and-int/lit16 p1, p1, 0xff

    .line 2322625
    const/16 p4, 0x81

    if-lt p1, p4, :cond_14

    const/16 p4, 0x9f

    if-le p1, p4, :cond_15

    :cond_14
    const/16 p4, 0xe0

    if-lt p1, p4, :cond_6

    const/16 p4, 0xeb

    if-gt p1, p4, :cond_6

    .line 2322626
    :cond_15
    add-int/lit8 v4, v4, 0x2

    goto :goto_9

    .line 2322627
    :cond_16
    const/4 v3, 0x1

    goto/16 :goto_2

    .line 2322628
    :catch_0
    goto/16 :goto_2

    :cond_17
    move p0, p4

    goto :goto_6
.end method
