.class public LX/F1R;
.super LX/1Qj;
.source ""

# interfaces
.implements LX/1Pf;


# instance fields
.field private final n:LX/1PT;

.field private final o:LX/F1t;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/1PY;LX/F1t;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Ljava/lang/Runnable;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1PY;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2192046
    invoke-direct {p0, p1, p3, p4}, LX/1Qj;-><init>(Landroid/content/Context;Ljava/lang/Runnable;LX/1PY;)V

    .line 2192047
    iput-object p2, p0, LX/F1R;->n:LX/1PT;

    .line 2192048
    iput-object p5, p0, LX/F1R;->o:LX/F1t;

    .line 2192049
    return-void
.end method


# virtual methods
.method public final c()LX/1PT;
    .locals 1

    .prologue
    .line 2192050
    iget-object v0, p0, LX/F1R;->n:LX/1PT;

    return-object v0
.end method

.method public final e()LX/1SX;
    .locals 2

    .prologue
    .line 2192051
    iget-object v0, p0, LX/F1R;->o:LX/F1t;

    .line 2192052
    iget-object v1, v0, LX/F1t;->a:LX/F26;

    invoke-virtual {v1, p0}, LX/F26;->a(LX/1Qj;)LX/F25;

    move-result-object v1

    move-object v0, v1

    .line 2192053
    return-object v0
.end method
