.class public final LX/HCI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/HCO;


# direct methods
.method public constructor <init>(LX/HCO;)V
    .locals 0

    .prologue
    .line 2438530
    iput-object p1, p0, LX/HCI;->a:LX/HCO;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x45fafa42

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2438531
    iget-object v0, p0, LX/HCI;->a:LX/HCO;

    iget-object v0, v0, LX/HCO;->c:LX/00H;

    .line 2438532
    iget-object v2, v0, LX/00H;->h:Ljava/lang/String;

    move-object v0, v2

    .line 2438533
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2438534
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2438535
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2438536
    const/high16 v0, 0x10000000

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2438537
    iget-object v0, p0, LX/HCI;->a:LX/HCO;

    iget-object v0, v0, LX/HCO;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v3, p0, LX/HCI;->a:LX/HCO;

    iget-object v3, v3, LX/HCO;->h:Landroid/content/Context;

    invoke-interface {v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2438538
    :cond_0
    const v0, -0x29f7b594

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
