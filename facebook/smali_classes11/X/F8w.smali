.class public LX/F8w;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2204831
    const-string v0, "contact_importer"

    const-string v1, "upload_profile_pic"

    const-string v2, "people_you_may_know"

    const-string v3, "native_name"

    invoke-static {v0, v1, v2, v3}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/F8w;->a:Ljava/util/List;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2204832
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/F8w;
    .locals 1

    .prologue
    .line 2204833
    new-instance v0, LX/F8w;

    invoke-direct {v0}, LX/F8w;-><init>()V

    .line 2204834
    move-object v0, v0

    .line 2204835
    return-object v0
.end method

.method public static final a(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/NuxStep;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/NuxStep;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2204836
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2204837
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/NuxStep;

    .line 2204838
    iget-object v3, v0, Lcom/facebook/ipc/model/NuxStep;->name:Ljava/lang/String;

    move-object v3, v3

    .line 2204839
    invoke-static {v3}, LX/F8w;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2204840
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2204841
    :cond_1
    return-object v1
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2204842
    sget-object v0, LX/F8w;->a:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
