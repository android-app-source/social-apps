.class public LX/FiL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/FiL;


# instance fields
.field public final a:LX/8i7;

.field public final b:LX/0Uh;


# direct methods
.method public constructor <init>(LX/8i7;LX/0Uh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2275105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2275106
    iput-object p1, p0, LX/FiL;->a:LX/8i7;

    .line 2275107
    iput-object p2, p0, LX/FiL;->b:LX/0Uh;

    .line 2275108
    return-void
.end method

.method public static a(LX/0QB;)LX/FiL;
    .locals 5

    .prologue
    .line 2275109
    sget-object v0, LX/FiL;->c:LX/FiL;

    if-nez v0, :cond_1

    .line 2275110
    const-class v1, LX/FiL;

    monitor-enter v1

    .line 2275111
    :try_start_0
    sget-object v0, LX/FiL;->c:LX/FiL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2275112
    if-eqz v2, :cond_0

    .line 2275113
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2275114
    new-instance p0, LX/FiL;

    invoke-static {v0}, LX/8i7;->a(LX/0QB;)LX/8i7;

    move-result-object v3

    check-cast v3, LX/8i7;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {p0, v3, v4}, LX/FiL;-><init>(LX/8i7;LX/0Uh;)V

    .line 2275115
    move-object v0, p0

    .line 2275116
    sput-object v0, LX/FiL;->c:LX/FiL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2275117
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2275118
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2275119
    :cond_1
    sget-object v0, LX/FiL;->c:LX/FiL;

    return-object v0

    .line 2275120
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2275121
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
