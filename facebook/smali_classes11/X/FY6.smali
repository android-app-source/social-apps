.class public final LX/FY6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BYY;


# instance fields
.field public final synthetic a:Landroid/net/Uri;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:LX/BO1;

.field public final synthetic d:LX/BUL;

.field public final synthetic e:LX/FY8;


# direct methods
.method public constructor <init>(LX/FY8;Landroid/net/Uri;Ljava/lang/String;LX/BO1;LX/BUL;)V
    .locals 0

    .prologue
    .line 2255778
    iput-object p1, p0, LX/FY6;->e:LX/FY8;

    iput-object p2, p0, LX/FY6;->a:Landroid/net/Uri;

    iput-object p3, p0, LX/FY6;->b:Ljava/lang/String;

    iput-object p4, p0, LX/FY6;->c:LX/BO1;

    iput-object p5, p0, LX/FY6;->d:LX/BUL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 18

    .prologue
    .line 2255779
    if-eqz p1, :cond_0

    .line 2255780
    new-instance v3, LX/BUL;

    move-object/from16 v0, p0

    iget-object v4, v0, LX/FY6;->a:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/FY6;->b:Ljava/lang/String;

    const-string v6, ""

    const-string v7, "saved_dashboard"

    move-object/from16 v0, p0

    iget-object v2, v0, LX/FY6;->c:LX/BO1;

    invoke-interface {v2}, LX/BO1;->V()J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v2, v0, LX/FY6;->c:LX/BO1;

    invoke-interface {v2}, LX/BO1;->j()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v2, v0, LX/FY6;->c:LX/BO1;

    invoke-interface {v2}, LX/BO1;->k()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v2, v0, LX/FY6;->c:LX/BO1;

    invoke-interface {v2}, LX/BO1;->m()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v2, v0, LX/FY6;->c:LX/BO1;

    invoke-interface {v2}, LX/BO1;->w()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v2, v0, LX/FY6;->c:LX/BO1;

    invoke-interface {v2}, LX/BO1;->C()Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v2, v0, LX/FY6;->c:LX/BO1;

    invoke-interface {v2}, LX/BO1;->D()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v2, v0, LX/FY6;->c:LX/BO1;

    invoke-interface {v2}, LX/BO1;->E()Ljava/lang/String;

    move-result-object v16

    sget-object v17, LX/2ft;->WAIT_FOR_OFF_PEAK_DATA_HAPPY_HOUR:LX/2ft;

    invoke-direct/range {v3 .. v17}, LX/BUL;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2ft;)V

    .line 2255781
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FY6;->e:LX/FY8;

    iget-object v2, v2, LX/FY8;->b:LX/BUA;

    invoke-virtual {v2, v3}, LX/BUA;->a(LX/BUL;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2255782
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FY6;->e:LX/FY8;

    iget-object v2, v2, LX/FY8;->b:LX/BUA;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/FY6;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/BUA;->a(Ljava/lang/String;)V

    .line 2255783
    :goto_0
    return-void

    .line 2255784
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FY6;->e:LX/FY8;

    iget-object v2, v2, LX/FY8;->b:LX/BUA;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/FY6;->d:LX/BUL;

    invoke-virtual {v2, v3}, LX/BUA;->a(LX/BUL;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2255785
    move-object/from16 v0, p0

    iget-object v2, v0, LX/FY6;->e:LX/FY8;

    iget-object v2, v2, LX/FY8;->b:LX/BUA;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/FY6;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, LX/BUA;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
