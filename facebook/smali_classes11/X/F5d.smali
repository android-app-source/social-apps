.class public LX/F5d;
.super Lcom/facebook/widget/recyclerview/BetterRecyclerView;
.source ""


# instance fields
.field public i:LX/F5h;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public j:LX/F5k;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/1OD;

.field public l:LX/1P1;

.field public m:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2199059
    invoke-direct {p0, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;-><init>(Landroid/content/Context;)V

    .line 2199060
    const/4 p1, 0x0

    .line 2199061
    const-class v0, LX/F5d;

    invoke-static {v0, p0}, LX/F5d;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2199062
    new-instance v0, LX/1P1;

    invoke-virtual {p0}, LX/F5d;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1P1;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/F5d;->l:LX/1P1;

    .line 2199063
    iget-object v0, p0, LX/F5d;->l:LX/1P1;

    invoke-virtual {v0, p1}, LX/1P1;->b(I)V

    .line 2199064
    iget-object v0, p0, LX/F5d;->l:LX/1P1;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2199065
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:LX/1Of;

    move-object v0, v0

    .line 2199066
    iput-boolean p1, v0, LX/1Of;->g:Z

    .line 2199067
    new-instance v0, LX/F5c;

    invoke-direct {v0, p0}, LX/F5c;-><init>(LX/F5d;)V

    iput-object v0, p0, LX/F5d;->k:LX/1OD;

    .line 2199068
    iput-boolean p1, p0, LX/F5d;->m:Z

    .line 2199069
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/F5d;

    invoke-static {p0}, LX/F5h;->b(LX/0QB;)LX/F5h;

    move-result-object v1

    check-cast v1, LX/F5h;

    invoke-static {p0}, LX/F5k;->b(LX/0QB;)LX/F5k;

    move-result-object p0

    check-cast p0, LX/F5k;

    iput-object v1, p1, LX/F5d;->i:LX/F5h;

    iput-object p0, p1, LX/F5d;->j:LX/F5k;

    return-void
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x3226e599

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2199070
    invoke-super {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->onAttachedToWindow()V

    .line 2199071
    iget-object v1, p0, LX/F5d;->j:LX/F5k;

    iget-object v2, p0, LX/F5d;->k:LX/1OD;

    invoke-virtual {v1, v2}, LX/1OM;->a(LX/1OD;)V

    .line 2199072
    iget-object v1, p0, LX/F5d;->j:LX/F5k;

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2199073
    iget-boolean v1, p0, LX/F5d;->m:Z

    if-nez v1, :cond_0

    .line 2199074
    iget-object v1, p0, LX/F5d;->i:LX/F5h;

    iget-object v2, p0, LX/F5d;->j:LX/F5k;

    invoke-virtual {v1, v2}, LX/F5h;->a(LX/F5k;)V

    .line 2199075
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/F5d;->m:Z

    .line 2199076
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x1a605a4f

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x6c0d3f29

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2199056
    invoke-super {p0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->onDetachedFromWindow()V

    .line 2199057
    iget-object v1, p0, LX/F5d;->j:LX/F5k;

    iget-object v2, p0, LX/F5d;->k:LX/1OD;

    invoke-virtual {v1, v2}, LX/1OM;->b(LX/1OD;)V

    .line 2199058
    const/16 v1, 0x2d

    const v2, -0x5e197d01

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
