.class public final LX/H5m;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<TE;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

.field public final synthetic c:LX/1Pn;

.field public final synthetic d:LX/E2b;

.field public final synthetic e:Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;LX/0Px;Lcom/facebook/reaction/common/ReactionUnitComponentNode;LX/1Pn;LX/E2b;)V
    .locals 0

    .prologue
    .line 2425030
    iput-object p1, p0, LX/H5m;->e:Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;

    iput-object p2, p0, LX/H5m;->a:LX/0Px;

    iput-object p3, p0, LX/H5m;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iput-object p4, p0, LX/H5m;->c:LX/1Pn;

    iput-object p5, p0, LX/H5m;->d:LX/E2b;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/2eI;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 2425031
    iget-object v0, p0, LX/H5m;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    iget-object v0, p0, LX/H5m;->a:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9uc;

    .line 2425032
    new-instance v3, Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    iget-object v4, p0, LX/H5m;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2425033
    iget-object v5, v4, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v4, v5

    .line 2425034
    iget-object v5, p0, LX/H5m;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2425035
    iget-object v6, v5, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v5, v6

    .line 2425036
    iget-object v6, p0, LX/H5m;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2425037
    iget-object v7, v6, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v6, v7

    .line 2425038
    invoke-direct {v3, v4, v0, v5, v6}, Lcom/facebook/reaction/common/ReactionUnitComponentNode;-><init>(Lcom/facebook/feed/rows/core/props/FeedProps;LX/9uc;Ljava/lang/String;Ljava/lang/String;)V

    .line 2425039
    iget-object v4, p0, LX/H5m;->e:Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;

    iget-object v4, v4, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;->e:LX/1s9;

    invoke-interface {v0}, LX/9uc;->a()Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;

    move-result-object v0

    invoke-virtual {v4, v0}, LX/1s9;->a(Lcom/facebook/graphql/enums/GraphQLReactionUnitComponentStyle;)Lcom/facebook/multirow/api/MultiRowPartWithIsNeeded;

    move-result-object v0

    .line 2425040
    instance-of v4, v0, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    if-eqz v4, :cond_1

    .line 2425041
    check-cast v0, Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;

    sget-object v4, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;->a:LX/1Cz;

    iget-object v5, p0, LX/H5m;->c:LX/1Pn;

    invoke-interface {v5}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, LX/H5m;->e:Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;

    iget-object v6, v6, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;->g:LX/1Qx;

    invoke-static {v0, v4, v5, v6}, LX/6Vo;->a(Lcom/facebook/multirow/api/MultiRowGroupPartDefinition;LX/1Cz;Landroid/content/Context;LX/1Qx;)LX/1RC;

    move-result-object v0

    invoke-virtual {p1, v0, v3}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2425042
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2425043
    :cond_1
    instance-of v4, v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    if-eqz v4, :cond_0

    .line 2425044
    check-cast v0, Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;

    sget-object v4, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;->a:LX/1Cz;

    iget-object v5, p0, LX/H5m;->c:LX/1Pn;

    invoke-interface {v5}, LX/1Pn;->getContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, LX/H5m;->e:Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;

    iget-object v6, v6, Lcom/facebook/notifications/multirow/partdefinition/NotificationsHScrollUnitComponentPartDefinition;->g:LX/1Qx;

    invoke-static {v0, v4, v5, v6}, LX/6Vo;->a(Lcom/facebook/feed/rows/core/parts/MultiRowSinglePartDefinition;LX/1Cz;Landroid/content/Context;LX/1Qx;)LX/1RC;

    move-result-object v0

    invoke-virtual {p1, v0, v3}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    goto :goto_1

    .line 2425045
    :cond_2
    return-void
.end method

.method public final c(I)V
    .locals 3

    .prologue
    .line 2425046
    iget-object v0, p0, LX/H5m;->c:LX/1Pn;

    check-cast v0, LX/1Pr;

    iget-object v1, p0, LX/H5m;->d:LX/E2b;

    iget-object v2, p0, LX/H5m;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    invoke-interface {v0, v1, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/E2c;

    .line 2425047
    iput p1, v0, LX/E2c;->d:I

    .line 2425048
    iget-object v0, p0, LX/H5m;->c:LX/1Pn;

    check-cast v0, LX/2kl;

    invoke-interface {v0}, LX/2kl;->md_()LX/2jc;

    move-result-object v0

    iget-object v1, p0, LX/H5m;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2425049
    iget-object v2, v1, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2425050
    iget-object v2, p0, LX/H5m;->b:Lcom/facebook/reaction/common/ReactionUnitComponentNode;

    .line 2425051
    iget-object p0, v2, Lcom/facebook/reaction/common/ReactionUnitComponentNode;->d:Ljava/lang/String;

    move-object v2, p0

    .line 2425052
    invoke-interface {v0, v1, v2}, LX/2jb;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2425053
    return-void
.end method
