.class public final LX/H7C;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "address"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "address"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "location"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "location"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "placeCurrentOpenHours"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "placeCurrentOpenHours"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "placeOpenStatus"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "placeOpenStatus"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2429017
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2429018
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerLocationDetailsModel;
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v9, 0x0

    const/4 v2, 0x0

    .line 2429019
    new-instance v0, LX/186;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    .line 2429020
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v3, p0, LX/H7C;->a:LX/15i;

    iget v5, p0, LX/H7C;->b:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v1, 0x614b33f6

    invoke-static {v3, v5, v1}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v1

    invoke-static {v0, v1}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v1

    .line 2429021
    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_1
    iget-object v5, p0, LX/H7C;->c:LX/15i;

    iget v6, p0, LX/H7C;->d:I

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const v3, -0x7f68b3a0

    invoke-static {v5, v6, v3}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v3

    invoke-static {v0, v3}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v3

    .line 2429022
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_2
    iget-object v6, p0, LX/H7C;->e:LX/15i;

    iget v7, p0, LX/H7C;->f:I

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const v5, -0xea5f3c

    invoke-static {v6, v7, v5}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v5

    invoke-static {v0, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2429023
    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_3
    iget-object v7, p0, LX/H7C;->g:LX/15i;

    iget v8, p0, LX/H7C;->h:I

    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    const v6, 0x469d319

    invoke-static {v7, v8, v6}, Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/offers/graphql/OfferQueriesModels$DraculaImplementation;

    move-result-object v6

    invoke-static {v0, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2429024
    iget-object v7, p0, LX/H7C;->i:Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    invoke-virtual {v0, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 2429025
    const/4 v8, 0x5

    invoke-virtual {v0, v8}, LX/186;->c(I)V

    .line 2429026
    invoke-virtual {v0, v9, v1}, LX/186;->b(II)V

    .line 2429027
    invoke-virtual {v0, v4, v3}, LX/186;->b(II)V

    .line 2429028
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, LX/186;->b(II)V

    .line 2429029
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v6}, LX/186;->b(II)V

    .line 2429030
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v7}, LX/186;->b(II)V

    .line 2429031
    invoke-virtual {v0}, LX/186;->d()I

    move-result v1

    .line 2429032
    invoke-virtual {v0, v1}, LX/186;->d(I)V

    .line 2429033
    invoke-virtual {v0}, LX/186;->e()[B

    move-result-object v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 2429034
    invoke-virtual {v1, v9}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2429035
    new-instance v0, LX/15i;

    move-object v3, v2

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2429036
    new-instance v1, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerLocationDetailsModel;

    invoke-direct {v1, v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferOwnerLocationDetailsModel;-><init>(LX/15i;)V

    .line 2429037
    return-object v1

    .line 2429038
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    .line 2429039
    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v0

    .line 2429040
    :catchall_2
    move-exception v0

    :try_start_6
    monitor-exit v5
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    throw v0

    .line 2429041
    :catchall_3
    move-exception v0

    :try_start_7
    monitor-exit v6
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v0
.end method
