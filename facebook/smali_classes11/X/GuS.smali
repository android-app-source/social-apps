.class public final LX/GuS;
.super LX/BWM;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 2403333
    iput-object p1, p0, LX/GuS;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403334
    invoke-direct {p0, p2}, LX/BWM;-><init>(Landroid/os/Handler;)V

    .line 2403335
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/webview/FacebookWebView;LX/BWN;)V
    .locals 5

    .prologue
    .line 2403336
    iget-object v0, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2403337
    const-string v1, "actions"

    invoke-interface {p2, v0, v1}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2403338
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 2403339
    iget-object v0, p0, LX/GuS;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    new-array v2, v2, [Lorg/json/JSONObject;

    .line 2403340
    iput-object v2, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ab:[Lorg/json/JSONObject;

    .line 2403341
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 2403342
    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 2403343
    iget-object v3, p0, LX/GuS;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v3, v3, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ab:[Lorg/json/JSONObject;

    aput-object v2, v3, v0

    .line 2403344
    const-string v3, "type"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2403345
    const-string v3, "type"

    invoke-virtual {v2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2403346
    const-string v4, "mark_unread"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2403347
    const v4, 0x7f020d19

    .line 2403348
    :goto_1
    move v3, v4

    .line 2403349
    if-lez v3, :cond_0

    .line 2403350
    const-string v4, "icon"

    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2403351
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2403352
    :catch_0
    move-exception v0

    .line 2403353
    iget-object v1, p0, LX/GuS;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v1}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->e()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Invalid JSON format"

    invoke-static {v1, v2, v0}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2403354
    iget-object v0, p0, LX/GuS;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    const/4 v1, 0x0

    .line 2403355
    iput-object v1, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->ab:[Lorg/json/JSONObject;

    .line 2403356
    :cond_1
    return-void

    .line 2403357
    :cond_2
    const-string v4, "mark_spam"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2403358
    const v4, 0x7f020d17

    goto :goto_1

    .line 2403359
    :cond_3
    const-string v4, "archive"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2403360
    const v4, 0x7f020d13

    goto :goto_1

    .line 2403361
    :cond_4
    const-string v4, "unarchive"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2403362
    const v4, 0x7f020d18

    goto :goto_1

    .line 2403363
    :cond_5
    const-string v4, "move"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2403364
    const v4, 0x7f020d16

    goto :goto_1

    .line 2403365
    :cond_6
    const-string v4, "delete"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2403366
    const v4, 0x7f020d14

    goto :goto_1

    .line 2403367
    :cond_7
    const-string v4, "forward"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2403368
    const v4, 0x7f020d15

    goto :goto_1

    .line 2403369
    :cond_8
    const/4 v4, -0x1

    goto :goto_1
.end method
