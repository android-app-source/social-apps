.class public final LX/GCW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/scrollview/LockableScrollView;

.field public final synthetic b:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;Lcom/facebook/widget/scrollview/LockableScrollView;)V
    .locals 0

    .prologue
    .line 2328360
    iput-object p1, p0, LX/GCW;->b:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    iput-object p2, p0, LX/GCW;->a:Lcom/facebook/widget/scrollview/LockableScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 2328361
    invoke-virtual {p1}, Landroid/view/View;->requestFocusFromTouch()Z

    .line 2328362
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2328363
    :goto_0
    iget-object v0, p0, LX/GCW;->a:Lcom/facebook/widget/scrollview/LockableScrollView;

    invoke-virtual {v0, p2}, Lcom/facebook/widget/scrollview/LockableScrollView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    .line 2328364
    :pswitch_0
    iget-object v0, p0, LX/GCW;->b:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    invoke-static {v0}, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->C(Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;)V

    .line 2328365
    iget-object v0, p0, LX/GCW;->b:Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    iget-object v0, v0, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;->q:LX/GF4;

    new-instance v1, LX/GFg;

    invoke-direct {v1}, LX/GFg;-><init>()V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    .line 2328366
    :pswitch_1
    iget-object v0, p0, LX/GCW;->a:Lcom/facebook/widget/scrollview/LockableScrollView;

    invoke-virtual {v0}, Lcom/facebook/widget/scrollview/LockableScrollView;->performClick()Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
