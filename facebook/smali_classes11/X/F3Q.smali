.class public final enum LX/F3Q;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/F3Q;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/F3Q;

.field public static final enum GET_GROUP_TAG_SUGGESTIONS:LX/F3Q;

.field public static final enum GET_GROUP_TOPIC_TAGS:LX/F3Q;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2194084
    new-instance v0, LX/F3Q;

    const-string v1, "GET_GROUP_TAG_SUGGESTIONS"

    invoke-direct {v0, v1, v2}, LX/F3Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F3Q;->GET_GROUP_TAG_SUGGESTIONS:LX/F3Q;

    .line 2194085
    new-instance v0, LX/F3Q;

    const-string v1, "GET_GROUP_TOPIC_TAGS"

    invoke-direct {v0, v1, v3}, LX/F3Q;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F3Q;->GET_GROUP_TOPIC_TAGS:LX/F3Q;

    .line 2194086
    const/4 v0, 0x2

    new-array v0, v0, [LX/F3Q;

    sget-object v1, LX/F3Q;->GET_GROUP_TAG_SUGGESTIONS:LX/F3Q;

    aput-object v1, v0, v2

    sget-object v1, LX/F3Q;->GET_GROUP_TOPIC_TAGS:LX/F3Q;

    aput-object v1, v0, v3

    sput-object v0, LX/F3Q;->$VALUES:[LX/F3Q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2194087
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/F3Q;
    .locals 1

    .prologue
    .line 2194088
    const-class v0, LX/F3Q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/F3Q;

    return-object v0
.end method

.method public static values()[LX/F3Q;
    .locals 1

    .prologue
    .line 2194089
    sget-object v0, LX/F3Q;->$VALUES:[LX/F3Q;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/F3Q;

    return-object v0
.end method
