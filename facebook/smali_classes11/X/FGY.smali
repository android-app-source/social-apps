.class public final enum LX/FGY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FGY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FGY;

.field public static final enum INVALID_RESULT_RETURNED:LX/FGY;

.field public static final enum MQTT_FAILED:LX/FGY;

.field public static final enum NO_EXISTING_MEDIA_FOUND:LX/FGY;

.field public static final enum NO_HASH_AVAILABLE:LX/FGY;

.field public static final enum SERVER_SIDE_FAILED:LX/FGY;

.field public static final enum UNKNOWN:LX/FGY;

.field public static final enum VALID_FBID_RETURNED:LX/FGY;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2219070
    new-instance v0, LX/FGY;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/FGY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FGY;->UNKNOWN:LX/FGY;

    .line 2219071
    new-instance v0, LX/FGY;

    const-string v1, "VALID_FBID_RETURNED"

    invoke-direct {v0, v1, v4}, LX/FGY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FGY;->VALID_FBID_RETURNED:LX/FGY;

    .line 2219072
    new-instance v0, LX/FGY;

    const-string v1, "NO_EXISTING_MEDIA_FOUND"

    invoke-direct {v0, v1, v5}, LX/FGY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FGY;->NO_EXISTING_MEDIA_FOUND:LX/FGY;

    .line 2219073
    new-instance v0, LX/FGY;

    const-string v1, "INVALID_RESULT_RETURNED"

    invoke-direct {v0, v1, v6}, LX/FGY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FGY;->INVALID_RESULT_RETURNED:LX/FGY;

    .line 2219074
    new-instance v0, LX/FGY;

    const-string v1, "SERVER_SIDE_FAILED"

    invoke-direct {v0, v1, v7}, LX/FGY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FGY;->SERVER_SIDE_FAILED:LX/FGY;

    .line 2219075
    new-instance v0, LX/FGY;

    const-string v1, "MQTT_FAILED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/FGY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FGY;->MQTT_FAILED:LX/FGY;

    .line 2219076
    new-instance v0, LX/FGY;

    const-string v1, "NO_HASH_AVAILABLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/FGY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FGY;->NO_HASH_AVAILABLE:LX/FGY;

    .line 2219077
    const/4 v0, 0x7

    new-array v0, v0, [LX/FGY;

    sget-object v1, LX/FGY;->UNKNOWN:LX/FGY;

    aput-object v1, v0, v3

    sget-object v1, LX/FGY;->VALID_FBID_RETURNED:LX/FGY;

    aput-object v1, v0, v4

    sget-object v1, LX/FGY;->NO_EXISTING_MEDIA_FOUND:LX/FGY;

    aput-object v1, v0, v5

    sget-object v1, LX/FGY;->INVALID_RESULT_RETURNED:LX/FGY;

    aput-object v1, v0, v6

    sget-object v1, LX/FGY;->SERVER_SIDE_FAILED:LX/FGY;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/FGY;->MQTT_FAILED:LX/FGY;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/FGY;->NO_HASH_AVAILABLE:LX/FGY;

    aput-object v2, v0, v1

    sput-object v0, LX/FGY;->$VALUES:[LX/FGY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2219069
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FGY;
    .locals 1

    .prologue
    .line 2219068
    const-class v0, LX/FGY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FGY;

    return-object v0
.end method

.method public static values()[LX/FGY;
    .locals 1

    .prologue
    .line 2219067
    sget-object v0, LX/FGY;->$VALUES:[LX/FGY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FGY;

    return-object v0
.end method
