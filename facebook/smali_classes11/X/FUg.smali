.class public LX/FUg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static f:LX/0Xm;


# instance fields
.field public a:LX/FUf;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Z


# direct methods
.method public constructor <init>(LX/FUf;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2249533
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2249534
    const-string v0, "START_INITIAL"

    iput-object v0, p0, LX/FUg;->c:Ljava/lang/String;

    .line 2249535
    iput-object p1, p0, LX/FUg;->a:LX/FUf;

    .line 2249536
    return-void
.end method

.method public static a(LX/0QB;)LX/FUg;
    .locals 4

    .prologue
    .line 2249537
    const-class v1, LX/FUg;

    monitor-enter v1

    .line 2249538
    :try_start_0
    sget-object v0, LX/FUg;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2249539
    sput-object v2, LX/FUg;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2249540
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2249541
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2249542
    new-instance p0, LX/FUg;

    invoke-static {v0}, LX/FUf;->b(LX/0QB;)LX/FUf;

    move-result-object v3

    check-cast v3, LX/FUf;

    invoke-direct {p0, v3}, LX/FUg;-><init>(LX/FUf;)V

    .line 2249543
    move-object v0, p0

    .line 2249544
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2249545
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FUg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2249546
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2249547
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
