.class public final LX/Fst;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<*>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FsE;

.field public final synthetic b:Ljava/lang/ref/WeakReference;

.field public final synthetic c:LX/Fsw;


# direct methods
.method public constructor <init>(LX/Fsw;LX/FsE;Ljava/lang/ref/WeakReference;)V
    .locals 0

    .prologue
    .line 2297694
    iput-object p1, p0, LX/Fst;->c:LX/Fsw;

    iput-object p2, p0, LX/Fst;->a:LX/FsE;

    iput-object p3, p0, LX/Fst;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2297695
    iget-object v0, p0, LX/Fst;->a:LX/FsE;

    const/4 v1, 0x1

    .line 2297696
    iput-boolean v1, v0, LX/FsE;->e:Z

    .line 2297697
    iget-object v0, p0, LX/Fst;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/TimelineFragment;

    .line 2297698
    if-nez v0, :cond_0

    .line 2297699
    :goto_0
    return-void

    .line 2297700
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/timeline/TimelineFragment;->J()V

    goto :goto_0
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2297701
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2297702
    iget-object v0, p0, LX/Fst;->a:LX/FsE;

    const/4 v1, 0x1

    .line 2297703
    iput-boolean v1, v0, LX/FsE;->e:Z

    .line 2297704
    invoke-static {p1}, LX/Fsw;->b(Lcom/facebook/graphql/executor/GraphQLResult;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2297705
    iget-object v0, p0, LX/Fst;->c:LX/Fsw;

    iget-object v0, v0, LX/Fsw;->c:LX/BQB;

    .line 2297706
    iget-object v1, v0, LX/BQB;->f:LX/BPC;

    .line 2297707
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/BPC;->c:Z

    .line 2297708
    iget-object v1, v0, LX/BQB;->c:LX/BQD;

    const-string v2, "TimelineHeaderCachedDataRead"

    invoke-virtual {v1, v2}, LX/BQD;->f(Ljava/lang/String;)V

    .line 2297709
    iget-object v0, p0, LX/Fst;->c:LX/Fsw;

    iget-object v0, v0, LX/Fsw;->d:[Z

    const/4 v1, 0x0

    aget-boolean v0, v0, v1

    if-nez v0, :cond_0

    .line 2297710
    iget-object v0, p0, LX/Fst;->b:Ljava/lang/ref/WeakReference;

    .line 2297711
    invoke-static {p1}, LX/Fsw;->b(Lcom/facebook/graphql/executor/GraphQLResult;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2297712
    :cond_0
    :goto_0
    return-void

    .line 2297713
    :cond_1
    invoke-static {p1, v0}, LX/Fsw;->d(Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/ref/WeakReference;)V

    goto :goto_0
.end method
