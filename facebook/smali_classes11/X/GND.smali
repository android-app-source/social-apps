.class public LX/GND;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:D

.field public final b:D


# direct methods
.method public constructor <init>(DD)V
    .locals 1

    .prologue
    .line 2345556
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2345557
    iput-wide p1, p0, LX/GND;->a:D

    .line 2345558
    iput-wide p3, p0, LX/GND;->b:D

    .line 2345559
    return-void
.end method

.method public static a(LX/31h;Landroid/view/View;)D
    .locals 10

    .prologue
    .line 2345550
    invoke-virtual {p0}, LX/31h;->a()LX/69F;

    move-result-object v0

    .line 2345551
    new-instance v1, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {p0, v1}, LX/31h;->a(Landroid/graphics/Point;)Lcom/facebook/android/maps/model/LatLng;

    move-result-object v1

    .line 2345552
    iget-object v0, v0, LX/69F;->e:LX/697;

    invoke-virtual {v0}, LX/697;->b()Lcom/facebook/android/maps/model/LatLng;

    move-result-object v4

    .line 2345553
    iget-wide v0, v1, Lcom/facebook/android/maps/model/LatLng;->b:D

    iget-wide v2, v4, Lcom/facebook/android/maps/model/LatLng;->b:D

    iget-wide v4, v4, Lcom/facebook/android/maps/model/LatLng;->a:D

    .line 2345554
    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    sub-double v8, v2, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v8

    mul-double/2addr v6, v8

    const-wide v8, 0x41584db040000000L    # 6371009.0

    mul-double/2addr v6, v8

    move-wide v0, v6

    .line 2345555
    return-wide v0
.end method

.method public static a(LX/680;Lcom/facebook/android/maps/model/LatLng;DI)F
    .locals 10

    .prologue
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    .line 2345543
    iget-wide v0, p1, Lcom/facebook/android/maps/model/LatLng;->b:D

    iget-wide v2, p1, Lcom/facebook/android/maps/model/LatLng;->a:D

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, LX/31h;->a(DDD)D

    move-result-wide v0

    invoke-static {v0, v1}, LX/31h;->d(D)F

    move-result v0

    iget-wide v2, p1, Lcom/facebook/android/maps/model/LatLng;->b:D

    invoke-static {v2, v3}, LX/31h;->d(D)F

    move-result v1

    sub-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    float-to-double v0, v0

    .line 2345544
    int-to-double v2, p4

    .line 2345545
    iget v4, p0, LX/680;->B:I

    move v4, v4

    .line 2345546
    int-to-double v4, v4

    mul-double/2addr v0, v4

    div-double v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    invoke-static {v8, v9}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    .line 2345547
    double-to-int v2, v0

    .line 2345548
    rem-double/2addr v0, v6

    invoke-static {v8, v9, v0, v1}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    .line 2345549
    int-to-double v2, v2

    sub-double/2addr v0, v6

    add-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method
