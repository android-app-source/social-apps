.class public final LX/Fth;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;)V
    .locals 0

    .prologue
    .line 2299072
    iput-object p1, p0, LX/Fth;->a:Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x459a04a0

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2299073
    new-instance v0, LX/89V;

    invoke-direct {v0}, LX/89V;-><init>()V

    sget-object v1, LX/89Z;->FAVORITE_MEDIA_PICKER:LX/89Z;

    .line 2299074
    iput-object v1, v0, LX/89V;->l:LX/89Z;

    .line 2299075
    move-object v0, v0

    .line 2299076
    iget-object v1, p0, LX/Fth;->a:Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;

    .line 2299077
    iget-object v3, v1, Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;->d:LX/0ad;

    sget-short v4, LX/0wf;->A:S

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, LX/0ad;->a(SZ)Z

    move-result v3

    move v3, v3

    .line 2299078
    if-eqz v3, :cond_0

    .line 2299079
    sget-object v3, LX/4gI;->ALL:LX/4gI;

    .line 2299080
    :goto_0
    move-object v1, v3

    .line 2299081
    iput-object v1, v0, LX/89V;->f:LX/4gI;

    .line 2299082
    move-object v0, v0

    .line 2299083
    invoke-virtual {v0}, LX/89V;->a()Lcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;

    move-result-object v3

    .line 2299084
    iget-object v0, p0, LX/Fth;->a:Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;

    iget-object v0, v0, Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;->c:LX/1b0;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v4, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;

    invoke-static {v1, v4}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    const-string v4, "session_id"

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, LX/1b0;->a(Landroid/app/Activity;ILcom/facebook/ipc/creativecam/CreativeCamLaunchConfig;Ljava/lang/String;Lcom/facebook/productionprompts/logging/PromptAnalytics;)V

    .line 2299085
    const v0, 0x70a8d969

    invoke-static {v2, v2, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    :cond_0
    sget-object v3, LX/4gI;->PHOTO_ONLY:LX/4gI;

    goto :goto_0
.end method
