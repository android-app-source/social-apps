.class public final enum LX/FlS;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FlS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FlS;

.field public static final enum CHILD:LX/FlS;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2280145
    new-instance v0, LX/FlS;

    const-string v1, "CHILD"

    invoke-direct {v0, v1, v2}, LX/FlS;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FlS;->CHILD:LX/FlS;

    .line 2280146
    const/4 v0, 0x1

    new-array v0, v0, [LX/FlS;

    sget-object v1, LX/FlS;->CHILD:LX/FlS;

    aput-object v1, v0, v2

    sput-object v0, LX/FlS;->$VALUES:[LX/FlS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2280147
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FlS;
    .locals 1

    .prologue
    .line 2280148
    const-class v0, LX/FlS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FlS;

    return-object v0
.end method

.method public static values()[LX/FlS;
    .locals 1

    .prologue
    .line 2280149
    sget-object v0, LX/FlS;->$VALUES:[LX/FlS;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FlS;

    return-object v0
.end method
