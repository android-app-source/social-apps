.class public LX/F9q;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/03V;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/03V;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2206152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2206153
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/F9q;->a:Landroid/content/Context;

    .line 2206154
    iput-object p2, p0, LX/F9q;->b:LX/03V;

    .line 2206155
    return-void
.end method


# virtual methods
.method public final a()Landroid/net/Uri;
    .locals 11

    .prologue
    .line 2206156
    :try_start_0
    const/4 v7, 0x1

    const/4 v10, 0x0

    const/4 v8, 0x0

    .line 2206157
    const-string v3, "android.permission.READ_PROFILE"

    .line 2206158
    iget-object v4, p0, LX/F9q;->a:Landroid/content/Context;

    invoke-virtual {v4, v3}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v3

    .line 2206159
    if-eqz v3, :cond_1

    .line 2206160
    :cond_0
    :goto_0
    move-object v0, v8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2206161
    :goto_1
    return-object v0

    .line 2206162
    :catch_0
    move-exception v0

    .line 2206163
    iget-object v1, p0, LX/F9q;->b:LX/03V;

    const-string v2, "Prefill Profile Photo via Me Profile in user account NUX flow on ICS+"

    invoke-virtual {v1, v2, v0}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2206164
    const/4 v0, 0x0

    goto :goto_1

    .line 2206165
    :cond_1
    iget-object v3, p0, LX/F9q;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Landroid/provider/ContactsContract$Profile;->CONTENT_URI:Landroid/net/Uri;

    const-string v5, "data"

    invoke-static {v4, v5}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/String;

    const-string v6, "photo_uri"

    aput-object v6, v5, v10

    const-string v6, "mimetype = ?"

    new-array v7, v7, [Ljava/lang/String;

    const-string v9, "vnd.android.cursor.item/photo"

    aput-object v9, v7, v10

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 2206166
    if-eqz v3, :cond_2

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-lez v4, :cond_2

    .line 2206167
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2206168
    const-string v4, "photo_uri"

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 2206169
    if-nez v4, :cond_3

    .line 2206170
    :cond_2
    :goto_2
    if-eqz v3, :cond_0

    .line 2206171
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 2206172
    :cond_3
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    goto :goto_2
.end method
