.class public final synthetic LX/G03;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final synthetic a:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2308402
    invoke-static {}, Lcom/facebook/graphql/enums/GraphQLGender;->values()[Lcom/facebook/graphql/enums/GraphQLGender;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, LX/G03;->a:[I

    :try_start_0
    sget-object v0, LX/G03;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGender;->FEMALE:Lcom/facebook/graphql/enums/GraphQLGender;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGender;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    :try_start_1
    sget-object v0, LX/G03;->a:[I

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLGender;->MALE:Lcom/facebook/graphql/enums/GraphQLGender;

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLGender;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_0

    :goto_1
    return-void

    :catch_0
    goto :goto_1

    :catch_1
    goto :goto_0
.end method
