.class public LX/Fi5;
.super LX/2SP;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile h:LX/Fi5;


# instance fields
.field public final a:LX/2Sc;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:LX/Fi3;

.field public d:LX/Cwv;

.field private e:Lcom/facebook/search/api/GraphSearchQuery;

.field public f:LX/2SR;

.field public g:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/Cwv;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/2Sc;Ljava/util/concurrent/ExecutorService;LX/Fi3;)V
    .locals 0
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2274140
    invoke-direct {p0}, LX/2SP;-><init>()V

    .line 2274141
    iput-object p1, p0, LX/Fi5;->a:LX/2Sc;

    .line 2274142
    iput-object p2, p0, LX/Fi5;->b:Ljava/util/concurrent/ExecutorService;

    .line 2274143
    iput-object p3, p0, LX/Fi5;->c:LX/Fi3;

    .line 2274144
    return-void
.end method

.method public static a(LX/0QB;)LX/Fi5;
    .locals 10

    .prologue
    .line 2274168
    sget-object v0, LX/Fi5;->h:LX/Fi5;

    if-nez v0, :cond_1

    .line 2274169
    const-class v1, LX/Fi5;

    monitor-enter v1

    .line 2274170
    :try_start_0
    sget-object v0, LX/Fi5;->h:LX/Fi5;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2274171
    if-eqz v2, :cond_0

    .line 2274172
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2274173
    new-instance v6, LX/Fi5;

    invoke-static {v0}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v3

    check-cast v3, LX/2Sc;

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    .line 2274174
    new-instance p0, LX/Fi3;

    .line 2274175
    new-instance v7, LX/Cwu;

    invoke-static {v0}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v5

    check-cast v5, LX/2Sc;

    invoke-direct {v7, v5}, LX/Cwu;-><init>(LX/2Sc;)V

    .line 2274176
    move-object v5, v7

    .line 2274177
    check-cast v5, LX/Cwu;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v7

    check-cast v7, LX/0tX;

    invoke-static {v0}, LX/8hr;->b(LX/0QB;)LX/8hr;

    move-result-object v8

    check-cast v8, LX/8hr;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v9

    check-cast v9, LX/0ad;

    invoke-direct {p0, v5, v7, v8, v9}, LX/Fi3;-><init>(LX/Cwu;LX/0tX;LX/8hr;LX/0ad;)V

    .line 2274178
    move-object v5, p0

    .line 2274179
    check-cast v5, LX/Fi3;

    invoke-direct {v6, v3, v4, v5}, LX/Fi5;-><init>(LX/2Sc;Ljava/util/concurrent/ExecutorService;LX/Fi3;)V

    .line 2274180
    move-object v0, v6

    .line 2274181
    sput-object v0, LX/Fi5;->h:LX/Fi5;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2274182
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2274183
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2274184
    :cond_1
    sget-object v0, LX/Fi5;->h:LX/Fi5;

    return-object v0

    .line 2274185
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2274186
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private declared-synchronized a(Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 8
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2274187
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/Fi5;->c:LX/Fi3;

    iget-object v1, p0, LX/Fi5;->e:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2274188
    iget-object v2, v1, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    move-object v1, v2

    .line 2274189
    sget-object v2, LX/0zS;->c:LX/0zS;

    .line 2274190
    new-instance v3, LX/9yO;

    invoke-direct {v3}, LX/9yO;-><init>()V

    move-object v3, v3

    .line 2274191
    const-string v4, "source"

    new-instance v5, LX/4FV;

    invoke-direct {v5}, LX/4FV;-><init>()V

    const-string v6, "dummy_source"

    invoke-virtual {v5, v6}, LX/4FV;->a(Ljava/lang/String;)LX/4FV;

    move-result-object v5

    sget-object v6, LX/Fi3;->a:Ljava/lang/Integer;

    invoke-virtual {v5, v6}, LX/4FV;->a(Ljava/lang/Integer;)LX/4FV;

    move-result-object v5

    invoke-static {v5}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/util/List;)LX/0gW;

    .line 2274192
    const-string v4, "location"

    iget-object v5, v0, LX/Fi3;->d:LX/8hr;

    invoke-virtual {v5}, LX/8hr;->a()LX/4FR;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2274193
    const-string v4, "ranking_model"

    iget-object v5, v0, LX/Fi3;->e:LX/0ad;

    sget-char v6, LX/100;->ct:C

    const-string v7, "mobile_webview_ss"

    invoke-interface {v5, v6, v7}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2274194
    const-string v4, "single_state_params"

    new-instance v5, LX/4FU;

    invoke-direct {v5}, LX/4FU;-><init>()V

    .line 2274195
    const-string v6, "share_url"

    invoke-virtual {v5, v6, v1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2274196
    move-object v5, v5

    .line 2274197
    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2274198
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    invoke-virtual {v3, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    const/4 v4, 0x1

    .line 2274199
    iput-boolean v4, v3, LX/0zO;->p:Z

    .line 2274200
    move-object v3, v3

    .line 2274201
    iput-object p1, v3, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2274202
    move-object v3, v3

    .line 2274203
    iget-object v4, v0, LX/Fi3;->c:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2274204
    new-instance v4, LX/Fi2;

    invoke-direct {v4, v0}, LX/Fi2;-><init>(LX/Fi3;)V

    invoke-static {v3, v4}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2274205
    iput-object v0, p0, LX/Fi5;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2274206
    new-instance v0, LX/Fi4;

    invoke-direct {v0, p0}, LX/Fi4;-><init>(LX/Fi5;)V

    .line 2274207
    iget-object v1, p0, LX/Fi5;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    iget-object v2, p0, LX/Fi5;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v0, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2274208
    monitor-exit p0

    return-void

    .line 2274209
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private j()Z
    .locals 1

    .prologue
    .line 2274210
    iget-object v0, p0, LX/Fi5;->d:LX/Cwv;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fi5;->d:LX/Cwv;

    invoke-virtual {v0}, LX/Cwv;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/7BE;
    .locals 1

    .prologue
    .line 2274165
    invoke-direct {p0}, LX/Fi5;->j()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/7BE;->READY:LX/7BE;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/7BE;->NOT_READY:LX/7BE;

    goto :goto_0
.end method

.method public final a(LX/2SR;LX/2Sp;)V
    .locals 0

    .prologue
    .line 2274166
    iput-object p1, p0, LX/Fi5;->f:LX/2SR;

    .line 2274167
    return-void
.end method

.method public final declared-synchronized a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V
    .locals 2
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2274155
    monitor-enter p0

    :try_start_0
    sget-object v0, LX/EPu;->MEMORY:LX/EPu;

    if-ne p2, v0, :cond_1

    sget-object v0, LX/7BE;->READY:LX/7BE;

    invoke-virtual {p0}, LX/2SP;->a()LX/7BE;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/7BE;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 2274156
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 2274157
    :cond_1
    :try_start_1
    iget-object v0, p0, LX/Fi5;->e:Lcom/facebook/search/api/GraphSearchQuery;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fi5;->e:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2274158
    iget-object v1, v0, Lcom/facebook/search/api/GraphSearchQuery;->i:LX/103;

    move-object v0, v1

    .line 2274159
    sget-object v1, LX/103;->URL:LX/103;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, LX/Fi5;->e:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2274160
    iget-object v1, v0, Lcom/facebook/search/api/GraphSearchQuery;->h:Ljava/lang/String;

    move-object v0, v1

    .line 2274161
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2274162
    iget-object v0, p0, LX/Fi5;->g:Lcom/google/common/util/concurrent/ListenableFuture;

    if-nez v0, :cond_0

    .line 2274163
    invoke-direct {p0, p1}, LX/Fi5;->a(Lcom/facebook/common/callercontext/CallerContext;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2274164
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 1

    .prologue
    .line 2274151
    iget-object v0, p0, LX/Fi5;->e:Lcom/facebook/search/api/GraphSearchQuery;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fi5;->e:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, p1}, Lcom/facebook/search/api/GraphSearchQuery;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2274152
    :goto_0
    return-void

    .line 2274153
    :cond_0
    iput-object p1, p0, LX/Fi5;->e:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2274154
    invoke-virtual {p0}, LX/2SP;->c()V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2274150
    const/4 v0, 0x1

    return v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2274148
    const/4 v0, 0x0

    iput-object v0, p0, LX/Fi5;->d:LX/Cwv;

    .line 2274149
    return-void
.end method

.method public final get()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2274145
    invoke-direct {p0}, LX/Fi5;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2274146
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2274147
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/Fi5;->d:LX/Cwv;

    invoke-virtual {v0}, LX/Cwv;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
