.class public LX/GvU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static j:LX/0Xm;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/concurrent/Executor;

.field private final c:LX/0tX;

.field public d:Lcom/facebook/katana/gdp/LoginModel;

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "LX/GvI;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/3rL;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3rL",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation
.end field

.field public g:Z

.field public h:Z

.field public i:LX/GvR;


# direct methods
.method public constructor <init>(LX/0Or;Ljava/util/concurrent/Executor;LX/0tX;)V
    .locals 2
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .param p2    # Ljava/util/concurrent/Executor;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            "LX/0tX;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2405507
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2405508
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/GvU;->e:Ljava/util/Set;

    .line 2405509
    iput-boolean v1, p0, LX/GvU;->g:Z

    .line 2405510
    iput-boolean v1, p0, LX/GvU;->h:Z

    .line 2405511
    iput-object p1, p0, LX/GvU;->a:LX/0Or;

    .line 2405512
    iput-object p2, p0, LX/GvU;->b:Ljava/util/concurrent/Executor;

    .line 2405513
    iput-object p3, p0, LX/GvU;->c:LX/0tX;

    .line 2405514
    return-void
.end method

.method public static a(LX/0QB;)LX/GvU;
    .locals 6

    .prologue
    .line 2405523
    const-class v1, LX/GvU;

    monitor-enter v1

    .line 2405524
    :try_start_0
    sget-object v0, LX/GvU;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2405525
    sput-object v2, LX/GvU;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2405526
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2405527
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2405528
    new-instance v5, LX/GvU;

    const/16 v3, 0x12cb

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v3

    check-cast v3, Ljava/util/concurrent/Executor;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-direct {v5, p0, v3, v4}, LX/GvU;-><init>(LX/0Or;Ljava/util/concurrent/Executor;LX/0tX;)V

    .line 2405529
    move-object v0, v5

    .line 2405530
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2405531
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GvU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2405532
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2405533
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/GvU;Z)V
    .locals 1

    .prologue
    .line 2405519
    iget-boolean v0, p0, LX/GvU;->g:Z

    if-eq p1, v0, :cond_0

    .line 2405520
    iput-boolean p1, p0, LX/GvU;->g:Z

    .line 2405521
    invoke-direct {p0}, LX/GvU;->k()V

    .line 2405522
    :cond_0
    return-void
.end method

.method private declared-synchronized i()V
    .locals 2

    .prologue
    .line 2405515
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/GvU;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GvI;

    .line 2405516
    invoke-interface {v0}, LX/GvI;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2405517
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2405518
    :cond_0
    monitor-exit p0

    return-void
.end method

.method public static declared-synchronized j(LX/GvU;)V
    .locals 2

    .prologue
    .line 2405481
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/GvU;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GvI;

    .line 2405482
    invoke-interface {v0}, LX/GvI;->lU_()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2405483
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2405484
    :cond_0
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized k()V
    .locals 2

    .prologue
    .line 2405503
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/GvU;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GvI;

    .line 2405504
    invoke-interface {v0}, LX/GvI;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2405505
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2405506
    :cond_0
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final a(LX/0if;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 2405534
    iget-object v0, p0, LX/GvU;->d:Lcom/facebook/katana/gdp/LoginModel;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, LX/45Y;->a(Z)V

    .line 2405535
    invoke-static {p0, v1}, LX/GvU;->a$redex0(LX/GvU;Z)V

    .line 2405536
    new-instance v0, LX/Gvd;

    invoke-direct {v0}, LX/Gvd;-><init>()V

    move-object v1, v0

    .line 2405537
    new-instance v2, LX/4Ir;

    invoke-direct {v2}, LX/4Ir;-><init>()V

    .line 2405538
    iget-object v0, p0, LX/GvU;->d:Lcom/facebook/katana/gdp/LoginModel;

    .line 2405539
    iget-object v3, v0, Lcom/facebook/katana/gdp/LoginModel;->b:Ljava/lang/String;

    move-object v0, v3

    .line 2405540
    const-string v3, "app_id"

    invoke-virtual {v2, v3, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405541
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2405542
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2405543
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2405544
    iget-object v0, p0, LX/GvU;->d:Lcom/facebook/katana/gdp/LoginModel;

    invoke-virtual {v0}, Lcom/facebook/katana/gdp/LoginModel;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/katana/gdp/PermissionItem;

    .line 2405545
    iget-boolean v7, v0, Lcom/facebook/katana/gdp/PermissionItem;->f:Z

    move v7, v7

    .line 2405546
    if-eqz v7, :cond_1

    .line 2405547
    iget-object v7, v0, Lcom/facebook/katana/gdp/PermissionItem;->b:LX/GvY;

    move-object v7, v7

    .line 2405548
    invoke-virtual {v7}, LX/GvY;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2405549
    :goto_2
    iget-object v7, v0, Lcom/facebook/katana/gdp/PermissionItem;->b:LX/GvY;

    move-object v0, v7

    .line 2405550
    invoke-virtual {v0}, LX/GvY;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2405551
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2405552
    :cond_1
    iget-object v7, v0, Lcom/facebook/katana/gdp/PermissionItem;->b:LX/GvY;

    move-object v7, v7

    .line 2405553
    invoke-virtual {v7}, LX/GvY;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2405554
    :cond_2
    const-string v0, "permissions"

    invoke-virtual {v2, v0, v5}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2405555
    const-string v0, "granted_permissions"

    invoke-virtual {v2, v0, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 2405556
    const-string v0, "params"

    invoke-virtual {v1, v0, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2405557
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2405558
    iget-object v1, p0, LX/GvU;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2405559
    new-instance v1, LX/GvT;

    invoke-direct {v1, p0, v3, v4, p1}, LX/GvT;-><init>(LX/GvU;Ljava/util/List;Ljava/util/List;LX/0if;)V

    iget-object v2, p0, LX/GvU;->b:Ljava/util/concurrent/Executor;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2405560
    return-void
.end method

.method public final a(LX/3rL;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3rL",
            "<",
            "Ljava/lang/Integer;",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2405499
    iget-object v0, p0, LX/GvU;->f:LX/3rL;

    invoke-static {v0, p1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2405500
    iput-object p1, p0, LX/GvU;->f:LX/3rL;

    .line 2405501
    invoke-direct {p0}, LX/GvU;->i()V

    .line 2405502
    :cond_0
    return-void
.end method

.method public final declared-synchronized a(LX/GvI;)V
    .locals 1

    .prologue
    .line 2405496
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/GvU;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2405497
    monitor-exit p0

    return-void

    .line 2405498
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(LX/GvI;)V
    .locals 1

    .prologue
    .line 2405493
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/GvU;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2405494
    monitor-exit p0

    return-void

    .line 2405495
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/katana/gdp/PermissionItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2405490
    iget-object v0, p0, LX/GvU;->d:Lcom/facebook/katana/gdp/LoginModel;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/45Y;->a(Z)V

    .line 2405491
    iget-object v0, p0, LX/GvU;->d:Lcom/facebook/katana/gdp/LoginModel;

    invoke-virtual {v0}, Lcom/facebook/katana/gdp/LoginModel;->a()Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 2405492
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2405485
    iget-object v0, p0, LX/GvU;->d:Lcom/facebook/katana/gdp/LoginModel;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/45Y;->a(Z)V

    .line 2405486
    iget-object v0, p0, LX/GvU;->d:Lcom/facebook/katana/gdp/LoginModel;

    .line 2405487
    iget-object p0, v0, Lcom/facebook/katana/gdp/LoginModel;->c:Ljava/lang/String;

    move-object v0, p0

    .line 2405488
    return-object v0

    .line 2405489
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
