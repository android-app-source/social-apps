.class public LX/GiS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0SG;

.field public final b:LX/0tX;

.field public final c:LX/1vi;

.field public final d:LX/2iz;

.field public final e:Lcom/facebook/reaction/ReactionUtil;

.field public final f:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0SG;LX/0tX;LX/1vi;LX/2iz;Lcom/facebook/reaction/ReactionUtil;LX/1Ck;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2385896
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2385897
    iput-object p1, p0, LX/GiS;->a:LX/0SG;

    .line 2385898
    iput-object p2, p0, LX/GiS;->b:LX/0tX;

    .line 2385899
    iput-object p3, p0, LX/GiS;->c:LX/1vi;

    .line 2385900
    iput-object p4, p0, LX/GiS;->d:LX/2iz;

    .line 2385901
    iput-object p5, p0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2385902
    iput-object p6, p0, LX/GiS;->f:LX/1Ck;

    .line 2385903
    return-void
.end method

.method public static a(LX/0Px;)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2385892
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;

    .line 2385893
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;->j()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->k()I

    move-result v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 2385894
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2385895
    :cond_0
    return v2
.end method

.method public static a(LX/2jY;)I
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 2385884
    if-nez p0, :cond_0

    move v0, v1

    .line 2385885
    :goto_0
    return v0

    .line 2385886
    :cond_0
    invoke-virtual {p0}, LX/2jY;->o()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v1

    move v2, v1

    :goto_1
    if-ge v4, v6, :cond_2

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9qT;

    .line 2385887
    invoke-interface {v0}, LX/9qT;->a()LX/0Px;

    move-result-object v7

    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v3, v2

    move v2, v1

    :goto_2
    if-ge v2, v8, :cond_1

    invoke-virtual {v7, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;

    .line 2385888
    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionStoriesModel$EdgesModel;->b()Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/reaction/protocol/graphql/FetchReactionGraphQLModels$ReactionUnitFragmentModel;->k()I

    move-result v0

    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 2385889
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 2385890
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v2, v3

    goto :goto_1

    :cond_2
    move v0, v2

    .line 2385891
    goto :goto_0
.end method

.method public static a(LX/GiS;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)LX/4FD;
    .locals 6
    .param p0    # LX/GiS;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 2385869
    new-instance v0, LX/4J3;

    invoke-direct {v0}, LX/4J3;-><init>()V

    const-string v1, "normal"

    invoke-virtual {v0, v1}, LX/4J3;->b(Ljava/lang/String;)LX/4J3;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/4J3;->a(Ljava/lang/String;)LX/4J3;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/4J3;->c(Ljava/lang/String;)LX/4J3;

    move-result-object v0

    iget-object v1, p0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2385870
    iget-object v2, v1, Lcom/facebook/reaction/ReactionUtil;->j:LX/0Px;

    move-object v1, v2

    .line 2385871
    invoke-virtual {v0, v1}, LX/4J3;->a(Ljava/util/List;)LX/4J3;

    move-result-object v0

    iget-object v1, p0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2385872
    iget-object v2, v1, Lcom/facebook/reaction/ReactionUtil;->t:LX/0Px;

    move-object v1, v2

    .line 2385873
    invoke-virtual {v0, v1}, LX/4J3;->e(Ljava/util/List;)LX/4J3;

    move-result-object v0

    iget-object v1, p0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2385874
    iget-object v2, v1, Lcom/facebook/reaction/ReactionUtil;->m:LX/1vj;

    move-object v1, v2

    .line 2385875
    invoke-virtual {v1}, LX/1vj;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4J3;->d(Ljava/util/List;)LX/4J3;

    move-result-object v0

    iget-object v1, p0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2385876
    iget-object v2, v1, Lcom/facebook/reaction/ReactionUtil;->r:LX/1s6;

    move-object v1, v2

    .line 2385877
    invoke-virtual {v1, p1}, LX/1s6;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4J3;->b(Ljava/util/List;)LX/4J3;

    move-result-object v0

    iget-object v1, p0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2385878
    iget-object v2, v1, Lcom/facebook/reaction/ReactionUtil;->w:LX/1vo;

    move-object v1, v2

    .line 2385879
    invoke-virtual {v1}, LX/1vo;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4J3;->c(Ljava/util/List;)LX/4J3;

    move-result-object v0

    iget-object v1, p0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    invoke-virtual {v1}, Lcom/facebook/reaction/ReactionUtil;->f()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4J3;->f(Ljava/util/List;)LX/4J3;

    move-result-object v0

    .line 2385880
    new-instance v1, LX/4FD;

    invoke-direct {v1}, LX/4FD;-><init>()V

    invoke-virtual {v1, p2}, LX/4FD;->a(Ljava/lang/String;)LX/4FD;

    move-result-object v1

    invoke-virtual {p4}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    .line 2385881
    const-string v3, "date"

    invoke-virtual {v1, v3, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2385882
    move-object v1, v1

    .line 2385883
    invoke-virtual {v1, v0}, LX/4FD;->a(LX/4J3;)LX/4FD;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/GiS;Ljava/lang/String;Ljava/lang/String;)LX/4FJ;
    .locals 2
    .param p0    # LX/GiS;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 2385853
    const-string v0, "normal"

    .line 2385854
    new-instance v1, LX/4J3;

    invoke-direct {v1}, LX/4J3;-><init>()V

    invoke-virtual {v1, v0}, LX/4J3;->b(Ljava/lang/String;)LX/4J3;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/4J3;->a(Ljava/lang/String;)LX/4J3;

    move-result-object v0

    invoke-virtual {v0, p2}, LX/4J3;->c(Ljava/lang/String;)LX/4J3;

    move-result-object v0

    iget-object v1, p0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2385855
    iget-object p2, v1, Lcom/facebook/reaction/ReactionUtil;->j:LX/0Px;

    move-object v1, p2

    .line 2385856
    invoke-virtual {v0, v1}, LX/4J3;->a(Ljava/util/List;)LX/4J3;

    move-result-object v0

    iget-object v1, p0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2385857
    iget-object p2, v1, Lcom/facebook/reaction/ReactionUtil;->t:LX/0Px;

    move-object v1, p2

    .line 2385858
    invoke-virtual {v0, v1}, LX/4J3;->e(Ljava/util/List;)LX/4J3;

    move-result-object v0

    iget-object v1, p0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2385859
    iget-object p2, v1, Lcom/facebook/reaction/ReactionUtil;->m:LX/1vj;

    move-object v1, p2

    .line 2385860
    invoke-virtual {v1}, LX/1vj;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4J3;->d(Ljava/util/List;)LX/4J3;

    move-result-object v0

    iget-object v1, p0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2385861
    iget-object p2, v1, Lcom/facebook/reaction/ReactionUtil;->r:LX/1s6;

    move-object v1, p2

    .line 2385862
    invoke-virtual {v1, p1}, LX/1s6;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4J3;->b(Ljava/util/List;)LX/4J3;

    move-result-object v0

    iget-object v1, p0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2385863
    iget-object p1, v1, Lcom/facebook/reaction/ReactionUtil;->w:LX/1vo;

    move-object v1, p1

    .line 2385864
    invoke-virtual {v1}, LX/1vo;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4J3;->c(Ljava/util/List;)LX/4J3;

    move-result-object v0

    iget-object v1, p0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    invoke-virtual {v1}, Lcom/facebook/reaction/ReactionUtil;->f()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4J3;->f(Ljava/util/List;)LX/4J3;

    move-result-object v0

    .line 2385865
    new-instance v1, LX/4FJ;

    invoke-direct {v1}, LX/4FJ;-><init>()V

    .line 2385866
    const-string p0, "reaction_context"

    invoke-virtual {v1, p0, v0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2385867
    move-object v0, v1

    .line 2385868
    return-object v0
.end method

.method public static b(LX/GiS;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/4FC;
    .locals 2
    .param p0    # LX/GiS;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param

    .prologue
    .line 2385904
    new-instance v0, LX/4J3;

    invoke-direct {v0}, LX/4J3;-><init>()V

    const-string v1, "normal"

    invoke-virtual {v0, v1}, LX/4J3;->b(Ljava/lang/String;)LX/4J3;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/4J3;->a(Ljava/lang/String;)LX/4J3;

    move-result-object v0

    invoke-virtual {v0, p3}, LX/4J3;->c(Ljava/lang/String;)LX/4J3;

    move-result-object v0

    iget-object v1, p0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2385905
    iget-object p3, v1, Lcom/facebook/reaction/ReactionUtil;->j:LX/0Px;

    move-object v1, p3

    .line 2385906
    invoke-virtual {v0, v1}, LX/4J3;->a(Ljava/util/List;)LX/4J3;

    move-result-object v0

    iget-object v1, p0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2385907
    iget-object p3, v1, Lcom/facebook/reaction/ReactionUtil;->t:LX/0Px;

    move-object v1, p3

    .line 2385908
    invoke-virtual {v0, v1}, LX/4J3;->e(Ljava/util/List;)LX/4J3;

    move-result-object v0

    iget-object v1, p0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2385909
    iget-object p3, v1, Lcom/facebook/reaction/ReactionUtil;->m:LX/1vj;

    move-object v1, p3

    .line 2385910
    invoke-virtual {v1}, LX/1vj;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4J3;->d(Ljava/util/List;)LX/4J3;

    move-result-object v0

    iget-object v1, p0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2385911
    iget-object p3, v1, Lcom/facebook/reaction/ReactionUtil;->r:LX/1s6;

    move-object v1, p3

    .line 2385912
    invoke-virtual {v1, p1}, LX/1s6;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4J3;->b(Ljava/util/List;)LX/4J3;

    move-result-object v0

    iget-object v1, p0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    .line 2385913
    iget-object p1, v1, Lcom/facebook/reaction/ReactionUtil;->w:LX/1vo;

    move-object v1, p1

    .line 2385914
    invoke-virtual {v1}, LX/1vo;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4J3;->c(Ljava/util/List;)LX/4J3;

    move-result-object v0

    iget-object v1, p0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    invoke-virtual {v1}, Lcom/facebook/reaction/ReactionUtil;->f()LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4J3;->f(Ljava/util/List;)LX/4J3;

    move-result-object v0

    .line 2385915
    new-instance v1, LX/4FC;

    invoke-direct {v1}, LX/4FC;-><init>()V

    .line 2385916
    const-string p0, "page_id"

    invoke-virtual {v1, p0, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2385917
    move-object v1, v1

    .line 2385918
    const-string p0, "reaction_context"

    invoke-virtual {v1, p0, v0}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2385919
    move-object v0, v1

    .line 2385920
    return-object v0
.end method

.method public static b(LX/0QB;)LX/GiS;
    .locals 7

    .prologue
    .line 2385851
    new-instance v0, LX/GiS;

    invoke-static {p0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v1

    check-cast v1, LX/0SG;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {p0}, LX/1vi;->a(LX/0QB;)LX/1vi;

    move-result-object v3

    check-cast v3, LX/1vi;

    invoke-static {p0}, LX/2iz;->a(LX/0QB;)LX/2iz;

    move-result-object v4

    check-cast v4, LX/2iz;

    invoke-static {p0}, Lcom/facebook/reaction/ReactionUtil;->b(LX/0QB;)Lcom/facebook/reaction/ReactionUtil;

    move-result-object v5

    check-cast v5, Lcom/facebook/reaction/ReactionUtil;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-direct/range {v0 .. v6}, LX/GiS;-><init>(LX/0SG;LX/0tX;LX/1vi;LX/2iz;Lcom/facebook/reaction/ReactionUtil;LX/1Ck;)V

    .line 2385852
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;LX/GiR;LX/GiJ;)LX/2jY;
    .locals 10
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p4    # LX/GiJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x0

    .line 2385803
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2385804
    iget-object v0, p0, LX/GiS;->d:LX/2iz;

    invoke-virtual {v0, v3, p1}, LX/2iz;->b(Ljava/lang/String;Ljava/lang/String;)LX/2jY;

    move-result-object v9

    .line 2385805
    const/4 v0, 0x1

    .line 2385806
    iput-boolean v0, v9, LX/2jY;->w:Z

    .line 2385807
    new-instance v0, Lcom/facebook/gametime/ui/reaction/GametimeReactionUtil$1;

    move-object v1, p0

    move-object v2, p2

    move-object v4, p1

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/facebook/gametime/ui/reaction/GametimeReactionUtil$1;-><init>(LX/GiS;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/GiR;LX/GiJ;)V

    .line 2385808
    iput-object v0, v9, LX/2jY;->A:Ljava/lang/Runnable;

    .line 2385809
    iget-object v0, p0, LX/GiS;->d:LX/2iz;

    invoke-virtual {v0, v3}, LX/2iz;->c(Ljava/lang/String;)V

    .line 2385810
    iget-object v0, p0, LX/GiS;->d:LX/2iz;

    invoke-virtual {v0, v3}, LX/2iz;->d(Ljava/lang/String;)V

    move-object v1, p0

    move-object v2, p2

    move-object v4, p1

    move-object v5, p3

    move-object v6, v8

    move-object v7, v8

    move-object v8, p4

    .line 2385811
    invoke-virtual/range {v1 .. v8}, LX/GiS;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/GiR;Ljava/lang/String;Ljava/lang/String;LX/GiJ;)V

    .line 2385812
    return-object v9
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;LX/GiR;Ljava/util/Date;LX/GiJ;)LX/2jY;
    .locals 11
    .param p1    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p5    # LX/GiJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/GiR;",
            "Ljava/util/Date;",
            "LX/GiJ;",
            ")",
            "LX/2jY;"
        }
    .end annotation

    .prologue
    .line 2385843
    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2385844
    iget-object v0, p0, LX/GiS;->d:LX/2iz;

    invoke-virtual {v0, v3, p1}, LX/2iz;->b(Ljava/lang/String;Ljava/lang/String;)LX/2jY;

    move-result-object v10

    .line 2385845
    const/4 v0, 0x1

    invoke-virtual {v10, v0}, LX/2jY;->f(Z)V

    .line 2385846
    new-instance v0, Lcom/facebook/gametime/ui/reaction/GametimeReactionUtil$3;

    move-object v1, p0

    move-object v2, p2

    move-object v4, p4

    move-object v5, p1

    move-object v6, p3

    move-object/from16 v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/facebook/gametime/ui/reaction/GametimeReactionUtil$3;-><init>(LX/GiS;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;LX/GiR;LX/GiJ;)V

    invoke-virtual {v10, v0}, LX/2jY;->a(Ljava/lang/Runnable;)V

    .line 2385847
    iget-object v0, p0, LX/GiS;->d:LX/2iz;

    invoke-virtual {v0, v3}, LX/2iz;->c(Ljava/lang/String;)V

    .line 2385848
    iget-object v0, p0, LX/GiS;->d:LX/2iz;

    invoke-virtual {v0, v3}, LX/2iz;->d(Ljava/lang/String;)V

    .line 2385849
    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, p0

    move-object v2, p2

    move-object v4, p4

    move-object v5, p1

    move-object v6, p3

    move-object/from16 v9, p5

    invoke-virtual/range {v1 .. v9}, LX/GiS;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;LX/GiR;Ljava/lang/String;Ljava/lang/String;LX/GiJ;)V

    .line 2385850
    return-object v10
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/GiR;Ljava/lang/String;Ljava/lang/String;LX/GiJ;)V
    .locals 6
    .param p3    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/GiJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2385828
    sget-object v0, LX/GiR;->HEAD_LOAD:LX/GiR;

    if-ne p4, v0, :cond_0

    move-object v0, p0

    move-object v1, p6

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p7

    .line 2385829
    new-instance p0, LX/Ggc;

    invoke-direct {p0}, LX/Ggc;-><init>()V

    move-object p0, p0

    .line 2385830
    iget-object p1, v0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    invoke-virtual {p1, p0, v4}, Lcom/facebook/reaction/ReactionUtil;->a(LX/0gW;Ljava/lang/String;)V

    .line 2385831
    const-string p1, "beforeCursor"

    invoke-virtual {p0, p1, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p1

    const-string p2, "count"

    const/16 p3, 0xa

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object p1

    const-string p2, "trigger_data"

    invoke-static {v0, v4, v2, v3}, LX/GiS;->b(LX/GiS;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/4FC;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2385832
    invoke-static {p0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object p0

    sget-object p1, LX/0zS;->c:LX/0zS;

    invoke-virtual {p0, p1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object p0

    .line 2385833
    iget-object p1, v0, LX/GiS;->f:LX/1Ck;

    iget-object p2, v0, LX/GiS;->b:LX/0tX;

    invoke-virtual {p2, p0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object p0

    new-instance p2, LX/GiQ;

    invoke-direct {p2, v0, v3, v5}, LX/GiQ;-><init>(LX/GiS;Ljava/lang/String;LX/GiJ;)V

    invoke-virtual {p1, v3, p0, p2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2385834
    iget-object p0, v0, LX/GiS;->c:LX/1vi;

    new-instance p1, LX/2jX;

    const/4 p2, 0x0

    invoke-direct {p1, v3, p2}, LX/2jX;-><init>(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)V

    invoke-virtual {p0, p1}, LX/0b4;->a(LX/0b7;)V

    .line 2385835
    :goto_0
    return-void

    :cond_0
    move-object v0, p0

    move-object v1, p5

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p7

    .line 2385836
    new-instance p0, LX/Gge;

    invoke-direct {p0}, LX/Gge;-><init>()V

    move-object p0, p0

    .line 2385837
    iget-object p1, v0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    invoke-virtual {p1, p0, v4}, Lcom/facebook/reaction/ReactionUtil;->a(LX/0gW;Ljava/lang/String;)V

    .line 2385838
    const-string p1, "afterCursor"

    invoke-virtual {p0, p1, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p1

    const-string p2, "count"

    const/16 p3, 0xa

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object p1

    const-string p2, "trigger_data"

    invoke-static {v0, v4, v2, v3}, LX/GiS;->b(LX/GiS;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/4FC;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2385839
    invoke-static {p0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object p0

    sget-object p1, LX/0zS;->c:LX/0zS;

    invoke-virtual {p0, p1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object p0

    .line 2385840
    iget-object p1, v0, LX/GiS;->f:LX/1Ck;

    iget-object p2, v0, LX/GiS;->b:LX/0tX;

    invoke-virtual {p2, p0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object p0

    new-instance p2, LX/GiQ;

    invoke-direct {p2, v0, v3, v5}, LX/GiQ;-><init>(LX/GiS;Ljava/lang/String;LX/GiJ;)V

    invoke-virtual {p1, v3, p0, p2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2385841
    iget-object p0, v0, LX/GiS;->c:LX/1vi;

    new-instance p1, LX/2jX;

    const/4 p2, 0x0

    invoke-direct {p1, v3, p2}, LX/2jX;-><init>(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)V

    invoke-virtual {p0, p1}, LX/0b4;->a(LX/0b7;)V

    .line 2385842
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;Ljava/lang/String;LX/GiR;Ljava/lang/String;Ljava/lang/String;LX/GiJ;)V
    .locals 7
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/ReactionSurface;
        .end annotation
    .end param
    .param p6    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/GiJ;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/Date;",
            "Ljava/lang/String;",
            "LX/GiR;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "LX/GiJ;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2385813
    sget-object v0, LX/GiR;->HEAD_LOAD:LX/GiR;

    if-ne p5, v0, :cond_0

    move-object v0, p0

    move-object v1, p7

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p8

    .line 2385814
    new-instance p0, LX/GgV;

    invoke-direct {p0}, LX/GgV;-><init>()V

    move-object p0, p0

    .line 2385815
    iget-object p1, v0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    invoke-virtual {p1, p0, v5}, Lcom/facebook/reaction/ReactionUtil;->a(LX/0gW;Ljava/lang/String;)V

    .line 2385816
    const-string p1, "beforeCursor"

    invoke-virtual {p0, p1, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p1

    const-string p2, "count"

    const/16 p3, 0xa

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object p1

    const-string p2, "trigger_data"

    invoke-static {v0, v5, v2, v3, v4}, LX/GiS;->a(LX/GiS;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)LX/4FD;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2385817
    invoke-static {p0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object p0

    sget-object p1, LX/0zS;->c:LX/0zS;

    invoke-virtual {p0, p1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object p0

    .line 2385818
    iget-object p1, v0, LX/GiS;->f:LX/1Ck;

    iget-object p2, v0, LX/GiS;->b:LX/0tX;

    invoke-virtual {p2, p0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object p0

    new-instance p2, LX/GiQ;

    invoke-direct {p2, v0, v3, v6}, LX/GiQ;-><init>(LX/GiS;Ljava/lang/String;LX/GiJ;)V

    invoke-virtual {p1, v3, p0, p2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2385819
    iget-object p0, v0, LX/GiS;->c:LX/1vi;

    new-instance p1, LX/2jX;

    const/4 p2, 0x0

    invoke-direct {p1, v3, p2}, LX/2jX;-><init>(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)V

    invoke-virtual {p0, p1}, LX/0b4;->a(LX/0b7;)V

    .line 2385820
    :goto_0
    return-void

    :cond_0
    move-object v0, p0

    move-object v1, p6

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p8

    .line 2385821
    new-instance p0, LX/GgX;

    invoke-direct {p0}, LX/GgX;-><init>()V

    move-object p0, p0

    .line 2385822
    iget-object p1, v0, LX/GiS;->e:Lcom/facebook/reaction/ReactionUtil;

    invoke-virtual {p1, p0, v5}, Lcom/facebook/reaction/ReactionUtil;->a(LX/0gW;Ljava/lang/String;)V

    .line 2385823
    const-string p1, "afterCursor"

    invoke-virtual {p0, p1, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object p1

    const-string p2, "count"

    const/16 p3, 0xa

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object p1

    const-string p2, "trigger_data"

    invoke-static {v0, v5, v2, v3, v4}, LX/GiS;->a(LX/GiS;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Date;)LX/4FD;

    move-result-object p3

    invoke-virtual {p1, p2, p3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2385824
    invoke-static {p0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object p0

    sget-object p1, LX/0zS;->c:LX/0zS;

    invoke-virtual {p0, p1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object p0

    .line 2385825
    iget-object p1, v0, LX/GiS;->f:LX/1Ck;

    iget-object p2, v0, LX/GiS;->b:LX/0tX;

    invoke-virtual {p2, p0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object p0

    new-instance p2, LX/GiQ;

    invoke-direct {p2, v0, v3, v6}, LX/GiQ;-><init>(LX/GiS;Ljava/lang/String;LX/GiJ;)V

    invoke-virtual {p1, v3, p0, p2}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2385826
    iget-object p0, v0, LX/GiS;->c:LX/1vi;

    new-instance p1, LX/2jX;

    const/4 p2, 0x0

    invoke-direct {p1, v3, p2}, LX/2jX;-><init>(Ljava/lang/String;Lcom/facebook/reaction/ReactionQueryParams;)V

    invoke-virtual {p0, p1}, LX/0b4;->a(LX/0b7;)V

    .line 2385827
    goto :goto_0
.end method
