.class public LX/F7y;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "mPageInfoFields"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field public b:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "mPageInfoFields"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation
.end field

.field private final c:LX/0rq;

.field private final d:LX/0tX;

.field private final e:LX/0TD;

.field private final f:LX/0dC;


# direct methods
.method public constructor <init>(LX/0rq;LX/0tX;LX/0TD;LX/0dC;)V
    .locals 3
    .param p3    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2202805
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2202806
    iput-object p1, p0, LX/F7y;->c:LX/0rq;

    .line 2202807
    iput-object p2, p0, LX/F7y;->d:LX/0tX;

    .line 2202808
    iput-object p3, p0, LX/F7y;->e:LX/0TD;

    .line 2202809
    iput-object p4, p0, LX/F7y;->f:LX/0dC;

    .line 2202810
    const/4 v2, 0x0

    const/4 p2, 0x1

    .line 2202811
    new-instance v0, LX/186;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, LX/186;-><init>(I)V

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    const/4 p1, 0x3

    invoke-virtual {v0, p1}, LX/186;->c(I)V

    const/4 p1, 0x0

    invoke-virtual {v0, p1, v1}, LX/186;->b(II)V

    invoke-virtual {v0, p2, p2}, LX/186;->a(IZ)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    const v1, -0x333c0da

    invoke-static {v0, v1}, LX/1vs;->a(LX/186;I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    move-object v0, v0

    .line 2202812
    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v2, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iput-object v1, p0, LX/F7y;->a:LX/15i;

    iput v0, p0, LX/F7y;->b:I

    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static b(LX/0QB;)LX/F7y;
    .locals 5

    .prologue
    .line 2202803
    new-instance v4, LX/F7y;

    invoke-static {p0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v0

    check-cast v0, LX/0rq;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, LX/0TD;

    invoke-static {p0}, LX/0dB;->b(LX/0QB;)LX/0dC;

    move-result-object v3

    check-cast v3, LX/0dC;

    invoke-direct {v4, v0, v1, v2, v3}, LX/F7y;-><init>(LX/0rq;LX/0tX;LX/0TD;LX/0dC;)V

    .line 2202804
    return-object v4
.end method


# virtual methods
.method public final a(II)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/growth/friendfinder/graphql/FetchFriendableContactsQueryGraphQLModels$FetchFriendableContactsQueryModel$AddressbooksModel$EdgesModel$NodeModel$FriendableContactsModel$NodesModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    .line 2202793
    new-instance v0, LX/F81;

    invoke-direct {v0}, LX/F81;-><init>()V

    move-object v0, v0

    .line 2202794
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, LX/F7y;->a:LX/15i;

    iget v3, p0, LX/F7y;->b:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2202795
    const-string v1, "for_this_device"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v1

    const-string v4, "first_devices"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v1

    const-string v4, "phone_id"

    iget-object v5, p0, LX/F7y;->f:LX/0dC;

    invoke-virtual {v5}, LX/0dC;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v4, "first_contacts"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v4, "size_param"

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v4, "after"

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v1

    const-string v2, "media_type"

    iget-object v3, p0, LX/F7y;->c:LX/0rq;

    invoke-virtual {v3}, LX/0rq;->b()LX/0wF;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2202796
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2202797
    sget-object v1, LX/0zS;->c:LX/0zS;

    .line 2202798
    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    .line 2202799
    iput-boolean v6, v1, LX/0zO;->p:Z

    .line 2202800
    iget-object v1, p0, LX/F7y;->d:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2202801
    new-instance v1, LX/F7x;

    invoke-direct {v1, p0}, LX/F7x;-><init>(LX/F7y;)V

    iget-object v2, p0, LX/F7y;->e:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0

    .line 2202802
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 2202792
    sget-object v1, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, LX/F7y;->a:LX/15i;

    iget v2, p0, LX/F7y;->b:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, LX/15i;->h(II)Z

    move-result v0

    return v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
