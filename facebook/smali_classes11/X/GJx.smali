.class public LX/GJx;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

.field public b:Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2339983
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2339984
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2339985
    invoke-super {p0}, LX/GHg;->a()V

    .line 2339986
    const/4 v0, 0x0

    iput-object v0, p0, LX/GJx;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;

    .line 2339987
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 2

    .prologue
    .line 2339988
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;

    .line 2339989
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2339990
    iput-object p1, p0, LX/GJx;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;

    .line 2339991
    iget-object v0, p0, LX/GJx;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result p1

    .line 2339992
    if-eqz p1, :cond_0

    const v0, 0x7f080ae7

    move v1, v0

    .line 2339993
    :goto_0
    if-eqz p1, :cond_1

    const v0, 0x7f080b74

    .line 2339994
    :goto_1
    iget-object p1, p0, LX/GJx;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;

    iget-object p2, p0, LX/GJx;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;

    invoke-virtual {p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;->getContext()Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;->setCreateButtonText(Ljava/lang/String;)V

    .line 2339995
    iget-object v1, p0, LX/GJx;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;

    iget-object p1, p0, LX/GJx;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;->setBoostText(Ljava/lang/String;)V

    .line 2339996
    iget-object v0, p0, LX/GJx;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;

    .line 2339997
    new-instance v1, LX/GJw;

    invoke-direct {v1, p0}, LX/GJw;-><init>(LX/GJx;)V

    move-object v1, v1

    .line 2339998
    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;->setCreadAdIconButtonListener(Landroid/view/View$OnClickListener;)V

    .line 2339999
    iget-object v0, p0, LX/GJx;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesIconButtonFooterView;->setVisibility(I)V

    .line 2340000
    return-void

    .line 2340001
    :cond_0
    iget-object v0, p0, LX/GJx;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->D()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "title"

    const/4 p2, -0x1

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    move v1, v0

    goto :goto_0

    .line 2340002
    :cond_1
    const v0, 0x7f080b73

    goto :goto_1
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2340003
    iput-object p1, p0, LX/GJx;->a:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2340004
    return-void
.end method
