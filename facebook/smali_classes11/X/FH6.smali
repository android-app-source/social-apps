.class public final LX/FH6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/ui/media/attachments/MediaUploadResult;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/ListenableFuture;

.field public final synthetic b:Lcom/facebook/ui/media/attachments/MediaResource;

.field public final synthetic c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/google/common/util/concurrent/ListenableFuture;Lcom/facebook/ui/media/attachments/MediaResource;)V
    .locals 0

    .prologue
    .line 2219854
    iput-object p1, p0, LX/FH6;->c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iput-object p2, p0, LX/FH6;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object p3, p0, LX/FH6;->b:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 3

    .prologue
    .line 2219855
    check-cast p1, Lcom/facebook/ui/media/attachments/MediaUploadResult;

    .line 2219856
    if-nez p1, :cond_0

    .line 2219857
    const/4 v0, 0x0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2219858
    :goto_0
    return-object v0

    .line 2219859
    :cond_0
    iget-object v0, p0, LX/FH6;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    if-eqz v0, :cond_1

    .line 2219860
    iget-object v0, p0, LX/FH6;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-static {v0}, LX/0Sa;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    .line 2219861
    :cond_1
    new-instance v0, LX/5zn;

    invoke-direct {v0}, LX/5zn;-><init>()V

    .line 2219862
    iget-object v1, p0, LX/FH6;->b:Lcom/facebook/ui/media/attachments/MediaResource;

    invoke-virtual {v0, v1}, LX/5zn;->a(Lcom/facebook/ui/media/attachments/MediaResource;)LX/5zn;

    .line 2219863
    iput-object p1, v0, LX/5zn;->w:Lcom/facebook/ui/media/attachments/MediaUploadResult;

    .line 2219864
    invoke-virtual {v0}, LX/5zn;->G()Lcom/facebook/ui/media/attachments/MediaResource;

    move-result-object v0

    .line 2219865
    iget-object v1, p1, Lcom/facebook/ui/media/attachments/MediaUploadResult;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2219866
    iget-object v2, p0, LX/FH6;->c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    iget-object v2, v2, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->M:LX/2OC;

    invoke-virtual {v2, v0, v1}, LX/2OC;->a(Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)V

    .line 2219867
    iget-object v2, p0, LX/FH6;->c:Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;

    invoke-static {v2, v0, v1}, Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;->b(Lcom/facebook/messaging/media/upload/MediaUploadManagerImpl;Lcom/facebook/ui/media/attachments/MediaResource;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
