.class public final LX/FzA;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;)V
    .locals 0

    .prologue
    .line 2307629
    iput-object p1, p0, LX/FzA;->a:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2307630
    iget-object v0, p0, LX/FzA;->a:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;

    iget-object v1, v0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->l:LX/Fym;

    iget-object v0, p0, LX/FzA;->a:Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;

    iget-object v0, v0, Lcom/facebook/timeline/inforeview/profilequestion/ui/ProfileQuestionPrivacySelectorView;->j:LX/FzB;

    invoke-virtual {v0, p3}, LX/3Tf;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    invoke-interface {v1, v0}, LX/Fym;->a(Lcom/facebook/graphql/model/GraphQLPrivacyOption;)V

    .line 2307631
    return-void
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2307632
    return-void
.end method
