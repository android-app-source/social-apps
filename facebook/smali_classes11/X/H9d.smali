.class public final LX/H9d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5OE;


# instance fields
.field public final synthetic a:LX/H9g;


# direct methods
.method public constructor <init>(LX/H9g;)V
    .locals 0

    .prologue
    .line 2434483
    iput-object p1, p0, LX/H9d;->a:LX/H9g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/MenuItem;)Z
    .locals 12

    .prologue
    const/4 v3, 0x1

    .line 2434484
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    if-nez v0, :cond_0

    .line 2434485
    iget-object v0, p0, LX/H9d;->a:LX/H9g;

    sget-object v1, LX/8Dz;->OFFENSIVE:LX/8Dz;

    invoke-static {v0, v1}, LX/H9g;->a$redex0(LX/H9g;LX/8Dz;)V

    .line 2434486
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    if-ne v0, v3, :cond_1

    .line 2434487
    iget-object v0, p0, LX/H9d;->a:LX/H9g;

    sget-object v1, LX/8Dz;->NOT_PUBLIC:LX/8Dz;

    invoke-static {v0, v1}, LX/H9g;->a$redex0(LX/H9g;LX/8Dz;)V

    .line 2434488
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 2434489
    iget-object v0, p0, LX/H9d;->a:LX/H9g;

    iget-object v0, v0, LX/H9g;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8W;

    iget-object v1, p0, LX/H9d;->a:LX/H9g;

    iget-object v1, v1, LX/H9g;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    iget-object v2, p0, LX/H9d;->a:LX/H9g;

    iget-object v2, v2, LX/H9g;->i:Landroid/app/Activity;

    .line 2434490
    sget-object v4, LX/9XI;->EVENT_TAPPED_SUGGEST_EDIT:LX/9XI;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2434491
    const-string v9, ""

    .line 2434492
    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->x()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$ProfilePictureModel;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 2434493
    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->x()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$ProfilePictureModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v9

    .line 2434494
    :cond_2
    iget-object v4, v0, LX/H8W;->e:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/Bgf;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->s()Ljava/lang/String;

    move-result-object v8

    sget-object v10, LX/CdT;->FINCH_EDIT:LX/CdT;

    const-string v11, "android_page_action_menu_suggest_edits"

    invoke-virtual/range {v5 .. v11}, LX/Bgf;->a(JLjava/lang/String;Ljava/lang/String;LX/CdT;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    .line 2434495
    if-nez v5, :cond_5

    .line 2434496
    iget-object v4, v0, LX/H8W;->d:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/03V;

    const-string v5, "page_identity_suggest_edit_fail"

    const-string v6, "Failed to resolve suggest edits intent!"

    invoke-virtual {v4, v5, v6}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2434497
    :cond_3
    :goto_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 2434498
    iget-object v0, p0, LX/H9d;->a:LX/H9g;

    sget-object v1, LX/8Dz;->CLOSED:LX/8Dz;

    invoke-static {v0, v1}, LX/H9g;->a$redex0(LX/H9g;LX/8Dz;)V

    .line 2434499
    :cond_4
    return v3

    .line 2434500
    :cond_5
    iget-object v4, v0, LX/H8W;->f:LX/0Ot;

    invoke-interface {v4}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/content/SecureContextHelper;

    const/16 v6, 0x2776

    invoke-interface {v4, v5, v6, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0
.end method
