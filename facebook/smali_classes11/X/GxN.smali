.class public LX/GxN;
.super Lcom/facebook/webview/FacebookWebView;
.source ""


# static fields
.field private static final l:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public i:LX/BWW;

.field public j:LX/GxM;

.field public k:LX/03R;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2408082
    const-class v0, LX/GxN;

    sput-object v0, LX/GxN;->l:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2408080
    invoke-direct {p0, p1}, Lcom/facebook/webview/FacebookWebView;-><init>(Landroid/content/Context;)V

    .line 2408081
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 2408078
    invoke-direct {p0, p1, p2}, Lcom/facebook/webview/FacebookWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2408079
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 2408075
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/webview/FacebookWebView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2408076
    const-class v0, LX/GxN;

    invoke-static {v0, p0}, LX/GxN;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2408077
    return-void
.end method

.method public static synthetic a(LX/GxN;)Lcom/facebook/prefs/shared/FbSharedPreferences;
    .locals 1

    .prologue
    .line 2408073
    iget-object v0, p0, Lcom/facebook/webview/FacebookWebView;->g:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object v0, v0

    .line 2408074
    return-object v0
.end method

.method private a(LX/BWW;LX/03R;)V
    .locals 0
    .param p2    # LX/03R;
        .annotation runtime Lcom/facebook/auth/annotations/IsMeUserAnEmployee;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2408070
    iput-object p1, p0, LX/GxN;->i:LX/BWW;

    .line 2408071
    iput-object p2, p0, LX/GxN;->k:LX/03R;

    .line 2408072
    return-void
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/GxN;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p0, LX/GxN;

    invoke-static {v1}, LX/Gws;->a(LX/0QB;)LX/Gws;

    move-result-object v0

    check-cast v0, LX/BWW;

    invoke-static {v1}, LX/0XD;->b(LX/0QB;)LX/03R;

    move-result-object v1

    check-cast v1, LX/03R;

    invoke-direct {p0, v0, v1}, LX/GxN;->a(LX/BWW;LX/03R;)V

    return-void
.end method

.method public static synthetic b(LX/GxN;)LX/44G;
    .locals 1

    .prologue
    .line 2408068
    iget-object v0, p0, Lcom/facebook/webview/BasicWebView;->a:LX/44G;

    move-object v0, v0

    .line 2408069
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 2408061
    invoke-super {p0, p1}, Lcom/facebook/webview/FacebookWebView;->a(Landroid/content/Context;)V

    .line 2408062
    const/4 v0, 0x0

    iput-object v0, p0, LX/GxN;->j:LX/GxM;

    .line 2408063
    invoke-virtual {p0}, LX/GxN;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 2408064
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/webkit/WebSettings;->getUserAgentString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {p1, v2}, LX/0sG;->a(Landroid/content/Context;Ljava/lang/Boolean;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/webkit/WebSettings;->setUserAgentString(Ljava/lang/String;)V

    .line 2408065
    new-instance v0, LX/GxL;

    invoke-direct {v0, p0, p1}, LX/GxL;-><init>(LX/GxN;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, LX/GxN;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 2408066
    new-instance v0, LX/GxJ;

    invoke-direct {v0, p0, p1}, LX/GxJ;-><init>(LX/GxN;Landroid/content/Context;)V

    invoke-virtual {p0, v0}, LX/GxN;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 2408067
    return-void
.end method

.method public setPageFinishedHandler(LX/GxM;)V
    .locals 0

    .prologue
    .line 2408059
    iput-object p1, p0, LX/GxN;->j:LX/GxM;

    .line 2408060
    return-void
.end method
