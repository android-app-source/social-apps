.class public final LX/GwH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Pz;

.field public final synthetic b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

.field public final synthetic c:LX/GwK;


# direct methods
.method public constructor <init>(LX/GwK;LX/0Pz;Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;)V
    .locals 0

    .prologue
    .line 2406812
    iput-object p1, p0, LX/GwH;->c:LX/GwK;

    iput-object p2, p0, LX/GwH;->a:LX/0Pz;

    iput-object p3, p0, LX/GwH;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2406813
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2406814
    if-eqz p1, :cond_0

    .line 2406815
    iget-boolean v0, p1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2406816
    if-nez v0, :cond_1

    .line 2406817
    :cond_0
    iget-object v0, p0, LX/GwH;->c:LX/GwK;

    const-string v1, "Failed to copy image."

    invoke-virtual {v0, v1}, LX/Gvy;->a(Ljava/lang/String;)V

    .line 2406818
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Failed to copy image."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Throwable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2406819
    :goto_0
    return-object v0

    .line 2406820
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 2406821
    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2406822
    new-instance v3, Ljava/io/File;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2406823
    iget-object v1, p0, LX/GwH;->a:LX/0Pz;

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2406824
    :cond_2
    iget-object v0, p0, LX/GwH;->b:Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    iget-object v1, p0, LX/GwH;->a:LX/0Pz;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iget-object v2, p0, LX/GwH;->c:LX/GwK;

    iget-object v2, v2, LX/GwK;->q:LX/74n;

    invoke-static {v1, v2}, Lcom/facebook/composer/attachments/ComposerAttachment;->a(Ljava/util/Collection;LX/74n;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialAttachments(LX/0Px;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v0

    invoke-static {v0}, LX/0Vg;->a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0
.end method
