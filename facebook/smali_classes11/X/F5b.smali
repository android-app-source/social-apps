.class public final LX/F5b;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2198941
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2198942
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/F5b;->a:Landroid/os/Bundle;

    .line 2198943
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)LX/F5b;
    .locals 1

    .prologue
    .line 2198944
    iget-object v0, p0, LX/F5b;->a:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 2198945
    return-object p0
.end method

.method public final a()Lcom/facebook/fbreact/fragment/FbReactFragment;
    .locals 2

    .prologue
    .line 2198946
    new-instance v0, Lcom/facebook/groups/fb4a/react/GroupsReactFragment;

    invoke-direct {v0}, Lcom/facebook/groups/fb4a/react/GroupsReactFragment;-><init>()V

    .line 2198947
    iget-object v1, p0, LX/F5b;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2198948
    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)LX/F5b;
    .locals 3

    .prologue
    .line 2198949
    iget-object v0, p0, LX/F5b;->a:Landroid/os/Bundle;

    const-string v1, "init_props"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 2198950
    if-nez v0, :cond_0

    .line 2198951
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2198952
    :cond_0
    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 2198953
    iget-object v1, p0, LX/F5b;->a:Landroid/os/Bundle;

    const-string v2, "init_props"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2198954
    return-object p0
.end method
