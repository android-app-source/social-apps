.class public final LX/GrU;
.super Ljava/util/HashMap;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic this$0:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;


# direct methods
.method public constructor <init>(Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;)V
    .locals 4

    .prologue
    .line 2398061
    iput-object p1, p0, LX/GrU;->this$0:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    invoke-direct {p0}, Ljava/util/HashMap;-><init>()V

    .line 2398062
    const-string v0, "instant_shopping_scrubbbablegif_initialized_time_ms"

    iget-object v1, p0, LX/GrU;->this$0:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-wide v2, v1, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->z:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/GrU;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2398063
    const-string v0, "instant_shopping_scrubbbablegif_done_decoding_time_ms"

    iget-object v1, p0, LX/GrU;->this$0:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-wide v2, v1, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->A:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/GrU;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2398064
    const-string v0, "instant_shopping_scrubbablegif_block_id"

    iget-object v1, p0, LX/GrU;->this$0:Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    iget-object v1, v1, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->D:LX/GoE;

    invoke-virtual {v1}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, LX/GrU;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2398065
    return-void
.end method
