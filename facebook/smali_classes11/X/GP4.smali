.class public abstract LX/GP4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<V:",
        "LX/GQ7;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final a:LX/GNs;

.field public b:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:I

.field public final d:I

.field private final e:LX/GOy;

.field public final f:LX/GQ7;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TV;"
        }
    .end annotation
.end field

.field public g:I

.field public h:I

.field public i:I

.field public j:I

.field public k:Z

.field public l:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;


# direct methods
.method public constructor <init>(LX/GQ7;Landroid/content/res/Resources;LX/GOy;LX/GNs;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;",
            "Landroid/content/res/Resources;",
            "LX/GOy;",
            "LX/GNs;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2348350
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2348351
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GP4;->k:Z

    .line 2348352
    iput-object p1, p0, LX/GP4;->f:LX/GQ7;

    .line 2348353
    iput-object p3, p0, LX/GP4;->e:LX/GOy;

    .line 2348354
    iput-object p4, p0, LX/GP4;->a:LX/GNs;

    .line 2348355
    const v0, 0x7f0a00d3

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/GP4;->c:I

    .line 2348356
    const v0, 0x7f0a00d6

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/GP4;->d:I

    .line 2348357
    return-void
.end method

.method public static l(LX/GP4;)V
    .locals 5

    .prologue
    .line 2348348
    invoke-virtual {p0}, LX/GP4;->c()Landroid/widget/EditText;

    move-result-object v0

    iget v1, p0, LX/GP4;->g:I

    iget v2, p0, LX/GP4;->h:I

    iget v3, p0, LX/GP4;->i:I

    iget v4, p0, LX/GP4;->j:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/EditText;->setPadding(IIII)V

    .line 2348349
    return-void
.end method


# virtual methods
.method public abstract a(Landroid/view/View;)V
.end method

.method public final a(Landroid/view/View;Landroid/view/View;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V
    .locals 0
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2348338
    iput-object p2, p0, LX/GP4;->b:Landroid/view/View;

    .line 2348339
    iput-object p3, p0, LX/GP4;->l:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    .line 2348340
    invoke-virtual {p0, p1}, LX/GP4;->a(Landroid/view/View;)V

    .line 2348341
    invoke-virtual {p0}, LX/GP4;->c()Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/EditText;->getPaddingLeft()I

    move-result p1

    iput p1, p0, LX/GP4;->g:I

    .line 2348342
    invoke-virtual {p0}, LX/GP4;->c()Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/EditText;->getPaddingTop()I

    move-result p1

    iput p1, p0, LX/GP4;->h:I

    .line 2348343
    invoke-virtual {p0}, LX/GP4;->c()Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/EditText;->getPaddingRight()I

    move-result p1

    iput p1, p0, LX/GP4;->i:I

    .line 2348344
    invoke-virtual {p0}, LX/GP4;->c()Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/EditText;->getPaddingBottom()I

    move-result p1

    iput p1, p0, LX/GP4;->j:I

    .line 2348345
    iget-object p1, p0, LX/GP4;->b:Landroid/view/View;

    if-eqz p1, :cond_0

    .line 2348346
    invoke-virtual {p0}, LX/GP4;->c()Landroid/widget/EditText;

    move-result-object p1

    const/4 p2, 0x5

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 2348347
    :cond_0
    return-void
.end method

.method public final a(Ljava/util/concurrent/ExecutorService;)V
    .locals 2

    .prologue
    .line 2348334
    iget-object v0, p0, LX/GP4;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "This must be called after nextView is bound"

    invoke-static {v0, v1}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2348335
    new-instance v0, Lcom/facebook/adspayments/validation/CardInputFieldController$1;

    invoke-direct {v0, p0}, Lcom/facebook/adspayments/validation/CardInputFieldController$1;-><init>(LX/GP4;)V

    const v1, 0xad1fc12

    invoke-static {p1, v0, v1}, LX/03X;->a(Ljava/util/concurrent/Executor;Ljava/lang/Runnable;I)V

    .line 2348336
    return-void

    .line 2348337
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2348316
    iput-boolean p1, p0, LX/GP4;->k:Z

    .line 2348317
    iget-boolean v0, p0, LX/GP4;->k:Z

    if-eqz v0, :cond_0

    .line 2348318
    invoke-virtual {p0}, LX/GP4;->lX_()V

    .line 2348319
    :goto_0
    return-void

    .line 2348320
    :cond_0
    invoke-virtual {p0}, LX/GP4;->c()Landroid/widget/EditText;

    move-result-object v0

    iget p1, p0, LX/GP4;->d:I

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setTextColor(I)V

    .line 2348321
    invoke-virtual {p0}, LX/GP4;->c()Landroid/widget/EditText;

    move-result-object v0

    const p1, 0x7f021411

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 2348322
    invoke-static {p0}, LX/GP4;->l(LX/GP4;)V

    .line 2348323
    invoke-virtual {p0}, LX/GP4;->d()Landroid/widget/TextView;

    move-result-object v0

    const/16 p1, 0x8

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2348324
    goto :goto_0
.end method

.method public abstract a()Z
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public abstract c()Landroid/widget/EditText;
.end method

.method public abstract d()Landroid/widget/TextView;
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 2348330
    invoke-virtual {p0}, LX/GP4;->a()Z

    move-result v0

    .line 2348331
    iget-object v1, p0, LX/GP4;->f:LX/GQ7;

    move-object v1, v1

    .line 2348332
    invoke-virtual {v1, v0}, LX/GQ7;->a(Z)V

    .line 2348333
    return v0
.end method

.method public lX_()V
    .locals 2

    .prologue
    .line 2348325
    invoke-virtual {p0}, LX/GP4;->c()Landroid/widget/EditText;

    move-result-object v0

    iget v1, p0, LX/GP4;->c:I

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTextColor(I)V

    .line 2348326
    invoke-virtual {p0}, LX/GP4;->c()Landroid/widget/EditText;

    move-result-object v0

    const v1, 0x7f021420

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setBackgroundResource(I)V

    .line 2348327
    invoke-static {p0}, LX/GP4;->l(LX/GP4;)V

    .line 2348328
    invoke-virtual {p0}, LX/GP4;->d()Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 2348329
    return-void
.end method
