.class public final LX/Gbw;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 17

    .prologue
    .line 2370756
    const/4 v11, 0x0

    .line 2370757
    const/4 v10, 0x0

    .line 2370758
    const/4 v9, 0x0

    .line 2370759
    const/4 v8, 0x0

    .line 2370760
    const-wide/16 v6, 0x0

    .line 2370761
    const/4 v5, 0x0

    .line 2370762
    const/4 v4, 0x0

    .line 2370763
    const/4 v3, 0x0

    .line 2370764
    const/4 v2, 0x0

    .line 2370765
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->START_OBJECT:LX/15z;

    if-eq v12, v13, :cond_b

    .line 2370766
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2370767
    const/4 v2, 0x0

    .line 2370768
    :goto_0
    return v2

    .line 2370769
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v13, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v13, :cond_7

    .line 2370770
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2370771
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2370772
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v13

    sget-object v14, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v13, v14, :cond_0

    if-eqz v7, :cond_0

    .line 2370773
    const-string v13, "datr"

    invoke-virtual {v7, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 2370774
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    move v12, v7

    goto :goto_1

    .line 2370775
    :cond_1
    const-string v13, "device_type"

    invoke-virtual {v7, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 2370776
    invoke-static/range {p0 .. p1}, LX/Gbv;->a(LX/15w;LX/186;)I

    move-result v7

    move v11, v7

    goto :goto_1

    .line 2370777
    :cond_2
    const-string v13, "has_pin"

    invoke-virtual {v7, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 2370778
    const/4 v6, 0x1

    .line 2370779
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v10, v7

    goto :goto_1

    .line 2370780
    :cond_3
    const-string v13, "is_current_device"

    invoke-virtual {v7, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    .line 2370781
    const/4 v3, 0x1

    .line 2370782
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move v9, v7

    goto :goto_1

    .line 2370783
    :cond_4
    const-string v13, "last_used_time"

    invoke-virtual {v7, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 2370784
    const/4 v2, 0x1

    .line 2370785
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto :goto_1

    .line 2370786
    :cond_5
    const-string v13, "user_agent"

    invoke-virtual {v7, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2370787
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    move v8, v7

    goto :goto_1

    .line 2370788
    :cond_6
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2370789
    :cond_7
    const/4 v7, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v7}, LX/186;->c(I)V

    .line 2370790
    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v12}, LX/186;->b(II)V

    .line 2370791
    const/4 v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v11}, LX/186;->b(II)V

    .line 2370792
    if-eqz v6, :cond_8

    .line 2370793
    const/4 v6, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v10}, LX/186;->a(IZ)V

    .line 2370794
    :cond_8
    if-eqz v3, :cond_9

    .line 2370795
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->a(IZ)V

    .line 2370796
    :cond_9
    if-eqz v2, :cond_a

    .line 2370797
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2370798
    :cond_a
    const/4 v2, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v8}, LX/186;->b(II)V

    .line 2370799
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_b
    move v12, v11

    move v11, v10

    move v10, v9

    move v9, v8

    move v8, v5

    move-wide v15, v6

    move v6, v4

    move-wide v4, v15

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2370800
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2370801
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2370802
    if-eqz v0, :cond_0

    .line 2370803
    const-string v1, "datr"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2370804
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2370805
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2370806
    if-eqz v0, :cond_2

    .line 2370807
    const-string v1, "device_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2370808
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2370809
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2370810
    if-eqz v1, :cond_1

    .line 2370811
    const-string p3, "text"

    invoke-virtual {p2, p3}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2370812
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2370813
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2370814
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2370815
    if-eqz v0, :cond_3

    .line 2370816
    const-string v1, "has_pin"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2370817
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2370818
    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2370819
    if-eqz v0, :cond_4

    .line 2370820
    const-string v1, "is_current_device"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2370821
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2370822
    :cond_4
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2370823
    cmp-long v2, v0, v2

    if-eqz v2, :cond_5

    .line 2370824
    const-string v2, "last_used_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2370825
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2370826
    :cond_5
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2370827
    if-eqz v0, :cond_6

    .line 2370828
    const-string v1, "user_agent"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2370829
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2370830
    :cond_6
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2370831
    return-void
.end method
