.class public LX/F4V;
.super LX/8tD;
.source ""


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/res/Resources;

.field private d:Lcom/facebook/graphql/executor/GraphQLResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>(LX/0Zr;LX/8RL;LX/8vE;LX/0tX;LX/0Or;Landroid/content/res/Resources;)V
    .locals 1
    .param p3    # LX/8vE;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zr;",
            "LX/8RL;",
            "Lcom/facebook/widget/tokenizedtypeahead/ui/listview/TypeaheadAdapterCallback;",
            "LX/0tX;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Landroid/content/res/Resources;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2197013
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, LX/8tD;-><init>(LX/0Zr;LX/8RK;LX/8vE;Z)V

    .line 2197014
    const/4 v0, 0x0

    iput-object v0, p0, LX/F4V;->d:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2197015
    const-string v0, ""

    iput-object v0, p0, LX/F4V;->e:Ljava/lang/String;

    .line 2197016
    iput-object p4, p0, LX/F4V;->a:LX/0tX;

    .line 2197017
    iput-object p5, p0, LX/F4V;->b:LX/0Or;

    .line 2197018
    iput-object p6, p0, LX/F4V;->c:Landroid/content/res/Resources;

    .line 2197019
    return-void
.end method

.method private a(Lcom/facebook/graphql/executor/GraphQLResult;LX/39y;)LX/39y;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;",
            ">;",
            "LX/39y;",
            ")",
            "LX/39y;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2197020
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2197021
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v4

    .line 2197022
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v5

    .line 2197023
    iget-object v0, p2, LX/39y;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    .line 2197024
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2197025
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/621;

    .line 2197026
    invoke-interface {v0}, LX/621;->b()Ljava/util/List;

    move-result-object v0

    .line 2197027
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8QL;

    .line 2197028
    instance-of v1, v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 2197029
    check-cast v1, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    invoke-virtual {v1}, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2197030
    new-instance v1, Lcom/facebook/groups/memberpicker/MemberPickerToken;

    check-cast v0, Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;

    iget-object v7, p0, LX/F4V;->c:Landroid/content/res/Resources;

    const v8, 0x7f083080

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v0, v7}, Lcom/facebook/groups/memberpicker/MemberPickerToken;-><init>(Lcom/facebook/widget/tokenizedtypeahead/model/SimpleUserToken;Ljava/lang/String;)V

    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2197031
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    .line 2197032
    if-eqz p1, :cond_5

    .line 2197033
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2197034
    if-eqz v0, :cond_5

    .line 2197035
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2197036
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2197037
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2197038
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2197039
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2197040
    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel;->a()LX/0Px;

    move-result-object v7

    .line 2197041
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v3, v2

    :goto_1
    if-ge v3, v8, :cond_5

    invoke-virtual {v7, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;

    .line 2197042
    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->a()Ljava/lang/String;

    move-result-object v9

    .line 2197043
    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->k()Ljava/lang/String;

    move-result-object v10

    .line 2197044
    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 2197045
    :goto_2
    invoke-virtual {v6, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, LX/F4V;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2197046
    invoke-static {v9}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, "can_add"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2197047
    new-instance v1, Lcom/facebook/groups/memberpicker/MemberPickerToken;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {v0, v10}, LX/DVf;->a(LX/DWH;Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/facebook/groups/memberpicker/MemberPickerToken;-><init>(Lcom/facebook/user/model/User;)V

    invoke-virtual {v4, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2197048
    :cond_2
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 2197049
    :cond_3
    const-string v1, ""

    move-object v2, v1

    goto :goto_2

    .line 2197050
    :cond_4
    invoke-static {v9}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "can_invite"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2197051
    new-instance v1, Lcom/facebook/groups/memberpicker/MemberPickerToken;

    invoke-virtual {v0}, Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel;->j()Lcom/facebook/groups/memberpicker/protocol/GroupSuggestedInviteSearchQueryModels$GroupInviteMembersSearchQueryModel$GroupInviteMembersModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-static {v0, v10}, LX/DVf;->a(LX/DWH;Ljava/lang/String;)Lcom/facebook/user/model/User;

    move-result-object v0

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Lcom/facebook/groups/memberpicker/MemberPickerToken;-><init>(Lcom/facebook/user/model/User;Z)V

    invoke-virtual {v5, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_3

    .line 2197052
    :cond_5
    new-instance v0, LX/39y;

    invoke-direct {v0}, LX/39y;-><init>()V

    .line 2197053
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 2197054
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2197055
    invoke-virtual {v5}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2197056
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    if-lez v4, :cond_6

    .line 2197057
    new-instance v4, LX/623;

    iget-object v5, p0, LX/F4V;->c:Landroid/content/res/Resources;

    const v6, 0x7f08307e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, v2}, LX/623;-><init>(Ljava/lang/String;LX/0Px;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2197058
    :cond_6
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v2

    if-lez v2, :cond_7

    .line 2197059
    new-instance v2, LX/623;

    iget-object v4, p0, LX/F4V;->c:Landroid/content/res/Resources;

    const v5, 0x7f08307f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4, v3}, LX/623;-><init>(Ljava/lang/String;LX/0Px;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2197060
    :cond_7
    iput-object v1, v0, LX/39y;->a:Ljava/lang/Object;

    .line 2197061
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iput v1, v0, LX/39y;->b:I

    .line 2197062
    return-object v0
.end method


# virtual methods
.method public final b(Ljava/lang/CharSequence;)LX/39y;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 2197063
    invoke-super {p0, p1}, LX/8tD;->b(Ljava/lang/CharSequence;)LX/39y;

    move-result-object v1

    .line 2197064
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2197065
    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_1

    .line 2197066
    :cond_0
    invoke-direct {p0, v4, v1}, LX/F4V;->a(Lcom/facebook/graphql/executor/GraphQLResult;LX/39y;)LX/39y;

    move-result-object v0

    .line 2197067
    :goto_0
    return-object v0

    .line 2197068
    :cond_1
    iget-object v2, p0, LX/F4V;->e:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2197069
    iget-object v0, p0, LX/F4V;->d:Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-direct {p0, v0, v1}, LX/F4V;->a(Lcom/facebook/graphql/executor/GraphQLResult;LX/39y;)LX/39y;

    move-result-object v0

    goto :goto_0

    .line 2197070
    :cond_2
    iput-object v4, p0, LX/F4V;->d:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2197071
    iput-object v0, p0, LX/F4V;->e:Ljava/lang/String;

    .line 2197072
    :try_start_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2197073
    new-instance v2, LX/DWE;

    invoke-direct {v2}, LX/DWE;-><init>()V

    move-object v3, v2

    .line 2197074
    const-string v5, "user_id"

    iget-object v2, p0, LX/F4V;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v5, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v5, "named"

    invoke-virtual {v2, v5, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v5, "max_count"

    const/16 p1, 0xc

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v2, v5, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2197075
    iget-object v2, p0, LX/F4V;->a:LX/0tX;

    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    sget-object v5, LX/0zS;->c:LX/0zS;

    invoke-virtual {v3, v5}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    move-object v0, v2

    .line 2197076
    const v2, -0x68741207

    invoke-static {v0, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/executor/GraphQLResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2197077
    iput-object v0, p0, LX/F4V;->d:Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2197078
    invoke-direct {p0, v0, v1}, LX/F4V;->a(Lcom/facebook/graphql/executor/GraphQLResult;LX/39y;)LX/39y;

    move-result-object v0

    goto :goto_0

    .line 2197079
    :catch_0
    invoke-direct {p0, v4, v1}, LX/F4V;->a(Lcom/facebook/graphql/executor/GraphQLResult;LX/39y;)LX/39y;

    move-result-object v0

    goto :goto_0
.end method
