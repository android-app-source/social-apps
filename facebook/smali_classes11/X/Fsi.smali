.class public final LX/Fsi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/GraphQLUser;",
        ">;",
        "Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Fsj;


# direct methods
.method public constructor <init>(LX/Fsj;)V
    .locals 0

    .prologue
    .line 2297469
    iput-object p1, p0, LX/Fsi;->a:LX/Fsj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2297470
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2297471
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2297472
    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->Z()Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2297473
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2297474
    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->Z()Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2297475
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected a non-null and non-empty result"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2297476
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2297477
    check-cast v0, Lcom/facebook/graphql/model/GraphQLUser;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->Z()Lcom/facebook/graphql/model/GraphQLTimelineStoriesConnection;

    move-result-object v0

    return-object v0
.end method
