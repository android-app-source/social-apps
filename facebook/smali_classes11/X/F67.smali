.class public LX/F67;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/5pX;

.field public final b:Landroid/app/Activity;

.field private final c:LX/F6J;

.field public final d:LX/1Kf;

.field public final e:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0kL;

.field private final g:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

.field public final h:LX/3H7;

.field public final i:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/5pX;Landroid/app/Activity;LX/F6K;LX/1Kf;LX/0Or;LX/0kL;Lcom/facebook/composer/publish/ComposerPublishServiceHelper;LX/3H7;Ljava/util/concurrent/ExecutorService;)V
    .locals 1
    .param p1    # LX/5pX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Landroid/app/Activity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/5pX;",
            "Landroid/app/Activity;",
            "LX/F6K;",
            "LX/1Kf;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/0kL;",
            "Lcom/facebook/composer/publish/ComposerPublishServiceHelper;",
            "LX/3H7;",
            "Ljava/util/concurrent/ExecutorService;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2200150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2200151
    iput-object p1, p0, LX/F67;->a:LX/5pX;

    .line 2200152
    iput-object p2, p0, LX/F67;->b:Landroid/app/Activity;

    .line 2200153
    iget-object v0, p0, LX/F67;->a:LX/5pX;

    invoke-virtual {p3, v0}, LX/F6K;->a(LX/5pX;)LX/F6J;

    move-result-object v0

    iput-object v0, p0, LX/F67;->c:LX/F6J;

    .line 2200154
    iput-object p4, p0, LX/F67;->d:LX/1Kf;

    .line 2200155
    iput-object p5, p0, LX/F67;->e:LX/0Or;

    .line 2200156
    iput-object p6, p0, LX/F67;->f:LX/0kL;

    .line 2200157
    iput-object p7, p0, LX/F67;->g:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    .line 2200158
    iput-object p8, p0, LX/F67;->h:LX/3H7;

    .line 2200159
    iput-object p9, p0, LX/F67;->i:Ljava/util/concurrent/ExecutorService;

    .line 2200160
    return-void
.end method


# virtual methods
.method public final a(ILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2200161
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 2200162
    :goto_0
    return-void

    .line 2200163
    :cond_0
    const-string v0, "publishEditPostParamsKey"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/composer/publish/common/EditPostParams;

    .line 2200164
    iget-object v1, p0, LX/F67;->g:Lcom/facebook/composer/publish/ComposerPublishServiceHelper;

    invoke-virtual {v1, p2}, Lcom/facebook/composer/publish/ComposerPublishServiceHelper;->c(Landroid/content/Intent;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2200165
    new-instance v2, LX/F66;

    invoke-direct {v2, p0, v0}, LX/F66;-><init>(LX/F67;Lcom/facebook/composer/publish/common/EditPostParams;)V

    iget-object v0, p0, LX/F67;->i:Ljava/util/concurrent/ExecutorService;

    invoke-static {v1, v2, v0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;LX/21D;)V
    .locals 2

    .prologue
    .line 2200166
    iget-object v0, p0, LX/F67;->c:LX/F6J;

    new-instance v1, LX/F64;

    invoke-direct {v1, p0, p2}, LX/F64;-><init>(LX/F67;LX/21D;)V

    .line 2200167
    sget-object p0, LX/5Go;->PLATFORM_DEFAULT:LX/5Go;

    invoke-virtual {v0, p1, p0, v1}, LX/F6J;->a(Ljava/lang/String;LX/5Go;LX/0TF;)V

    .line 2200168
    return-void
.end method
