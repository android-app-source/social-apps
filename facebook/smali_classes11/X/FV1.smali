.class public LX/FV1;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/FV1;


# instance fields
.field public final a:LX/79a;


# direct methods
.method public constructor <init>(LX/79a;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2250281
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2250282
    iput-object p1, p0, LX/FV1;->a:LX/79a;

    .line 2250283
    return-void
.end method

.method public static a(LX/0QB;)LX/FV1;
    .locals 4

    .prologue
    .line 2250294
    sget-object v0, LX/FV1;->b:LX/FV1;

    if-nez v0, :cond_1

    .line 2250295
    const-class v1, LX/FV1;

    monitor-enter v1

    .line 2250296
    :try_start_0
    sget-object v0, LX/FV1;->b:LX/FV1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2250297
    if-eqz v2, :cond_0

    .line 2250298
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2250299
    new-instance p0, LX/FV1;

    invoke-static {v0}, LX/79a;->a(LX/0QB;)LX/79a;

    move-result-object v3

    check-cast v3, LX/79a;

    invoke-direct {p0, v3}, LX/FV1;-><init>(LX/79a;)V

    .line 2250300
    move-object v0, p0

    .line 2250301
    sput-object v0, LX/FV1;->b:LX/FV1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2250302
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2250303
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2250304
    :cond_1
    sget-object v0, LX/FV1;->b:LX/FV1;

    return-object v0

    .line 2250305
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2250306
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/saved/views/SavedDashboardEmptyView;LX/0am;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/saved/views/SavedDashboardEmptyView;",
            "LX/0am",
            "<",
            "LX/79Y;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2250284
    const v0, 0x7f02179e

    invoke-virtual {p1, v0}, Lcom/facebook/saved/views/SavedDashboardEmptyView;->setIcon(I)V

    .line 2250285
    invoke-virtual {p2}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79Y;

    .line 2250286
    iget-object v1, v0, LX/79Y;->a:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-object v0, v1

    .line 2250287
    if-nez v0, :cond_1

    .line 2250288
    :cond_0
    sget-object v0, LX/79a;->a:LX/79Z;

    iget v0, v0, LX/79Z;->b:I

    .line 2250289
    :goto_0
    move v0, v0

    .line 2250290
    invoke-virtual {p1, v0}, Lcom/facebook/saved/views/SavedDashboardEmptyView;->setTitle(I)V

    .line 2250291
    return-void

    :cond_1
    iget-object v1, p0, LX/FV1;->a:LX/79a;

    invoke-virtual {p2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79Y;

    .line 2250292
    iget-object p0, v0, LX/79Y;->a:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    move-object v0, p0

    .line 2250293
    invoke-virtual {v1, v0}, LX/79a;->a(Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;)LX/0am;

    move-result-object v0

    sget-object v1, LX/79a;->a:LX/79Z;

    invoke-virtual {v0, v1}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/79Z;

    iget v0, v0, LX/79Z;->b:I

    goto :goto_0
.end method
