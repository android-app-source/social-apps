.class public LX/FBr;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FBr;


# direct methods
.method public constructor <init>()V
    .locals 4
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2209803
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2209804
    const-string v0, "install/?file={%s}&notes={%s}&appName={%s}"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "local_uri"

    const-string v2, "release_notes"

    const-string v3, "app_name"

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/selfupdate/SelfUpdateInstallActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2209805
    return-void
.end method

.method public static a(LX/0QB;)LX/FBr;
    .locals 3

    .prologue
    .line 2209806
    sget-object v0, LX/FBr;->a:LX/FBr;

    if-nez v0, :cond_1

    .line 2209807
    const-class v1, LX/FBr;

    monitor-enter v1

    .line 2209808
    :try_start_0
    sget-object v0, LX/FBr;->a:LX/FBr;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2209809
    if-eqz v2, :cond_0

    .line 2209810
    :try_start_1
    new-instance v0, LX/FBr;

    invoke-direct {v0}, LX/FBr;-><init>()V

    .line 2209811
    move-object v0, v0

    .line 2209812
    sput-object v0, LX/FBr;->a:LX/FBr;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2209813
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2209814
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2209815
    :cond_1
    sget-object v0, LX/FBr;->a:LX/FBr;

    return-object v0

    .line 2209816
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2209817
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
