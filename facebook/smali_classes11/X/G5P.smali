.class public LX/G5P;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/11i;

.field public final b:LX/0So;


# direct methods
.method public constructor <init>(LX/0So;LX/11i;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2318703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2318704
    iput-object p1, p0, LX/G5P;->b:LX/0So;

    .line 2318705
    iput-object p2, p0, LX/G5P;->a:LX/11i;

    .line 2318706
    return-void
.end method

.method public static a(LX/G5P;LX/0Pq;)V
    .locals 1

    .prologue
    .line 2318707
    iget-object v0, p0, LX/G5P;->a:LX/11i;

    invoke-interface {v0, p1}, LX/11i;->a(LX/0Pq;)LX/11o;

    .line 2318708
    return-void
.end method

.method public static a(LX/G5P;LX/0Pq;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2318709
    iget-object v0, p0, LX/G5P;->a:LX/11i;

    invoke-interface {v0, p1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 2318710
    if-eqz v0, :cond_0

    .line 2318711
    const v1, -0x5c32a2e4

    invoke-static {v0, p2, v1}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2318712
    :cond_0
    return-void
.end method

.method public static b(LX/G5P;LX/0Pq;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2318713
    iget-object v0, p0, LX/G5P;->a:LX/11i;

    invoke-interface {v0, p1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 2318714
    if-eqz v0, :cond_0

    .line 2318715
    const v1, -0x5b069d5b

    invoke-static {v0, p2, v1}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2318716
    :cond_0
    return-void
.end method

.method public static c(LX/G5P;LX/0Pq;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2318717
    iget-object v0, p0, LX/G5P;->a:LX/11i;

    invoke-interface {v0, p1}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v0

    .line 2318718
    if-eqz v0, :cond_0

    .line 2318719
    const v1, 0x6781b45e

    invoke-static {v0, p2, v1}, LX/096;->c(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2318720
    :cond_0
    return-void
.end method
