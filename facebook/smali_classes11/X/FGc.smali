.class public LX/FGc;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation build Ljavax/annotation/concurrent/Immutable;
.end annotation


# static fields
.field public static final a:LX/FGc;


# instance fields
.field public final b:LX/FGb;

.field public final c:Lcom/facebook/ui/media/attachments/MediaUploadResult;

.field public final d:Lcom/google/common/util/concurrent/ListenableFuture;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/FGc;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/FGa;

.field public final f:Ljava/lang/Throwable;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2219215
    new-instance v0, LX/FGc;

    sget-object v1, LX/FGb;->NOT_ACTIVE:LX/FGb;

    invoke-direct {v0, v1}, LX/FGc;-><init>(LX/FGb;)V

    sput-object v0, LX/FGc;->a:LX/FGc;

    return-void
.end method

.method private constructor <init>(LX/FGb;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2219193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2219194
    iput-object p1, p0, LX/FGc;->b:LX/FGb;

    .line 2219195
    iput-object v0, p0, LX/FGc;->c:Lcom/facebook/ui/media/attachments/MediaUploadResult;

    .line 2219196
    iput-object v0, p0, LX/FGc;->f:Ljava/lang/Throwable;

    .line 2219197
    iput-object v0, p0, LX/FGc;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2219198
    sget-object v0, LX/FGa;->UNKNOWN:LX/FGa;

    iput-object v0, p0, LX/FGc;->e:LX/FGa;

    .line 2219199
    return-void
.end method

.method private constructor <init>(LX/FGb;Lcom/facebook/ui/media/attachments/MediaUploadResult;LX/FGa;Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Throwable;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FGb;",
            "Lcom/facebook/ui/media/attachments/MediaUploadResult;",
            "LX/FGa;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/FGc;",
            ">;",
            "Ljava/lang/Throwable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2219200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2219201
    iput-object p1, p0, LX/FGc;->b:LX/FGb;

    .line 2219202
    iput-object p2, p0, LX/FGc;->c:Lcom/facebook/ui/media/attachments/MediaUploadResult;

    .line 2219203
    iput-object p4, p0, LX/FGc;->d:Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2219204
    iput-object p3, p0, LX/FGc;->e:LX/FGa;

    .line 2219205
    iput-object p5, p0, LX/FGc;->f:Ljava/lang/Throwable;

    .line 2219206
    return-void
.end method

.method public static a(LX/FGa;Lcom/google/common/util/concurrent/ListenableFuture;)LX/FGc;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FGa;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/FGc;",
            ">;)",
            "LX/FGc;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2219207
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2219208
    new-instance v0, LX/FGc;

    sget-object v1, LX/FGb;->IN_PROGRESS:LX/FGb;

    move-object v3, p0

    move-object v4, p1

    move-object v5, v2

    invoke-direct/range {v0 .. v5}, LX/FGc;-><init>(LX/FGb;Lcom/facebook/ui/media/attachments/MediaUploadResult;LX/FGa;Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static a(LX/FGa;Ljava/lang/Throwable;)LX/FGc;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2219209
    new-instance v0, LX/FGc;

    sget-object v1, LX/FGb;->FAILED:LX/FGb;

    move-object v3, p0

    move-object v4, v2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, LX/FGc;-><init>(LX/FGb;Lcom/facebook/ui/media/attachments/MediaUploadResult;LX/FGa;Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Throwable;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/ui/media/attachments/MediaUploadResult;)LX/FGc;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 2219210
    new-instance v0, LX/FGc;

    sget-object v1, LX/FGb;->SUCCEEDED:LX/FGb;

    move-object v2, p0

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, LX/FGc;-><init>(LX/FGb;Lcom/facebook/ui/media/attachments/MediaUploadResult;LX/FGa;Lcom/google/common/util/concurrent/ListenableFuture;Ljava/lang/Throwable;)V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2219211
    iget-object v0, p0, LX/FGc;->c:Lcom/facebook/ui/media/attachments/MediaUploadResult;

    if-eqz v0, :cond_0

    .line 2219212
    iget-object v0, p0, LX/FGc;->c:Lcom/facebook/ui/media/attachments/MediaUploadResult;

    .line 2219213
    iget-object p0, v0, Lcom/facebook/ui/media/attachments/MediaUploadResult;->a:Ljava/lang/String;

    move-object v0, p0

    .line 2219214
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
