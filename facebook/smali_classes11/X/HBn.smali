.class public LX/HBn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2437891
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2437892
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 6

    .prologue
    .line 2437893
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 2437894
    const-string v1, "com.facebook.katana.profile.id"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 2437895
    const-wide/16 v4, 0x0

    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, LX/0PB;->checkState(Z)V

    .line 2437896
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 2437897
    const-string v5, "com.facebook.katana.profile.id"

    invoke-virtual {v4, v5, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2437898
    new-instance v5, Lcom/facebook/pages/common/editpage/AllTemplatesFragment;

    invoke-direct {v5}, Lcom/facebook/pages/common/editpage/AllTemplatesFragment;-><init>()V

    .line 2437899
    invoke-virtual {v5, v4}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2437900
    move-object v0, v5

    .line 2437901
    return-object v0

    .line 2437902
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method
