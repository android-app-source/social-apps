.class public final LX/F96;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;)V
    .locals 0

    .prologue
    .line 2205116
    iput-object p1, p0, LX/F96;->a:Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v0, 0x1

    const v1, -0x1b4ea38b

    invoke-static {v8, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2205090
    iget-object v0, p0, LX/F96;->a:Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;

    iget-object v0, v0, Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;->c:LX/F9S;

    .line 2205091
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    const-string v2, "first_name"

    iget-object v3, v0, LX/F9S;->b:LX/F9Q;

    .line 2205092
    const v4, 0x7f0d1cf1

    invoke-virtual {v3, v4}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    .line 2205093
    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 2205094
    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const-string v2, "last_name"

    iget-object v3, v0, LX/F9S;->b:LX/F9Q;

    .line 2205095
    const v4, 0x7f0d1cf0

    invoke-virtual {v3, v4}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    .line 2205096
    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 2205097
    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    const-string v2, "locale"

    iget-object v3, v0, LX/F9S;->a:LX/0W9;

    invoke-virtual {v3}, LX/0W9;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    .line 2205098
    iget-object v2, v0, LX/F9S;->c:LX/F9R;

    if-eqz v2, :cond_1

    .line 2205099
    iget-object v2, v0, LX/F9S;->c:LX/F9R;

    .line 2205100
    const v3, 0x7f0d1e02

    invoke-virtual {v2, v3}, LX/F9R;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 2205101
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 2205102
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    .line 2205103
    const-string v3, "first_name_extra"

    invoke-virtual {v1, v3, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2205104
    :cond_0
    iget-object v2, v0, LX/F9S;->c:LX/F9R;

    .line 2205105
    const v3, 0x7f0d1e01

    invoke-virtual {v2, v3}, LX/F9R;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 2205106
    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v2, v3

    .line 2205107
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_1

    .line 2205108
    const-string v3, "last_name_extra"

    invoke-virtual {v1, v3, v2}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2205109
    :cond_1
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    move-object v5, v1

    .line 2205110
    new-instance v0, Lcom/facebook/api/growth/profile/SetNativeNameParams;

    const-string v1, "first_name"

    invoke-virtual {v5, v1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, "last_name"

    invoke-virtual {v5, v2}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "first_name_extra"

    invoke-virtual {v5, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "last_name_extra"

    invoke-virtual {v5, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string v7, "locale"

    invoke-virtual {v5, v7}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/facebook/api/growth/profile/SetNativeNameParams;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2205111
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2205112
    const-string v2, "growthSetNativeNameParams"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2205113
    iget-object v0, p0, LX/F96;->a:Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;

    iget-object v0, v0, Lcom/facebook/growth/nux/fragments/NUXNativeNameFragment;->b:LX/0aG;

    const-string v2, "growth_set_native_name"

    const v3, -0x62496308

    invoke-static {v0, v2, v1, v3}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2205114
    new-instance v1, LX/F95;

    invoke-direct {v1, p0}, LX/F95;-><init>(LX/F96;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2205115
    const v0, 0x3a8f9724

    invoke-static {v8, v8, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
