.class public final LX/FoL;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;)V
    .locals 0

    .prologue
    .line 2289332
    iput-object p1, p0, LX/FoL;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x4711bb8a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2289324
    iget-object v1, p0, LX/FoL;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->c:LX/0Zb;

    iget-object v2, p0, LX/FoL;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    iget-object v2, v2, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->n:Ljava/lang/String;

    invoke-static {v2}, LX/BOe;->n(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2289325
    iget-object v1, p0, LX/FoL;->a:Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;

    .line 2289326
    iget-object v2, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->o:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2289327
    iget-object v2, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/03V;

    const-string v4, "post_donation_page_fragment_error"

    const-string v5, "Sharing a charity page with no id."

    invoke-virtual {v2, v4, v5}, LX/03V;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2289328
    :goto_0
    const v1, -0x13129125

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2289329
    :cond_0
    const-string v4, "charityPageShare"

    .line 2289330
    iget-object v2, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->o:Ljava/lang/String;

    const v5, 0x25d6af

    invoke-static {v2, v5}, LX/16z;->a(Ljava/lang/String;I)Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v5

    .line 2289331
    iget-object v2, v1, Lcom/facebook/socialgood/thankyou/FundraiserThankYouFragment;->i:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Kf;

    const/4 p0, 0x0

    sget-object p1, LX/21D;->FUNDRAISER_THANK_YOU_PAGE:LX/21D;

    invoke-static {v5}, LX/89G;->a(Lcom/facebook/graphql/model/GraphQLEntity;)LX/89G;

    move-result-object v5

    invoke-virtual {v5}, LX/89G;->b()Lcom/facebook/ipc/composer/intent/ComposerShareParams;

    move-result-object v5

    invoke-static {p1, v4, v5}, LX/1nC;->a(LX/21D;Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerShareParams;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v4

    const/16 v5, 0x2768

    invoke-interface {v2, p0, v4, v5, v1}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/support/v4/app/Fragment;)V

    goto :goto_0
.end method
