.class public final LX/Gki;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

.field public final synthetic b:LX/Gkq;

.field public final synthetic c:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

.field public final synthetic d:LX/GkS;

.field public final synthetic e:I

.field public final synthetic f:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;Lcom/facebook/widget/recyclerview/BetterRecyclerView;LX/Gkq;Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;LX/GkS;I)V
    .locals 0

    .prologue
    .line 2389250
    iput-object p1, p0, LX/Gki;->f:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iput-object p2, p0, LX/Gki;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    iput-object p3, p0, LX/Gki;->b:LX/Gkq;

    iput-object p4, p0, LX/Gki;->c:Lcom/facebook/graphql/enums/GraphQLGroupSubscriptionLevel;

    iput-object p5, p0, LX/Gki;->d:LX/GkS;

    iput p6, p0, LX/Gki;->e:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2389239
    iget-object v0, p0, LX/Gki;->f:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->s:LX/DPS;

    iget-object v1, p0, LX/Gki;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/Gki;->f:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iget-object v3, p0, LX/Gki;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, LX/Gki;->b:LX/Gkq;

    .line 2389240
    iget-object v5, v4, LX/Gkq;->a:Ljava/lang/String;

    move-object v4, v5

    .line 2389241
    invoke-static {v2, v3, v4, v8}, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->a$redex0(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;Landroid/content/Context;Ljava/lang/String;Z)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v2

    iget-object v3, p0, LX/Gki;->f:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iget-object v4, p0, LX/Gki;->a:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    invoke-virtual {v4}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, LX/Gki;->b:LX/Gkq;

    .line 2389242
    iget-object v6, v5, LX/Gkq;->a:Ljava/lang/String;

    move-object v5, v6

    .line 2389243
    invoke-static {v3, v4, v5, v7}, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->a$redex0(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;Landroid/content/Context;Ljava/lang/String;Z)Landroid/content/DialogInterface$OnClickListener;

    move-result-object v3

    new-instance v4, LX/Gkh;

    invoke-direct {v4, p0}, LX/Gkh;-><init>(LX/Gki;)V

    iget-object v5, p0, LX/Gki;->b:LX/Gkq;

    .line 2389244
    iget-object v6, v5, LX/Gkq;->m:Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;

    move-object v5, v6

    .line 2389245
    iget-object v6, p0, LX/Gki;->b:LX/Gkq;

    .line 2389246
    iget-object p1, v6, LX/Gkq;->b:Ljava/lang/String;

    move-object v6, p1

    .line 2389247
    invoke-virtual/range {v0 .. v7}, LX/DPS;->a(Landroid/content/Context;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Landroid/content/DialogInterface$OnClickListener;Lcom/facebook/graphql/enums/GraphQLLeavingGroupScenario;Ljava/lang/String;Z)V

    .line 2389248
    iget-object v0, p0, LX/Gki;->f:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iget-object v0, v0, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->g:LX/Glj;

    const-string v1, "leave_group"

    iget v2, p0, LX/Gki;->e:I

    iget-object v3, p0, LX/Gki;->b:LX/Gkq;

    iget-object v4, p0, LX/Gki;->f:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iget-object v4, v4, Lcom/facebook/groups/groupsgrid/fragment/AbstractGroupsPogGridFragment;->l:LX/GlX;

    invoke-virtual {v4}, LX/GlX;->d()LX/Gkm;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/Glj;->a(Ljava/lang/String;ILX/Gkq;LX/Gkm;)V

    .line 2389249
    return v8
.end method
