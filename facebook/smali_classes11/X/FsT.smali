.class public LX/FsT;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0ad;

.field private final b:LX/Fsm;

.field private final c:LX/Fsg;

.field private final d:LX/FsI;

.field private final e:LX/G2W;

.field private final f:LX/FsX;

.field private final g:LX/0tX;

.field private final h:LX/G2L;


# direct methods
.method public constructor <init>(LX/0ad;LX/Fsm;LX/Fsg;LX/FsI;LX/G2W;LX/FsX;LX/0tX;LX/G2L;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2297001
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2297002
    iput-object p1, p0, LX/FsT;->a:LX/0ad;

    .line 2297003
    iput-object p2, p0, LX/FsT;->b:LX/Fsm;

    .line 2297004
    iput-object p3, p0, LX/FsT;->c:LX/Fsg;

    .line 2297005
    iput-object p4, p0, LX/FsT;->d:LX/FsI;

    .line 2297006
    iput-object p5, p0, LX/FsT;->e:LX/G2W;

    .line 2297007
    iput-object p6, p0, LX/FsT;->f:LX/FsX;

    .line 2297008
    iput-object p7, p0, LX/FsT;->g:LX/0tX;

    .line 2297009
    iput-object p8, p0, LX/FsT;->h:LX/G2L;

    .line 2297010
    return-void
.end method

.method private static a(LX/FsT;Ljava/lang/String;)J
    .locals 6
    .param p0    # LX/FsT;
        .annotation build Lcom/facebook/graphql/calls/ProfileTilesSectionType;
        .end annotation
    .end param

    .prologue
    const-wide/16 v4, 0x3840

    .line 2297085
    const-string v0, "PHOTOS"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2297086
    iget-object v0, p0, LX/FsT;->a:LX/0ad;

    sget-wide v2, LX/0wf;->al:J

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    .line 2297087
    :goto_0
    return-wide v0

    .line 2297088
    :cond_0
    const-string v0, "FRIENDS"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2297089
    iget-object v0, p0, LX/FsT;->a:LX/0ad;

    sget-wide v2, LX/0wf;->ak:J

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    goto :goto_0

    .line 2297090
    :cond_1
    iget-object v0, p0, LX/FsT;->a:LX/0ad;

    sget-wide v2, LX/0wf;->am:J

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v0

    goto :goto_0
.end method

.method private static a(LX/G1Q;LX/0zS;JILcom/facebook/common/callercontext/CallerContext;)LX/0zO;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/G1Q;",
            "LX/0zS;",
            "JI",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$TimelineProtilesQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2297076
    invoke-static {p0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 2297077
    iput p4, v0, LX/0zO;->B:I

    .line 2297078
    move-object v0, v0

    .line 2297079
    iput-object p5, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2297080
    move-object v0, v0

    .line 2297081
    invoke-virtual {v0, p1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    const/4 v1, 0x1

    .line 2297082
    iput-boolean v1, v0, LX/0zO;->p:Z

    .line 2297083
    move-object v0, v0

    .line 2297084
    return-object v0
.end method

.method private static a(LX/FsT;LX/0zO;)LX/0zX;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/0zO",
            "<TT;>;)",
            "LX/0zX",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 2297075
    iget-object v0, p0, LX/FsT;->g:LX/0tX;

    invoke-virtual {v0, p1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    invoke-static {v0}, LX/FsA;->a(Lcom/google/common/util/concurrent/ListenableFuture;)LX/0zX;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/G12;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zO;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/G12;",
            "LX/0zS;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsUserFieldsModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2297066
    iget-object v0, p0, LX/FsT;->d:LX/FsI;

    invoke-virtual {v0, p1}, LX/FsI;->a(LX/G12;)LX/5xH;

    move-result-object v0

    .line 2297067
    iget-object v1, p0, LX/FsT;->a:LX/0ad;

    sget-wide v2, LX/0wf;->an:J

    const-wide/32 v4, 0x15180

    invoke-interface {v1, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v2

    .line 2297068
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 2297069
    iput-object p4, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2297070
    move-object v0, v0

    .line 2297071
    invoke-virtual {v0, p2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, LX/0zO;->a(J)LX/0zO;

    move-result-object v0

    .line 2297072
    iput p3, v0, LX/0zO;->B:I

    .line 2297073
    move-object v0, v0

    .line 2297074
    return-object v0
.end method

.method public final a(LX/0v6;ILX/G12;Lcom/facebook/common/callercontext/CallerContext;)LX/0zX;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0v6;",
            "I",
            "LX/G12;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zX",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUnseenStoriesConnection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2297048
    iget-object v0, p0, LX/FsT;->b:LX/Fsm;

    .line 2297049
    iget-wide v4, p3, LX/G12;->a:J

    move-wide v2, v4

    .line 2297050
    new-instance v1, LX/5xn;

    invoke-direct {v1}, LX/5xn;-><init>()V

    move-object v1, v1

    .line 2297051
    const-string v4, "profile_id"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "angora_attachment_cover_image_size"

    iget-object v6, v0, LX/Fsm;->a:LX/0sa;

    invoke-virtual {v6}, LX/0sa;->s()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "angora_attachment_profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "reading_attachment_profile_image_width"

    iget-object v6, v0, LX/Fsm;->a:LX/0sa;

    invoke-virtual {v6}, LX/0sa;->M()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "reading_attachment_profile_image_height"

    iget-object v6, v0, LX/Fsm;->a:LX/0sa;

    invoke-virtual {v6}, LX/0sa;->N()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "num_faceboxes_and_tags"

    iget-object v6, v0, LX/Fsm;->a:LX/0sa;

    .line 2297052
    iget-object v7, v6, LX/0sa;->b:Ljava/lang/Integer;

    move-object v6, v7

    .line 2297053
    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "goodwill_small_accent_image"

    iget-object v6, v0, LX/Fsm;->b:LX/0rq;

    invoke-virtual {v6}, LX/0rq;->h()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "image_large_aspect_height"

    iget-object v6, v0, LX/Fsm;->a:LX/0sa;

    invoke-virtual {v6}, LX/0sa;->A()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "image_large_aspect_width"

    iget-object v6, v0, LX/Fsm;->a:LX/0sa;

    invoke-virtual {v6}, LX/0sa;->z()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "include_replies_in_total_count"

    iget-object v6, v0, LX/Fsm;->e:LX/0ad;

    sget-short v7, LX/0wg;->i:S

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, LX/0ad;->a(SZ)Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "default_image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    move-result-object v4

    const-string v5, "action_location"

    sget-object v6, LX/0wD;->TIMELINE:LX/0wD;

    invoke-virtual {v6}, LX/0wD;->stringValueOf()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2297054
    iget-object v4, v0, LX/Fsm;->c:LX/0se;

    iget-object v5, v0, LX/Fsm;->b:LX/0rq;

    invoke-virtual {v5}, LX/0rq;->c()LX/0wF;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, LX/0se;->a(LX/0gW;LX/0wF;)LX/0gW;

    .line 2297055
    iget-object v4, v0, LX/Fsm;->d:LX/0tI;

    invoke-virtual {v4, v1}, LX/0tI;->a(LX/0gW;)V

    .line 2297056
    invoke-static {v1}, LX/0wo;->a(LX/0gW;)V

    .line 2297057
    move-object v0, v1

    .line 2297058
    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->NON_INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, v1}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 2297059
    iput-object p4, v0, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2297060
    move-object v0, v0

    .line 2297061
    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2297062
    iput p2, v0, LX/0zO;->B:I

    .line 2297063
    move-object v0, v0

    .line 2297064
    invoke-virtual {p1, v0}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v0

    .line 2297065
    invoke-static {v0}, LX/FsD;->b(LX/0zX;)LX/0zX;

    move-result-object v0

    new-instance v1, LX/FsQ;

    invoke-direct {v1, p0}, LX/FsQ;-><init>(LX/FsT;)V

    invoke-virtual {v0, v1}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0v6;LX/0zO;)LX/0zX;
    .locals 2
    .param p1    # LX/0v6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0v6;",
            "LX/0zO",
            "<",
            "Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsUserFieldsModel;",
            ">;)",
            "LX/0zX",
            "<",
            "LX/FsM;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2297045
    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v0

    .line 2297046
    :goto_0
    invoke-static {v0}, LX/FsD;->b(LX/0zX;)LX/0zX;

    move-result-object v0

    new-instance v1, LX/FsR;

    invoke-direct {v1, p0}, LX/FsR;-><init>(LX/FsT;)V

    invoke-virtual {v0, v1}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v0

    return-object v0

    .line 2297047
    :cond_0
    invoke-static {p0, p2}, LX/FsT;->a(LX/FsT;LX/0zO;)LX/0zX;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/0v6;LX/0zO;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zX;
    .locals 6
    .param p1    # LX/0v6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0v6;",
            "LX/0zO",
            "<",
            "Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsUserFieldsModel;",
            ">;",
            "LX/0zS;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zX",
            "<",
            "Lcom/facebook/graphql/model/GraphQLTimelineSection;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2297043
    invoke-static {p2}, LX/Fsq;->a(LX/0zO;)LX/Fso;

    move-result-object v2

    .line 2297044
    iget-object v0, p0, LX/FsT;->f:LX/FsX;

    move-object v1, p1

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, LX/FsX;->a(LX/0v6;LX/Fso;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zX;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0v6;LX/G12;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zX;
    .locals 10
    .param p1    # LX/0v6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0v6;",
            "LX/G12;",
            "LX/0zS;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zX",
            "<",
            "Lcom/facebook/timeline/protocol/FetchTimelineTaggedMediaSetGraphQLInterfaces$TimelineTaggedMediaSetFields;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2297039
    iget-object v0, p0, LX/FsT;->a:LX/0ad;

    sget-wide v2, LX/0wf;->ao:J

    const-wide/16 v4, 0x3840

    invoke-interface {v0, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v6

    .line 2297040
    iget-object v1, p0, LX/FsT;->g:LX/0tX;

    .line 2297041
    iget-wide v8, p2, LX/G12;->a:J

    move-wide v2, v8

    .line 2297042
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    move-object v0, p1

    move v3, p4

    move-object v4, p5

    move-object v5, p3

    invoke-static/range {v0 .. v7}, LX/Fsl;->a(LX/0v6;LX/0tX;Ljava/lang/String;ILcom/facebook/common/callercontext/CallerContext;LX/0zS;J)LX/0zX;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0v6;Ljava/util/List;)LX/0zX;
    .locals 4
    .param p1    # LX/0v6;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0v6;",
            "Ljava/util/List",
            "<",
            "LX/0zO",
            "<",
            "Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$TimelineProtilesQueryModel;",
            ">;>;)",
            "LX/0zX",
            "<",
            "LX/FsK;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2297030
    new-instance v2, LX/FsS;

    invoke-direct {v2, p0}, LX/FsS;-><init>(LX/FsT;)V

    .line 2297031
    invoke-static {}, LX/0zX;->b()LX/0zX;

    move-result-object v0

    .line 2297032
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0zO;

    .line 2297033
    if-eqz p1, :cond_0

    invoke-virtual {p1, v0}, LX/0v6;->a(LX/0zO;)LX/0zX;

    move-result-object v0

    .line 2297034
    :goto_1
    invoke-static {v0}, LX/FsD;->b(LX/0zX;)LX/0zX;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0zX;->a(LX/0QK;)LX/0zX;

    move-result-object v0

    .line 2297035
    invoke-virtual {v1, v0}, LX/0zX;->a(LX/0zX;)LX/0zX;

    move-result-object v0

    move-object v1, v0

    .line 2297036
    goto :goto_0

    .line 2297037
    :cond_0
    invoke-static {p0, v0}, LX/FsT;->a(LX/FsT;LX/0zO;)LX/0zX;

    move-result-object v0

    goto :goto_1

    .line 2297038
    :cond_1
    return-object v1
.end method

.method public final b(LX/G12;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zO;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/G12;",
            "LX/0zS;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0zO",
            "<",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2297025
    iget-object v0, p0, LX/FsT;->c:LX/Fsg;

    new-instance v1, LX/Fsp;

    .line 2297026
    iget-wide v6, p1, LX/G12;->a:J

    move-wide v2, v6

    .line 2297027
    const/4 v4, 0x0

    .line 2297028
    iget-boolean v5, p1, LX/G12;->c:Z

    move v5, v5

    .line 2297029
    invoke-direct {v1, v2, v3, v4, v5}, LX/Fsp;-><init>(JLjava/lang/String;Z)V

    const/4 v5, 0x4

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, LX/Fsg;->a(LX/Fsp;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;I)LX/0zO;

    move-result-object v0

    return-object v0
.end method

.method public final c(LX/G12;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0Px;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/G12;",
            "LX/0zS;",
            "I",
            "Lcom/facebook/common/callercontext/CallerContext;",
            ")",
            "LX/0Px",
            "<",
            "LX/0zO",
            "<",
            "Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$TimelineProtilesQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2297011
    iget-object v1, p0, LX/FsT;->a:LX/0ad;

    sget-short v2, LX/0wf;->ar:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2297012
    iget-object v0, p0, LX/FsT;->e:LX/G2W;

    invoke-virtual {p1}, LX/G12;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    .line 2297013
    iget-object v2, v0, LX/G2W;->b:LX/G2L;

    invoke-virtual {v2}, LX/G2L;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/G2W;->a(Ljava/lang/String;Ljava/util/List;)LX/G1Q;

    move-result-object v2

    move-object v0, v2

    .line 2297014
    iget-object v1, p0, LX/FsT;->a:LX/0ad;

    sget-wide v2, LX/0wf;->am:J

    const-wide/16 v4, 0x3840

    invoke-interface {v1, v2, v3, v4, v5}, LX/0ad;->a(JJ)J

    move-result-wide v2

    move-object v1, p2

    move v4, p3

    move-object v5, p4

    .line 2297015
    invoke-static/range {v0 .. v5}, LX/FsT;->a(LX/G1Q;LX/0zS;JILcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v0

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2297016
    :goto_0
    return-object v0

    .line 2297017
    :cond_0
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2297018
    iget-object v1, p0, LX/FsT;->h:LX/G2L;

    invoke-virtual {v1}, LX/G2L;->b()LX/0Px;

    move-result-object v8

    invoke-virtual {v8}, LX/0Px;->size()I

    move-result v9

    move v6, v0

    .line 2297019
    :goto_1
    if-ge v6, v9, :cond_1

    invoke-virtual {v8, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/lang/String;

    .line 2297020
    iget-object v0, p0, LX/FsT;->e:LX/G2W;

    invoke-virtual {p1}, LX/G12;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/G2W;->a(Ljava/lang/String;Ljava/util/List;)LX/G1Q;

    move-result-object v0

    .line 2297021
    invoke-static {p0, v1}, LX/FsT;->a(LX/FsT;Ljava/lang/String;)J

    move-result-wide v2

    move-object v1, p2

    move v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, LX/FsT;->a(LX/G1Q;LX/0zS;JILcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v0

    .line 2297022
    invoke-virtual {v7, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2297023
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_1

    .line 2297024
    :cond_1
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    goto :goto_0
.end method
