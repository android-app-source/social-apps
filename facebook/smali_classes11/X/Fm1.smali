.class public final LX/Fm1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel;",
        ">;",
        "LX/0P1",
        "<",
        "Ljava/lang/String;",
        "LX/0Px",
        "<",
        "Lcom/facebook/user/model/User;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Fm2;


# direct methods
.method public constructor <init>(LX/Fm2;)V
    .locals 0

    .prologue
    .line 2281903
    iput-object p1, p0, LX/Fm1;->a:LX/Fm2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 10
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2281904
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2281905
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v1

    .line 2281906
    if-eqz p1, :cond_0

    .line 2281907
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2281908
    if-nez v0, :cond_1

    .line 2281909
    :cond_0
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    .line 2281910
    :goto_0
    move-object v0, v0

    .line 2281911
    return-object v0

    .line 2281912
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2281913
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel;->j()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel;

    move-result-object v0

    .line 2281914
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel;->a()LX/0Px;

    move-result-object v2

    if-nez v2, :cond_3

    .line 2281915
    :cond_2
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto :goto_0

    .line 2281916
    :cond_3
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2281917
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v2, 0x0

    move v4, v2

    :goto_1
    if-ge v4, v7, :cond_6

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel;

    .line 2281918
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v8

    .line 2281919
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    if-eqz v8, :cond_4

    .line 2281920
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel;->j()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel$CurrentCityModel;

    move-result-object v3

    if-nez v3, :cond_5

    const-string v3, ""

    .line 2281921
    :goto_2
    new-instance v9, LX/0XI;

    invoke-direct {v9}, LX/0XI;-><init>()V

    sget-object p0, LX/0XG;->FACEBOOK:LX/0XG;

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel;->k()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v9, p0, p1}, LX/0XI;->a(LX/0XG;Ljava/lang/String;)LX/0XI;

    move-result-object v9

    new-instance p0, Lcom/facebook/user/model/Name;

    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/facebook/user/model/Name;-><init>(Ljava/lang/String;)V

    .line 2281922
    iput-object p0, v9, LX/0XI;->g:Lcom/facebook/user/model/Name;

    .line 2281923
    move-object v2, v9

    .line 2281924
    invoke-virtual {v8}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v8

    .line 2281925
    iput-object v8, v2, LX/0XI;->n:Ljava/lang/String;

    .line 2281926
    move-object v2, v2

    .line 2281927
    iput-object v3, v2, LX/0XI;->x:Ljava/lang/String;

    .line 2281928
    move-object v2, v2

    .line 2281929
    invoke-virtual {v2}, LX/0XI;->aj()Lcom/facebook/user/model/User;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2281930
    :cond_4
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 2281931
    :cond_5
    invoke-virtual {v2}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel;->j()Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel$CurrentCityModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserInviteSuggestionsQueryModel$InviteSuggestionsModel$NodesModel$CurrentCityModel;->a()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    .line 2281932
    :cond_6
    move-object v0, v5

    .line 2281933
    const-string v2, "suggested_section_id"

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2281934
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v0

    goto/16 :goto_0
.end method
