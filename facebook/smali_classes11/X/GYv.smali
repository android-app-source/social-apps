.class public final LX/GYv;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GYw;


# direct methods
.method public constructor <init>(LX/GYw;)V
    .locals 0

    .prologue
    .line 2365832
    iput-object p1, p0, LX/GYv;->a:LX/GYw;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2365833
    iget-object v0, p0, LX/GYv;->a:LX/GYw;

    iget-object v0, v0, LX/GYw;->d:LX/GYk;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ProductCreationFields failure. pageId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LX/GYv;->a:LX/GYw;

    iget-object v2, v2, LX/GYw;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, LX/GYk;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2365834
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2365835
    check-cast p1, Lcom/facebook/commerce/publishing/graphql/FetchAdminCommerceProductCreationFieldsModels$PageShopProductCreationFieldsModel;

    .line 2365836
    iget-object v0, p0, LX/GYv;->a:LX/GYw;

    iget-object v0, v0, LX/GYw;->d:LX/GYk;

    invoke-interface {v0, p1}, LX/GYk;->a(Ljava/lang/Object;)V

    .line 2365837
    return-void
.end method
