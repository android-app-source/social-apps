.class public LX/GrZ;
.super LX/Cts;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/GnF;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/GrY;",
            ">;"
        }
    .end annotation
.end field

.field private d:LX/Ctg;


# direct methods
.method public constructor <init>(LX/Ctg;Ljava/util/List;Landroid/content/Context;LX/GnF;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/Ctg;",
            "Ljava/util/List",
            "<",
            "LX/GrY;",
            ">;",
            "Landroid/content/Context;",
            "LX/GnF;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2398373
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 2398374
    iput-object p1, p0, LX/GrZ;->d:LX/Ctg;

    .line 2398375
    iput-object p3, p0, LX/GrZ;->a:Landroid/content/Context;

    .line 2398376
    iput-object p4, p0, LX/GrZ;->b:LX/GnF;

    .line 2398377
    iput-object p2, p0, LX/GrZ;->c:Ljava/util/List;

    .line 2398378
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GrY;

    .line 2398379
    iget-object p3, v0, LX/GrY;->a:Lcom/facebook/instantshopping/view/widget/TouchTargetView;

    const/4 p4, 0x0

    invoke-virtual {p3, p4}, Lcom/facebook/instantshopping/view/widget/TouchTargetView;->setVisibility(I)V

    .line 2398380
    iget-object p3, v0, LX/GrY;->a:Lcom/facebook/instantshopping/view/widget/TouchTargetView;

    new-instance p4, LX/GrX;

    invoke-direct {p4, p0, v0}, LX/GrX;-><init>(LX/GrZ;LX/GrY;)V

    invoke-virtual {p3, p4}, Lcom/facebook/instantshopping/view/widget/TouchTargetView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 2398381
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2398382
    iget-object v0, p0, LX/GrZ;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GrY;

    .line 2398383
    iget-object v2, v0, LX/GrY;->a:Lcom/facebook/instantshopping/view/widget/TouchTargetView;

    if-eqz v2, :cond_0

    .line 2398384
    iget-object v0, v0, LX/GrY;->a:Lcom/facebook/instantshopping/view/widget/TouchTargetView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/facebook/instantshopping/view/widget/TouchTargetView;->setVisibility(I)V

    goto :goto_0

    .line 2398385
    :cond_1
    return-void
.end method

.method public final a(LX/CrS;)V
    .locals 10

    .prologue
    .line 2398386
    iget-object v0, p0, LX/GrZ;->c:Ljava/util/List;

    if-nez v0, :cond_1

    .line 2398387
    :cond_0
    return-void

    .line 2398388
    :cond_1
    iget-object v0, p0, LX/GrZ;->d:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2398389
    invoke-virtual {p0}, LX/Cts;->i()Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v1

    .line 2398390
    iget-object v0, p0, LX/GrZ;->d:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v0

    invoke-interface {v0}, LX/Ct1;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v0

    .line 2398391
    iget-object v2, v0, LX/CrW;->a:Landroid/graphics/Rect;

    move-object v2, v2

    .line 2398392
    iget-object v0, p0, LX/GrZ;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GrY;

    .line 2398393
    iget-object v4, v0, LX/GrY;->a:Lcom/facebook/instantshopping/view/widget/TouchTargetView;

    invoke-virtual {v4}, Lcom/facebook/instantshopping/view/widget/TouchTargetView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 2398394
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    iget-object v6, v0, LX/GrY;->b:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    mul-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2398395
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    iget-object v6, v0, LX/GrY;->b:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    mul-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 2398396
    iget-object v5, v0, LX/GrY;->a:Lcom/facebook/instantshopping/view/widget/TouchTargetView;

    invoke-virtual {v5, v4}, Lcom/facebook/instantshopping/view/widget/TouchTargetView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2398397
    new-instance v4, Landroid/graphics/Rect;

    iget-object v5, v0, LX/GrY;->b:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    invoke-virtual {v1}, LX/CrW;->i()I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    float-to-int v5, v5

    iget-object v6, v0, LX/GrY;->b:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v6, v7

    float-to-int v6, v6

    iget-object v7, v0, LX/GrY;->b:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v7, v8

    invoke-virtual {v1}, LX/CrW;->i()I

    move-result v8

    int-to-float v8, v8

    add-float/2addr v7, v8

    float-to-int v7, v7

    iget-object v8, v0, LX/GrY;->b:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v9

    int-to-float v9, v9

    mul-float/2addr v8, v9

    float-to-int v8, v8

    invoke-direct {v4, v5, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2398398
    iget-object v5, p0, LX/GrZ;->d:LX/Ctg;

    iget-object v0, v0, LX/GrY;->a:Lcom/facebook/instantshopping/view/widget/TouchTargetView;

    invoke-interface {v5, v0, v4}, LX/Ctg;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    goto :goto_0
.end method
