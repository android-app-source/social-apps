.class public final LX/Gfp;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Gfq;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/GfY;

.field public b:LX/1Pp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/Gfq;


# direct methods
.method public constructor <init>(LX/Gfq;)V
    .locals 1

    .prologue
    .line 2377987
    iput-object p1, p0, LX/Gfp;->c:LX/Gfq;

    .line 2377988
    move-object v0, p1

    .line 2377989
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2377990
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2377972
    const-string v0, "PaginatedPymlCardBottomComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2377973
    if-ne p0, p1, :cond_1

    .line 2377974
    :cond_0
    :goto_0
    return v0

    .line 2377975
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2377976
    goto :goto_0

    .line 2377977
    :cond_3
    check-cast p1, LX/Gfp;

    .line 2377978
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2377979
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2377980
    if-eq v2, v3, :cond_0

    .line 2377981
    iget-object v2, p0, LX/Gfp;->a:LX/GfY;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Gfp;->a:LX/GfY;

    iget-object v3, p1, LX/Gfp;->a:LX/GfY;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2377982
    goto :goto_0

    .line 2377983
    :cond_5
    iget-object v2, p1, LX/Gfp;->a:LX/GfY;

    if-nez v2, :cond_4

    .line 2377984
    :cond_6
    iget-object v2, p0, LX/Gfp;->b:LX/1Pp;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/Gfp;->b:LX/1Pp;

    iget-object v3, p1, LX/Gfp;->b:LX/1Pp;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2377985
    goto :goto_0

    .line 2377986
    :cond_7
    iget-object v2, p1, LX/Gfp;->b:LX/1Pp;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
