.class public LX/H5T;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/H5R;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H5U;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2424416
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/H5T;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/H5U;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2424413
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2424414
    iput-object p1, p0, LX/H5T;->b:LX/0Ot;

    .line 2424415
    return-void
.end method

.method public static a(LX/0QB;)LX/H5T;
    .locals 4

    .prologue
    .line 2424402
    const-class v1, LX/H5T;

    monitor-enter v1

    .line 2424403
    :try_start_0
    sget-object v0, LX/H5T;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2424404
    sput-object v2, LX/H5T;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2424405
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2424406
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2424407
    new-instance v3, LX/H5T;

    const/16 p0, 0x2adc

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/H5T;-><init>(LX/0Ot;)V

    .line 2424408
    move-object v0, v3

    .line 2424409
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2424410
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/H5T;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2424411
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2424412
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2424372
    const v0, 0x6c29a7d8

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2424384
    check-cast p2, LX/H5S;

    .line 2424385
    iget-object v0, p0, LX/H5T;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    iget v0, p2, LX/H5S;->a:I

    const/4 v4, 0x2

    const/4 v5, 0x0

    .line 2424386
    invoke-static {p1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v1

    const v2, 0x7f0a009f

    invoke-virtual {v1, v2}, LX/25Q;->i(I)LX/25Q;

    move-result-object v1

    invoke-virtual {v1}, LX/1X5;->c()LX/1Di;

    move-result-object v1

    const v2, 0x7f0b0a98

    invoke-interface {v1, v2}, LX/1Di;->q(I)LX/1Di;

    move-result-object v1

    const/4 v2, 0x3

    invoke-interface {v1, v2, v5}, LX/1Di;->k(II)LX/1Di;

    move-result-object v1

    .line 2424387
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    const/16 v3, 0x8

    invoke-interface {v2, v3, v5}, LX/1Dh;->w(II)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x1

    invoke-interface {v2, v3}, LX/1Dh;->R(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v2

    const v3, 0x7f0a0097

    invoke-interface {v2, v3}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v2

    .line 2424388
    const v3, 0x6c29a7d8

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v3

    move-object v3, v3

    .line 2424389
    invoke-interface {v2, v3}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    const/4 v3, 0x7

    const v4, 0x7f0b0a8f

    invoke-interface {v2, v3, v4}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    .line 2424390
    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2424391
    new-instance v6, Landroid/text/SpannableString;

    const v7, 0x7f081181

    invoke-virtual {p1, v7}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2424392
    const v7, 0x7f0a00a4

    invoke-static {v6, p1, v7}, LX/H5U;->a(Landroid/text/Spannable;LX/1De;I)V

    .line 2424393
    invoke-virtual {v4, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2424394
    if-lez v0, :cond_0

    .line 2424395
    new-instance v6, Landroid/text/SpannableStringBuilder;

    const v7, 0x7f081182

    invoke-virtual {p1, v7}, LX/1De;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0f0084

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 p0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    aput-object p2, v9, p0

    invoke-virtual {v7, v8, v0, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v6

    .line 2424396
    const v7, 0x7f0a00a4

    invoke-static {v6, p1, v7}, LX/H5U;->a(Landroid/text/Spannable;LX/1De;I)V

    .line 2424397
    invoke-virtual {v4, v6}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2424398
    :cond_0
    move-object v4, v4

    .line 2424399
    invoke-virtual {v3, v4}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    const v4, 0x7f0b0a8e

    invoke-virtual {v3, v4}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v2

    .line 2424400
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const v4, 0x7f0b0a91

    invoke-interface {v3, v4}, LX/1Dh;->J(I)LX/1Dh;

    move-result-object v3

    invoke-interface {v3, v1}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v1

    invoke-interface {v1}, LX/1Di;->k()LX/1Dg;

    move-result-object v1

    move-object v0, v1

    .line 2424401
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2424373
    invoke-static {}, LX/1dS;->b()V

    .line 2424374
    iget v0, p1, LX/1dQ;->b:I

    .line 2424375
    packed-switch v0, :pswitch_data_0

    .line 2424376
    :goto_0
    return-object v2

    .line 2424377
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2424378
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2424379
    check-cast v1, LX/H5S;

    .line 2424380
    iget-object v3, p0, LX/H5T;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/H5U;

    iget v4, v1, LX/H5S;->a:I

    .line 2424381
    iget-object v5, v3, LX/H5U;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    sget-object p2, LX/0ax;->cW:Ljava/lang/String;

    sget-object p0, LX/5Oz;->NOTIFICATIONS_FRIENDING_TAB_SEE_ALL_FRIEND_REQUESTS:LX/5Oz;

    invoke-virtual {p0}, LX/5Oz;->name()Ljava/lang/String;

    move-result-object p0

    sget-object v1, LX/5P0;->REQUESTS:LX/5P0;

    invoke-virtual {v1}, LX/5P0;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, p0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v5, p1, p2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2424382
    iget-object v5, v3, LX/H5U;->b:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/3TK;

    invoke-virtual {v5, v4}, LX/3TK;->b(I)V

    .line 2424383
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x6c29a7d8
        :pswitch_0
    .end packed-switch
.end method
