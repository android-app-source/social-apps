.class public LX/FyX;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""


# instance fields
.field public final a:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Landroid/widget/ImageView;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0zw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0zw",
            "<",
            "Lcom/facebook/widget/text/BetterTextView;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/view/animation/Animation;

.field public final d:Landroid/graphics/drawable/Drawable;

.field public final e:Landroid/graphics/drawable/Drawable;

.field public f:I

.field public g:I

.field public h:I

.field public i:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2306830
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2306831
    const v0, 0x7f0314e9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2306832
    iput-object p2, p0, LX/FyX;->i:Ljava/lang/String;

    .line 2306833
    const v0, 0x7f0d2f59

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2306834
    new-instance v1, LX/0zw;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, LX/FyX;->a:LX/0zw;

    .line 2306835
    const v0, 0x7f0d2f5a

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 2306836
    new-instance v1, LX/0zw;

    invoke-direct {v1, v0}, LX/0zw;-><init>(Landroid/view/ViewStub;)V

    iput-object v1, p0, LX/FyX;->b:LX/0zw;

    .line 2306837
    invoke-virtual {p0}, LX/FyX;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f020993

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, LX/FyX;->c:Landroid/view/animation/Animation;

    .line 2306838
    iget-object v0, p0, LX/FyX;->c:Landroid/view/animation/Animation;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 2306839
    iget-object v0, p0, LX/FyX;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2306840
    invoke-virtual {p0}, LX/FyX;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020723

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/FyX;->d:Landroid/graphics/drawable/Drawable;

    .line 2306841
    :goto_0
    invoke-virtual {p0}, LX/FyX;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020994

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/FyX;->e:Landroid/graphics/drawable/Drawable;

    .line 2306842
    const v0, 0x7f0a0048

    invoke-virtual {p0, v0}, LX/FyX;->setBackgroundResource(I)V

    .line 2306843
    invoke-virtual {p0}, LX/FyX;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0dae

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/FyX;->f:I

    .line 2306844
    invoke-virtual {p0}, LX/FyX;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0daf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/FyX;->g:I

    .line 2306845
    invoke-virtual {p0}, LX/FyX;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0db1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/FyX;->h:I

    .line 2306846
    invoke-virtual {p0}, LX/FyX;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0815e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/FyX;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 2306847
    return-void

    .line 2306848
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, LX/FyX;->d:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method
