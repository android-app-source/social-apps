.class public final enum LX/G1J;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/G1J;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/G1J;

.field public static final enum BOTTOM_ROW:LX/G1J;

.field public static final enum MIDDLE_ROW:LX/G1J;

.field public static final enum TOP_ROW:LX/G1J;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2311053
    new-instance v0, LX/G1J;

    const-string v1, "TOP_ROW"

    invoke-direct {v0, v1, v2}, LX/G1J;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G1J;->TOP_ROW:LX/G1J;

    .line 2311054
    new-instance v0, LX/G1J;

    const-string v1, "MIDDLE_ROW"

    invoke-direct {v0, v1, v3}, LX/G1J;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G1J;->MIDDLE_ROW:LX/G1J;

    .line 2311055
    new-instance v0, LX/G1J;

    const-string v1, "BOTTOM_ROW"

    invoke-direct {v0, v1, v4}, LX/G1J;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G1J;->BOTTOM_ROW:LX/G1J;

    .line 2311056
    const/4 v0, 0x3

    new-array v0, v0, [LX/G1J;

    sget-object v1, LX/G1J;->TOP_ROW:LX/G1J;

    aput-object v1, v0, v2

    sget-object v1, LX/G1J;->MIDDLE_ROW:LX/G1J;

    aput-object v1, v0, v3

    sget-object v1, LX/G1J;->BOTTOM_ROW:LX/G1J;

    aput-object v1, v0, v4

    sput-object v0, LX/G1J;->$VALUES:[LX/G1J;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2311057
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/G1J;
    .locals 1

    .prologue
    .line 2311058
    const-class v0, LX/G1J;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/G1J;

    return-object v0
.end method

.method public static values()[LX/G1J;
    .locals 1

    .prologue
    .line 2311059
    sget-object v0, LX/G1J;->$VALUES:[LX/G1J;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/G1J;

    return-object v0
.end method
