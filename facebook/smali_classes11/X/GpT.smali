.class public LX/GpT;
.super LX/Cne;
.source ""


# instance fields
.field public g:LX/Go7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public h:LX/Go4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private i:LX/GoE;

.field private j:LX/Gq0;


# direct methods
.method public constructor <init>(LX/Cow;)V
    .locals 2

    .prologue
    .line 2394995
    invoke-direct {p0, p1}, LX/Cne;-><init>(LX/Cow;)V

    .line 2394996
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/GpT;

    invoke-static {v0}, LX/Go7;->a(LX/0QB;)LX/Go7;

    move-result-object p1

    check-cast p1, LX/Go7;

    invoke-static {v0}, LX/Go4;->a(LX/0QB;)LX/Go4;

    move-result-object v0

    check-cast v0, LX/Go4;

    iput-object p1, p0, LX/GpT;->g:LX/Go7;

    iput-object v0, p0, LX/GpT;->h:LX/Go4;

    .line 2394997
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(LX/Clr;)V
    .locals 0

    .prologue
    .line 2394994
    check-cast p1, LX/Clw;

    invoke-virtual {p0, p1}, LX/Cne;->a(LX/Clw;)V

    return-void
.end method

.method public final a(LX/Clw;)V
    .locals 4

    .prologue
    .line 2394998
    check-cast p1, LX/Gow;

    .line 2394999
    invoke-virtual {p1}, LX/Got;->D()LX/GoE;

    move-result-object v0

    iput-object v0, p0, LX/GpT;->i:LX/GoE;

    .line 2395000
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395001
    check-cast v0, LX/Gq0;

    iput-object v0, p0, LX/GpT;->j:LX/Gq0;

    .line 2395002
    invoke-super {p0, p1}, LX/Cne;->a(LX/Clw;)V

    .line 2395003
    iget v0, p1, LX/Gow;->b:I

    move v0, v0

    .line 2395004
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2395005
    iget-object v1, p0, LX/GpT;->j:LX/Gq0;

    invoke-virtual {v1, v0}, LX/Gq0;->a(I)V

    .line 2395006
    :cond_0
    iget-object v0, p0, LX/GpT;->j:LX/Gq0;

    iget-object v1, p0, LX/GpT;->i:LX/GoE;

    .line 2395007
    iget-object v2, v0, LX/Gq0;->k:LX/Go0;

    invoke-virtual {v2, v1}, LX/Go0;->a(LX/GoE;)V

    .line 2395008
    invoke-virtual {p1}, LX/Got;->w()Ljava/lang/String;

    move-result-object v0

    .line 2395009
    iget-object v1, p0, LX/GpT;->j:LX/Gq0;

    iget-object v2, p0, LX/Cne;->d:LX/1Fb;

    invoke-interface {v2}, LX/1Fb;->c()I

    move-result v2

    iget-object v3, p0, LX/Cne;->d:LX/1Fb;

    invoke-interface {v3}, LX/1Fb;->a()I

    move-result v3

    invoke-virtual {v1, v0, v2, v3}, LX/Cow;->a(Ljava/lang/String;II)V

    .line 2395010
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2394989
    invoke-super {p0, p1}, LX/Cne;->a(Landroid/os/Bundle;)V

    .line 2394990
    iget-object v0, p0, LX/GpT;->i:LX/GoE;

    if-eqz v0, :cond_0

    .line 2394991
    iget-object v0, p0, LX/GpT;->g:LX/Go7;

    const-string v1, "image_element_start"

    iget-object v2, p0, LX/GpT;->i:LX/GoE;

    invoke-virtual {v2}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2394992
    iget-object v0, p0, LX/GpT;->h:LX/Go4;

    iget-object v1, p0, LX/GpT;->i:LX/GoE;

    invoke-virtual {v1}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Go4;->a(Ljava/lang/String;)V

    .line 2394993
    :cond_0
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2394984
    invoke-super {p0, p1}, LX/Cne;->b(Landroid/os/Bundle;)V

    .line 2394985
    iget-object v0, p0, LX/GpT;->i:LX/GoE;

    if-eqz v0, :cond_0

    .line 2394986
    iget-object v0, p0, LX/GpT;->g:LX/Go7;

    const-string v1, "image_element_end"

    iget-object v2, p0, LX/GpT;->i:LX/GoE;

    invoke-virtual {v2}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2394987
    iget-object v0, p0, LX/GpT;->h:LX/Go4;

    iget-object v1, p0, LX/GpT;->i:LX/GoE;

    invoke-virtual {v1}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Go4;->b(Ljava/lang/String;)V

    .line 2394988
    :cond_0
    return-void
.end method
