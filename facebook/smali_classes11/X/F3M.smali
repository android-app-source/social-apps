.class public final LX/F3M;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;)V
    .locals 0

    .prologue
    .line 2194021
    iput-object p1, p0, LX/F3M;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2194022
    iget-object v0, p0, LX/F3M;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    const/4 v1, 0x0

    .line 2194023
    iput-boolean v1, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->q:Z

    .line 2194024
    iget-object v0, p0, LX/F3M;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    invoke-static {v0}, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->n$redex0(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;)V

    .line 2194025
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2194026
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2194027
    iget-object v0, p0, LX/F3M;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    const/4 v1, 0x0

    .line 2194028
    iput-boolean v1, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->q:Z

    .line 2194029
    iget-object v1, p0, LX/F3M;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    .line 2194030
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2194031
    check-cast v0, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;

    .line 2194032
    iput-object v0, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->k:Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;

    .line 2194033
    iget-object v0, p0, LX/F3M;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    iget-object v0, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->k:Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;

    if-eqz v0, :cond_0

    .line 2194034
    iget-object v0, p0, LX/F3M;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    .line 2194035
    iget-object v1, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->i:LX/F30;

    iget-object p1, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->k:Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;

    .line 2194036
    iput-object p1, v1, LX/F30;->a:Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;

    .line 2194037
    iget-object v1, v0, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->i:LX/F30;

    const p1, 0x37ee095a

    invoke-static {v1, p1}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2194038
    :cond_0
    iget-object v0, p0, LX/F3M;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;

    invoke-static {v0}, Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;->n$redex0(Lcom/facebook/groups/editsettings/fragment/GroupEditTagsFragment;)V

    .line 2194039
    return-void
.end method
