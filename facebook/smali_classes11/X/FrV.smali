.class public LX/FrV;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public volatile a:LX/0Or;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/FqO;

.field private final c:LX/BP0;

.field private final d:LX/G11;

.field private final e:LX/G4x;

.field private final f:LX/G4m;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final g:LX/G1N;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final h:LX/BQB;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final i:Lcom/facebook/common/callercontext/CallerContext;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final j:Landroid/content/Context;

.field private k:LX/Frb;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private l:LX/Fre;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private m:LX/Fra;

.field private n:LX/Frd;

.field public o:LX/FsJ;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/FqO;LX/BP0;LX/G11;LX/G4x;LX/G4m;LX/G1N;LX/BQB;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/FqO;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/BP0;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/G11;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/G4x;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/G4m;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p7    # LX/G1N;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p8    # LX/BQB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p9    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation

        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2295693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2295694
    iput-object p1, p0, LX/FrV;->j:Landroid/content/Context;

    .line 2295695
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FqO;

    iput-object v0, p0, LX/FrV;->b:LX/FqO;

    .line 2295696
    iput-object p3, p0, LX/FrV;->c:LX/BP0;

    .line 2295697
    iput-object p4, p0, LX/FrV;->d:LX/G11;

    .line 2295698
    iput-object p5, p0, LX/FrV;->e:LX/G4x;

    .line 2295699
    iput-object p6, p0, LX/FrV;->f:LX/G4m;

    .line 2295700
    iput-object p7, p0, LX/FrV;->g:LX/G1N;

    .line 2295701
    iput-object p8, p0, LX/FrV;->h:LX/BQB;

    .line 2295702
    iput-object p9, p0, LX/FrV;->i:Lcom/facebook/common/callercontext/CallerContext;

    .line 2295703
    return-void
.end method

.method public static a(LX/FrV;LX/0Or;LX/Frb;LX/Fre;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FrV;",
            "LX/0Or",
            "<",
            "LX/0ad;",
            ">;",
            "LX/Frb;",
            "LX/Fre;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2295689
    iput-object p1, p0, LX/FrV;->a:LX/0Or;

    iput-object p2, p0, LX/FrV;->k:LX/Frb;

    iput-object p3, p0, LX/FrV;->l:LX/Fre;

    return-void
.end method


# virtual methods
.method public final a()LX/Fra;
    .locals 5

    .prologue
    .line 2295690
    iget-object v0, p0, LX/FrV;->m:LX/Fra;

    if-nez v0, :cond_0

    .line 2295691
    iget-object v0, p0, LX/FrV;->k:LX/Frb;

    iget-object v1, p0, LX/FrV;->j:Landroid/content/Context;

    iget-object v2, p0, LX/FrV;->c:LX/BP0;

    iget-object v3, p0, LX/FrV;->h:LX/BQB;

    iget-object v4, p0, LX/FrV;->i:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/Frb;->a(Landroid/content/Context;LX/BP0;LX/BQB;Lcom/facebook/common/callercontext/CallerContext;)LX/Fra;

    move-result-object v0

    iput-object v0, p0, LX/FrV;->m:LX/Fra;

    .line 2295692
    :cond_0
    iget-object v0, p0, LX/FrV;->m:LX/Fra;

    return-object v0
.end method

.method public final b()LX/Frd;
    .locals 9

    .prologue
    .line 2295686
    iget-object v0, p0, LX/FrV;->n:LX/Frd;

    if-nez v0, :cond_0

    .line 2295687
    iget-object v0, p0, LX/FrV;->l:LX/Fre;

    iget-object v1, p0, LX/FrV;->j:Landroid/content/Context;

    iget-object v2, p0, LX/FrV;->b:LX/FqO;

    iget-object v3, p0, LX/FrV;->c:LX/BP0;

    iget-object v4, p0, LX/FrV;->d:LX/G11;

    iget-object v5, p0, LX/FrV;->e:LX/G4x;

    iget-object v6, p0, LX/FrV;->f:LX/G4m;

    iget-object v7, p0, LX/FrV;->g:LX/G1N;

    iget-object v8, p0, LX/FrV;->i:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual/range {v0 .. v8}, LX/Fre;->a(Landroid/content/Context;LX/FqO;LX/5SB;LX/G11;LX/G4x;LX/G4m;LX/G1N;Lcom/facebook/common/callercontext/CallerContext;)LX/Frd;

    move-result-object v0

    iput-object v0, p0, LX/FrV;->n:LX/Frd;

    .line 2295688
    :cond_0
    iget-object v0, p0, LX/FrV;->n:LX/Frd;

    return-object v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 2295683
    invoke-virtual {p0}, LX/FrV;->b()LX/Frd;

    move-result-object v0

    invoke-virtual {v0}, LX/Frd;->b()V

    .line 2295684
    invoke-virtual {p0}, LX/FrV;->a()LX/Fra;

    move-result-object v0

    invoke-virtual {v0}, LX/Fra;->a()V

    .line 2295685
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2295679
    iget-object v0, p0, LX/FrV;->o:LX/FsJ;

    if-eqz v0, :cond_0

    .line 2295680
    invoke-virtual {p0}, LX/FrV;->b()LX/Frd;

    move-result-object v0

    iget-object v1, p0, LX/FrV;->o:LX/FsJ;

    invoke-virtual {v0, v1}, LX/Frd;->b(LX/FsJ;)V

    .line 2295681
    const/4 v0, 0x0

    iput-object v0, p0, LX/FrV;->o:LX/FsJ;

    .line 2295682
    :cond_0
    return-void
.end method
