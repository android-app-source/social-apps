.class public final LX/GDn;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GDp;


# direct methods
.method public constructor <init>(LX/GDp;)V
    .locals 0

    .prologue
    .line 2330517
    iput-object p1, p0, LX/GDn;->a:LX/GDp;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2330518
    iget-object v0, p0, LX/GDn;->a:LX/GDp;

    .line 2330519
    iget-object v1, v0, LX/GDp;->a:LX/4At;

    invoke-virtual {v1}, LX/4At;->stopShowingProgress()V

    .line 2330520
    instance-of v1, p1, LX/4Ua;

    if-eqz v1, :cond_0

    .line 2330521
    check-cast p1, LX/4Ua;

    .line 2330522
    iget-object v1, p1, LX/4Ua;->error:Lcom/facebook/graphql/error/GraphQLError;

    iget-object v1, v1, Lcom/facebook/graphql/error/GraphQLError;->description:Ljava/lang/String;

    const-string v2, "&#039;"

    const-string p0, "\'"

    invoke-virtual {v1, v2, p0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2330523
    iget-object v2, v0, LX/GDp;->d:LX/GF4;

    new-instance p0, LX/GFP;

    invoke-direct {p0, v1}, LX/GFP;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, LX/0b4;->a(LX/0b7;)V

    .line 2330524
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2330525
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2330526
    iget-object v0, p0, LX/GDn;->a:LX/GDp;

    const/4 p0, 0x1

    .line 2330527
    iget-object v1, v0, LX/GDp;->a:LX/4At;

    invoke-virtual {v1}, LX/4At;->stopShowingProgress()V

    .line 2330528
    iget-object v1, v0, LX/GDp;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->v()LX/0Px;

    move-result-object v1

    iget-object v2, v0, LX/GDp;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v2

    .line 2330529
    iget-object v3, v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v2, v3

    .line 2330530
    invoke-static {v1, v2}, LX/GG6;->b(LX/0Px;Ljava/lang/String;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-result-object v1

    .line 2330531
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->a()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2330532
    new-instance v3, LX/A94;

    invoke-direct {v3}, LX/A94;-><init>()V

    invoke-virtual {v3, v2, v1}, LX/A94;->a(LX/15i;I)LX/A94;

    move-result-object v1

    iget-object v2, v0, LX/GDp;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v2

    .line 2330533
    iget-object v3, v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v2, v3

    .line 2330534
    iput-object v2, v1, LX/A94;->c:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2330535
    move-object v1, v1

    .line 2330536
    iget-object v2, v0, LX/GDp;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v2

    .line 2330537
    iget-object v3, v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v2, v3

    .line 2330538
    iput-object v2, v1, LX/A94;->d:Ljava/lang/String;

    .line 2330539
    move-object v1, v1

    .line 2330540
    iput-boolean p0, v1, LX/A94;->e:Z

    .line 2330541
    move-object v2, v1

    .line 2330542
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2330543
    check-cast v1, Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;->k()Ljava/lang/String;

    move-result-object v1

    .line 2330544
    iput-object v1, v2, LX/A94;->g:Ljava/lang/String;

    .line 2330545
    move-object v1, v2

    .line 2330546
    iget-object v2, v0, LX/GDp;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v2

    .line 2330547
    iget-object v3, v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->l:LX/0Px;

    move-object v2, v3

    .line 2330548
    iput-object v2, v1, LX/A94;->h:LX/0Px;

    .line 2330549
    move-object v2, v1

    .line 2330550
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2330551
    check-cast v1, Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;->m()LX/2uF;

    move-result-object v1

    .line 2330552
    iput-object v1, v2, LX/A94;->j:LX/2uF;

    .line 2330553
    move-object v2, v2

    .line 2330554
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2330555
    check-cast v1, Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/EditSavedAudienceMutationModels$EditSavedAudienceMutationModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAudienceModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    move-result-object v1

    .line 2330556
    iput-object v1, v2, LX/A94;->k:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetSpecificationsModel;

    .line 2330557
    move-object v1, v2

    .line 2330558
    invoke-virtual {v1}, LX/A94;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-result-object v1

    .line 2330559
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 2330560
    const-string v3, "audience_extra"

    invoke-static {v2, v3, v1}, LX/4By;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Object;)V

    .line 2330561
    iget-object v1, v0, LX/GDp;->d:LX/GF4;

    new-instance v3, LX/GFS;

    invoke-direct {v3, v2, p0}, LX/GFS;-><init>(Landroid/content/Intent;Z)V

    invoke-virtual {v1, v3}, LX/0b4;->a(LX/0b7;)V

    .line 2330562
    return-void
.end method
