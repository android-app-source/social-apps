.class public final LX/GUt;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    .line 2358295
    const/4 v9, 0x0

    .line 2358296
    const/4 v8, 0x0

    .line 2358297
    const-wide/16 v6, 0x0

    .line 2358298
    const/4 v5, 0x0

    .line 2358299
    const/4 v4, 0x0

    .line 2358300
    const/4 v3, 0x0

    .line 2358301
    const/4 v2, 0x0

    .line 2358302
    const/4 v1, 0x0

    .line 2358303
    const/4 v0, 0x0

    .line 2358304
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v10, v11, :cond_b

    .line 2358305
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2358306
    const/4 v0, 0x0

    .line 2358307
    :goto_0
    return v0

    .line 2358308
    :cond_0
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v0

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v0, v11, :cond_6

    .line 2358309
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v0

    .line 2358310
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2358311
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_0

    if-eqz v0, :cond_0

    .line 2358312
    const-string v11, "is_sharing_enabled"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 2358313
    const/4 v0, 0x1

    .line 2358314
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v5

    move v10, v5

    move v5, v0

    goto :goto_1

    .line 2358315
    :cond_1
    const-string v11, "is_tracking_enabled"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 2358316
    const/4 v0, 0x1

    .line 2358317
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v4

    move v9, v4

    move v4, v0

    goto :goto_1

    .line 2358318
    :cond_2
    const-string v11, "pause_expiration_time"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 2358319
    const/4 v0, 0x1

    .line 2358320
    invoke-virtual {p0}, LX/15w;->F()J

    move-result-wide v2

    move v1, v0

    goto :goto_1

    .line 2358321
    :cond_3
    const-string v11, "show_nux"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 2358322
    const/4 v0, 0x1

    .line 2358323
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v6

    move v8, v6

    move v6, v0

    goto :goto_1

    .line 2358324
    :cond_4
    const-string v11, "upsell"

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2358325
    invoke-static {p0, p1}, LX/GUs;->a(LX/15w;LX/186;)I

    move-result v0

    move v7, v0

    goto :goto_1

    .line 2358326
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2358327
    :cond_6
    const/4 v0, 0x5

    invoke-virtual {p1, v0}, LX/186;->c(I)V

    .line 2358328
    if-eqz v5, :cond_7

    .line 2358329
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v10}, LX/186;->a(IZ)V

    .line 2358330
    :cond_7
    if-eqz v4, :cond_8

    .line 2358331
    const/4 v0, 0x1

    invoke-virtual {p1, v0, v9}, LX/186;->a(IZ)V

    .line 2358332
    :cond_8
    if-eqz v1, :cond_9

    .line 2358333
    const/4 v1, 0x2

    const-wide/16 v4, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IJJ)V

    .line 2358334
    :cond_9
    if-eqz v6, :cond_a

    .line 2358335
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v8}, LX/186;->a(IZ)V

    .line 2358336
    :cond_a
    const/4 v0, 0x4

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2358337
    invoke-virtual {p1}, LX/186;->d()I

    move-result v0

    goto/16 :goto_0

    :cond_b
    move v10, v9

    move v9, v8

    move v8, v5

    move v5, v3

    move v13, v4

    move v4, v2

    move-wide v2, v6

    move v7, v13

    move v6, v0

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2358338
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2358339
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2358340
    if-eqz v0, :cond_0

    .line 2358341
    const-string v1, "is_sharing_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2358342
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2358343
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2358344
    if-eqz v0, :cond_1

    .line 2358345
    const-string v1, "is_tracking_enabled"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2358346
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2358347
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2358348
    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    .line 2358349
    const-string v2, "pause_expiration_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2358350
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2358351
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2358352
    if-eqz v0, :cond_3

    .line 2358353
    const-string v1, "show_nux"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2358354
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2358355
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2358356
    if-eqz v0, :cond_4

    .line 2358357
    const-string v1, "upsell"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2358358
    invoke-static {p0, v0, p2, p3}, LX/GUs;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2358359
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2358360
    return-void
.end method
