.class public final LX/GL3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/GL7;


# direct methods
.method public constructor <init>(LX/GL7;)V
    .locals 0

    .prologue
    .line 2342065
    iput-object p1, p0, LX/GL3;->a:LX/GL7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2342066
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2342067
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2342068
    iget-object v0, p0, LX/GL3;->a:LX/GL7;

    iget-object v0, v0, LX/GL7;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2342069
    iput-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->q:Ljava/lang/String;

    .line 2342070
    return-void
.end method
