.class public LX/FMl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:[Ljava/lang/String;

.field private static volatile g:LX/FMl;


# instance fields
.field public final b:Landroid/content/Context;

.field private final c:LX/1Ml;

.field public final d:LX/2uq;

.field public final e:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final f:LX/2Oq;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2230617
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "android.permission.READ_SMS"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "android.permission.READ_CONTACTS"

    aput-object v2, v0, v1

    sput-object v0, LX/FMl;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;LX/1Ml;LX/2uq;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Oq;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2230618
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2230619
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/FMl;->b:Landroid/content/Context;

    .line 2230620
    iput-object p2, p0, LX/FMl;->c:LX/1Ml;

    .line 2230621
    iput-object p3, p0, LX/FMl;->d:LX/2uq;

    .line 2230622
    iput-object p4, p0, LX/FMl;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2230623
    iput-object p5, p0, LX/FMl;->f:LX/2Oq;

    .line 2230624
    return-void
.end method

.method public static a(LX/0QB;)LX/FMl;
    .locals 9

    .prologue
    .line 2230602
    sget-object v0, LX/FMl;->g:LX/FMl;

    if-nez v0, :cond_1

    .line 2230603
    const-class v1, LX/FMl;

    monitor-enter v1

    .line 2230604
    :try_start_0
    sget-object v0, LX/FMl;->g:LX/FMl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2230605
    if-eqz v2, :cond_0

    .line 2230606
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2230607
    new-instance v3, LX/FMl;

    const-class v4, Landroid/content/Context;

    invoke-interface {v0, v4}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/Context;

    invoke-static {v0}, LX/1Ml;->b(LX/0QB;)LX/1Ml;

    move-result-object v5

    check-cast v5, LX/1Ml;

    invoke-static {v0}, LX/2uq;->a(LX/0QB;)LX/2uq;

    move-result-object v6

    check-cast v6, LX/2uq;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v7

    check-cast v7, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/2Oq;->a(LX/0QB;)LX/2Oq;

    move-result-object v8

    check-cast v8, LX/2Oq;

    invoke-direct/range {v3 .. v8}, LX/FMl;-><init>(Landroid/content/Context;LX/1Ml;LX/2uq;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/2Oq;)V

    .line 2230608
    move-object v0, v3

    .line 2230609
    sput-object v0, LX/FMl;->g:LX/FMl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2230610
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2230611
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2230612
    :cond_1
    sget-object v0, LX/FMl;->g:LX/FMl;

    return-object v0

    .line 2230613
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2230614
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()Z
    .locals 2

    .prologue
    .line 2230616
    iget-object v0, p0, LX/FMl;->c:LX/1Ml;

    sget-object v1, LX/FMl;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/1Ml;->a([Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 2230615
    iget-object v0, p0, LX/FMl;->c:LX/1Ml;

    const-string v1, "android.permission.READ_PHONE_STATE"

    invoke-virtual {v0, v1}, LX/1Ml;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
