.class public final LX/GxB;
.super LX/BWJ;
.source ""


# instance fields
.field public final synthetic b:LX/GxF;


# direct methods
.method public constructor <init>(LX/GxF;)V
    .locals 0

    .prologue
    .line 2407758
    iput-object p1, p0, LX/GxB;->b:LX/GxF;

    invoke-direct {p0, p1}, LX/BWJ;-><init>(Lcom/facebook/webview/FacebookWebView;)V

    return-void
.end method


# virtual methods
.method public final onGeolocationPermissionsShowPrompt(Ljava/lang/String;Landroid/webkit/GeolocationPermissions$Callback;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2407759
    invoke-super {p0, p1, p2}, LX/BWJ;->onGeolocationPermissionsShowPrompt(Ljava/lang/String;Landroid/webkit/GeolocationPermissions$Callback;)V

    .line 2407760
    if-eqz p1, :cond_0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, LX/2yp;->d(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2407761
    invoke-interface {p2, p1, v2, v2}, Landroid/webkit/GeolocationPermissions$Callback;->invoke(Ljava/lang/String;ZZ)V

    .line 2407762
    :goto_0
    return-void

    .line 2407763
    :cond_0
    invoke-interface {p2, p1, v1, v1}, Landroid/webkit/GeolocationPermissions$Callback;->invoke(Ljava/lang/String;ZZ)V

    goto :goto_0
.end method
