.class public final LX/Gkx;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/groupsections/noncursored/protocol/FetchGroupSectionModels$FetchGroupSectionModel;",
        ">;>;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Gl6;


# direct methods
.method public constructor <init>(LX/Gl6;)V
    .locals 0

    .prologue
    .line 2389725
    iput-object p1, p0, LX/Gkx;->a:LX/Gl6;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2389726
    iget-object v0, p0, LX/Gkx;->a:LX/Gl6;

    iget-object v0, v0, LX/Gl6;->d:LX/GkH;

    invoke-interface {v0}, LX/GkH;->a()V

    .line 2389727
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2389728
    check-cast p1, Ljava/util/List;

    const/4 v1, 0x0

    .line 2389729
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v2, p0, LX/Gkx;->a:LX/Gl6;

    iget-object v2, v2, LX/Gl6;->g:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2389730
    iget-object v0, p0, LX/Gkx;->a:LX/Gl6;

    iget-object v0, v0, LX/Gl6;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkp;

    .line 2389731
    instance-of v2, v0, LX/Gkp;

    if-eqz v2, :cond_0

    .line 2389732
    add-int/lit8 v2, v1, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2389733
    check-cast v0, LX/Gkp;

    invoke-static {v0, v1}, LX/Gl6;->b(LX/Gkp;Lcom/facebook/graphql/executor/GraphQLResult;)Z

    move v1, v2

    goto :goto_1

    :cond_1
    move v0, v1

    .line 2389734
    goto :goto_0

    .line 2389735
    :cond_2
    iget-object v0, p0, LX/Gkx;->a:LX/Gl6;

    invoke-static {v0}, LX/Gl6;->g(LX/Gl6;)V

    .line 2389736
    return-void
.end method
