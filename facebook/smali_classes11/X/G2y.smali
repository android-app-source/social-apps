.class public final LX/G2y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "LX/3Fw;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/G2o;

.field public final synthetic b:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;LX/G2o;)V
    .locals 0

    .prologue
    .line 2314690
    iput-object p1, p0, LX/G2y;->b:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    iput-object p2, p0, LX/G2y;->a:LX/G2o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2314691
    iget-object v0, p0, LX/G2y;->b:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    iget-object v0, v0, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->c:LX/2dk;

    iget-object v1, p0, LX/G2y;->a:LX/G2o;

    .line 2314692
    iget-object v2, v1, LX/G2o;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-object v1, v2

    .line 2314693
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x14

    iget-object v3, p0, LX/G2y;->b:Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;

    iget-object v3, v3, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->j:Landroid/content/res/Resources;

    const v4, 0x7f0b0f69

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, LX/2hC;->SELF_PROFILE:LX/2hC;

    sget-object v5, Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowHScrollPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual/range {v0 .. v5}, LX/2dk;->a(Ljava/lang/String;ILjava/lang/Integer;LX/2hC;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
