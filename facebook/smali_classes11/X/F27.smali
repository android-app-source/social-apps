.class public LX/F27;
.super Landroid/graphics/drawable/Drawable;
.source ""


# instance fields
.field public final a:Landroid/content/res/Resources;

.field private final b:Landroid/graphics/drawable/LayerDrawable;

.field private c:Landroid/graphics/drawable/Drawable;

.field private d:F

.field private e:I

.field private f:I

.field private g:I

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;III)V
    .locals 6

    .prologue
    .line 2192876
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, LX/F27;-><init>(Landroid/content/res/Resources;IIIZ)V

    .line 2192877
    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;IIIZ)V
    .locals 2

    .prologue
    .line 2192867
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 2192868
    iput-object p1, p0, LX/F27;->a:Landroid/content/res/Resources;

    .line 2192869
    iput p2, p0, LX/F27;->e:I

    .line 2192870
    iput p3, p0, LX/F27;->f:I

    .line 2192871
    iput p4, p0, LX/F27;->g:I

    .line 2192872
    iput-boolean p5, p0, LX/F27;->h:Z

    .line 2192873
    const v0, 0x7f02192f

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    iput-object v0, p0, LX/F27;->b:Landroid/graphics/drawable/LayerDrawable;

    .line 2192874
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const v1, 0x7f0a0863

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, LX/F27;->c:Landroid/graphics/drawable/Drawable;

    .line 2192875
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 1

    .prologue
    .line 2192863
    iget v0, p0, LX/F27;->d:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 2192864
    iput p1, p0, LX/F27;->d:F

    .line 2192865
    invoke-virtual {p0}, LX/F27;->invalidateSelf()V

    .line 2192866
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 2192859
    iget v0, p0, LX/F27;->e:I

    if-eq p1, v0, :cond_0

    .line 2192860
    iput p1, p0, LX/F27;->e:I

    .line 2192861
    invoke-virtual {p0}, LX/F27;->invalidateSelf()V

    .line 2192862
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 2192831
    invoke-virtual {p0}, LX/F27;->getIntrinsicWidth()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0}, LX/F27;->getIntrinsicHeight()I

    move-result v1

    add-int/2addr v1, p2

    invoke-virtual {p0, p1, p2, v0, v1}, LX/F27;->setBounds(IIII)V

    .line 2192832
    return-void
.end method

.method public final a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 2192856
    iput-object p1, p0, LX/F27;->c:Landroid/graphics/drawable/Drawable;

    .line 2192857
    iget-object v0, p0, LX/F27;->b:Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/LayerDrawable;->invalidateSelf()V

    .line 2192858
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 2192852
    iget v0, p0, LX/F27;->f:I

    if-eq p1, v0, :cond_0

    .line 2192853
    iput p1, p0, LX/F27;->f:I

    .line 2192854
    invoke-virtual {p0}, LX/F27;->invalidateSelf()V

    .line 2192855
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 2192848
    iget v0, p0, LX/F27;->g:I

    if-eq p1, v0, :cond_0

    .line 2192849
    iput p1, p0, LX/F27;->g:I

    .line 2192850
    invoke-virtual {p0}, LX/F27;->invalidateSelf()V

    .line 2192851
    :cond_0
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    .line 2192838
    invoke-virtual {p0}, LX/F27;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    .line 2192839
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2192840
    iget v1, p0, LX/F27;->d:F

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v1, v2, v3}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 2192841
    iget-object v1, p0, LX/F27;->b:Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/LayerDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 2192842
    iget-object v1, p0, LX/F27;->c:Landroid/graphics/drawable/Drawable;

    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, p0, LX/F27;->e:I

    add-int/2addr v2, v3

    iget v3, v0, Landroid/graphics/Rect;->top:I

    iget v4, p0, LX/F27;->e:I

    add-int/2addr v3, v4

    iget v4, v0, Landroid/graphics/Rect;->right:I

    iget v5, p0, LX/F27;->e:I

    sub-int/2addr v4, v5

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget v5, p0, LX/F27;->f:I

    sub-int/2addr v0, v5

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2192843
    :try_start_0
    iget-boolean v0, p0, LX/F27;->h:Z

    if-eqz v0, :cond_0

    .line 2192844
    iget-object v0, p0, LX/F27;->b:Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/LayerDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 2192845
    :cond_0
    iget-object v0, p0, LX/F27;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2192846
    :goto_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2192847
    return-void

    :catch_0
    goto :goto_0
.end method

.method public final getIntrinsicHeight()I
    .locals 2

    .prologue
    .line 2192837
    iget v0, p0, LX/F27;->g:I

    iget v1, p0, LX/F27;->e:I

    add-int/2addr v0, v1

    iget v1, p0, LX/F27;->f:I

    add-int/2addr v0, v1

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 2

    .prologue
    .line 2192836
    iget v0, p0, LX/F27;->g:I

    iget v1, p0, LX/F27;->e:I

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 2192835
    const/4 v0, -0x1

    return v0
.end method

.method public final setAlpha(I)V
    .locals 2

    .prologue
    .line 2192834
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "If you need this, implement it"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 2

    .prologue
    .line 2192833
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "If you need this, implement it"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
