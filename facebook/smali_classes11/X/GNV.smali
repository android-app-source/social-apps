.class public LX/GNV;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/GNV;


# direct methods
.method public constructor <init>()V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2346122
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2346123
    const-string v0, "ads/mobile_preview"

    invoke-static {v0}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/facebook/adpreview/activity/AdPreviewActivity;

    invoke-virtual {p0, v0, v1}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;)V

    .line 2346124
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2346125
    const-string v1, "is_hype_ad_unit"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2346126
    const-string v1, "ads/hype_ad"

    invoke-static {v1}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-class v2, Lcom/facebook/adpreview/activity/AdPreviewActivity;

    invoke-virtual {p0, v1, v2, v0}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2346127
    return-void
.end method

.method public static a(LX/0QB;)LX/GNV;
    .locals 3

    .prologue
    .line 2346128
    sget-object v0, LX/GNV;->a:LX/GNV;

    if-nez v0, :cond_1

    .line 2346129
    const-class v1, LX/GNV;

    monitor-enter v1

    .line 2346130
    :try_start_0
    sget-object v0, LX/GNV;->a:LX/GNV;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2346131
    if-eqz v2, :cond_0

    .line 2346132
    :try_start_1
    new-instance v0, LX/GNV;

    invoke-direct {v0}, LX/GNV;-><init>()V

    .line 2346133
    move-object v0, v0

    .line 2346134
    sput-object v0, LX/GNV;->a:LX/GNV;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2346135
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2346136
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2346137
    :cond_1
    sget-object v0, LX/GNV;->a:LX/GNV;

    return-object v0

    .line 2346138
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2346139
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
