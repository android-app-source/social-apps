.class public final enum LX/GQy;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GQy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GQy;

.field public static final enum AUTO:LX/GQy;

.field public static final enum EXPLICIT_ONLY:LX/GQy;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2350976
    new-instance v0, LX/GQy;

    const-string v1, "AUTO"

    invoke-direct {v0, v1, v2}, LX/GQy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GQy;->AUTO:LX/GQy;

    .line 2350977
    new-instance v0, LX/GQy;

    const-string v1, "EXPLICIT_ONLY"

    invoke-direct {v0, v1, v3}, LX/GQy;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GQy;->EXPLICIT_ONLY:LX/GQy;

    .line 2350978
    const/4 v0, 0x2

    new-array v0, v0, [LX/GQy;

    sget-object v1, LX/GQy;->AUTO:LX/GQy;

    aput-object v1, v0, v2

    sget-object v1, LX/GQy;->EXPLICIT_ONLY:LX/GQy;

    aput-object v1, v0, v3

    sput-object v0, LX/GQy;->$VALUES:[LX/GQy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2350981
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GQy;
    .locals 1

    .prologue
    .line 2350980
    const-class v0, LX/GQy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GQy;

    return-object v0
.end method

.method public static values()[LX/GQy;
    .locals 1

    .prologue
    .line 2350979
    sget-object v0, LX/GQy;->$VALUES:[LX/GQy;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GQy;

    return-object v0
.end method
