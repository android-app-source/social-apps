.class public final LX/G6F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Landroid/widget/ScrollView;

.field public final synthetic b:Lcom/facebook/wem/watermark/WatermarkActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/wem/watermark/WatermarkActivity;Landroid/widget/ScrollView;)V
    .locals 0

    .prologue
    .line 2319834
    iput-object p1, p0, LX/G6F;->b:Lcom/facebook/wem/watermark/WatermarkActivity;

    iput-object p2, p0, LX/G6F;->a:Landroid/widget/ScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 2

    .prologue
    .line 2319835
    iget-object v0, p0, LX/G6F;->a:Landroid/widget/ScrollView;

    invoke-static {v0, p0}, LX/1r0;->a(Landroid/view/View;Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 2319836
    iget-object v0, p0, LX/G6F;->a:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getMeasuredHeight()I

    move-result v0

    .line 2319837
    iget-object v1, p0, LX/G6F;->b:Lcom/facebook/wem/watermark/WatermarkActivity;

    iget v1, v1, Lcom/facebook/wem/watermark/WatermarkActivity;->x:I

    if-le v0, v1, :cond_0

    .line 2319838
    iget-object v0, p0, LX/G6F;->a:Landroid/widget/ScrollView;

    invoke-virtual {v0}, Landroid/widget/ScrollView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2319839
    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 2319840
    const/4 v1, -0x2

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2319841
    iget-object v1, p0, LX/G6F;->a:Landroid/widget/ScrollView;

    invoke-virtual {v1, v0}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2319842
    :cond_0
    return-void
.end method
