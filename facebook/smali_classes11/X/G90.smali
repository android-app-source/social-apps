.class public final LX/G90;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:[B

.field private c:[LX/G92;

.field private final d:LX/G8o;

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "LX/G91;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final f:J


# direct methods
.method public constructor <init>(Ljava/lang/String;[B[LX/G92;LX/G8o;)V
    .locals 8

    .prologue
    .line 2321439
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v1 .. v7}, LX/G90;-><init>(Ljava/lang/String;[B[LX/G92;LX/G8o;J)V

    .line 2321440
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;[B[LX/G92;LX/G8o;J)V
    .locals 1

    .prologue
    .line 2321426
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2321427
    iput-object p1, p0, LX/G90;->a:Ljava/lang/String;

    .line 2321428
    iput-object p2, p0, LX/G90;->b:[B

    .line 2321429
    iput-object p3, p0, LX/G90;->c:[LX/G92;

    .line 2321430
    iput-object p4, p0, LX/G90;->d:LX/G8o;

    .line 2321431
    const/4 v0, 0x0

    iput-object v0, p0, LX/G90;->e:Ljava/util/Map;

    .line 2321432
    iput-wide p5, p0, LX/G90;->f:J

    .line 2321433
    return-void
.end method


# virtual methods
.method public final a(LX/G91;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2321435
    iget-object v0, p0, LX/G90;->e:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 2321436
    new-instance v0, Ljava/util/EnumMap;

    const-class v1, LX/G91;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, LX/G90;->e:Ljava/util/Map;

    .line 2321437
    :cond_0
    iget-object v0, p0, LX/G90;->e:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2321438
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2321434
    iget-object v0, p0, LX/G90;->a:Ljava/lang/String;

    return-object v0
.end method
