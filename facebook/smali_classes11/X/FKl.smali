.class public LX/FKl;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/2Hu;


# direct methods
.method public constructor <init>(LX/2Hu;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225503
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2225504
    iput-object p1, p0, LX/FKl;->a:LX/2Hu;

    .line 2225505
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/fbservice/service/OperationResult;
    .locals 8

    .prologue
    .line 2225506
    iget-object v0, p0, LX/FKl;->a:LX/2Hu;

    invoke-virtual {v0}, LX/2Hu;->a()LX/2gV;

    move-result-object v1

    .line 2225507
    :try_start_0
    invoke-virtual {v1}, LX/2gV;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2225508
    sget-object v0, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2225509
    invoke-virtual {v1}, LX/2gV;->f()V

    :goto_0
    return-object v0

    .line 2225510
    :cond_0
    :try_start_1
    new-instance v0, LX/1so;

    new-instance v2, LX/1sp;

    invoke-direct {v2}, LX/1sp;-><init>()V

    invoke-direct {v0, v2}, LX/1so;-><init>(LX/1sq;)V

    .line 2225511
    new-instance v2, LX/6mN;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {v2, v3, v4}, LX/6mN;-><init>(Ljava/lang/String;Ljava/lang/Boolean;)V

    invoke-virtual {v0, v2}, LX/1so;->a(LX/1u2;)[B

    move-result-object v0

    .line 2225512
    array-length v2, v0

    add-int/lit8 v2, v2, 0x1

    new-array v3, v2, [B

    .line 2225513
    const/4 v2, 0x0

    const/4 v4, 0x1

    array-length v5, v0

    invoke-static {v0, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2225514
    const-string v2, "/t_mf_as"

    const-wide/16 v4, 0x7530

    const-wide/16 v6, 0x0

    invoke-virtual/range {v1 .. v7}, LX/2gV;->a(Ljava/lang/String;[BJJ)Z

    move-result v0

    .line 2225515
    if-eqz v0, :cond_1

    .line 2225516
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2225517
    invoke-virtual {v1}, LX/2gV;->f()V

    goto :goto_0

    .line 2225518
    :cond_1
    :try_start_2
    sget-object v0, LX/1nY;->MQTT_SEND_FAILURE:LX/1nY;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 2225519
    invoke-virtual {v1}, LX/2gV;->f()V

    goto :goto_0

    .line 2225520
    :catch_0
    move-exception v0

    .line 2225521
    :try_start_3
    sget-object v2, LX/1nY;->MQTT_SEND_FAILURE:LX/1nY;

    invoke-static {v2, v0}, Lcom/facebook/fbservice/service/OperationResult;->forError(LX/1nY;Ljava/lang/Throwable;)Lcom/facebook/fbservice/service/OperationResult;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 2225522
    invoke-virtual {v1}, LX/2gV;->f()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, LX/2gV;->f()V

    throw v0
.end method
