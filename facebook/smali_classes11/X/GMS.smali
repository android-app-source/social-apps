.class public LX/GMS;
.super LX/GME;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GME",
        "<",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

.field private b:LX/GL7;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;LX/GL7;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2344616
    invoke-direct {p0}, LX/GME;-><init>()V

    .line 2344617
    iput-object p1, p0, LX/GMS;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    .line 2344618
    iput-object p2, p0, LX/GMS;->b:LX/GL7;

    .line 2344619
    return-void
.end method

.method public static a(LX/0QB;)LX/GMS;
    .locals 1

    .prologue
    .line 2344620
    invoke-static {p0}, LX/GMS;->b(LX/0QB;)LX/GMS;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/GMS;
    .locals 3

    .prologue
    .line 2344621
    new-instance v2, LX/GMS;

    invoke-static {p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->b(LX/0QB;)Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    invoke-static {p0}, LX/GL7;->b(LX/0QB;)LX/GL7;

    move-result-object v1

    check-cast v1, LX/GL7;

    invoke-direct {v2, v0, v1}, LX/GMS;-><init>(Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;LX/GL7;)V

    .line 2344622
    return-object v2
.end method

.method private e()Z
    .locals 2

    .prologue
    .line 2344568
    iget-object v0, p0, LX/GME;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2344569
    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v0

    .line 2344570
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->o:LX/GGG;

    move-object v0, v1

    .line 2344571
    sget-object v1, LX/GGG;->ADDRESS:LX/GGG;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2344623
    invoke-super {p0}, LX/GME;->a()V

    .line 2344624
    iget-object v0, p0, LX/GMS;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    .line 2344625
    iget-boolean v1, v0, LX/GHg;->a:Z

    move v0, v1

    .line 2344626
    if-eqz v0, :cond_0

    .line 2344627
    iget-object v0, p0, LX/GMS;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    invoke-virtual {v0}, LX/GHg;->a()V

    .line 2344628
    :cond_0
    iget-object v0, p0, LX/GMS;->b:LX/GL7;

    invoke-virtual {v0}, LX/GHg;->a()V

    .line 2344629
    return-void
.end method

.method public final a(LX/GCE;)V
    .locals 1

    .prologue
    .line 2344612
    invoke-super {p0, p1}, LX/GME;->a(LX/GCE;)V

    .line 2344613
    iget-object v0, p0, LX/GMS;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    invoke-virtual {v0, p1}, LX/GHg;->a(LX/GCE;)V

    .line 2344614
    iget-object v0, p0, LX/GMS;->b:LX/GL7;

    invoke-virtual {v0, p1}, LX/GHg;->a(LX/GCE;)V

    .line 2344615
    return-void
.end method

.method public final a(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 2344630
    iget-object v0, p0, LX/GME;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2344631
    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2344632
    sget-object v1, LX/8wL;->BOOSTED_COMPONENT_EDIT_TARGETING:LX/8wL;

    const v2, 0x7f080a98

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {p1, v1, v2, v3}, LX/8wJ;->a(Landroid/content/Context;LX/8wL;Ljava/lang/Integer;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2344633
    invoke-static {v0}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    move-result-object v2

    .line 2344634
    iget-object v3, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v3, v3

    .line 2344635
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2344636
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v4

    .line 2344637
    iput-object v4, v2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->e:Ljava/lang/String;

    .line 2344638
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-result-object v3

    .line 2344639
    iput-object v3, v2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->f:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    .line 2344640
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)V

    .line 2344641
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->w()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d(I)V

    .line 2344642
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->v()LX/0Px;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(LX/0Px;)V

    .line 2344643
    iget-object v3, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v3, v3

    .line 2344644
    iput-object v3, v2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2344645
    iget-object v3, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->g:Ljava/lang/String;

    move-object v3, v3

    .line 2344646
    iput-object v3, v2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->g:Ljava/lang/String;

    .line 2344647
    const-string v3, "data"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2344648
    move-object v0, v1

    .line 2344649
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2344650
    new-instance v2, LX/GFS;

    const/4 v3, 0x4

    const/4 v4, 0x1

    invoke-direct {v2, v0, v3, v4}, LX/GFS;-><init>(Landroid/content/Intent;IZ)V

    invoke-virtual {v1, v2}, LX/GCE;->a(LX/8wN;)V

    .line 2344651
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2344611
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;

    invoke-virtual {p0, p1, p2}, LX/GME;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 1

    .prologue
    .line 2344603
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2344604
    invoke-super {p0, p1}, LX/GME;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2344605
    iget-object v0, p0, LX/GMS;->b:LX/GL7;

    .line 2344606
    iput-object p1, v0, LX/GL7;->a:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2344607
    invoke-direct {p0}, LX/GMS;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2344608
    iget-object v0, p0, LX/GMS;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    .line 2344609
    iput-object p1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->m:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2344610
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 3
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x0

    .line 2344590
    invoke-super {p0, p1, p2}, LX/GME;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2344591
    iget-object v0, p0, LX/GMS;->b:LX/GL7;

    .line 2344592
    iget-object v1, p1, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;

    move-object v1, v1

    .line 2344593
    invoke-virtual {v0, v1, p2}, LX/GL7;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesSaveAudienceRowView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2344594
    invoke-direct {p0}, LX/GMS;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2344595
    iget-object v0, p0, LX/GMS;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    .line 2344596
    iget-object v1, p1, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

    move-object v1, v1

    .line 2344597
    invoke-virtual {v0, v1, p2}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2344598
    iget-object v0, p1, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

    move-object v0, v0

    .line 2344599
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->setVisibility(I)V

    .line 2344600
    iget-object v0, p1, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

    move-object v0, v0

    .line 2344601
    invoke-virtual {v0, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->setClickable(Z)V

    .line 2344602
    :cond_0
    return-void
.end method

.method public final b()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetingDescriptionModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2344586
    iget-object v0, p0, LX/GME;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2344587
    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2344588
    iget-object p0, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, p0

    .line 2344589
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->H()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$TargetingDescriptionModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$TargetingDescriptionModel;->a()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2344572
    iget-object v0, p0, LX/GME;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2344573
    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2344574
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v2

    .line 2344575
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    .line 2344576
    :goto_0
    return-object v0

    .line 2344577
    :cond_0
    iget-object v0, p0, LX/GME;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2344578
    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2344579
    iget-object v2, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v2

    .line 2344580
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->k()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 2344581
    goto :goto_0

    .line 2344582
    :cond_1
    iget-object v0, p0, LX/GME;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v0, v0

    .line 2344583
    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2344584
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v1

    .line 2344585
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;->n()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
