.class public LX/H19;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "LX/H0v;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2414825
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2414826
    return-void
.end method

.method public static a(LX/H0w;LX/H0w;)I
    .locals 2

    .prologue
    .line 2414827
    iget-object v0, p0, LX/H0w;->f:Ljava/lang/String;

    iget-object v1, p1, LX/H0w;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 2414828
    check-cast p1, LX/H0v;

    check-cast p2, LX/H0v;

    .line 2414829
    invoke-virtual {p1}, LX/H0v;->g()LX/H17;

    move-result-object v0

    invoke-virtual {p2}, LX/H0v;->g()LX/H17;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/H17;->compareTo(Ljava/lang/Enum;)I

    move-result v0

    .line 2414830
    if-eqz v0, :cond_1

    .line 2414831
    :cond_0
    :goto_0
    return v0

    .line 2414832
    :cond_1
    iget-object v0, p1, LX/H0v;->b:Ljava/lang/String;

    iget-object v1, p2, LX/H0v;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    .line 2414833
    if-nez v0, :cond_0

    .line 2414834
    sget-object v0, LX/H18;->a:[I

    invoke-virtual {p1}, LX/H0v;->g()LX/H17;

    move-result-object v1

    invoke-virtual {v1}, LX/H17;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2414835
    const/4 v0, 0x0

    goto :goto_0

    .line 2414836
    :pswitch_0
    const/4 v0, 0x0

    move v0, v0

    .line 2414837
    goto :goto_0

    .line 2414838
    :pswitch_1
    check-cast p1, LX/H1U;

    check-cast p2, LX/H1U;

    .line 2414839
    iget-object v0, p1, LX/H1U;->a:LX/H1b;

    iget-object v0, v0, LX/H0v;->b:Ljava/lang/String;

    iget-object v1, p2, LX/H1U;->a:LX/H1b;

    iget-object v1, v1, LX/H0v;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    move v0, v0

    .line 2414840
    goto :goto_0

    .line 2414841
    :pswitch_2
    check-cast p1, LX/H0z;

    check-cast p2, LX/H0z;

    .line 2414842
    invoke-static {p1, p2}, LX/H19;->a(LX/H0w;LX/H0w;)I

    move-result v0

    .line 2414843
    if-eqz v0, :cond_2

    .line 2414844
    :goto_1
    move v0, v0

    .line 2414845
    goto :goto_0

    .line 2414846
    :pswitch_3
    check-cast p1, LX/H0w;

    check-cast p2, LX/H0w;

    invoke-static {p1, p2}, LX/H19;->a(LX/H0w;LX/H0w;)I

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v0, p1, LX/H0z;->a:Ljava/lang/String;

    iget-object v1, p2, LX/H0z;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2414847
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2414848
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method
