.class public LX/GiF;
.super LX/E93;
.source ""


# instance fields
.field private g:Landroid/content/Context;

.field private h:LX/GiI;

.field private i:LX/1PT;

.field private j:LX/1SX;

.field private final k:Ljava/lang/String;

.field private final l:LX/GiO;

.field private final m:LX/3Tp;

.field private final n:LX/E1j;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1PT;LX/1SX;LX/0o8;Ljava/lang/String;LX/GiO;LX/0SG;LX/E2N;LX/3Tp;LX/E1j;LX/0bH;LX/1Db;LX/AjP;LX/1My;LX/1Db;LX/E1l;LX/3Tp;LX/E1j;Lcom/facebook/reaction/ReactionUtil;LX/GiI;LX/CvY;LX/Cfw;LX/3mH;LX/967;)V
    .locals 22
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/1PT;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/1SX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/0o8;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/GiO;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2385448
    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move-object/from16 v12, p12

    move-object/from16 v13, p13

    move-object/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v16, p16

    move-object/from16 v17, p19

    move-object/from16 v18, p21

    move-object/from16 v19, p22

    move-object/from16 v20, p23

    move-object/from16 v21, p24

    invoke-direct/range {v2 .. v21}, LX/E93;-><init>(Landroid/content/Context;LX/1PT;LX/1SX;LX/0o8;LX/0SG;LX/E2N;LX/3Tp;LX/E1j;LX/0bH;LX/1Db;LX/AjP;LX/1My;LX/1Db;LX/E1l;Lcom/facebook/reaction/ReactionUtil;LX/CvY;LX/Cfw;LX/3mH;LX/967;)V

    .line 2385449
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, LX/GiF;->g:Landroid/content/Context;

    .line 2385450
    move-object/from16 v0, p20

    move-object/from16 v1, p0

    iput-object v0, v1, LX/GiF;->h:LX/GiI;

    .line 2385451
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, LX/GiF;->i:LX/1PT;

    .line 2385452
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, LX/GiF;->j:LX/1SX;

    .line 2385453
    move-object/from16 v0, p5

    move-object/from16 v1, p0

    iput-object v0, v1, LX/GiF;->k:Ljava/lang/String;

    .line 2385454
    move-object/from16 v0, p6

    move-object/from16 v1, p0

    iput-object v0, v1, LX/GiF;->l:LX/GiO;

    .line 2385455
    move-object/from16 v0, p17

    move-object/from16 v1, p0

    iput-object v0, v1, LX/GiF;->m:LX/3Tp;

    .line 2385456
    move-object/from16 v0, p18

    move-object/from16 v1, p0

    iput-object v0, v1, LX/GiF;->n:LX/E1j;

    .line 2385457
    return-void
.end method


# virtual methods
.method public final q()LX/1Rq;
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 2385458
    iget-object v0, p0, LX/GiF;->h:LX/GiI;

    iget-object v1, p0, LX/GiF;->g:Landroid/content/Context;

    iget-object v2, p0, LX/GiF;->i:LX/1PT;

    iget-object v3, p0, LX/GiF;->j:LX/1SX;

    new-instance v4, Lcom/facebook/gametime/ui/reaction/GametimeAdapter$1;

    invoke-direct {v4, p0}, Lcom/facebook/gametime/ui/reaction/GametimeAdapter$1;-><init>(LX/GiF;)V

    iget-object v5, p0, LX/E8m;->d:Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;

    iget-object v6, p0, LX/GiF;->m:LX/3Tp;

    iget-object v7, p0, LX/GiF;->g:Landroid/content/Context;

    iget-object v8, p0, LX/E8m;->a:LX/0o8;

    invoke-virtual {v6, v7, v8}, LX/3Tp;->a(Landroid/content/Context;LX/0o8;)LX/3Tv;

    move-result-object v6

    iget-object v7, p0, LX/E8m;->a:LX/0o8;

    invoke-interface {v7}, LX/0o8;->m()LX/2ja;

    move-result-object v7

    iget-object v8, p0, LX/E8m;->f:LX/2jY;

    invoke-virtual {p0}, LX/E93;->r()LX/1PY;

    move-result-object v9

    iget-object v10, p0, LX/GiF;->k:Ljava/lang/String;

    iget-object v11, p0, LX/GiF;->l:LX/GiO;

    invoke-virtual/range {v0 .. v11}, LX/GiI;->a(Landroid/content/Context;LX/1PT;LX/1SX;Ljava/lang/Runnable;Lcom/facebook/reaction/common/logging/ReactionAnalyticsParams;LX/3Tv;LX/2ja;LX/2jY;LX/1PY;Ljava/lang/String;LX/GiO;)LX/GiH;

    move-result-object v0

    .line 2385459
    iget-object v1, p0, LX/GiF;->n:LX/E1j;

    .line 2385460
    iget-object v2, p0, LX/E93;->x:LX/E1l;

    move-object v2, v2

    .line 2385461
    invoke-virtual {v1, v0, v12, v2, v12}, LX/E1j;->a(LX/3U6;LX/1DZ;LX/E1l;LX/0g8;)LX/1Rq;

    move-result-object v0

    return-object v0
.end method
