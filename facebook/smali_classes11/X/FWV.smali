.class public final LX/FWV;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/composer/protocol/PostReviewParams;

.field public final synthetic b:LX/FWX;


# direct methods
.method public constructor <init>(LX/FWX;Lcom/facebook/composer/protocol/PostReviewParams;)V
    .locals 0

    .prologue
    .line 2252226
    iput-object p1, p0, LX/FWV;->b:LX/FWX;

    iput-object p2, p0, LX/FWV;->a:Lcom/facebook/composer/protocol/PostReviewParams;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    .line 2252227
    iget-object v0, p0, LX/FWV;->b:LX/FWX;

    iget-object v0, v0, LX/FWX;->e:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/FWV;->b:LX/FWX;

    iget-object v2, v2, LX/FWX;->b:Landroid/content/res/Resources;

    const v3, 0x7f0814e6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2252228
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2252195
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2252196
    iget-object v0, p0, LX/FWV;->b:LX/FWX;

    iget-object v0, v0, LX/FWX;->e:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/FWV;->b:LX/FWX;

    iget-object v2, v2, LX/FWX;->b:Landroid/content/res/Resources;

    const v3, 0x7f0814e5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2252197
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5tj;

    .line 2252198
    const/4 v1, 0x0

    .line 2252199
    invoke-interface {v0}, LX/5tj;->d()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 2252200
    new-instance v1, LX/5tm;

    invoke-direct {v1}, LX/5tm;-><init>()V

    invoke-interface {v0}, LX/5tj;->d()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2252201
    iput-object v2, v1, LX/5tm;->a:Ljava/lang/String;

    .line 2252202
    move-object v1, v1

    .line 2252203
    invoke-virtual {v1}, LX/5tm;->a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    move-result-object v1

    move-object v2, v1

    .line 2252204
    :goto_0
    new-instance v3, LX/5tz;

    invoke-direct {v3}, LX/5tz;-><init>()V

    invoke-interface {v0}, LX/5tj;->c()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;->b()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;->a()LX/0Px;

    move-result-object v1

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel$EdgesModel;

    invoke-virtual {v1}, Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel$EdgesModel;->b()Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v1

    .line 2252205
    iput-object v1, v3, LX/5tz;->b:Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    .line 2252206
    move-object v1, v3

    .line 2252207
    invoke-virtual {v1}, LX/5tz;->a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel$EdgesModel;

    move-result-object v1

    .line 2252208
    new-instance v3, LX/5ty;

    invoke-direct {v3}, LX/5ty;-><init>()V

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    .line 2252209
    iput-object v1, v3, LX/5ty;->a:LX/0Px;

    .line 2252210
    move-object v1, v3

    .line 2252211
    invoke-virtual {v1}, LX/5ty;->a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;

    move-result-object v1

    .line 2252212
    iget-object v3, p0, LX/FWV;->b:LX/FWX;

    iget-object v3, v3, LX/FWX;->g:LX/FW1;

    new-instance v4, LX/FW3;

    new-instance v5, LX/5tl;

    invoke-direct {v5}, LX/5tl;-><init>()V

    .line 2252213
    iput-object v2, v5, LX/5tl;->d:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel$ValueModel;

    .line 2252214
    move-object v2, v5

    .line 2252215
    invoke-interface {v0}, LX/5tj;->b()I

    move-result v0

    .line 2252216
    iput v0, v2, LX/5tl;->b:I

    .line 2252217
    move-object v0, v2

    .line 2252218
    new-instance v2, LX/5tx;

    invoke-direct {v2}, LX/5tx;-><init>()V

    .line 2252219
    iput-object v1, v2, LX/5tx;->b:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel$PrivacyOptionsModel;

    .line 2252220
    move-object v1, v2

    .line 2252221
    invoke-virtual {v1}, LX/5tx;->a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    move-result-object v1

    .line 2252222
    iput-object v1, v0, LX/5tl;->c:Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$SelectedPrivacyOptionFieldsModel;

    .line 2252223
    move-object v0, v0

    .line 2252224
    invoke-virtual {v0}, LX/5tl;->a()Lcom/facebook/reviews/protocol/graphql/ReviewFragmentsModels$ReviewBasicFieldsModel;

    move-result-object v0

    iget-object v1, p0, LX/FWV;->a:Lcom/facebook/composer/protocol/PostReviewParams;

    iget-wide v6, v1, Lcom/facebook/composer/protocol/PostReviewParams;->b:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v0, v1}, LX/FW3;-><init>(LX/55r;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, LX/0b4;->a(LX/0b7;)V

    .line 2252225
    return-void

    :cond_0
    move-object v2, v1

    goto :goto_0
.end method
