.class public LX/FML;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

.field private static volatile k:LX/FML;


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FM9;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMI;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FNX;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6jK;",
            ">;"
        }
    .end annotation
.end field

.field public f:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FM8;",
            ">;"
        }
    .end annotation
.end field

.field public g:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field public h:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3Rc;",
            ">;"
        }
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/ExecutorService;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2229586
    const-wide/16 v0, -0x66

    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b(J)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    sput-object v0, LX/FML;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2229604
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2229605
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2229606
    iput-object v0, p0, LX/FML;->b:LX/0Ot;

    .line 2229607
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2229608
    iput-object v0, p0, LX/FML;->c:LX/0Ot;

    .line 2229609
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2229610
    iput-object v0, p0, LX/FML;->d:LX/0Ot;

    .line 2229611
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2229612
    iput-object v0, p0, LX/FML;->e:LX/0Ot;

    .line 2229613
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2229614
    iput-object v0, p0, LX/FML;->f:LX/0Ot;

    .line 2229615
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2229616
    iput-object v0, p0, LX/FML;->g:LX/0Ot;

    .line 2229617
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2229618
    iput-object v0, p0, LX/FML;->h:LX/0Ot;

    .line 2229619
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2229620
    iput-object v0, p0, LX/FML;->i:LX/0Ot;

    .line 2229621
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2229622
    iput-object v0, p0, LX/FML;->j:LX/0Ot;

    .line 2229623
    return-void
.end method

.method public static a(LX/0QB;)LX/FML;
    .locals 12

    .prologue
    .line 2229589
    sget-object v0, LX/FML;->k:LX/FML;

    if-nez v0, :cond_1

    .line 2229590
    const-class v1, LX/FML;

    monitor-enter v1

    .line 2229591
    :try_start_0
    sget-object v0, LX/FML;->k:LX/FML;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2229592
    if-eqz v2, :cond_0

    .line 2229593
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2229594
    new-instance v3, LX/FML;

    invoke-direct {v3}, LX/FML;-><init>()V

    .line 2229595
    const/16 v4, 0x297b

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x2982

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x29b5

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x2986

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x297a

    invoke-static {v0, v8}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const-class v9, Landroid/content/Context;

    invoke-interface {v0, v9}, LX/0QC;->getLazy(Ljava/lang/Class;)LX/0Ot;

    move-result-object v9

    const/16 v10, 0xdaa

    invoke-static {v0, v10}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x140d

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 p0, 0x298f

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2229596
    iput-object v4, v3, LX/FML;->b:LX/0Ot;

    iput-object v5, v3, LX/FML;->c:LX/0Ot;

    iput-object v6, v3, LX/FML;->d:LX/0Ot;

    iput-object v7, v3, LX/FML;->e:LX/0Ot;

    iput-object v8, v3, LX/FML;->f:LX/0Ot;

    iput-object v9, v3, LX/FML;->g:LX/0Ot;

    iput-object v10, v3, LX/FML;->h:LX/0Ot;

    iput-object v11, v3, LX/FML;->i:LX/0Ot;

    iput-object p0, v3, LX/FML;->j:LX/0Ot;

    .line 2229597
    move-object v0, v3

    .line 2229598
    sput-object v0, LX/FML;->k:LX/FML;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2229599
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2229600
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2229601
    :cond_1
    sget-object v0, LX/FML;->k:LX/FML;

    return-object v0

    .line 2229602
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2229603
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2229588
    iget-object v0, p0, LX/FML;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/6jK;

    invoke-virtual {v0}, LX/6jK;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2229587
    iget-object v0, p0, LX/FML;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FMI;

    iget-object v1, p0, LX/FML;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/6jK;

    invoke-virtual {v1}, LX/6jK;->a()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/FMI;->a(Ljava/util/Set;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
