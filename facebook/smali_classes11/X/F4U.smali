.class public final enum LX/F4U;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/F4U;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/F4U;

.field public static final enum TASK_FETCH_AVAILABLE_COMMUNITIES:LX/F4U;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2196967
    new-instance v0, LX/F4U;

    const-string v1, "TASK_FETCH_AVAILABLE_COMMUNITIES"

    invoke-direct {v0, v1, v2}, LX/F4U;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F4U;->TASK_FETCH_AVAILABLE_COMMUNITIES:LX/F4U;

    .line 2196968
    const/4 v0, 0x1

    new-array v0, v0, [LX/F4U;

    sget-object v1, LX/F4U;->TASK_FETCH_AVAILABLE_COMMUNITIES:LX/F4U;

    aput-object v1, v0, v2

    sput-object v0, LX/F4U;->$VALUES:[LX/F4U;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2196971
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/F4U;
    .locals 1

    .prologue
    .line 2196970
    const-class v0, LX/F4U;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/F4U;

    return-object v0
.end method

.method public static values()[LX/F4U;
    .locals 1

    .prologue
    .line 2196969
    sget-object v0, LX/F4U;->$VALUES:[LX/F4U;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/F4U;

    return-object v0
.end method
