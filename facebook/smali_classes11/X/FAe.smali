.class public LX/FAe;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements LX/0hE;


# instance fields
.field public a:Landroid/view/View;

.field public b:Landroid/view/ViewGroup;

.field public c:Z

.field public d:LX/FAa;

.field public e:LX/394;

.field public f:Z

.field public final g:LX/Gn8;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2207915
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, LX/FAe;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2207916
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2207907
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, LX/FAe;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2207908
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 2207917
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2207918
    iput-object v2, p0, LX/FAe;->a:Landroid/view/View;

    .line 2207919
    iput-object v2, p0, LX/FAe;->b:Landroid/view/ViewGroup;

    .line 2207920
    iput-boolean v0, p0, LX/FAe;->c:Z

    .line 2207921
    iput-boolean v0, p0, LX/FAe;->f:Z

    .line 2207922
    new-instance v0, LX/FAa;

    invoke-direct {v0}, LX/FAa;-><init>()V

    iput-object v0, p0, LX/FAe;->d:LX/FAa;

    .line 2207923
    new-instance v0, LX/Gn8;

    invoke-direct {v0, p1}, LX/Gn8;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/FAe;->g:LX/Gn8;

    .line 2207924
    iget-object v0, p0, LX/FAe;->d:LX/FAa;

    invoke-virtual {p0}, LX/FAe;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Chc;->b(Landroid/content/Context;)V

    .line 2207925
    iget-object v0, p0, LX/FAe;->d:LX/FAa;

    invoke-virtual {p0}, LX/FAe;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Chc;->a(Landroid/content/Context;)V

    .line 2207926
    iget-object v0, p0, LX/FAe;->d:LX/FAa;

    invoke-virtual {p0}, LX/FAe;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, LX/Chc;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, LX/FAe;->a:Landroid/view/View;

    .line 2207927
    iget-object v0, p0, LX/FAe;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 2207928
    iget-object v0, p0, LX/FAe;->a:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusable(Z)V

    .line 2207929
    return-void
.end method


# virtual methods
.method public final synthetic a(LX/394;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2207930
    iput-object p1, p0, LX/FAe;->e:LX/394;

    .line 2207931
    move-object v0, p0

    .line 2207932
    return-object v0
.end method

.method public final a(LX/395;)V
    .locals 0

    .prologue
    .line 2207933
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2207934
    iget-boolean v0, p0, LX/FAe;->c:Z

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2207935
    invoke-virtual {p0}, LX/FAe;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2207936
    const/4 v0, 0x0

    .line 2207937
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/FAe;->d:LX/FAa;

    invoke-virtual {v0}, LX/Chc;->c()Z

    move-result v0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2207950
    iget-object v0, p0, LX/FAe;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 2207951
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/FAe;->c:Z

    .line 2207952
    iget-object v0, p0, LX/FAe;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/FAe;->b:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 2207953
    iget-object v0, p0, LX/FAe;->b:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/FAe;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2207954
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2207938
    invoke-virtual {p0}, LX/FAe;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/FAe;->b:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 2207939
    iget-object v0, p0, LX/FAe;->g:LX/Gn8;

    invoke-virtual {v0}, LX/Gn8;->c()V

    .line 2207940
    iget-object v0, p0, LX/FAe;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 2207941
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 2207948
    iget-object v0, p0, LX/FAe;->d:LX/FAa;

    invoke-virtual {v0}, LX/Chc;->mB_()V

    .line 2207949
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 2207946
    iget-object v0, p0, LX/FAe;->d:LX/FAa;

    invoke-virtual {v0}, LX/Chc;->e()V

    .line 2207947
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2207944
    iget-object v0, p0, LX/FAe;->d:LX/FAa;

    invoke-virtual {v0}, LX/Chc;->f()V

    .line 2207945
    return-void
.end method

.method public getFullScreenListener()LX/394;
    .locals 1

    .prologue
    .line 2207943
    iget-object v0, p0, LX/FAe;->e:LX/394;

    return-object v0
.end method

.method public final h()V
    .locals 0

    .prologue
    .line 2207942
    return-void
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 2207909
    invoke-virtual {p0}, LX/FAe;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 2207910
    if-eqz v0, :cond_1

    .line 2207911
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    .line 2207912
    mul-int/lit8 v1, v0, 0x64

    div-int/lit8 v1, v1, 0xf

    move v0, v1

    .line 2207913
    :goto_0
    move v0, v0

    .line 2207914
    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final j()V
    .locals 3

    .prologue
    .line 2207859
    iget-boolean v0, p0, LX/FAe;->f:Z

    move v0, v0

    .line 2207860
    if-nez v0, :cond_1

    .line 2207861
    iget-object v0, p0, LX/FAe;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, LX/FAe;->b:Landroid/view/ViewGroup;

    if-ne v0, v1, :cond_1

    .line 2207862
    iget-object v0, p0, LX/FAe;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearFocus()V

    .line 2207863
    iget-object v0, p0, LX/FAe;->g:LX/Gn8;

    .line 2207864
    iget-object v1, v0, LX/Gn8;->b:Landroid/view/Window;

    if-eqz v1, :cond_0

    .line 2207865
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_2

    .line 2207866
    iget-object v1, v0, LX/Gn8;->b:Landroid/view/Window;

    const/16 v2, 0x400

    invoke-virtual {v1, v2}, Landroid/view/Window;->clearFlags(I)V

    .line 2207867
    :cond_0
    :goto_0
    iget-object v0, p0, LX/FAe;->g:LX/Gn8;

    .line 2207868
    iget-object v1, v0, LX/Gn8;->a:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2207869
    iget-object v0, p0, LX/FAe;->b:Landroid/view/ViewGroup;

    iget-object v1, p0, LX/FAe;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2207870
    iget-object v0, p0, LX/FAe;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 2207871
    :cond_1
    return-void

    .line 2207872
    :cond_2
    iget-object v1, v0, LX/Gn8;->b:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_0
.end method

.method public final k()V
    .locals 3

    .prologue
    .line 2207875
    iget-object v0, p0, LX/FAe;->a:Landroid/view/View;

    new-instance v1, LX/FAb;

    invoke-direct {v1, p0}, LX/FAb;-><init>(LX/FAe;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 2207876
    new-instance v0, LX/FAc;

    invoke-direct {v0, p0}, LX/FAc;-><init>(LX/FAe;)V

    .line 2207877
    iget-object v1, p0, LX/FAe;->d:LX/FAa;

    invoke-virtual {v1, v0}, LX/Chc;->a(LX/ChK;)V

    .line 2207878
    new-instance v0, LX/FAd;

    invoke-direct {v0, p0}, LX/FAd;-><init>(LX/FAe;)V

    .line 2207879
    iget-object v1, p0, LX/FAe;->d:LX/FAa;

    .line 2207880
    iput-object v0, v1, LX/Chc;->I:LX/FAd;

    .line 2207881
    iget-object v0, p0, LX/FAe;->d:LX/FAa;

    iget-object v1, p0, LX/FAe;->a:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/Chc;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2207882
    iget-object v0, p0, LX/FAe;->d:LX/FAa;

    invoke-virtual {v0}, LX/FAa;->I()V

    .line 2207883
    return-void
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 2207884
    iget-object v0, p0, LX/FAe;->d:LX/FAa;

    .line 2207885
    iget-object p0, v0, LX/Chc;->G:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    move-object v0, p0

    .line 2207886
    invoke-virtual {v0}, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->b()V

    .line 2207887
    return-void
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x405d6c6a

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2207888
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onAttachedToWindow()V

    .line 2207889
    iget-object v1, p0, LX/FAe;->d:LX/FAa;

    invoke-virtual {v1}, LX/Chc;->j()Landroid/app/Dialog;

    .line 2207890
    invoke-virtual {p0}, LX/FAe;->e()V

    .line 2207891
    invoke-virtual {p0}, LX/FAe;->f()V

    .line 2207892
    const/16 v1, 0x2d

    const v2, -0x72752df6

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, 0x5b7d67c8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2207893
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->onDetachedFromWindow()V

    .line 2207894
    invoke-virtual {p0}, LX/FAe;->g()V

    .line 2207895
    invoke-virtual {p0}, LX/FAe;->h()V

    .line 2207896
    const/16 v1, 0x2d

    const v2, 0x436f9b5c

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setAllowLooping(Z)V
    .locals 0

    .prologue
    .line 2207897
    return-void
.end method

.method public setArguments(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2207898
    iget-object v0, p0, LX/FAe;->d:LX/FAa;

    invoke-virtual {v0, p1}, LX/Chc;->c(Landroid/os/Bundle;)V

    .line 2207899
    return-void
.end method

.method public setKeepAttached(Z)V
    .locals 0

    .prologue
    .line 2207873
    iput-boolean p1, p0, LX/FAe;->f:Z

    .line 2207874
    return-void
.end method

.method public setLogEnteringStartEvent(Z)V
    .locals 0

    .prologue
    .line 2207900
    return-void
.end method

.method public setLogExitingPauseEvent(Z)V
    .locals 0

    .prologue
    .line 2207901
    return-void
.end method

.method public setNextStoryFinder(LX/0QK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2207902
    return-void
.end method

.method public setPreviousStoryFinder(LX/0QK;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2207903
    return-void
.end method

.method public setVisible(I)V
    .locals 1

    .prologue
    .line 2207904
    iget-object v0, p0, LX/FAe;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 2207905
    return-void
.end method

.method public final t_(I)V
    .locals 0

    .prologue
    .line 2207906
    return-void
.end method
