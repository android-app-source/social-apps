.class public final LX/Fut;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Fuu;


# direct methods
.method public constructor <init>(LX/Fuu;)V
    .locals 0

    .prologue
    .line 2300602
    iput-object p1, p0, LX/Fut;->a:LX/Fuu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x7ee10e72

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2300603
    iget-object v0, p0, LX/Fut;->a:LX/Fuu;

    iget-object v0, v0, LX/Fuu;->f:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G3G;

    const-string v2, "profile_nux"

    const/4 p1, 0x0

    .line 2300604
    const-string v3, "profile_nux_entry_point_close_button_tapped"

    invoke-static {v0, v2, p1, p1, v3}, LX/G3G;->a(LX/G3G;Ljava/lang/String;LX/0Px;Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;Ljava/lang/String;)V

    .line 2300605
    iget-object v0, p0, LX/Fut;->a:LX/Fuu;

    iget-object v0, v0, LX/Fuu;->h:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 2300606
    iget-object v0, p0, LX/Fut;->a:LX/Fuu;

    iget-object v0, v0, LX/Fuu;->h:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2300607
    :cond_0
    :goto_0
    iget-object v0, p0, LX/Fut;->a:LX/Fuu;

    iget-object v0, v0, LX/Fuu;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G3B;

    iget-object v2, p0, LX/Fut;->a:LX/Fuu;

    iget-object v2, v2, LX/Fuu;->g:Ljava/lang/String;

    .line 2300608
    new-instance v3, LX/4In;

    invoke-direct {v3}, LX/4In;-><init>()V

    .line 2300609
    const-string p0, "actor_id"

    invoke-virtual {v3, p0, v2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2300610
    new-instance p0, LX/5yL;

    invoke-direct {p0}, LX/5yL;-><init>()V

    move-object p0, p0

    .line 2300611
    const-string p1, "input"

    invoke-virtual {p0, p1, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2300612
    invoke-static {p0}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v3

    .line 2300613
    iget-object p0, v0, LX/G3B;->a:LX/0tX;

    invoke-virtual {p0, v3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2300614
    const v0, -0x17eaa520

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2300615
    :cond_1
    iget-object v0, p0, LX/Fut;->a:LX/Fuu;

    iget-object v0, v0, LX/Fuu;->i:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 2300616
    iget-object v0, p0, LX/Fut;->a:LX/Fuu;

    iget-object v0, v0, LX/Fuu;->i:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method
