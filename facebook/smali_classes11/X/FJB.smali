.class public LX/FJB;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;

.field public final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/FJ7;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0wY;

.field public final d:LX/FJA;

.field public e:Landroid/graphics/PointF;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public g:Z

.field public h:Z

.field public i:D

.field public j:D

.field public k:I

.field public l:I

.field public m:LX/FJ6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:LX/FJ6;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;LX/0Px;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/momentsinvite/kenburns/ImageLoader;",
            "LX/0Px",
            "<",
            "LX/FJ7;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2223154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2223155
    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2223156
    iput-object p1, p0, LX/FJB;->a:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;

    .line 2223157
    iput-object p2, p0, LX/FJB;->b:LX/0Px;

    .line 2223158
    new-instance v0, LX/0wX;

    invoke-direct {v0}, LX/0wX;-><init>()V

    iput-object v0, p0, LX/FJB;->c:LX/0wY;

    .line 2223159
    new-instance v0, LX/FJA;

    invoke-direct {v0, p0}, LX/FJA;-><init>(LX/FJB;)V

    iput-object v0, p0, LX/FJB;->d:LX/FJA;

    .line 2223160
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2223161
    iput-object v0, p0, LX/FJB;->f:LX/0Px;

    .line 2223162
    return-void

    :cond_0
    move v0, v1

    .line 2223163
    goto :goto_0
.end method

.method public static b(LX/FJB;I)LX/FJ7;
    .locals 2

    .prologue
    .line 2223164
    iget-object v0, p0, LX/FJB;->b:LX/0Px;

    iget-object v1, p0, LX/FJB;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    rem-int v1, p1, v1

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FJ7;

    return-object v0
.end method

.method public static c(LX/FJB;)V
    .locals 7

    .prologue
    .line 2223165
    const/4 v6, 0x0

    const/4 v2, 0x0

    const-wide/16 v4, 0x0

    .line 2223166
    iget-object v0, p0, LX/FJB;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, LX/FJB;->b:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FJ7;

    .line 2223167
    invoke-interface {v0}, LX/FJ7;->a()V

    .line 2223168
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2223169
    :cond_0
    iput-wide v4, p0, LX/FJB;->i:D

    .line 2223170
    iput-wide v4, p0, LX/FJB;->j:D

    .line 2223171
    const/4 v0, -0x1

    iput v0, p0, LX/FJB;->k:I

    .line 2223172
    iput v2, p0, LX/FJB;->l:I

    .line 2223173
    iput-object v6, p0, LX/FJB;->m:LX/FJ6;

    .line 2223174
    iput-object v6, p0, LX/FJB;->n:LX/FJ6;

    .line 2223175
    const/4 v3, 0x1

    .line 2223176
    iget-object v0, p0, LX/FJB;->e:Landroid/graphics/PointF;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/FJB;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2223177
    :cond_1
    :goto_1
    return-void

    .line 2223178
    :cond_2
    iget-object v0, p0, LX/FJB;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v3, :cond_3

    .line 2223179
    invoke-static {p0}, LX/FJB;->m(LX/FJB;)LX/FJ7;

    move-result-object v1

    iget-object v0, p0, LX/FJB;->f:LX/0Px;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-interface {v1, v0}, LX/FJ7;->a(Landroid/net/Uri;)V

    .line 2223180
    invoke-static {p0}, LX/FJB;->m(LX/FJB;)LX/FJ7;

    move-result-object v0

    iget-object v1, p0, LX/FJB;->e:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    float-to-int v1, v1

    iget-object v2, p0, LX/FJB;->e:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    float-to-int v2, v2

    invoke-interface {v0, v1, v2}, LX/FJ7;->a(II)V

    .line 2223181
    :goto_2
    invoke-static {p0}, LX/FJB;->f(LX/FJB;)V

    goto :goto_1

    .line 2223182
    :cond_3
    new-instance v0, LX/FJ6;

    iget-object v1, p0, LX/FJB;->a:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;

    invoke-static {p0}, LX/FJB;->m(LX/FJB;)LX/FJ7;

    move-result-object v2

    const-wide/16 v4, 0x0

    invoke-direct {v0, v1, v2, v4, v5}, LX/FJ6;-><init>(Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;LX/FJ7;J)V

    iput-object v0, p0, LX/FJB;->m:LX/FJ6;

    .line 2223183
    iget-object v0, p0, LX/FJB;->m:LX/FJ6;

    iget-object v1, p0, LX/FJB;->e:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, LX/FJ6;->a(Landroid/graphics/PointF;)V

    .line 2223184
    iget-object v0, p0, LX/FJB;->m:LX/FJ6;

    .line 2223185
    iput-boolean v3, v0, LX/FJ6;->h:Z

    .line 2223186
    invoke-static {v0}, LX/FJ6;->d(LX/FJ6;)V

    .line 2223187
    const/4 v0, 0x0

    iput-object v0, p0, LX/FJB;->n:LX/FJ6;

    goto :goto_2
.end method

.method public static f(LX/FJB;)V
    .locals 2

    .prologue
    .line 2223188
    invoke-direct {p0}, LX/FJB;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/FJB;->h:Z

    if-nez v0, :cond_1

    .line 2223189
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/FJB;->h:Z

    .line 2223190
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    iput-wide v0, p0, LX/FJB;->j:D

    .line 2223191
    iget-object v0, p0, LX/FJB;->c:LX/0wY;

    iget-object v1, p0, LX/FJB;->d:LX/FJA;

    invoke-interface {v0, v1}, LX/0wY;->a(LX/0wa;)V

    .line 2223192
    :cond_0
    :goto_0
    return-void

    .line 2223193
    :cond_1
    invoke-direct {p0}, LX/FJB;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, LX/FJB;->h:Z

    if-eqz v0, :cond_0

    .line 2223194
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/FJB;->h:Z

    .line 2223195
    iget-object v0, p0, LX/FJB;->c:LX/0wY;

    iget-object v1, p0, LX/FJB;->d:LX/FJA;

    invoke-interface {v0, v1}, LX/0wY;->b(LX/0wa;)V

    goto :goto_0
.end method

.method private g()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 2223196
    iget-boolean v1, p0, LX/FJB;->g:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/FJB;->e:Landroid/graphics/PointF;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/FJB;->f:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h(LX/FJB;)I
    .locals 2

    .prologue
    .line 2223197
    iget-object v0, p0, LX/FJB;->f:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, LX/FJB;->k:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, LX/FJB;->f:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    rem-int/2addr v0, v1

    goto :goto_0
.end method

.method public static k(LX/FJB;)I
    .locals 2

    .prologue
    .line 2223198
    iget v0, p0, LX/FJB;->l:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, LX/FJB;->b:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    rem-int/2addr v0, v1

    return v0
.end method

.method public static m(LX/FJB;)LX/FJ7;
    .locals 1

    .prologue
    .line 2223199
    iget v0, p0, LX/FJB;->l:I

    invoke-static {p0, v0}, LX/FJB;->b(LX/FJB;I)LX/FJ7;

    move-result-object v0

    return-object v0
.end method

.method public static o(LX/FJB;)V
    .locals 6

    .prologue
    .line 2223200
    iget-object v0, p0, LX/FJB;->n:LX/FJ6;

    if-eqz v0, :cond_0

    .line 2223201
    :goto_0
    return-void

    .line 2223202
    :cond_0
    new-instance v0, LX/FJ6;

    iget-object v1, p0, LX/FJB;->a:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;

    .line 2223203
    invoke-static {p0}, LX/FJB;->k(LX/FJB;)I

    move-result v2

    invoke-static {p0, v2}, LX/FJB;->b(LX/FJB;I)LX/FJ7;

    move-result-object v2

    move-object v2, v2

    .line 2223204
    iget-wide v4, p0, LX/FJB;->i:D

    invoke-static {v4, v5}, Ljava/lang/Math;->round(D)J

    move-result-wide v4

    invoke-direct {v0, v1, v2, v4, v5}, LX/FJ6;-><init>(Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;LX/FJ7;J)V

    iput-object v0, p0, LX/FJB;->n:LX/FJ6;

    .line 2223205
    iget-object v0, p0, LX/FJB;->n:LX/FJ6;

    .line 2223206
    invoke-static {p0}, LX/FJB;->h(LX/FJB;)I

    move-result v1

    .line 2223207
    if-ltz v1, :cond_1

    iget-object v2, p0, LX/FJB;->f:LX/0Px;

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-lt v1, v2, :cond_3

    :cond_1
    const/4 v2, 0x0

    :goto_1
    move-object v1, v2

    .line 2223208
    move-object v1, v1

    .line 2223209
    iput-object v1, v0, LX/FJ6;->d:Landroid/net/Uri;

    .line 2223210
    const/4 v2, 0x0

    iput-object v2, v0, LX/FJ6;->e:Landroid/graphics/PointF;

    .line 2223211
    iget-object v2, v0, LX/FJ6;->d:Landroid/net/Uri;

    if-eqz v2, :cond_2

    .line 2223212
    iget-object v2, v0, LX/FJ6;->a:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;

    iget-object v3, v0, LX/FJ6;->d:Landroid/net/Uri;

    .line 2223213
    invoke-static {v3}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v4

    .line 2223214
    if-nez v4, :cond_4

    .line 2223215
    :cond_2
    :goto_2
    iget-object v0, p0, LX/FJB;->n:LX/FJ6;

    iget-object v1, p0, LX/FJB;->e:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, LX/FJ6;->a(Landroid/graphics/PointF;)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, LX/FJB;->f:LX/0Px;

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    goto :goto_1

    .line 2223216
    :cond_4
    iget-object v5, v2, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;->b:LX/1HI;

    sget-object v1, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v5, v4, v1}, LX/1HI;->b(LX/1bf;Ljava/lang/Object;)LX/1ca;

    move-result-object v4

    .line 2223217
    new-instance v5, LX/FJE;

    invoke-direct {v5, v2, v0, v3}, LX/FJE;-><init>(Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;LX/FJ6;Landroid/net/Uri;)V

    .line 2223218
    iget-object v1, v2, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteImageLoader;->c:Ljava/util/concurrent/Executor;

    invoke-interface {v4, v5, v1}, LX/1ca;->a(LX/1cj;Ljava/util/concurrent/Executor;)V

    goto :goto_2
.end method
