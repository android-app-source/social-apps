.class public LX/Fc0;
.super LX/FbI;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public final a:LX/Fc1;

.field public final b:LX/Fc3;


# direct methods
.method public constructor <init>(LX/Fc1;LX/Fc3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2261563
    invoke-direct {p0}, LX/FbI;-><init>()V

    .line 2261564
    iput-object p1, p0, LX/Fc0;->a:LX/Fc1;

    .line 2261565
    iput-object p2, p0, LX/Fc0;->b:LX/Fc3;

    .line 2261566
    return-void
.end method

.method public static a(LX/0QB;)LX/Fc0;
    .locals 5

    .prologue
    .line 2261567
    const-class v1, LX/Fc0;

    monitor-enter v1

    .line 2261568
    :try_start_0
    sget-object v0, LX/Fc0;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2261569
    sput-object v2, LX/Fc0;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2261570
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2261571
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2261572
    new-instance p0, LX/Fc0;

    invoke-static {v0}, LX/Fc1;->a(LX/0QB;)LX/Fc1;

    move-result-object v3

    check-cast v3, LX/Fc1;

    invoke-static {v0}, LX/Fc3;->a(LX/0QB;)LX/Fc3;

    move-result-object v4

    check-cast v4, LX/Fc3;

    invoke-direct {p0, v3, v4}, LX/Fc0;-><init>(LX/Fc1;LX/Fc3;)V

    .line 2261573
    move-object v0, p0

    .line 2261574
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2261575
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fc0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2261576
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2261577
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;)LX/8dV;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2261578
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    .line 2261579
    if-nez v1, :cond_1

    .line 2261580
    :cond_0
    :goto_0
    return-object v0

    .line 2261581
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLNode;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 2261582
    if-eqz v1, :cond_0

    .line 2261583
    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 2261584
    const/4 v2, 0x0

    :goto_1
    move-object v1, v2

    .line 2261585
    if-eqz v1, :cond_0

    .line 2261586
    invoke-virtual {v1, p1}, LX/FbI;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;)LX/8dV;

    move-result-object v0

    goto :goto_0

    .line 2261587
    :sswitch_0
    iget-object v2, p0, LX/Fc0;->a:LX/Fc1;

    goto :goto_1

    .line 2261588
    :sswitch_1
    iget-object v2, p0, LX/Fc0;->b:LX/Fc3;

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x4ed245b -> :sswitch_0
        0x1eaef984 -> :sswitch_1
    .end sparse-switch
.end method
