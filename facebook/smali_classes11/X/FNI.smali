.class public LX/FNI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:Landroid/net/Uri;

.field private static final d:[Ljava/lang/String;

.field private static final e:Z


# instance fields
.field public f:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public g:LX/2Oq;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public h:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public i:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMB;",
            ">;"
        }
    .end annotation
.end field

.field public j:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FMo;",
            ">;"
        }
    .end annotation
.end field

.field public k:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6jC;",
            ">;"
        }
    .end annotation
.end field

.field public m:LX/0Ot;
    .annotation runtime Lcom/facebook/messaging/annotations/ForSecureIntentHandlerActivity;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Landroid/content/ComponentName;",
            ">;"
        }
    .end annotation
.end field

.field public n:LX/FMI;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2232390
    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v1

    const-string v3, "thread_id"

    aput-object v3, v2, v0

    const-string v3, "type"

    aput-object v3, v2, v4

    sput-object v2, LX/FNI;->a:[Ljava/lang/String;

    .line 2232391
    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v1

    const-string v3, "thread_id"

    aput-object v3, v2, v0

    const-string v3, "msg_box"

    aput-object v3, v2, v4

    const-string v3, "m_type"

    aput-object v3, v2, v5

    sput-object v2, LX/FNI;->b:[Ljava/lang/String;

    .line 2232392
    sget-object v2, LX/2Uo;->a:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "simple"

    const-string v4, "true"

    invoke-virtual {v2, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    sput-object v2, LX/FNI;->c:Landroid/net/Uri;

    .line 2232393
    new-array v2, v0, [Ljava/lang/String;

    const-string v3, "recipient_ids"

    aput-object v3, v2, v1

    sput-object v2, LX/FNI;->d:[Ljava/lang/String;

    .line 2232394
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x17

    if-lt v2, v3, :cond_0

    :goto_0
    sput-boolean v0, LX/FNI;->e:Z

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2232395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2232396
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2232397
    iput-object v0, p0, LX/FNI;->i:LX/0Ot;

    .line 2232398
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2232399
    iput-object v0, p0, LX/FNI;->j:LX/0Ot;

    .line 2232400
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2232401
    iput-object v0, p0, LX/FNI;->k:LX/0Ot;

    .line 2232402
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2232403
    iput-object v0, p0, LX/FNI;->l:LX/0Ot;

    .line 2232404
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2232405
    iput-object v0, p0, LX/FNI;->m:LX/0Ot;

    .line 2232406
    return-void
.end method
