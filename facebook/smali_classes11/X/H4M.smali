.class public final LX/H4M;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TF;

.field public final synthetic b:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;

.field public final synthetic c:LX/H4N;


# direct methods
.method public constructor <init>(LX/H4N;LX/0TF;Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;)V
    .locals 0

    .prologue
    .line 2422649
    iput-object p1, p0, LX/H4M;->c:LX/H4N;

    iput-object p2, p0, LX/H4M;->a:LX/0TF;

    iput-object p3, p0, LX/H4M;->b:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2422650
    iget-object v0, p0, LX/H4M;->a:LX/0TF;

    if-eqz v0, :cond_0

    .line 2422651
    iget-object v0, p0, LX/H4M;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 2422652
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2422653
    check-cast p1, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;

    const/4 v1, 0x0

    .line 2422654
    iget-object v0, p0, LX/H4M;->a:LX/0TF;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    .line 2422655
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2422656
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;->a()LX/0Px;

    move-result-object v3

    move v0, v1

    .line 2422657
    :goto_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 2422658
    invoke-virtual {v3, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2422659
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2422660
    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;->j()Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$TypeaheadPlacesModel;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;->j()Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$TypeaheadPlacesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$TypeaheadPlacesModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2422661
    :cond_1
    :goto_1
    return-void

    .line 2422662
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;->j()Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$TypeaheadPlacesModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$TypeaheadPlacesModel;->a()LX/0Px;

    move-result-object v3

    .line 2422663
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2422664
    :goto_2
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 2422665
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadPlaceResultsConnectionFragmentModel$EdgesModel;

    .line 2422666
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadPlaceResultsConnectionFragmentModel$EdgesModel;->a()Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadPlaceFragmentModel;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 2422667
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadPlaceResultsConnectionFragmentModel$EdgesModel;->a()Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadPlaceFragmentModel;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2422668
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2422669
    :cond_4
    new-instance v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;

    iget-object v1, p0, LX/H4M;->b:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;

    .line 2422670
    iget-object v3, v1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->c:Ljava/lang/String;

    move-object v1, v3

    .line 2422671
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel;->j()Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$TypeaheadPlacesModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesPlacesAndTopicsQueryModel$TypeaheadPlacesModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesPlacesAndTopicsResult;-><init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;)V

    .line 2422672
    iget-object v1, p0, LX/H4M;->a:LX/0TF;

    invoke-interface {v1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_1
.end method
