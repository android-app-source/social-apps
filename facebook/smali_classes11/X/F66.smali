.class public final LX/F66;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/composer/publish/common/EditPostParams;

.field public final synthetic b:LX/F67;


# direct methods
.method public constructor <init>(LX/F67;Lcom/facebook/composer/publish/common/EditPostParams;)V
    .locals 0

    .prologue
    .line 2200149
    iput-object p1, p0, LX/F66;->b:LX/F67;

    iput-object p2, p0, LX/F66;->a:Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2200143
    iget-object v0, p0, LX/F66;->b:LX/F67;

    iget-object v0, v0, LX/F67;->a:LX/5pX;

    const v1, 0x7f0831d3

    invoke-static {v0, v1}, LX/0kL;->a(Landroid/content/Context;I)V

    .line 2200144
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2200145
    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 2200146
    iget-object v0, p0, LX/F66;->b:LX/F67;

    iget-object v0, v0, LX/F67;->h:LX/3H7;

    iget-object v1, p0, LX/F66;->a:Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-virtual {v1}, Lcom/facebook/composer/publish/common/EditPostParams;->getStoryId()Ljava/lang/String;

    move-result-object v1

    sget-object v3, LX/5Go;->GRAPHQL_DEFAULT:LX/5Go;

    sget-object v4, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    iget-object v5, p0, LX/F66;->a:Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-virtual {v5}, Lcom/facebook/composer/publish/common/EditPostParams;->getCacheIds()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, LX/F66;->a:Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-virtual {v5}, Lcom/facebook/composer/publish/common/EditPostParams;->getCacheIds()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    move-object v5, v2

    :goto_0
    sget-object v6, LX/21y;->DEFAULT_ORDER:LX/21y;

    invoke-virtual/range {v0 .. v7}, LX/3H7;->a(Ljava/lang/String;Ljava/lang/String;LX/5Go;LX/0rS;Ljava/lang/String;LX/21y;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    new-instance v1, LX/F65;

    invoke-direct {v1, p0}, LX/F65;-><init>(LX/F66;)V

    iget-object v2, p0, LX/F66;->b:LX/F67;

    iget-object v2, v2, LX/F67;->i:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2200147
    return-void

    .line 2200148
    :cond_1
    iget-object v5, p0, LX/F66;->a:Lcom/facebook/composer/publish/common/EditPostParams;

    invoke-virtual {v5}, Lcom/facebook/composer/publish/common/EditPostParams;->getCacheIds()LX/0Px;

    move-result-object v5

    invoke-virtual {v5, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    goto :goto_0
.end method
