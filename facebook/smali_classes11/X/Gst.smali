.class public final LX/Gst;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/InternSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/InternSettingsActivity;)V
    .locals 0

    .prologue
    .line 2400330
    iput-object p1, p0, LX/Gst;->a:Lcom/facebook/katana/InternSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2400331
    const-string v0, "cookies"

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2400332
    iget-object v0, p0, LX/Gst;->a:Lcom/facebook/katana/InternSettingsActivity;

    invoke-static {v0}, Landroid/webkit/CookieSyncManager;->createInstance(Landroid/content/Context;)Landroid/webkit/CookieSyncManager;

    .line 2400333
    invoke-static {}, Landroid/webkit/CookieManager;->getInstance()Landroid/webkit/CookieManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/webkit/CookieManager;->removeAllCookie()V

    .line 2400334
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 2400335
    :cond_1
    const-string v0, "cache"

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2400336
    new-instance v0, LX/0D4;

    iget-object v1, p0, LX/Gst;->a:Lcom/facebook/katana/InternSettingsActivity;

    invoke-direct {v0, v1}, LX/0D4;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, LX/0D4;->clearCache(Z)V

    .line 2400337
    iget-object v0, p0, LX/Gst;->a:Lcom/facebook/katana/InternSettingsActivity;

    invoke-static {v0}, LX/Gwx;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
