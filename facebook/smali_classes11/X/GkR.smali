.class public final LX/GkR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Landroid/content/Context;

.field public final synthetic c:Z

.field public final synthetic d:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;Ljava/lang/String;Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 2389097
    iput-object p1, p0, LX/GkR;->d:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iput-object p2, p0, LX/GkR;->a:Ljava/lang/String;

    iput-object p3, p0, LX/GkR;->b:Landroid/content/Context;

    iput-boolean p4, p0, LX/GkR;->c:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    .line 2389098
    iget-object v0, p0, LX/GkR;->d:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->iC_()LX/0gc;

    move-result-object v0

    const v1, 0x7f08302d

    invoke-static {v0, v1}, LX/DPS;->a(LX/0gc;I)Landroid/support/v4/app/DialogFragment;

    move-result-object v1

    .line 2389099
    iget-object v0, p0, LX/GkR;->d:Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;

    iget-object v0, v0, Lcom/facebook/groups/fb4a/groupsections/fragment/FB4AGroupGridFragment;->o:LX/Gkk;

    check-cast v0, LX/Gl6;

    iget-object v2, p0, LX/GkR;->a:Ljava/lang/String;

    new-instance v3, LX/GkQ;

    invoke-direct {v3, p0, v1}, LX/GkQ;-><init>(LX/GkR;Landroid/support/v4/app/DialogFragment;)V

    iget-boolean v1, p0, LX/GkR;->c:Z

    .line 2389100
    if-eqz v1, :cond_0

    const-string v4, "ALLOW_READD"

    .line 2389101
    :goto_0
    iget-object v5, v0, LX/Gl6;->m:LX/1Ck;

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object p1, LX/Gl5;->LEAVE_GROUP:LX/Gl5;

    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    iget-object p1, v0, LX/Gl6;->n:LX/3mF;

    const-string p2, "mobile_groups_dash"

    invoke-virtual {p1, v2, p2, v4}, LX/3mF;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    new-instance p1, LX/Gku;

    invoke-direct {p1, v0, v3, v2}, LX/Gku;-><init>(LX/Gl6;LX/GkQ;Ljava/lang/String;)V

    invoke-virtual {v5, p0, v4, p1}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2389102
    return-void

    .line 2389103
    :cond_0
    const-string v4, "PREVENT_READD"

    goto :goto_0
.end method
