.class public final enum LX/Gda;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gda;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gda;

.field public static final enum BACK_BUTTON:LX/Gda;

.field public static final enum INTERNAL_RESET:LX/Gda;

.field public static final enum NEWS_FEED_TAB:LX/Gda;

.field public static final enum TOPIC_SWIPE:LX/Gda;

.field public static final enum TOPIC_TAB:LX/Gda;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2374343
    new-instance v0, LX/Gda;

    const-string v1, "BACK_BUTTON"

    invoke-direct {v0, v1, v2}, LX/Gda;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gda;->BACK_BUTTON:LX/Gda;

    .line 2374344
    new-instance v0, LX/Gda;

    const-string v1, "TOPIC_TAB"

    invoke-direct {v0, v1, v3}, LX/Gda;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gda;->TOPIC_TAB:LX/Gda;

    .line 2374345
    new-instance v0, LX/Gda;

    const-string v1, "NEWS_FEED_TAB"

    invoke-direct {v0, v1, v4}, LX/Gda;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gda;->NEWS_FEED_TAB:LX/Gda;

    .line 2374346
    new-instance v0, LX/Gda;

    const-string v1, "TOPIC_SWIPE"

    invoke-direct {v0, v1, v5}, LX/Gda;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gda;->TOPIC_SWIPE:LX/Gda;

    .line 2374347
    new-instance v0, LX/Gda;

    const-string v1, "INTERNAL_RESET"

    invoke-direct {v0, v1, v6}, LX/Gda;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gda;->INTERNAL_RESET:LX/Gda;

    .line 2374348
    const/4 v0, 0x5

    new-array v0, v0, [LX/Gda;

    sget-object v1, LX/Gda;->BACK_BUTTON:LX/Gda;

    aput-object v1, v0, v2

    sget-object v1, LX/Gda;->TOPIC_TAB:LX/Gda;

    aput-object v1, v0, v3

    sget-object v1, LX/Gda;->NEWS_FEED_TAB:LX/Gda;

    aput-object v1, v0, v4

    sget-object v1, LX/Gda;->TOPIC_SWIPE:LX/Gda;

    aput-object v1, v0, v5

    sget-object v1, LX/Gda;->INTERNAL_RESET:LX/Gda;

    aput-object v1, v0, v6

    sput-object v0, LX/Gda;->$VALUES:[LX/Gda;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2374349
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gda;
    .locals 1

    .prologue
    .line 2374350
    const-class v0, LX/Gda;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gda;

    return-object v0
.end method

.method public static values()[LX/Gda;
    .locals 1

    .prologue
    .line 2374351
    sget-object v0, LX/Gda;->$VALUES:[LX/Gda;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gda;

    return-object v0
.end method
