.class public LX/GRv;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2351853
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2351854
    iput-object p1, p0, LX/GRv;->a:LX/0Zb;

    .line 2351855
    return-void
.end method

.method public static b(LX/0QB;)LX/GRv;
    .locals 2

    .prologue
    .line 2351856
    new-instance v1, LX/GRv;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/GRv;-><init>(LX/0Zb;)V

    .line 2351857
    return-object v1
.end method

.method public static b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;
    .locals 2

    .prologue
    .line 2351858
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {v0, p0}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "app_invite"

    .line 2351859
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2351860
    move-object v0, v0

    .line 2351861
    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;)V
    .locals 3

    .prologue
    .line 2351862
    invoke-static {p1}, LX/GRv;->b(Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2351863
    invoke-virtual {p2}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->mv_()Ljava/lang/String;

    move-result-object v1

    .line 2351864
    iput-object v1, v0, Lcom/facebook/analytics/logger/HoneyClientEvent;->e:Ljava/lang/String;

    .line 2351865
    const-string v1, "app_id"

    invoke-virtual {p2}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->c()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$ApplicationModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2351866
    const-string v1, "sender_id"

    invoke-virtual {p2}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel;->j()Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/appinvites/protocol/FetchAppInvitesListQueryModels$AppInviteFieldsModel$SenderModel;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2351867
    iget-object v1, p0, LX/GRv;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2351868
    return-void
.end method
