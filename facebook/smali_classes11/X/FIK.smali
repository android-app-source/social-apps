.class public LX/FIK;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile g:LX/FIK;


# instance fields
.field private final a:LX/2Uj;

.field private final b:LX/0SG;

.field private final c:LX/FIJ;

.field public d:Ljava/util/Random;

.field private e:Ljava/lang/String;

.field private f:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method public constructor <init>(LX/2Uj;LX/0SG;)V
    .locals 4
    .param p1    # LX/2Uj;
        .annotation runtime Lcom/facebook/messaging/media/upload/udp/UDPOutgoingPacketQueue;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2221957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2221958
    iput-object p1, p0, LX/FIK;->a:LX/2Uj;

    .line 2221959
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, LX/FIK;->d:Ljava/util/Random;

    .line 2221960
    new-instance v0, LX/FIJ;

    invoke-direct {v0, p0}, LX/FIJ;-><init>(LX/FIK;)V

    iput-object v0, p0, LX/FIK;->c:LX/FIJ;

    .line 2221961
    iput-object p2, p0, LX/FIK;->b:LX/0SG;

    .line 2221962
    const/16 p2, 0xa

    .line 2221963
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, p2}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2221964
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    .line 2221965
    const-string v2, "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"

    iget-object v3, p0, LX/FIK;->d:Ljava/util/Random;

    const/16 p1, 0x24

    invoke-virtual {v3, p1}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2221966
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2221967
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 2221968
    iput-object v0, p0, LX/FIK;->e:Ljava/lang/String;

    .line 2221969
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, LX/FIK;->f:Ljava/util/concurrent/atomic/AtomicLong;

    .line 2221970
    return-void
.end method

.method public static a(LX/0QB;)LX/FIK;
    .locals 5

    .prologue
    .line 2221971
    sget-object v0, LX/FIK;->g:LX/FIK;

    if-nez v0, :cond_1

    .line 2221972
    const-class v1, LX/FIK;

    monitor-enter v1

    .line 2221973
    :try_start_0
    sget-object v0, LX/FIK;->g:LX/FIK;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2221974
    if-eqz v2, :cond_0

    .line 2221975
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2221976
    new-instance p0, LX/FIK;

    invoke-static {v0}, LX/2Um;->a(LX/0QB;)LX/2Uj;

    move-result-object v3

    check-cast v3, LX/2Uj;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {p0, v3, v4}, LX/FIK;-><init>(LX/2Uj;LX/0SG;)V

    .line 2221977
    move-object v0, p0

    .line 2221978
    sput-object v0, LX/FIK;->g:LX/FIK;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2221979
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2221980
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2221981
    :cond_1
    sget-object v0, LX/FIK;->g:LX/FIK;

    return-object v0

    .line 2221982
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2221983
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2221984
    iget-object v1, p0, LX/FIK;->c:LX/FIJ;

    monitor-enter v1

    .line 2221985
    :try_start_0
    iget-object v0, p0, LX/FIK;->c:LX/FIJ;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, LX/FIK;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, LX/FIJ;->c:Ljava/lang/String;

    .line 2221986
    iget-object v0, p0, LX/FIK;->c:LX/FIJ;

    iput-object p2, v0, LX/FIJ;->d:Ljava/lang/String;

    .line 2221987
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public final a()Z
    .locals 4

    .prologue
    .line 2221988
    iget-object v0, p0, LX/FIK;->b:LX/0SG;

    invoke-interface {v0}, LX/0SG;->a()J

    move-result-wide v0

    iget-object v2, p0, LX/FIK;->f:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-virtual {p0}, LX/FIK;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 2221989
    iget-object v2, p0, LX/FIK;->c:LX/FIJ;

    monitor-enter v2

    .line 2221990
    :try_start_0
    iget-object v1, p0, LX/FIK;->c:LX/FIJ;

    .line 2221991
    iget-wide v8, v1, LX/FIJ;->b:J

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_1

    iget v8, v1, LX/FIJ;->a:I

    if-eqz v8, :cond_1

    iget-object v8, v1, LX/FIJ;->c:Ljava/lang/String;

    invoke-static {v8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    iget-object v8, v1, LX/FIJ;->d:Ljava/lang/String;

    invoke-static {v8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    const/4 v8, 0x1

    :goto_0
    move v1, v8

    .line 2221992
    if-eqz v1, :cond_0

    .line 2221993
    new-instance v1, LX/FII;

    invoke-direct {v1}, LX/FII;-><init>()V

    .line 2221994
    iget-object v3, p0, LX/FIK;->c:LX/FIJ;

    .line 2221995
    iput-object v3, v1, LX/FII;->f:LX/FIJ;

    .line 2221996
    move-object v3, v1

    .line 2221997
    invoke-static {}, LX/FIN;->a()[B

    move-result-object v4

    .line 2221998
    iput-object v4, v3, LX/FIH;->a:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2221999
    :try_start_1
    iget-object v3, p0, LX/FIK;->a:LX/2Uj;

    invoke-virtual {v1}, LX/FII;->a()LX/FIP;

    move-result-object v1

    invoke-virtual {v3, v1}, LX/2Uj;->a(LX/FIO;)V

    .line 2222000
    iget-object v1, p0, LX/FIK;->f:Ljava/util/concurrent/atomic/AtomicLong;

    iget-object v3, p0, LX/FIK;->b:LX/0SG;

    invoke-interface {v3}, LX/0SG;->a()J

    move-result-wide v4

    const-wide/16 v6, 0x1f4

    add-long/2addr v4, v6

    invoke-virtual {v1, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2222001
    const/4 v0, 0x1

    :try_start_2
    monitor-exit v2

    .line 2222002
    :goto_1
    return v0

    .line 2222003
    :catch_0
    move-exception v1

    .line 2222004
    const-class v3, LX/FIK;

    const-string v4, "Unable to build stun ping"

    invoke-static {v3, v4, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2222005
    monitor-exit v2

    goto :goto_1

    .line 2222006
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 2222007
    :cond_0
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :cond_1
    const/4 v8, 0x0

    goto :goto_0
.end method
