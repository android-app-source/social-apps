.class public LX/GLj;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D:",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ":",
        "Ljava/lang/Object;",
        ">",
        "LX/GHg",
        "<",
        "Lcom/facebook/widget/text/BetterEditTextView;",
        "TD;>;"
    }
.end annotation


# instance fields
.field private final a:Landroid/view/inputmethod/InputMethodManager;

.field private b:Lcom/facebook/widget/text/BetterEditTextView;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

.field private f:Landroid/text/TextWatcher;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field public i:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TD;"
        }
    .end annotation
.end field

.field public j:Z

.field private k:LX/GCE;


# direct methods
.method public constructor <init>(Landroid/view/inputmethod/InputMethodManager;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2343581
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2343582
    iput-object p1, p0, LX/GLj;->a:Landroid/view/inputmethod/InputMethodManager;

    .line 2343583
    return-void
.end method

.method public static a(LX/0QB;)LX/GLj;
    .locals 1

    .prologue
    .line 2343580
    invoke-static {p0}, LX/GLj;->b(LX/0QB;)LX/GLj;

    move-result-object v0

    return-object v0
.end method

.method public static a$redex0(LX/GLj;LX/GLi;)V
    .locals 2

    .prologue
    .line 2343575
    sget-object v0, LX/GLh;->a:[I

    invoke-virtual {p1}, LX/GLi;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2343576
    :goto_0
    return-void

    .line 2343577
    :pswitch_0
    iget-object v0, p0, LX/GLj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v1, p0, LX/GLj;->g:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    goto :goto_0

    .line 2343578
    :pswitch_1
    iget-object v0, p0, LX/GLj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v1, p0, LX/GLj;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    goto :goto_0

    .line 2343579
    :pswitch_2
    iget-object v0, p0, LX/GLj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterText(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(LX/0QB;)LX/GLj;
    .locals 2

    .prologue
    .line 2343573
    new-instance v1, LX/GLj;

    invoke-static {p0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-direct {v1, v0}, LX/GLj;-><init>(Landroid/view/inputmethod/InputMethodManager;)V

    .line 2343574
    return-object v1
.end method

.method public static b(Ljava/lang/String;)Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Landroid/util/Pair",
            "<",
            "LX/GLi;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2343564
    sget-object v1, LX/GLi;->VALID:LX/GLi;

    .line 2343565
    const-string v0, ""

    .line 2343566
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v2}, LX/0YN;->a([Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2343567
    const-string v0, "http://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "https://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "http://"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    .line 2343568
    :cond_0
    :goto_0
    sget-object v0, Landroid/util/Patterns;->WEB_URL:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2343569
    sget-object v0, LX/GLi;->FORMAT_ERROR:LX/GLi;

    .line 2343570
    :goto_1
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, LX/1H1;->c(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2343571
    sget-object v0, LX/GLi;->FACEBOOK_URL_ERROR:LX/GLi;

    .line 2343572
    :cond_1
    new-instance v1, Landroid/util/Pair;

    invoke-direct {v1, v0, p0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v1

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    move-object p0, v0

    goto :goto_0
.end method

.method public static b(LX/GLj;Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2343584
    iput-boolean p1, p0, LX/GLj;->j:Z

    .line 2343585
    iget-boolean v0, p0, LX/GHg;->a:Z

    move v0, v0

    .line 2343586
    if-nez v0, :cond_0

    .line 2343587
    :goto_0
    return-void

    .line 2343588
    :cond_0
    iget-object v0, p0, LX/GLj;->c:Ljava/lang/String;

    invoke-static {v0}, LX/GLj;->b(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v2

    .line 2343589
    iget-object v3, p0, LX/GLj;->k:LX/GCE;

    sget-object v4, LX/GG8;->INVALID_URL:LX/GG8;

    iget-boolean v0, p0, LX/GLj;->j:Z

    if-eqz v0, :cond_1

    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    sget-object v5, LX/GLi;->VALID:LX/GLi;

    if-ne v0, v5, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v3, v4, v0}, LX/GCE;->a(LX/GG8;Z)V

    .line 2343590
    if-nez p1, :cond_3

    .line 2343591
    iget-object v0, p0, LX/GLj;->a:Landroid/view/inputmethod/InputMethodManager;

    iget-object v2, p0, LX/GLj;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/BetterEditTextView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2343592
    :goto_2
    iget-object v0, p0, LX/GLj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    if-eqz p1, :cond_4

    :goto_3
    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setVisibility(I)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2343593
    goto :goto_1

    .line 2343594
    :cond_3
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, LX/GLi;

    invoke-static {p0, v0}, LX/GLj;->a$redex0(LX/GLj;LX/GLi;)V

    goto :goto_2

    .line 2343595
    :cond_4
    const/16 v1, 0x8

    goto :goto_3
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2343557
    invoke-super {p0}, LX/GHg;->a()V

    .line 2343558
    iput-object v2, p0, LX/GLj;->k:LX/GCE;

    .line 2343559
    iget-object v0, p0, LX/GLj;->b:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v1, p0, LX/GLj;->f:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2343560
    iput-object v2, p0, LX/GLj;->f:Landroid/text/TextWatcher;

    .line 2343561
    iput-object v2, p0, LX/GLj;->b:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2343562
    iput-object v2, p0, LX/GLj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2343563
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2343555
    const-string v0, "website_url_key"

    iget-object v1, p0, LX/GLj;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2343556
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 5
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2343531
    check-cast p1, Lcom/facebook/widget/text/BetterEditTextView;

    const/4 v0, 0x0

    .line 2343532
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2343533
    iput-object p2, p0, LX/GLj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2343534
    iput-object p1, p0, LX/GLj;->b:Lcom/facebook/widget/text/BetterEditTextView;

    .line 2343535
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2343536
    iput-object v1, p0, LX/GLj;->k:LX/GCE;

    .line 2343537
    iget-object v1, p0, LX/GLj;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080b16

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/GLj;->g:Ljava/lang/String;

    .line 2343538
    iget-object v1, p0, LX/GLj;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterEditTextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080b17

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LX/GLj;->h:Ljava/lang/String;

    .line 2343539
    iget-object v1, p0, LX/GLj;->i:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->R()Z

    move-result v1

    invoke-static {p0, v1}, LX/GLj;->b(LX/GLj;Z)V

    .line 2343540
    iget-object v1, p0, LX/GLj;->e:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    iget-object v2, p0, LX/GLj;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/BetterEditTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/GLj;->i:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->S()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setHeaderTitle(Ljava/lang/String;)V

    .line 2343541
    iget-object v1, p0, LX/GLj;->b:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v2, p0, LX/GLj;->b:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v2}, Lcom/facebook/widget/text/BetterEditTextView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/GLj;->i:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->T()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterEditTextView;->setHint(Ljava/lang/CharSequence;)V

    .line 2343542
    iget-object v1, p0, LX/GLj;->c:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/GLj;->c:Ljava/lang/String;

    invoke-static {v1}, LX/GLj;->b(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    iget-object v1, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    sget-object v2, LX/GLi;->VALID:LX/GLi;

    if-eq v1, v2, :cond_1

    .line 2343543
    :cond_0
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2343544
    sget-object v2, LX/GG8;->INVALID_URL:LX/GG8;

    new-instance v3, LX/GLe;

    invoke-direct {v3, p0}, LX/GLe;-><init>(LX/GLj;)V

    invoke-virtual {v1, v2, v0, v3}, LX/GCE;->a(LX/GG8;ZLX/GLe;)V

    .line 2343545
    :cond_1
    new-instance v1, LX/GLf;

    invoke-direct {v1, p0}, LX/GLf;-><init>(LX/GLj;)V

    iput-object v1, p0, LX/GLj;->f:Landroid/text/TextWatcher;

    .line 2343546
    iget-object v1, p0, LX/GLj;->b:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v2, p0, LX/GLj;->f:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterEditTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2343547
    iget-object v1, p0, LX/GLj;->b:Lcom/facebook/widget/text/BetterEditTextView;

    const/high16 v2, 0x80000

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterEditTextView;->setInputType(I)V

    .line 2343548
    iget-object v1, p0, LX/GLj;->c:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2343549
    iget-object v1, p0, LX/GLj;->b:Lcom/facebook/widget/text/BetterEditTextView;

    iget-object v2, p0, LX/GLj;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2343550
    :cond_2
    iget-object v1, p0, LX/GLj;->k:LX/GCE;

    new-instance v2, LX/GLg;

    invoke-direct {v2, p0}, LX/GLg;-><init>(LX/GLj;)V

    invoke-virtual {v1, v2}, LX/GCE;->a(LX/8wQ;)V

    .line 2343551
    iget-boolean v1, p0, LX/GLj;->j:Z

    if-eqz v1, :cond_4

    .line 2343552
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2343553
    sget-object v2, LX/GG8;->INVALID_URL:LX/GG8;

    iget-object v3, p0, LX/GLj;->c:Ljava/lang/String;

    invoke-static {v3}, LX/GLj;->b(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v3

    iget-object v3, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    sget-object v4, LX/GLi;->VALID:LX/GLi;

    if-ne v3, v4, :cond_3

    const/4 v0, 0x1

    :cond_3
    invoke-virtual {v1, v2, v0}, LX/GCE;->a(LX/GG8;Z)V

    .line 2343554
    :cond_4
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 1

    .prologue
    .line 2343527
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2343528
    iput-object p1, p0, LX/GLj;->i:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2343529
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->Q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GLj;->c:Ljava/lang/String;

    .line 2343530
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2343524
    if-eqz p1, :cond_0

    .line 2343525
    iget-object v0, p0, LX/GLj;->b:Lcom/facebook/widget/text/BetterEditTextView;

    const-string v1, "website_url_key"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/widget/text/BetterEditTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2343526
    :cond_0
    return-void
.end method
