.class public LX/GoA;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static n:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

.field public final b:LX/Go0;

.field public final c:LX/Chv;

.field public final d:LX/ChP;

.field public e:J

.field public f:Z

.field public g:J

.field public h:J

.field public i:Z

.field public j:F

.field public k:J

.field public final l:LX/CIb;

.field private final m:LX/Chz;


# direct methods
.method public constructor <init>(LX/Chv;Landroid/content/Context;LX/CIb;LX/Go0;Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2394126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2394127
    new-instance v0, LX/Go8;

    invoke-direct {v0, p0}, LX/Go8;-><init>(LX/GoA;)V

    iput-object v0, p0, LX/GoA;->d:LX/ChP;

    .line 2394128
    new-instance v0, LX/Go9;

    invoke-direct {v0, p0}, LX/Go9;-><init>(LX/GoA;)V

    iput-object v0, p0, LX/GoA;->m:LX/Chz;

    .line 2394129
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GoA;->f:Z

    .line 2394130
    iput-object p1, p0, LX/GoA;->c:LX/Chv;

    .line 2394131
    iput-object p3, p0, LX/GoA;->l:LX/CIb;

    .line 2394132
    iput-object p4, p0, LX/GoA;->b:LX/Go0;

    .line 2394133
    iput-object p5, p0, LX/GoA;->a:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    .line 2394134
    iget-object v0, p0, LX/GoA;->c:LX/Chv;

    iget-object v1, p0, LX/GoA;->d:LX/ChP;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2394135
    iget-object v0, p0, LX/GoA;->c:LX/Chv;

    iget-object v1, p0, LX/GoA;->m:LX/Chz;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2394136
    return-void
.end method

.method public static a(LX/0QB;)LX/GoA;
    .locals 9

    .prologue
    .line 2394137
    const-class v1, LX/GoA;

    monitor-enter v1

    .line 2394138
    :try_start_0
    sget-object v0, LX/GoA;->n:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2394139
    sput-object v2, LX/GoA;->n:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2394140
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2394141
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2394142
    new-instance v3, LX/GoA;

    invoke-static {v0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v4

    check-cast v4, LX/Chv;

    const-class v5, Landroid/content/Context;

    invoke-interface {v0, v5}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/content/Context;

    invoke-static {v0}, LX/CIb;->a(LX/0QB;)LX/CIb;

    move-result-object v6

    check-cast v6, LX/CIb;

    invoke-static {v0}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object v7

    check-cast v7, LX/Go0;

    invoke-static {v0}, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->a(LX/0QB;)Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    move-result-object v8

    check-cast v8, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    invoke-direct/range {v3 .. v8}, LX/GoA;-><init>(LX/Chv;Landroid/content/Context;LX/CIb;LX/Go0;Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;)V

    .line 2394143
    move-object v0, v3

    .line 2394144
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2394145
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GoA;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2394146
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2394147
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
