.class public LX/FEe;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FEd;


# instance fields
.field public final a:Lcom/facebook/messaging/games/GamesSelectionAdapter;

.field private final b:LX/FEW;

.field private final c:Landroid/support/v7/widget/RecyclerView;

.field public final d:LX/FEX;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/games/GamesSelectionAdapter;LX/FEW;Landroid/support/v7/widget/RecyclerView;LX/FEX;)V
    .locals 2
    .param p3    # Landroid/support/v7/widget/RecyclerView;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/FEX;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2216549
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2216550
    iput-object p1, p0, LX/FEe;->a:Lcom/facebook/messaging/games/GamesSelectionAdapter;

    .line 2216551
    iput-object p2, p0, LX/FEe;->b:LX/FEW;

    .line 2216552
    iput-object p3, p0, LX/FEe;->c:Landroid/support/v7/widget/RecyclerView;

    .line 2216553
    iput-object p4, p0, LX/FEe;->d:LX/FEX;

    .line 2216554
    iget-object v0, p0, LX/FEe;->c:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, LX/FEe;->a:Lcom/facebook/messaging/games/GamesSelectionAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(LX/1OM;)V

    .line 2216555
    iget-object v0, p0, LX/FEe;->a:Lcom/facebook/messaging/games/GamesSelectionAdapter;

    .line 2216556
    iput-object p4, v0, Lcom/facebook/messaging/games/GamesSelectionAdapter;->c:LX/FEX;

    .line 2216557
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 2216558
    iget-object v0, p0, LX/FEe;->b:LX/FEW;

    .line 2216559
    iget-object v1, v0, LX/FEW;->b:LX/1Ck;

    const-string v2, "games_list_query"

    invoke-virtual {v1, v2}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2216560
    iget-object v0, p0, LX/FEe;->b:LX/FEW;

    new-instance v1, LX/FEc;

    invoke-direct {v1, p0, p2}, LX/FEc;-><init>(LX/FEe;Ljava/lang/String;)V

    .line 2216561
    new-instance v2, LX/DeN;

    invoke-direct {v2}, LX/DeN;-><init>()V

    move-object v2, v2

    .line 2216562
    new-instance v3, LX/4HE;

    invoke-direct {v3}, LX/4HE;-><init>()V

    .line 2216563
    if-eqz p1, :cond_0

    .line 2216564
    const-string v4, "thread_id"

    invoke-virtual {v3, v4, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2216565
    :cond_0
    const-string v4, "text"

    invoke-virtual {v3, v4, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2216566
    const-string v4, "query_params"

    invoke-virtual {v2, v4, v3}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2216567
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    sget-object v3, LX/0zS;->a:LX/0zS;

    invoke-virtual {v2, v3}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v2

    const-wide/16 v4, 0xe10

    invoke-virtual {v2, v4, v5}, LX/0zO;->a(J)LX/0zO;

    move-result-object v2

    .line 2216568
    iget-object v3, v0, LX/FEW;->a:LX/0tX;

    invoke-virtual {v3, v2}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 2216569
    new-instance v3, LX/FEV;

    invoke-direct {v3, v0, v1}, LX/FEV;-><init>(LX/FEW;LX/FEc;)V

    .line 2216570
    iget-object v4, v0, LX/FEW;->b:LX/1Ck;

    const-string v5, "games_list_query"

    invoke-virtual {v4, v5, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2216571
    return-void
.end method
