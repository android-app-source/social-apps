.class public final LX/FpO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorFragment;)V
    .locals 0

    .prologue
    .line 2291535
    iput-object p1, p0, LX/FpO;->a:Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2291536
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 2291537
    const-string v1, "currency"

    iget-object v2, p0, LX/FpO;->a:Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorFragment;

    iget-object v2, v2, Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorFragment;->a:LX/FpT;

    invoke-virtual {v2, p3}, LX/FpT;->a(I)LX/FpS;

    move-result-object v2

    .line 2291538
    iget-object p1, v2, LX/FpS;->a:Ljava/lang/String;

    move-object v2, p1

    .line 2291539
    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2291540
    iget-object v1, p0, LX/FpO;->a:Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    .line 2291541
    iget-object v0, p0, LX/FpO;->a:Lcom/facebook/socialgood/ui/create/currencyselector/FundraiserCurrencySelectorFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2291542
    return-void
.end method
