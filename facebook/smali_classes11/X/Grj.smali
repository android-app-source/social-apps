.class public LX/Grj;
.super LX/Cts;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/D0y;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/Ctg;

.field public c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

.field private d:Landroid/view/View;

.field public e:Lcom/facebook/instantshopping/view/widget/InstantShoppingMapView;

.field private f:Lcom/facebook/fbui/glyph/GlyphView;

.field private g:I


# direct methods
.method public constructor <init>(LX/Ctg;Lcom/facebook/storelocator/StoreLocatorMapDelegate;Landroid/view/View;Lcom/facebook/instantshopping/view/widget/InstantShoppingMapView;Lcom/facebook/fbui/glyph/GlyphView;)V
    .locals 2

    .prologue
    .line 2398655
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 2398656
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2398657
    iput-object v0, p0, LX/Grj;->a:LX/0Ot;

    .line 2398658
    const-class v0, LX/Grj;

    invoke-static {v0, p0}, LX/Grj;->a(Ljava/lang/Class;LX/02k;)V

    .line 2398659
    iput-object p1, p0, LX/Grj;->b:LX/Ctg;

    .line 2398660
    iput-object p2, p0, LX/Grj;->c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    .line 2398661
    iput-object p5, p0, LX/Grj;->f:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2398662
    iput-object p3, p0, LX/Grj;->d:Landroid/view/View;

    .line 2398663
    iput-object p4, p0, LX/Grj;->e:Lcom/facebook/instantshopping/view/widget/InstantShoppingMapView;

    .line 2398664
    invoke-virtual {p0}, LX/Cts;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1c30

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, LX/Grj;->g:I

    .line 2398665
    iget-object v0, p0, LX/Grj;->f:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/Gri;

    invoke-direct {v1, p0}, LX/Gri;-><init>(LX/Grj;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2398666
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    check-cast p1, LX/Grj;

    const/16 p0, 0x359b

    invoke-static {v1, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v1

    iput-object v1, p1, LX/Grj;->a:LX/0Ot;

    return-void
.end method


# virtual methods
.method public final a(LX/CrS;)V
    .locals 6

    .prologue
    .line 2398667
    iget-object v0, p0, LX/Grj;->b:LX/Ctg;

    invoke-interface {v0}, LX/Ctg;->getMediaView()LX/Ct1;

    move-result-object v0

    invoke-interface {v0}, LX/Ct1;->getView()Landroid/view/View;

    move-result-object v0

    invoke-static {p1, v0}, LX/Cts;->a(LX/CrS;Landroid/view/View;)LX/CrW;

    move-result-object v0

    .line 2398668
    new-instance v1, Landroid/graphics/Rect;

    invoke-virtual {v0}, LX/CrW;->i()I

    move-result v2

    iget v3, p0, LX/Grj;->g:I

    add-int/2addr v2, v3

    invoke-virtual {v0}, LX/CrW;->g()I

    move-result v3

    iget-object v4, p0, LX/Grj;->d:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    iget v4, p0, LX/Grj;->g:I

    add-int/2addr v3, v4

    invoke-virtual {v0}, LX/CrW;->j()I

    move-result v4

    iget v5, p0, LX/Grj;->g:I

    sub-int/2addr v4, v5

    invoke-virtual {v0}, LX/CrW;->h()I

    move-result v0

    iget v5, p0, LX/Grj;->g:I

    sub-int/2addr v0, v5

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 2398669
    iget-object v0, p0, LX/Cts;->a:LX/Ctg;

    move-object v0, v0

    .line 2398670
    iget-object v2, p0, LX/Grj;->f:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-interface {v0, v2, v1}, LX/Ctg;->a(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2398671
    return-void
.end method
