.class public LX/Few;
.super LX/Dcc;
.source ""


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field private final b:LX/0tX;

.field public final c:LX/0se;

.field public final d:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/Fey;

.field private final f:LX/Fev;

.field private final g:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;",
            ">;>;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public final h:Z


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/0se;LX/Fey;LX/0QK;LX/0TF;Z)V
    .locals 2
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .param p5    # LX/0QK;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/0TF;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0tX;",
            "LX/0se;",
            "LX/Fey;",
            "LX/0QK",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;",
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoQueryModel;",
            ">;>;Z)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2266900
    invoke-direct {p0}, LX/Dcc;-><init>()V

    .line 2266901
    new-instance v0, LX/Fev;

    invoke-direct {v0, p0}, LX/Fev;-><init>(LX/Few;)V

    iput-object v0, p0, LX/Few;->f:LX/Fev;

    .line 2266902
    iput-object p1, p0, LX/Few;->a:Ljava/util/concurrent/ExecutorService;

    .line 2266903
    iput-object p2, p0, LX/Few;->b:LX/0tX;

    .line 2266904
    iput-object p3, p0, LX/Few;->c:LX/0se;

    .line 2266905
    iput-object p5, p0, LX/Few;->d:LX/0QK;

    .line 2266906
    iput-object p4, p0, LX/Few;->e:LX/Fey;

    .line 2266907
    iput-object p6, p0, LX/Few;->g:LX/0TF;

    .line 2266908
    iput-boolean p7, p0, LX/Few;->h:Z

    .line 2266909
    return-void
.end method

.method public static a(Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel$NodeModel;)Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;
    .locals 5

    .prologue
    .line 2266910
    invoke-virtual {p0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel$NodeModel;->E()Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$LinkMediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/search/protocol/Fb4aGraphSearchExternalMediaFragmentModels$Fb4aGraphSearchExternalMediaFragmentModel$LinkMediaModel;->a()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2266911
    new-instance v2, LX/8IM;

    invoke-direct {v2}, LX/8IM;-><init>()V

    new-instance v3, LX/4aM;

    invoke-direct {v3}, LX/4aM;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    .line 2266912
    iput-object v0, v3, LX/4aM;->b:Ljava/lang/String;

    .line 2266913
    move-object v0, v3

    .line 2266914
    invoke-virtual {v0}, LX/4aM;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v0

    .line 2266915
    iput-object v0, v2, LX/8IM;->l:Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    .line 2266916
    move-object v0, v2

    .line 2266917
    invoke-virtual {p0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel$NodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v1

    .line 2266918
    iput-object v1, v0, LX/8IM;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2266919
    move-object v0, v0

    .line 2266920
    invoke-virtual {p0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel$NodeModel;->D()Ljava/lang/String;

    move-result-object v1

    .line 2266921
    iput-object v1, v0, LX/8IM;->w:Ljava/lang/String;

    .line 2266922
    move-object v0, v0

    .line 2266923
    invoke-virtual {p0}, Lcom/facebook/search/protocol/Fb4aGraphSearchPhotoQueryModels$Fb4aGraphSearchPhotoResultsConnectionFragmentModel$EdgesModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 2266924
    iput-object v1, v0, LX/8IM;->g:Ljava/lang/String;

    .line 2266925
    move-object v0, v0

    .line 2266926
    invoke-virtual {v0}, LX/8IM;->a()Lcom/facebook/photos/pandora/protocols/PandoraQueryModels$PandoraMediaModel;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;IZ)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/facebook/photos/pandora/common/data/PandoraInstanceId;",
            "IZ)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/fbservice/service/OperationResult;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2266927
    iget-object v0, p0, LX/Few;->b:LX/0tX;

    .line 2266928
    new-instance v1, LX/9z0;

    invoke-direct {v1}, LX/9z0;-><init>()V

    move-object v2, v1

    .line 2266929
    iget-object v1, p0, LX/Few;->c:LX/0se;

    invoke-virtual {v1, v2}, LX/0se;->a(LX/0gW;)LX/0gW;

    .line 2266930
    check-cast p3, Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;

    .line 2266931
    const-string v3, "query"

    iget-object v1, p0, LX/Few;->d:LX/0QK;

    .line 2266932
    iget-object p5, p3, Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;->a:Ljava/lang/String;

    move-object p5, p5

    .line 2266933
    invoke-interface {v1, p5}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2266934
    const-string v1, "count"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2266935
    const-string v1, "tsid"

    .line 2266936
    iget-object v3, p3, Lcom/facebook/photos/pandora/common/data/GraphSearchPandoraInstanceId;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2266937
    invoke-virtual {v2, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2266938
    const-string v1, "callsite"

    sget-object v3, LX/CyW;->PHOTO_SEARCH:LX/CyW;

    invoke-virtual {v2, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2266939
    const-string v1, "should_fetch_photo_annotations"

    iget-boolean v3, p0, LX/Few;->h:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    .line 2266940
    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2266941
    const-string v1, "before_cursor"

    invoke-virtual {v2, v1, p1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2266942
    :cond_0
    invoke-static {p2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2266943
    const-string v1, "after_cursor"

    invoke-virtual {v2, v1, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2266944
    :cond_1
    move-object v1, v2

    .line 2266945
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v1

    sget-object v2, LX/0zS;->c:LX/0zS;

    invoke-virtual {v1, v2}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v1

    sget-object v2, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v1, v2}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2266946
    iget-object v1, p0, LX/Few;->g:LX/0TF;

    if-eqz v1, :cond_2

    .line 2266947
    iget-object v1, p0, LX/Few;->g:LX/0TF;

    iget-object v2, p0, LX/Few;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2266948
    :cond_2
    iget-object v1, p0, LX/Few;->f:LX/Fev;

    iget-object v2, p0, LX/Few;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
