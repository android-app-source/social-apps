.class public final LX/Ge1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

.field public final synthetic b:LX/Ge4;


# direct methods
.method public constructor <init>(LX/Ge4;Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;)V
    .locals 0

    .prologue
    .line 2374946
    iput-object p1, p0, LX/Ge1;->b:LX/Ge4;

    iput-object p2, p0, LX/Ge1;->a:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2374947
    iget-object v0, p0, LX/Ge1;->a:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;

    move-result-object v0

    .line 2374948
    iget-object v1, p0, LX/Ge1;->b:LX/Ge4;

    iget-object v2, p0, LX/Ge1;->a:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v0

    .line 2374949
    new-instance v3, LX/GeD;

    invoke-direct {v3}, LX/GeD;-><init>()V

    move-object v3, v3

    .line 2374950
    const-string v4, "node_id"

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->s()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "after_param"

    invoke-virtual {v4, v5, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v4

    const-string v5, "profile_pic_swipe_size_param"

    invoke-static {}, LX/0wB;->b()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "profile_image_size"

    invoke-static {}, LX/0wB;->b()I

    move-result p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v4

    const-string v5, "first_n"

    const/4 p0, 0x5

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {v4, v5, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2374951
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    .line 2374952
    iget-object v4, v1, LX/Ge4;->c:LX/0tX;

    invoke-virtual {v4, v3}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v3

    .line 2374953
    new-instance v4, LX/Ge3;

    invoke-direct {v4, v1}, LX/Ge3;-><init>(LX/Ge4;)V

    iget-object v5, v1, LX/Ge4;->d:Ljava/util/concurrent/Executor;

    invoke-static {v3, v4, v5}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v3

    move-object v0, v3

    .line 2374954
    return-object v0
.end method
