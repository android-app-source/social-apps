.class public LX/FiT;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field private final a:LX/0Zb;

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/Cvs;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Uh;

.field public final d:Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;

.field private final e:LX/2SV;

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3AW;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Zb;LX/0Or;LX/0Uh;Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;LX/2SV;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Zb;",
            "LX/0Or",
            "<",
            "LX/Cvs;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;",
            "LX/2SV;",
            "LX/0Ot",
            "<",
            "LX/3AW;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2275348
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2275349
    iput-object p1, p0, LX/FiT;->a:LX/0Zb;

    .line 2275350
    iput-object p2, p0, LX/FiT;->b:LX/0Or;

    .line 2275351
    iput-object p3, p0, LX/FiT;->c:LX/0Uh;

    .line 2275352
    iput-object p4, p0, LX/FiT;->d:Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;

    .line 2275353
    iput-object p5, p0, LX/FiT;->e:LX/2SV;

    .line 2275354
    iput-object p6, p0, LX/FiT;->f:LX/0Ot;

    .line 2275355
    return-void
.end method

.method public static a(LX/0QB;)LX/FiT;
    .locals 10

    .prologue
    .line 2275356
    const-class v1, LX/FiT;

    monitor-enter v1

    .line 2275357
    :try_start_0
    sget-object v0, LX/FiT;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2275358
    sput-object v2, LX/FiT;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2275359
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2275360
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2275361
    new-instance v3, LX/FiT;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v4

    check-cast v4, LX/0Zb;

    const/16 v5, 0x32da

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v6

    check-cast v6, LX/0Uh;

    invoke-static {v0}, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->a(LX/0QB;)Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;

    move-result-object v7

    check-cast v7, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;

    invoke-static {v0}, LX/2SV;->a(LX/0QB;)LX/2SV;

    move-result-object v8

    check-cast v8, LX/2SV;

    const/16 v9, 0x1369

    invoke-static {v0, v9}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v9

    invoke-direct/range {v3 .. v9}, LX/FiT;-><init>(LX/0Zb;LX/0Or;LX/0Uh;Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;LX/2SV;LX/0Ot;)V

    .line 2275362
    move-object v0, v3

    .line 2275363
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2275364
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FiT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2275365
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2275366
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Ljava/lang/String;LX/3zg;)V
    .locals 3
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2275367
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, "null_content_fragment"

    .line 2275368
    :cond_0
    new-instance v0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v1, "titlebar_search_pressed"

    invoke-direct {v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v1, "search_origin_activity_type"

    invoke-virtual {p2}, LX/3zg;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    const-string v1, "origin_fragment"

    invoke-virtual {v0, v1, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v0

    .line 2275369
    iget-object v1, p0, LX/FiT;->a:LX/0Zb;

    invoke-interface {v1, v0}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2275370
    const-class v0, Lcom/facebook/video/videohome/fragment/VideoHomeHomeFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2275371
    :goto_0
    return-void

    .line 2275372
    :cond_1
    iget-object v0, p0, LX/FiT;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3AW;

    .line 2275373
    sget-object v1, LX/04D;->VIDEO_HOME:LX/04D;

    sget-object p0, LX/0JD;->SEARCH_BAR:LX/0JD;

    const/4 p1, 0x0

    invoke-virtual {v0, v1, p0, p1}, LX/3AW;->a(LX/04D;LX/0JD;Ljava/lang/String;)V

    .line 2275374
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/Fragment;LX/3zg;)V
    .locals 1

    .prologue
    .line 2275375
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/FiT;->a(Landroid/support/v4/app/Fragment;LX/3zg;Landroid/os/Bundle;)V

    .line 2275376
    return-void
.end method

.method public final a(Landroid/support/v4/app/Fragment;LX/3zg;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2275377
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0, p2}, LX/FiT;->a(Ljava/lang/String;LX/3zg;)V

    .line 2275378
    iget-object v0, p0, LX/FiT;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Cvs;

    iget-object v1, p0, LX/FiT;->e:LX/2SV;

    invoke-virtual {v1}, LX/2SP;->a()LX/7BE;

    invoke-interface {v0}, LX/Cvs;->a()V

    .line 2275379
    const/4 v3, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2275380
    instance-of p2, p1, LX/0o2;

    .line 2275381
    if-eqz p2, :cond_2

    iget-object v2, p0, LX/FiT;->c:LX/0Uh;

    sget v4, LX/2SU;->e:I

    invoke-virtual {v2, v4}, LX/0Uh;->a(I)LX/03R;

    move-result-object v2

    invoke-virtual {v2, v1}, LX/03R;->asBoolean(Z)Z

    move-result v2

    if-eqz v2, :cond_2

    move v4, v0

    .line 2275382
    :goto_1
    if-eqz p2, :cond_3

    check-cast p1, LX/0o2;

    invoke-interface {p1}, LX/0o2;->j()Lcom/facebook/search/api/GraphSearchQuery;

    move-result-object v2

    .line 2275383
    :goto_2
    if-eqz p2, :cond_4

    invoke-static {v2}, LX/8ht;->c(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result p2

    if-eqz p2, :cond_4

    .line 2275384
    :goto_3
    if-nez v4, :cond_0

    if-eqz v0, :cond_5

    .line 2275385
    :cond_0
    iget-object v0, p0, LX/FiT;->d:Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;

    invoke-virtual {v0, v2, p3}, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->a(Lcom/facebook/search/api/GraphSearchQuery;Landroid/os/Bundle;)V

    .line 2275386
    :goto_4
    return-void

    .line 2275387
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move v4, v1

    .line 2275388
    goto :goto_1

    :cond_3
    move-object v2, v3

    .line 2275389
    goto :goto_2

    :cond_4
    move v0, v1

    .line 2275390
    goto :goto_3

    .line 2275391
    :cond_5
    iget-object v0, p0, LX/FiT;->d:Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;

    invoke-virtual {v0, v3, p3}, Lcom/facebook/search/titlebar/GraphSearchIntentLauncher;->a(Lcom/facebook/search/api/GraphSearchQuery;Landroid/os/Bundle;)V

    goto :goto_4
.end method
