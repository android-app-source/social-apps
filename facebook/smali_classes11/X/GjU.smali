.class public abstract LX/GjU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lcom/facebook/greetingcards/render/RenderCardFragment;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/greetingcards/render/RenderCardFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public b:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 2387560
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2387561
    invoke-virtual {p0}, LX/GjU;->a()Lcom/facebook/greetingcards/render/RenderCardFragment;

    move-result-object v0

    iput-object v0, p0, LX/GjU;->a:Lcom/facebook/greetingcards/render/RenderCardFragment;

    .line 2387562
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/GjU;->b:Landroid/os/Bundle;

    .line 2387563
    iget-object v0, p0, LX/GjU;->b:Landroid/os/Bundle;

    const-string v1, "args_mode"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2387564
    return-void
.end method


# virtual methods
.method public final a(LX/0Px;)LX/GjU;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "LX/GjU;"
        }
    .end annotation

    .prologue
    .line 2387565
    iget-object v0, p0, LX/GjU;->b:Landroid/os/Bundle;

    const-string v1, "args_button"

    invoke-static {p1}, LX/0R9;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 2387566
    return-object p0
.end method

.method public final a(Lcom/facebook/greetingcards/model/GreetingCard;)LX/GjU;
    .locals 1

    .prologue
    .line 2387558
    iget-object v0, p0, LX/GjU;->a:Lcom/facebook/greetingcards/render/RenderCardFragment;

    invoke-virtual {v0, p1}, Lcom/facebook/greetingcards/render/RenderCardFragment;->a(Lcom/facebook/greetingcards/model/GreetingCard;)V

    .line 2387559
    return-object p0
.end method

.method public abstract a()Lcom/facebook/greetingcards/render/RenderCardFragment;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public final b()Lcom/facebook/greetingcards/render/RenderCardFragment;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 2387556
    iget-object v0, p0, LX/GjU;->a:Lcom/facebook/greetingcards/render/RenderCardFragment;

    iget-object v1, p0, LX/GjU;->b:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2387557
    iget-object v0, p0, LX/GjU;->a:Lcom/facebook/greetingcards/render/RenderCardFragment;

    return-object v0
.end method
