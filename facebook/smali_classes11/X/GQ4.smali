.class public final LX/GQ4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/adspayments/activity/AdsPaymentsActivity;

.field public final synthetic b:LX/0P1;

.field public final synthetic c:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;LX/0P1;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 2349688
    iput-object p1, p0, LX/GQ4;->a:Lcom/facebook/adspayments/activity/AdsPaymentsActivity;

    iput-object p2, p0, LX/GQ4;->b:LX/0P1;

    iput-object p3, p0, LX/GQ4;->c:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 2349689
    iget-object v0, p0, LX/GQ4;->a:Lcom/facebook/adspayments/activity/AdsPaymentsActivity;

    const-string v1, "prepay_disclaimer"

    iget-object v2, p0, LX/GQ4;->b:LX/0P1;

    .line 2349690
    iget-object p1, v0, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->D:LX/ADW;

    const-string p2, "payments_action_continue"

    invoke-static {v0, p2, v1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->b(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object p2

    invoke-virtual {p2, v2}, Lcom/facebook/adspayments/analytics/PaymentsLogEvent;->b(Ljava/util/Map;)Lcom/facebook/adspayments/analytics/PaymentsLogEvent;

    move-result-object p2

    invoke-virtual {p1, p2}, LX/ADW;->a(Lcom/facebook/adspayments/analytics/PaymentsLogEvent;)V

    .line 2349691
    iget-object v0, p0, LX/GQ4;->c:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 2349692
    return-void
.end method
