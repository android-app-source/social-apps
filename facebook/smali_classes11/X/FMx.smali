.class public final LX/FMx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/stickers/model/Sticker;

.field public final synthetic b:Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;Lcom/facebook/stickers/model/Sticker;)V
    .locals 0

    .prologue
    .line 2231646
    iput-object p1, p0, LX/FMx;->b:Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;

    iput-object p2, p0, LX/FMx;->a:Lcom/facebook/stickers/model/Sticker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 2231639
    iget-object v0, p0, LX/FMx;->a:Lcom/facebook/stickers/model/Sticker;

    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->g:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 2231640
    iget-object v0, p0, LX/FMx;->b:Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;

    iget-object v0, v0, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8jJ;

    iget-object v1, p0, LX/FMx;->a:Lcom/facebook/stickers/model/Sticker;

    iget-object v1, v1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    sget-object v2, LX/3e1;->PREVIEW:LX/3e1;

    iget-object v3, p0, LX/FMx;->a:Lcom/facebook/stickers/model/Sticker;

    iget-object v3, v3, Lcom/facebook/stickers/model/Sticker;->g:Landroid/net/Uri;

    sget-object v4, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/8jJ;->a(Ljava/lang/String;LX/3e1;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2231641
    :goto_0
    if-eqz v0, :cond_2

    iget-object v1, p0, LX/FMx;->b:Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;

    invoke-static {v1}, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->b$redex0(Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, LX/1v3;->a(Ljava/util/concurrent/Future;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbservice/service/OperationResult;

    :goto_1
    return-object v0

    .line 2231642
    :cond_0
    iget-object v0, p0, LX/FMx;->a:Lcom/facebook/stickers/model/Sticker;

    iget-object v0, v0, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 2231643
    iget-object v0, p0, LX/FMx;->b:Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;

    iget-object v0, v0, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8jJ;

    iget-object v1, p0, LX/FMx;->a:Lcom/facebook/stickers/model/Sticker;

    iget-object v1, v1, Lcom/facebook/stickers/model/Sticker;->a:Ljava/lang/String;

    sget-object v2, LX/3e1;->STATIC:LX/3e1;

    iget-object v3, p0, LX/FMx;->a:Lcom/facebook/stickers/model/Sticker;

    iget-object v3, v3, Lcom/facebook/stickers/model/Sticker;->c:Landroid/net/Uri;

    sget-object v4, Lcom/facebook/messaging/sms/defaultapp/send/MmsStickerAttachmentHelper;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v0, v1, v2, v3, v4}, LX/8jJ;->a(Ljava/lang/String;LX/3e1;Landroid/net/Uri;Lcom/facebook/common/callercontext/CallerContext;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    goto :goto_0

    .line 2231644
    :cond_1
    new-instance v0, LX/EdT;

    const-string v1, "No usable uri for sticker"

    invoke-direct {v0, v1}, LX/EdT;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2231645
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
