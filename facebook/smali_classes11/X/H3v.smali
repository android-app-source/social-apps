.class public final LX/H3v;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H3o;


# instance fields
.field public final synthetic a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;)V
    .locals 0

    .prologue
    .line 2422091
    iput-object p1, p0, LX/H3v;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/H44;)V
    .locals 4

    .prologue
    .line 2422092
    iget-object v0, p0, LX/H3v;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p1, v0}, LX/H43;->a(LX/H44;Landroid/content/res/Resources;)Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    move-result-object v0

    .line 2422093
    if-nez v0, :cond_1

    .line 2422094
    :cond_0
    :goto_0
    return-void

    .line 2422095
    :cond_1
    iget-object v1, p0, LX/H3v;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    iget-object v1, v1, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->k:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    if-eqz v1, :cond_2

    .line 2422096
    iget-object v1, p0, LX/H3v;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    iget-object v1, v1, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->k:Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;

    .line 2422097
    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2422098
    iget-object v2, v1, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2422099
    iget-object p1, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v2, p1

    .line 2422100
    iput-object v0, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    .line 2422101
    invoke-static {v1}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->n(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;)V

    .line 2422102
    iget-object v2, v1, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->r:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2422103
    iget-object p1, v2, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->b:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-object v2, p1

    .line 2422104
    invoke-virtual {v2}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->h()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2422105
    sget-object v2, LX/H3k;->RESULT_LIST_FRAGMENT:LX/H3k;

    invoke-static {v1, v2}, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->a$redex0(Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;LX/H3k;)V

    .line 2422106
    :cond_2
    :goto_1
    iget-object v0, p0, LX/H3v;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->a:LX/H1i;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/H3v;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->m:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    if-eqz v0, :cond_0

    .line 2422107
    iget-object v0, p0, LX/H3v;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    iget-object v0, v0, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->a:LX/H1i;

    iget-object v1, p0, LX/H3v;->a:Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;

    iget-object v1, v1, Lcom/facebook/nearby/v2/intent/NearbyPlacesV2IntentFragment;->m:Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;

    .line 2422108
    iget-object v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesFragmentModel;->a:Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;

    move-object v1, v2

    .line 2422109
    iget-object v2, v1, Lcom/facebook/nearby/v2/logging/NearbyPlacesSession;->c:Ljava/lang/String;

    move-object v1, v2

    .line 2422110
    iget-object v2, v0, LX/H1i;->a:LX/0Zb;

    const-string v3, "search_results_user_selection"

    const/4 p0, 0x1

    invoke-interface {v2, v3, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v2

    .line 2422111
    invoke-virtual {v2}, LX/0oG;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2422112
    const-string v3, "places_recommendations"

    invoke-virtual {v2, v3}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string p0, "mechanism"

    const-string p1, "null_state"

    invoke-virtual {v3, p0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string p0, "event_target"

    const-string p1, "place_category"

    invoke-virtual {v3, p0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string p0, "event_type"

    const-string p1, "click"

    invoke-virtual {v3, p0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v3

    const-string p0, "session_id"

    invoke-virtual {v3, p0, v1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2422113
    invoke-virtual {v2}, LX/0oG;->d()V

    .line 2422114
    :cond_3
    goto :goto_0

    .line 2422115
    :cond_4
    iget-object v2, v1, Lcom/facebook/nearby/v2/NearbyPlacesV2Fragment;->q:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;

    .line 2422116
    iget-object p1, v2, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadFragment;->l:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadEditText;->requestFocus()Z

    .line 2422117
    goto :goto_1
.end method
