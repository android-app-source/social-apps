.class public final LX/Flx;
.super LX/63W;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;)V
    .locals 0

    .prologue
    .line 2281752
    iput-object p1, p0, LX/Flx;->a:Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;

    invoke-direct {p0}, LX/63W;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V
    .locals 10

    .prologue
    .line 2281753
    iget-object v0, p0, LX/Flx;->a:Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->f:LX/Fli;

    invoke-virtual {v0}, LX/Fli;->f()LX/0Rf;

    move-result-object v7

    .line 2281754
    if-eqz v7, :cond_0

    invoke-interface {v7}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2281755
    iget-object v0, p0, LX/Flx;->a:Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->a:LX/B9y;

    iget-object v1, p0, LX/Flx;->a:Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;

    invoke-virtual {v1}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, LX/Flx;->a:Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;

    iget-object v2, v2, Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;->g:Ljava/lang/String;

    iget-object v3, p0, LX/Flx;->a:Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;

    .line 2281756
    iget-object v4, v3, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v4, v4

    .line 2281757
    const-string v5, "fundraiser_title"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v3, v4

    .line 2281758
    iget-object v4, p0, LX/Flx;->a:Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;

    .line 2281759
    iget-object v5, v4, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v5, v5

    .line 2281760
    const-string v6, "owner_profile_picture_url"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    .line 2281761
    const/4 v5, 0x0

    iget-object v6, p0, LX/Flx;->a:Lcom/facebook/socialgood/guestlist/FundraiserMessageGuestsFrameFragment;

    .line 2281762
    iget-object v8, v6, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object v8, v8

    .line 2281763
    const-string v9, "extra_fundraiser_subtitle"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    move-object v6, v8

    .line 2281764
    const-string v8, "fundraiser"

    const/16 v9, 0x71

    invoke-virtual/range {v0 .. v9}, LX/B9y;->a(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/lang/String;I)V

    .line 2281765
    :cond_0
    return-void
.end method
