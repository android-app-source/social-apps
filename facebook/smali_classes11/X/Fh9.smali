.class public final LX/Fh9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/search/suggestions/SuggestionsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/suggestions/SuggestionsFragment;)V
    .locals 0

    .prologue
    .line 2271684
    iput-object p1, p0, LX/Fh9;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/widget/ProgressBar;LX/0g8;LX/2SP;LX/Fhs;LX/Fid;LX/1Qq;LX/Fgp;)V
    .locals 6

    .prologue
    .line 2271685
    iget-object v0, p0, LX/Fh9;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->D(Lcom/facebook/search/suggestions/SuggestionsFragment;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2271686
    :goto_0
    return-void

    .line 2271687
    :cond_0
    :try_start_0
    const-string v0, "SuggestionsFragment.onLoadNullState"

    const v1, -0x416aef9f

    invoke-static {v0, v1}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2271688
    sget-object v0, LX/7BE;->READY:LX/7BE;

    iget-object v1, p0, LX/Fh9;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->L$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;)Z

    move-result v1

    invoke-virtual {p4, v0, v1}, LX/Fhs;->a(LX/7BE;Z)V

    .line 2271689
    invoke-virtual {p3}, LX/2SP;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 2271690
    invoke-virtual {p3}, LX/2SP;->a()LX/7BE;

    move-result-object v1

    .line 2271691
    sget-object v2, LX/7BE;->READY:LX/7BE;

    invoke-virtual {v1, v2}, LX/7BE;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2271692
    sget-object v2, LX/7HZ;->IDLE:LX/7HZ;

    .line 2271693
    iput-object v2, p7, LX/Fgp;->v:LX/7HZ;

    .line 2271694
    :goto_1
    if-eqz p2, :cond_2

    iget-object v2, p0, LX/Fh9;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v2, v2, Lcom/facebook/search/suggestions/SuggestionsFragment;->U:Landroid/view/View;

    if-eqz v2, :cond_2

    iget-object v2, p0, LX/Fh9;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v2}, Lcom/facebook/search/suggestions/SuggestionsFragment;->L$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2271695
    const/16 v1, 0x8

    invoke-interface {p2, v1}, LX/0g8;->a(I)V

    .line 2271696
    iget-object v1, p0, LX/Fh9;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->U:Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2271697
    iget-object v1, p0, LX/Fh9;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v1, v1, Lcom/facebook/search/suggestions/SuggestionsFragment;->v:LX/2Sg;

    .line 2271698
    invoke-static {v1}, LX/2Sg;->k(LX/2Sg;)V

    .line 2271699
    :goto_2
    iget-object v1, p0, LX/Fh9;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    .line 2271700
    iget-object v2, p7, LX/Fgp;->v:LX/7HZ;

    move-object v2, v2

    .line 2271701
    invoke-static {v1, p1, p2, v2}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Landroid/widget/ProgressBar;LX/0g8;LX/7HZ;)V

    .line 2271702
    const/4 v2, 0x0

    invoke-virtual {p5}, LX/Fid;->size()I

    move-result p0

    add-int/lit8 p0, p0, 0x6

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v1

    invoke-static {p0, v1}, Ljava/lang/Math;->min(II)I

    move-result p0

    invoke-virtual {v0, v2, p0}, LX/0Px;->subList(II)LX/0Px;

    move-result-object v2

    .line 2271703
    invoke-virtual {p5, v2}, LX/Fid;->a(Ljava/util/List;)V

    .line 2271704
    invoke-virtual {v2}, LX/0Px;->size()I

    .line 2271705
    invoke-interface {p6}, LX/1Cw;->notifyDataSetChanged()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2271706
    const v0, 0x6462f1cf

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    .line 2271707
    :cond_1
    :try_start_1
    sget-object v2, LX/7HZ;->ACTIVE:LX/7HZ;

    .line 2271708
    iput-object v2, p7, LX/Fgp;->v:LX/7HZ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2271709
    goto :goto_1

    .line 2271710
    :catchall_0
    move-exception v0

    const v1, -0x31a739fb

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0

    .line 2271711
    :cond_2
    :try_start_2
    iget-object v2, p0, LX/Fh9;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v2, v2, Lcom/facebook/search/suggestions/SuggestionsFragment;->v:LX/2Sg;

    .line 2271712
    iget-object v3, v2, LX/2Sg;->i:LX/7BE;

    sget-object v4, LX/7BE;->PARTIAL:LX/7BE;

    if-ne v3, v4, :cond_5

    sget-object v3, LX/7BE;->READY:LX/7BE;

    if-ne v1, v3, :cond_5

    const/4 v3, 0x1

    :goto_3
    move v3, v3

    .line 2271713
    if-nez v3, :cond_4

    .line 2271714
    sget-object v3, LX/2Si;->WARM_START:LX/2Si;

    sget-object v4, LX/Cvr;->TYPEAHEAD_CLEARED:LX/Cvr;

    .line 2271715
    invoke-static {v2, v3}, LX/2Sg;->a(LX/2Sg;LX/2Si;)LX/11o;

    move-result-object v5

    .line 2271716
    invoke-static {v5, v4}, LX/2Sg;->a(LX/11o;LX/Cvr;)V

    .line 2271717
    const-string p3, "pre_fetch"

    invoke-interface {v5, p3}, LX/11o;->f(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_3

    .line 2271718
    const-string p3, "pre_fetch"

    const p4, 0x73f4d126    # 3.87928E31f

    invoke-static {v5, p3, p4}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2271719
    const-string p3, "on_resume"

    invoke-interface {v5, p3}, LX/11o;->f(Ljava/lang/String;)Z

    move-result p3

    if-eqz p3, :cond_3

    .line 2271720
    const-string p3, "on_resume"

    const p4, -0x7f0a3ad7

    invoke-static {v5, p3, p4}, LX/096;->b(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2271721
    :cond_3
    const-string p3, "post_processing"

    invoke-interface {v5, p3}, LX/11o;->f(Ljava/lang/String;)Z

    move-result p3

    if-nez p3, :cond_4

    .line 2271722
    const-string p3, "post_processing"

    const p4, 0x375d8518

    invoke-static {v5, p3, p4}, LX/096;->a(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2271723
    :cond_4
    iput-object v1, v2, LX/2Sg;->i:LX/7BE;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2271724
    goto/16 :goto_2

    :cond_5
    const/4 v3, 0x0

    goto :goto_3
.end method
