.class public LX/GIL;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;",
        "Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

.field public final b:LX/2U3;

.field public c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field private d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

.field public e:Ljava/lang/String;

.field private f:Landroid/content/res/Resources;

.field public g:LX/GDS;

.field private h:Landroid/view/inputmethod/InputMethodManager;

.field public i:LX/GCE;

.field public j:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;

.field public k:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

.field public l:Z

.field public m:Z

.field public n:Lcom/facebook/adinterfaces/model/CreativeAdModel;

.field private o:Landroid/text/Spanned;

.field public final p:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(LX/2U3;LX/GDS;Landroid/view/inputmethod/InputMethodManager;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2336437
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2336438
    new-instance v0, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    const-string v1, "ad_interfaces_get_direction_preview"

    invoke-direct {v0, v1}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, LX/GIL;->a:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    .line 2336439
    new-instance v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;

    invoke-direct {v0, p0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressViewController$1;-><init>(LX/GIL;)V

    iput-object v0, p0, LX/GIL;->p:Ljava/lang/Runnable;

    .line 2336440
    iput-object p1, p0, LX/GIL;->b:LX/2U3;

    .line 2336441
    iput-object p2, p0, LX/GIL;->g:LX/GDS;

    .line 2336442
    iput-object p3, p0, LX/GIL;->h:Landroid/view/inputmethod/InputMethodManager;

    .line 2336443
    return-void
.end method

.method public static a(LX/0QB;)LX/GIL;
    .locals 1

    .prologue
    .line 2336436
    invoke-static {p0}, LX/GIL;->b(LX/0QB;)LX/GIL;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/GIL;
    .locals 4

    .prologue
    .line 2336434
    new-instance v3, LX/GIL;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v0

    check-cast v0, LX/2U3;

    invoke-static {p0}, LX/GDS;->b(LX/0QB;)LX/GDS;

    move-result-object v1

    check-cast v1, LX/GDS;

    invoke-static {p0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v2

    check-cast v2, Landroid/view/inputmethod/InputMethodManager;

    invoke-direct {v3, v0, v1, v2}, LX/GIL;-><init>(LX/2U3;LX/GDS;Landroid/view/inputmethod/InputMethodManager;)V

    .line 2336435
    return-object v3
.end method

.method public static b$redex0(LX/GIL;Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2336422
    iput-boolean p1, p0, LX/GIL;->l:Z

    .line 2336423
    iget-object v2, p0, LX/GIL;->i:LX/GCE;

    sget-object v3, LX/GG8;->ADDRESS:LX/GG8;

    iget-boolean v0, p0, LX/GIL;->l:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/GIL;->m:Z

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, v0}, LX/GCE;->a(LX/GG8;Z)V

    .line 2336424
    iget-boolean v0, p0, LX/GHg;->a:Z

    move v0, v0

    .line 2336425
    if-nez v0, :cond_3

    .line 2336426
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 2336427
    goto :goto_0

    .line 2336428
    :cond_3
    if-nez p1, :cond_4

    .line 2336429
    iget-object v0, p0, LX/GIL;->h:Landroid/view/inputmethod/InputMethodManager;

    iget-object v2, p0, LX/GIL;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->getEditTextToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2336430
    :cond_4
    iget-object v0, p0, LX/GIL;->k:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    if-eqz p1, :cond_5

    :goto_2
    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setVisibility(I)V

    .line 2336431
    iget-boolean v0, p0, LX/GIL;->l:Z

    if-eqz v0, :cond_1

    .line 2336432
    iget-object v0, p0, LX/GIL;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;

    invoke-static {v0}, LX/GMo;->b(Landroid/view/View;)V

    goto :goto_1

    .line 2336433
    :cond_5
    const/16 v1, 0x8

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2336444
    invoke-super {p0}, LX/GHg;->a()V

    .line 2336445
    iget-object v0, p0, LX/GIL;->g:LX/GDS;

    invoke-virtual {v0}, LX/GDS;->a()V

    .line 2336446
    iget-object v0, p0, LX/GIL;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;

    iget-object v1, p0, LX/GIL;->p:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 2336447
    iput-object v2, p0, LX/GIL;->k:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2336448
    iput-object v2, p0, LX/GIL;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;

    .line 2336449
    iput-object v2, p0, LX/GIL;->o:Landroid/text/Spanned;

    .line 2336450
    iput-object v2, p0, LX/GIL;->i:LX/GCE;

    .line 2336451
    iput-object v2, p0, LX/GIL;->f:Landroid/content/res/Resources;

    .line 2336452
    iput-object v2, p0, LX/GIL;->h:Landroid/view/inputmethod/InputMethodManager;

    .line 2336453
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2336419
    invoke-super {p0, p1}, LX/GHg;->a(Landroid/os/Bundle;)V

    .line 2336420
    const-string v0, "address_extra"

    iget-object v1, p0, LX/GIL;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2336421
    return-void
.end method

.method public final a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 8
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2336395
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2336396
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2336397
    invoke-static {p2, v1}, LX/GMo;->a(Landroid/view/ViewGroup;I)V

    .line 2336398
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2336399
    iput-object v0, p0, LX/GIL;->i:LX/GCE;

    .line 2336400
    iput-object p1, p0, LX/GIL;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;

    .line 2336401
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, LX/GIL;->f:Landroid/content/res/Resources;

    .line 2336402
    iput-object p2, p0, LX/GIL;->k:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    .line 2336403
    iget-object v0, p0, LX/GIL;->i:LX/GCE;

    new-instance v3, LX/GIJ;

    invoke-direct {v3, p0}, LX/GIJ;-><init>(LX/GIL;)V

    invoke-virtual {v0, v3}, LX/GCE;->a(LX/8wQ;)V

    .line 2336404
    iget-object v0, p0, LX/GIL;->n:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2336405
    iget-object v3, v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-object v0, v3

    .line 2336406
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_DIRECTIONS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_0
    invoke-static {p0, v0}, LX/GIL;->b$redex0(LX/GIL;Z)V

    .line 2336407
    iget-object v0, p0, LX/GIL;->d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    if-eqz v0, :cond_3

    .line 2336408
    iput-boolean v1, p0, LX/GIL;->m:Z

    .line 2336409
    iget-object v0, p0, LX/GIL;->d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->setAddressString(Ljava/lang/String;)V

    .line 2336410
    iget-object v0, p0, LX/GIL;->a:Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    invoke-virtual {v0}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a()Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v0

    iget-object v3, p0, LX/GIL;->d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->ir_()D

    move-result-wide v4

    iget-object v3, p0, LX/GIL;->d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;->j()D

    move-result-wide v6

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(DD)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v0

    const/16 v3, 0xd

    invoke-virtual {v0, v3}, Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;->a(I)Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->setMapOptions(Lcom/facebook/android/maps/StaticMapView$StaticMapOptions;)V

    .line 2336411
    iget-object v0, p0, LX/GIL;->k:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    .line 2336412
    :goto_1
    iget-object v0, p0, LX/GIL;->i:LX/GCE;

    sget-object v3, LX/GG8;->ADDRESS:LX/GG8;

    iget-boolean v4, p0, LX/GIL;->l:Z

    if-eqz v4, :cond_0

    iget-boolean v4, p0, LX/GIL;->m:Z

    if-eqz v4, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    invoke-virtual {v0, v3, v2}, LX/GCE;->a(LX/GG8;Z)V

    .line 2336413
    new-instance v0, LX/GIK;

    invoke-direct {v0, p0}, LX/GIK;-><init>(LX/GIL;)V

    invoke-virtual {p1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->setOnAddressChangeListener(Landroid/text/TextWatcher;)V

    .line 2336414
    return-void

    :cond_2
    move v0, v2

    .line 2336415
    goto :goto_0

    .line 2336416
    :cond_3
    iput-boolean v2, p0, LX/GIL;->m:Z

    .line 2336417
    invoke-virtual {p1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->setMapEnabled(Z)V

    .line 2336418
    iget-object v0, p0, LX/GIL;->k:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    invoke-virtual {p0}, LX/GIL;->b()Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setFooterSpannableText(Landroid/text/Spanned;)V

    goto :goto_1
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 1

    .prologue
    .line 2336381
    check-cast p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336382
    iput-object p1, p0, LX/GIL;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336383
    iget-object v0, p0, LX/GIL;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336384
    iget-object p1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->m:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v0, p1

    .line 2336385
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GIL;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336386
    iget-object p1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->m:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v0, p1

    .line 2336387
    :goto_0
    iput-object v0, p0, LX/GIL;->d:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    .line 2336388
    iget-object v0, p0, LX/GIL;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336389
    iget-object p1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v0, p1

    .line 2336390
    iput-object v0, p0, LX/GIL;->n:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    .line 2336391
    return-void

    .line 2336392
    :cond_0
    iget-object v0, p0, LX/GIL;->c:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2336393
    iget-object p1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->h:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$GeoLocationModel;

    move-object v0, p1

    .line 2336394
    goto :goto_0
.end method

.method public final b()Landroid/text/Spanned;
    .locals 2

    .prologue
    .line 2336378
    iget-object v0, p0, LX/GIL;->o:Landroid/text/Spanned;

    if-nez v0, :cond_0

    .line 2336379
    iget-object v0, p0, LX/GIL;->f:Landroid/content/res/Resources;

    const v1, 0x7f080b35

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    iput-object v0, p0, LX/GIL;->o:Landroid/text/Spanned;

    .line 2336380
    :cond_0
    iget-object v0, p0, LX/GIL;->o:Landroid/text/Spanned;

    return-object v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2336369
    invoke-super {p0, p1}, LX/GHg;->b(Landroid/os/Bundle;)V

    .line 2336370
    if-nez p1, :cond_1

    .line 2336371
    :cond_0
    :goto_0
    return-void

    .line 2336372
    :cond_1
    const-string v0, "address_extra"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GIL;->e:Ljava/lang/String;

    .line 2336373
    iget-object v0, p0, LX/GIL;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2336374
    iget-boolean v0, p0, LX/GHg;->a:Z

    move v0, v0

    .line 2336375
    if-eqz v0, :cond_0

    .line 2336376
    iget-object v0, p0, LX/GIL;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;

    iget-object v1, p0, LX/GIL;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->setAddressString(Ljava/lang/String;)V

    .line 2336377
    iget-object v0, p0, LX/GIL;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;

    iget-object v1, p0, LX/GIL;->p:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesAddressEditView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
