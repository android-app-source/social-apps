.class public LX/H1c;
.super LX/5Jl;
.source ""

# interfaces
.implements LX/CNT;


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/CNb;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/CNc;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/CNc;)V
    .locals 2

    .prologue
    .line 2415604
    new-instance v0, LX/1P1;

    invoke-direct {v0, p1}, LX/1P1;-><init>(Landroid/content/Context;)V

    new-instance v1, LX/5K6;

    invoke-direct {v1}, LX/5K6;-><init>()V

    invoke-direct {p0, p1, v0, v1}, LX/5Jl;-><init>(Landroid/content/Context;LX/1P1;LX/5K5;)V

    .line 2415605
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2415606
    iput-object v0, p0, LX/H1c;->a:Ljava/util/List;

    .line 2415607
    iput-object p2, p0, LX/H1c;->b:LX/CNc;

    .line 2415608
    iget-object v0, p0, LX/H1c;->b:LX/CNc;

    iget-object v0, v0, LX/CNc;->b:LX/CNS;

    iput-object p0, v0, LX/CNS;->b:LX/CNT;

    .line 2415609
    return-void
.end method


# virtual methods
.method public final a(LX/1De;I)LX/1X1;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            "I)",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2415610
    iget-object v0, p0, LX/H1c;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CNb;

    iget-object v1, p0, LX/H1c;->b:LX/CNc;

    invoke-static {v0, v1, p1}, LX/CNd;->a(LX/CNb;LX/CNc;LX/1De;)LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/0Px;LX/0Px;LX/0Px;LX/0Px;LX/0Px;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/0Px",
            "<",
            "LX/CNb;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "LX/0Px",
            "<",
            "LX/CNb;",
            ">;",
            "LX/0Px",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2415611
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2415612
    iget-object v0, p0, LX/H1c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v2

    sub-int v5, v0, v2

    move v0, v1

    move v2, v1

    move v3, v1

    .line 2415613
    :goto_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v6, v5, :cond_3

    .line 2415614
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p1, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2415615
    add-int/lit8 v3, v3, 0x1

    .line 2415616
    goto :goto_0

    .line 2415617
    :cond_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p3, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 2415618
    invoke-virtual {p2, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2415619
    add-int/lit8 v2, v2, 0x1

    .line 2415620
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2415621
    :cond_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p5, v6}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 2415622
    invoke-virtual {p4, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2415623
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2415624
    :cond_2
    iget-object v6, p0, LX/H1c;->a:Ljava/util/List;

    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 2415625
    :cond_3
    iput-object v4, p0, LX/H1c;->a:Ljava/util/List;

    .line 2415626
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_2
    if-ge v2, v3, :cond_4

    invoke-virtual {p1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2415627
    invoke-virtual {p0, v0}, LX/3mY;->c(I)V

    .line 2415628
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 2415629
    :cond_4
    invoke-virtual {p3}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_3
    if-ge v2, v3, :cond_5

    invoke-virtual {p3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2415630
    invoke-virtual {p0, v0}, LX/3mY;->u_(I)V

    .line 2415631
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 2415632
    :cond_5
    invoke-virtual {p5}, LX/0Px;->size()I

    move-result v2

    :goto_4
    if-ge v1, v2, :cond_6

    invoke-virtual {p5, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2415633
    invoke-virtual {p0, v0}, LX/3mY;->b_(I)V

    .line 2415634
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 2415635
    :cond_6
    return-void
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2415636
    iget-object v0, p0, LX/H1c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2415637
    const/4 v0, 0x1

    return v0
.end method
