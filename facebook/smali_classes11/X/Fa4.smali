.class public final LX/Fa4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/0Px",
        "<",
        "LX/CwV;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0Px;

.field public final synthetic b:LX/Fa5;


# direct methods
.method public constructor <init>(LX/Fa5;LX/0Px;)V
    .locals 0

    .prologue
    .line 2258313
    iput-object p1, p0, LX/Fa4;->b:LX/Fa5;

    iput-object p2, p0, LX/Fa4;->a:LX/0Px;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2258322
    iget-object v0, p0, LX/Fa4;->b:LX/Fa5;

    iget-object v0, v0, LX/Fa5;->e:LX/03V;

    sget-object v1, LX/3Ql;->FETCH_NULL_STATE_SUBTOPIC_FAIL:LX/3Ql;

    invoke-virtual {v1}, LX/3Ql;->name()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Fail to fetch null state subtopics"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2258323
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2258324
    move-object v1, v1

    .line 2258325
    const/4 v2, 0x0

    .line 2258326
    iput-boolean v2, v1, LX/0VK;->d:Z

    .line 2258327
    move-object v1, v1

    .line 2258328
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2258329
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2258314
    check-cast p1, LX/0Px;

    .line 2258315
    if-nez p1, :cond_0

    .line 2258316
    iget-object v0, p0, LX/Fa4;->b:LX/Fa5;

    iget-object v0, v0, LX/Fa5;->e:LX/03V;

    sget-object v1, LX/3Ql;->FETCH_NULL_STATE_SUBTOPIC_FAIL:LX/3Ql;

    invoke-virtual {v1}, LX/3Ql;->name()Ljava/lang/String;

    move-result-object v1

    const-string v2, "The result of fetching null state subtopic is null"

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    const/4 v2, 0x0

    .line 2258317
    iput-boolean v2, v1, LX/0VK;->d:Z

    .line 2258318
    move-object v1, v1

    .line 2258319
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2258320
    :goto_0
    return-void

    .line 2258321
    :cond_0
    iget-object v0, p0, LX/Fa4;->b:LX/Fa5;

    iget-object v0, v0, LX/Fa5;->b:LX/Fa6;

    iget-object v1, p0, LX/Fa4;->a:LX/0Px;

    invoke-virtual {v0, v1, p1}, LX/Fa6;->a(LX/0Px;LX/0Px;)V

    goto :goto_0
.end method
