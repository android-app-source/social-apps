.class public final LX/FdK;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;)V
    .locals 0

    .prologue
    .line 2263401
    iput-object p1, p0, LX/FdK;->a:Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 2263402
    if-ltz p3, :cond_0

    iget-object v0, p0, LX/FdK;->a:Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;

    iget-object v0, v0, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->i:LX/Fcx;

    invoke-virtual {v0}, LX/Fcx;->getCount()I

    move-result v0

    if-le p3, v0, :cond_1

    .line 2263403
    :cond_0
    iget-object v0, p0, LX/FdK;->a:Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    .line 2263404
    :goto_0
    return-void

    .line 2263405
    :cond_1
    iget-object v0, p0, LX/FdK;->a:Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;

    iget-object v1, p0, LX/FdK;->a:Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;

    iget-object v1, v1, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->i:LX/Fcx;

    invoke-virtual {v1, p3}, LX/Fcx;->a(I)Lcom/facebook/search/results/protocol/filters/FilterValue;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;->a$redex0(Lcom/facebook/search/results/filters/ui/SearchFilterTypeaheadFragment;Lcom/facebook/search/results/protocol/filters/FilterValue;)V

    goto :goto_0
.end method
