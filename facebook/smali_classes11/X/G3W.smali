.class public final LX/G3W;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultBigProfilePictureFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;)V
    .locals 0

    .prologue
    .line 2315706
    iput-object p1, p0, LX/G3W;->a:Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2315715
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2315707
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2315708
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2315709
    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultBigProfilePictureFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultBigProfilePictureFieldsModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 2315710
    :goto_0
    iget-object v1, p0, LX/G3W;->a:Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;

    if-nez v0, :cond_0

    const-string v0, ""

    .line 2315711
    :cond_0
    iget-object p0, v1, Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;->m:Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;

    invoke-virtual {p0, v0}, Lcom/facebook/timeline/refresher/ProfileNuxRefresherHeaderView;->setUpProfilePicture(Ljava/lang/String;)V

    .line 2315712
    return-void

    .line 2315713
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2315714
    check-cast v0, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultBigProfilePictureFieldsModel;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultBigProfilePictureFieldsModel;->a()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
