.class public LX/Fbn;
.super LX/FbY;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FbY",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsPulseStoryUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Fbn;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2261065
    invoke-direct {p0}, LX/FbY;-><init>()V

    .line 2261066
    return-void
.end method

.method public static a(LX/0QB;)LX/Fbn;
    .locals 3

    .prologue
    .line 2261067
    sget-object v0, LX/Fbn;->a:LX/Fbn;

    if-nez v0, :cond_1

    .line 2261068
    const-class v1, LX/Fbn;

    monitor-enter v1

    .line 2261069
    :try_start_0
    sget-object v0, LX/Fbn;->a:LX/Fbn;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2261070
    if-eqz v2, :cond_0

    .line 2261071
    :try_start_1
    new-instance v0, LX/Fbn;

    invoke-direct {v0}, LX/Fbn;-><init>()V

    .line 2261072
    move-object v0, v0

    .line 2261073
    sput-object v0, LX/Fbn;->a:LX/Fbn;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2261074
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2261075
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2261076
    :cond_1
    sget-object v0, LX/Fbn;->a:LX/Fbn;

    return-object v0

    .line 2261077
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2261078
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261079
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->fM_()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->fM_()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    move-object v1, v0

    .line 2261080
    :goto_0
    if-nez v1, :cond_1

    .line 2261081
    const/4 v0, 0x0

    .line 2261082
    :goto_1
    return-object v0

    .line 2261083
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->b()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 2261084
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->fN_()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;

    move-result-object v0

    invoke-static {v0}, LX/A0T;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;)Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v0

    .line 2261085
    if-eqz v0, :cond_2

    .line 2261086
    invoke-static {v1, v0}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;)V

    .line 2261087
    :cond_2
    new-instance v0, Lcom/facebook/search/results/model/unit/SearchResultsPulseStoryUnit;

    invoke-virtual {p2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/search/results/model/unit/SearchResultsPulseStoryUnit;-><init>(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    goto :goto_1
.end method
