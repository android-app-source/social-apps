.class public final LX/GAg;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static volatile a:LX/GAg;


# instance fields
.field public final b:LX/0Xw;

.field public final c:LX/GAf;

.field public d:Lcom/facebook/Profile;


# direct methods
.method private constructor <init>(LX/0Xw;LX/GAf;)V
    .locals 1

    .prologue
    .line 2326023
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2326024
    const-string v0, "localBroadcastManager"

    invoke-static {p1, v0}, LX/Gsd;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2326025
    const-string v0, "profileCache"

    invoke-static {p2, v0}, LX/Gsd;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2326026
    iput-object p1, p0, LX/GAg;->b:LX/0Xw;

    .line 2326027
    iput-object p2, p0, LX/GAg;->c:LX/GAf;

    .line 2326028
    return-void
.end method

.method public static a()LX/GAg;
    .locals 4

    .prologue
    .line 2326029
    sget-object v0, LX/GAg;->a:LX/GAg;

    if-nez v0, :cond_1

    .line 2326030
    const-class v1, LX/GAg;

    monitor-enter v1

    .line 2326031
    :try_start_0
    sget-object v0, LX/GAg;->a:LX/GAg;

    if-nez v0, :cond_0

    .line 2326032
    invoke-static {}, LX/GAK;->f()Landroid/content/Context;

    move-result-object v0

    .line 2326033
    invoke-static {v0}, LX/0Xw;->a(Landroid/content/Context;)LX/0Xw;

    move-result-object v0

    .line 2326034
    new-instance v2, LX/GAg;

    new-instance v3, LX/GAf;

    invoke-direct {v3}, LX/GAf;-><init>()V

    invoke-direct {v2, v0, v3}, LX/GAg;-><init>(LX/0Xw;LX/GAf;)V

    sput-object v2, LX/GAg;->a:LX/GAg;

    .line 2326035
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2326036
    :cond_1
    sget-object v0, LX/GAg;->a:LX/GAg;

    return-object v0

    .line 2326037
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static a(LX/GAg;Lcom/facebook/Profile;Z)V
    .locals 4

    .prologue
    .line 2326038
    iget-object v0, p0, LX/GAg;->d:Lcom/facebook/Profile;

    .line 2326039
    iput-object p1, p0, LX/GAg;->d:Lcom/facebook/Profile;

    .line 2326040
    if-eqz p2, :cond_1

    .line 2326041
    if-eqz p1, :cond_3

    .line 2326042
    iget-object v1, p0, LX/GAg;->c:LX/GAf;

    .line 2326043
    const-string v2, "profile"

    invoke-static {p1, v2}, LX/Gsd;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2326044
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 2326045
    :try_start_0
    const-string v3, "id"

    iget-object p2, p1, Lcom/facebook/Profile;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2326046
    const-string v3, "first_name"

    iget-object p2, p1, Lcom/facebook/Profile;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2326047
    const-string v3, "middle_name"

    iget-object p2, p1, Lcom/facebook/Profile;->c:Ljava/lang/String;

    invoke-virtual {v2, v3, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2326048
    const-string v3, "last_name"

    iget-object p2, p1, Lcom/facebook/Profile;->d:Ljava/lang/String;

    invoke-virtual {v2, v3, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2326049
    const-string v3, "name"

    iget-object p2, p1, Lcom/facebook/Profile;->e:Ljava/lang/String;

    invoke-virtual {v2, v3, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2326050
    iget-object v3, p1, Lcom/facebook/Profile;->f:Landroid/net/Uri;

    if-eqz v3, :cond_0

    .line 2326051
    const-string v3, "link_uri"

    iget-object p2, p1, Lcom/facebook/Profile;->f:Landroid/net/Uri;

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, v3, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2326052
    :cond_0
    :goto_0
    move-object v2, v2

    .line 2326053
    if-eqz v2, :cond_1

    .line 2326054
    iget-object v3, v1, LX/GAf;->a:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string p2, "com.facebook.ProfileManager.CachedProfile"

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, p2, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 2326055
    :cond_1
    :goto_1
    invoke-static {v0, p1}, LX/Gsc;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2326056
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.facebook.sdk.ACTION_CURRENT_PROFILE_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2326057
    const-string v2, "com.facebook.sdk.EXTRA_OLD_PROFILE"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2326058
    const-string v2, "com.facebook.sdk.EXTRA_NEW_PROFILE"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2326059
    iget-object v2, p0, LX/GAg;->b:LX/0Xw;

    invoke-virtual {v2, v1}, LX/0Xw;->a(Landroid/content/Intent;)Z

    .line 2326060
    :cond_2
    return-void

    .line 2326061
    :cond_3
    iget-object v1, p0, LX/GAg;->c:LX/GAf;

    .line 2326062
    iget-object v2, v1, LX/GAf;->a:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "com.facebook.ProfileManager.CachedProfile"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 2326063
    goto :goto_1

    .line 2326064
    :catch_0
    const/4 v2, 0x0

    goto :goto_0
.end method
