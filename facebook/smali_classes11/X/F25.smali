.class public LX/F25;
.super LX/1SX;
.source ""


# static fields
.field public static final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public static final c:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final d:LX/1Sp;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2192824
    new-instance v0, LX/F1w;

    invoke-direct {v0}, LX/F1w;-><init>()V

    sput-object v0, LX/F25;->a:LX/0Or;

    .line 2192825
    new-instance v0, LX/F1x;

    invoke-direct {v0}, LX/F1x;-><init>()V

    sput-object v0, LX/F25;->b:LX/0Or;

    .line 2192826
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->UNTAG:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->DONT_LIKE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;->HIDE_FROM_TIMELINE:Lcom/facebook/graphql/enums/GraphQLNegativeFeedbackActionType;

    invoke-static {v0, v1, v2}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/F25;->c:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Zb;LX/17Q;LX/0Or;LX/0SG;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/1Qj;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0wL;)V
    .locals 41
    .param p23    # LX/1Qj;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/1Kf;",
            "LX/1Ck;",
            "LX/0Ot",
            "<",
            "LX/9LP;",
            ">;",
            "LX/0lC;",
            "LX/1Sa;",
            "LX/1Sj;",
            "LX/0Ot",
            "<",
            "LX/79m;",
            ">;",
            "LX/16H;",
            "LX/0Or",
            "<",
            "LX/1dv;",
            ">;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0bH;",
            "LX/0Zb;",
            "LX/17Q;",
            "LX/0Or",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0SG;",
            "LX/0Or",
            "<",
            "LX/8Q3;",
            ">;",
            "LX/0Or",
            "<",
            "LX/BNQ;",
            ">;",
            "LX/14w;",
            "LX/0ad;",
            "LX/0Or",
            "<",
            "LX/3Af;",
            ">;",
            "LX/1Qj;",
            "LX/0qn;",
            "Lcom/facebook/mobileconfig/factory/MobileConfigFactory;",
            "LX/0Ot",
            "<",
            "LX/9yI;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0gt;",
            ">;",
            "LX/1Sl;",
            "LX/0tX;",
            "LX/1Sm;",
            "LX/0wL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2192816
    sget-object v15, LX/F25;->a:LX/0Or;

    sget-object v16, LX/F25;->b:LX/0Or;

    sget-object v17, LX/F25;->a:LX/0Or;

    const/16 v20, 0x0

    sget-object v24, LX/F25;->a:LX/0Or;

    const/16 v37, 0x0

    const/16 v38, 0x0

    const/16 v39, 0x0

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p11

    move-object/from16 v13, p12

    move-object/from16 v14, p13

    move-object/from16 v18, p14

    move-object/from16 v19, p15

    move-object/from16 v21, p16

    move-object/from16 v22, p17

    move-object/from16 v23, p18

    move-object/from16 v25, p19

    move-object/from16 v26, p20

    move-object/from16 v27, p21

    move-object/from16 v28, p22

    move-object/from16 v29, p23

    move-object/from16 v30, p24

    move-object/from16 v31, p25

    move-object/from16 v32, p26

    move-object/from16 v33, p27

    move-object/from16 v34, p28

    move-object/from16 v35, p29

    move-object/from16 v36, p30

    move-object/from16 v40, p31

    invoke-direct/range {v1 .. v40}, LX/1SX;-><init>(LX/0Or;LX/0Or;LX/1Kf;LX/1Ck;LX/0Ot;LX/0lC;LX/1Sa;LX/1Sj;LX/0Ot;LX/16H;LX/0Or;LX/0Sh;LX/0bH;LX/0Or;LX/0Or;LX/0Or;LX/0Zb;LX/17Q;LX/0Or;LX/0Or;LX/0SG;LX/0Or;LX/0Or;LX/0Or;LX/14w;LX/0ad;LX/0Or;LX/1Pf;LX/0qn;LX/0W3;LX/0Ot;LX/0Or;LX/1Sl;LX/0tX;LX/1Sm;LX/0Ot;LX/0Ot;LX/0Ot;LX/0wL;)V

    .line 2192817
    new-instance v1, LX/F1y;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, LX/F1y;-><init>(LX/F25;)V

    move-object/from16 v0, p0

    iput-object v1, v0, LX/F25;->d:LX/1Sp;

    .line 2192818
    const-string v1, "goodwill_throwback"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, LX/1SX;->b(Ljava/lang/String;)V

    .line 2192819
    const-string v1, "otd_permalink"

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, LX/1SX;->a(Ljava/lang/String;)V

    .line 2192820
    move-object/from16 v0, p0

    iget-object v1, v0, LX/F25;->d:LX/1Sp;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, LX/1SX;->a(LX/1Sp;)V

    .line 2192821
    return-void
.end method


# virtual methods
.method public final d(Lcom/facebook/graphql/model/FeedUnit;)LX/1wH;
    .locals 1

    .prologue
    .line 2192822
    new-instance v0, LX/F24;

    invoke-direct {v0, p0}, LX/F24;-><init>(LX/F25;)V

    return-object v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2192823
    const/4 v0, 0x1

    return v0
.end method
