.class public LX/GzP;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static a:LX/00q;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2411817
    new-instance v0, LX/00q;

    invoke-direct {v0}, LX/00q;-><init>()V

    sput-object v0, LX/GzP;->a:LX/00q;

    .line 2411818
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2411819
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()V
    .locals 6

    .prologue
    .line 2411820
    invoke-static {}, Lcom/facebook/loom/provider/NativeEventProvider;->b()V

    .line 2411821
    sget-object v0, LX/GzP;->a:LX/00q;

    invoke-virtual {v0}, LX/00q;->n()V

    .line 2411822
    const v0, 0x8d0003

    sget-object v1, LX/GzP;->a:LX/00q;

    .line 2411823
    iget-wide v4, v1, LX/00q;->h:J

    move-wide v2, v4

    .line 2411824
    invoke-static {v0, v2, v3}, LX/GzP;->a(IJ)V

    .line 2411825
    const v0, 0x8d0004

    sget-object v1, LX/GzP;->a:LX/00q;

    .line 2411826
    iget-wide v4, v1, LX/00q;->j:J

    move-wide v2, v4

    .line 2411827
    invoke-static {v0, v2, v3}, LX/GzP;->a(IJ)V

    .line 2411828
    const v0, 0x8d0007

    sget-object v1, LX/GzP;->a:LX/00q;

    invoke-virtual {v1}, LX/00q;->j()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, LX/GzP;->a(IJ)V

    .line 2411829
    const v0, 0x8d0008

    sget-object v1, LX/GzP;->a:LX/00q;

    invoke-virtual {v1}, LX/00q;->k()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, LX/GzP;->a(IJ)V

    .line 2411830
    const v0, 0x8d0011

    invoke-static {}, Landroid/os/Debug;->getGlobalAllocCount()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, LX/GzP;->a(IJ)V

    .line 2411831
    const v0, 0x8d0012

    invoke-static {}, Landroid/os/Debug;->getGlobalAllocSize()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, LX/GzP;->a(IJ)V

    .line 2411832
    const v0, 0x8d0013

    invoke-static {}, Landroid/os/Debug;->getGlobalGcInvocationCount()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, LX/GzP;->a(IJ)V

    .line 2411833
    return-void
.end method

.method public static a(IJ)V
    .locals 3

    .prologue
    .line 2411834
    const/16 v0, 0x40

    const/16 v1, 0x33

    invoke-static {v0, v1, p0, p1, p2}, Lcom/facebook/loom/logger/Logger;->a(IIIJ)I

    .line 2411835
    return-void
.end method
