.class public LX/FjF;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/FjF",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2276560
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2276561
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/FjF;->b:LX/0Zi;

    .line 2276562
    iput-object p1, p0, LX/FjF;->a:LX/0Ot;

    .line 2276563
    return-void
.end method

.method public static a(LX/0QB;)LX/FjF;
    .locals 4

    .prologue
    .line 2276517
    const-class v1, LX/FjF;

    monitor-enter v1

    .line 2276518
    :try_start_0
    sget-object v0, LX/FjF;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2276519
    sput-object v2, LX/FjF;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2276520
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2276521
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2276522
    new-instance v3, LX/FjF;

    const/16 p0, 0x3506

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/FjF;-><init>(LX/0Ot;)V

    .line 2276523
    move-object v0, v3

    .line 2276524
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2276525
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FjF;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2276526
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2276527
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 10

    .prologue
    .line 2276530
    check-cast p2, LX/FjE;

    .line 2276531
    iget-object v0, p0, LX/FjF;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;

    iget-object v1, p2, LX/FjE;->a:LX/1Pp;

    iget-object v2, p2, LX/FjE;->b:Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;

    .line 2276532
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v3

    const/4 v4, 0x1

    invoke-interface {v3, v4}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v3

    const/4 v7, 0x2

    .line 2276533
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x1

    invoke-interface {v4, v5}, LX/1Dh;->S(I)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x6

    const v6, 0x7f0b0060

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x7

    const v6, 0x7f0b0062

    invoke-interface {v4, v5, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/39p;->a(LX/1De;)LX/39s;

    move-result-object v5

    sget-object v6, LX/1Wk;->NEWSFEED_FULLBLEED_SHADOW:LX/1Wk;

    invoke-virtual {v5, v6}, LX/39s;->a(LX/1Wk;)LX/39s;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->c(LX/1n6;)LX/1Dh;

    move-result-object v4

    .line 2276534
    iget-object v5, v2, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->k:LX/0Px;

    move-object v5, v5

    .line 2276535
    iget-object v6, v2, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->e:Landroid/net/Uri;

    move-object v6, v6

    .line 2276536
    const/4 p2, 0x5

    .line 2276537
    iget-object v8, v0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;->b:LX/0ad;

    sget-short v9, LX/100;->bO:S

    const/4 p0, 0x0

    invoke-interface {v8, v9, p0}, LX/0ad;->a(SZ)Z

    move-result v8

    if-nez v8, :cond_1

    .line 2276538
    if-eqz v5, :cond_0

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 2276539
    :cond_0
    const/4 v8, 0x0

    .line 2276540
    :goto_0
    move v8, v8

    .line 2276541
    if-eqz v8, :cond_3

    .line 2276542
    :cond_1
    const/4 v8, 0x0

    .line 2276543
    :goto_1
    move-object v5, v8

    .line 2276544
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    const v6, 0x7f0e0977

    invoke-static {p1, v5, v6}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v5

    .line 2276545
    iget-object v6, v2, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->b:Ljava/lang/String;

    move-object v6, v6

    .line 2276546
    invoke-virtual {v5, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    invoke-interface {v5, v7}, LX/1Di;->b(I)LX/1Di;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-interface {v5, v6}, LX/1Di;->a(F)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    .line 2276547
    iget v5, v2, Lcom/facebook/search/model/NullStateSuggestionTypeaheadUnit;->i:I

    move v5, v5

    .line 2276548
    const/4 v9, 0x0

    .line 2276549
    if-lez v5, :cond_2

    iget-object v6, v0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;->b:LX/0ad;

    sget-short v7, LX/100;->cd:S

    invoke-interface {v6, v7, v9}, LX/0ad;->a(SZ)Z

    move-result v6

    if-nez v6, :cond_6

    .line 2276550
    :cond_2
    const/4 v6, 0x0

    .line 2276551
    :goto_2
    move-object v5, v6

    .line 2276552
    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    move-object v4, v4

    .line 2276553
    invoke-interface {v3, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v3

    .line 2276554
    invoke-interface {v3}, LX/1Di;->k()LX/1Dg;

    move-result-object v3

    move-object v0, v3

    .line 2276555
    return-object v0

    :cond_3
    if-nez v6, :cond_4

    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v9

    iget-object v8, v0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;->c:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/1vg;

    invoke-virtual {v8, p1}, LX/1vg;->a(LX/1De;)LX/2xv;

    move-result-object v8

    const p0, -0x6f6b64

    invoke-virtual {v8, p0}, LX/2xv;->i(I)LX/2xv;

    move-result-object v8

    const p0, 0x7f02091d

    invoke-virtual {v8, p0}, LX/2xv;->h(I)LX/2xv;

    move-result-object v8

    invoke-virtual {v9, v8}, LX/1o5;->a(LX/1n6;)LX/1o5;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    const/4 v9, 0x1

    const/16 p0, 0xc

    invoke-interface {v8, v9, p0}, LX/1Di;->d(II)LX/1Di;

    move-result-object v8

    const/4 v9, 0x3

    const/16 p0, 0x8

    invoke-interface {v8, v9, p0}, LX/1Di;->d(II)LX/1Di;

    move-result-object v8

    const/4 v9, 0x4

    const/16 p0, 0xa

    invoke-interface {v8, v9, p0}, LX/1Di;->d(II)LX/1Di;

    move-result-object v8

    const/16 v9, 0x16

    invoke-interface {v8, p2, v9}, LX/1Di;->d(II)LX/1Di;

    move-result-object v8

    goto/16 :goto_1

    :cond_4
    iget-object v8, v0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;->d:LX/0Ot;

    invoke-interface {v8}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, LX/1nu;

    invoke-virtual {v8, p1}, LX/1nu;->c(LX/1De;)LX/1nw;

    move-result-object v8

    invoke-virtual {v8, v1}, LX/1nw;->a(LX/1Pp;)LX/1nw;

    move-result-object v8

    invoke-virtual {v8, v6}, LX/1nw;->a(Landroid/net/Uri;)LX/1nw;

    move-result-object v8

    sget-object v9, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v8, v9}, LX/1nw;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1nw;

    move-result-object v8

    invoke-virtual {v8}, LX/1X5;->c()LX/1Di;

    move-result-object v8

    const v9, 0x7f0b008a

    invoke-interface {v8, v9}, LX/1Di;->i(I)LX/1Di;

    move-result-object v8

    const v9, 0x7f0b008a

    invoke-interface {v8, v9}, LX/1Di;->q(I)LX/1Di;

    move-result-object v8

    const v9, 0x7f0b0060

    invoke-interface {v8, p2, v9}, LX/1Di;->c(II)LX/1Di;

    move-result-object v8

    goto/16 :goto_1

    :cond_5
    sget-object v8, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->BLENDED_VIDEOS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    invoke-virtual {v5, v8}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v8

    goto/16 :goto_0

    .line 2276556
    :cond_6
    invoke-virtual {p1}, LX/1De;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0f0113

    invoke-virtual {v6, v7, v5}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 2276557
    iget-object v6, v0, Lcom/facebook/search/typeahead/rows/nullstate/SearchNullStateSuggestionComponentSpec;->b:LX/0ad;

    sget-short v8, LX/100;->bG:S

    invoke-interface {v6, v8, v9}, LX/0ad;->a(SZ)Z

    move-result v6

    if-eqz v6, :cond_7

    const v6, 0x7f0e0979

    .line 2276558
    :goto_3
    invoke-static {p1, v9, v6}, LX/1na;->a(LX/1De;II)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, v7}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/4 v7, 0x2

    invoke-interface {v6, v7}, LX/1Di;->b(I)LX/1Di;

    move-result-object v6

    goto/16 :goto_2

    .line 2276559
    :cond_7
    const v6, 0x7f0e0978

    goto :goto_3
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2276528
    invoke-static {}, LX/1dS;->b()V

    .line 2276529
    const/4 v0, 0x0

    return-object v0
.end method
