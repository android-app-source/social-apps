.class public LX/GWg;
.super LX/398;
.source ""


# annotations
.annotation build Lcom/facebook/common/uri/annotations/UriMapPattern;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/GWg;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/01T;LX/0Uh;)V
    .locals 5
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/commerce/productdetails/gating/annotations/IsNativeProductDetailsEnabled;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/03R;",
            ">;",
            "LX/01T;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2362614
    invoke-direct {p0}, LX/398;-><init>()V

    .line 2362615
    iput-object p1, p0, LX/GWg;->a:LX/0Or;

    .line 2362616
    sget-object v0, LX/0ax;->fP:Ljava/lang/String;

    const-string v1, "{#%s}"

    const-string v2, "product_item_id"

    invoke-static {v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "{%s 0}"

    const-string v3, "product_ref_id"

    invoke-static {v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "{%s unknown}"

    const-string v4, "product_ref_type"

    invoke-static {v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2362617
    const/16 v1, 0x30f

    const/4 v2, 0x0

    invoke-virtual {p3, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, LX/01T;->FB4A:LX/01T;

    if-ne p2, v1, :cond_0

    .line 2362618
    const-class v1, Lcom/facebook/commerce/productdetails/intent/ProductDetailsActivity;

    invoke-static {}, LX/98s;->newBuilder()LX/98r;

    move-result-object v2

    const-string v3, "/shops_product_details"

    .line 2362619
    iput-object v3, v2, LX/98r;->a:Ljava/lang/String;

    .line 2362620
    move-object v2, v2

    .line 2362621
    const-string v3, "ShopsProductDetailsRoute"

    .line 2362622
    iput-object v3, v2, LX/98r;->b:Ljava/lang/String;

    .line 2362623
    move-object v2, v2

    .line 2362624
    const/4 v3, 0x1

    .line 2362625
    iput v3, v2, LX/98r;->h:I

    .line 2362626
    move-object v2, v2

    .line 2362627
    invoke-virtual {v2}, LX/98r;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 2362628
    :goto_0
    return-void

    .line 2362629
    :cond_0
    const-class v1, Lcom/facebook/base/activity/FragmentChromeActivity;

    sget-object v2, LX/0cQ;->PRODUCT_GROUP_FRAGMENT:LX/0cQ;

    invoke-virtual {v2}, LX/0cQ;->ordinal()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, LX/398;->a(Ljava/lang/String;Ljava/lang/Class;I)V

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/GWg;
    .locals 6

    .prologue
    .line 2362630
    sget-object v0, LX/GWg;->b:LX/GWg;

    if-nez v0, :cond_1

    .line 2362631
    const-class v1, LX/GWg;

    monitor-enter v1

    .line 2362632
    :try_start_0
    sget-object v0, LX/GWg;->b:LX/GWg;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2362633
    if-eqz v2, :cond_0

    .line 2362634
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2362635
    new-instance v5, LX/GWg;

    const/16 v3, 0x305

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/15N;->b(LX/0QB;)LX/01T;

    move-result-object v3

    check-cast v3, LX/01T;

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v4

    check-cast v4, LX/0Uh;

    invoke-direct {v5, p0, v3, v4}, LX/GWg;-><init>(LX/0Or;LX/01T;LX/0Uh;)V

    .line 2362636
    move-object v0, v5

    .line 2362637
    sput-object v0, LX/GWg;->b:LX/GWg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2362638
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2362639
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2362640
    :cond_1
    sget-object v0, LX/GWg;->b:LX/GWg;

    return-object v0

    .line 2362641
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2362642
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 2362643
    iget-object v0, p0, LX/GWg;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03R;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/03R;->asBoolean(Z)Z

    move-result v0

    return v0
.end method
