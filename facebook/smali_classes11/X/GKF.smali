.class public final LX/GKF;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field public final synthetic a:LX/GKQ;


# direct methods
.method public constructor <init>(LX/GKQ;)V
    .locals 0

    .prologue
    .line 2340616
    iput-object p1, p0, LX/GKF;->a:LX/GKQ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLongClick(Landroid/view/View;)Z
    .locals 3

    .prologue
    .line 2340617
    iget-object v0, p0, LX/GKF;->a:LX/GKQ;

    .line 2340618
    iget-object p0, v0, LX/GHg;->b:LX/GCE;

    move-object v0, p0

    .line 2340619
    iget-object p0, v0, LX/GCE;->f:LX/GG3;

    move-object v0, p0

    .line 2340620
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2340621
    iget-object p0, v0, LX/GG3;->f:LX/0Uh;

    const/16 p1, 0x5

    invoke-virtual {p0, p1, v1}, LX/0Uh;->a(IZ)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2340622
    sget-boolean p0, LX/GG3;->k:Z

    if-nez p0, :cond_0

    move v1, v2

    :cond_0
    sput-boolean v1, LX/GG3;->k:Z

    .line 2340623
    iget-object p0, v0, LX/GG3;->e:Landroid/content/Context;

    new-instance p1, Ljava/lang/StringBuilder;

    const-string v1, "Verbose Log mode "

    invoke-direct {p1, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-boolean v1, LX/GG3;->k:Z

    if-eqz v1, :cond_2

    const-string v1, "ENABLED"

    :goto_0
    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 2340624
    :cond_1
    const/4 v0, 0x1

    return v0

    .line 2340625
    :cond_2
    const-string v1, "DISABLED"

    goto :goto_0
.end method
