.class public final LX/Fm6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserPageModels$FundraiserSendInvitesMutationFieldsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;

.field private b:Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;)V
    .locals 0

    .prologue
    .line 2282015
    iput-object p1, p0, LX/Fm6;->a:Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2282016
    iput-object p2, p0, LX/Fm6;->b:Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;

    .line 2282017
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2282018
    iget-object v0, p0, LX/Fm6;->b:Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;

    const/4 v1, 0x0

    .line 2282019
    iput-boolean v1, v0, Lcom/facebook/widget/singleclickinvite/SingleClickInviteUserToken;->f:Z

    .line 2282020
    iget-object v0, p0, LX/Fm6;->a:Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;

    iget-object v0, v0, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Zb;

    iget-object v1, p0, LX/Fm6;->a:Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->k:Ljava/lang/String;

    iget-object v2, p0, LX/Fm6;->a:Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;

    iget-object v2, v2, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;->l:Ljava/lang/String;

    .line 2282021
    new-instance v3, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string p1, "fundraiser_single_click_invite_sent_failure"

    invoke-direct {v3, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "fundraiser_single_click_invite"

    .line 2282022
    iput-object p1, v3, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2282023
    move-object v3, v3

    .line 2282024
    const-string p1, "fundraiser_campaign_id"

    invoke-virtual {v3, p1, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    const-string p1, "source"

    invoke-virtual {v3, p1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v3

    move-object v1, v3

    .line 2282025
    invoke-interface {v0, v1}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2282026
    iget-object v0, p0, LX/Fm6;->a:Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2282027
    iget-object v0, p0, LX/Fm6;->a:Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment$FundraiserSendInvitesMutationCallback$1;

    invoke-direct {v1, p0}, Lcom/facebook/socialgood/inviter/FundraiserSingleClickInviteFragment$FundraiserSendInvitesMutationCallback$1;-><init>(LX/Fm6;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2282028
    :cond_0
    return-void
.end method

.method public final bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2282029
    return-void
.end method
