.class public LX/GQ5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 2349693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/view/View;III)V
    .locals 2

    .prologue
    .line 2349694
    invoke-static {p0, p1}, LX/GQ1;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 2349695
    const-class v1, Landroid/widget/ImageView;

    invoke-static {v0, v1}, LX/GQ1;->a(Landroid/view/ViewGroup;Ljava/lang/Class;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2349696
    const-class v1, Landroid/widget/TextView;

    invoke-static {v0, v1}, LX/GQ1;->a(Landroid/view/ViewGroup;Ljava/lang/Class;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(I)V

    .line 2349697
    return-void
.end method

.method public static a(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;Ljava/lang/Runnable;Lcom/facebook/payments/paymentmethods/model/PaymentOption;)V
    .locals 6

    .prologue
    .line 2349698
    const-string v0, "payment_method_type"

    invoke-interface {p2}, Lcom/facebook/payments/paymentmethods/model/PaymentOption;->e()LX/6zP;

    move-result-object v1

    invoke-interface {v1}, LX/6LU;->getValue()Ljava/lang/Object;

    move-result-object v1

    const-string v2, "credential_id"

    invoke-interface {p2}, Lcom/facebook/payments/paymentmethods/model/PaymentOption;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    .line 2349699
    new-instance v1, LX/31Y;

    invoke-direct {v1, p0}, LX/31Y;-><init>(Landroid/content/Context;)V

    invoke-interface {p2}, Lcom/facebook/payments/paymentmethods/model/PaymentOption;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "boletobancario_santander_BR"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 2349700
    invoke-virtual {p0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f031005

    const/4 v5, 0x0

    const/4 p2, 0x0

    invoke-virtual {v3, v4, v5, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 2349701
    if-eqz v2, :cond_0

    .line 2349702
    const v3, 0x7f0d04f9

    invoke-static {v4, v3}, LX/GQ1;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 2349703
    const v5, 0x7f0827d4

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(I)V

    .line 2349704
    const v3, 0x7f0d267b

    const v5, 0x7f02007c

    const p2, 0x7f0827d6

    invoke-static {v4, v3, v5, p2}, LX/GQ5;->a(Landroid/view/View;III)V

    .line 2349705
    const v3, 0x7f0d267c

    const v5, 0x7f020121

    const p2, 0x7f0827d8

    invoke-static {v4, v3, v5, p2}, LX/GQ5;->a(Landroid/view/View;III)V

    .line 2349706
    const v3, 0x7f0d267d

    const v5, 0x7f020081

    const p2, 0x7f0827d7

    invoke-static {v4, v3, v5, p2}, LX/GQ5;->a(Landroid/view/View;III)V

    .line 2349707
    :cond_0
    move-object v2, v4

    .line 2349708
    invoke-virtual {v1, v2}, LX/0ju;->b(Landroid/view/View;)LX/0ju;

    move-result-object v1

    const v2, 0x7f08002a

    new-instance v3, LX/GQ4;

    invoke-direct {v3, p0, v0, p1}, LX/GQ4;-><init>(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;LX/0P1;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const v2, 0x7f080027

    new-instance v3, LX/GQ3;

    invoke-direct {v3, p0, v0}, LX/GQ3;-><init>(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;LX/0P1;)V

    invoke-virtual {v1, v2, v3}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0ju;->a(Z)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    .line 2349709
    const-string v1, "prepay_disclaimer"

    invoke-virtual {p0, v1, v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->c(Ljava/lang/String;Ljava/util/Map;)V

    .line 2349710
    return-void
.end method
