.class public final LX/Gjz;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/2W4;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/2W4",
        "<",
        "LX/Gk3;",
        "Lcom/facebook/greetingcards/verve/model/VMDeck;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:LX/0lp;


# direct methods
.method public constructor <init>(LX/0lp;)V
    .locals 0

    .prologue
    .line 2388704
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2388705
    iput-object p1, p0, LX/Gjz;->a:LX/0lp;

    .line 2388706
    return-void
.end method


# virtual methods
.method public final a(LX/2WG;LX/1gI;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2388699
    check-cast p1, LX/Gk3;

    .line 2388700
    invoke-interface {p2}, LX/1gI;->a()Ljava/io/InputStream;

    move-result-object v0

    .line 2388701
    :try_start_0
    iget-object v1, p0, LX/Gjz;->a:LX/0lp;

    invoke-virtual {v1, v0}, LX/0lp;->a(Ljava/io/InputStream;)LX/15w;

    move-result-object v1

    .line 2388702
    const-class p1, Lcom/facebook/greetingcards/verve/model/VMDeck;

    invoke-virtual {v1, p1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/greetingcards/verve/model/VMDeck;

    move-object v1, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2388703
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    throw v1
.end method

.method public final a(LX/2WG;[B)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2388697
    iget-object v0, p0, LX/Gjz;->a:LX/0lp;

    invoke-virtual {v0, p2}, LX/0lp;->a([B)LX/15w;

    move-result-object v0

    .line 2388698
    const-class v1, Lcom/facebook/greetingcards/verve/model/VMDeck;

    invoke-virtual {v0, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/greetingcards/verve/model/VMDeck;

    return-object v0
.end method
