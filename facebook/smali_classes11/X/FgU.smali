.class public abstract LX/FgU;
.super LX/7HQ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/7HQ",
        "<",
        "Lcom/facebook/search/model/TypeaheadUnit;",
        ">;"
    }
.end annotation


# instance fields
.field private b:LX/Fgp;

.field private c:Lcom/facebook/search/model/TypeaheadUnit;


# direct methods
.method public constructor <init>(LX/7Hg;LX/7Hl;LX/7HW;LX/7Hk;LX/2Sd;)V
    .locals 0

    .prologue
    .line 2270412
    invoke-direct/range {p0 .. p5}, LX/7HQ;-><init>(LX/7Hg;LX/7Hl;LX/7HW;LX/7Hk;LX/2Sd;)V

    .line 2270413
    return-void
.end method


# virtual methods
.method public a(LX/7Hi;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2270401
    iget-object v0, p1, LX/7Hi;->c:LX/7HY;

    move-object v0, v0

    .line 2270402
    sget-object v1, LX/7HY;->REMOTE:LX/7HY;

    if-ne v0, v1, :cond_0

    .line 2270403
    iget-object v0, p1, LX/7Hi;->b:LX/7Hc;

    move-object v0, v0

    .line 2270404
    iget-object v1, v0, LX/7Hc;->b:LX/0Px;

    move-object v0, v1

    .line 2270405
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2270406
    iget-object v0, p1, LX/7Hi;->b:LX/7Hc;

    move-object v0, v0

    .line 2270407
    iget-object v1, v0, LX/7Hc;->b:LX/0Px;

    move-object v0, v1

    .line 2270408
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/model/TypeaheadUnit;

    :goto_0
    iput-object v0, p0, LX/FgU;->c:Lcom/facebook/search/model/TypeaheadUnit;

    .line 2270409
    :cond_0
    invoke-super {p0, p1}, LX/7HQ;->a(LX/7Hi;)V

    .line 2270410
    return-void

    .line 2270411
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract a(LX/Cvp;)V
.end method

.method public a(LX/Fgp;)V
    .locals 0

    .prologue
    .line 2270397
    iput-object p1, p0, LX/FgU;->b:LX/Fgp;

    .line 2270398
    return-void
.end method

.method public b(LX/0P1;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2270399
    invoke-virtual {p0, p1}, LX/7HQ;->a(LX/0P1;)V

    .line 2270400
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 2270389
    invoke-virtual {p0}, LX/FgU;->mI_()V

    .line 2270390
    return-void
.end method

.method public h()Lcom/facebook/search/model/TypeaheadUnit;
    .locals 1

    .prologue
    .line 2270391
    iget-object v0, p0, LX/FgU;->c:Lcom/facebook/search/model/TypeaheadUnit;

    return-object v0
.end method

.method public mI_()V
    .locals 9

    .prologue
    .line 2270392
    iget-object v0, p0, LX/FgU;->b:LX/Fgp;

    if-eqz v0, :cond_0

    .line 2270393
    iget-object v0, p0, LX/FgU;->b:LX/Fgp;

    .line 2270394
    iget-object v1, v0, LX/Fgp;->s:LX/Fh9;

    if-nez v1, :cond_1

    .line 2270395
    :cond_0
    :goto_0
    return-void

    .line 2270396
    :cond_1
    iget-object v1, v0, LX/Fgp;->s:LX/Fh9;

    invoke-static {v0}, LX/Fgp;->r(LX/Fgp;)Landroid/widget/ProgressBar;

    move-result-object v2

    iget-object v3, v0, LX/Fgp;->p:LX/0g8;

    iget-object v4, v0, LX/Fgp;->n:LX/2SP;

    iget-object v5, v0, LX/Fgp;->q:LX/Fhs;

    iget-object v6, v0, LX/Fgp;->d:LX/Fid;

    iget-object v7, v0, LX/Fgp;->o:LX/1Qq;

    move-object v8, v0

    invoke-virtual/range {v1 .. v8}, LX/Fh9;->a(Landroid/widget/ProgressBar;LX/0g8;LX/2SP;LX/Fhs;LX/Fid;LX/1Qq;LX/Fgp;)V

    goto :goto_0
.end method
