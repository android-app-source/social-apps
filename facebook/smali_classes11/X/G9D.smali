.class public LX/G9D;
.super LX/G8p;
.source ""


# static fields
.field private static final a:[B


# instance fields
.field public b:[B

.field public final c:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2321775
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, LX/G9D;->a:[B

    return-void
.end method

.method public constructor <init>(LX/G8w;)V
    .locals 1

    .prologue
    .line 2321776
    invoke-direct {p0, p1}, LX/G8p;-><init>(LX/G8w;)V

    .line 2321777
    sget-object v0, LX/G9D;->a:[B

    iput-object v0, p0, LX/G9D;->b:[B

    .line 2321778
    const/16 v0, 0x20

    new-array v0, v0, [I

    iput-object v0, p0, LX/G9D;->c:[I

    .line 2321779
    return-void
.end method

.method private static a([I)I
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 2321780
    array-length v6, p0

    move v2, v4

    move v0, v4

    move v1, v4

    move v3, v4

    .line 2321781
    :goto_0
    if-ge v2, v6, :cond_2

    .line 2321782
    aget v5, p0, v2

    if-le v5, v0, :cond_0

    .line 2321783
    aget v0, p0, v2

    move v1, v2

    .line 2321784
    :cond_0
    aget v5, p0, v2

    if-le v5, v3, :cond_1

    .line 2321785
    aget v3, p0, v2

    .line 2321786
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move v2, v4

    move v5, v4

    .line 2321787
    :goto_1
    if-ge v4, v6, :cond_3

    .line 2321788
    sub-int v0, v4, v1

    .line 2321789
    aget v7, p0, v4

    mul-int/2addr v7, v0

    mul-int/2addr v0, v7

    .line 2321790
    if-le v0, v2, :cond_8

    move v2, v4

    .line 2321791
    :goto_2
    add-int/lit8 v4, v4, 0x1

    move v5, v2

    move v2, v0

    goto :goto_1

    .line 2321792
    :cond_3
    if-le v1, v5, :cond_7

    .line 2321793
    :goto_3
    sub-int v0, v1, v5

    div-int/lit8 v2, v6, 0x10

    if-gt v0, v2, :cond_4

    .line 2321794
    sget-object v0, LX/G8x;->c:LX/G8x;

    move-object v0, v0

    .line 2321795
    throw v0

    .line 2321796
    :cond_4
    add-int/lit8 v6, v1, -0x1

    .line 2321797
    const/4 v2, -0x1

    .line 2321798
    add-int/lit8 v4, v1, -0x1

    :goto_4
    if-le v4, v5, :cond_5

    .line 2321799
    sub-int v0, v4, v5

    .line 2321800
    mul-int/2addr v0, v0

    sub-int v7, v1, v4

    mul-int/2addr v0, v7

    aget v7, p0, v4

    sub-int v7, v3, v7

    mul-int/2addr v0, v7

    .line 2321801
    if-le v0, v2, :cond_6

    move v2, v4

    .line 2321802
    :goto_5
    add-int/lit8 v4, v4, -0x1

    move v6, v2

    move v2, v0

    goto :goto_4

    .line 2321803
    :cond_5
    shl-int/lit8 v0, v6, 0x3

    return v0

    :cond_6
    move v0, v2

    move v2, v6

    goto :goto_5

    :cond_7
    move v8, v5

    move v5, v1

    move v1, v8

    goto :goto_3

    :cond_8
    move v0, v2

    move v2, v5

    goto :goto_2
.end method


# virtual methods
.method public b()LX/G96;
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 2321804
    iget-object v0, p0, LX/G8p;->a:LX/G8w;

    move-object v3, v0

    .line 2321805
    iget v0, v3, LX/G8w;->a:I

    move v4, v0

    .line 2321806
    iget v0, v3, LX/G8w;->b:I

    move v5, v0

    .line 2321807
    new-instance v6, LX/G96;

    invoke-direct {v6, v4, v5}, LX/G96;-><init>(II)V

    .line 2321808
    const/4 v2, 0x0

    .line 2321809
    iget-object v0, p0, LX/G9D;->b:[B

    array-length v0, v0

    if-ge v0, v4, :cond_0

    .line 2321810
    new-array v0, v4, [B

    iput-object v0, p0, LX/G9D;->b:[B

    :cond_0
    move v0, v2

    .line 2321811
    :goto_0
    const/16 v7, 0x20

    if-ge v0, v7, :cond_1

    .line 2321812
    iget-object v7, p0, LX/G9D;->c:[I

    aput v2, v7, v0

    .line 2321813
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2321814
    :cond_1
    iget-object v7, p0, LX/G9D;->c:[I

    .line 2321815
    const/4 v0, 0x1

    move v2, v0

    :goto_1
    const/4 v0, 0x5

    if-ge v2, v0, :cond_3

    .line 2321816
    mul-int v0, v5, v2

    div-int/lit8 v0, v0, 0x5

    .line 2321817
    iget-object v8, p0, LX/G9D;->b:[B

    invoke-virtual {v3, v0, v8}, LX/G8w;->a(I[B)[B

    move-result-object v8

    .line 2321818
    mul-int/lit8 v0, v4, 0x4

    div-int/lit8 v9, v0, 0x5

    .line 2321819
    div-int/lit8 v0, v4, 0x5

    :goto_2
    if-ge v0, v9, :cond_2

    .line 2321820
    aget-byte v10, v8, v0

    and-int/lit16 v10, v10, 0xff

    .line 2321821
    shr-int/lit8 v10, v10, 0x3

    aget v11, v7, v10

    add-int/lit8 v11, v11, 0x1

    aput v11, v7, v10

    .line 2321822
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2321823
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 2321824
    :cond_3
    invoke-static {v7}, LX/G9D;->a([I)I

    move-result v7

    .line 2321825
    invoke-virtual {v3}, LX/G8w;->a()[B

    move-result-object v3

    move v2, v1

    .line 2321826
    :goto_3
    if-ge v2, v5, :cond_6

    .line 2321827
    mul-int v8, v2, v4

    move v0, v1

    .line 2321828
    :goto_4
    if-ge v0, v4, :cond_5

    .line 2321829
    add-int v9, v8, v0

    aget-byte v9, v3, v9

    and-int/lit16 v9, v9, 0xff

    .line 2321830
    if-ge v9, v7, :cond_4

    .line 2321831
    invoke-virtual {v6, v0, v2}, LX/G96;->b(II)V

    .line 2321832
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 2321833
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 2321834
    :cond_6
    return-object v6
.end method
