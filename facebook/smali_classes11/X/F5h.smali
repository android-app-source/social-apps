.class public LX/F5h;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/1Ck;

.field public final b:LX/0tX;

.field public final c:Landroid/content/res/Resources;

.field public final d:Z


# direct methods
.method public constructor <init>(LX/1Ck;LX/0tX;Landroid/content/res/Resources;Ljava/lang/Boolean;)V
    .locals 1
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2199110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2199111
    iput-object p1, p0, LX/F5h;->a:LX/1Ck;

    .line 2199112
    iput-object p2, p0, LX/F5h;->b:LX/0tX;

    .line 2199113
    iput-object p3, p0, LX/F5h;->c:Landroid/content/res/Resources;

    .line 2199114
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/F5h;->d:Z

    .line 2199115
    return-void
.end method

.method public static b(LX/0QB;)LX/F5h;
    .locals 5

    .prologue
    .line 2199116
    new-instance v4, LX/F5h;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v0

    check-cast v0, LX/1Ck;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v1

    check-cast v1, LX/0tX;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-direct {v4, v0, v1, v2, v3}, LX/F5h;-><init>(LX/1Ck;LX/0tX;Landroid/content/res/Resources;Ljava/lang/Boolean;)V

    .line 2199117
    return-object v4
.end method


# virtual methods
.method public final a(LX/F5k;)V
    .locals 4

    .prologue
    .line 2199118
    iget-object v0, p0, LX/F5h;->a:LX/1Ck;

    sget-object v1, LX/F5g;->GROUPS_CREATION_SUGGESTIONS_QUERY:LX/F5g;

    invoke-virtual {v0, v1}, LX/1Ck;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2199119
    :goto_0
    return-void

    .line 2199120
    :cond_0
    new-instance v0, LX/F5e;

    invoke-direct {v0, p0}, LX/F5e;-><init>(LX/F5h;)V

    .line 2199121
    new-instance v1, LX/F5f;

    invoke-direct {v1, p0, p1}, LX/F5f;-><init>(LX/F5h;LX/F5k;)V

    .line 2199122
    iget-object v2, p0, LX/F5h;->a:LX/1Ck;

    sget-object v3, LX/F5g;->GROUPS_CREATION_SUGGESTIONS_QUERY:LX/F5g;

    invoke-virtual {v2, v3, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_0
.end method
