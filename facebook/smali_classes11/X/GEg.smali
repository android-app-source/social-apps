.class public LX/GEg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GEW",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;",
        "Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/0ad;

.field private b:LX/GJK;


# direct methods
.method public constructor <init>(LX/0ad;LX/GJK;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2331977
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2331978
    iput-object p1, p0, LX/GEg;->a:LX/0ad;

    .line 2331979
    iput-object p2, p0, LX/GEg;->b:LX/GJK;

    .line 2331980
    return-void
.end method

.method public static a(LX/0QB;)LX/GEg;
    .locals 3

    .prologue
    .line 2331981
    new-instance v2, LX/GEg;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v0

    check-cast v0, LX/0ad;

    invoke-static {p0}, LX/GJK;->c(LX/0QB;)LX/GJK;

    move-result-object v1

    check-cast v1, LX/GJK;

    invoke-direct {v2, v0, v1}, LX/GEg;-><init>(LX/0ad;LX/GJK;)V

    .line 2331982
    move-object v0, v2

    .line 2331983
    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2331984
    const v0, 0x7f03004f

    return v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 2

    .prologue
    .line 2331985
    check-cast p1, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    .line 2331986
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v0

    .line 2331987
    sget-object v1, LX/GGB;->ACTIVE:LX/GGB;

    if-eq v0, v1, :cond_0

    sget-object v1, LX/GGB;->PENDING:LX/GGB;

    if-ne v0, v1, :cond_1

    .line 2331988
    :cond_0
    const/4 v0, 0x1

    .line 2331989
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()LX/GHg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesViewController",
            "<",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;",
            "Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2331990
    iget-object v0, p0, LX/GEg;->b:LX/GJK;

    return-object v0
.end method

.method public final c()LX/8wK;
    .locals 1

    .prologue
    .line 2331991
    sget-object v0, LX/8wK;->DURATION_BUDGET:LX/8wK;

    return-object v0
.end method
