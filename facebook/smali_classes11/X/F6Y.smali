.class public final LX/F6Y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;)V
    .locals 0

    .prologue
    .line 2200460
    iput-object p1, p0, LX/F6Y;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x2

    const v2, 0x4ce56d26    # 1.20285488E8f

    invoke-static {v4, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2200461
    iget-object v3, p0, LX/F6Y;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    iget-object v3, v3, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->w:Landroid/widget/EditText;

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/F6Y;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    iget-object v3, v3, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->w:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, LX/F6Y;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    iget-object v3, v3, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->w:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2200462
    :cond_0
    const v0, -0x634dece2

    invoke-static {v4, v4, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2200463
    :goto_0
    return-void

    .line 2200464
    :cond_1
    iget-object v3, p0, LX/F6Y;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    iget-object v3, v3, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->w:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2200465
    iget-object v4, p0, LX/F6Y;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    iget-object v4, v4, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->z:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, p0, LX/F6Y;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    iget-object v4, v4, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->z:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2200466
    :goto_1
    iget-object v4, p0, LX/F6Y;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    iget-object v5, p0, LX/F6Y;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    iget-object v5, v5, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->w:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2200467
    invoke-static {v4, v5, v0}, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->a$redex0(Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;Ljava/lang/String;Z)V

    .line 2200468
    iget-object v0, p0, LX/F6Y;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    invoke-static {v0}, LX/2Na;->a(Landroid/app/Activity;)V

    .line 2200469
    iget-object v0, p0, LX/F6Y;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    iget-object v0, v0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->v:LX/3fx;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, LX/F6Y;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    iget-object v5, v5, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->x:Lcom/facebook/widget/countryspinner/CountrySpinner;

    invoke-virtual {v5}, Lcom/facebook/widget/countryspinner/CountrySpinner;->getSelectedCountryDialingCode()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, LX/3fx;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2200470
    iget-object v3, p0, LX/F6Y;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    iget-object v3, v3, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->x:Lcom/facebook/widget/countryspinner/CountrySpinner;

    invoke-virtual {v3}, Lcom/facebook/widget/countryspinner/CountrySpinner;->getSelectedCountryIsoCode()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/facebook/growth/model/Contactpoint;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/growth/model/Contactpoint;

    move-result-object v3

    .line 2200471
    new-instance v4, Lcom/facebook/api/growth/UserSetContactInfoMethod$Params;

    invoke-direct {v4, v0, v1}, Lcom/facebook/api/growth/UserSetContactInfoMethod$Params;-><init>(Ljava/lang/String;Z)V

    .line 2200472
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2200473
    const-string v1, "growthUserSetContactInfoParamsKey"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2200474
    iget-object v1, p0, LX/F6Y;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    iget-object v1, v1, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->p:LX/0aG;

    const-string v4, "growth_user_set_contact_info"

    const v5, 0x5ddc84ce

    invoke-static {v1, v4, v0, v5}, LX/04N;->a(LX/0aG;Ljava/lang/String;Landroid/os/Bundle;I)LX/1MF;

    move-result-object v0

    new-instance v1, LX/4At;

    iget-object v4, p0, LX/F6Y;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    const v5, 0x7f080037

    invoke-direct {v1, v4, v5}, LX/4At;-><init>(Landroid/content/Context;I)V

    invoke-interface {v0, v1}, LX/1MF;->setOperationProgressIndicator(LX/4At;)LX/1MF;

    move-result-object v0

    invoke-interface {v0}, LX/1MF;->start()LX/1ML;

    move-result-object v0

    .line 2200475
    new-instance v1, LX/F6X;

    invoke-direct {v1, p0, v3}, LX/F6X;-><init>(LX/F6Y;Lcom/facebook/growth/model/Contactpoint;)V

    invoke-static {v0, v1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2200476
    const v0, -0x47ce6cb5

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 2200477
    goto/16 :goto_1
.end method
