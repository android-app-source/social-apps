.class public final LX/Fj7;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Fj8;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2276348
    invoke-static {}, LX/Fj8;->q()LX/Fj8;

    move-result-object v0

    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2276349
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2276347
    const-string v0, "NullStateSectionHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2276330
    if-ne p0, p1, :cond_1

    .line 2276331
    :cond_0
    :goto_0
    return v0

    .line 2276332
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2276333
    goto :goto_0

    .line 2276334
    :cond_3
    check-cast p1, LX/Fj7;

    .line 2276335
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2276336
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2276337
    if-eq v2, v3, :cond_0

    .line 2276338
    iget-object v2, p0, LX/Fj7;->a:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Fj7;->a:Ljava/lang/String;

    iget-object v3, p1, LX/Fj7;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2276339
    goto :goto_0

    .line 2276340
    :cond_5
    iget-object v2, p1, LX/Fj7;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2276341
    :cond_6
    iget-object v2, p0, LX/Fj7;->b:Ljava/lang/String;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Fj7;->b:Ljava/lang/String;

    iget-object v3, p1, LX/Fj7;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2276342
    goto :goto_0

    .line 2276343
    :cond_8
    iget-object v2, p1, LX/Fj7;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2276344
    :cond_9
    iget-object v2, p0, LX/Fj7;->c:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_a

    iget-object v2, p0, LX/Fj7;->c:Landroid/view/View$OnClickListener;

    iget-object v3, p1, LX/Fj7;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2276345
    goto :goto_0

    .line 2276346
    :cond_a
    iget-object v2, p1, LX/Fj7;->c:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
