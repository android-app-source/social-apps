.class public final LX/FsY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsUserFieldsModel;",
        ">;",
        "LX/FsM;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Fse;


# direct methods
.method public constructor <init>(LX/Fse;)V
    .locals 0

    .prologue
    .line 2297237
    iput-object p1, p0, LX/FsY;->a:LX/Fse;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2297230
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2297231
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2297232
    check-cast v0, Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsUserFieldsModel;

    invoke-static {v0}, LX/5xF;->a(Lcom/facebook/timeline/protocol/FetchTimelineSectionGraphQLModels$TimelineFirstUnitsUserFieldsModel;)Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v0

    .line 2297233
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->s()Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-result-object v1

    invoke-static {v1}, LX/Fs4;->a(Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;)V

    .line 2297234
    new-instance v1, LX/FsM;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->Y()Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLUser;->s()Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;

    move-result-object v0

    .line 2297235
    iget-object v3, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v3, v3

    .line 2297236
    invoke-direct {v1, v2, v0, v3}, LX/FsM;-><init>(Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;Lcom/facebook/graphql/model/GraphQLTimelineSectionsConnection;LX/0ta;)V

    return-object v1
.end method
