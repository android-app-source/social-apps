.class public LX/FAa;
.super LX/Chc;
.source ""

# interfaces
.implements LX/ChL;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "LX/Chc",
        "<",
        "LX/0zO",
        "<TT;>;",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<TT;>;>;",
        "Lcom/facebook/instantshopping/InstantShoppingDocumentDelegate;"
    }
.end annotation


# static fields
.field private static final ai:Ljava/lang/String;


# instance fields
.field public B:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public C:LX/Gr9;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public D:LX/GrE;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public E:LX/8Yg;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public F:LX/8Yh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public G:LX/193;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public H:LX/Go0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public I:LX/Go1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public J:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public K:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public L:LX/Gny;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public M:LX/0Uh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public N:LX/GnF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public O:LX/CIe;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public P:LX/CIZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Q:LX/CIb;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public R:LX/2yr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public S:LX/Go2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public T:LX/Go4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public U:LX/CIi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public V:LX/Cl7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public W:LX/Go7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public X:LX/3kp;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Y:LX/GoA;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public Z:LX/GoD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private aA:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

.field public aB:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;",
            ">;"
        }
    .end annotation
.end field

.field public aC:Z

.field private aD:Z

.field public aE:Z

.field public aF:LX/Go5;

.field public aG:LX/Clr;

.field public aH:Z

.field private aI:LX/Gr7;

.field private final aJ:LX/GnR;

.field private final aK:LX/GnQ;

.field private final aL:LX/GnT;

.field private final aM:LX/GnS;

.field private final aN:LX/GnO;

.field private final aO:LX/GnP;

.field public aa:LX/Gn2;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ab:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ac:Lcom/facebook/prefs/shared/FbSharedPreferences;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ad:LX/0if;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ae:LX/ClD;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public af:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Chi;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ag:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Gn5;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public ah:LX/Gn1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final aj:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/GpR;",
            ">;"
        }
    .end annotation
.end field

.field public ak:Landroid/widget/TextView;

.field public al:Landroid/widget/LinearLayout;

.field public am:Landroid/view/ViewGroup;

.field public an:LX/Gmy;

.field public ao:Ljava/lang/String;

.field public ap:LX/Gq2;

.field public aq:LX/FAg;

.field public ar:LX/GoH;

.field public as:Ljava/lang/String;

.field public at:Ljava/lang/String;

.field private au:Ljava/lang/String;

.field private av:Ljava/lang/String;

.field private aw:LX/0lF;

.field private ax:Ljava/lang/String;

.field private ay:Ljava/lang/String;

.field public az:LX/0ht;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2207616
    const-class v0, LX/FAa;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/FAa;->ai:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2207606
    invoke-direct {p0}, LX/Chc;-><init>()V

    .line 2207607
    const/4 v0, 0x0

    iput-object v0, p0, LX/FAa;->an:LX/Gmy;

    .line 2207608
    new-instance v0, LX/FAP;

    invoke-direct {v0, p0}, LX/FAP;-><init>(LX/FAa;)V

    iput-object v0, p0, LX/FAa;->aJ:LX/GnR;

    .line 2207609
    new-instance v0, LX/FAQ;

    invoke-direct {v0, p0}, LX/FAQ;-><init>(LX/FAa;)V

    iput-object v0, p0, LX/FAa;->aK:LX/GnQ;

    .line 2207610
    new-instance v0, LX/FAT;

    invoke-direct {v0, p0}, LX/FAT;-><init>(LX/FAa;)V

    iput-object v0, p0, LX/FAa;->aL:LX/GnT;

    .line 2207611
    new-instance v0, LX/FAU;

    invoke-direct {v0, p0}, LX/FAU;-><init>(LX/FAa;)V

    iput-object v0, p0, LX/FAa;->aM:LX/GnS;

    .line 2207612
    new-instance v0, LX/FAV;

    invoke-direct {v0, p0}, LX/FAV;-><init>(LX/FAa;)V

    iput-object v0, p0, LX/FAa;->aN:LX/GnO;

    .line 2207613
    new-instance v0, LX/FAW;

    invoke-direct {v0, p0}, LX/FAW;-><init>(LX/FAa;)V

    iput-object v0, p0, LX/FAa;->aO:LX/GnP;

    .line 2207614
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/FAa;->aj:Ljava/util/List;

    .line 2207615
    return-void
.end method

.method private Q()V
    .locals 7

    .prologue
    .line 2207617
    iget-object v0, p0, LX/FAa;->H:LX/Go0;

    invoke-virtual {p0}, LX/FAa;->b()Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, LX/FAa;->aw:LX/0lF;

    .line 2207618
    iget-object v3, v0, LX/Go0;->d:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->clear()V

    .line 2207619
    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2207620
    iget-object v5, v0, LX/Go0;->d:Ljava/util/Map;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v5, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2207621
    :cond_0
    iput-object v2, v0, LX/Go0;->f:LX/0lF;

    .line 2207622
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2207623
    invoke-static {v0}, LX/FAa;->k(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/FAa;->au:Ljava/lang/String;

    .line 2207624
    iget-object v0, p0, LX/FAa;->au:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2207625
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2207626
    const-string v1, "instant_shopping_product_id"

    iget-object v2, p0, LX/FAa;->au:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2207627
    const-string v1, "instant_shopping_product_view"

    iget-object v2, p0, LX/FAa;->av:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2207628
    iget-object v1, p0, LX/FAa;->H:LX/Go0;

    .line 2207629
    iput-object v0, v1, LX/Go0;->e:Ljava/util/Map;

    .line 2207630
    :cond_1
    iget-object v0, p0, LX/FAa;->H:LX/Go0;

    const-string v1, "instant_shopping_document_open"

    invoke-virtual {v0, v1}, LX/Go0;->a(Ljava/lang/String;)V

    .line 2207631
    iget-object v0, p0, LX/FAa;->aa:LX/Gn2;

    iget-object v1, p0, LX/FAa;->at:Ljava/lang/String;

    iget-object v2, p0, LX/FAa;->au:Ljava/lang/String;

    const/4 v3, 0x1

    .line 2207632
    if-eqz v1, :cond_3

    if-nez v2, :cond_3

    .line 2207633
    iput-boolean v3, v0, LX/Gn2;->c:Z

    .line 2207634
    :cond_2
    :goto_1
    return-void

    .line 2207635
    :cond_3
    if-eqz v2, :cond_2

    .line 2207636
    iput-boolean v3, v0, LX/Gn2;->d:Z

    goto :goto_1
.end method

.method public static R(LX/FAa;)V
    .locals 5

    .prologue
    .line 2207637
    iget-object v0, p0, LX/Chc;->D:Landroid/view/View;

    move-object v0, v0

    .line 2207638
    const v1, 0x7f0d1883

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2207639
    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 2207640
    return-void
.end method

.method public static S(LX/FAa;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2207641
    iget-object v0, p0, LX/Chc;->D:Landroid/view/View;

    move-object v0, v0

    .line 2207642
    const v1, 0x7f0d0541

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2207643
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2207644
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 2207645
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 2207646
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2207647
    return-void
.end method

.method public static T(LX/FAa;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2207648
    iget-object v0, p0, LX/Chc;->D:Landroid/view/View;

    move-object v0, v0

    .line 2207649
    const v1, 0x7f0d188c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2207650
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 2207651
    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 2207652
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 2207653
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2207654
    return-void
.end method

.method public static X(LX/FAa;)V
    .locals 4

    .prologue
    .line 2207655
    iget-object v0, p0, LX/FAa;->P:LX/CIZ;

    iget-object v1, p0, LX/FAa;->at:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/CIZ;->c(Ljava/lang/String;)I

    move-result v0

    .line 2207656
    iget-object v1, p0, LX/FAa;->aj:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/GpR;

    .line 2207657
    instance-of v3, v1, LX/GpU;

    if-eqz v3, :cond_0

    .line 2207658
    check-cast v1, LX/GpU;

    .line 2207659
    iget-object v3, v1, LX/CnT;->d:LX/CnG;

    move-object v3, v3

    .line 2207660
    check-cast v3, LX/Gq2;

    invoke-virtual {v3, v0}, LX/Gq2;->a(I)V

    .line 2207661
    goto :goto_0

    .line 2207662
    :cond_1
    return-void
.end method

.method private Y()V
    .locals 12

    .prologue
    .line 2207663
    iget-object v0, p0, LX/Chc;->G:Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;

    move-object v0, v0

    .line 2207664
    iget-boolean v1, v0, Lcom/facebook/richdocument/view/widget/ViewSwipeToDismissTransitioner;->u:Z

    move v0, v1

    .line 2207665
    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FAa;->aF:LX/Go5;

    if-nez v0, :cond_0

    .line 2207666
    sget-object v0, LX/Go5;->DOCUMENT_SWIPED_BACK:LX/Go5;

    iput-object v0, p0, LX/FAa;->aF:LX/Go5;

    .line 2207667
    :cond_0
    iget-object v0, p0, LX/FAa;->aF:LX/Go5;

    if-nez v0, :cond_1

    .line 2207668
    sget-object v0, LX/Go5;->UNKNOWN:LX/Go5;

    iput-object v0, p0, LX/FAa;->aF:LX/Go5;

    .line 2207669
    :cond_1
    iget-object v0, p0, LX/FAa;->W:LX/Go7;

    invoke-virtual {p0}, LX/FAa;->b()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Go7;->a(Ljava/util/Map;)V

    .line 2207670
    iget-object v0, p0, LX/FAa;->S:LX/Go2;

    invoke-virtual {v0}, LX/Go2;->c()V

    .line 2207671
    iget-object v0, p0, LX/FAa;->W:LX/Go7;

    const-string v1, "swipe_to_open_time"

    iget-object v2, p0, LX/FAa;->S:LX/Go2;

    .line 2207672
    iget-wide v4, v2, LX/Go2;->f:J

    move-wide v2, v4

    .line 2207673
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2207674
    iget-object v0, p0, LX/FAa;->W:LX/Go7;

    iget-object v1, p0, LX/FAa;->S:LX/Go2;

    .line 2207675
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 2207676
    const-string v5, "canvas_dwell_time"

    iget-wide v6, v1, LX/Go2;->c:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2207677
    const-string v5, "links_dwell_time"

    iget-wide v6, v1, LX/Go2;->e:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2207678
    const-wide/16 v8, 0x0

    .line 2207679
    iput-wide v8, v1, LX/Go2;->c:J

    .line 2207680
    iput-wide v8, v1, LX/Go2;->d:J

    .line 2207681
    iput-wide v8, v1, LX/Go2;->e:J

    .line 2207682
    iput-wide v8, v1, LX/Go2;->b:J

    .line 2207683
    iput-wide v8, v1, LX/Go2;->f:J

    .line 2207684
    const/4 v8, 0x0

    iput-boolean v8, v1, LX/Go2;->h:Z

    .line 2207685
    move-object v1, v4

    .line 2207686
    invoke-virtual {v0, v1}, LX/Go7;->a(Ljava/util/Map;)V

    .line 2207687
    iget-object v0, p0, LX/FAa;->W:LX/Go7;

    const-string v1, "depth_percent"

    iget-object v2, p0, LX/FAa;->V:LX/Cl7;

    invoke-virtual {v2}, LX/Cl7;->b()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2207688
    iget-object v0, p0, LX/FAa;->W:LX/Go7;

    const-string v1, "document_closed"

    iget-object v2, p0, LX/FAa;->aF:LX/Go5;

    invoke-virtual {v2}, LX/Go5;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2207689
    iget-object v0, p0, LX/FAa;->W:LX/Go7;

    const-string v1, "element_dwell_time"

    iget-object v2, p0, LX/FAa;->T:LX/Go4;

    .line 2207690
    new-instance v6, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 2207691
    iget-object v4, v2, LX/Go4;->a:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    .line 2207692
    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Go3;

    .line 2207693
    iget-wide v10, v4, LX/Go3;->e:D

    move-wide v8, v10

    .line 2207694
    const-wide v10, 0x408f400000000000L    # 1000.0

    div-double v10, v8, v10

    move-wide v8, v10

    .line 2207695
    invoke-virtual {v6, v5, v8, v9}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    goto :goto_0

    .line 2207696
    :cond_2
    move-object v2, v6

    .line 2207697
    invoke-virtual {v0, v1, v2}, LX/Go7;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2207698
    iget-object v0, p0, LX/FAa;->ae:LX/ClD;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/ClD;->c(Landroid/content/Context;)V

    .line 2207699
    return-void
.end method

.method private a(LX/0Px;Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$FooterElementsModel;",
            ">;",
            "Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2207700
    new-instance v0, LX/FAg;

    .line 2207701
    iget-object v1, p0, LX/Chc;->D:Landroid/view/View;

    move-object v1, v1

    .line 2207702
    invoke-virtual {p2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->n()Z

    move-result v2

    invoke-direct {v0, v1, v2}, LX/FAg;-><init>(Landroid/view/View;Z)V

    iput-object v0, p0, LX/FAa;->aq:LX/FAg;

    .line 2207703
    iget-object v0, p0, LX/FAa;->aq:LX/FAg;

    const/4 v3, 0x1

    .line 2207704
    iget-object v1, v0, LX/FAg;->f:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 2207705
    invoke-virtual {p2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->k()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$DocumentBodyElementsModel;

    move-result-object v1

    .line 2207706
    if-nez v1, :cond_0

    .line 2207707
    :goto_0
    return-void

    .line 2207708
    :cond_0
    invoke-static {v1}, LX/GoK;->a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$DocumentBodyElementsModel;)Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;

    move-result-object v2

    .line 2207709
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->M()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel$ElementDescriptorModel;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->M()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel$ElementDescriptorModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel$ElementDescriptorModel;->a()Ljava/lang/String;

    move-result-object v1

    .line 2207710
    :goto_1
    if-nez v1, :cond_b

    .line 2207711
    const/4 v1, 0x0

    .line 2207712
    :cond_1
    :goto_2
    move-object p0, v1

    .line 2207713
    const/4 v4, 0x0

    .line 2207714
    if-nez p0, :cond_c

    .line 2207715
    :cond_2
    :goto_3
    move v4, v4

    .line 2207716
    if-eqz v4, :cond_a

    .line 2207717
    iget-object v4, v0, LX/FAg;->a:Landroid/view/View;

    const p2, 0x7f0d1820

    invoke-virtual {v4, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, v0, LX/FAg;->c:Landroid/widget/LinearLayout;

    .line 2207718
    invoke-static {v0}, LX/FAg;->a(LX/FAg;)V

    .line 2207719
    :goto_4
    if-eqz p0, :cond_3

    .line 2207720
    iget-object v4, v0, LX/FAg;->c:Landroid/widget/LinearLayout;

    new-instance p2, Landroid/graphics/drawable/ColorDrawable;

    invoke-static {p0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p0

    invoke-direct {p2, p0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v4, p2}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2207721
    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 2207722
    :cond_4
    const/4 v1, 0x0

    .line 2207723
    :goto_5
    move v1, v1

    .line 2207724
    if-eqz v1, :cond_6

    .line 2207725
    invoke-static {v0, v3}, LX/FAg;->b(LX/FAg;Z)V

    goto :goto_0

    .line 2207726
    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    .line 2207727
    :cond_6
    if-eqz v2, :cond_7

    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->F()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2207728
    :cond_7
    invoke-static {v0}, LX/FAg;->b(LX/FAg;)V

    .line 2207729
    invoke-static {v0}, LX/FAg;->a(LX/FAg;)V

    .line 2207730
    const/4 v1, 0x0

    invoke-static {v0, v1}, LX/FAg;->b(LX/FAg;Z)V

    goto :goto_0

    .line 2207731
    :cond_8
    invoke-static {v0, v3}, LX/FAg;->b(LX/FAg;Z)V

    .line 2207732
    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->F()LX/0Px;

    move-result-object v1

    .line 2207733
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    if-lez v2, :cond_9

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingFooterElementFragmentModel$FooterElementsModel;

    .line 2207734
    sget-object v3, LX/FAf;->a:[I

    invoke-interface {v2}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2207735
    new-instance v2, LX/GoP;

    invoke-direct {v2}, LX/GoP;-><init>()V

    .line 2207736
    iput-object v1, v2, LX/GoP;->b:LX/0Px;

    .line 2207737
    invoke-static {v0, v2}, LX/FAg;->a(LX/FAg;LX/GoP;)V

    .line 2207738
    :cond_9
    :goto_6
    goto/16 :goto_0

    .line 2207739
    :cond_a
    iget-object v4, v0, LX/FAg;->a:Landroid/view/View;

    const p2, 0x7f0d181f

    invoke-virtual {v4, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, v0, LX/FAg;->c:Landroid/widget/LinearLayout;

    .line 2207740
    invoke-static {v0}, LX/FAg;->b(LX/FAg;)V

    goto :goto_4

    :cond_b
    const-string v4, "#"

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string p0, "#"

    invoke-direct {v4, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_2

    .line 2207741
    :cond_c
    invoke-static {p0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p2

    .line 2207742
    invoke-static {p2}, Landroid/graphics/Color;->alpha(I)I

    move-result p2

    int-to-float p2, p2

    .line 2207743
    const/high16 v1, 0x437f0000    # 255.0f

    cmpg-float p2, p2, v1

    if-gez p2, :cond_2

    const/4 v4, 0x1

    goto/16 :goto_3

    .line 2207744
    :cond_d
    new-instance v1, LX/GoP;

    invoke-direct {v1}, LX/GoP;-><init>()V

    .line 2207745
    iput-object p1, v1, LX/GoP;->a:LX/0Px;

    .line 2207746
    invoke-static {v0, v1}, LX/FAg;->a(LX/FAg;LX/GoP;)V

    .line 2207747
    const/4 v1, 0x1

    goto/16 :goto_5

    .line 2207748
    :pswitch_0
    iget-object v3, v0, LX/FAg;->c:Landroid/widget/LinearLayout;

    .line 2207749
    new-instance v4, Lcom/facebook/instantshopping/view/block/impl/FooterImageBlockViewImpl;

    invoke-direct {v4, v3}, Lcom/facebook/instantshopping/view/block/impl/FooterImageBlockViewImpl;-><init>(Landroid/view/View;)V

    move-object v3, v4

    .line 2207750
    new-instance v4, LX/Gpc;

    invoke-direct {v4, v3, v2}, LX/Gpc;-><init>(LX/Gpv;LX/CHi;)V

    .line 2207751
    invoke-virtual {v3, v4}, LX/Cod;->a(LX/CnT;)V

    .line 2207752
    iget-object v3, v0, LX/FAg;->d:LX/Gpd;

    invoke-static {}, LX/GoK;->a()LX/Gp5;

    invoke-virtual {v3}, LX/Gpd;->b()V

    .line 2207753
    goto :goto_6

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method private static a(LX/FAa;LX/Chv;LX/Gr9;LX/GrE;LX/8Yg;LX/8Yh;LX/193;LX/Go0;LX/Go1;LX/0Ot;Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;LX/Gny;LX/0Uh;LX/GnF;LX/CIe;LX/CIZ;LX/CIb;LX/2yr;LX/Go2;LX/Go4;LX/CIi;LX/Cl7;LX/Go7;LX/3kp;LX/GoA;LX/GoD;LX/Gn2;LX/0ad;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0if;LX/ClD;LX/0Ot;LX/0Ot;LX/Gn1;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/FAa;",
            "LX/Chv;",
            "LX/Gr9;",
            "LX/GrE;",
            "LX/8Yg;",
            "LX/8Yh;",
            "LX/193;",
            "LX/Go0;",
            "LX/Go1;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;",
            "LX/Gny;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/GnF;",
            "LX/CIe;",
            "LX/CIZ;",
            "LX/CIb;",
            "LX/2yr;",
            "LX/Go2;",
            "LX/Go4;",
            "LX/CIi;",
            "LX/Cl7;",
            "LX/Go7;",
            "LX/3kp;",
            "LX/GoA;",
            "LX/GoD;",
            "LX/Gn2;",
            "LX/0ad;",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "Lcom/facebook/funnellogger/FunnelLogger;",
            "LX/ClD;",
            "LX/0Ot",
            "<",
            "LX/Chi;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/Gn5;",
            ">;",
            "LX/Gn1;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2207754
    iput-object p1, p0, LX/FAa;->B:LX/Chv;

    iput-object p2, p0, LX/FAa;->C:LX/Gr9;

    iput-object p3, p0, LX/FAa;->D:LX/GrE;

    iput-object p4, p0, LX/FAa;->E:LX/8Yg;

    iput-object p5, p0, LX/FAa;->F:LX/8Yh;

    iput-object p6, p0, LX/FAa;->G:LX/193;

    iput-object p7, p0, LX/FAa;->H:LX/Go0;

    iput-object p8, p0, LX/FAa;->I:LX/Go1;

    iput-object p9, p0, LX/FAa;->J:LX/0Ot;

    iput-object p10, p0, LX/FAa;->K:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    iput-object p11, p0, LX/FAa;->L:LX/Gny;

    iput-object p12, p0, LX/FAa;->M:LX/0Uh;

    iput-object p13, p0, LX/FAa;->N:LX/GnF;

    iput-object p14, p0, LX/FAa;->O:LX/CIe;

    move-object/from16 v0, p15

    iput-object v0, p0, LX/FAa;->P:LX/CIZ;

    move-object/from16 v0, p16

    iput-object v0, p0, LX/FAa;->Q:LX/CIb;

    move-object/from16 v0, p17

    iput-object v0, p0, LX/FAa;->R:LX/2yr;

    move-object/from16 v0, p18

    iput-object v0, p0, LX/FAa;->S:LX/Go2;

    move-object/from16 v0, p19

    iput-object v0, p0, LX/FAa;->T:LX/Go4;

    move-object/from16 v0, p20

    iput-object v0, p0, LX/FAa;->U:LX/CIi;

    move-object/from16 v0, p21

    iput-object v0, p0, LX/FAa;->V:LX/Cl7;

    move-object/from16 v0, p22

    iput-object v0, p0, LX/FAa;->W:LX/Go7;

    move-object/from16 v0, p23

    iput-object v0, p0, LX/FAa;->X:LX/3kp;

    move-object/from16 v0, p24

    iput-object v0, p0, LX/FAa;->Y:LX/GoA;

    move-object/from16 v0, p25

    iput-object v0, p0, LX/FAa;->Z:LX/GoD;

    move-object/from16 v0, p26

    iput-object v0, p0, LX/FAa;->aa:LX/Gn2;

    move-object/from16 v0, p27

    iput-object v0, p0, LX/FAa;->ab:LX/0ad;

    move-object/from16 v0, p28

    iput-object v0, p0, LX/FAa;->ac:Lcom/facebook/prefs/shared/FbSharedPreferences;

    move-object/from16 v0, p29

    iput-object v0, p0, LX/FAa;->ad:LX/0if;

    move-object/from16 v0, p30

    iput-object v0, p0, LX/FAa;->ae:LX/ClD;

    move-object/from16 v0, p31

    iput-object v0, p0, LX/FAa;->af:LX/0Ot;

    move-object/from16 v0, p32

    iput-object v0, p0, LX/FAa;->ag:LX/0Ot;

    move-object/from16 v0, p33

    iput-object v0, p0, LX/FAa;->ah:LX/Gn1;

    return-void
.end method

.method public static a(LX/FAa;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2207755
    invoke-static {p1}, LX/CIa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2207756
    const/4 v0, 0x0

    .line 2207757
    if-nez v1, :cond_3

    .line 2207758
    :cond_0
    :goto_0
    move v0, v0

    .line 2207759
    if-eqz v0, :cond_2

    .line 2207760
    iget-object v0, p0, LX/Chc;->D:Landroid/view/View;

    move-object v0, v0

    .line 2207761
    const v2, 0x7f0d188c

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/FAa;->am:Landroid/view/ViewGroup;

    .line 2207762
    invoke-static {p0}, LX/FAa;->S(LX/FAa;)V

    .line 2207763
    :goto_1
    iget-object v0, p0, LX/FAa;->am:Landroid/view/ViewGroup;

    const v2, 0x7f0d1883

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/text/BetterTextView;

    iput-object v0, p0, LX/FAa;->ak:Landroid/widget/TextView;

    .line 2207764
    if-eqz v1, :cond_1

    .line 2207765
    iget-object v0, p0, LX/FAa;->am:Landroid/view/ViewGroup;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-direct {v2, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2207766
    :cond_1
    return-void

    .line 2207767
    :cond_2
    iget-object v0, p0, LX/Chc;->D:Landroid/view/View;

    move-object v0, v0

    .line 2207768
    const v2, 0x7f0d0541

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, LX/FAa;->am:Landroid/view/ViewGroup;

    .line 2207769
    invoke-static {p0}, LX/FAa;->T(LX/FAa;)V

    goto :goto_1

    .line 2207770
    :cond_3
    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v2

    .line 2207771
    invoke-static {v2}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    int-to-float v2, v2

    .line 2207772
    const/high16 p1, 0x437f0000    # 255.0f

    cmpg-float v2, v2, p1

    if-gez v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2207773
    iget-object v1, p0, LX/FAa;->ab:LX/0ad;

    sget v2, LX/CHN;->n:I

    invoke-interface {v1, v2, v0}, LX/0ad;->a(II)I

    move-result v2

    .line 2207774
    if-nez v2, :cond_1

    .line 2207775
    :cond_0
    return-void

    .line 2207776
    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->k()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$DocumentBodyElementsModel;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2207777
    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->k()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$DocumentBodyElementsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$DocumentBodyElementsModel;->a()LX/0Px;

    move-result-object v3

    move v1, v0

    .line 2207778
    :goto_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    if-ge v1, v2, :cond_0

    .line 2207779
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel;->j()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;

    move-result-object v0

    .line 2207780
    invoke-interface {v0}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    if-eq v4, v5, :cond_2

    invoke-interface {v0}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v4

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->BUTTON:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    if-ne v4, v5, :cond_3

    :cond_2
    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->P()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 2207781
    iget-object v4, p0, LX/FAa;->R:LX/2yr;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->P()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, v5, p1}, LX/2yr;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2207782
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 36

    invoke-static/range {p1 .. p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v35

    move-object/from16 v2, p0

    check-cast v2, LX/FAa;

    invoke-static/range {v35 .. v35}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v3

    check-cast v3, LX/Chv;

    invoke-static/range {v35 .. v35}, LX/Gr9;->a(LX/0QB;)LX/Gr9;

    move-result-object v4

    check-cast v4, LX/Gr9;

    invoke-static/range {v35 .. v35}, LX/GrE;->a(LX/0QB;)LX/GrE;

    move-result-object v5

    check-cast v5, LX/GrE;

    invoke-static/range {v35 .. v35}, LX/8Yg;->a(LX/0QB;)LX/8Yg;

    move-result-object v6

    check-cast v6, LX/8Yg;

    invoke-static/range {v35 .. v35}, LX/8Yh;->a(LX/0QB;)LX/8Yh;

    move-result-object v7

    check-cast v7, LX/8Yh;

    const-class v8, LX/193;

    move-object/from16 v0, v35

    invoke-interface {v0, v8}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/193;

    invoke-static/range {v35 .. v35}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object v9

    check-cast v9, LX/Go0;

    invoke-static/range {v35 .. v35}, LX/Go1;->a(LX/0QB;)LX/Go1;

    move-result-object v10

    check-cast v10, LX/Go1;

    const/16 v11, 0x259

    move-object/from16 v0, v35

    invoke-static {v0, v11}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v11

    invoke-static/range {v35 .. v35}, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->a(LX/0QB;)Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    move-result-object v12

    check-cast v12, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    invoke-static/range {v35 .. v35}, LX/Gny;->a(LX/0QB;)LX/Gny;

    move-result-object v13

    check-cast v13, LX/Gny;

    invoke-static/range {v35 .. v35}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v14

    check-cast v14, LX/0Uh;

    invoke-static/range {v35 .. v35}, LX/GnF;->a(LX/0QB;)LX/GnF;

    move-result-object v15

    check-cast v15, LX/GnF;

    invoke-static/range {v35 .. v35}, LX/CIe;->a(LX/0QB;)LX/CIe;

    move-result-object v16

    check-cast v16, LX/CIe;

    invoke-static/range {v35 .. v35}, LX/CIZ;->a(LX/0QB;)LX/CIZ;

    move-result-object v17

    check-cast v17, LX/CIZ;

    invoke-static/range {v35 .. v35}, LX/CIb;->a(LX/0QB;)LX/CIb;

    move-result-object v18

    check-cast v18, LX/CIb;

    invoke-static/range {v35 .. v35}, LX/2yr;->a(LX/0QB;)LX/2yr;

    move-result-object v19

    check-cast v19, LX/2yr;

    invoke-static/range {v35 .. v35}, LX/Go2;->a(LX/0QB;)LX/Go2;

    move-result-object v20

    check-cast v20, LX/Go2;

    invoke-static/range {v35 .. v35}, LX/Go4;->a(LX/0QB;)LX/Go4;

    move-result-object v21

    check-cast v21, LX/Go4;

    invoke-static/range {v35 .. v35}, LX/CIi;->a(LX/0QB;)LX/CIi;

    move-result-object v22

    check-cast v22, LX/CIi;

    invoke-static/range {v35 .. v35}, LX/Cl7;->a(LX/0QB;)LX/Cl7;

    move-result-object v23

    check-cast v23, LX/Cl7;

    invoke-static/range {v35 .. v35}, LX/Go7;->a(LX/0QB;)LX/Go7;

    move-result-object v24

    check-cast v24, LX/Go7;

    invoke-static/range {v35 .. v35}, LX/3kp;->a(LX/0QB;)LX/3kp;

    move-result-object v25

    check-cast v25, LX/3kp;

    invoke-static/range {v35 .. v35}, LX/GoA;->a(LX/0QB;)LX/GoA;

    move-result-object v26

    check-cast v26, LX/GoA;

    invoke-static/range {v35 .. v35}, LX/GoD;->a(LX/0QB;)LX/GoD;

    move-result-object v27

    check-cast v27, LX/GoD;

    invoke-static/range {v35 .. v35}, LX/Gn2;->a(LX/0QB;)LX/Gn2;

    move-result-object v28

    check-cast v28, LX/Gn2;

    invoke-static/range {v35 .. v35}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v29

    check-cast v29, LX/0ad;

    invoke-static/range {v35 .. v35}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v30

    check-cast v30, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static/range {v35 .. v35}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v31

    check-cast v31, LX/0if;

    invoke-static/range {v35 .. v35}, LX/ClD;->a(LX/0QB;)LX/ClD;

    move-result-object v32

    check-cast v32, LX/ClD;

    const/16 v33, 0x31dc

    move-object/from16 v0, v35

    move/from16 v1, v33

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v33

    const/16 v34, 0x252d

    move-object/from16 v0, v35

    move/from16 v1, v34

    invoke-static {v0, v1}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v34

    invoke-static/range {v35 .. v35}, LX/Gn1;->b(LX/0QB;)LX/Gn1;

    move-result-object v35

    check-cast v35, LX/Gn1;

    invoke-static/range {v2 .. v35}, LX/FAa;->a(LX/FAa;LX/Chv;LX/Gr9;LX/GrE;LX/8Yg;LX/8Yh;LX/193;LX/Go0;LX/Go1;LX/0Ot;Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;LX/Gny;LX/0Uh;LX/GnF;LX/CIe;LX/CIZ;LX/CIb;LX/2yr;LX/Go2;LX/Go4;LX/CIi;LX/Cl7;LX/Go7;LX/3kp;LX/GoA;LX/GoD;LX/Gn2;LX/0ad;Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0if;LX/ClD;LX/0Ot;LX/0Ot;LX/Gn1;)V

    return-void
.end method

.method private static a(LX/1a1;)Z
    .locals 1

    .prologue
    .line 2207784
    if-eqz p0, :cond_0

    instance-of v0, p0, LX/Cs4;

    if-eqz v0, :cond_0

    check-cast p0, LX/Cs4;

    invoke-virtual {p0}, LX/Cs4;->w()LX/CnT;

    move-result-object v0

    instance-of v0, v0, LX/Gpf;

    if-eqz v0, :cond_0

    .line 2207785
    const/4 v0, 0x1

    .line 2207786
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static aa(LX/FAa;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2207835
    iget-object v0, p0, LX/FAa;->ay:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2207836
    iget-object v0, p0, LX/FAa;->ay:Ljava/lang/String;

    .line 2207837
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/FAa;->au:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/FAa;->at:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, LX/FAa;->au:Ljava/lang/String;

    goto :goto_0
.end method

.method private ab()V
    .locals 2

    .prologue
    .line 2207830
    iget-object v0, p0, LX/FAa;->Q:LX/CIb;

    invoke-static {p0}, LX/FAa;->aa(LX/FAa;)Ljava/lang/String;

    move-result-object v1

    .line 2207831
    iput-object v1, v0, LX/CIb;->a:Ljava/lang/String;

    .line 2207832
    iget-object v0, p0, LX/FAa;->Q:LX/CIb;

    iget-object v1, p0, LX/FAa;->at:Ljava/lang/String;

    .line 2207833
    iput-object v1, v0, LX/CIb;->b:Ljava/lang/String;

    .line 2207834
    return-void
.end method

.method private ag()V
    .locals 3

    .prologue
    .line 2207823
    invoke-static {p0}, LX/FAa;->ah(LX/FAa;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2207824
    :goto_0
    return-void

    .line 2207825
    :cond_0
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2207826
    check-cast v0, Lcom/facebook/instantshopping/view/widget/InstantShoppingRecyclerView;

    iget-object v1, p0, LX/FAa;->ag:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Gn5;

    iget-object v2, p0, LX/FAa;->aw:LX/0lF;

    .line 2207827
    new-instance p0, LX/Gn4;

    invoke-direct {p0, v1, v2}, LX/Gn4;-><init>(LX/Gn5;LX/0lF;)V

    move-object v1, p0

    .line 2207828
    iput-object v1, v0, Lcom/facebook/instantshopping/view/widget/InstantShoppingRecyclerView;->n:LX/Gn3;

    .line 2207829
    goto :goto_0
.end method

.method private static ah(LX/FAa;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2207822
    iget-object v1, p0, LX/FAa;->aB:LX/0Px;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->ENABLE_SWIPE_TO_OPEN:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    invoke-virtual {v1, v2}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/FAa;->ab:LX/0ad;

    sget-short v2, LX/CHN;->g:S

    invoke-interface {v1, v2, v0}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(LX/GoH;)V
    .locals 2

    .prologue
    .line 2207810
    iget-object v0, p1, LX/GoH;->e:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;

    move-object v0, v0

    .line 2207811
    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2207812
    if-nez v0, :cond_0

    .line 2207813
    :goto_0
    return-void

    .line 2207814
    :cond_0
    invoke-static {v0}, LX/CIa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2207815
    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    .line 2207816
    iget-object v1, p0, LX/FAa;->an:LX/Gmy;

    .line 2207817
    iput v0, v1, LX/Gmy;->b:I

    .line 2207818
    iget-object v1, p0, LX/FAa;->al:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    .line 2207819
    iget-object v1, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v1, v1

    .line 2207820
    new-instance p1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {p1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, p1}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2207821
    goto :goto_0
.end method

.method public static d(LX/FAa;I)V
    .locals 3

    .prologue
    .line 2207806
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2207807
    check-cast v0, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;

    .line 2207808
    new-instance v1, LX/Ct6;

    new-instance v2, LX/FAN;

    invoke-direct {v2, p0}, LX/FAN;-><init>(LX/FAa;)V

    invoke-direct {v1, p1, v2}, LX/Ct6;-><init>(ILX/Ct5;)V

    invoke-virtual {v0, v1}, Lcom/facebook/richdocument/view/widget/RichDocumentRecyclerView;->a(LX/Ct6;)V

    .line 2207809
    return-void
.end method

.method public static e(LX/FAa;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2207799
    invoke-static {p1}, LX/FAa;->i(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/FAa;->ay:Ljava/lang/String;

    .line 2207800
    invoke-static {p1}, LX/FAa;->f(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/FAa;->at:Ljava/lang/String;

    .line 2207801
    invoke-static {p1}, LX/FAa;->g(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/FAa;->ax:Ljava/lang/String;

    .line 2207802
    invoke-static {p1}, LX/FAa;->k(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/FAa;->au:Ljava/lang/String;

    .line 2207803
    const-string v0, "extra_instant_shopping_product_view"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v0, v0

    .line 2207804
    iput-object v0, p0, LX/FAa;->av:Ljava/lang/String;

    .line 2207805
    return-void
.end method

.method private static f(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2207798
    const-string v0, "extra_instant_shopping_catalog_id"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static g(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2207783
    const-string v0, "extra_instant_shopping_catalog_view"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static h(Landroid/os/Bundle;)LX/0lF;
    .locals 4

    .prologue
    .line 2207790
    const-string v0, "tracking_codes"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2207791
    if-nez v0, :cond_0

    .line 2207792
    const/4 v0, 0x0

    .line 2207793
    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    invoke-static {}, LX/0lB;->i()LX/0lB;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/0lC;->a(Ljava/lang/String;)LX/0lF;
    :try_end_0
    .catch LX/28F; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 2207794
    :catch_0
    move-exception v0

    .line 2207795
    new-instance v1, Landroid/os/ParcelFormatException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to process event "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, LX/28F;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/ParcelFormatException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2207796
    :catch_1
    move-exception v0

    .line 2207797
    new-instance v1, Landroid/os/ParcelFormatException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to process event "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/ParcelFormatException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static i(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2207789
    const-string v0, "extra_native_document_id"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static k(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2207788
    const-string v0, "extra_instant_shopping_product_id"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final A()LX/3x6;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2207787
    iget-object v0, p0, LX/FAa;->an:LX/Gmy;

    return-object v0
.end method

.method public final D()LX/CH4;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/richdocument/fetcher/RichDocumentFetcher",
            "<",
            "LX/0zO",
            "<TT;>;",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 2207593
    iget-object v0, p0, LX/FAa;->K:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    return-object v0
.end method

.method public final E()LX/CGs;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/CGs",
            "<",
            "LX/0zO",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 2207594
    new-instance v0, LX/CId;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/FAa;->at:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, LX/CId;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 2207595
    iget-object v1, p0, LX/FAa;->ay:Ljava/lang/String;

    .line 2207596
    iput-object v1, v0, LX/CId;->d:Ljava/lang/String;

    .line 2207597
    iget-object v1, p0, LX/FAa;->au:Ljava/lang/String;

    .line 2207598
    iput-object v1, v0, LX/CId;->a:Ljava/lang/String;

    .line 2207599
    iget-object v1, p0, LX/FAa;->av:Ljava/lang/String;

    .line 2207600
    iput-object v1, v0, LX/CId;->b:Ljava/lang/String;

    .line 2207601
    iget-object v1, p0, LX/FAa;->ax:Ljava/lang/String;

    .line 2207602
    iput-object v1, v0, LX/CId;->f:Ljava/lang/String;

    .line 2207603
    iget-object v1, p0, LX/FAa;->ay:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2207604
    iget-object v1, p0, LX/FAa;->L:LX/Gny;

    const-string v2, "instant_shopping_catalog_activity"

    invoke-virtual {v1, v2}, LX/Gnx;->b(Ljava/lang/String;)V

    .line 2207605
    :cond_0
    return-object v0
.end method

.method public final F()V
    .locals 1

    .prologue
    .line 2207073
    invoke-super {p0}, LX/Chc;->F()V

    .line 2207074
    iget-object v0, p0, LX/FAa;->aA:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->a()V

    .line 2207075
    return-void
.end method

.method public final G()V
    .locals 9

    .prologue
    .line 2207076
    invoke-super {p0}, LX/Chc;->G()V

    .line 2207077
    iget-object v0, p0, LX/FAa;->ar:LX/GoH;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FAa;->aG:LX/Clr;

    if-eqz v0, :cond_0

    .line 2207078
    iget-object v0, p0, LX/FAa;->ar:LX/GoH;

    const/4 v1, 0x0

    iget-object v2, p0, LX/FAa;->aG:LX/Clr;

    invoke-virtual {v0, v1, v2}, LX/Clo;->b(ILX/Clr;)V

    .line 2207079
    :cond_0
    invoke-static {p0}, LX/FAa;->ah(LX/FAa;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2207080
    iget-object v0, p0, LX/FAa;->ar:LX/GoH;

    .line 2207081
    invoke-virtual {v0}, LX/Clo;->d()I

    move-result v2

    .line 2207082
    const/4 v3, 0x0

    .line 2207083
    iget-object v1, v0, LX/GoH;->e:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;

    move-object v1, v1

    .line 2207084
    invoke-virtual {v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->k()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$DocumentBodyElementsModel;

    move-result-object v1

    .line 2207085
    if-nez v1, :cond_5

    move-object v1, v3

    .line 2207086
    :goto_0
    move-object v1, v1

    .line 2207087
    if-nez v1, :cond_1

    if-lez v2, :cond_1

    .line 2207088
    add-int/lit8 v1, v2, -0x1

    invoke-virtual {v0, v1}, LX/Clo;->a(I)LX/Clr;

    move-result-object v1

    .line 2207089
    instance-of v2, v1, LX/GpF;

    if-eqz v2, :cond_4

    .line 2207090
    check-cast v1, LX/GpF;

    invoke-virtual {v1}, LX/GpF;->a()LX/Clo;

    move-result-object v1

    .line 2207091
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1}, LX/Clo;->d()I

    move-result v3

    if-ge v2, v3, :cond_b

    .line 2207092
    invoke-virtual {v1, v2}, LX/Clo;->a(I)LX/Clr;

    move-result-object v3

    invoke-static {v3}, LX/GoK;->a(LX/Clr;)LX/GpO;

    move-result-object v3

    .line 2207093
    if-eqz v3, :cond_a

    move-object v2, v3

    .line 2207094
    :goto_2
    move-object v1, v2

    .line 2207095
    :cond_1
    :goto_3
    move-object v1, v1

    .line 2207096
    if-eqz v1, :cond_3

    .line 2207097
    iget-object v0, p0, LX/FAa;->ag:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gn5;

    .line 2207098
    iput-object v1, v0, LX/Gn5;->f:LX/GpO;

    .line 2207099
    iget-object v0, p0, LX/FAa;->ar:LX/GoH;

    sget-object v1, LX/GoJ;->SWIPE_TO_OPEN_INDICATOR:LX/GoJ;

    const/4 v2, 0x0

    .line 2207100
    sget-object v3, LX/GoI;->b:[I

    invoke-virtual {v1}, LX/GoJ;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 2207101
    :cond_2
    :goto_4
    move-object v0, v2

    .line 2207102
    if-eqz v0, :cond_3

    .line 2207103
    iget-object v1, p0, LX/FAa;->ar:LX/GoH;

    invoke-virtual {v1, v0}, LX/Clo;->a(LX/Clr;)V

    .line 2207104
    :cond_3
    iget-object v0, p0, LX/FAa;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chi;

    iget-object v1, p0, LX/FAa;->ar:LX/GoH;

    invoke-virtual {v1}, LX/Clo;->d()I

    move-result v1

    .line 2207105
    iput v1, v0, LX/Chi;->l:I

    .line 2207106
    iget-object v0, p0, LX/FAa;->aA:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    invoke-virtual {v0}, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;->b()V

    .line 2207107
    return-void

    .line 2207108
    :cond_4
    invoke-static {v1}, LX/GoK;->a(LX/Clr;)LX/GpO;

    move-result-object v1

    goto :goto_3

    .line 2207109
    :cond_5
    invoke-static {v1}, LX/GoK;->a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$DocumentBodyElementsModel;)Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;

    move-result-object v1

    .line 2207110
    if-nez v1, :cond_6

    move-object v1, v3

    .line 2207111
    goto :goto_0

    .line 2207112
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->F()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v1, 0x0

    move v4, v1

    :goto_5
    if-ge v4, v6, :cond_9

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingFooterElementFragmentModel$FooterElementsModel;

    .line 2207113
    invoke-virtual {v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingFooterElementFragmentModel$FooterElementsModel;->j()LX/CHX;

    move-result-object v7

    .line 2207114
    invoke-static {v7}, LX/Gn5;->a(LX/CHX;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 2207115
    new-instance v3, LX/GpN;

    invoke-direct {v3}, LX/GpN;-><init>()V

    .line 2207116
    iput-object v7, v3, LX/GpN;->b:LX/CHX;

    .line 2207117
    invoke-interface {v1}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v4

    if-eqz v4, :cond_7

    invoke-interface {v1}, LX/CHb;->jt_()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_7

    .line 2207118
    new-instance v4, LX/GoE;

    invoke-interface {v1}, LX/CHb;->jt_()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v5, v1}, LX/GoE;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2207119
    iput-object v4, v3, LX/GpN;->a:LX/GoE;

    .line 2207120
    :cond_7
    invoke-virtual {v3}, LX/GpN;->a()LX/GpO;

    move-result-object v1

    goto/16 :goto_0

    .line 2207121
    :cond_8
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_5

    :cond_9
    move-object v1, v3

    .line 2207122
    goto/16 :goto_0

    .line 2207123
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 2207124
    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 2207125
    :pswitch_0
    iget-object v3, v0, LX/GoH;->e:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;

    move-object v3, v3

    .line 2207126
    invoke-virtual {v3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2207127
    if-eqz v3, :cond_2

    .line 2207128
    new-instance v4, LX/GpG;

    sget v2, LX/CIY;->a:I

    const/16 v5, 0x7b

    invoke-direct {v4, v2, v5}, LX/GpG;-><init>(II)V

    .line 2207129
    invoke-static {v3}, LX/CIa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2207130
    iput-object v2, v4, LX/GpG;->a:Ljava/lang/String;

    .line 2207131
    new-instance v2, LX/GpH;

    invoke-direct {v2, v4}, LX/GpH;-><init>(LX/GpG;)V

    goto/16 :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final I()V
    .locals 3

    .prologue
    .line 2207132
    invoke-super {p0}, LX/Chc;->I()V

    .line 2207133
    iget-object v0, p0, LX/FAa;->B:LX/Chv;

    iget-object v1, p0, LX/FAa;->aJ:LX/GnR;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2207134
    iget-object v0, p0, LX/FAa;->B:LX/Chv;

    iget-object v1, p0, LX/FAa;->aM:LX/GnS;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2207135
    iget-object v0, p0, LX/FAa;->B:LX/Chv;

    iget-object v1, p0, LX/FAa;->aN:LX/GnO;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2207136
    iget-object v0, p0, LX/FAa;->B:LX/Chv;

    iget-object v1, p0, LX/FAa;->aO:LX/GnP;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2207137
    iget-object v0, p0, LX/FAa;->B:LX/Chv;

    iget-object v1, p0, LX/FAa;->aL:LX/GnT;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2207138
    iget-object v0, p0, LX/FAa;->B:LX/Chv;

    iget-object v1, p0, LX/FAa;->aK:LX/GnQ;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2207139
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2207140
    invoke-static {v0}, LX/FAa;->h(Landroid/os/Bundle;)LX/0lF;

    move-result-object v0

    iput-object v0, p0, LX/FAa;->aw:LX/0lF;

    .line 2207141
    iget-object v0, p0, LX/FAa;->Q:LX/CIb;

    iget-object v1, p0, LX/FAa;->aw:LX/0lF;

    .line 2207142
    iput-object v1, v0, LX/CIb;->c:LX/0lF;

    .line 2207143
    invoke-direct {p0}, LX/FAa;->ab()V

    .line 2207144
    iget-object v0, p0, LX/FAa;->X:LX/3kp;

    sget-object v1, LX/Gn0;->d:LX/0Tn;

    invoke-virtual {v0, v1}, LX/3kp;->a(LX/0Tn;)V

    .line 2207145
    iget-object v0, p0, LX/FAa;->L:LX/Gny;

    const-string v1, "instant_shopping_catalog_activity"

    invoke-virtual {v0, v1}, LX/Gnx;->b(Ljava/lang/String;)V

    .line 2207146
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2207147
    invoke-static {p0, v0}, LX/FAa;->e(LX/FAa;Landroid/os/Bundle;)V

    .line 2207148
    iget-object v0, p0, LX/FAa;->ad:LX/0if;

    sget-object v1, LX/0ig;->w:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 2207149
    iget-object v0, p0, LX/FAa;->ad:LX/0if;

    sget-object v1, LX/0ig;->w:LX/0ih;

    invoke-static {p0}, LX/FAa;->aa(LX/FAa;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2207150
    iget-object v0, p0, LX/FAa;->ay:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FAa;->at:Ljava/lang/String;

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2207151
    sget-object v0, LX/Go5;->DOCUMENT_CLOSED_ON_PRELAUNCH:LX/Go5;

    iput-object v0, p0, LX/FAa;->aF:LX/Go5;

    .line 2207152
    invoke-virtual {p0}, LX/Chc;->v()V

    .line 2207153
    :cond_0
    return-void
.end method

.method public final N()V
    .locals 0

    .prologue
    .line 2207154
    return-void
.end method

.method public final O()LX/0Pq;
    .locals 1

    .prologue
    .line 2207155
    sget-object v0, LX/GoC;->a:LX/GoB;

    return-object v0
.end method

.method public final P()I
    .locals 2

    .prologue
    .line 2207156
    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 2207157
    if-eqz v0, :cond_0

    .line 2207158
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    .line 2207159
    mul-int/lit8 v1, v0, 0x64

    div-int/lit8 v1, v1, 0xf

    move v0, v1

    .line 2207160
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)LX/CtQ;
    .locals 5

    .prologue
    .line 2207161
    new-instance v0, LX/Grf;

    iget-object v1, p0, LX/FAa;->U:LX/CIi;

    .line 2207162
    iget-object v2, v1, LX/CIi;->c:LX/0ad;

    sget v3, LX/CHN;->y:I

    const/16 v4, 0x19

    invoke-interface {v2, v3, v4}, LX/0ad;->a(II)I

    move-result v2

    .line 2207163
    int-to-float v2, v2

    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v2, v3

    move v1, v2

    .line 2207164
    iget-object v2, p0, LX/FAa;->U:LX/CIi;

    .line 2207165
    iget-object v3, v2, LX/CIi;->c:LX/0ad;

    sget v4, LX/CHN;->x:I

    const/16 p0, 0x4b

    invoke-interface {v3, v4, p0}, LX/0ad;->a(II)I

    move-result v3

    .line 2207166
    int-to-float v3, v3

    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v3, v4

    move v2, v3

    .line 2207167
    invoke-direct {v0, p1, v1, v2}, LX/Grf;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;FF)V

    return-object v0
.end method

.method public final a(Landroid/view/View;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2207168
    const v0, 0x7f0d05b0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2207169
    const-string v0, "instant_shopping"

    return-object v0
.end method

.method public final a(LX/ChZ;)V
    .locals 4

    .prologue
    .line 2207170
    iget-object v0, p0, LX/FAa;->D:LX/GrE;

    .line 2207171
    sget-object v1, LX/ChZ;->ON:LX/ChZ;

    if-ne p1, v1, :cond_1

    const/4 v1, 0x1

    move v2, v1

    .line 2207172
    :goto_0
    iput-boolean v2, v0, LX/GrE;->t:Z

    .line 2207173
    iget-object v1, v0, LX/GrE;->u:Lcom/facebook/widget/PhotoToggleButton;

    if-eqz v1, :cond_0

    .line 2207174
    iget-object v1, v0, LX/GrE;->u:Lcom/facebook/widget/PhotoToggleButton;

    invoke-virtual {v1, v2}, Lcom/facebook/widget/PhotoToggleButton;->setChecked(Z)V

    .line 2207175
    iget-object v1, v0, LX/GrE;->d:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    sget-object v1, LX/Gn0;->f:LX/0Tn;

    iget-object p0, v0, LX/GrE;->l:Ljava/lang/String;

    invoke-virtual {v1, p0}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v1

    check-cast v1, LX/0Tn;

    invoke-interface {v3, v1, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v1

    invoke-interface {v1}, LX/0hN;->commit()V

    .line 2207176
    :cond_0
    return-void

    .line 2207177
    :cond_1
    const/4 v1, 0x0

    move v2, v1

    goto :goto_0
.end method

.method public final a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 7
    .param p2    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2207178
    invoke-super {p0, p1, p2}, LX/Chc;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 2207179
    iget-object v0, p0, LX/FAa;->Y:LX/GoA;

    .line 2207180
    iget-object v1, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v1, v1

    .line 2207181
    const-string v3, "click_time"

    const-wide/16 v5, 0x0

    invoke-virtual {v1, v3, v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, v0, LX/GoA;->e:J

    .line 2207182
    iget-object v0, p0, LX/FAa;->ag:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gn5;

    invoke-virtual {p0}, LX/Chc;->K()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, LX/FAa;->aI:LX/Gr7;

    .line 2207183
    iput-object v1, v0, LX/Gn5;->d:Landroid/app/Activity;

    .line 2207184
    iput-object v2, v0, LX/Gn5;->e:LX/1P1;

    .line 2207185
    iget-object v0, p0, LX/FAa;->M:LX/0Uh;

    const/16 v1, 0x26

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2207186
    invoke-direct {p0}, LX/FAa;->Q()V

    .line 2207187
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)LX/1P1;
    .locals 3

    .prologue
    .line 2207188
    new-instance v0, LX/Gr7;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2207189
    const/4 v2, 0x4

    move v2, v2

    .line 2207190
    invoke-direct {v0, v1, p1, v2}, LX/Gr7;-><init>(Landroid/content/Context;Landroid/support/v7/widget/RecyclerView;I)V

    iput-object v0, p0, LX/FAa;->aI:LX/Gr7;

    .line 2207191
    iget-object v0, p0, LX/FAa;->aI:LX/Gr7;

    .line 2207192
    new-instance v1, LX/FAY;

    invoke-direct {v1, p0}, LX/FAY;-><init>(LX/FAa;)V

    move-object v1, v1

    .line 2207193
    iput-object v1, v0, LX/3wu;->h:LX/3wr;

    .line 2207194
    iget-object v0, p0, LX/FAa;->aI:LX/Gr7;

    return-object v0
.end method

.method public final b(Ljava/lang/Object;)LX/Clo;
    .locals 7

    .prologue
    .line 2207195
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2207196
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2207197
    invoke-static {v0}, LX/CIc;->a(Ljava/lang/Object;)Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;

    move-result-object v1

    .line 2207198
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->k()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$DocumentBodyElementsModel;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2207199
    :cond_0
    const/4 v0, 0x0

    .line 2207200
    :goto_0
    return-object v0

    .line 2207201
    :cond_1
    invoke-virtual {v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->k()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$DocumentBodyElementsModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$DocumentBodyElementsModel;->a()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    .line 2207202
    const-string v3, "instant_shopping_num_blocks_fetched"

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v2

    .line 2207203
    iget-object v3, p0, LX/FAa;->L:LX/Gny;

    const-string v4, "instant_shopping_graphql_first_fetch"

    invoke-virtual {v3, v4, v2}, LX/Gnx;->b(Ljava/lang/String;LX/0P1;)V

    .line 2207204
    iget-object v3, p0, LX/FAa;->L:LX/Gny;

    const-string v4, "instant_shopping_first_parse"

    invoke-virtual {v3, v4, v2}, LX/Gnx;->a(Ljava/lang/String;LX/0P1;)V

    .line 2207205
    invoke-static {v1}, LX/GoK;->a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;)LX/GoH;

    move-result-object v2

    iput-object v2, p0, LX/FAa;->ar:LX/GoH;

    .line 2207206
    invoke-virtual {v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->o()LX/0Px;

    move-result-object v2

    iput-object v2, p0, LX/FAa;->aB:LX/0Px;

    .line 2207207
    invoke-virtual {v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->q()Ljava/lang/String;

    move-result-object v2

    .line 2207208
    iget-object v3, p0, LX/FAa;->ar:LX/GoH;

    .line 2207209
    iput-object v2, v3, LX/GoH;->b:Ljava/lang/String;

    .line 2207210
    iget-object v2, p0, LX/FAa;->ar:LX/GoH;

    .line 2207211
    invoke-static {v0}, LX/CIc;->a(Ljava/lang/Object;)Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;

    move-result-object v3

    iput-object v3, v2, LX/GoH;->e:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;

    .line 2207212
    invoke-static {v0}, LX/CIc;->a(Ljava/lang/Object;)Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->l()LX/0Px;

    move-result-object v3

    move-object v3, v3

    .line 2207213
    iput-object v3, v2, LX/GoH;->d:LX/0Px;

    .line 2207214
    invoke-static {v0}, LX/CIc;->a(Ljava/lang/Object;)Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->m()LX/2uF;

    move-result-object v3

    move-object v3, v3

    .line 2207215
    iput-object v3, v2, LX/GoH;->c:LX/2uF;

    .line 2207216
    invoke-static {v0}, LX/CIc;->a(Ljava/lang/Object;)Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->j()LX/2uF;

    move-result-object v3

    move-object v3, v3

    .line 2207217
    iput-object v3, v2, LX/GoH;->f:LX/2uF;

    .line 2207218
    iget-object v0, p0, LX/FAa;->L:LX/Gny;

    const-string v2, "instant_shopping_first_parse"

    invoke-virtual {v0, v2}, LX/Gnx;->b(Ljava/lang/String;)V

    .line 2207219
    iget-object v0, p0, LX/FAa;->B:LX/Chv;

    new-instance v2, LX/CiP;

    iget-object v3, p0, LX/FAa;->ar:LX/GoH;

    iget-object v4, p1, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    invoke-direct {v2, v3, v4}, LX/CiP;-><init>(LX/Clo;LX/0ta;)V

    invoke-virtual {v0, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2207220
    invoke-direct {p0, v1}, LX/FAa;->a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;)V

    .line 2207221
    invoke-virtual {v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->p()LX/0Px;

    move-result-object v0

    .line 2207222
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v3

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_2

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2207223
    iget-object v4, p0, LX/FAa;->N:LX/GnF;

    .line 2207224
    iget-object v5, v4, LX/GnF;->d:LX/0Sh;

    new-instance v6, Lcom/facebook/instantshopping/action/InstantShoppingActionUtils$GetRequestAsyncTask;

    new-instance p1, LX/GnB;

    invoke-direct {p1, v4, v1}, LX/GnB;-><init>(LX/GnF;Ljava/lang/String;)V

    invoke-direct {v6, v4, v1, p1}, Lcom/facebook/instantshopping/action/InstantShoppingActionUtils$GetRequestAsyncTask;-><init>(LX/GnF;Ljava/lang/String;LX/0TF;)V

    const/4 p1, 0x0

    new-array p1, p1, [Ljava/lang/Void;

    invoke-virtual {v5, v6, p1}, LX/0Sh;->a(LX/3nE;[Ljava/lang/Object;)LX/3nE;

    .line 2207225
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2207226
    :cond_2
    invoke-static {p0}, LX/FAa;->X(LX/FAa;)V

    .line 2207227
    invoke-static {p0}, LX/FAa;->aa(LX/FAa;)Ljava/lang/String;

    move-result-object v0

    .line 2207228
    iget-object v1, p0, LX/FAa;->O:LX/CIe;

    invoke-virtual {v1, v0}, LX/CIe;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2207229
    iget-object v1, p0, LX/FAa;->O:LX/CIe;

    invoke-virtual {v1, v0}, LX/CIe;->a(Ljava/lang/String;)Z

    move-result v0

    .line 2207230
    iget-object v1, p0, LX/FAa;->aj:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/GpR;

    .line 2207231
    instance-of v3, v1, LX/Gpq;

    if-eqz v3, :cond_3

    .line 2207232
    check-cast v1, LX/Gpq;

    .line 2207233
    iget-object v3, v1, LX/CnT;->d:LX/CnG;

    move-object v3, v3

    .line 2207234
    check-cast v3, LX/Gql;

    invoke-virtual {v3, v0}, LX/Gql;->a(Z)V

    .line 2207235
    goto :goto_2

    .line 2207236
    :cond_4
    iget-object v1, p0, LX/FAa;->aq:LX/FAg;

    if-eqz v1, :cond_5

    .line 2207237
    iget-object v1, p0, LX/FAa;->aq:LX/FAg;

    .line 2207238
    iget-object v2, v1, LX/FAg;->d:LX/Gpd;

    if-eqz v2, :cond_5

    .line 2207239
    iget-object v2, v1, LX/FAg;->d:LX/Gpd;

    invoke-virtual {v2, v0}, LX/Gpd;->a(Z)V

    .line 2207240
    :cond_5
    iget-object v0, p0, LX/FAa;->ar:LX/GoH;

    goto/16 :goto_0
.end method

.method public final b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p2    # Landroid/view/ViewGroup;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2207241
    new-instance v0, LX/Gmy;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/Gmy;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/FAa;->an:LX/Gmy;

    .line 2207242
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0, v1}, LX/FAa;->a(Ljava/lang/Object;Landroid/content/Context;)V

    .line 2207243
    iget-object v0, p0, LX/FAa;->Z:LX/GoD;

    .line 2207244
    iget-object v2, v0, LX/GoD;->a:LX/0SG;

    invoke-interface {v2}, LX/0SG;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    move-object v0, v2

    .line 2207245
    iput-object v0, p0, LX/FAa;->as:Ljava/lang/String;

    .line 2207246
    invoke-super {p0, p1, p2, p3}, LX/Chc;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    .line 2207247
    const v0, 0x7f0d181f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/FAa;->al:Landroid/widget/LinearLayout;

    .line 2207248
    const v0, 0x7f0d05b0

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    iput-object v0, p0, LX/FAa;->aA:Lcom/facebook/widget/loadingindicator/LoadingIndicatorView;

    .line 2207249
    iget-object v0, p0, LX/FAa;->ah:LX/Gn1;

    invoke-virtual {v0, p0}, LX/Chj;->a(LX/ChL;)V

    .line 2207250
    return-object v1
.end method

.method public final b()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2207251
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2207252
    invoke-static {v0}, LX/FAa;->f(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2207253
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2207254
    invoke-static {v0}, LX/FAa;->f(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    .line 2207255
    :goto_0
    iget-object v1, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v1, v1

    .line 2207256
    invoke-static {v1}, LX/FAa;->g(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/FAa;->as:Ljava/lang/String;

    .line 2207257
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 2207258
    const-string p0, "instant_shopping_catalog_id"

    invoke-interface {v3, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2207259
    if-eqz v1, :cond_0

    .line 2207260
    const-string p0, "instant_shopping_catalog_view"

    invoke-interface {v3, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2207261
    :cond_0
    const-string p0, "instant_shopping_catalog_session_id"

    invoke-interface {v3, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2207262
    move-object v0, v3

    .line 2207263
    return-object v0

    .line 2207264
    :cond_1
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2207265
    invoke-static {v0}, LX/FAa;->i(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(LX/Clo;)V
    .locals 13

    .prologue
    .line 2207266
    check-cast p1, LX/GoH;

    .line 2207267
    iget-object v0, p1, LX/GoH;->f:LX/2uF;

    move-object v0, v0

    .line 2207268
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    .line 2207269
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2207270
    new-instance v4, LX/CHR;

    invoke-direct {v4, v3, v2}, LX/CHR;-><init>(LX/15i;I)V

    invoke-virtual {v1, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2207271
    :cond_0
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 2207272
    const/4 v0, 0x0

    move v3, v0

    :goto_1
    invoke-virtual {p1}, LX/Clo;->d()I

    move-result v0

    if-ge v3, v0, :cond_2

    .line 2207273
    invoke-virtual {p1, v3}, LX/Clo;->a(I)LX/Clr;

    move-result-object v2

    .line 2207274
    instance-of v0, v2, LX/GoG;

    if-eqz v0, :cond_1

    move-object v0, v2

    check-cast v0, LX/GoG;

    invoke-interface {v0}, LX/GoG;->g()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;

    move-result-object v0

    if-eqz v0, :cond_1

    move-object v0, v2

    check-cast v0, LX/GoG;

    invoke-interface {v0}, LX/GoG;->g()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2207275
    check-cast v2, LX/GoG;

    invoke-interface {v2}, LX/GoG;->g()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2207276
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 2207277
    :cond_2
    move-object v0, v4

    .line 2207278
    iget-object v2, p0, LX/FAa;->E:LX/8Yg;

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v0, v3}, LX/8Yg;->a(LX/0Px;Ljava/util/Set;Z)Ljava/util/Map;

    move-result-object v1

    .line 2207279
    invoke-static {v0, v1}, LX/8Yg;->a(Ljava/util/Set;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 2207280
    iget-object v1, p0, LX/FAa;->F:LX/8Yh;

    .line 2207281
    iput-object v0, v1, LX/8Yh;->a:Ljava/util/Map;

    .line 2207282
    invoke-direct {p0, p1}, LX/FAa;->b(LX/GoH;)V

    .line 2207283
    iget-object v0, p1, LX/GoH;->e:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;

    move-object v0, v0

    .line 2207284
    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/FAa;->ao:Ljava/lang/String;

    .line 2207285
    iget-object v0, p1, LX/GoH;->c:LX/2uF;

    move-object v0, v0

    .line 2207286
    iget-object v1, p1, LX/GoH;->e:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;

    move-object v1, v1

    .line 2207287
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2207288
    invoke-virtual {v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->k()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$DocumentBodyElementsModel;

    move-result-object v2

    .line 2207289
    if-nez v2, :cond_5

    .line 2207290
    :cond_3
    :goto_2
    iget-object v0, p1, LX/GoH;->d:LX/0Px;

    move-object v0, v0

    .line 2207291
    iget-object v1, p1, LX/GoH;->e:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;

    move-object v1, v1

    .line 2207292
    invoke-direct {p0, v0, v1}, LX/FAa;->a(LX/0Px;Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;)V

    .line 2207293
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2207294
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v1

    .line 2207295
    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2207296
    iget-object v0, p0, LX/FAa;->C:LX/Gr9;

    .line 2207297
    iget-object v1, p0, LX/Chc;->D:Landroid/view/View;

    move-object v1, v1

    .line 2207298
    const v2, 0x7f0d1882

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v0, LX/Gr9;->d:Landroid/widget/ImageView;

    .line 2207299
    iget-object v2, v0, LX/Gr9;->g:LX/68u;

    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v2, v3}, LX/68u;->a(Landroid/view/animation/Interpolator;)V

    .line 2207300
    iget-object v2, v0, LX/Gr9;->g:LX/68u;

    const/4 v3, -0x1

    .line 2207301
    iput v3, v2, LX/68u;->x:I

    .line 2207302
    iget-object v2, v0, LX/Gr9;->g:LX/68u;

    sget v3, LX/CoL;->o:I

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, LX/68u;->a(J)LX/68u;

    .line 2207303
    iget-object v2, v0, LX/Gr9;->g:LX/68u;

    iget-object v3, v0, LX/Gr9;->d:Landroid/widget/ImageView;

    .line 2207304
    iput-object v3, v2, LX/68u;->A:Ljava/lang/Object;

    .line 2207305
    iget-object v2, v0, LX/Gr9;->g:LX/68u;

    new-instance v3, LX/Gr8;

    invoke-direct {v3, v0}, LX/Gr8;-><init>(LX/Gr9;)V

    invoke-virtual {v2, v3}, LX/68u;->a(LX/67q;)V

    .line 2207306
    iget-object v2, v0, LX/Gr9;->a:Landroid/view/Display;

    iget-object v3, v0, LX/Gr9;->b:Landroid/graphics/Point;

    invoke-virtual {v2, v3}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2207307
    iget-object v0, p0, LX/FAa;->an:LX/Gmy;

    .line 2207308
    iget-object v1, v0, LX/Gmy;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 2207309
    const/4 v4, 0x0

    .line 2207310
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2207311
    const-string v1, "extra_rich_media_element_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2207312
    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 2207313
    :cond_4
    :goto_3
    invoke-direct {p0}, LX/FAa;->ag()V

    .line 2207314
    iget-object v0, p0, LX/FAa;->I:LX/Go1;

    .line 2207315
    iget-object v1, v0, LX/Go1;->b:LX/Chv;

    invoke-virtual {v1, v0}, LX/0b4;->a(LX/0b2;)Z

    .line 2207316
    return-void

    .line 2207317
    :cond_5
    const/4 v3, 0x0

    .line 2207318
    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel$DocumentBodyElementsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    move v4, v8

    :goto_4
    if-ge v4, v6, :cond_13

    invoke-virtual {v5, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel;

    .line 2207319
    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel;->a()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;

    move-result-object v7

    if-eqz v7, :cond_a

    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel;->a()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;

    move-result-object v7

    invoke-interface {v7}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v7

    sget-object v10, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->HEADER:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    if-ne v7, v10, :cond_a

    .line 2207320
    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel;->a()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;

    move-result-object v2

    move-object v10, v2

    .line 2207321
    :goto_5
    if-eqz v10, :cond_b

    invoke-virtual {v10}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->M()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel$ElementDescriptorModel;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-virtual {v10}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->M()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel$ElementDescriptorModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel$ElementDescriptorModel;->a()Ljava/lang/String;

    move-result-object v2

    .line 2207322
    :goto_6
    invoke-static {p0, v2}, LX/FAa;->a(LX/FAa;Ljava/lang/String;)V

    .line 2207323
    invoke-static {v2}, LX/CIa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingDocumentFragmentModel;->a()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/CIa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/CIa;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v7

    .line 2207324
    iget-object v2, p0, LX/FAa;->D:LX/GrE;

    .line 2207325
    iget-object v3, p0, LX/Chc;->D:Landroid/view/View;

    move-object v3, v3

    .line 2207326
    invoke-static {p0}, LX/FAa;->aa(LX/FAa;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, LX/FAa;->P()I

    move-result v5

    iget-object v6, p0, LX/FAa;->aB:LX/0Px;

    const/4 v1, 0x0

    .line 2207327
    iput-object v3, v2, LX/GrE;->k:Landroid/view/View;

    .line 2207328
    iput-object v4, v2, LX/GrE;->l:Ljava/lang/String;

    .line 2207329
    iput v5, v2, LX/GrE;->m:I

    .line 2207330
    iput-object v6, v2, LX/GrE;->n:LX/0Px;

    .line 2207331
    iput-boolean v7, v2, LX/GrE;->o:Z

    .line 2207332
    iget-object v11, v2, LX/GrE;->k:Landroid/view/View;

    const v12, 0x7f0d1877

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, v2, LX/GrE;->h:Landroid/view/View;

    .line 2207333
    iget-object v11, v2, LX/GrE;->k:Landroid/view/View;

    const v12, 0x7f0d1880

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    iput-object v11, v2, LX/GrE;->i:Landroid/view/View;

    .line 2207334
    iget-object v11, v2, LX/GrE;->b:LX/3kp;

    sget-object v12, LX/Gn0;->e:LX/0Tn;

    invoke-virtual {v11, v12}, LX/3kp;->a(LX/0Tn;)V

    .line 2207335
    iget-object v11, v2, LX/GrE;->f:LX/Chv;

    invoke-virtual {v11, v2}, LX/0b4;->a(LX/0b2;)Z

    .line 2207336
    iget-object v11, v2, LX/GrE;->e:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0b1bf9

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v11

    float-to-int v11, v11

    iput v11, v2, LX/GrE;->q:I

    .line 2207337
    iput-boolean v1, v2, LX/GrE;->r:Z

    .line 2207338
    iput-boolean v1, v2, LX/GrE;->s:Z

    .line 2207339
    iput-boolean v1, v2, LX/GrE;->t:Z

    .line 2207340
    new-instance v3, LX/FAX;

    invoke-direct {v3, p0}, LX/FAX;-><init>(LX/FAa;)V

    .line 2207341
    iget-object v2, p0, LX/FAa;->D:LX/GrE;

    invoke-virtual {v2, v3}, LX/GrE;->a(Landroid/view/View$OnClickListener;)V

    .line 2207342
    iget-object v2, p0, LX/FAa;->aB:LX/0Px;

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;->AUDIO_CONTROL_FLOATING:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentPresentationStyle;

    invoke-virtual {v2, v4}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v2

    .line 2207343
    iget-boolean v4, p0, LX/FAa;->aH:Z

    if-nez v4, :cond_6

    .line 2207344
    sget-boolean v4, LX/GoK;->a:Z

    move v4, v4

    .line 2207345
    if-eqz v4, :cond_c

    if-eqz v2, :cond_c

    :cond_6
    move v2, v9

    :goto_7
    iput-boolean v2, p0, LX/FAa;->aC:Z

    .line 2207346
    iget-boolean v2, p0, LX/FAa;->aC:Z

    if-eqz v2, :cond_d

    .line 2207347
    iget-object v2, p0, LX/FAa;->D:LX/GrE;

    invoke-virtual {v2}, LX/GrE;->b()V

    .line 2207348
    :goto_8
    iget-object v2, p0, LX/FAa;->ao:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 2207349
    iget-object v2, p0, LX/FAa;->ak:Landroid/widget/TextView;

    iget-object v4, p0, LX/FAa;->ao:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2207350
    :goto_9
    if-eqz v0, :cond_7

    invoke-virtual {v0}, LX/39O;->c()I

    move-result v2

    if-nez v2, :cond_14

    .line 2207351
    :cond_7
    const/4 v2, 0x0

    .line 2207352
    :goto_a
    move v2, v2

    .line 2207353
    if-nez v2, :cond_3

    .line 2207354
    if-eqz v10, :cond_8

    invoke-virtual {v10}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->G()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 2207355
    :cond_8
    iget-object v2, p0, LX/FAa;->ao:Ljava/lang/String;

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2207356
    invoke-static {p0}, LX/FAa;->T(LX/FAa;)V

    .line 2207357
    invoke-static {p0}, LX/FAa;->S(LX/FAa;)V

    .line 2207358
    iget-object v2, p0, LX/FAa;->D:LX/GrE;

    invoke-virtual {v2, v3}, LX/GrE;->a(Landroid/view/View$OnClickListener;)V

    .line 2207359
    iget-boolean v2, p0, LX/FAa;->aC:Z

    if-eqz v2, :cond_9

    .line 2207360
    iget-object v2, p0, LX/FAa;->D:LX/GrE;

    invoke-virtual {v2}, LX/GrE;->b()V

    .line 2207361
    :cond_9
    iget-object v2, p0, LX/FAa;->D:LX/GrE;

    .line 2207362
    const/4 v3, 0x1

    iput-boolean v3, v2, LX/GrE;->r:Z

    .line 2207363
    iget-object v3, v2, LX/GrE;->i:Landroid/view/View;

    new-instance v4, LX/GrB;

    invoke-direct {v4, v2}, LX/GrB;-><init>(LX/GrE;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2207364
    goto/16 :goto_2

    .line 2207365
    :cond_a
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_4

    .line 2207366
    :cond_b
    const-string v2, "#00000000"

    goto/16 :goto_6

    :cond_c
    move v2, v8

    .line 2207367
    goto :goto_7

    .line 2207368
    :cond_d
    iget-object v2, p0, LX/FAa;->ac:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    sget-object v2, LX/Gn0;->f:LX/0Tn;

    invoke-static {p0}, LX/FAa;->aa(LX/FAa;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v2

    check-cast v2, LX/0Tn;

    invoke-interface {v4, v2, v9}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    goto :goto_8

    .line 2207369
    :cond_e
    iget-object v2, p0, LX/FAa;->ak:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_9

    .line 2207370
    :cond_f
    invoke-virtual {v10}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->G()LX/0Px;

    move-result-object v2

    .line 2207371
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    const/4 v3, 0x0

    move v4, v3

    :goto_b
    if-ge v4, v5, :cond_12

    invoke-virtual {v2, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingHeaderElementFragmentModel$HeaderElementsModel;

    .line 2207372
    sget-object v6, LX/FAO;->a:[I

    invoke-interface {v3}, LX/CHb;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->ordinal()I

    move-result v8

    aget v6, v6, v8

    packed-switch v6, :pswitch_data_0

    .line 2207373
    :goto_c
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_b

    .line 2207374
    :pswitch_0
    new-instance v6, LX/Gpb;

    iget-object v8, p0, LX/FAa;->am:Landroid/view/ViewGroup;

    .line 2207375
    new-instance v9, Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;

    invoke-direct {v9, v8}, Lcom/facebook/instantshopping/view/block/impl/HeaderLogoBlockViewImpl;-><init>(Landroid/view/View;)V

    move-object v8, v9

    .line 2207376
    invoke-direct {v6, v8, v3}, LX/Gpb;-><init>(LX/Gpv;LX/CHi;)V

    .line 2207377
    invoke-static {}, LX/GoK;->b()LX/Gp6;

    invoke-virtual {v6}, LX/GpR;->b()V

    .line 2207378
    iget-object v3, p0, LX/FAa;->ak:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_c

    .line 2207379
    :pswitch_1
    invoke-interface {v3}, LX/CHb;->c()LX/CHa;

    move-result-object v6

    if-eqz v6, :cond_10

    invoke-interface {v3}, LX/CHb;->c()LX/CHa;

    move-result-object v6

    invoke-interface {v6}, LX/CHa;->jv_()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;

    move-result-object v6

    if-eqz v6, :cond_10

    invoke-interface {v3}, LX/CHb;->c()LX/CHa;

    move-result-object v6

    invoke-interface {v6}, LX/CHa;->jv_()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;->e()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_10

    .line 2207380
    invoke-interface {v3}, LX/CHb;->c()LX/CHa;

    move-result-object v6

    invoke-interface {v6}, LX/CHa;->jv_()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;->e()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/CIa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2207381
    iget-object v8, p0, LX/FAa;->ak:Landroid/widget/TextView;

    invoke-static {v6}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2207382
    :cond_10
    invoke-interface {v3}, LX/CHb;->d()LX/8Z4;

    move-result-object v6

    if-eqz v6, :cond_11

    invoke-interface {v3}, LX/CHb;->d()LX/8Z4;

    move-result-object v3

    invoke-interface {v3}, LX/8Z4;->d()Ljava/lang/String;

    move-result-object v3

    :goto_d
    iput-object v3, p0, LX/FAa;->ao:Ljava/lang/String;

    .line 2207383
    iget-object v3, p0, LX/FAa;->ak:Landroid/widget/TextView;

    iget-object v6, p0, LX/FAa;->ao:Ljava/lang/String;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_c

    .line 2207384
    :cond_11
    iget-object v3, p0, LX/FAa;->ao:Ljava/lang/String;

    goto :goto_d

    .line 2207385
    :pswitch_2
    new-instance v6, LX/GpS;

    iget-object v8, p0, LX/FAa;->am:Landroid/view/ViewGroup;

    .line 2207386
    new-instance v9, LX/Gpz;

    invoke-direct {v9, v8}, LX/Gpz;-><init>(Landroid/view/View;)V

    move-object v8, v9

    .line 2207387
    invoke-direct {v6, v8, v3}, LX/GpS;-><init>(LX/Gpv;Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingHeaderElementFragmentModel$HeaderElementsModel;)V

    .line 2207388
    iput-boolean v7, v6, LX/GpS;->e:Z

    .line 2207389
    invoke-static {}, LX/GoK;->b()LX/Gp6;

    invoke-virtual {v6}, LX/GpR;->b()V

    goto/16 :goto_c

    .line 2207390
    :cond_12
    goto/16 :goto_2

    :cond_13
    move-object v10, v3

    goto/16 :goto_5

    .line 2207391
    :cond_14
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v5

    :goto_e
    invoke-interface {v5}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_15

    invoke-interface {v5}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v6, v2, LX/1vs;->a:LX/15i;

    iget v8, v2, LX/1vs;->b:I

    .line 2207392
    const/4 v4, 0x0

    .line 2207393
    sget-object v9, LX/FAO;->b:[I

    const/4 v2, 0x2

    const-class v11, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    sget-object v12, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    invoke-virtual {v6, v8, v2, v11, v12}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingHeaderElementType;->ordinal()I

    move-result v2

    aget v2, v9, v2

    packed-switch v2, :pswitch_data_1

    move-object v2, v4

    .line 2207394
    :goto_f
    iget-object v4, p0, LX/FAa;->aj:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_e

    .line 2207395
    :pswitch_3
    iget-object v2, p0, LX/FAa;->am:Landroid/view/ViewGroup;

    .line 2207396
    new-instance v4, LX/Gq2;

    invoke-direct {v4, v2}, LX/Gq2;-><init>(Landroid/view/View;)V

    move-object v2, v4

    .line 2207397
    check-cast v2, LX/Gq2;

    iput-object v2, p0, LX/FAa;->ap:LX/Gq2;

    .line 2207398
    new-instance v2, LX/GpU;

    iget-object v4, p0, LX/FAa;->ap:LX/Gq2;

    invoke-direct {v2, v4, v6, v8}, LX/GpU;-><init>(LX/Gpv;LX/15i;I)V

    .line 2207399
    invoke-static {}, LX/GoK;->b()LX/Gp6;

    invoke-virtual {v2}, LX/GpR;->b()V

    .line 2207400
    invoke-static {p0}, LX/FAa;->R(LX/FAa;)V

    goto :goto_f

    .line 2207401
    :pswitch_4
    new-instance v2, LX/Gpq;

    iget-object v4, p0, LX/FAa;->am:Landroid/view/ViewGroup;

    .line 2207402
    new-instance v9, LX/Gql;

    invoke-direct {v9, v4}, LX/Gql;-><init>(Landroid/view/View;)V

    move-object v4, v9

    .line 2207403
    invoke-direct {v2, v4, v6, v8}, LX/Gpq;-><init>(LX/Gpv;LX/15i;I)V

    .line 2207404
    invoke-static {}, LX/GoK;->b()LX/Gp6;

    invoke-virtual {v2}, LX/GpR;->b()V

    .line 2207405
    invoke-static {p0}, LX/FAa;->R(LX/FAa;)V

    goto :goto_f

    .line 2207406
    :cond_15
    const/4 v2, 0x1

    goto/16 :goto_a

    .line 2207407
    :cond_16
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v0, v0

    .line 2207408
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v1

    .line 2207409
    check-cast v0, LX/CoJ;

    move v3, v4

    .line 2207410
    :goto_10
    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v1

    if-ge v3, v1, :cond_4

    .line 2207411
    invoke-virtual {v0, v3}, LX/CoJ;->e(I)LX/Clr;

    move-result-object v1

    check-cast v1, LX/Gob;

    .line 2207412
    instance-of v2, v1, LX/Goy;

    if-eqz v2, :cond_18

    move-object v2, v1

    .line 2207413
    check-cast v2, LX/Goy;

    invoke-virtual {v2}, LX/Goy;->a()LX/0Px;

    move-result-object v7

    .line 2207414
    invoke-virtual {v7}, LX/0Px;->size()I

    move-result v8

    move v5, v4

    :goto_11
    if-ge v5, v8, :cond_18

    invoke-virtual {v7, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingCompositeBlockElementFragmentModel$BlockElementsModel;

    .line 2207415
    invoke-interface {v2}, LX/CHb;->jt_()Ljava/lang/String;

    move-result-object v2

    .line 2207416
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_17

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 2207417
    invoke-static {p0, v3}, LX/FAa;->d(LX/FAa;I)V

    goto/16 :goto_3

    .line 2207418
    :cond_17
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_11

    .line 2207419
    :cond_18
    invoke-interface {v1}, LX/GoY;->D()LX/GoE;

    move-result-object v1

    .line 2207420
    if-eqz v1, :cond_19

    .line 2207421
    iget-object v2, v1, LX/GoE;->b:Ljava/lang/String;

    move-object v1, v2

    .line 2207422
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_19

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_19

    .line 2207423
    invoke-static {p0, v3}, LX/FAa;->d(LX/FAa;I)V

    goto/16 :goto_3

    .line 2207424
    :cond_19
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_10

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2207425
    invoke-super {p0, p1}, LX/Chc;->b(Landroid/os/Bundle;)V

    .line 2207426
    const-string v0, "instant_shopping_catalog_session_id"

    iget-object v1, p0, LX/FAa;->as:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2207427
    return-void
.end method

.method public final b(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 2207428
    iget-object v0, p0, LX/FAa;->J:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/FAa;->ai:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".onFetchError"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error attempting to more blocks. catalog id("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/FAa;->at:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    .line 2207429
    iput-object p1, v1, LX/0VK;->c:Ljava/lang/Throwable;

    .line 2207430
    move-object v1, v1

    .line 2207431
    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    .line 2207432
    iget-object v0, p0, LX/FAa;->L:LX/Gny;

    const-string v1, "instant_shopping_graphql_first_fetch"

    .line 2207433
    invoke-static {v0}, LX/Gnx;->b(LX/Gnx;)LX/11o;

    move-result-object v2

    iput-object v2, v0, LX/Gnx;->c:LX/11o;

    .line 2207434
    iget-object v2, v0, LX/Gnx;->c:LX/11o;

    if-eqz v2, :cond_0

    iget-object v2, v0, LX/Gnx;->c:LX/11o;

    invoke-interface {v2, v1}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2207435
    iget-object v2, v0, LX/Gnx;->c:LX/11o;

    const v3, 0x4fd3f4a8

    invoke-static {v2, v1, v3}, LX/096;->d(LX/11o;Ljava/lang/String;I)LX/11o;

    .line 2207436
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2207437
    sget-object v0, LX/Go5;->ANDROID_BACK_BUTTON_PRESSED:LX/Go5;

    iput-object v0, p0, LX/FAa;->aF:LX/Go5;

    .line 2207438
    invoke-super {p0}, LX/Chc;->c()Z

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 2207439
    iget-object v0, p0, LX/FAa;->ah:LX/Gn1;

    invoke-virtual {v0}, LX/Chj;->c()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FAa;->ah:LX/Gn1;

    invoke-virtual {v0}, LX/Chj;->a()LX/ChL;

    move-result-object v0

    if-ne v0, p0, :cond_4

    .line 2207440
    :cond_0
    invoke-super {p0}, LX/Chc;->e()V

    .line 2207441
    iput-boolean v1, p0, LX/FAa;->aD:Z

    .line 2207442
    iput-boolean v1, p0, LX/FAa;->aE:Z

    .line 2207443
    iget-object v0, p0, LX/FAa;->W:LX/Go7;

    const-string v1, "canvas_resume"

    invoke-virtual {v0, v1}, LX/Go7;->a(Ljava/lang/String;)V

    .line 2207444
    iget-object v0, p0, LX/Chc;->E:Landroid/os/Bundle;

    move-object v0, v0

    .line 2207445
    invoke-static {v0}, LX/FAa;->k(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2207446
    iget-object v0, p0, LX/FAa;->H:LX/Go0;

    const/4 v1, 0x0

    .line 2207447
    iput-object v1, v0, LX/Go0;->e:Ljava/util/Map;

    .line 2207448
    :cond_1
    invoke-static {p0}, LX/FAa;->X(LX/FAa;)V

    .line 2207449
    invoke-direct {p0}, LX/FAa;->ab()V

    .line 2207450
    iget-object v0, p0, LX/FAa;->H:LX/Go0;

    const-string v1, "instant_shopping_document_resume"

    invoke-virtual {v0, v1}, LX/Go0;->a(Ljava/lang/String;)V

    .line 2207451
    iget-object v0, p0, LX/FAa;->S:LX/Go2;

    .line 2207452
    const/4 v2, 0x0

    iput-boolean v2, v0, LX/Go2;->g:Z

    .line 2207453
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, LX/Go2;->b:J

    .line 2207454
    iget-wide v2, v0, LX/Go2;->d:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    .line 2207455
    iget-wide v2, v0, LX/Go2;->b:J

    .line 2207456
    iget-wide v6, v0, LX/Go2;->e:J

    iget-wide v8, v0, LX/Go2;->d:J

    sub-long v8, v2, v8

    add-long/2addr v6, v8

    iput-wide v6, v0, LX/Go2;->e:J

    .line 2207457
    iget-boolean v6, v0, LX/Go2;->h:Z

    if-eqz v6, :cond_2

    .line 2207458
    const/4 v6, 0x0

    iput-boolean v6, v0, LX/Go2;->h:Z

    .line 2207459
    iget-wide v6, v0, LX/Go2;->f:J

    iget-wide v8, v0, LX/Go2;->d:J

    sub-long v8, v2, v8

    add-long/2addr v6, v8

    iput-wide v6, v0, LX/Go2;->f:J

    .line 2207460
    :cond_2
    const-wide/16 v2, -0x1

    iput-wide v2, v0, LX/Go2;->d:J

    .line 2207461
    iget-object v0, p0, LX/FAa;->T:LX/Go4;

    .line 2207462
    iget-object v2, v0, LX/Go4;->a:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 2207463
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Go3;

    .line 2207464
    iget-boolean v4, v2, LX/Go3;->c:Z

    move v4, v4

    .line 2207465
    if-eqz v4, :cond_3

    .line 2207466
    invoke-static {v2}, LX/Go3;->j(LX/Go3;)V

    .line 2207467
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    long-to-double v5, v5

    iput-wide v5, v2, LX/Go3;->d:D

    .line 2207468
    goto :goto_0

    .line 2207469
    :cond_4
    return-void
.end method

.method public final f()V
    .locals 10

    .prologue
    .line 2207470
    iget-object v0, p0, LX/FAa;->ah:LX/Gn1;

    invoke-virtual {v0}, LX/Chj;->c()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FAa;->ah:LX/Gn1;

    invoke-virtual {v0}, LX/Chj;->a()LX/ChL;

    move-result-object v0

    if-ne v0, p0, :cond_2

    .line 2207471
    :cond_0
    invoke-super {p0}, LX/Chc;->f()V

    .line 2207472
    iget-object v0, p0, LX/FAa;->W:LX/Go7;

    const-string v1, "canvas_pause"

    invoke-virtual {v0, v1}, LX/Go7;->a(Ljava/lang/String;)V

    .line 2207473
    iget-object v0, p0, LX/FAa;->S:LX/Go2;

    invoke-virtual {v0}, LX/Go2;->a()V

    .line 2207474
    iget-object v0, p0, LX/FAa;->Y:LX/GoA;

    iget-object v1, p0, LX/FAa;->V:LX/Cl7;

    invoke-virtual {v1}, LX/Cl7;->b()F

    move-result v1

    .line 2207475
    iput v1, v0, LX/GoA;->j:F

    .line 2207476
    iget-object v0, p0, LX/FAa;->Y:LX/GoA;

    iget-object v1, p0, LX/FAa;->S:LX/Go2;

    .line 2207477
    iget-wide v4, v1, LX/Go2;->c:J

    iget-wide v6, v1, LX/Go2;->e:J

    add-long/2addr v4, v6

    move-wide v2, v4

    .line 2207478
    iput-wide v2, v0, LX/GoA;->k:J

    .line 2207479
    iget-object v0, p0, LX/FAa;->Y:LX/GoA;

    const-wide/16 v6, 0x0

    .line 2207480
    iget-boolean v4, v0, LX/GoA;->f:Z

    if-nez v4, :cond_1

    iget-object v4, v0, LX/GoA;->l:LX/CIb;

    .line 2207481
    iget-object v5, v4, LX/CIb;->a:Ljava/lang/String;

    move-object v4, v5

    .line 2207482
    invoke-static {v4}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-wide v4, v0, LX/GoA;->g:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_1

    iget-wide v4, v0, LX/GoA;->h:J

    cmp-long v4, v4, v6

    if-nez v4, :cond_3

    .line 2207483
    :cond_1
    :goto_0
    iget-object v0, p0, LX/FAa;->H:LX/Go0;

    const-string v1, "instant_shopping_document_pause"

    invoke-virtual {v0, v1}, LX/Go0;->a(Ljava/lang/String;)V

    .line 2207484
    iget-object v0, p0, LX/FAa;->T:LX/Go4;

    invoke-virtual {v0}, LX/Go4;->a()V

    .line 2207485
    :cond_2
    return-void

    .line 2207486
    :cond_3
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 2207487
    const-string v5, "TTI"

    iget-wide v6, v0, LX/GoA;->g:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2207488
    const-string v5, "catalog_download_time"

    iget-object v6, v0, LX/GoA;->a:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    iget-object v7, v0, LX/GoA;->l:LX/CIb;

    .line 2207489
    iget-object v8, v7, LX/CIb;->a:Ljava/lang/String;

    move-object v7, v8

    .line 2207490
    iget-object v8, v6, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->d:Ljava/util/Map;

    invoke-interface {v8, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2207491
    iget-object v8, v6, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->d:Ljava/util/Map;

    invoke-interface {v8, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 2207492
    :goto_1
    move-wide v6, v8

    .line 2207493
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2207494
    const-string v5, "catalog_render_time"

    iget-wide v6, v0, LX/GoA;->h:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2207495
    const-string v5, "catalog_content_cached"

    iget-boolean v6, v0, LX/GoA;->i:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2207496
    const-string v5, "dwell_time"

    iget-wide v6, v0, LX/GoA;->k:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2207497
    const-string v5, "depth_percent"

    iget v6, v0, LX/GoA;->j:F

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2207498
    iget-object v5, v0, LX/GoA;->b:LX/Go0;

    const-string v6, "instant_shopping_perf_event"

    invoke-virtual {v5, v6, v4}, LX/Go0;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2207499
    const/4 v4, 0x1

    iput-boolean v4, v0, LX/GoA;->f:Z

    goto :goto_0

    :cond_4
    const-wide/16 v8, 0x0

    goto :goto_1
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 2207500
    invoke-super {p0}, LX/Chc;->g()V

    .line 2207501
    iget-object v0, p0, LX/FAa;->ah:LX/Gn1;

    invoke-virtual {v0, p0}, LX/Chj;->b(LX/ChL;)V

    .line 2207502
    return-void
.end method

.method public final k()LX/ChZ;
    .locals 1

    .prologue
    .line 2207503
    iget-object v0, p0, LX/FAa;->D:LX/GrE;

    .line 2207504
    iget-boolean p0, v0, LX/GrE;->t:Z

    if-eqz p0, :cond_0

    sget-object p0, LX/ChZ;->ON:LX/ChZ;

    :goto_0
    move-object v0, p0

    .line 2207505
    return-object v0

    :cond_0
    sget-object p0, LX/ChZ;->OFF:LX/ChZ;

    goto :goto_0
.end method

.method public final m()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 2207506
    invoke-super {p0}, LX/Chc;->m()V

    .line 2207507
    iget-object v0, p0, LX/Chc;->K:LX/1OR;

    move-object v0, v0

    .line 2207508
    check-cast v0, LX/1P1;

    .line 2207509
    invoke-virtual {v0}, LX/1P1;->o()I

    move-result v1

    .line 2207510
    invoke-virtual {v0}, LX/1OR;->D()I

    move-result v2

    .line 2207511
    const/4 v4, -0x1

    if-eq v1, v4, :cond_0

    add-int/lit8 v1, v1, 0x1

    if-ge v1, v2, :cond_1

    .line 2207512
    :cond_0
    iget-object v1, p0, LX/FAa;->C:LX/Gr9;

    .line 2207513
    iget-object v2, v1, LX/Gr9;->d:Landroid/widget/ImageView;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2207514
    :cond_1
    iget-object v0, p0, LX/Chc;->a:LX/0Ot;

    move-object v0, v0

    .line 2207515
    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chv;

    new-instance v1, LX/Cic;

    .line 2207516
    iget-object v2, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v2, v2

    .line 2207517
    invoke-direct {v1, v2, v3, v3}, LX/Cic;-><init>(Landroid/support/v7/widget/RecyclerView;II)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2207518
    iget-object v0, p0, LX/FAa;->G:LX/193;

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "instant_shopping"

    invoke-virtual {v3}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "_instant_shoppin_scroller"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/193;->a(Ljava/lang/Boolean;Ljava/lang/String;)LX/195;

    move-result-object v0

    .line 2207519
    new-instance v1, LX/FAZ;

    invoke-direct {v1, p0}, LX/FAZ;-><init>(LX/FAa;)V

    .line 2207520
    iput-object v1, v0, LX/195;->k:LX/1KG;

    .line 2207521
    iget-object v1, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v1, v1

    .line 2207522
    invoke-virtual {p0, v1, v0}, LX/Chc;->a(Lcom/facebook/widget/recyclerview/BetterRecyclerView;LX/195;)V

    .line 2207523
    return-void
.end method

.method public final mB_()V
    .locals 3

    .prologue
    .line 2207524
    invoke-super {p0}, LX/Chc;->mB_()V

    .line 2207525
    iget-object v0, p0, LX/FAa;->M:LX/0Uh;

    const/16 v1, 0x26

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2207526
    invoke-direct {p0}, LX/FAa;->Q()V

    .line 2207527
    :cond_0
    return-void
.end method

.method public final t()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2207528
    invoke-super {p0}, LX/Chc;->t()V

    .line 2207529
    iget-object v0, p0, LX/Chc;->F:Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    move-object v4, v0

    .line 2207530
    if-eqz v4, :cond_1

    .line 2207531
    iget-object v0, v4, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 2207532
    if-eqz v0, :cond_1

    .line 2207533
    invoke-virtual {v4}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getChildCount()I

    move-result v5

    move v3, v2

    move v1, v2

    :goto_0
    if-ge v3, v5, :cond_2

    .line 2207534
    invoke-virtual {v4, v3}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2207535
    invoke-virtual {v4, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v0

    .line 2207536
    invoke-static {v0}, LX/FAa;->a(LX/1a1;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2207537
    const/4 v1, 0x1

    .line 2207538
    check-cast v0, LX/Cs4;

    invoke-virtual {v0}, LX/Cs4;->w()LX/CnT;

    move-result-object v0

    check-cast v0, LX/Gpf;

    .line 2207539
    invoke-virtual {v0}, LX/Gpf;->d()V

    :cond_0
    move v0, v1

    .line 2207540
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 2207541
    :cond_2
    if-nez v1, :cond_4

    .line 2207542
    iget-object v0, p0, LX/FAa;->aI:LX/Gr7;

    const/16 v1, 0x77

    .line 2207543
    iget-object v3, v0, LX/Gr7;->t:LX/0YU;

    invoke-virtual {v3, v1}, LX/0YU;->a(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Gr6;

    .line 2207544
    if-eqz v3, :cond_3

    invoke-virtual {v3}, LX/Gr6;->a()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2207545
    :cond_3
    const/4 v3, 0x0

    .line 2207546
    :goto_1
    move-object v0, v3

    .line 2207547
    invoke-static {v0}, LX/FAa;->a(LX/1a1;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2207548
    check-cast v0, LX/Cs4;

    invoke-virtual {v0}, LX/Cs4;->w()LX/CnT;

    move-result-object v0

    check-cast v0, LX/Gpf;

    .line 2207549
    invoke-virtual {v0}, LX/Gpf;->d()V

    .line 2207550
    :cond_4
    return-void

    :cond_5
    invoke-virtual {v3, v2}, LX/Gr6;->a(I)LX/1a1;

    move-result-object v3

    goto :goto_1
.end method

.method public final v()V
    .locals 5

    .prologue
    .line 2207552
    iget-boolean v0, p0, LX/FAa;->aD:Z

    if-eqz v0, :cond_0

    .line 2207553
    :goto_0
    return-void

    .line 2207554
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/FAa;->aD:Z

    .line 2207555
    iget-object v0, p0, LX/FAa;->W:LX/Go7;

    const-string v1, "canvas_depth"

    iget-object v2, p0, LX/FAa;->ae:LX/ClD;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/ClD;->a(Landroid/content/Context;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2207556
    iget-object v0, p0, LX/FAa;->S:LX/Go2;

    invoke-virtual {v0}, LX/Go2;->c()V

    .line 2207557
    invoke-super {p0}, LX/Chc;->v()V

    .line 2207558
    invoke-virtual {p0}, LX/Chc;->t()V

    .line 2207559
    iget-object v0, p0, LX/FAa;->ad:LX/0if;

    sget-object v1, LX/0ig;->w:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->c(LX/0ih;)V

    .line 2207560
    invoke-direct {p0}, LX/FAa;->Y()V

    .line 2207561
    iget-boolean v0, p0, LX/FAa;->aC:Z

    if-eqz v0, :cond_1

    .line 2207562
    const-string v1, "instant_shopping_audio_button_on_exit"

    iget-object v2, p0, LX/FAa;->ac:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v0, LX/Gn0;->f:LX/0Tn;

    iget-object v3, p0, LX/FAa;->Q:LX/CIb;

    .line 2207563
    iget-object v4, v3, LX/CIb;->a:Ljava/lang/String;

    move-object v3, v4

    .line 2207564
    invoke-virtual {v0, v3}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    const/4 v3, 0x0

    invoke-interface {v2, v0, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    .line 2207565
    iget-object v2, p0, LX/FAa;->H:LX/Go0;

    new-instance v3, LX/FAM;

    invoke-direct {v3, p0, v0}, LX/FAM;-><init>(LX/FAa;Ljava/lang/String;)V

    invoke-virtual {v2, v1, v3}, LX/Go0;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 2207566
    :cond_1
    iget-object v0, p0, LX/FAa;->W:LX/Go7;

    .line 2207567
    iget-object v1, v0, LX/Go7;->b:Ljava/util/Map;

    move-object v0, v1

    .line 2207568
    iget-boolean v1, p0, LX/FAa;->aE:Z

    if-eqz v1, :cond_4

    .line 2207569
    :goto_1
    iget-object v0, p0, LX/FAa;->aa:LX/Gn2;

    iget-object v1, p0, LX/FAa;->at:Ljava/lang/String;

    iget-object v2, p0, LX/FAa;->au:Ljava/lang/String;

    invoke-virtual {p0}, LX/Chc;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2207570
    if-eqz v1, :cond_5

    if-nez v2, :cond_5

    .line 2207571
    iget-boolean v4, v0, LX/Gn2;->c:Z

    if-eqz v4, :cond_2

    iget-boolean v4, v0, LX/Gn2;->d:Z

    if-eqz v4, :cond_7

    .line 2207572
    :cond_2
    :goto_2
    iget-object v0, p0, LX/FAa;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chi;

    invoke-virtual {v0}, LX/Chi;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2207573
    iget-object v0, p0, LX/FAa;->af:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Chi;

    .line 2207574
    const/4 p0, 0x0

    iput-object p0, v0, LX/Chi;->n:LX/Clo;

    .line 2207575
    :cond_3
    goto/16 :goto_0

    .line 2207576
    :cond_4
    const/4 v1, 0x1

    iput-boolean v1, p0, LX/FAa;->aE:Z

    .line 2207577
    iget-object v1, p0, LX/FAa;->H:LX/Go0;

    const-string v2, "instant_shopping_document_close"

    invoke-virtual {v1, v2, v0}, LX/Go0;->a(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_1

    .line 2207578
    :cond_5
    if-eqz v2, :cond_2

    .line 2207579
    iget-boolean v4, v0, LX/Gn2;->d:Z

    if-eqz v4, :cond_6

    iget-boolean v4, v0, LX/Gn2;->e:Z

    if-eqz v4, :cond_8

    .line 2207580
    :cond_6
    :goto_3
    goto :goto_2

    .line 2207581
    :cond_7
    iget-object v4, v0, LX/Gn2;->b:LX/0ad;

    sget-char v1, LX/CHN;->m:C

    const/4 v2, 0x0

    invoke-interface {v4, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2207582
    if-eqz v1, :cond_2

    const-string v4, "0"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 2207583
    iget-object v4, v0, LX/Gn2;->a:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0gt;

    .line 2207584
    iput-object v1, v4, LX/0gt;->a:Ljava/lang/String;

    .line 2207585
    move-object v4, v4

    .line 2207586
    invoke-virtual {v4, v3}, LX/0gt;->a(Landroid/content/Context;)V

    goto :goto_2

    .line 2207587
    :cond_8
    iget-object v4, v0, LX/Gn2;->b:LX/0ad;

    sget-char v1, LX/CHN;->l:C

    const/4 v2, 0x0

    invoke-interface {v4, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2207588
    if-eqz v1, :cond_6

    const-string v4, "0"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 2207589
    iget-object v4, v0, LX/Gn2;->a:LX/0Or;

    invoke-interface {v4}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/0gt;

    .line 2207590
    iput-object v1, v4, LX/0gt;->a:Ljava/lang/String;

    .line 2207591
    move-object v4, v4

    .line 2207592
    invoke-virtual {v4, v3}, LX/0gt;->a(Landroid/content/Context;)V

    goto :goto_3
.end method

.method public final y()I
    .locals 1

    .prologue
    .line 2207551
    const v0, 0x7f03099b

    return v0
.end method
