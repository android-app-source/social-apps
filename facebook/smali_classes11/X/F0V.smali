.class public final LX/F0V;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 27

    .prologue
    .line 2189685
    const/16 v23, 0x0

    .line 2189686
    const/16 v22, 0x0

    .line 2189687
    const/16 v21, 0x0

    .line 2189688
    const/16 v20, 0x0

    .line 2189689
    const/16 v19, 0x0

    .line 2189690
    const/16 v18, 0x0

    .line 2189691
    const/16 v17, 0x0

    .line 2189692
    const/16 v16, 0x0

    .line 2189693
    const/4 v15, 0x0

    .line 2189694
    const/4 v14, 0x0

    .line 2189695
    const/4 v13, 0x0

    .line 2189696
    const/4 v12, 0x0

    .line 2189697
    const/4 v11, 0x0

    .line 2189698
    const/4 v10, 0x0

    .line 2189699
    const/4 v9, 0x0

    .line 2189700
    const/4 v8, 0x0

    .line 2189701
    const/4 v5, 0x0

    .line 2189702
    const-wide/16 v6, 0x0

    .line 2189703
    const/4 v4, 0x0

    .line 2189704
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_1

    .line 2189705
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2189706
    const/4 v4, 0x0

    .line 2189707
    :goto_0
    return v4

    .line 2189708
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2189709
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v24

    sget-object v25, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v24

    move-object/from16 v1, v25

    if-eq v0, v1, :cond_13

    .line 2189710
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v24

    .line 2189711
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2189712
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v25

    sget-object v26, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v25

    move-object/from16 v1, v26

    if-eq v0, v1, :cond_1

    if-eqz v24, :cond_1

    .line 2189713
    const-string v25, "accent_images"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_2

    .line 2189714
    invoke-static/range {p0 .. p1}, LX/F0R;->a(LX/15w;LX/186;)I

    move-result v23

    goto :goto_1

    .line 2189715
    :cond_2
    const-string v25, "closing_image"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_3

    .line 2189716
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v22

    goto :goto_1

    .line 2189717
    :cond_3
    const-string v25, "closing_text"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_4

    .line 2189718
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v21

    move-object/from16 v0, p1

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v21

    goto :goto_1

    .line 2189719
    :cond_4
    const-string v25, "header_decorative_image"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_5

    .line 2189720
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v20

    goto :goto_1

    .line 2189721
    :cond_5
    const-string v25, "header_favicon"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_6

    .line 2189722
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v19

    goto :goto_1

    .line 2189723
    :cond_6
    const-string v25, "header_text"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_7

    .line 2189724
    invoke-static/range {p0 .. p1}, LX/F0S;->a(LX/15w;LX/186;)I

    move-result v18

    goto :goto_1

    .line 2189725
    :cond_7
    const-string v25, "main_image"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_8

    .line 2189726
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v17

    goto/16 :goto_1

    .line 2189727
    :cond_8
    const-string v25, "notice_button_color"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_9

    .line 2189728
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, LX/186;->b(Ljava/lang/String;)I

    move-result v16

    goto/16 :goto_1

    .line 2189729
    :cond_9
    const-string v25, "notice_button_label"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_a

    .line 2189730
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->b(Ljava/lang/String;)I

    move-result v15

    goto/16 :goto_1

    .line 2189731
    :cond_a
    const-string v25, "notice_image"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_b

    .line 2189732
    invoke-static/range {p0 .. p1}, LX/32Q;->a(LX/15w;LX/186;)I

    move-result v14

    goto/16 :goto_1

    .line 2189733
    :cond_b
    const-string v25, "notice_subtitle"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_c

    .line 2189734
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_1

    .line 2189735
    :cond_c
    const-string v25, "notice_title"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_d

    .line 2189736
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, LX/186;->b(Ljava/lang/String;)I

    move-result v12

    goto/16 :goto_1

    .line 2189737
    :cond_d
    const-string v25, "notice_title_color"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_e

    .line 2189738
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto/16 :goto_1

    .line 2189739
    :cond_e
    const-string v25, "render_style"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_f

    .line 2189740
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto/16 :goto_1

    .line 2189741
    :cond_f
    const-string v25, "theme"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_10

    .line 2189742
    invoke-static/range {p0 .. p1}, LX/F0T;->a(LX/15w;LX/186;)I

    move-result v9

    goto/16 :goto_1

    .line 2189743
    :cond_10
    const-string v25, "throwback_settings"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_11

    .line 2189744
    invoke-static/range {p0 .. p1}, LX/F0U;->a(LX/15w;LX/186;)I

    move-result v8

    goto/16 :goto_1

    .line 2189745
    :cond_11
    const-string v25, "throwback_units"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v25

    if-eqz v25, :cond_12

    .line 2189746
    invoke-static/range {p0 .. p1}, LX/F0X;->a(LX/15w;LX/186;)I

    move-result v5

    goto/16 :goto_1

    .line 2189747
    :cond_12
    const-string v25, "title_date"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v24

    if-eqz v24, :cond_0

    .line 2189748
    const/4 v4, 0x1

    .line 2189749
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v6

    goto/16 :goto_1

    .line 2189750
    :cond_13
    const/16 v24, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v1}, LX/186;->c(I)V

    .line 2189751
    const/16 v24, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v24

    move/from16 v2, v23

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2189752
    const/16 v23, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v23

    move/from16 v2, v22

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2189753
    const/16 v22, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v22

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2189754
    const/16 v21, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v21

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2189755
    const/16 v20, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v20

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2189756
    const/16 v19, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v19

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2189757
    const/16 v18, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v18

    move/from16 v2, v17

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2189758
    const/16 v17, 0x7

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, LX/186;->b(II)V

    .line 2189759
    const/16 v16, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v1, v15}, LX/186;->b(II)V

    .line 2189760
    const/16 v15, 0x9

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 2189761
    const/16 v14, 0xa

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v13}, LX/186;->b(II)V

    .line 2189762
    const/16 v13, 0xb

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v12}, LX/186;->b(II)V

    .line 2189763
    const/16 v12, 0xc

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v11}, LX/186;->b(II)V

    .line 2189764
    const/16 v11, 0xd

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v10}, LX/186;->b(II)V

    .line 2189765
    const/16 v10, 0xe

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v9}, LX/186;->b(II)V

    .line 2189766
    const/16 v9, 0xf

    move-object/from16 v0, p1

    invoke-virtual {v0, v9, v8}, LX/186;->b(II)V

    .line 2189767
    const/16 v8, 0x10

    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v5}, LX/186;->b(II)V

    .line 2189768
    if-eqz v4, :cond_14

    .line 2189769
    const/16 v5, 0x11

    const-wide/16 v8, 0x0

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, LX/186;->a(IJJ)V

    .line 2189770
    :cond_14
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v4

    goto/16 :goto_0
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 5

    .prologue
    const-wide/16 v2, 0x0

    .line 2189771
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2189772
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2189773
    if-eqz v0, :cond_0

    .line 2189774
    const-string v1, "accent_images"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189775
    invoke-static {p0, v0, p2, p3}, LX/F0R;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2189776
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2189777
    if-eqz v0, :cond_1

    .line 2189778
    const-string v1, "closing_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189779
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2189780
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2189781
    if-eqz v0, :cond_2

    .line 2189782
    const-string v1, "closing_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189783
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2189784
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2189785
    if-eqz v0, :cond_3

    .line 2189786
    const-string v1, "header_decorative_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189787
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2189788
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2189789
    if-eqz v0, :cond_4

    .line 2189790
    const-string v1, "header_favicon"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189791
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2189792
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2189793
    if-eqz v0, :cond_6

    .line 2189794
    const-string v1, "header_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189795
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2189796
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2189797
    if-eqz v1, :cond_5

    .line 2189798
    const-string v4, "text"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189799
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2189800
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2189801
    :cond_6
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2189802
    if-eqz v0, :cond_7

    .line 2189803
    const-string v1, "main_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189804
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2189805
    :cond_7
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2189806
    if-eqz v0, :cond_8

    .line 2189807
    const-string v1, "notice_button_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189808
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2189809
    :cond_8
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2189810
    if-eqz v0, :cond_9

    .line 2189811
    const-string v1, "notice_button_label"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189812
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2189813
    :cond_9
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2189814
    if-eqz v0, :cond_a

    .line 2189815
    const-string v1, "notice_image"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189816
    invoke-static {p0, v0, p2}, LX/32Q;->a(LX/15i;ILX/0nX;)V

    .line 2189817
    :cond_a
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2189818
    if-eqz v0, :cond_b

    .line 2189819
    const-string v1, "notice_subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189820
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2189821
    :cond_b
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2189822
    if-eqz v0, :cond_c

    .line 2189823
    const-string v1, "notice_title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189824
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2189825
    :cond_c
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2189826
    if-eqz v0, :cond_d

    .line 2189827
    const-string v1, "notice_title_color"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189828
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2189829
    :cond_d
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2189830
    if-eqz v0, :cond_e

    .line 2189831
    const-string v1, "render_style"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189832
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2189833
    :cond_e
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2189834
    if-eqz v0, :cond_f

    .line 2189835
    const-string v1, "theme"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189836
    invoke-static {p0, v0, p2, p3}, LX/F0T;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2189837
    :cond_f
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2189838
    if-eqz v0, :cond_11

    .line 2189839
    const-string v1, "throwback_settings"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189840
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2189841
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2189842
    if-eqz v1, :cond_10

    .line 2189843
    const-string v4, "subscription_status"

    invoke-virtual {p2, v4}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189844
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2189845
    :cond_10
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2189846
    :cond_11
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2189847
    if-eqz v0, :cond_12

    .line 2189848
    const-string v1, "throwback_units"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189849
    invoke-static {p0, v0, p2, p3}, LX/F0X;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2189850
    :cond_12
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2189851
    cmp-long v2, v0, v2

    if-eqz v2, :cond_13

    .line 2189852
    const-string v2, "title_date"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2189853
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2189854
    :cond_13
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2189855
    return-void
.end method
