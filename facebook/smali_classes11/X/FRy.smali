.class public LX/FRy;
.super LX/6tx;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/6tx",
        "<",
        "Lcom/facebook/payments/settings/protocol/GetPayAccountResult;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2241163
    const-class v0, Lcom/facebook/payments/settings/protocol/GetPayAccountResult;

    invoke-direct {p0, p1, v0}, LX/6tx;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;Ljava/lang/Class;)V

    .line 2241164
    return-void
.end method

.method public static b(LX/0QB;)LX/FRy;
    .locals 2

    .prologue
    .line 2241165
    new-instance v1, LX/FRy;

    invoke-static {p0}, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;->b(LX/0QB;)Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/common/PaymentNetworkOperationHelper;

    invoke-direct {v1, v0}, LX/FRy;-><init>(Lcom/facebook/payments/common/PaymentNetworkOperationHelper;)V

    .line 2241166
    return-object v1
.end method


# virtual methods
.method public final a(LX/1pN;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 2241167
    invoke-virtual {p1}, LX/1pN;->d()LX/0lF;

    move-result-object v0

    .line 2241168
    const-string v1, "viewer"

    invoke-virtual {v0, v1}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    .line 2241169
    const-string v2, "pay_account"

    invoke-virtual {v1, v2}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0lF;

    .line 2241170
    new-instance v2, Lcom/facebook/payments/settings/protocol/GetPayAccountResult;

    const-string v3, "balance"

    invoke-virtual {v1, v3}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v3

    .line 2241171
    invoke-static {v3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2241172
    new-instance v4, Lcom/facebook/payments/currency/CurrencyAmount;

    const-string v5, "currency"

    invoke-virtual {v3, v5}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v5

    invoke-static {v5}, LX/16N;->b(LX/0lF;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "amount_in_hundredths"

    invoke-virtual {v3, v6}, LX/0lF;->a(Ljava/lang/String;)LX/0lF;

    move-result-object v6

    invoke-static {v6}, LX/16N;->c(LX/0lF;)J

    move-result-wide v6

    invoke-direct {v4, v5, v6, v7}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;J)V

    move-object v3, v4

    .line 2241173
    const-string v4, "subscriptions"

    invoke-static {v1, v4}, LX/16N;->c(LX/0lF;Ljava/lang/String;)Ljava/lang/Iterable;

    move-result-object v4

    invoke-static {v4}, LX/0Ph;->b(Ljava/lang/Iterable;)I

    move-result v4

    move v1, v4

    .line 2241174
    invoke-direct {v2, v3, v1}, Lcom/facebook/payments/settings/protocol/GetPayAccountResult;-><init>(Lcom/facebook/payments/currency/CurrencyAmount;I)V

    move-object v0, v2

    .line 2241175
    return-object v0
.end method

.method public final b()LX/14N;
    .locals 4

    .prologue
    .line 2241176
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v0

    .line 2241177
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "q"

    const-string v3, "viewer() {pay_account {balance {currency, amount_in_hundredths},subscriptions{creation_time}}}"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2241178
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v1

    const-string v2, "get_pay_account"

    .line 2241179
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2241180
    move-object v1, v1

    .line 2241181
    const-string v2, "GET"

    .line 2241182
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2241183
    move-object v1, v1

    .line 2241184
    const-string v2, "graphql"

    .line 2241185
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2241186
    move-object v1, v1

    .line 2241187
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2241188
    move-object v0, v1

    .line 2241189
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2241190
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2241191
    move-object v0, v0

    .line 2241192
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2241193
    const-string v0, "get_pay_account"

    return-object v0
.end method
