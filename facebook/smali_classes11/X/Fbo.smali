.class public LX/Fbo;
.super LX/FbY;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FbY",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsPulseTopicMetadataUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Fbo;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2261088
    invoke-direct {p0}, LX/FbY;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/Fbo;
    .locals 3

    .prologue
    .line 2261089
    sget-object v0, LX/Fbo;->a:LX/Fbo;

    if-nez v0, :cond_1

    .line 2261090
    const-class v1, LX/Fbo;

    monitor-enter v1

    .line 2261091
    :try_start_0
    sget-object v0, LX/Fbo;->a:LX/Fbo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2261092
    if-eqz v2, :cond_0

    .line 2261093
    :try_start_1
    new-instance v0, LX/Fbo;

    invoke-direct {v0}, LX/Fbo;-><init>()V

    .line 2261094
    move-object v0, v0

    .line 2261095
    sput-object v0, LX/Fbo;->a:LX/Fbo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2261096
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2261097
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2261098
    :cond_1
    sget-object v0, LX/Fbo;->a:LX/Fbo;

    return-object v0

    .line 2261099
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2261100
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2261101
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2261102
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 2261103
    if-eqz v0, :cond_0

    new-instance v0, Lcom/facebook/search/results/model/unit/SearchResultsPulseTopicMetadataUnit;

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    invoke-virtual {p2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/search/results/model/unit/SearchResultsPulseTopicMetadataUnit;-><init>(Lcom/facebook/graphql/model/GraphQLNode;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
