.class public LX/FaH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/16E;
.implements LX/0i1;


# static fields
.field public static final a:Lcom/facebook/interstitial/manager/InterstitialTrigger;


# instance fields
.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2258582
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SEARCH_NULL_STATE:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    sput-object v0, LX/FaH;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2258583
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2258584
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2258585
    iput-object v0, p0, LX/FaH;->b:LX/0Ot;

    .line 2258586
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2258587
    iput-object v0, p0, LX/FaH;->c:LX/0Ot;

    .line 2258588
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2258589
    iput-object v0, p0, LX/FaH;->d:LX/0Ot;

    .line 2258590
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 3

    .prologue
    .line 2258591
    iget-object v0, p0, LX/FaH;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/3Qm;->l:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    move v0, v0

    .line 2258592
    if-eqz v0, :cond_0

    const-wide/16 v0, 0x2710

    :goto_0
    return-wide v0

    :cond_0
    const-wide/32 v0, 0x5265c00

    goto :goto_0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2258593
    iget-object v0, p0, LX/FaH;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-char v1, LX/100;->bV:C

    const-string v2, ""

    invoke-interface {v0, v1, v2}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2258594
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    .line 2258595
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/FaH;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/100;->bP:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/FaH;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0ad;

    sget-short v1, LX/100;->bN:S

    invoke-interface {v0, v1, v3}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    goto :goto_0

    :cond_1
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 2258596
    return-void
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2258597
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2258598
    instance-of v0, p2, LX/0g8;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2258599
    check-cast p2, LX/0g8;

    .line 2258600
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031281

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    .line 2258601
    iget-object v1, p0, LX/FaH;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    sget-char v2, LX/100;->bV:C

    const-string v3, ""

    invoke-interface {v1, v2, v3}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2258602
    iget-object v1, v0, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->a:Landroid/view/View;

    move-object v1, v1

    .line 2258603
    new-instance v2, LX/FaG;

    invoke-direct {v2, p0, p2, v0}, LX/FaG;-><init>(LX/FaH;LX/0g8;Lcom/facebook/fbui/widget/contentview/ContentView;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2258604
    invoke-interface {p2, v0}, LX/0g8;->d(Landroid/view/View;)V

    .line 2258605
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 2258606
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2258607
    const-string v0, "4446"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2258608
    sget-object v0, LX/FaH;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
