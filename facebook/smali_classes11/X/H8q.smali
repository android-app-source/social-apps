.class public LX/H8q;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8Z;


# static fields
.field private static final a:I

.field private static final b:I


# instance fields
.field private final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H8W;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

.field private h:Landroid/content/Context;

.field private i:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2433724
    const v0, 0x7f020952

    sput v0, LX/H8q;->a:I

    .line 2433725
    const v0, 0x7f081656

    sput v0, LX/H8q;->b:I

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)V
    .locals 3
    .param p6    # Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/H8W;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/17Y;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageActionData$Page;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2433715
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2433716
    iput-object p1, p0, LX/H8q;->c:LX/0Ot;

    .line 2433717
    iput-object p5, p0, LX/H8q;->d:LX/0Ot;

    .line 2433718
    iput-object p2, p0, LX/H8q;->e:LX/0Ot;

    .line 2433719
    iput-object p4, p0, LX/H8q;->f:LX/0Ot;

    .line 2433720
    iput-object p6, p0, LX/H8q;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    .line 2433721
    const-class v0, Landroid/app/Activity;

    invoke-static {p7, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, LX/H8q;->h:Landroid/content/Context;

    .line 2433722
    invoke-interface {p3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uh;

    sget v1, LX/8Dm;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    iput-boolean v0, p0, LX/H8q;->i:Z

    .line 2433723
    return-void
.end method


# virtual methods
.method public final a()LX/HA7;
    .locals 7

    .prologue
    .line 2433726
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    sget v2, LX/H8q;->b:I

    sget v3, LX/H8q;->a:I

    const/4 v4, 0x1

    .line 2433727
    iget-object v5, p0, LX/H8q;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->E()LX/0Px;

    move-result-object v5

    .line 2433728
    new-instance v6, LX/8A4;

    invoke-direct {v6, v5}, LX/8A4;-><init>(Ljava/util/List;)V

    sget-object p0, LX/8A3;->EDIT_PROFILE:LX/8A3;

    invoke-virtual {v6, p0}, LX/8A4;->a(LX/8A3;)Z

    move-result v6

    move v5, v6

    .line 2433729
    move v5, v5

    .line 2433730
    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final b()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2433714
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    sget v2, LX/H8q;->b:I

    sget v3, LX/H8q;->a:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 2433704
    iget-object v0, p0, LX/H8q;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->w()Lcom/facebook/graphql/enums/GraphQLPlaceType;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLPlaceType;->RESIDENCE:Lcom/facebook/graphql/enums/GraphQLPlaceType;

    if-ne v0, v1, :cond_0

    .line 2433705
    iget-object v0, p0, LX/H8q;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8W;

    iget-object v1, p0, LX/H8q;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->p()Z

    move-result v1

    invoke-virtual {v0, v1}, LX/H8W;->a(Z)LX/CSL;

    move-result-object v0

    iget-object v1, p0, LX/H8q;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v1, p0, LX/H8q;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->s()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, LX/CSL;->b(JLjava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2433706
    iget-object v0, p0, LX/H8q;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x2780

    iget-object v1, p0, LX/H8q;->h:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-interface {v0, v2, v3, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2433707
    :goto_0
    return-void

    .line 2433708
    :cond_0
    iget-object v0, p0, LX/H8q;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9XE;

    sget-object v1, LX/9X4;->EVENT_ADMIN_EDIT_PAGE:LX/9X4;

    iget-object v2, p0, LX/H8q;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LX/9XE;->b(LX/9X2;J)V

    .line 2433709
    iget-boolean v0, p0, LX/H8q;->i:Z

    if-eqz v0, :cond_1

    sget-object v0, LX/8Dq;->d:Ljava/lang/String;

    .line 2433710
    :goto_1
    iget-object v1, p0, LX/H8q;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2433711
    iget-object v0, p0, LX/H8q;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    iget-object v2, p0, LX/H8q;->h:Landroid/content/Context;

    invoke-interface {v0, v2, v1}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    .line 2433712
    iget-object v0, p0, LX/H8q;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x2782

    iget-object v1, p0, LX/H8q;->h:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-interface {v0, v2, v3, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_0

    .line 2433713
    :cond_1
    sget-object v0, LX/8Dq;->c:Ljava/lang/String;

    goto :goto_1
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2433703
    const/4 v0, 0x0

    return-object v0
.end method
