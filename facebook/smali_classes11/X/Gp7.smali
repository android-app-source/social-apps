.class public LX/Gp7;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2394719
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0Px;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2394742
    if-nez p0, :cond_0

    .line 2394743
    const/4 v0, 0x0

    .line 2394744
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->PHOTO_GRAY_OVERLAY:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-virtual {p0, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static b(LX/0Px;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLInterfaces$InstantShoppingAnnotationsFragment$;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2394735
    if-nez p0, :cond_0

    move v0, v1

    .line 2394736
    :goto_0
    return v0

    .line 2394737
    :cond_0
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_2

    invoke-virtual {p0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingAnnotationsFragmentModel;

    .line 2394738
    sget-object v4, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->PHOTO:Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingAnnotationsFragmentModel;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentElementType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2394739
    const/4 v0, 0x1

    goto :goto_0

    .line 2394740
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v1

    .line 2394741
    goto :goto_0
.end method

.method public static c(LX/0Px;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2394732
    if-nez p0, :cond_0

    .line 2394733
    const/4 v0, 0x0

    .line 2394734
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->SHOW_INTERACTION_HINT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-virtual {p0, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static d(LX/0Px;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2394729
    if-nez p0, :cond_0

    .line 2394730
    const/4 v0, 0x0

    .line 2394731
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->EXPANDABLE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-virtual {p0, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static f(LX/0Px;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2394726
    if-nez p0, :cond_0

    .line 2394727
    const/4 v0, 0x0

    .line 2394728
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->FIT_TO_HEIGHT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-virtual {p0, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static h(LX/0Px;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2394723
    if-nez p0, :cond_0

    .line 2394724
    const/4 v0, 0x0

    .line 2394725
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->ADJUSTED_FIT_TO_HEIGHT:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-virtual {p0, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static i(LX/0Px;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2394720
    if-nez p0, :cond_0

    .line 2394721
    const/4 v0, 0x0

    .line 2394722
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;->LANDSCAPE_ENABLED:Lcom/facebook/graphql/enums/GraphQLInstantShoppingPresentationStyle;

    invoke-virtual {p0, v0}, LX/0Px;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
