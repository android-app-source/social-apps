.class public LX/Fl9;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/widget/FrameLayout;

.field public b:I

.field public c:Lcom/facebook/resources/ui/FbButton;

.field public d:I

.field private e:Landroid/content/Context;

.field private f:LX/0zG;

.field private g:Landroid/view/ViewStub;

.field private h:Landroid/view/View$OnClickListener;

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0zG;Landroid/view/ViewStub;Landroid/view/View$OnClickListener;I)V
    .locals 0

    .prologue
    .line 2279674
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2279675
    iput-object p1, p0, LX/Fl9;->e:Landroid/content/Context;

    .line 2279676
    iput-object p2, p0, LX/Fl9;->f:LX/0zG;

    .line 2279677
    iput-object p3, p0, LX/Fl9;->g:Landroid/view/ViewStub;

    .line 2279678
    iput-object p4, p0, LX/Fl9;->h:Landroid/view/View$OnClickListener;

    .line 2279679
    iput p5, p0, LX/Fl9;->i:I

    .line 2279680
    return-void
.end method

.method public static b(LX/Fl9;)Landroid/widget/FrameLayout;
    .locals 2

    .prologue
    .line 2279681
    iget-object v0, p0, LX/Fl9;->a:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    .line 2279682
    iget-object v0, p0, LX/Fl9;->a:Landroid/widget/FrameLayout;

    .line 2279683
    :goto_0
    return-object v0

    .line 2279684
    :cond_0
    iget-object v0, p0, LX/Fl9;->g:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, LX/Fl9;->a:Landroid/widget/FrameLayout;

    .line 2279685
    iget-object v0, p0, LX/Fl9;->a:Landroid/widget/FrameLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 2279686
    iget-object v0, p0, LX/Fl9;->a:Landroid/widget/FrameLayout;

    invoke-direct {p0}, LX/Fl9;->c()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    .line 2279687
    iget-object v0, p0, LX/Fl9;->a:Landroid/widget/FrameLayout;

    const v1, 0x7f0d13b3

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbButton;

    iput-object v0, p0, LX/Fl9;->c:Lcom/facebook/resources/ui/FbButton;

    .line 2279688
    iget-object v0, p0, LX/Fl9;->c:Lcom/facebook/resources/ui/FbButton;

    if-eqz v0, :cond_1

    .line 2279689
    iget-object v0, p0, LX/Fl9;->c:Lcom/facebook/resources/ui/FbButton;

    iget-object v1, p0, LX/Fl9;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2279690
    :cond_1
    invoke-direct {p0}, LX/Fl9;->c()I

    move-result v0

    iput v0, p0, LX/Fl9;->d:I

    .line 2279691
    iget-object v0, p0, LX/Fl9;->a:Landroid/widget/FrameLayout;

    goto :goto_0
.end method

.method private c()I
    .locals 2

    .prologue
    .line 2279692
    iget-object v0, p0, LX/Fl9;->f:LX/0zG;

    invoke-interface {v0}, LX/0QR;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0h5;

    .line 2279693
    instance-of v0, v0, LX/63T;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fl9;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0413

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget v1, p0, LX/Fl9;->i:I

    add-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
