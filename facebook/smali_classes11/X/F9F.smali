.class public final LX/F9F;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/4At;

.field public final synthetic c:Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;Ljava/lang/String;LX/4At;)V
    .locals 0

    .prologue
    .line 2205286
    iput-object p1, p0, LX/F9F;->c:Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;

    iput-object p2, p0, LX/F9F;->a:Ljava/lang/String;

    iput-object p3, p0, LX/F9F;->b:LX/4At;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2205287
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, LX/F9F;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 2205288
    iget-object v0, p0, LX/F9F;->b:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2205289
    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2205290
    invoke-direct {p0}, LX/F9F;->b()V

    .line 2205291
    iget-object v0, p0, LX/F9F;->c:Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;

    invoke-static {v0}, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;->l(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;)V

    .line 2205292
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2205293
    invoke-direct {p0}, LX/F9F;->b()V

    .line 2205294
    iget-object v0, p0, LX/F9F;->c:Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    instance-of v0, v0, LX/8DG;

    if-eqz v0, :cond_0

    .line 2205295
    const-string v1, "upload_profile_pic"

    .line 2205296
    iget-object v0, p0, LX/F9F;->c:Lcom/facebook/growth/nux/fragments/NUXProfilePhotoImportGoogleFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, LX/8DG;

    invoke-interface {v0, v1}, LX/8DG;->b(Ljava/lang/String;)V

    .line 2205297
    :cond_0
    return-void
.end method
