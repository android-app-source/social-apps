.class public LX/H9r;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8Z;


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I


# instance fields
.field public final d:LX/H8W;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Sa;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H89;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8DD;",
            ">;"
        }
    .end annotation
.end field

.field public k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

.field public l:Landroid/content/Context;

.field private m:LX/H8E;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2434696
    const v0, 0x7f020781

    sput v0, LX/H9r;->a:I

    .line 2434697
    const v0, 0x7f081788

    sput v0, LX/H9r;->b:I

    .line 2434698
    const v0, 0x7f08178a

    sput v0, LX/H9r;->c:I

    return-void
.end method

.method public constructor <init>(LX/H8W;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)V
    .locals 0
    .param p8    # Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/H8W;",
            "LX/0Ot",
            "<",
            "LX/1Sa;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1Ck;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/H89;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/8DD;",
            ">;",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageActionData$Page;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2434685
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2434686
    iput-object p1, p0, LX/H9r;->d:LX/H8W;

    .line 2434687
    iput-object p2, p0, LX/H9r;->e:LX/0Ot;

    .line 2434688
    iput-object p3, p0, LX/H9r;->f:LX/0Ot;

    .line 2434689
    iput-object p4, p0, LX/H9r;->g:LX/0Ot;

    .line 2434690
    iput-object p5, p0, LX/H9r;->h:LX/0Ot;

    .line 2434691
    iput-object p6, p0, LX/H9r;->i:LX/0Ot;

    .line 2434692
    iput-object p7, p0, LX/H9r;->j:LX/0Ot;

    .line 2434693
    iput-object p8, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    .line 2434694
    iput-object p9, p0, LX/H9r;->l:Landroid/content/Context;

    .line 2434695
    return-void
.end method

.method public static a$redex0(LX/H9r;Z)V
    .locals 2

    .prologue
    .line 2434628
    iget-object v0, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    instance-of v0, v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    if-eqz v0, :cond_0

    invoke-direct {p0}, LX/H9r;->g()Z

    move-result v0

    if-ne v0, p1, :cond_1

    .line 2434629
    :cond_0
    :goto_0
    return-void

    .line 2434630
    :cond_1
    iget-object v0, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    check-cast v0, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-static {v0}, LX/9YE;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;)LX/9YE;

    move-result-object v1

    if-eqz p1, :cond_2

    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2434631
    :goto_1
    iput-object v0, v1, LX/9YE;->C:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2434632
    move-object v0, v1

    .line 2434633
    invoke-virtual {v0}, LX/9YE;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    move-result-object v0

    iput-object v0, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    .line 2434634
    iget-object v0, p0, LX/H9r;->d:LX/H8W;

    new-instance v1, LX/HDa;

    invoke-direct {v1, p1}, LX/HDa;-><init>(Z)V

    invoke-virtual {v0, v1}, LX/H8W;->a(LX/HDS;)V

    goto :goto_0

    .line 2434635
    :cond_2
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    goto :goto_1
.end method

.method public static b(LX/H9r;Z)LX/9XB;
    .locals 1

    .prologue
    .line 2434680
    if-eqz p1, :cond_1

    .line 2434681
    iget-object v0, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/9XB;->EVENT_PLACE_SAVE_SUCCESS:LX/9XB;

    .line 2434682
    :goto_0
    return-object v0

    .line 2434683
    :cond_0
    sget-object v0, LX/9XB;->EVENT_PAGE_SAVE_SUCCESS:LX/9XB;

    goto :goto_0

    .line 2434684
    :cond_1
    iget-object v0, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/9XB;->EVENT_PLACE_UNSAVE_SUCCESS:LX/9XB;

    goto :goto_0

    :cond_2
    sget-object v0, LX/9XB;->EVENT_PAGE_UNSAVE_SUCCESS:LX/9XB;

    goto :goto_0
.end method

.method public static c(LX/H9r;Z)LX/9XA;
    .locals 1

    .prologue
    .line 2434675
    if-eqz p1, :cond_1

    .line 2434676
    iget-object v0, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/9XA;->EVENT_PLACE_SAVE_ERROR:LX/9XA;

    .line 2434677
    :goto_0
    return-object v0

    .line 2434678
    :cond_0
    sget-object v0, LX/9XA;->EVENT_PAGE_SAVE_ERROR:LX/9XA;

    goto :goto_0

    .line 2434679
    :cond_1
    iget-object v0, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/9XA;->EVENT_PLACE_UNSAVE_ERROR:LX/9XA;

    goto :goto_0

    :cond_2
    sget-object v0, LX/9XA;->EVENT_PAGE_UNSAVE_ERROR:LX/9XA;

    goto :goto_0
.end method

.method private g()Z
    .locals 2

    .prologue
    .line 2434674
    iget-object v0, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->F()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()LX/HA7;
    .locals 9

    .prologue
    const/4 v4, 0x1

    .line 2434667
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    invoke-direct {p0}, LX/H9r;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    sget v2, LX/H9r;->c:I

    :goto_0
    sget v3, LX/H9r;->a:I

    const/4 v5, 0x0

    .line 2434668
    iget-object v6, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->y()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v6

    if-eqz v6, :cond_0

    iget-object v6, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->F()Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v6

    sget-object v7, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVABLE:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v6, v7, :cond_2

    .line 2434669
    :cond_0
    :goto_1
    move v5, v5

    .line 2434670
    invoke-direct {p0}, LX/H9r;->g()Z

    move-result v7

    const/4 v8, 0x0

    move v6, v4

    invoke-direct/range {v0 .. v8}, LX/HA7;-><init>(IIIIZZZLjava/lang/String;)V

    return-object v0

    :cond_1
    sget v2, LX/H9r;->b:I

    goto :goto_0

    .line 2434671
    :cond_2
    iget-object v6, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->E()LX/0Px;

    move-result-object v6

    if-eqz v6, :cond_3

    iget-object v6, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->E()LX/0Px;

    move-result-object v6

    invoke-static {v6}, LX/8A4;->a(LX/0Px;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 2434672
    :cond_3
    iget-object v6, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->y()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v6

    .line 2434673
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;->b()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v6}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;->j()Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v6}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;->d()LX/1oQ;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v6}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;->d()LX/1oQ;

    move-result-object v6

    invoke-interface {v6}, LX/1oQ;->c()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    const/4 v5, 0x1

    goto :goto_1
.end method

.method public final b()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2434666
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    sget v2, LX/H9r;->b:I

    sget v3, LX/H9r;->a:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final c()V
    .locals 8

    .prologue
    .line 2434639
    iget-object v0, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->y()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2434640
    :goto_0
    return-void

    .line 2434641
    :cond_0
    invoke-direct {p0}, LX/H9r;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2434642
    const/4 v3, 0x0

    .line 2434643
    invoke-static {p0, v3}, LX/H9r;->a$redex0(LX/H9r;Z)V

    .line 2434644
    iget-object v0, p0, LX/H9r;->j:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8DD;

    invoke-virtual {v0}, LX/8D6;->b()V

    .line 2434645
    iget-object v1, p0, LX/H9r;->d:LX/H8W;

    iget-object v0, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->m()Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, LX/9XI;->EVENT_TAPPED_UNSAVE_PLACE:LX/9XI;

    :goto_1
    iget-object v2, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2434646
    invoke-static {p0, v3}, LX/H9r;->b(LX/H9r;Z)LX/9XB;

    move-result-object v2

    .line 2434647
    invoke-static {p0, v3}, LX/H9r;->c(LX/H9r;Z)LX/9XA;

    move-result-object v3

    .line 2434648
    iget-object v0, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->y()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2434649
    iget-object v0, p0, LX/H9r;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    sget-object v4, LX/8Dp;->UNSAVE:LX/8Dp;

    iget-object v1, p0, LX/H9r;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/H89;

    iget-object v5, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->y()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;->b()Ljava/lang/String;

    move-result-object v6

    .line 2434650
    const/4 v7, 0x0

    invoke-static {v1, v5, v6, v7}, LX/H89;->a(LX/H89;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v1, v7

    .line 2434651
    new-instance v5, LX/H9o;

    invoke-direct {v5, p0, v2, v3}, LX/H9o;-><init>(LX/H9r;LX/9XB;LX/9XA;)V

    invoke-virtual {v0, v4, v1, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2434652
    :cond_1
    goto :goto_0

    .line 2434653
    :cond_2
    const/4 v0, 0x1

    .line 2434654
    invoke-static {p0, v0}, LX/H9r;->b(LX/H9r;Z)LX/9XB;

    move-result-object v2

    .line 2434655
    invoke-static {p0, v0}, LX/H9r;->c(LX/H9r;Z)LX/9XA;

    move-result-object v3

    .line 2434656
    invoke-static {p0, v0}, LX/H9r;->a$redex0(LX/H9r;Z)V

    .line 2434657
    iget-object v0, p0, LX/H9r;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Sa;

    iget-object v1, p0, LX/H9r;->l:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/1Sa;->a(Landroid/content/Context;)V

    .line 2434658
    iget-object v1, p0, LX/H9r;->d:LX/H8W;

    iget-object v0, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->m()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, LX/9XI;->EVENT_TAPPED_SAVE_PLACE:LX/9XI;

    :goto_2
    iget-object v4, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2434659
    iget-object v0, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->y()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2434660
    iget-object v0, p0, LX/H9r;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Ck;

    sget-object v4, LX/8Dp;->SAVE:LX/8Dp;

    iget-object v1, p0, LX/H9r;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/H89;

    iget-object v5, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, LX/H9r;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->y()Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/api/graphql/saved/SaveDefaultsGraphQLModels$SavableTimelineAppCollectionExtraFieldsModel;->b()Ljava/lang/String;

    move-result-object v6

    .line 2434661
    const/4 v7, 0x1

    invoke-static {v1, v5, v6, v7}, LX/H89;->a(LX/H89;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v7

    move-object v1, v7

    .line 2434662
    new-instance v5, LX/H9n;

    invoke-direct {v5, p0, v2, v3}, LX/H9n;-><init>(LX/H9r;LX/9XB;LX/9XA;)V

    invoke-virtual {v0, v4, v1, v5}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2434663
    :cond_3
    goto/16 :goto_0

    .line 2434664
    :cond_4
    sget-object v0, LX/9XI;->PAGE_EVENT_TAPPED_UNSAVE_MEDIA_PAGE:LX/9XI;

    goto/16 :goto_1

    .line 2434665
    :cond_5
    sget-object v0, LX/9XI;->PAGE_EVENT_TAPPED_SAVE_MEDIA_PAGE:LX/9XI;

    goto :goto_2
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2434636
    iget-object v0, p0, LX/H9r;->m:LX/H8E;

    if-nez v0, :cond_0

    .line 2434637
    new-instance v0, LX/H9q;

    invoke-direct {v0, p0}, LX/H9q;-><init>(LX/H9r;)V

    iput-object v0, p0, LX/H9r;->m:LX/H8E;

    .line 2434638
    :cond_0
    iget-object v0, p0, LX/H9r;->m:LX/H8E;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
