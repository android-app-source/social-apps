.class public LX/Grf;
.super LX/CtQ;
.source ""


# direct methods
.method public constructor <init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;FF)V
    .locals 0

    .prologue
    .line 2398606
    invoke-direct {p0, p1, p2, p3}, LX/CtQ;-><init>(Landroid/support/v7/widget/RecyclerView;FF)V

    .line 2398607
    return-void
.end method


# virtual methods
.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 2398608
    monitor-enter p0

    .line 2398609
    :try_start_0
    iget-object v0, p0, LX/CtQ;->b:Ljava/util/List;

    move-object v0, v0

    .line 2398610
    invoke-static {v0}, LX/18h;->a(Ljava/util/Collection;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 2398611
    :goto_0
    monitor-exit p0

    return-void

    .line 2398612
    :cond_0
    :try_start_1
    invoke-virtual {p0}, LX/Grf;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2398613
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 6

    .prologue
    .line 2398614
    monitor-enter p0

    const/4 v1, 0x0

    .line 2398615
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2398616
    iget-object v0, p0, LX/CtQ;->b:Ljava/util/List;

    move-object v0, v0

    .line 2398617
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Coc;

    .line 2398618
    invoke-interface {v0}, LX/Coc;->p()Landroid/view/View;

    move-result-object v0

    .line 2398619
    instance-of v4, v0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    if-eqz v4, :cond_1

    .line 2398620
    invoke-static {v0}, LX/Csu;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {v0}, LX/Csu;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    if-lt v4, v5, :cond_1

    .line 2398621
    :goto_1
    if-nez v0, :cond_3

    .line 2398622
    invoke-virtual {p0, v2}, LX/Csu;->a(Ljava/util/Collection;)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 2398623
    :goto_2
    if-eqz v1, :cond_2

    .line 2398624
    iget-object v0, p0, LX/CtQ;->b:Ljava/util/List;

    move-object v0, v0

    .line 2398625
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Coc;

    .line 2398626
    invoke-interface {v0}, LX/Coc;->p()Landroid/view/View;

    move-result-object v3

    if-ne v1, v3, :cond_0

    invoke-interface {v0}, LX/Coc;->q()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2398627
    invoke-interface {v0}, LX/Coc;->r()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 2398628
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2398629
    :cond_1
    :try_start_1
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2398630
    :cond_2
    monitor-exit p0

    return-void

    :cond_3
    move-object v1, v0

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method
