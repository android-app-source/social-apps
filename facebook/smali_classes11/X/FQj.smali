.class public LX/FQj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation build Ljavax/annotation/concurrent/ThreadSafe;
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile d:LX/FQj;


# instance fields
.field private final a:LX/FQk;

.field private final b:LX/2U2;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/2U2",
            "<",
            "Ljava/lang/String;",
            "LX/FQk;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/0SG;


# direct methods
.method public constructor <init>(LX/0rd;LX/0SG;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2239634
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2239635
    new-instance v0, LX/FQk;

    sget-object v1, LX/FQi;->NONE:LX/FQi;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, LX/FQk;-><init>(LX/FQi;Z)V

    iput-object v0, p0, LX/FQj;->a:LX/FQk;

    .line 2239636
    iput-object p2, p0, LX/FQj;->c:LX/0SG;

    .line 2239637
    new-instance v0, LX/2U2;

    iget-object v1, p0, LX/FQj;->c:LX/0SG;

    const/16 v2, 0x80

    invoke-direct {v0, p1, v1, v2}, LX/2U2;-><init>(LX/0rd;LX/0SG;I)V

    iput-object v0, p0, LX/FQj;->b:LX/2U2;

    .line 2239638
    return-void
.end method

.method public static a(LX/0QB;)LX/FQj;
    .locals 5

    .prologue
    .line 2239639
    sget-object v0, LX/FQj;->d:LX/FQj;

    if-nez v0, :cond_1

    .line 2239640
    const-class v1, LX/FQj;

    monitor-enter v1

    .line 2239641
    :try_start_0
    sget-object v0, LX/FQj;->d:LX/FQj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2239642
    if-eqz v2, :cond_0

    .line 2239643
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2239644
    new-instance p0, LX/FQj;

    invoke-static {v0}, LX/0rZ;->a(LX/0QB;)LX/0rd;

    move-result-object v3

    check-cast v3, LX/0rd;

    invoke-static {v0}, LX/0SB;->a(LX/0QB;)LX/0SF;

    move-result-object v4

    check-cast v4, LX/0SG;

    invoke-direct {p0, v3, v4}, LX/FQj;-><init>(LX/0rd;LX/0SG;)V

    .line 2239645
    move-object v0, p0

    .line 2239646
    sput-object v0, LX/FQj;->d:LX/FQj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2239647
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2239648
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2239649
    :cond_1
    sget-object v0, LX/FQj;->d:LX/FQj;

    return-object v0

    .line 2239650
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2239651
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final clearUserData()V
    .locals 1

    .prologue
    .line 2239632
    iget-object v0, p0, LX/FQj;->b:LX/2U2;

    invoke-virtual {v0}, LX/2U2;->a()V

    .line 2239633
    return-void
.end method
