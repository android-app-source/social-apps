.class public LX/Gmy;
.super LX/3x6;
.source ""

# interfaces
.implements LX/0Ya;


# instance fields
.field public final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "LX/Gmx;",
            ">;"
        }
    .end annotation
.end field

.field public b:I

.field public c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2393279
    invoke-direct {p0}, LX/3x6;-><init>()V

    .line 2393280
    const/4 v0, -0x1

    iput v0, p0, LX/Gmy;->b:I

    .line 2393281
    iput-object p1, p0, LX/Gmy;->c:Landroid/content/Context;

    .line 2393282
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Gmy;->a:Ljava/util/Map;

    .line 2393283
    return-void
.end method

.method private static a(LX/CoJ;I)LX/Clr;
    .locals 1

    .prologue
    .line 2393276
    if-ltz p1, :cond_0

    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 2393277
    :cond_0
    const/4 v0, 0x0

    .line 2393278
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, p1}, LX/CoJ;->e(I)LX/Clr;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;I)I
    .locals 1

    .prologue
    .line 2393242
    int-to-float v0, p1

    invoke-static {p0, v0}, LX/0tP;->a(Landroid/content/Context;F)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 10

    .prologue
    .line 2393255
    invoke-virtual {p2}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v7

    .line 2393256
    new-instance v5, Landroid/graphics/Paint;

    invoke-direct {v5}, Landroid/graphics/Paint;-><init>()V

    .line 2393257
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v7, :cond_0

    .line 2393258
    invoke-virtual {p2, v6}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 2393259
    invoke-virtual {p2, v4}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)LX/1a1;

    move-result-object v0

    check-cast v0, LX/Cs4;

    .line 2393260
    iget-object v1, p2, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v1, v1

    .line 2393261
    check-cast v1, LX/CoJ;

    invoke-virtual {v0}, LX/1a1;->e()I

    move-result v2

    invoke-static {v1, v2}, LX/Gmy;->a(LX/CoJ;I)LX/Clr;

    move-result-object v2

    .line 2393262
    iget v3, p0, LX/Gmy;->b:I

    .line 2393263
    if-eqz v2, :cond_1

    instance-of v1, v2, LX/GoX;

    if-eqz v1, :cond_1

    move-object v1, v2

    check-cast v1, LX/GoX;

    invoke-interface {v1}, LX/GoX;->lE_()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2393264
    check-cast v2, LX/GoX;

    invoke-interface {v2}, LX/GoX;->lE_()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    .line 2393265
    :goto_1
    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 2393266
    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2393267
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, LX/1a3;

    .line 2393268
    iget-object v2, p0, LX/Gmy;->a:Ljava/util/Map;

    invoke-virtual {v0}, LX/1a1;->e()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gmx;

    .line 2393269
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v2

    iget v3, v0, LX/Gmx;->a:I

    sub-int/2addr v2, v3

    iget v3, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v2, v3

    .line 2393270
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v3

    iget v8, v0, LX/Gmx;->b:I

    sub-int/2addr v3, v8

    .line 2393271
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v8

    iget v9, v0, LX/Gmx;->c:I

    add-int/2addr v8, v9

    iget v1, v1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v8, v1

    .line 2393272
    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v1

    iget v0, v0, LX/Gmx;->d:I

    add-int/2addr v0, v1

    .line 2393273
    int-to-float v1, v2

    int-to-float v2, v3

    int-to-float v3, v8

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 2393274
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 2393275
    :cond_0
    return-void

    :cond_1
    move v1, v3

    goto :goto_1
.end method

.method public final a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;LX/1Ok;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2393243
    invoke-static {p2}, Landroid/support/v7/widget/RecyclerView;->d(Landroid/view/View;)I

    move-result v6

    .line 2393244
    iget-object v0, p0, LX/Gmy;->a:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gmx;

    .line 2393245
    if-eqz v0, :cond_0

    .line 2393246
    iget v1, v0, LX/Gmx;->a:I

    iget v2, v0, LX/Gmx;->b:I

    iget v3, v0, LX/Gmx;->c:I

    iget v0, v0, LX/Gmx;->d:I

    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 2393247
    :goto_0
    return-void

    .line 2393248
    :cond_0
    iget-object v0, p3, Landroid/support/v7/widget/RecyclerView;->o:LX/1OM;

    move-object v0, v0

    .line 2393249
    check-cast v0, LX/CoJ;

    .line 2393250
    invoke-static {v0, v6}, LX/Gmy;->a(LX/CoJ;I)LX/Clr;

    move-result-object v0

    .line 2393251
    if-eqz v0, :cond_1

    instance-of v1, v0, LX/GoX;

    if-eqz v1, :cond_1

    new-instance v1, LX/Gmx;

    check-cast v0, LX/GoX;

    invoke-direct {v1, p0, v0, v2}, LX/Gmx;-><init>(LX/Gmy;LX/GoX;I)V

    move-object v0, v1

    .line 2393252
    :goto_1
    iget-object v1, p0, LX/Gmy;->a:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2393253
    iget v1, v0, LX/Gmx;->a:I

    iget v2, v0, LX/Gmx;->b:I

    iget v3, v0, LX/Gmx;->c:I

    iget v0, v0, LX/Gmx;->d:I

    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 2393254
    :cond_1
    new-instance v0, LX/Gmx;

    move-object v1, p0

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, LX/Gmx;-><init>(LX/Gmy;IIII)V

    goto :goto_1
.end method
