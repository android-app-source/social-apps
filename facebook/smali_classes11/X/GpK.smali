.class public LX/GpK;
.super LX/Gop;
.source ""

# interfaces
.implements LX/Gob;
.implements LX/Clr;


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:LX/GoE;

.field public final g:Ljava/util/Calendar;

.field public final h:Ljava/util/Calendar;

.field public final i:Z


# direct methods
.method public constructor <init>(LX/GpJ;)V
    .locals 2

    .prologue
    .line 2394863
    invoke-direct {p0, p1}, LX/Gop;-><init>(LX/Gor;)V

    .line 2394864
    iget-object v0, p1, LX/GpJ;->a:Ljava/lang/String;

    iput-object v0, p0, LX/GpK;->a:Ljava/lang/String;

    .line 2394865
    iget v0, p1, LX/GpJ;->b:I

    iput v0, p0, LX/GpK;->b:I

    .line 2394866
    iget v0, p1, LX/GpJ;->c:I

    iput v0, p0, LX/GpK;->c:I

    .line 2394867
    iget v0, p1, LX/GpJ;->d:I

    iput v0, p0, LX/GpK;->d:I

    .line 2394868
    iget v0, p1, LX/GpJ;->e:I

    iput v0, p0, LX/GpK;->e:I

    .line 2394869
    iget-object v0, p1, LX/GpJ;->f:LX/GoE;

    iput-object v0, p0, LX/GpK;->f:LX/GoE;

    .line 2394870
    iget v0, p0, LX/GpK;->c:I

    iget v1, p0, LX/GpK;->b:I

    invoke-static {v0, v1}, LX/GpK;->a(II)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/GpK;->g:Ljava/util/Calendar;

    .line 2394871
    iget v0, p0, LX/GpK;->e:I

    iget v1, p0, LX/GpK;->d:I

    invoke-static {v0, v1}, LX/GpK;->a(II)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, LX/GpK;->h:Ljava/util/Calendar;

    .line 2394872
    iget-object v0, p0, LX/GpK;->g:Ljava/util/Calendar;

    iget-object v1, p0, LX/GpK;->h:Ljava/util/Calendar;

    invoke-static {v0, v1}, LX/GpK;->a(Ljava/util/Calendar;Ljava/util/Calendar;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/GpK;->i:Z

    .line 2394873
    return-void

    .line 2394874
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(II)Ljava/util/Calendar;
    .locals 3

    .prologue
    .line 2394875
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 2394876
    const/4 v1, 0x2

    add-int/lit8 v2, p0, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 2394877
    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    .line 2394878
    return-object v0
.end method

.method public static a(Ljava/util/Calendar;Ljava/util/Calendar;)Z
    .locals 4

    .prologue
    const/4 v2, 0x5

    const/4 v3, 0x2

    const/4 v0, 0x1

    .line 2394879
    invoke-virtual {p0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    invoke-virtual {p1, v0}, Ljava/util/Calendar;->get(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final D()LX/GoE;
    .locals 1

    .prologue
    .line 2394880
    iget-object v0, p0, LX/GpK;->f:LX/GoE;

    return-object v0
.end method
