.class public LX/FJT;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private final b:LX/1Vr;

.field private final c:Lcom/facebook/content/SecureContextHelper;

.field private final d:LX/FIz;

.field private final e:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0tX;

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2223617
    const-class v0, LX/FJT;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/FJT;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1Vg;Lcom/facebook/content/SecureContextHelper;LX/FIz;LX/1Ck;LX/0tX;LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Vg;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/FIz;",
            "LX/1Ck;",
            "LX/0tX;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2223618
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2223619
    new-instance v0, LX/1Wx;

    invoke-direct {v0}, LX/1Wx;-><init>()V

    invoke-virtual {p1, v0}, LX/1Vg;->a(LX/1Vq;)LX/1Vr;

    move-result-object v0

    iput-object v0, p0, LX/FJT;->b:LX/1Vr;

    .line 2223620
    iput-object p2, p0, LX/FJT;->c:Lcom/facebook/content/SecureContextHelper;

    .line 2223621
    iput-object p3, p0, LX/FJT;->d:LX/FIz;

    .line 2223622
    iput-object p4, p0, LX/FJT;->e:LX/1Ck;

    .line 2223623
    iput-object p5, p0, LX/FJT;->f:LX/0tX;

    .line 2223624
    iput-object p6, p0, LX/FJT;->g:LX/0Ot;

    .line 2223625
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/6gC;Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;ZLX/FJK;)V
    .locals 6
    .param p5    # LX/FJK;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2223626
    invoke-virtual {p0, p1}, LX/FJT;->a(Landroid/content/Context;)Z

    move-result v1

    .line 2223627
    iget-object v0, p0, LX/FJT;->d:LX/FIz;

    invoke-virtual {p3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;->b()Ljava/lang/String;

    move-result-object v2

    .line 2223628
    new-instance v4, Lcom/facebook/analytics/logger/HoneyClientEvent;

    const-string v3, "moments_invite_clicked"

    invoke-direct {v4, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2223629
    invoke-static {p2, v4}, LX/FIz;->a(LX/6gC;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2223630
    const-string v3, "share_id"

    .line 2223631
    iget-object v5, p2, LX/6gC;->f:Ljava/lang/String;

    move-object v5, v5

    .line 2223632
    invoke-virtual {v4, v3, v5}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2223633
    const-string v3, "invite_app_installed"

    invoke-virtual {v4, v3, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Z)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2223634
    const-string v5, "invite_click_target"

    if-eqz p4, :cond_8

    const-string v3, "photo"

    :goto_0
    invoke-virtual {v4, v5, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2223635
    const-string v3, "invite_click_action"

    invoke-virtual {v4, v3, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2223636
    iget-object v3, v0, LX/FIz;->a:LX/0Zb;

    invoke-interface {v3, v4}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2223637
    if-eqz v1, :cond_1

    invoke-virtual {p3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;->cU_()Ljava/lang/String;

    move-result-object v0

    .line 2223638
    :goto_1
    if-eqz v0, :cond_4

    .line 2223639
    invoke-virtual {p3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;->b()Ljava/lang/String;

    .line 2223640
    invoke-virtual {p3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;->a()Lcom/facebook/graphql/enums/GraphQLMomentsAppMessengerInviteActionType;

    move-result-object v2

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLMomentsAppMessengerInviteActionType;->INSTALL:Lcom/facebook/graphql/enums/GraphQLMomentsAppMessengerInviteActionType;

    if-ne v2, v3, :cond_2

    .line 2223641
    iget-object v1, p0, LX/FJT;->b:LX/1Vr;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, LX/1Vr;->a(Ljava/lang/String;Landroid/content/Context;)V

    .line 2223642
    :cond_0
    :goto_2
    return-void

    .line 2223643
    :cond_1
    invoke-virtual {p3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;->cV_()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2223644
    :cond_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2223645
    const/4 v3, 0x0

    .line 2223646
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xe

    if-ge v4, v5, :cond_9

    .line 2223647
    :cond_3
    :goto_3
    move v3, v3

    .line 2223648
    if-eqz v3, :cond_6

    .line 2223649
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/facebook/browser/lite/BrowserLiteActivity;

    invoke-direct {v0, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    .line 2223650
    new-instance v2, LX/0DW;

    invoke-direct {v2}, LX/0DW;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2, v3}, LX/0DW;->a(Ljava/util/Locale;)LX/0DW;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, LX/0DW;->b(Z)LX/0DW;

    move-result-object v2

    .line 2223651
    invoke-virtual {v2}, LX/0DW;->a()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 2223652
    iget-object v2, p0, LX/FJT;->c:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v2, v0, p1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2223653
    :cond_4
    :goto_4
    if-eqz v1, :cond_7

    invoke-virtual {p3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 2223654
    :goto_5
    if-eqz v0, :cond_0

    .line 2223655
    invoke-virtual {p3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;->b()Ljava/lang/String;

    .line 2223656
    new-instance v1, LX/4HG;

    invoke-direct {v1}, LX/4HG;-><init>()V

    .line 2223657
    const-string v2, "payload_json"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2223658
    new-instance v0, LX/FJ0;

    invoke-direct {v0}, LX/FJ0;-><init>()V

    move-object v0, v0

    .line 2223659
    const-string v2, "input"

    invoke-virtual {v0, v2, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2223660
    if-eqz p5, :cond_5

    .line 2223661
    invoke-interface {p5}, LX/FJK;->a()V

    .line 2223662
    :cond_5
    iget-object v1, p0, LX/FJT;->e:LX/1Ck;

    invoke-virtual {p3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;->b()Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/FJR;

    invoke-direct {v3, p0, v0}, LX/FJR;-><init>(LX/FJT;LX/FJ0;)V

    new-instance v0, LX/FJS;

    invoke-direct {v0, p0, p5, p1}, LX/FJS;-><init>(LX/FJT;LX/FJK;Landroid/content/Context;)V

    invoke-virtual {v1, v2, v3, v0}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    goto :goto_2

    .line 2223663
    :cond_6
    iget-object v2, p0, LX/FJT;->b:LX/1Vr;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, LX/1Vr;->b(Ljava/lang/String;Landroid/content/Context;)V

    goto :goto_4

    .line 2223664
    :cond_7
    invoke-virtual {p3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 2223665
    :cond_8
    const-string v3, "button"

    goto/16 :goto_0

    .line 2223666
    :cond_9
    invoke-static {v2}, LX/1H1;->c(Landroid/net/Uri;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2223667
    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    .line 2223668
    if-eqz v4, :cond_3

    const-string v5, "moments_app"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2223669
    const/4 v3, 0x1

    goto/16 :goto_3
.end method

.method public final a(Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 2223670
    iget-object v0, p0, LX/FJT;->b:LX/1Vr;

    invoke-virtual {v0}, LX/1Vr;->a()Z

    move-result v0

    return v0
.end method
