.class public final LX/Gmg;
.super LX/ChV;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;)V
    .locals 0

    .prologue
    .line 2392557
    iput-object p1, p0, LX/Gmg;->a:Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;

    invoke-direct {p0}, LX/ChV;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 7

    .prologue
    .line 2392558
    iget-object v0, p0, LX/Gmg;->a:Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;

    const/4 v4, 0x0

    .line 2392559
    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    if-eqz v1, :cond_2

    .line 2392560
    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v1}, LX/CsS;->getFragmentCount()I

    move-result v6

    move v5, v4

    move v2, v4

    move v3, v4

    .line 2392561
    :goto_0
    if-ge v5, v6, :cond_3

    .line 2392562
    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v1, v5}, LX/CsS;->c(I)Lcom/facebook/richdocument/view/carousel/PageableFragment;

    move-result-object v1

    check-cast v1, Lcom/facebook/instantarticles/InstantArticlesFragment;

    .line 2392563
    iget-boolean p0, v1, Lcom/facebook/instantarticles/InstantArticlesFragment;->y:Z

    move p0, p0

    .line 2392564
    if-eqz p0, :cond_0

    .line 2392565
    iget-object p0, v1, Landroid/support/v4/app/Fragment;->mArguments:Landroid/os/Bundle;

    move-object p0, p0

    .line 2392566
    const-string p1, "open_action"

    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .line 2392567
    if-eqz p0, :cond_1

    const-string p1, "clicked"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    .line 2392568
    add-int/lit8 v2, v2, 0x1

    .line 2392569
    :goto_1
    invoke-virtual {v1}, Lcom/facebook/instantarticles/InstantArticlesFragment;->s()V

    .line 2392570
    :cond_0
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_0

    .line 2392571
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    move v2, v4

    move v3, v4

    .line 2392572
    :cond_3
    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->t:LX/0Zb;

    const-string v5, "carousel_open_action_count"

    invoke-interface {v1, v5, v4}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v1

    .line 2392573
    invoke-virtual {v1}, LX/0oG;->a()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2392574
    const-string v4, "instant_articles_session_id"

    iget-object v5, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->q:LX/ClD;

    .line 2392575
    iget-object v6, v5, LX/ClD;->h:Ljava/lang/String;

    move-object v5, v6

    .line 2392576
    invoke-virtual {v1, v4, v5}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2392577
    const-string v4, "swipe_count"

    invoke-virtual {v1, v4, v3}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2392578
    const-string v3, "click_count"

    invoke-virtual {v1, v3, v2}, LX/0oG;->a(Ljava/lang/String;I)LX/0oG;

    .line 2392579
    const-string v2, "native_article_story"

    invoke-virtual {v1, v2}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    .line 2392580
    invoke-virtual {v1}, LX/0oG;->d()V

    .line 2392581
    :cond_4
    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->v:LX/8bM;

    invoke-virtual {v1}, LX/8bM;->b()V

    .line 2392582
    invoke-virtual {v0}, Lcom/facebook/ui/dialogs/FbDialogFragment;->h()Landroid/app/Activity;

    move-result-object v1

    .line 2392583
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/app/Activity;->getRequestedOrientation()I

    move-result v2

    iget v3, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->Q:I

    if-eq v2, v3, :cond_5

    .line 2392584
    iget v2, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->Q:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 2392585
    :cond_5
    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->n:LX/Chv;

    iget-object v2, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->C:LX/ChV;

    invoke-virtual {v1, v2}, LX/0b4;->b(LX/0b2;)Z

    .line 2392586
    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->G:LX/Gmd;

    iget-object v2, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->n:LX/Chv;

    invoke-virtual {v1, v2}, LX/Gmd;->b(LX/0b4;)V

    .line 2392587
    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    invoke-virtual {v1}, LX/CsS;->getViewPager()Landroid/support/v4/view/ViewPager;

    move-result-object v1

    check-cast v1, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;

    iget-object v2, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->G:LX/Gmd;

    invoke-virtual {v1, v2}, Lcom/facebook/instantarticles/view/InstantArticlesCarouselViewPager;->b(LX/IXp;)V

    .line 2392588
    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    iget-object v2, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->H:Lcom/facebook/instantarticles/CarouselArticlePrefetcher;

    invoke-virtual {v1, v2}, LX/CsS;->b(LX/0hc;)V

    .line 2392589
    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    iget-object v2, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->H:Lcom/facebook/instantarticles/CarouselArticlePrefetcher;

    invoke-virtual {v1, v2}, LX/CsS;->b(LX/CqC;)V

    .line 2392590
    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    iget-object v2, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->D:LX/CqC;

    invoke-virtual {v1, v2}, LX/CsS;->b(LX/CqC;)V

    .line 2392591
    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->J:LX/IXr;

    instance-of v1, v1, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    if-eqz v1, :cond_6

    .line 2392592
    iget-object v2, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->o:LX/Chr;

    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->J:LX/IXr;

    check-cast v1, Lcom/facebook/instantarticles/view/InstantArticlesCollapsingHeader;

    .line 2392593
    if-eqz v1, :cond_6

    .line 2392594
    iget-object v3, v2, LX/Chr;->a:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2392595
    :cond_6
    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->P:LX/0hc;

    if-eqz v1, :cond_7

    .line 2392596
    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->F:Lcom/facebook/instantarticles/view/InstantArticlesPagerWithSharedHeaderAndPageIndicator;

    iget-object v2, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->P:LX/0hc;

    invoke-virtual {v1, v2}, LX/CsS;->b(LX/0hc;)V

    .line 2392597
    :cond_7
    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->r:LX/IXE;

    invoke-virtual {v1}, LX/IXE;->c()V

    .line 2392598
    iget-object v1, v0, Lcom/facebook/instantarticles/InstantArticlesCarouselDialogFragment;->s:LX/CqH;

    invoke-virtual {v1}, LX/CqH;->c()V

    .line 2392599
    :try_start_0
    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->d()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 2392600
    :goto_2
    return-void

    :catch_0
    goto :goto_2
.end method
