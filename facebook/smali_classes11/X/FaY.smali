.class public LX/FaY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/FaY;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/13B;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Landroid/content/Context;

.field public final d:Landroid/view/WindowManager;

.field public e:Lcom/facebook/fig/button/FigButton;

.field public f:Lcom/facebook/fig/button/FigButton;

.field public g:LX/FaF;

.field public h:LX/3Af;

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2258908
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2258909
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2258910
    iput-object v0, p0, LX/FaY;->a:LX/0Ot;

    .line 2258911
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2258912
    iput-object v0, p0, LX/FaY;->b:LX/0Ot;

    .line 2258913
    iput-object p1, p0, LX/FaY;->c:Landroid/content/Context;

    .line 2258914
    iget-object v0, p0, LX/FaY;->c:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, LX/FaY;->d:Landroid/view/WindowManager;

    .line 2258915
    sget-object v0, LX/FaF;->UNSET:LX/FaF;

    iput-object v0, p0, LX/FaY;->g:LX/FaF;

    .line 2258916
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/FaY;->i:Z

    .line 2258917
    return-void
.end method

.method public static a(LX/0QB;)LX/FaY;
    .locals 5

    .prologue
    .line 2258888
    sget-object v0, LX/FaY;->j:LX/FaY;

    if-nez v0, :cond_1

    .line 2258889
    const-class v1, LX/FaY;

    monitor-enter v1

    .line 2258890
    :try_start_0
    sget-object v0, LX/FaY;->j:LX/FaY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2258891
    if-eqz v2, :cond_0

    .line 2258892
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2258893
    new-instance v4, LX/FaY;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {v4, v3}, LX/FaY;-><init>(Landroid/content/Context;)V

    .line 2258894
    const/16 v3, 0x1141

    invoke-static {v0, v3}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 p0, 0xf9a

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2258895
    iput-object v3, v4, LX/FaY;->a:LX/0Ot;

    iput-object p0, v4, LX/FaY;->b:LX/0Ot;

    .line 2258896
    move-object v0, v4

    .line 2258897
    sput-object v0, LX/FaY;->j:LX/FaY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2258898
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2258899
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2258900
    :cond_1
    sget-object v0, LX/FaY;->j:LX/FaY;

    return-object v0

    .line 2258901
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2258902
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/FaY;LX/FaF;LX/FaW;)V
    .locals 4

    .prologue
    .line 2258903
    iget-boolean v0, p0, LX/FaY;->i:Z

    if-eqz v0, :cond_0

    .line 2258904
    iget-object v0, p0, LX/FaY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13B;

    invoke-virtual {p1}, LX/FaF;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, LX/FaW;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2258905
    const-string v3, "OPT_OUT_DIALOG"

    const-string p1, "feature"

    const-string p2, "action"

    invoke-static {p1, v1, p2, v2}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object p1

    invoke-static {v0, v3, p1}, LX/13B;->b(LX/13B;Ljava/lang/String;LX/0P1;)V

    .line 2258906
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/FaY;->i:Z

    .line 2258907
    return-void
.end method

.method private c(LX/FaF;)V
    .locals 2

    .prologue
    .line 2258884
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/FaY;->i:Z

    .line 2258885
    iget-object v0, p0, LX/FaY;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13B;

    invoke-virtual {p1}, LX/FaF;->name()Ljava/lang/String;

    move-result-object v1

    .line 2258886
    const-string p0, "OPT_OUT_DIALOG"

    const-string p1, "feature"

    invoke-static {p1, v1}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object p1

    invoke-static {v0, p0, p1}, LX/13B;->a(LX/13B;Ljava/lang/String;LX/0P1;)V

    .line 2258887
    return-void
.end method


# virtual methods
.method public final a(LX/FaF;LX/FaX;Landroid/os/IBinder;)V
    .locals 5
    .param p3    # Landroid/os/IBinder;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2258853
    sget-object v0, LX/FaF;->UNSET:LX/FaF;

    if-eq p1, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2258854
    new-instance v0, LX/3Af;

    iget-object v1, p0, LX/FaY;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, LX/3Af;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, LX/FaY;->h:LX/3Af;

    .line 2258855
    iget-object v0, p0, LX/FaY;->h:LX/3Af;

    const/4 v4, 0x0

    .line 2258856
    iget-object v1, p0, LX/FaY;->c:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2258857
    const v2, 0x7f03014b

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 2258858
    iget-object v1, p0, LX/FaY;->d:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 2258859
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    .line 2258860
    invoke-virtual {v1, v3}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 2258861
    iget v1, v3, Landroid/graphics/Point;->x:I

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 2258862
    invoke-static {v4, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 2258863
    invoke-virtual {v2, v1, v3}, Landroid/view/View;->measure(II)V

    .line 2258864
    const v1, 0x7f0d061c

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/button/FigButton;

    iput-object v1, p0, LX/FaY;->e:Lcom/facebook/fig/button/FigButton;

    .line 2258865
    iget-object v1, p0, LX/FaY;->e:Lcom/facebook/fig/button/FigButton;

    new-instance v3, LX/FaU;

    invoke-direct {v3, p0, p2}, LX/FaU;-><init>(LX/FaY;LX/FaX;)V

    invoke-virtual {v1, v3}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2258866
    const v1, 0x7f0d061b

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/button/FigButton;

    iput-object v1, p0, LX/FaY;->f:Lcom/facebook/fig/button/FigButton;

    .line 2258867
    iget-object v1, p0, LX/FaY;->f:Lcom/facebook/fig/button/FigButton;

    new-instance v3, LX/FaV;

    invoke-direct {v3, p0, p2}, LX/FaV;-><init>(LX/FaY;LX/FaX;)V

    invoke-virtual {v1, v3}, Lcom/facebook/fig/button/FigButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2258868
    new-instance v1, LX/34b;

    iget-object v3, p0, LX/FaY;->c:Landroid/content/Context;

    invoke-direct {v1, v3}, LX/34b;-><init>(Landroid/content/Context;)V

    .line 2258869
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, LX/34b;->a(Landroid/view/View;F)V

    .line 2258870
    move-object v1, v1

    .line 2258871
    invoke-virtual {v0, v1}, LX/3Af;->a(LX/1OM;)V

    .line 2258872
    iget-object v0, p0, LX/FaY;->h:LX/3Af;

    new-instance v1, LX/FaT;

    invoke-direct {v1, p0}, LX/FaT;-><init>(LX/FaY;)V

    invoke-virtual {v0, v1}, LX/3Af;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 2258873
    if-eqz p3, :cond_0

    .line 2258874
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 2258875
    iput-object p3, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 2258876
    const/16 v1, 0x3e8

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 2258877
    const/4 v1, -0x3

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 2258878
    iget-object v1, p0, LX/FaY;->h:LX/3Af;

    invoke-virtual {v1}, LX/3Af;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 2258879
    :cond_0
    iput-object p1, p0, LX/FaY;->g:LX/FaF;

    .line 2258880
    iget-object v0, p0, LX/FaY;->g:LX/FaF;

    invoke-direct {p0, v0}, LX/FaY;->c(LX/FaF;)V

    .line 2258881
    iget-object v0, p0, LX/FaY;->h:LX/3Af;

    invoke-virtual {v0}, LX/3Af;->show()V

    .line 2258882
    return-void

    .line 2258883
    :cond_1
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final a(LX/FaF;)Z
    .locals 3

    .prologue
    .line 2258852
    iget-object v0, p0, LX/FaY;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-virtual {p1}, LX/FaF;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/7CP;->c(Ljava/lang/String;)LX/0Tn;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    return v0
.end method
