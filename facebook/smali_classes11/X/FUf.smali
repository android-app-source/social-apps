.class public LX/FUf;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0if;


# direct methods
.method public constructor <init>(LX/0if;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2249510
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2249511
    iput-object p1, p0, LX/FUf;->a:LX/0if;

    .line 2249512
    return-void
.end method

.method public static a(LX/0QB;)LX/FUf;
    .locals 1

    .prologue
    .line 2249513
    invoke-static {p0}, LX/FUf;->b(LX/0QB;)LX/FUf;

    move-result-object v0

    return-object v0
.end method

.method public static a([Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2249514
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2249515
    if-nez p0, :cond_0

    .line 2249516
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2249517
    :goto_0
    return-object v0

    .line 2249518
    :cond_0
    const/4 v0, 0x0

    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_2

    .line 2249519
    if-lez v0, :cond_1

    .line 2249520
    const/16 v2, 0x2c

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2249521
    :cond_1
    aget-object v2, p0, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2249522
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2249523
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/FUf;
    .locals 2

    .prologue
    .line 2249524
    new-instance v1, LX/FUf;

    invoke-static {p0}, LX/0if;->a(LX/0QB;)LX/0if;

    move-result-object v0

    check-cast v0, LX/0if;

    invoke-direct {v1, v0}, LX/FUf;-><init>(LX/0if;)V

    .line 2249525
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2249526
    iget-object v0, p0, LX/FUf;->a:LX/0if;

    sget-object v1, LX/0ig;->H:LX/0ih;

    invoke-virtual {v0, v1}, LX/0if;->a(LX/0ih;)V

    .line 2249527
    iget-object v0, p0, LX/FUf;->a:LX/0if;

    sget-object v1, LX/0ig;->H:LX/0ih;

    invoke-virtual {v0, v1, p1}, LX/0if;->a(LX/0ih;Ljava/lang/String;)V

    .line 2249528
    iget-object v0, p0, LX/FUf;->a:LX/0if;

    sget-object v1, LX/0ig;->H:LX/0ih;

    const-string v2, "QR_STARTED_LIFECYCLE_ENTRY_POINT"

    invoke-virtual {v0, v1, v2, p2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2249529
    return-void
.end method

.method public final c(Ljava/lang/Exception;)V
    .locals 5

    .prologue
    .line 2249530
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v0

    const-string v1, "msg"

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v0

    .line 2249531
    iget-object v1, p0, LX/FUf;->a:LX/0if;

    sget-object v2, LX/0ig;->H:LX/0ih;

    const-string v3, "EXCEPTION_ON_IMPORT_FILE"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4, v0}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2249532
    return-void
.end method
