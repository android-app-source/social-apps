.class public final LX/F8F;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 2203834
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2203835
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2203836
    :goto_0
    return v1

    .line 2203837
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2203838
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 2203839
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2203840
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2203841
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2203842
    const-string v3, "edges"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2203843
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2203844
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 2203845
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2203846
    const/4 v3, 0x0

    .line 2203847
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_8

    .line 2203848
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2203849
    :goto_3
    move v2, v3

    .line 2203850
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2203851
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 2203852
    goto :goto_1

    .line 2203853
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2203854
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2203855
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 2203856
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2203857
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 2203858
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2203859
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2203860
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 2203861
    const-string v5, "node"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2203862
    const/4 v4, 0x0

    .line 2203863
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v5, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v5, :cond_c

    .line 2203864
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2203865
    :goto_5
    move v2, v4

    .line 2203866
    goto :goto_4

    .line 2203867
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2203868
    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 2203869
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_8
    move v2, v3

    goto :goto_4

    .line 2203870
    :cond_9
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2203871
    :cond_a
    :goto_6
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->END_OBJECT:LX/15z;

    if-eq v5, v6, :cond_b

    .line 2203872
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v5

    .line 2203873
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2203874
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v6

    sget-object v7, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v6, v7, :cond_a

    if-eqz v5, :cond_a

    .line 2203875
    const-string v6, "invitable_contacts"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 2203876
    const/4 v5, 0x0

    .line 2203877
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v6, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v6, :cond_12

    .line 2203878
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2203879
    :goto_7
    move v2, v5

    .line 2203880
    goto :goto_6

    .line 2203881
    :cond_b
    const/4 v5, 0x1

    invoke-virtual {p1, v5}, LX/186;->c(I)V

    .line 2203882
    invoke-virtual {p1, v4, v2}, LX/186;->b(II)V

    .line 2203883
    invoke-virtual {p1}, LX/186;->d()I

    move-result v4

    goto :goto_5

    :cond_c
    move v2, v4

    goto :goto_6

    .line 2203884
    :cond_d
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2203885
    :cond_e
    :goto_8
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_11

    .line 2203886
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2203887
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2203888
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_e

    if-eqz v7, :cond_e

    .line 2203889
    const-string v8, "nodes"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_10

    .line 2203890
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2203891
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->START_ARRAY:LX/15z;

    if-ne v7, v8, :cond_f

    .line 2203892
    :goto_9
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_ARRAY:LX/15z;

    if-eq v7, v8, :cond_f

    .line 2203893
    invoke-static {p0, p1}, LX/F8E;->b(LX/15w;LX/186;)I

    move-result v7

    .line 2203894
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    .line 2203895
    :cond_f
    invoke-static {v6, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v6

    move v6, v6

    .line 2203896
    goto :goto_8

    .line 2203897
    :cond_10
    const-string v8, "page_info"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 2203898
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 2203899
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v9, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v9, :cond_18

    .line 2203900
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2203901
    :goto_a
    move v2, v7

    .line 2203902
    goto :goto_8

    .line 2203903
    :cond_11
    const/4 v7, 0x2

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2203904
    invoke-virtual {p1, v5, v6}, LX/186;->b(II)V

    .line 2203905
    const/4 v5, 0x1

    invoke-virtual {p1, v5, v2}, LX/186;->b(II)V

    .line 2203906
    invoke-virtual {p1}, LX/186;->d()I

    move-result v5

    goto/16 :goto_7

    :cond_12
    move v2, v5

    move v6, v5

    goto :goto_8

    .line 2203907
    :cond_13
    const-string v12, "has_next_page"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_15

    .line 2203908
    invoke-virtual {p0}, LX/15w;->H()Z

    move-result v2

    move v9, v2

    move v2, v8

    .line 2203909
    :cond_14
    :goto_b
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_16

    .line 2203910
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2203911
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2203912
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_14

    if-eqz v11, :cond_14

    .line 2203913
    const-string v12, "end_cursor"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_13

    .line 2203914
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    goto :goto_b

    .line 2203915
    :cond_15
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_b

    .line 2203916
    :cond_16
    const/4 v11, 0x2

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2203917
    invoke-virtual {p1, v7, v10}, LX/186;->b(II)V

    .line 2203918
    if-eqz v2, :cond_17

    .line 2203919
    invoke-virtual {p1, v8, v9}, LX/186;->a(IZ)V

    .line 2203920
    :cond_17
    invoke-virtual {p1}, LX/186;->d()I

    move-result v7

    goto :goto_a

    :cond_18
    move v2, v7

    move v9, v7

    move v10, v7

    goto :goto_b
.end method
