.class public final LX/GgC;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/GgD;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/GfY;

.field public b:LX/1Pp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/GgD;


# direct methods
.method public constructor <init>(LX/GgD;)V
    .locals 1

    .prologue
    .line 2378691
    iput-object p1, p0, LX/GgC;->c:LX/GgD;

    .line 2378692
    move-object v0, p1

    .line 2378693
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2378694
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2378695
    const-string v0, "PaginatedPymlShareComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2378696
    if-ne p0, p1, :cond_1

    .line 2378697
    :cond_0
    :goto_0
    return v0

    .line 2378698
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2378699
    goto :goto_0

    .line 2378700
    :cond_3
    check-cast p1, LX/GgC;

    .line 2378701
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2378702
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2378703
    if-eq v2, v3, :cond_0

    .line 2378704
    iget-object v2, p0, LX/GgC;->a:LX/GfY;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/GgC;->a:LX/GfY;

    iget-object v3, p1, LX/GgC;->a:LX/GfY;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2378705
    goto :goto_0

    .line 2378706
    :cond_5
    iget-object v2, p1, LX/GgC;->a:LX/GfY;

    if-nez v2, :cond_4

    .line 2378707
    :cond_6
    iget-object v2, p0, LX/GgC;->b:LX/1Pp;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/GgC;->b:LX/1Pp;

    iget-object v3, p1, LX/GgC;->b:LX/1Pp;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2378708
    goto :goto_0

    .line 2378709
    :cond_7
    iget-object v2, p1, LX/GgC;->b:LX/1Pp;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
