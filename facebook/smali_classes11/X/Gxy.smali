.class public LX/Gxy;
.super LX/4ot;
.source ""


# instance fields
.field private final b:LX/Gxx;

.field public final c:LX/27g;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Gxx;LX/27g;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2408736
    invoke-direct {p0, p1}, LX/4ot;-><init>(Landroid/content/Context;)V

    .line 2408737
    iput-object p2, p0, LX/Gxy;->b:LX/Gxx;

    .line 2408738
    iput-object p3, p0, LX/Gxy;->c:LX/27g;

    .line 2408739
    sget-object v0, LX/0eC;->b:LX/0Tn;

    invoke-virtual {v0}, LX/0To;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Gxy;->setKey(Ljava/lang/String;)V

    .line 2408740
    iget-object v0, p0, LX/Gxy;->c:LX/27g;

    invoke-virtual {v0}, LX/27g;->b()LX/27h;

    move-result-object v0

    .line 2408741
    invoke-virtual {v0}, LX/27h;->b()[Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/Gxy;->setEntries([Ljava/lang/CharSequence;)V

    .line 2408742
    invoke-virtual {v0}, LX/27h;->a()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Gxy;->setEntryValues([Ljava/lang/CharSequence;)V

    .line 2408743
    const-string v0, "device"

    move-object v0, v0

    .line 2408744
    invoke-virtual {p0, v0}, LX/Gxy;->setDefaultValue(Ljava/lang/Object;)V

    .line 2408745
    const v0, 0x7f0835c4

    invoke-virtual {p0, v0}, LX/Gxy;->setTitle(I)V

    .line 2408746
    return-void
.end method


# virtual methods
.method public final callChangeListener(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 2408722
    const-string v0, "device"

    invoke-virtual {p0, v0}, LX/4or;->getPersistedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v0, p1

    .line 2408723
    check-cast v0, Ljava/lang/String;

    .line 2408724
    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2408725
    iget-object v2, p0, LX/Gxy;->b:LX/Gxx;

    invoke-virtual {v2, v1, v0}, LX/Gxx;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2408726
    :cond_0
    invoke-super {p0, p1}, LX/4ot;->callChangeListener(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final showDialog(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 2408727
    invoke-virtual {p0}, LX/Gxy;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2408728
    iget-object v1, p0, LX/Gxy;->b:LX/Gxx;

    .line 2408729
    new-instance v2, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v3, LX/Gxw;->VISIT:LX/Gxw;

    invoke-virtual {v3}, LX/Gxw;->getAnalyticsName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v3, "language_switcher"

    .line 2408730
    iput-object v3, v2, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2408731
    move-object v2, v2

    .line 2408732
    const-string v3, "current_app_locale"

    invoke-virtual {v2, v3, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    const-string v3, "system_locale"

    invoke-static {}, LX/0W9;->e()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2408733
    iget-object v3, v1, LX/Gxx;->a:LX/0Zb;

    invoke-interface {v3, v2}, LX/0Zb;->c(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2408734
    invoke-super {p0, p1}, LX/4ot;->showDialog(Landroid/os/Bundle;)V

    .line 2408735
    return-void
.end method
