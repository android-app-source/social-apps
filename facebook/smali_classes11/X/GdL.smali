.class public LX/GdL;
.super LX/98h;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/98h",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Object;

.field private static volatile c:LX/GdL;


# instance fields
.field private final b:LX/87v;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2373728
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/GdL;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/87v;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2373712
    invoke-direct {p0}, LX/98h;-><init>()V

    .line 2373713
    iput-object p1, p0, LX/GdL;->b:LX/87v;

    .line 2373714
    return-void
.end method

.method public static a(LX/0QB;)LX/GdL;
    .locals 4

    .prologue
    .line 2373715
    sget-object v0, LX/GdL;->c:LX/GdL;

    if-nez v0, :cond_1

    .line 2373716
    const-class v1, LX/GdL;

    monitor-enter v1

    .line 2373717
    :try_start_0
    sget-object v0, LX/GdL;->c:LX/GdL;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2373718
    if-eqz v2, :cond_0

    .line 2373719
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2373720
    new-instance p0, LX/GdL;

    invoke-static {v0}, LX/87v;->b(LX/0QB;)LX/87v;

    move-result-object v3

    check-cast v3, LX/87v;

    invoke-direct {p0, v3}, LX/GdL;-><init>(LX/87v;)V

    .line 2373721
    move-object v0, p0

    .line 2373722
    sput-object v0, LX/GdL;->c:LX/GdL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2373723
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2373724
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2373725
    :cond_1
    sget-object v0, LX/GdL;->c:LX/GdL;

    return-object v0

    .line 2373726
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2373727
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/content/Intent;)LX/98g;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/Intent;",
            ")",
            "LX/98g",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2373710
    iget-object v0, p0, LX/GdL;->b:LX/87v;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, LX/87v;->a(ZLX/87u;)V

    .line 2373711
    new-instance v0, LX/98g;

    sget-object v1, LX/GdL;->a:Ljava/lang/Object;

    invoke-direct {v0, v1, v2}, LX/98g;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2373709
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2373708
    const/4 v0, 0x1

    return v0
.end method
