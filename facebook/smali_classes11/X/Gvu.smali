.class public final LX/Gvu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Ljava/lang/String;",
        ">;",
        "LX/0Px",
        "<",
        "Lcom/facebook/ipc/composer/model/ComposerTaggedUser;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Gvy;


# direct methods
.method public constructor <init>(LX/Gvy;)V
    .locals 0

    .prologue
    .line 2406205
    iput-object p1, p0, LX/Gvu;->a:LX/Gvy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .prologue
    .line 2406206
    check-cast p1, Ljava/util/ArrayList;

    .line 2406207
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2406208
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2406209
    invoke-static {v0}, LX/Gvy;->e(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 2406210
    if-eqz v0, :cond_0

    .line 2406211
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/facebook/ipc/composer/model/ComposerTaggedUser;->a(J)LX/5Rc;

    move-result-object v0

    invoke-virtual {v0}, LX/5Rc;->a()Lcom/facebook/ipc/composer/model/ComposerTaggedUser;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2406212
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2406213
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2406214
    const/4 v0, 0x0

    return v0
.end method
