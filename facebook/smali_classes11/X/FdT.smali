.class public LX/FdT;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "LX/5uu;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Landroid/content/Context;

.field public final b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/5uu;",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2263644
    const v0, 0x7f03129c

    invoke-direct {p0, p1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    .line 2263645
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/FdT;->b:Ljava/util/HashMap;

    .line 2263646
    iput-object p1, p0, LX/FdT;->a:Landroid/content/Context;

    .line 2263647
    return-void
.end method


# virtual methods
.method public final a()LX/5uu;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2263609
    iget-object v1, p0, LX/FdT;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_1

    .line 2263610
    :cond_0
    :goto_0
    return-object v0

    .line 2263611
    :cond_1
    iget-object v1, p0, LX/FdT;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2263612
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/FdT;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5uu;

    goto :goto_0

    .line 2263613
    :cond_2
    iget-object v1, p0, LX/FdT;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5uu;

    goto :goto_1
.end method

.method public final a(LX/0Px;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "LX/5uu;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2263632
    invoke-super {p0, p1}, Landroid/widget/ArrayAdapter;->addAll(Ljava/util/Collection;)V

    .line 2263633
    const/4 v3, 0x0

    .line 2263634
    invoke-virtual {p1}, LX/0Px;->size()I

    move-result v5

    move v4, v3

    :goto_0
    if-ge v4, v5, :cond_2

    invoke-virtual {p1, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5uu;

    .line 2263635
    invoke-interface {v0}, LX/5uu;->e()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2263636
    invoke-interface {v0}, LX/5uu;->e()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    move v2, v3

    :goto_1
    if-ge v2, v7, :cond_1

    invoke-virtual {v6, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    .line 2263637
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->b()Ljava/lang/String;

    move-result-object v8

    if-eqz v8, :cond_0

    .line 2263638
    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->a()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->b()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v0}, LX/5uu;->d()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 2263639
    iget-object v8, p0, LX/FdT;->b:Ljava/util/HashMap;

    new-instance v9, LX/CyH;

    invoke-interface {v0}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v11

    invoke-virtual {v11}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->b()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->c()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v9, v10, v11, v1}, LX/CyH;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v9}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2263640
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2263641
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 2263642
    :cond_2
    const v0, -0x33cbfc72    # -4.718956E7f

    invoke-static {p0, v0}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2263643
    return-void
.end method

.method public final b()LX/0Px;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/CyH;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2263626
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2263627
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, LX/FdT;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2263628
    iget-object v0, p0, LX/FdT;->b:Ljava/util/HashMap;

    invoke-virtual {p0, v1}, LX/FdT;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2263629
    iget-object v0, p0, LX/FdT;->b:Ljava/util/HashMap;

    invoke-virtual {p0, v1}, LX/FdT;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-virtual {v2, v0}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2263630
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2263631
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 2263614
    if-nez p2, :cond_0

    .line 2263615
    iget-object v0, p0, LX/FdT;->a:Landroid/content/Context;

    const v1, 0x7f03129c

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 2263616
    :cond_0
    invoke-virtual {p0, p1}, LX/FdT;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/5uu;

    .line 2263617
    const v1, 0x7f0d2b89

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/glyph/GlyphView;

    .line 2263618
    const v2, 0x7f0d2b8a

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/facebook/widget/text/BetterTextView;

    .line 2263619
    iget-object v3, p0, LX/FdT;->b:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_1

    .line 2263620
    const v3, -0xb4b0aa

    invoke-virtual {v1, v3}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    .line 2263621
    invoke-interface {v0}, LX/5uu;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2263622
    :goto_0
    sget-object v2, LX/Fdj;->a:LX/0P1;

    invoke-interface {v0}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setImageResource(I)V

    .line 2263623
    return-object p2

    .line 2263624
    :cond_1
    iget-object v4, p0, LX/FdT;->a:Landroid/content/Context;

    iget-object v3, p0, LX/FdT;->b:Ljava/util/HashMap;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0Px;

    invoke-static {v4, v3}, LX/Fdj;->a(Landroid/content/Context;LX/0Px;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2263625
    const v2, -0xbd984e

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/glyph/GlyphView;->setGlyphColor(I)V

    goto :goto_0
.end method
