.class public final LX/Fdn;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/resources/ui/FbCheckBox;

.field public final synthetic b:Lcom/facebook/search/results/protocol/filters/FilterValue;

.field public final synthetic c:LX/Fdo;


# direct methods
.method public constructor <init>(LX/Fdo;Lcom/facebook/resources/ui/FbCheckBox;Lcom/facebook/search/results/protocol/filters/FilterValue;)V
    .locals 0

    .prologue
    .line 2264195
    iput-object p1, p0, LX/Fdn;->c:LX/Fdo;

    iput-object p2, p0, LX/Fdn;->a:Lcom/facebook/resources/ui/FbCheckBox;

    iput-object p3, p0, LX/Fdn;->b:Lcom/facebook/search/results/protocol/filters/FilterValue;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x7801d752

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2264196
    iget-object v1, p0, LX/Fdn;->a:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v1}, Lcom/facebook/resources/ui/FbCheckBox;->toggle()V

    .line 2264197
    iget-object v1, p0, LX/Fdn;->c:LX/Fdo;

    iget-object v1, v1, LX/Fdo;->h:Ljava/util/HashSet;

    iget-object v2, p0, LX/Fdn;->b:Lcom/facebook/search/results/protocol/filters/FilterValue;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2264198
    iget-object v1, p0, LX/Fdn;->c:LX/Fdo;

    iget-object v1, v1, LX/Fdo;->h:Ljava/util/HashSet;

    iget-object v2, p0, LX/Fdn;->b:Lcom/facebook/search/results/protocol/filters/FilterValue;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 2264199
    :goto_0
    const v1, 0x1cab1b4c

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2264200
    :cond_0
    iget-object v1, p0, LX/Fdn;->c:LX/Fdo;

    iget-object v1, v1, LX/Fdo;->h:Ljava/util/HashSet;

    iget-object v2, p0, LX/Fdn;->b:Lcom/facebook/search/results/protocol/filters/FilterValue;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
