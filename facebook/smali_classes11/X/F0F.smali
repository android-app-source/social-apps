.class public final LX/F0F;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public c:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;",
            ">;"
        }
    .end annotation
.end field

.field public d:Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

.field public e:LX/0us;

.field public f:LX/0ta;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2188311
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;)LX/F0F;
    .locals 2

    .prologue
    .line 2188312
    new-instance v0, LX/F0F;

    invoke-direct {v0}, LX/F0F;-><init>()V

    iget-object v1, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->a:LX/0Px;

    .line 2188313
    iput-object v1, v0, LX/F0F;->a:LX/0Px;

    .line 2188314
    move-object v0, v0

    .line 2188315
    iget-object v1, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->b:LX/0Px;

    .line 2188316
    iput-object v1, v0, LX/F0F;->b:LX/0Px;

    .line 2188317
    move-object v0, v0

    .line 2188318
    iget-object v1, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->c:LX/0P1;

    .line 2188319
    iput-object v1, v0, LX/F0F;->c:LX/0P1;

    .line 2188320
    move-object v0, v0

    .line 2188321
    iget-object v1, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->d:Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

    .line 2188322
    iput-object v1, v0, LX/F0F;->d:Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

    .line 2188323
    move-object v0, v0

    .line 2188324
    iget-object v1, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->e:LX/0us;

    .line 2188325
    iput-object v1, v0, LX/F0F;->e:LX/0us;

    .line 2188326
    move-object v0, v0

    .line 2188327
    iget-object v1, p0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;->f:LX/0ta;

    .line 2188328
    iput-object v1, v0, LX/F0F;->f:LX/0ta;

    .line 2188329
    move-object v0, v0

    .line 2188330
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;)LX/F0F;
    .locals 0

    .prologue
    .line 2188331
    iput-object p1, p0, LX/F0F;->d:Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

    .line 2188332
    return-object p0
.end method

.method public final a()Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;
    .locals 8

    .prologue
    .line 2188333
    new-instance v0, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;

    iget-object v1, p0, LX/F0F;->a:LX/0Px;

    iget-object v2, p0, LX/F0F;->b:LX/0Px;

    iget-object v3, p0, LX/F0F;->c:LX/0P1;

    iget-object v4, p0, LX/F0F;->d:Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

    iget-object v5, p0, LX/F0F;->e:LX/0us;

    iget-object v6, p0, LX/F0F;->f:LX/0ta;

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;-><init>(LX/0Px;LX/0Px;LX/0P1;Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;LX/0us;LX/0ta;B)V

    return-object v0
.end method
