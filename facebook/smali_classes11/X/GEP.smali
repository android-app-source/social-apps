.class public final LX/GEP;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GDc;

.field public final synthetic b:LX/GER;


# direct methods
.method public constructor <init>(LX/GER;LX/GDc;)V
    .locals 0

    .prologue
    .line 2331662
    iput-object p1, p0, LX/GEP;->b:LX/GER;

    iput-object p2, p0, LX/GEP;->a:LX/GDc;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2331663
    iget-object v0, p0, LX/GEP;->b:LX/GER;

    iget-object v0, v0, LX/GER;->a:LX/2U3;

    const-class v1, LX/GER;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Call to ShowCheckoutExperience failed"

    invoke-virtual {v0, v1, v2}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2331664
    iget-object v0, p0, LX/GEP;->a:LX/GDc;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, LX/GDc;->a(Ljava/lang/Boolean;)V

    .line 2331665
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2331666
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2331667
    if-eqz p1, :cond_0

    .line 2331668
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331669
    if-nez v0, :cond_1

    .line 2331670
    :cond_0
    iget-object v0, p0, LX/GEP;->b:LX/GER;

    iget-object v0, v0, LX/GER;->a:LX/2U3;

    const-class v1, LX/GER;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "Call to ShowCheckoutExperience successful but null returned"

    invoke-virtual {v0, v1, v2}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2331671
    iget-object v0, p0, LX/GEP;->a:LX/GDc;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, LX/GDc;->a(Ljava/lang/Boolean;)V

    .line 2331672
    :goto_0
    return-void

    .line 2331673
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2331674
    check-cast v0, Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel;

    .line 2331675
    iget-object v1, p0, LX/GEP;->a:LX/GDc;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/ShowCheckoutExperienceQueryModels$ShowCheckoutExperienceQueryModel;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-interface {v1, v0}, LX/GDc;->a(Ljava/lang/Boolean;)V

    goto :goto_0
.end method
