.class public abstract LX/F6e;
.super LX/3Tf;
.source ""


# instance fields
.field public c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/F6b;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Landroid/content/Context;

.field public final f:LX/89v;

.field public final g:LX/F6i;

.field public final h:LX/F6k;

.field public final i:LX/9Tk;

.field public final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
            ">;"
        }
    .end annotation
.end field

.field public final k:J

.field public final l:Landroid/view/LayoutInflater;

.field public final m:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public final n:Ljava/util/Hashtable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Hashtable",
            "<",
            "Ljava/lang/Long;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<*>;>;"
        }
    .end annotation
.end field

.field public o:Z

.field public p:LX/3nE;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/3nE",
            "<",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
            ">;",
            "Ljava/lang/Void;",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
            ">;>;"
        }
    .end annotation
.end field

.field public q:LX/0Tf;

.field public r:LX/0W9;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/89v;LX/F6i;LX/F6k;LX/9Tk;Ljava/util/Map;JLX/0Tf;LX/0W9;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/89v;",
            "LX/F6i;",
            "LX/F6k;",
            "LX/9Tk;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lcom/facebook/ipc/model/FacebookPhonebookContact;",
            ">;J",
            "LX/0Tf;",
            "LX/0W9;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2200636
    invoke-direct {p0}, LX/3Tf;-><init>()V

    .line 2200637
    iput-object p1, p0, LX/F6e;->e:Landroid/content/Context;

    .line 2200638
    iput-object p2, p0, LX/F6e;->f:LX/89v;

    .line 2200639
    iput-object p3, p0, LX/F6e;->g:LX/F6i;

    .line 2200640
    iput-object p4, p0, LX/F6e;->h:LX/F6k;

    .line 2200641
    iput-object p5, p0, LX/F6e;->i:LX/9Tk;

    .line 2200642
    iput-object p6, p0, LX/F6e;->j:Ljava/util/Map;

    .line 2200643
    iput-wide p7, p0, LX/F6e;->k:J

    .line 2200644
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/F6e;->l:Landroid/view/LayoutInflater;

    .line 2200645
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/F6e;->d:Ljava/util/List;

    .line 2200646
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/F6e;->m:Ljava/util/Set;

    .line 2200647
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, LX/F6e;->n:Ljava/util/Hashtable;

    .line 2200648
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p6}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, LX/F6e;->c:Ljava/util/List;

    .line 2200649
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/F6e;->o:Z

    .line 2200650
    iput-object p9, p0, LX/F6e;->q:LX/0Tf;

    .line 2200651
    iput-object p10, p0, LX/F6e;->r:LX/0W9;

    .line 2200652
    const/4 p2, 0x0

    .line 2200653
    new-instance v0, LX/F6d;

    invoke-direct {v0, p0}, LX/F6d;-><init>(LX/F6e;)V

    iput-object v0, p0, LX/F6e;->p:LX/3nE;

    .line 2200654
    iget-object v0, p0, LX/F6e;->p:LX/3nE;

    iget-object v1, p0, LX/F6e;->e:Landroid/content/Context;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/util/List;

    iget-object p1, p0, LX/F6e;->c:Ljava/util/List;

    aput-object p1, v2, p2

    invoke-virtual {v0, v1, v2}, LX/3nE;->a(Landroid/content/Context;[Ljava/lang/Object;)LX/3nE;

    .line 2200655
    return-void
.end method

.method public static a(LX/F6e;Lcom/facebook/ipc/model/FacebookPhonebookContact;ILandroid/view/View;)Landroid/text/Spanned;
    .locals 6

    .prologue
    .line 2200709
    invoke-virtual {p1}, Lcom/facebook/ipc/model/FacebookPhonebookContact;->a()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2200710
    iget-object v0, p0, LX/F6e;->m:Ljava/util/Set;

    invoke-virtual {p0, p1}, LX/F6e;->a(Lcom/facebook/ipc/model/FacebookPhonebookContact;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2200711
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2200712
    :cond_0
    :goto_0
    return-object v0

    .line 2200713
    :cond_1
    new-instance v0, Landroid/text/SpannableString;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LX/F6e;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2200714
    new-instance v1, Landroid/text/SpannableString;

    iget-object v2, p0, LX/F6e;->e:Landroid/content/Context;

    const v3, 0x7f080032

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2200715
    new-instance v2, LX/F6a;

    invoke-direct {v2, p0, p2, p3}, LX/F6a;-><init>(LX/F6e;ILandroid/view/View;)V

    .line 2200716
    const/4 v3, 0x0

    invoke-interface {v1}, Landroid/text/Spannable;->length()I

    move-result v4

    const/16 v5, 0x21

    invoke-interface {v1, v2, v3, v4, v5}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 2200717
    iget-object v2, p0, LX/F6e;->n:Ljava/util/Hashtable;

    invoke-virtual {p0, p1}, LX/F6e;->a(Lcom/facebook/ipc/model/FacebookPhonebookContact;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Hashtable;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 2200718
    goto :goto_0
.end method


# virtual methods
.method public a(I)I
    .locals 1

    .prologue
    .line 2200708
    const/4 v0, 0x0

    return v0
.end method

.method public abstract a(Lcom/facebook/ipc/model/FacebookPhonebookContact;)J
.end method

.method public a(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2200683
    invoke-virtual {p0, p1, p2}, LX/F6e;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;

    .line 2200684
    if-nez p4, :cond_0

    .line 2200685
    iget-object v1, p0, LX/F6e;->l:Landroid/view/LayoutInflater;

    const v2, 0x7f03104c

    invoke-virtual {v1, v2, p5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p4

    .line 2200686
    :cond_0
    invoke-virtual {p0, p4}, LX/F6e;->a(Landroid/view/View;)V

    .line 2200687
    invoke-virtual {p0, p1, p2}, LX/F6e;->d(II)I

    move-result v2

    .line 2200688
    const v1, 0x7f0d2616

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v3, v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;->name:Ljava/lang/String;

    .line 2200689
    const/16 v4, 0x40

    invoke-virtual {v3, v4}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    .line 2200690
    if-lez v4, :cond_1

    .line 2200691
    const/4 v5, 0x0

    invoke-virtual {v3, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 2200692
    :cond_1
    move-object v3, v3

    .line 2200693
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2200694
    const v1, 0x7f0d271a

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2200695
    invoke-static {p0, v0, v2, p4}, LX/F6e;->a(LX/F6e;Lcom/facebook/ipc/model/FacebookPhonebookContact;ILandroid/view/View;)Landroid/text/Spanned;

    move-result-object v3

    sget-object v4, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v1, v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 2200696
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2200697
    const v1, 0x7f0d271b

    invoke-virtual {p4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 2200698
    invoke-virtual {p0}, LX/F6e;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 2200699
    new-instance v3, LX/F6Z;

    invoke-direct {v3, p0, v2, p4}, LX/F6Z;-><init>(LX/F6e;ILandroid/view/View;)V

    move-object v2, v3

    .line 2200700
    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2200701
    iget-boolean v2, p0, LX/F6e;->o:Z

    if-eqz v2, :cond_3

    .line 2200702
    iget-object v2, p0, LX/F6e;->m:Ljava/util/Set;

    invoke-virtual {p0, v0}, LX/F6e;->a(Lcom/facebook/ipc/model/FacebookPhonebookContact;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2200703
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 2200704
    :goto_0
    iget-boolean v0, p0, LX/F6e;->o:Z

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2200705
    return-object p4

    .line 2200706
    :cond_2
    invoke-virtual {v1, v6}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    .line 2200707
    :cond_3
    invoke-virtual {v1, v6}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 2200678
    if-nez p2, :cond_0

    .line 2200679
    iget-object v0, p0, LX/F6e;->l:Landroid/view/LayoutInflater;

    const v1, 0x7f03079c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 2200680
    check-cast v0, Landroid/widget/TextView;

    .line 2200681
    iget-object v2, p0, LX/F6e;->d:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/F6b;

    invoke-virtual {v2}, LX/F6b;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2200682
    return-object v1

    :cond_0
    move-object v1, p2

    goto :goto_0
.end method

.method public a(II)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2200677
    iget-object v0, p0, LX/F6e;->c:Ljava/util/List;

    invoke-virtual {p0, p1, p2}, LX/F6e;->d(II)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public abstract a(Landroid/view/View;)V
.end method

.method public b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2200676
    iget-object v0, p0, LX/F6e;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public b(ILandroid/view/View;)V
    .locals 10

    .prologue
    .line 2200667
    iget-object v0, p0, LX/F6e;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/FacebookPhonebookContact;

    .line 2200668
    invoke-virtual {p0, v0}, LX/F6e;->a(Lcom/facebook/ipc/model/FacebookPhonebookContact;)J

    move-result-wide v2

    .line 2200669
    iget-object v1, p0, LX/F6e;->m:Ljava/util/Set;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2200670
    iget-object v5, p0, LX/F6e;->q:LX/0Tf;

    new-instance v6, Lcom/facebook/growth/contactimporter/BaseInvitesAdapter$2;

    invoke-direct {v6, p0, v2, v3}, Lcom/facebook/growth/contactimporter/BaseInvitesAdapter$2;-><init>(LX/F6e;J)V

    const-wide/16 v7, 0x4

    sget-object v9, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v5, v6, v7, v8, v9}, LX/0Tf;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)LX/0YG;

    move-result-object v5

    .line 2200671
    iget-object v6, p0, LX/F6e;->i:LX/9Tk;

    iget-object v7, p0, LX/F6e;->f:LX/89v;

    iget-object v7, v7, LX/89v;->value:Ljava/lang/String;

    sget-object v8, LX/9Ti;->FRIEND_FINDER_API:LX/9Ti;

    invoke-virtual {v6, v7, v8}, LX/9Tk;->a(Ljava/lang/String;LX/9Ti;)V

    .line 2200672
    iget-object v6, p0, LX/F6e;->n:Ljava/util/Hashtable;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v6, v7, v5}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2200673
    const v1, 0x7f0d271a

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-static {p0, v0, p1, p2}, LX/F6e;->a(LX/F6e;Lcom/facebook/ipc/model/FacebookPhonebookContact;ILandroid/view/View;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2200674
    const v0, 0x7f0d271b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2200675
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 2200666
    iget-object v0, p0, LX/F6e;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public b(II)Z
    .locals 1

    .prologue
    .line 2200665
    const/4 v0, 0x1

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 2200664
    iget-object v0, p0, LX/F6e;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public c(I)I
    .locals 1

    .prologue
    .line 2200661
    iget-object v0, p0, LX/F6e;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F6b;

    .line 2200662
    iget p0, v0, LX/F6b;->c:I

    move v0, p0

    .line 2200663
    return v0
.end method

.method public c(II)I
    .locals 1

    .prologue
    .line 2200660
    const/4 v0, 0x1

    return v0
.end method

.method public d(II)I
    .locals 1

    .prologue
    .line 2200657
    invoke-virtual {p0, p1}, LX/F6e;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F6b;

    .line 2200658
    iget p0, v0, LX/F6b;->b:I

    move v0, p0

    .line 2200659
    add-int/2addr v0, p2

    return v0
.end method

.method public abstract d()Ljava/lang/String;
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 2200656
    const/4 v0, 0x2

    return v0
.end method

.method public abstract h()Ljava/lang/String;
.end method
