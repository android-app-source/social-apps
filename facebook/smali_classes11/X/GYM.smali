.class public final enum LX/GYM;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GYM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GYM;

.field public static final enum DELETE_SHOP_MUTATION:LX/GYM;

.field public static final enum FETCH_COMMERCE_STORE_QUERY:LX/GYM;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2365008
    new-instance v0, LX/GYM;

    const-string v1, "DELETE_SHOP_MUTATION"

    invoke-direct {v0, v1, v2}, LX/GYM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GYM;->DELETE_SHOP_MUTATION:LX/GYM;

    new-instance v0, LX/GYM;

    const-string v1, "FETCH_COMMERCE_STORE_QUERY"

    invoke-direct {v0, v1, v3}, LX/GYM;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GYM;->FETCH_COMMERCE_STORE_QUERY:LX/GYM;

    .line 2365009
    const/4 v0, 0x2

    new-array v0, v0, [LX/GYM;

    sget-object v1, LX/GYM;->DELETE_SHOP_MUTATION:LX/GYM;

    aput-object v1, v0, v2

    sget-object v1, LX/GYM;->FETCH_COMMERCE_STORE_QUERY:LX/GYM;

    aput-object v1, v0, v3

    sput-object v0, LX/GYM;->$VALUES:[LX/GYM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2365005
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GYM;
    .locals 1

    .prologue
    .line 2365006
    const-class v0, LX/GYM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GYM;

    return-object v0
.end method

.method public static values()[LX/GYM;
    .locals 1

    .prologue
    .line 2365007
    sget-object v0, LX/GYM;->$VALUES:[LX/GYM;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GYM;

    return-object v0
.end method
