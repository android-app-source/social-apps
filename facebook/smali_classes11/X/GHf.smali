.class public final LX/GHf;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Landroid/content/res/Resources;

.field public final synthetic b:LX/GHh;


# direct methods
.method public constructor <init>(LX/GHh;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 2335119
    iput-object p1, p0, LX/GHf;->b:LX/GHh;

    iput-object p2, p0, LX/GHf;->a:Landroid/content/res/Resources;

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    .line 2335120
    iget-object v0, p0, LX/GHf;->b:LX/GHh;

    .line 2335121
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2335122
    new-instance v2, LX/GFS;

    .line 2335123
    iget-object v3, v0, LX/GHh;->a:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v3

    invoke-virtual {v3}, LX/8wL;->getEntryPoint()Ljava/lang/String;

    move-result-object v3

    .line 2335124
    const-string v4, "close/"

    invoke-static {v4}, LX/0ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2335125
    new-instance v5, Landroid/net/Uri$Builder;

    invoke-direct {v5}, Landroid/net/Uri$Builder;-><init>()V

    const-string p0, "https"

    invoke-virtual {v5, p0}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string p0, "m.facebook.com"

    invoke-virtual {v5, p0}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string p0, "ads"

    invoke-virtual {v5, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string p0, "payments"

    invoke-virtual {v5, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string p0, "settle"

    invoke-virtual {v5, p0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string p0, "ad_account_id"

    iget-object p1, v0, LX/GHh;->a:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->l()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v5, p0, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string p0, "entry_point"

    invoke-virtual {v5, p0, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v5, "success_uri"

    invoke-virtual {v3, v5, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v5, "cancel_uri"

    invoke-virtual {v3, v5, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    move-object v3, v3

    .line 2335126
    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2335127
    invoke-virtual {v4, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2335128
    const-string v5, "force_in_app_browser"

    const/4 p0, 0x1

    invoke-virtual {v4, v5, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2335129
    move-object v3, v4

    .line 2335130
    const/4 v4, 0x6

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, LX/GFS;-><init>(Landroid/content/Intent;IZ)V

    invoke-virtual {v1, v2}, LX/GCE;->a(LX/8wN;)V

    .line 2335131
    return-void
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 2

    .prologue
    .line 2335132
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2335133
    iget-object v0, p0, LX/GHf;->a:Landroid/content/res/Resources;

    const v1, 0x7f0a0124

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2335134
    return-void
.end method
