.class public LX/Fd6;
.super LX/0ht;
.source ""


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/search/results/protocol/filters/FilterValue;",
            ">;"
        }
    .end annotation
.end field

.field public final l:LX/Fc6;

.field public final m:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public n:Landroid/graphics/drawable/Drawable;

.field public o:Landroid/graphics/drawable/Drawable;

.field public p:I

.field public q:I

.field public r:I

.field public s:Lcom/facebook/fbui/glyph/GlyphView;

.field public t:Lcom/facebook/ui/search/SearchEditText;

.field public u:Landroid/view/LayoutInflater;

.field public v:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

.field public w:Lcom/facebook/search/results/filters/ui/FilterPopoverListView;

.field public x:LX/Fd7;

.field public y:LX/Fd4;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/Fc7;)V
    .locals 9
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 2263091
    invoke-direct {p0, p1, v8}, LX/0ht;-><init>(Landroid/content/Context;I)V

    .line 2263092
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Fd6;->a:Ljava/util/List;

    .line 2263093
    const-string v0, "city"

    const v1, 0x7f082080

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "employer"

    const v3, 0x7f082081

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const-string v4, "friends"

    const v5, 0x7f082082

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const-string v6, "school"

    const v7, 0x7f082083

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-static/range {v0 .. v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    iput-object v0, p0, LX/Fd6;->m:LX/0P1;

    .line 2263094
    new-instance v0, LX/Fd2;

    invoke-direct {v0, p0}, LX/Fd2;-><init>(LX/Fd6;)V

    invoke-virtual {p2, v0}, LX/Fc7;->a(LX/Fc4;)LX/Fc6;

    move-result-object v0

    iput-object v0, p0, LX/Fd6;->l:LX/Fc6;

    .line 2263095
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a06a2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/Fd6;->q:I

    .line 2263096
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a06a1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/Fd6;->r:I

    .line 2263097
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a06a3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, LX/Fd6;->p:I

    .line 2263098
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, LX/Fd6;->u:Landroid/view/LayoutInflater;

    .line 2263099
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget v1, p0, LX/Fd6;->q:I

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, LX/Fd6;->n:Landroid/graphics/drawable/Drawable;

    .line 2263100
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    iget v1, p0, LX/Fd6;->r:I

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, LX/Fd6;->o:Landroid/graphics/drawable/Drawable;

    .line 2263101
    iget-object v0, p0, LX/Fd6;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f0312b1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    iput-object v0, p0, LX/Fd6;->v:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    .line 2263102
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/0ht;->c(Z)V

    .line 2263103
    new-instance v0, LX/Fd7;

    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/Fd6;->a:Ljava/util/List;

    invoke-direct {v0, v1, v2}, LX/Fd7;-><init>(Landroid/content/Context;Ljava/util/List;)V

    iput-object v0, p0, LX/Fd6;->x:LX/Fd7;

    .line 2263104
    iget-object v0, p0, LX/Fd6;->v:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    const v1, 0x7f0d2ba3

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/results/filters/ui/FilterPopoverListView;

    iput-object v0, p0, LX/Fd6;->w:Lcom/facebook/search/results/filters/ui/FilterPopoverListView;

    .line 2263105
    iget-object v0, p0, LX/Fd6;->v:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    const v1, 0x7f0d1477

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/search/SearchEditText;

    iput-object v0, p0, LX/Fd6;->t:Lcom/facebook/ui/search/SearchEditText;

    .line 2263106
    iget-object v0, p0, LX/Fd6;->v:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    const v1, 0x7f0d1478

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v0, p0, LX/Fd6;->s:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2263107
    iget-object v0, p0, LX/Fd6;->s:Lcom/facebook/fbui/glyph/GlyphView;

    new-instance v1, LX/Fcz;

    invoke-direct {v1, p0}, LX/Fcz;-><init>(LX/Fd6;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2263108
    iget-object v0, p0, LX/Fd6;->t:Lcom/facebook/ui/search/SearchEditText;

    new-instance v1, LX/Fd5;

    invoke-direct {v1, p0}, LX/Fd5;-><init>(LX/Fd6;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2263109
    iget-object v0, p0, LX/Fd6;->t:Lcom/facebook/ui/search/SearchEditText;

    new-instance v1, LX/Fd3;

    invoke-direct {v1, p0}, LX/Fd3;-><init>(LX/Fd6;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2263110
    iget-object v0, p0, LX/Fd6;->w:Lcom/facebook/search/results/filters/ui/FilterPopoverListView;

    iget-object v1, p0, LX/Fd6;->x:LX/Fd7;

    invoke-virtual {v0, v1}, Lcom/facebook/search/results/filters/ui/FilterPopoverListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2263111
    iget-object v0, p0, LX/Fd6;->w:Lcom/facebook/search/results/filters/ui/FilterPopoverListView;

    new-instance v1, LX/Fd0;

    invoke-direct {v1, p0}, LX/Fd0;-><init>(LX/Fd6;)V

    invoke-virtual {v0, v1}, Lcom/facebook/search/results/filters/ui/FilterPopoverListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2263112
    iget-object v0, p0, LX/Fd6;->w:Lcom/facebook/search/results/filters/ui/FilterPopoverListView;

    new-instance v1, LX/Fd1;

    invoke-direct {v1, p0}, LX/Fd1;-><init>(LX/Fd6;)V

    .line 2263113
    iput-object v1, v0, Lcom/facebook/widget/listview/BetterListView;->y:LX/2i6;

    .line 2263114
    return-void
.end method

.method public static r(LX/Fd6;)V
    .locals 3

    .prologue
    .line 2263115
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, LX/Fd6;->t:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1}, Lcom/facebook/ui/search/SearchEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 2263116
    return-void
.end method

.method public static s(LX/Fd6;)V
    .locals 2

    .prologue
    .line 2263117
    iget-object v0, p0, LX/Fd6;->t:Lcom/facebook/ui/search/SearchEditText;

    iget v1, p0, LX/Fd6;->q:I

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setTextColor(I)V

    .line 2263118
    return-void
.end method

.method public static u(LX/Fd6;)V
    .locals 2

    .prologue
    .line 2263119
    iget-object v0, p0, LX/Fd6;->t:Lcom/facebook/ui/search/SearchEditText;

    iget v1, p0, LX/Fd6;->p:I

    invoke-virtual {v0, v1}, Lcom/facebook/ui/search/SearchEditText;->setTextColor(I)V

    .line 2263120
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;ZLandroid/view/WindowManager$LayoutParams;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2263121
    invoke-super {p0, p1, p2, p3}, LX/0ht;->a(Landroid/view/View;ZLandroid/view/WindowManager$LayoutParams;)V

    .line 2263122
    if-nez p2, :cond_1

    .line 2263123
    :cond_0
    :goto_0
    return-void

    .line 2263124
    :cond_1
    iget-object v0, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 2263125
    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 2263126
    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 2263127
    iget-object v1, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2263128
    iget-object v0, p0, LX/Fd6;->w:Lcom/facebook/search/results/filters/ui/FilterPopoverListView;

    .line 2263129
    iget-boolean v1, v0, Lcom/facebook/search/results/filters/ui/FilterPopoverListView;->b:Z

    move v0, v1

    .line 2263130
    if-nez v0, :cond_0

    .line 2263131
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 2263132
    invoke-virtual {p1, v0}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2263133
    aget v0, v0, v2

    .line 2263134
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 2263135
    iget-object v2, p0, LX/0ht;->f:Lcom/facebook/fbui/popover/PopoverViewFlipper;

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    invoke-virtual {v2, v0}, Lcom/facebook/fbui/popover/PopoverViewFlipper;->setNubOffset(I)V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2263136
    iget-object v0, p0, LX/Fd6;->v:Lcom/facebook/fbui/widget/layout/SegmentedLinearLayout;

    invoke-virtual {p0, v0}, LX/0ht;->d(Landroid/view/View;)V

    .line 2263137
    invoke-super {p0}, LX/0ht;->d()V

    .line 2263138
    return-void
.end method

.method public final l()V
    .locals 3

    .prologue
    .line 2263139
    iget-object v0, p0, LX/Fd6;->l:LX/Fc6;

    .line 2263140
    iget-object v1, v0, LX/Fc6;->b:LX/Fcp;

    .line 2263141
    iget-object v2, v1, LX/Fcp;->d:LX/1Ck;

    invoke-virtual {v2}, LX/1Ck;->c()V

    .line 2263142
    iget-object v1, v0, LX/Fc6;->e:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    .line 2263143
    iget-boolean v0, p0, LX/0ht;->r:Z

    move v0, v0

    .line 2263144
    if-eqz v0, :cond_0

    .line 2263145
    invoke-static {p0}, LX/Fd6;->r(LX/Fd6;)V

    .line 2263146
    :cond_0
    invoke-super {p0}, LX/0ht;->l()V

    .line 2263147
    return-void
.end method
