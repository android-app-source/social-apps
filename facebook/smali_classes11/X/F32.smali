.class public final LX/F32;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;)V
    .locals 0

    .prologue
    .line 2193635
    iput-object p1, p0, LX/F32;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x72f8ac71

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2193597
    iget-object v1, p0, LX/F32;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    const/4 v4, 0x0

    .line 2193598
    iget-object v3, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->l:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v3}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2193599
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2193600
    invoke-static {v3}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_b

    move v5, v6

    .line 2193601
    :goto_0
    const/4 v9, 0x0

    .line 2193602
    move v8, v9

    :goto_1
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result p1

    if-ge v8, p1, :cond_0

    .line 2193603
    invoke-virtual {v3, v8}, Ljava/lang/String;->charAt(I)C

    move-result p1

    invoke-static {p1}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result p1

    if-eqz p1, :cond_e

    .line 2193604
    const/4 v9, 0x1

    .line 2193605
    :cond_0
    move v8, v9

    .line 2193606
    if-eqz v5, :cond_1

    if-eqz v8, :cond_1

    move v7, v6

    .line 2193607
    :cond_1
    if-nez v7, :cond_2

    .line 2193608
    if-nez v5, :cond_c

    iget-object v6, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a:Landroid/content/res/Resources;

    const v8, 0x7f0831c2

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    :goto_2
    if-nez v5, :cond_d

    iget-object v5, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a:Landroid/content/res/Resources;

    const v8, 0x7f0831c3

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 2193609
    :goto_3
    new-instance v8, LX/0ju;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9}, LX/0ju;-><init>(Landroid/content/Context;)V

    .line 2193610
    invoke-virtual {v8, v6}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v8

    invoke-virtual {v8, v5}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v8

    iget-object v9, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a:Landroid/content/res/Resources;

    const p1, 0x7f080016

    invoke-virtual {v9, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 p1, 0x0

    invoke-virtual {v8, v9, p1}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v8

    invoke-virtual {v8}, LX/0ju;->b()LX/2EJ;

    .line 2193611
    :cond_2
    move v5, v7

    .line 2193612
    if-nez v5, :cond_4

    .line 2193613
    const/4 v3, 0x0

    .line 2193614
    :goto_4
    move v1, v3

    .line 2193615
    if-eqz v1, :cond_3

    .line 2193616
    iget-object v1, p0, LX/F32;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;

    invoke-static {v1}, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->m(Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;)V

    .line 2193617
    :cond_3
    const v1, -0x6eb0d133

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2193618
    :cond_4
    iget-object v5, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->n:Lcom/facebook/widget/text/BetterEditTextView;

    invoke-virtual {v5}, Lcom/facebook/widget/text/BetterEditTextView;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 2193619
    iget-object v6, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->o:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    if-eqz v6, :cond_a

    .line 2193620
    iget-object v6, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->o:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v6}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->s()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    move-object v3, v4

    .line 2193621
    :cond_5
    iget-object v6, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->o:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v6}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->m()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 2193622
    :goto_5
    if-nez v3, :cond_6

    if-eqz v4, :cond_9

    .line 2193623
    :cond_6
    iget-object v5, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->b:LX/F2L;

    iget-object v6, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->q:Ljava/lang/String;

    .line 2193624
    new-instance v7, LX/4Fk;

    invoke-direct {v7}, LX/4Fk;-><init>()V

    .line 2193625
    if-eqz v3, :cond_7

    .line 2193626
    const-string v8, "name"

    invoke-virtual {v7, v8, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2193627
    :cond_7
    if-eqz v4, :cond_8

    .line 2193628
    const-string v8, "description"

    invoke-virtual {v7, v8, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2193629
    :cond_8
    new-instance v8, LX/F2D;

    invoke-direct {v8, v5}, LX/F2D;-><init>(LX/F2L;)V

    .line 2193630
    invoke-static {v5, v6, v7, v8}, LX/F2L;->a(LX/F2L;Ljava/lang/String;LX/4Fk;LX/0TF;)V

    .line 2193631
    :cond_9
    const/4 v3, 0x1

    goto :goto_4

    :cond_a
    move-object v4, v5

    goto :goto_5

    :cond_b
    move v5, v7

    .line 2193632
    goto/16 :goto_0

    .line 2193633
    :cond_c
    iget-object v6, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a:Landroid/content/res/Resources;

    const v8, 0x7f0831c4

    invoke-virtual {v6, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_2

    :cond_d
    iget-object v5, v1, Lcom/facebook/groups/editsettings/fragment/GroupEditNameDescriptionFragment;->a:Landroid/content/res/Resources;

    const v8, 0x7f0831c5

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_3

    .line 2193634
    :cond_e
    add-int/lit8 v8, v8, 0x1

    goto/16 :goto_1
.end method
