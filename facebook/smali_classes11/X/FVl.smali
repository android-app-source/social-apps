.class public final LX/FVl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FUw;


# instance fields
.field public final synthetic a:LX/FVn;


# direct methods
.method public constructor <init>(LX/FVn;)V
    .locals 0

    .prologue
    .line 2251050
    iput-object p1, p0, LX/FVl;->a:LX/FVn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2251051
    iget-object v0, p0, LX/FVl;->a:LX/FVn;

    iget-object v0, v0, LX/FVn;->j:LX/FVS;

    invoke-virtual {v0}, LX/FVS;->f()V

    .line 2251052
    return-void
.end method

.method public final a(LX/FVy;)V
    .locals 14

    .prologue
    .line 2251053
    iget-object v0, p1, LX/FVy;->a:LX/0am;

    move-object v0, v0

    .line 2251054
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2251055
    iget-object v0, p0, LX/FVl;->a:LX/FVn;

    sget-object v1, LX/FWA;->FROM_CACHE:LX/FWA;

    .line 2251056
    invoke-static {v0, p1, v1}, LX/FVn;->a$redex0(LX/FVn;LX/FVy;LX/FWA;)V

    .line 2251057
    iget-object v0, p0, LX/FVl;->a:LX/FVn;

    iget-object v1, v0, LX/FVn;->j:LX/FVS;

    iget-object v0, p0, LX/FVl;->a:LX/FVn;

    iget-object v0, v0, LX/FVn;->r:LX/FWC;

    invoke-virtual {v0}, LX/FWC;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 2251058
    :goto_0
    iget-object v2, p1, LX/FVy;->c:Ljava/lang/Long;

    move-object v2, v2

    .line 2251059
    iget-object v3, v1, LX/FVS;->g:LX/FWh;

    .line 2251060
    if-nez v2, :cond_3

    .line 2251061
    const-string v6, "SAVED_CACHED_ITEM_LOAD"

    .line 2251062
    const/4 v7, 0x0

    invoke-static {v3, v6, v7}, LX/FWh;->a(LX/FWh;Ljava/lang/String;LX/0P1;)V

    .line 2251063
    :goto_1
    invoke-static {v1}, LX/FVS;->k(LX/FVS;)V

    .line 2251064
    const/4 v3, 0x2

    iput v3, v1, LX/FVS;->c:I

    .line 2251065
    if-nez v0, :cond_0

    .line 2251066
    iget-object v3, v1, LX/FVS;->i:LX/FVp;

    new-instance v4, LX/FVQ;

    invoke-direct {v4, v1}, LX/FVQ;-><init>(LX/FVS;)V

    iget-object v5, v1, LX/FVS;->f:LX/0am;

    invoke-virtual {v3, v4, v5}, LX/FVp;->a(LX/FVP;LX/0am;)Z

    move-result v3

    .line 2251067
    if-eqz v3, :cond_0

    .line 2251068
    iget-object v3, v1, LX/FVS;->g:LX/FWh;

    .line 2251069
    const-string v4, "SAVED_DASH_START_TO_CACHE_ITEM_DRAWN"

    invoke-static {v3, v4}, LX/FWh;->b(LX/FWh;Ljava/lang/String;)V

    .line 2251070
    :cond_0
    :goto_2
    return-void

    .line 2251071
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2251072
    :cond_2
    iget-object v0, p0, LX/FVl;->a:LX/FVn;

    iget-object v0, v0, LX/FVn;->j:LX/FVS;

    invoke-virtual {v0}, LX/FVS;->f()V

    goto :goto_2

    .line 2251073
    :cond_3
    const-string v8, "SAVED_CACHED_ITEM_LOAD"

    const-string v9, "cache_age"

    if-nez v2, :cond_4

    const-string v6, ""

    :goto_3
    const-string v10, "SAVED_DASH_START_EARLY_FETCH_ADVANTAGE"

    iget-object v7, v3, LX/FWh;->e:Ljava/lang/Long;

    if-nez v7, :cond_5

    const-string v7, ""

    :goto_4
    invoke-static {v9, v6, v10, v7}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v6

    invoke-static {v3, v8, v6}, LX/FWh;->a(LX/FWh;Ljava/lang/String;LX/0P1;)V

    goto :goto_1

    :cond_4
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    goto :goto_3

    :cond_5
    iget-object v7, v3, LX/FWh;->e:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    goto :goto_4
.end method
