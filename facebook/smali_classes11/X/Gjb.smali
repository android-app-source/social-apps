.class public final LX/Gjb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/7hQ;


# instance fields
.field public final synthetic a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/greetingcards/render/FoldingPopoverFragment;)V
    .locals 0

    .prologue
    .line 2388026
    iput-object p1, p0, LX/Gjb;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2388009
    iget-object v0, p0, LX/Gjb;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-boolean v0, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->u:Z

    if-eqz v0, :cond_3

    .line 2388010
    iget-object v0, p0, LX/Gjb;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v0, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->x:LX/JOF;

    if-eqz v0, :cond_1

    .line 2388011
    iget-object v0, p0, LX/Gjb;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v0, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->x:LX/JOF;

    .line 2388012
    iget-object v1, v0, LX/JOF;->a:LX/JOG;

    iget-object v1, v1, LX/JOG;->g:LX/JOH;

    iget-object v1, v1, LX/JOH;->c:LX/JOL;

    iget-object v1, v1, LX/JOL;->a:LX/Gk2;

    if-nez v1, :cond_0

    .line 2388013
    iget-object v1, v0, LX/JOF;->a:LX/JOG;

    iget-object v1, v1, LX/JOG;->g:LX/JOH;

    iget-object v1, v1, LX/JOH;->c:LX/JOL;

    iget-object v2, v0, LX/JOF;->a:LX/JOG;

    iget-object v2, v2, LX/JOG;->c:Lcom/facebook/greetingcards/render/RenderCardFragment;

    .line 2388014
    iget-object v3, v2, Lcom/facebook/greetingcards/render/RenderCardFragment;->o:LX/Gk2;

    move-object v2, v3

    .line 2388015
    iput-object v2, v1, LX/JOL;->a:LX/Gk2;

    .line 2388016
    :cond_0
    iget-object v1, v0, LX/JOF;->a:LX/JOG;

    iget-object v1, v1, LX/JOG;->g:LX/JOH;

    iget-object v1, v1, LX/JOH;->c:LX/JOL;

    iget-object v1, v1, LX/JOL;->f:LX/Gjt;

    if-eqz v1, :cond_1

    .line 2388017
    iget-object v1, v0, LX/JOF;->a:LX/JOG;

    iget-object v1, v1, LX/JOG;->g:LX/JOH;

    iget-object v1, v1, LX/JOH;->c:LX/JOL;

    iget-object v1, v1, LX/JOL;->f:LX/Gjt;

    invoke-virtual {v1}, LX/Gjt;->b()V

    .line 2388018
    :cond_1
    iget-object v0, p0, LX/Gjb;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2388019
    :cond_2
    :goto_0
    return-void

    .line 2388020
    :cond_3
    iget-object v0, p0, LX/Gjb;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v0, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->x:LX/JOF;

    if-eqz v0, :cond_2

    .line 2388021
    iget-object v0, p0, LX/Gjb;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v0, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->x:LX/JOF;

    .line 2388022
    iget-object v1, v0, LX/JOF;->a:LX/JOG;

    iget-object v1, v1, LX/JOG;->c:Lcom/facebook/greetingcards/render/RenderCardFragment;

    .line 2388023
    iget-object v2, v1, Lcom/facebook/greetingcards/render/RenderCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    if-eqz v2, :cond_4

    iget-object v2, v1, Lcom/facebook/greetingcards/render/RenderCardFragment;->u:LX/Gjp;

    sget-object v0, LX/Gjp;->LOAD_COMPLETE:LX/Gjp;

    if-eq v2, v0, :cond_4

    .line 2388024
    iget-object v2, v1, Lcom/facebook/greetingcards/render/RenderCardFragment;->a:Lcom/facebook/greetingcards/model/GreetingCard;

    invoke-static {v1, v2}, Lcom/facebook/greetingcards/render/RenderCardFragment;->b(Lcom/facebook/greetingcards/render/RenderCardFragment;Lcom/facebook/greetingcards/model/GreetingCard;)V

    .line 2388025
    :cond_4
    goto :goto_0
.end method

.method public final a(LX/8YK;)V
    .locals 12

    .prologue
    .line 2387916
    invoke-virtual {p1}, LX/8YK;->b()D

    move-result-wide v0

    double-to-float v0, v0

    .line 2387917
    iget-object v1, p0, LX/Gjb;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v1, v1, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->p:LX/GjY;

    const/4 v10, 0x0

    const/4 v9, 0x4

    const/high16 v8, 0x3f800000    # 1.0f

    .line 2387918
    iput v0, v1, LX/GjY;->f:F

    .line 2387919
    iget-boolean v2, v1, LX/GjY;->p:Z

    if-nez v2, :cond_0

    iget-object v2, v1, LX/GjY;->b:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, v1, LX/GjY;->c:Landroid/view/View;

    if-nez v2, :cond_7

    .line 2387920
    :cond_0
    :goto_0
    iget-boolean v2, v1, LX/GjY;->o:Z

    if-eqz v2, :cond_3

    .line 2387921
    const/4 v5, 0x0

    .line 2387922
    invoke-virtual {v1}, LX/GjY;->getWidth()I

    move-result v2

    .line 2387923
    invoke-virtual {v1}, LX/GjY;->getHeight()I

    move-result v3

    .line 2387924
    iget-object v4, v1, LX/GjY;->n:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_1

    .line 2387925
    iget-object v4, v1, LX/GjY;->n:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    .line 2387926
    iput-object v5, v1, LX/GjY;->n:Landroid/graphics/Bitmap;

    .line 2387927
    :cond_1
    if-eqz v2, :cond_2

    if-eqz v3, :cond_2

    .line 2387928
    :try_start_0
    sget-object v4, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, v1, LX/GjY;->n:Landroid/graphics/Bitmap;

    .line 2387929
    new-instance v2, Landroid/graphics/Canvas;

    iget-object v3, v1, LX/GjY;->n:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 2387930
    iget-object v3, v1, LX/GjY;->c:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 2387931
    :cond_2
    :goto_1
    iget-object v2, v1, LX/GjY;->d:LX/GjX;

    iget-object v3, v1, LX/GjY;->n:Landroid/graphics/Bitmap;

    const/16 v4, 0x30

    invoke-virtual {v2, v3, v4}, LX/GjX;->a(Landroid/graphics/Bitmap;I)V

    .line 2387932
    iget-object v2, v1, LX/GjY;->e:LX/GjX;

    iget-object v3, v1, LX/GjY;->n:Landroid/graphics/Bitmap;

    const/16 v4, 0x50

    invoke-virtual {v2, v3, v4}, LX/GjX;->a(Landroid/graphics/Bitmap;I)V

    .line 2387933
    iput-boolean v10, v1, LX/GjY;->o:Z

    .line 2387934
    :cond_3
    iget v2, v1, LX/GjY;->j:F

    sub-float v3, v8, v0

    mul-float/2addr v2, v3

    sub-float v2, v8, v2

    .line 2387935
    iget v3, v1, LX/GjY;->k:F

    sub-float v4, v8, v0

    mul-float/2addr v3, v4

    sub-float v3, v8, v3

    .line 2387936
    cmpl-float v4, v0, v8

    if-nez v4, :cond_5

    .line 2387937
    iget-object v4, v1, LX/GjY;->b:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 2387938
    iget-object v4, v1, LX/GjY;->c:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    .line 2387939
    iget-object v4, v1, LX/GjY;->d:LX/GjX;

    invoke-virtual {v4, v9}, LX/GjX;->setVisibility(I)V

    .line 2387940
    iget-object v4, v1, LX/GjY;->e:LX/GjX;

    invoke-virtual {v4, v9}, LX/GjX;->setVisibility(I)V

    .line 2387941
    :goto_2
    iget-object v4, v1, LX/GjY;->e:LX/GjX;

    invoke-virtual {v4, v2}, LX/GjX;->setScaleX(F)V

    .line 2387942
    iget-object v2, v1, LX/GjY;->e:LX/GjX;

    invoke-virtual {v2, v3}, LX/GjX;->setScaleY(F)V

    .line 2387943
    iget-object v2, v1, LX/GjY;->e:LX/GjX;

    iget v3, v1, LX/GjY;->i:F

    neg-float v3, v3

    sub-float v4, v8, v0

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, LX/GjX;->setTranslationY(F)V

    .line 2387944
    iget-object v1, p0, LX/Gjb;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v1, v1, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->v:LX/GjZ;

    if-eqz v1, :cond_4

    .line 2387945
    iget-object v1, p0, LX/Gjb;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v1, v1, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->v:LX/GjZ;

    const/high16 v3, 0x3f800000    # 1.0f

    .line 2387946
    iget-object v2, v1, LX/GjZ;->b:Landroid/view/View;

    if-eqz v2, :cond_4

    .line 2387947
    const v2, 0x3d4cccd0    # 0.050000012f

    mul-float/2addr v2, v0

    sub-float v2, v3, v2

    .line 2387948
    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 2387949
    iget-object v3, v1, LX/GjZ;->b:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setScaleX(F)V

    .line 2387950
    iget-object v3, v1, LX/GjZ;->b:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setScaleY(F)V

    .line 2387951
    const/high16 v2, 0x43320000    # 178.0f

    mul-float/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 2387952
    invoke-virtual {v1}, LX/GjZ;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 2387953
    :cond_4
    return-void

    .line 2387954
    :cond_5
    float-to-double v4, v0

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    cmpl-double v4, v4, v6

    if-lez v4, :cond_6

    .line 2387955
    iget-object v4, v1, LX/GjY;->b:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 2387956
    iget-object v4, v1, LX/GjY;->c:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 2387957
    iget-object v4, v1, LX/GjY;->d:LX/GjX;

    invoke-virtual {v4, v10}, LX/GjX;->setVisibility(I)V

    .line 2387958
    iget-object v4, v1, LX/GjY;->e:LX/GjX;

    invoke-virtual {v4, v10}, LX/GjX;->setVisibility(I)V

    .line 2387959
    iget-object v4, v1, LX/GjY;->d:LX/GjX;

    invoke-virtual {v4, v2}, LX/GjX;->setScaleX(F)V

    .line 2387960
    iget-object v4, v1, LX/GjY;->d:LX/GjX;

    invoke-virtual {v4, v3}, LX/GjX;->setScaleY(F)V

    .line 2387961
    iget-object v4, v1, LX/GjY;->d:LX/GjX;

    const/high16 v5, -0x3ccc0000    # -180.0f

    sub-float v6, v8, v0

    mul-float/2addr v5, v6

    invoke-virtual {v4, v5}, LX/GjX;->setRotationX(F)V

    .line 2387962
    iget-object v4, v1, LX/GjY;->m:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerY()I

    move-result v4

    iget-object v5, v1, LX/GjY;->l:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    .line 2387963
    iget-object v5, v1, LX/GjY;->d:LX/GjX;

    neg-float v4, v4

    sub-float v6, v8, v0

    mul-float/2addr v4, v6

    invoke-virtual {v5, v4}, LX/GjX;->setTranslationY(F)V

    goto/16 :goto_2

    .line 2387964
    :cond_6
    iget-object v4, v1, LX/GjY;->b:Landroid/view/View;

    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    .line 2387965
    iget-object v4, v1, LX/GjY;->c:Landroid/view/View;

    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    .line 2387966
    iget-object v4, v1, LX/GjY;->d:LX/GjX;

    invoke-virtual {v4, v9}, LX/GjX;->setVisibility(I)V

    .line 2387967
    iget-object v4, v1, LX/GjY;->e:LX/GjX;

    invoke-virtual {v4, v10}, LX/GjX;->setVisibility(I)V

    .line 2387968
    iget-object v4, v1, LX/GjY;->b:Landroid/view/View;

    iget v5, v1, LX/GjY;->h:F

    mul-float/2addr v5, v0

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationX(F)V

    .line 2387969
    iget-object v4, v1, LX/GjY;->b:Landroid/view/View;

    iget v5, v1, LX/GjY;->i:F

    mul-float/2addr v5, v0

    invoke-virtual {v4, v5}, Landroid/view/View;->setTranslationY(F)V

    .line 2387970
    iget v4, v1, LX/GjY;->g:F

    mul-float/2addr v4, v0

    sub-float v4, v8, v4

    .line 2387971
    iget-object v5, v1, LX/GjY;->b:Landroid/view/View;

    invoke-virtual {v5, v4}, Landroid/view/View;->setScaleX(F)V

    .line 2387972
    iget-object v5, v1, LX/GjY;->b:Landroid/view/View;

    invoke-virtual {v5, v4}, Landroid/view/View;->setScaleY(F)V

    .line 2387973
    iget-object v4, v1, LX/GjY;->b:Landroid/view/View;

    const/high16 v5, 0x43340000    # 180.0f

    mul-float/2addr v5, v0

    invoke-virtual {v4, v5}, Landroid/view/View;->setRotationX(F)V

    goto/16 :goto_2

    .line 2387974
    :cond_7
    const/4 v11, -0x1

    const/4 v7, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    .line 2387975
    invoke-virtual {v1}, LX/GjY;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v6

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 2387976
    new-instance v3, LX/GjX;

    invoke-virtual {v1}, LX/GjY;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/GjX;-><init>(Landroid/content/Context;)V

    iput-object v3, v1, LX/GjY;->d:LX/GjX;

    .line 2387977
    iget-object v3, v1, LX/GjY;->d:LX/GjX;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v5, 0x30

    invoke-direct {v4, v11, v2, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v3, v4}, LX/GjX;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2387978
    iget-object v3, v1, LX/GjY;->d:LX/GjX;

    invoke-virtual {v1}, LX/GjY;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v6

    invoke-virtual {v3, v4}, LX/GjX;->setPivotY(F)V

    .line 2387979
    iget-object v3, v1, LX/GjY;->d:LX/GjX;

    invoke-virtual {v1}, LX/GjY;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v6

    invoke-virtual {v3, v4}, LX/GjX;->setPivotX(F)V

    .line 2387980
    new-instance v3, LX/GjX;

    invoke-virtual {v1}, LX/GjY;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, LX/GjX;-><init>(Landroid/content/Context;)V

    iput-object v3, v1, LX/GjY;->e:LX/GjX;

    .line 2387981
    iget-object v3, v1, LX/GjY;->e:LX/GjX;

    new-instance v4, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v5, 0x50

    invoke-direct {v4, v11, v2, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    invoke-virtual {v3, v4}, LX/GjX;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2387982
    iget-object v2, v1, LX/GjY;->e:LX/GjX;

    invoke-virtual {v2, v7}, LX/GjX;->setPivotY(F)V

    .line 2387983
    iget-object v2, v1, LX/GjY;->e:LX/GjX;

    invoke-virtual {v1}, LX/GjY;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v6

    invoke-virtual {v2, v3}, LX/GjX;->setPivotX(F)V

    .line 2387984
    iget-object v2, v1, LX/GjY;->b:Landroid/view/View;

    invoke-virtual {v1}, LX/GjY;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    mul-int/lit8 v3, v3, 0x30

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setCameraDistance(F)V

    .line 2387985
    iget-object v2, v1, LX/GjY;->b:Landroid/view/View;

    invoke-virtual {v2, v7}, Landroid/view/View;->setPivotY(F)V

    .line 2387986
    iget-object v2, v1, LX/GjY;->b:Landroid/view/View;

    iget-object v3, v1, LX/GjY;->b:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v6

    const v4, 0x3dcccccd    # 0.1f

    add-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/view/View;->setPivotX(F)V

    .line 2387987
    iget-object v2, v1, LX/GjY;->e:LX/GjX;

    invoke-virtual {v1, v2}, LX/GjY;->addView(Landroid/view/View;)V

    .line 2387988
    iget-object v2, v1, LX/GjY;->d:LX/GjX;

    invoke-virtual {v1, v2}, LX/GjY;->addView(Landroid/view/View;)V

    .line 2387989
    iget-object v2, v1, LX/GjY;->b:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->bringToFront()V

    .line 2387990
    const/high16 v5, 0x3f800000    # 1.0f

    .line 2387991
    iget-object v2, v1, LX/GjY;->b:Landroid/view/View;

    invoke-static {v2}, LX/1r0;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v2

    iput-object v2, v1, LX/GjY;->l:Landroid/graphics/Rect;

    .line 2387992
    iget-object v2, v1, LX/GjY;->c:Landroid/view/View;

    invoke-static {v2}, LX/1r0;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v2

    iput-object v2, v1, LX/GjY;->m:Landroid/graphics/Rect;

    .line 2387993
    iget-object v2, v1, LX/GjY;->l:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-float v2, v2

    .line 2387994
    iget-object v3, v1, LX/GjY;->m:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v3

    int-to-float v3, v3

    .line 2387995
    sub-float v2, v3, v2

    iput v2, v1, LX/GjY;->h:F

    .line 2387996
    iget-object v2, v1, LX/GjY;->l:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    .line 2387997
    iget-object v3, v1, LX/GjY;->m:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    int-to-float v3, v3

    .line 2387998
    sub-float v2, v3, v2

    iput v2, v1, LX/GjY;->i:F

    .line 2387999
    iget-object v2, v1, LX/GjY;->l:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    .line 2388000
    iget-object v3, v1, LX/GjY;->m:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    .line 2388001
    div-float v4, v3, v2

    sub-float v4, v5, v4

    iput v4, v1, LX/GjY;->g:F

    .line 2388002
    div-float/2addr v2, v3

    sub-float v2, v5, v2

    iput v2, v1, LX/GjY;->j:F

    .line 2388003
    iget-object v2, v1, LX/GjY;->m:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    .line 2388004
    iget-object v3, v1, LX/GjY;->l:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    div-float v2, v3, v2

    sub-float v2, v5, v2

    iput v2, v1, LX/GjY;->k:F

    .line 2388005
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/GjY;->p:Z

    goto/16 :goto_0

    .line 2388006
    :catch_0
    move-exception v2

    .line 2388007
    sget-object v3, LX/GjY;->a:Ljava/lang/Class;

    const-string v4, "Cached bitmap caused an OutOfMemory"

    invoke-static {v3, v4, v2}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2388008
    iput-object v5, v1, LX/GjY;->n:Landroid/graphics/Bitmap;

    goto/16 :goto_1
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 2387915
    return-void
.end method

.method public final b(LX/8YK;)V
    .locals 2

    .prologue
    .line 2387911
    invoke-virtual {p1}, LX/8YK;->b()D

    move-result-wide v0

    double-to-float v0, v0

    .line 2387912
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_0

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 2387913
    :cond_0
    iget-object v0, p0, LX/Gjb;->a:Lcom/facebook/greetingcards/render/FoldingPopoverFragment;

    iget-object v0, v0, Lcom/facebook/greetingcards/render/FoldingPopoverFragment;->p:LX/GjY;

    invoke-virtual {v0}, LX/GjY;->a()V

    .line 2387914
    :cond_1
    return-void
.end method
