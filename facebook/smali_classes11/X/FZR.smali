.class public final LX/FZR;
.super LX/2Ip;
.source ""


# instance fields
.field public final synthetic a:LX/FZS;


# direct methods
.method public constructor <init>(LX/FZS;)V
    .locals 0

    .prologue
    .line 2257202
    iput-object p1, p0, LX/FZR;->a:LX/FZS;

    invoke-direct {p0}, LX/2Ip;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 2257203
    check-cast p1, LX/2f2;

    .line 2257204
    if-eqz p1, :cond_0

    iget-boolean v0, p1, LX/2f2;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->INCOMING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v0, v1, :cond_1

    iget-object v0, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->OUTGOING_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v0, v1, :cond_1

    iget-object v0, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v0, v1, :cond_1

    iget-object v0, p1, LX/2f2;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    if-eq v0, v1, :cond_1

    .line 2257205
    :cond_0
    :goto_0
    return-void

    .line 2257206
    :cond_1
    iget-wide v0, p1, LX/2f2;->a:J

    .line 2257207
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2257208
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2257209
    iget-object v0, p0, LX/FZR;->a:LX/FZS;

    iget-object v0, v0, LX/FZS;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/3Qc;

    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    sget-object v2, LX/8ca;->FRIENDSHIP_STATUS_CHANGE:LX/8ca;

    invoke-virtual {v0, v1, v2}, LX/3Qc;->a(LX/0Px;LX/8ca;)V

    goto :goto_0
.end method
