.class public LX/GJK;
.super LX/GJJ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GJJ",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;",
        ">;"
    }
.end annotation


# instance fields
.field public g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

.field private final h:LX/GG6;

.field private i:LX/GMU;

.field private j:LX/2U3;

.field public k:I

.field public l:LX/GK4;

.field private final m:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private n:LX/GNC;

.field public o:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

.field public p:I

.field private q:I

.field private r:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

.field public s:I


# direct methods
.method public constructor <init>(LX/GG6;LX/2U3;Landroid/view/inputmethod/InputMethodManager;LX/GMU;LX/1Ck;LX/GE2;LX/GLN;LX/0Sh;LX/0tX;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/GK4;LX/GNC;LX/0ad;)V
    .locals 13
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2338557
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    move-object/from16 v12, p13

    invoke-direct/range {v1 .. v12}, LX/GJJ;-><init>(LX/GG6;LX/2U3;Landroid/view/inputmethod/InputMethodManager;LX/GMU;LX/1Ck;LX/GE2;LX/GLN;LX/0Sh;LX/0tX;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0ad;)V

    .line 2338558
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/GJK;->m:Ljava/util/List;

    .line 2338559
    iput-object p1, p0, LX/GJK;->h:LX/GG6;

    .line 2338560
    move-object/from16 v0, p4

    iput-object v0, p0, LX/GJK;->i:LX/GMU;

    .line 2338561
    iput-object p2, p0, LX/GJK;->j:LX/2U3;

    .line 2338562
    move-object/from16 v0, p11

    iput-object v0, p0, LX/GJK;->l:LX/GK4;

    .line 2338563
    move-object/from16 v0, p12

    iput-object v0, p0, LX/GJK;->n:LX/GNC;

    .line 2338564
    return-void
.end method

.method public static A(LX/GJK;)V
    .locals 14

    .prologue
    const/4 v3, 0x5

    .line 2338565
    invoke-direct {p0}, LX/GJK;->C()I

    move-result v1

    .line 2338566
    const/4 v0, -0x1

    if-eq v1, v0, :cond_0

    .line 2338567
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {v0, v3}, LX/GJD;->d(I)V

    .line 2338568
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {v0, v1}, LX/GJD;->f(I)V

    .line 2338569
    :goto_0
    return-void

    .line 2338570
    :cond_0
    invoke-direct {p0}, LX/GJK;->D()Landroid/text/Spanned;

    move-result-object v1

    .line 2338571
    new-instance v0, LX/A99;

    invoke-direct {v0}, LX/A99;-><init>()V

    iget-object v2, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->g()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v2

    .line 2338572
    iput-object v2, v0, LX/A99;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    .line 2338573
    move-object v0, v0

    .line 2338574
    const/4 v8, 0x1

    const/4 v6, 0x0

    const/4 v13, 0x0

    .line 2338575
    new-instance v4, LX/186;

    const/16 v5, 0x80

    invoke-direct {v4, v5}, LX/186;-><init>(I)V

    .line 2338576
    iget-object v5, v0, LX/A99;->b:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    invoke-static {v4, v5}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v5

    .line 2338577
    iget-object v7, v0, LX/A99;->c:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    invoke-virtual {v4, v7}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v7

    .line 2338578
    iget-object v9, v0, LX/A99;->e:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    invoke-static {v4, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2338579
    iget-object v10, v0, LX/A99;->f:Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    invoke-virtual {v4, v10}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v10

    .line 2338580
    iget-object v11, v0, LX/A99;->g:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    invoke-static {v4, v11}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v11

    .line 2338581
    const/4 v12, 0x7

    invoke-virtual {v4, v12}, LX/186;->c(I)V

    .line 2338582
    iget v12, v0, LX/A99;->a:I

    invoke-virtual {v4, v13, v12, v13}, LX/186;->a(III)V

    .line 2338583
    invoke-virtual {v4, v8, v5}, LX/186;->b(II)V

    .line 2338584
    const/4 v5, 0x2

    invoke-virtual {v4, v5, v7}, LX/186;->b(II)V

    .line 2338585
    const/4 v5, 0x3

    iget v7, v0, LX/A99;->d:I

    invoke-virtual {v4, v5, v7, v13}, LX/186;->a(III)V

    .line 2338586
    const/4 v5, 0x4

    invoke-virtual {v4, v5, v9}, LX/186;->b(II)V

    .line 2338587
    const/4 v5, 0x5

    invoke-virtual {v4, v5, v10}, LX/186;->b(II)V

    .line 2338588
    const/4 v5, 0x6

    invoke-virtual {v4, v5, v11}, LX/186;->b(II)V

    .line 2338589
    invoke-virtual {v4}, LX/186;->d()I

    move-result v5

    .line 2338590
    invoke-virtual {v4, v5}, LX/186;->d(I)V

    .line 2338591
    invoke-virtual {v4}, LX/186;->e()[B

    move-result-object v4

    invoke-static {v4}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 2338592
    invoke-virtual {v5, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2338593
    new-instance v4, LX/15i;

    move-object v7, v6

    move-object v9, v6

    invoke-direct/range {v4 .. v9}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2338594
    new-instance v5, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;

    invoke-direct {v5, v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;-><init>(LX/15i;)V

    .line 2338595
    move-object v0, v5

    .line 2338596
    invoke-virtual {p0, v0}, LX/GJK;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;)Landroid/text/Spanned;

    move-result-object v2

    .line 2338597
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {v0, v1, v2, v3}, LX/GJD;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    .line 2338598
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {v0, v3}, LX/GJD;->e(I)V

    .line 2338599
    iget v0, p0, LX/GJK;->s:I

    if-ne v0, v3, :cond_1

    .line 2338600
    invoke-static {p0}, LX/GJK;->B(LX/GJK;)V

    goto/16 :goto_0

    .line 2338601
    :cond_1
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {v0, v3}, LX/GJD;->f(I)V

    goto/16 :goto_0
.end method

.method public static B(LX/GJK;)V
    .locals 2

    .prologue
    .line 2338602
    iget-object v0, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, LX/GJK;->r:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2338603
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2338604
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2338605
    iget-object v1, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0, v1}, LX/GG3;->o(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2338606
    iget-object v0, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    iput-object v0, p0, LX/GJK;->r:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    .line 2338607
    :cond_0
    iget v0, p0, LX/GJK;->q:I

    iget-object v1, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 2338608
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2338609
    iget-object v1, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v1

    .line 2338610
    iget-object v1, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0, v1}, LX/GG3;->r(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2338611
    iget-object v0, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i()I

    move-result v0

    iput v0, p0, LX/GJK;->q:I

    .line 2338612
    :cond_1
    return-void
.end method

.method private C()I
    .locals 4

    .prologue
    .line 2338613
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->a()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2338614
    iget-object v0, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    .line 2338615
    iget-object v0, p0, LX/GJK;->m:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2338616
    iget-object v3, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j()I

    move-result v3

    if-ne v0, v3, :cond_0

    iget-object v0, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-static {v0}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-static {v2}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-nez v0, :cond_0

    .line 2338617
    :goto_1
    return v1

    .line 2338618
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2338619
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private D()Landroid/text/Spanned;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2338620
    iget-object v0, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j()I

    move-result v0

    .line 2338621
    iget-object v1, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    .line 2338622
    invoke-static {v1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v2

    iget-object v3, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->y()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v3

    invoke-static {v3}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v2

    if-nez v2, :cond_0

    .line 2338623
    iget-object v1, p0, LX/GJI;->b:Landroid/content/res/Resources;

    const v2, 0x7f0f004a

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 2338624
    :goto_0
    return-object v0

    .line 2338625
    :cond_0
    invoke-virtual {p0, v1}, LX/GJJ;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 2338626
    if-nez v0, :cond_1

    .line 2338627
    iget-object v0, p0, LX/GJI;->b:Landroid/content/res/Resources;

    const v2, 0x7f080b92

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v1, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0

    .line 2338628
    :cond_1
    iget-object v2, p0, LX/GJI;->b:Landroid/content/res/Resources;

    const v3, 0x7f0f0049

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v6

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0
.end method

.method public static E(LX/GJK;)I
    .locals 2

    .prologue
    .line 2338629
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {v0}, LX/GJD;->getSelectedIndex()I

    move-result v0

    .line 2338630
    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 2338631
    iget v0, p0, LX/GJK;->p:I

    .line 2338632
    :goto_0
    return v0

    .line 2338633
    :cond_0
    iget-object v1, p0, LX/GJK;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 2338634
    const/4 v0, 0x0

    goto :goto_0

    .line 2338635
    :cond_1
    iget-object v1, p0, LX/GJK;->m:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public static F(LX/GJK;)I
    .locals 2

    .prologue
    .line 2338636
    iget v0, p0, LX/GJK;->k:I

    invoke-static {p0}, LX/GJK;->E(LX/GJK;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private a(Ljava/lang/CharSequence;I)Landroid/text/Spanned;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2338637
    if-nez p2, :cond_0

    .line 2338638
    iget-object v0, p0, LX/GJI;->b:Landroid/content/res/Resources;

    const v1, 0x7f080b92

    new-array v2, v4, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 2338639
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/GJI;->b:Landroid/content/res/Resources;

    const v1, 0x7f0f0049

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 5
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2338640
    invoke-super {p0, p1, p2}, LX/GJJ;->a(Lcom/facebook/adinterfaces/ui/BudgetOptionsView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2338641
    iget-object v0, p0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setVisibility(I)V

    .line 2338642
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    .line 2338643
    iget-object v1, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->g:Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;

    move-object v1, v1

    .line 2338644
    iget-object v2, p0, LX/GJK;->n:LX/GNC;

    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 2338645
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v4, v0

    .line 2338646
    iget-object v0, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2, v3, v4, v0}, LX/GNC;->a(Landroid/content/Context;LX/GCE;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesFooterView;->setConfirmAdButtonListener(Landroid/view/View$OnClickListener;)V

    .line 2338647
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    new-instance v1, LX/GJE;

    invoke-direct {v1, p0}, LX/GJE;-><init>(LX/GJK;)V

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->setCustomDurationBudgetOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2338648
    invoke-direct {p0}, LX/GJK;->f()V

    .line 2338649
    return-void
.end method

.method public static c(LX/0QB;)LX/GJK;
    .locals 14

    .prologue
    .line 2338555
    new-instance v0, LX/GJK;

    invoke-static {p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v1

    check-cast v1, LX/GG6;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v2

    check-cast v2, LX/2U3;

    invoke-static {p0}, LX/10d;->b(LX/0QB;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    invoke-static {p0}, LX/GMU;->a(LX/0QB;)LX/GMU;

    move-result-object v4

    check-cast v4, LX/GMU;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {p0}, LX/GE2;->b(LX/0QB;)LX/GE2;

    move-result-object v6

    check-cast v6, LX/GE2;

    const-class v7, LX/GLN;

    invoke-interface {p0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/GLN;

    invoke-static {p0}, LX/0Sh;->a(LX/0QB;)LX/0Sh;

    move-result-object v8

    check-cast v8, LX/0Sh;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v9

    check-cast v9, LX/0tX;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v10

    check-cast v10, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p0}, LX/GK4;->a(LX/0QB;)LX/GK4;

    move-result-object v11

    check-cast v11, LX/GK4;

    invoke-static {p0}, LX/GNC;->a(LX/0QB;)LX/GNC;

    move-result-object v12

    check-cast v12, LX/GNC;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v13

    check-cast v13, LX/0ad;

    invoke-direct/range {v0 .. v13}, LX/GJK;-><init>(LX/GG6;LX/2U3;Landroid/view/inputmethod/InputMethodManager;LX/GMU;LX/1Ck;LX/GE2;LX/GLN;LX/0Sh;LX/0tX;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/GK4;LX/GNC;LX/0ad;)V

    .line 2338556
    return-object v0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 2338496
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2338497
    const/16 v1, 0x10

    new-instance v2, LX/GJF;

    invoke-direct {v2, p0}, LX/GJF;-><init>(LX/GJK;)V

    invoke-virtual {v0, v1, v2}, LX/GCE;->a(ILX/GFR;)V

    .line 2338498
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;)Landroid/text/Spanned;
    .locals 7

    .prologue
    .line 2338492
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v0

    .line 2338493
    if-nez v0, :cond_0

    .line 2338494
    iget-object v0, p0, LX/GJI;->b:Landroid/content/res/Resources;

    const v1, 0x7f080b94

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 2338495
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, LX/GJI;->b:Landroid/content/res/Resources;

    const v2, 0x7f080b8e

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, LX/GJK;->h:LX/GG6;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;->a()I

    move-result v6

    invoke-virtual {v5, v6}, LX/GG6;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, LX/GJK;->h:LX/GG6;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;->j()I

    move-result v0

    invoke-virtual {v5, v0}, LX/GG6;->a(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(LX/GGB;)V
    .locals 2

    .prologue
    .line 2338499
    iget-object v0, p0, LX/GJI;->q:Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;

    move-object v0, v0

    .line 2338500
    const v1, 0x7f080b8a

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setHeaderTitleResource(I)V

    .line 2338501
    return-void
.end method

.method public final bridge synthetic a(LX/GJD;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2338502
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-direct {p0, p1, p2}, LX/GJK;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2338503
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-direct {p0, p1, p2}, LX/GJK;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2338504
    check-cast p1, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {p0, p1}, LX/GJJ;->a(Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;)V
    .locals 1

    .prologue
    .line 2338505
    invoke-super {p0, p1}, LX/GJJ;->a(Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;)V

    .line 2338506
    iput-object p1, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    .line 2338507
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i()I

    move-result v0

    iput v0, p0, LX/GJK;->k:I

    .line 2338508
    const/4 v0, -0x1

    iput v0, p0, LX/GJK;->s:I

    .line 2338509
    iget-object v0, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i()I

    move-result v0

    iput v0, p0, LX/GJK;->q:I

    .line 2338510
    iget-object v0, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    iput-object v0, p0, LX/GJK;->r:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    .line 2338511
    return-void
.end method

.method public final bridge synthetic a(Lcom/facebook/adinterfaces/ui/BudgetOptionsView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2338512
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-direct {p0, p1, p2}, LX/GJK;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final b()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;
    .locals 2

    .prologue
    .line 2338513
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {v0}, LX/GJD;->getSelectedIndex()I

    move-result v0

    .line 2338514
    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    .line 2338515
    invoke-super {p0}, LX/GJJ;->b()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    .line 2338516
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, LX/GJK;->o:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    goto :goto_0
.end method

.method public final c()V
    .locals 10

    .prologue
    const/16 v1, 0x8

    .line 2338517
    iget-object v0, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2338518
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->setSummaryVisibility(I)V

    .line 2338519
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->setFooterViewVisibility(I)V

    .line 2338520
    iget-object v0, p0, LX/GJK;->j:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "No budget recommendation"

    invoke-virtual {v0, v1, v2}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2338521
    :goto_0
    return-void

    .line 2338522
    :cond_0
    iget-object v0, p0, LX/GJK;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2338523
    const/4 v0, 0x0

    move v3, v0

    :goto_1
    iget-object v0, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->a()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 2338524
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {v0, v3}, LX/GJD;->e(I)V

    .line 2338525
    iget-object v0, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;

    .line 2338526
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;

    move-result-object v4

    .line 2338527
    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    invoke-virtual {p0, v1}, LX/GJJ;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/lang/CharSequence;

    move-result-object v5

    .line 2338528
    if-nez v5, :cond_2

    .line 2338529
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {v0, v3}, LX/GJD;->d(I)V

    .line 2338530
    :cond_1
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 2338531
    :cond_2
    iget-object v1, p0, LX/GJK;->i:LX/GMU;

    iget-object v2, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->y()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v6

    invoke-virtual {v1, v2, v6}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 2338532
    iget-object v2, p0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    move-object v2, v2

    .line 2338533
    invoke-static {v2}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->v()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    invoke-static {v2}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 2338534
    invoke-virtual {v1}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v6

    invoke-virtual {v2}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v8

    div-long/2addr v6, v8

    long-to-int v1, v6

    .line 2338535
    mul-int/lit8 v2, v3, 0x2

    add-int/lit8 v2, v2, 0x1

    .line 2338536
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2338537
    iget-object v2, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v2

    sget-object v6, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-ne v2, v6, :cond_4

    .line 2338538
    iget-object v2, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->k()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    move v2, v1

    .line 2338539
    :goto_3
    iget-object v1, p0, LX/GJK;->m:Ljava/util/List;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2338540
    invoke-direct {p0, v5, v2}, LX/GJK;->a(Ljava/lang/CharSequence;I)Landroid/text/Spanned;

    move-result-object v5

    .line 2338541
    iget-object v1, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v1, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {p0, v4}, LX/GJK;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;)Landroid/text/Spanned;

    move-result-object v6

    invoke-virtual {v1, v5, v6, v3}, LX/GJD;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;I)V

    .line 2338542
    iget-object v1, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v1, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {p0, v4}, LX/GJJ;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;)Landroid/text/Spanned;

    move-result-object v5

    invoke-virtual {v1, v5, v3}, LX/GJD;->a(Ljava/lang/CharSequence;I)V

    .line 2338543
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2338544
    iget-object v0, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V

    .line 2338545
    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    iput-object v0, p0, LX/GJK;->r:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    .line 2338546
    iget-object v0, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    iget v1, p0, LX/GJK;->k:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a(I)V

    .line 2338547
    iget-object v0, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    .line 2338548
    iput v2, v0, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->k:I

    .line 2338549
    iget v0, p0, LX/GJK;->k:I

    add-int/2addr v0, v2

    iput v0, p0, LX/GJK;->q:I

    .line 2338550
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {v0, v3}, LX/GJD;->f(I)V

    .line 2338551
    iput v3, p0, LX/GJK;->s:I

    .line 2338552
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    iget-object v1, p0, LX/GJK;->l:LX/GK4;

    invoke-virtual {v1}, LX/GK4;->a()Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->a(Landroid/text/Spanned;)V

    .line 2338553
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    iget-object v1, p0, LX/GJK;->g:Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    check-cast v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iget-object v2, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v2, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v2, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v2, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v1, v4, v2}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;Landroid/content/res/Resources;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;->setSummaryText(Landroid/text/Spanned;)V

    goto/16 :goto_2

    .line 2338554
    :cond_3
    iget-object v0, p0, LX/GJI;->a:Lcom/facebook/adinterfaces/ui/BudgetOptionsView;

    check-cast v0, Lcom/facebook/adinterfaces/ui/AdInterfacesBudgetDurationView;

    new-instance v1, LX/GJG;

    invoke-direct {v1, p0}, LX/GJG;-><init>(LX/GJK;)V

    invoke-virtual {v0, v1}, LX/GJD;->setOnCheckChangedListener(LX/Bc9;)V

    goto/16 :goto_0

    :cond_4
    move v2, v1

    goto/16 :goto_3
.end method
