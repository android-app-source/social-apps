.class public LX/GDm;
.super LX/GDY;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GDY",
        "<",
        "LX/AAy;",
        "Lcom/facebook/adinterfaces/protocol/BoostedComponentEditMutationModels$BoostedComponentEditMutationModel;",
        ">;"
    }
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field private final b:LX/GF4;

.field public final c:LX/GG6;

.field public final d:LX/GG3;

.field public e:LX/GNL;

.field public f:Landroid/content/Context;

.field private g:Z


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/GF4;LX/GG6;LX/GNL;LX/GG3;LX/2U3;)V
    .locals 6
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2330373
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p6

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, LX/GDY;-><init>(LX/0tX;LX/1Ck;LX/GF4;LX/GG3;LX/2U3;)V

    .line 2330374
    iput-object p3, p0, LX/GDm;->b:LX/GF4;

    .line 2330375
    iput-object p4, p0, LX/GDm;->c:LX/GG6;

    .line 2330376
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GDm;->g:Z

    .line 2330377
    iput-object p6, p0, LX/GDm;->d:LX/GG3;

    .line 2330378
    iput-object p5, p0, LX/GDm;->e:LX/GNL;

    .line 2330379
    return-void
.end method

.method public static a(LX/0QB;)LX/GDm;
    .locals 11

    .prologue
    .line 2330380
    const-class v1, LX/GDm;

    monitor-enter v1

    .line 2330381
    :try_start_0
    sget-object v0, LX/GDm;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2330382
    sput-object v2, LX/GDm;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2330383
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2330384
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2330385
    new-instance v3, LX/GDm;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v5

    check-cast v5, LX/1Ck;

    invoke-static {v0}, LX/GF4;->a(LX/0QB;)LX/GF4;

    move-result-object v6

    check-cast v6, LX/GF4;

    invoke-static {v0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v7

    check-cast v7, LX/GG6;

    invoke-static {v0}, LX/GNL;->b(LX/0QB;)LX/GNL;

    move-result-object v8

    check-cast v8, LX/GNL;

    invoke-static {v0}, LX/GG3;->a(LX/0QB;)LX/GG3;

    move-result-object v9

    check-cast v9, LX/GG3;

    invoke-static {v0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v10

    check-cast v10, LX/2U3;

    invoke-direct/range {v3 .. v10}, LX/GDm;-><init>(LX/0tX;LX/1Ck;LX/GF4;LX/GG6;LX/GNL;LX/GG3;LX/2U3;)V

    .line 2330386
    move-object v0, v3

    .line 2330387
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2330388
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GDm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2330389
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2330390
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Z)LX/0zP;
    .locals 11

    .prologue
    .line 2330391
    new-instance v0, LX/AAy;

    invoke-direct {v0}, LX/AAy;-><init>()V

    move-object v0, v0

    .line 2330392
    const-string v1, "input"

    .line 2330393
    new-instance v3, LX/4DE;

    invoke-direct {v3}, LX/4DE;-><init>()V

    .line 2330394
    iget-object v4, p0, LX/GDm;->d:LX/GG3;

    .line 2330395
    iget-object v5, v4, LX/GG3;->g:Ljava/lang/String;

    move-object v4, v5

    .line 2330396
    const-string v5, "flow_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330397
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c()Ljava/lang/String;

    move-result-object v4

    .line 2330398
    const-string v5, "page_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330399
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->b()LX/8wL;

    move-result-object v4

    .line 2330400
    invoke-virtual {v4}, LX/8wL;->getComponentAppEnum()Ljava/lang/String;

    move-result-object v5

    move-object v4, v5

    .line 2330401
    const-string v5, "boosted_component_app"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330402
    iget-object v4, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->d:Lcom/facebook/adinterfaces/model/EventSpecModel;

    move-object v4, v4

    .line 2330403
    if-eqz v4, :cond_0

    .line 2330404
    iget-object v4, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->d:Lcom/facebook/adinterfaces/model/EventSpecModel;

    move-object v4, v4

    .line 2330405
    iget-object v5, v4, Lcom/facebook/adinterfaces/model/EventSpecModel;->a:Ljava/lang/String;

    move-object v4, v5

    .line 2330406
    const-string v5, "target_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330407
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 2330408
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->d()Ljava/lang/String;

    move-result-object v4

    .line 2330409
    const-string v5, "object_id"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330410
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 2330411
    iget-object v4, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v4, v4

    .line 2330412
    if-eqz v4, :cond_3

    .line 2330413
    iget-object v4, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v4, v4

    .line 2330414
    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->m()Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v4

    invoke-virtual {v4}, LX/GGB;->toString()Ljava/lang/String;

    move-result-object v4

    .line 2330415
    iget-object v5, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v5, v5

    .line 2330416
    invoke-virtual {v5}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->m()Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2330417
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v4

    invoke-virtual {v4}, LX/GGB;->name()Ljava/lang/String;

    move-result-object v4

    .line 2330418
    const-string v5, "status"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330419
    :cond_2
    :goto_0
    move-object v2, v3

    .line 2330420
    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/AAy;

    return-object v0

    .line 2330421
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v4

    if-eqz v4, :cond_4

    invoke-static {p1}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2330422
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 2330423
    const-string v5, "budget"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2330424
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i()I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_5

    iget-object v4, p0, LX/GDm;->c:LX/GG6;

    invoke-virtual {v4, p1}, LX/GG6;->c(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2330425
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j()I

    move-result v4

    if-lez v4, :cond_9

    .line 2330426
    iget-object v4, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v4, v4

    .line 2330427
    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->F()J

    move-result-wide v5

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->j()I

    move-result v4

    .line 2330428
    int-to-long v7, v4

    const-wide/32 v9, 0x15180

    mul-long/2addr v7, v9

    add-long/2addr v7, v5

    move-wide v5, v7

    .line 2330429
    long-to-int v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4DE;->a(Ljava/lang/Integer;)LX/4DE;

    .line 2330430
    :cond_5
    :goto_1
    iget-object v4, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v4, v4

    .line 2330431
    if-eqz v4, :cond_7

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2330432
    iget-object v6, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v7, v6

    .line 2330433
    iget-object v6, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v6, v6

    .line 2330434
    invoke-virtual {v6}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->q()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel;

    move-result-object v8

    .line 2330435
    if-nez v8, :cond_f

    .line 2330436
    if-eqz v7, :cond_e

    .line 2330437
    :cond_6
    :goto_2
    move v4, v4

    .line 2330438
    if-eqz v4, :cond_7

    .line 2330439
    iget-object v4, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v5, v4

    .line 2330440
    iget-object v4, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v4, v4

    .line 2330441
    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->q()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel;

    .line 2330442
    new-instance v4, LX/4Cf;

    invoke-direct {v4}, LX/4Cf;-><init>()V

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, LX/4Cf;->a(Ljava/lang/String;)LX/4Cf;

    move-result-object v4

    .line 2330443
    iget-object v6, v5, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-object v6, v6

    .line 2330444
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->LIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-eq v6, v7, :cond_14

    .line 2330445
    :goto_3
    move-object v4, v4

    .line 2330446
    const-string v5, "creative"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2330447
    :cond_7
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v4

    if-eqz v4, :cond_8

    const/4 v4, 0x0

    invoke-static {p1, v4}, LX/GG6;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Z)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 2330448
    new-instance v4, LX/4DB;

    invoke-direct {v4}, LX/4DB;-><init>()V

    .line 2330449
    iget-object v5, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->e:Ljava/lang/String;

    move-object v5, v5

    .line 2330450
    if-eqz v5, :cond_a

    .line 2330451
    iget-object v5, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->e:Ljava/lang/String;

    move-object v5, v5

    .line 2330452
    invoke-virtual {v4, v5}, LX/4DB;->b(Ljava/lang/String;)LX/4DB;

    .line 2330453
    :goto_4
    iget-object v5, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->f:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v5, v5

    .line 2330454
    if-eqz v5, :cond_c

    .line 2330455
    iget-object v5, p1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->f:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v5, v5

    .line 2330456
    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4DB;->a(Ljava/lang/String;)LX/4DB;

    .line 2330457
    :goto_5
    const-string v5, "audience"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;LX/0gS;)V

    .line 2330458
    :cond_8
    iget-object v4, p1, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->x:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    move-object v4, v4

    .line 2330459
    if-eqz v4, :cond_2

    .line 2330460
    iget v4, p1, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->w:I

    move v4, v4

    .line 2330461
    if-lez v4, :cond_d

    .line 2330462
    iget-object v4, p1, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->x:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    move-object v4, v4

    .line 2330463
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;->NO_PACING:Lcom/facebook/graphql/enums/GraphQLAdsApiPacingType;

    if-ne v4, v5, :cond_d

    .line 2330464
    iget v4, p1, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->w:I

    move v4, v4

    .line 2330465
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 2330466
    const-string v5, "bid_amount"

    invoke-virtual {v3, v5, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2330467
    const-string v4, "NO_PACING"

    invoke-virtual {v3, v4}, LX/4DE;->g(Ljava/lang/String;)LX/4DE;

    goto/16 :goto_0

    .line 2330468
    :cond_9
    iget-object v4, p0, LX/GDm;->c:LX/GG6;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->i()I

    move-result v5

    invoke-virtual {v4, v5}, LX/GG6;->c(I)J

    move-result-wide v5

    long-to-int v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/4DE;->a(Ljava/lang/Integer;)LX/4DE;

    goto/16 :goto_1

    .line 2330469
    :cond_a
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v5

    .line 2330470
    iget-object v6, v5, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v5, v6

    .line 2330471
    if-eqz v5, :cond_b

    .line 2330472
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v5

    .line 2330473
    iget-object v6, v5, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v5, v6

    .line 2330474
    invoke-virtual {v4, v5}, LX/4DB;->b(Ljava/lang/String;)LX/4DB;

    goto :goto_4

    .line 2330475
    :cond_b
    iget-object v5, p0, LX/GDm;->e:LX/GNL;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v6

    invoke-virtual {v5, v6}, LX/GNL;->a(Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;)LX/4Ch;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4DB;->a(LX/4Ch;)LX/4DB;

    goto :goto_4

    .line 2330476
    :cond_c
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v5

    .line 2330477
    iget-object v6, v5, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v5, v6

    .line 2330478
    invoke-virtual {v5}, Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;->name()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, LX/4DB;->a(Ljava/lang/String;)LX/4DB;

    goto :goto_5

    .line 2330479
    :cond_d
    const-string v4, "STANDARD"

    invoke-virtual {v3, v4}, LX/4DE;->g(Ljava/lang/String;)LX/4DE;

    goto/16 :goto_0

    :cond_e
    move v4, v5

    .line 2330480
    goto/16 :goto_2

    .line 2330481
    :cond_f
    if-eqz v7, :cond_6

    .line 2330482
    invoke-virtual {v8}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel;->k()LX/1vs;

    move-result-object v6

    iget v6, v6, LX/1vs;->b:I

    if-nez v6, :cond_10

    move v4, v5

    .line 2330483
    goto/16 :goto_2

    .line 2330484
    :cond_10
    iget-object v6, v7, Lcom/facebook/adinterfaces/model/CreativeAdModel;->k:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    move-object v6, v6

    .line 2330485
    sget-object v9, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->LIKE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    if-ne v6, v9, :cond_11

    .line 2330486
    iget-object v6, v7, Lcom/facebook/adinterfaces/model/CreativeAdModel;->c:Ljava/lang/String;

    move-object v6, v6

    .line 2330487
    invoke-virtual {v8}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_12

    move v6, v4

    :goto_6
    if-nez v6, :cond_6

    .line 2330488
    iget-object v6, v7, Lcom/facebook/adinterfaces/model/CreativeAdModel;->e:Ljava/lang/String;

    move-object v6, v6

    .line 2330489
    invoke-virtual {v8}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel;->l()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    :cond_11
    move v4, v5

    .line 2330490
    goto/16 :goto_2

    .line 2330491
    :cond_12
    invoke-virtual {v8}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentCreativeModel;->k()LX/1vs;

    move-result-object v6

    iget-object v9, v6, LX/1vs;->a:LX/15i;

    iget v6, v6, LX/1vs;->b:I

    .line 2330492
    iget-object v10, v7, Lcom/facebook/adinterfaces/model/CreativeAdModel;->f:Ljava/lang/String;

    move-object v10, v10

    .line 2330493
    invoke-virtual {v9, v6, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v10, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_13

    move v6, v4

    goto :goto_6

    :cond_13
    move v6, v5

    goto :goto_6

    .line 2330494
    :cond_14
    iget-object v6, v5, Lcom/facebook/adinterfaces/model/CreativeAdModel;->c:Ljava/lang/String;

    move-object v6, v6

    .line 2330495
    invoke-virtual {v4, v6}, LX/4Cf;->c(Ljava/lang/String;)LX/4Cf;

    .line 2330496
    iget-object v6, v5, Lcom/facebook/adinterfaces/model/CreativeAdModel;->f:Ljava/lang/String;

    move-object v5, v6

    .line 2330497
    invoke-virtual {v4, v5}, LX/4Cf;->d(Ljava/lang/String;)LX/4Cf;

    goto/16 :goto_3
.end method

.method public final a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2330498
    iput-object p2, p0, LX/GDm;->f:Landroid/content/Context;

    .line 2330499
    const v0, 0x7f080af2

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, LX/GDY;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;IZ)V

    .line 2330500
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GDm;->g:Z

    .line 2330501
    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Landroid/content/Context;LX/A8v;)V
    .locals 6

    .prologue
    .line 2330502
    iput-object p2, p0, LX/GDm;->f:Landroid/content/Context;

    .line 2330503
    const v4, 0x7f080af2

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    .line 2330504
    iget-object p0, v0, LX/GDY;->d:LX/GG3;

    invoke-virtual {p0, v3, v1}, LX/GG3;->a(LX/A8v;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2330505
    if-nez v5, :cond_0

    .line 2330506
    new-instance p0, LX/4At;

    invoke-virtual {v2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v2, p1}, LX/4At;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object p0, v0, LX/GDY;->a:LX/4At;

    .line 2330507
    iget-object p0, v0, LX/GDY;->a:LX/4At;

    invoke-virtual {p0}, LX/4At;->beginShowingProgress()V

    .line 2330508
    :cond_0
    iget-object p0, v0, LX/GDY;->c:LX/1Ck;

    sget-object p1, LX/GDX;->MUTATION_TASK:LX/GDX;

    iget-object p2, v0, LX/GDY;->b:LX/0tX;

    const/4 p3, 0x0

    invoke-virtual {v0, v1, p3}, LX/GDY;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;Z)LX/0zP;

    move-result-object p3

    invoke-static {p3}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object p3

    invoke-virtual {p2, p3}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object p2

    new-instance p3, LX/GDU;

    invoke-direct {p3, v0, v3, v1}, LX/GDU;-><init>(LX/GDY;LX/A8v;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)V

    invoke-virtual {p0, p1, p2, p3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2330509
    return-void
.end method

.method public final a(Lcom/facebook/graphql/executor/GraphQLResult;)V
    .locals 4
    .param p1    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/adinterfaces/protocol/BoostedComponentEditMutationModels$BoostedComponentEditMutationModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2330510
    iget-boolean v0, p0, LX/GDm;->g:Z

    if-eqz v0, :cond_0

    .line 2330511
    iget-object v0, p0, LX/GDm;->f:Landroid/content/Context;

    check-cast v0, Lcom/facebook/base/activity/FbFragmentActivity;

    .line 2330512
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/facebook/base/activity/FbFragmentActivity;->setResult(I)V

    .line 2330513
    invoke-virtual {v0}, Lcom/facebook/base/activity/FbFragmentActivity;->finish()V

    .line 2330514
    :goto_0
    return-void

    .line 2330515
    :cond_0
    iget-object v0, p0, LX/GDm;->b:LX/GF4;

    new-instance v1, LX/GFV;

    iget-object v2, p0, LX/GDY;->a:LX/4At;

    sget-object v3, LX/8wK;->PROMOTION_DETAILS:LX/8wK;

    invoke-direct {v1, v2, v3}, LX/GFV;-><init>(LX/4At;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2330516
    const/4 v0, 0x0

    return v0
.end method
