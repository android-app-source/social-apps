.class public LX/Gf6;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pk;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Gf7;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Gf6",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Gf7;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2376834
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2376835
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Gf6;->b:LX/0Zi;

    .line 2376836
    iput-object p1, p0, LX/Gf6;->a:LX/0Ot;

    .line 2376837
    return-void
.end method

.method public static a(LX/0QB;)LX/Gf6;
    .locals 4

    .prologue
    .line 2376823
    const-class v1, LX/Gf6;

    monitor-enter v1

    .line 2376824
    :try_start_0
    sget-object v0, LX/Gf6;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2376825
    sput-object v2, LX/Gf6;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2376826
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2376827
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2376828
    new-instance v3, LX/Gf6;

    const/16 p0, 0x210e

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Gf6;-><init>(LX/0Ot;)V

    .line 2376829
    move-object v0, v3

    .line 2376830
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2376831
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gf6;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2376832
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2376833
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 13

    .prologue
    .line 2376838
    check-cast p2, LX/Gf5;

    .line 2376839
    iget-object v0, p0, LX/Gf6;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gf7;

    iget-object v2, p2, LX/Gf5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p2, LX/Gf5;->b:LX/25E;

    iget-boolean v4, p2, LX/Gf5;->c:Z

    iget-boolean v5, p2, LX/Gf5;->d:Z

    move-object v1, p1

    const/4 v7, 0x0

    const/4 p2, 0x6

    const/4 p1, 0x2

    const/4 p0, 0x1

    const/4 v12, 0x0

    .line 2376840
    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, v12}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v8

    invoke-static {v1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6, p1}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v9

    invoke-static {v1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v10

    iget-object v11, v0, LX/Gf7;->a:LX/Gdi;

    .line 2376841
    iget-object v6, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v6, v6

    .line 2376842
    check-cast v6, Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    .line 2376843
    invoke-virtual {v11, v6, v3}, LX/Gdi;->b(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;LX/25E;)Landroid/text/Spannable;

    move-result-object v0

    .line 2376844
    if-eqz v0, :cond_2

    .line 2376845
    :goto_0
    move-object v6, v0

    .line 2376846
    invoke-virtual {v10, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v6

    invoke-virtual {v6, p0}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v6

    sget-object v10, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v6, v10}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v6

    const v10, 0x7f0b0050

    invoke-virtual {v6, v10}, LX/1ne;->q(I)LX/1ne;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-interface {v6, v10}, LX/1Di;->a(F)LX/1Di;

    move-result-object v6

    invoke-interface {v6, p0, p2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v6

    const/4 v10, 0x3

    invoke-interface {v6, v10, p2}, LX/1Di;->h(II)LX/1Di;

    move-result-object v6

    invoke-interface {v9, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v9

    if-eqz v4, :cond_0

    :goto_1
    invoke-interface {v9, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-interface {v8, v6}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-static {v1}, LX/25N;->c(LX/1De;)LX/25Q;

    move-result-object v7

    const v8, 0x7f0a0460

    invoke-virtual {v7, v8}, LX/25Q;->i(I)LX/25Q;

    move-result-object v7

    invoke-virtual {v7}, LX/1X5;->c()LX/1Di;

    move-result-object v7

    const v8, 0x7f0b0033

    invoke-interface {v7, v8}, LX/1Di;->q(I)LX/1Di;

    move-result-object v7

    invoke-interface {v6, v7}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v6

    invoke-interface {v6}, LX/1Di;->k()LX/1Dg;

    move-result-object v6

    move-object v0, v6

    .line 2376847
    return-object v0

    :cond_0
    invoke-static {v1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v6

    const v10, 0x7f020aea

    invoke-virtual {v6, v10}, LX/1o5;->h(I)LX/1o5;

    move-result-object v6

    invoke-virtual {v6}, LX/1X5;->c()LX/1Di;

    move-result-object v6

    const/16 v10, 0xd

    invoke-interface {v6, v10}, LX/1Di;->j(I)LX/1Di;

    move-result-object v6

    const/16 v10, 0xe

    invoke-interface {v6, v10}, LX/1Di;->r(I)LX/1Di;

    move-result-object v6

    const/16 v10, 0x8

    const/16 v11, 0xc

    invoke-interface {v6, v10, v11}, LX/1Di;->n(II)LX/1Di;

    move-result-object v6

    const v10, 0x7f0b00eb

    invoke-interface {v6, v12, v10}, LX/1Di;->c(II)LX/1Di;

    move-result-object v6

    invoke-interface {v6, p1}, LX/1Di;->b(I)LX/1Di;

    move-result-object v10

    if-eqz v5, :cond_1

    move-object v6, v7

    :goto_2
    invoke-interface {v10, v6}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v7

    goto :goto_1

    .line 2376848
    :cond_1
    const v6, -0x238fcfd3

    const/4 v7, 0x0

    invoke-static {v1, v6, v7}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 2376849
    goto :goto_2

    .line 2376850
    :cond_2
    invoke-interface {v3}, LX/25E;->s()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 2376851
    if-eqz v0, :cond_3

    .line 2376852
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLTextWithEntities;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 2376853
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2376813
    invoke-static {}, LX/1dS;->b()V

    .line 2376814
    iget v0, p1, LX/1dQ;->b:I

    .line 2376815
    packed-switch v0, :pswitch_data_0

    .line 2376816
    :goto_0
    return-object v2

    .line 2376817
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2376818
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2376819
    check-cast v1, LX/Gf5;

    .line 2376820
    iget-object p1, p0, LX/Gf6;->a:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    iget-object p1, v1, LX/Gf5;->e:LX/1Pk;

    iget-object p2, v1, LX/Gf5;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2376821
    invoke-interface {p1}, LX/1Pk;->e()LX/1SX;

    move-result-object p0

    invoke-virtual {p0, p2, v0}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 2376822
    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0x238fcfd3
        :pswitch_0
    .end packed-switch
.end method

.method public final c(LX/1De;)LX/Gf4;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/Gf6",
            "<TE;>.Builder;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2376805
    new-instance v1, LX/Gf5;

    invoke-direct {v1, p0}, LX/Gf5;-><init>(LX/Gf6;)V

    .line 2376806
    iget-object v2, p0, LX/Gf6;->b:LX/0Zi;

    invoke-virtual {v2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Gf4;

    .line 2376807
    if-nez v2, :cond_0

    .line 2376808
    new-instance v2, LX/Gf4;

    invoke-direct {v2, p0}, LX/Gf4;-><init>(LX/Gf6;)V

    .line 2376809
    :cond_0
    invoke-static {v2, p1, v0, v0, v1}, LX/Gf4;->a$redex0(LX/Gf4;LX/1De;IILX/Gf5;)V

    .line 2376810
    move-object v1, v2

    .line 2376811
    move-object v0, v1

    .line 2376812
    return-object v0
.end method
