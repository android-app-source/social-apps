.class public LX/FVS;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation build Ljavax/annotation/concurrent/NotThreadSafe;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static j:LX/0Xm;


# instance fields
.field public b:LX/62l;

.field public c:I

.field public d:I

.field public e:Z

.field public f:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/79Y;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/FWh;

.field public final h:LX/03V;

.field public i:LX/FVp;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2250805
    const-class v0, LX/FVS;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/FVS;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/FWh;LX/03V;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x2

    .line 2250798
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2250799
    iput v0, p0, LX/FVS;->c:I

    .line 2250800
    iput v0, p0, LX/FVS;->d:I

    .line 2250801
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/FVS;->e:Z

    .line 2250802
    iput-object p1, p0, LX/FVS;->g:LX/FWh;

    .line 2250803
    iput-object p2, p0, LX/FVS;->h:LX/03V;

    .line 2250804
    return-void
.end method

.method public static a(LX/0QB;)LX/FVS;
    .locals 5

    .prologue
    .line 2250787
    const-class v1, LX/FVS;

    monitor-enter v1

    .line 2250788
    :try_start_0
    sget-object v0, LX/FVS;->j:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2250789
    sput-object v2, LX/FVS;->j:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2250790
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2250791
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2250792
    new-instance p0, LX/FVS;

    invoke-static {v0}, LX/FWh;->a(LX/0QB;)LX/FWh;

    move-result-object v3

    check-cast v3, LX/FWh;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v4

    check-cast v4, LX/03V;

    invoke-direct {p0, v3, v4}, LX/FVS;-><init>(LX/FWh;LX/03V;)V

    .line 2250793
    move-object v0, p0

    .line 2250794
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2250795
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FVS;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2250796
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2250797
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static j(LX/FVS;)V
    .locals 2

    .prologue
    .line 2250806
    iget-object v0, p0, LX/FVS;->b:LX/62l;

    const v1, 0x7f081abe

    invoke-virtual {v0, v1}, LX/62l;->b(I)V

    .line 2250807
    iget-object v0, p0, LX/FVS;->g:LX/FWh;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/FWh;->a(Z)V

    .line 2250808
    return-void
.end method

.method public static k(LX/FVS;)V
    .locals 1

    .prologue
    .line 2250785
    iget-object v0, p0, LX/FVS;->b:LX/62l;

    invoke-virtual {v0}, LX/62l;->f()V

    .line 2250786
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Long;LX/0am;)V
    .locals 9
    .param p1    # Ljava/lang/Long;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Long;",
            "LX/0am",
            "<",
            "LX/79Y;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2250768
    iput-object p2, p0, LX/FVS;->f:LX/0am;

    .line 2250769
    iget-boolean v0, p0, LX/FVS;->e:Z

    if-nez v0, :cond_0

    .line 2250770
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/FVS;->e:Z

    .line 2250771
    if-nez p1, :cond_1

    .line 2250772
    iget-object v0, p0, LX/FVS;->g:LX/FWh;

    .line 2250773
    const/4 v4, 0x0

    iget-wide v6, v0, LX/FWh;->d:J

    invoke-static {v0, v4, v6, v7}, LX/FWh;->a(LX/FWh;ZJ)V

    .line 2250774
    iget-wide v4, v0, LX/FWh;->d:J

    invoke-static {v0, v4, v5}, LX/FWh;->b(LX/FWh;J)V

    .line 2250775
    :cond_0
    :goto_0
    iget-object v0, p0, LX/FVS;->b:LX/62l;

    invoke-virtual {v0}, LX/62l;->d()V

    .line 2250776
    return-void

    .line 2250777
    :cond_1
    iget-object v0, p0, LX/FVS;->g:LX/FWh;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 2250778
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v0, LX/FWh;->e:Ljava/lang/Long;

    .line 2250779
    iget-object v4, v0, LX/FWh;->b:LX/0So;

    invoke-interface {v4}, LX/0So;->now()J

    move-result-wide v4

    sub-long/2addr v4, v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v0, LX/FWh;->f:Ljava/lang/Long;

    .line 2250780
    const/4 v4, 0x1

    iget-object v5, v0, LX/FWh;->f:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v0, v4, v6, v7}, LX/FWh;->a(LX/FWh;ZJ)V

    .line 2250781
    const-string v4, "SAVED_EARLY_FETCH_LOAD"

    iget-object v5, v0, LX/FWh;->f:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v0, v4, v6, v7}, LX/FWh;->a(LX/FWh;Ljava/lang/String;J)V

    .line 2250782
    const-string v4, "SAVED_EARLY_FETCH_LOAD"

    const-string v5, "SAVED_DASH_START_EARLY_FETCH_ADVANTAGE"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, LX/0Rh;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rh;

    move-result-object v5

    invoke-static {v0, v4, v5}, LX/FWh;->a(LX/FWh;Ljava/lang/String;LX/0P1;)V

    .line 2250783
    iget-wide v4, v0, LX/FWh;->d:J

    invoke-static {v0, v4, v5}, LX/FWh;->b(LX/FWh;J)V

    .line 2250784
    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 2250761
    iget-object v0, p0, LX/FVS;->g:LX/FWh;

    .line 2250762
    invoke-static {v0}, LX/FWh;->n(LX/FWh;)LX/11o;

    move-result-object v1

    .line 2250763
    if-eqz v1, :cond_0

    const-string v2, "SAVED_CACHED_ITEM_LOAD"

    invoke-interface {v1, v2}, LX/11o;->f(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2250764
    const-string v1, "SAVED_CACHED_ITEM_LOAD"

    invoke-static {v0, v1}, LX/FWh;->b(LX/FWh;Ljava/lang/String;)V

    .line 2250765
    const-string v1, "SAVED_DASH_START_TO_CACHE_ITEM_DRAWN"

    invoke-static {v0, v1}, LX/FWh;->b(LX/FWh;Ljava/lang/String;)V

    .line 2250766
    :cond_0
    const/4 v0, 0x2

    iput v0, p0, LX/FVS;->c:I

    .line 2250767
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 2250754
    iget-object v0, p0, LX/FVS;->g:LX/FWh;

    .line 2250755
    const-string v2, "SAVED_CACHED_ITEM_LOAD"

    invoke-static {v0, v2}, LX/FWh;->c(LX/FWh;Ljava/lang/String;)V

    .line 2250756
    const-string v2, "SAVED_DASH_START_TO_CACHE_ITEM_DRAWN"

    invoke-static {v0, v2}, LX/FWh;->c(LX/FWh;Ljava/lang/String;)V

    .line 2250757
    iget v0, p0, LX/FVS;->d:I

    if-ne v0, v1, :cond_0

    .line 2250758
    invoke-static {p0}, LX/FVS;->j(LX/FVS;)V

    .line 2250759
    :cond_0
    iput v1, p0, LX/FVS;->c:I

    .line 2250760
    return-void
.end method
