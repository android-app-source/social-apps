.class public final LX/H4H;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesLocationSearchQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TF;

.field public final synthetic b:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;

.field public final synthetic c:LX/H4I;


# direct methods
.method public constructor <init>(LX/H4I;LX/0TF;Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;)V
    .locals 0

    .prologue
    .line 2422521
    iput-object p1, p0, LX/H4H;->c:LX/H4I;

    iput-object p2, p0, LX/H4H;->a:LX/0TF;

    iput-object p3, p0, LX/H4H;->b:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2422522
    iget-object v0, p0, LX/H4H;->a:LX/0TF;

    if-eqz v0, :cond_0

    .line 2422523
    iget-object v0, p0, LX/H4H;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 2422524
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2422525
    check-cast p1, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesLocationSearchQueryModel;

    .line 2422526
    iget-object v0, p0, LX/H4H;->a:LX/0TF;

    if-eqz v0, :cond_2

    .line 2422527
    new-instance v2, LX/0Pz;

    invoke-direct {v2}, LX/0Pz;-><init>()V

    .line 2422528
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesLocationSearchQueryModel;->a()Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesLocationSearchQueryModel$LocationsModel;

    move-result-object v0

    .line 2422529
    if-eqz v0, :cond_1

    .line 2422530
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesLocationSearchQueryModel$LocationsModel;->a()LX/0Px;

    move-result-object v3

    .line 2422531
    invoke-virtual {v3}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2422532
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 2422533
    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel;

    .line 2422534
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel;->a()Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 2422535
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel;->a()Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2422536
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2422537
    :cond_1
    new-instance v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    iget-object v1, p0, LX/H4H;->b:Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;

    .line 2422538
    iget-object v3, v1, Lcom/facebook/nearby/v2/typeahead/NearbyPlacesTypeaheadParams;->c:Ljava/lang/String;

    move-object v1, v3

    .line 2422539
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesLocationSearchQueryModel;->a()Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesLocationSearchQueryModel$LocationsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesLocationSearchQueryModel$LocationsModel;->j()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;-><init>(Ljava/lang/String;LX/0Px;Ljava/lang/String;)V

    .line 2422540
    iget-object v1, p0, LX/H4H;->a:LX/0TF;

    invoke-interface {v1, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    .line 2422541
    :cond_2
    return-void
.end method
