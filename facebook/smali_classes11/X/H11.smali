.class public final LX/H11;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Landroid/content/ComponentName;

.field public final synthetic b:Lcom/facebook/mobileconfig/ui/MainFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/mobileconfig/ui/MainFragment;Landroid/content/ComponentName;)V
    .locals 0

    .prologue
    .line 2414698
    iput-object p1, p0, LX/H11;->b:Lcom/facebook/mobileconfig/ui/MainFragment;

    iput-object p2, p0, LX/H11;->a:Landroid/content/ComponentName;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const v0, 0x755ad953

    invoke-static {v5, v4, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2414699
    iget-object v0, p0, LX/H11;->b:Lcom/facebook/mobileconfig/ui/MainFragment;

    iget-object v0, v0, Lcom/facebook/mobileconfig/ui/MainFragment;->f:Landroid/content/Context;

    check-cast v0, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;

    .line 2414700
    check-cast p1, Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {p1}, Lcom/facebook/resources/ui/FbCheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2414701
    iget-object v2, p0, LX/H11;->b:Lcom/facebook/mobileconfig/ui/MainFragment;

    iget-object v2, v2, Lcom/facebook/mobileconfig/ui/MainFragment;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, LX/H11;->a:Landroid/content/ComponentName;

    invoke-virtual {v2, v3, v4, v4}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 2414702
    const-string v2, "Enabled MobileConfig from internal settings but MobileConfig may still be disabled for some other reasons (e.g. GK)."

    invoke-virtual {v0, v2}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->c(Ljava/lang/CharSequence;)V

    .line 2414703
    :goto_0
    const v0, -0x4cbc5eb4

    invoke-static {v0, v1}, LX/02F;->a(II)V

    return-void

    .line 2414704
    :cond_0
    iget-object v2, p0, LX/H11;->b:Lcom/facebook/mobileconfig/ui/MainFragment;

    iget-object v2, v2, Lcom/facebook/mobileconfig/ui/MainFragment;->f:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, LX/H11;->a:Landroid/content/ComponentName;

    invoke-virtual {v2, v3, v5, v4}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 2414705
    const-string v2, "Disabled MobileConfig from internal settings."

    invoke-virtual {v0, v2}, Lcom/facebook/mobileconfig/ui/MobileConfigPreferenceActivity;->c(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
