.class public final LX/Fol;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V
    .locals 0

    .prologue
    .line 2290108
    iput-object p1, p0, LX/Fol;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x2

    const v1, 0x10f0fb1b

    invoke-static {v0, v2, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2290109
    iget-object v1, p0, LX/Fol;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    invoke-virtual {v1}, Lcom/facebook/fig/textinput/FigEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2290110
    iget-object v1, p0, LX/Fol;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-static {v1}, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->s(Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;)V

    .line 2290111
    iget-object v1, p0, LX/Fol;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    .line 2290112
    iput-boolean v2, v1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->d:Z

    .line 2290113
    iget-object v1, p0, LX/Fol;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    iget-object v1, v1, Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;->m:Lcom/facebook/fig/textinput/FigEditText;

    const-string v2, ""

    invoke-virtual {v1, v2}, Lcom/facebook/fig/textinput/FigEditText;->setText(Ljava/lang/CharSequence;)V

    .line 2290114
    :goto_0
    const v1, -0x6fc38f9a

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2290115
    :cond_0
    iget-object v1, p0, LX/Fol;->a:Lcom/facebook/socialgood/ui/FundraiserCreationCharitySearchFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method
