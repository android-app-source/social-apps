.class public final LX/F1V;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/F0H;


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;Z)V
    .locals 0

    .prologue
    .line 2192073
    iput-object p1, p0, LX/F1V;->b:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    iput-boolean p2, p0, LX/F1V;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2192062
    iget-object v0, p0, LX/F1V;->b:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    iget-object v1, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->g:LX/F0J;

    iget-boolean v0, p0, LX/F1V;->a:Z

    if-eqz v0, :cond_2

    sget-object v0, LX/F0I;->STATUS_SUBSCRIBED_ALL:LX/F0I;

    .line 2192063
    :goto_0
    iput-object v0, v1, LX/F0J;->g:LX/F0I;

    .line 2192064
    iget-object v0, p0, LX/F1V;->b:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->t(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)LX/1Qq;

    move-result-object v0

    invoke-interface {v0}, LX/1Qq;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 2192065
    iget-object v0, p0, LX/F1V;->b:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    iget-object v0, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->a:LX/F08;

    invoke-virtual {v0}, LX/F08;->f()V

    .line 2192066
    :cond_0
    iget-object v0, p0, LX/F1V;->b:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    iget-object v0, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->i:LX/1Cn;

    sget-object v1, LX/CF0;->THROWBACK_FEED_MENU:LX/CF0;

    iget-boolean v2, p0, LX/F1V;->a:Z

    invoke-virtual {v0, v1, v2}, LX/1Cn;->a(LX/CF0;Z)V

    .line 2192067
    iget-object v0, p0, LX/F1V;->b:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/F1V;->b:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    iget-object v0, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->g:LX/F0J;

    invoke-virtual {v0}, LX/F0J;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2192068
    iget-object v0, p0, LX/F1V;->b:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    iget-object v0, v0, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->g:LX/F0J;

    invoke-virtual {v0}, LX/F0J;->b()V

    .line 2192069
    :cond_1
    return-void

    .line 2192070
    :cond_2
    sget-object v0, LX/F0I;->STATUS_UNSUBSCRIBED:LX/F0I;

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 2192071
    iget-object v0, p0, LX/F1V;->b:Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;

    invoke-static {v0}, Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;->k(Lcom/facebook/goodwill/feed/ui/ThrowbackFeedFragment;)V

    .line 2192072
    return-void
.end method
