.class public LX/GEu;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GEW",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/GKy;

.field private final b:LX/GK3;


# direct methods
.method public constructor <init>(LX/GKy;LX/GK3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2332151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2332152
    iput-object p1, p0, LX/GEu;->a:LX/GKy;

    .line 2332153
    iput-object p2, p0, LX/GEu;->b:LX/GK3;

    .line 2332154
    return-void
.end method

.method public static a(LX/0QB;)LX/GEu;
    .locals 3

    .prologue
    .line 2332155
    new-instance v2, LX/GEu;

    invoke-static {p0}, LX/GKy;->b(LX/0QB;)LX/GKy;

    move-result-object v0

    check-cast v0, LX/GKy;

    invoke-static {p0}, LX/GK3;->b(LX/0QB;)LX/GK3;

    move-result-object v1

    check-cast v1, LX/GK3;

    invoke-direct {v2, v0, v1}, LX/GEu;-><init>(LX/GKy;LX/GK3;)V

    .line 2332156
    move-object v0, v2

    .line 2332157
    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2332158
    const v0, 0x7f030069

    return v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2332159
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->A()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    .line 2332160
    :goto_0
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v3

    .line 2332161
    sget-object v4, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    if-eq v3, v4, :cond_1

    sget-object v4, LX/GGB;->INACTIVE:LX/GGB;

    if-eq v3, v4, :cond_1

    sget-object v4, LX/GGB;->ERROR:LX/GGB;

    if-eq v3, v4, :cond_1

    sget-object v4, LX/GGB;->REJECTED:LX/GGB;

    if-eq v3, v4, :cond_1

    iget-object v3, p0, LX/GEu;->a:LX/GKy;

    invoke-virtual {v3}, LX/GKy;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    move v3, v1

    .line 2332162
    :goto_1
    if-nez v3, :cond_2

    if-nez v0, :cond_2

    :goto_2
    return v1

    :cond_0
    move v0, v2

    .line 2332163
    goto :goto_0

    :cond_1
    move v3, v2

    .line 2332164
    goto :goto_1

    :cond_2
    move v1, v2

    .line 2332165
    goto :goto_2
.end method

.method public final b()LX/GHg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesViewController",
            "<",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;",
            "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2332166
    iget-object v0, p0, LX/GEu;->b:LX/GK3;

    return-object v0
.end method

.method public final c()LX/8wK;
    .locals 1

    .prologue
    .line 2332167
    sget-object v0, LX/8wK;->INSIGHTS_SUMMARY:LX/8wK;

    return-object v0
.end method
