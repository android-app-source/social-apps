.class public final LX/GOt;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;

.field public final synthetic b:LX/GOu;


# direct methods
.method public constructor <init>(LX/GOu;Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;)V
    .locals 0

    .prologue
    .line 2348037
    iput-object p1, p0, LX/GOt;->b:LX/GOu;

    iput-object p2, p0, LX/GOt;->a:Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, -0x5cc2449a

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2348031
    iget-object v1, p0, LX/GOt;->b:LX/GOu;

    iget-object v1, v1, LX/GOu;->a:Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;

    invoke-virtual {v1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->v()Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    move-result-object v1

    .line 2348032
    iget-object v2, v1, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;->c:LX/ADX;

    move-object v1, v2

    .line 2348033
    invoke-virtual {v1}, LX/ADX;->isNUX()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2348034
    iget-object v1, p0, LX/GOt;->b:LX/GOu;

    iget-object v1, v1, LX/GOu;->a:Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;

    new-instance v2, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity$8$1$1;

    invoke-direct {v2, p0}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity$8$1$1;-><init>(LX/GOt;)V

    iget-object v3, p0, LX/GOt;->a:Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;

    invoke-static {v1, v2, v3}, LX/GQ5;->a(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;Ljava/lang/Runnable;Lcom/facebook/payments/paymentmethods/model/PaymentOption;)V

    .line 2348035
    :goto_0
    const v1, -0x6e85b260

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2348036
    :cond_0
    iget-object v1, p0, LX/GOt;->b:LX/GOu;

    iget-object v1, v1, LX/GOu;->a:Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;

    iget-object v2, p0, LX/GOt;->a:Lcom/facebook/payments/paymentmethods/model/AltpayPaymentOption;

    invoke-static {v1, v2}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->b(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;Lcom/facebook/payments/paymentmethods/model/PaymentOption;)V

    goto :goto_0
.end method
