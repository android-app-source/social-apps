.class public LX/Gqv;
.super LX/Cr6;
.source ""


# instance fields
.field public a:Z

.field private b:I


# direct methods
.method public constructor <init>(LX/Ctg;LX/CrK;)V
    .locals 0

    .prologue
    .line 2397048
    invoke-direct {p0, p1, p2}, LX/Cr6;-><init>(LX/Ctg;LX/CrK;)V

    .line 2397049
    return-void
.end method


# virtual methods
.method public k()Z
    .locals 1

    .prologue
    .line 2397059
    const/4 v0, 0x0

    return v0
.end method

.method public final l()V
    .locals 3

    .prologue
    .line 2397050
    invoke-virtual {p0}, LX/Cqj;->n()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2397051
    instance-of v1, v0, Landroid/support/v7/widget/RecyclerView;

    if-nez v1, :cond_0

    .line 2397052
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2397053
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-virtual {p0, v1, v2}, LX/Cqi;->a(II)V

    .line 2397054
    iget v1, p0, LX/Gqv;->b:I

    if-gtz v1, :cond_1

    .line 2397055
    iget-boolean v1, p0, LX/Gqv;->a:Z

    if-eqz v1, :cond_2

    invoke-virtual {p0}, LX/Cqj;->n()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    :goto_0
    iput v1, p0, LX/Gqv;->b:I

    .line 2397056
    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iget v1, p0, LX/Gqv;->b:I

    invoke-virtual {p0, v0, v1}, LX/Cqi;->b(II)V

    .line 2397057
    return-void

    .line 2397058
    :cond_2
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v1

    goto :goto_0
.end method
