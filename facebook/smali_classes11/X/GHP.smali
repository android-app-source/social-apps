.class public LX/GHP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GGc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GGc",
        "<",
        "Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final i:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/GEW",
            "<*-",
            "Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;",
            ">;>;"
        }
    .end annotation
.end field

.field private final j:LX/GE8;

.field public final k:LX/0ad;


# direct methods
.method public constructor <init>(LX/GE8;LX/GEw;LX/GF0;LX/GMK;LX/GLH;LX/GIA;LX/GEY;LX/GK8;LX/GJC;LX/GMR;LX/GJi;LX/GJ2;LX/GEb;LX/0ad;LX/GKQ;)V
    .locals 7
    .param p2    # LX/GEw;
        .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostedComponent;
        .end annotation
    .end param
    .param p15    # LX/GKQ;
        .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostedComponent;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2334992
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2334993
    iput-object p1, p0, LX/GHP;->j:LX/GE8;

    .line 2334994
    move-object/from16 v0, p14

    iput-object v0, p0, LX/GHP;->k:LX/0ad;

    .line 2334995
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    invoke-virtual {v1, p3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030067

    sget-object v4, LX/GHP;->a:LX/GGX;

    sget-object v5, LX/8wK;->INFO_CARD:LX/8wK;

    move-object/from16 v0, p12

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03005a

    sget-object v4, LX/GHP;->a:LX/GGX;

    sget-object v5, LX/8wK;->ERROR_CARD:LX/8wK;

    move-object/from16 v0, p11

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03008c

    sget-object v4, LX/GHP;->g:LX/GGX;

    sget-object v5, LX/8wK;->TARGETING:LX/8wK;

    invoke-direct {v2, v3, p8, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03006e

    sget-object v4, LX/GHP;->e:LX/GGX;

    sget-object v5, LX/8wK;->AD_PREVIEW:LX/8wK;

    move-object/from16 v0, p15

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03004e

    sget-object v4, LX/GHP;->g:LX/GGX;

    sget-object v5, LX/8wK;->BUDGET:LX/8wK;

    invoke-direct {v2, v3, p4, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030082

    sget-object v4, LX/GHP;->g:LX/GGX;

    sget-object v5, LX/8wK;->DURATION:LX/8wK;

    invoke-direct {v2, v3, p5, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, p7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03003d

    sget-object v4, LX/GHP;->a:LX/GGX;

    sget-object v5, LX/8wK;->ACCOUNT:LX/8wK;

    invoke-direct {v2, v3, p6, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    move-object/from16 v0, p13

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030077

    sget-object v4, LX/GHP;->d:LX/GGX;

    sget-object v5, LX/8wK;->PROMOTION_DETAILS:LX/8wK;

    move-object/from16 v0, p10

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030087

    const/4 v4, 0x0

    sget-object v5, LX/GHP;->a:LX/GGX;

    sget-object v6, LX/8wK;->SPACER:LX/8wK;

    invoke-direct {v2, v3, v4, v5, v6}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03005c

    sget-object v4, LX/GHP;->c:LX/GGX;

    sget-object v5, LX/8wK;->FOOTER:LX/8wK;

    move-object/from16 v0, p9

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, p0, LX/GHP;->i:LX/0Px;

    .line 2334996
    return-void
.end method

.method public static b(LX/0QB;)LX/GHP;
    .locals 16

    .prologue
    .line 2334990
    new-instance v0, LX/GHP;

    invoke-static/range {p0 .. p0}, LX/GE8;->a(LX/0QB;)LX/GE8;

    move-result-object v1

    check-cast v1, LX/GE8;

    invoke-static/range {p0 .. p0}, LX/GDH;->a(LX/0QB;)LX/GEw;

    move-result-object v2

    check-cast v2, LX/GEw;

    invoke-static/range {p0 .. p0}, LX/GF0;->a(LX/0QB;)LX/GF0;

    move-result-object v3

    check-cast v3, LX/GF0;

    invoke-static/range {p0 .. p0}, LX/GMK;->a(LX/0QB;)LX/GMK;

    move-result-object v4

    check-cast v4, LX/GMK;

    invoke-static/range {p0 .. p0}, LX/GLH;->a(LX/0QB;)LX/GLH;

    move-result-object v5

    check-cast v5, LX/GLH;

    invoke-static/range {p0 .. p0}, LX/GIA;->a(LX/0QB;)LX/GIA;

    move-result-object v6

    check-cast v6, LX/GIA;

    invoke-static/range {p0 .. p0}, LX/GEY;->a(LX/0QB;)LX/GEY;

    move-result-object v7

    check-cast v7, LX/GEY;

    invoke-static/range {p0 .. p0}, LX/GK8;->b(LX/0QB;)LX/GK8;

    move-result-object v8

    check-cast v8, LX/GK8;

    invoke-static/range {p0 .. p0}, LX/GJC;->a(LX/0QB;)LX/GJC;

    move-result-object v9

    check-cast v9, LX/GJC;

    invoke-static/range {p0 .. p0}, LX/GMR;->a(LX/0QB;)LX/GMR;

    move-result-object v10

    check-cast v10, LX/GMR;

    invoke-static/range {p0 .. p0}, LX/GJi;->b(LX/0QB;)LX/GJi;

    move-result-object v11

    check-cast v11, LX/GJi;

    invoke-static/range {p0 .. p0}, LX/GJ2;->b(LX/0QB;)LX/GJ2;

    move-result-object v12

    check-cast v12, LX/GJ2;

    invoke-static/range {p0 .. p0}, LX/GEb;->a(LX/0QB;)LX/GEb;

    move-result-object v13

    check-cast v13, LX/GEb;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v14

    check-cast v14, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/GCJ;->a(LX/0QB;)LX/GKQ;

    move-result-object v15

    check-cast v15, LX/GKQ;

    invoke-direct/range {v0 .. v15}, LX/GHP;-><init>(LX/GE8;LX/GEw;LX/GF0;LX/GMK;LX/GLH;LX/GIA;LX/GEY;LX/GK8;LX/GJC;LX/GMR;LX/GJi;LX/GJ2;LX/GEb;LX/0ad;LX/GKQ;)V

    .line 2334991
    return-object v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/GEW",
            "<*-",
            "Lcom/facebook/adinterfaces/model/localawareness/AdInterfacesLocalAwarenessDataModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2335001
    iget-object v0, p0, LX/GHP;->i:LX/0Px;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;LX/GCY;)V
    .locals 7

    .prologue
    .line 2334997
    iget-object v0, p0, LX/GHP;->k:LX/0ad;

    sget-short v1, LX/GDK;->z:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    move v5, v0

    .line 2334998
    iget-object v0, p0, LX/GHP;->j:LX/GE8;

    const-string v1, "page_id"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/8wL;->LOCAL_AWARENESS:LX/8wL;

    invoke-static {p1}, LX/8wJ;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/GHO;

    invoke-direct {v4, p0, p2}, LX/GHO;-><init>(LX/GHP;LX/GCY;)V

    if-nez v5, :cond_0

    const-string v5, "1"

    const-string v6, "restore_saved_settings"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    const/4 v5, 0x1

    :goto_0
    invoke-virtual/range {v0 .. v5}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->a(Ljava/lang/String;LX/8wL;Ljava/lang/String;LX/GCY;Z)V

    .line 2334999
    return-void

    .line 2335000
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method
