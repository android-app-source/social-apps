.class public final enum LX/Gyn;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gyn;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gyn;

.field public static final enum LOAD_HISTORY_SETTING:LX/Gyn;

.field public static final enum SAVE_HISTORY_SETTING:LX/Gyn;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2410370
    new-instance v0, LX/Gyn;

    const-string v1, "LOAD_HISTORY_SETTING"

    invoke-direct {v0, v1, v2}, LX/Gyn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gyn;->LOAD_HISTORY_SETTING:LX/Gyn;

    .line 2410371
    new-instance v0, LX/Gyn;

    const-string v1, "SAVE_HISTORY_SETTING"

    invoke-direct {v0, v1, v3}, LX/Gyn;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gyn;->SAVE_HISTORY_SETTING:LX/Gyn;

    .line 2410372
    const/4 v0, 0x2

    new-array v0, v0, [LX/Gyn;

    sget-object v1, LX/Gyn;->LOAD_HISTORY_SETTING:LX/Gyn;

    aput-object v1, v0, v2

    sget-object v1, LX/Gyn;->SAVE_HISTORY_SETTING:LX/Gyn;

    aput-object v1, v0, v3

    sput-object v0, LX/Gyn;->$VALUES:[LX/Gyn;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2410367
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gyn;
    .locals 1

    .prologue
    .line 2410369
    const-class v0, LX/Gyn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gyn;

    return-object v0
.end method

.method public static values()[LX/Gyn;
    .locals 1

    .prologue
    .line 2410368
    sget-object v0, LX/Gyn;->$VALUES:[LX/Gyn;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gyn;

    return-object v0
.end method
