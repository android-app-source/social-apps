.class public LX/HDQ;
.super LX/1a1;
.source ""


# instance fields
.field public final l:Landroid/content/Context;

.field private final m:LX/23P;

.field private final n:LX/HDK;

.field private final o:LX/0wM;

.field private final p:LX/HDI;

.field public final q:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field private final r:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;"
        }
    .end annotation
.end field

.field private final s:Lcom/facebook/resources/ui/FbTextView;

.field private final t:Lcom/facebook/resources/ui/FbTextView;

.field private final u:Lcom/facebook/resources/ui/FbTextView;

.field private final v:Landroid/widget/LinearLayout;

.field private final w:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLPageActionType;",
            "LX/HDF;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/23P;LX/HDK;LX/0wM;LX/HDI;LX/0Ot;LX/0Ot;Landroid/view/View;)V
    .locals 2
    .param p7    # Landroid/view/View;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/23P;",
            "LX/HDK;",
            "LX/0wM;",
            "LX/HDI;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2441170
    invoke-direct {p0, p7}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2441171
    invoke-virtual {p7}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, LX/HDQ;->l:Landroid/content/Context;

    .line 2441172
    iput-object p1, p0, LX/HDQ;->m:LX/23P;

    .line 2441173
    iput-object p2, p0, LX/HDQ;->n:LX/HDK;

    .line 2441174
    iput-object p3, p0, LX/HDQ;->o:LX/0wM;

    .line 2441175
    iput-object p4, p0, LX/HDQ;->p:LX/HDI;

    .line 2441176
    iput-object p5, p0, LX/HDQ;->q:LX/0Ot;

    .line 2441177
    iput-object p6, p0, LX/HDQ;->r:LX/0Ot;

    .line 2441178
    const v0, 0x7f0d0d28

    invoke-virtual {p7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/HDQ;->s:Lcom/facebook/resources/ui/FbTextView;

    .line 2441179
    iget-object v0, p0, LX/HDQ;->s:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/HDQ;->m:LX/23P;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2441180
    const v0, 0x7f0d0d29

    invoke-virtual {p7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/HDQ;->t:Lcom/facebook/resources/ui/FbTextView;

    .line 2441181
    iget-object v0, p0, LX/HDQ;->t:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/HDQ;->m:LX/23P;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2441182
    const v0, 0x7f0d0d2a

    invoke-virtual {p7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/HDQ;->u:Lcom/facebook/resources/ui/FbTextView;

    .line 2441183
    iget-object v0, p0, LX/HDQ;->u:Lcom/facebook/resources/ui/FbTextView;

    iget-object v1, p0, LX/HDQ;->m:LX/23P;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbTextView;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    .line 2441184
    const v0, 0x7f0d0d2b

    invoke-virtual {p7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/HDQ;->v:Landroid/widget/LinearLayout;

    .line 2441185
    iget-object v0, p0, LX/HDQ;->p:LX/HDI;

    .line 2441186
    iget-object v1, v0, LX/HDI;->a:LX/0P1;

    move-object v0, v1

    .line 2441187
    iput-object v0, p0, LX/HDQ;->w:LX/0P1;

    .line 2441188
    return-void
.end method

.method private static a(LX/HDQ;Ljava/lang/String;ILjava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;
    .locals 4
    .param p1    # Ljava/lang/String;
        .annotation build Landroid/support/annotation/DrawableRes;
        .end annotation
    .end param

    .prologue
    .line 2441189
    iget-object v0, p0, LX/HDQ;->l:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030457

    iget-object v2, p0, LX/HDQ;->v:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    .line 2441190
    if-eqz p2, :cond_0

    .line 2441191
    iget-object v1, p0, LX/HDQ;->o:LX/0wM;

    iget-object v2, p0, LX/HDQ;->l:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a00a6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, p2, v2}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2441192
    :cond_0
    invoke-static {v0}, LX/8FY;->a(Landroid/view/View;)V

    .line 2441193
    invoke-virtual {v0, p1}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2441194
    if-nez p4, :cond_1

    .line 2441195
    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setAlpha(F)V

    .line 2441196
    new-instance v1, LX/HDP;

    invoke-direct {v1, p0}, LX/HDP;-><init>(LX/HDQ;)V

    invoke-virtual {v0, v1}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2441197
    :goto_0
    return-object v0

    .line 2441198
    :cond_1
    invoke-virtual {v0, p4}, Lcom/facebook/fig/listitem/FigListItem;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2441199
    invoke-virtual {v0, p3}, Lcom/facebook/fig/listitem/FigListItem;->setActionText(Ljava/lang/CharSequence;)V

    .line 2441200
    invoke-virtual {v0, p4}, Lcom/facebook/fig/listitem/FigListItem;->setActionOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private static a(LX/HDQ;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2441201
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2441202
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2441203
    :goto_0
    return-object v0

    .line 2441204
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->p()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->p()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2441205
    invoke-virtual {p1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->p()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PagePreviewOnlyActionDataModel$PreviewTextModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2441206
    :cond_1
    iget-object v0, p0, LX/HDQ;->l:Landroid/content/Context;

    const v1, 0x7f0836be

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a$redex0(LX/HDQ;JLcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;Lcom/facebook/graphql/enums/GraphQLPageActionType;)V
    .locals 7

    .prologue
    .line 2441207
    iget-object v0, p0, LX/HDQ;->r:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9XE;

    sget-object v1, LX/9X7;->EDIT_TAP_TAB_ACTION:LX/9X7;

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, LX/9XE;->a(LX/9X7;JLcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;Lcom/facebook/graphql/enums/GraphQLPageActionType;)V

    .line 2441208
    return-void
.end method


# virtual methods
.method public final a(JLcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;Lcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V
    .locals 17
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "bindData"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    .line 2441209
    invoke-virtual/range {p3 .. p3}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;->c()Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;

    move-result-object v2

    .line 2441210
    invoke-interface/range {p3 .. p3}, LX/HCg;->a()Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;

    move-result-object v5

    .line 2441211
    const/4 v3, 0x0

    .line 2441212
    if-eqz v2, :cond_8

    .line 2441213
    invoke-virtual {v2}, Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$EditPageDataModel$AddableTabsChannelModel;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v2, 0x0

    move v4, v2

    :goto_0
    if-ge v4, v7, :cond_8

    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    .line 2441214
    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v8

    .line 2441215
    if-eqz v8, :cond_2

    .line 2441216
    move-object/from16 v0, p0

    iget-object v2, v0, LX/HDQ;->w:LX/0P1;

    invoke-virtual {v8}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v9

    invoke-virtual {v2, v9}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, LX/HDQ;->w:LX/0P1;

    invoke-virtual {v8}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v9

    invoke-virtual {v2, v9}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/HDF;

    .line 2441217
    :goto_1
    if-eqz v2, :cond_2

    invoke-virtual {v8}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    move-result-object v9

    if-eqz v9, :cond_2

    invoke-virtual {v8}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->j()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenProfileTabActionDataModel$NamePropercaseModel;->a()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_2

    invoke-interface {v2}, LX/HDE;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2441218
    const/4 v2, 0x1

    .line 2441219
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, LX/HDQ;->s:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v2, :cond_3

    const/4 v2, 0x0

    :goto_3
    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2441220
    move-object/from16 v0, p0

    iget-object v2, v0, LX/HDQ;->s:Lcom/facebook/resources/ui/FbTextView;

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2441221
    move-object/from16 v0, p0

    iget-object v3, v0, LX/HDQ;->t:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v5}, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;->b()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    :goto_4
    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2441222
    move-object/from16 v0, p0

    iget-object v2, v0, LX/HDQ;->t:Lcom/facebook/resources/ui/FbTextView;

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2441223
    move-object/from16 v0, p0

    iget-object v2, v0, LX/HDQ;->u:Lcom/facebook/resources/ui/FbTextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2441224
    move-object/from16 v0, p0

    iget-object v2, v0, LX/HDQ;->u:Lcom/facebook/resources/ui/FbTextView;

    move-object/from16 v0, p7

    invoke-virtual {v2, v0}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2441225
    move-object/from16 v0, p0

    iget-object v2, v0, LX/HDQ;->v:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 2441226
    invoke-virtual {v5}, Lcom/facebook/pages/common/surface/tabs/edit/graphql/PageReorderTabQueryModels$PageReorderTabDataModel$ProfileTabNavigationEditChannelModel;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result v12

    const/4 v2, 0x0

    move v10, v2

    :goto_5
    if-ge v10, v12, :cond_7

    invoke-virtual {v11, v10}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;

    .line 2441227
    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageOpenActionEditActionDataModel;->a()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;

    move-result-object v7

    .line 2441228
    if-eqz v7, :cond_0

    .line 2441229
    move-object/from16 v0, p0

    iget-object v2, v0, LX/HDQ;->w:LX/0P1;

    invoke-virtual {v7}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, LX/HDQ;->w:LX/0P1;

    invoke-virtual {v7}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;->b()Lcom/facebook/graphql/enums/GraphQLPageActionType;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/HDF;

    move-object v8, v2

    .line 2441230
    :goto_6
    if-eqz v8, :cond_0

    invoke-interface {v8}, LX/HDF;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2441231
    move-object/from16 v0, p0

    iget-object v13, v0, LX/HDQ;->v:Landroid/widget/LinearLayout;

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/HDQ;->a(LX/HDQ;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)Ljava/lang/String;

    move-result-object v14

    invoke-interface {v8}, LX/HDE;->d()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v2, v0, LX/HDQ;->l:Landroid/content/Context;

    invoke-interface {v8}, LX/HDE;->e()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    invoke-static {v7}, LX/HQA;->a(LX/9Y7;)Z

    move-result v2

    if-eqz v2, :cond_6

    new-instance v2, LX/HDO;

    move-object/from16 v3, p0

    move-wide/from16 v4, p1

    move-object/from16 v6, p4

    move-object/from16 v9, p3

    invoke-direct/range {v2 .. v9}, LX/HDO;-><init>(LX/HDQ;JLcom/facebook/graphql/enums/GraphQLPagesSurfaceTemplateType;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;LX/HDF;Lcom/facebook/pages/common/editpage/graphql/FetchEditPageQueryModels$FetchEditPageQueryModel;)V

    :goto_7
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-static {v0, v14, v15, v1, v2}, LX/HDQ;->a(LX/HDQ;Ljava/lang/String;ILjava/lang/String;Landroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v13, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2441232
    :cond_0
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_5

    .line 2441233
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/HDQ;->n:LX/HDK;

    invoke-virtual {v2, v8}, LX/HDK;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)LX/HDJ;

    move-result-object v2

    goto/16 :goto_1

    .line 2441234
    :cond_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_0

    .line 2441235
    :cond_3
    const/16 v2, 0x8

    goto/16 :goto_3

    .line 2441236
    :cond_4
    const/16 v2, 0x8

    goto/16 :goto_4

    .line 2441237
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, LX/HDQ;->n:LX/HDK;

    invoke-virtual {v2, v7}, LX/HDK;->a(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel;)LX/HDJ;

    move-result-object v8

    goto :goto_6

    .line 2441238
    :cond_6
    const/4 v2, 0x0

    goto :goto_7

    .line 2441239
    :cond_7
    return-void

    :cond_8
    move v2, v3

    goto/16 :goto_2
.end method
