.class public final LX/GYb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:LX/GYd;


# direct methods
.method public constructor <init>(LX/GYd;)V
    .locals 0

    .prologue
    .line 2365248
    iput-object p1, p0, LX/GYb;->a:LX/GYd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    .line 2365249
    iget-object v0, p0, LX/GYb;->a:LX/GYd;

    iget-object v0, v0, LX/GYd;->a:Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;

    .line 2365250
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->S:LX/4At;

    invoke-virtual {v1}, LX/4At;->beginShowingProgress()V

    .line 2365251
    iget-object v1, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->g:LX/1Ck;

    sget-object v2, LX/GYj;->DELETE_PRODUCT_ITEM:LX/GYj;

    iget-object v3, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->d:LX/7kY;

    iget-object v4, v0, Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;->x:LX/7ja;

    invoke-interface {v4}, LX/7ja;->e()Ljava/lang/String;

    move-result-object v4

    .line 2365252
    iget-object v5, v3, LX/7kY;->a:LX/0tX;

    invoke-static {v4}, LX/7jZ;->a(Ljava/lang/String;)LX/4Ia;

    move-result-object p0

    .line 2365253
    new-instance p1, LX/7jP;

    invoke-direct {p1}, LX/7jP;-><init>()V

    move-object p1, p1

    .line 2365254
    const-string p2, "input"

    invoke-virtual {p1, p2, p0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object p1

    check-cast p1, LX/7jP;

    invoke-static {p1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object p1

    move-object p0, p1

    .line 2365255
    invoke-virtual {v5, p0}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 2365256
    new-instance p0, LX/7kV;

    invoke-direct {p0, v3, v4}, LX/7kV;-><init>(LX/7kY;Ljava/lang/String;)V

    invoke-static {v5, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;)V

    .line 2365257
    move-object v3, v5

    .line 2365258
    new-instance v4, LX/GYe;

    invoke-direct {v4, v0}, LX/GYe;-><init>(Lcom/facebook/commerce/publishing/fragments/adminproduct/AdminProductFragment;)V

    invoke-virtual {v1, v2, v3, v4}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2365259
    return-void
.end method
