.class public final LX/H9k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/BND;


# instance fields
.field public final synthetic a:LX/4BY;

.field public final synthetic b:LX/CSL;

.field public final synthetic c:LX/H9l;


# direct methods
.method public constructor <init>(LX/H9l;LX/4BY;LX/CSL;)V
    .locals 0

    .prologue
    .line 2434582
    iput-object p1, p0, LX/H9k;->c:LX/H9l;

    iput-object p2, p0, LX/H9k;->a:LX/4BY;

    iput-object p3, p0, LX/H9k;->b:LX/CSL;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2434579
    iget-object v0, p0, LX/H9k;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2434580
    iget-object v0, p0, LX/H9k;->c:LX/H9l;

    iget-object v0, v0, LX/H9l;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8W;

    const v1, 0x7f081502

    invoke-virtual {v0, v1}, LX/H8W;->a(I)V

    .line 2434581
    return-void
.end method

.method public final a(LX/55r;)V
    .locals 14

    .prologue
    const/4 v5, 0x0

    .line 2434574
    iget-object v0, p0, LX/H9k;->a:LX/4BY;

    invoke-virtual {v0}, LX/4BY;->dismiss()V

    .line 2434575
    if-nez p1, :cond_0

    .line 2434576
    iget-object v0, p0, LX/H9k;->c:LX/H9l;

    iget-object v0, v0, LX/H9l;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    iget-object v1, p0, LX/H9k;->b:LX/CSL;

    iget-object v2, p0, LX/H9k;->c:LX/H9l;

    iget-object v2, v2, LX/H9l;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    iget-object v4, p0, LX/H9k;->c:LX/H9l;

    iget-object v4, v4, LX/H9l;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->s()Ljava/lang/String;

    move-result-object v4

    const-string v6, "actionbar_button"

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, LX/CSL;->a(JLjava/lang/String;Lcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/lang/String;I)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    const/16 v3, 0x277b

    iget-object v1, p0, LX/H9k;->c:LX/H9l;

    iget-object v1, v1, LX/H9l;->g:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-interface {v0, v5, v2, v3, v1}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    .line 2434577
    :goto_0
    return-void

    .line 2434578
    :cond_0
    iget-object v0, p0, LX/H9k;->c:LX/H9l;

    iget-object v0, v0, LX/H9l;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1Kf;

    iget-object v6, p0, LX/H9k;->b:LX/CSL;

    iget-object v1, p0, LX/H9k;->c:LX/H9l;

    iget-object v1, v1, LX/H9l;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->s()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1}, LX/BNJ;->a(LX/55r;)I

    move-result v8

    invoke-static {p1}, LX/BNJ;->b(LX/55r;)Ljava/lang/String;

    move-result-object v9

    iget-object v1, p0, LX/H9k;->c:LX/H9l;

    iget-object v1, v1, LX/H9l;->f:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-static {p1}, LX/BNJ;->c(LX/55r;)Lcom/facebook/graphql/model/GraphQLPrivacyOption;

    move-result-object v12

    const-string v13, "actionbar_button"

    invoke-virtual/range {v6 .. v13}, LX/CSL;->a(Ljava/lang/String;ILjava/lang/String;JLcom/facebook/graphql/model/GraphQLPrivacyOption;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    const/16 v3, 0x277c

    iget-object v1, p0, LX/H9k;->c:LX/H9l;

    iget-object v1, v1, LX/H9l;->g:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-interface {v0, v5, v2, v3, v1}, LX/1Kf;->a(Ljava/lang/String;Lcom/facebook/ipc/composer/intent/ComposerConfiguration;ILandroid/app/Activity;)V

    goto :goto_0
.end method
