.class public LX/GK4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field public b:LX/GMm;

.field public c:Landroid/text/Spanned;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/GMm;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2340261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2340262
    iput-object p1, p0, LX/GK4;->a:Landroid/content/Context;

    .line 2340263
    iput-object p2, p0, LX/GK4;->b:LX/GMm;

    .line 2340264
    return-void
.end method

.method public static a(LX/0QB;)LX/GK4;
    .locals 5

    .prologue
    .line 2340265
    const-class v1, LX/GK4;

    monitor-enter v1

    .line 2340266
    :try_start_0
    sget-object v0, LX/GK4;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2340267
    sput-object v2, LX/GK4;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2340268
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2340269
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2340270
    new-instance p0, LX/GK4;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/GMm;->a(LX/0QB;)LX/GMm;

    move-result-object v4

    check-cast v4, LX/GMm;

    invoke-direct {p0, v3, v4}, LX/GK4;-><init>(Landroid/content/Context;LX/GMm;)V

    .line 2340271
    move-object v0, p0

    .line 2340272
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2340273
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GK4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2340274
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2340275
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Landroid/text/Spanned;
    .locals 9

    .prologue
    .line 2340276
    iget-object v0, p0, LX/GK4;->c:Landroid/text/Spanned;

    if-nez v0, :cond_0

    .line 2340277
    const/16 v8, 0x21

    .line 2340278
    iget-object v0, p0, LX/GK4;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2340279
    const v1, 0x7f080a8d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2340280
    const v2, 0x7f080a8e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2340281
    const v3, 0x7f080a8f

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2340282
    const v4, 0x7f0a0124

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 2340283
    new-instance v5, LX/47x;

    invoke-direct {v5, v0}, LX/47x;-><init>(Landroid/content/res/Resources;)V

    .line 2340284
    invoke-virtual {v5, v1}, LX/47x;->a(Ljava/lang/CharSequence;)LX/47x;

    .line 2340285
    const-string v0, "[[advertising_guidelines_link]]"

    iget-object v1, p0, LX/GK4;->b:LX/GMm;

    const-string v6, "https://m.facebook.com/ad_guidelines.php"

    iget-object v7, p0, LX/GK4;->a:Landroid/content/Context;

    invoke-virtual {v1, v6, v4, v7}, LX/GMm;->a(Ljava/lang/String;ILandroid/content/Context;)Landroid/text/style/ClickableSpan;

    move-result-object v1

    invoke-virtual {v5, v0, v3, v1, v8}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2340286
    const-string v1, "[[facebook_terms_link]]"

    iget-object v0, p0, LX/GK4;->b:LX/GMm;

    .line 2340287
    sget-object v3, LX/GMm;->c:LX/01T;

    sget-object v6, LX/01T;->FB4A:LX/01T;

    if-ne v3, v6, :cond_2

    iget-object v3, v0, LX/GMm;->e:LX/16I;

    invoke-virtual {v3}, LX/16I;->a()Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v3, 0x1

    :goto_0
    move v0, v3

    .line 2340288
    if-eqz v0, :cond_1

    iget-object v0, p0, LX/GK4;->b:LX/GMm;

    iget-object v3, p0, LX/GK4;->a:Landroid/content/Context;

    .line 2340289
    new-instance v6, LX/GMl;

    invoke-direct {v6, v0, v3, v4}, LX/GMl;-><init>(LX/GMm;Landroid/content/Context;I)V

    move-object v0, v6

    .line 2340290
    :goto_1
    invoke-virtual {v5, v1, v2, v0, v8}, LX/47x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;I)LX/47x;

    .line 2340291
    invoke-virtual {v5}, LX/47x;->b()Landroid/text/SpannableString;

    move-result-object v0

    iput-object v0, p0, LX/GK4;->c:Landroid/text/Spanned;

    .line 2340292
    :cond_0
    iget-object v0, p0, LX/GK4;->c:Landroid/text/Spanned;

    return-object v0

    .line 2340293
    :cond_1
    iget-object v0, p0, LX/GK4;->b:LX/GMm;

    const-string v3, "https://m.facebook.com/legal/terms"

    iget-object v6, p0, LX/GK4;->a:Landroid/content/Context;

    invoke-virtual {v0, v3, v4, v6}, LX/GMm;->a(Ljava/lang/String;ILandroid/content/Context;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method
