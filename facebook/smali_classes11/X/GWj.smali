.class public LX/GWj;
.super Lcom/facebook/widget/CustomFrameLayout;
.source ""

# interfaces
.implements LX/DaM;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/facebook/widget/CustomFrameLayout;",
        "LX/DaM",
        "<",
        "Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;

.field public b:LX/GMm;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2362685
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;-><init>(Landroid/content/Context;)V

    .line 2362686
    const-class p1, LX/GWj;

    invoke-static {p1, p0}, LX/GWj;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2362687
    const p1, 0x7f031033

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->setContentView(I)V

    .line 2362688
    const p1, 0x7f0d26d9

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomFrameLayout;->c(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;

    iput-object p1, p0, LX/GWj;->a:Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;

    .line 2362689
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/GWj;

    invoke-static {p0}, LX/GMm;->a(LX/0QB;)LX/GMm;

    move-result-object p0

    check-cast p0, LX/GMm;

    iput-object p0, p1, LX/GWj;->b:LX/GMm;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2362690
    check-cast p1, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    const v5, -0x23642ae7

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2362691
    iget-object v0, p0, LX/GWj;->a:Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;

    invoke-virtual {v0, v2}, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->setVisibility(I)V

    .line 2362692
    iget-object v0, p0, LX/GWj;->a:Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;

    invoke-virtual {p0}, LX/GWj;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0837ca

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->setFirstDataLabel(Ljava/lang/String;)V

    .line 2362693
    iget-object v0, p0, LX/GWj;->a:Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;

    invoke-virtual {p0}, LX/GWj;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0837c9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->setSecondDataLabel(Ljava/lang/String;)V

    .line 2362694
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->t()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->t()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2362695
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->u()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2362696
    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    if-eqz v0, :cond_7

    .line 2362697
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->u()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2362698
    if-eqz v0, :cond_5

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, LX/39O;->a()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_2
    if-eqz v0, :cond_1

    .line 2362699
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->u()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v3, v0, v2, v5}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_3
    invoke-virtual {v0, v2}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2362700
    const-class v4, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    invoke-virtual {v3, v0, v2, v4, v5}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    .line 2362701
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    if-eq v0, v3, :cond_0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->CREATING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    if-eq v0, v3, :cond_0

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->PENDING:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    if-ne v0, v3, :cond_1

    .line 2362702
    :cond_0
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;->ACTIVE:Lcom/facebook/graphql/enums/GraphQLBoostedComponentStatus;

    if-ne v0, v3, :cond_9

    .line 2362703
    invoke-virtual {p0}, LX/GWj;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0837c8

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 2362704
    :goto_4
    move-object v0, v3

    .line 2362705
    iget-object v3, p0, LX/GWj;->a:Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;

    new-instance v4, LX/GWi;

    invoke-direct {v4, p0, p1}, LX/GWi;-><init>(LX/GWj;Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)V

    invoke-virtual {v3, v0, v4}, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->a(Ljava/lang/String;Landroid/view/View$OnClickListener;)V

    .line 2362706
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->k()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    if-eqz v0, :cond_2

    .line 2362707
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2362708
    invoke-virtual {v3, v0, v1}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 2362709
    const/4 v4, 0x2

    invoke-virtual {v3, v0, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 2362710
    iget-object v3, p0, LX/GWj;->a:Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;

    add-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->setFirstDataValue(Ljava/lang/String;)V

    .line 2362711
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->k()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    iget-object v3, p0, LX/GWj;->a:Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/facebook/commerce/productdetails/ui/insightsandpromotion/InsightsAndPromotionView;->setSecondDataValue(Ljava/lang/String;)V

    .line 2362712
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 2362713
    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto/16 :goto_0

    :cond_5
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_1

    :cond_6
    move v0, v2

    goto/16 :goto_2

    :cond_7
    move v0, v2

    goto/16 :goto_2

    .line 2362714
    :cond_8
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto/16 :goto_3

    .line 2362715
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_9
    invoke-virtual {p0}, LX/GWj;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0837c7

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_4
.end method
