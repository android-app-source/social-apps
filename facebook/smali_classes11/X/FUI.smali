.class public final LX/FUI;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FUH;


# instance fields
.field public final synthetic a:Ljava/io/File;

.field public final synthetic b:Lcom/facebook/qrcode/QRCodeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/qrcode/QRCodeFragment;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 2248087
    iput-object p1, p0, LX/FUI;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iput-object p2, p0, LX/FUI;->a:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 2248088
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2248089
    iget-object v1, p0, LX/FUI;->a:Ljava/io/File;

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 2248090
    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 2248091
    iget-object v1, p0, LX/FUI;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v1, v1, Lcom/facebook/qrcode/QRCodeFragment;->e:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2248092
    iget-object v0, p0, LX/FUI;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->v:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0832dd

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2248093
    iget-object v0, p0, LX/FUI;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v1, v0, Lcom/facebook/qrcode/QRCodeFragment;->p:LX/FUW;

    iget-object v0, p0, LX/FUI;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->M:LX/FUV;

    sget-object v2, LX/FUV;->VANITY:LX/FUV;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    .line 2248094
    :goto_0
    const-string v2, "qrcode_saved"

    const-string v3, "vanity"

    invoke-static {v1, v2, v3, v0}, LX/FUW;->a(LX/FUW;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 2248095
    iget-object v0, p0, LX/FUI;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    .line 2248096
    iget-object v1, v0, LX/FUf;->a:LX/0if;

    sget-object v2, LX/0ig;->H:LX/0ih;

    const-string v3, "MY_CODE_SAVE_SUCCESS"

    invoke-virtual {v1, v2, v3}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2248097
    return-void

    .line 2248098
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Exception;)V
    .locals 5

    .prologue
    .line 2248099
    iget-object v0, p0, LX/FUI;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->v:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0832e6

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2248100
    iget-object v0, p0, LX/FUI;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    .line 2248101
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v1

    const-string v2, "msg"

    invoke-virtual {p1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v1

    .line 2248102
    iget-object v2, v0, LX/FUf;->a:LX/0if;

    sget-object v3, LX/0ig;->H:LX/0ih;

    const-string v4, "EXCEPTION_ON_SAVE"

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v2, v3, v4, p0, v1}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2248103
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 2248104
    iget-object v0, p0, LX/FUI;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->v:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f0832e3

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2248105
    iget-object v0, p0, LX/FUI;->b:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    .line 2248106
    iget-object v1, v0, LX/FUf;->a:LX/0if;

    sget-object v2, LX/0ig;->H:LX/0ih;

    const-string p0, "ERROR_ON_SAVE"

    invoke-virtual {v1, v2, p0}, LX/0if;->b(LX/0ih;Ljava/lang/String;)V

    .line 2248107
    return-void
.end method
