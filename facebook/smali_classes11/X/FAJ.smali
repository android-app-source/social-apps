.class public final LX/FAJ;
.super Ljava/lang/RuntimeException;
.source ""


# instance fields
.field private final mHost:Ljava/lang/String;

.field private final mPort:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 2206893
    const-string v0, "Strict socket created for %s:%d"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, p1, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    .line 2206894
    iput-object p1, p0, LX/FAJ;->mHost:Ljava/lang/String;

    .line 2206895
    iput p2, p0, LX/FAJ;->mPort:I

    .line 2206896
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 2206897
    const-string v0, "StrictSocketException[%s:%d]"

    iget-object v1, p0, LX/FAJ;->mHost:Ljava/lang/String;

    iget v2, p0, LX/FAJ;->mPort:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
