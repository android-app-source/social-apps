.class public final LX/FSb;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FSc;


# direct methods
.method public constructor <init>(LX/FSc;)V
    .locals 0

    .prologue
    .line 2242296
    iput-object p1, p0, LX/FSb;->a:LX/FSc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2242293
    iget-object v0, p0, LX/FSb;->a:LX/FSc;

    iget-object v0, v0, LX/FSc;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2242294
    sget-object v0, LX/FSc;->a:Ljava/lang/String;

    const-string v1, "NewsFeedPrefetcher reports failure."

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2242295
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2242291
    iget-object v0, p0, LX/FSb;->a:LX/FSc;

    iget-object v0, v0, LX/FSc;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2242292
    return-void
.end method
