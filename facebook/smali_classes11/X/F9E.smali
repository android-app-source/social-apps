.class public final LX/F9E;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/io/File;

.field public final synthetic b:LX/4At;

.field public final synthetic c:Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;Ljava/io/File;LX/4At;)V
    .locals 0

    .prologue
    .line 2205164
    iput-object p1, p0, LX/F9E;->c:Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;

    iput-object p2, p0, LX/F9E;->a:Ljava/io/File;

    iput-object p3, p0, LX/F9E;->b:LX/4At;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 2205176
    iget-object v0, p0, LX/F9E;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 2205177
    iget-object v0, p0, LX/F9E;->b:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2205178
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2205179
    invoke-direct {p0}, LX/F9E;->b()V

    .line 2205180
    iget-object v0, p0, LX/F9E;->c:Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;

    iget-object v0, v0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->n:LX/F8y;

    if-eqz v0, :cond_0

    .line 2205181
    :cond_0
    iget-object v0, p0, LX/F9E;->c:Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;

    invoke-static {v0}, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->m(Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;)V

    .line 2205182
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2205165
    invoke-direct {p0}, LX/F9E;->b()V

    .line 2205166
    iget-object v0, p0, LX/F9E;->c:Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;

    iget-object v0, v0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->n:LX/F8y;

    if-eqz v0, :cond_0

    .line 2205167
    iget-object v0, p0, LX/F9E;->c:Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;

    iget-object v0, v0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->n:LX/F8y;

    invoke-virtual {v0}, LX/F8y;->a()V

    .line 2205168
    :cond_0
    iget-object v0, p0, LX/F9E;->c:Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;

    iget-object v0, v0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->h:LX/BYf;

    invoke-virtual {v0}, LX/BYf;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2205169
    iget-object v0, p0, LX/F9E;->c:Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;

    .line 2205170
    iget-object v1, v0, Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;->i:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0Xl;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.facebook.zero.ACTION_ZERO_REFRESH_TOKEN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "zero_token_request_reason"

    sget-object p1, LX/32P;->INCENTIVE_PROVISIONED_FORCE_FETCH:LX/32P;

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v2

    invoke-interface {v1, v2}, LX/0Xl;->a(Landroid/content/Intent;)V

    .line 2205171
    :cond_1
    iget-object v0, p0, LX/F9E;->c:Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2205172
    instance-of v0, v0, LX/8DG;

    if-eqz v0, :cond_2

    .line 2205173
    const-string v1, "upload_profile_pic"

    .line 2205174
    iget-object v0, p0, LX/F9E;->c:Lcom/facebook/growth/nux/fragments/NUXProfilePhotoFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, LX/8DG;

    invoke-interface {v0, v1}, LX/8DG;->b(Ljava/lang/String;)V

    .line 2205175
    :cond_2
    return-void
.end method
