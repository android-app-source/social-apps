.class public final enum LX/FHR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FHR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FHR;

.field public static final enum transcode:LX/FHR;

.field public static final enum trim:LX/FHR;

.field public static final enum unknown:LX/FHR;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2220348
    new-instance v0, LX/FHR;

    const-string v1, "trim"

    invoke-direct {v0, v1, v2}, LX/FHR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHR;->trim:LX/FHR;

    new-instance v0, LX/FHR;

    const-string v1, "transcode"

    invoke-direct {v0, v1, v3}, LX/FHR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHR;->transcode:LX/FHR;

    new-instance v0, LX/FHR;

    const-string v1, "unknown"

    invoke-direct {v0, v1, v4}, LX/FHR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHR;->unknown:LX/FHR;

    .line 2220349
    const/4 v0, 0x3

    new-array v0, v0, [LX/FHR;

    sget-object v1, LX/FHR;->trim:LX/FHR;

    aput-object v1, v0, v2

    sget-object v1, LX/FHR;->transcode:LX/FHR;

    aput-object v1, v0, v3

    sget-object v1, LX/FHR;->unknown:LX/FHR;

    aput-object v1, v0, v4

    sput-object v0, LX/FHR;->$VALUES:[LX/FHR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2220347
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FHR;
    .locals 1

    .prologue
    .line 2220346
    const-class v0, LX/FHR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FHR;

    return-object v0
.end method

.method public static values()[LX/FHR;
    .locals 1

    .prologue
    .line 2220345
    sget-object v0, LX/FHR;->$VALUES:[LX/FHR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FHR;

    return-object v0
.end method
