.class public final LX/GDw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lcom/google/common/util/concurrent/ListenableFuture;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/common/util/concurrent/ListenableFuture",
        "<",
        "LX/0Px",
        "<",
        "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/ListenableFuture;

.field public final synthetic b:LX/0QK;

.field public final synthetic c:LX/GDy;


# direct methods
.method public constructor <init>(LX/GDy;Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)V
    .locals 0

    .prologue
    .line 2330657
    iput-object p1, p0, LX/GDw;->c:LX/GDy;

    iput-object p2, p0, LX/GDw;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    iput-object p3, p0, LX/GDw;->b:LX/0QK;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 2330651
    iget-object v0, p0, LX/GDw;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, p1, p2}, Lcom/google/common/util/concurrent/ListenableFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    .line 2330652
    return-void
.end method

.method public final cancel(Z)Z
    .locals 1

    .prologue
    .line 2330655
    iget-object v0, p0, LX/GDw;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0, p1}, Lcom/google/common/util/concurrent/ListenableFuture;->cancel(Z)Z

    move-result v0

    return v0
.end method

.method public final get()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2330656
    iget-object v0, p0, LX/GDw;->b:LX/0QK;

    iget-object v1, p0, LX/GDw;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    const v2, 0x3fcced1e

    invoke-static {v1, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2330654
    iget-object v0, p0, LX/GDw;->b:LX/0QK;

    iget-object v1, p0, LX/GDw;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    const v2, 0x34308559

    invoke-static {v1, p1, p2, p3, v2}, LX/03Q;->a(Ljava/util/concurrent/Future;JLjava/util/concurrent/TimeUnit;I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    return-object v0
.end method

.method public final isCancelled()Z
    .locals 1

    .prologue
    .line 2330653
    iget-object v0, p0, LX/GDw;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isCancelled()Z

    move-result v0

    return v0
.end method

.method public final isDone()Z
    .locals 1

    .prologue
    .line 2330650
    iget-object v0, p0, LX/GDw;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    invoke-interface {v0}, Lcom/google/common/util/concurrent/ListenableFuture;->isDone()Z

    move-result v0

    return v0
.end method
