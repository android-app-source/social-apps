.class public final LX/G06;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;)V
    .locals 0

    .prologue
    .line 2308488
    iput-object p1, p0, LX/G06;->a:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    .prologue
    .line 2308483
    add-int v0, p2, p3

    add-int/lit8 v0, v0, 0x3

    if-ge v0, p4, :cond_1

    .line 2308484
    :cond_0
    :goto_0
    return-void

    .line 2308485
    :cond_1
    iget-object v0, p0, LX/G06;->a:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/AbstractMemorialActivity;->q:LX/1Ck;

    invoke-virtual {v0}, LX/1Ck;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, LX/G06;->a:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    iget-object v0, v0, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->s:LX/2dj;

    invoke-virtual {v0}, LX/2dj;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2308486
    iget-object v0, p0, LX/G06;->a:Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;->c(Lcom/facebook/timeline/legacycontact/MemorialFriendRequestsActivity;Z)V

    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 2308487
    return-void
.end method
