.class public LX/GqW;
.super LX/Cos;
.source ""

# interfaces
.implements Lcom/facebook/common/callercontext/CallerContextable;
.implements LX/CnG;
.implements LX/Ct1;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cos",
        "<",
        "LX/Gph;",
        "Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;",
        ">;",
        "Lcom/facebook/common/callercontext/CallerContextable;",
        "Lcom/facebook/instantshopping/view/block/InstantShoppingScrubbableGIFBlockView;"
    }
.end annotation


# static fields
.field public static final e:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field public a:LX/Gqx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/DLX;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:Landroid/net/Uri;

.field private final k:LX/Ci3;

.field public l:LX/GrT;

.field public m:LX/GrS;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2396620
    const-class v0, LX/GqW;

    sput-object v0, LX/GqW;->e:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/Ctg;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2396621
    invoke-direct {p0, p1, p2}, LX/Cos;-><init>(LX/Ctg;Landroid/view/View;)V

    .line 2396622
    new-instance v0, LX/GqT;

    invoke-direct {v0, p0}, LX/GqT;-><init>(LX/GqW;)V

    iput-object v0, p0, LX/GqW;->k:LX/Ci3;

    .line 2396623
    const-class v0, LX/GqW;

    invoke-static {v0, p0}, LX/GqW;->a(Ljava/lang/Class;LX/02k;)V

    .line 2396624
    iget-object v0, p0, LX/GqW;->c:LX/Chv;

    iget-object v1, p0, LX/GqW;->k:LX/Ci3;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2396625
    new-instance v0, LX/GrT;

    invoke-direct {v0, p1}, LX/GrT;-><init>(LX/Ctg;)V

    iput-object v0, p0, LX/GqW;->l:LX/GrT;

    .line 2396626
    new-instance v0, LX/GrS;

    invoke-direct {v0, p1}, LX/GrS;-><init>(LX/Ctg;)V

    iput-object v0, p0, LX/GqW;->m:LX/GrS;

    .line 2396627
    iget-object v0, p0, LX/GqW;->m:LX/GrS;

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 2396628
    iget-object v0, p0, LX/GqW;->l:LX/GrT;

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 2396629
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/GqW;

    invoke-static {p0}, LX/Gqx;->b(LX/0QB;)LX/Gqx;

    move-result-object v1

    check-cast v1, LX/Gqx;

    invoke-static {p0}, LX/DLX;->b(LX/0QB;)LX/DLX;

    move-result-object v2

    check-cast v2, LX/DLX;

    invoke-static {p0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object p0

    check-cast p0, LX/Chv;

    iput-object v1, p1, LX/GqW;->a:LX/Gqx;

    iput-object v2, p1, LX/GqW;->b:LX/DLX;

    iput-object p0, p1, LX/GqW;->c:LX/Chv;

    return-void
.end method

.method public static a$redex0(LX/GqW;Ljava/io/InputStream;)V
    .locals 6

    .prologue
    .line 2396593
    const/4 v1, 0x0

    .line 2396594
    const/16 v0, 0x400

    new-array v4, v0, [B

    .line 2396595
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/scrubbableGIF"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2396596
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2396597
    const/4 v2, 0x1

    .line 2396598
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2396599
    invoke-virtual {v0}, Ljava/io/File;->mkdir()Z

    move-result v2

    .line 2396600
    :cond_0
    if-eqz v2, :cond_5

    .line 2396601
    :goto_0
    move-object v0, v0

    .line 2396602
    if-nez v0, :cond_4

    .line 2396603
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    move-object v3, v0

    .line 2396604
    :goto_1
    const-string v0, "tmp"

    const-string v2, ".mp4"

    invoke-static {v0, v2, v3}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v5

    .line 2396605
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2396606
    :goto_2
    :try_start_1
    invoke-virtual {p1, v4}, Ljava/io/InputStream;->read([B)I

    move-result v0

    if-lez v0, :cond_2

    .line 2396607
    const/4 v1, 0x0

    invoke-virtual {v2, v4, v1, v0}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    goto :goto_2

    .line 2396608
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 2396609
    :goto_3
    :try_start_2
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2396610
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_4
    if-eqz v2, :cond_1

    .line 2396611
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    :cond_1
    throw v0

    .line 2396612
    :cond_2
    :try_start_3
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    .line 2396613
    if-eqz v0, :cond_3

    .line 2396614
    invoke-virtual {v3}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2396615
    iput-object v1, v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->q:Landroid/net/Uri;

    .line 2396616
    invoke-virtual {v5}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->setVideoUri(Landroid/net/Uri;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 2396617
    :cond_3
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V

    return-void

    .line 2396618
    :catchall_1
    move-exception v0

    move-object v2, v1

    goto :goto_4

    :catchall_2
    move-exception v0

    goto :goto_4

    .line 2396619
    :catch_1
    move-exception v0

    goto :goto_3

    :cond_4
    move-object v3, v0

    goto :goto_1

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/Ctg;LX/CrN;Z)LX/CqX;
    .locals 7

    .prologue
    .line 2396592
    iget-object v0, p0, LX/GqW;->a:LX/Gqx;

    sget-object v1, LX/CrN;->NON_INTERACTIVE_ASPECT_FIT:LX/CrN;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v3, p1

    move v4, p3

    invoke-virtual/range {v0 .. v6}, LX/Gqx;->a(LX/CrN;Landroid/content/Context;LX/Ctg;ZZZ)LX/CqX;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2396630
    invoke-super {p0, p1}, LX/Cos;->a(Landroid/os/Bundle;)V

    .line 2396631
    iget-object v0, p0, LX/GqW;->l:LX/GrT;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    .line 2396632
    iget-boolean v1, v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->x:Z

    move v0, v1

    .line 2396633
    if-eqz v0, :cond_0

    .line 2396634
    iget-object v0, p0, LX/GqW;->l:LX/GrT;

    .line 2396635
    iget-object v1, v0, LX/GrT;->b:Landroid/widget/ProgressBar;

    const/16 p1, 0x8

    invoke-virtual {v1, p1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2396636
    :cond_0
    iget-object v0, p0, LX/GqW;->m:LX/GrS;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/view/widget/ScrubbableGIFPlayer;->getNumberOfTimesInstructionsAnimated()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_1

    .line 2396637
    iget-object v0, p0, LX/GqW;->m:LX/GrS;

    invoke-virtual {v0}, LX/GrS;->m()V

    .line 2396638
    :cond_1
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2396586
    invoke-super {p0, p1}, LX/Cos;->b(Landroid/os/Bundle;)V

    .line 2396587
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2396588
    :goto_0
    return-void

    .line 2396589
    :cond_0
    new-instance v1, LX/GqU;

    invoke-direct {v1, p0}, LX/GqU;-><init>(LX/GqW;)V

    .line 2396590
    iget-object v0, p0, LX/Cos;->f:LX/CoR;

    move-object v2, v0

    .line 2396591
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    new-instance v3, LX/CoQ;

    sget-object v4, LX/CoP;->PERCENTAGE:LX/CoP;

    const/16 v5, 0xa

    invoke-direct {v3, v4, v5}, LX/CoQ;-><init>(LX/CoP;I)V

    invoke-virtual {v2, v0, v3, v1}, LX/CoR;->a(Landroid/view/View;LX/CoQ;LX/CoO;)V

    goto :goto_0
.end method

.method public final getMediaAspectRatio()F
    .locals 1

    .prologue
    .line 2396585
    const/4 v0, 0x0

    return v0
.end method

.method public final getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 2396584
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public final jk_()Z
    .locals 1

    .prologue
    .line 2396583
    const/4 v0, 0x0

    return v0
.end method
