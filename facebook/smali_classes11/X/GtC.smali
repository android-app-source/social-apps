.class public final LX/GtC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/InternSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/InternSettingsActivity;)V
    .locals 0

    .prologue
    .line 2400447
    iput-object p1, p0, LX/GtC;->a:Lcom/facebook/katana/InternSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2400448
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/GtC;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v1, v1, Lcom/facebook/katana/InternSettingsActivity;->u:Landroid/content/Context;

    const-class v2, Lcom/facebook/globallibrarycollector/GlobalLibraryCollectorService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2400449
    const-string v1, "upload_all_libs"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2400450
    iget-object v1, p0, LX/GtC;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v1, v1, Lcom/facebook/katana/InternSettingsActivity;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/GtC;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v2, v2, Lcom/facebook/katana/InternSettingsActivity;->u:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    .line 2400451
    return v3
.end method
