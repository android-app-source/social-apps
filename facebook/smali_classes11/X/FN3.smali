.class public final LX/FN3;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Lorg/apache/http/client/ResponseHandler;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/http/client/ResponseHandler",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;

.field private final b:Ljava/io/File;


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;Ljava/io/File;)V
    .locals 0

    .prologue
    .line 2232019
    iput-object p1, p0, LX/FN3;->a:Lcom/facebook/messaging/sms/defaultapp/send/SmsSenderHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2232020
    iput-object p2, p0, LX/FN3;->b:Ljava/io/File;

    .line 2232021
    return-void
.end method


# virtual methods
.method public final handleResponse(Lorg/apache/http/HttpResponse;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2232022
    invoke-interface {p1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 2232023
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 2232024
    :try_start_0
    iget-object v0, p0, LX/FN3;->b:Ljava/io/File;

    const/4 v2, 0x0

    new-array v2, v2, [LX/3AS;

    invoke-static {v0, v2}, LX/1t3;->a(Ljava/io/File;[LX/3AS;)LX/3AU;

    move-result-object v0

    .line 2232025
    invoke-virtual {v0, v1}, LX/3AU;->a(Ljava/io/InputStream;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2232026
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 2232027
    const/4 v0, 0x0

    return-object v0

    .line 2232028
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
.end method
