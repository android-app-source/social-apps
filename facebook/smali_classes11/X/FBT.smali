.class public final LX/FBT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$UnreadThreadQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/common/util/concurrent/SettableFuture;

.field public final synthetic b:LX/FBU;


# direct methods
.method public constructor <init>(LX/FBU;Lcom/google/common/util/concurrent/SettableFuture;)V
    .locals 0

    .prologue
    .line 2209432
    iput-object p1, p0, LX/FBT;->b:LX/FBU;

    iput-object p2, p0, LX/FBT;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2209433
    iget-object v0, p0, LX/FBT;->a:Lcom/google/common/util/concurrent/SettableFuture;

    invoke-virtual {v0, p1}, Lcom/google/common/util/concurrent/SettableFuture;->setException(Ljava/lang/Throwable;)Z

    .line 2209434
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 13
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2209400
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v12, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2209401
    if-eqz p1, :cond_0

    .line 2209402
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2209403
    if-nez v0, :cond_1

    .line 2209404
    :cond_0
    iget-object v0, p0, LX/FBT;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, 0x7351dc2f

    invoke-static {v0, v12, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    .line 2209405
    :goto_0
    return-void

    .line 2209406
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2209407
    :try_start_0
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2209408
    check-cast v0, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$UnreadThreadQueryModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/diode/UnreadThreadQueryModels$UnreadThreadQueryModel;->a()LX/1vs;

    move-result-object v0

    iget-object v4, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const/4 v5, 0x0

    const v6, 0x3a8661ad

    invoke-static {v4, v0, v5, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2209409
    if-eqz v0, :cond_3

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    .line 2209410
    :goto_1
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, LX/2sN;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, LX/2sN;->b()LX/1vs;

    move-result-object v0

    iget-object v5, v0, LX/1vs;->a:LX/15i;

    iget v6, v0, LX/1vs;->b:I

    .line 2209411
    new-instance v7, LX/FBR;

    invoke-direct {v7}, LX/FBR;-><init>()V

    .line 2209412
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 2209413
    const/4 v0, 0x0

    invoke-virtual {v5, v6, v0}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_5

    .line 2209414
    const/4 v0, 0x0

    invoke-virtual {v5, v6, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2209415
    const/4 v9, 0x0

    invoke-virtual {v5, v0, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_3
    if-eqz v0, :cond_6

    .line 2209416
    const/4 v0, 0x0

    invoke-virtual {v5, v6, v0}, LX/15i;->g(II)I

    move-result v0

    const/4 v9, 0x0

    invoke-virtual {v5, v0, v9}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 2209417
    iput-object v0, v7, LX/FBR;->a:Landroid/net/Uri;

    .line 2209418
    :cond_2
    invoke-static {v8}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    .line 2209419
    iput-object v0, v7, LX/FBR;->b:LX/0Px;

    .line 2209420
    const/4 v0, 0x1

    invoke-virtual {v5, v6, v0}, LX/15i;->g(II)I

    move-result v0

    const/4 v6, 0x0

    invoke-virtual {v5, v0, v6}, LX/15i;->j(II)I

    move-result v0

    .line 2209421
    iput v0, v7, LX/FBR;->d:I

    .line 2209422
    invoke-virtual {v7}, LX/FBR;->a()LX/FBS;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 2209423
    :catch_0
    iget-object v0, p0, LX/FBT;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, 0x73018558

    invoke-static {v0, v12, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    goto/16 :goto_0

    .line 2209424
    :cond_3
    :try_start_1
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_1

    :cond_4
    move v0, v2

    .line 2209425
    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_3

    .line 2209426
    :cond_6
    const/4 v0, 0x3

    invoke-virtual {v5, v6, v0}, LX/15i;->g(II)I

    move-result v0

    const/4 v9, 0x1

    const v10, -0x2cb05235

    invoke-static {v5, v0, v9, v10}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    .line 2209427
    if-eqz v0, :cond_7

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    :goto_4
    invoke-virtual {v0}, LX/3Sa;->e()LX/3Sh;

    move-result-object v0

    :goto_5
    invoke-interface {v0}, LX/2sN;->a()Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-interface {v0}, LX/2sN;->b()LX/1vs;

    move-result-object v9

    iget-object v10, v9, LX/1vs;->a:LX/15i;

    iget v9, v9, LX/1vs;->b:I

    .line 2209428
    const/4 v11, 0x0

    invoke-virtual {v10, v9, v11}, LX/15i;->g(II)I

    move-result v9

    const/4 v11, 0x1

    invoke-virtual {v10, v9, v11}, LX/15i;->g(II)I

    move-result v9

    .line 2209429
    const/4 v11, 0x0

    invoke-virtual {v10, v9, v11}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 2209430
    :cond_7
    invoke-static {}, LX/2uF;->h()LX/2uF;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_4

    .line 2209431
    :cond_8
    iget-object v0, p0, LX/FBT;->a:Lcom/google/common/util/concurrent/SettableFuture;

    const v1, -0x236b0c2a

    invoke-static {v0, v3, v1}, LX/03Q;->a(Lcom/google/common/util/concurrent/SettableFuture;Ljava/lang/Object;I)Z

    goto/16 :goto_0
.end method
