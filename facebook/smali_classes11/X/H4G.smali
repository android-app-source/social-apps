.class public LX/H4G;
.super Landroid/widget/BaseAdapter;
.source ""


# static fields
.field public static final h:[I


# instance fields
.field private a:Ljava/lang/Boolean;

.field public b:Z

.field public c:Landroid/content/Context;

.field public d:LX/H4F;

.field private e:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

.field public f:LX/I57;

.field public g:LX/0wM;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2422499
    const/4 v0, 0x3

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x7f082128

    aput v2, v0, v1

    const/4 v1, 0x1

    const v2, 0x7f082129

    aput v2, v0, v1

    const/4 v1, 0x2

    const v2, 0x7f08212a

    aput v2, v0, v1

    sput-object v0, LX/H4G;->h:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;Z)V
    .locals 2

    .prologue
    .line 2422501
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2422502
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2422503
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2422504
    iput-object p2, p0, LX/H4G;->e:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    .line 2422505
    iput-object p1, p0, LX/H4G;->c:Landroid/content/Context;

    .line 2422506
    new-instance v0, LX/0wM;

    iget-object v1, p0, LX/H4G;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, LX/0wM;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, LX/H4G;->g:LX/0wM;

    .line 2422507
    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, LX/H4G;->a:Ljava/lang/Boolean;

    .line 2422508
    iget-object v0, p0, LX/H4G;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2422509
    sget-object v0, LX/H4F;->SEARCH_RADIUS_5:LX/H4F;

    iput-object v0, p0, LX/H4G;->d:LX/H4F;

    .line 2422510
    :cond_0
    return-void
.end method

.method private a()I
    .locals 1

    .prologue
    .line 2422500
    invoke-direct {p0}, LX/H4G;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 2422446
    iget-object v0, p0, LX/H4G;->e:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->f()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 2422496
    if-nez p1, :cond_0

    .line 2422497
    const/4 v0, 0x0

    .line 2422498
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, LX/H4G;->a()I

    move-result v0

    sub-int v0, p1, v0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2422511
    iget-object v0, p0, LX/H4G;->e:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->c:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    if-nez v0, :cond_0

    .line 2422512
    const/4 v0, 0x0

    .line 2422513
    :goto_0
    return v0

    .line 2422514
    :cond_0
    iget-object v0, p0, LX/H4G;->e:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->a:Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2422515
    iget-object v0, p0, LX/H4G;->e:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->c:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    .line 2422516
    iget-object p0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;->b:LX/0Px;

    move-object v0, p0

    .line 2422517
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2422518
    :cond_1
    iget-object v0, p0, LX/H4G;->e:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->c:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    .line 2422519
    iget-object p0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;->b:LX/0Px;

    move-object v0, p0

    .line 2422520
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2422485
    invoke-static {}, LX/H4E;->values()[LX/H4E;

    move-result-object v1

    invoke-virtual {p0, p1}, LX/H4G;->getItemViewType(I)I

    move-result v2

    aget-object v1, v1, v2

    .line 2422486
    sget-object v2, LX/H4D;->a:[I

    invoke-virtual {v1}, LX/H4E;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    .line 2422487
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "there are no object associate with location"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2422488
    :pswitch_0
    iget-object v1, p0, LX/H4G;->e:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v1, v1, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->c:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    if-nez v1, :cond_1

    .line 2422489
    :cond_0
    :goto_0
    :pswitch_1
    return-object v0

    .line 2422490
    :cond_1
    invoke-direct {p0}, LX/H4G;->a()I

    move-result v1

    sub-int v1, p1, v1

    .line 2422491
    iget-object v2, p0, LX/H4G;->e:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v2, v2, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->c:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    .line 2422492
    iget-object p1, v2, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;->b:LX/0Px;

    move-object v2, p1

    .line 2422493
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-le v2, v1, :cond_0

    iget-object v0, p0, LX/H4G;->e:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesTypeaheadModel;->c:Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;

    .line 2422494
    iget-object v2, v0, Lcom/facebook/nearby/v2/typeahead/model/NearbyPlacesLocationResult;->b:LX/0Px;

    move-object v0, v2

    .line 2422495
    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final getItemId(I)J
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2422481
    invoke-direct {p0}, LX/H4G;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    move-wide v0, v2

    .line 2422482
    :goto_0
    return-wide v0

    .line 2422483
    :cond_0
    invoke-virtual {p0, p1}, LX/H4G;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;

    .line 2422484
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    move-wide v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2422478
    invoke-direct {p0}, LX/H4G;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 2422479
    sget-object v0, LX/H4E;->CURRENT_LOCATION_CELL:LX/H4E;

    invoke-virtual {v0}, LX/H4E;->ordinal()I

    move-result v0

    .line 2422480
    :goto_0
    return v0

    :cond_0
    sget-object v0, LX/H4E;->LOCATION_CELL:LX/H4E;

    invoke-virtual {v0}, LX/H4E;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 2422448
    invoke-static {}, LX/H4E;->values()[LX/H4E;

    move-result-object v0

    invoke-virtual {p0, p1}, LX/H4G;->getItemViewType(I)I

    move-result v1

    aget-object v2, v0, v1

    .line 2422449
    if-nez p2, :cond_0

    .line 2422450
    iget-object v0, p0, LX/H4G;->c:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 2422451
    const v1, 0x7f03152b

    invoke-virtual {v0, v1, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/contentview/ContentView;

    move-object p2, v0

    .line 2422452
    :goto_0
    const v0, 0x7f0d2fc1

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 2422453
    const v1, 0x7f0d2fc2

    invoke-virtual {p2, v1}, Lcom/facebook/fbui/widget/contentview/ContentView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/resources/ui/FbButton;

    .line 2422454
    sget-object v3, LX/H4D;->a:[I

    invoke-virtual {v2}, LX/H4E;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    .line 2422455
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2422456
    :cond_0
    check-cast p2, Lcom/facebook/fbui/widget/contentview/ContentView;

    goto :goto_0

    .line 2422457
    :pswitch_0
    iget-object v2, p0, LX/H4G;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f082123

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2422458
    iget-object v2, p0, LX/H4G;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2422459
    iget-boolean v2, p0, LX/H4G;->b:Z

    if-eqz v2, :cond_1

    .line 2422460
    iget-object v2, p0, LX/H4G;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020145

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2422461
    const v0, 0x7f0e08a6

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 2422462
    invoke-virtual {v1, v5}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    .line 2422463
    const/4 v4, 0x0

    .line 2422464
    sget-object v0, LX/H4G;->h:[I

    iget-object v2, p0, LX/H4G;->d:LX/H4F;

    invoke-virtual {v2}, LX/H4F;->ordinal()I

    move-result v2

    aget v0, v0, v2

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbButton;->setText(I)V

    .line 2422465
    iget-object v0, p0, LX/H4G;->g:LX/0wM;

    const v2, 0x7f020a13

    const v3, -0xa76f01

    invoke-virtual {v0, v2, v3}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2422466
    invoke-virtual {v1, v4, v4, v0, v4}, Lcom/facebook/resources/ui/FbButton;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 2422467
    new-instance v0, LX/H4B;

    invoke-direct {v0, p0}, LX/H4B;-><init>(LX/H4G;)V

    invoke-virtual {v1, v0}, Lcom/facebook/resources/ui/FbButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2422468
    :goto_1
    return-object p2

    .line 2422469
    :cond_1
    iget-object v2, p0, LX/H4G;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020d59

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2422470
    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    goto :goto_1

    .line 2422471
    :cond_2
    iget-object v2, p0, LX/H4G;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020145

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2422472
    const v0, 0x7f0e08a6

    invoke-virtual {p2, v0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleTextAppearance(I)V

    .line 2422473
    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    goto :goto_1

    .line 2422474
    :pswitch_1
    invoke-virtual {p0, p1}, LX/H4G;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;

    .line 2422475
    invoke-virtual {v2}, Lcom/facebook/nearby/v2/network/NearbyPlacesTypeaheadGraphQLModels$FBNearbyPlacesTypeaheadLocationResultsConnectionFragmentModel$EdgesModel$NodeModel;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2422476
    iget-object v2, p0, LX/H4G;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020d59

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2422477
    invoke-virtual {v1, v4}, Lcom/facebook/resources/ui/FbButton;->setVisibility(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2422447
    sget-object v0, LX/H4E;->COUNT:LX/H4E;

    invoke-virtual {v0}, LX/H4E;->ordinal()I

    move-result v0

    return v0
.end method
