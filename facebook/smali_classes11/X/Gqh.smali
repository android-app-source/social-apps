.class public LX/Gqh;
.super LX/Cow;
.source ""


# instance fields
.field public k:LX/Gqx;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/Go0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/GnF;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public n:LX/2yr;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public o:LX/0ad;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public p:LX/Go7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/GrY;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/Grc;

.field public s:Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;

.field public t:LX/GrH;

.field public u:Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;

.field public v:Ljava/lang/String;

.field public w:Z

.field private x:LX/CrN;

.field public y:LX/GrZ;


# direct methods
.method public constructor <init>(LX/Ctg;Landroid/view/View;)V
    .locals 7

    .prologue
    .line 2396870
    invoke-direct {p0, p1, p2}, LX/Cow;-><init>(LX/Ctg;Landroid/view/View;)V

    .line 2396871
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/Gqh;

    invoke-static {v0}, LX/Gqx;->b(LX/0QB;)LX/Gqx;

    move-result-object v3

    check-cast v3, LX/Gqx;

    invoke-static {v0}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object v4

    check-cast v4, LX/Go0;

    invoke-static {v0}, LX/GnF;->a(LX/0QB;)LX/GnF;

    move-result-object v5

    check-cast v5, LX/GnF;

    invoke-static {v0}, LX/2yr;->b(LX/0QB;)LX/2yr;

    move-result-object v6

    check-cast v6, LX/2yr;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object p2

    check-cast p2, LX/0ad;

    invoke-static {v0}, LX/Go7;->a(LX/0QB;)LX/Go7;

    move-result-object v0

    check-cast v0, LX/Go7;

    iput-object v3, v2, LX/Gqh;->k:LX/Gqx;

    iput-object v4, v2, LX/Gqh;->l:LX/Go0;

    iput-object v5, v2, LX/Gqh;->m:LX/GnF;

    iput-object v6, v2, LX/Gqh;->n:LX/2yr;

    iput-object p2, v2, LX/Gqh;->o:LX/0ad;

    iput-object v0, v2, LX/Gqh;->p:LX/Go7;

    .line 2396872
    new-instance v0, LX/GrH;

    invoke-direct {v0, p1}, LX/GrH;-><init>(LX/Ctg;)V

    iput-object v0, p0, LX/Gqh;->t:LX/GrH;

    .line 2396873
    iget-object v0, p0, LX/Gqh;->t:LX/GrH;

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 2396874
    new-instance v0, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;

    invoke-direct {v0, p1}, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;-><init>(LX/Ctg;)V

    iput-object v0, p0, LX/Gqh;->u:Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;

    .line 2396875
    iget-object v0, p0, LX/Gqh;->u:Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 2396876
    new-instance v0, LX/Grc;

    invoke-direct {v0, p1}, LX/Grc;-><init>(LX/Ctg;)V

    iput-object v0, p0, LX/Gqh;->r:LX/Grc;

    .line 2396877
    iget-object v0, p0, LX/Gqh;->r:LX/Grc;

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 2396878
    invoke-static {p0}, LX/Gqh;->p(LX/Gqh;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2396879
    new-instance v0, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;

    invoke-direct {v0, p1}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;-><init>(LX/Ctg;)V

    iput-object v0, p0, LX/Gqh;->s:Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;

    .line 2396880
    iget-object v0, p0, LX/Gqh;->s:Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;

    invoke-virtual {p0, v0}, LX/Cos;->a(LX/Ctr;)V

    .line 2396881
    :cond_0
    return-void
.end method

.method public static a(LX/15i;I)Landroid/graphics/RectF;
    .locals 6

    .prologue
    .line 2396882
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->l(II)D

    move-result-wide v0

    double-to-float v0, v0

    .line 2396883
    const/4 v1, 0x3

    invoke-virtual {p0, p1, v1}, LX/15i;->l(II)D

    move-result-wide v2

    double-to-float v1, v2

    .line 2396884
    new-instance v2, Landroid/graphics/RectF;

    const/4 v3, 0x1

    invoke-virtual {p0, p1, v3}, LX/15i;->l(II)D

    move-result-wide v4

    double-to-float v3, v4

    sub-float/2addr v3, v0

    const/4 v4, 0x0

    invoke-virtual {p0, p1, v4}, LX/15i;->l(II)D

    move-result-wide v4

    double-to-float v4, v4

    sub-float/2addr v4, v1

    invoke-direct {v2, v0, v1, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v2
.end method

.method public static b(Landroid/view/View;)LX/Cow;
    .locals 2

    .prologue
    .line 2396886
    new-instance v1, LX/Gqh;

    move-object v0, p0

    check-cast v0, LX/Ctg;

    invoke-direct {v1, v0, p0}, LX/Gqh;-><init>(LX/Ctg;Landroid/view/View;)V

    return-object v1
.end method

.method private n()Z
    .locals 3

    .prologue
    .line 2396846
    iget-object v0, p0, LX/Gqh;->o:LX/0ad;

    sget-short v1, LX/CHN;->p:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method

.method public static p(LX/Gqh;)Z
    .locals 3

    .prologue
    .line 2396885
    iget-object v0, p0, LX/Gqh;->o:LX/0ad;

    sget-short v1, LX/CHN;->u:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(LX/Ctg;LX/CrN;Z)LX/CqX;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2396864
    iput-object p2, p0, LX/Gqh;->x:LX/CrN;

    .line 2396865
    sget-object v0, LX/CrN;->FULLSCREEN:LX/CrN;

    if-eq p2, v0, :cond_0

    sget-object v0, LX/CrN;->FULLSCREEN_SLIDE:LX/CrN;

    if-eq p2, v0, :cond_0

    sget-object v0, LX/CrN;->FULLSCREEN_SLIDESHOW:LX/CrN;

    if-ne p2, v0, :cond_1

    .line 2396866
    :cond_0
    iget-object v0, p0, LX/Gqh;->r:LX/Grc;

    sget-object v1, LX/Grb;->TILT_TO_PAN:LX/Grb;

    invoke-virtual {v0, v1}, LX/Grc;->a(LX/Grb;)V

    .line 2396867
    :cond_1
    iget-object v0, p0, LX/Gqh;->k:LX/Gqx;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-boolean v1, p0, LX/Gqh;->w:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, LX/Gqh;->o:LX/0ad;

    sget-short v3, LX/CHN;->k:S

    invoke-interface {v1, v3, v6}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v5, 0x1

    :goto_0
    move-object v1, p2

    move-object v3, p1

    move v4, p3

    invoke-virtual/range {v0 .. v6}, LX/Gqx;->a(LX/CrN;Landroid/content/Context;LX/Ctg;ZZZ)LX/CqX;

    move-result-object v0

    return-object v0

    :cond_2
    move v5, v6

    goto :goto_0
.end method

.method public a(LX/GoE;)V
    .locals 1

    .prologue
    .line 2396868
    iget-object v0, p0, LX/Gqh;->l:LX/Go0;

    invoke-virtual {v0, p1}, LX/Go0;->a(LX/GoE;)V

    .line 2396869
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2396850
    iget-object v0, p0, LX/Gqh;->r:LX/Grc;

    invoke-virtual {v0}, LX/Grc;->a()V

    .line 2396851
    iget-object v0, p0, LX/Gqh;->t:LX/GrH;

    .line 2396852
    iget-object v1, v0, LX/GrH;->a:Landroid/widget/ImageView;

    if-nez v1, :cond_2

    .line 2396853
    :goto_0
    iget-object v0, p0, LX/Gqh;->u:Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/view/widget/ImageOverlayPlugin;->k()V

    .line 2396854
    const/4 v0, 0x0

    .line 2396855
    iget-object v1, p0, LX/Cos;->a:LX/Ctg;

    move-object v1, v1

    .line 2396856
    invoke-interface {v1, v0}, LX/Ctg;->setOverlayBackgroundColor(I)V

    .line 2396857
    invoke-static {p0}, LX/Gqh;->p(LX/Gqh;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Gqh;->s:Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;

    if-eqz v0, :cond_0

    .line 2396858
    iget-object v0, p0, LX/Gqh;->s:Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->k()V

    .line 2396859
    :cond_0
    iget-object v0, p0, LX/Gqh;->y:LX/GrZ;

    if-eqz v0, :cond_1

    .line 2396860
    iget-object v0, p0, LX/Gqh;->y:LX/GrZ;

    invoke-virtual {v0}, LX/GrZ;->a()V

    .line 2396861
    :cond_1
    invoke-super {p0, p1}, LX/Cow;->a(Landroid/os/Bundle;)V

    .line 2396862
    return-void

    .line 2396863
    :cond_2
    iget-object v1, v0, LX/GrH;->a:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;II)V
    .locals 3

    .prologue
    .line 2396847
    iget-object v0, p0, LX/Gqh;->o:LX/0ad;

    sget-short v1, LX/CHN;->f:S

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2396848
    :goto_0
    return-void

    .line 2396849
    :cond_0
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/facebook/richdocument/view/widget/RichDocumentImageView;->a(Ljava/lang/String;II)V

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2396845
    const/4 v0, 0x0

    return v0
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2396838
    invoke-super {p0, p1}, LX/Cow;->b(Landroid/os/Bundle;)V

    .line 2396839
    invoke-direct {p0}, LX/Gqh;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2396840
    iget-object v0, p0, LX/Gqh;->n:LX/2yr;

    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/Gqh;->v:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, LX/2yr;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2396841
    :cond_0
    iget-object v0, p0, LX/Gqh;->x:LX/CrN;

    sget-object v1, LX/CrN;->FULLSCREEN:LX/CrN;

    if-ne v0, v1, :cond_1

    .line 2396842
    invoke-static {p0}, LX/Gqh;->p(LX/Gqh;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Gqh;->s:Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;

    if-eqz v0, :cond_1

    .line 2396843
    iget-object v0, p0, LX/Gqh;->s:Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/view/widget/media/TiltToPanPlugin;->a()V

    .line 2396844
    :cond_1
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2396827
    invoke-super {p0, p1}, LX/Cow;->c(Landroid/os/Bundle;)V

    .line 2396828
    invoke-direct {p0}, LX/Gqh;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2396829
    iget-object v0, p0, LX/Gqh;->n:LX/2yr;

    .line 2396830
    iget-object v1, v0, LX/2yr;->b:LX/CHQ;

    if-eqz v1, :cond_3

    .line 2396831
    iget-object v1, v0, LX/2yr;->b:LX/CHQ;

    .line 2396832
    iget-object p0, v1, LX/CHQ;->b:LX/1ca;

    if-eqz p0, :cond_0

    .line 2396833
    iget-object p0, v1, LX/CHQ;->b:LX/1ca;

    invoke-interface {p0}, LX/1ca;->g()Z

    .line 2396834
    :cond_0
    iget-object p0, v1, LX/CHQ;->d:LX/8Ys;

    if-eqz p0, :cond_1

    .line 2396835
    iget-object p0, v1, LX/CHQ;->a:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    iget-object p0, p0, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->f:LX/8bW;

    iget-object p1, v1, LX/CHQ;->d:LX/8Ys;

    invoke-interface {p1}, LX/8Ys;->s()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, LX/8bW;->a(Ljava/lang/String;)V

    .line 2396836
    :cond_1
    const/4 v1, 0x0

    iput-object v1, v0, LX/2yr;->b:LX/CHQ;

    .line 2396837
    :cond_2
    :goto_0
    return-void

    :cond_3
    goto :goto_0
.end method
