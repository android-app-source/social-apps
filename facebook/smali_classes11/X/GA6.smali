.class public final LX/GA6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GA5;


# instance fields
.field public final synthetic a:Lcom/facebook/AccessToken;

.field public final synthetic b:LX/G9z;

.field public final synthetic c:Ljava/util/concurrent/atomic/AtomicBoolean;

.field public final synthetic d:LX/GA7;

.field public final synthetic e:Ljava/util/Set;

.field public final synthetic f:Ljava/util/Set;

.field public final synthetic g:LX/GA8;


# direct methods
.method public constructor <init>(LX/GA8;Lcom/facebook/AccessToken;LX/G9z;Ljava/util/concurrent/atomic/AtomicBoolean;LX/GA7;Ljava/util/Set;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 2324629
    iput-object p1, p0, LX/GA6;->g:LX/GA8;

    iput-object p2, p0, LX/GA6;->a:Lcom/facebook/AccessToken;

    iput-object p3, p0, LX/GA6;->b:LX/G9z;

    iput-object p4, p0, LX/GA6;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object p5, p0, LX/GA6;->d:LX/GA7;

    iput-object p6, p0, LX/GA6;->e:Ljava/util/Set;

    iput-object p7, p0, LX/GA6;->f:Ljava/util/Set;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 2324630
    :try_start_0
    invoke-static {}, LX/GA8;->a()LX/GA8;

    move-result-object v0

    .line 2324631
    iget-object v1, v0, LX/GA8;->d:Lcom/facebook/AccessToken;

    move-object v0, v1

    .line 2324632
    if-eqz v0, :cond_0

    invoke-static {}, LX/GA8;->a()LX/GA8;

    move-result-object v0

    .line 2324633
    iget-object v1, v0, LX/GA8;->d:Lcom/facebook/AccessToken;

    move-object v0, v1

    .line 2324634
    iget-object v1, v0, Lcom/facebook/AccessToken;->l:Ljava/lang/String;

    move-object v0, v1

    .line 2324635
    iget-object v1, p0, LX/GA6;->a:Lcom/facebook/AccessToken;

    .line 2324636
    iget-object v2, v1, Lcom/facebook/AccessToken;->l:Ljava/lang/String;

    move-object v1, v2

    .line 2324637
    if-eq v0, v1, :cond_3

    .line 2324638
    :cond_0
    iget-object v0, p0, LX/GA6;->b:LX/G9z;

    if-eqz v0, :cond_1

    .line 2324639
    new-instance v0, LX/GAA;

    const-string v1, "No current access token to refresh"

    invoke-direct {v0, v1}, LX/GAA;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2324640
    :cond_1
    iget-object v0, p0, LX/GA6;->g:LX/GA8;

    iget-object v0, v0, LX/GA8;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v12}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2324641
    :cond_2
    :goto_0
    return-void

    .line 2324642
    :cond_3
    :try_start_1
    iget-object v0, p0, LX/GA6;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, LX/GA6;->d:LX/GA7;

    iget-object v0, v0, LX/GA7;->a:Ljava/lang/String;

    if-nez v0, :cond_5

    iget-object v0, p0, LX/GA6;->d:LX/GA7;

    iget v0, v0, LX/GA7;->b:I

    if-nez v0, :cond_5

    .line 2324643
    iget-object v0, p0, LX/GA6;->b:LX/G9z;

    if-eqz v0, :cond_4

    .line 2324644
    new-instance v0, LX/GAA;

    const-string v1, "Failed to refresh access token"

    invoke-direct {v0, v1}, LX/GAA;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2324645
    :cond_4
    iget-object v0, p0, LX/GA6;->g:LX/GA8;

    iget-object v0, v0, LX/GA8;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v12}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto :goto_0

    .line 2324646
    :cond_5
    :try_start_2
    new-instance v0, Lcom/facebook/AccessToken;

    iget-object v1, p0, LX/GA6;->d:LX/GA7;

    iget-object v1, v1, LX/GA7;->a:Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, p0, LX/GA6;->d:LX/GA7;

    iget-object v1, v1, LX/GA7;->a:Ljava/lang/String;

    :goto_1
    iget-object v2, p0, LX/GA6;->a:Lcom/facebook/AccessToken;

    .line 2324647
    iget-object v3, v2, Lcom/facebook/AccessToken;->k:Ljava/lang/String;

    move-object v2, v3

    .line 2324648
    iget-object v3, p0, LX/GA6;->a:Lcom/facebook/AccessToken;

    .line 2324649
    iget-object v4, v3, Lcom/facebook/AccessToken;->l:Ljava/lang/String;

    move-object v3, v4

    .line 2324650
    iget-object v4, p0, LX/GA6;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v4

    if-eqz v4, :cond_7

    iget-object v4, p0, LX/GA6;->e:Ljava/util/Set;

    :goto_2
    iget-object v5, p0, LX/GA6;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v5, p0, LX/GA6;->f:Ljava/util/Set;

    :goto_3
    iget-object v6, p0, LX/GA6;->a:Lcom/facebook/AccessToken;

    .line 2324651
    iget-object v7, v6, Lcom/facebook/AccessToken;->i:LX/GA9;

    move-object v6, v7

    .line 2324652
    iget-object v7, p0, LX/GA6;->d:LX/GA7;

    iget v7, v7, LX/GA7;->b:I

    if-eqz v7, :cond_9

    new-instance v7, Ljava/util/Date;

    iget-object v8, p0, LX/GA6;->d:LX/GA7;

    iget v8, v8, LX/GA7;->b:I

    int-to-long v8, v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    :goto_4
    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    invoke-direct/range {v0 .. v8}, Lcom/facebook/AccessToken;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Ljava/util/Collection;LX/GA9;Ljava/util/Date;Ljava/util/Date;)V

    .line 2324653
    invoke-static {}, LX/GA8;->a()LX/GA8;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/GA8;->a(Lcom/facebook/AccessToken;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2324654
    iget-object v0, p0, LX/GA6;->g:LX/GA8;

    iget-object v0, v0, LX/GA8;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v12}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2324655
    iget-object v0, p0, LX/GA6;->b:LX/G9z;

    if-eqz v0, :cond_2

    goto/16 :goto_0

    .line 2324656
    :cond_6
    :try_start_3
    iget-object v1, p0, LX/GA6;->a:Lcom/facebook/AccessToken;

    .line 2324657
    iget-object v2, v1, Lcom/facebook/AccessToken;->h:Ljava/lang/String;

    move-object v1, v2

    .line 2324658
    goto :goto_1

    :cond_7
    iget-object v4, p0, LX/GA6;->a:Lcom/facebook/AccessToken;

    .line 2324659
    iget-object v5, v4, Lcom/facebook/AccessToken;->f:Ljava/util/Set;

    move-object v4, v5

    .line 2324660
    goto :goto_2

    :cond_8
    iget-object v5, p0, LX/GA6;->a:Lcom/facebook/AccessToken;

    .line 2324661
    iget-object v6, v5, Lcom/facebook/AccessToken;->g:Ljava/util/Set;

    move-object v5, v6

    .line 2324662
    goto :goto_3

    :cond_9
    iget-object v7, p0, LX/GA6;->a:Lcom/facebook/AccessToken;

    .line 2324663
    iget-object v8, v7, Lcom/facebook/AccessToken;->e:Ljava/util/Date;

    move-object v7, v8
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2324664
    goto :goto_4

    .line 2324665
    :catchall_0
    move-exception v0

    iget-object v1, p0, LX/GA6;->g:LX/GA8;

    iget-object v1, v1, LX/GA8;->e:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v12}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 2324666
    throw v0
.end method
