.class public final LX/GTp;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLInterfaces$BackgroundLocationPrivacyPickerOptions;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;)V
    .locals 0

    .prologue
    .line 2355888
    iput-object p1, p0, LX/GTp;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x0

    .line 2355889
    invoke-virtual {p1}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;->a()LX/0Px;

    move-result-object v6

    .line 2355890
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v7

    .line 2355891
    iget-object v0, p0, LX/GTp;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    .line 2355892
    iput-object v2, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->X:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    .line 2355893
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v8

    move v4, v5

    move-object v3, v2

    :goto_0
    if-ge v4, v8, :cond_2

    .line 2355894
    invoke-virtual {v6, v4}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionEdgeModel;

    .line 2355895
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionEdgeModel;->c()Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    move-result-object v1

    .line 2355896
    invoke-virtual {v7, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2355897
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionEdgeModel;->a()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 2355898
    iget-object v9, p0, LX/GTp;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    .line 2355899
    iput-object v1, v9, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->X:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    .line 2355900
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionEdgeModel;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v3, v1

    .line 2355901
    :cond_1
    const-string v0, "friends"

    invoke-virtual {v1}, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;->b()LX/1Fd;

    move-result-object v9

    invoke-interface {v9}, LX/1Fd;->d()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    move-object v0, v1

    .line 2355902
    :goto_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move-object v2, v0

    goto :goto_0

    .line 2355903
    :cond_2
    const-string v0, "informational"

    iget-object v1, p0, LX/GTp;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->U:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/GTp;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->X:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    if-nez v0, :cond_3

    .line 2355904
    iget-object v0, p0, LX/GTp;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->w:LX/03V;

    const-string v1, "background_location_one_page_nux_status_abnormal"

    const-string v4, "informational nux does not have selected privacy option"

    invoke-virtual {v0, v1, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2355905
    :cond_3
    iget-object v0, p0, LX/GTp;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->X:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    if-eqz v0, :cond_6

    iget-object v0, p0, LX/GTp;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v3, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->X:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    .line 2355906
    :cond_4
    :goto_2
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2355907
    iget-object v1, p0, LX/GTp;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->K:Lcom/facebook/fbui/popover/PopoverSpinner;

    iget-object v2, p0, LX/GTp;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v2, v2, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->Z:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/popover/PopoverSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 2355908
    iget-object v1, p0, LX/GTp;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    new-instance v2, LX/GUB;

    invoke-direct {v2}, LX/GUB;-><init>()V

    .line 2355909
    iput-object v2, v1, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->Y:LX/GUB;

    .line 2355910
    iget-object v1, p0, LX/GTp;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->Y:LX/GUB;

    .line 2355911
    iput-boolean v5, v1, LX/GUB;->b:Z

    .line 2355912
    iget-object v1, p0, LX/GTp;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->Y:LX/GUB;

    invoke-virtual {v1, v0}, LX/GUB;->a(LX/0Px;)V

    .line 2355913
    iget-object v1, p0, LX/GTp;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->K:Lcom/facebook/fbui/popover/PopoverSpinner;

    iget-object v2, p0, LX/GTp;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v2, v2, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->Y:LX/GUB;

    invoke-virtual {v1, v2}, Lcom/facebook/fbui/popover/PopoverSpinner;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2355914
    invoke-virtual {v0, v3}, LX/0Px;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 2355915
    const/4 v1, -0x1

    if-eq v0, v1, :cond_5

    .line 2355916
    iget-object v1, p0, LX/GTp;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->K:Lcom/facebook/fbui/popover/PopoverSpinner;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/popover/PopoverSpinner;->setSelection(I)V

    .line 2355917
    :cond_5
    iget-object v0, p0, LX/GTp;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->J:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2355918
    iget-object v0, p0, LX/GTp;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->K:Lcom/facebook/fbui/popover/PopoverSpinner;

    invoke-virtual {v0, v5}, Lcom/facebook/fbui/popover/PopoverSpinner;->setVisibility(I)V

    .line 2355919
    return-void

    .line 2355920
    :cond_6
    if-nez v3, :cond_4

    move-object v3, v2

    goto :goto_2

    :cond_7
    move-object v0, v2

    goto :goto_1
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2355921
    sget-object v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->B:Ljava/lang/Class;

    const-string v1, "failed to get privacy options"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2355922
    iget-object v0, p0, LX/GTp;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->w:LX/03V;

    const-string v1, "background_location_one_page_nux_privacy_data_fetch_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2355923
    iget-object v0, p0, LX/GTp;->a:Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;->y:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080039

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2355924
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2355925
    check-cast p1, Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;

    invoke-direct {p0, p1}, LX/GTp;->a(Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionsModel;)V

    return-void
.end method
