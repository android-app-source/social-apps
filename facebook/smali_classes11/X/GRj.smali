.class public final LX/GRj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Lcom/facebook/appinvites/fragment/AppInvitesFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/appinvites/fragment/AppInvitesFragment;)V
    .locals 0

    .prologue
    .line 2351654
    iput-object p1, p0, LX/GRj;->a:Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 2351655
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v0

    if-nez v0, :cond_1

    .line 2351656
    :cond_0
    :goto_0
    return v2

    .line 2351657
    :cond_1
    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    invoke-virtual {p2, v2}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    .line 2351658
    iget-object v0, p0, LX/GRj;->a:Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    iget-object v0, v0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->n:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getOverScrollMode()I

    move-result v0

    if-eqz v0, :cond_0

    .line 2351659
    iget-object v0, p0, LX/GRj;->a:Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    iget-object v0, v0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->n:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOverScrollMode(I)V

    goto :goto_0

    .line 2351660
    :cond_2
    iget-object v0, p0, LX/GRj;->a:Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    iget-object v0, v0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->n:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getOverScrollMode()I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 2351661
    iget-object v0, p0, LX/GRj;->a:Lcom/facebook/appinvites/fragment/AppInvitesFragment;

    iget-object v0, v0, Lcom/facebook/appinvites/fragment/AppInvitesFragment;->n:Landroid/widget/ListView;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setOverScrollMode(I)V

    goto :goto_0
.end method
