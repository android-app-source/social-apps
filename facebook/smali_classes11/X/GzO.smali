.class public LX/GzO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2411814
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2411815
    iput-object p1, p0, LX/GzO;->a:Landroid/content/Context;

    .line 2411816
    return-void
.end method


# virtual methods
.method public final clearUserData()V
    .locals 2

    .prologue
    .line 2411811
    invoke-static {}, LX/02o;->b()LX/02o;

    move-result-object v0

    iget-object v1, p0, LX/GzO;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, LX/02o;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2411812
    const-string v0, "LoomTraceCleaner"

    const-string v1, "Could not clear config or traces!"

    invoke-static {v0, v1}, LX/01m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2411813
    :cond_0
    return-void
.end method
