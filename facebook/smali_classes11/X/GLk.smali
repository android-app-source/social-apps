.class public final LX/GLk;
.super Landroid/text/style/ClickableSpan;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

.field public final synthetic b:I

.field public final synthetic c:LX/GLp;


# direct methods
.method public constructor <init>(LX/GLp;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;I)V
    .locals 0

    .prologue
    .line 2343596
    iput-object p1, p0, LX/GLk;->c:LX/GLp;

    iput-object p2, p0, LX/GLk;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    iput p3, p0, LX/GLk;->b:I

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2343597
    sget-object v0, LX/GLo;->a:[I

    iget-object v1, p0, LX/GLk;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;->k()Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2343598
    :goto_0
    iget-object v0, p0, LX/GLk;->c:LX/GLp;

    iget-object v0, v0, LX/GLp;->c:Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/ui/compat/fbrelativelayout/FbRelativeLayout;->setVisibility(I)V

    .line 2343599
    return-void

    .line 2343600
    :pswitch_0
    iget-object v0, p0, LX/GLk;->c:LX/GLp;

    .line 2343601
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2343602
    new-instance v1, LX/GFv;

    iget-object v2, p0, LX/GLk;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    invoke-direct {v1, v2}, LX/GFv;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    goto :goto_0

    .line 2343603
    :pswitch_1
    iget-object v0, p0, LX/GLk;->c:LX/GLp;

    .line 2343604
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2343605
    new-instance v1, LX/GFI;

    iget-object v2, p0, LX/GLk;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    invoke-direct {v1, v2}, LX/GFI;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    goto :goto_0

    .line 2343606
    :pswitch_2
    iget-object v0, p0, LX/GLk;->c:LX/GLp;

    .line 2343607
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2343608
    new-instance v1, LX/GFC;

    iget-object v2, p0, LX/GLk;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    invoke-direct {v1, v2}, LX/GFC;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    goto :goto_0

    .line 2343609
    :pswitch_3
    iget-object v0, p0, LX/GLk;->c:LX/GLp;

    .line 2343610
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2343611
    new-instance v1, LX/GFe;

    iget-object v2, p0, LX/GLk;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    invoke-direct {v1, v2}, LX/GFe;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    goto :goto_0

    .line 2343612
    :pswitch_4
    iget-object v0, p0, LX/GLk;->c:LX/GLp;

    .line 2343613
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2343614
    new-instance v1, LX/GF6;

    iget-object v2, p0, LX/GLk;->a:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    invoke-direct {v1, v2}, LX/GF6;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    .prologue
    .line 2343615
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setUnderlineText(Z)V

    .line 2343616
    iget v0, p0, LX/GLk;->b:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    .line 2343617
    return-void
.end method
