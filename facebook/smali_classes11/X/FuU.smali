.class public LX/FuU;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/G5K;

.field private final b:LX/G5I;


# direct methods
.method public constructor <init>(LX/G5K;LX/G5I;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2300041
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2300042
    iput-object p1, p0, LX/FuU;->a:LX/G5K;

    .line 2300043
    iput-object p2, p0, LX/FuU;->b:LX/G5I;

    .line 2300044
    return-void
.end method


# virtual methods
.method public final a(LX/1Pf;)LX/1SX;
    .locals 4

    .prologue
    .line 2300045
    invoke-interface {p1}, LX/1Po;->c()LX/1PT;

    move-result-object v1

    .line 2300046
    instance-of v0, v1, LX/FuQ;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    move-object v0, v1

    .line 2300047
    check-cast v0, LX/FuQ;

    .line 2300048
    sget-object v2, LX/FuT;->a:[I

    invoke-interface {v1}, LX/1PT;->a()LX/1Qt;

    move-result-object v3

    invoke-virtual {v3}, LX/1Qt;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2300049
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported feedListType: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, LX/1PT;->a()LX/1Qt;

    move-result-object v1

    invoke-virtual {v1}, LX/1Qt;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2300050
    :pswitch_0
    iget-object v1, p0, LX/FuU;->a:LX/G5K;

    .line 2300051
    iget-object v2, v0, LX/FuQ;->a:LX/5SB;

    move-object v0, v2

    .line 2300052
    invoke-virtual {v1, v0, p1}, LX/G5K;->a(LX/5SB;LX/1Pf;)LX/G5J;

    move-result-object v0

    .line 2300053
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v1, p0, LX/FuU;->b:LX/G5I;

    .line 2300054
    iget-object v2, v0, LX/FuQ;->a:LX/5SB;

    move-object v0, v2

    .line 2300055
    invoke-virtual {v1, v0, p1}, LX/G5I;->a(LX/5SB;LX/1Pf;)LX/G5H;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
