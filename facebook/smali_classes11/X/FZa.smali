.class public final LX/FZa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field public final synthetic a:Lcom/facebook/ui/search/SearchEditText;

.field public final synthetic b:Lcom/facebook/search/fragment/GraphSearchFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/fragment/GraphSearchFragment;Lcom/facebook/ui/search/SearchEditText;)V
    .locals 0

    .prologue
    .line 2257405
    iput-object p1, p0, LX/FZa;->b:Lcom/facebook/search/fragment/GraphSearchFragment;

    iput-object p2, p0, LX/FZa;->a:Lcom/facebook/ui/search/SearchEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 2257406
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_3

    .line 2257407
    iget-object v0, p0, LX/FZa;->b:Lcom/facebook/search/fragment/GraphSearchFragment;

    iget-object v0, v0, Lcom/facebook/search/fragment/GraphSearchFragment;->p:LX/FZe;

    iget-object v1, p0, LX/FZa;->a:Lcom/facebook/ui/search/SearchEditText;

    invoke-virtual {v1}, Lcom/facebook/ui/search/SearchEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    .line 2257408
    iget-object p0, v0, LX/FZe;->e:LX/FZX;

    invoke-virtual {p0}, LX/FZX;->c()Landroid/support/v4/app/Fragment;

    move-result-object p0

    .line 2257409
    if-eqz p0, :cond_2

    instance-of p0, p0, Lcom/facebook/search/suggestions/SuggestionsFragment;

    if-nez p0, :cond_2

    iget-object p0, v0, LX/FZe;->A:Lcom/facebook/search/model/awareness/AwarenessGraphSearchConfiguration;

    if-nez p0, :cond_2

    .line 2257410
    iget-object p0, v0, LX/FZe;->j:LX/0gc;

    const-string p1, "chromeless:content:fragment:tag"

    invoke-virtual {p0, p1}, LX/0gc;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object p0

    .line 2257411
    instance-of p1, p0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isVisible()Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2257412
    check-cast p0, Lcom/facebook/ufiservices/flyout/UFIPopoverFragment;

    invoke-virtual {p0}, Lcom/facebook/widget/popover/PopoverFragment;->k()V

    .line 2257413
    :cond_0
    iget-object p0, v0, LX/FZe;->d:LX/FZW;

    iget-object p1, v0, LX/FZe;->y:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {p0, p1}, LX/FZW;->a(Lcom/facebook/search/api/GraphSearchQuery;)Lcom/facebook/search/suggestions/SuggestionsFragment;

    move-result-object p0

    .line 2257414
    iget-object p1, v0, LX/FZe;->e:LX/FZX;

    invoke-virtual {p1, p0}, LX/FZX;->b(Landroid/support/v4/app/Fragment;)V

    .line 2257415
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object p2, v0, LX/FZe;->y:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2257416
    if-eqz p2, :cond_1

    .line 2257417
    invoke-static {p0, p2}, Lcom/facebook/search/suggestions/SuggestionsFragment;->c(Lcom/facebook/search/suggestions/SuggestionsFragment;Lcom/facebook/search/api/GraphSearchQuery;)V

    .line 2257418
    :cond_1
    invoke-static {p0, p1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->b$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Ljava/lang/String;)V

    .line 2257419
    :cond_2
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 2257420
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 2257421
    invoke-virtual {p1}, Landroid/view/View;->performClick()Z

    goto :goto_0
.end method
