.class public final LX/GQE;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

.field public final synthetic b:Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)V
    .locals 0

    .prologue
    .line 2349781
    iput-object p1, p0, LX/GQE;->b:Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;

    iput-object p2, p0, LX/GQE;->a:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x217b67aa

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2349782
    iget-object v1, p0, LX/GQE;->b:Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;

    iget-object v1, v1, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->b:LX/ADW;

    sget-object v2, LX/ADV;->SELECT_COUNTRY_STATE:LX/ADV;

    iget-object v3, p0, LX/GQE;->a:Lcom/facebook/adspayments/analytics/PaymentsFlowContext;

    invoke-virtual {v1, v2, v3}, LX/ADW;->a(LX/ADV;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)Lcom/facebook/adspayments/analytics/PaymentsReliabilityTransitionLogEvent;

    .line 2349783
    iget-object v1, p0, LX/GQE;->b:Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;

    iget-object v1, v1, Lcom/facebook/adspayments/view/AdsBillingCountrySelectorView;->e:LX/7Tj;

    invoke-virtual {v1, p1}, LX/0ht;->a(Landroid/view/View;)V

    .line 2349784
    const v1, 0x58c2013f

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
