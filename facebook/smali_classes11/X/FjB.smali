.class public final LX/FjB;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Cwb;

.field public final synthetic b:LX/FjC;


# direct methods
.method public constructor <init>(LX/FjC;LX/Cwb;)V
    .locals 0

    .prologue
    .line 2276416
    iput-object p1, p0, LX/FjB;->b:LX/FjC;

    iput-object p2, p0, LX/FjB;->a:LX/Cwb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x2683e4bb

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2276417
    iget-object v0, p0, LX/FjB;->a:LX/Cwb;

    sget-object v1, LX/Cwb;->RECENT:LX/Cwb;

    if-ne v0, v1, :cond_0

    .line 2276418
    iget-object v0, p0, LX/FjB;->b:LX/FjC;

    iget-object v0, v0, LX/FjC;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2SY;

    invoke-virtual {v0}, LX/2SY;->f()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2276419
    sget-object v0, LX/0ax;->bM:Ljava/lang/String;

    iget-object v1, p0, LX/FjB;->b:LX/FjC;

    iget-object v1, v1, LX/FjC;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 2276420
    :goto_0
    iget-object v0, p0, LX/FjB;->b:LX/FjC;

    iget-object v0, v0, LX/FjC;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0, v3, v1}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    .line 2276421
    const v0, -0x54b447d7

    invoke-static {v0, v2}, LX/02F;->a(II)V

    :goto_1
    return-void

    .line 2276422
    :cond_0
    iget-object v0, p0, LX/FjB;->a:LX/Cwb;

    sget-object v1, LX/Cwb;->RECENT_VIDEOS:LX/Cwb;

    if-ne v0, v1, :cond_1

    .line 2276423
    iget-object v0, p0, LX/FjB;->b:LX/FjC;

    iget-object v0, v0, LX/FjC;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/2Sr;

    invoke-virtual {v0}, LX/2Sr;->f()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2276424
    sget-object v0, LX/0ax;->bN:Ljava/lang/String;

    iget-object v1, p0, LX/FjB;->b:LX/FjC;

    iget-object v1, v1, LX/FjC;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    sget-object v3, LX/7C8;->VIDEOS:LX/7C8;

    invoke-static {v0, v1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 2276425
    :cond_1
    const v0, -0x1d933f88

    invoke-static {v0, v2}, LX/02F;->a(II)V

    goto :goto_1
.end method
