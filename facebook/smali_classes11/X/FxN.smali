.class public LX/FxN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/8LV;

.field private final b:LX/1EZ;

.field private final c:Lcom/facebook/auth/viewercontext/ViewerContext;

.field private final d:Lcom/facebook/ui/media/attachments/MediaResourceHelper;


# direct methods
.method public constructor <init>(LX/8LV;LX/1EZ;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/ui/media/attachments/MediaResourceHelper;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2304796
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2304797
    iput-object p1, p0, LX/FxN;->a:LX/8LV;

    .line 2304798
    iput-object p2, p0, LX/FxN;->b:LX/1EZ;

    .line 2304799
    iput-object p3, p0, LX/FxN;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    .line 2304800
    iput-object p4, p0, LX/FxN;->d:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    .line 2304801
    return-void
.end method

.method public static a(LX/0QB;)LX/FxN;
    .locals 7

    .prologue
    .line 2304802
    const-class v1, LX/FxN;

    monitor-enter v1

    .line 2304803
    :try_start_0
    sget-object v0, LX/FxN;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2304804
    sput-object v2, LX/FxN;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2304805
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2304806
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2304807
    new-instance p0, LX/FxN;

    invoke-static {v0}, LX/8LV;->b(LX/0QB;)LX/8LV;

    move-result-object v3

    check-cast v3, LX/8LV;

    invoke-static {v0}, LX/1EZ;->a(LX/0QB;)LX/1EZ;

    move-result-object v4

    check-cast v4, LX/1EZ;

    invoke-static {v0}, LX/0eQ;->b(LX/0QB;)Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v5

    check-cast v5, Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {v0}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a(LX/0QB;)Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    move-result-object v6

    check-cast v6, Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    invoke-direct {p0, v3, v4, v5, v6}, LX/FxN;-><init>(LX/8LV;LX/1EZ;Lcom/facebook/auth/viewercontext/ViewerContext;Lcom/facebook/ui/media/attachments/MediaResourceHelper;)V

    .line 2304808
    move-object v0, p0

    .line 2304809
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2304810
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FxN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2304811
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2304812
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;)V
    .locals 12

    .prologue
    .line 2304813
    new-instance v0, LX/74m;

    invoke-direct {v0}, LX/74m;-><init>()V

    new-instance v1, LX/4gN;

    invoke-direct {v1}, LX/4gN;-><init>()V

    new-instance v2, LX/4gP;

    invoke-direct {v2}, LX/4gP;-><init>()V

    .line 2304814
    iget-object v3, p1, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;->a:Landroid/net/Uri;

    move-object v3, v3

    .line 2304815
    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4gP;->a(Ljava/lang/String;)LX/4gP;

    move-result-object v2

    iget-object v3, p0, LX/FxN;->d:Lcom/facebook/ui/media/attachments/MediaResourceHelper;

    .line 2304816
    iget-object v4, p1, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;->a:Landroid/net/Uri;

    move-object v4, v4

    .line 2304817
    invoke-virtual {v3, v4}, Lcom/facebook/ui/media/attachments/MediaResourceHelper;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/facebook/ipc/media/data/MimeType;->a(Ljava/lang/String;)Lcom/facebook/ipc/media/data/MimeType;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/4gP;->a(Lcom/facebook/ipc/media/data/MimeType;)LX/4gP;

    move-result-object v2

    sget-object v3, LX/4gQ;->Video:LX/4gQ;

    invoke-virtual {v2, v3}, LX/4gP;->a(LX/4gQ;)LX/4gP;

    move-result-object v2

    .line 2304818
    iget-object v3, p1, Lcom/facebook/timeline/header/intro/favphotos/edit/FavoriteVideo;->a:Landroid/net/Uri;

    move-object v3, v3

    .line 2304819
    invoke-virtual {v2, v3}, LX/4gP;->a(Landroid/net/Uri;)LX/4gP;

    move-result-object v2

    invoke-virtual {v2}, LX/4gP;->a()Lcom/facebook/ipc/media/data/MediaData;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/4gN;->a(Lcom/facebook/ipc/media/data/MediaData;)LX/4gN;

    move-result-object v1

    invoke-virtual {v1}, LX/4gN;->a()Lcom/facebook/ipc/media/data/LocalMediaData;

    move-result-object v1

    .line 2304820
    iput-object v1, v0, LX/74m;->e:Lcom/facebook/ipc/media/data/LocalMediaData;

    .line 2304821
    move-object v0, v0

    .line 2304822
    invoke-virtual {v0}, LX/74m;->a()Lcom/facebook/photos/base/media/VideoItem;

    move-result-object v0

    .line 2304823
    iget-object v1, p0, LX/FxN;->a:LX/8LV;

    iget-object v2, p0, LX/FxN;->c:Lcom/facebook/auth/viewercontext/ViewerContext;

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2304824
    iget-object v5, v2, Lcom/facebook/auth/viewercontext/ViewerContext;->a:Ljava/lang/String;

    move-object v5, v5

    .line 2304825
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    .line 2304826
    new-instance v7, LX/8LQ;

    invoke-direct {v7}, LX/8LQ;-><init>()V

    .line 2304827
    iput-object v3, v7, LX/8LQ;->a:Ljava/lang/String;

    .line 2304828
    move-object v7, v7

    .line 2304829
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v8

    .line 2304830
    iput-object v8, v7, LX/8LQ;->b:LX/0Px;

    .line 2304831
    move-object v7, v7

    .line 2304832
    iput-wide v5, v7, LX/8LQ;->h:J

    .line 2304833
    move-object v5, v7

    .line 2304834
    const-string v6, "profile_intro_card"

    .line 2304835
    iput-object v6, v5, LX/8LQ;->i:Ljava/lang/String;

    .line 2304836
    move-object v5, v5

    .line 2304837
    const-wide/16 v7, -0x1

    .line 2304838
    iput-wide v7, v5, LX/8LQ;->j:J

    .line 2304839
    move-object v5, v5

    .line 2304840
    sget-object v6, Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;->d:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 2304841
    iput-object v6, v5, LX/8LQ;->o:Lcom/facebook/photos/upload/protocol/PhotoUploadPrivacy;

    .line 2304842
    move-object v5, v5

    .line 2304843
    sget-object v6, LX/8LR;->PROFILE_INTRO_CARD:LX/8LR;

    .line 2304844
    iput-object v6, v5, LX/8LQ;->p:LX/8LR;

    .line 2304845
    move-object v5, v5

    .line 2304846
    sget-object v6, LX/8LS;->PROFILE_INTRO_CARD_VIDEO:LX/8LS;

    .line 2304847
    iput-object v6, v5, LX/8LQ;->q:LX/8LS;

    .line 2304848
    move-object v5, v5

    .line 2304849
    iget-object v6, v1, LX/8LV;->b:LX/0SG;

    invoke-interface {v6}, LX/0SG;->a()J

    move-result-wide v7

    const-wide/16 v9, 0x3e8

    div-long/2addr v7, v9

    .line 2304850
    iput-wide v7, v5, LX/8LQ;->x:J

    .line 2304851
    move-object v5, v5

    .line 2304852
    const/16 v6, 0x28

    invoke-virtual {v5, v6}, LX/8LQ;->e(I)LX/8LQ;

    move-result-object v5

    const/4 v6, -0x2

    .line 2304853
    iput v6, v5, LX/8LQ;->W:I

    .line 2304854
    move-object v5, v5

    .line 2304855
    invoke-virtual {v5}, LX/8LQ;->a()Lcom/facebook/photos/upload/operation/UploadOperation;

    move-result-object v5

    move-object v0, v5

    .line 2304856
    iget-object v1, p0, LX/FxN;->b:LX/1EZ;

    invoke-virtual {v1, v0}, LX/1EZ;->a(Lcom/facebook/photos/upload/operation/UploadOperation;)V

    .line 2304857
    return-void
.end method
