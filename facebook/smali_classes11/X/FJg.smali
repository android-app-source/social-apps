.class public LX/FJg;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2223735
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 2223736
    return-void
.end method

.method public static a(Ljava/lang/Boolean;)Ljava/lang/Boolean;
    .locals 2
    .param p0    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/messaging/annotations/IsBadgeTrayNotificationsGkEnabled;
        .end annotation
    .end param
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/annotations/IsBadgeTrayNotificationsEnabled;
    .end annotation

    .prologue
    .line 2223737
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 2223738
    return-void
.end method
