.class public final LX/GW0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GVw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GVw",
        "<",
        "LX/GWt;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2360303
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static b(Landroid/view/ViewGroup;LX/7iP;)LX/GWt;
    .locals 2

    .prologue
    .line 2360298
    new-instance v0, LX/GWt;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/GWt;-><init>(Landroid/content/Context;)V

    .line 2360299
    iput-object p1, v0, LX/GWt;->c:LX/7iP;

    .line 2360300
    iget-object v1, v0, LX/GWt;->a:LX/GWq;

    iget-object p0, v0, LX/GWt;->c:LX/7iP;

    .line 2360301
    iput-object p0, v1, LX/GWq;->c:LX/7iP;

    .line 2360302
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2360297
    sget-object v0, LX/7iP;->UNKNOWN:LX/7iP;

    invoke-static {p1, v0}, LX/GW0;->b(Landroid/view/ViewGroup;LX/7iP;)LX/GWt;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Landroid/view/ViewGroup;LX/7iP;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2360296
    invoke-static {p1, p2}, LX/GW0;->b(Landroid/view/ViewGroup;LX/7iP;)LX/GWt;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;
    .locals 1

    .prologue
    .line 2360282
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->MORE_FROM_SHOP:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    return-object v0
.end method

.method public final a(Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2360283
    if-eqz p1, :cond_1

    .line 2360284
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->v()LX/1vs;

    move-result-object v0

    iget v0, v0, LX/1vs;->b:I

    .line 2360285
    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_4

    .line 2360286
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->v()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2360287
    if-eqz v0, :cond_2

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    if-eqz v0, :cond_7

    .line 2360288
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->v()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    const-class v4, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$RecommendedProductItemsModel$NodesModel;

    invoke-virtual {v3, v0, v2, v4}, LX/15i;->h(IILjava/lang/Class;)LX/22e;

    move-result-object v0

    .line 2360289
    if-eqz v0, :cond_5

    invoke-static {v0}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v0

    :goto_3
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_4
    if-eqz v0, :cond_9

    .line 2360290
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->v()LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2360291
    invoke-static {v3, v0}, LX/GWt;->a(LX/15i;I)LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    :goto_5
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_0

    .line 2360292
    :cond_2
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2360293
    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_2

    .line 2360294
    :cond_5
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2360295
    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_4

    :cond_7
    move v0, v2

    goto :goto_4

    :cond_8
    move v1, v2

    goto :goto_5

    :cond_9
    move v1, v2

    goto :goto_5
.end method
