.class public final enum LX/GOH;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GOH;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GOH;

.field public static final enum SEND_BUSINESS_ADDRESS:LX/GOH;

.field public static final enum SEND_POSTAL_CODE:LX/GOH;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2347350
    new-instance v0, LX/GOH;

    const-string v1, "SEND_POSTAL_CODE"

    invoke-direct {v0, v1, v2}, LX/GOH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GOH;->SEND_POSTAL_CODE:LX/GOH;

    .line 2347351
    new-instance v0, LX/GOH;

    const-string v1, "SEND_BUSINESS_ADDRESS"

    invoke-direct {v0, v1, v3}, LX/GOH;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GOH;->SEND_BUSINESS_ADDRESS:LX/GOH;

    .line 2347352
    const/4 v0, 0x2

    new-array v0, v0, [LX/GOH;

    sget-object v1, LX/GOH;->SEND_POSTAL_CODE:LX/GOH;

    aput-object v1, v0, v2

    sget-object v1, LX/GOH;->SEND_BUSINESS_ADDRESS:LX/GOH;

    aput-object v1, v0, v3

    sput-object v0, LX/GOH;->$VALUES:[LX/GOH;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2347347
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GOH;
    .locals 1

    .prologue
    .line 2347349
    const-class v0, LX/GOH;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GOH;

    return-object v0
.end method

.method public static values()[LX/GOH;
    .locals 1

    .prologue
    .line 2347348
    sget-object v0, LX/GOH;->$VALUES:[LX/GOH;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GOH;

    return-object v0
.end method
