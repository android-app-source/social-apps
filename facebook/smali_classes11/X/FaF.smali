.class public final enum LX/FaF;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FaF;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FaF;

.field public static final enum GROUPS_PILL:LX/FaF;

.field public static final enum LEARNING_NUX:LX/FaF;

.field public static final enum PROFILE_PILL:LX/FaF;

.field public static final enum TUTORIAL_NUX:LX/FaF;

.field public static final enum UNSET:LX/FaF;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2258549
    new-instance v0, LX/FaF;

    const-string v1, "GROUPS_PILL"

    invoke-direct {v0, v1, v2}, LX/FaF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FaF;->GROUPS_PILL:LX/FaF;

    .line 2258550
    new-instance v0, LX/FaF;

    const-string v1, "LEARNING_NUX"

    invoke-direct {v0, v1, v3}, LX/FaF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FaF;->LEARNING_NUX:LX/FaF;

    .line 2258551
    new-instance v0, LX/FaF;

    const-string v1, "PROFILE_PILL"

    invoke-direct {v0, v1, v4}, LX/FaF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FaF;->PROFILE_PILL:LX/FaF;

    .line 2258552
    new-instance v0, LX/FaF;

    const-string v1, "TUTORIAL_NUX"

    invoke-direct {v0, v1, v5}, LX/FaF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FaF;->TUTORIAL_NUX:LX/FaF;

    .line 2258553
    new-instance v0, LX/FaF;

    const-string v1, "UNSET"

    invoke-direct {v0, v1, v6}, LX/FaF;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FaF;->UNSET:LX/FaF;

    .line 2258554
    const/4 v0, 0x5

    new-array v0, v0, [LX/FaF;

    sget-object v1, LX/FaF;->GROUPS_PILL:LX/FaF;

    aput-object v1, v0, v2

    sget-object v1, LX/FaF;->LEARNING_NUX:LX/FaF;

    aput-object v1, v0, v3

    sget-object v1, LX/FaF;->PROFILE_PILL:LX/FaF;

    aput-object v1, v0, v4

    sget-object v1, LX/FaF;->TUTORIAL_NUX:LX/FaF;

    aput-object v1, v0, v5

    sget-object v1, LX/FaF;->UNSET:LX/FaF;

    aput-object v1, v0, v6

    sput-object v0, LX/FaF;->$VALUES:[LX/FaF;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2258555
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FaF;
    .locals 1

    .prologue
    .line 2258556
    const-class v0, LX/FaF;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FaF;

    return-object v0
.end method

.method public static values()[LX/FaF;
    .locals 1

    .prologue
    .line 2258557
    sget-object v0, LX/FaF;->$VALUES:[LX/FaF;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FaF;

    return-object v0
.end method
