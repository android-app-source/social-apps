.class public LX/GDb;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static f:LX/0Xm;


# instance fields
.field private final b:LX/1Ck;

.field private final c:LX/0tX;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/03V;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2329962
    const-class v0, LX/GDb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/GDb;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/1Ck;LX/0tX;LX/03V;LX/0Or;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUserId;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ck;",
            "LX/0tX;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            "LX/0Or",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2329963
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2329964
    iput-object p1, p0, LX/GDb;->b:LX/1Ck;

    .line 2329965
    iput-object p2, p0, LX/GDb;->c:LX/0tX;

    .line 2329966
    iput-object p3, p0, LX/GDb;->e:LX/03V;

    .line 2329967
    iput-object p4, p0, LX/GDb;->d:LX/0Or;

    .line 2329968
    return-void
.end method

.method public static a(LX/0QB;)LX/GDb;
    .locals 7

    .prologue
    .line 2329969
    const-class v1, LX/GDb;

    monitor-enter v1

    .line 2329970
    :try_start_0
    sget-object v0, LX/GDb;->f:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2329971
    sput-object v2, LX/GDb;->f:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2329972
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2329973
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2329974
    new-instance v6, LX/GDb;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v5

    check-cast v5, LX/03V;

    const/16 p0, 0x15e7

    invoke-static {v0, p0}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-direct {v6, v3, v4, v5, p0}, LX/GDb;-><init>(LX/1Ck;LX/0tX;LX/03V;LX/0Or;)V

    .line 2329975
    move-object v0, v6

    .line 2329976
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2329977
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GDb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2329978
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2329979
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(Landroid/content/Context;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;
    .locals 3

    .prologue
    .line 2329980
    new-instance v0, LX/5Pu;

    invoke-direct {v0}, LX/5Pu;-><init>()V

    const v1, 0x7f080b9b

    invoke-virtual {p0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2329981
    iput-object v1, v0, LX/5Pu;->b:Ljava/lang/String;

    .line 2329982
    move-object v0, v0

    .line 2329983
    invoke-virtual {v0}, LX/5Pu;->a()Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    move-result-object v0

    .line 2329984
    new-instance v1, LX/A97;

    invoke-direct {v1}, LX/A97;-><init>()V

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;->TIP:Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

    .line 2329985
    iput-object v2, v1, LX/A97;->b:Lcom/facebook/graphql/enums/GraphQLBoostedComponentMessageType;

    .line 2329986
    move-object v1, v1

    .line 2329987
    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;->GENERIC:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    .line 2329988
    iput-object v2, v1, LX/A97;->c:Lcom/facebook/graphql/enums/GraphQLBoostedComponentSpecElement;

    .line 2329989
    move-object v1, v1

    .line 2329990
    iput-object v0, v1, LX/A97;->d:Lcom/facebook/graphql/linkutil/GraphQLLinkExtractorGraphQLModels$LinkableTextWithEntitiesModel;

    .line 2329991
    move-object v0, v1

    .line 2329992
    const/4 v1, 0x1

    .line 2329993
    iput-boolean v1, v0, LX/A97;->a:Z

    .line 2329994
    move-object v0, v0

    .line 2329995
    invoke-virtual {v0}, LX/A97;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    move-result-object v0

    .line 2329996
    return-object v0
.end method

.method public static a$redex0(LX/GDb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2329997
    iget-object v0, p0, LX/GDb;->e:LX/03V;

    sget-object v1, LX/GDb;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; couponPromotionGroupID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", adAccountID: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", referral: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2329998
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;LX/GCa;)V
    .locals 10

    .prologue
    .line 2329999
    new-instance v1, LX/4Dq;

    invoke-direct {v1}, LX/4Dq;-><init>()V

    .line 2330000
    const-string v0, "promotion_group_id"

    invoke-virtual {v1, v0, p1}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330001
    const-string v0, "ad_account_id"

    invoke-virtual {v1, v0, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330002
    iget-object v0, p0, LX/GDb;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2330003
    const-string v2, "actor_id"

    invoke-virtual {v1, v2, v0}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330004
    const-string v0, "referral"

    invoke-virtual {v1, v0, p3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2330005
    iget-object v7, p0, LX/GDb;->b:LX/1Ck;

    sget-object v8, LX/GDa;->COUPON_CLAIM:LX/GDa;

    iget-object v0, p0, LX/GDb;->c:LX/0tX;

    .line 2330006
    new-instance v2, LX/AB2;

    invoke-direct {v2}, LX/AB2;-><init>()V

    move-object v2, v2

    .line 2330007
    const-string v3, "input"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v2

    check-cast v2, LX/AB2;

    move-object v1, v2

    .line 2330008
    invoke-static {v1}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v9

    new-instance v0, LX/GDZ;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, LX/GDZ;-><init>(LX/GDb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/GCa;Landroid/content/Context;)V

    invoke-virtual {v7, v8, v9, v0}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2330009
    return-void
.end method
