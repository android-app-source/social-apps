.class public LX/F30;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public a:Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;

.field public b:LX/F3H;

.field private c:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2193549
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2193550
    iput-object p1, p0, LX/F30;->c:Landroid/view/LayoutInflater;

    .line 2193551
    return-void
.end method

.method private a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2193581
    iget-object v0, p0, LX/F30;->a:Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;->a()Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel;->a()Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel$NodeModel;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/F30;)Z
    .locals 1

    .prologue
    .line 2193580
    iget-object v0, p0, LX/F30;->a:Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/F30;->a:Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;->a()Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/F30;->a:Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;->a()Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 2193577
    invoke-static {p0}, LX/F30;->a(LX/F30;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2193578
    const/4 v0, 0x0

    .line 2193579
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/F30;->a:Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;->a()Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2193576
    invoke-direct {p0, p1}, LX/F30;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2193575
    iget-object v0, p0, LX/F30;->a:Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;->a()Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel;->a()Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2193553
    invoke-virtual {p0}, LX/F30;->getCount()I

    move-result v0

    if-gt p1, v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2193554
    if-nez p2, :cond_0

    .line 2193555
    iget-object v0, p0, LX/F30;->c:Landroid/view/LayoutInflater;

    const v1, 0x7f030835

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 2193556
    :cond_0
    invoke-direct {p0, p1}, LX/F30;->a(I)Ljava/lang/String;

    move-result-object v3

    .line 2193557
    if-eqz v3, :cond_1

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2193558
    iget-object v0, p0, LX/F30;->a:Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;->a()Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel;

    .line 2193559
    invoke-static {p0}, LX/F30;->a(LX/F30;)Z

    move-result v1

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel;->a()Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel$NodeModel;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 2193560
    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel;->a()Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel$NodeModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel$NodeModel;->l()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    .line 2193561
    if-eqz v1, :cond_3

    move v1, v4

    :goto_1
    if-eqz v1, :cond_6

    .line 2193562
    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel;->a()Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel$NodeModel;->l()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    .line 2193563
    invoke-virtual {v1, v0, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    :goto_2
    move v0, v4

    .line 2193564
    if-eqz v0, :cond_1

    .line 2193565
    const v0, 0x7f0d1572

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    .line 2193566
    iget-object v1, p0, LX/F30;->a:Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel;->a()Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel;->a()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel;->a()Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel$NodeModel;

    move-result-object v1

    .line 2193567
    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel$NodeModel;->l()LX/1vs;

    move-result-object v4

    iget-object v5, v4, LX/1vs;->a:LX/15i;

    iget v4, v4, LX/1vs;->b:I

    invoke-virtual {v5, v4, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    .line 2193568
    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchTagSearchSuggestionsModels$FetchTagSearchSuggestionsQueryModel$ResultsModel$EdgesModel$NodeModel;->j()Ljava/lang/String;

    move-result-object v1

    .line 2193569
    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 2193570
    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Landroid/net/Uri;)V

    .line 2193571
    invoke-virtual {v0, v3}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2193572
    new-instance v0, LX/F2z;

    invoke-direct {v0, p0, v3, v1}, LX/F2z;-><init>(LX/F30;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2193573
    :cond_1
    return-object p2

    :cond_2
    move v0, v2

    .line 2193574
    goto/16 :goto_0

    :cond_3
    move v1, v5

    goto :goto_1

    :cond_4
    move v1, v5

    goto :goto_1

    :cond_5
    move v4, v5

    goto :goto_2

    :cond_6
    move v4, v5

    goto :goto_2
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 2193552
    const/4 v0, 0x1

    return v0
.end method
