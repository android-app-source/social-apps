.class public final LX/GAy;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GAx;


# instance fields
.field public final synthetic a:LX/GAz;


# direct methods
.method public constructor <init>(LX/GAz;)V
    .locals 0

    .prologue
    .line 2326248
    iput-object p1, p0, LX/GAy;->a:LX/GAz;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 15

    .prologue
    .line 2326249
    iget-object v0, p0, LX/GAy;->a:LX/GAz;

    .line 2326250
    iget-object v1, v0, LX/GAz;->c:LX/2Dt;

    .line 2326251
    iget-object v2, v1, LX/2Dt;->a:LX/0Uh;

    const/16 v3, 0x16

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, LX/0Uh;->a(IZ)Z

    move-result v2

    move v1, v2

    .line 2326252
    if-eqz v1, :cond_0

    const-wide/16 v1, 0x128e

    .line 2326253
    :goto_0
    iget-object v3, v0, LX/GAz;->b:LX/2UC;

    iget-wide v5, v0, LX/GAz;->f:J

    iget-object v4, v0, LX/GAz;->a:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v7

    iget-wide v9, v0, LX/GAz;->f:J

    sub-long/2addr v7, v9

    add-long/2addr v1, v7

    invoke-virtual {v3, v5, v6, v1, v2}, LX/2UC;->a(JJ)Ljava/util/List;

    move-result-object v1

    .line 2326254
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v3

    const-string v4, "(^|\\D)(\\d{4,10})($|\\D)"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    const/4 v5, 0x2

    invoke-static {v1, v3, v4, v5}, LX/2UC;->a(Ljava/util/List;Ljava/util/Set;Ljava/util/regex/Pattern;I)Ljava/util/List;

    move-result-object v1

    .line 2326255
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2326256
    invoke-static {v0}, LX/GAz;->d(LX/GAz;)V

    .line 2326257
    :goto_1
    return-void

    .line 2326258
    :cond_0
    const-wide/16 v1, 0xfa

    goto :goto_0

    .line 2326259
    :cond_1
    iget-object v2, v0, LX/GAz;->e:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/EiE;

    iget-object v1, v1, LX/EiE;->a:Ljava/lang/String;

    .line 2326260
    iget-object v11, v2, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->h:LX/0SG;

    invoke-interface {v11}, LX/0SG;->a()J

    move-result-wide v11

    iget-wide v13, v2, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->R:J

    sub-long/2addr v11, v13

    iput-wide v11, v2, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->S:J

    .line 2326261
    invoke-static {v2, v1}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->b(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;Ljava/lang/String;)V

    .line 2326262
    goto :goto_1
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 2326263
    return-void
.end method
