.class public LX/Fhm;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field public a:Lcom/facebook/resources/ui/FbTextView;

.field public b:LX/Fh0;

.field public c:LX/0ht;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2273691
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2273692
    const v0, 0x7f0307e9

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->setContentView(I)V

    .line 2273693
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Fhm;->setOrientation(I)V

    .line 2273694
    const v0, 0x7f0d14e2

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomLinearLayout;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/Fhm;->a:Lcom/facebook/resources/ui/FbTextView;

    .line 2273695
    iget-object v0, p0, LX/Fhm;->a:Lcom/facebook/resources/ui/FbTextView;

    new-instance p1, LX/Fhl;

    invoke-direct {p1, p0}, LX/Fhl;-><init>(LX/Fhm;)V

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2273696
    return-void
.end method


# virtual methods
.method public final onMeasure(II)V
    .locals 3

    .prologue
    .line 2273697
    invoke-virtual {p0}, LX/Fhm;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2273698
    const v1, 0x7f0b1794

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 2273699
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v0, v0

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    .line 2273700
    float-to-int v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 2273701
    invoke-super {p0, v0, p2}, Lcom/facebook/widget/CustomLinearLayout;->onMeasure(II)V

    .line 2273702
    return-void
.end method
