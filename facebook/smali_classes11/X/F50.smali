.class public final LX/F50;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:LX/F51;


# direct methods
.method public constructor <init>(LX/F51;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 2197892
    iput-object p1, p0, LX/F50;->b:LX/F51;

    iput-object p2, p0, LX/F50;->a:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2197879
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 2197880
    iget-object v1, p0, LX/F50;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 2197881
    iget-object v1, p0, LX/F50;->b:LX/F51;

    iget-object v1, v1, LX/F51;->a:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v1, v0

    .line 2197882
    iget-object v1, p0, LX/F50;->b:LX/F51;

    iget v1, v1, LX/F51;->c:I

    if-eq v1, v0, :cond_1

    iget-object v1, p0, LX/F50;->b:LX/F51;

    iget v1, v1, LX/F51;->d:I

    if-le v0, v1, :cond_1

    .line 2197883
    iget-object v1, p0, LX/F50;->b:LX/F51;

    iget-object v1, v1, LX/F51;->b:LX/F53;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/F53;->a(Z)V

    .line 2197884
    iget-object v1, p0, LX/F50;->b:LX/F51;

    .line 2197885
    iput v0, v1, LX/F51;->c:I

    .line 2197886
    :cond_0
    :goto_0
    return-void

    .line 2197887
    :cond_1
    iget-object v1, p0, LX/F50;->b:LX/F51;

    iget v1, v1, LX/F51;->c:I

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/F50;->b:LX/F51;

    iget v1, v1, LX/F51;->d:I

    if-gt v0, v1, :cond_0

    .line 2197888
    iget-object v0, p0, LX/F50;->b:LX/F51;

    iget-object v0, v0, LX/F51;->b:LX/F53;

    invoke-virtual {v0, v2}, LX/F53;->a(Z)V

    .line 2197889
    iget-object v0, p0, LX/F50;->b:LX/F51;

    .line 2197890
    iput v2, v0, LX/F51;->c:I

    .line 2197891
    goto :goto_0
.end method
