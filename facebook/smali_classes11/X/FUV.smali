.class public final enum LX/FUV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FUV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FUV;

.field public static final enum STANDARD:LX/FUV;

.field public static final enum VANITY:LX/FUV;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2248305
    new-instance v0, LX/FUV;

    const-string v1, "STANDARD"

    invoke-direct {v0, v1, v2}, LX/FUV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FUV;->STANDARD:LX/FUV;

    new-instance v0, LX/FUV;

    const-string v1, "VANITY"

    invoke-direct {v0, v1, v3}, LX/FUV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FUV;->VANITY:LX/FUV;

    .line 2248306
    const/4 v0, 0x2

    new-array v0, v0, [LX/FUV;

    sget-object v1, LX/FUV;->STANDARD:LX/FUV;

    aput-object v1, v0, v2

    sget-object v1, LX/FUV;->VANITY:LX/FUV;

    aput-object v1, v0, v3

    sput-object v0, LX/FUV;->$VALUES:[LX/FUV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2248307
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FUV;
    .locals 1

    .prologue
    .line 2248308
    const-class v0, LX/FUV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FUV;

    return-object v0
.end method

.method public static values()[LX/FUV;
    .locals 1

    .prologue
    .line 2248309
    sget-object v0, LX/FUV;->$VALUES:[LX/FUV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FUV;

    return-object v0
.end method
