.class public final LX/Gye;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/location/ui/LocationSettingsFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/location/ui/LocationSettingsFragment;)V
    .locals 0

    .prologue
    .line 2410245
    iput-object p1, p0, LX/Gye;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .prologue
    .line 2410246
    if-nez p2, :cond_0

    iget-object v0, p0, LX/Gye;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    iget-boolean v0, v0, Lcom/facebook/location/ui/LocationSettingsFragment;->J:Z

    if-eqz v0, :cond_0

    .line 2410247
    iget-object v0, p0, LX/Gye;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    .line 2410248
    new-instance p0, LX/0ju;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1}, LX/0ju;-><init>(Landroid/content/Context;)V

    const p1, 0x7f0837aa

    invoke-virtual {p0, p1}, LX/0ju;->b(I)LX/0ju;

    move-result-object p0

    const p1, 0x7f080016

    new-instance p2, LX/Gya;

    invoke-direct {p2, v0}, LX/Gya;-><init>(Lcom/facebook/location/ui/LocationSettingsFragment;)V

    invoke-virtual {p0, p1, p2}, LX/0ju;->a(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object p0

    const p1, 0x7f080017

    new-instance p2, LX/GyZ;

    invoke-direct {p2, v0}, LX/GyZ;-><init>(Lcom/facebook/location/ui/LocationSettingsFragment;)V

    invoke-virtual {p0, p1, p2}, LX/0ju;->b(ILandroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object p0

    new-instance p1, LX/GyY;

    invoke-direct {p1, v0}, LX/GyY;-><init>(Lcom/facebook/location/ui/LocationSettingsFragment;)V

    invoke-virtual {p0, p1}, LX/0ju;->a(Landroid/content/DialogInterface$OnCancelListener;)LX/0ju;

    move-result-object p0

    invoke-virtual {p0}, LX/0ju;->b()LX/2EJ;

    .line 2410249
    :goto_0
    return-void

    .line 2410250
    :cond_0
    iget-object v0, p0, LX/Gye;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    iget-object v0, v0, Lcom/facebook/location/ui/LocationSettingsFragment;->o:LX/GyW;

    invoke-virtual {v0, p2}, LX/GyW;->a(Z)V

    .line 2410251
    iget-object v0, p0, LX/Gye;->a:Lcom/facebook/location/ui/LocationSettingsFragment;

    invoke-static {v0, p2}, Lcom/facebook/location/ui/LocationSettingsFragment;->a$redex0(Lcom/facebook/location/ui/LocationSettingsFragment;Z)V

    goto :goto_0
.end method
