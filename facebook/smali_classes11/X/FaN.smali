.class public LX/FaN;
.super LX/0hs;
.source ""


# instance fields
.field public a:Lcom/facebook/widget/text/BetterTextView;

.field public l:Lcom/facebook/widget/text/BetterTextView;

.field public m:Lcom/facebook/widget/CustomLinearLayout;

.field public n:Lcom/facebook/fig/button/FigButton;

.field public o:Lcom/facebook/fig/button/FigButton;

.field public p:Lcom/facebook/fbui/glyph/GlyphView;

.field public q:Landroid/view/View;

.field public r:LX/0Sh;

.field public s:Landroid/view/WindowManager;

.field public t:F

.field private u:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Sh;LX/0ad;Ljava/lang/Boolean;)V
    .locals 3
    .param p4    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/tablet/IsTablet;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2258637
    invoke-direct {p0, p1}, LX/0hs;-><init>(Landroid/content/Context;)V

    .line 2258638
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/FaN;->u:Z

    .line 2258639
    const v0, 0x7f031272

    const/4 p4, 0x0

    .line 2258640
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 2258641
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object p1

    .line 2258642
    invoke-virtual {p1, v0, p4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p3

    .line 2258643
    const v1, 0x7f0d2b53

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    iput-object v1, p0, LX/FaN;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2258644
    const v1, 0x7f0d2b54

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/text/BetterTextView;

    iput-object v1, p0, LX/FaN;->l:Lcom/facebook/widget/text/BetterTextView;

    .line 2258645
    const v1, 0x7f0d2b51

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fbui/glyph/GlyphView;

    iput-object v1, p0, LX/FaN;->p:Lcom/facebook/fbui/glyph/GlyphView;

    .line 2258646
    const v1, 0x7f0d2b55

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/widget/CustomLinearLayout;

    iput-object v1, p0, LX/FaN;->m:Lcom/facebook/widget/CustomLinearLayout;

    .line 2258647
    const v1, 0x7f0d2b57

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/button/FigButton;

    iput-object v1, p0, LX/FaN;->n:Lcom/facebook/fig/button/FigButton;

    .line 2258648
    const v1, 0x7f0d2b56

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/fig/button/FigButton;

    iput-object v1, p0, LX/FaN;->o:Lcom/facebook/fig/button/FigButton;

    .line 2258649
    invoke-virtual {p0, p3}, LX/0ht;->d(Landroid/view/View;)V

    .line 2258650
    const v1, 0x7f031271

    invoke-virtual {p1, v1, p4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, LX/FaN;->q:Landroid/view/View;

    .line 2258651
    iput-object p2, p0, LX/FaN;->r:LX/0Sh;

    .line 2258652
    const-string v1, "window"

    invoke-virtual {v2, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    iput-object v1, p0, LX/FaN;->s:Landroid/view/WindowManager;

    .line 2258653
    const/4 v1, 0x0

    iput v1, p0, LX/FaN;->t:F

    .line 2258654
    iget-object v1, p0, LX/0ht;->g:LX/5OY;

    invoke-virtual {v1, p4}, LX/5OY;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2258655
    return-void
.end method

.method public static g(LX/FaN;Landroid/view/View;)Landroid/view/WindowManager$LayoutParams;
    .locals 6

    .prologue
    .line 2258709
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v0}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 2258710
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    iput-object v1, v0, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 2258711
    const/16 v1, 0x3e8

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 2258712
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 2258713
    iget v2, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 2258714
    iget v3, p0, LX/0ht;->A:I

    move v3, v3

    .line 2258715
    sub-int/2addr v2, v3

    .line 2258716
    iget v3, p0, LX/0ht;->C:I

    move v3, v3

    .line 2258717
    sub-int/2addr v2, v3

    .line 2258718
    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    .line 2258719
    iget v3, p0, LX/0ht;->B:I

    move v3, v3

    .line 2258720
    sub-int/2addr v1, v3

    .line 2258721
    iget v3, p0, LX/0ht;->D:I

    move v3, v3

    .line 2258722
    sub-int/2addr v1, v3

    .line 2258723
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 2258724
    const/4 v4, 0x2

    new-array v4, v4, [I

    .line 2258725
    invoke-virtual {p1, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2258726
    const/4 v5, 0x1

    aget v4, v4, v5

    add-int/2addr v3, v4

    iput v3, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2258727
    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2258728
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->y:I

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2258729
    iget v1, p0, LX/FaN;->t:F

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->alpha:F

    .line 2258730
    const/16 v1, 0x33

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 2258731
    const/16 v1, 0x8

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 2258732
    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 2258733
    iget-object v0, p0, LX/FaN;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2258734
    iget-object v1, p0, LX/FaN;->a:Lcom/facebook/widget/text/BetterTextView;

    if-lez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2258735
    return-void

    .line 2258736
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final a(Landroid/view/View;ZLandroid/view/WindowManager$LayoutParams;)V
    .locals 9

    .prologue
    const/high16 v7, -0x80000000

    .line 2258670
    iget-object v0, p0, LX/FaN;->p:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 2258671
    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 2258672
    iget v0, p0, LX/0ht;->A:I

    move v0, v0

    .line 2258673
    iget v1, p0, LX/0ht;->C:I

    move v2, v1

    .line 2258674
    iget v1, p0, LX/0ht;->B:I

    move v1, v1

    .line 2258675
    iget v3, p0, LX/0ht;->D:I

    move v3, v3

    .line 2258676
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    .line 2258677
    iget v5, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int/2addr v5, v0

    sub-int/2addr v5, v2

    .line 2258678
    iget v6, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int v1, v6, v1

    sub-int/2addr v1, v3

    .line 2258679
    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 2258680
    invoke-static {v1, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 2258681
    iget-object v5, p0, LX/0ht;->g:LX/5OY;

    invoke-virtual {v5, v3, v1}, LX/5OY;->measure(II)V

    .line 2258682
    iget-object v1, p0, LX/0ht;->g:LX/5OY;

    invoke-virtual {v1}, LX/5OY;->getMeasuredWidth()I

    move-result v3

    .line 2258683
    iget-object v1, p0, LX/0ht;->g:LX/5OY;

    invoke-virtual {v1}, LX/5OY;->getMeasuredHeight()I

    move-result v1

    .line 2258684
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v5

    .line 2258685
    const/4 v6, 0x2

    new-array v6, v6, [I

    .line 2258686
    invoke-virtual {p1, v6}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 2258687
    const/4 v7, 0x0

    aget v7, v6, v7

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    .line 2258688
    iput v3, p3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2258689
    const/4 v8, 0x1

    aget v6, v6, v8

    add-int/2addr v5, v6

    iput v5, p3, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 2258690
    iput v1, p3, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 2258691
    const/16 v1, 0x33

    iput v1, p3, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 2258692
    div-int/lit8 v1, v3, 0x2

    sub-int v1, v7, v1

    .line 2258693
    if-ge v1, v0, :cond_1

    .line 2258694
    :goto_0
    iget v1, p3, Landroid/view/ViewGroup$LayoutParams;->width:I

    add-int/2addr v0, v1

    iput v0, p3, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 2258695
    iget-object v0, p0, LX/FaN;->s:Landroid/view/WindowManager;

    iget-object v1, p0, LX/FaN;->q:Landroid/view/View;

    invoke-static {p0, p1}, LX/FaN;->g(LX/FaN;Landroid/view/View;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 2258696
    iget-boolean v0, p0, LX/FaN;->u:Z

    if-eqz v0, :cond_0

    .line 2258697
    iget-object v0, p0, LX/FaN;->p:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getMeasuredWidth()I

    move-result v1

    .line 2258698
    iget-object v0, p0, LX/FaN;->p:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v0}, Lcom/facebook/fbui/glyph/GlyphView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 2258699
    div-int/lit8 v1, v1, 0x2

    sub-int v1, v7, v1

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 2258700
    const v1, 0x800003

    iput v1, v0, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 2258701
    iget-object v1, p0, LX/FaN;->p:Lcom/facebook/fbui/glyph/GlyphView;

    invoke-virtual {v1, v0}, Lcom/facebook/fbui/glyph/GlyphView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2258702
    :cond_0
    return-void

    .line 2258703
    :cond_1
    add-int v0, v1, v3

    iget v5, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int/2addr v5, v2

    if-le v0, v5, :cond_2

    .line 2258704
    iget v0, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int/2addr v0, v2

    sub-int/2addr v0, v3

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 2258705
    iget-object v0, p0, LX/FaN;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2258706
    iget-object v1, p0, LX/FaN;->a:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2258707
    return-void

    .line 2258708
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 2258666
    iget-object v0, p0, LX/FaN;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(I)V

    .line 2258667
    iget-object v1, p0, LX/FaN;->l:Lcom/facebook/widget/text/BetterTextView;

    if-lez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2258668
    return-void

    .line 2258669
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 2258662
    iget-object v0, p0, LX/FaN;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-virtual {v0, p1}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2258663
    iget-object v1, p0, LX/FaN;->l:Lcom/facebook/widget/text/BetterTextView;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterTextView;->setVisibility(I)V

    .line 2258664
    return-void

    .line 2258665
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2258659
    iget-object v0, p0, LX/FaN;->r:LX/0Sh;

    new-instance v1, Lcom/facebook/search/quickpromotion/SearchAwarenessFormattedTooltip$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/search/quickpromotion/SearchAwarenessFormattedTooltip$1;-><init>(LX/FaN;Landroid/view/View;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 2258660
    invoke-super {p0, p1}, LX/0hs;->f(Landroid/view/View;)V

    .line 2258661
    return-void
.end method

.method public final l()V
    .locals 2

    .prologue
    .line 2258656
    invoke-super {p0}, LX/0hs;->l()V

    .line 2258657
    iget-object v0, p0, LX/FaN;->r:LX/0Sh;

    new-instance v1, Lcom/facebook/search/quickpromotion/SearchAwarenessFormattedTooltip$4;

    invoke-direct {v1, p0}, Lcom/facebook/search/quickpromotion/SearchAwarenessFormattedTooltip$4;-><init>(LX/FaN;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    .line 2258658
    return-void
.end method
