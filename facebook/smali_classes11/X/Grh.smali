.class public final LX/Grh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/68J;


# instance fields
.field public final synthetic a:LX/Gri;


# direct methods
.method public constructor <init>(LX/Gri;)V
    .locals 0

    .prologue
    .line 2398645
    iput-object p1, p0, LX/Grh;->a:LX/Gri;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(LX/680;)V
    .locals 6

    .prologue
    .line 2398646
    invoke-virtual {p1}, LX/680;->r()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2398647
    invoke-virtual {p1}, LX/680;->e()Landroid/location/Location;

    move-result-object v0

    .line 2398648
    if-eqz v0, :cond_0

    .line 2398649
    new-instance v1, Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/facebook/android/maps/model/LatLng;-><init>(DD)V

    const/high16 v0, 0x41700000    # 15.0f

    invoke-static {v1, v0}, LX/67e;->a(Lcom/facebook/android/maps/model/LatLng;F)LX/67d;

    move-result-object v0

    invoke-virtual {p1, v0}, LX/680;->a(LX/67d;)V

    .line 2398650
    :cond_0
    iget-object v0, p0, LX/Grh;->a:LX/Gri;

    iget-object v0, v0, LX/Gri;->a:LX/Grj;

    iget-object v0, v0, LX/Grj;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/D0y;

    iget-object v1, p0, LX/Grh;->a:LX/Gri;

    iget-object v1, v1, LX/Gri;->a:LX/Grj;

    iget-object v1, v1, LX/Grj;->c:Lcom/facebook/storelocator/StoreLocatorMapDelegate;

    invoke-virtual {v1}, Lcom/facebook/storelocator/StoreLocatorMapDelegate;->f()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/D0y;->c(Ljava/util/Map;)V

    .line 2398651
    :cond_1
    return-void
.end method
