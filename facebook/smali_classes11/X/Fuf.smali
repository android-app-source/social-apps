.class public LX/Fuf;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/Fue;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2300311
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2300312
    return-void
.end method


# virtual methods
.method public final a(LX/Fva;LX/BQ1;LX/G4m;LX/BP0;)LX/Fue;
    .locals 19

    .prologue
    .line 2300313
    new-instance v1, LX/Fue;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/Fsr;->a(LX/0QB;)LX/Fsr;

    move-result-object v4

    check-cast v4, LX/Fsr;

    const/16 v5, 0x369b

    move-object/from16 v0, p0

    invoke-static {v0, v5}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x455

    move-object/from16 v0, p0

    invoke-static {v0, v6}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    const/16 v7, 0x2eb

    move-object/from16 v0, p0

    invoke-static {v0, v7}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v7

    invoke-static/range {p0 .. p0}, LX/FwD;->a(LX/0QB;)LX/FwD;

    move-result-object v8

    check-cast v8, LX/FwD;

    const-class v9, LX/FrB;

    move-object/from16 v0, p0

    invoke-interface {v0, v9}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v9

    check-cast v9, LX/FrB;

    invoke-static/range {p0 .. p0}, LX/BQB;->a(LX/0QB;)LX/BQB;

    move-result-object v10

    check-cast v10, LX/BQB;

    invoke-static/range {p0 .. p0}, LX/8p8;->a(LX/0QB;)LX/8p8;

    move-result-object v11

    check-cast v11, LX/8p8;

    invoke-static/range {p0 .. p0}, LX/Fx5;->a(LX/0QB;)LX/Fx5;

    move-result-object v12

    check-cast v12, LX/Fx5;

    invoke-static/range {p0 .. p0}, LX/Fw7;->a(LX/0QB;)LX/Fw7;

    move-result-object v13

    check-cast v13, LX/Fw7;

    invoke-static/range {p0 .. p0}, LX/0oL;->a(LX/0QB;)Ljava/lang/Boolean;

    move-result-object v14

    check-cast v14, Ljava/lang/Boolean;

    move-object/from16 v15, p1

    move-object/from16 v16, p2

    move-object/from16 v17, p3

    move-object/from16 v18, p4

    invoke-direct/range {v1 .. v18}, LX/Fue;-><init>(Landroid/content/Context;LX/0ad;LX/Fsr;LX/0Or;LX/0Or;LX/0Or;LX/FwD;LX/FrB;LX/BQB;LX/8p8;LX/Fx5;LX/Fw7;Ljava/lang/Boolean;LX/Fva;LX/BQ1;LX/G4m;LX/BP0;)V

    .line 2300314
    return-object v1
.end method
