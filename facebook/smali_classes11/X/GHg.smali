.class public abstract LX/GHg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Landroid/view/View;",
        "D::",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesViewController",
        "<TT;TD;>;"
    }
.end annotation


# instance fields
.field public a:Z

.field public b:LX/GCE;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2335146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2335147
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GHg;->a:Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 2335144
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GHg;->a:Z

    .line 2335145
    return-void
.end method

.method public a(LX/GCE;)V
    .locals 0

    .prologue
    .line 2335142
    iput-object p1, p0, LX/GHg;->b:LX/GCE;

    .line 2335143
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2335141
    return-void
.end method

.method public a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 2
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2335137
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GHg;->a:Z

    .line 2335138
    iget-object v0, p0, LX/GHg;->b:LX/GCE;

    move-object v0, v0

    .line 2335139
    new-instance v1, LX/GLq;

    invoke-direct {v1, p0, p2}, LX/GLq;-><init>(LX/GHg;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wQ;)V

    .line 2335140
    return-void
.end method

.method public abstract a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)V"
        }
    .end annotation
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2335135
    return-void
.end method

.method public h_(Z)V
    .locals 0

    .prologue
    .line 2335136
    return-void
.end method
