.class public LX/GDp;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static g:LX/0Xm;


# instance fields
.field public a:LX/4At;

.field public b:LX/1Ck;

.field public c:LX/0tX;

.field public d:LX/GF4;

.field public e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field public f:LX/GNL;


# direct methods
.method public constructor <init>(LX/1Ck;LX/0tX;LX/GNL;LX/GF4;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2330568
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2330569
    iput-object p1, p0, LX/GDp;->b:LX/1Ck;

    .line 2330570
    iput-object p2, p0, LX/GDp;->c:LX/0tX;

    .line 2330571
    iput-object p4, p0, LX/GDp;->d:LX/GF4;

    .line 2330572
    iput-object p3, p0, LX/GDp;->f:LX/GNL;

    .line 2330573
    return-void
.end method

.method public static a(LX/0QB;)LX/GDp;
    .locals 7

    .prologue
    .line 2330574
    const-class v1, LX/GDp;

    monitor-enter v1

    .line 2330575
    :try_start_0
    sget-object v0, LX/GDp;->g:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2330576
    sput-object v2, LX/GDp;->g:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2330577
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2330578
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2330579
    new-instance p0, LX/GDp;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v3

    check-cast v3, LX/1Ck;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/GNL;->b(LX/0QB;)LX/GNL;

    move-result-object v5

    check-cast v5, LX/GNL;

    invoke-static {v0}, LX/GF4;->a(LX/0QB;)LX/GF4;

    move-result-object v6

    check-cast v6, LX/GF4;

    invoke-direct {p0, v3, v4, v5, v6}, LX/GDp;-><init>(LX/1Ck;LX/0tX;LX/GNL;LX/GF4;)V

    .line 2330580
    move-object v0, p0

    .line 2330581
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2330582
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GDp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2330583
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2330584
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
