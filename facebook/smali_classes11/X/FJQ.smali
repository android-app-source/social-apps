.class public final LX/FJQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;

.field public final synthetic b:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;

.field public c:Z


# direct methods
.method public constructor <init>(Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;)V
    .locals 1

    .prologue
    .line 2223387
    iput-object p1, p0, LX/FJQ;->b:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;

    iput-object p2, p0, LX/FJQ;->a:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2223388
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/FJQ;->c:Z

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, -0x4062331b

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v6

    .line 2223389
    iget-boolean v0, p0, LX/FJQ;->c:Z

    if-eqz v0, :cond_0

    .line 2223390
    const v0, -0x64c4cbdd

    invoke-static {v2, v2, v0, v6}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2223391
    :goto_0
    return-void

    .line 2223392
    :cond_0
    iget-object v0, p0, LX/FJQ;->b:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;

    iget-object v0, v0, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FJT;

    iget-object v1, p0, LX/FJQ;->b:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;

    invoke-virtual {v1}, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, LX/FJQ;->b:Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;

    iget-object v2, v2, Lcom/facebook/messaging/momentsinvite/ui/MomentsInviteView;->i:LX/6gC;

    iget-object v3, p0, LX/FJQ;->a:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;

    const/4 v4, 0x0

    new-instance v5, LX/FJP;

    invoke-direct {v5, p0}, LX/FJP;-><init>(LX/FJQ;)V

    invoke-virtual/range {v0 .. v5}, LX/FJT;->a(Landroid/content/Context;LX/6gC;Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;ZLX/FJK;)V

    .line 2223393
    const v0, 0x658c5e16

    invoke-static {v0, v6}, LX/02F;->a(II)V

    goto :goto_0
.end method
