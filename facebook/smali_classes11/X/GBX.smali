.class public final LX/GBX;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2327031
    iput-object p1, p0, LX/GBX;->b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iput-object p2, p0, LX/GBX;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 2

    .prologue
    .line 2327035
    iget-object v0, p0, LX/GBX;->b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->ds_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2327036
    :goto_0
    return-void

    .line 2327037
    :cond_0
    iget-object v0, p1, Lcom/facebook/fbservice/service/ServiceException;->errorCode:LX/1nY;

    move-object v0, v0

    .line 2327038
    sget-object v1, LX/1nY;->CONNECTION_FAILURE:LX/1nY;

    if-ne v0, v1, :cond_1

    .line 2327039
    iget-object v0, p0, LX/GBX;->b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    const/4 v1, 0x1

    .line 2327040
    iput-boolean v1, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->F:Z

    .line 2327041
    :goto_1
    iget-object v0, p0, LX/GBX;->b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    .line 2327042
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->b(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;Z)V

    .line 2327043
    iget-object v1, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->y:Lcom/facebook/ui/search/SearchEditText;

    if-nez v1, :cond_2

    .line 2327044
    invoke-static {v0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->o$redex0(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    .line 2327045
    :goto_2
    goto :goto_0

    .line 2327046
    :cond_1
    iget-object v0, p0, LX/GBX;->b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    invoke-static {v0}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->w$redex0(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    goto :goto_1

    .line 2327047
    :cond_2
    iget-boolean v1, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->F:Z

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->j:LX/2Dt;

    invoke-virtual {v1}, LX/2Dt;->e()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2327048
    new-instance v1, LX/0ju;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v1, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const p0, 0x7f08003f

    invoke-virtual {v0, p0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const p0, 0x7f083514

    invoke-virtual {v0, p0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, LX/0ju;->b(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const p0, 0x7f080016

    invoke-virtual {v0, p0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p0

    const/4 p1, 0x0

    invoke-virtual {v1, p0, p1}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    goto :goto_2

    .line 2327049
    :cond_3
    new-instance v1, LX/0ju;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-direct {v1, p0}, LX/0ju;-><init>(Landroid/content/Context;)V

    const p0, 0x7f083535

    invoke-virtual {v0, p0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, LX/0ju;->a(Ljava/lang/CharSequence;)LX/0ju;

    move-result-object v1

    const p0, 0x7f083536

    invoke-virtual {v0, p0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p0

    new-instance p1, LX/GBS;

    invoke-direct {p1, v0}, LX/GBS;-><init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    invoke-virtual {v1, p0, p1}, LX/0ju;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    const p0, 0x7f083537

    invoke-virtual {v0, p0}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p0

    new-instance p1, LX/GBR;

    invoke-direct {p1, v0}, LX/GBR;-><init>(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;)V

    invoke-virtual {v1, p0, p1}, LX/0ju;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)LX/0ju;

    move-result-object v1

    invoke-virtual {v1}, LX/0ju;->b()LX/2EJ;

    goto :goto_2
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2327032
    iget-object v0, p0, LX/GBX;->b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->b(Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;Z)V

    .line 2327033
    iget-object v0, p0, LX/GBX;->b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v0, v0, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->m:LX/2Ng;

    iget-object v1, p0, LX/GBX;->b:Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;

    iget-object v1, v1, Lcom/facebook/account/recovery/fragment/AccountConfirmFragment;->H:Ljava/lang/String;

    iget-object v2, p0, LX/GBX;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, LX/2Ng;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2327034
    return-void
.end method
