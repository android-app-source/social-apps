.class public final LX/G5y;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/wem/sahayta/protocol/UpdateMentorSelectionMutationModels$UpdateMentorSelectionMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/G5z;


# direct methods
.method public constructor <init>(LX/G5z;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2319584
    iput-object p1, p0, LX/G5y;->b:LX/G5z;

    iput-object p2, p0, LX/G5y;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2319585
    iget-object v0, p0, LX/G5y;->b:LX/G5z;

    iget-object v0, v0, LX/G5z;->d:LX/G60;

    iget-object v0, v0, LX/G60;->a:LX/G62;

    iget-object v1, p0, LX/G5y;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, LX/G62;->a(Ljava/lang/String;)V

    .line 2319586
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2319587
    iget-object v0, p0, LX/G5y;->b:LX/G5z;

    iget-object v0, v0, LX/G5z;->b:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f081567

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, LX/G5y;->b:LX/G5z;

    iget-object v4, v4, LX/G5z;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2319588
    iget-object v1, p0, LX/G5y;->b:LX/G5z;

    iget-object v1, v1, LX/G5z;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2319589
    return-void
.end method
