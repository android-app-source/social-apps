.class public LX/GNI;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Ljava/lang/Double;

.field private static volatile g:LX/GNI;


# instance fields
.field private final b:LX/GNn;

.field private final c:LX/GG3;

.field private final d:LX/GOy;

.field private e:Lcom/facebook/content/SecureContextHelper;

.field private f:LX/17W;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2345615
    const-wide v0, 0x3feccccccccccccdL    # 0.9

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sput-object v0, LX/GNI;->a:Ljava/lang/Double;

    return-void
.end method

.method public constructor <init>(LX/GNn;LX/GG3;LX/GOy;Lcom/facebook/content/SecureContextHelper;LX/17W;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2345703
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2345704
    iput-object p1, p0, LX/GNI;->b:LX/GNn;

    .line 2345705
    iput-object p2, p0, LX/GNI;->c:LX/GG3;

    .line 2345706
    iput-object p3, p0, LX/GNI;->d:LX/GOy;

    .line 2345707
    iput-object p4, p0, LX/GNI;->e:Lcom/facebook/content/SecureContextHelper;

    .line 2345708
    iput-object p5, p0, LX/GNI;->f:LX/17W;

    .line 2345709
    return-void
.end method

.method public static a(LX/0QB;)LX/GNI;
    .locals 9

    .prologue
    .line 2345690
    sget-object v0, LX/GNI;->g:LX/GNI;

    if-nez v0, :cond_1

    .line 2345691
    const-class v1, LX/GNI;

    monitor-enter v1

    .line 2345692
    :try_start_0
    sget-object v0, LX/GNI;->g:LX/GNI;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2345693
    if-eqz v2, :cond_0

    .line 2345694
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2345695
    new-instance v3, LX/GNI;

    const-class v4, LX/GNn;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/GNn;

    invoke-static {v0}, LX/GG3;->a(LX/0QB;)LX/GG3;

    move-result-object v5

    check-cast v5, LX/GG3;

    invoke-static {v0}, LX/GOy;->b(LX/0QB;)LX/GOy;

    move-result-object v6

    check-cast v6, LX/GOy;

    invoke-static {v0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v7

    check-cast v7, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v8

    check-cast v8, LX/17W;

    invoke-direct/range {v3 .. v8}, LX/GNI;-><init>(LX/GNn;LX/GG3;LX/GOy;Lcom/facebook/content/SecureContextHelper;LX/17W;)V

    .line 2345696
    move-object v0, v3

    .line 2345697
    sput-object v0, LX/GNI;->g:LX/GNI;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2345698
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2345699
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2345700
    :cond_1
    sget-object v0, LX/GNI;->g:LX/GNI;

    return-object v0

    .line 2345701
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2345702
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;LX/GCE;Landroid/content/Context;LX/GNI;LX/GDm;)Landroid/view/View$OnClickListener;
    .locals 6

    .prologue
    .line 2345688
    new-instance v3, LX/GNF;

    invoke-direct {v3, p1, p0, p4, p2}, LX/GNF;-><init>(LX/GCE;Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;LX/GDm;Landroid/content/Context;)V

    .line 2345689
    new-instance v0, LX/GNG;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/GNG;-><init>(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;LX/GCE;LX/GFR;LX/GNI;LX/GDm;)V

    return-object v0
.end method

.method public static a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;
    .locals 8

    .prologue
    .line 2345685
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    invoke-virtual {v0}, LX/8wL;->name()Ljava/lang/String;

    move-result-object v0

    .line 2345686
    new-instance v1, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    invoke-static {p1}, LX/GNI;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, LX/6xj;->PICKER_SCREEN:LX/6xj;

    invoke-static {p0, p1}, LX/GNI;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Lcom/facebook/payments/currency/CurrencyAmount;

    move-result-object v5

    invoke-static {p0}, LX/GMU;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v6, 0x1

    :goto_0
    invoke-static {p1}, LX/GNI;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)LX/ADX;

    move-result-object v7

    move-object v2, v0

    invoke-direct/range {v1 .. v7}, Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;-><init>(Ljava/lang/String;Ljava/lang/String;LX/6xj;Lcom/facebook/payments/currency/CurrencyAmount;ZLX/ADX;)V

    move-object v0, v1

    .line 2345687
    return-object v0

    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2345676
    instance-of v3, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-nez v3, :cond_1

    .line 2345677
    :cond_0
    :goto_0
    return-object v0

    .line 2345678
    :cond_1
    check-cast p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2345679
    iget-object v3, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v3, v3

    .line 2345680
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->r()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;

    move-result-object v3

    .line 2345681
    if-nez v3, :cond_3

    :cond_2
    :goto_1
    if-nez v1, :cond_0

    .line 2345682
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->m()LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-virtual {v1, v0, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2345683
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel$DefaultSpecModel;->m()LX/1vs;

    move-result-object v4

    iget v4, v4, LX/1vs;->b:I

    .line 2345684
    if-eqz v4, :cond_2

    move v1, v2

    goto :goto_1
.end method

.method public static a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2345675
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->t()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Intent;LX/GCE;)V
    .locals 3

    .prologue
    .line 2345673
    new-instance v0, LX/GFS;

    sget v1, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->u:I

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, LX/GFS;-><init>(Landroid/content/Intent;IZ)V

    invoke-virtual {p1, v0}, LX/GCE;->a(LX/8wN;)V

    .line 2345674
    return-void
.end method

.method public static a(Lcom/facebook/adspayments/analytics/PaymentsFlowContext;LX/GCE;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;Landroid/content/Context;Lcom/facebook/common/locale/Country;Z)V
    .locals 2

    .prologue
    .line 2345668
    invoke-static {p2}, LX/GNI;->f(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Ljava/lang/String;

    move-result-object v0

    .line 2345669
    const-string v1, "BRL"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;->s:Lcom/facebook/common/locale/Country;

    invoke-virtual {p4, v0}, Lcom/facebook/common/locale/Country;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2345670
    sget-object p4, Lcom/facebook/common/locale/Country;->a:Lcom/facebook/common/locale/Country;

    .line 2345671
    :cond_0
    invoke-static {p3, p0, p4, p5}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0, p1}, LX/GNI;->a(Landroid/content/Intent;LX/GCE;)V

    .line 2345672
    return-void
.end method

.method public static b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)LX/ADX;
    .locals 1

    .prologue
    .line 2345667
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->z()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/ADX;->of(Ljava/lang/String;)LX/ADX;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Lcom/facebook/payments/currency/CurrencyAmount;
    .locals 6

    .prologue
    .line 2345665
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    .line 2345666
    new-instance v1, Lcom/facebook/payments/currency/CurrencyAmount;

    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v0

    invoke-static {v4, v5, v0}, Lcom/facebook/payments/currency/CurrencyAmount;->a(JI)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/facebook/payments/currency/CurrencyAmount;-><init>(Ljava/lang/String;Ljava/math/BigDecimal;)V

    return-object v1
.end method

.method public static c(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Z
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 2345602
    if-nez p0, :cond_1

    move v2, v1

    :goto_0
    if-eqz v2, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_5

    move v2, v1

    :goto_2
    if-eqz v2, :cond_7

    .line 2345603
    :cond_0
    :goto_3
    return v0

    .line 2345604
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->y()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-nez v2, :cond_2

    move v2, v1

    goto :goto_0

    :cond_2
    move v2, v0

    goto :goto_0

    .line 2345605
    :cond_3
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->y()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2345606
    invoke-virtual {v3, v2, v0}, LX/15i;->g(II)I

    move-result v2

    if-nez v2, :cond_4

    move v2, v1

    goto :goto_1

    :cond_4
    move v2, v0

    goto :goto_1

    .line 2345607
    :cond_5
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->y()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2345608
    invoke-virtual {v3, v2, v1}, LX/15i;->g(II)I

    move-result v2

    if-nez v2, :cond_6

    move v2, v1

    goto :goto_2

    :cond_6
    move v2, v0

    goto :goto_2

    .line 2345609
    :cond_7
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->y()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    invoke-virtual {v3, v2, v0}, LX/15i;->g(II)I

    move-result v2

    .line 2345610
    invoke-virtual {v3, v2, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    .line 2345611
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->y()LX/1vs;

    move-result-object v3

    iget-object v4, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    invoke-virtual {v4, v3, v1}, LX/15i;->g(II)I

    move-result v3

    .line 2345612
    invoke-virtual {v4, v3, v5}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 2345613
    invoke-virtual {v2, v3}, Ljava/lang/Long;->compareTo(Ljava/lang/Long;)I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget-object v2, LX/GNI;->a:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-double v2, v2

    mul-double/2addr v2, v6

    double-to-long v2, v2

    cmp-long v2, v4, v2

    if-ltz v2, :cond_0

    move v0, v1

    .line 2345614
    goto :goto_3
.end method

.method public static e(LX/GNI;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Lcom/facebook/common/locale/Country;
    .locals 2

    .prologue
    .line 2345664
    iget-object v0, p0, LX/GNI;->d:LX/GOy;

    invoke-static {p1}, LX/GNI;->g(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Lcom/facebook/common/locale/Country;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GOy;->a(Lcom/facebook/common/locale/Country;)Lcom/facebook/common/locale/Country;

    move-result-object v0

    return-object v0
.end method

.method private static f(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2345663
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static g(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Lcom/facebook/common/locale/Country;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2345658
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->j()LX/1vs;

    move-result-object v1

    iget v1, v1, LX/1vs;->b:I

    if-nez v1, :cond_1

    .line 2345659
    :cond_0
    :goto_0
    return-object v0

    .line 2345660
    :cond_1
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    .line 2345661
    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2345662
    invoke-static {v1}, Lcom/facebook/common/locale/Country;->a(Ljava/lang/String;)Lcom/facebook/common/locale/Country;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/GCE;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;LX/0gc;Landroid/content/Context;Z)V
    .locals 6

    .prologue
    .line 2345636
    iget-object v0, p0, LX/GNI;->c:LX/GG3;

    invoke-virtual {v0, p2}, LX/GG3;->g(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2345637
    invoke-static {p2, p3}, LX/GNI;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    move-result-object v0

    .line 2345638
    iput-object v0, p1, LX/GCE;->k:Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    .line 2345639
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2345640
    invoke-virtual {p3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->z()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/ADX;->of(Ljava/lang/String;)LX/ADX;

    move-result-object v3

    invoke-virtual {v3}, LX/ADX;->isPUX()Z

    move-result v3

    if-nez v3, :cond_0

    move v3, v1

    .line 2345641
    :goto_0
    if-eqz v3, :cond_1

    sget-object v3, Lcom/facebook/adspayments/activity/BrazilianAdsPaymentsActivity;->s:Lcom/facebook/common/locale/Country;

    invoke-static {p3}, LX/GNI;->g(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Lcom/facebook/common/locale/Country;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/common/locale/Country;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    :goto_1
    move v1, v1

    .line 2345642
    invoke-static {p3}, LX/GNI;->g(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Lcom/facebook/common/locale/Country;

    move-result-object v2

    .line 2345643
    iget-object v3, p0, LX/GNI;->d:LX/GOy;

    invoke-virtual {v3, v0, v1, v2}, LX/GOy;->a(Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;ZLcom/facebook/common/locale/Country;)V

    .line 2345644
    invoke-static {p0, p3}, LX/GNI;->e(LX/GNI;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Lcom/facebook/common/locale/Country;

    move-result-object v4

    .line 2345645
    iget-object v3, p0, LX/GNI;->d:LX/GOy;

    invoke-static {p3}, LX/GNI;->f(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v1, v2, v5}, LX/GOy;->a(ZLcom/facebook/common/locale/Country;Ljava/lang/String;)LX/GOx;

    move-result-object v1

    .line 2345646
    sget-object v2, LX/GNH;->a:[I

    invoke-virtual {v1}, LX/GOx;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 2345647
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "%s not a valid %s"

    const-class v3, LX/GOx;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    move-object v1, p1

    move-object v2, p3

    move-object v3, p5

    move v5, p6

    .line 2345648
    invoke-static/range {v0 .. v5}, LX/GNI;->a(Lcom/facebook/adspayments/analytics/PaymentsFlowContext;LX/GCE;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;Landroid/content/Context;Lcom/facebook/common/locale/Country;Z)V

    .line 2345649
    :goto_2
    return-void

    .line 2345650
    :pswitch_1
    iget-object v1, p0, LX/GNI;->b:LX/GNn;

    .line 2345651
    new-instance v5, LX/GNm;

    invoke-static {v1}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v2

    check-cast v2, LX/0tX;

    invoke-static {v1}, LX/ADW;->a(LX/0QB;)LX/ADW;

    move-result-object v3

    check-cast v3, LX/ADW;

    invoke-direct {v5, p4, v2, v3}, LX/GNm;-><init>(LX/0gc;LX/0tX;LX/ADW;)V

    .line 2345652
    move-object v1, v5

    .line 2345653
    const-string v2, "add_payment_method_tag"

    const/4 v3, 0x3

    invoke-virtual {v1, v0, v4, v2, v3}, LX/GNm;->a(Lcom/facebook/adspayments/analytics/PaymentsFlowContext;Lcom/facebook/common/locale/Country;Ljava/lang/String;I)V

    goto :goto_2

    .line 2345654
    :pswitch_2
    invoke-static {p5, v0}, Lcom/facebook/adspayments/activity/BrazilianTaxIdActivity;->a(Landroid/content/Context;Lcom/facebook/adspayments/analytics/PaymentsFlowContext;)Landroid/content/Intent;

    move-result-object v1

    .line 2345655
    iget-object v2, p0, LX/GNI;->e:Lcom/facebook/content/SecureContextHelper;

    const/16 v3, 0x12d

    const-class v0, Landroid/app/Activity;

    invoke-static {p5, v0}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-interface {v2, v1, v3, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    goto :goto_2

    :cond_0
    move v3, v2

    .line 2345656
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2345657
    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;LX/GCE;Landroid/content/Context;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 2345616
    iget-object v0, p3, LX/GCE;->k:Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    move-object v0, v0

    .line 2345617
    if-nez v0, :cond_0

    .line 2345618
    invoke-static {p2, p1}, LX/GNI;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;)Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    move-result-object v0

    .line 2345619
    iput-object v0, p3, LX/GCE;->k:Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    .line 2345620
    :cond_0
    iget-object v0, p3, LX/GCE;->k:Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

    move-object v4, v0

    .line 2345621
    invoke-static {p2}, LX/GNI;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;

    move-result-object v0

    .line 2345622
    iget-object v1, p3, LX/GCE;->a:Ljava/lang/String;

    move-object v1, v1

    .line 2345623
    iget-object v2, p3, LX/GCE;->b:Ljava/lang/String;

    move-object v2, v2

    .line 2345624
    sget-object v5, LX/0ax;->v:Ljava/lang/String;

    const/16 v6, 0xe

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 2345625
    iget-object v8, v4, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->d:Ljava/lang/String;

    move-object v8, v8

    .line 2345626
    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-interface {p2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->c()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    if-nez v0, :cond_1

    const-string v0, ""

    :cond_1
    aput-object v0, v6, v7

    const/4 v0, 0x3

    const-string v7, ""

    aput-object v7, v6, v0

    const/4 v0, 0x4

    .line 2345627
    invoke-interface {p2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;->p()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->j()LX/0Px;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;

    invoke-virtual {v7}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->j()Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentEstimateType;->toString()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    move-object v7, v7

    .line 2345628
    aput-object v7, v6, v0

    const/4 v0, 0x5

    invoke-interface {p2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->g()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;->a()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v0

    const/4 v0, 0x6

    invoke-interface {p2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->g()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;->j()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v0

    const/4 v0, 0x7

    invoke-interface {p2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->i()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v0

    const/16 v0, 0x8

    invoke-interface {p2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v0

    const/16 v0, 0x9

    invoke-interface {p2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->h()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v0

    const/16 v0, 0xa

    .line 2345629
    check-cast p2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2345630
    iget-object v7, p2, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v7, v7

    .line 2345631
    invoke-virtual {v7}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->o()Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;->toString()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v7, v8}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    move-object v7, v7

    .line 2345632
    aput-object v7, v6, v0

    const/16 v7, 0xb

    if-nez v1, :cond_2

    const-string v0, ""

    :goto_0
    aput-object v0, v6, v7

    const/16 v1, 0xc

    if-nez v2, :cond_3

    const-string v0, ""

    :goto_1
    aput-object v0, v6, v1

    const/16 v0, 0xd

    invoke-virtual {v4}, Lcom/facebook/adspayments/analytics/PaymentsFlowContext;->h()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v6, v0

    invoke-static {v5, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2345633
    iget-object v0, p0, LX/GNI;->f:LX/17W;

    sget v5, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->y:I

    const-class v1, Landroid/app/Activity;

    invoke-static {p4, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/app/Activity;

    move-object v1, p4

    move-object v4, v3

    invoke-virtual/range {v0 .. v6}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;ILandroid/app/Activity;)Z

    .line 2345634
    return-void

    :cond_2
    move-object v0, v1

    .line 2345635
    goto :goto_0

    :cond_3
    move-object v0, v2

    goto :goto_1
.end method
