.class public final LX/GOv;
.super LX/GNw;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GNw",
        "<",
        "Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;)V
    .locals 0

    .prologue
    .line 2348050
    iput-object p1, p0, LX/GOv;->a:Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;

    invoke-direct {p0, p1}, LX/GNw;-><init>(Lcom/facebook/adspayments/activity/AdsPaymentsActivity;)V

    return-void
.end method

.method private a(Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;)V
    .locals 2

    .prologue
    .line 2348051
    iget-object v0, p0, LX/GOv;->a:Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;

    invoke-virtual {v0}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->u()V

    .line 2348052
    if-eqz p1, :cond_0

    .line 2348053
    iget-object v0, p0, LX/GOv;->a:Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;

    const-string v1, "add_paypal"

    invoke-virtual {v0, p1, v1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->a(Lcom/facebook/payments/paymentmethods/model/PaymentOption;Ljava/lang/String;)V

    .line 2348054
    iget-object v0, p0, LX/GOv;->a:Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;

    invoke-virtual {v0, p1}, Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;->a(Lcom/facebook/payments/paymentmethods/model/PaymentOption;)V

    .line 2348055
    :goto_0
    return-void

    .line 2348056
    :cond_0
    iget-object v0, p0, LX/GOv;->a:Lcom/facebook/adspayments/activity/SelectPaymentOptionActivity;

    const-string v1, "add_paypal"

    invoke-virtual {v0, v1}, Lcom/facebook/adspayments/activity/AdsPaymentsActivity;->c(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2348057
    check-cast p1, Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;

    invoke-direct {p0, p1}, LX/GOv;->a(Lcom/facebook/payments/paymentmethods/model/PayPalBillingAgreement;)V

    return-void
.end method
