.class public LX/FKo;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FKo;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225540
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2225541
    return-void
.end method

.method public static a(Lcom/facebook/user/model/UserIdentifier;)LX/0lF;
    .locals 7

    .prologue
    .line 2225558
    instance-of v0, p0, Lcom/facebook/user/model/UserSmsIdentifier;

    if-eqz v0, :cond_0

    .line 2225559
    check-cast p0, Lcom/facebook/user/model/UserSmsIdentifier;

    .line 2225560
    new-instance v1, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 2225561
    const-string v0, "type"

    const-string v2, "phone"

    invoke-virtual {v1, v0, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2225562
    invoke-virtual {p0}, Lcom/facebook/user/model/UserSmsIdentifier;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XI;->p(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2225563
    if-nez v0, :cond_1

    .line 2225564
    invoke-virtual {p0}, Lcom/facebook/user/model/UserSmsIdentifier;->a()Ljava/lang/String;

    move-result-object v0

    .line 2225565
    :goto_0
    const-string v2, "phone"

    invoke-virtual {v1, v2, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2225566
    move-object v0, v1

    .line 2225567
    :goto_1
    return-object v0

    .line 2225568
    :cond_0
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 2225569
    const-string v1, "type"

    const-string v2, "id"

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2225570
    const-string v1, "id"

    invoke-interface {p0}, Lcom/facebook/user/model/UserIdentifier;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_1

    .line 2225571
    :cond_1
    const-class v2, LX/FKo;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "%s is not an E164 phone number."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/facebook/user/model/UserSmsIdentifier;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, LX/01m;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)LX/0lF;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/facebook/user/model/UserIdentifier;",
            ">;)",
            "LX/0lF;"
        }
    .end annotation

    .prologue
    .line 2225554
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 2225555
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/UserIdentifier;

    .line 2225556
    invoke-static {v0}, LX/FKo;->a(Lcom/facebook/user/model/UserIdentifier;)LX/0lF;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_0

    .line 2225557
    :cond_0
    return-object v1
.end method

.method public static a(LX/0QB;)LX/FKo;
    .locals 3

    .prologue
    .line 2225542
    sget-object v0, LX/FKo;->a:LX/FKo;

    if-nez v0, :cond_1

    .line 2225543
    const-class v1, LX/FKo;

    monitor-enter v1

    .line 2225544
    :try_start_0
    sget-object v0, LX/FKo;->a:LX/FKo;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2225545
    if-eqz v2, :cond_0

    .line 2225546
    :try_start_1
    new-instance v0, LX/FKo;

    invoke-direct {v0}, LX/FKo;-><init>()V

    .line 2225547
    move-object v0, v0

    .line 2225548
    sput-object v0, LX/FKo;->a:LX/FKo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2225549
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2225550
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2225551
    :cond_1
    sget-object v0, LX/FKo;->a:LX/FKo;

    return-object v0

    .line 2225552
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2225553
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
