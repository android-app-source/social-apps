.class public LX/FbU;
.super LX/FbD;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FbD",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsEmptyUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FbU;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2260349
    invoke-direct {p0}, LX/FbD;-><init>()V

    .line 2260350
    return-void
.end method

.method public static a(LX/0QB;)LX/FbU;
    .locals 3

    .prologue
    .line 2260351
    sget-object v0, LX/FbU;->a:LX/FbU;

    if-nez v0, :cond_1

    .line 2260352
    const-class v1, LX/FbU;

    monitor-enter v1

    .line 2260353
    :try_start_0
    sget-object v0, LX/FbU;->a:LX/FbU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2260354
    if-eqz v2, :cond_0

    .line 2260355
    :try_start_1
    new-instance v0, LX/FbU;

    invoke-direct {v0}, LX/FbU;-><init>()V

    .line 2260356
    move-object v0, v0

    .line 2260357
    sput-object v0, LX/FbU;->a:LX/FbU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2260358
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2260359
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2260360
    :cond_1
    sget-object v0, LX/FbU;->a:LX/FbU;

    return-object v0

    .line 2260361
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2260362
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
    .locals 2
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260363
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->q()Ljava/lang/String;

    move-result-object v1

    .line 2260364
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/facebook/search/results/model/unit/SearchResultsEmptyUnit;

    invoke-direct {v0, v1}, Lcom/facebook/search/results/model/unit/SearchResultsEmptyUnit;-><init>(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
