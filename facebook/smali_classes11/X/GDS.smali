.class public LX/GDS;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1Ck;

.field private b:LX/2U3;

.field public final c:LX/0tX;


# direct methods
.method public constructor <init>(LX/0tX;LX/1Ck;LX/2U3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2329822
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2329823
    iput-object p1, p0, LX/GDS;->c:LX/0tX;

    .line 2329824
    iput-object p2, p0, LX/GDS;->a:LX/1Ck;

    .line 2329825
    iput-object p3, p0, LX/GDS;->b:LX/2U3;

    .line 2329826
    return-void
.end method

.method public static a$redex0(LX/GDS;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2329827
    iget-object v0, p0, LX/GDS;->b:LX/2U3;

    const-class v1, LX/GDS;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "GraphQL error"

    invoke-virtual {v0, v1, v2, p1}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2329828
    return-void
.end method

.method public static b(LX/0QB;)LX/GDS;
    .locals 4

    .prologue
    .line 2329829
    new-instance v3, LX/GDS;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v2

    check-cast v2, LX/2U3;

    invoke-direct {v3, v0, v1, v2}, LX/GDS;-><init>(LX/0tX;LX/1Ck;LX/2U3;)V

    .line 2329830
    return-object v3
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2329831
    iget-object v0, p0, LX/GDS;->a:LX/1Ck;

    sget-object v1, LX/GDR;->TASK_REVERSE_GEOCODE:LX/GDR;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2329832
    iget-object v0, p0, LX/GDS;->a:LX/1Ck;

    sget-object v1, LX/GDR;->TASK_GEOCODE_ADDRESS:LX/GDR;

    invoke-virtual {v0, v1}, LX/1Ck;->c(Ljava/lang/Object;)V

    .line 2329833
    return-void
.end method

.method public final a(Ljava/lang/String;LX/GDQ;)V
    .locals 4

    .prologue
    .line 2329834
    new-instance v0, LX/4FN;

    invoke-direct {v0}, LX/4FN;-><init>()V

    new-instance v1, LX/4FL;

    invoke-direct {v1}, LX/4FL;-><init>()V

    invoke-virtual {v1, p1}, LX/4FL;->a(Ljava/lang/String;)LX/4FL;

    move-result-object v1

    invoke-static {v1}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4FN;->a(Ljava/util/List;)LX/4FN;

    move-result-object v0

    .line 2329835
    new-instance v1, LX/A8x;

    invoke-direct {v1}, LX/A8x;-><init>()V

    move-object v1, v1

    .line 2329836
    const-string v2, "addresses"

    invoke-virtual {v1, v2, v0}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v0

    check-cast v0, LX/A8x;

    invoke-static {v0}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    .line 2329837
    iget-object v1, p0, LX/GDS;->a:LX/1Ck;

    sget-object v2, LX/GDR;->TASK_GEOCODE_ADDRESS:LX/GDR;

    iget-object v3, p0, LX/GDS;->c:LX/0tX;

    invoke-virtual {v3, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    new-instance v3, LX/GDO;

    invoke-direct {v3, p0, p2}, LX/GDO;-><init>(LX/GDS;LX/GDQ;)V

    invoke-virtual {v1, v2, v0, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2329838
    return-void
.end method
