.class public LX/Gtt;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

.field private static final c:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;


# instance fields
.field public d:LX/3DA;

.field public e:LX/1ro;

.field public f:LX/Gto;

.field public g:LX/0SI;

.field private h:LX/2Yg;

.field public i:LX/0Sh;

.field public j:Landroid/content/Context;

.field public k:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0iA;",
            ">;"
        }
    .end annotation
.end field

.field private l:LX/2iv;

.field public m:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

.field public n:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

.field public o:I

.field private p:Ljava/lang/String;

.field public q:Z

.field public final r:LX/95a;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/95a",
            "<",
            "Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;",
            "LX/3DK;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2402968
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "com.facebook.katana.activity.ImmersiveActivity"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "com.facebook.katana.activity.react.ImmersiveReactActivity"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, LX/Gtt;->a:Ljava/util/HashSet;

    .line 2402969
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const v1, 0x7f0207ee

    .line 2402970
    iput v1, v0, LX/108;->i:I

    .line 2402971
    move-object v0, v0

    .line 2402972
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    sput-object v0, LX/Gtt;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    .line 2402973
    invoke-static {}, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->a()LX/108;

    move-result-object v0

    const v1, 0x7f0207df

    .line 2402974
    iput v1, v0, LX/108;->i:I

    .line 2402975
    move-object v0, v0

    .line 2402976
    invoke-virtual {v0}, LX/108;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    sput-object v0, LX/Gtt;->c:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    return-void
.end method

.method public constructor <init>(LX/3DA;LX/1ro;LX/0SI;LX/2Yg;LX/0Sh;LX/0Or;Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;LX/2iv;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/notifications/util/NotificationStoryLauncher;",
            "LX/1ro;",
            "LX/0SI;",
            "LX/2Yg;",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            "LX/0Or",
            "<",
            "LX/0iA;",
            ">;",
            "Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;",
            "LX/2iv;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2402957
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2402958
    new-instance v0, LX/Gtq;

    invoke-direct {v0, p0}, LX/Gtq;-><init>(LX/Gtt;)V

    iput-object v0, p0, LX/Gtt;->r:LX/95a;

    .line 2402959
    iput-object p7, p0, LX/Gtt;->m:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    .line 2402960
    iput-object p8, p0, LX/Gtt;->l:LX/2iv;

    .line 2402961
    iput-object p1, p0, LX/Gtt;->d:LX/3DA;

    .line 2402962
    iput-object p3, p0, LX/Gtt;->g:LX/0SI;

    .line 2402963
    iput-object p4, p0, LX/Gtt;->h:LX/2Yg;

    .line 2402964
    iput-object p5, p0, LX/Gtt;->i:LX/0Sh;

    .line 2402965
    iput-object p6, p0, LX/Gtt;->k:LX/0Or;

    .line 2402966
    iput-object p2, p0, LX/Gtt;->e:LX/1ro;

    .line 2402967
    return-void
.end method

.method private static a(LX/Gtt;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 2402954
    invoke-direct {p0}, LX/Gtt;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2402955
    const/4 v0, -0x1

    .line 2402956
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/Gtt;->l:LX/2iv;

    invoke-virtual {v0, p1}, LX/2iv;->a(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public static a$redex0(LX/Gtt;I)V
    .locals 7

    .prologue
    .line 2402911
    invoke-static {p0, p1}, LX/Gtt;->c(LX/Gtt;I)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    .line 2402912
    if-nez v0, :cond_0

    .line 2402913
    :goto_0
    return-void

    .line 2402914
    :cond_0
    invoke-static {p0, p1}, LX/Gtt;->c(LX/Gtt;I)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 2402915
    if-nez v2, :cond_5

    .line 2402916
    :cond_1
    :goto_1
    iget-object v1, p0, LX/Gtt;->d:LX/3DA;

    iget-object v2, p0, LX/Gtt;->j:Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    .line 2402917
    iget-object v4, v3, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v4

    .line 2402918
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2402919
    invoke-static {v1, v4, p1}, LX/3DA;->a(LX/3DA;Lcom/facebook/graphql/model/GraphQLStory;I)LX/BDx;

    move-result-object v5

    .line 2402920
    if-eqz v5, :cond_8

    iget-object v6, v5, LX/BDx;->a:Landroid/os/Bundle;

    if-eqz v6, :cond_8

    .line 2402921
    iget-object v4, v1, LX/3DA;->j:LX/17Y;

    .line 2402922
    iget-object v6, v5, LX/BDx;->b:Ljava/lang/String;

    move-object v6, v6

    .line 2402923
    invoke-interface {v4, v2, v6}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 2402924
    if-nez v4, :cond_7

    .line 2402925
    const/4 v4, 0x0

    .line 2402926
    :goto_2
    move-object v1, v4

    .line 2402927
    const/4 v2, 0x0

    .line 2402928
    if-nez v1, :cond_9

    .line 2402929
    :cond_2
    :goto_3
    move v2, v2

    .line 2402930
    if-eqz v2, :cond_4

    .line 2402931
    iput p1, p0, LX/Gtt;->o:I

    .line 2402932
    iget v2, p0, LX/Gtt;->o:I

    invoke-static {p0}, LX/Gtt;->f(LX/Gtt;)I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    if-le v2, v3, :cond_3

    .line 2402933
    iget-object v2, p0, LX/Gtt;->m:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    iget-object v3, p0, LX/Gtt;->g:LX/0SI;

    invoke-interface {v3}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b(Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 2402934
    :cond_3
    sget-object v2, LX/3B2;->next_notification_loaded:LX/3B2;

    invoke-static {p0, v2, p1}, LX/Gtt;->a$redex0(LX/Gtt;LX/3B2;I)V

    .line 2402935
    invoke-static {p0}, LX/Gtt;->e(LX/Gtt;)V

    .line 2402936
    iget-object v2, p0, LX/Gtt;->f:LX/Gto;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, LX/63U;->j_(Z)V

    .line 2402937
    iget-object v2, p0, LX/Gtt;->n:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const v3, 0x7f0a00d1

    invoke-virtual {v2, v3}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setBackgroundResource(I)V

    .line 2402938
    iget-object v2, p0, LX/Gtt;->f:LX/Gto;

    invoke-virtual {v2, v1}, LX/Gto;->a_(Landroid/content/Intent;)V

    .line 2402939
    :goto_4
    goto :goto_0

    .line 2402940
    :cond_4
    sget-object v1, LX/3B2;->next_notification_fallback:LX/3B2;

    invoke-static {p0, v1, p1}, LX/Gtt;->a$redex0(LX/Gtt;LX/3B2;I)V

    .line 2402941
    iget-object v1, p0, LX/Gtt;->d:LX/3DA;

    iget-object v2, p0, LX/Gtt;->j:Landroid/content/Context;

    invoke-static {v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->c(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v3

    invoke-virtual {v1, v2, v3, p1}, LX/3DA;->a(Landroid/content/Context;Lcom/facebook/feed/rows/core/props/FeedProps;I)Z

    goto :goto_4

    .line 2402942
    :cond_5
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x1

    .line 2402943
    :goto_5
    if-eqz v1, :cond_1

    .line 2402944
    iget-object v1, p0, LX/Gtt;->e:LX/1ro;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/1ro;->a(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 2402945
    :cond_6
    const/4 v1, 0x0

    goto :goto_5

    .line 2402946
    :cond_7
    iget-object v5, v5, LX/BDx;->a:Landroid/os/Bundle;

    invoke-virtual {v4, v5}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    goto :goto_2

    .line 2402947
    :cond_8
    invoke-static {v1, v4, p1}, LX/3DA;->b(LX/3DA;Lcom/facebook/graphql/model/GraphQLStory;I)Landroid/content/Intent;

    move-result-object v4

    goto :goto_2

    .line 2402948
    :cond_9
    iget-object v3, p0, LX/Gtt;->j:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 2402949
    if-eqz v3, :cond_2

    .line 2402950
    invoke-virtual {v1, v3}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v3

    .line 2402951
    if-eqz v3, :cond_a

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    .line 2402952
    :goto_6
    if-eqz v3, :cond_2

    sget-object v2, LX/Gtt;->a:Ljava/util/HashSet;

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    goto/16 :goto_3

    .line 2402953
    :cond_a
    const/4 v3, 0x0

    goto :goto_6
.end method

.method public static a$redex0(LX/Gtt;LX/3B2;I)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2402892
    new-instance v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    invoke-direct {v1}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;-><init>()V

    .line 2402893
    iput-boolean v0, v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->j:Z

    .line 2402894
    move-object v1, v1

    .line 2402895
    iput p2, v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->u:I

    .line 2402896
    move-object v1, v1

    .line 2402897
    invoke-static {p0, p2}, LX/Gtt;->c(LX/Gtt;I)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    .line 2402898
    if-eqz v2, :cond_0

    .line 2402899
    sget-object v3, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->SEEN_AND_READ:Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/facebook/graphql/enums/GraphQLStorySeenState;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 2402900
    :goto_0
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->c()Ljava/lang/String;

    move-result-object v3

    .line 2402901
    iput-object v3, v1, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->i:Ljava/lang/String;

    .line 2402902
    move-object v3, v1

    .line 2402903
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLStory;->aF()Lcom/facebook/graphql/enums/GraphQLStorySeenState;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->a(Lcom/facebook/graphql/enums/GraphQLStorySeenState;)Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;

    move-result-object v2

    .line 2402904
    iput-boolean v0, v2, Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;->k:Z

    .line 2402905
    :cond_0
    iget-object v0, p0, LX/Gtt;->h:LX/2Yg;

    .line 2402906
    if-nez v1, :cond_2

    .line 2402907
    :goto_1
    return-void

    .line 2402908
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2402909
    :cond_2
    iget-object v2, v0, LX/2Yg;->e:LX/0ii;

    invoke-virtual {v2, v1}, LX/0ii;->a(Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    .line 2402910
    iget-object v2, v0, LX/2Yg;->b:LX/0Zb;

    new-instance v3, Lcom/facebook/notifications/logging/NotificationsLogger$NextNotificationEvent;

    invoke-direct {v3, v0, p1, v1}, Lcom/facebook/notifications/logging/NotificationsLogger$NextNotificationEvent;-><init>(LX/2Yg;LX/3B2;Lcom/facebook/notifications/logging/NotificationsLogger$NotificationLogObject;)V

    invoke-interface {v2, v3}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    goto :goto_1
.end method

.method public static c(LX/Gtt;I)Lcom/facebook/graphql/model/GraphQLStory;
    .locals 1

    .prologue
    .line 2402891
    invoke-direct {p0}, LX/Gtt;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Gtt;->l:LX/2iv;

    invoke-virtual {v0, p1}, LX/2iv;->b(I)Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/notifications/protocol/FetchNotificationsGraphQLModelsWrapper$NotificationsEdgeFieldsModel;->m()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(I)Z
    .locals 1

    .prologue
    .line 2402890
    if-ltz p1, :cond_0

    invoke-static {p0}, LX/Gtt;->f(LX/Gtt;)I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(LX/Gtt;)V
    .locals 2

    .prologue
    .line 2402882
    iget-object v0, p0, LX/Gtt;->n:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSearchButtonVisible(Z)V

    .line 2402883
    iget v0, p0, LX/Gtt;->o:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, LX/Gtt;->d(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2402884
    iget-object v0, p0, LX/Gtt;->n:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    sget-object v1, LX/Gtt;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2402885
    :goto_0
    iget v0, p0, LX/Gtt;->o:I

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, LX/Gtt;->d(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2402886
    iget-object v0, p0, LX/Gtt;->n:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    sget-object v1, LX/Gtt;->c:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSecondaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    .line 2402887
    :goto_1
    return-void

    .line 2402888
    :cond_0
    iget-object v0, p0, LX/Gtt;->n:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    sget-object v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setPrimaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto :goto_0

    .line 2402889
    :cond_1
    iget-object v0, p0, LX/Gtt;->n:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    sget-object v1, Lcom/facebook/widget/titlebar/TitleBarButtonSpec;->b:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSecondaryButton(Lcom/facebook/widget/titlebar/TitleBarButtonSpec;)V

    goto :goto_1
.end method

.method public static f(LX/Gtt;)I
    .locals 1

    .prologue
    .line 2402881
    iget-object v0, p0, LX/Gtt;->l:LX/2iv;

    invoke-virtual {v0}, LX/2iv;->size()I

    move-result v0

    return v0
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 2402843
    invoke-static {p0}, LX/Gtt;->f(LX/Gtt;)I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static j(LX/Gtt;)V
    .locals 9

    .prologue
    const/4 v2, -0x1

    .line 2402863
    iget-object v0, p0, LX/Gtt;->l:LX/2iv;

    iget-object v1, p0, LX/Gtt;->m:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v1}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->b()LX/2kM;

    move-result-object v1

    .line 2402864
    iput-object v1, v0, LX/2iv;->a:LX/2kM;

    .line 2402865
    iget v0, p0, LX/Gtt;->o:I

    if-ne v0, v2, :cond_0

    .line 2402866
    iget-object v0, p0, LX/Gtt;->p:Ljava/lang/String;

    invoke-static {p0, v0}, LX/Gtt;->a(LX/Gtt;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Gtt;->o:I

    .line 2402867
    :cond_0
    iget v0, p0, LX/Gtt;->o:I

    if-ne v0, v2, :cond_1

    .line 2402868
    :goto_0
    return-void

    .line 2402869
    :cond_1
    invoke-static {p0}, LX/Gtt;->e(LX/Gtt;)V

    .line 2402870
    iget-boolean v3, p0, LX/Gtt;->q:Z

    if-eqz v3, :cond_3

    .line 2402871
    :cond_2
    :goto_1
    goto :goto_0

    .line 2402872
    :cond_3
    iget-object v3, p0, LX/Gtt;->k:LX/0Or;

    invoke-interface {v3}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/0iA;

    .line 2402873
    new-instance v4, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v5, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->NEXT_NOTIFICATION_UI_RENDERED:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v4, v5}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    const-class v5, LX/Gtu;

    invoke-virtual {v3, v4, v5}, LX/0iA;->a(Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;)LX/0i1;

    move-result-object v4

    check-cast v4, LX/Gtu;

    .line 2402874
    if-eqz v4, :cond_2

    .line 2402875
    iget-object v5, p0, LX/Gtt;->n:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const v6, 0x7f0d1286

    invoke-virtual {v5, v6}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 2402876
    if-eqz v5, :cond_4

    :goto_2
    move-object v5, v5

    .line 2402877
    if-eqz v5, :cond_2

    .line 2402878
    new-instance v6, Lcom/facebook/katana/activity/NextNotificationManager$4;

    invoke-direct {v6, p0, v4, v5, v3}, Lcom/facebook/katana/activity/NextNotificationManager$4;-><init>(LX/Gtt;LX/Gtu;Landroid/view/View;LX/0iA;)V

    .line 2402879
    const/4 v3, 0x1

    iput-boolean v3, p0, LX/Gtt;->q:Z

    .line 2402880
    iget-object v3, p0, LX/Gtt;->i:LX/0Sh;

    const-wide/16 v7, 0xbb8

    invoke-virtual {v3, v6, v7, v8}, LX/0Sh;->b(Ljava/lang/Runnable;J)V

    goto :goto_1

    :cond_4
    iget-object v5, p0, LX/Gtt;->n:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    const v6, 0x7f0d00bd

    invoke-virtual {v5, v6}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->findViewById(I)Landroid/view/View;

    move-result-object v5

    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/content/Context;LX/Gto;Lcom/facebook/ui/titlebar/Fb4aTitleBar;Landroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2402844
    iput-object p3, p0, LX/Gtt;->n:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    .line 2402845
    iput-object p1, p0, LX/Gtt;->j:Landroid/content/Context;

    .line 2402846
    iput-object p2, p0, LX/Gtt;->f:LX/Gto;

    .line 2402847
    const-string v0, "notification_position_in_jewel"

    invoke-virtual {p4, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, LX/Gtt;->o:I

    .line 2402848
    iget v0, p0, LX/Gtt;->o:I

    if-ne v0, v1, :cond_0

    .line 2402849
    const-string v0, "push_notification_id"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Gtt;->p:Ljava/lang/String;

    .line 2402850
    iget-object v0, p0, LX/Gtt;->p:Ljava/lang/String;

    invoke-static {p0, v0}, LX/Gtt;->a(LX/Gtt;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, LX/Gtt;->o:I

    .line 2402851
    :cond_0
    iget-object v0, p0, LX/Gtt;->n:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/Gtr;

    invoke-direct {v1, p0}, LX/Gtr;-><init>(LX/Gtt;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setActionButtonOnClickListener(LX/107;)V

    .line 2402852
    iget-object v0, p0, LX/Gtt;->n:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    new-instance v1, LX/Gts;

    invoke-direct {v1, p0}, LX/Gts;-><init>(LX/Gtt;)V

    invoke-virtual {v0, v1}, Lcom/facebook/ui/titlebar/Fb4aTitleBar;->setSecondaryActionButtonOnClickListener(LX/107;)V

    .line 2402853
    sget-object v0, LX/3B2;->next_notification_started:LX/3B2;

    iget v1, p0, LX/Gtt;->o:I

    invoke-static {p0, v0, v1}, LX/Gtt;->a$redex0(LX/Gtt;LX/3B2;I)V

    .line 2402854
    iget-object v0, p0, LX/Gtt;->m:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->a()LX/2kW;

    move-result-object v0

    iget-object v1, p0, LX/Gtt;->r:LX/95a;

    invoke-virtual {v0, v1}, LX/2kW;->a(LX/1vq;)V

    .line 2402855
    iget-object v0, p0, LX/Gtt;->m:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2402856
    invoke-static {p0}, LX/Gtt;->j(LX/Gtt;)V

    .line 2402857
    :cond_1
    :goto_0
    return-void

    .line 2402858
    :cond_2
    iget-object v0, p0, LX/Gtt;->m:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    invoke-virtual {v0}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->a()LX/2kW;

    move-result-object v0

    .line 2402859
    iget-boolean v1, v0, LX/2kW;->p:Z

    move v0, v1

    .line 2402860
    if-eqz v0, :cond_1

    .line 2402861
    iget-object v0, p0, LX/Gtt;->m:Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;

    iget-object v1, p0, LX/Gtt;->g:LX/0SI;

    invoke-interface {v1}, LX/0SI;->d()Lcom/facebook/auth/viewercontext/ViewerContext;

    move-result-object v1

    sget-object p1, LX/2ub;->FRAGMENT_LOADED:LX/2ub;

    const/4 p2, 0x0

    invoke-virtual {v0, v1, p1, p2}, Lcom/facebook/notifications/sync/NotificationsConnectionControllerSyncManager;->a(Lcom/facebook/auth/viewercontext/ViewerContext;LX/2ub;LX/Drn;)V

    .line 2402862
    goto :goto_0
.end method
