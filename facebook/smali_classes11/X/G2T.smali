.class public LX/G2T;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field private final a:LX/0hB;

.field public final b:Landroid/content/Context;

.field private final c:LX/0rq;

.field private final d:LX/0sa;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0hB;LX/0rq;LX/0sa;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2314098
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2314099
    iput-object p1, p0, LX/G2T;->b:Landroid/content/Context;

    .line 2314100
    iput-object p2, p0, LX/G2T;->a:LX/0hB;

    .line 2314101
    iput-object p3, p0, LX/G2T;->c:LX/0rq;

    .line 2314102
    iput-object p4, p0, LX/G2T;->d:LX/0sa;

    .line 2314103
    return-void
.end method

.method public static a(LX/0QB;)LX/G2T;
    .locals 7

    .prologue
    .line 2314060
    const-class v1, LX/G2T;

    monitor-enter v1

    .line 2314061
    :try_start_0
    sget-object v0, LX/G2T;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2314062
    sput-object v2, LX/G2T;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2314063
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2314064
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2314065
    new-instance p0, LX/G2T;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0hB;->a(LX/0QB;)LX/0hB;

    move-result-object v4

    check-cast v4, LX/0hB;

    invoke-static {v0}, LX/0rq;->a(LX/0QB;)LX/0rq;

    move-result-object v5

    check-cast v5, LX/0rq;

    invoke-static {v0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v6

    check-cast v6, LX/0sa;

    invoke-direct {p0, v3, v4, v5, v6}, LX/G2T;-><init>(Landroid/content/Context;LX/0hB;LX/0rq;LX/0sa;)V

    .line 2314066
    move-object v0, p0

    .line 2314067
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2314068
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/G2T;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2314069
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2314070
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static b(Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;)Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2314094
    invoke-static {p0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2314095
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2314096
    const/4 v0, 0x0

    .line 2314097
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultProfilePictureFieldsModel;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(LX/G2T;)I
    .locals 2

    .prologue
    .line 2314104
    iget-object v0, p0, LX/G2T;->b:Landroid/content/Context;

    const-class v1, LX/0fE;

    invoke-static {v0, v1}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0fE;

    .line 2314105
    if-eqz v0, :cond_0

    invoke-interface {v0}, LX/0fE;->x()LX/0gz;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2314106
    invoke-interface {v0}, LX/0fE;->x()LX/0gz;

    move-result-object v0

    .line 2314107
    iget v1, v0, LX/0gz;->a:I

    move v0, v1

    .line 2314108
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/G2T;->a:LX/0hB;

    invoke-virtual {v0}, LX/0hB;->c()I

    move-result v0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;)LX/G6g;
    .locals 8
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2314090
    invoke-virtual {p0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->a()LX/5vn;

    move-result-object v1

    .line 2314091
    if-nez v1, :cond_0

    .line 2314092
    const/4 v0, 0x0

    .line 2314093
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, LX/G6g;

    invoke-interface {v1}, LX/5vn;->c()D

    move-result-wide v2

    double-to-int v2, v2

    invoke-interface {v1}, LX/5vn;->d()D

    move-result-wide v4

    double-to-int v3, v4

    invoke-interface {v1}, LX/5vn;->b()D

    move-result-wide v4

    double-to-int v4, v4

    invoke-interface {v1}, LX/5vn;->a()D

    move-result-wide v6

    double-to-int v1, v6

    invoke-direct {v0, v2, v3, v4, v1}, LX/G6g;-><init>(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public final b()I
    .locals 3

    .prologue
    .line 2314086
    iget-object v0, p0, LX/G2T;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b1492

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    .line 2314087
    iget-object v1, p0, LX/G2T;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b1493

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x6

    .line 2314088
    invoke-static {p0}, LX/G2T;->d(LX/G2T;)I

    move-result v2

    sub-int v0, v2, v0

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x3

    .line 2314089
    return v0
.end method

.method public final c(Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;)LX/1Fb;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2314071
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2314072
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2314073
    const/4 v0, 0x0

    .line 2314074
    :goto_0
    return-object v0

    .line 2314075
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->a()LX/5vn;

    move-result-object v0

    .line 2314076
    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->ai_()LX/1Fb;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-interface {v0}, LX/5vn;->b()D

    move-result-wide v0

    double-to-int v0, v0

    .line 2314077
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2314078
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v1

    invoke-static {v1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2314079
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v1

    const/4 v2, 0x6

    .line 2314080
    iget-object v3, p0, LX/G2T;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b1480

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    move v3, v3

    .line 2314081
    const/4 v4, 0x6

    div-int/2addr v4, v0

    add-int/lit8 v4, v4, -0x1

    .line 2314082
    iget-object v5, p0, LX/G2T;->b:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const p1, 0x7f0b1494

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    mul-int/2addr v4, v5

    .line 2314083
    invoke-static {p0}, LX/G2T;->d(LX/G2T;)I

    move-result v5

    sub-int v3, v5, v3

    sub-int/2addr v3, v4

    move v3, v3

    .line 2314084
    invoke-static {v1, v0, v2, v3}, LX/G0r;->a(LX/1U8;III)LX/1Fb;

    move-result-object v1

    move-object v0, v1

    .line 2314085
    goto :goto_0
.end method
