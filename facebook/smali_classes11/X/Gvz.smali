.class public final LX/Gvz;
.super Landroid/accounts/AbstractAccountAuthenticator;
.source ""


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2406418
    invoke-direct {p0, p1}, Landroid/accounts/AbstractAccountAuthenticator;-><init>(Landroid/content/Context;)V

    .line 2406419
    iput-object p1, p0, LX/Gvz;->a:Landroid/content/Context;

    .line 2406420
    return-void
.end method


# virtual methods
.method public final addAccount(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 2406421
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2406422
    iget-object v1, p0, LX/Gvz;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/facebook/katana/platform/FacebookAuthenticationService;->a(Landroid/content/Context;)I

    move-result v1

    if-lez v1, :cond_0

    .line 2406423
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 2406424
    new-instance v2, Lcom/facebook/katana/platform/FacebookAuthenticationService$FacebookAccountAuthenticator$1;

    invoke-direct {v2, p0}, Lcom/facebook/katana/platform/FacebookAuthenticationService$FacebookAccountAuthenticator$1;-><init>(LX/Gvz;)V

    const v3, -0x682813b3    # -1.39537E-24f

    invoke-static {v1, v2, v3}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2406425
    const-string v1, "errorCode"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2406426
    const-string v1, "errorMessage"

    iget-object v2, p0, LX/Gvz;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0800cc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2406427
    :goto_0
    return-object v0

    .line 2406428
    :cond_0
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, LX/Gvz;->a:Landroid/content/Context;

    const-class v3, Lcom/facebook/katana/platform/FacebookAuthenticationActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2406429
    new-instance v2, LX/Gvp;

    invoke-direct {v2}, LX/Gvp;-><init>()V

    move-object v2, v2

    .line 2406430
    iput-object p1, v2, LX/Gvp;->a:Landroid/accounts/AccountAuthenticatorResponse;

    .line 2406431
    move-object v2, v2

    .line 2406432
    iput-object v1, v2, LX/Gvp;->b:Landroid/content/ComponentName;

    .line 2406433
    move-object v1, v2

    .line 2406434
    invoke-virtual {v1}, LX/Gvp;->a()Landroid/content/Intent;

    move-result-object v1

    .line 2406435
    const-string v2, "intent"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public final confirmCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 2406416
    const/4 v0, 0x0

    return-object v0
.end method

.method public final editProperties(Landroid/accounts/AccountAuthenticatorResponse;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 2406417
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final getAuthToken(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 2406415
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getAuthTokenLabel(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2406414
    iget-object v0, p0, LX/Gvz;->a:Landroid/content/Context;

    const v1, 0x7f0800c7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final hasFeatures(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 2406411
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2406412
    const-string v1, "booleanResult"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2406413
    return-object v0
.end method

.method public final updateCredentials(Landroid/accounts/AccountAuthenticatorResponse;Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 2406410
    const/4 v0, 0x0

    return-object v0
.end method
