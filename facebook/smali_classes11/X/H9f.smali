.class public final LX/H9f;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:LX/H9g;


# direct methods
.method public constructor <init>(LX/H9g;)V
    .locals 0

    .prologue
    .line 2434506
    iput-object p1, p0, LX/H9f;->a:LX/H9g;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2434507
    iget-object v0, p0, LX/H9f;->a:LX/H9g;

    iget-object v0, v0, LX/H9g;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08179f

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2434508
    iget-object v0, p0, LX/H9f;->a:LX/H9g;

    iget-object v0, v0, LX/H9g;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8W;

    sget-object v1, LX/9XA;->EVENT_PLACE_REPORT_ERROR:LX/9XA;

    iget-object v2, p0, LX/H9f;->a:LX/H9g;

    iget-object v2, v2, LX/H9g;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2434509
    iget-object v0, p0, LX/H9f;->a:LX/H9g;

    iget-object v0, v0, LX/H9g;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "page_identity_report_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2434510
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2434511
    iget-object v0, p0, LX/H9f;->a:LX/H9g;

    iget-object v0, v0, LX/H9g;->g:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f08179e

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2434512
    iget-object v0, p0, LX/H9f;->a:LX/H9g;

    iget-object v0, v0, LX/H9g;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8W;

    sget-object v1, LX/9XB;->EVENT_PLACE_REPORT_SUCCESS:LX/9XB;

    iget-object v2, p0, LX/H9f;->a:LX/H9g;

    iget-object v2, v2, LX/H9g;->k:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2434513
    return-void
.end method
