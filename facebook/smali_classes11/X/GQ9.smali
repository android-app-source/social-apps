.class public LX/GQ9;
.super LX/GQ7;
.source ""


# static fields
.field public static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    .line 2349727
    sget-object v0, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->VISA:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    sget-object v1, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->AMEX:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    sget-object v2, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->DISCOVER:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    sget-object v3, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->JCB:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    sget-object v4, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->MASTER_CARD:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    invoke-static {v0, v1, v2, v3, v4}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    sput-object v0, LX/GQ9;->a:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2349728
    invoke-direct {p0}, LX/GQ7;-><init>()V

    .line 2349729
    return-void
.end method

.method public static d(Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2349730
    invoke-static {p0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2349731
    :cond_0
    :goto_0
    return v0

    .line 2349732
    :cond_1
    invoke-static {p0}, LX/6yU;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2349733
    invoke-static {v1}, LX/6yU;->a(Ljava/lang/String;)Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    move-result-object v2

    .line 2349734
    sget-object v3, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->UNKNOWN:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    if-eq v2, v3, :cond_0

    .line 2349735
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2}, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->getCardLength()I

    move-result v2

    if-ne v3, v2, :cond_0

    invoke-static {v1}, LX/GQ9;->e(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static e(Ljava/lang/String;)Z
    .locals 6
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2349736
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v3

    move v2, v1

    move v0, v1

    .line 2349737
    :goto_0
    if-ge v2, v3, :cond_2

    .line 2349738
    add-int/lit8 v4, v3, -0x1

    sub-int/2addr v4, v2

    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 2349739
    add-int/lit8 v4, v4, -0x30

    .line 2349740
    rem-int/lit8 v5, v2, 0x2

    if-nez v5, :cond_0

    .line 2349741
    add-int/2addr v0, v4

    .line 2349742
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2349743
    :cond_0
    mul-int/lit8 v4, v4, 0x2

    .line 2349744
    const/16 v5, 0x9

    if-le v4, v5, :cond_1

    .line 2349745
    add-int/lit8 v4, v4, -0xa

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v0, v4

    goto :goto_1

    .line 2349746
    :cond_1
    add-int/2addr v0, v4

    goto :goto_1

    .line 2349747
    :cond_2
    rem-int/lit8 v0, v0, 0xa

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_3
    move v0, v1

    goto :goto_2
.end method
