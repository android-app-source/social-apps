.class public LX/GhM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0jq;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2383986
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2383987
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/support/v4/app/Fragment;
    .locals 4

    .prologue
    .line 2383988
    new-instance v1, Lcom/facebook/gametime/ui/reaction/GametimeDashboardFragment;

    invoke-direct {v1}, Lcom/facebook/gametime/ui/reaction/GametimeDashboardFragment;-><init>()V

    .line 2383989
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2383990
    :goto_0
    const-string v2, "ptr_enabled"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2383991
    invoke-virtual {v1, v0}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2383992
    return-object v1

    .line 2383993
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method
