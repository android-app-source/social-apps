.class public abstract enum LX/GMY;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/GMX;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GMY;",
        ">;",
        "LX/GMX;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GMY;

.field public static final enum IDLE:LX/GMY;

.field public static final enum LOADING:LX/GMY;


# instance fields
.field public mBackgroundAlpha:I


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2344876
    new-instance v0, LX/GMZ;

    const-string v1, "LOADING"

    const/16 v2, 0x80

    invoke-direct {v0, v1, v3, v2}, LX/GMZ;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/GMY;->LOADING:LX/GMY;

    .line 2344877
    new-instance v0, LX/GMa;

    const-string v1, "IDLE"

    const/16 v2, 0xff

    invoke-direct {v0, v1, v4, v2}, LX/GMa;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/GMY;->IDLE:LX/GMY;

    .line 2344878
    const/4 v0, 0x2

    new-array v0, v0, [LX/GMY;

    sget-object v1, LX/GMY;->LOADING:LX/GMY;

    aput-object v1, v0, v3

    sget-object v1, LX/GMY;->IDLE:LX/GMY;

    aput-object v1, v0, v4

    sput-object v0, LX/GMY;->$VALUES:[LX/GMY;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2344879
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2344880
    iput p3, p0, LX/GMY;->mBackgroundAlpha:I

    .line 2344881
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;IILX/GMW;)V
    .locals 0

    .prologue
    .line 2344875
    invoke-direct {p0, p1, p2, p3}, LX/GMY;-><init>(Ljava/lang/String;II)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GMY;
    .locals 1

    .prologue
    .line 2344874
    const-class v0, LX/GMY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GMY;

    return-object v0
.end method

.method public static values()[LX/GMY;
    .locals 1

    .prologue
    .line 2344873
    sget-object v0, LX/GMY;->$VALUES:[LX/GMY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GMY;

    return-object v0
.end method
