.class public LX/FL5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Lcom/facebook/messaging/service/model/ModifyThreadParams;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2226065
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2226066
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 6

    .prologue
    .line 2226035
    check-cast p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;

    .line 2226036
    const-string v0, "%s/nicknames"

    invoke-virtual {p1}, Lcom/facebook/messaging/service/model/ModifyThreadParams;->u()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/DoA;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2226037
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2226038
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "format"

    const-string v4, "json"

    invoke-direct {v0, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2226039
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "participant_id"

    .line 2226040
    iget-object v4, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->k:Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;

    move-object v4, v4

    .line 2226041
    iget-object v5, v4, Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;->a:Ljava/lang/String;

    move-object v4, v5

    .line 2226042
    invoke-direct {v0, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2226043
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "source"

    .line 2226044
    iget-object v4, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->m:Ljava/lang/String;

    move-object v4, v4

    .line 2226045
    invoke-direct {v0, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2226046
    iget-object v0, p1, Lcom/facebook/messaging/service/model/ModifyThreadParams;->k:Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;

    move-object v0, v0

    .line 2226047
    iget-object v3, v0, Lcom/facebook/messaging/service/model/ModifyThreadParams$NicknamePair;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2226048
    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2226049
    const-string v0, "DELETE"

    .line 2226050
    :goto_0
    new-instance v3, LX/14O;

    invoke-direct {v3}, LX/14O;-><init>()V

    const-string v4, "setThreadParticipantNickname"

    .line 2226051
    iput-object v4, v3, LX/14O;->b:Ljava/lang/String;

    .line 2226052
    move-object v3, v3

    .line 2226053
    iput-object v0, v3, LX/14O;->c:Ljava/lang/String;

    .line 2226054
    move-object v0, v3

    .line 2226055
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 2226056
    move-object v0, v0

    .line 2226057
    iput-object v2, v0, LX/14O;->g:Ljava/util/List;

    .line 2226058
    move-object v0, v0

    .line 2226059
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2226060
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2226061
    move-object v0, v0

    .line 2226062
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0

    .line 2226063
    :cond_0
    const-string v0, "POST"

    .line 2226064
    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "nickname"

    invoke-direct {v4, v5, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2226033
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2226034
    const/4 v0, 0x0

    return-object v0
.end method
