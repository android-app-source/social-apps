.class public interface abstract LX/H72;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H71;


# annotations
.annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
    from = "OfferViewData"
    processor = "com.facebook.dracula.transformer.Transformer"
.end annotation


# virtual methods
.method public abstract F()LX/1vs;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getAppDataForOffer"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract G()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDataModel;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getOffer"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract H()LX/0Px;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPhotos"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/offers/graphql/OfferQueriesInterfaces$PhotoData$;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract I()Lcom/facebook/offers/graphql/OfferQueriesModels$OfferStoryFieldsModel;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getRootShareStory"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method

.method public abstract J()LX/0Px;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getVideos"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<+",
            "Lcom/facebook/offers/graphql/OfferQueriesInterfaces$VideoData$;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/Nonnull;
    .end annotation
.end method

.method public abstract K()LX/H6z;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getViewerClaim"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end method
