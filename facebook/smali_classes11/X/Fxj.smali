.class public LX/Fxj;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Fx7;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/Fx7",
        "<",
        "Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLInterfaces$FavoritePhoto;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Fxj;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2306086
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2306087
    return-void
.end method

.method public static a(LX/0QB;)LX/Fxj;
    .locals 3

    .prologue
    .line 2306074
    sget-object v0, LX/Fxj;->a:LX/Fxj;

    if-nez v0, :cond_1

    .line 2306075
    const-class v1, LX/Fxj;

    monitor-enter v1

    .line 2306076
    :try_start_0
    sget-object v0, LX/Fxj;->a:LX/Fxj;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2306077
    if-eqz v2, :cond_0

    .line 2306078
    :try_start_1
    new-instance v0, LX/Fxj;

    invoke-direct {v0}, LX/Fxj;-><init>()V

    .line 2306079
    move-object v0, v0

    .line 2306080
    sput-object v0, LX/Fxj;->a:LX/Fxj;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2306081
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2306082
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2306083
    :cond_1
    sget-object v0, LX/Fxj;->a:LX/Fxj;

    return-object v0

    .line 2306084
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2306085
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/0Px;I)LX/5vo;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLInterfaces$FavoritePhoto;",
            ">;I)",
            "LX/5vo;"
        }
    .end annotation

    .prologue
    .line 2306072
    invoke-virtual {p1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;->b()LX/5vo;

    move-result-object v0

    return-object v0
.end method

.method public final b(LX/0Px;I)LX/5vn;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<+",
            "Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLInterfaces$FavoritePhoto;",
            ">;I)",
            "LX/5vn;"
        }
    .end annotation

    .prologue
    .line 2306073
    invoke-virtual {p1, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;

    invoke-virtual {v0}, Lcom/facebook/timeline/header/intro/favphotos/protocol/FavPhotosGraphQLModels$FavoritePhotoModel;->a()LX/5vn;

    move-result-object v0

    return-object v0
.end method
