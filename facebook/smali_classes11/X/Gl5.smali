.class public final enum LX/Gl5;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gl5;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gl5;

.field public static final enum EDIT_FAVORITES:LX/Gl5;

.field public static final enum EDIT_HIDDEN:LX/Gl5;

.field public static final enum INITIAL_FETCH_TASK:LX/Gl5;

.field public static final enum LEAVE_GROUP:LX/Gl5;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2389797
    new-instance v0, LX/Gl5;

    const-string v1, "INITIAL_FETCH_TASK"

    invoke-direct {v0, v1, v2}, LX/Gl5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gl5;->INITIAL_FETCH_TASK:LX/Gl5;

    .line 2389798
    new-instance v0, LX/Gl5;

    const-string v1, "EDIT_FAVORITES"

    invoke-direct {v0, v1, v3}, LX/Gl5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gl5;->EDIT_FAVORITES:LX/Gl5;

    .line 2389799
    new-instance v0, LX/Gl5;

    const-string v1, "EDIT_HIDDEN"

    invoke-direct {v0, v1, v4}, LX/Gl5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gl5;->EDIT_HIDDEN:LX/Gl5;

    .line 2389800
    new-instance v0, LX/Gl5;

    const-string v1, "LEAVE_GROUP"

    invoke-direct {v0, v1, v5}, LX/Gl5;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gl5;->LEAVE_GROUP:LX/Gl5;

    .line 2389801
    const/4 v0, 0x4

    new-array v0, v0, [LX/Gl5;

    sget-object v1, LX/Gl5;->INITIAL_FETCH_TASK:LX/Gl5;

    aput-object v1, v0, v2

    sget-object v1, LX/Gl5;->EDIT_FAVORITES:LX/Gl5;

    aput-object v1, v0, v3

    sget-object v1, LX/Gl5;->EDIT_HIDDEN:LX/Gl5;

    aput-object v1, v0, v4

    sget-object v1, LX/Gl5;->LEAVE_GROUP:LX/Gl5;

    aput-object v1, v0, v5

    sput-object v0, LX/Gl5;->$VALUES:[LX/Gl5;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2389796
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gl5;
    .locals 1

    .prologue
    .line 2389795
    const-class v0, LX/Gl5;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gl5;

    return-object v0
.end method

.method public static values()[LX/Gl5;
    .locals 1

    .prologue
    .line 2389794
    sget-object v0, LX/Gl5;->$VALUES:[LX/Gl5;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gl5;

    return-object v0
.end method
