.class public final LX/GUU;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/backgroundlocation/settings/write/BackgroundLocationSettingsMutationsModels$BackgroundLocationSettingsPauseMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/support/v4/app/DialogFragment;

.field public final synthetic b:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;Landroid/support/v4/app/DialogFragment;)V
    .locals 0

    .prologue
    .line 2357424
    iput-object p1, p0, LX/GUU;->b:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iput-object p2, p0, LX/GUU;->a:Landroid/support/v4/app/DialogFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2357425
    iget-object v1, p0, LX/GUU;->b:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v0, p0, LX/GUU;->b:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-boolean v0, v0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ai:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 2357426
    :goto_0
    iput-boolean v0, v1, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->ai:Z

    .line 2357427
    iget-object v0, p0, LX/GUU;->b:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v1, p0, LX/GUU;->b:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->L:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    invoke-static {v0, v1}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->b(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)V

    .line 2357428
    iget-object v0, p0, LX/GUU;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2357429
    return-void

    .line 2357430
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2357431
    iget-object v0, p0, LX/GUU;->b:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v1, p0, LX/GUU;->b:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v1, v1, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->L:Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;

    invoke-static {v0, v1}, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->b(Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;Lcom/facebook/backgroundlocation/privacypicker/graphql/BackgroundLocationPrivacyPickerGraphQLModels$BackgroundLocationPrivacyPickerOptionModel;)V

    .line 2357432
    iget-object v0, p0, LX/GUU;->a:Landroid/support/v4/app/DialogFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2357433
    iget-object v0, p0, LX/GUU;->b:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->K:LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080039

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2357434
    iget-object v0, p0, LX/GUU;->b:Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;

    iget-object v0, v0, Lcom/facebook/backgroundlocation/settings/BackgroundLocationSettingsActivity;->S:LX/03V;

    const-string v1, "background_location_settings_location_sharing_pause_fail"

    invoke-virtual {v0, v1, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2357435
    return-void
.end method

.method public final synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2357436
    invoke-direct {p0}, LX/GUU;->a()V

    return-void
.end method
