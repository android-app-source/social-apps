.class public LX/FAm;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Hz;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/FAm;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation build Lcom/facebook/inject/ForAppContext;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2208081
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2208082
    iput-object p1, p0, LX/FAm;->a:Landroid/content/Context;

    .line 2208083
    return-void
.end method

.method public static a(LX/0QB;)LX/FAm;
    .locals 5

    .prologue
    .line 2208084
    sget-object v0, LX/FAm;->b:LX/FAm;

    if-nez v0, :cond_1

    .line 2208085
    const-class v1, LX/FAm;

    monitor-enter v1

    .line 2208086
    :try_start_0
    sget-object v0, LX/FAm;->b:LX/FAm;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2208087
    if-eqz v2, :cond_0

    .line 2208088
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2208089
    new-instance v4, LX/FAm;

    const-class v3, Landroid/content/Context;

    const-class p0, Lcom/facebook/inject/ForAppContext;

    invoke-interface {v0, v3, p0}, LX/0QC;->getInstance(Ljava/lang/Class;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-direct {v4, v3}, LX/FAm;-><init>(Landroid/content/Context;)V

    .line 2208090
    move-object v0, v4

    .line 2208091
    sput-object v0, LX/FAm;->b:LX/FAm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2208092
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2208093
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2208094
    :cond_1
    sget-object v0, LX/FAm;->b:LX/FAm;

    return-object v0

    .line 2208095
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2208096
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2208097
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/FAm;->a:Landroid/content/Context;

    const-class v2, Lcom/facebook/vault/ui/VaultSettingsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 2208098
    iget-object v0, p0, LX/FAm;->a:Landroid/content/Context;

    sget-object v1, LX/5Rz;->LIB_FB4A:LX/5Rz;

    invoke-virtual {v1}, LX/5Rz;->name()Ljava/lang/String;

    move-result-object v1

    new-instance v2, LX/5Rw;

    invoke-direct {v2}, LX/5Rw;-><init>()V

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v3

    const-string v4, "0"

    invoke-virtual {v2, v3, v4}, LX/5Rw;->a(Landroid/net/Uri;Ljava/lang/String;)LX/5Rw;

    move-result-object v2

    sget-object v3, LX/5Rr;->CROP:LX/5Rr;

    invoke-virtual {v2, v3}, LX/5Rw;->a(LX/5Rr;)LX/5Rw;

    move-result-object v2

    sget-object v3, LX/5Rr;->DOODLE:LX/5Rr;

    invoke-virtual {v2, v3}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v2

    sget-object v3, LX/5Rr;->TEXT:LX/5Rr;

    invoke-virtual {v2, v3}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v2

    sget-object v3, LX/5Rr;->STICKER:LX/5Rr;

    invoke-virtual {v2, v3}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v2

    sget-object v3, LX/5Rr;->FILTER:LX/5Rr;

    invoke-virtual {v2, v3}, LX/5Rw;->b(LX/5Rr;)LX/5Rw;

    move-result-object v2

    sget-object v3, LX/5Rq;->ZOOM_CROP:LX/5Rq;

    invoke-virtual {v2, v3}, LX/5Rw;->a(LX/5Rq;)LX/5Rw;

    move-result-object v2

    invoke-virtual {v2}, LX/5Rw;->a()Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/5Rs;->a(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/ipc/editgallery/EditGalleryLaunchConfiguration;)Landroid/content/Intent;

    move-result-object v0

    .line 2208099
    return-object v0
.end method
