.class public final LX/G5a;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "LX/6N1;",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/search/api/SearchTypeaheadResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/G5c;


# direct methods
.method public constructor <init>(LX/G5c;)V
    .locals 0

    .prologue
    .line 2318963
    iput-object p1, p0, LX/G5a;->a:LX/G5c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 2318964
    check-cast p1, LX/6N1;

    .line 2318965
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2318966
    :goto_0
    invoke-interface {p1}, LX/6N1;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2318967
    invoke-interface {p1}, LX/6N1;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/contacts/graphql/Contact;

    .line 2318968
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v4

    .line 2318969
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->o()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_0

    invoke-virtual {v5, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/contacts/graphql/ContactPhone;

    .line 2318970
    invoke-virtual {v1}, Lcom/facebook/contacts/graphql/ContactPhone;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2318971
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2318972
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->e()Lcom/facebook/user/model/Name;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/user/model/Name;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/facebook/contacts/graphql/Contact;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    const/4 v10, 0x0

    .line 2318973
    invoke-static {}, Lcom/facebook/search/api/SearchTypeaheadResult;->newBuilder()LX/7BL;

    move-result-object v8

    const-string v9, ""

    .line 2318974
    iput-object v9, v8, LX/7BL;->a:Ljava/lang/String;

    .line 2318975
    move-object v8, v8

    .line 2318976
    sget-object v9, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2318977
    iput-object v9, v8, LX/7BL;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2318978
    move-object v8, v8

    .line 2318979
    iput-object v10, v8, LX/7BL;->d:Landroid/net/Uri;

    .line 2318980
    move-object v8, v8

    .line 2318981
    iput-object v10, v8, LX/7BL;->e:Landroid/net/Uri;

    .line 2318982
    move-object v8, v8

    .line 2318983
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    .line 2318984
    iput-object v9, v8, LX/7BL;->f:Landroid/net/Uri;

    .line 2318985
    move-object v8, v8

    .line 2318986
    const-string v9, ""

    .line 2318987
    iput-object v9, v8, LX/7BL;->g:Ljava/lang/String;

    .line 2318988
    move-object v8, v8

    .line 2318989
    iput-object v2, v8, LX/7BL;->l:Ljava/lang/String;

    .line 2318990
    move-object v8, v8

    .line 2318991
    sget-object v9, LX/7BK;->USER:LX/7BK;

    .line 2318992
    iput-object v9, v8, LX/7BL;->m:LX/7BK;

    .line 2318993
    move-object v8, v8

    .line 2318994
    iput-wide v6, v8, LX/7BL;->n:J

    .line 2318995
    move-object v8, v8

    .line 2318996
    iput-object v4, v8, LX/7BL;->r:Ljava/util/List;

    .line 2318997
    move-object v8, v8

    .line 2318998
    invoke-virtual {v8}, LX/7BL;->a()Lcom/facebook/search/api/SearchTypeaheadResult;

    move-result-object v8

    move-object v0, v8

    .line 2318999
    invoke-virtual {v3, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2319000
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method
