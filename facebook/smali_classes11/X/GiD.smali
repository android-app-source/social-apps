.class public final LX/GiD;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;)V
    .locals 0

    .prologue
    .line 2385338
    iput-object p1, p0, LX/GiD;->a:Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Ljava/util/concurrent/CancellationException;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2385339
    iget-object v0, p0, LX/GiD;->a:Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;

    .line 2385340
    iput-boolean v1, v0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->q:Z

    .line 2385341
    iget-object v0, p0, LX/GiD;->a:Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;

    iget-object v0, v0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->o:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2385342
    return-void
.end method

.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2385343
    iget-object v0, p0, LX/GiD;->a:Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;

    .line 2385344
    iput-boolean v1, v0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->q:Z

    .line 2385345
    iget-object v0, p0, LX/GiD;->a:Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;

    iget-object v0, v0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->o:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2385346
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 2385347
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v2, 0x0

    .line 2385348
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2385349
    if-eqz v0, :cond_0

    .line 2385350
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2385351
    check-cast v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel;

    invoke-virtual {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel;->j()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel$SportsMatchDataModel;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2385352
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2385353
    check-cast v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel;

    invoke-virtual {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel;->j()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel$SportsMatchDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel$SportsMatchDataModel;->j()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel$SportsMatchDataModel$FactsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2385354
    :cond_0
    iget-object v0, p0, LX/GiD;->a:Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;

    .line 2385355
    iput-boolean v2, v0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->q:Z

    .line 2385356
    iget-object v0, p0, LX/GiD;->a:Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;

    iget-object v0, v0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->o:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    .line 2385357
    :goto_0
    return-void

    .line 2385358
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2385359
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2385360
    check-cast v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel;

    invoke-virtual {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel;->j()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel$SportsMatchDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel$SportsMatchDataModel;->j()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel$SportsMatchDataModel$FactsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel$SportsMatchDataModel$FactsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    .line 2385361
    :goto_1
    if-ge v1, v5, :cond_3

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel$SportsMatchDataModel$FactsModel$EdgesModel;

    .line 2385362
    invoke-virtual {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel$SportsMatchDataModel$FactsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 2385363
    invoke-virtual {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel$SportsMatchDataModel$FactsModel$EdgesModel;->a()Lcom/facebook/reaction/protocol/components/ReactionComponentsGraphQLModels$GametimeDataFactFragmentModel;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2385364
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2385365
    :cond_3
    iget-object v0, p0, LX/GiD;->a:Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;

    iget-object v0, v0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->n:LX/Gi5;

    .line 2385366
    iget-object v1, v0, LX/Gi5;->a:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 2385367
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2385368
    check-cast v0, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel;

    invoke-virtual {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel;->j()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel$SportsMatchDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel$SportsMatchDataModel;->j()Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel$SportsMatchDataModel$FactsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/gametime/graphql/GametimeFragmentsGraphQLModels$GametimeSportsPlaysQueryModel$SportsMatchDataModel$FactsModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v0

    .line 2385369
    if-eqz v0, :cond_4

    .line 2385370
    iget-object v1, p0, LX/GiD;->a:Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->b()Z

    move-result v3

    .line 2385371
    iput-boolean v3, v1, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->k:Z

    .line 2385372
    iget-object v1, p0, LX/GiD;->a:Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;

    invoke-virtual {v0}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;->a()Ljava/lang/String;

    move-result-object v0

    .line 2385373
    iput-object v0, v1, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->j:Ljava/lang/String;

    .line 2385374
    :cond_4
    iget-object v0, p0, LX/GiD;->a:Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;

    invoke-static {v0}, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->b$redex0(Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;)LX/1Qq;

    move-result-object v0

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2385375
    iget-object v0, p0, LX/GiD;->a:Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;

    .line 2385376
    iput-boolean v2, v0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->q:Z

    .line 2385377
    iget-object v0, p0, LX/GiD;->a:Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;

    iget-object v0, v0, Lcom/facebook/gametime/ui/plays/GametimePlaysFragment;->o:Lcom/facebook/widget/FbSwipeRefreshLayout;

    invoke-virtual {v0, v2}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    goto :goto_0
.end method
