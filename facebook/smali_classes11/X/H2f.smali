.class public final LX/H2f;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 14

    .prologue
    const/4 v6, 0x1

    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 2418612
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_7

    .line 2418613
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2418614
    :goto_0
    return v1

    .line 2418615
    :cond_0
    const-string v11, "search_score"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 2418616
    invoke-virtual {p0}, LX/15w;->G()D

    move-result-wide v2

    move v0, v6

    .line 2418617
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v10

    sget-object v11, LX/15z;->END_OBJECT:LX/15z;

    if-eq v10, v11, :cond_5

    .line 2418618
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v10

    .line 2418619
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2418620
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v11, v12, :cond_1

    if-eqz v10, :cond_1

    .line 2418621
    const-string v11, "node"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 2418622
    invoke-static {p0, p1}, LX/H2e;->a(LX/15w;LX/186;)I

    move-result v9

    goto :goto_1

    .line 2418623
    :cond_2
    const-string v11, "result_decoration"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 2418624
    invoke-static {p0, p1}, LX/H2L;->a(LX/15w;LX/186;)I

    move-result v8

    goto :goto_1

    .line 2418625
    :cond_3
    const-string v11, "social_context"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 2418626
    const/4 v10, 0x0

    .line 2418627
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v7

    sget-object v11, LX/15z;->START_OBJECT:LX/15z;

    if-eq v7, v11, :cond_b

    .line 2418628
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2418629
    :goto_2
    move v7, v10

    .line 2418630
    goto :goto_1

    .line 2418631
    :cond_4
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    goto :goto_1

    .line 2418632
    :cond_5
    const/4 v10, 0x4

    invoke-virtual {p1, v10}, LX/186;->c(I)V

    .line 2418633
    invoke-virtual {p1, v1, v9}, LX/186;->b(II)V

    .line 2418634
    invoke-virtual {p1, v6, v8}, LX/186;->b(II)V

    .line 2418635
    if-eqz v0, :cond_6

    .line 2418636
    const/4 v1, 0x2

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, LX/186;->a(IDD)V

    .line 2418637
    :cond_6
    const/4 v0, 0x3

    invoke-virtual {p1, v0, v7}, LX/186;->b(II)V

    .line 2418638
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_7
    move v0, v1

    move v7, v1

    move-wide v2, v4

    move v8, v1

    move v9, v1

    goto :goto_1

    .line 2418639
    :cond_8
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2418640
    :cond_9
    :goto_3
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v11

    sget-object v12, LX/15z;->END_OBJECT:LX/15z;

    if-eq v11, v12, :cond_a

    .line 2418641
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v11

    .line 2418642
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2418643
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v12

    sget-object v13, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v12, v13, :cond_9

    if-eqz v11, :cond_9

    .line 2418644
    const-string v12, "text"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    .line 2418645
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, LX/186;->b(Ljava/lang/String;)I

    move-result v7

    goto :goto_3

    .line 2418646
    :cond_a
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, LX/186;->c(I)V

    .line 2418647
    invoke-virtual {p1, v10, v7}, LX/186;->b(II)V

    .line 2418648
    invoke-virtual {p1}, LX/186;->d()I

    move-result v10

    goto :goto_2

    :cond_b
    move v7, v10

    goto :goto_3
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2418649
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2418650
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2418651
    if-eqz v0, :cond_0

    .line 2418652
    const-string v1, "node"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2418653
    invoke-static {p0, v0, p2, p3}, LX/H2e;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2418654
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2418655
    if-eqz v0, :cond_1

    .line 2418656
    const-string v1, "result_decoration"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2418657
    invoke-static {p0, v0, p2, p3}, LX/H2L;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2418658
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0, v2, v3}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 2418659
    cmpl-double v2, v0, v2

    if-eqz v2, :cond_2

    .line 2418660
    const-string v2, "search_score"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2418661
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 2418662
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2418663
    if-eqz v0, :cond_4

    .line 2418664
    const-string v1, "social_context"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2418665
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2418666
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v1

    .line 2418667
    if-eqz v1, :cond_3

    .line 2418668
    const-string v2, "text"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2418669
    invoke-virtual {p2, v1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2418670
    :cond_3
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2418671
    :cond_4
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2418672
    return-void
.end method
