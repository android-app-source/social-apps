.class public final LX/GdM;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/87u;


# instance fields
.field public final synthetic a:Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;)V
    .locals 0

    .prologue
    .line 2373729
    iput-object p1, p0, LX/GdM;->a:Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILcom/facebook/graphql/model/GraphQLPrivacyOption;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2373732
    iget-object v2, p0, LX/GdM;->a:Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;

    .line 2373733
    iget-boolean p2, v2, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v2, p2

    .line 2373734
    if-eqz v2, :cond_1

    .line 2373735
    iget-object v2, p0, LX/GdM;->a:Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;

    if-lez p1, :cond_0

    :goto_0
    invoke-static {v2, v0}, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->a$redex0(Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;Z)V

    .line 2373736
    iget-object v0, p0, LX/GdM;->a:Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;

    .line 2373737
    iput-boolean v1, v0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->j:Z

    .line 2373738
    :goto_1
    return-void

    :cond_0
    move v0, v1

    .line 2373739
    goto :goto_0

    .line 2373740
    :cond_1
    iget-object v1, p0, LX/GdM;->a:Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;

    .line 2373741
    iput-boolean v0, v1, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->j:Z

    .line 2373742
    goto :goto_1
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2373730
    sget-object v0, Lcom/facebook/feed/goodfriends/GoodFriendsFeedFragment;->e:Ljava/lang/String;

    const-string v1, "Fail to retrieve good friends"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2373731
    return-void
.end method
