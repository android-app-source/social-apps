.class public LX/FFl;
.super LX/0Q6;
.source ""


# annotations
.annotation build Lcom/facebook/inject/InjectorModule;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2217611
    invoke-direct {p0}, LX/0Q6;-><init>()V

    .line 2217612
    return-void
.end method

.method public static a(LX/0ad;)Ljava/lang/Boolean;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/lightweightactions/providers/IsLightweightActionsComposerEntryPointEnabled;
    .end annotation

    .prologue
    .line 2217613
    sget-short v0, LX/FFn;->e:S

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0ad;)Ljava/lang/Boolean;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/lightweightactions/providers/IsLightweightActionsPeopleTabAllEntryPointEnabled;
    .end annotation

    .prologue
    .line 2217606
    sget-short v0, LX/FFn;->h:S

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static c(LX/0ad;)Ljava/lang/Boolean;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/lightweightactions/providers/IsLightweightActionsPeopleTabActiveEntryPointEnabled;
    .end annotation

    .prologue
    .line 2217609
    sget-short v0, LX/FFn;->g:S

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static d(LX/0ad;)Ljava/lang/Boolean;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/lightweightactions/providers/IsLightweightActionsActiveNowEntryPointEnabled;
    .end annotation

    .prologue
    .line 2217610
    sget-short v0, LX/FFn;->d:S

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static e(LX/0ad;)Ljava/lang/Boolean;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/lightweightactions/providers/IsLightweightActionsNewConnectionEntryPointEnabled;
    .end annotation

    .prologue
    .line 2217608
    sget-short v0, LX/FFn;->f:S

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static f(LX/0ad;)Ljava/lang/Boolean;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/lightweightactions/providers/IsStickerAnimationEnabled;
    .end annotation

    .prologue
    .line 2217607
    sget-short v0, LX/FFn;->c:S

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, LX/0ad;->a(SZ)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method public static g(LX/0ad;)Ljava/lang/String;
    .locals 2
    .annotation build Lcom/facebook/inject/ProviderMethod;
    .end annotation

    .annotation runtime Lcom/facebook/messaging/lightweightactions/providers/DefaultStickerIds;
    .end annotation

    .prologue
    .line 2217605
    sget-char v0, LX/FFn;->b:C

    const-string v1, ""

    invoke-interface {p0, v0, v1}, LX/0ad;->a(CLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final configure()V
    .locals 1

    .prologue
    .line 2217604
    return-void
.end method
