.class public LX/FvO;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FvM;


# instance fields
.field private final a:LX/BQB;

.field private final b:LX/BPC;

.field private final c:Landroid/os/Handler;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation
.end field

.field private final d:LX/Fv9;

.field public e:Lcom/facebook/timeline/TimelineFragment;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private f:Z

.field private g:Z


# direct methods
.method public constructor <init>(LX/BQB;LX/BPC;Landroid/os/Handler;LX/Fv9;)V
    .locals 1
    .param p3    # Landroid/os/Handler;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p4    # LX/Fv9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2301398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2301399
    iput-boolean v0, p0, LX/FvO;->f:Z

    .line 2301400
    iput-boolean v0, p0, LX/FvO;->g:Z

    .line 2301401
    iput-object p1, p0, LX/FvO;->a:LX/BQB;

    .line 2301402
    iput-object p2, p0, LX/FvO;->b:LX/BPC;

    .line 2301403
    iput-object p3, p0, LX/FvO;->c:Landroid/os/Handler;

    .line 2301404
    iput-object p4, p0, LX/FvO;->d:LX/Fv9;

    .line 2301405
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2301391
    iget-object v0, p0, LX/FvO;->b:LX/BPC;

    invoke-virtual {v0}, LX/BPC;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FvO;->e:Lcom/facebook/timeline/TimelineFragment;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/FvO;->f:Z

    if-nez v0, :cond_0

    .line 2301392
    iput-boolean v3, p0, LX/FvO;->f:Z

    .line 2301393
    iget-object v0, p0, LX/FvO;->c:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/timeline/header/TimelineHeaderPerfControllerImpl$1;

    invoke-direct {v1, p0}, Lcom/facebook/timeline/header/TimelineHeaderPerfControllerImpl$1;-><init>(LX/FvO;)V

    const v2, -0x604c3c21

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2301394
    :cond_0
    iget-object v0, p0, LX/FvO;->b:LX/BPC;

    invoke-virtual {v0}, LX/BPC;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/FvO;->e:Lcom/facebook/timeline/TimelineFragment;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, LX/FvO;->g:Z

    if-nez v0, :cond_1

    .line 2301395
    iput-boolean v3, p0, LX/FvO;->g:Z

    .line 2301396
    iget-object v0, p0, LX/FvO;->c:Landroid/os/Handler;

    new-instance v1, Lcom/facebook/timeline/header/TimelineHeaderPerfControllerImpl$2;

    invoke-direct {v1, p0}, Lcom/facebook/timeline/header/TimelineHeaderPerfControllerImpl$2;-><init>(LX/FvO;)V

    const v2, -0xbb75cad

    invoke-static {v0, v1, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    .line 2301397
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(LX/BPA;)V
    .locals 11

    .prologue
    .line 2301357
    iget-object v0, p0, LX/FvO;->a:LX/BQB;

    const v3, 0x1a0018

    .line 2301358
    sget-object v1, LX/BPA;->PHOTO_NONE:LX/BPA;

    if-ne p1, v1, :cond_2

    .line 2301359
    iget-object v1, v0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-interface {v1, v3}, Lcom/facebook/quicklog/QuickPerformanceLogger;->d(I)V

    .line 2301360
    :cond_0
    :goto_0
    iget-object v1, v0, LX/BQB;->f:LX/BPC;

    .line 2301361
    iget-object v2, v1, LX/BPC;->e:LX/BPA;

    move-object v1, v2

    .line 2301362
    sget-object v2, LX/BPA;->PHOTO_HIGH_RES:LX/BPA;

    if-eq v1, v2, :cond_1

    .line 2301363
    iget-object v1, v0, LX/BQB;->f:LX/BPC;

    .line 2301364
    iput-object p1, v1, LX/BPC;->e:LX/BPA;

    .line 2301365
    :cond_1
    const-string v1, "cover photo loaded"

    invoke-static {v0, v1}, LX/BQB;->i(LX/BQB;Ljava/lang/String;)V

    .line 2301366
    invoke-static {v0}, LX/BQB;->v(LX/BQB;)V

    .line 2301367
    invoke-direct {p0}, LX/FvO;->a()V

    .line 2301368
    return-void

    .line 2301369
    :cond_2
    sget-object v1, LX/BPA;->PHOTO_LOW_RES:LX/BPA;

    if-eq p1, v1, :cond_3

    sget-object v1, LX/BPA;->PHOTO_MINI_PREVIEW:LX/BPA;

    if-ne p1, v1, :cond_4

    :cond_3
    iget-object v1, v0, LX/BQB;->f:LX/BPC;

    .line 2301370
    iget-object v2, v1, LX/BPC;->e:LX/BPA;

    move-object v1, v2

    .line 2301371
    sget-object v2, LX/BPA;->PHOTO_NOT_LOADED:LX/BPA;

    if-ne v1, v2, :cond_4

    .line 2301372
    iget-object v1, v0, LX/BQB;->c:LX/BQD;

    const-string v2, "TimelineLoadCoverPhotoLowRes"

    invoke-virtual {v1, v2}, LX/BQD;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 2301373
    :cond_4
    sget-object v1, LX/BPA;->PHOTO_HIGH_RES:LX/BPA;

    if-ne p1, v1, :cond_6

    iget-object v1, v0, LX/BQB;->f:LX/BPC;

    .line 2301374
    iget-object v2, v1, LX/BPC;->e:LX/BPA;

    move-object v1, v2

    .line 2301375
    sget-object v2, LX/BPA;->PHOTO_HIGH_RES:LX/BPA;

    if-eq v1, v2, :cond_6

    .line 2301376
    iget-object v1, v0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v2, 0x2

    invoke-interface {v1, v3, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2301377
    new-instance v2, LX/0P2;

    invoke-direct {v2}, LX/0P2;-><init>()V

    const-string v3, "slow_connection"

    iget-object v1, v0, LX/BQB;->d:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->g()Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "yes"

    :goto_1
    invoke-virtual {v2, v3, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    .line 2301378
    iget-object v2, v0, LX/BQB;->c:LX/BQD;

    const-string v3, "TimelineLoadCoverPhoto"

    invoke-virtual {v2, v3, v1}, LX/BQD;->b(Ljava/lang/String;LX/0P1;)V

    goto :goto_0

    .line 2301379
    :cond_5
    const-string v1, "no"

    goto :goto_1

    .line 2301380
    :cond_6
    sget-object v1, LX/BPA;->PHOTO_LOW_RES_FAILED:LX/BPA;

    if-ne p1, v1, :cond_9

    .line 2301381
    new-instance v2, LX/0P2;

    invoke-direct {v2}, LX/0P2;-><init>()V

    const-string v3, "slow_connection"

    iget-object v1, v0, LX/BQB;->d:LX/0kb;

    invoke-virtual {v1}, LX/0kb;->g()Z

    move-result v1

    if-eqz v1, :cond_8

    const-string v1, "yes"

    :goto_2
    invoke-virtual {v2, v3, v1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v1

    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    .line 2301382
    iget-object v2, v0, LX/BQB;->c:LX/BQD;

    const-string v3, "TimelineLoadCoverPhotoLowRes"

    .line 2301383
    iget-object v4, v2, LX/BQD;->a:LX/11i;

    sget-object v5, LX/BQF;->a:LX/BQE;

    invoke-interface {v4, v5}, LX/11i;->e(LX/0Pq;)LX/11o;

    move-result-object v4

    .line 2301384
    if-eqz v4, :cond_7

    .line 2301385
    const/4 v6, 0x0

    iget-object v5, v2, LX/BQD;->c:LX/0So;

    invoke-interface {v5}, LX/0So;->now()J

    move-result-wide v8

    const v10, -0x757a53df

    move-object v5, v3

    move-object v7, v1

    invoke-static/range {v4 .. v10}, LX/096;->c(LX/11o;Ljava/lang/String;Ljava/lang/String;LX/0P1;JI)LX/11o;

    .line 2301386
    :cond_7
    goto/16 :goto_0

    .line 2301387
    :cond_8
    const-string v1, "no"

    goto :goto_2

    .line 2301388
    :cond_9
    sget-object v1, LX/BPA;->PHOTO_HIGH_RES_FAILED:LX/BPA;

    if-ne p1, v1, :cond_0

    .line 2301389
    iget-object v1, v0, LX/BQB;->b:Lcom/facebook/quicklog/QuickPerformanceLogger;

    const/4 v2, 0x3

    invoke-interface {v1, v3, v2}, Lcom/facebook/quicklog/QuickPerformanceLogger;->b(IS)V

    .line 2301390
    iget-object v1, v0, LX/BQB;->c:LX/BQD;

    const-string v2, "TimelineLoadCoverPhoto"

    invoke-virtual {v1, v2}, LX/BQD;->d(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final a(LX/BQ1;LX/G4q;)V
    .locals 3
    .param p1    # LX/BQ1;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2301336
    iget-object v0, p0, LX/FvO;->a:LX/BQB;

    .line 2301337
    iget-boolean v1, v0, LX/BQB;->k:Z

    if-nez v1, :cond_0

    .line 2301338
    iget-object v1, v0, LX/BQB;->c:LX/BQD;

    const-string v2, "TimelineFirstOnDraw"

    invoke-virtual {v1, v2}, LX/BQD;->f(Ljava/lang/String;)V

    .line 2301339
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/BQB;->k:Z

    .line 2301340
    :cond_0
    if-nez p1, :cond_2

    .line 2301341
    :cond_1
    :goto_0
    return-void

    .line 2301342
    :cond_2
    invoke-virtual {p1}, LX/BPy;->j()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2301343
    iget-object v0, p0, LX/FvO;->a:LX/BQB;

    .line 2301344
    iget-object v1, v0, LX/BQB;->f:LX/BPC;

    .line 2301345
    iget-boolean v2, v1, LX/BPC;->a:Z

    move v1, v2

    .line 2301346
    if-eqz v1, :cond_3

    .line 2301347
    :goto_1
    const/4 v0, 0x0

    .line 2301348
    iput-object v0, p2, LX/G4q;->a:LX/FqL;

    .line 2301349
    invoke-direct {p0}, LX/FvO;->a()V

    goto :goto_0

    .line 2301350
    :cond_3
    iget-object v1, v0, LX/BQB;->c:LX/BQD;

    const-string v2, "TimelineRenderCoreHeader"

    invoke-virtual {v1, v2}, LX/BQD;->f(Ljava/lang/String;)V

    .line 2301351
    iget-object v1, v0, LX/BQB;->f:LX/BPC;

    .line 2301352
    const/4 v2, 0x1

    iput-boolean v2, v1, LX/BPC;->a:Z

    .line 2301353
    const-string v1, "core_header_rendered"

    invoke-static {v0, v1}, LX/BQB;->f(LX/BQB;Ljava/lang/String;)V

    .line 2301354
    const-string v1, "core_header_rendered"

    invoke-static {v0, v1}, LX/BQB;->g(LX/BQB;Ljava/lang/String;)V

    .line 2301355
    const-string v1, "header rendered"

    invoke-static {v0, v1}, LX/BQB;->i(LX/BQB;Ljava/lang/String;)V

    .line 2301356
    invoke-static {v0}, LX/BQB;->v(LX/BQB;)V

    goto :goto_1
.end method

.method public final a(Lcom/facebook/timeline/TimelineFragment;)V
    .locals 0
    .param p1    # Lcom/facebook/timeline/TimelineFragment;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2301334
    iput-object p1, p0, LX/FvO;->e:Lcom/facebook/timeline/TimelineFragment;

    .line 2301335
    return-void
.end method

.method public final b(LX/BPA;)V
    .locals 3

    .prologue
    .line 2301315
    iget-object v0, p0, LX/FvO;->a:LX/BQB;

    .line 2301316
    sget-object v1, LX/BPA;->PHOTO_LOW_RES:LX/BPA;

    if-ne p1, v1, :cond_1

    iget-object v1, v0, LX/BQB;->f:LX/BPC;

    .line 2301317
    iget-object v2, v1, LX/BPC;->d:LX/BPA;

    move-object v1, v2

    .line 2301318
    sget-object v2, LX/BPA;->PHOTO_NOT_LOADED:LX/BPA;

    if-ne v1, v2, :cond_1

    .line 2301319
    iget-object v1, v0, LX/BQB;->c:LX/BQD;

    const-string v2, "TimelineLoadProfilePicPreview"

    invoke-virtual {v1, v2}, LX/BQD;->b(Ljava/lang/String;)V

    .line 2301320
    :cond_0
    :goto_0
    iget-object v1, v0, LX/BQB;->f:LX/BPC;

    .line 2301321
    iput-object p1, v1, LX/BPC;->d:LX/BPA;

    .line 2301322
    const-string v1, "profile photo loaded"

    invoke-static {v0, v1}, LX/BQB;->i(LX/BQB;Ljava/lang/String;)V

    .line 2301323
    invoke-static {v0}, LX/BQB;->v(LX/BQB;)V

    .line 2301324
    invoke-direct {p0}, LX/FvO;->a()V

    .line 2301325
    return-void

    .line 2301326
    :cond_1
    sget-object v1, LX/BPA;->PHOTO_HIGH_RES:LX/BPA;

    if-ne p1, v1, :cond_2

    iget-object v1, v0, LX/BQB;->f:LX/BPC;

    .line 2301327
    iget-object v2, v1, LX/BPC;->d:LX/BPA;

    move-object v1, v2

    .line 2301328
    sget-object v2, LX/BPA;->PHOTO_HIGH_RES:LX/BPA;

    if-eq v1, v2, :cond_2

    .line 2301329
    iget-object v1, v0, LX/BQB;->c:LX/BQD;

    const-string v2, "TimelineLoadProfilePic"

    invoke-virtual {v1, v2}, LX/BQD;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 2301330
    :cond_2
    sget-object v1, LX/BPA;->PHOTO_LOW_RES_FAILED:LX/BPA;

    if-ne p1, v1, :cond_3

    .line 2301331
    iget-object v1, v0, LX/BQB;->c:LX/BQD;

    const-string v2, "TimelineLoadProfilePicPreview"

    invoke-virtual {v1, v2}, LX/BQD;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 2301332
    :cond_3
    sget-object v1, LX/BPA;->PHOTO_HIGH_RES_FAILED:LX/BPA;

    if-ne p1, v1, :cond_0

    .line 2301333
    iget-object v1, v0, LX/BQB;->c:LX/BQD;

    const-string v2, "TimelineLoadProfilePic"

    invoke-virtual {v1, v2}, LX/BQD;->d(Ljava/lang/String;)V

    goto :goto_0
.end method
