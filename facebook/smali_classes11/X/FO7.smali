.class public LX/FO7;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/3MV;

.field private final b:LX/3Mw;

.field private final c:Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/3Mx;

.field private f:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/3MV;LX/3Mw;Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;LX/0Or;LX/3Mx;)V
    .locals 0
    .param p4    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/ViewerContextUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/3MV;",
            "LX/3Mw;",
            "Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;",
            "LX/3Mx;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2234349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2234350
    iput-object p1, p0, LX/FO7;->a:LX/3MV;

    .line 2234351
    iput-object p2, p0, LX/FO7;->b:LX/3Mw;

    .line 2234352
    iput-object p3, p0, LX/FO7;->c:Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    .line 2234353
    iput-object p4, p0, LX/FO7;->d:LX/0Or;

    .line 2234354
    iput-object p5, p0, LX/FO7;->e:LX/3Mx;

    .line 2234355
    return-void
.end method

.method private static a(LX/FO7;)J
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2234346
    iget-object v0, p0, LX/FO7;->e:LX/3Mx;

    const-string v1, "Last thread info is missing"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, LX/FO7;->f:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, LX/3Mx;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2234347
    iget-object v0, p0, LX/FO7;->e:LX/3Mx;

    const-string v1, "Last thread time is missing"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, LX/FO7;->f:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;->aa()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, LX/3Mx;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2234348
    iget-object v0, p0, LX/FO7;->f:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;->aa()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(LX/0Px;)LX/0Px;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2234340
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v2

    .line 2234341
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {p0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    .line 2234342
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;->A()Z

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_0

    .line 2234343
    invoke-virtual {v2, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2234344
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2234345
    :cond_1
    invoke-virtual {v2}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method private static a(LX/FO7;ILcom/facebook/common/callercontext/CallerContext;LX/6ek;)Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;
    .locals 7
    .param p1    # I
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2234356
    new-instance v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;

    invoke-static {p0}, LX/FO7;->a(LX/FO7;)J

    move-result-wide v2

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    invoke-static {p0}, LX/FO7;->b(LX/FO7;)J

    move-result-wide v4

    move-object v1, p3

    move v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;-><init>(LX/6ek;JJI)V

    .line 2234357
    invoke-virtual {p0, v0, p2}, LX/FO7;->a(Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/List;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;",
            ">;",
            "LX/0Px",
            "<",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2234336
    invoke-virtual {p2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2234337
    :goto_0
    return-void

    .line 2234338
    :cond_0
    invoke-virtual {p2}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p2, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    iput-object v0, p0, LX/FO7;->f:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    .line 2234339
    invoke-interface {p1, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private static b(LX/FO7;)J
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2234329
    iget-object v0, p0, LX/FO7;->e:LX/3Mx;

    const-string v1, "Last thread info is missing"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, LX/FO7;->f:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, LX/3Mx;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2234330
    iget-object v1, p0, LX/FO7;->b:LX/3Mw;

    iget-object v2, p0, LX/FO7;->f:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    iget-object v0, p0, LX/FO7;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v1, v2, v0}, LX/3Mw;->a(LX/5Vt;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threadkey/ThreadKey;

    move-result-object v0

    .line 2234331
    if-nez v0, :cond_0

    .line 2234332
    iget-object v0, p0, LX/FO7;->e:LX/3Mx;

    const-string v1, "Last thread info empty thread key"

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, LX/FO7;->f:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;->n()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, LX/3Mx;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2234333
    iget-object v0, p0, LX/FO7;->e:LX/3Mx;

    const-string v1, "Last thread info thread key empty fbid "

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p0, LX/FO7;->f:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;->n()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, LX/3Mx;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2234334
    iget-object v0, p0, LX/FO7;->f:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;->n()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel$ThreadKeyModel;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 2234335
    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->j()J

    move-result-wide v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;
    .locals 12
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 2234251
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 2234252
    iget v0, p1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->e:I

    move v10, v0

    .line 2234253
    iget-object v0, p0, LX/FO7;->c:Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel;

    move-result-object v1

    .line 2234254
    iget-object v0, p0, LX/FO7;->e:LX/3Mx;

    const-string v2, "Fetch-more-other-threads null response"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v0, v2, v3}, LX/3Mx;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2234255
    iget-object v0, p0, LX/FO7;->e:LX/3Mx;

    const-string v2, "Page info missing"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel;->j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel$PageInfoModel;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v0, v2, v3}, LX/3Mx;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2234256
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel;->a()LX/0Px;

    move-result-object v0

    invoke-static {v0}, LX/FO7;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    .line 2234257
    invoke-direct {p0, v9, v0}, LX/FO7;->a(Ljava/util/List;LX/0Px;)V

    move-object v4, v1

    .line 2234258
    :goto_0
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v1

    if-ge v1, v10, :cond_0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2234259
    new-instance v0, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;

    .line 2234260
    iget-object v1, p1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->a:LX/6ek;

    move-object v1, v1

    .line 2234261
    invoke-static {p0}, LX/FO7;->a(LX/FO7;)J

    move-result-wide v2

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    invoke-static {p0}, LX/FO7;->b(LX/FO7;)J

    move-result-wide v4

    const/4 v6, 0x4

    invoke-direct/range {v0 .. v6}, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;-><init>(LX/6ek;JJI)V

    .line 2234262
    iget-object v1, p0, LX/FO7;->c:Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    invoke-virtual {v1, v0, p2}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel;

    move-result-object v2

    .line 2234263
    iget-object v1, p0, LX/FO7;->e:LX/3Mx;

    const-string v3, "Fetch-more-other-threads null response"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v1, v3, v4}, LX/3Mx;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2234264
    iget-object v1, p0, LX/FO7;->e:LX/3Mx;

    const-string v3, "Page info missing"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel;->j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel$PageInfoModel;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v1, v3, v4}, LX/3Mx;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2234265
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel;->a()LX/0Px;

    move-result-object v1

    invoke-static {v1}, LX/FO7;->a(LX/0Px;)LX/0Px;

    move-result-object v1

    .line 2234266
    invoke-direct {p0, v9, v1}, LX/FO7;->a(Ljava/util/List;LX/0Px;)V

    move-object v4, v2

    move-object p1, v0

    move-object v0, v1

    goto :goto_0

    .line 2234267
    :cond_0
    new-instance v1, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;

    sget-object v2, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_SERVER:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2234268
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchMoreThreadsParams;->a:LX/6ek;

    move-object v3, v0

    .line 2234269
    iget-object v5, p0, LX/FO7;->b:LX/3Mw;

    invoke-static {v9}, LX/0Px;->copyOf(Ljava/util/Collection;)LX/0Px;

    move-result-object v6

    const/4 v11, 0x0

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, v10, :cond_1

    move v4, v7

    :goto_1
    iget-object v0, p0, LX/FO7;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v5, v6, v11, v4, v0}, LX/3Mw;->a(LX/0Px;LX/0P1;ZLcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-result-object v4

    iget-object v0, p0, LX/FO7;->a:LX/3MV;

    invoke-virtual {v0, v9}, LX/3MV;->a(Ljava/util/List;)LX/0Px;

    move-result-object v5

    .line 2234270
    sget-object v0, LX/0SF;->a:LX/0SF;

    move-object v0, v0

    .line 2234271
    invoke-virtual {v0}, LX/0SF;->a()J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;-><init>(Lcom/facebook/fbservice/results/DataFetchDisposition;LX/6ek;Lcom/facebook/messaging/model/threads/ThreadsCollection;LX/0Px;J)V

    return-object v1

    :cond_1
    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel;->j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel$PageInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$MoreThreadsQueryModel$MessageThreadsModel$PageInfoModel;->a()Z

    move-result v0

    if-nez v0, :cond_2

    move v4, v7

    goto :goto_1

    :cond_2
    move v4, v8

    goto :goto_1
.end method

.method public final a(Lcom/facebook/messaging/service/model/FetchThreadListParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/service/model/FetchThreadListResult;
    .locals 9
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2234272
    iget-object v0, p0, LX/FO7;->c:Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;

    invoke-virtual {v0, p1, p2}, Lcom/facebook/messaging/threads/graphql/GQLThreadQueryHelper;->a(Lcom/facebook/messaging/service/model/FetchThreadListParams;Lcom/facebook/common/callercontext/CallerContext;)Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel;

    move-result-object v4

    .line 2234273
    iget-object v0, p0, LX/FO7;->e:LX/3Mx;

    const-string v1, "Fetch-other-threads null response"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-virtual {v0, v1, v5}, LX/3Mx;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2234274
    iget-object v0, p0, LX/FO7;->e:LX/3Mx;

    const-string v1, "Page info missing"

    new-array v5, v2, [Ljava/lang/Object;

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->k()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel$PageInfoModel;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-virtual {v0, v1, v5}, LX/3Mx;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2234275
    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->j()LX/0Px;

    move-result-object v1

    .line 2234276
    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2234277
    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    iput-object v0, p0, LX/FO7;->f:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadInfoModel;

    .line 2234278
    :cond_0
    iget-object v0, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v0, v0

    .line 2234279
    sget-object v5, LX/6ek;->OTHER:LX/6ek;

    if-ne v0, v5, :cond_1

    .line 2234280
    invoke-static {v1}, LX/FO7;->a(LX/0Px;)LX/0Px;

    move-result-object v0

    move-object v1, v0

    .line 2234281
    :cond_1
    iget-object v5, p0, LX/FO7;->b:LX/3Mw;

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->j()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    invoke-virtual {p1}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->g()I

    move-result v6

    if-ge v0, v6, :cond_4

    :goto_0
    iget-object v0, p0, LX/FO7;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    invoke-virtual {v5, v1, v2, v0}, LX/3Mw;->a(LX/0Px;ZLcom/facebook/user/model/User;)Lcom/facebook/messaging/model/threads/ThreadsCollection;

    move-result-object v0

    .line 2234282
    iget-object v2, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v2, v2

    .line 2234283
    sget-object v5, LX/6ek;->OTHER:LX/6ek;

    if-ne v2, v5, :cond_3

    .line 2234284
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->e()I

    move-result v2

    const/4 v5, 0x4

    if-ge v2, v5, :cond_3

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {p1}, Lcom/facebook/messaging/service/model/FetchThreadListParams;->g()I

    move-result v2

    if-ne v1, v2, :cond_3

    .line 2234285
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->e()I

    move-result v1

    rsub-int/lit8 v1, v1, 0x4

    .line 2234286
    iget-object v2, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v2, v2

    .line 2234287
    invoke-static {p0, v1, p2, v2}, LX/FO7;->a(LX/FO7;ILcom/facebook/common/callercontext/CallerContext;LX/6ek;)Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;

    move-result-object v1

    .line 2234288
    iget-object v1, v1, Lcom/facebook/messaging/service/model/FetchMoreThreadsResult;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    const/4 v5, 0x0

    .line 2234289
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2234290
    :cond_2
    :goto_1
    move-object v0, v0

    .line 2234291
    :cond_3
    invoke-static {}, Lcom/facebook/messaging/service/model/FetchThreadListResult;->newBuilder()LX/6iK;

    move-result-object v1

    sget-object v2, Lcom/facebook/fbservice/results/DataFetchDisposition;->FROM_SERVER:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2234292
    iput-object v2, v1, LX/6iK;->a:Lcom/facebook/fbservice/results/DataFetchDisposition;

    .line 2234293
    move-object v1, v1

    .line 2234294
    iget-object v2, p1, Lcom/facebook/messaging/service/model/FetchThreadListParams;->b:LX/6ek;

    move-object v2, v2

    .line 2234295
    iput-object v2, v1, LX/6iK;->b:LX/6ek;

    .line 2234296
    move-object v1, v1

    .line 2234297
    iput-object v0, v1, LX/6iK;->c:Lcom/facebook/messaging/model/threads/ThreadsCollection;

    .line 2234298
    move-object v0, v1

    .line 2234299
    iput-boolean v3, v0, LX/6iK;->i:Z

    .line 2234300
    move-object v0, v0

    .line 2234301
    sget-object v1, LX/0SF;->a:LX/0SF;

    move-object v1, v1

    .line 2234302
    invoke-virtual {v1}, LX/0SF;->a()J

    move-result-wide v2

    .line 2234303
    iput-wide v2, v0, LX/6iK;->j:J

    .line 2234304
    move-object v0, v0

    .line 2234305
    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$ThreadListQueryModel$MessageThreadsModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2234306
    iput-wide v2, v0, LX/6iK;->l:J

    .line 2234307
    move-object v0, v0

    .line 2234308
    invoke-virtual {v0}, LX/6iK;->m()Lcom/facebook/messaging/service/model/FetchThreadListResult;

    move-result-object v0

    .line 2234309
    return-object v0

    :cond_4
    move v2, v3

    .line 2234310
    goto :goto_0

    .line 2234311
    :cond_5
    invoke-virtual {v0}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d()Z

    move-result v2

    if-eqz v2, :cond_6

    move-object v0, v1

    .line 2234312
    goto :goto_1

    .line 2234313
    :cond_6
    invoke-virtual {v1}, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2234314
    new-instance v7, LX/0UE;

    invoke-direct {v7}, LX/0UE;-><init>()V

    .line 2234315
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v8

    .line 2234316
    iget-object v2, v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object p0, v2

    .line 2234317
    invoke-virtual {p0}, LX/0Px;->size()I

    move-result p2

    move v6, v5

    :goto_2
    if-ge v6, p2, :cond_7

    invoke-virtual {p0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2234318
    invoke-virtual {v8, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2234319
    iget-object v2, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v7, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2234320
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    goto :goto_2

    .line 2234321
    :cond_7
    iget-object v2, v1, Lcom/facebook/messaging/model/threads/ThreadsCollection;->c:LX/0Px;

    move-object v6, v2

    .line 2234322
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result p0

    :goto_3
    if-ge v5, p0, :cond_9

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/messaging/model/threads/ThreadSummary;

    .line 2234323
    iget-object p2, v2, Lcom/facebook/messaging/model/threads/ThreadSummary;->a:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-interface {v7, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_8

    .line 2234324
    invoke-virtual {v8, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2234325
    :cond_8
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_3

    .line 2234326
    :cond_9
    new-instance v0, Lcom/facebook/messaging/model/threads/ThreadsCollection;

    invoke-virtual {v8}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2234327
    iget-boolean v5, v1, Lcom/facebook/messaging/model/threads/ThreadsCollection;->d:Z

    move v5, v5

    .line 2234328
    invoke-direct {v0, v2, v5}, Lcom/facebook/messaging/model/threads/ThreadsCollection;-><init>(LX/0Px;Z)V

    goto/16 :goto_1
.end method
