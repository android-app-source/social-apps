.class public final enum LX/Gzf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gzf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gzf;

.field public static final enum DISCARD_FORM_CHANGES:LX/Gzf;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2412197
    new-instance v0, LX/Gzf;

    const-string v1, "DISCARD_FORM_CHANGES"

    invoke-direct {v0, v1, v2}, LX/Gzf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gzf;->DISCARD_FORM_CHANGES:LX/Gzf;

    .line 2412198
    const/4 v0, 0x1

    new-array v0, v0, [LX/Gzf;

    sget-object v1, LX/Gzf;->DISCARD_FORM_CHANGES:LX/Gzf;

    aput-object v1, v0, v2

    sput-object v0, LX/Gzf;->$VALUES:[LX/Gzf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2412199
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gzf;
    .locals 1

    .prologue
    .line 2412200
    const-class v0, LX/Gzf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gzf;

    return-object v0
.end method

.method public static values()[LX/Gzf;
    .locals 1

    .prologue
    .line 2412201
    sget-object v0, LX/Gzf;->$VALUES:[LX/Gzf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gzf;

    return-object v0
.end method
