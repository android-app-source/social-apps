.class public LX/Fsw;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static e:LX/0Xm;


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/0Vd",
            "<*>;>;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation runtime Lcom/facebook/common/executors/ForUiThread;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/BQB;

.field public final d:[Z


# direct methods
.method public constructor <init>(LX/0Or;LX/BQB;)V
    .locals 2
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "LX/BQB;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2297807
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2297808
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/Fsw;->a:Ljava/util/List;

    .line 2297809
    const/4 v0, 0x1

    new-array v0, v0, [Z

    aput-boolean v1, v0, v1

    iput-object v0, p0, LX/Fsw;->d:[Z

    .line 2297810
    iput-object p1, p0, LX/Fsw;->b:LX/0Or;

    .line 2297811
    iput-object p2, p0, LX/Fsw;->c:LX/BQB;

    .line 2297812
    return-void
.end method

.method public static a(LX/0QB;)LX/Fsw;
    .locals 5

    .prologue
    .line 2297770
    const-class v1, LX/Fsw;

    monitor-enter v1

    .line 2297771
    :try_start_0
    sget-object v0, LX/Fsw;->e:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2297772
    sput-object v2, LX/Fsw;->e:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2297773
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2297774
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2297775
    new-instance v4, LX/Fsw;

    const/16 v3, 0x1430

    invoke-static {v0, v3}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/BQB;->a(LX/0QB;)LX/BQB;

    move-result-object v3

    check-cast v3, LX/BQB;

    invoke-direct {v4, p0, v3}, LX/Fsw;-><init>(LX/0Or;LX/BQB;)V

    .line 2297776
    move-object v0, v4

    .line 2297777
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2297778
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fsw;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2297779
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2297780
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(ZLX/FsE;)V
    .locals 2
    .param p1    # LX/FsE;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    .line 2297781
    if-eqz p1, :cond_3

    .line 2297782
    iget-object v0, p1, LX/FsE;->a:LX/1Zp;

    if-eqz v0, :cond_0

    .line 2297783
    iget-object v0, p1, LX/FsE;->a:LX/1Zp;

    invoke-virtual {v0, v1}, LX/1Zp;->cancel(Z)Z

    .line 2297784
    :cond_0
    if-eqz p0, :cond_1

    iget-object v0, p1, LX/FsE;->b:LX/1Zp;

    if-eqz v0, :cond_1

    .line 2297785
    iget-object v0, p1, LX/FsE;->b:LX/1Zp;

    invoke-virtual {v0, v1}, LX/1Zp;->cancel(Z)Z

    .line 2297786
    :cond_1
    iget-object v0, p1, LX/FsE;->c:LX/1Zp;

    if-eqz v0, :cond_2

    .line 2297787
    iget-object v0, p1, LX/FsE;->c:LX/1Zp;

    invoke-virtual {v0, v1}, LX/1Zp;->cancel(Z)Z

    .line 2297788
    :cond_2
    iget-object v0, p1, LX/FsE;->d:LX/1Zp;

    if-eqz v0, :cond_3

    .line 2297789
    iget-object v0, p1, LX/FsE;->d:LX/1Zp;

    invoke-virtual {v0, v1}, LX/1Zp;->cancel(Z)Z

    .line 2297790
    :cond_3
    return-void
.end method

.method public static b(Lcom/facebook/graphql/executor/GraphQLResult;)Z
    .locals 1
    .param p0    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .line 2297791
    if-eqz p0, :cond_0

    .line 2297792
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2297793
    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Lcom/facebook/graphql/executor/GraphQLResult;Ljava/lang/ref/WeakReference;)V
    .locals 4
    .param p0    # Lcom/facebook/graphql/executor/GraphQLResult;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<*>;",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/facebook/timeline/delegate/TimelineFragmentHeaderFetchCallbackDelegate$Listener;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2297794
    invoke-virtual {p1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/TimelineFragment;

    .line 2297795
    if-nez v0, :cond_0

    .line 2297796
    :goto_0
    return-void

    .line 2297797
    :cond_0
    :try_start_0
    const-string v1, "TimelineFragment.onFetchHeaderSucceeded"

    const v2, -0x64831e1f

    invoke-static {v1, v2}, LX/02m;->a(Ljava/lang/String;I)V

    .line 2297798
    const/4 v3, 0x0

    .line 2297799
    sget-object v2, LX/0ta;->NO_DATA:LX/0ta;

    .line 2297800
    sget-object v1, Lcom/facebook/timeline/protocol/ResultSource;->UNDEFINED:Lcom/facebook/timeline/protocol/ResultSource;

    .line 2297801
    if-eqz p0, :cond_1

    .line 2297802
    iget-object v1, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v3, v1

    .line 2297803
    iget-object v1, p0, Lcom/facebook/fbservice/results/BaseResult;->freshness:LX/0ta;

    move-object v2, v1

    .line 2297804
    invoke-static {v2}, Lcom/facebook/timeline/protocol/ResultSource;->fromGraphQLResultDataFreshness(LX/0ta;)Lcom/facebook/timeline/protocol/ResultSource;

    move-result-object v1

    .line 2297805
    :cond_1
    invoke-virtual {v0, v3, v2, v1}, Lcom/facebook/timeline/TimelineFragment;->a(Ljava/lang/Object;LX/0ta;Lcom/facebook/timeline/protocol/ResultSource;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2297806
    const v0, -0x24db1c22

    invoke-static {v0}, LX/02m;->a(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    const v1, 0x49ee2f61

    invoke-static {v1}, LX/02m;->a(I)V

    throw v0
.end method
