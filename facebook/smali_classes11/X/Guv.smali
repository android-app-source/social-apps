.class public final LX/Guv;
.super LX/BWM;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 2404029
    iput-object p1, p0, LX/Guv;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2404030
    invoke-direct {p0, p2}, LX/BWM;-><init>(Landroid/os/Handler;)V

    .line 2404031
    return-void
.end method


# virtual methods
.method public final a(LX/BWN;)V
    .locals 3

    .prologue
    .line 2404032
    iget-object v0, p0, LX/Guv;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-interface {p1}, LX/BWN;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "pageLoading"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    .line 2404033
    iput-boolean v1, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->L:Z

    .line 2404034
    return-void
.end method

.method public final a(Lcom/facebook/webview/FacebookWebView;LX/BWN;)V
    .locals 5

    .prologue
    .line 2404035
    iget-object v0, p0, LX/Guv;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v1, p0, LX/Guv;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-boolean v1, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->L:Z

    .line 2404036
    iput-boolean v1, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->S:Z

    .line 2404037
    iget-object v0, p0, LX/Guv;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2404038
    iget-object v1, v0, Landroid/support/v4/app/Fragment;->mView:Landroid/view/View;

    move-object v0, v1

    .line 2404039
    if-nez v0, :cond_1

    .line 2404040
    :cond_0
    :goto_0
    return-void

    .line 2404041
    :cond_1
    iget-object v0, p0, LX/Guv;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v0}, Lcom/facebook/base/fragment/FbFragment;->jG_()Landroid/app/Activity;

    move-result-object v1

    .line 2404042
    iget-object v0, p0, LX/Guv;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->V:LX/0zF;

    .line 2404043
    iget-object v2, v0, LX/0zF;->a:Lcom/facebook/ui/titlebar/Fb4aTitleBar;

    move-object v0, v2

    .line 2404044
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 2404045
    iget-object v0, p0, LX/Guv;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->T:LX/0hx;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, LX/0hx;->a(Z)V

    .line 2404046
    iget-object v0, p0, LX/Guv;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v2, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->T:LX/0hx;

    iget-object v0, p0, LX/Guv;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-boolean v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->L:Z

    if-eqz v0, :cond_2

    sget-object v0, LX/3zI;->LOCAL_DATA:LX/3zI;

    :goto_1
    iget-object v3, p0, LX/Guv;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v3}, Lcom/facebook/katana/fragment/BaseFacebookFragment;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/Guv;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v4, v4, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->U:LX/0kv;

    invoke-virtual {v4, v1}, LX/0kv;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v3, v1}, LX/0hx;->a(LX/3zI;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    sget-object v0, LX/3zI;->NETWORK_DATA:LX/3zI;

    goto :goto_1
.end method
