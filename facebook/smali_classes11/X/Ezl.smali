.class public LX/Ezl;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static h:LX/0Xm;


# instance fields
.field public final a:LX/Ezn;

.field private final b:LX/Ezp;

.field public final c:LX/Ezm;

.field public d:LX/Ezk;

.field public e:LX/Ezg;

.field private f:LX/Ezj;

.field private final g:Z


# direct methods
.method public constructor <init>(LX/Ezn;LX/Ezp;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2187636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2187637
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Ezl;->g:Z

    .line 2187638
    iput-object p1, p0, LX/Ezl;->a:LX/Ezn;

    .line 2187639
    iput-object p2, p0, LX/Ezl;->b:LX/Ezp;

    .line 2187640
    new-instance v0, LX/Ezm;

    invoke-direct {v0}, LX/Ezm;-><init>()V

    iput-object v0, p0, LX/Ezl;->c:LX/Ezm;

    .line 2187641
    return-void
.end method

.method public static a(LX/0QB;)LX/Ezl;
    .locals 7

    .prologue
    .line 2187622
    const-class v1, LX/Ezl;

    monitor-enter v1

    .line 2187623
    :try_start_0
    sget-object v0, LX/Ezl;->h:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2187624
    sput-object v2, LX/Ezl;->h:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2187625
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2187626
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2187627
    new-instance v5, LX/Ezl;

    invoke-static {v0}, LX/Ezn;->a(LX/0QB;)LX/Ezn;

    move-result-object v3

    check-cast v3, LX/Ezn;

    .line 2187628
    new-instance p0, LX/Ezp;

    invoke-static {v0}, LX/2dk;->b(LX/0QB;)LX/2dk;

    move-result-object v4

    check-cast v4, LX/2dk;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-direct {p0, v4, v6}, LX/Ezp;-><init>(LX/2dk;LX/1Ck;)V

    .line 2187629
    move-object v4, p0

    .line 2187630
    check-cast v4, LX/Ezp;

    invoke-direct {v5, v3, v4}, LX/Ezl;-><init>(LX/Ezn;LX/Ezp;)V

    .line 2187631
    move-object v0, v5

    .line 2187632
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2187633
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Ezl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2187634
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2187635
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(I)Lcom/facebook/friends/model/PersonYouMayKnow;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2187590
    iget-object v0, p0, LX/Ezl;->a:LX/Ezn;

    .line 2187591
    iget-object v1, v0, LX/Ezn;->a:Ljava/util/List;

    move-object v0, v1

    .line 2187592
    add-int/lit8 v1, p1, -0x1

    .line 2187593
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    if-gez v1, :cond_1

    .line 2187594
    :cond_0
    const/4 v0, 0x0

    .line 2187595
    :goto_0
    return-object v0

    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/friends/model/PersonYouMayKnow;

    goto :goto_0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 2187613
    sget-object v0, LX/Ezk;->LOADING:LX/Ezk;

    iput-object v0, p0, LX/Ezl;->d:LX/Ezk;

    .line 2187614
    iget-object v0, p0, LX/Ezl;->a:LX/Ezn;

    .line 2187615
    iget-object v1, v0, LX/Ezn;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-object v0, v1

    .line 2187616
    if-nez v0, :cond_0

    .line 2187617
    iget-object v0, p0, LX/Ezl;->b:LX/Ezp;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/Ezp;->a(Ljava/lang/String;)V

    .line 2187618
    :goto_0
    return-void

    .line 2187619
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2187620
    iget-object v1, p0, LX/Ezl;->b:LX/Ezp;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/Ezp;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 2187621
    :cond_1
    sget-object v0, LX/Ezk;->IDLE:LX/Ezk;

    iput-object v0, p0, LX/Ezl;->d:LX/Ezk;

    goto :goto_0
.end method

.method public final a(LX/Ezg;)V
    .locals 1

    .prologue
    .line 2187609
    iput-object p1, p0, LX/Ezl;->e:LX/Ezg;

    .line 2187610
    iget-object v0, p0, LX/Ezl;->b:LX/Ezp;

    .line 2187611
    iput-object p0, v0, LX/Ezp;->c:LX/Ezl;

    .line 2187612
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 2187599
    iget-object v0, p0, LX/Ezl;->b:LX/Ezp;

    .line 2187600
    iget-object v1, v0, LX/Ezp;->b:LX/1Ck;

    invoke-virtual {v1}, LX/1Ck;->c()V

    .line 2187601
    const/4 v1, 0x0

    iput-object v1, v0, LX/Ezp;->c:LX/Ezl;

    .line 2187602
    const/4 v0, 0x0

    iput-object v0, p0, LX/Ezl;->e:LX/Ezg;

    .line 2187603
    if-eqz p1, :cond_0

    .line 2187604
    :goto_0
    return-void

    .line 2187605
    :cond_0
    iget-object v0, p0, LX/Ezl;->a:LX/Ezn;

    .line 2187606
    iget-object v1, v0, LX/Ezn;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 2187607
    const/4 v1, 0x0

    iput-object v1, v0, LX/Ezn;->b:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2187608
    goto :goto_0
.end method

.method public final b()LX/Ezj;
    .locals 2

    .prologue
    .line 2187596
    iget-object v0, p0, LX/Ezl;->f:LX/Ezj;

    if-nez v0, :cond_0

    .line 2187597
    new-instance v0, LX/Ezj;

    invoke-direct {v0, p0}, LX/Ezj;-><init>(LX/Ezl;)V

    iput-object v0, p0, LX/Ezl;->f:LX/Ezj;

    .line 2187598
    :cond_0
    iget-object v0, p0, LX/Ezl;->f:LX/Ezj;

    return-object v0
.end method
