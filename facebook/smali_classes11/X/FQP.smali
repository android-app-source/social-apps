.class public final enum LX/FQP;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FQP;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FQP;

.field public static final enum FRIENDS_WHO_REVIEWED:LX/FQP;

.field public static final enum FRIENDS_WHO_VISITED:LX/FQP;

.field public static final enum NONE:LX/FQP;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2239343
    new-instance v0, LX/FQP;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v2}, LX/FQP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FQP;->NONE:LX/FQP;

    .line 2239344
    new-instance v0, LX/FQP;

    const-string v1, "FRIENDS_WHO_VISITED"

    invoke-direct {v0, v1, v3}, LX/FQP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FQP;->FRIENDS_WHO_VISITED:LX/FQP;

    .line 2239345
    new-instance v0, LX/FQP;

    const-string v1, "FRIENDS_WHO_REVIEWED"

    invoke-direct {v0, v1, v4}, LX/FQP;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FQP;->FRIENDS_WHO_REVIEWED:LX/FQP;

    .line 2239346
    const/4 v0, 0x3

    new-array v0, v0, [LX/FQP;

    sget-object v1, LX/FQP;->NONE:LX/FQP;

    aput-object v1, v0, v2

    sget-object v1, LX/FQP;->FRIENDS_WHO_VISITED:LX/FQP;

    aput-object v1, v0, v3

    sget-object v1, LX/FQP;->FRIENDS_WHO_REVIEWED:LX/FQP;

    aput-object v1, v0, v4

    sput-object v0, LX/FQP;->$VALUES:[LX/FQP;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2239349
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getComponentTypeForPlace(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;)LX/FQP;
    .locals 1

    .prologue
    .line 2239350
    if-nez p0, :cond_0

    .line 2239351
    sget-object v0, LX/FQP;->NONE:LX/FQP;

    .line 2239352
    :goto_0
    return-object v0

    .line 2239353
    :cond_0
    invoke-virtual {p0}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->e()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;

    move-result-object v0

    .line 2239354
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyPlacesFriendsWhoVisitedFragmentModel$FriendsWhoVisitedModel;->a()I

    move-result v0

    if-nez v0, :cond_5

    .line 2239355
    :cond_1
    const/4 v0, 0x0

    .line 2239356
    :goto_1
    move v0, v0

    .line 2239357
    if-eqz v0, :cond_2

    .line 2239358
    sget-object v0, LX/FQP;->FRIENDS_WHO_VISITED:LX/FQP;

    goto :goto_0

    .line 2239359
    :cond_2
    iget-object v0, p0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->a:LX/CQM;

    invoke-interface {v0}, LX/CQM;->D()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;

    move-result-object v0

    move-object v0, v0

    .line 2239360
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$NearbyRecommendationsConnectionFragmentModel;->a()I

    move-result v0

    if-nez v0, :cond_6

    .line 2239361
    :cond_3
    const/4 v0, 0x0

    .line 2239362
    :goto_2
    move v0, v0

    .line 2239363
    if-eqz v0, :cond_4

    .line 2239364
    sget-object v0, LX/FQP;->FRIENDS_WHO_REVIEWED:LX/FQP;

    goto :goto_0

    .line 2239365
    :cond_4
    sget-object v0, LX/FQP;->NONE:LX/FQP;

    goto :goto_0

    :cond_5
    const/4 v0, 0x1

    goto :goto_1

    :cond_6
    const/4 v0, 0x1

    goto :goto_2
.end method

.method public static valueOf(Ljava/lang/String;)LX/FQP;
    .locals 1

    .prologue
    .line 2239348
    const-class v0, LX/FQP;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FQP;

    return-object v0
.end method

.method public static values()[LX/FQP;
    .locals 1

    .prologue
    .line 2239347
    sget-object v0, LX/FQP;->$VALUES:[LX/FQP;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FQP;

    return-object v0
.end method
