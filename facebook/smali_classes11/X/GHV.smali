.class public LX/GHV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GGc;


# instance fields
.field public i:LX/GGX;

.field public j:LX/GGX;

.field public k:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation
.end field

.field private l:LX/GE3;

.field public m:LX/0ad;


# direct methods
.method public constructor <init>(LX/GE3;LX/GEw;LX/GF0;LX/GIX;LX/GJ2;LX/GIA;LX/GEY;LX/GJi;LX/GMR;LX/GMS;LX/GMK;LX/GLH;LX/GEb;LX/GKn;LX/GLd;LX/GKQ;LX/0ad;)V
    .locals 7
    .param p2    # LX/GEw;
        .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostedComponent;
        .end annotation
    .end param
    .param p16    # LX/GKQ;
        .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostedComponent;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2335025
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2335026
    new-instance v1, LX/GHT;

    invoke-direct {v1, p0}, LX/GHT;-><init>(LX/GHV;)V

    iput-object v1, p0, LX/GHV;->i:LX/GGX;

    .line 2335027
    new-instance v1, LX/GHU;

    invoke-direct {v1, p0}, LX/GHU;-><init>(LX/GHV;)V

    iput-object v1, p0, LX/GHV;->j:LX/GGX;

    .line 2335028
    move-object/from16 v0, p17

    iput-object v0, p0, LX/GHV;->m:LX/0ad;

    .line 2335029
    iput-object p1, p0, LX/GHV;->l:LX/GE3;

    .line 2335030
    new-instance v1, LX/0Pz;

    invoke-direct {v1}, LX/0Pz;-><init>()V

    invoke-virtual {v1, p3}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03006e

    sget-object v4, LX/GHV;->e:LX/GGX;

    sget-object v5, LX/8wK;->AD_PREVIEW:LX/8wK;

    move-object/from16 v0, p16

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030067

    sget-object v4, LX/GHV;->a:LX/GGX;

    sget-object v5, LX/8wK;->INFO_CARD:LX/8wK;

    invoke-direct {v2, v3, p5, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03005a

    sget-object v4, LX/GHV;->a:LX/GGX;

    sget-object v5, LX/8wK;->ERROR_CARD:LX/8wK;

    invoke-direct {v2, v3, p8, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    move-object/from16 v0, p13

    invoke-virtual {v1, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030077

    sget-object v4, LX/GHV;->d:LX/GGX;

    sget-object v5, LX/8wK;->PROMOTION_DETAILS:LX/8wK;

    move-object/from16 v0, p9

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03008d

    sget-object v4, LX/GHV;->d:LX/GGX;

    sget-object v5, LX/8wK;->TARGETING_DESCRIPTION:LX/8wK;

    move-object/from16 v0, p10

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030094

    iget-object v4, p0, LX/GHV;->j:LX/GGX;

    sget-object v5, LX/8wK;->TARGETING:LX/8wK;

    move-object/from16 v0, p15

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030049

    iget-object v4, p0, LX/GHV;->i:LX/GGX;

    sget-object v5, LX/8wK;->TARGETING:LX/8wK;

    invoke-direct {v2, v3, p4, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03004e

    sget-object v4, LX/GHV;->g:LX/GGX;

    sget-object v5, LX/8wK;->BUDGET:LX/8wK;

    move-object/from16 v0, p11

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030082

    sget-object v4, LX/GHV;->g:LX/GGX;

    sget-object v5, LX/8wK;->DURATION:LX/8wK;

    move-object/from16 v0, p12

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1, p7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03003d

    sget-object v4, LX/GHV;->a:LX/GGX;

    sget-object v5, LX/8wK;->ACCOUNT:LX/8wK;

    invoke-direct {v2, v3, p6, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f03005c

    sget-object v4, LX/GHV;->c:LX/GGX;

    sget-object v5, LX/8wK;->FOOTER:LX/8wK;

    move-object/from16 v0, p14

    invoke-direct {v2, v3, v0, v4, v5}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    new-instance v2, LX/GEZ;

    const v3, 0x7f030087

    const/4 v4, 0x0

    sget-object v5, LX/GHV;->a:LX/GGX;

    sget-object v6, LX/8wK;->SPACER:LX/8wK;

    invoke-direct {v2, v3, v4, v5, v6}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v1, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v1

    invoke-virtual {v1}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    iput-object v1, p0, LX/GHV;->k:LX/0Px;

    .line 2335031
    return-void
.end method

.method public static b(LX/0QB;)LX/GHV;
    .locals 18

    .prologue
    .line 2335032
    new-instance v0, LX/GHV;

    invoke-static/range {p0 .. p0}, LX/GE3;->a(LX/0QB;)LX/GE3;

    move-result-object v1

    check-cast v1, LX/GE3;

    invoke-static/range {p0 .. p0}, LX/GDH;->a(LX/0QB;)LX/GEw;

    move-result-object v2

    check-cast v2, LX/GEw;

    invoke-static/range {p0 .. p0}, LX/GF0;->a(LX/0QB;)LX/GF0;

    move-result-object v3

    check-cast v3, LX/GF0;

    invoke-static/range {p0 .. p0}, LX/GIX;->a(LX/0QB;)LX/GIX;

    move-result-object v4

    check-cast v4, LX/GIX;

    invoke-static/range {p0 .. p0}, LX/GJ2;->b(LX/0QB;)LX/GJ2;

    move-result-object v5

    check-cast v5, LX/GJ2;

    invoke-static/range {p0 .. p0}, LX/GIA;->a(LX/0QB;)LX/GIA;

    move-result-object v6

    check-cast v6, LX/GIA;

    invoke-static/range {p0 .. p0}, LX/GEY;->a(LX/0QB;)LX/GEY;

    move-result-object v7

    check-cast v7, LX/GEY;

    invoke-static/range {p0 .. p0}, LX/GJi;->b(LX/0QB;)LX/GJi;

    move-result-object v8

    check-cast v8, LX/GJi;

    invoke-static/range {p0 .. p0}, LX/GMR;->a(LX/0QB;)LX/GMR;

    move-result-object v9

    check-cast v9, LX/GMR;

    invoke-static/range {p0 .. p0}, LX/GMS;->a(LX/0QB;)LX/GMS;

    move-result-object v10

    check-cast v10, LX/GMS;

    invoke-static/range {p0 .. p0}, LX/GMK;->a(LX/0QB;)LX/GMK;

    move-result-object v11

    check-cast v11, LX/GMK;

    invoke-static/range {p0 .. p0}, LX/GLH;->a(LX/0QB;)LX/GLH;

    move-result-object v12

    check-cast v12, LX/GLH;

    invoke-static/range {p0 .. p0}, LX/GEb;->a(LX/0QB;)LX/GEb;

    move-result-object v13

    check-cast v13, LX/GEb;

    invoke-static/range {p0 .. p0}, LX/GKn;->a(LX/0QB;)LX/GKn;

    move-result-object v14

    check-cast v14, LX/GKn;

    invoke-static/range {p0 .. p0}, LX/GLd;->b(LX/0QB;)LX/GLd;

    move-result-object v15

    check-cast v15, LX/GLd;

    invoke-static/range {p0 .. p0}, LX/GCJ;->a(LX/0QB;)LX/GKQ;

    move-result-object v16

    check-cast v16, LX/GKQ;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v17

    check-cast v17, LX/0ad;

    invoke-direct/range {v0 .. v17}, LX/GHV;-><init>(LX/GE3;LX/GEw;LX/GF0;LX/GIX;LX/GJ2;LX/GIA;LX/GEY;LX/GJi;LX/GMR;LX/GMS;LX/GMK;LX/GLH;LX/GEb;LX/GKn;LX/GLd;LX/GKQ;LX/0ad;)V

    .line 2335033
    return-object v0
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2335034
    iget-object v0, p0, LX/GHV;->k:LX/0Px;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;LX/GCY;)V
    .locals 6

    .prologue
    .line 2335035
    const-string v0, "page_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2335036
    invoke-static {p1}, LX/8wJ;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v3

    .line 2335037
    iget-object v0, p0, LX/GHV;->m:LX/0ad;

    sget-short v2, LX/GDK;->z:S

    const/4 v4, 0x0

    invoke-interface {v0, v2, v4}, LX/0ad;->a(SZ)Z

    move-result v0

    move v5, v0

    .line 2335038
    iget-object v0, p0, LX/GHV;->l:LX/GE3;

    sget-object v2, LX/8wL;->PROMOTE_CTA:LX/8wL;

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/adinterfaces/api/FetchBoostedComponentDataMethod;->a(Ljava/lang/String;LX/8wL;Ljava/lang/String;LX/GCY;Z)V

    .line 2335039
    return-void
.end method
