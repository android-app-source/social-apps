.class public final LX/G1o;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 2312972
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_8

    .line 2312973
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2312974
    :goto_0
    return v1

    .line 2312975
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2312976
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v7

    sget-object v8, LX/15z;->END_OBJECT:LX/15z;

    if-eq v7, v8, :cond_7

    .line 2312977
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v7

    .line 2312978
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2312979
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v8

    sget-object v9, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v8, v9, :cond_1

    if-eqz v7, :cond_1

    .line 2312980
    const-string v8, "action_link"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 2312981
    invoke-static {p0, p1}, LX/G1n;->a(LX/15w;LX/186;)I

    move-result v6

    goto :goto_1

    .line 2312982
    :cond_2
    const-string v8, "footer"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2312983
    invoke-static {p0, p1}, LX/G1p;->a(LX/15w;LX/186;)I

    move-result v5

    goto :goto_1

    .line 2312984
    :cond_3
    const-string v8, "profile_tile_section_type"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2312985
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v4

    invoke-virtual {p1, v4}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v4

    goto :goto_1

    .line 2312986
    :cond_4
    const-string v8, "profile_tile_views"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 2312987
    invoke-static {p0, p1}, LX/G1v;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2312988
    :cond_5
    const-string v8, "subtitle"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 2312989
    invoke-static {p0, p1}, LX/G1q;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2312990
    :cond_6
    const-string v8, "title"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2312991
    invoke-static {p0, p1}, LX/G1r;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2312992
    :cond_7
    const/4 v7, 0x6

    invoke-virtual {p1, v7}, LX/186;->c(I)V

    .line 2312993
    invoke-virtual {p1, v1, v6}, LX/186;->b(II)V

    .line 2312994
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v5}, LX/186;->b(II)V

    .line 2312995
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v4}, LX/186;->b(II)V

    .line 2312996
    const/4 v1, 0x3

    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2312997
    const/4 v1, 0x4

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2312998
    const/4 v1, 0x5

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2312999
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto/16 :goto_0

    :cond_8
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    goto/16 :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 2313000
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2313001
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2313002
    if-eqz v0, :cond_0

    .line 2313003
    const-string v1, "action_link"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2313004
    invoke-static {p0, v0, p2, p3}, LX/G1n;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2313005
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2313006
    if-eqz v0, :cond_1

    .line 2313007
    const-string v1, "footer"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2313008
    invoke-static {p0, v0, p2}, LX/G1p;->a(LX/15i;ILX/0nX;)V

    .line 2313009
    :cond_1
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2313010
    if-eqz v0, :cond_2

    .line 2313011
    const-string v0, "profile_tile_section_type"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2313012
    invoke-virtual {p0, p1, v2}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2313013
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2313014
    if-eqz v0, :cond_3

    .line 2313015
    const-string v1, "profile_tile_views"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2313016
    invoke-static {p0, v0, p2, p3}, LX/G1v;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2313017
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2313018
    if-eqz v0, :cond_4

    .line 2313019
    const-string v1, "subtitle"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2313020
    invoke-static {p0, v0, p2}, LX/G1q;->a(LX/15i;ILX/0nX;)V

    .line 2313021
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2313022
    if-eqz v0, :cond_5

    .line 2313023
    const-string v1, "title"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2313024
    invoke-static {p0, v0, p2}, LX/G1r;->a(LX/15i;ILX/0nX;)V

    .line 2313025
    :cond_5
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2313026
    return-void
.end method
