.class public LX/Gfg;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:Lcom/facebook/intent/feed/IFeedIntentBuilder;


# direct methods
.method public constructor <init>(Lcom/facebook/intent/feed/IFeedIntentBuilder;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2377732
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2377733
    iput-object p1, p0, LX/Gfg;->a:Lcom/facebook/intent/feed/IFeedIntentBuilder;

    .line 2377734
    return-void
.end method

.method public static a(LX/0QB;)LX/Gfg;
    .locals 4

    .prologue
    .line 2377735
    const-class v1, LX/Gfg;

    monitor-enter v1

    .line 2377736
    :try_start_0
    sget-object v0, LX/Gfg;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2377737
    sput-object v2, LX/Gfg;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2377738
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2377739
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2377740
    new-instance p0, LX/Gfg;

    invoke-static {v0}, LX/1nB;->a(LX/0QB;)LX/1nB;

    move-result-object v3

    check-cast v3, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    invoke-direct {p0, v3}, LX/Gfg;-><init>(Lcom/facebook/intent/feed/IFeedIntentBuilder;)V

    .line 2377741
    move-object v0, p0

    .line 2377742
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2377743
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gfg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2377744
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2377745
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
