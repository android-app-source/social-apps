.class public final LX/FdX;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;)V
    .locals 0

    .prologue
    .line 2263655
    iput-object p1, p0, LX/FdX;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x26a524e7

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2263656
    iget-object v1, p0, LX/FdX;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    iget-object v1, v1, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->q:LX/FdZ;

    if-eqz v1, :cond_3

    .line 2263657
    iget-object v1, p0, LX/FdX;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    iget-object v1, v1, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->q:LX/FdZ;

    iget-object v2, p0, LX/FdX;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    iget-object v2, v2, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;->s:LX/FdR;

    .line 2263658
    new-instance v7, LX/0Pz;

    invoke-direct {v7}, LX/0Pz;-><init>()V

    .line 2263659
    iget-object v4, v2, LX/FdR;->c:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v8

    const/4 v4, 0x0

    move v6, v4

    :goto_0
    if-ge v6, v8, :cond_2

    iget-object v4, v2, LX/FdR;->c:LX/0Px;

    invoke-virtual {v4, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/5uu;

    .line 2263660
    iget-object v5, v2, LX/FdR;->d:Ljava/util/HashMap;

    invoke-interface {v4}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/CyH;

    .line 2263661
    if-nez v5, :cond_0

    .line 2263662
    iget-object v5, v2, LX/FdR;->b:LX/FcV;

    invoke-interface {v4}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, LX/FcV;->b(Ljava/lang/String;)LX/FcI;

    move-result-object v5

    .line 2263663
    invoke-interface {v4}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v4}, LX/5uu;->e()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    move-result-object p1

    invoke-static {v2, v5, v9, p1}, LX/FdR;->a(LX/FdR;LX/FcI;Ljava/lang/String;Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;)V

    .line 2263664
    iget-object v5, v2, LX/FdR;->d:Ljava/util/HashMap;

    invoke-interface {v4}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/CyH;

    .line 2263665
    :cond_0
    if-eqz v5, :cond_1

    .line 2263666
    iget-object v9, v5, LX/CyH;->c:LX/4FP;

    move-object v9, v9

    .line 2263667
    invoke-virtual {v9}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v9

    const-string p1, "value"

    invoke-interface {v9, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    if-eqz v9, :cond_1

    .line 2263668
    iget-object v9, v5, LX/CyH;->c:LX/4FP;

    move-object v5, v9

    .line 2263669
    invoke-virtual {v5}, LX/0gS;->b()Ljava/util/Map;

    move-result-object v5

    const-string v9, "value"

    invoke-interface {v5, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    const-string v9, "default"

    invoke-virtual {v5, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2263670
    iget-object v5, v2, LX/FdR;->d:Ljava/util/HashMap;

    invoke-interface {v4}, LX/5uu;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v7, v4}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2263671
    :cond_1
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto :goto_0

    .line 2263672
    :cond_2
    invoke-virtual {v7}, LX/0Pz;->b()LX/0Px;

    move-result-object v4

    move-object v2, v4

    .line 2263673
    invoke-interface {v1, v2}, LX/FdZ;->a(LX/0Px;)V

    .line 2263674
    :cond_3
    iget-object v1, p0, LX/FdX;->a:Lcom/facebook/search/results/filters/ui/SearchResultPageFilterFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/DialogFragment;->c()V

    .line 2263675
    const v1, -0xfe11b6b

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
