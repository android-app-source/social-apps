.class public final LX/FZD;
.super LX/4or;
.source ""


# instance fields
.field public final synthetic a:LX/0Tn;

.field public final synthetic b:Lcom/facebook/search/debug/SearchDebugActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/search/debug/SearchDebugActivity;Landroid/content/Context;LX/0Tn;)V
    .locals 0

    .prologue
    .line 2256980
    iput-object p1, p0, LX/FZD;->b:Lcom/facebook/search/debug/SearchDebugActivity;

    iput-object p3, p0, LX/FZD;->a:LX/0Tn;

    invoke-direct {p0, p2}, LX/4or;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final getPersistedString(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2256981
    iget-object v0, p0, LX/FZD;->b:Lcom/facebook/search/debug/SearchDebugActivity;

    iget-object v0, v0, Lcom/facebook/search/debug/SearchDebugActivity;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/FZD;->a:LX/0Tn;

    invoke-interface {v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2256982
    iget-object v0, p0, LX/FZD;->b:Lcom/facebook/search/debug/SearchDebugActivity;

    iget-object v0, v0, Lcom/facebook/search/debug/SearchDebugActivity;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    iget-object v1, p0, LX/FZD;->a:LX/0Tn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "post search"

    .line 2256983
    :goto_0
    return-object v0

    .line 2256984
    :cond_0
    const-string v0, "simple search"

    goto :goto_0

    .line 2256985
    :cond_1
    const-string v0, "unset"

    goto :goto_0
.end method

.method public final persistString(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 2256986
    iget-object v0, p0, LX/FZD;->b:Lcom/facebook/search/debug/SearchDebugActivity;

    iget-object v0, v0, Lcom/facebook/search/debug/SearchDebugActivity;->f:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v3

    .line 2256987
    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 2256988
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown selection: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2256989
    :sswitch_0
    const-string v4, "unset"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_1
    const-string v4, "simple search"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v2

    goto :goto_0

    :sswitch_2
    const-string v4, "post search"

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 2256990
    :pswitch_0
    iget-object v0, p0, LX/FZD;->a:LX/0Tn;

    invoke-interface {v3, v0}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    .line 2256991
    :goto_1
    invoke-interface {v3}, LX/0hN;->commit()V

    .line 2256992
    return v2

    .line 2256993
    :pswitch_1
    iget-object v0, p0, LX/FZD;->a:LX/0Tn;

    invoke-interface {v3, v0, v1}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    goto :goto_1

    .line 2256994
    :pswitch_2
    iget-object v0, p0, LX/FZD;->a:LX/0Tn;

    invoke-interface {v3, v0, v2}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7e4e502a -> :sswitch_1
        0x6a47b29 -> :sswitch_0
        0x236e4128 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final setSummary(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 2256995
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    invoke-super {p0, p1}, LX/4or;->setSummary(Ljava/lang/CharSequence;)V

    .line 2256996
    return-void

    .line 2256997
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Current Override: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "unset"

    invoke-virtual {p0, v1}, LX/FZD;->getPersistedString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method
