.class public final LX/Fyk;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;)V
    .locals 0

    .prologue
    .line 2307136
    iput-object p1, p0, LX/Fyk;->a:Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, -0x16cbf4e0

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2307137
    iget-object v1, p0, LX/Fyk;->a:Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;

    iget-object v1, v1, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;->l:LX/Fyh;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Fyk;->a:Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;

    iget-object v1, v1, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;->m:Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Fyk;->a:Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;

    iget-object v1, v1, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;->m:Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    invoke-virtual {v1}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;->c()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2307138
    iget-object v1, p0, LX/Fyk;->a:Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;

    iget-object v1, v1, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;->l:LX/Fyh;

    iget-object v2, p0, LX/Fyk;->a:Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;

    invoke-virtual {v2}, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/Fyk;->a:Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;

    iget-object v3, v3, Lcom/facebook/timeline/inforeview/InfoReviewOverflowLinkView;->m:Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;

    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/TimelineInfoReviewGraphQLModels$InfoReviewOverflowLinkModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/Fyh;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 2307139
    :cond_0
    const v1, -0x6618c2f4

    invoke-static {v4, v4, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
