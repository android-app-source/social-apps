.class public LX/Gn0;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0pQ;


# static fields
.field public static final a:LX/0Tn;

.field public static final b:LX/0Tn;

.field public static final c:LX/0Tn;

.field public static final d:LX/0Tn;

.field public static final e:LX/0Tn;

.field public static final f:LX/0Tn;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2393288
    sget-object v0, LX/0Tm;->a:LX/0Tn;

    const-string v1, "instant_shopping/"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    .line 2393289
    sput-object v0, LX/Gn0;->a:LX/0Tn;

    const-string v1, "has_seen_checkout_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Gn0;->b:LX/0Tn;

    .line 2393290
    sget-object v0, LX/Gn0;->a:LX/0Tn;

    const-string v1, "save_nux_seen_count"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Gn0;->c:LX/0Tn;

    .line 2393291
    sget-object v0, LX/Gn0;->a:LX/0Tn;

    const-string v1, "has_seen_save_modal"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Gn0;->d:LX/0Tn;

    .line 2393292
    sget-object v0, LX/Gn0;->a:LX/0Tn;

    const-string v1, "has_seen_audio_nux"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Gn0;->e:LX/0Tn;

    .line 2393293
    sget-object v0, LX/Gn0;->a:LX/0Tn;

    const-string v1, "audio_is_state"

    invoke-virtual {v0, v1}, LX/0To;->a(Ljava/lang/String;)LX/0To;

    move-result-object v0

    check-cast v0, LX/0Tn;

    sput-object v0, LX/Gn0;->f:LX/0Tn;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2393294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2393295
    return-void
.end method


# virtual methods
.method public final a()LX/0Rf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Rf",
            "<",
            "LX/0Tn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2393296
    sget-object v0, LX/Gn0;->b:LX/0Tn;

    sget-object v1, LX/Gn0;->c:LX/0Tn;

    invoke-static {v0, v1}, LX/0Rf;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    return-object v0
.end method
