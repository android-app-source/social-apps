.class public LX/GVe;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/GVe;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2359691
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2359692
    iput-object p1, p0, LX/GVe;->a:LX/0Zb;

    .line 2359693
    return-void
.end method

.method public static a(LX/0QB;)LX/GVe;
    .locals 4

    .prologue
    .line 2359694
    sget-object v0, LX/GVe;->b:LX/GVe;

    if-nez v0, :cond_1

    .line 2359695
    const-class v1, LX/GVe;

    monitor-enter v1

    .line 2359696
    :try_start_0
    sget-object v0, LX/GVe;->b:LX/GVe;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2359697
    if-eqz v2, :cond_0

    .line 2359698
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2359699
    new-instance p0, LX/GVe;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/GVe;-><init>(LX/0Zb;)V

    .line 2359700
    move-object v0, p0

    .line 2359701
    sput-object v0, LX/GVe;->b:LX/GVe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2359702
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2359703
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2359704
    :cond_1
    sget-object v0, LX/GVe;->b:LX/GVe;

    return-object v0

    .line 2359705
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2359706
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Boolean;)V
    .locals 3

    .prologue
    .line 2359707
    iget-object v0, p0, LX/GVe;->a:LX/0Zb;

    new-instance v1, Lcom/facebook/analytics/logger/HoneyClientEvent;

    sget-object v2, LX/GVd;->CAPTCHA_SOLVE_REQUEST_SUCCEEDED:LX/GVd;

    invoke-virtual {v2}, LX/GVd;->getEventName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string v2, "captcha"

    .line 2359708
    iput-object v2, v1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2359709
    move-object v1, v1

    .line 2359710
    const-string v2, "captcha_solved"

    invoke-virtual {v1, v2, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->b(Lcom/facebook/analytics/HoneyAnalyticsEvent;I)V

    .line 2359711
    return-void
.end method
