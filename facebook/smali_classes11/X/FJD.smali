.class public LX/FJD;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2223259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;Landroid/content/res/Resources;LX/0Uh;)LX/6gC;
    .locals 7
    .param p0    # Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 2223260
    if-nez p0, :cond_0

    move-object v0, v1

    .line 2223261
    :goto_0
    return-object v0

    .line 2223262
    :cond_0
    invoke-static {}, LX/6gD;->newBuilder()LX/6gD;

    move-result-object v2

    .line 2223263
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->c()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v0

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    .line 2223264
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->n()Ljava/lang/String;

    move-result-object v3

    .line 2223265
    iput-object v3, v2, LX/6gD;->a:Ljava/lang/String;

    .line 2223266
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->d()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonStoryAttachmentFieldsModel$DescriptionModel;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 2223267
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->d()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonStoryAttachmentFieldsModel$DescriptionModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonStoryAttachmentFieldsModel$DescriptionModel;->a()Ljava/lang/String;

    move-result-object v3

    .line 2223268
    iput-object v3, v2, LX/6gD;->b:Ljava/lang/String;

    .line 2223269
    :cond_1
    invoke-static {v0}, LX/FJD;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/6gD;->a(Ljava/util/List;)LX/6gD;

    .line 2223270
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 2223271
    iput-object v3, v2, LX/6gD;->d:Ljava/lang/String;

    .line 2223272
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v3

    .line 2223273
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    if-nez v4, :cond_3

    .line 2223274
    :cond_2
    invoke-virtual {v2}, LX/6gD;->i()LX/6gC;

    move-result-object v0

    goto :goto_0

    .line 2223275
    :cond_3
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    .line 2223276
    const v5, -0x2b718aa2

    if-eq v4, v5, :cond_4

    const v5, 0x1a2bf082

    if-eq v4, v5, :cond_4

    .line 2223277
    invoke-virtual {v2}, LX/6gD;->i()LX/6gC;

    move-result-object v0

    goto :goto_0

    .line 2223278
    :cond_4
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->m()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cv()Ljava/lang/String;

    move-result-object v4

    .line 2223279
    iput-object v4, v2, LX/6gD;->e:Ljava/lang/String;

    .line 2223280
    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->av()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cc()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_6

    :cond_5
    const/16 v4, 0x184

    invoke-virtual {p2, v4, v6}, LX/0Uh;->a(IZ)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2223281
    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->av()LX/0Px;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/6gD;->a(LX/0Px;)LX/6gD;

    .line 2223282
    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cc()LX/0Px;

    move-result-object v4

    invoke-virtual {v2, v4}, LX/6gD;->b(LX/0Px;)LX/6gD;

    .line 2223283
    :cond_6
    iget-object v4, v2, LX/6gD;->g:LX/0Px;

    move-object v4, v4

    .line 2223284
    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 2223285
    iget-object v4, v2, LX/6gD;->h:LX/0Px;

    move-object v4, v4

    .line 2223286
    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_7

    .line 2223287
    new-instance v4, LX/5XX;

    invoke-direct {v4}, LX/5XX;-><init>()V

    .line 2223288
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLMomentsAppMessengerInviteActionType;->INSTALL:Lcom/facebook/graphql/enums/GraphQLMomentsAppMessengerInviteActionType;

    .line 2223289
    iput-object v5, v4, LX/5XX;->a:Lcom/facebook/graphql/enums/GraphQLMomentsAppMessengerInviteActionType;

    .line 2223290
    const-string v5, "getmoments"

    .line 2223291
    iput-object v5, v4, LX/5XX;->b:Ljava/lang/String;

    .line 2223292
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v6}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$ActionLinksModel;

    .line 2223293
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$ActionLinksModel;->b()Ljava/lang/String;

    move-result-object v5

    .line 2223294
    iput-object v5, v4, LX/5XX;->e:Ljava/lang/String;

    .line 2223295
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$ActionLinksModel;->c()Ljava/lang/String;

    move-result-object v0

    .line 2223296
    iput-object v0, v4, LX/5XX;->g:Ljava/lang/String;

    .line 2223297
    const v0, 0x7f0832c7

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cN()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "moments://"

    :goto_1
    invoke-static {v5, v1, v0}, LX/FJD;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;

    move-result-object v0

    .line 2223298
    invoke-virtual {v4}, LX/5XX;->a()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;

    move-result-object v1

    invoke-static {v1, v0}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    .line 2223299
    invoke-virtual {v2, v0}, LX/6gD;->a(LX/0Px;)LX/6gD;

    .line 2223300
    invoke-virtual {v2, v0}, LX/6gD;->b(LX/0Px;)LX/6gD;

    .line 2223301
    :cond_7
    invoke-virtual {v2}, LX/6gD;->i()LX/6gC;

    move-result-object v0

    goto/16 :goto_0

    .line 2223302
    :cond_8
    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cN()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;
    .locals 2
    .param p1    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2223303
    new-instance v0, LX/5XX;

    invoke-direct {v0}, LX/5XX;-><init>()V

    .line 2223304
    sget-object v1, Lcom/facebook/graphql/enums/GraphQLMomentsAppMessengerInviteActionType;->INTENT_POSTBACK:Lcom/facebook/graphql/enums/GraphQLMomentsAppMessengerInviteActionType;

    .line 2223305
    iput-object v1, v0, LX/5XX;->a:Lcom/facebook/graphql/enums/GraphQLMomentsAppMessengerInviteActionType;

    .line 2223306
    const-string v1, "viewinmoments"

    .line 2223307
    iput-object v1, v0, LX/5XX;->b:Ljava/lang/String;

    .line 2223308
    iput-object p0, v0, LX/5XX;->e:Ljava/lang/String;

    .line 2223309
    iput-object p1, v0, LX/5XX;->g:Ljava/lang/String;

    .line 2223310
    iput-object p2, v0, LX/5XX;->f:Ljava/lang/String;

    .line 2223311
    invoke-virtual {v0}, LX/5XX;->a()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MomentsAppInvitationActionLinkFragmentModel;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/messaging/graphql/threads/ThreadQueriesInterfaces$XMAAttachmentStoryFields;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2223312
    invoke-static {}, LX/0R9;->a()Ljava/util/ArrayList;

    move-result-object v2

    .line 2223313
    invoke-virtual {p0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->k()LX/0Px;

    move-result-object v3

    .line 2223314
    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    invoke-virtual {v3, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;

    .line 2223315
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;->b()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel$MediaModel;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;->b()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel$MediaModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel$MediaModel;->b()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel$MediaModel$ImageModel;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 2223316
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel;->b()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel$MediaModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel$MediaModel;->b()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel$MediaModel$ImageModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel$SubattachmentsModel$MediaModel$ImageModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2223317
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2223318
    :cond_1
    return-object v2
.end method
