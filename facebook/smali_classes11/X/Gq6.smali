.class public LX/Gq6;
.super LX/Cod;
.source ""

# interfaces
.implements LX/Gpt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/GpV;",
        ">;",
        "LX/Gpt;"
    }
.end annotation


# instance fields
.field public a:LX/Go0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final b:Landroid/widget/LinearLayout;

.field public c:Landroid/view/View$OnClickListener;

.field public d:LX/GoE;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 2396190
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 2396191
    const-class v0, LX/Gq6;

    invoke-static {v0, p0}, LX/Gq6;->a(Ljava/lang/Class;LX/02k;)V

    .line 2396192
    const v0, 0x7f0d1819

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, LX/Gq6;->b:Landroid/widget/LinearLayout;

    .line 2396193
    new-instance v0, LX/Gq5;

    invoke-direct {v0, p0}, LX/Gq5;-><init>(LX/Gq6;)V

    iput-object v0, p0, LX/Gq6;->c:Landroid/view/View$OnClickListener;

    .line 2396194
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Gq6;

    invoke-static {p0}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object p0

    check-cast p0, LX/Go0;

    iput-object p0, p1, LX/Gq6;->a:LX/Go0;

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;ZLX/GoE;)V
    .locals 2

    .prologue
    .line 2396195
    iput-object p3, p0, LX/Gq6;->d:LX/GoE;

    .line 2396196
    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->b()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2396197
    invoke-virtual {p1}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;->b()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    .line 2396198
    invoke-virtual {p0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2396199
    const p1, 0x7f030964

    iget-object p2, p0, LX/Gq6;->b:Landroid/widget/LinearLayout;

    const/4 p3, 0x0

    invoke-virtual {v1, p1, p2, p3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/facebook/instantshopping/view/widget/ColorPickerPileImageItemLayout;

    .line 2396200
    iget-object p1, p0, LX/Gq6;->b:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 2396201
    invoke-virtual {v1}, Lcom/facebook/instantshopping/view/widget/ColorPickerPileImageItemLayout;->a()V

    .line 2396202
    invoke-virtual {v1, v0}, Lcom/facebook/instantshopping/view/widget/ColorPickerPileImageItemLayout;->setImageUrl(Ljava/lang/String;)V

    .line 2396203
    iget-object p1, p0, LX/Gq6;->c:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, p1}, Lcom/facebook/instantshopping/view/widget/ColorPickerPileImageItemLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2396204
    :cond_0
    return-void
.end method
