.class public final LX/GNg;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;)V
    .locals 0

    .prologue
    .line 2346363
    iput-object p1, p0, LX/GNg;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2346364
    iget-object v0, p0, LX/GNg;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    const v1, 0x7f08006f

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 2346365
    iget-object v1, p0, LX/GNg;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v1, v1, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->p:LX/62O;

    iget-object v2, p0, LX/GNg;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v2, v2, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->r:LX/1DI;

    invoke-virtual {v1, v0, v2}, LX/62O;->a(Ljava/lang/String;LX/1DI;)V

    .line 2346366
    iget-object v0, p0, LX/GNg;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->e:LX/03V;

    sget-object v1, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->k:Ljava/lang/String;

    const-string v2, "Failed to fetch ad preview."

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2346367
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2346368
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2346369
    if-eqz p1, :cond_0

    .line 2346370
    iget-object v0, p0, LX/GNg;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v1, v0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->i:LX/GGW;

    iget-object v0, p0, LX/GNg;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v2, v0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->n:Landroid/widget/LinearLayout;

    .line 2346371
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2346372
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v1, v2, v0}, LX/GGW;->a(Landroid/view/ViewGroup;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2346373
    :cond_0
    iget-object v0, p0, LX/GNg;->a:Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;

    iget-object v0, v0, Lcom/facebook/adsexperiencetool/AdsInjectConfirmationFragment;->p:LX/62O;

    invoke-virtual {v0}, LX/62O;->b()V

    .line 2346374
    return-void
.end method
