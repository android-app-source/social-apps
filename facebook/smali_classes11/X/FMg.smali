.class public LX/FMg;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0YZ;


# static fields
.field private static final g:Ljava/lang/Object;

.field private static h:LX/1ql;

.field private static final i:J


# instance fields
.field public a:Lcom/facebook/content/SecureContextHelper;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/2Oq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1r1;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/FMl;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/FMw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/2Uq;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2230208
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/FMg;->g:Ljava/lang/Object;

    .line 2230209
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5a

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, LX/FMg;->i:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2230210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/app/Service;I)V
    .locals 2

    .prologue
    .line 2230211
    sget-object v1, LX/FMg;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 2230212
    :try_start_0
    sget-object v0, LX/FMg;->h:LX/1ql;

    if-eqz v0, :cond_0

    .line 2230213
    invoke-virtual {p0, p1}, Landroid/app/Service;->stopSelfResult(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2230214
    sget-object v0, LX/FMg;->h:LX/1ql;

    invoke-virtual {v0}, LX/1ql;->d()V

    .line 2230215
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 2230216
    sget-object v1, LX/FMg;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 2230217
    :try_start_0
    sget-object v0, LX/FMg;->h:LX/1ql;

    if-nez v0, :cond_0

    .line 2230218
    iget-object v0, p0, LX/FMg;->c:LX/1r1;

    const/4 v2, 0x1

    const-string v3, "startSmsHandling"

    invoke-virtual {v0, v2, v3}, LX/1r1;->a(ILjava/lang/String;)LX/1ql;

    move-result-object v0

    .line 2230219
    sput-object v0, LX/FMg;->h:LX/1ql;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, LX/1ql;->a(Z)V

    .line 2230220
    :cond_0
    sget-object v0, LX/FMg;->h:LX/1ql;

    sget-wide v2, LX/FMg;->i:J

    invoke-virtual {v0, v2, v3}, LX/1ql;->a(J)V

    .line 2230221
    iget-object v0, p0, LX/FMg;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v0, p2, p1}, Lcom/facebook/content/SecureContextHelper;->c(Landroid/content/Intent;Landroid/content/Context;)Landroid/content/ComponentName;

    .line 2230222
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;LX/0Yf;)V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/4 v0, 0x2

    const/16 v1, 0x26

    const v2, 0x10a4f839

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2230223
    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    move-object v2, p0

    check-cast v2, LX/FMg;

    invoke-static {v1}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-static {v1}, LX/2Oq;->a(LX/0QB;)LX/2Oq;

    move-result-object v4

    check-cast v4, LX/2Oq;

    invoke-static {v1}, LX/1r1;->a(LX/0QB;)LX/1r1;

    move-result-object v5

    check-cast v5, LX/1r1;

    invoke-static {v1}, LX/FMl;->a(LX/0QB;)LX/FMl;

    move-result-object v6

    check-cast v6, LX/FMl;

    invoke-static {v1}, LX/FMw;->a(LX/0QB;)LX/FMw;

    move-result-object v7

    check-cast v7, LX/FMw;

    invoke-static {v1}, LX/2Uq;->a(LX/0QB;)LX/2Uq;

    move-result-object v1

    check-cast v1, LX/2Uq;

    iput-object v3, v2, LX/FMg;->a:Lcom/facebook/content/SecureContextHelper;

    iput-object v4, v2, LX/FMg;->b:LX/2Oq;

    iput-object v5, v2, LX/FMg;->c:LX/1r1;

    iput-object v6, v2, LX/FMg;->d:LX/FMl;

    iput-object v7, v2, LX/FMg;->e:LX/FMw;

    iput-object v1, v2, LX/FMg;->f:LX/2Uq;

    .line 2230224
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2230225
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    const/4 v3, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v3, :pswitch_data_0

    move v1, v2

    .line 2230226
    :pswitch_0
    move v1, v1

    .line 2230227
    if-eqz v1, :cond_1

    iget-object v1, p0, LX/FMg;->b:LX/2Oq;

    invoke-virtual {v1}, LX/2Oq;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2230228
    :cond_1
    iget-object v1, p0, LX/FMg;->d:LX/FMl;

    invoke-virtual {v1}, LX/FMl;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2230229
    const-class v1, LX/FMk;

    invoke-virtual {p2, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 2230230
    const-string v1, "result_code"

    invoke-interface {p3}, LX/0Yf;->getResultCode()I

    move-result v2

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 2230231
    invoke-direct {p0, p1, p2}, LX/FMg;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 2230232
    :goto_1
    const v1, 0x1ffcaee5

    invoke-static {v1, v0}, LX/02F;->e(II)V

    return-void

    .line 2230233
    :cond_2
    iget-object v1, p0, LX/FMg;->d:LX/FMl;

    .line 2230234
    iget-object v2, v1, LX/FMl;->f:LX/2Oq;

    invoke-virtual {v2}, LX/2Oq;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, LX/FMl;->b()Z

    move-result v2

    if-nez v2, :cond_4

    .line 2230235
    iget-object v2, v1, LX/FMl;->f:LX/2Oq;

    .line 2230236
    invoke-static {v2}, LX/2Oq;->h(LX/2Oq;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2230237
    iget-object v3, v2, LX/2Oq;->i:LX/3Fd;

    .line 2230238
    iget-object v4, v3, LX/3Fd;->a:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v4}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v4

    sget-object v5, LX/3Kr;->ab:LX/0Tn;

    const-string v2, ""

    invoke-interface {v4, v5, v2}, LX/0hN;->a(LX/0Tn;Ljava/lang/String;)LX/0hN;

    move-result-object v4

    invoke-interface {v4}, LX/0hN;->commit()V

    .line 2230239
    :cond_3
    iget-object v2, v1, LX/FMl;->d:LX/2uq;

    sget-object v3, LX/FMK;->PERMISSION_CHANGE:LX/FMK;

    iget-object v4, v1, LX/FMl;->b:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, LX/2uq;->a(Ljava/lang/Object;Landroid/content/Context;Z)Z

    .line 2230240
    iget-object v2, v1, LX/FMl;->e:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {v2}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object v2

    sget-object v3, LX/3Kr;->c:LX/0Tn;

    invoke-interface {v2, v3}, LX/0hN;->a(LX/0Tn;)LX/0hN;

    move-result-object v2

    invoke-interface {v2}, LX/0hN;->commit()V

    .line 2230241
    :cond_4
    goto :goto_1

    .line 2230242
    :cond_5
    iget-object v1, p0, LX/FMg;->e:LX/FMw;

    invoke-virtual {v1}, LX/FMw;->a()V

    goto :goto_1

    .line 2230243
    :sswitch_0
    const-string v5, "com.facebook.messaging.sms.COMPOSE_SMS"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v3, v1

    goto/16 :goto_0

    :sswitch_1
    const-string v5, "com.facebook.messaging.sms.MESSAGE_SENT"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v3, v2

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x7ba264b6 -> :sswitch_1
        -0x6d951cae -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
