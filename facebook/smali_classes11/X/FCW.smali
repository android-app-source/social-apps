.class public final enum LX/FCW;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FCW;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FCW;

.field public static final enum FAILURE_PERMANENT:LX/FCW;

.field public static final enum FAILURE_RETRYABLE:LX/FCW;

.field public static final enum SUCCESS_GRAPH:LX/FCW;

.field public static final enum SUCCESS_MQTT:LX/FCW;

.field public static final enum UNKNOWN:LX/FCW;


# instance fields
.field public final rawValue:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2210375
    new-instance v0, LX/FCW;

    const-string v1, "UNKNOWN"

    const-string v2, "u"

    invoke-direct {v0, v1, v3, v2}, LX/FCW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FCW;->UNKNOWN:LX/FCW;

    .line 2210376
    new-instance v0, LX/FCW;

    const-string v1, "SUCCESS_MQTT"

    const-string v2, "m"

    invoke-direct {v0, v1, v4, v2}, LX/FCW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FCW;->SUCCESS_MQTT:LX/FCW;

    .line 2210377
    new-instance v0, LX/FCW;

    const-string v1, "SUCCESS_GRAPH"

    const-string v2, "g"

    invoke-direct {v0, v1, v5, v2}, LX/FCW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FCW;->SUCCESS_GRAPH:LX/FCW;

    .line 2210378
    new-instance v0, LX/FCW;

    const-string v1, "FAILURE_RETRYABLE"

    const-string v2, "f"

    invoke-direct {v0, v1, v6, v2}, LX/FCW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FCW;->FAILURE_RETRYABLE:LX/FCW;

    .line 2210379
    new-instance v0, LX/FCW;

    const-string v1, "FAILURE_PERMANENT"

    const-string v2, "p"

    invoke-direct {v0, v1, v7, v2}, LX/FCW;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FCW;->FAILURE_PERMANENT:LX/FCW;

    .line 2210380
    const/4 v0, 0x5

    new-array v0, v0, [LX/FCW;

    sget-object v1, LX/FCW;->UNKNOWN:LX/FCW;

    aput-object v1, v0, v3

    sget-object v1, LX/FCW;->SUCCESS_MQTT:LX/FCW;

    aput-object v1, v0, v4

    sget-object v1, LX/FCW;->SUCCESS_GRAPH:LX/FCW;

    aput-object v1, v0, v5

    sget-object v1, LX/FCW;->FAILURE_RETRYABLE:LX/FCW;

    aput-object v1, v0, v6

    sget-object v1, LX/FCW;->FAILURE_PERMANENT:LX/FCW;

    aput-object v1, v0, v7

    sput-object v0, LX/FCW;->$VALUES:[LX/FCW;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2210381
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2210382
    iput-object p3, p0, LX/FCW;->rawValue:Ljava/lang/String;

    .line 2210383
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FCW;
    .locals 1

    .prologue
    .line 2210384
    const-class v0, LX/FCW;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FCW;

    return-object v0
.end method

.method public static values()[LX/FCW;
    .locals 1

    .prologue
    .line 2210385
    sget-object v0, LX/FCW;->$VALUES:[LX/FCW;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FCW;

    return-object v0
.end method
