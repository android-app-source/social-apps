.class public final enum LX/HB6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HB6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HB6;

.field public static final enum REQUEST:LX/HB6;

.field public static final enum SPINNER:LX/HB6;

.field public static final enum UNKNOWN:LX/HB6;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2436441
    new-instance v0, LX/HB6;

    const-string v1, "REQUEST"

    invoke-direct {v0, v1, v2}, LX/HB6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HB6;->REQUEST:LX/HB6;

    .line 2436442
    new-instance v0, LX/HB6;

    const-string v1, "SPINNER"

    invoke-direct {v0, v1, v3}, LX/HB6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HB6;->SPINNER:LX/HB6;

    .line 2436443
    new-instance v0, LX/HB6;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v4}, LX/HB6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HB6;->UNKNOWN:LX/HB6;

    .line 2436444
    const/4 v0, 0x3

    new-array v0, v0, [LX/HB6;

    sget-object v1, LX/HB6;->REQUEST:LX/HB6;

    aput-object v1, v0, v2

    sget-object v1, LX/HB6;->SPINNER:LX/HB6;

    aput-object v1, v0, v3

    sget-object v1, LX/HB6;->UNKNOWN:LX/HB6;

    aput-object v1, v0, v4

    sput-object v0, LX/HB6;->$VALUES:[LX/HB6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2436440
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HB6;
    .locals 1

    .prologue
    .line 2436439
    const-class v0, LX/HB6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HB6;

    return-object v0
.end method

.method public static values()[LX/HB6;
    .locals 1

    .prologue
    .line 2436438
    sget-object v0, LX/HB6;->$VALUES:[LX/HB6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HB6;

    return-object v0
.end method
