.class public LX/GEY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GEW",
        "<",
        "Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;",
        "Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/GHh;


# direct methods
.method public constructor <init>(LX/GHh;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2331875
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2331876
    iput-object p1, p0, LX/GEY;->a:LX/GHh;

    .line 2331877
    return-void
.end method

.method public static a(LX/0QB;)LX/GEY;
    .locals 1

    .prologue
    .line 2331874
    invoke-static {p0}, LX/GEY;->b(LX/0QB;)LX/GEY;

    move-result-object v0

    return-object v0
.end method

.method public static b(LX/0QB;)LX/GEY;
    .locals 2

    .prologue
    .line 2331868
    new-instance v1, LX/GEY;

    .line 2331869
    new-instance v0, LX/GHh;

    invoke-direct {v0}, LX/GHh;-><init>()V

    .line 2331870
    move-object v0, v0

    .line 2331871
    move-object v0, v0

    .line 2331872
    check-cast v0, LX/GHh;

    invoke-direct {v1, v0}, LX/GEY;-><init>(LX/GHh;)V

    .line 2331873
    return-object v1
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2331878
    const v0, 0x7f03005a

    return v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 3

    .prologue
    .line 2331863
    check-cast p1, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;

    const/4 v0, 0x0

    .line 2331864
    invoke-static {p1}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2331865
    :cond_0
    :goto_0
    return v0

    .line 2331866
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->a()LX/GGB;

    move-result-object v1

    .line 2331867
    sget-object v2, LX/GGB;->INACTIVE:LX/GGB;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/GGB;->ACTIVE:LX/GGB;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/GGB;->PAUSED:LX/GGB;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/GGB;->EXTENDABLE:LX/GGB;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/GGB;->FINISHED:LX/GGB;

    if-eq v1, v2, :cond_2

    sget-object v2, LX/GGB;->PENDING:LX/GGB;

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()LX/GHg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesViewController",
            "<",
            "Lcom/facebook/widget/text/textwithentitiesview/TextWithEntitiesView;",
            "Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2331862
    iget-object v0, p0, LX/GEY;->a:LX/GHh;

    return-object v0
.end method

.method public final c()LX/8wK;
    .locals 1

    .prologue
    .line 2331861
    sget-object v0, LX/8wK;->ACCOUNT_ERROR_CARD:LX/8wK;

    return-object v0
.end method
