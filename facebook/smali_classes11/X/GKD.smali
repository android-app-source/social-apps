.class public final LX/GKD;
.super LX/46Z;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/android/maps/model/LatLng;

.field public final synthetic b:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;


# direct methods
.method public constructor <init>(Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;Lcom/facebook/android/maps/model/LatLng;)V
    .locals 0

    .prologue
    .line 2340503
    iput-object p1, p0, LX/GKD;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    iput-object p2, p0, LX/GKD;->a:Lcom/facebook/android/maps/model/LatLng;

    invoke-direct {p0}, LX/46Z;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1    # Landroid/graphics/Bitmap;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2340504
    if-eqz p1, :cond_0

    .line 2340505
    iget-object v0, p0, LX/GKD;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->j:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;

    iget-object v1, p0, LX/GKD;->a:Lcom/facebook/android/maps/model/LatLng;

    invoke-virtual {v0, v1, p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewView;->a(Lcom/facebook/android/maps/model/LatLng;Landroid/graphics/Bitmap;)V

    .line 2340506
    :cond_0
    return-void
.end method

.method public final f(LX/1ca;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1ca",
            "<",
            "LX/1FJ",
            "<",
            "LX/1ln;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 2340507
    iget-object v0, p0, LX/GKD;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    iget-object v0, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->f:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to download page picture for page "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LX/GKD;->b:Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;

    iget-object v3, v3, Lcom/facebook/adinterfaces/ui/AdInterfacesMapPreviewViewController;->m:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2340508
    return-void
.end method
