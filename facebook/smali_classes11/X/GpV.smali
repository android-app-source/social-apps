.class public LX/GpV;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "LX/Gpt;",
        "Lcom/facebook/instantshopping/model/data/ColorPickerBlockData;",
        ">;"
    }
.end annotation


# instance fields
.field public d:Z


# direct methods
.method public constructor <init>(LX/Gpt;)V
    .locals 0

    .prologue
    .line 2395033
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2395034
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 7

    .prologue
    .line 2395035
    check-cast p1, LX/Gox;

    .line 2395036
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 2395037
    iget-boolean v0, p0, LX/GpV;->d:Z

    if-eqz v0, :cond_0

    .line 2395038
    :goto_0
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395039
    check-cast v0, LX/Gq4;

    invoke-interface {p1}, LX/GoY;->D()LX/GoE;

    move-result-object v1

    .line 2395040
    iget-object v2, v0, LX/Gq4;->b:LX/Go0;

    invoke-virtual {v2, v1}, LX/Go0;->a(LX/GoE;)V

    .line 2395041
    return-void

    :cond_0
    move v2, v3

    .line 2395042
    :goto_1
    invoke-virtual {p1}, LX/Gox;->b()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 2395043
    iget-object v0, p1, LX/Gox;->a:LX/CHe;

    invoke-interface {v0}, LX/CHd;->k()I

    move-result v0

    move v0, v0

    .line 2395044
    if-ne v2, v0, :cond_1

    move v4, v5

    .line 2395045
    :goto_2
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395046
    check-cast v0, LX/Gpt;

    invoke-virtual {p1}, LX/Gox;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;

    invoke-interface {p1}, LX/GoY;->D()LX/GoE;

    move-result-object v6

    invoke-interface {v0, v1, v4, v6}, LX/Gpt;->a(Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingColorSelectorColorFragmentModel;ZLX/GoE;)V

    .line 2395047
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move v4, v3

    .line 2395048
    goto :goto_2

    .line 2395049
    :cond_2
    iput-boolean v5, p0, LX/GpV;->d:Z

    goto :goto_0
.end method
