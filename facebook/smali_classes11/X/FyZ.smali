.class public final LX/FyZ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;)V
    .locals 0

    .prologue
    .line 2306890
    iput-object p1, p0, LX/FyZ;->a:Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v6, 0x2

    const/4 v0, 0x1

    const v1, 0x4b604139    # 1.4696761E7f

    invoke-static {v6, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2306891
    iget-object v0, p0, LX/FyZ;->a:Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;

    iget-object v0, v0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->h:LX/BPy;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FyZ;->a:Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;

    iget-object v0, v0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->i:LX/5SB;

    if-eqz v0, :cond_0

    .line 2306892
    iget-object v0, p0, LX/FyZ;->a:Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;

    iget-object v0, v0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->e:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2306893
    iget-object v0, p0, LX/FyZ;->a:Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;

    invoke-static {v0}, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->b(Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;)V

    .line 2306894
    iget-object v0, p0, LX/FyZ;->a:Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;

    iget-object v0, v0, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/BPp;

    .line 2306895
    new-instance v3, LX/BPg;

    iget-object v1, p0, LX/FyZ;->a:Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;

    iget-object v1, v1, Lcom/facebook/timeline/header/ui/PlutoniumFriendRequestView;->i:LX/5SB;

    .line 2306896
    iget-wide v7, v1, LX/5SB;->b:J

    move-wide v4, v7

    .line 2306897
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2na;

    invoke-direct {v3, v4, v5, v1}, LX/BPg;-><init>(JLX/2na;)V

    invoke-virtual {v0, v3}, LX/0b4;->a(LX/0b7;)V

    .line 2306898
    :cond_0
    const v0, 0x27eee399

    invoke-static {v6, v6, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
