.class public LX/Ge4;
.super LX/25J;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/25J",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile j:LX/Ge4;


# instance fields
.field private final a:I

.field private final b:I

.field public final c:LX/0tX;

.field public final d:Ljava/util/concurrent/Executor;

.field public final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final g:LX/1Ck;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1Ck",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/189;

.field public final i:LX/0bH;


# direct methods
.method public constructor <init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/1Ck;LX/189;LX/0bH;)V
    .locals 1
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2375021
    invoke-direct {p0}, LX/25J;-><init>()V

    .line 2375022
    const/4 v0, 0x2

    iput v0, p0, LX/Ge4;->a:I

    .line 2375023
    const/4 v0, 0x5

    iput v0, p0, LX/Ge4;->b:I

    .line 2375024
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Ge4;->e:Ljava/util/Set;

    .line 2375025
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/Ge4;->f:Ljava/util/Set;

    .line 2375026
    iput-object p1, p0, LX/Ge4;->c:LX/0tX;

    .line 2375027
    iput-object p2, p0, LX/Ge4;->d:Ljava/util/concurrent/Executor;

    .line 2375028
    iput-object p3, p0, LX/Ge4;->g:LX/1Ck;

    .line 2375029
    iput-object p4, p0, LX/Ge4;->h:LX/189;

    .line 2375030
    iput-object p5, p0, LX/Ge4;->i:LX/0bH;

    .line 2375031
    return-void
.end method

.method public static a(LX/0QB;)LX/Ge4;
    .locals 9

    .prologue
    .line 2375008
    sget-object v0, LX/Ge4;->j:LX/Ge4;

    if-nez v0, :cond_1

    .line 2375009
    const-class v1, LX/Ge4;

    monitor-enter v1

    .line 2375010
    :try_start_0
    sget-object v0, LX/Ge4;->j:LX/Ge4;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2375011
    if-eqz v2, :cond_0

    .line 2375012
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2375013
    new-instance v3, LX/Ge4;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v4

    check-cast v4, LX/0tX;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static {v0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v6

    check-cast v6, LX/1Ck;

    invoke-static {v0}, LX/189;->b(LX/0QB;)LX/189;

    move-result-object v7

    check-cast v7, LX/189;

    invoke-static {v0}, LX/0bH;->a(LX/0QB;)LX/0bH;

    move-result-object v8

    check-cast v8, LX/0bH;

    invoke-direct/range {v3 .. v8}, LX/Ge4;-><init>(LX/0tX;Ljava/util/concurrent/ExecutorService;LX/1Ck;LX/189;LX/0bH;)V

    .line 2375014
    move-object v0, v3

    .line 2375015
    sput-object v0, LX/Ge4;->j:LX/Ge4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2375016
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2375017
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2375018
    :cond_1
    sget-object v0, LX/Ge4;->j:LX/Ge4;

    return-object v0

    .line 2375019
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2375020
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;)Z
    .locals 2

    .prologue
    .line 2375007
    iget-object v0, p0, LX/Ge4;->f:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPageInfo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;I)Z
    .locals 2

    .prologue
    .line 2375032
    iget-object v0, p0, LX/Ge4;->e:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    add-int/lit8 v0, p2, 0x2

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->o()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)Z
    .locals 1

    .prologue
    .line 2375006
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    invoke-virtual {p0, p1}, LX/Ge4;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)Z
    .locals 1

    .prologue
    .line 2375005
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    invoke-virtual {p0, p1, p2}, LX/Ge4;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;I)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;)V
    .locals 4

    .prologue
    .line 2375002
    iget-object v0, p0, LX/Ge4;->e:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2375003
    iget-object v0, p0, LX/Ge4;->g:LX/1Ck;

    const/4 v1, 0x0

    new-instance v2, LX/Ge1;

    invoke-direct {v2, p0, p1}, LX/Ge1;-><init>(LX/Ge4;Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;)V

    new-instance v3, LX/Ge2;

    invoke-direct {v3, p0, p1}, LX/Ge2;-><init>(LX/Ge4;Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2375004
    return-void
.end method

.method public final bridge synthetic b(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;)V
    .locals 0

    .prologue
    .line 2375001
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    invoke-virtual {p0, p1}, LX/Ge4;->b(Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;)V

    return-void
.end method
