.class public LX/Gpj;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/instantshopping/view/block/InstantShoppingSwipeToOpenIndicatorBlockView;",
        "Lcom/facebook/instantshopping/model/data/InstantShoppingSwipeToOpenIndicatorBlockData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/GqY;)V
    .locals 0

    .prologue
    .line 2395652
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2395653
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 4

    .prologue
    .line 2395654
    check-cast p1, LX/GpH;

    .line 2395655
    iget-object v0, p1, LX/GpH;->a:Ljava/lang/String;

    move-object v1, v0

    .line 2395656
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395657
    check-cast v0, LX/GqY;

    .line 2395658
    iget-object v2, v0, LX/GqY;->a:Lcom/facebook/widget/CustomLinearLayout;

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result p1

    invoke-direct {v3, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v3}, Lcom/facebook/widget/CustomLinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 2395659
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395660
    check-cast v0, LX/GqY;

    .line 2395661
    const/4 v2, 0x0

    invoke-static {v2, v1}, LX/CIa;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/high16 v2, -0x1000000

    .line 2395662
    :goto_0
    iget-object v3, v0, LX/GqY;->b:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2395663
    iget-object v0, v3, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v3, v0

    .line 2395664
    invoke-virtual {v3, v2}, LX/CtG;->setTextColor(I)V

    .line 2395665
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395666
    check-cast v0, LX/GqY;

    .line 2395667
    const/4 v2, 0x0

    invoke-static {v2, v1}, LX/CIa;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x7f020dc1

    .line 2395668
    :goto_1
    iget-object v3, v0, LX/GqY;->c:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2395669
    return-void

    .line 2395670
    :cond_0
    const/4 v2, -0x1

    goto :goto_0

    .line 2395671
    :cond_1
    const v2, 0x7f020dc2

    goto :goto_1
.end method
