.class public final enum LX/FCY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FCY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FCY;

.field public static final enum GRAPH:LX/FCY;

.field public static final enum MQTT:LX/FCY;


# instance fields
.field public final channelName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2210401
    new-instance v0, LX/FCY;

    const-string v1, "MQTT"

    const-string v2, "mqtt"

    invoke-direct {v0, v1, v3, v2}, LX/FCY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FCY;->MQTT:LX/FCY;

    .line 2210402
    new-instance v0, LX/FCY;

    const-string v1, "GRAPH"

    const-string v2, "graph"

    invoke-direct {v0, v1, v4, v2}, LX/FCY;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FCY;->GRAPH:LX/FCY;

    .line 2210403
    const/4 v0, 0x2

    new-array v0, v0, [LX/FCY;

    sget-object v1, LX/FCY;->MQTT:LX/FCY;

    aput-object v1, v0, v3

    sget-object v1, LX/FCY;->GRAPH:LX/FCY;

    aput-object v1, v0, v4

    sput-object v0, LX/FCY;->$VALUES:[LX/FCY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2210396
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2210397
    iput-object p3, p0, LX/FCY;->channelName:Ljava/lang/String;

    .line 2210398
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FCY;
    .locals 1

    .prologue
    .line 2210399
    const-class v0, LX/FCY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FCY;

    return-object v0
.end method

.method public static values()[LX/FCY;
    .locals 1

    .prologue
    .line 2210400
    sget-object v0, LX/FCY;->$VALUES:[LX/FCY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FCY;

    return-object v0
.end method
