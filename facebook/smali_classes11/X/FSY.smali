.class public final LX/FSY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Landroid/app/job/JobParameters;

.field public final synthetic b:Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchLollipopService;


# direct methods
.method public constructor <init>(Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchLollipopService;Landroid/app/job/JobParameters;)V
    .locals 0

    .prologue
    .line 2242085
    iput-object p1, p0, LX/FSY;->b:Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchLollipopService;

    iput-object p2, p0, LX/FSY;->a:Landroid/app/job/JobParameters;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2242086
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    .line 2242087
    iget-object v0, p0, LX/FSY;->b:Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchLollipopService;

    iget-object v1, p0, LX/FSY;->a:Landroid/app/job/JobParameters;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchLollipopService;->jobFinished(Landroid/app/job/JobParameters;Z)V

    .line 2242088
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2242089
    iget-object v0, p0, LX/FSY;->b:Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchLollipopService;

    iget-object v1, p0, LX/FSY;->a:Landroid/app/job/JobParameters;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetchLollipopService;->jobFinished(Landroid/app/job/JobParameters;Z)V

    .line 2242090
    return-void
.end method
