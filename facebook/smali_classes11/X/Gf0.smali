.class public LX/Gf0;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pr;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static c:LX/0Xm;


# instance fields
.field private a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Gf0",
            "<TE;>.Builder;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2376472
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2376473
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    iput-object v0, p0, LX/Gf0;->b:LX/0Zi;

    .line 2376474
    iput-object p1, p0, LX/Gf0;->a:LX/0Ot;

    .line 2376475
    return-void
.end method

.method public static a(LX/0QB;)LX/Gf0;
    .locals 4

    .prologue
    .line 2376476
    const-class v1, LX/Gf0;

    monitor-enter v1

    .line 2376477
    :try_start_0
    sget-object v0, LX/Gf0;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2376478
    sput-object v2, LX/Gf0;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2376479
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2376480
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2376481
    new-instance v3, LX/Gf0;

    const/16 p0, 0x210a

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Gf0;-><init>(LX/0Ot;)V

    .line 2376482
    move-object v0, v3

    .line 2376483
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2376484
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gf0;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2376485
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2376486
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private a(Landroid/view/View;LX/1X1;)V
    .locals 10

    .prologue
    .line 2376487
    check-cast p2, LX/Gez;

    .line 2376488
    iget-object v0, p0, LX/Gf0;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;

    iget-object v1, p2, LX/Gez;->a:Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;

    iget-object v2, p2, LX/Gez;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    const/4 v7, 0x0

    .line 2376489
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v5

    .line 2376490
    iget-object v3, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v3, v3

    .line 2376491
    check-cast v3, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v3}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    .line 2376492
    iget-object v3, v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;->e:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/1nA;

    invoke-static {v5}, LX/3Bd;->a(Lcom/facebook/graphql/model/GraphQLPage;)LX/1y5;

    move-result-object v5

    iget-object v6, v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;->f:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;->x()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v6

    if-eqz v6, :cond_0

    const/4 v6, 0x1

    :goto_0
    invoke-static {v1, v4}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v4

    invoke-static {v6, v4}, LX/17Q;->a(ZLX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    move-object v4, p1

    move-object v8, v7

    move-object v9, v7

    invoke-virtual/range {v3 .. v9}, LX/1nA;->a(Landroid/view/View;LX/1y5;Lcom/facebook/analytics/logger/HoneyClientEvent;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;)V

    .line 2376493
    return-void

    .line 2376494
    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method private b(LX/1X1;)V
    .locals 13

    .prologue
    .line 2376495
    check-cast p1, LX/Gez;

    .line 2376496
    iget-object v0, p0, LX/Gf0;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;

    iget-object v1, p1, LX/Gez;->a:Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;

    iget-object v2, p1, LX/Gez;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/Gez;->c:LX/3mj;

    iget-object v4, p1, LX/Gez;->d:LX/1Pr;

    const/4 v9, 0x1

    const/4 v11, 0x0

    .line 2376497
    new-instance v6, LX/GfH;

    invoke-direct {v6, v1}, LX/GfH;-><init>(Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;)V

    .line 2376498
    iget-object v5, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 2376499
    check-cast v5, LX/0jW;

    invoke-interface {v4, v6, v5}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v5

    move-object v7, v5

    check-cast v7, LX/GfI;

    .line 2376500
    iget-object v5, v7, LX/GfI;->a:Ljava/lang/Boolean;

    move-object v5, v5

    .line 2376501
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_0

    move v5, v9

    :goto_0
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 2376502
    iput-object v5, v7, LX/GfI;->a:Ljava/lang/Boolean;

    .line 2376503
    iget-object v5, v7, LX/GfI;->a:Ljava/lang/Boolean;

    move-object v5, v5

    .line 2376504
    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 2376505
    iput-boolean v5, v7, LX/GfI;->b:Z

    .line 2376506
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;->k()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v6

    .line 2376507
    iget-object v5, v2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v5, v5

    .line 2376508
    check-cast v5, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v5}, LX/1Wr;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/FeedUnit;

    move-result-object v5

    move-object v10, v5

    check-cast v10, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;

    .line 2376509
    iget-object v5, v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;->g:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/GeG;

    .line 2376510
    iget-object v8, v7, LX/GfI;->a:Ljava/lang/Boolean;

    move-object v7, v8

    .line 2376511
    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    invoke-static {v1, v10}, LX/16z;->a(LX/16h;LX/16h;)LX/162;

    move-result-object v8

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;->x()Lcom/facebook/graphql/model/GraphQLSponsoredData;

    move-result-object v12

    if-eqz v12, :cond_1

    :goto_1
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-static {v10}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnit;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {v5 .. v10}, LX/GeG;->a(Lcom/facebook/graphql/model/GraphQLPage;ZLX/162;Ljava/lang/Boolean;Ljava/lang/String;)V

    .line 2376512
    invoke-virtual {v3}, LX/3mj;->a()V

    .line 2376513
    return-void

    :cond_0
    move v5, v11

    .line 2376514
    goto :goto_0

    :cond_1
    move v9, v11

    .line 2376515
    goto :goto_1
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 6

    .prologue
    .line 2376516
    check-cast p2, LX/Gez;

    .line 2376517
    iget-object v0, p0, LX/Gf0;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;

    iget-object v2, p2, LX/Gez;->a:Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;

    iget-object v3, p2, LX/Gez;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v4, p2, LX/Gez;->c:LX/3mj;

    iget-object v5, p2, LX/Gez;->d:LX/1Pr;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/facebook/feedplugins/pyml/rows/components/PageYouMayLikeChainingItemComponentSpec;->a(LX/1De;Lcom/facebook/graphql/model/GraphQLPagesYouMayLikeFeedUnitItem;Lcom/facebook/feed/rows/core/props/FeedProps;LX/3mj;LX/1Pr;)LX/1Dg;

    move-result-object v0

    .line 2376518
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2376519
    invoke-static {}, LX/1dS;->b()V

    .line 2376520
    iget v0, p1, LX/1dQ;->b:I

    .line 2376521
    sparse-switch v0, :sswitch_data_0

    .line 2376522
    :goto_0
    return-object v2

    .line 2376523
    :sswitch_0
    check-cast p2, LX/3Ae;

    .line 2376524
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0, v1}, LX/Gf0;->a(Landroid/view/View;LX/1X1;)V

    goto :goto_0

    .line 2376525
    :sswitch_1
    iget-object v0, p1, LX/1dQ;->a:LX/1X1;

    invoke-direct {p0, v0}, LX/Gf0;->b(LX/1X1;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1552f40b -> :sswitch_1
        0x5259f580 -> :sswitch_0
    .end sparse-switch
.end method
