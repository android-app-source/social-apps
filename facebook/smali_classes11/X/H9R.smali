.class public LX/H9R;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8Z;


# static fields
.field private static final a:I

.field private static final b:I


# instance fields
.field private final c:LX/H8W;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field public g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

.field private h:Landroid/content/Context;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2434358
    const v0, 0x7f020809

    sput v0, LX/H9R;->a:I

    .line 2434359
    const v0, 0x7f081791

    sput v0, LX/H9R;->b:I

    return-void
.end method

.method public constructor <init>(LX/H8W;LX/0Ot;LX/0Ot;LX/0Ot;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)V
    .locals 0
    .param p5    # Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/H8W;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/intent/feed/IFeedIntentBuilder;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageActionData$Page;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2434360
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2434361
    iput-object p1, p0, LX/H9R;->c:LX/H8W;

    .line 2434362
    iput-object p2, p0, LX/H9R;->d:LX/0Ot;

    .line 2434363
    iput-object p3, p0, LX/H9R;->e:LX/0Ot;

    .line 2434364
    iput-object p4, p0, LX/H9R;->f:LX/0Ot;

    .line 2434365
    iput-object p5, p0, LX/H9R;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    .line 2434366
    iput-object p6, p0, LX/H9R;->h:Landroid/content/Context;

    .line 2434367
    return-void
.end method


# virtual methods
.method public final a()LX/HA7;
    .locals 6

    .prologue
    .line 2434355
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    sget v2, LX/H9R;->b:I

    sget v3, LX/H9R;->a:I

    const/4 v4, 0x1

    .line 2434356
    iget-object v5, p0, LX/H9R;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->E()LX/0Px;

    move-result-object v5

    invoke-static {v5}, LX/8A4;->a(LX/0Px;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, LX/H9R;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->A()Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v5, 0x1

    :goto_0
    move v5, v5

    .line 2434357
    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0

    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public final b()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2434346
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    sget v2, LX/H9R;->b:I

    sget v3, LX/H9R;->a:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2434348
    iget-object v0, p0, LX/H9R;->c:LX/H8W;

    sget-object v1, LX/9XI;->EVENT_TAPPED_MESSAGE:LX/9XI;

    iget-object v2, p0, LX/H9R;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2434349
    sget-object v0, LX/0ax;->ai:Ljava/lang/String;

    iget-object v1, p0, LX/H9R;->g:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2434350
    iget-object v0, p0, LX/H9R;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/intent/feed/IFeedIntentBuilder;

    iget-object v2, p0, LX/H9R;->h:Landroid/content/Context;

    invoke-interface {v0, v2, v1}, Lcom/facebook/intent/feed/IFeedIntentBuilder;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 2434351
    if-nez v1, :cond_0

    .line 2434352
    iget-object v0, p0, LX/H9R;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    const-string v1, "page_identity_message_fail"

    const-string v2, "Failed to resolve message compose URI!"

    invoke-virtual {v0, v1, v2}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2434353
    :goto_0
    return-void

    .line 2434354
    :cond_0
    iget-object v0, p0, LX/H9R;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/H9R;->h:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2434347
    const/4 v0, 0x0

    return-object v0
.end method
