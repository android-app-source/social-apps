.class public final LX/FZ8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "LX/Cw5;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FZG;


# direct methods
.method public constructor <init>(LX/FZG;)V
    .locals 0

    .prologue
    .line 2256950
    iput-object p1, p0, LX/FZ8;->a:LX/FZG;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(LX/Cw5;)V
    .locals 3
    .param p1    # LX/Cw5;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2256951
    if-eqz p1, :cond_0

    .line 2256952
    iget-object v0, p0, LX/FZ8;->a:LX/FZG;

    iget-object v0, v0, LX/FZG;->a:Lcom/facebook/search/debug/SearchDebugActivity;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Successfully Fetched "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2256953
    iget-object v2, p1, LX/Cw5;->a:LX/0Px;

    move-object v2, v2

    .line 2256954
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " recent searches!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2256955
    :cond_0
    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2256956
    iget-object v0, p0, LX/FZ8;->a:LX/FZG;

    iget-object v0, v0, LX/FZG;->a:Lcom/facebook/search/debug/SearchDebugActivity;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Refresh Failed! Exception: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2256957
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2256958
    check-cast p1, LX/Cw5;

    invoke-direct {p0, p1}, LX/FZ8;->a(LX/Cw5;)V

    return-void
.end method
