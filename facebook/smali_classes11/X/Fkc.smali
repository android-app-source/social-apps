.class public final LX/Fkc;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/Fkf;


# direct methods
.method public constructor <init>(LX/Fkf;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2278806
    iput-object p1, p0, LX/Fkc;->b:LX/Fkf;

    iput-object p2, p0, LX/Fkc;->a:Ljava/lang/String;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2278807
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2278808
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v3, 0x0

    .line 2278809
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2278810
    const/4 v1, 0x0

    .line 2278811
    if-eqz p1, :cond_2

    .line 2278812
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2278813
    if-eqz v0, :cond_2

    .line 2278814
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2278815
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel;->a()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2278816
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2278817
    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel;

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel;->a()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel;

    move-result-object v5

    .line 2278818
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel;->j()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel$PageInfoModel;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 2278819
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel;->j()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel$PageInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel$PageInfoModel;->j()Z

    move-result v1

    .line 2278820
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel;->j()Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel$PageInfoModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel$PageInfoModel;->a()Ljava/lang/String;

    move-result-object v0

    move v2, v1

    move-object v1, v0

    .line 2278821
    :goto_0
    invoke-virtual {v5}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserBeneficiarySearchQueryModel$PeopleResultsModel;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->size()I

    move-result v6

    :goto_1
    if-ge v3, v6, :cond_1

    invoke-virtual {v5, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;

    .line 2278822
    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->k()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-virtual {v0}, Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;->l()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 2278823
    new-instance v7, LX/FkY;

    invoke-direct {v7, v0}, LX/FkY;-><init>(Lcom/facebook/socialgood/protocol/FundraiserBeneficiarySearchModels$FundraiserFriendBeneficiarySearchFragmentModel;)V

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2278824
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    move v3, v2

    .line 2278825
    :cond_2
    iget-object v0, p0, LX/Fkc;->b:LX/Fkf;

    iget-object v0, v0, LX/Fkf;->c:Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;

    iget-object v2, p0, LX/Fkc;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v4, v3, v1}, Lcom/facebook/socialgood/create/beneficiaryselector/FundraiserBeneficiarySearchFragment;->a(Ljava/lang/String;Ljava/util/ArrayList;ZLjava/lang/String;)V

    .line 2278826
    return-void

    :cond_3
    move v2, v3

    goto :goto_0
.end method
