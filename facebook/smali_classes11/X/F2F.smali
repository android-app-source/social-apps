.class public final LX/F2F;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/editsettings/protocol/FetchGroupEditSettingsModels$FBGroupEditSettingsMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/F3A;

.field public final synthetic b:LX/F2L;


# direct methods
.method public constructor <init>(LX/F2L;LX/F3A;)V
    .locals 0

    .prologue
    .line 2192921
    iput-object p1, p0, LX/F2F;->b:LX/F2L;

    iput-object p2, p0, LX/F2F;->a:LX/F3A;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2192922
    iget-object v0, p0, LX/F2F;->a:LX/F3A;

    .line 2192923
    iget-object v1, v0, LX/F3A;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2192924
    iget-object v1, v0, LX/F3A;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    iget-object v2, v0, LX/F3A;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const p0, 0x7f0831d2

    invoke-virtual {v2, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object p0, v0, LX/F3A;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    const p1, 0x7f0831b1

    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p0

    invoke-static {v1, v2, p0}, Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;->a$redex0(Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 2192925
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2192926
    iget-object v0, p0, LX/F2F;->a:LX/F3A;

    .line 2192927
    iget-object p0, v0, LX/F3A;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->isAdded()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2192928
    iget-object p0, v0, LX/F3A;->a:Lcom/facebook/groups/editsettings/fragment/GroupEditPurposeFragment;

    invoke-virtual {p0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object p0

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->onBackPressed()V

    .line 2192929
    :cond_0
    return-void
.end method
