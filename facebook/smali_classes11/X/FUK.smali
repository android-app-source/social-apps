.class public final LX/FUK;
.super LX/79T;
.source ""


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/Runnable;

.field public final synthetic c:Lcom/facebook/qrcode/QRCodeFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/qrcode/QRCodeFragment;Ljava/lang/String;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 2248131
    iput-object p1, p0, LX/FUK;->c:Lcom/facebook/qrcode/QRCodeFragment;

    iput-object p2, p0, LX/FUK;->a:Ljava/lang/String;

    iput-object p3, p0, LX/FUK;->b:Ljava/lang/Runnable;

    invoke-direct {p0}, LX/79T;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 2248135
    iget-object v0, p0, LX/FUK;->c:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    iget-object v1, p0, LX/FUK;->a:Ljava/lang/String;

    .line 2248136
    iget-object v2, v0, LX/FUf;->a:LX/0if;

    sget-object v3, LX/0ig;->H:LX/0ih;

    const-string v4, "CHECK_PERMS_OK"

    invoke-virtual {v2, v3, v4, v1}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2248137
    iget-object v0, p0, LX/FUK;->b:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 2248138
    iget-object v0, p0, LX/FUK;->b:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 2248139
    :cond_0
    return-void
.end method

.method public final a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2248140
    iget-object v0, p0, LX/FUK;->c:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    iget-object v1, p0, LX/FUK;->a:Ljava/lang/String;

    .line 2248141
    invoke-static {}, LX/1rQ;->a()LX/1rQ;

    move-result-object v2

    const-string v3, "not_granted"

    invoke-static {p1}, LX/FUf;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v2

    const-string v3, "never_ask"

    invoke-static {p2}, LX/FUf;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/1rQ;->a(Ljava/lang/String;Ljava/lang/String;)LX/1rQ;

    move-result-object v2

    .line 2248142
    iget-object v3, v0, LX/FUf;->a:LX/0if;

    sget-object v4, LX/0ig;->H:LX/0ih;

    const-string p0, "CHECK_PERMS_NOT_GRANTED"

    invoke-virtual {v3, v4, p0, v1, v2}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;LX/1rQ;)V

    .line 2248143
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2248132
    iget-object v0, p0, LX/FUK;->c:Lcom/facebook/qrcode/QRCodeFragment;

    iget-object v0, v0, Lcom/facebook/qrcode/QRCodeFragment;->q:LX/FUf;

    iget-object v1, p0, LX/FUK;->a:Ljava/lang/String;

    .line 2248133
    iget-object v2, v0, LX/FUf;->a:LX/0if;

    sget-object v3, LX/0ig;->H:LX/0ih;

    const-string p0, "CHECK_PERMS_CANCELED"

    invoke-virtual {v2, v3, p0, v1}, LX/0if;->a(LX/0ih;Ljava/lang/String;Ljava/lang/String;)V

    .line 2248134
    return-void
.end method
