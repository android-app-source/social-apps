.class public LX/FI8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "LX/FI6;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FI8;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2221488
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(LX/0QB;)LX/FI8;
    .locals 3

    .prologue
    .line 2221476
    sget-object v0, LX/FI8;->a:LX/FI8;

    if-nez v0, :cond_1

    .line 2221477
    const-class v1, LX/FI8;

    monitor-enter v1

    .line 2221478
    :try_start_0
    sget-object v0, LX/FI8;->a:LX/FI8;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2221479
    if-eqz v2, :cond_0

    .line 2221480
    :try_start_1
    new-instance v0, LX/FI8;

    invoke-direct {v0}, LX/FI8;-><init>()V

    .line 2221481
    move-object v0, v0

    .line 2221482
    sput-object v0, LX/FI8;->a:LX/FI8;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2221483
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2221484
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2221485
    :cond_1
    sget-object v0, LX/FI8;->a:LX/FI8;

    return-object v0

    .line 2221486
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2221487
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 12

    .prologue
    .line 2221494
    check-cast p1, LX/FI6;

    .line 2221495
    invoke-static {}, LX/14N;->newBuilder()LX/14O;

    move-result-object v0

    const-string v1, "start_stream_upload"

    .line 2221496
    iput-object v1, v0, LX/14O;->b:Ljava/lang/String;

    .line 2221497
    move-object v0, v0

    .line 2221498
    const-string v1, "POST"

    .line 2221499
    iput-object v1, v0, LX/14O;->c:Ljava/lang/String;

    .line 2221500
    move-object v0, v0

    .line 2221501
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "messenger_videos/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, LX/0Yk;->a()Ljava/util/UUID;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/?phase=transfer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2221502
    iput-object v1, v0, LX/14O;->d:Ljava/lang/String;

    .line 2221503
    move-object v0, v0

    .line 2221504
    const/4 v1, 0x1

    .line 2221505
    iput-boolean v1, v0, LX/14O;->o:Z

    .line 2221506
    move-object v0, v0

    .line 2221507
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v6

    .line 2221508
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "Stream-Id"

    iget-object v9, p1, LX/FI6;->a:Ljava/lang/String;

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221509
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "Segment-Type"

    iget-object v9, p1, LX/FI6;->d:LX/FI7;

    invoke-virtual {v9}, LX/FI7;->getValue()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221510
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "Segment-Start-Offset"

    iget-wide v10, p1, LX/FI6;->b:J

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221511
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "X-Entity-Length"

    iget-object v9, p1, LX/FI6;->e:Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->length()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221512
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "X-Entity-Name"

    iget-object v9, p1, LX/FI6;->e:Ljava/io/File;

    invoke-virtual {v9}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221513
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "X-Entity-Type"

    iget-object v9, p1, LX/FI6;->c:Ljava/lang/String;

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221514
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string v8, "Offset"

    const-string v9, "0"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2221515
    invoke-virtual {v6}, LX/0Pz;->b()LX/0Px;

    move-result-object v6

    move-object v1, v6

    .line 2221516
    iput-object v1, v0, LX/14O;->g:Ljava/util/List;

    .line 2221517
    move-object v0, v0

    .line 2221518
    sget-object v1, LX/14S;->JSON:LX/14S;

    .line 2221519
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2221520
    move-object v0, v0

    .line 2221521
    sget-object v1, LX/14R;->FILE_PART_ENTITY:LX/14R;

    .line 2221522
    iput-object v1, v0, LX/14O;->w:LX/14R;

    .line 2221523
    move-object v0, v0

    .line 2221524
    iget-object v1, p1, LX/FI6;->e:Ljava/io/File;

    const/4 v2, 0x0

    iget-object v3, p1, LX/FI6;->e:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->length()J

    move-result-wide v4

    long-to-int v3, v4

    invoke-virtual {v0, v1, v2, v3}, LX/14O;->a(Ljava/io/File;II)LX/14O;

    move-result-object v0

    sget-object v1, LX/14P;->RETRY_SAFE:LX/14P;

    invoke-virtual {v0, v1}, LX/14O;->a(LX/14P;)LX/14O;

    move-result-object v0

    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2221489
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2221490
    iget v0, p2, LX/1pN;->b:I

    move v0, v0

    .line 2221491
    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 2221492
    new-instance v0, Lorg/apache/http/HttpException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Video segment transcoding upload failed. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/HttpException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2221493
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
