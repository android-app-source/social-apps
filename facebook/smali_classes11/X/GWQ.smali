.class public final LX/GWQ;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2361865
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2361866
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2361867
    :goto_0
    return v1

    .line 2361868
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2361869
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_OBJECT:LX/15z;

    if-eq v2, v3, :cond_3

    .line 2361870
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2361871
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2361872
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v3, v4, :cond_1

    if-eqz v2, :cond_1

    .line 2361873
    const-string v3, "nodes"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2361874
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2361875
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->START_ARRAY:LX/15z;

    if-ne v2, v3, :cond_2

    .line 2361876
    :goto_2
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v3, LX/15z;->END_ARRAY:LX/15z;

    if-eq v2, v3, :cond_2

    .line 2361877
    const/4 v3, 0x0

    .line 2361878
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v2

    sget-object v4, LX/15z;->START_OBJECT:LX/15z;

    if-eq v2, v4, :cond_8

    .line 2361879
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2361880
    :goto_3
    move v2, v3

    .line 2361881
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 2361882
    :cond_2
    invoke-static {v0, p1}, LX/1pR;->a(Ljava/util/List;LX/186;)I

    move-result v0

    move v0, v0

    .line 2361883
    goto :goto_1

    .line 2361884
    :cond_3
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, LX/186;->c(I)V

    .line 2361885
    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2361886
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    .line 2361887
    :cond_5
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2361888
    :cond_6
    :goto_4
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_7

    .line 2361889
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2361890
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2361891
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_6

    if-eqz v4, :cond_6

    .line 2361892
    const-string v5, "section_type"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 2361893
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    goto :goto_4

    .line 2361894
    :cond_7
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2361895
    invoke-virtual {p1, v3, v2}, LX/186;->b(II)V

    .line 2361896
    invoke-virtual {p1}, LX/186;->d()I

    move-result v3

    goto :goto_3

    :cond_8
    move v2, v3

    goto :goto_4
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    .line 2361897
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2361898
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2361899
    if-eqz v0, :cond_2

    .line 2361900
    const-string v1, "nodes"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361901
    invoke-virtual {p2}, LX/0nX;->d()V

    .line 2361902
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0}, LX/15i;->c(I)I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 2361903
    invoke-virtual {p0, v0, v1}, LX/15i;->q(II)I

    move-result v2

    const/4 p3, 0x0

    .line 2361904
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2361905
    invoke-virtual {p0, v2, p3}, LX/15i;->g(II)I

    move-result p1

    .line 2361906
    if-eqz p1, :cond_0

    .line 2361907
    const-string p1, "section_type"

    invoke-virtual {p2, p1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2361908
    invoke-virtual {p0, v2, p3}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2361909
    :cond_0
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2361910
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2361911
    :cond_1
    invoke-virtual {p2}, LX/0nX;->e()V

    .line 2361912
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2361913
    return-void
.end method
