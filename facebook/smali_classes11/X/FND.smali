.class public LX/FND;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/3Lx;

.field public final b:Landroid/view/LayoutInflater;

.field public final c:LX/FOd;

.field public final d:Landroid/content/Context;

.field public final e:Ljava/lang/String;

.field public final f:Lcom/facebook/user/model/User;

.field public final g:LX/FN9;

.field public final h:Z

.field public i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/facebook/user/model/User;ZLX/FN9;LX/3Lx;Landroid/view/LayoutInflater;LX/FOd;)V
    .locals 0
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/user/model/User;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/FN9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2232331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2232332
    iput-object p1, p0, LX/FND;->d:Landroid/content/Context;

    .line 2232333
    iput-object p2, p0, LX/FND;->e:Ljava/lang/String;

    .line 2232334
    iput-object p3, p0, LX/FND;->f:Lcom/facebook/user/model/User;

    .line 2232335
    iput-boolean p4, p0, LX/FND;->h:Z

    .line 2232336
    iput-object p5, p0, LX/FND;->g:LX/FN9;

    .line 2232337
    iput-object p6, p0, LX/FND;->a:LX/3Lx;

    .line 2232338
    iput-object p7, p0, LX/FND;->b:Landroid/view/LayoutInflater;

    .line 2232339
    iput-object p8, p0, LX/FND;->c:LX/FOd;

    .line 2232340
    return-void
.end method
