.class public LX/Gpl;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/instantshopping/view/block/InstantShoppingTextBlockView;",
        "LX/Cly;",
        ">;"
    }
.end annotation


# instance fields
.field public d:LX/Go7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Go4;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/GoE;


# direct methods
.method public constructor <init>(LX/Gqc;)V
    .locals 2

    .prologue
    .line 2395673
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2395674
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/Gpl;

    invoke-static {v0}, LX/Go7;->a(LX/0QB;)LX/Go7;

    move-result-object p1

    check-cast p1, LX/Go7;

    invoke-static {v0}, LX/Go4;->a(LX/0QB;)LX/Go4;

    move-result-object v0

    check-cast v0, LX/Go4;

    iput-object p1, p0, LX/Gpl;->d:LX/Go7;

    iput-object v0, p0, LX/Gpl;->e:LX/Go4;

    .line 2395675
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 7

    .prologue
    .line 2395676
    check-cast p1, LX/Cly;

    const/4 v4, 0x0

    .line 2395677
    check-cast p1, LX/GpI;

    .line 2395678
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395679
    check-cast v0, LX/Gqc;

    invoke-interface {v0, v4}, LX/CnG;->a(Landroid/os/Bundle;)V

    .line 2395680
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395681
    check-cast v0, LX/Gqc;

    .line 2395682
    iget-boolean v1, p1, LX/GpI;->c:Z

    move v1, v1

    .line 2395683
    iget-object v2, v0, LX/Gqc;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2395684
    iput-boolean v1, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->m:Z

    .line 2395685
    invoke-virtual {p0}, LX/CnT;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 2395686
    new-instance v2, LX/GoU;

    iget-object v3, p0, LX/Gpl;->f:LX/GoE;

    invoke-direct {v2, v1, v3}, LX/GoU;-><init>(Landroid/content/Context;LX/GoE;)V

    invoke-interface {p1}, LX/Cly;->e()LX/8Z4;

    move-result-object v3

    invoke-virtual {v2, v3}, LX/Cle;->a(LX/8Z4;)LX/Cle;

    move-result-object v2

    invoke-virtual {v2}, LX/Cle;->a()LX/Clf;

    move-result-object v2

    move-object v1, v2

    .line 2395687
    invoke-interface {p1}, LX/GoX;->B()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;

    move-result-object v2

    invoke-interface {p1}, LX/GoX;->A()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, LX/Gqc;->a(LX/Clf;Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;)V

    .line 2395688
    invoke-interface {p1}, LX/GoY;->D()LX/GoE;

    move-result-object v1

    iput-object v1, p0, LX/Gpl;->f:LX/GoE;

    .line 2395689
    iget-object v1, p0, LX/Gpl;->f:LX/GoE;

    .line 2395690
    iget-object v2, v0, LX/Gqc;->a:LX/Go0;

    invoke-virtual {v2, v1}, LX/Go0;->a(LX/GoE;)V

    .line 2395691
    invoke-interface {p1}, LX/Cly;->e()LX/8Z4;

    move-result-object v1

    invoke-interface {v1}, LX/8Z4;->b()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, LX/Cly;->e()LX/8Z4;

    move-result-object v1

    invoke-interface {v1}, LX/8Z4;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2395692
    :cond_0
    iget-object v1, p0, LX/Gpl;->f:LX/GoE;

    invoke-virtual {v0, v1, v4}, LX/Gqc;->a(LX/GoE;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;)V

    .line 2395693
    :cond_1
    return-void

    .line 2395694
    :cond_2
    invoke-interface {p1}, LX/Cly;->e()LX/8Z4;

    move-result-object v1

    invoke-interface {v1}, LX/8Z4;->b()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel$EntityRangesModel;

    .line 2395695
    sget-object v5, LX/Gpk;->a:[I

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel$EntityRangesModel;->b()Lcom/facebook/graphql/enums/GraphQLComposedEntityType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/enums/GraphQLComposedEntityType;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 2395696
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2395697
    :pswitch_0
    iget-object v5, p0, LX/Gpl;->f:LX/GoE;

    invoke-virtual {v1}, Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentTextModel$EntityRangesModel;->a()Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, LX/Gqc;->a(LX/GoE;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2395698
    invoke-super {p0, p1}, LX/CnT;->a(Landroid/os/Bundle;)V

    .line 2395699
    iget-object v0, p0, LX/Gpl;->d:LX/Go7;

    const-string v1, "richtext_element_start"

    iget-object v2, p0, LX/Gpl;->f:LX/GoE;

    invoke-virtual {v2}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2395700
    iget-object v0, p0, LX/Gpl;->e:LX/Go4;

    iget-object v1, p0, LX/Gpl;->f:LX/GoE;

    invoke-virtual {v1}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Go4;->a(Ljava/lang/String;)V

    .line 2395701
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 2395702
    invoke-super {p0, p1}, LX/CnT;->b(Landroid/os/Bundle;)V

    .line 2395703
    iget-object v0, p0, LX/Gpl;->d:LX/Go7;

    const-string v1, "richtext_element_end"

    iget-object v2, p0, LX/Gpl;->f:LX/GoE;

    invoke-virtual {v2}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Go7;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2395704
    iget-object v0, p0, LX/Gpl;->e:LX/Go4;

    iget-object v1, p0, LX/Gpl;->f:LX/GoE;

    invoke-virtual {v1}, LX/GoE;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Go4;->b(Ljava/lang/String;)V

    .line 2395705
    return-void
.end method
