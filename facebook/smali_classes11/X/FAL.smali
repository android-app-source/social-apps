.class public LX/FAL;
.super LX/4ot;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private final b:LX/FAK;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/FAK;)V
    .locals 1

    .prologue
    .line 2206932
    invoke-direct {p0, p1}, LX/4ot;-><init>(Landroid/content/Context;)V

    .line 2206933
    iput-object p2, p0, LX/FAL;->b:LX/FAK;

    .line 2206934
    const v0, 0x7f08314e

    invoke-virtual {p0, v0}, LX/FAL;->setTitle(I)V

    .line 2206935
    const-string v0, "off"

    invoke-virtual {p0, v0}, LX/FAL;->setDefaultValue(Ljava/lang/Object;)V

    .line 2206936
    const v0, 0x7f100047

    invoke-virtual {p0, v0}, LX/FAL;->setEntries(I)V

    .line 2206937
    const v0, 0x7f100048

    invoke-virtual {p0, v0}, LX/FAL;->setEntryValues(I)V

    .line 2206938
    invoke-virtual {p0, p0}, LX/FAL;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 2206939
    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2206940
    instance-of v1, p2, Ljava/lang/String;

    if-nez v1, :cond_1

    .line 2206941
    :cond_0
    :goto_0
    return v0

    .line 2206942
    :cond_1
    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, LX/FAI;->valueOf(Ljava/lang/String;)LX/FAI;

    move-result-object v1

    .line 2206943
    if-eqz v1, :cond_0

    .line 2206944
    iget-object v0, p0, LX/FAL;->b:LX/FAK;

    invoke-virtual {v0, v1}, LX/FAK;->a(LX/FAI;)V

    .line 2206945
    const/4 v0, 0x1

    goto :goto_0
.end method
