.class public LX/GH0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/GEF;

.field public final b:LX/GG6;

.field public final c:LX/2U3;

.field private final d:LX/0ad;


# direct methods
.method public constructor <init>(LX/GEF;LX/GG6;LX/2U3;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2334657
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2334658
    iput-object p1, p0, LX/GH0;->a:LX/GEF;

    .line 2334659
    iput-object p2, p0, LX/GH0;->b:LX/GG6;

    .line 2334660
    iput-object p3, p0, LX/GH0;->c:LX/2U3;

    .line 2334661
    iput-object p4, p0, LX/GH0;->d:LX/0ad;

    .line 2334662
    return-void
.end method

.method public static a(LX/0QB;)LX/GH0;
    .locals 1

    .prologue
    .line 2334663
    invoke-static {p0}, LX/GH0;->b(LX/0QB;)LX/GH0;

    move-result-object v0

    return-object v0
.end method

.method private static b(LX/0QB;)LX/GH0;
    .locals 15

    .prologue
    .line 2334652
    new-instance v4, LX/GH0;

    .line 2334653
    new-instance v5, LX/GEF;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v6

    check-cast v6, LX/0tX;

    const-class v7, LX/2U5;

    invoke-interface {p0, v7}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v7

    check-cast v7, LX/2U5;

    invoke-static {p0}, LX/2U4;->a(LX/0QB;)LX/2U4;

    move-result-object v8

    check-cast v8, LX/2U4;

    invoke-static {p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v9

    check-cast v9, LX/GG6;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v10

    check-cast v10, Ljava/util/concurrent/Executor;

    invoke-static {p0}, LX/0XX;->a(LX/0QB;)Lcom/facebook/quicklog/QuickPerformanceLogger;

    move-result-object v11

    check-cast v11, Lcom/facebook/quicklog/QuickPerformanceLogger;

    invoke-static {p0}, LX/0W9;->a(LX/0QB;)LX/0W9;

    move-result-object v12

    check-cast v12, LX/0W9;

    invoke-static {p0}, LX/0sh;->a(LX/0QB;)LX/0sh;

    move-result-object v13

    check-cast v13, LX/0sh;

    invoke-static {p0}, LX/16I;->a(LX/0QB;)LX/16I;

    move-result-object v14

    check-cast v14, LX/16I;

    invoke-direct/range {v5 .. v14}, LX/GEF;-><init>(LX/0tX;LX/2U5;LX/2U4;LX/GG6;Ljava/util/concurrent/Executor;Lcom/facebook/quicklog/QuickPerformanceLogger;LX/0W9;LX/0sh;LX/16I;)V

    .line 2334654
    move-object v0, v5

    .line 2334655
    check-cast v0, LX/GEF;

    invoke-static {p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v1

    check-cast v1, LX/GG6;

    invoke-static {p0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v2

    check-cast v2, LX/2U3;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {v4, v0, v1, v2, v3}, LX/GH0;-><init>(LX/GEF;LX/GG6;LX/2U3;LX/0ad;)V

    .line 2334656
    return-object v4
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8wL;LX/GCY;)V
    .locals 17

    .prologue
    .line 2334644
    sget-object v1, LX/8wL;->BOOST_EVENT:LX/8wL;

    move-object/from16 v0, p4

    if-ne v0, v1, :cond_0

    const-string v8, "boosted_event_mobile"

    .line 2334645
    :goto_0
    new-instance v1, LX/GGy;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    invoke-direct/range {v1 .. v8}, LX/GGy;-><init>(LX/GH0;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8wL;LX/GCY;Ljava/lang/String;)V

    .line 2334646
    new-instance v9, LX/GGz;

    move-object/from16 v10, p0

    move-object/from16 v11, p1

    move-object/from16 v12, p2

    move-object/from16 v13, p3

    move-object/from16 v14, p4

    move-object/from16 v15, p5

    move-object/from16 v16, v8

    invoke-direct/range {v9 .. v16}, LX/GGz;-><init>(LX/GH0;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/8wL;LX/GCY;Ljava/lang/String;)V

    .line 2334647
    sget-object v2, LX/8wL;->BOOST_POST:LX/8wL;

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, LX/8wL;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, LX/GH0;->d:LX/0ad;

    sget-short v3, LX/GDK;->D:S

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2334648
    move-object/from16 v0, p0

    iget-object v4, v0, LX/GH0;->a:LX/GEF;

    const/4 v10, 0x1

    move-object/from16 v5, p4

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-object v8, v1

    invoke-virtual/range {v4 .. v10}, LX/GEF;->a(LX/8wL;Ljava/lang/String;Ljava/lang/String;LX/GEA;LX/GGz;Z)V

    .line 2334649
    :goto_1
    return-void

    .line 2334650
    :cond_0
    const-string v8, "boosted_post_mobile"

    goto :goto_0

    .line 2334651
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, LX/GH0;->a:LX/GEF;

    const/4 v7, 0x1

    sget-object v8, LX/0zS;->c:LX/0zS;

    move-object/from16 v3, p4

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object v6, v1

    invoke-virtual/range {v2 .. v8}, LX/GEF;->a(LX/8wL;Ljava/lang/String;Ljava/lang/String;LX/GEA;ZLX/0zS;)V

    goto :goto_1
.end method
