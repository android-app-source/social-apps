.class public LX/FbG;
.super LX/FbF;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FbF",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStoryAttachment;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:Lcom/facebook/graphql/enums/GraphQLObjectType;

.field private static volatile b:LX/FbG;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2259769
    new-instance v0, Lcom/facebook/graphql/enums/GraphQLObjectType;

    const v1, -0x4b900828

    invoke-direct {v0, v1}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(I)V

    sput-object v0, LX/FbG;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-void
.end method

.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2259767
    invoke-direct {p0}, LX/FbF;-><init>()V

    .line 2259768
    return-void
.end method

.method public static a(LX/0QB;)LX/FbG;
    .locals 3

    .prologue
    .line 2259770
    sget-object v0, LX/FbG;->b:LX/FbG;

    if-nez v0, :cond_1

    .line 2259771
    const-class v1, LX/FbG;

    monitor-enter v1

    .line 2259772
    :try_start_0
    sget-object v0, LX/FbG;->b:LX/FbG;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2259773
    if-eqz v2, :cond_0

    .line 2259774
    :try_start_1
    new-instance v0, LX/FbG;

    invoke-direct {v0}, LX/FbG;-><init>()V

    .line 2259775
    move-object v0, v0

    .line 2259776
    sput-object v0, LX/FbG;->b:LX/FbG;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2259777
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2259778
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2259779
    :cond_1
    sget-object v0, LX/FbG;->b:LX/FbG;

    return-object v0

    .line 2259780
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2259781
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(ILcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;Ljava/lang/String;)Ljava/lang/Object;
    .locals 2
    .param p3    # Ljava/lang/String;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2259756
    const/4 v0, 0x0

    .line 2259757
    const/4 v1, 0x5

    if-ne p1, v1, :cond_1

    .line 2259758
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->e()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    const/4 p0, 0x0

    .line 2259759
    invoke-static {v0}, LX/6X3;->b(Lcom/facebook/graphql/model/GraphQLNode;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2259760
    if-eqz v1, :cond_3

    invoke-static {v1}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v1

    .line 2259761
    :goto_1
    if-nez v1, :cond_4

    move-object v1, p0

    .line 2259762
    :cond_2
    :goto_2
    move-object v0, v1

    .line 2259763
    goto :goto_0

    :cond_3
    move-object v1, p0

    .line 2259764
    goto :goto_1

    .line 2259765
    :cond_4
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object p1

    if-eqz p1, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object p1

    invoke-virtual {p1}, LX/0Px;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_5

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->x()LX/0Px;

    move-result-object v1

    const/4 p1, 0x0

    invoke-virtual {v1, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2259766
    :cond_5
    sget-object p1, LX/Brd;->a:LX/0Rl;

    invoke-interface {p1, v1}, LX/0Rl;->apply(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_2

    move-object v1, p0

    goto :goto_2
.end method

.method public final b(Ljava/lang/Object;)Lcom/facebook/graphql/enums/GraphQLObjectType;
    .locals 1
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2259755
    sget-object v0, LX/FbG;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    return-object v0
.end method

.method public final c(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2259753
    check-cast p1, Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    .line 2259754
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLMedia;->T()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
