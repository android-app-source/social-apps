.class public LX/Fxm;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/Fsr;


# direct methods
.method public constructor <init>(LX/Fsr;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2306094
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2306095
    iput-object p1, p0, LX/Fxm;->a:LX/Fsr;

    .line 2306096
    return-void
.end method

.method public static a(LX/0QB;)LX/Fxm;
    .locals 4

    .prologue
    .line 2306097
    const-class v1, LX/Fxm;

    monitor-enter v1

    .line 2306098
    :try_start_0
    sget-object v0, LX/Fxm;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2306099
    sput-object v2, LX/Fxm;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2306100
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2306101
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2306102
    new-instance p0, LX/Fxm;

    invoke-static {v0}, LX/Fsr;->a(LX/0QB;)LX/Fsr;

    move-result-object v3

    check-cast v3, LX/Fsr;

    invoke-direct {p0, v3}, LX/Fxm;-><init>(LX/Fsr;)V

    .line 2306103
    move-object v0, p0

    .line 2306104
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2306105
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Fxm;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2306106
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2306107
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
