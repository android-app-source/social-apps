.class public LX/Fby;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FbA;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/FbA",
        "<",
        "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLInterfaces$KeywordSearchModuleFragment;",
        "Lcom/facebook/search/results/model/unit/SearchResultsUnsupportedFeedUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Fby;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2261471
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2261472
    return-void
.end method

.method public static a(LX/0QB;)LX/Fby;
    .locals 3

    .prologue
    .line 2261473
    sget-object v0, LX/Fby;->a:LX/Fby;

    if-nez v0, :cond_1

    .line 2261474
    const-class v1, LX/Fby;

    monitor-enter v1

    .line 2261475
    :try_start_0
    sget-object v0, LX/Fby;->a:LX/Fby;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2261476
    if-eqz v2, :cond_0

    .line 2261477
    :try_start_1
    new-instance v0, LX/Fby;

    invoke-direct {v0}, LX/Fby;-><init>()V

    .line 2261478
    move-object v0, v0

    .line 2261479
    sput-object v0, LX/Fby;->a:LX/Fby;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2261480
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2261481
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2261482
    :cond_1
    sget-object v0, LX/Fby;->a:LX/Fby;

    return-object v0

    .line 2261483
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2261484
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/0Px;
    .locals 2

    .prologue
    .line 2261485
    check-cast p1, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;

    .line 2261486
    new-instance v0, Lcom/facebook/search/results/model/unit/SearchResultsUnsupportedFeedUnit;

    invoke-direct {v0, p1}, Lcom/facebook/search/results/model/unit/SearchResultsUnsupportedFeedUnit;-><init>(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)V

    .line 2261487
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/search/model/SearchResultsBaseFeedUnit;->a(Ljava/lang/String;)V

    .line 2261488
    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method
