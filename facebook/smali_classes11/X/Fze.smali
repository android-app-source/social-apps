.class public LX/Fze;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:LX/0Rf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Rf",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public c:LX/1mR;

.field public d:LX/1Ck;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2308193
    const-string v0, "FETCH_MEMORIAL_PROFILE_PICTURE_QUERY_TAG"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/Fze;->a:LX/0Rf;

    .line 2308194
    const-string v0, "FETCH_MEMORIAL_COVER_PHOTO_QUERY_TAG"

    invoke-static {v0}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v0

    sput-object v0, LX/Fze;->b:LX/0Rf;

    return-void
.end method

.method public constructor <init>(LX/1mR;LX/1Ck;)V
    .locals 0
    .param p2    # LX/1Ck;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2308195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2308196
    iput-object p1, p0, LX/Fze;->c:LX/1mR;

    .line 2308197
    iput-object p2, p0, LX/Fze;->d:LX/1Ck;

    .line 2308198
    return-void
.end method
