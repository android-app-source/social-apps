.class public LX/FCa;
.super LX/2WG;
.source ""


# instance fields
.field public final a:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 2210417
    invoke-direct {p0}, LX/2WG;-><init>()V

    .line 2210418
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2210419
    iput-object p1, p0, LX/FCa;->a:Landroid/net/Uri;

    .line 2210420
    return-void
.end method


# virtual methods
.method public final a()LX/1bh;
    .locals 2

    .prologue
    .line 2210416
    new-instance v0, LX/1ed;

    iget-object v1, p0, LX/FCa;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, LX/1ed;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2210410
    if-ne p0, p1, :cond_0

    .line 2210411
    const/4 v0, 0x1

    .line 2210412
    :goto_0
    return v0

    .line 2210413
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 2210414
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2210415
    :cond_2
    iget-object v0, p0, LX/FCa;->a:Landroid/net/Uri;

    check-cast p1, LX/FCa;

    iget-object v1, p1, LX/FCa;->a:Landroid/net/Uri;

    invoke-static {v0, v1}, LX/0kk;->equal(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 2210409
    iget-object v0, p0, LX/FCa;->a:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/FCa;->a:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
