.class public final LX/H3m;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/0TF;

.field public final synthetic b:LX/H3n;


# direct methods
.method public constructor <init>(LX/H3n;LX/0TF;)V
    .locals 0

    .prologue
    .line 2421876
    iput-object p1, p0, LX/H3m;->b:LX/H3n;

    iput-object p2, p0, LX/H3m;->a:LX/0TF;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2421877
    iget-object v0, p0, LX/H3m;->a:LX/0TF;

    if-eqz v0, :cond_0

    .line 2421878
    iget-object v0, p0, LX/H3m;->a:LX/0TF;

    invoke-interface {v0, p1}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 2421879
    :cond_0
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 2421880
    check-cast p1, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel;

    .line 2421881
    iget-object v0, p0, LX/H3m;->a:LX/0TF;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel;->a()Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2421882
    :cond_0
    :goto_0
    return-void

    .line 2421883
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel;->a()Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel;->a()LX/0Px;

    move-result-object v2

    .line 2421884
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2421885
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2421886
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 2421887
    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel;

    .line 2421888
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel;->a()Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 2421889
    invoke-virtual {v0}, Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel;->a()Lcom/facebook/nearby/v2/network/NearbyPlacesHereCardSuggestionGraphQLModels$FBNearbyPlacesHereCardHugeResultCellQueryModel$SuggestionsModel$EdgesModel$NodeModel;

    move-result-object v0

    .line 2421890
    new-instance v4, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    invoke-direct {v4, v0}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;-><init>(LX/CQM;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2421891
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2421892
    :cond_3
    iget-object v0, p0, LX/H3m;->a:LX/0TF;

    invoke-interface {v0, v3}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0
.end method
