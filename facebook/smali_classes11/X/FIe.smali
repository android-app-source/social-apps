.class public LX/FIe;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2222461
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2222462
    return-void
.end method

.method public static a(Ljava/io/File;Ljava/util/List;ILX/FIn;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;I",
            "LX/FIn;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/FIQ;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2222422
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 2222423
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2222424
    new-instance v3, Ljava/io/BufferedInputStream;

    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v3, v0}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 2222425
    new-instance v0, LX/FIH;

    invoke-direct {v0}, LX/FIH;-><init>()V

    const/16 v1, 0x10

    .line 2222426
    iput v1, v0, LX/FIH;->d:I

    .line 2222427
    move-object v0, v0

    .line 2222428
    iput p2, v0, LX/FIH;->c:I

    .line 2222429
    move-object v4, v0

    .line 2222430
    const/4 v0, -0x1

    .line 2222431
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2222432
    sub-int v1, v0, v1

    add-int/lit8 v1, v1, -0x1

    mul-int/2addr v1, p2

    int-to-long v6, v1

    .line 2222433
    invoke-static {v3, v6, v7}, LX/47v;->a(Ljava/io/InputStream;J)J

    move-result-wide v8

    .line 2222434
    cmp-long v1, v8, v6

    if-gez v1, :cond_0

    .line 2222435
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to read chunks from, chunkIDs go pact the end of the file."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2222436
    :cond_0
    :try_start_0
    invoke-static {}, LX/FIN;->a()[B

    move-result-object v6

    .line 2222437
    invoke-static {v6}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 2222438
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 2222439
    sget-object v7, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v7}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget v7, p3, LX/FIn;->b:I

    invoke-virtual {v1, v7}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget-wide v8, p3, LX/FIn;->a:J

    invoke-virtual {v1, v8, v9}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 2222440
    const/4 v1, 0x0

    .line 2222441
    :goto_1
    if-ge v1, p2, :cond_1

    .line 2222442
    add-int/lit8 v7, v1, 0x10

    sub-int v8, p2, v1

    invoke-virtual {v3, v6, v7, v8}, Ljava/io/BufferedInputStream;->read([BII)I

    move-result v7

    .line 2222443
    if-ltz v7, :cond_1

    .line 2222444
    add-int/2addr v1, v7

    .line 2222445
    goto :goto_1

    .line 2222446
    :cond_1
    iput-object v6, v4, LX/FIH;->a:[B

    .line 2222447
    move-object v1, v4

    .line 2222448
    iput v0, v1, LX/FIH;->e:I

    .line 2222449
    move-object v1, v1

    .line 2222450
    new-instance v6, LX/FIQ;

    iget-object v7, v1, LX/FIH;->a:[B

    iget v8, v1, LX/FIH;->c:I

    iget v9, v1, LX/FIH;->d:I

    iget p0, v1, LX/FIH;->e:I

    invoke-direct {v6, v7, v8, v9, p0}, LX/FIQ;-><init>([BIII)V

    move-object v1, v6

    .line 2222451
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v0

    .line 2222452
    goto :goto_0

    .line 2222453
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 2222454
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIQ;

    .line 2222455
    iget-object v4, v0, LX/FIO;->a:[B

    move-object v0, v4

    .line 2222456
    invoke-static {v0}, LX/FIN;->a([B)V

    goto :goto_2

    .line 2222457
    :cond_2
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    .line 2222458
    throw v1

    .line 2222459
    :cond_3
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V

    .line 2222460
    return-object v2
.end method

.method public static a(Ljava/util/List;)Ljava/util/Map;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/FIO;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "LX/FIn;",
            "Ljava/util/List",
            "<",
            "LX/FIO;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2222412
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2222413
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FIO;

    .line 2222414
    iget-object v5, v0, LX/FIO;->a:[B

    move-object v5, v5

    .line 2222415
    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 2222416
    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 2222417
    new-instance v6, LX/FIn;

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Ljava/nio/ByteBuffer;->getInt(I)I

    move-result v7

    const/16 v8, 0x8

    invoke-virtual {v5, v8}, Ljava/nio/ByteBuffer;->getLong(I)J

    move-result-wide v9

    invoke-direct {v6, v7, v9, v10}, LX/FIn;-><init>(IJ)V

    move-object v1, v6

    .line 2222418
    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2222419
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2222420
    :cond_0
    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2222421
    :cond_1
    return-object v2
.end method
