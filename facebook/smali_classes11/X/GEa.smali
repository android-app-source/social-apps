.class public LX/GEa;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CSJ;


# instance fields
.field private final a:LX/13Q;

.field private final b:LX/0sh;

.field private final c:LX/2U4;

.field private final d:LX/0ad;


# direct methods
.method public constructor <init>(LX/13Q;LX/0sh;LX/2U4;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2331915
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2331916
    iput-object p1, p0, LX/GEa;->a:LX/13Q;

    .line 2331917
    iput-object p2, p0, LX/GEa;->b:LX/0sh;

    .line 2331918
    iput-object p3, p0, LX/GEa;->c:LX/2U4;

    .line 2331919
    iput-object p4, p0, LX/GEa;->d:LX/0ad;

    .line 2331920
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 2331909
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-nez v0, :cond_0

    .line 2331910
    const/4 v0, 0x0

    .line 2331911
    :goto_0
    return v0

    .line 2331912
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    .line 2331913
    const-class v1, Lcom/facebook/adinterfaces/AdInterfacesObjectiveActivity;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    .line 2331914
    invoke-static {v0, v1}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final b(Landroid/content/Intent;)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 2331890
    const-string v0, "storyId"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2331891
    const-string v0, "page_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2331892
    const-string v0, "objective"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/8wL;

    .line 2331893
    if-eqz v4, :cond_0

    if-eqz v3, :cond_0

    sget-object v5, LX/8wL;->BOOST_POST:LX/8wL;

    invoke-virtual {v5, v0}, LX/8wL;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 2331894
    :cond_0
    iget-object v0, p0, LX/GEa;->a:LX/13Q;

    const-string v2, "spotty_ads_lwi_blocked_not_bp"

    invoke-virtual {v0, v2}, LX/13Q;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2331895
    :goto_0
    return v0

    .line 2331896
    :cond_1
    iget-object v5, p0, LX/GEa;->d:LX/0ad;

    sget-short v6, LX/GDK;->D:S

    invoke-interface {v5, v6, v2}, LX/0ad;->a(SZ)Z

    move-result v5

    if-nez v5, :cond_2

    .line 2331897
    iget-object v0, p0, LX/GEa;->a:LX/13Q;

    const-string v2, "spotty_ads_lwi_blocked_failed_qe"

    invoke-virtual {v0, v2}, LX/13Q;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2331898
    goto :goto_0

    .line 2331899
    :cond_2
    iget-object v5, p0, LX/GEa;->c:LX/2U4;

    invoke-virtual {v5, v4, v3, v0}, LX/2U4;->a(Ljava/lang/String;Ljava/lang/String;LX/8wL;)LX/0zO;

    move-result-object v4

    .line 2331900
    iget-object v5, p0, LX/GEa;->b:LX/0sh;

    invoke-virtual {v5, v4}, LX/0sh;->d(LX/0zO;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 2331901
    iget-object v0, p0, LX/GEa;->a:LX/13Q;

    const-string v2, "spotty_ads_lwi_blocked_no_cached_admin_info"

    invoke-virtual {v0, v2}, LX/13Q;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2331902
    goto :goto_0

    .line 2331903
    :cond_3
    iget-object v4, p0, LX/GEa;->c:LX/2U4;

    invoke-virtual {v4, v3, v0, v1}, LX/2U4;->a(Ljava/lang/String;LX/8wL;Z)LX/0zO;

    move-result-object v0

    .line 2331904
    iget-object v3, p0, LX/GEa;->b:LX/0sh;

    invoke-virtual {v3, v0}, LX/0sh;->d(LX/0zO;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2331905
    iget-object v0, p0, LX/GEa;->a:LX/13Q;

    const-string v2, "spotty_ads_lwi_blocked_no_cached_post_promotion_info"

    invoke-virtual {v0, v2}, LX/13Q;->a(Ljava/lang/String;)V

    move v0, v1

    .line 2331906
    goto :goto_0

    .line 2331907
    :cond_4
    iget-object v0, p0, LX/GEa;->a:LX/13Q;

    const-string v1, "spotty_ads_lwi_passed"

    invoke-virtual {v0, v1}, LX/13Q;->a(Ljava/lang/String;)V

    move v0, v2

    .line 2331908
    goto :goto_0
.end method

.method public final c(Landroid/content/Intent;)LX/AEn;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2331889
    const/4 v0, 0x0

    return-object v0
.end method
