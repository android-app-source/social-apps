.class public final LX/Fy9;
.super LX/BPh;
.source ""


# instance fields
.field public final synthetic a:LX/FyD;


# direct methods
.method public constructor <init>(LX/FyD;)V
    .locals 0

    .prologue
    .line 2306368
    iput-object p1, p0, LX/Fy9;->a:LX/FyD;

    invoke-direct {p0}, LX/BPh;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 6

    .prologue
    .line 2306369
    check-cast p1, LX/BPg;

    .line 2306370
    iget-object v0, p0, LX/Fy9;->a:LX/FyD;

    iget-object v0, v0, LX/FyD;->b:LX/Fy4;

    if-eqz v0, :cond_0

    .line 2306371
    iget-object v0, p0, LX/Fy9;->a:LX/FyD;

    iget-object v0, v0, LX/FyD;->b:LX/Fy4;

    iget-wide v2, p1, LX/BPg;->a:J

    iget-object v1, p1, LX/BPg;->b:LX/2na;

    sget-object v4, LX/2hA;->PROFILE:LX/2hA;

    const/4 p1, 0x1

    .line 2306372
    sget-object v5, LX/2na;->CONFIRM:LX/2na;

    if-ne v1, v5, :cond_1

    move v5, p1

    .line 2306373
    :goto_0
    if-eqz v5, :cond_2

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->ARE_FRIENDS:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object p0, v5

    .line 2306374
    :goto_1
    invoke-static {v0, v2, v3, p0, p1}, LX/Fy4;->a$redex0(LX/Fy4;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Z)V

    .line 2306375
    iget-object v5, v0, LX/Fy4;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/2dj;

    invoke-virtual {v5, v2, v3, v1, v4}, LX/2dj;->a(JLX/2na;LX/2hA;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    .line 2306376
    new-instance p1, LX/Fxx;

    invoke-direct {p1, v0, v2, v3, p0}, LX/Fxx;-><init>(LX/Fy4;JLcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    iget-object p0, v0, LX/Fy4;->i:Ljava/util/concurrent/Executor;

    invoke-static {v5, p1, p0}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    .line 2306377
    :cond_0
    return-void

    .line 2306378
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 2306379
    :cond_2
    sget-object v5, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object p0, v5

    goto :goto_1
.end method
