.class public final LX/Gsw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/InternSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/InternSettingsActivity;)V
    .locals 0

    .prologue
    .line 2400360
    iput-object p1, p0, LX/Gsw;->a:Lcom/facebook/katana/InternSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 2400361
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2400362
    iget-object v1, p0, LX/Gsw;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v1, v1, Lcom/facebook/katana/InternSettingsActivity;->B:LX/44G;

    .line 2400363
    iget-object p0, v1, LX/44G;->b:Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-interface {p0}, Lcom/facebook/prefs/shared/FbSharedPreferences;->edit()LX/0hN;

    move-result-object p0

    sget-object p1, LX/44G;->a:LX/0Tn;

    invoke-interface {p0, p1, v0}, LX/0hN;->putBoolean(LX/0Tn;Z)LX/0hN;

    move-result-object p0

    invoke-interface {p0}, LX/0hN;->commit()V

    .line 2400364
    const/4 v0, 0x1

    return v0
.end method
