.class public final enum LX/FWA;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FWA;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FWA;

.field public static final enum FROM_CACHE:LX/FWA;

.field public static final enum FROM_SERVER:LX/FWA;

.field public static final enum NONE:LX/FWA;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2251667
    new-instance v0, LX/FWA;

    const-string v1, "FROM_SERVER"

    invoke-direct {v0, v1, v2}, LX/FWA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FWA;->FROM_SERVER:LX/FWA;

    .line 2251668
    new-instance v0, LX/FWA;

    const-string v1, "FROM_CACHE"

    invoke-direct {v0, v1, v3}, LX/FWA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FWA;->FROM_CACHE:LX/FWA;

    .line 2251669
    new-instance v0, LX/FWA;

    const-string v1, "NONE"

    invoke-direct {v0, v1, v4}, LX/FWA;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FWA;->NONE:LX/FWA;

    .line 2251670
    const/4 v0, 0x3

    new-array v0, v0, [LX/FWA;

    sget-object v1, LX/FWA;->FROM_SERVER:LX/FWA;

    aput-object v1, v0, v2

    sget-object v1, LX/FWA;->FROM_CACHE:LX/FWA;

    aput-object v1, v0, v3

    sget-object v1, LX/FWA;->NONE:LX/FWA;

    aput-object v1, v0, v4

    sput-object v0, LX/FWA;->$VALUES:[LX/FWA;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2251671
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FWA;
    .locals 1

    .prologue
    .line 2251672
    const-class v0, LX/FWA;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FWA;

    return-object v0
.end method

.method public static values()[LX/FWA;
    .locals 1

    .prologue
    .line 2251673
    sget-object v0, LX/FWA;->$VALUES:[LX/FWA;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FWA;

    return-object v0
.end method
