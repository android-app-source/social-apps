.class public LX/FdA;
.super Lcom/facebook/widget/CustomRelativeLayout;
.source ""

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field public a:Lcom/facebook/widget/text/BetterTextView;

.field public b:Lcom/facebook/resources/ui/FbCheckBox;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2263245
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, LX/FdA;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2263246
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2263247
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomRelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2263248
    const p1, 0x7f03128f

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->setContentView(I)V

    .line 2263249
    const p1, 0x7f0d02c4

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/widget/text/BetterTextView;

    iput-object p1, p0, LX/FdA;->a:Lcom/facebook/widget/text/BetterTextView;

    .line 2263250
    const p1, 0x7f0d0298

    invoke-virtual {p0, p1}, Lcom/facebook/widget/CustomRelativeLayout;->a(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/facebook/resources/ui/FbCheckBox;

    iput-object p1, p0, LX/FdA;->b:Lcom/facebook/resources/ui/FbCheckBox;

    .line 2263251
    invoke-virtual {p0}, LX/FdA;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    const p2, 0x7f0b14df

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result p1

    .line 2263252
    invoke-virtual {p0}, LX/FdA;->getPaddingLeft()I

    move-result p2

    invoke-virtual {p0}, LX/FdA;->getPaddingRight()I

    move-result p3

    invoke-virtual {p0, p2, p1, p3, p1}, LX/FdA;->setPadding(IIII)V

    .line 2263253
    return-void
.end method


# virtual methods
.method public final isChecked()Z
    .locals 1

    .prologue
    .line 2263254
    iget-boolean v0, p0, LX/FdA;->c:Z

    return v0
.end method

.method public final performClick()Z
    .locals 1

    .prologue
    .line 2263255
    invoke-virtual {p0}, LX/FdA;->toggle()V

    .line 2263256
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->performClick()Z

    move-result v0

    return v0
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 2263257
    iget-boolean v0, p0, LX/FdA;->c:Z

    if-eq v0, p1, :cond_0

    .line 2263258
    iput-boolean p1, p0, LX/FdA;->c:Z

    .line 2263259
    iget-object v0, p0, LX/FdA;->b:Lcom/facebook/resources/ui/FbCheckBox;

    invoke-virtual {v0, p1}, Lcom/facebook/resources/ui/FbCheckBox;->setChecked(Z)V

    .line 2263260
    invoke-super {p0}, Lcom/facebook/widget/CustomRelativeLayout;->refreshDrawableState()V

    .line 2263261
    :cond_0
    return-void
.end method

.method public final toggle()V
    .locals 1

    .prologue
    .line 2263262
    iget-boolean v0, p0, LX/FdA;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, LX/FdA;->setChecked(Z)V

    .line 2263263
    return-void

    .line 2263264
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
