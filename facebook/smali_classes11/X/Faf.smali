.class public final LX/Faf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;

.field public final synthetic b:Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;)V
    .locals 0

    .prologue
    .line 2258974
    iput-object p1, p0, LX/Faf;->b:Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;

    iput-object p2, p0, LX/Faf;->a:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const v1, 0x4321d86b

    invoke-static {v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2258975
    iget-object v0, p0, LX/Faf;->a:Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;

    invoke-virtual {v0}, Lcom/facebook/search/protocol/awareness/SearchAwarenessModels$TutorialNuxCarouselFieldsFragmentModel;->s()Ljava/lang/String;

    move-result-object v2

    .line 2258976
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2258977
    iget-object v0, p0, LX/Faf;->b:Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;

    iget-object v0, v0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->t:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17Y;

    iget-object v3, p0, LX/Faf;->b:Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;

    invoke-virtual {v3}, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v3, v2}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2258978
    if-eqz v0, :cond_0

    .line 2258979
    iget-object v2, p0, LX/Faf;->b:Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;

    invoke-virtual {v2, v0}, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->startActivity(Landroid/content/Intent;)V

    .line 2258980
    :cond_0
    iget-object v0, p0, LX/Faf;->b:Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;

    iget-object v0, v0, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->u:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/13B;

    const-string v2, "secondary_action"

    invoke-virtual {v0, v2}, LX/13B;->b(Ljava/lang/String;)V

    .line 2258981
    iget-object v0, p0, LX/Faf;->b:Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;

    invoke-virtual {v0}, Lcom/facebook/search/quickpromotion/SearchAwarenessTutorialNuxActivity;->finish()V

    .line 2258982
    const v0, 0x649eb717

    invoke-static {v4, v4, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
