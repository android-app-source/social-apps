.class public LX/FYR;
.super LX/EkO;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/EkO",
        "<",
        "LX/BO1;",
        "LX/1a1;",
        ">;",
        "Lcom/facebook/widget/recyclerview/interleaved/InterleavedInnerAdapter;"
    }
.end annotation


# static fields
.field private static final b:I


# instance fields
.field private final c:Landroid/support/v4/app/FragmentActivity;

.field private final d:Ljava/lang/String;

.field private final e:LX/FYc;

.field private final f:LX/FYP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2256034
    const v0, 0x7f031250

    sput v0, LX/FYR;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;LX/FYc;)V
    .locals 1
    .param p1    # Landroid/support/v4/app/FragmentActivity;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Ljava/lang/String;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2256035
    invoke-direct {p0, p1}, LX/EkO;-><init>(Landroid/content/Context;)V

    .line 2256036
    new-instance v0, LX/FYP;

    invoke-direct {v0}, LX/FYP;-><init>()V

    iput-object v0, p0, LX/FYR;->f:LX/FYP;

    .line 2256037
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/1OM;->a(Z)V

    .line 2256038
    iput-object p1, p0, LX/FYR;->c:Landroid/support/v4/app/FragmentActivity;

    .line 2256039
    iput-object p2, p0, LX/FYR;->d:Ljava/lang/String;

    .line 2256040
    iput-object p3, p0, LX/FYR;->e:LX/FYc;

    .line 2256041
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 5

    .prologue
    .line 2256042
    iget-object v0, p0, LX/FYR;->c:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 2256043
    iget-object v1, p0, LX/FYR;->e:LX/FYc;

    iget-object v2, p0, LX/FYR;->c:Landroid/support/v4/app/FragmentActivity;

    const/4 v3, 0x0

    invoke-virtual {v0, p2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iget-object v3, p0, LX/FYR;->d:Ljava/lang/String;

    iget-object v4, p0, LX/FYR;->f:LX/FYP;

    invoke-virtual {v1, v2, v0, v3, v4}, LX/FYc;->a(Landroid/support/v4/app/FragmentActivity;Landroid/view/View;Ljava/lang/String;LX/FYP;)LX/FYb;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1a1;I)V
    .locals 1

    .prologue
    .line 2256044
    iget-object v0, p0, LX/EkN;->b:Landroid/database/Cursor;

    move-object v0, v0

    .line 2256045
    invoke-interface {v0, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 2256046
    check-cast p1, LX/FYb;

    invoke-virtual {p0}, LX/EkO;->d()LX/AU0;

    move-result-object v0

    check-cast v0, LX/BO1;

    invoke-virtual {p1, v0, p2}, LX/FYb;->a(LX/BO1;I)V

    .line 2256047
    return-void
.end method

.method public final a(LX/1a1;LX/AU0;)V
    .locals 2

    .prologue
    .line 2256048
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Should convert to delegation to better avoid this method"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final c(LX/1a1;)V
    .locals 0

    .prologue
    .line 2256049
    check-cast p1, LX/FYb;

    invoke-virtual {p1}, LX/FYb;->w()V

    .line 2256050
    return-void
.end method

.method public final d(LX/1a1;)V
    .locals 0

    .prologue
    .line 2256051
    check-cast p1, LX/FYb;

    invoke-virtual {p1}, LX/FYb;->x()V

    .line 2256052
    return-void
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2256053
    sget v0, LX/FYR;->b:I

    return v0
.end method
