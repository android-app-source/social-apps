.class public final enum LX/H0h;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/H0h;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/H0h;

.field public static final enum DESCRIPTION:LX/H0h;

.field public static final enum FIELD_EDIT_TEXT:LX/H0h;

.field public static final enum FIELD_LABEL:LX/H0h;

.field public static final enum TITLE:LX/H0h;


# instance fields
.field private final viewType:I


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2414107
    new-instance v0, LX/H0h;

    const-string v1, "TITLE"

    const v2, 0x7f0d00ea

    invoke-direct {v0, v1, v3, v2}, LX/H0h;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/H0h;->TITLE:LX/H0h;

    .line 2414108
    new-instance v0, LX/H0h;

    const-string v1, "DESCRIPTION"

    const v2, 0x7f0d00e7

    invoke-direct {v0, v1, v4, v2}, LX/H0h;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/H0h;->DESCRIPTION:LX/H0h;

    .line 2414109
    new-instance v0, LX/H0h;

    const-string v1, "FIELD_LABEL"

    const v2, 0x7f0d00e5

    invoke-direct {v0, v1, v5, v2}, LX/H0h;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/H0h;->FIELD_LABEL:LX/H0h;

    .line 2414110
    new-instance v0, LX/H0h;

    const-string v1, "FIELD_EDIT_TEXT"

    const v2, 0x7f0d00e4

    invoke-direct {v0, v1, v6, v2}, LX/H0h;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/H0h;->FIELD_EDIT_TEXT:LX/H0h;

    .line 2414111
    const/4 v0, 0x4

    new-array v0, v0, [LX/H0h;

    sget-object v1, LX/H0h;->TITLE:LX/H0h;

    aput-object v1, v0, v3

    sget-object v1, LX/H0h;->DESCRIPTION:LX/H0h;

    aput-object v1, v0, v4

    sget-object v1, LX/H0h;->FIELD_LABEL:LX/H0h;

    aput-object v1, v0, v5

    sget-object v1, LX/H0h;->FIELD_EDIT_TEXT:LX/H0h;

    aput-object v1, v0, v6

    sput-object v0, LX/H0h;->$VALUES:[LX/H0h;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2414112
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2414113
    iput p3, p0, LX/H0h;->viewType:I

    .line 2414114
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/H0h;
    .locals 1

    .prologue
    .line 2414115
    const-class v0, LX/H0h;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/H0h;

    return-object v0
.end method

.method public static values()[LX/H0h;
    .locals 1

    .prologue
    .line 2414116
    sget-object v0, LX/H0h;->$VALUES:[LX/H0h;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/H0h;

    return-object v0
.end method


# virtual methods
.method public final toInt()I
    .locals 1

    .prologue
    .line 2414117
    iget v0, p0, LX/H0h;->viewType:I

    return v0
.end method
