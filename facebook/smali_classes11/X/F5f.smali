.class public final LX/F5f;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/F5k;

.field public final synthetic b:LX/F5h;


# direct methods
.method public constructor <init>(LX/F5h;LX/F5k;)V
    .locals 0

    .prologue
    .line 2199104
    iput-object p1, p0, LX/F5f;->b:LX/F5h;

    iput-object p2, p0, LX/F5f;->a:LX/F5k;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 2199081
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2199082
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2199083
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2199084
    check-cast v0, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel;

    .line 2199085
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2199086
    invoke-virtual {v0}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel;->a()Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel;->a()Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel;->a()LX/0Px;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2199087
    invoke-virtual {v0}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel;->a()Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    .line 2199088
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    invoke-virtual {v4, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;

    .line 2199089
    new-instance p1, LX/F5n;

    invoke-direct {p1}, LX/F5n;-><init>()V

    .line 2199090
    iput-object v1, p1, LX/F5n;->a:Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;

    .line 2199091
    move-object v1, p1

    .line 2199092
    new-instance p1, LX/F5o;

    iget-object v0, v1, LX/F5n;->a:Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;

    invoke-direct {p1, v0}, LX/F5o;-><init>(Lcom/facebook/groups/gysc/protocol/FetchGroupsGyscModels$FetchGroupCreationSuggestionsModel$GroupsYouShouldCreateModel$NodesModel;)V

    move-object v1, p1

    .line 2199093
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2199094
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2199095
    :cond_0
    move-object v0, v3

    .line 2199096
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2199097
    iget-object v1, p0, LX/F5f;->a:LX/F5k;

    .line 2199098
    if-nez v0, :cond_2

    .line 2199099
    :cond_1
    :goto_1
    return-void

    .line 2199100
    :cond_2
    goto :goto_3

    .line 2199101
    :goto_2
    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    goto :goto_1

    .line 2199102
    :goto_3
    goto :goto_4

    .line 2199103
    :goto_4
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v3, v1, LX/F5k;->d:Ljava/util/List;

    goto :goto_2
.end method
