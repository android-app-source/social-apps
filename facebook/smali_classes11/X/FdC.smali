.class public final LX/FdC;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/Fd8;


# direct methods
.method public constructor <init>(LX/Fd8;)V
    .locals 0

    .prologue
    .line 2263272
    iput-object p1, p0, LX/FdC;->a:LX/Fd8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, -0x2d2c7019

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2263273
    iget-object v0, p0, LX/FdC;->a:LX/Fd8;

    iget-object v0, v0, LX/Fd8;->b:LX/FcX;

    if-eqz v0, :cond_0

    .line 2263274
    iget-object v0, p0, LX/FdC;->a:LX/Fd8;

    iget-object v2, v0, LX/Fd8;->b:LX/FcX;

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2263275
    iget-object v4, v2, LX/FcX;->a:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v4, v0}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object v4

    .line 2263276
    if-nez v4, :cond_1

    .line 2263277
    :cond_0
    :goto_0
    const v0, 0x158fb186

    invoke-static {v3, v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2263278
    :cond_1
    invoke-virtual {v4}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->c()Ljava/lang/String;

    move-result-object v5

    .line 2263279
    iget-object v4, v2, LX/FcX;->d:LX/FcJ;

    iget-object v4, v4, LX/FcJ;->f:Ljava/util/Map;

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_5

    const/4 v4, 0x1

    .line 2263280
    :goto_1
    iget-object v6, v2, LX/FcX;->d:LX/FcJ;

    iget-object v6, v6, LX/FcJ;->f:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-interface {v6, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2263281
    iget-object v4, v2, LX/FcX;->b:LX/FdQ;

    new-instance v5, LX/CyH;

    iget-object v6, v2, LX/FcX;->c:LX/CyH;

    .line 2263282
    iget-object v7, v6, LX/CyH;->a:Ljava/lang/String;

    move-object v6, v7

    .line 2263283
    iget-object v7, v2, LX/FcX;->d:LX/FcJ;

    .line 2263284
    invoke-virtual {v7}, LX/FcJ;->f()Z

    move-result v8

    if-eqz v8, :cond_6

    .line 2263285
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    .line 2263286
    iget-object v8, v7, LX/FcJ;->h:Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;

    invoke-virtual {v8}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel;->a()LX/0Px;

    move-result-object v11

    invoke-virtual {v11}, LX/0Px;->size()I

    move-result p0

    const/4 v8, 0x0

    move v9, v8

    :goto_2
    if-ge v9, p0, :cond_4

    invoke-virtual {v11, v9}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;

    .line 2263287
    invoke-virtual {v8}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValuesFragmentModel$FilterValuesModel$EdgesModel;->a()Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;

    move-result-object p1

    .line 2263288
    if-eqz p1, :cond_3

    iget-object v8, v7, LX/FcJ;->f:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2263289
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->length()I

    move-result v8

    if-lez v8, :cond_2

    .line 2263290
    const-string v8, ", "

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2263291
    :cond_2
    invoke-virtual {p1}, Lcom/facebook/search/results/protocol/filters/SearchResultPageFilterFragmentsModels$SearchResultPageFilterValueNodeFragmentModel;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2263292
    :cond_3
    add-int/lit8 v8, v9, 0x1

    move v9, v8

    goto :goto_2

    .line 2263293
    :cond_4
    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 2263294
    :goto_3
    move-object v7, v8

    .line 2263295
    iget-object v8, v2, LX/FcX;->d:LX/FcJ;

    iget-object v9, v2, LX/FcX;->d:LX/FcJ;

    iget-object v9, v9, LX/FcJ;->f:Ljava/util/Map;

    invoke-static {v8, v9}, LX/FcJ;->a$redex0(LX/FcJ;Ljava/util/Map;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v6, v7, v8}, LX/CyH;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, LX/FdQ;->a(LX/CyH;)V

    goto/16 :goto_0

    .line 2263296
    :cond_5
    const/4 v4, 0x0

    goto :goto_1

    :cond_6
    const-string v8, ""

    goto :goto_3
.end method
