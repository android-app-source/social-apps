.class public final LX/Gc8;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;I)V
    .locals 0

    .prologue
    .line 2371054
    iput-object p1, p0, LX/Gc8;->b:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    iput p2, p0, LX/Gc8;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 2371055
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 2371056
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2371057
    move v0, v1

    :goto_0
    iget-object v2, p0, LX/Gc8;->b:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    iget-object v2, v2, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a:LX/Gbz;

    invoke-virtual {v2}, LX/Gbz;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 2371058
    iget v2, p0, LX/Gc8;->a:I

    if-eq v0, v2, :cond_0

    .line 2371059
    iget-object v2, p0, LX/Gc8;->b:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    iget-object v2, v2, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->i:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2371060
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2371061
    :cond_1
    return-void
.end method
