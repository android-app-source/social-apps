.class public final enum LX/Fox;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Fox;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Fox;

.field public static final enum FETCH_CATEGORY_CHARITY:LX/Fox;

.field public static final enum FETCH_QUERY_CHARITY:LX/Fox;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2290463
    new-instance v0, LX/Fox;

    const-string v1, "FETCH_CATEGORY_CHARITY"

    invoke-direct {v0, v1, v2}, LX/Fox;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fox;->FETCH_CATEGORY_CHARITY:LX/Fox;

    .line 2290464
    new-instance v0, LX/Fox;

    const-string v1, "FETCH_QUERY_CHARITY"

    invoke-direct {v0, v1, v3}, LX/Fox;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fox;->FETCH_QUERY_CHARITY:LX/Fox;

    .line 2290465
    const/4 v0, 0x2

    new-array v0, v0, [LX/Fox;

    sget-object v1, LX/Fox;->FETCH_CATEGORY_CHARITY:LX/Fox;

    aput-object v1, v0, v2

    sget-object v1, LX/Fox;->FETCH_QUERY_CHARITY:LX/Fox;

    aput-object v1, v0, v3

    sput-object v0, LX/Fox;->$VALUES:[LX/Fox;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2290466
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Fox;
    .locals 1

    .prologue
    .line 2290467
    const-class v0, LX/Fox;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Fox;

    return-object v0
.end method

.method public static values()[LX/Fox;
    .locals 1

    .prologue
    .line 2290468
    sget-object v0, LX/Fox;->$VALUES:[LX/Fox;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Fox;

    return-object v0
.end method
