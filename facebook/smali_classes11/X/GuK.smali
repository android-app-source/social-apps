.class public final LX/GuK;
.super LX/BWM;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 2403227
    iput-object p1, p0, LX/GuK;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403228
    invoke-direct {p0, p2}, LX/BWM;-><init>(Landroid/os/Handler;)V

    .line 2403229
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/webview/FacebookWebView;LX/BWN;)V
    .locals 6

    .prologue
    .line 2403230
    iget-object v0, p0, LX/GuK;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403231
    iget-boolean v1, v0, Landroid/support/v4/app/Fragment;->mResumed:Z

    move v0, v1

    .line 2403232
    if-nez v0, :cond_0

    .line 2403233
    :goto_0
    return-void

    .line 2403234
    :cond_0
    iget-object v0, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2403235
    const-string v1, "options"

    invoke-interface {p2, v0, v1}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2403236
    iget-object v0, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2403237
    const-string v2, "dismiss_script"

    invoke-interface {p2, v0, v2}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2403238
    :try_start_0
    iget-object v0, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v0

    .line 2403239
    const-string v3, "selected_index"

    invoke-interface {p2, v0, v3}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 2403240
    :goto_1
    iget-object v3, p0, LX/GuK;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v3, v3, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->G:LX/0lp;

    iget-object v4, p0, LX/GuK;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v4, v4, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    .line 2403241
    new-instance v5, Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;

    invoke-direct {v5}, Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;-><init>()V

    .line 2403242
    iput-object v3, v5, Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;->l:LX/0lp;

    .line 2403243
    iput-object v4, v5, Lcom/facebook/katana/activity/faceweb/dialog/FeedFilterPickerDialogFragment;->m:Lcom/facebook/webview/FacebookWebView;

    .line 2403244
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    .line 2403245
    const-string p2, "feed_filter_buttons"

    invoke-virtual {p1, p2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2403246
    const-string p2, "feed_filter_dismiss_script"

    invoke-virtual {p1, p2, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2403247
    const-string p2, "feed_filter_selected_index"

    invoke-virtual {p1, p2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2403248
    invoke-virtual {v5, p1}, Landroid/support/v4/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 2403249
    move-object v0, v5

    .line 2403250
    iget-object v1, p0, LX/GuK;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2403251
    iget-object v2, v1, Landroid/support/v4/app/Fragment;->mFragmentManager:LX/0jz;

    move-object v1, v2

    .line 2403252
    const-string v2, "dialog"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    goto :goto_0

    .line 2403253
    :catch_0
    iget-object v0, p0, LX/GuK;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->s:LX/03V;

    const-string v3, "fb4a_displaying_faceweb_feedfilterpicker"

    const-string v4, "Failed to parse argument selected_index"

    invoke-virtual {v0, v3, v4}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2403254
    const/4 v0, 0x0

    goto :goto_1
.end method
