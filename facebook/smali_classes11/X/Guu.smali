.class public final LX/Guu;
.super LX/BWM;
.source ""


# instance fields
.field public final synthetic b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/activity/faceweb/FacewebFragment;Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 2404004
    iput-object p1, p0, LX/Guu;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2404005
    invoke-direct {p0, p2}, LX/BWM;-><init>(Landroid/os/Handler;)V

    .line 2404006
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/webview/FacebookWebView;LX/BWN;)V
    .locals 8

    .prologue
    .line 2404007
    iget-object v0, p0, LX/Guu;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    .line 2404008
    iget-object v1, p1, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v1, v1

    .line 2404009
    const-string v2, "callback"

    invoke-interface {p2, v1, v2}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2404010
    iput-object v1, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->J:Ljava/lang/String;

    .line 2404011
    iget-object v0, p0, LX/Guu;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    if-eqz v0, :cond_1

    iget-object v0, p0, LX/Guu;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-object v0, v0, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->r:LX/GxF;

    .line 2404012
    iget-object v1, v0, Lcom/facebook/webview/FacebookWebView;->k:Ljava/lang/String;

    move-object v0, v1

    .line 2404013
    :goto_0
    iget-object v1, p0, LX/Guu;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    const-string v2, "target"

    invoke-interface {p2, v0, v2}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->a(Ljava/lang/String;)J

    move-result-wide v2

    .line 2404014
    iput-wide v2, v1, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->q:J

    .line 2404015
    const-string v1, "true"

    const-string v2, "photosOnly"

    invoke-interface {p2, v0, v2}, LX/BWN;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    .line 2404016
    new-instance v1, LX/8AA;

    sget-object v2, LX/8AB;->FACEWEB:LX/8AB;

    invoke-direct {v1, v2}, LX/8AA;-><init>(LX/8AB;)V

    invoke-virtual {v1}, LX/8AA;->i()LX/8AA;

    move-result-object v1

    sget-object v2, LX/21D;->FACEWEB:LX/21D;

    const-string v3, "facewebPhotoButton"

    invoke-static {v2, v3}, LX/1nC;->a(LX/21D;Ljava/lang/String;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    new-instance v3, LX/89I;

    iget-object v4, p0, LX/Guu;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    iget-wide v4, v4, Lcom/facebook/katana/activity/faceweb/FacewebFragment;->q:J

    sget-object v6, LX/2rw;->OTHER:LX/2rw;

    invoke-direct {v3, v4, v5, v6}, LX/89I;-><init>(JLX/2rw;)V

    invoke-virtual {v3}, LX/89I;->a()Lcom/facebook/ipc/composer/intent/ComposerTargetData;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setInitialTargetData(Lcom/facebook/ipc/composer/intent/ComposerTargetData;)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->setIsFireAndForget(Z)Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;->a()Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    move-result-object v2

    .line 2404017
    iput-object v2, v1, LX/8AA;->a:Lcom/facebook/ipc/composer/intent/ComposerConfiguration;

    .line 2404018
    move-object v1, v1

    .line 2404019
    if-eqz v0, :cond_0

    .line 2404020
    invoke-virtual {v1}, LX/8AA;->j()LX/8AA;

    .line 2404021
    :cond_0
    iget-object v0, p0, LX/Guu;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/facebook/ipc/simplepicker/SimplePickerIntent;->a(Landroid/content/Context;LX/8AA;)Landroid/content/Intent;

    move-result-object v0

    .line 2404022
    const-string v1, "extra_source_activity"

    iget-object v2, p0, LX/Guu;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2404023
    iget-object v1, p0, LX/Guu;->b:Lcom/facebook/katana/activity/faceweb/FacewebFragment;

    const/16 v2, 0x32

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 2404024
    return-void

    .line 2404025
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
