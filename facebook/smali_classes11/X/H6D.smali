.class public final LX/H6D;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/offers/fragment/OfferDetailPageFragment;Z)V
    .locals 0

    .prologue
    .line 2425970
    iput-object p1, p0, LX/H6D;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iput-boolean p2, p0, LX/H6D;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2425938
    const-string v0, "OfferDetailPageFragment"

    const-string v1, "Error loading offers"

    invoke-static {v0, v1, p1}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2425939
    iget-object v0, p0, LX/H6D;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->N(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    .line 2425940
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2425941
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2425942
    if-eqz p1, :cond_0

    .line 2425943
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2425944
    if-eqz v0, :cond_0

    .line 2425945
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Results: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2425946
    iget-object v1, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v1, v1

    .line 2425947
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 2425948
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2425949
    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;

    invoke-virtual {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;->L()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 2425950
    iget-object v0, p0, LX/H6D;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->N(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    .line 2425951
    :goto_0
    return-void

    .line 2425952
    :sswitch_0
    iget-object v1, p0, LX/H6D;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    .line 2425953
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2425954
    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;

    const/4 p1, 0x0

    .line 2425955
    new-instance v2, LX/H83;

    invoke-static {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;->a(LX/H6w;)Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;

    move-result-object v3

    invoke-direct {v2, p1, p1, v3}, LX/H83;-><init>(LX/H72;LX/H70;Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;)V

    move-object v0, v2

    .line 2425956
    iput-object v0, v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    .line 2425957
    :goto_1
    iget-object v0, p0, LX/H6D;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    iget-object v0, v0, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->b:LX/0Sh;

    new-instance v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment$1$1;

    invoke-direct {v1, p0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment$1$1;-><init>(LX/H6D;)V

    invoke-virtual {v0, v1}, LX/0Sh;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 2425958
    :sswitch_1
    iget-object v1, p0, LX/H6D;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    .line 2425959
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2425960
    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;

    const/4 p1, 0x0

    .line 2425961
    new-instance v2, LX/H83;

    invoke-static {v0}, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;->a(LX/H70;)Lcom/facebook/offers/graphql/OfferQueriesModels$OfferClaimDataModel;

    move-result-object v3

    invoke-direct {v2, p1, v3, p1}, LX/H83;-><init>(LX/H72;LX/H70;Lcom/facebook/offers/graphql/OfferQueriesModels$CouponDataModel;)V

    move-object v0, v2

    .line 2425962
    iput-object v0, v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    .line 2425963
    goto :goto_1

    .line 2425964
    :sswitch_2
    iget-object v1, p0, LX/H6D;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    .line 2425965
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2425966
    check-cast v0, Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;

    invoke-static {v0}, LX/H83;->c(Lcom/facebook/offers/graphql/OfferQueriesModels$OfferDetailQueryModel;)LX/H83;

    move-result-object v0

    .line 2425967
    iput-object v0, v1, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->av:LX/H83;

    .line 2425968
    goto :goto_1

    .line 2425969
    :cond_0
    iget-object v0, p0, LX/H6D;->b:Lcom/facebook/offers/fragment/OfferDetailPageFragment;

    invoke-static {v0}, Lcom/facebook/offers/fragment/OfferDetailPageFragment;->N(Lcom/facebook/offers/fragment/OfferDetailPageFragment;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        -0x14a5a4ff -> :sswitch_2
        0x78a7c446 -> :sswitch_0
        0x7ee69360 -> :sswitch_1
    .end sparse-switch
.end method
