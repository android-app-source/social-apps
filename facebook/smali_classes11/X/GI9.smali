.class public final LX/GI9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field public final synthetic a:LX/GIA;


# direct methods
.method public constructor <init>(LX/GIA;)V
    .locals 0

    .prologue
    .line 2335793
    iput-object p1, p0, LX/GI9;->a:LX/GIA;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2335778
    iget-object v0, p0, LX/GI9;->a:LX/GIA;

    iget-object v0, v0, LX/GIA;->l:Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;

    .line 2335779
    iget-object v2, v0, Lcom/facebook/adinterfaces/ui/AdInterfacesAccountView;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;

    move-object v0, v2

    .line 2335780
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesSpinnerView;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2335781
    :cond_0
    :goto_0
    return-void

    .line 2335782
    :cond_1
    iget-object v0, p0, LX/GI9;->a:LX/GIA;

    iget-object v0, v0, LX/GIA;->i:LX/0Px;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2335783
    iget-object v0, p0, LX/GI9;->a:LX/GIA;

    iget-object v0, v0, LX/GIA;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, LX/0PB;->checkState(Z)V

    .line 2335784
    iget-object v0, p0, LX/GI9;->a:LX/GIA;

    iget-object v0, v0, LX/GIA;->i:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-lt p3, v0, :cond_3

    .line 2335785
    :goto_2
    iget-object v0, p0, LX/GI9;->a:LX/GIA;

    iget-object v0, v0, LX/GIA;->i:LX/0Px;

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->t()Ljava/lang/String;

    move-result-object v0

    .line 2335786
    iget-object v1, p0, LX/GI9;->a:LX/GIA;

    iget-object v1, v1, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, LX/0YN;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2335787
    iget-object v1, p0, LX/GI9;->a:LX/GIA;

    iget-object v1, v1, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->l()Ljava/lang/String;

    move-result-object v1

    .line 2335788
    iget-object v2, p0, LX/GI9;->a:LX/GIA;

    iget-object v2, v2, LX/GIA;->k:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2, v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b(Ljava/lang/String;)V

    .line 2335789
    iget-object v2, p0, LX/GI9;->a:LX/GIA;

    .line 2335790
    iget-object v3, v2, LX/GHg;->b:LX/GCE;

    move-object v2, v3

    .line 2335791
    new-instance v3, LX/GFj;

    invoke-direct {v3, v1, v0}, LX/GFj;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, LX/GCE;->a(LX/8wN;)V

    goto :goto_0

    :cond_2
    move v0, v1

    .line 2335792
    goto :goto_1

    :cond_3
    move v1, p3

    goto :goto_2
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 2335777
    return-void
.end method
