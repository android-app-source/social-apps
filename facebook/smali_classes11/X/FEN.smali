.class public final LX/FEN;
.super LX/2EJ;
.source ""


# instance fields
.field private final b:LX/11S;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field public e:Ljava/util/Calendar;

.field public f:LX/FEH;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/11S;)V
    .locals 0

    .prologue
    .line 2216222
    invoke-direct {p0, p1}, LX/2EJ;-><init>(Landroid/content/Context;)V

    .line 2216223
    iput-object p2, p0, LX/FEN;->b:LX/11S;

    .line 2216224
    return-void
.end method

.method public static d$redex0(LX/FEN;)V
    .locals 3

    .prologue
    .line 2216226
    iget-object v0, p0, LX/FEN;->c:Landroid/widget/TextView;

    iget-object v1, p0, LX/FEN;->b:LX/11S;

    iget-object v2, p0, LX/FEN;->e:Ljava/util/Calendar;

    invoke-static {v1, v2}, LX/FEO;->c(LX/11S;Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2216227
    iget-object v0, p0, LX/FEN;->d:Landroid/widget/TextView;

    iget-object v1, p0, LX/FEN;->b:LX/11S;

    iget-object v2, p0, LX/FEN;->e:Ljava/util/Calendar;

    invoke-static {v1, v2}, LX/FEO;->d(LX/11S;Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2216228
    return-void
.end method


# virtual methods
.method public final a(ILjava/util/Calendar;LX/FEH;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2216229
    iput-object p2, p0, LX/FEN;->e:Ljava/util/Calendar;

    .line 2216230
    iput-object p3, p0, LX/FEN;->f:LX/FEH;

    .line 2216231
    invoke-virtual {p0}, LX/FEN;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 2216232
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 2216233
    new-instance v2, Landroid/widget/FrameLayout;

    invoke-direct {v2, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 2216234
    const v3, 0x7f030f58

    invoke-virtual {v1, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 2216235
    invoke-virtual {p0, v2}, LX/2EJ;->a(Landroid/view/View;)V

    .line 2216236
    invoke-virtual {p0, v4}, LX/2EJ;->a(Ljava/lang/CharSequence;)V

    .line 2216237
    invoke-virtual {v0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, LX/FEN;->setTitle(Ljava/lang/CharSequence;)V

    .line 2216238
    const v2, 0x7f083268

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, LX/FEI;

    invoke-direct {v3, p0}, LX/FEI;-><init>(LX/FEN;)V

    invoke-virtual {p0, v2, v3}, LX/2EJ;->a(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 2216239
    const v2, 0x7f083269

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v4}, LX/2EJ;->b(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 2216240
    const v0, 0x7f0d2513

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/FEN;->c:Landroid/widget/TextView;

    .line 2216241
    iget-object v0, p0, LX/FEN;->c:Landroid/widget/TextView;

    new-instance v2, LX/FEJ;

    invoke-direct {v2, p0}, LX/FEJ;-><init>(LX/FEN;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2216242
    const v0, 0x7f0d2514

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, LX/FEN;->d:Landroid/widget/TextView;

    .line 2216243
    iget-object v0, p0, LX/FEN;->d:Landroid/widget/TextView;

    new-instance v1, LX/FEK;

    invoke-direct {v1, p0}, LX/FEK;-><init>(LX/FEN;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2216244
    invoke-static {p0}, LX/FEN;->d$redex0(LX/FEN;)V

    .line 2216245
    invoke-super {p0}, LX/2EJ;->show()V

    .line 2216246
    return-void
.end method

.method public final show()V
    .locals 2

    .prologue
    .line 2216225
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Use show(int, Calendar, Listener) instead"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
