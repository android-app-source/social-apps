.class public LX/FRX;
.super LX/48b;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/FRX;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field public final b:LX/6xb;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/6xb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2240466
    invoke-direct {p0}, LX/48b;-><init>()V

    .line 2240467
    iput-object p1, p0, LX/FRX;->a:Landroid/content/res/Resources;

    .line 2240468
    iput-object p2, p0, LX/FRX;->b:LX/6xb;

    .line 2240469
    return-void
.end method

.method public static a(LX/0QB;)LX/FRX;
    .locals 5

    .prologue
    .line 2240470
    sget-object v0, LX/FRX;->c:LX/FRX;

    if-nez v0, :cond_1

    .line 2240471
    const-class v1, LX/FRX;

    monitor-enter v1

    .line 2240472
    :try_start_0
    sget-object v0, LX/FRX;->c:LX/FRX;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2240473
    if-eqz v2, :cond_0

    .line 2240474
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2240475
    new-instance p0, LX/FRX;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/6xb;->a(LX/0QB;)LX/6xb;

    move-result-object v4

    check-cast v4, LX/6xb;

    invoke-direct {p0, v3, v4}, LX/FRX;-><init>(Landroid/content/res/Resources;LX/6xb;)V

    .line 2240476
    move-object v0, p0

    .line 2240477
    sput-object v0, LX/FRX;->c:LX/FRX;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2240478
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2240479
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2240480
    :cond_1
    sget-object v0, LX/FRX;->c:LX/FRX;

    return-object v0

    .line 2240481
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2240482
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 7

    .prologue
    .line 2240483
    invoke-super {p0, p1}, LX/48b;->a(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    .line 2240484
    sget-object v1, LX/71C;->FB_PAYMENT_SETTINGS:LX/71C;

    iget-object v2, p0, LX/FRX;->a:Landroid/content/res/Resources;

    const v3, 0x7f081e0a

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "p2p_payment_general_settings"

    const/4 v6, 0x1

    .line 2240485
    invoke-static {}, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->newBuilder()LX/71A;

    move-result-object v4

    invoke-static {}, Lcom/facebook/payments/picker/model/PickerScreenStyleParams;->newBuilder()LX/71E;

    move-result-object v5

    invoke-virtual {v5}, LX/71E;->c()Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    move-result-object v5

    .line 2240486
    iput-object v5, v4, LX/71A;->e:Lcom/facebook/payments/picker/model/PickerScreenStyleParams;

    .line 2240487
    move-object v4, v4

    .line 2240488
    sget-object v5, LX/6xZ;->PAYMENTS_SETTINGS:LX/6xZ;

    sget-object p1, LX/6xY;->PAYMENTS_SETTINGS:LX/6xY;

    invoke-static {p1}, Lcom/facebook/payments/logging/PaymentsLoggingSessionData;->a(LX/6xY;)LX/6xd;

    move-result-object p1

    invoke-virtual {p1}, LX/6xd;->a()Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    move-result-object p1

    invoke-static {v5, p1}, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->a(LX/6xZ;Lcom/facebook/payments/logging/PaymentsLoggingSessionData;)LX/718;

    move-result-object v5

    .line 2240489
    iput-object v3, v5, LX/718;->c:Ljava/lang/String;

    .line 2240490
    move-object v5, v5

    .line 2240491
    invoke-virtual {v5}, LX/718;->a()Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    move-result-object v5

    move-object v5, v5

    .line 2240492
    iput-object v5, v4, LX/71A;->a:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    .line 2240493
    move-object v4, v4

    .line 2240494
    iput-object v1, v4, LX/71A;->b:LX/71C;

    .line 2240495
    move-object v4, v4

    .line 2240496
    sget-object v5, LX/6xg;->MOR_NONE:LX/6xg;

    .line 2240497
    iput-object v5, v4, LX/71A;->c:LX/6xg;

    .line 2240498
    move-object v4, v4

    .line 2240499
    iput-object v2, v4, LX/71A;->d:Ljava/lang/String;

    .line 2240500
    move-object v4, v4

    .line 2240501
    invoke-static {}, Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;->newBuilder()LX/FRv;

    move-result-object v5

    .line 2240502
    iput-boolean v6, v5, LX/FRv;->a:Z

    .line 2240503
    move-object v5, v5

    .line 2240504
    iput-boolean v6, v5, LX/FRv;->b:Z

    .line 2240505
    move-object v5, v5

    .line 2240506
    invoke-virtual {v5}, LX/FRv;->c()Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;

    move-result-object v5

    .line 2240507
    iput-object v5, v4, LX/71A;->f:Lcom/facebook/payments/picker/model/PickerScreenFetcherParams;

    .line 2240508
    move-object v4, v4

    .line 2240509
    invoke-virtual {v4}, LX/71A;->h()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v4

    move-object v1, v4

    .line 2240510
    new-instance v2, LX/FRZ;

    invoke-direct {v2}, LX/FRZ;-><init>()V

    move-object v2, v2

    .line 2240511
    iput-object v1, v2, LX/FRZ;->a:Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    .line 2240512
    move-object v1, v2

    .line 2240513
    new-instance v2, Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;

    invoke-direct {v2, v1}, Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;-><init>(LX/FRZ;)V

    move-object v1, v2

    .line 2240514
    iget-object v2, p0, LX/FRX;->b:LX/6xb;

    invoke-virtual {v1}, Lcom/facebook/payments/settings/PaymentSettingsPickerScreenConfig;->a()Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;

    move-result-object v3

    iget-object v3, v3, Lcom/facebook/payments/picker/model/PickerScreenCommonConfig;->b:Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;

    iget-object v3, v3, Lcom/facebook/payments/picker/model/PickerScreenAnalyticsParams;->b:Lcom/facebook/payments/logging/PaymentsLoggingSessionData;

    sget-object v4, LX/6xZ;->PAYMENTS_SETTINGS:LX/6xZ;

    const-string v5, "p2p_payment_general_settings_loaded"

    invoke-virtual {v2, v3, v4, v5}, LX/6xb;->a(Lcom/facebook/payments/logging/PaymentsLoggingSessionData;LX/6xZ;Ljava/lang/String;)V

    .line 2240515
    invoke-static {v0, v1}, Lcom/facebook/payments/picker/PickerScreenActivity;->a(Landroid/content/Intent;Lcom/facebook/payments/picker/model/PickerScreenConfig;)V

    .line 2240516
    return-object v0
.end method
