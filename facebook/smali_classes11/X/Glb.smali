.class public LX/Glb;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public final h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/Gkn;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/GlX;


# direct methods
.method public constructor <init>(LX/GlX;)V
    .locals 1

    .prologue
    .line 2391101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2391102
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Glb;->h:Ljava/util/HashMap;

    .line 2391103
    iput-object p1, p0, LX/Glb;->i:LX/GlX;

    .line 2391104
    invoke-static {p0}, LX/Glb;->a(LX/Glb;)V

    .line 2391105
    return-void
.end method

.method public static a(LX/Glb;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 2391106
    iput v0, p0, LX/Glb;->a:I

    .line 2391107
    iput v0, p0, LX/Glb;->b:I

    .line 2391108
    iput v0, p0, LX/Glb;->c:I

    .line 2391109
    iput v0, p0, LX/Glb;->d:I

    .line 2391110
    iput v0, p0, LX/Glb;->e:I

    .line 2391111
    iput v0, p0, LX/Glb;->f:I

    .line 2391112
    iput v0, p0, LX/Glb;->g:I

    .line 2391113
    iget-object v0, p0, LX/Glb;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 2391114
    return-void
.end method

.method public static a(LX/Gkp;)Z
    .locals 1

    .prologue
    .line 2391115
    invoke-virtual {p0}, LX/Gkp;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/Gkp;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/util/HashMap;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "LX/Gkn;",
            "Lcom/facebook/groups/groupsections/GroupsSectionInterface;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2391116
    sget-object v0, LX/Gkn;->FILTERED_GROUPS_SECTION:LX/Gkn;

    invoke-virtual {p0, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Gkp;

    .line 2391117
    sget-object v1, LX/Gkn;->FAVORITES_SECTION:LX/Gkn;

    invoke-virtual {p0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Gkp;

    .line 2391118
    sget-object v2, LX/Gkn;->RECENTLY_JOINED_SECTION:LX/Gkn;

    invoke-virtual {p0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Gkp;

    .line 2391119
    invoke-static {v1}, LX/Gla;->a(LX/Gkp;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, LX/Gkp;->b()I

    move-result v0

    invoke-virtual {v2}, LX/Gkp;->b()I

    move-result v1

    add-int/2addr v0, v1

    const/16 v1, 0x9

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Enum;)I
    .locals 1

    .prologue
    .line 2391120
    iget-object v0, p0, LX/Glb;->h:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Glb;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2391121
    iget-object v0, p0, LX/Glb;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2391122
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final a(I)LX/Gkn;
    .locals 6
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2391123
    iget-object v0, p0, LX/Glb;->h:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Glb;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v2

    .line 2391124
    :goto_0
    return-object v0

    .line 2391125
    :cond_1
    invoke-static {}, LX/Gkn;->values()[LX/Gkn;

    move-result-object v4

    array-length v5, v4

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v5, :cond_3

    aget-object v1, v4, v3

    .line 2391126
    iget-object v0, p0, LX/Glb;->h:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/Glb;->h:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_2

    move-object v0, v1

    .line 2391127
    goto :goto_0

    .line 2391128
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_3
    move-object v0, v2

    .line 2391129
    goto :goto_0
.end method
