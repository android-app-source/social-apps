.class public LX/FBS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8Vc;


# instance fields
.field private final a:Landroid/net/Uri;

.field private final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field public final d:I


# direct methods
.method public constructor <init>(Landroid/net/Uri;LX/0Px;Ljava/lang/String;I)V
    .locals 0
    .param p1    # Landroid/net/Uri;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "LX/0Px",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 2209377
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2209378
    iput-object p1, p0, LX/FBS;->a:Landroid/net/Uri;

    .line 2209379
    iput-object p2, p0, LX/FBS;->b:LX/0Px;

    .line 2209380
    iput-object p3, p0, LX/FBS;->c:Ljava/lang/String;

    .line 2209381
    iput p4, p0, LX/FBS;->d:I

    .line 2209382
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2209374
    iget-object v0, p0, LX/FBS;->a:Landroid/net/Uri;

    if-eqz v0, :cond_0

    .line 2209375
    const/4 v0, 0x1

    .line 2209376
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, LX/FBS;->b:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    goto :goto_0
.end method

.method public final a(III)LX/1bf;
    .locals 1

    .prologue
    .line 2209368
    invoke-virtual {p0}, LX/FBS;->a()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2209369
    iget-object v0, p0, LX/FBS;->a:Landroid/net/Uri;

    if-eqz v0, :cond_1

    .line 2209370
    iget-object v0, p0, LX/FBS;->a:Landroid/net/Uri;

    invoke-static {v0}, LX/1bf;->a(Landroid/net/Uri;)LX/1bf;

    move-result-object v0

    .line 2209371
    :goto_1
    return-object v0

    .line 2209372
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2209373
    :cond_1
    iget-object v0, p0, LX/FBS;->b:LX/0Px;

    invoke-virtual {v0, p1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v0

    goto :goto_1
.end method

.method public final b(III)LX/1bf;
    .locals 1

    .prologue
    .line 2209367
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()LX/8ue;
    .locals 1

    .prologue
    .line 2209366
    sget-object v0, LX/8ue;->NONE:LX/8ue;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2209361
    iget-object v0, p0, LX/FBS;->a:Landroid/net/Uri;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2209364
    sget-object v0, LX/0Q7;->a:LX/0Px;

    move-object v0, v0

    .line 2209365
    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2209363
    iget-object v0, p0, LX/FBS;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final f()I
    .locals 1
    .annotation build Landroid/support/annotation/ColorInt;
    .end annotation

    .prologue
    .line 2209362
    const/4 v0, 0x0

    return v0
.end method
