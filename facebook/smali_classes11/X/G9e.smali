.class public final enum LX/G9e;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/G9e;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/G9e;

.field public static final enum ALPHANUMERIC:LX/G9e;

.field public static final enum BYTE:LX/G9e;

.field public static final enum ECI:LX/G9e;

.field public static final enum FNC1_FIRST_POSITION:LX/G9e;

.field public static final enum FNC1_SECOND_POSITION:LX/G9e;

.field public static final enum HANZI:LX/G9e;

.field public static final enum KANJI:LX/G9e;

.field public static final enum NUMERIC:LX/G9e;

.field public static final enum STRUCTURED_APPEND:LX/G9e;

.field public static final enum TERMINATOR:LX/G9e;


# instance fields
.field private final bits:I

.field private final characterCountBitsForVersions:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x3

    .line 2323119
    new-instance v0, LX/G9e;

    const-string v1, "TERMINATOR"

    new-array v2, v5, [I

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v6, v2, v6}, LX/G9e;-><init>(Ljava/lang/String;I[II)V

    sput-object v0, LX/G9e;->TERMINATOR:LX/G9e;

    .line 2323120
    new-instance v0, LX/G9e;

    const-string v1, "NUMERIC"

    new-array v2, v5, [I

    fill-array-data v2, :array_1

    invoke-direct {v0, v1, v7, v2, v7}, LX/G9e;-><init>(Ljava/lang/String;I[II)V

    sput-object v0, LX/G9e;->NUMERIC:LX/G9e;

    .line 2323121
    new-instance v0, LX/G9e;

    const-string v1, "ALPHANUMERIC"

    new-array v2, v5, [I

    fill-array-data v2, :array_2

    invoke-direct {v0, v1, v8, v2, v8}, LX/G9e;-><init>(Ljava/lang/String;I[II)V

    sput-object v0, LX/G9e;->ALPHANUMERIC:LX/G9e;

    .line 2323122
    new-instance v0, LX/G9e;

    const-string v1, "STRUCTURED_APPEND"

    new-array v2, v5, [I

    fill-array-data v2, :array_3

    invoke-direct {v0, v1, v5, v2, v5}, LX/G9e;-><init>(Ljava/lang/String;I[II)V

    sput-object v0, LX/G9e;->STRUCTURED_APPEND:LX/G9e;

    .line 2323123
    new-instance v0, LX/G9e;

    const-string v1, "BYTE"

    new-array v2, v5, [I

    fill-array-data v2, :array_4

    invoke-direct {v0, v1, v9, v2, v9}, LX/G9e;-><init>(Ljava/lang/String;I[II)V

    sput-object v0, LX/G9e;->BYTE:LX/G9e;

    .line 2323124
    new-instance v0, LX/G9e;

    const-string v1, "ECI"

    const/4 v2, 0x5

    new-array v3, v5, [I

    fill-array-data v3, :array_5

    const/4 v4, 0x7

    invoke-direct {v0, v1, v2, v3, v4}, LX/G9e;-><init>(Ljava/lang/String;I[II)V

    sput-object v0, LX/G9e;->ECI:LX/G9e;

    .line 2323125
    new-instance v0, LX/G9e;

    const-string v1, "KANJI"

    const/4 v2, 0x6

    new-array v3, v5, [I

    fill-array-data v3, :array_6

    const/16 v4, 0x8

    invoke-direct {v0, v1, v2, v3, v4}, LX/G9e;-><init>(Ljava/lang/String;I[II)V

    sput-object v0, LX/G9e;->KANJI:LX/G9e;

    .line 2323126
    new-instance v0, LX/G9e;

    const-string v1, "FNC1_FIRST_POSITION"

    const/4 v2, 0x7

    new-array v3, v5, [I

    fill-array-data v3, :array_7

    const/4 v4, 0x5

    invoke-direct {v0, v1, v2, v3, v4}, LX/G9e;-><init>(Ljava/lang/String;I[II)V

    sput-object v0, LX/G9e;->FNC1_FIRST_POSITION:LX/G9e;

    .line 2323127
    new-instance v0, LX/G9e;

    const-string v1, "FNC1_SECOND_POSITION"

    const/16 v2, 0x8

    new-array v3, v5, [I

    fill-array-data v3, :array_8

    const/16 v4, 0x9

    invoke-direct {v0, v1, v2, v3, v4}, LX/G9e;-><init>(Ljava/lang/String;I[II)V

    sput-object v0, LX/G9e;->FNC1_SECOND_POSITION:LX/G9e;

    .line 2323128
    new-instance v0, LX/G9e;

    const-string v1, "HANZI"

    const/16 v2, 0x9

    new-array v3, v5, [I

    fill-array-data v3, :array_9

    const/16 v4, 0xd

    invoke-direct {v0, v1, v2, v3, v4}, LX/G9e;-><init>(Ljava/lang/String;I[II)V

    sput-object v0, LX/G9e;->HANZI:LX/G9e;

    .line 2323129
    const/16 v0, 0xa

    new-array v0, v0, [LX/G9e;

    sget-object v1, LX/G9e;->TERMINATOR:LX/G9e;

    aput-object v1, v0, v6

    sget-object v1, LX/G9e;->NUMERIC:LX/G9e;

    aput-object v1, v0, v7

    sget-object v1, LX/G9e;->ALPHANUMERIC:LX/G9e;

    aput-object v1, v0, v8

    sget-object v1, LX/G9e;->STRUCTURED_APPEND:LX/G9e;

    aput-object v1, v0, v5

    sget-object v1, LX/G9e;->BYTE:LX/G9e;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/G9e;->ECI:LX/G9e;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/G9e;->KANJI:LX/G9e;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/G9e;->FNC1_FIRST_POSITION:LX/G9e;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/G9e;->FNC1_SECOND_POSITION:LX/G9e;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/G9e;->HANZI:LX/G9e;

    aput-object v2, v0, v1

    sput-object v0, LX/G9e;->$VALUES:[LX/G9e;

    return-void

    .line 2323130
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data

    .line 2323131
    :array_1
    .array-data 4
        0xa
        0xc
        0xe
    .end array-data

    .line 2323132
    :array_2
    .array-data 4
        0x9
        0xb
        0xd
    .end array-data

    .line 2323133
    :array_3
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data

    .line 2323134
    :array_4
    .array-data 4
        0x8
        0x10
        0x10
    .end array-data

    .line 2323135
    :array_5
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data

    .line 2323136
    :array_6
    .array-data 4
        0x8
        0xa
        0xc
    .end array-data

    .line 2323137
    :array_7
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data

    .line 2323138
    :array_8
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data

    .line 2323139
    :array_9
    .array-data 4
        0x8
        0xa
        0xc
    .end array-data
.end method

.method private constructor <init>(Ljava/lang/String;I[II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([II)V"
        }
    .end annotation

    .prologue
    .line 2323093
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2323094
    iput-object p3, p0, LX/G9e;->characterCountBitsForVersions:[I

    .line 2323095
    iput p4, p0, LX/G9e;->bits:I

    .line 2323096
    return-void
.end method

.method public static forBits(I)LX/G9e;
    .locals 1

    .prologue
    .line 2323106
    packed-switch p0, :pswitch_data_0

    .line 2323107
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2323108
    :pswitch_1
    sget-object v0, LX/G9e;->TERMINATOR:LX/G9e;

    .line 2323109
    :goto_0
    return-object v0

    .line 2323110
    :pswitch_2
    sget-object v0, LX/G9e;->NUMERIC:LX/G9e;

    goto :goto_0

    .line 2323111
    :pswitch_3
    sget-object v0, LX/G9e;->ALPHANUMERIC:LX/G9e;

    goto :goto_0

    .line 2323112
    :pswitch_4
    sget-object v0, LX/G9e;->STRUCTURED_APPEND:LX/G9e;

    goto :goto_0

    .line 2323113
    :pswitch_5
    sget-object v0, LX/G9e;->BYTE:LX/G9e;

    goto :goto_0

    .line 2323114
    :pswitch_6
    sget-object v0, LX/G9e;->FNC1_FIRST_POSITION:LX/G9e;

    goto :goto_0

    .line 2323115
    :pswitch_7
    sget-object v0, LX/G9e;->ECI:LX/G9e;

    goto :goto_0

    .line 2323116
    :pswitch_8
    sget-object v0, LX/G9e;->KANJI:LX/G9e;

    goto :goto_0

    .line 2323117
    :pswitch_9
    sget-object v0, LX/G9e;->FNC1_SECOND_POSITION:LX/G9e;

    goto :goto_0

    .line 2323118
    :pswitch_a
    sget-object v0, LX/G9e;->HANZI:LX/G9e;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)LX/G9e;
    .locals 1

    .prologue
    .line 2323140
    const-class v0, LX/G9e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/G9e;

    return-object v0
.end method

.method public static values()[LX/G9e;
    .locals 1

    .prologue
    .line 2323105
    sget-object v0, LX/G9e;->$VALUES:[LX/G9e;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/G9e;

    return-object v0
.end method


# virtual methods
.method public final getBits()I
    .locals 1

    .prologue
    .line 2323104
    iget v0, p0, LX/G9e;->bits:I

    return v0
.end method

.method public final getCharacterCountBits(LX/G9i;)I
    .locals 2

    .prologue
    .line 2323097
    iget v0, p1, LX/G9i;->c:I

    move v0, v0

    .line 2323098
    const/16 v1, 0x9

    if-gt v0, v1, :cond_0

    .line 2323099
    const/4 v0, 0x0

    .line 2323100
    :goto_0
    iget-object v1, p0, LX/G9e;->characterCountBitsForVersions:[I

    aget v0, v1, v0

    return v0

    .line 2323101
    :cond_0
    const/16 v1, 0x1a

    if-gt v0, v1, :cond_1

    .line 2323102
    const/4 v0, 0x1

    goto :goto_0

    .line 2323103
    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method
