.class public final LX/Fto;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;

.field public final synthetic b:Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;)V
    .locals 0

    .prologue
    .line 2299174
    iput-object p1, p0, LX/Fto;->b:Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;

    iput-object p2, p0, LX/Fto;->a:Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v0, 0x1

    const v1, 0x4cccb8d1    # 1.07333256E8f

    invoke-static {v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v2

    .line 2299175
    iget-object v0, p0, LX/Fto;->b:Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;

    iget-object v0, v0, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, LX/Fto;->b:Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;

    iget-object v3, v3, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;->f:Ljava/lang/String;

    iget-object v4, p0, LX/Fto;->a:Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;

    invoke-virtual {v4}, Lcom/facebook/timeline/favmediapicker/protocol/FetchFavoriteMediaPickerSuggestionsModels$SuggestedMediasetModel;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f083408

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/facebook/timeline/favmediapicker/rows/parts/SuggestedMediasetSectionPartDefinition;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-static {v1, v3, v4, v5, v6}, Lcom/facebook/photos/pandora/ui/mediaset/PandoraMediaSetActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/common/callercontext/CallerContext;)Landroid/content/Intent;

    move-result-object v3

    const/4 v4, 0x3

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v5, Lcom/facebook/timeline/favmediapicker/ui/FavoriteMediaPickerActivity;

    invoke-static {v1, v5}, LX/0WH;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-interface {v0, v3, v4, v1}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;ILandroid/app/Activity;)V

    .line 2299176
    const v0, -0x55a047b7

    invoke-static {v7, v7, v0, v2}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
