.class public final LX/GtR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/InternSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/InternSettingsActivity;)V
    .locals 0

    .prologue
    .line 2400556
    iput-object p1, p0, LX/GtR;->a:Lcom/facebook/katana/InternSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2400557
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 2400558
    if-nez v0, :cond_1

    move v0, v1

    .line 2400559
    :goto_0
    sget-object v2, LX/00L;->mReportSender:LX/00M;

    .line 2400560
    iput-boolean v0, v2, LX/00M;->mSkipSslCertChecks:Z

    .line 2400561
    :try_start_0
    sget-object v2, LX/00L;->mConfig:LX/00K;

    .line 2400562
    iget-object p0, v2, LX/00K;->mApplicationContext:Landroid/content/Context;

    move-object v2, p0

    .line 2400563
    const-string p0, "skip_cert_checks.txt"

    invoke-virtual {v2, p0}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 2400564
    if-eqz v0, :cond_2

    .line 2400565
    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    .line 2400566
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result p0

    if-nez p0, :cond_0

    .line 2400567
    sget-object p0, LX/00L;->LOG_TAG:Ljava/lang/String;

    new-instance p1, Ljava/lang/StringBuilder;

    const-string p2, "Failed to create skip cert checks file: "

    invoke-direct {p1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2400568
    :cond_0
    :goto_1
    return v1

    .line 2400569
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2400570
    :cond_2
    :try_start_1
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 2400571
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result p0

    if-eqz p0, :cond_0

    .line 2400572
    sget-object p0, LX/00L;->LOG_TAG:Ljava/lang/String;

    new-instance p1, Ljava/lang/StringBuilder;

    const-string p2, "Failed to delete skip cert checks file: "

    invoke-direct {p1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 2400573
    :catch_0
    move-exception v2

    .line 2400574
    sget-object p0, LX/00L;->LOG_TAG:Ljava/lang/String;

    const-string p1, "could not create ssl cert checks file."

    invoke-static {p0, p1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
