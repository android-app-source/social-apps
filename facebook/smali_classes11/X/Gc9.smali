.class public final LX/Gc9;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field public final synthetic a:I

.field public final synthetic b:Landroid/view/animation/TranslateAnimation;

.field public final synthetic c:Landroid/view/animation/TranslateAnimation;

.field public final synthetic d:I

.field public final synthetic e:[I

.field public final synthetic f:Landroid/view/View;

.field public final synthetic g:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;ILandroid/view/animation/TranslateAnimation;Landroid/view/animation/TranslateAnimation;I[ILandroid/view/View;)V
    .locals 0

    .prologue
    .line 2371076
    iput-object p1, p0, LX/Gc9;->g:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    iput p2, p0, LX/Gc9;->a:I

    iput-object p3, p0, LX/Gc9;->b:Landroid/view/animation/TranslateAnimation;

    iput-object p4, p0, LX/Gc9;->c:Landroid/view/animation/TranslateAnimation;

    iput p5, p0, LX/Gc9;->d:I

    iput-object p6, p0, LX/Gc9;->e:[I

    iput-object p7, p0, LX/Gc9;->f:Landroid/view/View;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2371064
    move v0, v1

    :goto_0
    iget v2, p0, LX/Gc9;->a:I

    if-ge v0, v2, :cond_0

    .line 2371065
    iget-object v2, p0, LX/Gc9;->g:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    iget-object v2, v2, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->i:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget-object v5, p0, LX/Gc9;->b:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v2, v5}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2371066
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2371067
    :cond_0
    iget v0, p0, LX/Gc9;->a:I

    add-int/lit8 v0, v0, 0x1

    :goto_1
    iget-object v2, p0, LX/Gc9;->g:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    iget-object v2, v2, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->a:LX/Gbz;

    invoke-virtual {v2}, LX/Gbz;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 2371068
    iget-object v2, p0, LX/Gc9;->g:Lcom/facebook/devicebasedlogin/ui/DBLAccountsListHorizontalFragment;

    iget-object v2, v2, Lcom/facebook/devicebasedlogin/ui/DBLAccountsListFragment;->i:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget-object v5, p0, LX/Gc9;->c:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v2, v5}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2371069
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2371070
    :cond_1
    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget v2, p0, LX/Gc9;->d:I

    iget-object v5, p0, LX/Gc9;->e:[I

    aget v5, v5, v1

    sub-int/2addr v2, v5

    int-to-float v2, v2

    move v5, v3

    move v6, v4

    move v7, v3

    move v8, v4

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    .line 2371071
    const-wide/16 v4, 0x1f4

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 2371072
    invoke-virtual {v0}, Landroid/view/animation/TranslateAnimation;->reset()V

    .line 2371073
    invoke-virtual {v0, v3}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 2371074
    iget-object v1, p0, LX/Gc9;->f:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 2371075
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 2371062
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 2371063
    return-void
.end method
