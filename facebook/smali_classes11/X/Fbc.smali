.class public LX/Fbc;
.super LX/FbY;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FbY",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsSalePostUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Fbc;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2260612
    invoke-direct {p0}, LX/FbY;-><init>()V

    .line 2260613
    return-void
.end method

.method public static a(LX/0QB;)LX/Fbc;
    .locals 3

    .prologue
    .line 2260614
    sget-object v0, LX/Fbc;->a:LX/Fbc;

    if-nez v0, :cond_1

    .line 2260615
    const-class v1, LX/Fbc;

    monitor-enter v1

    .line 2260616
    :try_start_0
    sget-object v0, LX/Fbc;->a:LX/Fbc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2260617
    if-eqz v2, :cond_0

    .line 2260618
    :try_start_1
    new-instance v0, LX/Fbc;

    invoke-direct {v0}, LX/Fbc;-><init>()V

    .line 2260619
    move-object v0, v0

    .line 2260620
    sput-object v0, LX/Fbc;->a:LX/Fbc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2260621
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2260622
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2260623
    :cond_1
    sget-object v0, LX/Fbc;->a:LX/Fbc;

    return-object v0

    .line 2260624
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2260625
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
    .locals 3
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260626
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->fM_()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->fM_()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    move-object v1, v0

    .line 2260627
    :goto_0
    if-nez v1, :cond_1

    .line 2260628
    const/4 v0, 0x0

    .line 2260629
    :goto_1
    return-object v0

    .line 2260630
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->b()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 2260631
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;->fN_()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;

    move-result-object v0

    invoke-static {v0}, LX/A0T;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchResultDecorationModel;)Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;

    move-result-object v0

    .line 2260632
    if-eqz v0, :cond_2

    .line 2260633
    invoke-static {v1, v0}, LX/0x1;->a(Lcom/facebook/graphql/model/GraphQLStory;Lcom/facebook/graphql/model/GraphQLGraphSearchResultDecoration;)V

    .line 2260634
    :cond_2
    new-instance v0, Lcom/facebook/search/results/model/unit/SearchResultsSalePostUnit;

    invoke-virtual {p2}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->e()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/facebook/search/results/model/unit/SearchResultsSalePostUnit;-><init>(Lcom/facebook/graphql/model/GraphQLStory;Ljava/lang/String;)V

    goto :goto_1
.end method
