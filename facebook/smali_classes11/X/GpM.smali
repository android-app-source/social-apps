.class public LX/GpM;
.super LX/Got;
.source ""

# interfaces
.implements LX/Goc;
.implements LX/God;
.implements LX/Gon;
.implements LX/Gof;
.implements LX/Gog;
.implements LX/Goj;
.implements LX/Clv;
.implements LX/Cm4;


# instance fields
.field private final a:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;

.field public final b:Z


# direct methods
.method public constructor <init>(LX/GpL;)V
    .locals 1

    .prologue
    .line 2394892
    invoke-direct {p0, p1}, LX/Got;-><init>(LX/Gos;)V

    .line 2394893
    iget-object v0, p1, LX/GpL;->a:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;

    iput-object v0, p0, LX/GpM;->a:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;

    .line 2394894
    iget-boolean v0, p1, LX/GpL;->b:Z

    iput-boolean v0, p0, LX/GpM;->b:Z

    .line 2394895
    return-void
.end method


# virtual methods
.method public final d()Lcom/facebook/graphql/enums/GraphQLDocumentElementType;
    .locals 1

    .prologue
    .line 2394896
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLDocumentElementType;->VIDEO:Lcom/facebook/graphql/enums/GraphQLDocumentElementType;

    return-object v0
.end method

.method public final iY_()Z
    .locals 1

    .prologue
    .line 2394890
    const/4 v0, 0x0

    return v0
.end method

.method public final r()LX/8Ys;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394891
    iget-object v0, p0, LX/GpM;->a:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->N()LX/8Ys;

    move-result-object v0

    return-object v0
.end method

.method public final s()LX/8Yr;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394889
    const/4 v0, 0x0

    return-object v0
.end method

.method public final t()Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394888
    iget-object v0, p0, LX/GpM;->a:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->K()Lcom/facebook/graphql/enums/GraphQLDocumentVideoAutoplayStyle;

    move-result-object v0

    return-object v0
.end method

.method public final u()Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394887
    iget-object v0, p0, LX/GpM;->a:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->L()Lcom/facebook/graphql/enums/GraphQLDocumentVideoControlStyle;

    move-result-object v0

    return-object v0
.end method

.method public final v()Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2394886
    iget-object v0, p0, LX/GpM;->a:Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$ShoppingDocumentElementsEdgeModel$NodeModel;->O()Lcom/facebook/graphql/enums/GraphQLDocumentVideoLoopingStyle;

    move-result-object v0

    return-object v0
.end method
