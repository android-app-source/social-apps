.class public LX/GTc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/13F;
.implements LX/13G;
.implements LX/6Xx;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static final a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile j:LX/GTc;


# instance fields
.field public final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Uh;

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0y3;",
            ">;"
        }
    .end annotation
.end field

.field private final f:LX/GTb;

.field private final g:LX/0ad;

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/NuxStep;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2355703
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SESSION_COLD_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/GTc;->a:LX/0Px;

    .line 2355704
    new-instance v0, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v1, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->SESSION_COLD_START:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v0, v1}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    new-instance v1, Lcom/facebook/interstitial/manager/InterstitialTrigger;

    sget-object v2, Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;->APP_FOREGROUND:Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;

    invoke-direct {v1, v2}, Lcom/facebook/interstitial/manager/InterstitialTrigger;-><init>(Lcom/facebook/interstitial/manager/InterstitialTrigger$Action;)V

    invoke-static {v0, v1}, LX/0Px;->of(Ljava/lang/Object;Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    sput-object v0, LX/GTc;->b:LX/0Px;

    return-void
.end method

.method public constructor <init>(LX/0Or;LX/0Uh;LX/0Ot;LX/GTb;LX/0ad;)V
    .locals 1
    .param p1    # LX/0Or;
        .annotation runtime Lcom/facebook/backgroundlocation/nux/IsBackgroundLocationNuxAvailable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "Ljava/lang/Boolean;",
            ">;",
            "Lcom/facebook/gk/store/GatekeeperStore;",
            "LX/0Ot",
            "<",
            "LX/0y3;",
            ">;",
            "LX/GTb;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2355695
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2355696
    const-string v0, "nearby_friends_undecided"

    iput-object v0, p0, LX/GTc;->i:Ljava/lang/String;

    .line 2355697
    iput-object p1, p0, LX/GTc;->c:LX/0Or;

    .line 2355698
    iput-object p2, p0, LX/GTc;->d:LX/0Uh;

    .line 2355699
    iput-object p3, p0, LX/GTc;->e:LX/0Ot;

    .line 2355700
    iput-object p4, p0, LX/GTc;->f:LX/GTb;

    .line 2355701
    iput-object p5, p0, LX/GTc;->g:LX/0ad;

    .line 2355702
    return-void
.end method

.method public static a(LX/0QB;)LX/GTc;
    .locals 9

    .prologue
    .line 2355679
    sget-object v0, LX/GTc;->j:LX/GTc;

    if-nez v0, :cond_1

    .line 2355680
    const-class v1, LX/GTc;

    monitor-enter v1

    .line 2355681
    :try_start_0
    sget-object v0, LX/GTc;->j:LX/GTc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2355682
    if-eqz v2, :cond_0

    .line 2355683
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2355684
    new-instance v3, LX/GTc;

    const/16 v4, 0x1457

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    invoke-static {v0}, LX/0US;->a(LX/0QB;)LX/0Uh;

    move-result-object v5

    check-cast v5, LX/0Uh;

    const/16 v6, 0xc83

    invoke-static {v0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    .line 2355685
    new-instance v8, LX/GTb;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v7

    check-cast v7, LX/0Zb;

    invoke-direct {v8, v7}, LX/GTb;-><init>(LX/0Zb;)V

    .line 2355686
    move-object v7, v8

    .line 2355687
    check-cast v7, LX/GTb;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v3 .. v8}, LX/GTc;-><init>(LX/0Or;LX/0Uh;LX/0Ot;LX/GTb;LX/0ad;)V

    .line 2355688
    move-object v0, v3

    .line 2355689
    sput-object v0, LX/GTc;->j:LX/GTc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2355690
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2355691
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2355692
    :cond_1
    sget-object v0, LX/GTc;->j:LX/GTc;

    return-object v0

    .line 2355693
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2355694
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 2355678
    const-wide/32 v0, 0x240c8400

    return-wide v0
.end method

.method public final a(ILandroid/content/Intent;)LX/0am;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/content/Intent;",
            ")",
            "LX/0am",
            "<",
            "Landroid/content/Intent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2355675
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    if-eqz p2, :cond_0

    .line 2355676
    invoke-static {p2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 2355677
    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/interstitial/manager/InterstitialTrigger;)LX/10S;
    .locals 8

    .prologue
    .line 2355636
    iget-object v0, p0, LX/GTc;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2355637
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    sget-object v1, LX/GTa;->NOT_ALLOWED_IN_APP:LX/GTa;

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 2355638
    :goto_0
    move-object v3, v0

    .line 2355639
    iget-object v4, p0, LX/GTc;->f:LX/GTb;

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, LX/10S;

    iget-object v1, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, LX/GTa;

    iget-object v2, p0, LX/GTc;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0y3;

    invoke-virtual {v2}, LX/0y3;->b()LX/1rv;

    move-result-object v2

    iget-object v5, p0, LX/GTc;->h:Ljava/util/List;

    .line 2355640
    iget-object v6, v4, LX/GTb;->a:LX/0Zb;

    const-string v7, "background_location_nux_interstitial_eligibility_state"

    .line 2355641
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    invoke-direct {p0, v7}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    const-string p1, "background_location"

    .line 2355642
    iput-object p1, p0, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2355643
    move-object p0, p0

    .line 2355644
    move-object v7, p0

    .line 2355645
    const-string p0, "state"

    invoke-virtual {v7, p0, v0}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "reason"

    invoke-virtual {v7, p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "location_state"

    iget-object p1, v2, LX/1rv;->a:LX/0yG;

    invoke-virtual {v7, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    const-string p0, "nux_steps"

    .line 2355646
    if-nez v5, :cond_8

    .line 2355647
    const/4 p1, 0x0

    .line 2355648
    :goto_1
    move-object p1, p1

    .line 2355649
    invoke-virtual {v7, p0, p1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->a(Ljava/lang/String;LX/0lF;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v7

    invoke-interface {v6, v7}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2355650
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, LX/10S;

    return-object v0

    .line 2355651
    :cond_0
    iget-object v0, p0, LX/GTc;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0y3;

    invoke-virtual {v0}, LX/0y3;->a()LX/0yG;

    move-result-object v0

    .line 2355652
    iget-object v1, p0, LX/GTc;->d:LX/0Uh;

    const/16 v2, 0x57f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2355653
    invoke-static {v0}, LX/1rv;->a(LX/0yG;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2355654
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    sget-object v1, LX/GTa;->LOCATION_UNAVAILABLE:LX/GTa;

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    .line 2355655
    :cond_1
    sget-object v1, LX/0yG;->OKAY:LX/0yG;

    if-eq v0, v1, :cond_2

    .line 2355656
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    sget-object v1, LX/GTa;->LOCATION_UNAVAILABLE:LX/GTa;

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    .line 2355657
    :cond_2
    iget-object v0, p0, LX/GTc;->h:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, LX/GTc;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2355658
    :cond_3
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    sget-object v1, LX/GTa;->EMPTY_SERVER_DATA:LX/GTa;

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_0

    .line 2355659
    :cond_4
    iget-object v0, p0, LX/GTc;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ipc/model/NuxStep;

    .line 2355660
    invoke-virtual {v0}, Lcom/facebook/ipc/model/NuxStep;->b()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2355661
    iget-object v1, v0, Lcom/facebook/ipc/model/NuxStep;->name:Ljava/lang/String;

    move-object v0, v1

    .line 2355662
    invoke-static {v0}, LX/GTf;->fromStepName(Ljava/lang/String;)LX/GTf;

    move-result-object v0

    .line 2355663
    :goto_2
    move-object v0, v0

    .line 2355664
    sget-object v1, LX/GTf;->SHARING:LX/GTf;

    if-eq v0, v1, :cond_6

    .line 2355665
    sget-object v0, LX/10S;->INELIGIBLE:LX/10S;

    sget-object v1, LX/GTa;->WRONG_NUX_STEP:LX/GTa;

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_0

    .line 2355666
    :cond_6
    sget-object v0, LX/10S;->ELIGIBLE:LX/10S;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto/16 :goto_0

    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    .line 2355667
    :cond_8
    sget-object p1, LX/0mC;->a:LX/0mC;

    invoke-virtual {p1}, LX/0mC;->b()LX/162;

    move-result-object v4

    .line 2355668
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_9

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/facebook/ipc/model/NuxStep;

    .line 2355669
    new-instance v1, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 2355670
    const-string v2, "name"

    iget-object v5, p1, Lcom/facebook/ipc/model/NuxStep;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v5}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2355671
    const-string v2, "isCurrent"

    iget v5, p1, Lcom/facebook/ipc/model/NuxStep;->isCurrent:I

    invoke-virtual {v1, v2, v5}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    .line 2355672
    move-object p1, v1

    .line 2355673
    invoke-virtual {v4, p1}, LX/162;->a(LX/0lF;)LX/162;

    goto :goto_3

    :cond_9
    move-object p1, v4

    .line 2355674
    goto/16 :goto_1
.end method

.method public final a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 2355705
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/facebook/backgroundlocation/nux/BackgroundLocationOnePageNuxActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2355706
    const-string v1, "source"

    const-string v2, "cold_start"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2355707
    const-string v1, "nux_type"

    iget-object v2, p0, LX/GTc;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2355708
    return-object v0
.end method

.method public final a(J)V
    .locals 0

    .prologue
    .line 2355635
    return-void
.end method

.method public final a(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 2355624
    check-cast p1, Lcom/facebook/interstitial/api/InterstitialNUXFetchResult;

    .line 2355625
    iget-object v0, p1, Lcom/facebook/interstitial/api/InterstitialNUXFetchResult;->steps:Ljava/util/List;

    iput-object v0, p0, LX/GTc;->h:Ljava/util/List;

    .line 2355626
    iget-object v0, p1, Lcom/facebook/interstitial/api/InterstitialNUXFetchResult;->extraData:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/interstitial/api/InterstitialNUXFetchResult;->extraData:Ljava/util/Map;

    const-string v1, "nux_type"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/facebook/interstitial/api/InterstitialNUXFetchResult;->extraData:Ljava/util/Map;

    const-string v1, "nux_type"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    iput-object v0, p0, LX/GTc;->i:Ljava/lang/String;

    .line 2355627
    return-void

    .line 2355628
    :cond_0
    const-string v0, "nearby_friends_undecided"

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2355634
    const-string v0, "1631"

    return-object v0
.end method

.method public final c()LX/0Px;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/interstitial/manager/InterstitialTrigger;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2355631
    iget-object v0, p0, LX/GTc;->d:LX/0Uh;

    const/16 v1, 0x2ec

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/0Uh;->a(IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2355632
    sget-object v0, LX/GTc;->b:LX/0Px;

    .line 2355633
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/GTc;->a:LX/0Px;

    goto :goto_0
.end method

.method public final d()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<+",
            "Landroid/os/Parcelable;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2355630
    const-class v0, Lcom/facebook/interstitial/api/InterstitialNUXFetchResult;

    return-object v0
.end method

.method public final e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/facebook/ipc/model/NuxStep;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2355629
    iget-object v0, p0, LX/GTc;->h:Ljava/util/List;

    return-object v0
.end method
