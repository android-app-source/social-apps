.class public final enum LX/Foo;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Foo;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Foo;

.field public static final enum CATEGORY:LX/Foo;

.field public static final enum CHARITY:LX/Foo;

.field public static final enum DAF_DISCLOSURE:LX/Foo;

.field public static final enum LOADER:LX/Foo;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2290149
    new-instance v0, LX/Foo;

    const-string v1, "LOADER"

    invoke-direct {v0, v1, v2}, LX/Foo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Foo;->LOADER:LX/Foo;

    .line 2290150
    new-instance v0, LX/Foo;

    const-string v1, "CHARITY"

    invoke-direct {v0, v1, v3}, LX/Foo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Foo;->CHARITY:LX/Foo;

    .line 2290151
    new-instance v0, LX/Foo;

    const-string v1, "CATEGORY"

    invoke-direct {v0, v1, v4}, LX/Foo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Foo;->CATEGORY:LX/Foo;

    .line 2290152
    new-instance v0, LX/Foo;

    const-string v1, "DAF_DISCLOSURE"

    invoke-direct {v0, v1, v5}, LX/Foo;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Foo;->DAF_DISCLOSURE:LX/Foo;

    .line 2290153
    const/4 v0, 0x4

    new-array v0, v0, [LX/Foo;

    sget-object v1, LX/Foo;->LOADER:LX/Foo;

    aput-object v1, v0, v2

    sget-object v1, LX/Foo;->CHARITY:LX/Foo;

    aput-object v1, v0, v3

    sget-object v1, LX/Foo;->CATEGORY:LX/Foo;

    aput-object v1, v0, v4

    sget-object v1, LX/Foo;->DAF_DISCLOSURE:LX/Foo;

    aput-object v1, v0, v5

    sput-object v0, LX/Foo;->$VALUES:[LX/Foo;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2290154
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Foo;
    .locals 1

    .prologue
    .line 2290155
    const-class v0, LX/Foo;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Foo;

    return-object v0
.end method

.method public static values()[LX/Foo;
    .locals 1

    .prologue
    .line 2290156
    sget-object v0, LX/Foo;->$VALUES:[LX/Foo;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Foo;

    return-object v0
.end method
