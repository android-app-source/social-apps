.class public LX/H0I;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/0tX;

.field private b:LX/1Ck;


# direct methods
.method private constructor <init>(LX/0tX;LX/1Ck;)V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2413768
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2413769
    iput-object p1, p0, LX/H0I;->a:LX/0tX;

    .line 2413770
    iput-object p2, p0, LX/H0I;->b:LX/1Ck;

    .line 2413771
    return-void
.end method

.method public static b(LX/0QB;)LX/H0I;
    .locals 3

    .prologue
    .line 2413772
    new-instance v2, LX/H0I;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-direct {v2, v0, v1}, LX/H0I;-><init>(LX/0tX;LX/1Ck;)V

    .line 2413773
    return-object v2
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/professionalservices/getquote/model/FormData;LX/Gzi;)V
    .locals 8
    .param p5    # LX/Gzi;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2413774
    iget-object v6, p0, LX/H0I;->b:LX/1Ck;

    const-string v7, "getquote_create_form"

    new-instance v0, LX/H0G;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, LX/H0G;-><init>(LX/H0I;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/facebook/messaging/professionalservices/getquote/model/FormData;)V

    new-instance v1, LX/H0H;

    invoke-direct {v1, p0, p5}, LX/H0H;-><init>(LX/H0I;LX/Gzi;)V

    invoke-virtual {v6, v7, v0, v1}, LX/1Ck;->a(Ljava/lang/Object;Ljava/util/concurrent/Callable;LX/0Ve;)Z

    .line 2413775
    return-void
.end method
