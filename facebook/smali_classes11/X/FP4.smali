.class public LX/FP4;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/location/Location;

.field public b:Landroid/location/Location;

.field public c:Ljava/lang/String;

.field public d:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

.field public e:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

.field private f:F

.field public g:Z

.field public h:Ljava/lang/String;

.field public i:LX/FPn;

.field public j:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

.field public k:Z


# direct methods
.method public constructor <init>(Landroid/location/Location;Landroid/location/Location;Ljava/lang/String;Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;LX/FPn;Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;FZLjava/lang/String;Z)V
    .locals 0

    .prologue
    .line 2236630
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2236631
    iput-object p1, p0, LX/FP4;->a:Landroid/location/Location;

    .line 2236632
    iput-object p2, p0, LX/FP4;->b:Landroid/location/Location;

    .line 2236633
    iput-object p3, p0, LX/FP4;->c:Ljava/lang/String;

    .line 2236634
    iput-object p4, p0, LX/FP4;->d:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    .line 2236635
    iput-object p5, p0, LX/FP4;->e:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    .line 2236636
    iput-object p6, p0, LX/FP4;->i:LX/FPn;

    .line 2236637
    iput-object p7, p0, LX/FP4;->j:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    .line 2236638
    iput p8, p0, LX/FP4;->f:F

    .line 2236639
    iput-boolean p9, p0, LX/FP4;->g:Z

    .line 2236640
    iput-object p10, p0, LX/FP4;->h:Ljava/lang/String;

    .line 2236641
    iput-boolean p11, p0, LX/FP4;->k:Z

    .line 2236642
    return-void
.end method


# virtual methods
.method public final a()Landroid/location/Location;
    .locals 1

    .prologue
    .line 2236629
    iget-object v0, p0, LX/FP4;->a:Landroid/location/Location;

    return-object v0
.end method

.method public final b()Landroid/location/Location;
    .locals 1

    .prologue
    .line 2236628
    iget-object v0, p0, LX/FP4;->b:Landroid/location/Location;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2236627
    iget-object v0, p0, LX/FP4;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;
    .locals 1

    .prologue
    .line 2236626
    iget-object v0, p0, LX/FP4;->d:Lcom/facebook/nearby/v2/network/BrowseNearbyPlacesGraphQLModels$GeoRectangleFragmentModel;

    return-object v0
.end method

.method public final e()Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;
    .locals 1

    .prologue
    .line 2236625
    iget-object v0, p0, LX/FP4;->e:Lcom/facebook/nearby/v2/model/NearbyPlacesResultListQueryTopic;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2236622
    iget-object v0, p0, LX/FP4;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final i()LX/FPn;
    .locals 1

    .prologue
    .line 2236624
    iget-object v0, p0, LX/FP4;->i:LX/FPn;

    return-object v0
.end method

.method public final j()Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;
    .locals 1

    .prologue
    .line 2236623
    iget-object v0, p0, LX/FP4;->j:Lcom/facebook/nearby/v2/resultlist/model/NearbyPlacesResultListFilterSet;

    return-object v0
.end method
