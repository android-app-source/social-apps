.class public LX/FSc;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0c5;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static volatile h:LX/FSc;


# instance fields
.field private final b:LX/0ad;

.field private final c:LX/0TD;

.field public final d:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final e:LX/0qX;

.field public final f:LX/FSa;

.field private final g:LX/FSZ;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2242297
    const-class v0, LX/FSc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/FSc;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0ad;LX/0TD;LX/0qX;LX/FSa;LX/FSZ;)V
    .locals 2
    .param p2    # LX/0TD;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2242298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2242299
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, LX/FSc;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 2242300
    iput-object p1, p0, LX/FSc;->b:LX/0ad;

    .line 2242301
    iput-object p2, p0, LX/FSc;->c:LX/0TD;

    .line 2242302
    iput-object p3, p0, LX/FSc;->e:LX/0qX;

    .line 2242303
    iput-object p4, p0, LX/FSc;->f:LX/FSa;

    .line 2242304
    iput-object p5, p0, LX/FSc;->g:LX/FSZ;

    .line 2242305
    return-void
.end method

.method public static a(LX/0QB;)LX/FSc;
    .locals 9

    .prologue
    .line 2242306
    sget-object v0, LX/FSc;->h:LX/FSc;

    if-nez v0, :cond_1

    .line 2242307
    const-class v1, LX/FSc;

    monitor-enter v1

    .line 2242308
    :try_start_0
    sget-object v0, LX/FSc;->h:LX/FSc;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2242309
    if-eqz v2, :cond_0

    .line 2242310
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2242311
    new-instance v3, LX/FSc;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, LX/0TD;

    invoke-static {v0}, LX/0qX;->a(LX/0QB;)LX/0qX;

    move-result-object v6

    check-cast v6, LX/0qX;

    invoke-static {v0}, LX/FSa;->a(LX/0QB;)LX/FSa;

    move-result-object v7

    check-cast v7, LX/FSa;

    invoke-static {v0}, LX/FSZ;->b(LX/0QB;)LX/FSZ;

    move-result-object v8

    check-cast v8, LX/FSZ;

    invoke-direct/range {v3 .. v8}, LX/FSc;-><init>(LX/0ad;LX/0TD;LX/0qX;LX/FSa;LX/FSZ;)V

    .line 2242312
    move-object v0, v3

    .line 2242313
    sput-object v0, LX/FSc;->h:LX/FSc;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2242314
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2242315
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2242316
    :cond_1
    sget-object v0, LX/FSc;->h:LX/FSc;

    return-object v0

    .line 2242317
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2242318
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 14

    .prologue
    .line 2242319
    iget-object v0, p0, LX/FSc;->e:LX/0qX;

    invoke-static {}, LX/FSZ;->a()Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/0qX;->a(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    move-result-object v0

    .line 2242320
    iget-object v1, p0, LX/FSc;->g:LX/FSZ;

    invoke-virtual {v1, v0}, LX/FSZ;->a(Lorg/json/JSONObject;)V

    .line 2242321
    iget-object v0, p0, LX/FSc;->g:LX/FSZ;

    const-wide/16 v8, 0x0

    .line 2242322
    invoke-static {v0}, LX/FSZ;->e(LX/FSZ;)Ljava/util/GregorianCalendar;

    move-result-object v7

    .line 2242323
    if-eqz v7, :cond_0

    iget-object v6, v0, LX/FSZ;->f:Ljava/util/ArrayList;

    if-nez v6, :cond_2

    .line 2242324
    :cond_0
    :goto_0
    move-wide v0, v8

    .line 2242325
    iget-object v2, p0, LX/FSc;->e:LX/0qX;

    const/16 v3, 0x3c

    .line 2242326
    sget-wide v6, LX/0X5;->cr:J

    invoke-static {v2, v6, v7, v3}, LX/0qX;->a(LX/0qX;JI)I

    move-result v6

    move v2, v6

    .line 2242327
    int-to-long v4, v2

    cmp-long v3, v0, v4

    if-gez v3, :cond_1

    .line 2242328
    int-to-long v0, v2

    .line 2242329
    :cond_1
    invoke-virtual {p0, v0, v1}, LX/FSc;->a(J)V

    .line 2242330
    return-void

    .line 2242331
    :cond_2
    invoke-static {v0, v7}, LX/FSZ;->a(LX/FSZ;Ljava/util/GregorianCalendar;)I

    move-result v6

    .line 2242332
    :try_start_0
    iget-object v10, v0, LX/FSZ;->f:Ljava/util/ArrayList;

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-wide/16 v12, 0x3c

    mul-long/2addr v10, v12

    invoke-static {v7}, LX/FSZ;->d(Ljava/util/GregorianCalendar;)J
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v6

    sub-long v6, v10, v6

    .line 2242333
    cmp-long v8, v6, v8

    if-gez v8, :cond_3

    .line 2242334
    const-wide/16 v8, 0x2760

    add-long/2addr v6, v8

    .line 2242335
    :cond_3
    :goto_1
    iget-object v8, v0, LX/FSZ;->j:Ljava/util/Random;

    const/16 v9, 0x15

    invoke-virtual {v8, v9}, Ljava/util/Random;->nextInt(I)I

    move-result v8

    add-int/lit8 v8, v8, -0xa

    .line 2242336
    int-to-long v8, v8

    add-long/2addr v8, v6

    goto :goto_0

    .line 2242337
    :catch_0
    move-exception v6

    .line 2242338
    sget-object v7, LX/FSZ;->b:Ljava/lang/String;

    const-string v10, "IndexOutOfBoundsException in getNextDelayMins."

    invoke-static {v7, v10, v6}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-wide v6, v8

    goto :goto_1
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 2242339
    iget-object v0, p0, LX/FSc;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2242340
    :goto_0
    return-void

    .line 2242341
    :cond_0
    iget-object v0, p0, LX/FSc;->c:LX/0TD;

    new-instance v1, Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetcher$1;

    invoke-direct {v1, p0, p1, p2}, Lcom/facebook/prefetch/feed/scheduler/NewsFeedPrefetcher$1;-><init>(LX/FSc;J)V

    invoke-interface {v0, v1}, LX/0TD;->a(Ljava/lang/Runnable;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    .line 2242342
    new-instance v1, LX/FSb;

    invoke-direct {v1, p0}, LX/FSb;-><init>(LX/FSc;)V

    iget-object v2, p0, LX/FSc;->c:LX/0TD;

    invoke-static {v0, v1, v2}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0TF;Ljava/util/concurrent/Executor;)V

    goto :goto_0
.end method

.method public final clearUserData()V
    .locals 1

    .prologue
    .line 2242343
    iget-object v0, p0, LX/FSc;->f:LX/FSa;

    invoke-virtual {v0}, LX/FSa;->a()V

    .line 2242344
    return-void
.end method
