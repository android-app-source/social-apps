.class public final LX/Gvq;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Gvy;


# direct methods
.method public constructor <init>(LX/Gvy;)V
    .locals 0

    .prologue
    .line 2406121
    iput-object p1, p0, LX/Gvq;->a:LX/Gvy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2406132
    iget-object v0, p0, LX/Gvq;->a:LX/Gvy;

    .line 2406133
    iput-boolean v1, v0, LX/Gvy;->n:Z

    .line 2406134
    iget-object v0, p0, LX/Gvq;->a:LX/Gvy;

    .line 2406135
    iput-boolean v1, v0, LX/Gvy;->o:Z

    .line 2406136
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 2406122
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2406123
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultDataParcelable()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Result;

    .line 2406124
    iget-object v1, p0, LX/Gvq;->a:LX/Gvy;

    .line 2406125
    iget-boolean v2, v0, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Result;->a:Z

    move v2, v2

    .line 2406126
    iput-boolean v2, v1, LX/Gvy;->n:Z

    .line 2406127
    iget-object v1, v0, Lcom/facebook/platform/server/protocol/GetAppPermissionsMethod$Result;->b:Ljava/util/List;

    move-object v0, v1

    .line 2406128
    iget-object v1, p0, LX/Gvq;->a:LX/Gvy;

    const-string v2, "publish_stream"

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "publish_actions"

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 2406129
    :goto_0
    iput-boolean v0, v1, LX/Gvy;->o:Z

    .line 2406130
    return-void

    .line 2406131
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
