.class public final LX/Fec;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1PH;


# instance fields
.field public final synthetic a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)V
    .locals 0

    .prologue
    .line 2265994
    iput-object p1, p0, LX/Fec;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 2265995
    iget-object v0, p0, LX/Fec;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    invoke-static {v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->x(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)V

    .line 2265996
    iget-object v0, p0, LX/Fec;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ay:LX/Ffv;

    if-nez v0, :cond_0

    .line 2265997
    iget-object v0, p0, LX/Fec;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v1, p0, LX/Fec;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->G:LX/Ffw;

    iget-object v2, p0, LX/Fec;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v2, v2, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->az:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    iget-object v3, p0, LX/Fec;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    .line 2265998
    new-instance v4, LX/Fef;

    invoke-direct {v4, v3}, LX/Fef;-><init>(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)V

    move-object v3, v4

    .line 2265999
    invoke-virtual {v1, v2, v3}, LX/Ffw;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/Fee;)LX/Ffv;

    move-result-object v1

    .line 2266000
    iput-object v1, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ay:LX/Ffv;

    .line 2266001
    :cond_0
    iget-object v0, p0, LX/Fec;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ay:LX/Ffv;

    iget-object v1, p0, LX/Fec;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v1, v1, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->ak:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/Ffv;->a(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2266002
    iget-object v0, p0, LX/Fec;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    iget-object v0, v0, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->q:LX/CvY;

    iget-object v1, p0, LX/Fec;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    .line 2266003
    iget-object v2, v1, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v2, v2

    .line 2266004
    move-object v1, v2

    .line 2266005
    sget-object v2, LX/CvJ;->PULL_TO_REFRESH:LX/CvJ;

    invoke-static {v2, v1}, LX/CvY;->a(LX/CvJ;Lcom/facebook/search/results/model/SearchResultsMutableContext;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v2

    .line 2266006
    invoke-static {v0, v1, v2}, LX/CvY;->a(LX/CvY;Lcom/facebook/search/results/model/SearchResultsMutableContext;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2266007
    return-void
.end method
