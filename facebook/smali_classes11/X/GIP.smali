.class public final LX/GIP;
.super LX/GFB;
.source ""


# instance fields
.field public final synthetic a:LX/GIT;

.field private b:Z


# direct methods
.method public constructor <init>(LX/GIT;)V
    .locals 0

    .prologue
    .line 2336573
    iput-object p1, p0, LX/GIP;->a:LX/GIT;

    invoke-direct {p0}, LX/GFB;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(LX/0b7;)V
    .locals 3

    .prologue
    .line 2336574
    iget-boolean v0, p0, LX/GIP;->b:Z

    if-eqz v0, :cond_0

    .line 2336575
    :goto_0
    return-void

    .line 2336576
    :cond_0
    iget-object v0, p0, LX/GIP;->a:LX/GIT;

    iget-object v0, v0, LX/GIT;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedTargetingView;->setVisibility(I)V

    .line 2336577
    iget-object v0, p0, LX/GIP;->a:LX/GIT;

    iget-object v0, v0, LX/GIT;->a:Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;

    iget-object v1, p0, LX/GIP;->a:LX/GIT;

    iget-object v1, v1, LX/GIT;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v1

    .line 2336578
    iget-object v2, v1, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->h:Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;

    move-object v1, v2

    .line 2336579
    iget-object v2, p0, LX/GIP;->a:LX/GIT;

    iget-object v2, v2, LX/GIT;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/model/BaseAdInterfacesData;->m()Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;

    move-result-object v2

    .line 2336580
    iget-object p1, v2, Lcom/facebook/adinterfaces/model/AdInterfacesTargetingData;->p:Ljava/lang/String;

    move-object v2, p1

    .line 2336581
    invoke-virtual {v0, v1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesUnifiedAudienceOptionsView;->b(Lcom/facebook/graphql/enums/GraphQLBoostedPostAudienceOption;Ljava/lang/String;)V

    .line 2336582
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GIP;->b:Z

    goto :goto_0
.end method
