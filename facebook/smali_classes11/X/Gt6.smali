.class public final LX/Gt6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/InternSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/InternSettingsActivity;)V
    .locals 0

    .prologue
    .line 2400420
    iput-object p1, p0, LX/Gt6;->a:Lcom/facebook/katana/InternSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 5

    .prologue
    .line 2400421
    iget-object v0, p0, LX/Gt6;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v0, v0, Lcom/facebook/katana/InternSettingsActivity;->w:LX/0YI;

    .line 2400422
    iget-object v1, v0, LX/0YI;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "LastRunTime"

    const-wide/16 v3, 0x0

    invoke-interface {v1, v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2400423
    const/4 v0, 0x1

    return v0
.end method
