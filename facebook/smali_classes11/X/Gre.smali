.class public LX/Gre;
.super LX/Cts;
.source ""

# interfaces
.implements LX/Ctn;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cts",
        "<",
        "Ljava/lang/Void;",
        ">;",
        "LX/Ctn;"
    }
.end annotation


# instance fields
.field public a:LX/CqV;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field private b:LX/CuX;

.field private c:Landroid/view/VelocityTracker;

.field private d:Z


# direct methods
.method public constructor <init>(LX/Ctg;LX/CuX;)V
    .locals 1

    .prologue
    .line 2398568
    invoke-direct {p0, p1}, LX/Cts;-><init>(LX/Ctg;)V

    .line 2398569
    const-class v0, LX/Gre;

    invoke-static {v0, p0}, LX/Gre;->a(Ljava/lang/Class;LX/02k;)V

    .line 2398570
    iput-object p2, p0, LX/Gre;->b:LX/CuX;

    .line 2398571
    return-void
.end method

.method private static a(Ljava/lang/Class;LX/02k;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "LX/02k;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-interface {p1}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/Gre;

    invoke-static {p0}, LX/CqV;->a(LX/0QB;)LX/CqV;

    move-result-object p0

    check-cast p0, LX/CqV;

    iput-object p0, p1, LX/Gre;->a:LX/CqV;

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 2398572
    iget-object v1, p0, LX/Gre;->a:LX/CqV;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sget-object v2, LX/CqU;->SPHERICAL_VIDEO:LX/CqU;

    invoke-virtual {v1, v0, v2}, LX/CqV;->a(ZLX/CqU;)V

    .line 2398573
    return-void

    .line 2398574
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Z)V
    .locals 4

    .prologue
    .line 2398575
    iget-object v1, p0, LX/Gre;->a:LX/CqV;

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sget-object v2, LX/CqU;->SPHERICAL_VIDEO:LX/CqU;

    invoke-virtual {p0}, LX/Cts;->g()Landroid/view/ViewGroup;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, LX/CqV;->a(ZLX/CqU;Landroid/view/View;)V

    .line 2398576
    return-void

    .line 2398577
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/MotionEvent;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2398578
    iget-object v2, p0, LX/Gre;->c:Landroid/view/VelocityTracker;

    if-nez v2, :cond_0

    .line 2398579
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v2

    iput-object v2, p0, LX/Gre;->c:Landroid/view/VelocityTracker;

    .line 2398580
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 2398581
    :goto_1
    return v0

    .line 2398582
    :cond_0
    iget-object v2, p0, LX/Gre;->c:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_0

    .line 2398583
    :pswitch_0
    iget-object v2, p0, LX/Gre;->c:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 2398584
    invoke-direct {p0, v1}, LX/Gre;->a(Z)V

    .line 2398585
    invoke-direct {p0, v1}, LX/Gre;->b(Z)V

    goto :goto_1

    .line 2398586
    :pswitch_1
    iget-object v1, p0, LX/Gre;->c:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->recycle()V

    .line 2398587
    const/4 v1, 0x0

    iput-object v1, p0, LX/Gre;->c:Landroid/view/VelocityTracker;

    .line 2398588
    iput-boolean v0, p0, LX/Gre;->d:Z

    .line 2398589
    invoke-direct {p0, v0}, LX/Gre;->a(Z)V

    .line 2398590
    invoke-direct {p0, v0}, LX/Gre;->b(Z)V

    goto :goto_1

    .line 2398591
    :pswitch_2
    iget-boolean v2, p0, LX/Gre;->d:Z

    if-eqz v2, :cond_1

    move v0, v1

    .line 2398592
    goto :goto_1

    .line 2398593
    :cond_1
    iget-object v2, p0, LX/Gre;->c:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 2398594
    iget-object v2, p0, LX/Gre;->c:Landroid/view/VelocityTracker;

    const/high16 v3, 0x42c80000    # 100.0f

    invoke-virtual {v2, v1, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 2398595
    iget-object v2, p0, LX/Gre;->c:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-double v2, v2

    .line 2398596
    iget-object v4, p0, LX/Gre;->c:Landroid/view/VelocityTracker;

    invoke-virtual {v4}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-double v4, v4

    .line 2398597
    const-wide/16 v6, 0x0

    cmpl-double v6, v4, v6

    if-lez v6, :cond_2

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    mul-double/2addr v2, v6

    cmpl-double v2, v4, v2

    if-lez v2, :cond_2

    .line 2398598
    invoke-direct {p0, v1}, LX/Gre;->a(Z)V

    .line 2398599
    invoke-direct {p0, v1}, LX/Gre;->b(Z)V

    .line 2398600
    iput-boolean v1, p0, LX/Gre;->d:Z

    move v0, v1

    .line 2398601
    goto :goto_1

    .line 2398602
    :cond_2
    invoke-direct {p0, v0}, LX/Gre;->a(Z)V

    .line 2398603
    invoke-direct {p0, v0}, LX/Gre;->b(Z)V

    .line 2398604
    iput-boolean v0, p0, LX/Gre;->d:Z

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 2398605
    iget-object v0, p0, LX/Gre;->b:LX/CuX;

    invoke-virtual {v0, p1}, LX/CuX;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
