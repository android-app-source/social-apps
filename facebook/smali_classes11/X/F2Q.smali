.class public final LX/F2Q;
.super LX/DMC;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/DMC",
        "<",
        "LX/Daz;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/15i;

.field public final synthetic b:I

.field public final synthetic c:LX/F2U;


# direct methods
.method public constructor <init>(LX/F2U;LX/DML;LX/15i;I)V
    .locals 0

    .prologue
    .line 2193069
    iput-object p1, p0, LX/F2Q;->c:LX/F2U;

    iput-object p3, p0, LX/F2Q;->a:LX/15i;

    iput p4, p0, LX/F2Q;->b:I

    invoke-direct {p0, p2}, LX/DMC;-><init>(LX/DML;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 8

    .prologue
    .line 2193070
    check-cast p1, LX/Daz;

    const/4 v7, 0x2

    .line 2193071
    iget-object v0, p0, LX/F2Q;->a:LX/15i;

    iget v1, p0, LX/F2Q;->b:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, LX/F2Q;->a:LX/15i;

    iget v2, p0, LX/F2Q;->b:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, LX/F2Q;->a:LX/15i;

    iget v3, p0, LX/F2Q;->b:I

    const-class v4, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v0, v3, v7, v4, v5}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->name()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, LX/F2Q;->c:LX/F2U;

    iget-object v3, v3, LX/F2U;->h:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    iget-object v0, p0, LX/F2Q;->a:LX/15i;

    iget v4, p0, LX/F2Q;->b:I

    const-class v5, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    sget-object v6, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v0, v4, v7, v5, v6}, LX/15i;->a(IILjava/lang/Class;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->name()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v2, v3, v0}, LX/Daz;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 2193072
    iget-object v0, p0, LX/F2Q;->c:LX/F2U;

    iget-object v0, v0, LX/F2U;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, LX/Daz;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2193073
    return-void
.end method
