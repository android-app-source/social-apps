.class public abstract enum LX/GCi;
.super Ljava/lang/Enum;
.source ""

# interfaces
.implements LX/GCC;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GCi;",
        ">;",
        "LX/GCC;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GCi;

.field public static final enum BOOK_NOW_WRAPPER:LX/GCi;

.field public static final enum CALL_NOW_WRAPPER:LX/GCi;

.field public static final enum GET_DIRECTIONS_WRAPPER:LX/GCi;

.field public static final enum LEARN_MORE_WRAPPER:LX/GCi;

.field public static final enum NO_BUTTON_WRAPPER:LX/GCi;

.field public static final enum OFFER_WRAPPER:LX/GCi;

.field public static final enum SAVE_WRAPPER:LX/GCi;

.field public static final enum SEND_MESSAGE_WRAPPER:LX/GCi;

.field public static final enum SHOP_NOW_WRAPPER:LX/GCi;

.field public static final enum SIGN_UP_WRAPPER:LX/GCi;


# instance fields
.field private mDescriptionText:Landroid/text/Spanned;

.field private final mDescriptionTextResourceId:I

.field private mText:Ljava/lang/String;

.field private final mTextResourceId:I


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2329123
    new-instance v0, LX/GCl;

    const-string v1, "GET_DIRECTIONS_WRAPPER"

    const v2, 0x7f080a72

    const v3, 0x7f080a83

    invoke-direct {v0, v1, v5, v2, v3}, LX/GCl;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/GCi;->GET_DIRECTIONS_WRAPPER:LX/GCi;

    .line 2329124
    new-instance v0, LX/GCm;

    const-string v1, "CALL_NOW_WRAPPER"

    const v2, 0x7f080a75

    const v3, 0x7f080a84

    invoke-direct {v0, v1, v6, v2, v3}, LX/GCm;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/GCi;->CALL_NOW_WRAPPER:LX/GCi;

    .line 2329125
    new-instance v0, LX/GCn;

    const-string v1, "SEND_MESSAGE_WRAPPER"

    const v2, 0x7f080a76

    const v3, 0x7f080a85

    invoke-direct {v0, v1, v7, v2, v3}, LX/GCn;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/GCi;->SEND_MESSAGE_WRAPPER:LX/GCi;

    .line 2329126
    new-instance v0, LX/GCo;

    const-string v1, "LEARN_MORE_WRAPPER"

    const v2, 0x7f080a77

    const v3, 0x7f080a87

    invoke-direct {v0, v1, v8, v2, v3}, LX/GCo;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/GCi;->LEARN_MORE_WRAPPER:LX/GCi;

    .line 2329127
    new-instance v0, LX/GCp;

    const-string v1, "NO_BUTTON_WRAPPER"

    const v2, 0x7f080a74

    const v3, 0x7f080a86

    invoke-direct {v0, v1, v9, v2, v3}, LX/GCp;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/GCi;->NO_BUTTON_WRAPPER:LX/GCi;

    .line 2329128
    new-instance v0, LX/GCq;

    const-string v1, "BOOK_NOW_WRAPPER"

    const/4 v2, 0x5

    const v3, 0x7f080a78

    const v4, 0x7f080a79

    invoke-direct {v0, v1, v2, v3, v4}, LX/GCq;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/GCi;->BOOK_NOW_WRAPPER:LX/GCi;

    .line 2329129
    new-instance v0, LX/GCr;

    const-string v1, "SHOP_NOW_WRAPPER"

    const/4 v2, 0x6

    const v3, 0x7f080a7a

    const v4, 0x7f080a7b

    invoke-direct {v0, v1, v2, v3, v4}, LX/GCr;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/GCi;->SHOP_NOW_WRAPPER:LX/GCi;

    .line 2329130
    new-instance v0, LX/GCs;

    const-string v1, "SAVE_WRAPPER"

    const/4 v2, 0x7

    const v3, 0x7f080a7c

    const v4, 0x7f080a7d

    invoke-direct {v0, v1, v2, v3, v4}, LX/GCs;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/GCi;->SAVE_WRAPPER:LX/GCi;

    .line 2329131
    new-instance v0, LX/GCt;

    const-string v1, "SIGN_UP_WRAPPER"

    const/16 v2, 0x8

    const v3, 0x7f080a7e

    const v4, 0x7f080a7f

    invoke-direct {v0, v1, v2, v3, v4}, LX/GCt;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/GCi;->SIGN_UP_WRAPPER:LX/GCi;

    .line 2329132
    new-instance v0, LX/GCj;

    const-string v1, "OFFER_WRAPPER"

    const/16 v2, 0x9

    const v3, 0x7f080a80

    const v4, 0x7f080a81

    invoke-direct {v0, v1, v2, v3, v4}, LX/GCj;-><init>(Ljava/lang/String;III)V

    sput-object v0, LX/GCi;->OFFER_WRAPPER:LX/GCi;

    .line 2329133
    const/16 v0, 0xa

    new-array v0, v0, [LX/GCi;

    sget-object v1, LX/GCi;->GET_DIRECTIONS_WRAPPER:LX/GCi;

    aput-object v1, v0, v5

    sget-object v1, LX/GCi;->CALL_NOW_WRAPPER:LX/GCi;

    aput-object v1, v0, v6

    sget-object v1, LX/GCi;->SEND_MESSAGE_WRAPPER:LX/GCi;

    aput-object v1, v0, v7

    sget-object v1, LX/GCi;->LEARN_MORE_WRAPPER:LX/GCi;

    aput-object v1, v0, v8

    sget-object v1, LX/GCi;->NO_BUTTON_WRAPPER:LX/GCi;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, LX/GCi;->BOOK_NOW_WRAPPER:LX/GCi;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/GCi;->SHOP_NOW_WRAPPER:LX/GCi;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/GCi;->SAVE_WRAPPER:LX/GCi;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/GCi;->SIGN_UP_WRAPPER:LX/GCi;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/GCi;->OFFER_WRAPPER:LX/GCi;

    aput-object v2, v0, v1

    sput-object v0, LX/GCi;->$VALUES:[LX/GCi;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;III)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)V"
        }
    .end annotation

    .prologue
    .line 2329134
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2329135
    iput p3, p0, LX/GCi;->mTextResourceId:I

    .line 2329136
    iput p4, p0, LX/GCi;->mDescriptionTextResourceId:I

    .line 2329137
    return-void
.end method

.method public static fromGraphQLTypeCallToAction(Lcom/facebook/graphql/enums/GraphQLCallToActionType;)LX/GCi;
    .locals 2

    .prologue
    .line 2329138
    sget-object v0, LX/GCk;->a:[I

    invoke-virtual {p0}, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2329139
    sget-object v0, LX/GCi;->NO_BUTTON_WRAPPER:LX/GCi;

    :goto_0
    return-object v0

    .line 2329140
    :pswitch_0
    sget-object v0, LX/GCi;->NO_BUTTON_WRAPPER:LX/GCi;

    goto :goto_0

    .line 2329141
    :pswitch_1
    sget-object v0, LX/GCi;->GET_DIRECTIONS_WRAPPER:LX/GCi;

    goto :goto_0

    .line 2329142
    :pswitch_2
    sget-object v0, LX/GCi;->CALL_NOW_WRAPPER:LX/GCi;

    goto :goto_0

    .line 2329143
    :pswitch_3
    sget-object v0, LX/GCi;->SEND_MESSAGE_WRAPPER:LX/GCi;

    goto :goto_0

    .line 2329144
    :pswitch_4
    sget-object v0, LX/GCi;->LEARN_MORE_WRAPPER:LX/GCi;

    goto :goto_0

    .line 2329145
    :pswitch_5
    sget-object v0, LX/GCi;->BOOK_NOW_WRAPPER:LX/GCi;

    goto :goto_0

    .line 2329146
    :pswitch_6
    sget-object v0, LX/GCi;->SAVE_WRAPPER:LX/GCi;

    goto :goto_0

    .line 2329147
    :pswitch_7
    sget-object v0, LX/GCi;->SHOP_NOW_WRAPPER:LX/GCi;

    goto :goto_0

    .line 2329148
    :pswitch_8
    sget-object v0, LX/GCi;->SIGN_UP_WRAPPER:LX/GCi;

    goto :goto_0

    .line 2329149
    :pswitch_9
    sget-object v0, LX/GCi;->OFFER_WRAPPER:LX/GCi;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static isCTAValidForURL(Lcom/facebook/graphql/enums/GraphQLCallToActionType;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2329150
    const/4 v2, 0x4

    new-array v2, v2, [Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->CALL_NOW:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v3, v2, v1

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->MESSAGE_PAGE:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v3, v2, v0

    const/4 v3, 0x2

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->GET_DIRECTIONS:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    sget-object v4, Lcom/facebook/graphql/enums/GraphQLCallToActionType;->NO_BUTTON:Lcom/facebook/graphql/enums/GraphQLCallToActionType;

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    .line 2329151
    invoke-interface {v2, p0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)LX/GCi;
    .locals 1

    .prologue
    .line 2329152
    const-class v0, LX/GCi;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GCi;

    return-object v0
.end method

.method public static values()[LX/GCi;
    .locals 1

    .prologue
    .line 2329153
    sget-object v0, LX/GCi;->$VALUES:[LX/GCi;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GCi;

    return-object v0
.end method


# virtual methods
.method public getDescriptionText(Landroid/content/Context;)Landroid/text/Spanned;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2329154
    iget v1, p0, LX/GCi;->mDescriptionTextResourceId:I

    if-eqz v1, :cond_0

    if-nez p1, :cond_1

    .line 2329155
    :cond_0
    :goto_0
    return-object v0

    .line 2329156
    :cond_1
    iget-object v1, p0, LX/GCi;->mDescriptionText:Landroid/text/Spanned;

    if-nez v1, :cond_2

    .line 2329157
    iget v1, p0, LX/GCi;->mDescriptionTextResourceId:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 2329158
    if-nez v1, :cond_3

    :goto_1
    iput-object v0, p0, LX/GCi;->mDescriptionText:Landroid/text/Spanned;

    .line 2329159
    :cond_2
    iget-object v0, p0, LX/GCi;->mDescriptionText:Landroid/text/Spanned;

    goto :goto_0

    .line 2329160
    :cond_3
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_1
.end method

.method public getText(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2329161
    iget v0, p0, LX/GCi;->mTextResourceId:I

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 2329162
    :cond_0
    const/4 v0, 0x0

    .line 2329163
    :goto_0
    return-object v0

    .line 2329164
    :cond_1
    iget-object v0, p0, LX/GCi;->mText:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 2329165
    iget v0, p0, LX/GCi;->mTextResourceId:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GCi;->mText:Ljava/lang/String;

    .line 2329166
    :cond_2
    iget-object v0, p0, LX/GCi;->mText:Ljava/lang/String;

    goto :goto_0
.end method
