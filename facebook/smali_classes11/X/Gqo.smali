.class public final LX/Gqo;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:LX/CHX;

.field public final synthetic b:LX/CHX;

.field public final synthetic c:LX/GoE;

.field public final synthetic d:LX/Gqp;


# direct methods
.method public constructor <init>(LX/Gqp;LX/CHX;LX/CHX;LX/GoE;)V
    .locals 0

    .prologue
    .line 2396956
    iput-object p1, p0, LX/Gqo;->d:LX/Gqp;

    iput-object p2, p0, LX/Gqo;->a:LX/CHX;

    iput-object p3, p0, LX/Gqo;->b:LX/CHX;

    iput-object p4, p0, LX/Gqo;->c:LX/GoE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x2

    const v3, -0x4b4fcdbe

    invoke-static {v0, v1, v3}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v3

    .line 2396942
    iget-object v0, p0, LX/Gqo;->d:LX/Gqp;

    iget-object v0, v0, LX/Gqp;->j:LX/Gpr;

    .line 2396943
    iget-boolean v4, v0, LX/Gpr;->d:Z

    move v4, v4

    .line 2396944
    iget-object v0, p0, LX/Gqo;->d:LX/Gqp;

    iget-object v5, v0, LX/Gqp;->j:LX/Gpr;

    if-nez v4, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v5, v0}, LX/Gpr;->a(Z)V

    .line 2396945
    if-eqz v4, :cond_3

    iget-object v0, p0, LX/Gqo;->a:LX/CHX;

    .line 2396946
    :goto_1
    iget-object v5, p0, LX/Gqo;->d:LX/Gqp;

    iget-object v5, v5, LX/Gqp;->a:LX/GnF;

    iget-object v6, p0, LX/Gqo;->d:LX/Gqp;

    invoke-virtual {v6}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, LX/Gqo;->c:LX/GoE;

    new-instance v8, LX/Gqn;

    invoke-direct {v8, p0, v4}, LX/Gqn;-><init>(LX/Gqo;Z)V

    invoke-virtual {v5, v6, v0, v7, v8}, LX/GnF;->a(Landroid/content/Context;LX/CHX;LX/GoE;Ljava/util/Map;)V

    .line 2396947
    iget-object v0, p0, LX/Gqo;->b:LX/CHX;

    invoke-interface {v0}, LX/CHW;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->SAVE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    if-eq v0, v5, :cond_0

    iget-object v0, p0, LX/Gqo;->a:LX/CHX;

    invoke-interface {v0}, LX/CHW;->a()Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    move-result-object v0

    sget-object v5, Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;->UNSAVE:Lcom/facebook/graphql/enums/GraphQLInstantShoppingActionType;

    if-ne v0, v5, :cond_1

    .line 2396948
    :cond_0
    iget-object v0, p0, LX/Gqo;->d:LX/Gqp;

    iget-object v0, v0, LX/Gqp;->b:LX/Chv;

    new-instance v5, LX/Gna;

    if-nez v4, :cond_4

    :goto_2
    invoke-direct {v5, v1}, LX/Gna;-><init>(Z)V

    invoke-virtual {v0, v5}, LX/0b4;->a(LX/0b7;)V

    .line 2396949
    iget-object v0, p0, LX/Gqo;->d:LX/Gqp;

    iget-object v0, v0, LX/Gqp;->e:LX/1mR;

    iget-object v1, p0, LX/Gqo;->d:LX/Gqp;

    iget-object v1, v1, LX/Gqp;->f:Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;

    iget-object v2, p0, LX/Gqo;->d:LX/Gqp;

    iget-object v2, v2, LX/Gqp;->d:LX/CIb;

    .line 2396950
    iget-object v4, v2, LX/CIb;->a:Ljava/lang/String;

    move-object v2, v4

    .line 2396951
    invoke-virtual {v1, v2}, Lcom/facebook/instantshopping/fetcher/InstantShoppingDocumentFetcher;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/1mR;->a(Ljava/util/Set;)Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2396952
    :cond_1
    const v0, -0x37307dda

    invoke-static {v0, v3}, LX/02F;->a(II)V

    return-void

    :cond_2
    move v0, v2

    .line 2396953
    goto :goto_0

    .line 2396954
    :cond_3
    iget-object v0, p0, LX/Gqo;->b:LX/CHX;

    goto :goto_1

    :cond_4
    move v1, v2

    .line 2396955
    goto :goto_2
.end method
