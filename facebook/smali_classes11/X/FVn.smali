.class public LX/FVn;
.super LX/62n;
.source ""

# interfaces
.implements LX/FVN;
.implements LX/FVT;


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public final b:LX/FWe;

.field private final c:LX/FWL;

.field public final d:LX/FVO;

.field public final e:LX/FW1;

.field public final f:LX/FVu;

.field public final g:LX/7kj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/7kj",
            "<",
            "LX/FVt;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/FWj;

.field public final i:LX/03V;

.field public final j:LX/FVS;

.field public final k:LX/FVp;

.field public final l:LX/FVX;

.field private final m:LX/FWb;

.field public n:LX/1B1;

.field public final o:LX/FVq;

.field private final p:LX/0ad;

.field public q:LX/0g8;

.field public r:LX/FWC;

.field public s:LX/0am;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0am",
            "<",
            "LX/79Y;",
            ">;"
        }
    .end annotation
.end field

.field public t:LX/FVy;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2251275
    const-class v0, LX/FVn;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/FVn;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/FWC;LX/FWL;LX/FWe;LX/FVO;LX/FW1;LX/FVu;LX/7kj;LX/FWk;LX/03V;LX/FVS;LX/FVp;LX/FVX;LX/FWb;LX/FVq;LX/0ad;)V
    .locals 2
    .param p1    # LX/FWC;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2251256
    invoke-direct {p0}, LX/62n;-><init>()V

    .line 2251257
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    iput-object v1, p0, LX/FVn;->s:LX/0am;

    .line 2251258
    iput-object p1, p0, LX/FVn;->r:LX/FWC;

    .line 2251259
    iput-object p2, p0, LX/FVn;->c:LX/FWL;

    .line 2251260
    iput-object p3, p0, LX/FVn;->b:LX/FWe;

    .line 2251261
    iput-object p4, p0, LX/FVn;->d:LX/FVO;

    .line 2251262
    iput-object p5, p0, LX/FVn;->e:LX/FW1;

    .line 2251263
    iput-object p6, p0, LX/FVn;->f:LX/FVu;

    .line 2251264
    iput-object p7, p0, LX/FVn;->g:LX/7kj;

    .line 2251265
    invoke-virtual {p8, p0}, LX/FWk;->a(LX/FVn;)LX/FWj;

    move-result-object v1

    iput-object v1, p0, LX/FVn;->h:LX/FWj;

    .line 2251266
    iput-object p9, p0, LX/FVn;->i:LX/03V;

    .line 2251267
    iput-object p10, p0, LX/FVn;->j:LX/FVS;

    .line 2251268
    iput-object p11, p0, LX/FVn;->k:LX/FVp;

    .line 2251269
    iput-object p12, p0, LX/FVn;->l:LX/FVX;

    .line 2251270
    iput-object p13, p0, LX/FVn;->m:LX/FWb;

    .line 2251271
    new-instance v1, LX/1B1;

    invoke-direct {v1}, LX/1B1;-><init>()V

    iput-object v1, p0, LX/FVn;->n:LX/1B1;

    .line 2251272
    move-object/from16 v0, p14

    iput-object v0, p0, LX/FVn;->o:LX/FVq;

    .line 2251273
    move-object/from16 v0, p15

    iput-object v0, p0, LX/FVn;->p:LX/0ad;

    .line 2251274
    return-void
.end method

.method private a(LX/0am;Lcom/google/common/util/concurrent/ListenableFuture;Z)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;",
            ">;",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/FVy;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 2251245
    iget-object v0, p0, LX/FVn;->j:LX/FVS;

    .line 2251246
    iget-object v2, v0, LX/FVS;->g:LX/FWh;

    .line 2251247
    const-string v5, "SAVED_FRESH_ITEM_LOAD"

    iget-object v3, v2, LX/FWh;->f:Ljava/lang/Long;

    if-nez v3, :cond_1

    iget-object v3, v2, LX/FWh;->b:LX/0So;

    invoke-interface {v3}, LX/0So;->now()J

    move-result-wide v3

    :goto_0
    invoke-static {v2, v5, v3, v4}, LX/FWh;->a(LX/FWh;Ljava/lang/String;J)V

    .line 2251248
    const/4 v2, 0x1

    iput v2, v0, LX/FVS;->d:I

    .line 2251249
    const/4 v0, 0x0

    iput-object v0, p0, LX/FVn;->t:LX/FVy;

    .line 2251250
    new-instance v0, LX/FVm;

    invoke-direct {v0, p0, p3}, LX/FVm;-><init>(LX/FVn;Z)V

    .line 2251251
    if-eqz p2, :cond_0

    .line 2251252
    iget-object v1, p0, LX/FVn;->b:LX/FWe;

    invoke-virtual {v1, p2, v0}, LX/FWe;->b(Lcom/google/common/util/concurrent/ListenableFuture;LX/FUw;)V

    .line 2251253
    :goto_1
    return-void

    .line 2251254
    :cond_0
    iget-object v1, p0, LX/FVn;->b:LX/FWe;

    invoke-virtual {v1, p1, v0}, LX/FWe;->b(LX/0am;LX/FUw;)V

    goto :goto_1

    .line 2251255
    :cond_1
    iget-object v3, v2, LX/FWh;->f:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    goto :goto_0
.end method

.method public static a$redex0(LX/FVn;LX/FVy;LX/FWA;)V
    .locals 4

    .prologue
    .line 2251133
    iget-object v0, p1, LX/FVy;->a:LX/0am;

    move-object v0, v0

    .line 2251134
    sget-object v1, LX/0Q7;->a:LX/0Px;

    move-object v1, v1

    .line 2251135
    invoke-virtual {v0, v1}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Px;

    .line 2251136
    iget-object v1, p0, LX/FVn;->r:LX/FWC;

    .line 2251137
    iget-object v2, v1, LX/FWC;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2251138
    iget-object v2, v1, LX/FWC;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2251139
    iget-object v2, v1, LX/FWC;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2251140
    iget-object v2, v1, LX/FWC;->d:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, LX/FWC;->a(LX/FWC;Ljava/util/List;I)V

    .line 2251141
    iput-object p2, v1, LX/FWC;->e:LX/FWA;

    .line 2251142
    const v2, -0x436c5ad3

    invoke-static {v1, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2251143
    iget-object v0, p0, LX/FVn;->d:LX/FVO;

    .line 2251144
    iget-boolean v1, p1, LX/FVy;->b:Z

    move v1, v1

    .line 2251145
    iput-boolean v1, v0, LX/FVO;->c:Z

    .line 2251146
    iget-object v0, p0, LX/FVn;->q:LX/0g8;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0g8;->d(I)V

    .line 2251147
    return-void
.end method

.method public static c(LX/FVn;Z)V
    .locals 5

    .prologue
    .line 2251226
    iget-object v0, p0, LX/FVn;->d:LX/FVO;

    invoke-virtual {v0}, LX/FVO;->d()V

    .line 2251227
    iget-object v1, p0, LX/FVn;->j:LX/FVS;

    iget-object v0, p0, LX/FVn;->r:LX/FWC;

    invoke-virtual {v0}, LX/FWC;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    const/4 v3, 0x1

    .line 2251228
    iget-object v2, v1, LX/FVS;->g:LX/FWh;

    .line 2251229
    const-string v4, "SAVED_FRESH_ITEM_LOAD"

    invoke-static {v2, v4}, LX/FWh;->c(LX/FWh;Ljava/lang/String;)V

    .line 2251230
    const-string v4, "SAVED_DASH_START_TO_FRESH_ITEM_DRAWN"

    invoke-static {v2, v4}, LX/FWh;->c(LX/FWh;Ljava/lang/String;)V

    .line 2251231
    iget v2, v1, LX/FVS;->c:I

    if-eq v2, v3, :cond_0

    .line 2251232
    if-eqz v0, :cond_3

    if-nez p1, :cond_3

    .line 2251233
    invoke-static {v1}, LX/FVS;->k(LX/FVS;)V

    .line 2251234
    iget-object v2, v1, LX/FVS;->g:LX/FWh;

    invoke-virtual {v2, v3}, LX/FWh;->a(Z)V

    .line 2251235
    :cond_0
    :goto_1
    const/4 v2, 0x2

    iput v2, v1, LX/FVS;->d:I

    .line 2251236
    iget-object v0, p0, LX/FVn;->o:LX/FVq;

    .line 2251237
    iget-object v1, v0, LX/FVq;->a:LX/16I;

    invoke-virtual {v1}, LX/16I;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2251238
    :cond_1
    :goto_2
    return-void

    .line 2251239
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 2251240
    :cond_3
    invoke-static {v1}, LX/FVS;->j(LX/FVS;)V

    goto :goto_1

    .line 2251241
    :cond_4
    iget-object v1, v0, LX/FVq;->b:LX/0Yb;

    if-nez v1, :cond_5

    .line 2251242
    iget-object v1, v0, LX/FVq;->a:LX/16I;

    sget-object v2, LX/1Ed;->CONNECTED:LX/1Ed;

    new-instance v3, Lcom/facebook/saved/controller/SavedItemsReconnectController$1;

    invoke-direct {v3, v0}, Lcom/facebook/saved/controller/SavedItemsReconnectController$1;-><init>(LX/FVq;)V

    invoke-virtual {v1, v2, v3}, LX/16I;->a(LX/1Ed;Ljava/lang/Runnable;)LX/0Yb;

    move-result-object v1

    iput-object v1, v0, LX/FVq;->b:LX/0Yb;

    .line 2251243
    :cond_5
    iget-object v1, v0, LX/FVq;->b:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->a()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2251244
    iget-object v1, v0, LX/FVq;->b:LX/0Yb;

    invoke-virtual {v1}, LX/0Yb;->b()V

    goto :goto_2
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2251223
    iget-object v0, p0, LX/FVn;->k:LX/FVp;

    invoke-virtual {v0}, LX/62n;->a()V

    .line 2251224
    iget-object v0, p0, LX/FVn;->d:LX/FVO;

    invoke-virtual {v0}, LX/FVO;->d()V

    .line 2251225
    return-void
.end method

.method public final a(LX/0am;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0am",
            "<",
            "LX/79Y;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2251185
    iput-object p1, p0, LX/FVn;->s:LX/0am;

    .line 2251186
    iget-object v0, p0, LX/FVn;->d:LX/FVO;

    invoke-virtual {v0}, LX/FVO;->g()V

    .line 2251187
    iget-object v0, p0, LX/FVn;->l:LX/FVX;

    .line 2251188
    invoke-static {v0}, LX/FVX;->g(LX/FVX;)V

    .line 2251189
    iget-object v0, p0, LX/FVn;->g:LX/7kj;

    .line 2251190
    iget-object v2, v0, LX/7kj;->a:LX/1Kt;

    const/4 v3, 0x0

    iget-object v4, v0, LX/7kj;->c:LX/0g8;

    invoke-virtual {v2, v3, v4}, LX/1Kt;->a(ZLX/0g8;)V

    .line 2251191
    iget-object v0, p0, LX/FVn;->r:LX/FWC;

    .line 2251192
    iget-object v2, v0, LX/FWC;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2251193
    sget-object v2, LX/FWA;->NONE:LX/FWA;

    iput-object v2, v0, LX/FWC;->e:LX/FWA;

    .line 2251194
    iget-object v2, v0, LX/FWC;->g:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 2251195
    const v2, -0x4164d42f

    invoke-static {v0, v2}, LX/08p;->a(Landroid/widget/BaseAdapter;I)V

    .line 2251196
    iget-object v0, p0, LX/FVn;->g:LX/7kj;

    .line 2251197
    iget-object v2, v0, LX/7kj;->a:LX/1Kt;

    const/4 v3, 0x1

    iget-object v4, v0, LX/7kj;->c:LX/0g8;

    invoke-virtual {v2, v3, v4}, LX/1Kt;->a(ZLX/0g8;)V

    .line 2251198
    iget-object v0, p0, LX/FVn;->b:LX/FWe;

    invoke-virtual {v0}, LX/FWe;->b()V

    .line 2251199
    invoke-static {p1}, LX/FWL;->a(LX/0am;)LX/0am;

    move-result-object v3

    .line 2251200
    iget-object v0, p0, LX/FVn;->m:LX/FWb;

    invoke-virtual {v0}, LX/98h;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2251201
    iget-object v0, p0, LX/FVn;->m:LX/FWb;

    invoke-virtual {v3}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, LX/98h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FWa;

    .line 2251202
    if-eqz v0, :cond_0

    .line 2251203
    iget-object v1, v0, LX/FWa;->b:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v2, v1

    .line 2251204
    iget-object v1, v0, LX/FWa;->a:Lcom/google/common/util/concurrent/ListenableFuture;

    move-object v1, v1

    .line 2251205
    iget-object v0, p0, LX/FVn;->m:LX/FWb;

    invoke-virtual {v0}, LX/98h;->d()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 2251206
    :goto_0
    iget-object v4, p0, LX/FVn;->j:LX/FVS;

    const/4 v5, 0x2

    .line 2251207
    iput v5, v4, LX/FVS;->c:I

    .line 2251208
    iput v5, v4, LX/FVS;->d:I

    .line 2251209
    iget-object v4, p0, LX/FVn;->j:LX/FVS;

    invoke-virtual {v4, v0, p1}, LX/FVS;->a(Ljava/lang/Long;LX/0am;)V

    .line 2251210
    iget-object v6, p0, LX/FVn;->j:LX/FVS;

    .line 2251211
    iget-object v8, v6, LX/FVS;->g:LX/FWh;

    .line 2251212
    const-string v11, "SAVED_CACHED_ITEM_LOAD"

    iget-object v9, v8, LX/FWh;->f:Ljava/lang/Long;

    if-nez v9, :cond_2

    iget-object v9, v8, LX/FWh;->b:LX/0So;

    invoke-interface {v9}, LX/0So;->now()J

    move-result-wide v9

    :goto_1
    invoke-static {v8, v11, v9, v10}, LX/FWh;->a(LX/FWh;Ljava/lang/String;J)V

    .line 2251213
    const/4 v8, 0x1

    iput v8, v6, LX/FVS;->c:I

    .line 2251214
    new-instance v6, LX/FVl;

    invoke-direct {v6, p0}, LX/FVl;-><init>(LX/FVn;)V

    .line 2251215
    if-eqz v2, :cond_1

    .line 2251216
    iget-object v7, p0, LX/FVn;->b:LX/FWe;

    invoke-virtual {v7, v2, v6}, LX/FWe;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/FUw;)V

    .line 2251217
    :goto_2
    const/4 v0, 0x0

    invoke-direct {p0, v3, v1, v0}, LX/FVn;->a(LX/0am;Lcom/google/common/util/concurrent/ListenableFuture;Z)V

    .line 2251218
    return-void

    :cond_0
    move-object v0, v1

    move-object v2, v1

    goto :goto_0

    .line 2251219
    :cond_1
    iget-object v7, p0, LX/FVn;->b:LX/FWe;

    .line 2251220
    iget-object v8, v7, LX/FWe;->b:LX/FWZ;

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v9

    invoke-virtual {v8, v3, v9}, LX/FWZ;->b(LX/0am;LX/0am;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v8

    invoke-virtual {v7, v8, v6}, LX/FWe;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/FUw;)V

    .line 2251221
    goto :goto_2

    .line 2251222
    :cond_2
    iget-object v9, v8, LX/FWh;->f:Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    goto :goto_1
.end method

.method public final b()V
    .locals 8

    .prologue
    .line 2251168
    iget-object v0, p0, LX/FVn;->r:LX/FWC;

    .line 2251169
    iget-object v1, v0, LX/FWC;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2251170
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    .line 2251171
    :goto_0
    move-object v0, v1

    .line 2251172
    invoke-virtual {v0}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2251173
    iget-object v1, p0, LX/FVn;->b:LX/FWe;

    iget-object v2, p0, LX/FVn;->s:LX/0am;

    invoke-static {v2}, LX/FWL;->a(LX/0am;)LX/0am;

    move-result-object v2

    new-instance v3, LX/FVe;

    invoke-direct {v3, p0}, LX/FVe;-><init>(LX/FVn;)V

    .line 2251174
    invoke-virtual {v0}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v4, 0x1

    :goto_1
    invoke-static {v4}, LX/0PB;->checkArgument(Z)V

    .line 2251175
    iget-object v4, v1, LX/FWe;->a:LX/1Ck;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "task_key_fetch_fresh_saved_items"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v1, LX/FWe;->b:LX/FWZ;

    invoke-virtual {v6, v2, v0}, LX/FWZ;->a(LX/0am;LX/0am;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    invoke-static {v1, v3}, LX/FWe;->a(LX/FWe;LX/FUw;)LX/0Vd;

    move-result-object v7

    invoke-virtual {v4, v5, v6, v7}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2251176
    iget-object v0, p0, LX/FVn;->d:LX/FVO;

    .line 2251177
    sget-object v1, LX/FVM;->LOADING_MORE:LX/FVM;

    iput-object v1, v0, LX/FVO;->b:LX/FVM;

    .line 2251178
    :cond_0
    return-void

    .line 2251179
    :cond_1
    iget-object v1, v0, LX/FWC;->d:Ljava/util/ArrayList;

    iget-object v2, v0, LX/FWC;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FVs;

    .line 2251180
    instance-of v2, v1, LX/FVt;

    const-string v3, "Last item of list should never be a header."

    invoke-static {v2, v3}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2251181
    check-cast v1, LX/FVt;

    .line 2251182
    iget-object v2, v1, LX/FVt;->i:Ljava/lang/String;

    move-object v1, v2

    .line 2251183
    invoke-static {v1}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v1

    goto :goto_0

    .line 2251184
    :cond_2
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 2251152
    if-eqz p1, :cond_0

    .line 2251153
    iget-object v0, p0, LX/FVn;->b:LX/FWe;

    invoke-virtual {v0}, LX/FWe;->b()V

    .line 2251154
    iget-object v0, p0, LX/FVn;->j:LX/FVS;

    .line 2251155
    invoke-virtual {v0}, LX/FVS;->d()V

    .line 2251156
    const/4 v1, 0x2

    iput v1, v0, LX/FVS;->d:I

    .line 2251157
    iget-object v0, p0, LX/FVn;->l:LX/FVX;

    .line 2251158
    invoke-static {v0}, LX/FVX;->g(LX/FVX;)V

    .line 2251159
    iget-object v0, p0, LX/FVn;->s:LX/0am;

    invoke-static {v0}, LX/FWL;->a(LX/0am;)LX/0am;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, LX/FVn;->a(LX/0am;Lcom/google/common/util/concurrent/ListenableFuture;Z)V

    .line 2251160
    :cond_0
    iget-object v0, p0, LX/FVn;->k:LX/FVp;

    invoke-virtual {v0, p1}, LX/62n;->b(Z)V

    .line 2251161
    iget-object v0, p0, LX/FVn;->d:LX/FVO;

    .line 2251162
    iget-object v1, v0, LX/FVO;->b:LX/FVM;

    .line 2251163
    sget-object v2, LX/FVM;->REFRESHING:LX/FVM;

    iput-object v2, v0, LX/FVO;->b:LX/FVM;

    .line 2251164
    sget-object v2, LX/FVM;->LOADING_MORE:LX/FVM;

    if-ne v1, v2, :cond_1

    .line 2251165
    iget-object v1, v0, LX/FVO;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FVN;

    .line 2251166
    invoke-interface {v1}, LX/FVN;->c()V

    goto :goto_0

    .line 2251167
    :cond_1
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 2251151
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 2251150
    return-void
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 2251149
    return-void
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 2251148
    return-void
.end method
