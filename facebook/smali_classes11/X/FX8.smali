.class public final LX/FX8;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 62

    .prologue
    .line 2254004
    const/16 v54, 0x0

    .line 2254005
    const/16 v53, 0x0

    .line 2254006
    const/16 v52, 0x0

    .line 2254007
    const/16 v49, 0x0

    .line 2254008
    const-wide/16 v50, 0x0

    .line 2254009
    const/16 v48, 0x0

    .line 2254010
    const/16 v47, 0x0

    .line 2254011
    const/16 v46, 0x0

    .line 2254012
    const/16 v45, 0x0

    .line 2254013
    const/16 v44, 0x0

    .line 2254014
    const/16 v43, 0x0

    .line 2254015
    const/16 v42, 0x0

    .line 2254016
    const/16 v41, 0x0

    .line 2254017
    const/16 v40, 0x0

    .line 2254018
    const/16 v37, 0x0

    .line 2254019
    const-wide/16 v38, 0x0

    .line 2254020
    const/16 v36, 0x0

    .line 2254021
    const/16 v35, 0x0

    .line 2254022
    const/16 v34, 0x0

    .line 2254023
    const/16 v33, 0x0

    .line 2254024
    const/16 v32, 0x0

    .line 2254025
    const-wide/16 v30, 0x0

    .line 2254026
    const-wide/16 v28, 0x0

    .line 2254027
    const/16 v27, 0x0

    .line 2254028
    const/16 v26, 0x0

    .line 2254029
    const/16 v25, 0x0

    .line 2254030
    const/16 v24, 0x0

    .line 2254031
    const/16 v23, 0x0

    .line 2254032
    const/16 v22, 0x0

    .line 2254033
    const/16 v21, 0x0

    .line 2254034
    const/16 v20, 0x0

    .line 2254035
    const/16 v19, 0x0

    .line 2254036
    const/16 v18, 0x0

    .line 2254037
    const/16 v17, 0x0

    .line 2254038
    const/16 v16, 0x0

    .line 2254039
    const/4 v15, 0x0

    .line 2254040
    const/4 v14, 0x0

    .line 2254041
    const/4 v13, 0x0

    .line 2254042
    const/4 v12, 0x0

    .line 2254043
    const/4 v11, 0x0

    .line 2254044
    const/4 v10, 0x0

    .line 2254045
    const/4 v9, 0x0

    .line 2254046
    const/4 v8, 0x0

    .line 2254047
    const/4 v7, 0x0

    .line 2254048
    const/4 v6, 0x0

    .line 2254049
    const/4 v5, 0x0

    .line 2254050
    const/4 v4, 0x0

    .line 2254051
    const/4 v3, 0x0

    .line 2254052
    const/4 v2, 0x0

    .line 2254053
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v55

    sget-object v56, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v55

    move-object/from16 v1, v56

    if-eq v0, v1, :cond_34

    .line 2254054
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2254055
    const/4 v2, 0x0

    .line 2254056
    :goto_0
    return v2

    .line 2254057
    :cond_0
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v2

    sget-object v56, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v56

    if-eq v2, v0, :cond_21

    .line 2254058
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v2

    .line 2254059
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2254060
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v56

    sget-object v57, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v56

    move-object/from16 v1, v57

    if-eq v0, v1, :cond_0

    if-eqz v2, :cond_0

    .line 2254061
    const-string v56, "__type__"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-nez v56, :cond_1

    const-string v56, "__typename"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_2

    .line 2254062
    :cond_1
    invoke-static/range {p0 .. p0}, Lcom/facebook/graphql/enums/GraphQLObjectType;->a(LX/15w;)Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Lcom/facebook/flatbuffers/Flattenable;)I

    move-result v2

    move/from16 v55, v2

    goto :goto_1

    .line 2254063
    :cond_2
    const-string v56, "can_viewer_share"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_3

    .line 2254064
    const/4 v2, 0x1

    .line 2254065
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v7

    move/from16 v54, v7

    move v7, v2

    goto :goto_1

    .line 2254066
    :cond_3
    const-string v56, "enable_focus"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_4

    .line 2254067
    const/4 v2, 0x1

    .line 2254068
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v6

    move/from16 v53, v6

    move v6, v2

    goto :goto_1

    .line 2254069
    :cond_4
    const-string v56, "event_viewer_capability"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_5

    .line 2254070
    invoke-static/range {p0 .. p1}, LX/7AH;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v52, v2

    goto :goto_1

    .line 2254071
    :cond_5
    const-string v56, "focus_width_degrees"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_6

    .line 2254072
    const/4 v2, 0x1

    .line 2254073
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v4

    move v3, v2

    goto/16 :goto_1

    .line 2254074
    :cond_6
    const-string v56, "global_share"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_7

    .line 2254075
    invoke-static/range {p0 .. p1}, LX/7AI;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v51, v2

    goto/16 :goto_1

    .line 2254076
    :cond_7
    const-string v56, "guided_tour"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_8

    .line 2254077
    invoke-static/range {p0 .. p1}, LX/5CB;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v50, v2

    goto/16 :goto_1

    .line 2254078
    :cond_8
    const-string v56, "height"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_9

    .line 2254079
    const/4 v2, 0x1

    .line 2254080
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v22

    move/from16 v49, v22

    move/from16 v22, v2

    goto/16 :goto_1

    .line 2254081
    :cond_9
    const-string v56, "id"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_a

    .line 2254082
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v48, v2

    goto/16 :goto_1

    .line 2254083
    :cond_a
    const-string v56, "initial_view_heading_degrees"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_b

    .line 2254084
    const/4 v2, 0x1

    .line 2254085
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v21

    move/from16 v47, v21

    move/from16 v21, v2

    goto/16 :goto_1

    .line 2254086
    :cond_b
    const-string v56, "initial_view_pitch_degrees"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_c

    .line 2254087
    const/4 v2, 0x1

    .line 2254088
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v20

    move/from16 v46, v20

    move/from16 v20, v2

    goto/16 :goto_1

    .line 2254089
    :cond_c
    const-string v56, "initial_view_roll_degrees"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_d

    .line 2254090
    const/4 v2, 0x1

    .line 2254091
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v19

    move/from16 v45, v19

    move/from16 v19, v2

    goto/16 :goto_1

    .line 2254092
    :cond_d
    const-string v56, "is_playable"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_e

    .line 2254093
    const/4 v2, 0x1

    .line 2254094
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v18

    move/from16 v44, v18

    move/from16 v18, v2

    goto/16 :goto_1

    .line 2254095
    :cond_e
    const-string v56, "is_save_offline_allowed"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_f

    .line 2254096
    const/4 v2, 0x1

    .line 2254097
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v17

    move/from16 v43, v17

    move/from16 v17, v2

    goto/16 :goto_1

    .line 2254098
    :cond_f
    const-string v56, "is_spherical"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_10

    .line 2254099
    const/4 v2, 0x1

    .line 2254100
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v16

    move/from16 v42, v16

    move/from16 v16, v2

    goto/16 :goto_1

    .line 2254101
    :cond_10
    const-string v56, "off_focus_level"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_11

    .line 2254102
    const/4 v2, 0x1

    .line 2254103
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v40

    move v15, v2

    goto/16 :goto_1

    .line 2254104
    :cond_11
    const-string v56, "playable_duration_in_ms"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_12

    .line 2254105
    const/4 v2, 0x1

    .line 2254106
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v14

    move/from16 v39, v14

    move v14, v2

    goto/16 :goto_1

    .line 2254107
    :cond_12
    const-string v56, "playable_url"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_13

    .line 2254108
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v38, v2

    goto/16 :goto_1

    .line 2254109
    :cond_13
    const-string v56, "playlist"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_14

    .line 2254110
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v37, v2

    goto/16 :goto_1

    .line 2254111
    :cond_14
    const-string v56, "preferredPlayableUrlString"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_15

    .line 2254112
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v36, v2

    goto/16 :goto_1

    .line 2254113
    :cond_15
    const-string v56, "projection_type"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_16

    .line 2254114
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v31, v2

    goto/16 :goto_1

    .line 2254115
    :cond_16
    const-string v56, "sphericalFullscreenAspectRatio"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_17

    .line 2254116
    const/4 v2, 0x1

    .line 2254117
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v34

    move v13, v2

    goto/16 :goto_1

    .line 2254118
    :cond_17
    const-string v56, "sphericalInlineAspectRatio"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_18

    .line 2254119
    const/4 v2, 0x1

    .line 2254120
    invoke-virtual/range {p0 .. p0}, LX/15w;->G()D

    move-result-wide v32

    move v12, v2

    goto/16 :goto_1

    .line 2254121
    :cond_18
    const-string v56, "sphericalPlayableUrlHdString"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_19

    .line 2254122
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v30, v2

    goto/16 :goto_1

    .line 2254123
    :cond_19
    const-string v56, "sphericalPlayableUrlSdString"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_1a

    .line 2254124
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->b(Ljava/lang/String;)I

    move-result v2

    move/from16 v29, v2

    goto/16 :goto_1

    .line 2254125
    :cond_1a
    const-string v56, "sphericalPreferredFov"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_1b

    .line 2254126
    const/4 v2, 0x1

    .line 2254127
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v11

    move/from16 v28, v11

    move v11, v2

    goto/16 :goto_1

    .line 2254128
    :cond_1b
    const-string v56, "video_channel"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_1c

    .line 2254129
    invoke-static/range {p0 .. p1}, LX/FX7;->a(LX/15w;LX/186;)I

    move-result v2

    move/from16 v27, v2

    goto/16 :goto_1

    .line 2254130
    :cond_1c
    const-string v56, "video_full_size"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_1d

    .line 2254131
    const/4 v2, 0x1

    .line 2254132
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v10

    move/from16 v26, v10

    move v10, v2

    goto/16 :goto_1

    .line 2254133
    :cond_1d
    const-string v56, "viewer_last_play_position_ms"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_1e

    .line 2254134
    const/4 v2, 0x1

    .line 2254135
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v9

    move/from16 v25, v9

    move v9, v2

    goto/16 :goto_1

    .line 2254136
    :cond_1e
    const-string v56, "viewer_saved_state"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v56

    if-eqz v56, :cond_1f

    .line 2254137
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLSavedState;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    move/from16 v24, v2

    goto/16 :goto_1

    .line 2254138
    :cond_1f
    const-string v56, "width"

    move-object/from16 v0, v56

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 2254139
    const/4 v2, 0x1

    .line 2254140
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v8

    move/from16 v23, v8

    move v8, v2

    goto/16 :goto_1

    .line 2254141
    :cond_20
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    goto/16 :goto_1

    .line 2254142
    :cond_21
    const/16 v2, 0x1f

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, LX/186;->c(I)V

    .line 2254143
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v55

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2254144
    if-eqz v7, :cond_22

    .line 2254145
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v54

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2254146
    :cond_22
    if-eqz v6, :cond_23

    .line 2254147
    const/4 v2, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v53

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2254148
    :cond_23
    const/4 v2, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v52

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2254149
    if-eqz v3, :cond_24

    .line 2254150
    const/4 v3, 0x4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 2254151
    :cond_24
    const/4 v2, 0x5

    move-object/from16 v0, p1

    move/from16 v1, v51

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2254152
    const/4 v2, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v50

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2254153
    if-eqz v22, :cond_25

    .line 2254154
    const/4 v2, 0x7

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v49

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 2254155
    :cond_25
    const/16 v2, 0x8

    move-object/from16 v0, p1

    move/from16 v1, v48

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2254156
    if-eqz v21, :cond_26

    .line 2254157
    const/16 v2, 0x9

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v47

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 2254158
    :cond_26
    if-eqz v20, :cond_27

    .line 2254159
    const/16 v2, 0xa

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v46

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 2254160
    :cond_27
    if-eqz v19, :cond_28

    .line 2254161
    const/16 v2, 0xb

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v45

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 2254162
    :cond_28
    if-eqz v18, :cond_29

    .line 2254163
    const/16 v2, 0xc

    move-object/from16 v0, p1

    move/from16 v1, v44

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2254164
    :cond_29
    if-eqz v17, :cond_2a

    .line 2254165
    const/16 v2, 0xd

    move-object/from16 v0, p1

    move/from16 v1, v43

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2254166
    :cond_2a
    if-eqz v16, :cond_2b

    .line 2254167
    const/16 v2, 0xe

    move-object/from16 v0, p1

    move/from16 v1, v42

    invoke-virtual {v0, v2, v1}, LX/186;->a(IZ)V

    .line 2254168
    :cond_2b
    if-eqz v15, :cond_2c

    .line 2254169
    const/16 v3, 0xf

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v40

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 2254170
    :cond_2c
    if-eqz v14, :cond_2d

    .line 2254171
    const/16 v2, 0x10

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v39

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 2254172
    :cond_2d
    const/16 v2, 0x11

    move-object/from16 v0, p1

    move/from16 v1, v38

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2254173
    const/16 v2, 0x12

    move-object/from16 v0, p1

    move/from16 v1, v37

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2254174
    const/16 v2, 0x13

    move-object/from16 v0, p1

    move/from16 v1, v36

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2254175
    const/16 v2, 0x14

    move-object/from16 v0, p1

    move/from16 v1, v31

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2254176
    if-eqz v13, :cond_2e

    .line 2254177
    const/16 v3, 0x15

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v34

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 2254178
    :cond_2e
    if-eqz v12, :cond_2f

    .line 2254179
    const/16 v3, 0x16

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    move-wide/from16 v4, v32

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IDD)V

    .line 2254180
    :cond_2f
    const/16 v2, 0x17

    move-object/from16 v0, p1

    move/from16 v1, v30

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2254181
    const/16 v2, 0x18

    move-object/from16 v0, p1

    move/from16 v1, v29

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2254182
    if-eqz v11, :cond_30

    .line 2254183
    const/16 v2, 0x19

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v28

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 2254184
    :cond_30
    const/16 v2, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v27

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2254185
    if-eqz v10, :cond_31

    .line 2254186
    const/16 v2, 0x1b

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v26

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 2254187
    :cond_31
    if-eqz v9, :cond_32

    .line 2254188
    const/16 v2, 0x1c

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v25

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 2254189
    :cond_32
    const/16 v2, 0x1d

    move-object/from16 v0, p1

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, LX/186;->b(II)V

    .line 2254190
    if-eqz v8, :cond_33

    .line 2254191
    const/16 v2, 0x1e

    const/4 v3, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-virtual {v0, v2, v1, v3}, LX/186;->a(III)V

    .line 2254192
    :cond_33
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_34
    move/from16 v55, v54

    move/from16 v54, v53

    move/from16 v53, v52

    move/from16 v52, v49

    move/from16 v49, v46

    move/from16 v46, v43

    move/from16 v43, v40

    move/from16 v58, v9

    move v9, v3

    move/from16 v3, v17

    move/from16 v17, v11

    move v11, v5

    move/from16 v59, v13

    move v13, v7

    move/from16 v7, v19

    move/from16 v19, v59

    move/from16 v60, v16

    move/from16 v16, v10

    move v10, v4

    move-wide/from16 v4, v50

    move/from16 v50, v47

    move/from16 v51, v48

    move/from16 v47, v44

    move/from16 v48, v45

    move/from16 v44, v41

    move/from16 v45, v42

    move-wide/from16 v40, v38

    move/from16 v42, v37

    move/from16 v37, v34

    move/from16 v38, v35

    move/from16 v39, v36

    move/from16 v36, v33

    move-wide/from16 v34, v30

    move/from16 v30, v27

    move/from16 v31, v32

    move-wide/from16 v32, v28

    move/from16 v27, v24

    move/from16 v29, v26

    move/from16 v28, v25

    move/from16 v24, v21

    move/from16 v21, v15

    move/from16 v26, v23

    move/from16 v25, v22

    move/from16 v23, v20

    move/from16 v22, v60

    move/from16 v15, v58

    move/from16 v20, v14

    move v14, v8

    move v8, v2

    move/from16 v61, v6

    move/from16 v6, v18

    move/from16 v18, v12

    move/from16 v12, v61

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 7

    .prologue
    const/16 v6, 0x1d

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    .line 2254193
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2254194
    invoke-virtual {p0, p1, v3}, LX/15i;->g(II)I

    move-result v0

    .line 2254195
    if-eqz v0, :cond_0

    .line 2254196
    const-string v0, "__type__"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254197
    invoke-static {p0, p1, v3, p2}, LX/2bt;->b(LX/15i;IILX/0nX;)V

    .line 2254198
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2254199
    if-eqz v0, :cond_1

    .line 2254200
    const-string v1, "can_viewer_share"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254201
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2254202
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2254203
    if-eqz v0, :cond_2

    .line 2254204
    const-string v1, "enable_focus"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254205
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2254206
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2254207
    if-eqz v0, :cond_3

    .line 2254208
    const-string v1, "event_viewer_capability"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254209
    invoke-static {p0, v0, p2}, LX/7AH;->a(LX/15i;ILX/0nX;)V

    .line 2254210
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 2254211
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_4

    .line 2254212
    const-string v2, "focus_width_degrees"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254213
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 2254214
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2254215
    if-eqz v0, :cond_5

    .line 2254216
    const-string v1, "global_share"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254217
    invoke-static {p0, v0, p2}, LX/7AI;->a(LX/15i;ILX/0nX;)V

    .line 2254218
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2254219
    if-eqz v0, :cond_6

    .line 2254220
    const-string v1, "guided_tour"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254221
    invoke-static {p0, v0, p2, p3}, LX/5CB;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2254222
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2254223
    if-eqz v0, :cond_7

    .line 2254224
    const-string v1, "height"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254225
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2254226
    :cond_7
    const/16 v0, 0x8

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2254227
    if-eqz v0, :cond_8

    .line 2254228
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254229
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2254230
    :cond_8
    const/16 v0, 0x9

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2254231
    if-eqz v0, :cond_9

    .line 2254232
    const-string v1, "initial_view_heading_degrees"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254233
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2254234
    :cond_9
    const/16 v0, 0xa

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2254235
    if-eqz v0, :cond_a

    .line 2254236
    const-string v1, "initial_view_pitch_degrees"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254237
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2254238
    :cond_a
    const/16 v0, 0xb

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2254239
    if-eqz v0, :cond_b

    .line 2254240
    const-string v1, "initial_view_roll_degrees"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254241
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2254242
    :cond_b
    const/16 v0, 0xc

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2254243
    if-eqz v0, :cond_c

    .line 2254244
    const-string v1, "is_playable"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254245
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2254246
    :cond_c
    const/16 v0, 0xd

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2254247
    if-eqz v0, :cond_d

    .line 2254248
    const-string v1, "is_save_offline_allowed"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254249
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2254250
    :cond_d
    const/16 v0, 0xe

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2254251
    if-eqz v0, :cond_e

    .line 2254252
    const-string v1, "is_spherical"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254253
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2254254
    :cond_e
    const/16 v0, 0xf

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 2254255
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_f

    .line 2254256
    const-string v2, "off_focus_level"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254257
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 2254258
    :cond_f
    const/16 v0, 0x10

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2254259
    if-eqz v0, :cond_10

    .line 2254260
    const-string v1, "playable_duration_in_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254261
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2254262
    :cond_10
    const/16 v0, 0x11

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2254263
    if-eqz v0, :cond_11

    .line 2254264
    const-string v1, "playable_url"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254265
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2254266
    :cond_11
    const/16 v0, 0x12

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2254267
    if-eqz v0, :cond_12

    .line 2254268
    const-string v1, "playlist"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254269
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2254270
    :cond_12
    const/16 v0, 0x13

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2254271
    if-eqz v0, :cond_13

    .line 2254272
    const-string v1, "preferredPlayableUrlString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254273
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2254274
    :cond_13
    const/16 v0, 0x14

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2254275
    if-eqz v0, :cond_14

    .line 2254276
    const-string v1, "projection_type"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254277
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2254278
    :cond_14
    const/16 v0, 0x15

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 2254279
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_15

    .line 2254280
    const-string v2, "sphericalFullscreenAspectRatio"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254281
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 2254282
    :cond_15
    const/16 v0, 0x16

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IID)D

    move-result-wide v0

    .line 2254283
    cmpl-double v2, v0, v4

    if-eqz v2, :cond_16

    .line 2254284
    const-string v2, "sphericalInlineAspectRatio"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254285
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(D)V

    .line 2254286
    :cond_16
    const/16 v0, 0x17

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2254287
    if-eqz v0, :cond_17

    .line 2254288
    const-string v1, "sphericalPlayableUrlHdString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254289
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2254290
    :cond_17
    const/16 v0, 0x18

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2254291
    if-eqz v0, :cond_18

    .line 2254292
    const-string v1, "sphericalPlayableUrlSdString"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254293
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2254294
    :cond_18
    const/16 v0, 0x19

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2254295
    if-eqz v0, :cond_19

    .line 2254296
    const-string v1, "sphericalPreferredFov"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254297
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2254298
    :cond_19
    const/16 v0, 0x1a

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2254299
    if-eqz v0, :cond_1a

    .line 2254300
    const-string v1, "video_channel"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254301
    invoke-static {p0, v0, p2}, LX/FX7;->a(LX/15i;ILX/0nX;)V

    .line 2254302
    :cond_1a
    const/16 v0, 0x1b

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2254303
    if-eqz v0, :cond_1b

    .line 2254304
    const-string v1, "video_full_size"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254305
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2254306
    :cond_1b
    const/16 v0, 0x1c

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2254307
    if-eqz v0, :cond_1c

    .line 2254308
    const-string v1, "viewer_last_play_position_ms"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254309
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2254310
    :cond_1c
    invoke-virtual {p0, p1, v6}, LX/15i;->g(II)I

    move-result v0

    .line 2254311
    if-eqz v0, :cond_1d

    .line 2254312
    const-string v0, "viewer_saved_state"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254313
    invoke-virtual {p0, p1, v6}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2254314
    :cond_1d
    const/16 v0, 0x1e

    invoke-virtual {p0, p1, v0, v3}, LX/15i;->a(III)I

    move-result v0

    .line 2254315
    if-eqz v0, :cond_1e

    .line 2254316
    const-string v1, "width"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2254317
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2254318
    :cond_1e
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2254319
    return-void
.end method
