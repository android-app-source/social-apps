.class public final LX/GTd;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GTe;


# direct methods
.method public constructor <init>(LX/GTe;)V
    .locals 0

    .prologue
    .line 2355709
    iput-object p1, p0, LX/GTd;->a:LX/GTe;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2355710
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    .line 2355711
    if-nez p1, :cond_0

    .line 2355712
    sget-object v0, LX/GTe;->a:Ljava/lang/Class;

    const-string v1, "Null result from updating NUX status"

    invoke-static {v0, v1}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2355713
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2355714
    :goto_0
    return-object v0

    .line 2355715
    :cond_0
    iget-object v0, p0, LX/GTd;->a:LX/GTe;

    .line 2355716
    new-instance v1, Landroid/content/Intent;

    iget-object v2, v0, LX/GTe;->d:LX/0aU;

    const-string p0, "NEARBY_FRIENDS_SETTINGS_CHANGED_ACTION"

    invoke-virtual {v2, p0}, LX/0aU;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2355717
    iget-object v2, v0, LX/GTe;->c:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 2355718
    iget-boolean v0, p1, Lcom/facebook/fbservice/service/OperationResult;->success:Z

    move v0, v0

    .line 2355719
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method
