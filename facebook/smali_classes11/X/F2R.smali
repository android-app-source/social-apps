.class public final LX/F2R;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/F2S;


# direct methods
.method public constructor <init>(LX/F2S;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2193092
    iput-object p1, p0, LX/F2R;->b:LX/F2S;

    iput-object p2, p0, LX/F2R;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    .prologue
    .line 2193074
    iget-object v0, p0, LX/F2R;->b:LX/F2S;

    iget-object v0, v0, LX/F2S;->a:LX/F2U;

    iget-object v0, v0, LX/F2U;->a:LX/F2L;

    iget-object v1, p0, LX/F2R;->b:LX/F2S;

    iget-object v1, v1, LX/F2S;->a:LX/F2U;

    iget-object v1, v1, LX/F2U;->i:Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;

    invoke-virtual {v1}, Lcom/facebook/groups/editsettings/protocol/FetchGroupSettingsModels$FetchGroupSettingsModel;->q()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, LX/F2R;->a:Ljava/lang/String;

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v2

    iget-object v3, p0, LX/F2R;->b:LX/F2S;

    iget-object v3, v3, LX/F2S;->a:LX/F2U;

    iget-object v3, v3, LX/F2U;->b:LX/0gc;

    .line 2193075
    iget-object v4, v0, LX/F2L;->e:Landroid/content/res/Resources;

    const v5, 0x7f0831cf

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    const/4 p2, 0x0

    invoke-static {v4, v5, p2}, Lcom/facebook/ui/dialogs/ProgressDialogFragment;->a(Ljava/lang/String;ZZ)Landroid/support/v4/app/DialogFragment;

    move-result-object v4

    .line 2193076
    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Landroid/support/v4/app/DialogFragment;->a(LX/0gc;Ljava/lang/String;)V

    .line 2193077
    new-instance v5, LX/4Fk;

    invoke-direct {v5}, LX/4Fk;-><init>()V

    .line 2193078
    sget-object p2, LX/F2K;->a:[I

    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLGroupVisibility;->ordinal()I

    move-result v3

    aget p2, p2, v3

    packed-switch p2, :pswitch_data_0

    .line 2193079
    const/4 p2, 0x0

    :goto_0
    move-object p2, p2

    .line 2193080
    const-string v2, "visibility_setting"

    invoke-virtual {v5, v2, p2}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2193081
    move-object v5, v5

    .line 2193082
    new-instance p2, LX/F2G;

    invoke-direct {p2, v0, v4}, LX/F2G;-><init>(LX/F2L;Landroid/support/v4/app/DialogFragment;)V

    .line 2193083
    invoke-static {v0, v1, v5, p2}, LX/F2L;->a(LX/F2L;Ljava/lang/String;LX/4Fk;LX/0TF;)V

    .line 2193084
    iget-object v0, p0, LX/F2R;->b:LX/F2S;

    iget-object v0, v0, LX/F2S;->a:LX/F2U;

    iget-object v1, p0, LX/F2R;->a:Ljava/lang/String;

    .line 2193085
    iput-object v1, v0, LX/F2U;->h:Ljava/lang/String;

    .line 2193086
    iget-object v0, p0, LX/F2R;->b:LX/F2S;

    iget-object v0, v0, LX/F2S;->a:LX/F2U;

    invoke-static {v0}, LX/F2U;->b(LX/F2U;)V

    .line 2193087
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 2193088
    return-void

    .line 2193089
    :pswitch_0
    const-string p2, "OPEN"

    goto :goto_0

    .line 2193090
    :pswitch_1
    const-string p2, "CLOSED"

    goto :goto_0

    .line 2193091
    :pswitch_2
    const-string p2, "SECRET"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
