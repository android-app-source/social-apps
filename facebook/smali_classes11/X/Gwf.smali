.class public LX/Gwf;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static d:LX/0Xm;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/2Xn;

.field public final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/2Xn;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/2Xn;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2407217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2407218
    iput-object p2, p0, LX/Gwf;->a:LX/0Or;

    .line 2407219
    iput-object p3, p0, LX/Gwf;->b:LX/2Xn;

    .line 2407220
    sget-object v0, LX/26p;->f:LX/0Tn;

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/Gwf;->c:Ljava/lang/String;

    .line 2407221
    return-void
.end method

.method public static a(LX/0QB;)LX/Gwf;
    .locals 6

    .prologue
    .line 2407222
    const-class v1, LX/Gwf;

    monitor-enter v1

    .line 2407223
    :try_start_0
    sget-object v0, LX/Gwf;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2407224
    sput-object v2, LX/Gwf;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2407225
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2407226
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2407227
    new-instance v5, LX/Gwf;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v3

    check-cast v3, Lcom/facebook/prefs/shared/FbSharedPreferences;

    const/16 v4, 0xb83

    invoke-static {v0, v4}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object p0

    invoke-static {v0}, LX/2Xn;->b(LX/0QB;)LX/2Xn;

    move-result-object v4

    check-cast v4, LX/2Xn;

    invoke-direct {v5, v3, p0, v4}, LX/Gwf;-><init>(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Or;LX/2Xn;)V

    .line 2407228
    move-object v0, v5

    .line 2407229
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2407230
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gwf;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2407231
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2407232
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 8

    .prologue
    .line 2407233
    const/4 v4, 0x0

    .line 2407234
    new-instance v1, LX/28d;

    .line 2407235
    iget-object v2, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v2, v2

    .line 2407236
    const-string v3, "passwordCredentials"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/facebook/auth/credentials/PasswordCredentials;

    iget-object v3, p0, LX/Gwf;->c:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v6, v4

    move-object v7, v4

    invoke-direct/range {v1 .. v7}, LX/28d;-><init>(Lcom/facebook/auth/credentials/PasswordCredentials;Ljava/lang/String;Landroid/location/Location;ZLjava/lang/String;Ljava/lang/String;)V

    .line 2407237
    iget-object v2, p0, LX/Gwf;->a:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/11H;

    iget-object v3, p0, LX/Gwf;->b:LX/2Xn;

    invoke-virtual {v2, v3, v1}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2407238
    sget-object v1, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v1, v1

    .line 2407239
    move-object v0, v1

    .line 2407240
    return-object v0
.end method
