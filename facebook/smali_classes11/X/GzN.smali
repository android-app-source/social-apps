.class public final enum LX/GzN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GzN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GzN;

.field public static final enum Failed:LX/GzN;

.field public static final enum Successful:LX/GzN;

.field public static final enum Uploading:LX/GzN;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2411806
    new-instance v0, LX/GzN;

    const-string v1, "Uploading"

    invoke-direct {v0, v1, v2}, LX/GzN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GzN;->Uploading:LX/GzN;

    new-instance v0, LX/GzN;

    const-string v1, "Successful"

    invoke-direct {v0, v1, v3}, LX/GzN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GzN;->Successful:LX/GzN;

    new-instance v0, LX/GzN;

    const-string v1, "Failed"

    invoke-direct {v0, v1, v4}, LX/GzN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GzN;->Failed:LX/GzN;

    .line 2411807
    const/4 v0, 0x3

    new-array v0, v0, [LX/GzN;

    sget-object v1, LX/GzN;->Uploading:LX/GzN;

    aput-object v1, v0, v2

    sget-object v1, LX/GzN;->Successful:LX/GzN;

    aput-object v1, v0, v3

    sget-object v1, LX/GzN;->Failed:LX/GzN;

    aput-object v1, v0, v4

    sput-object v0, LX/GzN;->$VALUES:[LX/GzN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2411808
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GzN;
    .locals 1

    .prologue
    .line 2411809
    const-class v0, LX/GzN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GzN;

    return-object v0
.end method

.method public static values()[LX/GzN;
    .locals 1

    .prologue
    .line 2411810
    sget-object v0, LX/GzN;->$VALUES:[LX/GzN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GzN;

    return-object v0
.end method
