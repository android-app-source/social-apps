.class public final LX/G8q;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/G8p;

.field private b:LX/G96;


# direct methods
.method public constructor <init>(LX/G8p;)V
    .locals 2

    .prologue
    .line 2321249
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2321250
    if-nez p1, :cond_0

    .line 2321251
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Binarizer must be non-null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2321252
    :cond_0
    iput-object p1, p0, LX/G8q;->a:LX/G8p;

    .line 2321253
    return-void
.end method


# virtual methods
.method public final a()LX/G96;
    .locals 1

    .prologue
    .line 2321254
    iget-object v0, p0, LX/G8q;->b:LX/G96;

    if-nez v0, :cond_0

    .line 2321255
    iget-object v0, p0, LX/G8q;->a:LX/G8p;

    invoke-virtual {v0}, LX/G8p;->b()LX/G96;

    move-result-object v0

    iput-object v0, p0, LX/G8q;->b:LX/G96;

    .line 2321256
    :cond_0
    iget-object v0, p0, LX/G8q;->b:LX/G96;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2321257
    :try_start_0
    invoke-virtual {p0}, LX/G8q;->a()LX/G96;

    move-result-object v0

    invoke-virtual {v0}, LX/G96;->toString()Ljava/lang/String;
    :try_end_0
    .catch LX/G8x; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 2321258
    :goto_0
    return-object v0

    :catch_0
    const-string v0, ""

    goto :goto_0
.end method
