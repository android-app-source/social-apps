.class public LX/FsU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FrR;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/FsU;


# instance fields
.field private final a:LX/0tX;

.field private final b:LX/Fsj;

.field private final c:LX/FsT;

.field private final d:LX/0ad;


# direct methods
.method public constructor <init>(LX/0tX;LX/Fsj;LX/FsT;LX/0ad;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2297091
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2297092
    iput-object p1, p0, LX/FsU;->a:LX/0tX;

    .line 2297093
    iput-object p2, p0, LX/FsU;->b:LX/Fsj;

    .line 2297094
    iput-object p3, p0, LX/FsU;->c:LX/FsT;

    .line 2297095
    iput-object p4, p0, LX/FsU;->d:LX/0ad;

    .line 2297096
    return-void
.end method

.method public static a(LX/FsU;LX/0v6;LX/0zS;LX/G12;ILcom/facebook/common/callercontext/CallerContext;)LX/FsJ;
    .locals 17
    .param p0    # LX/FsU;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2297097
    const/4 v12, 0x0

    .line 2297098
    if-eqz p1, :cond_0

    invoke-virtual/range {p3 .. p3}, LX/G12;->c()Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, LX/0zS;->b:LX/0zS;

    move-object/from16 v0, p2

    if-eq v0, v4, :cond_0

    .line 2297099
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FsU;->c:LX/FsT;

    move-object/from16 v0, p1

    move/from16 v1, p4

    move-object/from16 v2, p3

    move-object/from16 v3, p5

    invoke-virtual {v4, v0, v1, v2, v3}, LX/FsT;->a(LX/0v6;ILX/G12;Lcom/facebook/common/callercontext/CallerContext;)LX/0zX;

    move-result-object v12

    .line 2297100
    :cond_0
    const/4 v6, 0x0

    .line 2297101
    const/4 v5, 0x0

    .line 2297102
    const/4 v10, 0x0

    .line 2297103
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FsU;->d:LX/0ad;

    sget-short v7, LX/0wf;->aO:S

    const/4 v8, 0x0

    invoke-interface {v4, v7, v8}, LX/0ad;->a(SZ)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2297104
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FsU;->c:LX/FsT;

    move-object/from16 v0, p3

    move-object/from16 v1, p2

    move/from16 v2, p4

    move-object/from16 v3, p5

    invoke-virtual {v4, v0, v1, v2, v3}, LX/FsT;->a(LX/G12;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v6

    .line 2297105
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FsU;->c:LX/FsT;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v6}, LX/FsT;->a(LX/0v6;LX/0zO;)LX/0zX;

    move-result-object v11

    .line 2297106
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FsU;->c:LX/FsT;

    move-object/from16 v5, p1

    move-object/from16 v7, p2

    move/from16 v8, p4

    move-object/from16 v9, p5

    invoke-virtual/range {v4 .. v9}, LX/FsT;->a(LX/0v6;LX/0zO;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zX;

    move-result-object v4

    move-object v14, v10

    move-object v15, v4

    move-object/from16 v16, v11

    .line 2297107
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FsU;->c:LX/FsT;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/FsU;->d:LX/0ad;

    move-object/from16 v0, p2

    invoke-static {v5, v0}, LX/Fsl;->a(LX/0ad;LX/0zS;)LX/0zS;

    move-result-object v7

    move-object/from16 v5, p1

    move-object/from16 v6, p3

    move/from16 v8, p4

    move-object/from16 v9, p5

    invoke-virtual/range {v4 .. v9}, LX/FsT;->a(LX/0v6;LX/G12;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zX;

    move-result-object v8

    .line 2297108
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FsU;->c:LX/FsT;

    move-object/from16 v0, p3

    move-object/from16 v1, p2

    move/from16 v2, p4

    move-object/from16 v3, p5

    invoke-virtual {v4, v0, v1, v2, v3}, LX/FsT;->c(LX/G12;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0Px;

    move-result-object v4

    .line 2297109
    move-object/from16 v0, p0

    iget-object v5, v0, LX/FsU;->c:LX/FsT;

    move-object/from16 v0, p1

    invoke-virtual {v5, v0, v4}, LX/FsT;->a(LX/0v6;Ljava/util/List;)LX/0zX;

    move-result-object v11

    .line 2297110
    new-instance v4, LX/FsJ;

    invoke-static {}, LX/0zX;->b()LX/0zX;

    move-result-object v9

    invoke-static {}, LX/0zX;->b()LX/0zX;

    move-result-object v10

    const/4 v13, 0x0

    move-object/from16 v5, v16

    move-object v6, v15

    move-object v7, v14

    invoke-direct/range {v4 .. v13}, LX/FsJ;-><init>(LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;LX/0zX;)V

    return-object v4

    .line 2297111
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, LX/FsU;->c:LX/FsT;

    move-object/from16 v0, p3

    move-object/from16 v1, p2

    move/from16 v2, p4

    move-object/from16 v3, p5

    invoke-virtual {v4, v0, v1, v2, v3}, LX/FsT;->b(LX/G12;LX/0zS;ILcom/facebook/common/callercontext/CallerContext;)LX/0zO;

    move-result-object v4

    .line 2297112
    move-object/from16 v0, p0

    iget-object v7, v0, LX/FsU;->b:LX/Fsj;

    move-object/from16 v0, p1

    invoke-virtual {v7, v0, v4}, LX/Fsj;->a(LX/0v6;LX/0zO;)LX/0zX;

    move-result-object v4

    move-object v14, v4

    move-object v15, v5

    move-object/from16 v16, v6

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/FsU;
    .locals 3

    .prologue
    .line 2297113
    sget-object v0, LX/FsU;->e:LX/FsU;

    if-nez v0, :cond_1

    .line 2297114
    const-class v1, LX/FsU;

    monitor-enter v1

    .line 2297115
    :try_start_0
    sget-object v0, LX/FsU;->e:LX/FsU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2297116
    if-eqz v2, :cond_0

    .line 2297117
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    invoke-static {v0}, LX/FsU;->b(LX/0QB;)LX/FsU;

    move-result-object v0

    sput-object v0, LX/FsU;->e:LX/FsU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2297118
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2297119
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2297120
    :cond_1
    sget-object v0, LX/FsU;->e:LX/FsU;

    return-object v0

    .line 2297121
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2297122
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/FsU;
    .locals 14

    .prologue
    .line 2297123
    new-instance v4, LX/FsU;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v0

    check-cast v0, LX/0tX;

    invoke-static {p0}, LX/Fsj;->a(LX/0QB;)LX/Fsj;

    move-result-object v1

    check-cast v1, LX/Fsj;

    .line 2297124
    new-instance v5, LX/FsT;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v6

    check-cast v6, LX/0ad;

    invoke-static {p0}, LX/Fsm;->a(LX/0QB;)LX/Fsm;

    move-result-object v7

    check-cast v7, LX/Fsm;

    invoke-static {p0}, LX/Fsg;->a(LX/0QB;)LX/Fsg;

    move-result-object v8

    check-cast v8, LX/Fsg;

    invoke-static {p0}, LX/FsI;->a(LX/0QB;)LX/FsI;

    move-result-object v9

    check-cast v9, LX/FsI;

    invoke-static {p0}, LX/G2W;->a(LX/0QB;)LX/G2W;

    move-result-object v10

    check-cast v10, LX/G2W;

    invoke-static {p0}, LX/FsX;->b(LX/0QB;)LX/FsX;

    move-result-object v11

    check-cast v11, LX/FsX;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v12

    check-cast v12, LX/0tX;

    invoke-static {p0}, LX/G2L;->a(LX/0QB;)LX/G2L;

    move-result-object v13

    check-cast v13, LX/G2L;

    invoke-direct/range {v5 .. v13}, LX/FsT;-><init>(LX/0ad;LX/Fsm;LX/Fsg;LX/FsI;LX/G2W;LX/FsX;LX/0tX;LX/G2L;)V

    .line 2297125
    move-object v2, v5

    .line 2297126
    check-cast v2, LX/FsT;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-direct {v4, v0, v1, v2, v3}, LX/FsU;-><init>(LX/0tX;LX/Fsj;LX/FsT;LX/0ad;)V

    .line 2297127
    return-object v4
.end method


# virtual methods
.method public final a(LX/0v6;IZLX/G12;Lcom/facebook/common/callercontext/CallerContext;)LX/FsJ;
    .locals 10

    .prologue
    .line 2297128
    iget-object v0, p0, LX/FsU;->d:LX/0ad;

    sget-short v1, LX/0wf;->aq:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2297129
    if-eqz p3, :cond_2

    .line 2297130
    sget-object v5, LX/0zS;->d:LX/0zS;

    move-object v3, p0

    move-object v4, p1

    move-object v6, p4

    move v7, p2

    move-object v8, p5

    invoke-static/range {v3 .. v8}, LX/FsU;->a(LX/FsU;LX/0v6;LX/0zS;LX/G12;ILcom/facebook/common/callercontext/CallerContext;)LX/FsJ;

    move-result-object v3

    .line 2297131
    :goto_0
    move-object v0, v3

    .line 2297132
    :goto_1
    return-object v0

    .line 2297133
    :cond_0
    if-eqz p3, :cond_3

    sget-object v5, LX/0zS;->d:LX/0zS;

    :goto_2
    move-object v3, p0

    move-object v4, p1

    move-object v6, p4

    move v7, p2

    move-object v8, p5

    .line 2297134
    invoke-static/range {v3 .. v8}, LX/FsU;->a(LX/FsU;LX/0v6;LX/0zS;LX/G12;ILcom/facebook/common/callercontext/CallerContext;)LX/FsJ;

    move-result-object v3

    .line 2297135
    if-nez p3, :cond_1

    .line 2297136
    invoke-static {p4, p5, v3, p0}, LX/Fs8;->a(LX/G12;Lcom/facebook/common/callercontext/CallerContext;LX/FsJ;LX/FrR;)LX/FsJ;

    move-result-object v3

    .line 2297137
    :cond_1
    move-object v0, v3

    .line 2297138
    goto :goto_1

    .line 2297139
    :cond_2
    const/4 v4, 0x0

    sget-object v5, LX/0zS;->b:LX/0zS;

    move-object v3, p0

    move-object v6, p4

    move v7, p2

    move-object v8, p5

    invoke-static/range {v3 .. v8}, LX/FsU;->a(LX/FsU;LX/0v6;LX/0zS;LX/G12;ILcom/facebook/common/callercontext/CallerContext;)LX/FsJ;

    move-result-object v9

    .line 2297140
    sget-object v5, LX/0zS;->d:LX/0zS;

    move-object v3, p0

    move-object v4, p1

    move-object v6, p4

    move v7, p2

    move-object v8, p5

    invoke-static/range {v3 .. v8}, LX/FsU;->a(LX/FsU;LX/0v6;LX/0zS;LX/G12;ILcom/facebook/common/callercontext/CallerContext;)LX/FsJ;

    move-result-object v3

    .line 2297141
    invoke-static {v9, v3}, LX/Fs5;->a(LX/FsJ;LX/FsJ;)LX/FsJ;

    move-result-object v3

    goto :goto_0

    .line 2297142
    :cond_3
    sget-object v5, LX/0zS;->a:LX/0zS;

    goto :goto_2
.end method

.method public final a(ZLX/G12;Lcom/facebook/common/callercontext/CallerContext;)LX/FsJ;
    .locals 6

    .prologue
    .line 2297143
    new-instance v1, LX/0v6;

    const-string v0, "TimelineNonSelfFirstUnits"

    invoke-direct {v1, v0}, LX/0v6;-><init>(Ljava/lang/String;)V

    .line 2297144
    const/4 v2, 0x0

    move-object v0, p0

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, LX/FsU;->a(LX/0v6;IZLX/G12;Lcom/facebook/common/callercontext/CallerContext;)LX/FsJ;

    move-result-object v0

    .line 2297145
    iget-object v2, p0, LX/FsU;->a:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0v6;)V

    .line 2297146
    return-object v0
.end method
