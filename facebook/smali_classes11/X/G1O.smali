.class public final LX/G1O;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptModel;",
        "LX/G1P;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2311083
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 2311084
    check-cast p1, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptModel;

    .line 2311085
    invoke-virtual {p1}, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptModel;->a()Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;

    move-result-object v0

    .line 2311086
    new-instance v1, LX/G1P;

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;->a()Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptApproximateCountFieldsModel;

    move-result-object v2

    invoke-static {v2}, LX/0am;->of(Ljava/lang/Object;)LX/0am;

    move-result-object v2

    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v3

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/facebook/timeline/protocol/FetchTimelinePromptGraphQLModels$TimelinePromptFieldsModel;->c()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    invoke-direct/range {v1 .. v6}, LX/G1P;-><init>(LX/0am;LX/0am;Ljava/lang/String;Ljava/lang/String;Z)V

    move-object v0, v1

    .line 2311087
    return-object v0
.end method
