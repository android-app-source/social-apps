.class public final LX/F8I;
.super Landroid/widget/Filter;
.source ""


# instance fields
.field public final synthetic a:LX/F8M;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "LX/F8P;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/F8M;)V
    .locals 0

    .prologue
    .line 2203948
    iput-object p1, p0, LX/F8I;->a:LX/F8M;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method public final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 2203926
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2203927
    :cond_0
    return-object v6

    .line 2203928
    :cond_1
    iget-object v0, p0, LX/F8I;->a:LX/F8M;

    iget-object v0, v0, LX/F8M;->c:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 2203929
    if-nez v0, :cond_4

    .line 2203930
    iget-object v0, p0, LX/F8I;->a:LX/F8M;

    iget-object v0, v0, LX/F8M;->d:LX/0W9;

    invoke-virtual {v0}, LX/0W9;->a()Ljava/util/Locale;

    move-result-object v0

    move-object v1, v0

    .line 2203931
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, LX/F8I;->a:LX/F8M;

    iget-object v2, v2, LX/F8M;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, LX/F8I;->b:Ljava/util/List;

    .line 2203932
    iget-object v0, p0, LX/F8I;->a:LX/F8M;

    iget-object v0, v0, LX/F8M;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F8P;

    .line 2203933
    iget-object v3, v0, LX/F8P;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2203934
    invoke-virtual {v3, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v3

    .line 2203935
    iget-object v4, v0, LX/F8P;->c:Ljava/lang/String;

    move-object v4, v4

    .line 2203936
    invoke-virtual {v4, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    .line 2203937
    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_5

    :cond_3
    const/4 v7, 0x1

    :goto_2
    move v3, v7

    .line 2203938
    if-eqz v3, :cond_2

    .line 2203939
    iget-object v3, p0, LX/F8I;->b:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_0

    :cond_5
    const/4 v7, 0x0

    goto :goto_2
.end method

.method public final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2

    .prologue
    .line 2203940
    invoke-static {p1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2203941
    iget-object v0, p0, LX/F8I;->a:LX/F8M;

    iget-object v1, p0, LX/F8I;->a:LX/F8M;

    iget-object v1, v1, LX/F8M;->a:Ljava/util/List;

    .line 2203942
    iput-object v1, v0, LX/F8M;->k:Ljava/util/List;

    .line 2203943
    :goto_0
    iget-object v0, p0, LX/F8I;->a:LX/F8M;

    invoke-virtual {v0}, LX/1OM;->notifyDataSetChanged()V

    .line 2203944
    return-void

    .line 2203945
    :cond_0
    iget-object v0, p0, LX/F8I;->a:LX/F8M;

    iget-object v1, p0, LX/F8I;->b:Ljava/util/List;

    .line 2203946
    iput-object v1, v0, LX/F8M;->k:Ljava/util/List;

    .line 2203947
    goto :goto_0
.end method
