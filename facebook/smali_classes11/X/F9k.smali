.class public LX/F9k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static k:LX/0Xm;


# instance fields
.field public final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/30Z;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/30Y;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EeJ;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EeH;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EeE;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/EeB;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/F9e;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/F9h;",
            ">;"
        }
    .end annotation
.end field

.field public final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/F9j;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Or;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/30Z;",
            ">;",
            "LX/0Or",
            "<",
            "Lcom/facebook/http/protocol/SingleMethodRunner;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/30Y;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EeJ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EeH;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EeE;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/EeB;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/F9e;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/F9h;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/F9j;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2205839
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2205840
    iput-object p1, p0, LX/F9k;->a:LX/0Or;

    .line 2205841
    iput-object p2, p0, LX/F9k;->b:LX/0Or;

    .line 2205842
    iput-object p3, p0, LX/F9k;->c:LX/0Ot;

    .line 2205843
    iput-object p4, p0, LX/F9k;->d:LX/0Ot;

    .line 2205844
    iput-object p5, p0, LX/F9k;->e:LX/0Ot;

    .line 2205845
    iput-object p6, p0, LX/F9k;->f:LX/0Ot;

    .line 2205846
    iput-object p7, p0, LX/F9k;->g:LX/0Ot;

    .line 2205847
    iput-object p8, p0, LX/F9k;->h:LX/0Ot;

    .line 2205848
    iput-object p9, p0, LX/F9k;->i:LX/0Ot;

    .line 2205849
    iput-object p10, p0, LX/F9k;->j:LX/0Ot;

    .line 2205850
    return-void
.end method

.method public static a(LX/0QB;)LX/F9k;
    .locals 14

    .prologue
    .line 2205851
    const-class v1, LX/F9k;

    monitor-enter v1

    .line 2205852
    :try_start_0
    sget-object v0, LX/F9k;->k:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2205853
    sput-object v2, LX/F9k;->k:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2205854
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2205855
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2205856
    new-instance v3, LX/F9k;

    const/16 v4, 0x3fb

    invoke-static {v0, v4}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v4

    const/16 v5, 0xb83

    invoke-static {v0, v5}, LX/0SL;->a(LX/0QB;I)LX/0Or;

    move-result-object v5

    const/16 v6, 0x3ff

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x1764

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x1763

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    const/16 v9, 0x1762

    invoke-static {v0, v9}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v9

    const/16 v10, 0x1761

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x24d0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x24d1

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x24d2

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-direct/range {v3 .. v13}, LX/F9k;-><init>(LX/0Or;LX/0Or;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2205857
    move-object v0, v3

    .line 2205858
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2205859
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/F9k;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2205860
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2205861
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2205862
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2205863
    const-string v1, "growth_set_profile_photo"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2205864
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2205865
    const-string v1, "growthSetProfilePhotoParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/growth/profile/SetProfilePhotoParams;

    .line 2205866
    iget-object v1, p0, LX/F9k;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, LX/F9k;->d:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0e6;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2205867
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2205868
    move-object v0, v0

    .line 2205869
    :goto_0
    return-object v0

    .line 2205870
    :cond_0
    const-string v1, "growth_set_native_name"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2205871
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2205872
    const-string v1, "growthSetNativeNameParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/growth/profile/SetNativeNameParams;

    .line 2205873
    iget-object v1, p0, LX/F9k;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, LX/F9k;->e:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0e6;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/String;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 2205874
    goto :goto_0

    .line 2205875
    :cond_1
    const-string v1, "growth_users_invite"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2205876
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2205877
    const-string v1, "growthUsersInviteParamsKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/growth/contactimporter/UsersInviteParams;

    .line 2205878
    iget-object v1, p0, LX/F9k;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, LX/F9k;->f:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0e6;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/util/ArrayList;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 2205879
    goto :goto_0

    .line 2205880
    :cond_2
    const-string v1, "growth_user_set_contact_info"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2205881
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2205882
    const-string v1, "growthUserSetContactInfoParamsKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/api/growth/UserSetContactInfoMethod$Params;

    .line 2205883
    iget-object v1, p0, LX/F9k;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, LX/F9k;->g:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0e6;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2205884
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2205885
    move-object v0, v0

    .line 2205886
    goto/16 :goto_0

    .line 2205887
    :cond_3
    const-string v1, "growth_friend_finder"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2205888
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2205889
    const-string v1, "growthFriendFinderParamsKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/protocol/FriendFinderMethod$Params;

    .line 2205890
    iget-object v1, p0, LX/F9k;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, LX/F9k;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0e6;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/protocol/FriendFinderMethod$Result;

    .line 2205891
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 2205892
    goto/16 :goto_0

    .line 2205893
    :cond_4
    const-string v1, "growth_friend_finder_pymk"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2205894
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2205895
    const-string v1, "growthFriendFinderPYMKParamsKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Params;

    .line 2205896
    iget-object v1, p0, LX/F9k;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, LX/F9k;->i:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0e6;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/growth/protocol/FriendFinderPYMKMethod$Result;

    .line 2205897
    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 2205898
    goto/16 :goto_0

    .line 2205899
    :cond_5
    const-string v1, "growth_set_continuous_contacts_upload"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2205900
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2205901
    const-string v1, "growthSetContinuousContactsUploadParamsKey"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, LX/F9i;

    .line 2205902
    iget-object v1, p0, LX/F9k;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/30Y;

    invoke-virtual {v1}, LX/30Y;->a()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2205903
    sget-object v1, LX/F9i;->ON:LX/F9i;

    if-ne v0, v1, :cond_8

    const-string v1, "ci"

    move-object v2, v1

    .line 2205904
    :goto_1
    iget-object v1, p0, LX/F9k;->a:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/30Z;

    sget-object v3, LX/F9i;->ON:LX/F9i;

    if-ne v0, v3, :cond_9

    const-string v3, "on"

    :goto_2
    invoke-virtual {v1, v3, v2}, LX/30Z;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2205905
    sget-object v1, LX/F9i;->ON:LX/F9i;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, LX/F9k;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/30Y;

    invoke-virtual {v0}, LX/30Y;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 2205906
    iget-object v0, p0, LX/F9k;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/30Z;

    const-string v1, "FB_NUX_CI"

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, LX/30Z;->a(Ljava/lang/String;ZI)Z

    .line 2205907
    :cond_6
    :goto_3
    sget-object v0, Lcom/facebook/fbservice/service/OperationResult;->SUCCESS_RESULT_EMPTY:Lcom/facebook/fbservice/service/OperationResult;

    move-object v0, v0

    .line 2205908
    move-object v0, v0

    .line 2205909
    goto/16 :goto_0

    .line 2205910
    :cond_7
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "Unknown type"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2205911
    :cond_8
    const-string v1, "user_setting"

    move-object v2, v1

    goto :goto_1

    .line 2205912
    :cond_9
    const-string v3, "off"

    goto :goto_2

    .line 2205913
    :cond_a
    iget-object v1, p0, LX/F9k;->b:LX/0Or;

    invoke-interface {v1}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/11H;

    iget-object v2, p0, LX/F9k;->j:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0e6;

    .line 2205914
    iget-object v3, p1, LX/1qK;->mCallerContext:Lcom/facebook/common/callercontext/CallerContext;

    move-object v3, v3

    .line 2205915
    invoke-virtual {v1, v2, v0, v3}, LX/11H;->a(LX/0e6;Ljava/lang/Object;Lcom/facebook/common/callercontext/CallerContext;)Ljava/lang/Object;

    goto :goto_3
.end method
