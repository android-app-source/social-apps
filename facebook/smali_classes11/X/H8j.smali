.class public final LX/H8j;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8E8;


# instance fields
.field public final synthetic a:LX/0Rl;

.field public final synthetic b:LX/4At;

.field public final synthetic c:LX/H8k;


# direct methods
.method public constructor <init>(LX/H8k;LX/0Rl;LX/4At;)V
    .locals 0

    .prologue
    .line 2433605
    iput-object p1, p0, LX/H8j;->c:LX/H8k;

    iput-object p2, p0, LX/H8j;->a:LX/0Rl;

    iput-object p3, p0, LX/H8j;->b:LX/4At;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2433606
    iget-object v0, p0, LX/H8j;->c:LX/H8k;

    iget-object v0, v0, LX/H8k;->k:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 2433607
    iget-object v0, p0, LX/H8j;->b:LX/4At;

    invoke-virtual {v0}, LX/4At;->beginShowingProgress()V

    .line 2433608
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 1

    .prologue
    .line 2433609
    iget-object v0, p0, LX/H8j;->a:LX/0Rl;

    invoke-interface {v0, p1}, LX/0Rl;->apply(Ljava/lang/Object;)Z

    .line 2433610
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2433611
    iget-object v0, p0, LX/H8j;->c:LX/H8k;

    iget-object v0, v0, LX/H8k;->k:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 2433612
    iget-object v0, p0, LX/H8j;->b:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2433613
    iget-object v0, p0, LX/H8j;->c:LX/H8k;

    iget-object v0, v0, LX/H8k;->i:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/H8j;->c:LX/H8k;

    iget-object v2, v2, LX/H8k;->k:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081829

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2433614
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 1

    .prologue
    .line 2433615
    iget-object v0, p0, LX/H8j;->c:LX/H8k;

    iget-object v0, v0, LX/H8k;->k:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 2433616
    iget-object v0, p0, LX/H8j;->b:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2433617
    iget-object v0, p0, LX/H8j;->a:LX/0Rl;

    invoke-interface {v0, p1}, LX/0Rl;->apply(Ljava/lang/Object;)Z

    .line 2433618
    :cond_0
    return-void
.end method
