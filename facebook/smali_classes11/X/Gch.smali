.class public LX/Gch;
.super LX/0Wl;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Wl",
        "<",
        "LX/Gcg;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2372251
    invoke-direct {p0}, LX/0Wl;-><init>()V

    .line 2372252
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)LX/Gcg;
    .locals 17

    .prologue
    .line 2372249
    new-instance v1, LX/Gcg;

    invoke-static/range {p0 .. p0}, LX/0Zr;->a(LX/0QB;)LX/0Zr;

    move-result-object v3

    check-cast v3, LX/0Zr;

    invoke-static/range {p0 .. p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v4

    check-cast v4, Landroid/content/res/Resources;

    invoke-static/range {p0 .. p0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0UA;->a(LX/0QB;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static/range {p0 .. p0}, LX/0Ss;->a(LX/0QB;)Landroid/os/Handler;

    move-result-object v7

    check-cast v7, Landroid/os/Handler;

    const-class v2, LX/6KO;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v8

    check-cast v8, LX/6KO;

    invoke-static/range {p0 .. p0}, LX/6KT;->a(LX/0QB;)LX/6KT;

    move-result-object v9

    check-cast v9, LX/6KT;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, Lcom/facebook/facedetection/detector/MacerFaceDetector;->a(LX/0QB;)Lcom/facebook/facedetection/detector/MacerFaceDetector;

    move-result-object v11

    check-cast v11, Lcom/facebook/facedetection/detector/MacerFaceDetector;

    invoke-static/range {p0 .. p0}, LX/7yL;->a(LX/0QB;)LX/7yL;

    move-result-object v12

    check-cast v12, LX/7yL;

    invoke-static/range {p0 .. p0}, LX/10M;->a(LX/0QB;)LX/10M;

    move-result-object v13

    check-cast v13, LX/10M;

    invoke-static/range {p0 .. p0}, LX/10O;->a(LX/0QB;)LX/10O;

    move-result-object v14

    check-cast v14, LX/10O;

    const-class v2, LX/2J4;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v15

    check-cast v15, LX/2J4;

    invoke-static/range {p0 .. p0}, LX/1Et;->a(LX/0QB;)LX/1FZ;

    move-result-object v16

    check-cast v16, LX/1FZ;

    move-object/from16 v2, p1

    invoke-direct/range {v1 .. v16}, LX/Gcg;-><init>(Ljava/lang/String;LX/0Zr;Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;Ljava/util/concurrent/ExecutorService;Landroid/os/Handler;LX/6KO;LX/6KT;Landroid/content/Context;Lcom/facebook/facedetection/detector/MacerFaceDetector;LX/7yL;LX/10M;LX/10O;LX/2J4;LX/1FZ;)V

    .line 2372250
    return-object v1
.end method
