.class public final LX/GWV;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2362091
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_5

    .line 2362092
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2362093
    :goto_0
    return v1

    .line 2362094
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2362095
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->END_OBJECT:LX/15z;

    if-eq v4, v5, :cond_4

    .line 2362096
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v4

    .line 2362097
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2362098
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v5

    sget-object v6, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v5, v6, :cond_1

    if-eqz v4, :cond_1

    .line 2362099
    const-string v5, "feedback"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2362100
    invoke-static {p0, p1}, LX/GWe;->a(LX/15w;LX/186;)I

    move-result v3

    goto :goto_1

    .line 2362101
    :cond_2
    const-string v5, "product_items_edge"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2362102
    invoke-static {p0, p1}, LX/GWU;->a(LX/15w;LX/186;)I

    move-result v2

    goto :goto_1

    .line 2362103
    :cond_3
    const-string v5, "variant_labels"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2362104
    invoke-static {p0, p1}, LX/2gu;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2362105
    :cond_4
    const/4 v4, 0x3

    invoke-virtual {p1, v4}, LX/186;->c(I)V

    .line 2362106
    invoke-virtual {p1, v1, v3}, LX/186;->b(II)V

    .line 2362107
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2362108
    const/4 v1, 0x2

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2362109
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_5
    move v0, v1

    move v2, v1

    move v3, v1

    goto :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 2362110
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2362111
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2362112
    if-eqz v0, :cond_0

    .line 2362113
    const-string v1, "feedback"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362114
    invoke-static {p0, v0, p2, p3}, LX/GWe;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2362115
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2362116
    if-eqz v0, :cond_1

    .line 2362117
    const-string v1, "product_items_edge"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362118
    invoke-static {p0, v0, p2, p3}, LX/GWU;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2362119
    :cond_1
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2362120
    if-eqz v0, :cond_2

    .line 2362121
    const-string v0, "variant_labels"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2362122
    invoke-virtual {p0, p1, v2}, LX/15i;->f(II)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0, p2}, LX/2bt;->a(Ljava/util/Iterator;LX/0nX;)V

    .line 2362123
    :cond_2
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2362124
    return-void
.end method
