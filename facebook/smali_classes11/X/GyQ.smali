.class public LX/GyQ;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/GyO;",
            ">;"
        }
    .end annotation
.end field

.field private static c:LX/0Xm;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/local/surface/PlaceComponentSpec;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2410079
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/GyQ;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/local/surface/PlaceComponentSpec;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2410080
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2410081
    iput-object p1, p0, LX/GyQ;->b:LX/0Ot;

    .line 2410082
    return-void
.end method

.method public static a(LX/0QB;)LX/GyQ;
    .locals 4

    .prologue
    .line 2410083
    const-class v1, LX/GyQ;

    monitor-enter v1

    .line 2410084
    :try_start_0
    sget-object v0, LX/GyQ;->c:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2410085
    sput-object v2, LX/GyQ;->c:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2410086
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2410087
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2410088
    new-instance v3, LX/GyQ;

    const/16 p0, 0x25f1

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/GyQ;-><init>(LX/0Ot;)V

    .line 2410089
    move-object v0, v3

    .line 2410090
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2410091
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/GyQ;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2410092
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2410093
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static d(LX/1De;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2410094
    const v0, 0x4a12abea    # 2403066.5f

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 12

    .prologue
    .line 2410095
    check-cast p2, LX/GyP;

    .line 2410096
    iget-object v0, p0, LX/GyQ;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/local/surface/PlaceComponentSpec;

    iget-object v1, p2, LX/GyP;->a:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;

    const/4 v3, 0x0

    const/16 v8, 0x8

    const/4 v7, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 2410097
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->m()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, LX/1Dh;->b(Ljava/lang/CharSequence;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v8, v8}, LX/1Dh;->v(II)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1um;->c(LX/1De;)LX/1up;

    move-result-object v5

    iget-object v2, v0, Lcom/facebook/local/surface/PlaceComponentSpec;->b:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/1Ad;

    invoke-virtual {v1}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, LX/1Ad;->b(Ljava/lang/String;)LX/1Ad;

    move-result-object v2

    sget-object v6, Lcom/facebook/local/surface/PlaceComponentSpec;->a:Lcom/facebook/common/callercontext/CallerContext;

    invoke-virtual {v2, v6}, LX/1Ad;->a(Lcom/facebook/common/callercontext/CallerContext;)LX/1Ad;

    move-result-object v2

    invoke-virtual {v2}, LX/1Ad;->q()Lcom/facebook/drawee/fbpipeline/FbPipelineDraweeController;

    move-result-object v2

    invoke-virtual {v5, v2}, LX/1up;->a(LX/1aZ;)LX/1up;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->c()I

    move-result v5

    invoke-interface {v2, v5}, LX/1Di;->g(I)LX/1Di;

    move-result-object v2

    invoke-virtual {v1}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->a()I

    move-result v5

    invoke-interface {v2, v5}, LX/1Di;->g(I)LX/1Di;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Di;->b(I)LX/1Di;

    move-result-object v2

    const/4 v5, 0x5

    invoke-interface {v2, v5, v8}, LX/1Di;->d(II)LX/1Di;

    move-result-object v2

    invoke-static {p1}, LX/GyQ;->d(LX/1De;)LX/1dQ;

    move-result-object v5

    invoke-interface {v2, v5}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v9}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v2

    invoke-interface {v2, v7}, LX/1Dh;->B(I)LX/1Dh;

    move-result-object v2

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v2, v5}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/GyQ;->d(LX/1De;)LX/1dQ;

    move-result-object v5

    invoke-interface {v2, v5}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    iget-object v6, v0, Lcom/facebook/local/surface/PlaceComponentSpec;->c:LX/GyN;

    new-instance v7, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->m()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->t()Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-result-object v8

    .line 2410098
    sget-object v11, LX/GyM;->a:[I

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;->ordinal()I

    move-result p0

    aget v11, v11, p0

    packed-switch v11, :pswitch_data_0

    .line 2410099
    const/4 v11, 0x0

    :goto_0
    move-object v11, v11

    .line 2410100
    if-eqz v11, :cond_0

    .line 2410101
    const-string p0, "\u2060"

    invoke-virtual {v7, p0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object p0

    sget-object p2, LX/GyN;->a:Ljava/lang/CharSequence;

    invoke-virtual {p0, p2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2410102
    new-instance p0, Landroid/text/style/ImageSpan;

    const/4 p2, 0x1

    invoke-direct {p0, v11, p2}, Landroid/text/style/ImageSpan;-><init>(Landroid/graphics/drawable/Drawable;I)V

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result p2

    sget-object v6, LX/GyN;->a:Ljava/lang/CharSequence;

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    sub-int/2addr p2, v6

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    const/16 v8, 0x11

    invoke-virtual {v7, p0, p2, v6, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 2410103
    :cond_0
    move-object v6, v7

    .line 2410104
    invoke-virtual {v5, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v10}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b0052

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a00ab

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v9}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    invoke-interface {v2, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v5

    iget-object v6, v0, Lcom/facebook/local/surface/PlaceComponentSpec;->c:LX/GyN;

    .line 2410105
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    .line 2410106
    invoke-virtual {v1}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->n()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;

    move-result-object v7

    .line 2410107
    invoke-virtual {v1}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->o()Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$PlaceOpenStatusModel;

    .line 2410108
    invoke-virtual {v1}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->p()Lcom/facebook/graphql/enums/GraphQLPageOpenHoursDisplayDecisionEnum;

    .line 2410109
    new-instance v8, Landroid/text/SpannableStringBuilder;

    invoke-direct {v8}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2410110
    invoke-static {v6, p1, v7}, LX/GyN;->a(LX/GyN;Landroid/content/Context;Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel$OverallStarRatingModel;)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-static {v8, v7}, LX/GyN;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2410111
    invoke-virtual {v1}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->s()LX/0Px;

    move-result-object v7

    const/4 v11, 0x0

    invoke-virtual {v7, v11}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/CharSequence;

    invoke-static {v8, v7}, LX/GyN;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2410112
    invoke-virtual {v1}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->q()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, LX/GyN;->a(Landroid/text/SpannableStringBuilder;Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2410113
    move-object v6, v8

    .line 2410114
    invoke-virtual {v5, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v10}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v5

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v5

    const v6, 0x7f0b0050

    invoke-virtual {v5, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v5

    const v6, 0x7f0a00a4

    invoke-virtual {v5, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v5

    invoke-virtual {v5, v9}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    invoke-interface {v2, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v5

    iget-object v2, v0, Lcom/facebook/local/surface/PlaceComponentSpec;->c:LX/GyN;

    invoke-virtual {v2, v1}, LX/GyN;->a(Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object v2, v3

    :goto_1
    invoke-interface {v5, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-static {p1, v1}, LX/GyN;->a(Landroid/content/Context;Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-static {v5}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    :goto_2
    invoke-interface {v2, v3}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v4, v2}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v2

    invoke-interface {v2}, LX/1Di;->k()LX/1Dg;

    move-result-object v2

    move-object v0, v2

    .line 2410115
    return-object v0

    :cond_1
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v2

    iget-object v6, v0, Lcom/facebook/local/surface/PlaceComponentSpec;->c:LX/GyN;

    invoke-virtual {v6, v1}, LX/GyN;->a(Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v2, v6}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v10}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v2

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v6}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v2

    const v6, 0x7f0b0050

    invoke-virtual {v2, v6}, LX/1ne;->q(I)LX/1ne;

    move-result-object v2

    const v6, 0x7f0a00a4

    invoke-virtual {v2, v6}, LX/1ne;->n(I)LX/1ne;

    move-result-object v2

    invoke-virtual {v2, v9}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v2

    invoke-virtual {v2}, LX/1X5;->c()LX/1Di;

    move-result-object v2

    goto :goto_1

    :cond_2
    invoke-static {p1}, LX/1na;->c(LX/1De;)LX/1ne;

    move-result-object v3

    invoke-static {p1, v1}, LX/GyN;->a(Landroid/content/Context;Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v3, v5}, LX/1ne;->a(Ljava/lang/CharSequence;)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v10}, LX/1ne;->b(Z)LX/1ne;

    move-result-object v3

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v3, v5}, LX/1ne;->a(Landroid/text/TextUtils$TruncateAt;)LX/1ne;

    move-result-object v3

    const v5, 0x7f0b0050

    invoke-virtual {v3, v5}, LX/1ne;->q(I)LX/1ne;

    move-result-object v3

    const v5, 0x7f0a00a4

    invoke-virtual {v3, v5}, LX/1ne;->n(I)LX/1ne;

    move-result-object v3

    invoke-virtual {v3, v9}, LX/1ne;->a(Z)LX/1ne;

    move-result-object v3

    invoke-virtual {v3}, LX/1X5;->c()LX/1Di;

    move-result-object v3

    goto :goto_2

    .line 2410116
    :pswitch_0
    iget-object v11, v6, LX/GyN;->c:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    .line 2410117
    :pswitch_1
    iget-object v11, v6, LX/GyN;->b:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2410118
    invoke-static {}, LX/1dS;->b()V

    .line 2410119
    iget v0, p1, LX/1dQ;->b:I

    .line 2410120
    packed-switch v0, :pswitch_data_0

    .line 2410121
    :goto_0
    return-object v2

    .line 2410122
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2410123
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2410124
    check-cast v1, LX/GyP;

    .line 2410125
    iget-object v3, p0, LX/GyQ;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/local/surface/PlaceComponentSpec;

    iget-object v4, v1, LX/GyP;->a:Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;

    .line 2410126
    invoke-virtual {v4}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->k()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    .line 2410127
    :goto_1
    goto :goto_0

    .line 2410128
    :cond_0
    iget-object p1, v3, Lcom/facebook/local/surface/PlaceComponentSpec;->d:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    const p1, 0x25d6af

    const/4 p2, 0x1

    new-array p2, p2, [Ljava/lang/Object;

    const/4 p0, 0x0

    invoke-virtual {v4}, Lcom/facebook/local/surface/LocalDiscoveryQueryModels$PlaceNodeModel;->k()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p2, p0

    invoke-static {p1, p2}, LX/1nG;->a(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    .line 2410129
    iget-object p1, v3, Lcom/facebook/local/surface/PlaceComponentSpec;->e:LX/0Ot;

    invoke-interface {p1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, LX/17W;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p0

    invoke-virtual {p1, p0, p2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x4a12abea
        :pswitch_0
    .end packed-switch
.end method
