.class public final LX/GTY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0QK;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0QK",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;",
        ">;",
        "Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLInterfaces$BackgroundLocationNuxFriendsSharingLocation;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/GTZ;


# direct methods
.method public constructor <init>(LX/GTZ;)V
    .locals 0

    .prologue
    .line 2355584
    iput-object p1, p0, LX/GTY;->a:LX/GTZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final apply(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2355585
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    const/4 v1, 0x0

    .line 2355586
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2355587
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2355588
    check-cast v0, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;

    .line 2355589
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel;->j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;

    move-result-object v0

    .line 2355590
    if-nez v0, :cond_0

    move-object v0, v1

    .line 2355591
    :goto_0
    return-object v0

    .line 2355592
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->k()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2355593
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->k()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    move-result-object v0

    goto :goto_0

    .line 2355594
    :cond_1
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 2355595
    invoke-virtual {v0}, Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxQueryModel$LocationSharingModel;->j()Lcom/facebook/backgroundlocation/nux/graphql/BackgroundLocationNUXGraphQLModels$BackgroundLocationNuxFriendsSharingLocationModel;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 2355596
    goto :goto_0
.end method
