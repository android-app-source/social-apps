.class public final LX/Fd5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:LX/Fd6;


# direct methods
.method public constructor <init>(LX/Fd6;)V
    .locals 0

    .prologue
    .line 2263080
    iput-object p1, p0, LX/Fd5;->a:LX/Fd6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 2263081
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 2263082
    iget-object v0, p0, LX/Fd5;->a:LX/Fd6;

    iget-object v0, v0, LX/Fd6;->s:Lcom/facebook/fbui/glyph/GlyphView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    .line 2263083
    :goto_0
    iget-object v0, p0, LX/Fd5;->a:LX/Fd6;

    iget-object v0, v0, LX/Fd6;->l:LX/Fc6;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/Fc6;->a(Ljava/lang/String;)V

    .line 2263084
    return-void

    .line 2263085
    :cond_0
    iget-object v0, p0, LX/Fd5;->a:LX/Fd6;

    invoke-static {v0}, LX/Fd6;->s(LX/Fd6;)V

    .line 2263086
    iget-object v0, p0, LX/Fd5;->a:LX/Fd6;

    iget-object v0, v0, LX/Fd6;->s:Lcom/facebook/fbui/glyph/GlyphView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/facebook/fbui/glyph/GlyphView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 2263087
    if-nez p2, :cond_0

    if-lez p4, :cond_0

    .line 2263088
    iget-object v0, p0, LX/Fd5;->a:LX/Fd6;

    invoke-static {v0}, LX/Fd6;->u(LX/Fd6;)V

    .line 2263089
    :cond_0
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2263090
    return-void
.end method
