.class public LX/FhN;
.super LX/FhH;
.source ""


# direct methods
.method public constructor <init>(LX/1Ck;LX/2Sc;LX/Fhc;LX/0Ot;LX/0Ot;LX/0Ot;LX/7Hq;LX/0ad;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1Ck;",
            "LX/2Sc;",
            "LX/Fhc;",
            "LX/0Ot",
            "<",
            "LX/FhQ;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FhX;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;",
            "LX/7Hq;",
            "LX/0ad;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2272842
    sget-short v1, LX/100;->t:S

    const/4 v2, 0x0

    move-object/from16 v0, p8

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v2, LX/7BZ;->ENTITY_ONLY_MODE:LX/7BZ;

    :goto_0
    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v1 .. v9}, LX/FhH;-><init>(LX/7BZ;LX/1Ck;LX/2Sc;LX/Fhc;LX/0Ot;LX/0Ot;LX/0Ot;LX/7Hq;)V

    .line 2272843
    return-void

    .line 2272844
    :cond_0
    sget-object v2, LX/7BZ;->DEFAULT_KEYWORD_MODE:LX/7BZ;

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/FhN;
    .locals 9

    .prologue
    .line 2272845
    new-instance v0, LX/FhN;

    invoke-static {p0}, LX/1Ck;->b(LX/0QB;)LX/1Ck;

    move-result-object v1

    check-cast v1, LX/1Ck;

    invoke-static {p0}, LX/2Sc;->a(LX/0QB;)LX/2Sc;

    move-result-object v2

    check-cast v2, LX/2Sc;

    invoke-static {p0}, LX/Fhc;->a(LX/0QB;)LX/Fhc;

    move-result-object v3

    check-cast v3, LX/Fhc;

    const/16 v4, 0x34bf

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x34c2

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0x271

    invoke-static {p0, v6}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v6

    invoke-static {p0}, LX/FhZ;->a(LX/0QB;)LX/7Hq;

    move-result-object v7

    check-cast v7, LX/7Hq;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v8

    check-cast v8, LX/0ad;

    invoke-direct/range {v0 .. v8}, LX/FhN;-><init>(LX/1Ck;LX/2Sc;LX/Fhc;LX/0Ot;LX/0Ot;LX/0Ot;LX/7Hq;LX/0ad;)V

    .line 2272846
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2272847
    const-string v0, "fetch_entity_remote_typeahead"

    return-object v0
.end method

.method public final b()LX/7HY;
    .locals 1

    .prologue
    .line 2272848
    sget-object v0, LX/7HY;->REMOTE_ENTITY:LX/7HY;

    return-object v0
.end method
