.class public LX/FlR;
.super LX/FlQ;
.source ""


# instance fields
.field public b:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2280135
    invoke-direct {p0}, LX/FlQ;-><init>()V

    .line 2280136
    return-void
.end method


# virtual methods
.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2280137
    if-nez p2, :cond_0

    .line 2280138
    new-instance p2, LX/Flt;

    iget-object v0, p0, LX/FlR;->b:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/Flt;-><init>(Landroid/content/Context;)V

    .line 2280139
    :goto_0
    iget-object v0, p0, LX/FlQ;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Flv;

    .line 2280140
    invoke-virtual {v0}, LX/Flv;->d()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2280141
    invoke-virtual {p2}, LX/Flt;->getResources()Landroid/content/res/Resources;

    move-result-object p0

    invoke-virtual {v0, p0}, LX/Flv;->a(Landroid/content/res/Resources;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(Ljava/lang/CharSequence;)V

    .line 2280142
    invoke-virtual {v0}, LX/Flv;->e()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailUri(Ljava/lang/String;)V

    .line 2280143
    return-object p2

    .line 2280144
    :cond_0
    check-cast p2, LX/Flt;

    goto :goto_0
.end method
