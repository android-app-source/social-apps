.class public final enum LX/F8O;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/F8O;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/F8O;

.field public static final enum INVITED:LX/F8O;

.field public static final enum PENDING_CANNOT_UNDO:LX/F8O;

.field public static final enum PENDING_CAN_UNDO:LX/F8O;

.field public static final enum UNINVITED:LX/F8O;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2204056
    new-instance v0, LX/F8O;

    const-string v1, "UNINVITED"

    invoke-direct {v0, v1, v2}, LX/F8O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F8O;->UNINVITED:LX/F8O;

    .line 2204057
    new-instance v0, LX/F8O;

    const-string v1, "PENDING_CAN_UNDO"

    invoke-direct {v0, v1, v3}, LX/F8O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F8O;->PENDING_CAN_UNDO:LX/F8O;

    .line 2204058
    new-instance v0, LX/F8O;

    const-string v1, "PENDING_CANNOT_UNDO"

    invoke-direct {v0, v1, v4}, LX/F8O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F8O;->PENDING_CANNOT_UNDO:LX/F8O;

    .line 2204059
    new-instance v0, LX/F8O;

    const-string v1, "INVITED"

    invoke-direct {v0, v1, v5}, LX/F8O;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/F8O;->INVITED:LX/F8O;

    .line 2204060
    const/4 v0, 0x4

    new-array v0, v0, [LX/F8O;

    sget-object v1, LX/F8O;->UNINVITED:LX/F8O;

    aput-object v1, v0, v2

    sget-object v1, LX/F8O;->PENDING_CAN_UNDO:LX/F8O;

    aput-object v1, v0, v3

    sget-object v1, LX/F8O;->PENDING_CANNOT_UNDO:LX/F8O;

    aput-object v1, v0, v4

    sget-object v1, LX/F8O;->INVITED:LX/F8O;

    aput-object v1, v0, v5

    sput-object v0, LX/F8O;->$VALUES:[LX/F8O;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2204061
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/F8O;
    .locals 1

    .prologue
    .line 2204062
    const-class v0, LX/F8O;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/F8O;

    return-object v0
.end method

.method public static values()[LX/F8O;
    .locals 1

    .prologue
    .line 2204063
    sget-object v0, LX/F8O;->$VALUES:[LX/F8O;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/F8O;

    return-object v0
.end method
