.class public LX/F7L;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/83X;


# instance fields
.field private final a:J

.field private final b:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Z

.field private f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field private g:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;


# direct methods
.method public constructor <init>(LX/F7K;)V
    .locals 2

    .prologue
    .line 2201676
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2201677
    iget-wide v0, p1, LX/F7K;->a:J

    iput-wide v0, p0, LX/F7L;->a:J

    .line 2201678
    iget-object v0, p1, LX/F7K;->b:Ljava/lang/String;

    iput-object v0, p0, LX/F7L;->b:Ljava/lang/String;

    .line 2201679
    iget-object v0, p1, LX/F7K;->c:Ljava/lang/String;

    iput-object v0, p0, LX/F7L;->c:Ljava/lang/String;

    .line 2201680
    iget v0, p1, LX/F7K;->d:I

    iput v0, p0, LX/F7L;->d:I

    .line 2201681
    iget-boolean v0, p1, LX/F7K;->e:Z

    iput-boolean v0, p0, LX/F7L;->e:Z

    .line 2201682
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, LX/F7L;->g:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, LX/F7L;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2201683
    return-void
.end method

.method public constructor <init>(Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;Z)V
    .locals 2

    .prologue
    .line 2201668
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2201669
    iget-wide v0, p1, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->userId:J

    iput-wide v0, p0, LX/F7L;->a:J

    .line 2201670
    iget-object v0, p1, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->profilePic:Ljava/lang/String;

    iput-object v0, p0, LX/F7L;->b:Ljava/lang/String;

    .line 2201671
    iget-object v0, p1, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->name:Ljava/lang/String;

    iput-object v0, p0, LX/F7L;->c:Ljava/lang/String;

    .line 2201672
    iget v0, p1, Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;->mutualFriends:I

    iput v0, p0, LX/F7L;->d:I

    .line 2201673
    iput-boolean p2, p0, LX/F7L;->e:Z

    .line 2201674
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->CAN_REQUEST:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, LX/F7L;->g:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, LX/F7L;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2201675
    return-void
.end method

.method public static b(Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;)LX/F7L;
    .locals 2

    .prologue
    .line 2201667
    new-instance v0, LX/F7L;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, LX/F7L;-><init>(Lcom/facebook/api/growth/contactimporter/PhonebookLookupResultContact;Z)V

    return-object v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 2201666
    iget-wide v0, p0, LX/F7L;->a:J

    return-wide v0
.end method

.method public final a(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 0

    .prologue
    .line 2201664
    iput-object p1, p0, LX/F7L;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2201665
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2201662
    iget-object v0, p0, LX/F7L;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 1

    .prologue
    .line 2201684
    iget-object v0, p0, LX/F7L;->g:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object v0, p0, LX/F7L;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2201685
    iput-object p1, p0, LX/F7L;->g:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2201686
    return-void
.end method

.method public final c()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 1

    .prologue
    .line 2201663
    iget-object v0, p0, LX/F7L;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2201661
    iget-object v0, p0, LX/F7L;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 2201660
    iget v0, p0, LX/F7L;->d:I

    return v0
.end method

.method public final f()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;
    .locals 1

    .prologue
    .line 2201659
    iget-object v0, p0, LX/F7L;->g:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    return-object v0
.end method

.method public final g()LX/2h7;
    .locals 1

    .prologue
    .line 2201658
    iget-boolean v0, p0, LX/F7L;->e:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/2h7;->PYMK_FRIEND_FINDER:LX/2h7;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, LX/2h7;->FRIEND_FINDER:LX/2h7;

    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2201657
    const/4 v0, 0x0

    return-object v0
.end method
