.class public LX/FOM;
.super Landroid/view/View;
.source ""


# instance fields
.field public a:LX/FOL;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field private b:LX/FOI;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v0, 0x0

    .line 2235072
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2235073
    invoke-direct {p0, p2, v0, v0}, LX/FOM;->a(Landroid/util/AttributeSet;II)V

    .line 2235074
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2235069
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2235070
    const/4 v0, 0x0

    invoke-direct {p0, p2, p3, v0}, LX/FOM;->a(Landroid/util/AttributeSet;II)V

    .line 2235071
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;II)V
    .locals 0
    .param p2    # Landroid/util/AttributeSet;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2235066
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2235067
    invoke-direct {p0, p2, p3, p4}, LX/FOM;->a(Landroid/util/AttributeSet;II)V

    .line 2235068
    return-void
.end method

.method private a(Landroid/util/AttributeSet;II)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2235054
    const-class v0, LX/FOM;

    invoke-static {v0, p0}, LX/FOM;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2235055
    invoke-virtual {p0}, LX/FOM;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->FaceView:[I

    invoke-virtual {v0, p1, v1, p2, p3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 2235056
    const/16 v1, 0x0

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 2235057
    const/16 v1, 0x1

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 2235058
    const/16 v1, 0x2

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v5

    .line 2235059
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 2235060
    if-lez v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must have tile size attribute"

    invoke-static {v0, v1}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2235061
    iget-object v0, p0, LX/FOM;->a:LX/FOL;

    invoke-virtual {p0}, LX/FOM;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v4, 0x6

    invoke-virtual/range {v0 .. v5}, LX/FOL;->a(Landroid/content/Context;IIIZ)LX/FOI;

    move-result-object v0

    iput-object v0, p0, LX/FOM;->b:LX/FOI;

    .line 2235062
    iget-object v0, p0, LX/FOM;->b:LX/FOI;

    invoke-virtual {v0, p0}, LX/FOI;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 2235063
    invoke-virtual {p0, v6}, LX/FOM;->setWillNotDraw(Z)V

    .line 2235064
    return-void

    :cond_0
    move v0, v6

    .line 2235065
    goto :goto_0
.end method

.method private static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0}, LX/FOM;->a(Ljava/lang/Object;Landroid/content/Context;)V

    return-void
.end method

.method private static a(Ljava/lang/Object;Landroid/content/Context;)V
    .locals 2

    invoke-static {p1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/FOM;

    const-class v1, LX/FOL;

    invoke-interface {v0, v1}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v0

    check-cast v0, LX/FOL;

    iput-object v0, p0, LX/FOM;->a:LX/FOL;

    return-void
.end method


# virtual methods
.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0xc11d669

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2235075
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 2235076
    iget-object v1, p0, LX/FOM;->b:LX/FOI;

    invoke-virtual {v1}, LX/FOI;->a()V

    .line 2235077
    const/16 v1, 0x2d

    const v2, 0x2d3207a1

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x50756876

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2235048
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 2235049
    iget-object v1, p0, LX/FOM;->b:LX/FOI;

    invoke-virtual {v1}, LX/FOI;->b()V

    .line 2235050
    iget-object v1, p0, LX/FOM;->b:LX/FOI;

    .line 2235051
    sget-object v2, LX/0Q7;->a:LX/0Px;

    move-object v2, v2

    .line 2235052
    invoke-virtual {v1, v2}, LX/FOI;->a(LX/0Px;)V

    .line 2235053
    const/16 v1, 0x2d

    const v2, 0x2d68c786

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 2235042
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 2235043
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 2235044
    invoke-virtual {p0}, LX/FOM;->getPaddingLeft()I

    move-result v0

    int-to-float v0, v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2235045
    iget-object v0, p0, LX/FOM;->b:LX/FOI;

    invoke-virtual {v0, p1}, LX/FOI;->draw(Landroid/graphics/Canvas;)V

    .line 2235046
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 2235047
    return-void
.end method

.method public final onMeasure(II)V
    .locals 7

    .prologue
    .line 2235033
    invoke-virtual {p0}, LX/FOM;->getPaddingLeft()I

    move-result v0

    .line 2235034
    invoke-virtual {p0}, LX/FOM;->getPaddingRight()I

    move-result v1

    .line 2235035
    invoke-virtual {p0}, LX/FOM;->getPaddingTop()I

    move-result v2

    .line 2235036
    invoke-virtual {p0}, LX/FOM;->getPaddingBottom()I

    move-result v3

    .line 2235037
    iget-object v4, p0, LX/FOM;->b:LX/FOI;

    invoke-virtual {v4}, LX/FOI;->getIntrinsicWidth()I

    move-result v4

    add-int/2addr v4, v0

    add-int/2addr v4, v1

    invoke-static {v4, p1}, LX/FOM;->resolveSize(II)I

    move-result v4

    .line 2235038
    iget-object v5, p0, LX/FOM;->b:LX/FOI;

    invoke-virtual {v5}, LX/FOI;->getIntrinsicHeight()I

    move-result v5

    add-int/2addr v5, v2

    add-int/2addr v5, v3

    invoke-static {v5, p2}, LX/FOM;->resolveSize(II)I

    move-result v5

    .line 2235039
    invoke-virtual {p0, v4, v5}, LX/FOM;->setMeasuredDimension(II)V

    .line 2235040
    iget-object v6, p0, LX/FOM;->b:LX/FOI;

    sub-int v1, v4, v1

    sub-int v3, v5, v3

    invoke-virtual {v6, v0, v2, v1, v3}, LX/FOI;->setBounds(IIII)V

    .line 2235041
    return-void
.end method

.method public setUserKeys(LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Px",
            "<",
            "Lcom/facebook/user/model/UserKey;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2235030
    iget-object v0, p0, LX/FOM;->b:LX/FOI;

    invoke-virtual {v0, p1}, LX/FOI;->a(LX/0Px;)V

    .line 2235031
    invoke-virtual {p0}, LX/FOM;->requestLayout()V

    .line 2235032
    return-void
.end method

.method public final verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 2235029
    iget-object v0, p0, LX/FOM;->b:LX/FOI;

    if-eq p1, v0, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
