.class public LX/FWn;
.super LX/0ht;
.source ""

# interfaces
.implements LX/5OE;
.implements LX/5OF;


# instance fields
.field public a:LX/5OG;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 2252674
    invoke-direct {p0, p1}, LX/0ht;-><init>(Landroid/content/Context;)V

    .line 2252675
    sget-object v0, LX/5OV;->SLIDE_UP:LX/5OV;

    invoke-virtual {p0, v0}, LX/0ht;->a(LX/5OV;)V

    .line 2252676
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/0ht;->c(Z)V

    .line 2252677
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2252672
    invoke-virtual {p0}, LX/0ht;->l()V

    .line 2252673
    return-void
.end method

.method public final a(LX/5OG;Z)V
    .locals 0

    .prologue
    .line 2252637
    iput-object p1, p0, LX/FWn;->a:LX/5OG;

    .line 2252638
    iget-object p2, p0, LX/FWn;->a:LX/5OG;

    invoke-virtual {p2, p0}, LX/5OG;->a(LX/5OE;)V

    .line 2252639
    iget-object p2, p0, LX/FWn;->a:LX/5OG;

    invoke-virtual {p2, p0}, LX/5OG;->a(LX/5OF;)V

    .line 2252640
    invoke-virtual {p0}, LX/0ht;->d()V

    .line 2252641
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 2252645
    iget-object v0, p0, LX/FWn;->a:LX/5OG;

    if-eqz v0, :cond_5

    iget-object v0, p0, LX/FWn;->a:LX/5OG;

    invoke-virtual {v0}, LX/5OG;->hasVisibleItems()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 2252646
    new-instance v1, LX/5OK;

    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, LX/5OK;-><init>(Landroid/content/Context;)V

    .line 2252647
    iget-object v0, p0, LX/FWn;->a:LX/5OG;

    invoke-virtual {v1, v0}, LX/5OK;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2252648
    iget-object v0, p0, LX/FWn;->a:LX/5OG;

    invoke-virtual {v1, v0}, LX/5OK;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2252649
    iget-boolean v0, p0, LX/0ht;->e:Z

    invoke-virtual {v1, v0}, LX/5OK;->setShowFullWidth(Z)V

    .line 2252650
    iget v0, p0, LX/0ht;->c:I

    invoke-virtual {v1, v0}, LX/5OK;->setMaxWidth(I)V

    .line 2252651
    const/high16 v0, 0x41200000    # 10.0f

    invoke-virtual {v1, v0}, LX/5OK;->setMaxRows(F)V

    .line 2252652
    invoke-virtual {p0}, LX/0ht;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b1164

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2252653
    iget v2, v1, LX/5OK;->e:I

    if-eq v2, v0, :cond_0

    .line 2252654
    iput v0, v1, LX/5OK;->e:I

    .line 2252655
    invoke-virtual {v1}, LX/5OK;->requestLayout()V

    .line 2252656
    invoke-virtual {v1}, LX/5OK;->invalidate()V

    .line 2252657
    :cond_0
    invoke-virtual {p0}, LX/0ht;->h()Landroid/view/View;

    move-result-object v0

    .line 2252658
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    :goto_0
    invoke-virtual {v1, v0}, LX/5OK;->setMinimumWidth(I)V

    move-object v0, v1

    .line 2252659
    :goto_1
    move-object v0, v0

    .line 2252660
    if-nez v0, :cond_1

    .line 2252661
    :goto_2
    return-void

    .line 2252662
    :cond_1
    iget-boolean v1, p0, LX/0ht;->r:Z

    move v1, v1

    .line 2252663
    if-eqz v1, :cond_2

    .line 2252664
    invoke-virtual {p0, v0}, LX/0ht;->e(Landroid/view/View;)V

    .line 2252665
    invoke-virtual {p0}, LX/0ht;->e()V

    goto :goto_2

    .line 2252666
    :cond_2
    invoke-virtual {p0, v0}, LX/0ht;->d(Landroid/view/View;)V

    .line 2252667
    if-eqz p1, :cond_3

    .line 2252668
    invoke-super {p0, p1}, LX/0ht;->a(Landroid/view/View;)V

    goto :goto_2

    .line 2252669
    :cond_3
    invoke-super {p0}, LX/0ht;->d()V

    goto :goto_2

    .line 2252670
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 2252671
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 2252644
    const/4 v0, 0x0

    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 2252642
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/0ht;->a(Landroid/view/View;)V

    .line 2252643
    return-void
.end method
