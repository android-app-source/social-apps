.class public LX/G46;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Landroid/view/animation/Interpolator;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2317074
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/view/View;)V
    .locals 2
    .param p0    # Landroid/view/View;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2317093
    if-eqz p0, :cond_0

    .line 2317094
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 2317095
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(LX/G3c;Lcom/facebook/timeline/refresher/ProfileRefresherView;Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;Z)V
    .locals 6
    .param p1    # LX/G3c;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;
        .annotation build Landroid/support/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2317075
    const/4 v0, 0x1

    .line 2317076
    if-eqz p4, :cond_2

    .line 2317077
    :cond_0
    :goto_0
    move v0, v0

    .line 2317078
    if-eqz v0, :cond_1

    .line 2317079
    iget-object v0, p2, Lcom/facebook/timeline/refresher/ProfileRefresherView;->l:Landroid/view/View;

    move-object v0, v0

    .line 2317080
    if-eqz v0, :cond_1

    .line 2317081
    iget-object v0, p2, Lcom/facebook/timeline/refresher/ProfileRefresherView;->l:Landroid/view/View;

    move-object v0, v0

    .line 2317082
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    .line 2317083
    iget-object v0, p2, Lcom/facebook/timeline/refresher/ProfileRefresherView;->l:Landroid/view/View;

    move-object v0, v0

    .line 2317084
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2317085
    :cond_1
    new-instance v0, LX/G45;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move v4, p4

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, LX/G45;-><init>(LX/G46;LX/G3c;Lcom/facebook/timeline/refresher/ProfileRefresherHeaderFragment;ZLcom/facebook/timeline/refresher/ProfileRefresherView;)V

    .line 2317086
    iget-object v1, p2, Lcom/facebook/timeline/refresher/ProfileRefresherView;->o:Landroid/widget/LinearLayout;

    move-object v1, v1

    .line 2317087
    iget-object v2, p2, Lcom/facebook/timeline/refresher/ProfileRefresherView;->o:Landroid/widget/LinearLayout;

    move-object v2, v2

    .line 2317088
    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v2

    int-to-float v2, v2

    const/4 v4, 0x0

    .line 2317089
    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 2317090
    invoke-virtual {v1, v4}, Landroid/view/View;->setAlpha(F)V

    .line 2317091
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    iget-object v4, p0, LX/G46;->a:Landroid/view/animation/Interpolator;

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 2317092
    return-void

    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->PROFILE_PICTURE:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->COVER_PHOTO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_BIO:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-eq v1, v2, :cond_0

    invoke-virtual {p1}, LX/G3c;->e()Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;->INTRO_CARD_FEATURED_PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileWizardStepType;

    if-eq v1, v2, :cond_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
