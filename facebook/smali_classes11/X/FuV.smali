.class public final enum LX/FuV;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FuV;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FuV;

.field public static final enum DELETED_STORY:LX/FuV;

.field public static final enum HIDDEN_STORY:LX/FuV;

.field public static final enum LOADING:LX/FuV;

.field public static final enum NO_STORIES:LX/FuV;

.field public static final enum PROMPT:LX/FuV;

.field public static final enum SCRUBBER:LX/FuV;

.field public static final enum SEE_MORE:LX/FuV;

.field public static final enum STORY_BASE:LX/FuV;

.field public static final enum STORY_EDGE_NARROW_OR_AGG:LX/FuV;

.field public static final enum STORY_EDGE_WIDE:LX/FuV;

.field public static final enum UNKNOWN:LX/FuV;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2300056
    new-instance v0, LX/FuV;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v3}, LX/FuV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FuV;->UNKNOWN:LX/FuV;

    .line 2300057
    new-instance v0, LX/FuV;

    const-string v1, "STORY_BASE"

    invoke-direct {v0, v1, v4}, LX/FuV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FuV;->STORY_BASE:LX/FuV;

    .line 2300058
    new-instance v0, LX/FuV;

    const-string v1, "STORY_EDGE_WIDE"

    invoke-direct {v0, v1, v5}, LX/FuV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FuV;->STORY_EDGE_WIDE:LX/FuV;

    .line 2300059
    new-instance v0, LX/FuV;

    const-string v1, "STORY_EDGE_NARROW_OR_AGG"

    invoke-direct {v0, v1, v6}, LX/FuV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FuV;->STORY_EDGE_NARROW_OR_AGG:LX/FuV;

    .line 2300060
    new-instance v0, LX/FuV;

    const-string v1, "SCRUBBER"

    invoke-direct {v0, v1, v7}, LX/FuV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FuV;->SCRUBBER:LX/FuV;

    .line 2300061
    new-instance v0, LX/FuV;

    const-string v1, "SEE_MORE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/FuV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FuV;->SEE_MORE:LX/FuV;

    .line 2300062
    new-instance v0, LX/FuV;

    const-string v1, "LOADING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/FuV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FuV;->LOADING:LX/FuV;

    .line 2300063
    new-instance v0, LX/FuV;

    const-string v1, "NO_STORIES"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/FuV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FuV;->NO_STORIES:LX/FuV;

    .line 2300064
    new-instance v0, LX/FuV;

    const-string v1, "HIDDEN_STORY"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/FuV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FuV;->HIDDEN_STORY:LX/FuV;

    .line 2300065
    new-instance v0, LX/FuV;

    const-string v1, "DELETED_STORY"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/FuV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FuV;->DELETED_STORY:LX/FuV;

    .line 2300066
    new-instance v0, LX/FuV;

    const-string v1, "PROMPT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/FuV;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FuV;->PROMPT:LX/FuV;

    .line 2300067
    const/16 v0, 0xb

    new-array v0, v0, [LX/FuV;

    sget-object v1, LX/FuV;->UNKNOWN:LX/FuV;

    aput-object v1, v0, v3

    sget-object v1, LX/FuV;->STORY_BASE:LX/FuV;

    aput-object v1, v0, v4

    sget-object v1, LX/FuV;->STORY_EDGE_WIDE:LX/FuV;

    aput-object v1, v0, v5

    sget-object v1, LX/FuV;->STORY_EDGE_NARROW_OR_AGG:LX/FuV;

    aput-object v1, v0, v6

    sget-object v1, LX/FuV;->SCRUBBER:LX/FuV;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/FuV;->SEE_MORE:LX/FuV;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/FuV;->LOADING:LX/FuV;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/FuV;->NO_STORIES:LX/FuV;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/FuV;->HIDDEN_STORY:LX/FuV;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/FuV;->DELETED_STORY:LX/FuV;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/FuV;->PROMPT:LX/FuV;

    aput-object v2, v0, v1

    sput-object v0, LX/FuV;->$VALUES:[LX/FuV;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2300068
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FuV;
    .locals 1

    .prologue
    .line 2300069
    const-class v0, LX/FuV;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FuV;

    return-object v0
.end method

.method public static values()[LX/FuV;
    .locals 1

    .prologue
    .line 2300070
    sget-object v0, LX/FuV;->$VALUES:[LX/FuV;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FuV;

    return-object v0
.end method
