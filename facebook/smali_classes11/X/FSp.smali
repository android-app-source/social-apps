.class public final LX/FSp;
.super LX/0gW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0gW",
        "<",
        "Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsBucketItemQueryModel;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 11

    .prologue
    .line 2242534
    const-class v1, Lcom/facebook/profile/discovery/protocol/DiscoveryCurationGraphQLModels$CurationTagsBucketItemQueryModel;

    const v0, -0x3be1f4e4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x2

    const-string v5, "CurationTagsBucketItemQuery"

    const-string v6, "c7dce3169b753875b31eec130a57c1c0"

    const-string v7, "profile_discovery_bucket_item"

    const-string v8, "10155069963471729"

    const-string v9, "10155259086551729"

    .line 2242535
    sget-object v0, LX/0Re;->a:LX/0Re;

    move-object v10, v0

    .line 2242536
    move-object v0, p0

    invoke-direct/range {v0 .. v10}, LX/0gW;-><init>(Ljava/lang/Class;Ljava/lang/Integer;ZILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;)V

    .line 2242537
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2242538
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 2242539
    sparse-switch v0, :sswitch_data_0

    .line 2242540
    :goto_0
    return-object p1

    .line 2242541
    :sswitch_0
    const-string p1, "0"

    goto :goto_0

    .line 2242542
    :sswitch_1
    const-string p1, "1"

    goto :goto_0

    .line 2242543
    :sswitch_2
    const-string p1, "2"

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        -0x70b5e4f5 -> :sswitch_2
        -0x55d541c4 -> :sswitch_1
        0x48c12072 -> :sswitch_0
    .end sparse-switch
.end method
