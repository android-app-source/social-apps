.class public LX/Gqc;
.super LX/Cod;
.source ""

# interfaces
.implements LX/CnG;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cod",
        "<",
        "LX/Gpl;",
        ">;",
        "Lcom/facebook/instantshopping/view/block/InstantShoppingTextBlockView;"
    }
.end annotation


# instance fields
.field public a:LX/Go0;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/1xP;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/CIh;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Go7;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public final e:Lcom/facebook/richdocument/view/widget/RichTextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 2396678
    invoke-direct {p0, p1}, LX/Cod;-><init>(Landroid/view/View;)V

    .line 2396679
    const v0, 0x7f0d1846

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/richdocument/view/widget/RichTextView;

    iput-object v0, p0, LX/Gqc;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2396680
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    check-cast p0, LX/Gqc;

    invoke-static {v0}, LX/Go0;->a(LX/0QB;)LX/Go0;

    move-result-object v2

    check-cast v2, LX/Go0;

    invoke-static {v0}, LX/1xP;->a(LX/0QB;)LX/1xP;

    move-result-object v3

    check-cast v3, LX/1xP;

    invoke-static {v0}, LX/CIh;->b(LX/0QB;)LX/CIh;

    move-result-object p1

    check-cast p1, LX/CIh;

    invoke-static {v0}, LX/Go7;->a(LX/0QB;)LX/Go7;

    move-result-object v0

    check-cast v0, LX/Go7;

    iput-object v2, p0, LX/Gqc;->a:LX/Go0;

    iput-object v3, p0, LX/Gqc;->b:LX/1xP;

    iput-object p1, p0, LX/Gqc;->c:LX/CIh;

    iput-object v0, p0, LX/Gqc;->d:LX/Go7;

    .line 2396681
    return-void
.end method


# virtual methods
.method public final a(LX/Clf;Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;)V
    .locals 2

    .prologue
    .line 2396682
    iget-object v0, p0, LX/Gqc;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2396683
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 2396684
    invoke-virtual {v0, p1}, LX/CtG;->setText(LX/Clf;)V

    .line 2396685
    iget-object v0, p0, LX/Gqc;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2396686
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 2396687
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, LX/CtG;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 2396688
    iget-object v0, p0, LX/Gqc;->c:LX/CIh;

    iget-object v1, p0, LX/Gqc;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2396689
    iget-object p0, v1, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v1, p0

    .line 2396690
    invoke-virtual {v0, v1, p3, p2}, LX/CIh;->a(Landroid/widget/TextView;Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;)V

    .line 2396691
    return-void
.end method

.method public final a(LX/GoE;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;)V
    .locals 2

    .prologue
    .line 2396692
    iget-object v0, p0, LX/Gqc;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2396693
    iget-object v1, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, v1

    .line 2396694
    new-instance v1, LX/Gqb;

    invoke-direct {v1, p0, p2, p1}, LX/Gqb;-><init>(LX/Gqc;Lcom/facebook/richdocument/model/graphql/RichDocumentGraphQlModels$RichDocumentCommonEntityModel;LX/GoE;)V

    invoke-virtual {v0, v1}, LX/CtG;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2396695
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2396696
    invoke-super {p0, p1}, LX/Cod;->a(Landroid/os/Bundle;)V

    .line 2396697
    iget-object v0, p0, LX/Gqc;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2396698
    iget-object p0, v0, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v0, p0

    .line 2396699
    invoke-virtual {v0}, LX/CtG;->a()V

    .line 2396700
    return-void
.end method
