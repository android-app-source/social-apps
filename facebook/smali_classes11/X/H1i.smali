.class public LX/H1i;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2415749
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2415750
    iput-object p1, p0, LX/H1i;->a:LX/0Zb;

    .line 2415751
    return-void
.end method

.method public static a(LX/H1i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2415752
    iget-object v0, p0, LX/H1i;->a:LX/0Zb;

    const-string v1, "nearby_places_here_card_tap_page"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2415753
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2415754
    const-string v1, "places_recommendations"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "target_id"

    invoke-virtual {v1, v2, p2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "mechanism"

    const-string v3, "here_card"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "event_target"

    const-string v3, "place"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "event_type"

    const-string v3, "click"

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "session_id"

    invoke-virtual {v1, v2, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2415755
    const-string v1, "photo_in_collection"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2415756
    const-string v1, "tap_action"

    const-string v2, "photo_in_collection"

    invoke-virtual {v0, v1, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2415757
    :cond_0
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2415758
    :cond_1
    return-void
.end method

.method public static b(LX/0QB;)LX/H1i;
    .locals 2

    .prologue
    .line 2415759
    new-instance v1, LX/H1i;

    invoke-static {p0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v0

    check-cast v0, LX/0Zb;

    invoke-direct {v1, v0}, LX/H1i;-><init>(LX/0Zb;)V

    .line 2415760
    return-object v1
.end method


# virtual methods
.method public final a(Ljava/util/List;Lcom/facebook/graphql/model/GraphQLGeoRectangle;F)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLGeoRectangle;",
            "F)V"
        }
    .end annotation

    .prologue
    .line 2415761
    iget-object v0, p0, LX/H1i;->a:LX/0Zb;

    const-string v1, "request_sent"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v0

    .line 2415762
    invoke-virtual {v0}, LX/0oG;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2415763
    const-string v1, "places_recommendations"

    invoke-virtual {v0, v1}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "category"

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "map_region"

    .line 2415764
    if-nez p2, :cond_1

    .line 2415765
    const-string v6, ""

    .line 2415766
    :goto_0
    move-object v3, v6

    .line 2415767
    invoke-virtual {v1, v2, v3}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object v1

    const-string v2, "zoom"

    float-to-double v4, p3

    invoke-virtual {v1, v2, v4, v5}, LX/0oG;->a(Ljava/lang/String;D)LX/0oG;

    .line 2415768
    invoke-virtual {v0}, LX/0oG;->d()V

    .line 2415769
    :cond_0
    return-void

    :cond_1
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "{N: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->j()D

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " S: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->k()D

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " E: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->a()D

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " W: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p2}, Lcom/facebook/graphql/model/GraphQLGeoRectangle;->l()D

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "}"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method
