.class public LX/FtV;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1Pn;
.implements LX/1Pp;
.implements LX/1Pr;
.implements LX/1Pt;
.implements LX/1Pu;


# instance fields
.field private final a:LX/1QB;

.field private final b:LX/99Q;

.field private final c:LX/1QF;

.field private final d:LX/1QP;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/1QB;LX/99Q;LX/1QF;LX/1Q6;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2298822
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2298823
    iput-object p2, p0, LX/FtV;->a:LX/1QB;

    .line 2298824
    iput-object p3, p0, LX/FtV;->b:LX/99Q;

    .line 2298825
    iput-object p4, p0, LX/FtV;->c:LX/1QF;

    .line 2298826
    invoke-static {p1}, LX/1Q6;->a(Landroid/content/Context;)LX/1QP;

    move-result-object v0

    iput-object v0, p0, LX/FtV;->d:LX/1QP;

    .line 2298827
    return-void
.end method


# virtual methods
.method public final a(LX/1KL;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 2298818
    iget-object v0, p0, LX/FtV;->c:LX/1QF;

    invoke-virtual {v0, p1}, LX/1QF;->a(LX/1KL;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1KL;LX/0jW;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;",
            "LX/0jW;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 2298819
    iget-object v0, p0, LX/FtV;->c:LX/1QF;

    invoke-virtual {v0, p1, p2}, LX/1QF;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/1Rb;)V
    .locals 1

    .prologue
    .line 2298820
    iget-object v0, p0, LX/FtV;->b:LX/99Q;

    invoke-virtual {v0, p1}, LX/99Q;->a(LX/1Rb;)V

    .line 2298821
    return-void
.end method

.method public final a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 2298828
    iget-object v0, p0, LX/FtV;->a:LX/1QB;

    invoke-virtual {v0, p1, p2, p3, p4}, LX/1QB;->a(LX/1aZ;Ljava/lang/String;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2298829
    return-void
.end method

.method public final a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 2298804
    iget-object v0, p0, LX/FtV;->b:LX/99Q;

    invoke-virtual {v0, p1, p2}, LX/99Q;->a(LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2298805
    return-void
.end method

.method public final a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V
    .locals 1

    .prologue
    .line 2298830
    iget-object v0, p0, LX/FtV;->b:LX/99Q;

    invoke-virtual {v0, p1, p2, p3}, LX/99Q;->a(LX/1f9;LX/1bf;Lcom/facebook/common/callercontext/CallerContext;)V

    .line 2298831
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2298815
    iget-object v0, p0, LX/FtV;->a:LX/1QB;

    invoke-virtual {v0, p1}, LX/1QB;->a(Ljava/lang/String;)V

    .line 2298816
    return-void
.end method

.method public final a(LX/1KL;Ljava/lang/Object;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<K:",
            "Ljava/lang/Object;",
            "T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/1KL",
            "<TK;TT;>;TT;)Z"
        }
    .end annotation

    .prologue
    .line 2298817
    iget-object v0, p0, LX/FtV;->c:LX/1QF;

    invoke-virtual {v0, p1, p2}, LX/1QF;->a(LX/1KL;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2298813
    iget-object v0, p0, LX/FtV;->c:LX/1QF;

    invoke-virtual {v0, p1}, LX/1QF;->b(Ljava/lang/String;)V

    .line 2298814
    return-void
.end method

.method public final getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 2298812
    iget-object v0, p0, LX/FtV;->d:LX/1QP;

    invoke-virtual {v0}, LX/1QP;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 2298811
    iget-object v0, p0, LX/FtV;->b:LX/99Q;

    invoke-virtual {v0}, LX/99Q;->l()Z

    move-result v0

    return v0
.end method

.method public final m()LX/1f9;
    .locals 1

    .prologue
    .line 2298810
    iget-object v0, p0, LX/FtV;->b:LX/99Q;

    invoke-virtual {v0}, LX/99Q;->m()LX/1f9;

    move-result-object v0

    return-object v0
.end method

.method public final o()LX/1Rb;
    .locals 1

    .prologue
    .line 2298809
    iget-object v0, p0, LX/FtV;->b:LX/99Q;

    invoke-virtual {v0}, LX/99Q;->o()LX/1Rb;

    move-result-object v0

    return-object v0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 2298807
    iget-object v0, p0, LX/FtV;->b:LX/99Q;

    invoke-virtual {v0}, LX/99Q;->p()V

    .line 2298808
    return-void
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 2298806
    iget-object v0, p0, LX/FtV;->b:LX/99Q;

    invoke-virtual {v0}, LX/99Q;->q()Z

    move-result v0

    return v0
.end method
