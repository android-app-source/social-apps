.class public LX/F14;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/17z;
.implements LX/0jQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/17z",
        "<",
        "Lcom/facebook/graphql/model/FeedUnit;",
        ">;",
        "LX/0jQ;"
    }
.end annotation


# instance fields
.field private a:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/model/GraphQLUser;

.field public c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

.field public d:Z


# direct methods
.method public constructor <init>(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLUser;Lcom/facebook/graphql/model/GraphQLTextWithEntities;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;",
            "Lcom/facebook/graphql/model/GraphQLUser;",
            "Lcom/facebook/graphql/model/GraphQLTextWithEntities;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2191084
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2191085
    iput-object p1, p0, LX/F14;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2191086
    iput-object p2, p0, LX/F14;->b:Lcom/facebook/graphql/model/GraphQLUser;

    .line 2191087
    iput-object p3, p0, LX/F14;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2191088
    iput-boolean p4, p0, LX/F14;->d:Z

    .line 2191089
    return-void
.end method


# virtual methods
.method public final c()Lcom/facebook/graphql/model/FeedUnit;
    .locals 1

    .prologue
    .line 2191090
    iget-object v0, p0, LX/F14;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2191091
    iget-object p0, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, p0

    .line 2191092
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    return-object v0
.end method

.method public final g()Lcom/facebook/feed/rows/core/props/FeedProps;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2191093
    iget-object v0, p0, LX/F14;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    return-object v0
.end method
