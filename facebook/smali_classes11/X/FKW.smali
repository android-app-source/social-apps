.class public LX/FKW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0e6;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0e6",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225172
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2225173
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)LX/14N;
    .locals 4

    .prologue
    .line 2225174
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2225175
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "delete_all"

    const-string v3, "true"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225176
    new-instance v1, LX/14O;

    invoke-direct {v1}, LX/14O;-><init>()V

    const-string v2, "clearMontageViewers"

    .line 2225177
    iput-object v2, v1, LX/14O;->b:Ljava/lang/String;

    .line 2225178
    move-object v1, v1

    .line 2225179
    const-string v2, "DELETE"

    .line 2225180
    iput-object v2, v1, LX/14O;->c:Ljava/lang/String;

    .line 2225181
    move-object v1, v1

    .line 2225182
    const-string v2, "me/montage_thread_viewers"

    .line 2225183
    iput-object v2, v1, LX/14O;->d:Ljava/lang/String;

    .line 2225184
    move-object v1, v1

    .line 2225185
    iput-object v0, v1, LX/14O;->g:Ljava/util/List;

    .line 2225186
    move-object v0, v1

    .line 2225187
    sget-object v1, LX/14S;->STRING:LX/14S;

    .line 2225188
    iput-object v1, v0, LX/14O;->k:LX/14S;

    .line 2225189
    move-object v0, v0

    .line 2225190
    invoke-virtual {v0}, LX/14O;->C()LX/14N;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;LX/1pN;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2225191
    invoke-virtual {p2}, LX/1pN;->j()V

    .line 2225192
    const/4 v0, 0x0

    return-object v0
.end method
