.class public LX/F0B;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedQueryModel;",
            ">;",
            "Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0QK;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QK",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackMoreStoriesQueryModel;",
            ">;",
            "Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/concurrent/Executor;

.field public d:LX/0tX;

.field private final e:LX/0sa;

.field private final f:LX/0w9;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/0sU;

.field public i:I

.field public j:Lcom/facebook/api/feedtype/FeedType;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/0sa;LX/0Ot;LX/0w9;LX/0sU;)V
    .locals 1
    .param p1    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "LX/0tX;",
            "LX/0sa;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;",
            ">;",
            "LX/0w9;",
            "LX/0sU;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2188230
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2188231
    new-instance v0, LX/F09;

    invoke-direct {v0, p0}, LX/F09;-><init>(LX/F0B;)V

    iput-object v0, p0, LX/F0B;->a:LX/0QK;

    .line 2188232
    new-instance v0, LX/F0A;

    invoke-direct {v0, p0}, LX/F0A;-><init>(LX/F0B;)V

    iput-object v0, p0, LX/F0B;->b:LX/0QK;

    .line 2188233
    iput-object p1, p0, LX/F0B;->c:Ljava/util/concurrent/Executor;

    .line 2188234
    iput-object p2, p0, LX/F0B;->d:LX/0tX;

    .line 2188235
    iput-object p3, p0, LX/F0B;->e:LX/0sa;

    .line 2188236
    iput-object p5, p0, LX/F0B;->f:LX/0w9;

    .line 2188237
    iput-object p4, p0, LX/F0B;->g:LX/0Ot;

    .line 2188238
    iput-object p6, p0, LX/F0B;->h:LX/0sU;

    .line 2188239
    return-void
.end method

.method public static a(LX/F0B;LX/0gf;Ljava/util/Map;)LX/0zO;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0gf;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)",
            "LX/0zO",
            "<",
            "Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedQueryModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2188203
    new-instance v0, LX/0rT;

    invoke-direct {v0}, LX/0rT;-><init>()V

    iget v1, p0, LX/F0B;->i:I

    .line 2188204
    iput v1, v0, LX/0rT;->c:I

    .line 2188205
    move-object v0, v0

    .line 2188206
    iget-object v1, p0, LX/F0B;->j:Lcom/facebook/api/feedtype/FeedType;

    .line 2188207
    iput-object v1, v0, LX/0rT;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 2188208
    move-object v0, v0

    .line 2188209
    sget-object v1, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 2188210
    iput-object v1, v0, LX/0rT;->a:LX/0rS;

    .line 2188211
    move-object v0, v0

    .line 2188212
    sget-object v1, Lcom/facebook/api/feed/FeedFetchContext;->a:Lcom/facebook/api/feed/FeedFetchContext;

    .line 2188213
    iput-object v1, v0, LX/0rT;->l:Lcom/facebook/api/feed/FeedFetchContext;

    .line 2188214
    move-object v0, v0

    .line 2188215
    invoke-virtual {v0, p1}, LX/0rT;->a(LX/0gf;)LX/0rT;

    move-result-object v0

    invoke-virtual {v0}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v0

    .line 2188216
    new-instance v1, LX/F0L;

    invoke-direct {v1}, LX/F0L;-><init>()V

    move-object v1, v1

    .line 2188217
    invoke-static {p0, v1, v0}, LX/F0B;->a(LX/F0B;LX/0gW;Lcom/facebook/api/feed/FetchFeedParams;)V

    .line 2188218
    const-string v0, "image_scale"

    invoke-static {}, LX/0wB;->a()LX/0wC;

    move-result-object p0

    invoke-virtual {v1, v0, p0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Enum;)LX/0gW;

    .line 2188219
    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2188220
    :cond_0
    :goto_0
    invoke-static {v1}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v0

    sget-object v1, LX/0zS;->c:LX/0zS;

    invoke-virtual {v0, v1}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v0

    .line 2188221
    return-object v0

    .line 2188222
    :cond_1
    const-string v0, "source"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2188223
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_2

    .line 2188224
    const-string p0, "source"

    invoke-virtual {v1, p0, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2188225
    :cond_2
    const-string v0, "campaign_id"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2188226
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 2188227
    const-string v0, "story_id"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2188228
    :cond_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_0

    .line 2188229
    const-string p0, "storyID"

    invoke-virtual {v1, p0, v0}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    goto :goto_0
.end method

.method public static a(LX/0QB;)LX/F0B;
    .locals 8

    .prologue
    .line 2188200
    new-instance v1, LX/F0B;

    invoke-static {p0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {p0}, LX/0sa;->a(LX/0QB;)LX/0sa;

    move-result-object v4

    check-cast v4, LX/0sa;

    const/16 v5, 0x788

    invoke-static {p0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-static {p0}, LX/0w9;->a(LX/0QB;)LX/0w9;

    move-result-object v6

    check-cast v6, LX/0w9;

    invoke-static {p0}, LX/0sU;->a(LX/0QB;)LX/0sU;

    move-result-object v7

    check-cast v7, LX/0sU;

    invoke-direct/range {v1 .. v7}, LX/F0B;-><init>(Ljava/util/concurrent/ExecutorService;LX/0tX;LX/0sa;LX/0Ot;LX/0w9;LX/0sU;)V

    .line 2188201
    move-object v0, v1

    .line 2188202
    return-object v0
.end method

.method public static a(LX/F0B;Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;LX/0ta;)Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;
    .locals 13

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 2188066
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;->a()LX/0Px;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2188067
    :cond_0
    :goto_0
    return-object v5

    .line 2188068
    :cond_1
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v10

    .line 2188069
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v11

    .line 2188070
    invoke-static {}, LX/0P1;->builder()LX/0P2;

    move-result-object v12

    move v2, v3

    .line 2188071
    :goto_1
    invoke-virtual {p1}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ge v2, v0, :cond_b

    .line 2188072
    :try_start_0
    invoke-virtual {p1}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;

    .line 2188073
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->a()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    move-result-object v1

    .line 2188074
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    const v6, 0x14583a39

    if-eq v4, v6, :cond_f

    .line 2188075
    :cond_2
    const/4 v4, 0x0

    .line 2188076
    :goto_2
    move-object v6, v4

    .line 2188077
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->a()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    move-result-object v1

    .line 2188078
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v4

    const v7, -0x24a39ab2

    if-eq v4, v7, :cond_10

    .line 2188079
    :cond_3
    const/4 v4, 0x0

    .line 2188080
    :goto_3
    move-object v4, v4

    .line 2188081
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->a()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    move-result-object v1

    .line 2188082
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    invoke-virtual {v7}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v7

    const v9, 0x6c38a593

    if-eq v7, v9, :cond_11

    .line 2188083
    :cond_4
    const/4 v7, 0x0

    .line 2188084
    :goto_4
    move-object v7, v7

    .line 2188085
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->a()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    move-result-object v1

    invoke-static {v1}, LX/6X2;->e(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;)Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;

    move-result-object v9

    .line 2188086
    if-eqz v4, :cond_6

    .line 2188087
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    move-result-object v1

    if-eqz v1, :cond_5

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;->m()Ljava/lang/String;

    move-result-object v1

    .line 2188088
    :goto_5
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "FriendversaryCampaignStory-"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2188089
    :goto_6
    new-instance v6, LX/1u8;

    invoke-direct {v6}, LX/1u8;-><init>()V

    .line 2188090
    iput-object v4, v6, LX/1u8;->g:Lcom/facebook/graphql/model/FeedUnit;

    .line 2188091
    move-object v4, v6

    .line 2188092
    iput-object v1, v4, LX/1u8;->d:Ljava/lang/String;

    .line 2188093
    move-object v1, v4

    .line 2188094
    invoke-virtual {v1}, LX/1u8;->a()Lcom/facebook/graphql/model/GraphQLFeedUnitEdge;

    move-result-object v1

    .line 2188095
    invoke-virtual {v10, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2188096
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2188097
    :goto_7
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    :cond_5
    move-object v1, v5

    .line 2188098
    goto :goto_5

    .line 2188099
    :cond_6
    if-eqz v6, :cond_7

    .line 2188100
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v1, "FriendversaryStory-"

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;->l()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;->a()LX/0Px;

    move-result-object v1

    const/4 v7, 0x0

    invoke-virtual {v1, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListEdge;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListEdge;->j()Lcom/facebook/graphql/model/GraphQLUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLUser;->v()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v4, v6

    goto :goto_6

    .line 2188101
    :cond_7
    if-eqz v7, :cond_8

    .line 2188102
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "AnniversaryStory-"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;->k()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v4, v7

    goto :goto_6

    .line 2188103
    :cond_8
    if-eqz v9, :cond_9

    .line 2188104
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "CampaignPermalinkStory-"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackCampaignPermalinkStory;->k()Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLGoodwillCampaign;->p()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v4, v9

    goto :goto_6

    .line 2188105
    :cond_9
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->a()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;

    move-result-object v1

    invoke-static {v1}, LX/6X2;->a(Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 2188106
    if-eqz v4, :cond_a

    .line 2188107
    invoke-static {v4}, LX/23u;->a(Lcom/facebook/graphql/model/GraphQLStory;)LX/23u;

    move-result-object v1

    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitEdgeModel;->k()Lcom/facebook/goodwill/feed/protocol/ThrowbackFeedUnitsModels$ThrowbackShareableModel;

    move-result-object v4

    .line 2188108
    if-nez v4, :cond_12

    .line 2188109
    const/4 v6, 0x0

    .line 2188110
    :goto_8
    move-object v4, v6

    .line 2188111
    iput-object v4, v1, LX/23u;->at:Lcom/facebook/graphql/model/GraphQLEntity;

    .line 2188112
    move-object v1, v1

    .line 2188113
    invoke-virtual {v1}, LX/23u;->a()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v4

    .line 2188114
    :cond_a
    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto/16 :goto_6

    .line 2188115
    :catch_0
    move-exception v0

    .line 2188116
    const-class v1, LX/F0B;

    const-string v4, "Caught exception converting ThrowbackUnit into GraphQLStory."

    invoke-static {v1, v4, v0}, LX/01m;->b(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_7

    .line 2188117
    :cond_b
    invoke-virtual {p1}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;->k()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    :goto_9
    if-ge v3, v2, :cond_c

    invoke-virtual {v1, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;

    .line 2188118
    invoke-virtual {v0}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackSectionFragmentModel;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v12, v4, v0}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2188119
    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    .line 2188120
    :cond_c
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v9

    .line 2188121
    new-instance v0, LX/0rT;

    invoke-direct {v0}, LX/0rT;-><init>()V

    iget v1, p0, LX/F0B;->i:I

    .line 2188122
    iput v1, v0, LX/0rT;->c:I

    .line 2188123
    move-object v0, v0

    .line 2188124
    iget-object v1, p0, LX/F0B;->j:Lcom/facebook/api/feedtype/FeedType;

    .line 2188125
    iput-object v1, v0, LX/0rT;->b:Lcom/facebook/api/feedtype/FeedType;

    .line 2188126
    move-object v0, v0

    .line 2188127
    sget-object v1, LX/0rS;->CHECK_SERVER_FOR_NEW_DATA:LX/0rS;

    .line 2188128
    iput-object v1, v0, LX/0rT;->a:LX/0rS;

    .line 2188129
    move-object v0, v0

    .line 2188130
    sget-object v1, Lcom/facebook/api/feed/FeedFetchContext;->a:Lcom/facebook/api/feed/FeedFetchContext;

    .line 2188131
    iput-object v1, v0, LX/0rT;->l:Lcom/facebook/api/feed/FeedFetchContext;

    .line 2188132
    move-object v0, v0

    .line 2188133
    invoke-virtual {v0}, LX/0rT;->t()Lcom/facebook/api/feed/FetchFeedParams;

    move-result-object v1

    .line 2188134
    new-instance v0, Lcom/facebook/api/feed/FetchFeedResult;

    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v3

    invoke-static {v3}, LX/9JZ;->a(LX/0us;)Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v3

    const-string v4, ""

    const-wide/16 v6, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/facebook/api/feed/FetchFeedResult;-><init>(Lcom/facebook/api/feed/FetchFeedParams;LX/0Px;Lcom/facebook/graphql/model/GraphQLPageInfo;Ljava/lang/String;LX/0ta;JZ)V

    .line 2188135
    iget-object v1, p0, LX/F0B;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;

    invoke-virtual {v1, v0}, Lcom/facebook/feed/util/injection/FeedClientSideInjectionTool;->a(Lcom/facebook/api/feed/FetchFeedResult;)Lcom/facebook/api/feed/FetchFeedResult;

    move-result-object v0

    .line 2188136
    invoke-virtual {v0}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v1

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v1

    invoke-virtual {v9}, LX/0Px;->size()I

    move-result v2

    if-lt v1, v2, :cond_e

    .line 2188137
    invoke-virtual {v0}, Lcom/facebook/api/feed/FetchFeedResult;->b()LX/0Px;

    move-result-object v0

    .line 2188138
    :goto_a
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2188139
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v2

    if-ne v2, v8, :cond_d

    invoke-virtual {v1}, LX/0Px;->size()I

    move-result v2

    if-nez v2, :cond_d

    .line 2188140
    const-string v1, "0"

    invoke-virtual {v11, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2188141
    invoke-virtual {v11}, LX/0Pz;->b()LX/0Px;

    move-result-object v1

    .line 2188142
    :cond_d
    new-instance v2, LX/F0F;

    invoke-direct {v2}, LX/F0F;-><init>()V

    .line 2188143
    iput-object v0, v2, LX/F0F;->a:LX/0Px;

    .line 2188144
    move-object v0, v2

    .line 2188145
    iput-object v1, v0, LX/F0F;->b:LX/0Px;

    .line 2188146
    move-object v0, v0

    .line 2188147
    invoke-virtual {v12}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    .line 2188148
    iput-object v1, v0, LX/F0F;->c:LX/0P1;

    .line 2188149
    move-object v0, v0

    .line 2188150
    invoke-virtual {p1}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQL2Models$DefaultPageInfoFieldsModel;

    move-result-object v1

    .line 2188151
    iput-object v1, v0, LX/F0F;->e:LX/0us;

    .line 2188152
    move-object v0, v0

    .line 2188153
    iput-object p2, v0, LX/F0F;->f:LX/0ta;

    .line 2188154
    move-object v0, v0

    .line 2188155
    invoke-virtual {v0}, LX/F0F;->a()Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;

    move-result-object v5

    goto/16 :goto_0

    :cond_e
    move-object v0, v9

    goto :goto_a

    .line 2188156
    :cond_f
    :try_start_1
    new-instance v4, LX/4Wa;

    invoke-direct {v4}, LX/4Wa;-><init>()V

    .line 2188157
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->k()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v6

    .line 2188158
    iput-object v6, v4, LX/4Wa;->b:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2188159
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->K()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    move-result-object v6

    .line 2188160
    iput-object v6, v4, LX/4Wa;->d:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendListConnection;

    .line 2188161
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Y()Ljava/lang/String;

    move-result-object v6

    .line 2188162
    iput-object v6, v4, LX/4Wa;->e:Ljava/lang/String;

    .line 2188163
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aa()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    move-result-object v6

    .line 2188164
    iput-object v6, v4, LX/4Wa;->f:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    .line 2188165
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aj()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v6

    .line 2188166
    iput-object v6, v4, LX/4Wa;->g:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2188167
    new-instance v6, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;

    invoke-direct {v6, v4}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryStory;-><init>(LX/4Wa;)V

    .line 2188168
    move-object v4, v6

    .line 2188169
    goto/16 :goto_2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 2188170
    :cond_10
    :try_start_2
    new-instance v4, LX/4WZ;

    invoke-direct {v4}, LX/4WZ;-><init>()V

    .line 2188171
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->J()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v7

    .line 2188172
    iput-object v7, v4, LX/4WZ;->c:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2188173
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->L()Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    move-result-object v7

    .line 2188174
    iput-object v7, v4, LX/4WZ;->d:Lcom/facebook/graphql/model/GraphQLGoodwillFriendversaryCampaign;

    .line 2188175
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->Y()Ljava/lang/String;

    move-result-object v7

    .line 2188176
    iput-object v7, v4, LX/4WZ;->e:Ljava/lang/String;

    .line 2188177
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aa()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    move-result-object v7

    .line 2188178
    iput-object v7, v4, LX/4WZ;->f:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackSection;

    .line 2188179
    new-instance v7, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;

    invoke-direct {v7, v4}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFriendversaryPromotionStory;-><init>(LX/4WZ;)V

    .line 2188180
    move-object v4, v7

    .line 2188181
    goto/16 :goto_3
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 2188182
    :cond_11
    :try_start_3
    new-instance v7, LX/4WX;

    invoke-direct {v7}, LX/4WX;-><init>()V

    .line 2188183
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->p()Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    move-result-object v9

    .line 2188184
    iput-object v9, v7, LX/4WX;->b:Lcom/facebook/graphql/model/GraphQLGoodwillAnniversaryCampaign;

    .line 2188185
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->B()Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPermalinkColorPalette;

    move-result-object v9

    .line 2188186
    iput-object v9, v7, LX/4WX;->c:Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackPermalinkColorPalette;

    .line 2188187
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->ag()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    .line 2188188
    iput-object v9, v7, LX/4WX;->e:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2188189
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackFeedUnit;->aj()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v9

    .line 2188190
    iput-object v9, v7, LX/4WX;->f:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2188191
    new-instance v9, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;

    invoke-direct {v9, v7}, Lcom/facebook/graphql/model/GraphQLGoodwillThrowbackAnniversaryCampaignStory;-><init>(LX/4WX;)V

    .line 2188192
    move-object v7, v9

    .line 2188193
    goto/16 :goto_4
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 2188194
    :cond_12
    new-instance v6, LX/170;

    invoke-direct {v6}, LX/170;-><init>()V

    .line 2188195
    invoke-virtual {v4}, Lcom/facebook/goodwill/feed/protocol/ThrowbackFeedUnitsModels$ThrowbackShareableModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v7

    .line 2188196
    iput-object v7, v6, LX/170;->ab:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2188197
    invoke-virtual {v4}, Lcom/facebook/goodwill/feed/protocol/ThrowbackFeedUnitsModels$ThrowbackShareableModel;->c()Ljava/lang/String;

    move-result-object v7

    .line 2188198
    iput-object v7, v6, LX/170;->o:Ljava/lang/String;

    .line 2188199
    invoke-virtual {v6}, LX/170;->a()Lcom/facebook/graphql/model/GraphQLEntity;

    move-result-object v6

    goto/16 :goto_8
.end method

.method public static a(LX/F0B;LX/0gW;Lcom/facebook/api/feed/FetchFeedParams;)V
    .locals 2
    .param p1    # LX/0gW;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2188052
    invoke-static {p1}, LX/0w9;->a(LX/0gW;)LX/0gW;

    .line 2188053
    iget-object v0, p0, LX/F0B;->f:LX/0w9;

    invoke-virtual {v0, p1}, LX/0w9;->b(LX/0gW;)LX/0gW;

    .line 2188054
    const/4 v0, 0x0

    const-string v1, "after"

    invoke-static {p1, p2, v0, v1}, LX/0w9;->a(LX/0gW;Lcom/facebook/api/feed/FetchFeedParams;Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2188055
    iget-object v0, p0, LX/F0B;->f:LX/0w9;

    invoke-virtual {v0, p1}, LX/0w9;->c(LX/0gW;)LX/0gW;

    .line 2188056
    invoke-static {p1}, LX/0w9;->d(LX/0gW;)LX/0gW;

    .line 2188057
    const-string v0, "max_friendversary_friends"

    const-string v1, "50"

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2188058
    const-string v0, "profile_image_size"

    invoke-static {}, LX/0sa;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    .line 2188059
    if-eqz p2, :cond_0

    .line 2188060
    const-string v0, "first"

    .line 2188061
    iget v1, p2, Lcom/facebook/api/feed/FetchFeedParams;->c:I

    move v1, v1

    .line 2188062
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2188063
    const-string v0, "feed_story_render_location"

    const-string v1, "feed_mobile"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    .line 2188064
    :cond_0
    iget-object v0, p0, LX/F0B;->h:LX/0sU;

    invoke-virtual {v0, p1}, LX/0sU;->a(LX/0gW;)V

    .line 2188065
    return-void
.end method

.method public static a$redex0(LX/F0B;Lcom/facebook/graphql/executor/GraphQLResult;)Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedQueryModel;",
            ">;)",
            "Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;"
        }
    .end annotation

    .prologue
    .line 2188032
    if-eqz p1, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/executor/GraphQLResult;->c()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/executor/GraphQLResult;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedQueryModel;

    invoke-virtual {v2}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedQueryModel;->a()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;

    move-result-object v2

    if-nez v2, :cond_1

    .line 2188033
    :cond_0
    const/4 v2, 0x0

    .line 2188034
    :goto_0
    return-object v2

    .line 2188035
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/facebook/graphql/executor/GraphQLResult;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedQueryModel;

    invoke-virtual {v2}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedQueryModel;->a()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;

    move-result-object v21

    .line 2188036
    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->y()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/facebook/fbservice/results/BaseResult;->getFreshness()LX/0ta;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, LX/F0B;->a(LX/F0B;Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedUnitsConnectionFragmentModel;LX/0ta;)Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;

    move-result-object v22

    .line 2188037
    if-nez v22, :cond_2

    .line 2188038
    const/4 v2, 0x0

    goto :goto_0

    .line 2188039
    :cond_2
    new-instance v17, LX/0Pz;

    invoke-direct/range {v17 .. v17}, LX/0Pz;-><init>()V

    .line 2188040
    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->a()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$AccentImagesModel;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->a()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$AccentImagesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$AccentImagesModel;->a()LX/2uF;

    move-result-object v2

    if-eqz v2, :cond_4

    .line 2188041
    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->a()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$AccentImagesModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$AccentImagesModel;->a()LX/2uF;

    move-result-object v2

    invoke-virtual {v2}, LX/3Sa;->e()LX/3Sh;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, LX/2sN;->a()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v3}, LX/2sN;->b()LX/1vs;

    move-result-object v2

    iget-object v4, v2, LX/1vs;->a:LX/15i;

    iget v5, v2, LX/1vs;->b:I

    .line 2188042
    const/4 v2, 0x1

    const-class v6, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v4, v5, v2, v6}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    if-eqz v2, :cond_3

    const/4 v2, 0x1

    const-class v6, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v4, v5, v2, v6}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 2188043
    const/4 v2, 0x1

    const-class v6, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v4, v5, v2, v6}, LX/15i;->f(IILjava/lang/Class;)Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    invoke-virtual {v2}, Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;->b()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2188044
    :cond_4
    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->x()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-nez v2, :cond_5

    const/16 v16, 0x0

    .line 2188045
    :goto_2
    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->n()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    if-nez v2, :cond_6

    const/16 v20, 0x0

    .line 2188046
    :goto_3
    new-instance v3, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;

    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->z()J

    move-result-wide v4

    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->o()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v6

    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->w()Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;

    move-result-object v7

    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->j()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v8

    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->k()Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->r()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v10

    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->t()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->u()Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->s()Ljava/lang/String;

    move-result-object v13

    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->q()Ljava/lang/String;

    move-result-object v14

    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->p()Ljava/lang/String;

    move-result-object v15

    invoke-virtual/range {v17 .. v17}, LX/0Pz;->b()LX/0Px;

    move-result-object v17

    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->v()Ljava/lang/String;

    move-result-object v18

    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->l()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v19

    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->m()Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultImageFieldsModel;

    move-result-object v21

    invoke-direct/range {v3 .. v21}, Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;-><init>(JLX/1Fb;Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel$ThemeModel;LX/1Fb;Ljava/lang/String;LX/1Fb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/0Px;Ljava/lang/String;LX/1Fb;Ljava/lang/String;LX/1Fb;)V

    .line 2188047
    new-instance v2, LX/F0F;

    invoke-direct {v2}, LX/F0F;-><init>()V

    invoke-static/range {v22 .. v22}, LX/F0F;->a(Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;)LX/F0F;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/F0F;->a(Lcom/facebook/goodwill/feed/data/ThrowbackFeedResources;)LX/F0F;

    move-result-object v2

    invoke-virtual {v2}, LX/F0F;->a()Lcom/facebook/goodwill/feed/data/ThrowbackFeedStories;

    move-result-object v2

    goto/16 :goto_0

    .line 2188048
    :cond_5
    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->x()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2188049
    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v16

    goto :goto_2

    .line 2188050
    :cond_6
    invoke-virtual/range {v21 .. v21}, Lcom/facebook/goodwill/feed/protocol/FetchThrowbackFeedGraphQLModels$ThrowbackFeedFragmentModel;->n()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2188051
    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v20

    goto :goto_3
.end method
