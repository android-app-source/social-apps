.class public LX/FVL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static p:LX/0Xm;


# instance fields
.field public final a:Landroid/content/Context;

.field private final b:LX/FV6;

.field private final c:LX/FVw;

.field public final d:LX/17W;

.field public final e:LX/FWJ;

.field public final f:LX/FW1;

.field public final g:LX/FVu;

.field public final h:LX/5up;

.field public final i:LX/16H;

.field private final j:LX/FWL;

.field public final k:Landroid/content/res/Resources;

.field public final l:LX/0kL;

.field public final m:LX/0hy;

.field public final n:Lcom/facebook/content/SecureContextHelper;

.field public final o:LX/D3w;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/FV6;LX/FVw;LX/17W;LX/FWJ;LX/FW1;LX/FVu;LX/5up;LX/16H;LX/FWL;Landroid/content/res/Resources;LX/0kL;LX/0hy;Lcom/facebook/content/SecureContextHelper;LX/D3w;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2250690
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2250691
    iput-object p1, p0, LX/FVL;->a:Landroid/content/Context;

    .line 2250692
    iput-object p2, p0, LX/FVL;->b:LX/FV6;

    .line 2250693
    iput-object p3, p0, LX/FVL;->c:LX/FVw;

    .line 2250694
    iput-object p4, p0, LX/FVL;->d:LX/17W;

    .line 2250695
    iput-object p5, p0, LX/FVL;->e:LX/FWJ;

    .line 2250696
    iput-object p6, p0, LX/FVL;->f:LX/FW1;

    .line 2250697
    iput-object p7, p0, LX/FVL;->g:LX/FVu;

    .line 2250698
    iput-object p8, p0, LX/FVL;->h:LX/5up;

    .line 2250699
    iput-object p9, p0, LX/FVL;->i:LX/16H;

    .line 2250700
    iput-object p10, p0, LX/FVL;->j:LX/FWL;

    .line 2250701
    iput-object p11, p0, LX/FVL;->k:Landroid/content/res/Resources;

    .line 2250702
    iput-object p12, p0, LX/FVL;->l:LX/0kL;

    .line 2250703
    iput-object p13, p0, LX/FVL;->m:LX/0hy;

    .line 2250704
    iput-object p14, p0, LX/FVL;->n:Lcom/facebook/content/SecureContextHelper;

    .line 2250705
    iput-object p15, p0, LX/FVL;->o:LX/D3w;

    .line 2250706
    return-void
.end method

.method public static a(LX/0QB;)LX/FVL;
    .locals 3

    .prologue
    .line 2250682
    const-class v1, LX/FVL;

    monitor-enter v1

    .line 2250683
    :try_start_0
    sget-object v0, LX/FVL;->p:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2250684
    sput-object v2, LX/FVL;->p:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2250685
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2250686
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/FVL;->b(LX/0QB;)LX/FVL;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2250687
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FVL;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2250688
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2250689
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/FVL;
    .locals 17

    .prologue
    .line 2250680
    new-instance v1, LX/FVL;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/FV6;->a(LX/0QB;)LX/FV6;

    move-result-object v3

    check-cast v3, LX/FV6;

    const-class v4, LX/FVw;

    move-object/from16 v0, p0

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/FVw;

    invoke-static/range {p0 .. p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v5

    check-cast v5, LX/17W;

    invoke-static/range {p0 .. p0}, LX/FWJ;->a(LX/0QB;)LX/FWJ;

    move-result-object v6

    check-cast v6, LX/FWJ;

    invoke-static/range {p0 .. p0}, LX/FW1;->a(LX/0QB;)LX/FW1;

    move-result-object v7

    check-cast v7, LX/FW1;

    invoke-static/range {p0 .. p0}, LX/FVu;->a(LX/0QB;)LX/FVu;

    move-result-object v8

    check-cast v8, LX/FVu;

    invoke-static/range {p0 .. p0}, LX/5up;->a(LX/0QB;)LX/5up;

    move-result-object v9

    check-cast v9, LX/5up;

    invoke-static/range {p0 .. p0}, LX/16H;->a(LX/0QB;)LX/16H;

    move-result-object v10

    check-cast v10, LX/16H;

    invoke-static/range {p0 .. p0}, LX/FWL;->a(LX/0QB;)LX/FWL;

    move-result-object v11

    check-cast v11, LX/FWL;

    invoke-static/range {p0 .. p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v12

    check-cast v12, Landroid/content/res/Resources;

    invoke-static/range {p0 .. p0}, LX/0kL;->a(LX/0QB;)LX/0kL;

    move-result-object v13

    check-cast v13, LX/0kL;

    invoke-static/range {p0 .. p0}, LX/10P;->a(LX/0QB;)LX/10P;

    move-result-object v14

    check-cast v14, LX/0hy;

    invoke-static/range {p0 .. p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v15

    check-cast v15, Lcom/facebook/content/SecureContextHelper;

    invoke-static/range {p0 .. p0}, LX/D3w;->a(LX/0QB;)LX/D3w;

    move-result-object v16

    check-cast v16, LX/D3w;

    invoke-direct/range {v1 .. v16}, LX/FVL;-><init>(Landroid/content/Context;LX/FV6;LX/FVw;LX/17W;LX/FWJ;LX/FW1;LX/FVu;LX/5up;LX/16H;LX/FWL;Landroid/content/res/Resources;LX/0kL;LX/0hy;Lcom/facebook/content/SecureContextHelper;LX/D3w;)V

    .line 2250681
    return-object v1
.end method

.method public static e(LX/FVL;LX/FVt;)LX/2h0;
    .locals 1

    .prologue
    .line 2250679
    new-instance v0, LX/FVJ;

    invoke-direct {v0, p0, p1}, LX/FVJ;-><init>(LX/FVL;LX/FVt;)V

    return-object v0
.end method


# virtual methods
.method public final a(LX/FVt;)V
    .locals 5
    .param p1    # LX/FVt;
        .annotation build Ljavax/annotation/Nonnull;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2250527
    iget-object v0, p1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v0, v0

    .line 2250528
    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Saved item target is null."

    invoke-static {v0, v3}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2250529
    iget-object v0, p1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v0, v0

    .line 2250530
    invoke-virtual {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Saved item ID is null or empty."

    invoke-static {v1, v0}, LX/0PB;->checkArgument(ZLjava/lang/Object;)V

    .line 2250531
    invoke-static {p1}, LX/FWJ;->d(LX/FVt;)Lcom/facebook/graphql/enums/GraphQLSavedState;

    move-result-object v0

    .line 2250532
    sget-object v1, LX/FVK;->a:[I

    invoke-virtual {v0}, Lcom/facebook/graphql/enums/GraphQLSavedState;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 2250533
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Operation to update saved state to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " cannot be undone."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    move v0, v2

    .line 2250534
    goto :goto_0

    :cond_1
    move v1, v2

    .line 2250535
    goto :goto_1

    .line 2250536
    :pswitch_0
    iget-object v0, p0, LX/FVL;->i:LX/16H;

    .line 2250537
    iget-object v1, p1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v1, v1

    .line 2250538
    invoke-virtual {v1}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/8bb;->UNDO_ARCHIVE:LX/8bb;

    invoke-virtual {v0, v1, v2}, LX/16H;->a(Ljava/lang/String;LX/8bb;)V

    .line 2250539
    iget-object v0, p0, LX/FVL;->h:LX/5up;

    .line 2250540
    iget-object v1, p1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v1, v1

    .line 2250541
    invoke-virtual {v1}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v1

    const-string v2, "native_saved_dashboard"

    const-string v3, "undo_button"

    invoke-static {p0, p1}, LX/FVL;->e(LX/FVL;LX/FVt;)LX/2h0;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/5up;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    .line 2250542
    iget-object v0, p0, LX/FVL;->f:LX/FW1;

    new-instance v1, LX/FW2;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/FVu;->a(LX/FVt;Lcom/facebook/graphql/enums/GraphQLSavedState;Z)LX/FVt;

    move-result-object v2

    invoke-direct {v1, v2}, LX/FW2;-><init>(LX/FVt;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2250543
    :goto_2
    return-void

    .line 2250544
    :pswitch_1
    iget-object v0, p0, LX/FVL;->i:LX/16H;

    .line 2250545
    iget-object v1, p1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v1, v1

    .line 2250546
    invoke-virtual {v1}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v1

    sget-object v2, LX/8bb;->UNDO_UNARCHIVE:LX/8bb;

    invoke-virtual {v0, v1, v2}, LX/16H;->a(Ljava/lang/String;LX/8bb;)V

    .line 2250547
    iget-object v0, p0, LX/FVL;->h:LX/5up;

    .line 2250548
    iget-object v1, p1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v1, v1

    .line 2250549
    invoke-virtual {v1}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v1

    const-string v2, "native_saved_dashboard"

    const-string v3, "undo_button"

    invoke-static {p0, p1}, LX/FVL;->e(LX/FVL;LX/FVt;)LX/2h0;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, LX/5up;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/2h0;)V

    .line 2250550
    iget-object v0, p0, LX/FVL;->f:LX/FW1;

    new-instance v1, LX/FW2;

    sget-object v3, Lcom/facebook/graphql/enums/GraphQLSavedState;->ARCHIVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    const/4 v4, 0x0

    invoke-static {p1, v3, v4}, LX/FVu;->a(LX/FVt;Lcom/facebook/graphql/enums/GraphQLSavedState;Z)LX/FVt;

    move-result-object v2

    invoke-direct {v1, v2}, LX/FW2;-><init>(LX/FVt;)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    .line 2250551
    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(LX/FVt;Landroid/view/View;Landroid/support/v4/app/Fragment;Ljava/lang/String;)V
    .locals 8
    .param p4    # Ljava/lang/String;
        .annotation build Lcom/facebook/graphql/calls/CollectionCurationMechanismsValue;
        .end annotation
    .end param

    .prologue
    .line 2250651
    iget-object v0, p0, LX/FVL;->b:LX/FV6;

    iget-object v1, p0, LX/FVL;->c:LX/FVw;

    .line 2250652
    new-instance p0, LX/FVv;

    invoke-static {v1}, LX/FWJ;->a(LX/0QB;)LX/FWJ;

    move-result-object v2

    check-cast v2, LX/FWJ;

    invoke-direct {p0, p1, v2}, LX/FVv;-><init>(LX/FVt;LX/FWJ;)V

    .line 2250653
    move-object v1, p0

    .line 2250654
    new-instance v4, LX/6WS;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v4, v2}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2250655
    invoke-virtual {v4}, LX/5OM;->c()LX/5OG;

    move-result-object v5

    .line 2250656
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v3

    .line 2250657
    iget-object v2, v0, LX/FV6;->b:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FV3;

    .line 2250658
    invoke-interface {v2}, LX/FV3;->a()Ljava/lang/Class;

    move-result-object v7

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {v7, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2250659
    invoke-interface {v2, v1}, LX/FV3;->b(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2250660
    invoke-virtual {v3, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_0

    .line 2250661
    :cond_1
    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    move-object v6, v2

    .line 2250662
    invoke-virtual {v6}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 2250663
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v7

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    if-ge v3, v7, :cond_2

    invoke-virtual {v6, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/FV3;

    .line 2250664
    invoke-interface {v2, v1}, LX/FV3;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 2250665
    if-nez p0, :cond_5

    invoke-interface {v2}, LX/FV3;->b()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v5, p0}, LX/5OG;->a(Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object p0

    .line 2250666
    :goto_2
    invoke-interface {v2}, LX/FV3;->d()I

    move-result p1

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2250667
    new-instance p1, LX/FV5;

    invoke-direct {p1, v0, v2, v1, p3}, LX/FV5;-><init>(LX/FV6;LX/FV3;LX/FVH;Landroid/support/v4/app/Fragment;)V

    invoke-interface {p0, p1}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2250668
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 2250669
    :cond_2
    invoke-virtual {v4, p2}, LX/0ht;->a(Landroid/view/View;)V

    .line 2250670
    iget-object v2, v0, LX/FV6;->a:LX/16H;

    const-string v3, "saved_dashboard"

    invoke-interface {v1}, LX/FVE;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/0XM;->nullToEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 2250671
    invoke-static {}, LX/0Px;->builder()LX/0Pz;

    move-result-object v0

    .line 2250672
    invoke-virtual {v6}, LX/0Px;->size()I

    move-result v1

    const/4 v5, 0x0

    move v7, v5

    :goto_3
    if-ge v7, v1, :cond_3

    invoke-virtual {v6, v7}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/FV3;

    .line 2250673
    invoke-interface {v5}, LX/FV3;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2250674
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto :goto_3

    .line 2250675
    :cond_3
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v5

    move-object v5, v5

    .line 2250676
    invoke-virtual {v2, v3, v4, v5, p4}, LX/16H;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;)V

    .line 2250677
    :cond_4
    return-void

    .line 2250678
    :cond_5
    invoke-interface {v2}, LX/FV3;->b()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v5, p1, p0}, LX/5OG;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)LX/3Ai;

    move-result-object p0

    goto :goto_2
.end method

.method public final a(Landroid/widget/Adapter;ILX/0am;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/Adapter;",
            "I",
            "LX/0am",
            "<",
            "LX/79Y;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2250552
    invoke-interface {p1, p2}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .line 2250553
    instance-of v0, v1, LX/FVt;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, LX/FVt;

    .line 2250554
    iget-boolean p1, v0, LX/FVt;->j:Z

    move v0, p1

    .line 2250555
    if-eqz v0, :cond_1

    .line 2250556
    :cond_0
    :goto_0
    return-void

    .line 2250557
    :cond_1
    check-cast v1, LX/FVt;

    .line 2250558
    iget-object v0, v1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v0, v0

    .line 2250559
    if-nez v0, :cond_3

    const-string v0, ""

    move-object p1, v0

    .line 2250560
    :goto_1
    invoke-static {p3}, LX/FWL;->a(LX/0am;)LX/0am;

    move-result-object v0

    sget-object p2, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;->ALL:Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    invoke-virtual {v0, p2}, LX/0am;->or(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;

    .line 2250561
    iget-object p2, p0, LX/FVL;->i:LX/16H;

    invoke-virtual {p2, p1, v0}, LX/16H;->a(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedDashboardSectionType;)V

    .line 2250562
    invoke-virtual {v1}, LX/FVt;->m()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2250563
    iget-object v2, v1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v2, v2

    .line 2250564
    if-eqz v2, :cond_4

    .line 2250565
    new-instance v2, LX/2oI;

    invoke-direct {v2}, LX/2oI;-><init>()V

    .line 2250566
    iget-object v3, v1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v3, v3

    .line 2250567
    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v3

    .line 2250568
    iput-object v3, v2, LX/2oI;->N:Ljava/lang/String;

    .line 2250569
    move-object v2, v2

    .line 2250570
    iget-object v3, v1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v3, v3

    .line 2250571
    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->w()Z

    move-result v3

    .line 2250572
    iput-boolean v3, v2, LX/2oI;->aq:Z

    .line 2250573
    move-object v2, v2

    .line 2250574
    iget-object v3, v1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v3, v3

    .line 2250575
    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->G()I

    move-result v3

    .line 2250576
    iput v3, v2, LX/2oI;->bC:I

    .line 2250577
    move-object v2, v2

    .line 2250578
    iget-object v3, v1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v3, v3

    .line 2250579
    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->C()D

    move-result-wide v4

    .line 2250580
    iput-wide v4, v2, LX/2oI;->bx:D

    .line 2250581
    move-object v2, v2

    .line 2250582
    iget-object v3, v1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v3, v3

    .line 2250583
    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->F()Ljava/lang/String;

    move-result-object v3

    .line 2250584
    iput-object v3, v2, LX/2oI;->bA:Ljava/lang/String;

    .line 2250585
    move-object v2, v2

    .line 2250586
    iget-object v3, v1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v3, v3

    .line 2250587
    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->E()Ljava/lang/String;

    move-result-object v3

    .line 2250588
    iput-object v3, v2, LX/2oI;->bz:Ljava/lang/String;

    .line 2250589
    move-object v2, v2

    .line 2250590
    iget-object v3, v1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v3, v3

    .line 2250591
    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->D()D

    move-result-wide v4

    .line 2250592
    iput-wide v4, v2, LX/2oI;->by:D

    .line 2250593
    move-object v2, v2

    .line 2250594
    iget-object v3, v1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v3, v3

    .line 2250595
    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->s()I

    move-result v3

    .line 2250596
    iput v3, v2, LX/2oI;->aa:I

    .line 2250597
    move-object v2, v2

    .line 2250598
    iget-object v3, v1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v3, v3

    .line 2250599
    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->t()I

    move-result v3

    .line 2250600
    iput v3, v2, LX/2oI;->ab:I

    .line 2250601
    move-object v2, v2

    .line 2250602
    iget-object v3, v1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v3, v3

    .line 2250603
    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->u()I

    move-result v3

    .line 2250604
    iput v3, v2, LX/2oI;->ac:I

    .line 2250605
    move-object v2, v2

    .line 2250606
    iget-object v3, v1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v3, v3

    .line 2250607
    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->z()Ljava/lang/String;

    move-result-object v3

    .line 2250608
    iput-object v3, v2, LX/2oI;->aT:Ljava/lang/String;

    .line 2250609
    move-object v2, v2

    .line 2250610
    iget-object v3, p0, LX/FVL;->o:LX/D3w;

    invoke-virtual {v2}, LX/2oI;->a()Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    iget-object v4, p0, LX/FVL;->a:Landroid/content/Context;

    sget-object v5, LX/04D;->SAVED_DASHBOARD:LX/04D;

    invoke-virtual {v3, v2, v4, v5}, LX/D3w;->a(Lcom/facebook/graphql/model/GraphQLVideo;Landroid/content/Context;LX/04D;)V

    .line 2250611
    :cond_2
    :goto_2
    goto/16 :goto_0

    .line 2250612
    :cond_3
    iget-object v0, v1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v0, v0

    .line 2250613
    invoke-virtual {v0}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v0

    move-object p1, v0

    goto/16 :goto_1

    .line 2250614
    :cond_4
    iget-object v2, v1, LX/FVt;->k:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;

    move-object v2, v2

    .line 2250615
    if-eqz v2, :cond_6

    .line 2250616
    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;->c()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;->b()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType;->g()I

    move-result v3

    const v4, 0x4c808d5

    if-ne v3, v4, :cond_5

    .line 2250617
    new-instance v3, LX/89k;

    invoke-direct {v3}, LX/89k;-><init>()V

    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;->c()Ljava/lang/String;

    move-result-object v2

    .line 2250618
    iput-object v2, v3, LX/89k;->b:Ljava/lang/String;

    .line 2250619
    move-object v2, v3

    .line 2250620
    invoke-virtual {v2}, LX/89k;->a()Lcom/facebook/ipc/feed/PermalinkStoryIdParams;

    move-result-object v2

    .line 2250621
    iget-object v3, p0, LX/FVL;->m:LX/0hy;

    invoke-interface {v3, v2}, LX/0hy;->a(Lcom/facebook/ipc/feed/PermalinkStoryIdParams;)Landroid/content/Intent;

    move-result-object v2

    .line 2250622
    iget-object v3, p0, LX/FVL;->n:Lcom/facebook/content/SecureContextHelper;

    iget-object v4, p0, LX/FVL;->a:Landroid/content/Context;

    invoke-interface {v3, v2, v4}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_2

    .line 2250623
    :cond_5
    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;->d()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 2250624
    iget-object v3, p0, LX/FVL;->d:LX/17W;

    iget-object v4, p0, LX/FVL;->a:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$PermalinkNodeModel;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_2

    .line 2250625
    :cond_6
    iget-object v2, p0, LX/FVL;->e:LX/FWJ;

    .line 2250626
    if-nez v1, :cond_8

    .line 2250627
    const/4 v3, 0x0

    .line 2250628
    :cond_7
    :goto_3
    move-object v2, v3

    .line 2250629
    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 2250630
    iget-object v3, p0, LX/FVL;->d:LX/17W;

    iget-object v4, p0, LX/FVL;->a:Landroid/content/Context;

    invoke-virtual {v3, v4, v2}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;)Z

    goto :goto_2

    .line 2250631
    :cond_8
    iget-object v3, v1, LX/FVt;->l:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;

    move-object v3, v3

    .line 2250632
    if-eqz v3, :cond_a

    .line 2250633
    iget-object v3, v1, LX/FVt;->l:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;

    move-object v3, v3

    .line 2250634
    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;->j()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel$InstantArticleModel;

    move-result-object v3

    if-eqz v3, :cond_a

    .line 2250635
    iget-object v3, v1, LX/FVt;->l:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;

    move-object v3, v3

    .line 2250636
    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel;->j()Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel$InstantArticleModel;

    move-result-object v4

    .line 2250637
    const-string v3, ""

    .line 2250638
    invoke-virtual {v4}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel$InstantArticleModel;->k()LX/1vs;

    move-result-object v5

    iget v5, v5, LX/1vs;->b:I

    if-eqz v5, :cond_9

    .line 2250639
    invoke-virtual {v4}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel$InstantArticleModel;->k()LX/1vs;

    move-result-object v3

    iget-object v5, v3, LX/1vs;->a:LX/15i;

    iget v3, v3, LX/1vs;->b:I

    const/4 v6, 0x0

    invoke-virtual {v5, v3, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 2250640
    :cond_9
    sget-object v5, LX/0ax;->ho:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedItemModel$GlobalShareModel$InstantArticleModel;->j()Ljava/lang/String;

    move-result-object v4

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {v5, v4, v3, v6}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 2250641
    :cond_a
    iget-object v3, v1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v3, v3

    .line 2250642
    if-eqz v3, :cond_c

    .line 2250643
    iget-object v3, v1, LX/FVt;->g:Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;

    move-object v3, v3

    .line 2250644
    invoke-virtual {v1}, LX/FVt;->m()Z

    move-result v4

    if-eqz v4, :cond_b

    .line 2250645
    sget-object v4, LX/0ax;->eW:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->z()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v5, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    .line 2250646
    :cond_b
    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v4

    if-eqz v4, :cond_c

    .line 2250647
    iget-object v4, v2, LX/FWJ;->a:LX/1nG;

    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->j()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v5

    invoke-virtual {v3}, Lcom/facebook/saved/protocol/graphql/FetchSavedItemsGraphQLModels$SavedDashboardItemFieldsModel;->r()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, LX/1nG;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2250648
    if-nez v3, :cond_7

    .line 2250649
    :cond_c
    iget-object v3, v1, LX/FVt;->f:Ljava/lang/String;

    move-object v3, v3

    .line 2250650
    goto/16 :goto_3
.end method
