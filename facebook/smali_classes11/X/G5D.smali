.class public final LX/G5D;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLStory;

.field public final synthetic b:LX/G5G;


# direct methods
.method public constructor <init>(LX/G5G;Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 0

    .prologue
    .line 2318594
    iput-object p1, p0, LX/G5D;->b:LX/G5G;

    iput-object p2, p0, LX/G5D;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 8

    .prologue
    .line 2318595
    iget-object v0, p0, LX/G5D;->b:LX/G5G;

    iget-object v0, v0, LX/G5G;->c:LX/G5H;

    iget-object v0, v0, LX/G5H;->i:LX/1Ck;

    const-string v1, "TASK_PIN_POST"

    iget-object v2, p0, LX/G5D;->b:LX/G5G;

    iget-object v2, v2, LX/G5G;->c:LX/G5H;

    iget-object v2, v2, LX/G5H;->g:LX/Fzo;

    iget-object v3, p0, LX/G5D;->b:LX/G5G;

    iget-object v3, v3, LX/G5G;->c:LX/G5H;

    iget-object v3, v3, LX/D2z;->a:LX/5SB;

    .line 2318596
    iget-wide v6, v3, LX/5SB;->b:J

    move-wide v4, v6

    .line 2318597
    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/G5D;->a:Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v4

    .line 2318598
    iget-object v5, v2, LX/Fzo;->b:LX/Fzy;

    .line 2318599
    new-instance v6, LX/4K9;

    invoke-direct {v6}, LX/4K9;-><init>()V

    .line 2318600
    const-string v7, "user_id"

    invoke-virtual {v6, v7, v3}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2318601
    move-object v6, v6

    .line 2318602
    const-string v7, "story_id"

    invoke-virtual {v6, v7, v4}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2318603
    move-object v6, v6

    .line 2318604
    new-instance v7, LX/G0N;

    invoke-direct {v7}, LX/G0N;-><init>()V

    move-object v7, v7

    .line 2318605
    const-string p1, "input"

    invoke-virtual {v7, p1, v6}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    .line 2318606
    invoke-static {v7}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v6

    invoke-static {v5, v6}, LX/Fzy;->a(LX/Fzy;LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    new-instance v7, LX/Fzs;

    invoke-direct {v7, v5}, LX/Fzs;-><init>(LX/Fzy;)V

    iget-object p1, v5, LX/Fzy;->b:Ljava/util/concurrent/ExecutorService;

    invoke-static {v6, v7, p1}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    move-object v5, v6

    .line 2318607
    new-instance v6, LX/Fzi;

    invoke-direct {v6, v2}, LX/Fzi;-><init>(LX/Fzo;)V

    iget-object v7, v2, LX/Fzo;->a:Ljava/util/concurrent/ExecutorService;

    invoke-static {v5, v6, v7}, LX/0Vg;->a(Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;Ljava/util/concurrent/Executor;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v5

    move-object v2, v5

    .line 2318608
    new-instance v3, LX/G5C;

    invoke-direct {v3, p0}, LX/G5C;-><init>(LX/G5D;)V

    invoke-virtual {v0, v1, v2, v3}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2318609
    const/4 v0, 0x0

    return v0
.end method
