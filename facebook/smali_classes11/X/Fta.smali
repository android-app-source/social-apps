.class public final LX/Fta;
.super LX/2eI;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/2eI",
        "<",
        "Lcom/facebook/timeline/favmediapicker/rows/environments/FavoriteMediaPickerEnvironment;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/List;

.field public final synthetic b:Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 2298857
    iput-object p1, p0, LX/Fta;->b:Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;

    iput-object p2, p0, LX/Fta;->a:Ljava/util/List;

    invoke-direct {p0}, LX/2eI;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2298858
    iget-object v0, p0, LX/Fta;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public final a(LX/2eI;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/sections/hscrollrecyclerview/PageSubParts",
            "<",
            "Lcom/facebook/timeline/favmediapicker/rows/environments/FavoriteMediaPickerEnvironment;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2298859
    iget-object v0, p0, LX/Fta;->b:Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;

    iget-object v0, v0, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->c:Lcom/facebook/timeline/favmediapicker/rows/parts/OpenCameraPartDefinition;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    .line 2298860
    iget-object v0, p0, LX/Fta;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/media/util/model/MediaModel;

    .line 2298861
    iget-object v2, v0, Lcom/facebook/media/util/model/MediaModel;->b:LX/1po;

    move-object v2, v2

    .line 2298862
    sget-object v3, LX/1po;->PHOTO:LX/1po;

    if-ne v2, v3, :cond_0

    .line 2298863
    iget-object v2, p0, LX/Fta;->b:Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;

    .line 2298864
    iget-object v3, v2, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->i:Landroid/content/res/Resources;

    const v4, 0x7f0b2292

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 2298865
    iget-object v4, v0, Lcom/facebook/media/util/model/MediaModel;->d:Landroid/net/Uri;

    move-object v4, v4

    .line 2298866
    new-instance v5, LX/2f8;

    invoke-direct {v5}, LX/2f8;-><init>()V

    sget-object v6, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->b:Lcom/facebook/common/callercontext/CallerContext;

    .line 2298867
    iput-object v6, v5, LX/2f8;->c:Lcom/facebook/common/callercontext/CallerContext;

    .line 2298868
    move-object v5, v5

    .line 2298869
    invoke-static {v4}, LX/1bX;->a(Landroid/net/Uri;)LX/1bX;

    move-result-object v6

    new-instance v7, LX/1o9;

    invoke-direct {v7, v3, v3}, LX/1o9;-><init>(II)V

    .line 2298870
    iput-object v7, v6, LX/1bX;->c:LX/1o9;

    .line 2298871
    move-object v3, v6

    .line 2298872
    invoke-virtual {v3}, LX/1bX;->n()LX/1bf;

    move-result-object v3

    .line 2298873
    iput-object v3, v5, LX/2f8;->a:LX/1bf;

    .line 2298874
    move-object v3, v5

    .line 2298875
    invoke-virtual {v3}, LX/2f8;->a()LX/2f9;

    move-result-object v3

    .line 2298876
    new-instance v5, LX/Fte;

    .line 2298877
    new-instance v6, LX/Ftb;

    invoke-direct {v6, v2, v4}, LX/Ftb;-><init>(Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;Landroid/net/Uri;)V

    move-object v4, v6

    .line 2298878
    invoke-direct {v5, v3, v4}, LX/Fte;-><init>(LX/2f9;Landroid/view/View$OnClickListener;)V

    move-object v0, v5

    .line 2298879
    iget-object v2, p0, LX/Fta;->b:Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;

    iget-object v2, v2, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->d:Lcom/facebook/timeline/favmediapicker/rows/parts/ClickablePhotoPartDefinition;

    invoke-virtual {p1, v2, v0}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    goto :goto_0

    .line 2298880
    :cond_0
    iget-object v2, p0, LX/Fta;->b:Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;

    .line 2298881
    iget-object v3, v0, Lcom/facebook/media/util/model/MediaModel;->d:Landroid/net/Uri;

    move-object v3, v3

    .line 2298882
    new-instance v4, LX/Ftg;

    .line 2298883
    new-instance v5, LX/Ftc;

    invoke-direct {v5, v2, v3}, LX/Ftc;-><init>(Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;Landroid/net/Uri;)V

    move-object v5, v5

    .line 2298884
    invoke-direct {v4, v3, v5}, LX/Ftg;-><init>(Landroid/net/Uri;Landroid/view/View$OnClickListener;)V

    move-object v0, v4

    .line 2298885
    iget-object v2, p0, LX/Fta;->b:Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;

    iget-object v2, v2, Lcom/facebook/timeline/favmediapicker/rows/parts/CameraRollPartDefinition;->e:Lcom/facebook/timeline/favmediapicker/rows/parts/ClickableVideoPartDefinition;

    invoke-virtual {p1, v2, v0}, LX/2eI;->a(LX/1RC;Ljava/lang/Object;)V

    goto :goto_0

    .line 2298886
    :cond_1
    return-void
.end method

.method public final c(I)V
    .locals 0

    .prologue
    .line 2298887
    return-void
.end method
