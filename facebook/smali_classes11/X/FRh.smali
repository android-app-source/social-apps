.class public LX/FRh;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6w3;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6w3",
        "<",
        "Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/737;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/6qh;


# direct methods
.method public constructor <init>(LX/0Or;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Or",
            "<",
            "LX/737;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2240681
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2240682
    iput-object p1, p0, LX/FRh;->a:LX/0Or;

    .line 2240683
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 2240684
    invoke-static {}, Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;->newBuilder()LX/FRv;

    move-result-object v0

    const/4 v1, 0x1

    .line 2240685
    iput-boolean v1, v0, LX/FRv;->a:Z

    .line 2240686
    move-object v0, v0

    .line 2240687
    invoke-virtual {v0}, LX/FRv;->c()Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;

    move-result-object v0

    invoke-direct {p0, v0}, LX/FRh;->a(Landroid/os/Parcelable;)V

    .line 2240688
    return-void
.end method

.method private a(Landroid/os/Parcelable;)V
    .locals 4

    .prologue
    .line 2240677
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2240678
    const-string v1, "extra_reset_data"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 2240679
    iget-object v1, p0, LX/FRh;->b:LX/6qh;

    new-instance v2, LX/73T;

    sget-object v3, LX/73S;->RESET:LX/73S;

    invoke-direct {v2, v3, v0}, LX/73T;-><init>(LX/73S;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2}, LX/6qh;->a(LX/73T;)V

    .line 2240680
    return-void
.end method


# virtual methods
.method public final a(LX/6qh;LX/70k;)V
    .locals 0

    .prologue
    .line 2240643
    iput-object p1, p0, LX/FRh;->b:LX/6qh;

    .line 2240644
    return-void
.end method

.method public final a(Lcom/facebook/payments/picker/model/PickerRunTimeData;IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 2240645
    check-cast p1, Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;

    const/4 v1, -0x1

    .line 2240646
    sparse-switch p2, :sswitch_data_0

    .line 2240647
    :cond_0
    :goto_0
    return-void

    .line 2240648
    :sswitch_0
    if-ne p3, v1, :cond_0

    .line 2240649
    iget-object v0, p0, LX/FRh;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/737;

    invoke-virtual {v0}, LX/0QG;->a()V

    .line 2240650
    :sswitch_1
    if-ne p3, v1, :cond_0

    .line 2240651
    const-string v0, "shipping_address"

    invoke-virtual {p4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/payments/shipping/model/MailingAddress;

    .line 2240652
    invoke-static {}, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->newBuilder()LX/FRt;

    move-result-object v2

    .line 2240653
    iget-object v1, p1, Lcom/facebook/payments/picker/model/SimplePickerRunTimeData;->c:Lcom/facebook/payments/picker/model/CoreClientData;

    move-object v1, v1

    .line 2240654
    check-cast v1, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;

    .line 2240655
    iget-object p1, v1, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 2240656
    iput-object p1, v2, LX/FRt;->a:Lcom/facebook/payments/paymentmethods/model/PaymentMethodsInfo;

    .line 2240657
    iget-object p1, v1, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->b:LX/0am;

    .line 2240658
    iput-object p1, v2, LX/FRt;->b:LX/0am;

    .line 2240659
    iget-object p1, v1, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->c:Lcom/facebook/payments/history/model/PaymentTransactions;

    .line 2240660
    iput-object p1, v2, LX/FRt;->c:Lcom/facebook/payments/history/model/PaymentTransactions;

    .line 2240661
    iget-object p1, v1, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2240662
    iput-object p1, v2, LX/FRt;->d:Lcom/facebook/payments/currency/CurrencyAmount;

    .line 2240663
    iget p1, v1, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->e:I

    .line 2240664
    iput p1, v2, LX/FRt;->e:I

    .line 2240665
    iget-object p1, v1, Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;->f:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 2240666
    iput-object p1, v2, LX/FRt;->f:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 2240667
    move-object v1, v2

    .line 2240668
    invoke-static {v0}, LX/0am;->fromNullable(Ljava/lang/Object;)LX/0am;

    move-result-object v0

    .line 2240669
    iput-object v0, v1, LX/FRt;->b:LX/0am;

    .line 2240670
    move-object v0, v1

    .line 2240671
    invoke-virtual {v0}, LX/FRt;->g()Lcom/facebook/payments/settings/model/PaymentSettingsCoreClientData;

    move-result-object v0

    invoke-direct {p0, v0}, LX/FRh;->a(Landroid/os/Parcelable;)V

    goto :goto_0

    .line 2240672
    :sswitch_2
    if-ne p3, v1, :cond_0

    .line 2240673
    invoke-direct {p0}, LX/FRh;->a()V

    goto :goto_0

    .line 2240674
    :sswitch_3
    invoke-direct {p0}, LX/FRh;->a()V

    goto :goto_0

    .line 2240675
    :sswitch_4
    if-ne p3, v1, :cond_0

    .line 2240676
    invoke-static {}, Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;->newBuilder()LX/FRv;

    move-result-object v0

    invoke-virtual {v0}, LX/FRv;->c()Lcom/facebook/payments/settings/model/PaymentSettingsPickerScreenFetcherParams;

    move-result-object v0

    invoke-direct {p0, v0}, LX/FRh;->a(Landroid/os/Parcelable;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x65 -> :sswitch_0
        0x192 -> :sswitch_1
        0x193 -> :sswitch_2
        0x194 -> :sswitch_4
    .end sparse-switch
.end method
