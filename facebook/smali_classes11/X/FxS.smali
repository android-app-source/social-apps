.class public final LX/FxS;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;)V
    .locals 0

    .prologue
    .line 2304941
    iput-object p1, p0, LX/FxS;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 12

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x5b931a63

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2304942
    iget-object v1, p0, LX/FxS;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-boolean v1, v1, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->H:Z

    if-eqz v1, :cond_0

    .line 2304943
    const v1, 0x48db82ee

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    .line 2304944
    :goto_0
    return-void

    .line 2304945
    :cond_0
    iget-object v1, p0, LX/FxS;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->a$redex0(Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;Z)V

    .line 2304946
    iget-object v1, p0, LX/FxS;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    const/4 v2, -0x1

    .line 2304947
    iput v2, v1, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->D:I

    .line 2304948
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    const-string v2, "button"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    const-string v2, "empty_state_view"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2304949
    :cond_1
    iget-object v1, p0, LX/FxS;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-object v1, v1, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->g:LX/BQ9;

    iget-object v2, p0, LX/FxS;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-wide v2, v2, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->F:J

    .line 2304950
    const/4 v9, 0x0

    sget-object v10, LX/9lQ;->SELF:LX/9lQ;

    const-string v11, "fav_photos_add_in_editing_view_click"

    move-object v6, v1

    move-wide v7, v2

    invoke-static/range {v6 .. v11}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2304951
    if-eqz v5, :cond_2

    .line 2304952
    iget-object v6, v1, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2304953
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    const-string v2, "tile"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2304954
    iget-object v1, p0, LX/FxS;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-object v1, v1, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->g:LX/BQ9;

    iget-object v2, p0, LX/FxS;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-wide v2, v2, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->F:J

    .line 2304955
    const/4 v9, 0x0

    sget-object v10, LX/9lQ;->SELF:LX/9lQ;

    const-string v11, "fav_photos_add_in_editing_view_tile_click"

    move-object v6, v1

    move-wide v7, v2

    invoke-static/range {v6 .. v11}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2304956
    if-eqz v5, :cond_3

    .line 2304957
    iget-object v6, v1, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2304958
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    const-string v2, "add_top_row_add_button"

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2304959
    iget-object v1, p0, LX/FxS;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-object v1, v1, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->g:LX/BQ9;

    iget-object v2, p0, LX/FxS;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-wide v2, v2, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->F:J

    .line 2304960
    const/4 v9, 0x0

    sget-object v10, LX/9lQ;->SELF:LX/9lQ;

    const-string v11, "fav_photos_add_in_editing_view_top_row_add_button_click"

    move-object v6, v1

    move-wide v7, v2

    invoke-static/range {v6 .. v11}, LX/BQ9;->a(LX/BQ9;JLjava/lang/String;LX/9lQ;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    .line 2304961
    if-eqz v5, :cond_4

    .line 2304962
    iget-object v6, v1, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v6, v5}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2304963
    :cond_4
    iget-object v1, p0, LX/FxS;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-object v1, v1, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->x:LX/FxI;

    iget-object v2, p0, LX/FxS;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-object v3, p0, LX/FxS;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-object v3, v3, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->B:LX/2yQ;

    iget-object v4, p0, LX/FxS;->a:Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;

    iget-object v4, v4, Lcom/facebook/timeline/header/intro/favphotos/edit/TimelineEditFavPhotosFragment;->C:LX/2dD;

    .line 2304964
    iget-object v5, v1, LX/FxI;->c:LX/0ad;

    sget-short v6, LX/0wf;->D:S

    const/4 v7, 0x0

    invoke-interface {v5, v6, v7}, LX/0ad;->a(SZ)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2304965
    invoke-static {v1, v2}, LX/FxI;->d(LX/FxI;Landroid/support/v4/app/Fragment;)V

    .line 2304966
    :goto_1
    const v1, 0x2f541e01

    invoke-static {v1, v0}, LX/02F;->a(II)V

    goto/16 :goto_0

    .line 2304967
    :cond_5
    new-instance v5, LX/6WS;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2304968
    iput-object v3, v5, LX/0ht;->I:LX/2yQ;

    .line 2304969
    iput-object v4, v5, LX/0ht;->H:LX/2dD;

    .line 2304970
    invoke-virtual {v5}, LX/5OM;->c()LX/5OG;

    move-result-object v6

    .line 2304971
    const v7, 0x7f081605

    invoke-virtual {v6, v7}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v7

    invoke-static {v1, v2}, LX/FxI;->a(LX/FxI;Landroid/support/v4/app/Fragment;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v8

    invoke-virtual {v7, v8}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2304972
    const v7, 0x7f0815cc

    invoke-virtual {v6, v7}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v6

    invoke-static {v1, v2}, LX/FxI;->c(LX/FxI;Landroid/support/v4/app/Fragment;)Landroid/view/MenuItem$OnMenuItemClickListener;

    move-result-object v7

    invoke-virtual {v6, v7}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2304973
    invoke-virtual {v5, p1}, LX/0ht;->a(Landroid/view/View;)V

    .line 2304974
    goto :goto_1
.end method
