.class public LX/Gqx;
.super LX/CrO;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/Gqx;


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(LX/8bZ;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2397061
    invoke-direct {p0, p1}, LX/CrO;-><init>(LX/8bZ;)V

    .line 2397062
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Gqx;->a:Z

    .line 2397063
    return-void
.end method

.method public static b(LX/0QB;)LX/Gqx;
    .locals 4

    .prologue
    .line 2397064
    sget-object v0, LX/Gqx;->b:LX/Gqx;

    if-nez v0, :cond_1

    .line 2397065
    const-class v1, LX/Gqx;

    monitor-enter v1

    .line 2397066
    :try_start_0
    sget-object v0, LX/Gqx;->b:LX/Gqx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2397067
    if-eqz v2, :cond_0

    .line 2397068
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2397069
    new-instance p0, LX/Gqx;

    invoke-static {v0}, LX/8bZ;->b(LX/0QB;)LX/8bZ;

    move-result-object v3

    check-cast v3, LX/8bZ;

    invoke-direct {p0, v3}, LX/Gqx;-><init>(LX/8bZ;)V

    .line 2397070
    move-object v0, p0

    .line 2397071
    sput-object v0, LX/Gqx;->b:LX/Gqx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2397072
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2397073
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2397074
    :cond_1
    sget-object v0, LX/Gqx;->b:LX/Gqx;

    return-object v0

    .line 2397075
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2397076
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(LX/CrN;Landroid/content/Context;LX/Ctg;Z)LX/CqX;
    .locals 3

    .prologue
    .line 2397077
    new-instance v1, LX/CrK;

    invoke-direct {v1, p2}, LX/CrK;-><init>(Landroid/content/Context;)V

    .line 2397078
    sget-object v0, LX/Gqw;->a:[I

    invoke-virtual {p1}, LX/CrN;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 2397079
    new-instance v0, LX/Gqu;

    invoke-direct {v0, p3, v1}, LX/Gqu;-><init>(LX/Ctg;LX/CrK;)V

    .line 2397080
    iget-boolean v1, p0, LX/Gqx;->a:Z

    .line 2397081
    iput-boolean v1, v0, LX/Gqu;->b:Z

    .line 2397082
    :goto_0
    return-object v0

    .line 2397083
    :pswitch_0
    new-instance v0, LX/Cr9;

    invoke-direct {v0, p3, v1}, LX/Cr9;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 2397084
    :pswitch_1
    new-instance v0, LX/CrA;

    invoke-direct {v0, p3, v1}, LX/CrA;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 2397085
    :pswitch_2
    new-instance v0, LX/Gqv;

    invoke-direct {v0, p3, v1}, LX/Gqv;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 2397086
    :pswitch_3
    new-instance v0, LX/Gqu;

    invoke-direct {v0, p3, v1}, LX/Gqu;-><init>(LX/Ctg;LX/CrK;)V

    .line 2397087
    iget-boolean v1, p0, LX/Gqx;->a:Z

    .line 2397088
    iput-boolean v1, v0, LX/Gqu;->b:Z

    .line 2397089
    goto :goto_0

    .line 2397090
    :pswitch_4
    new-instance v0, LX/Cqp;

    invoke-direct {v0, p3, v1}, LX/Cqp;-><init>(LX/Ctg;LX/CrK;)V

    .line 2397091
    iget-boolean v1, p0, LX/Gqx;->a:Z

    .line 2397092
    iput-boolean v1, v0, LX/Cqp;->a:Z

    .line 2397093
    goto :goto_0

    .line 2397094
    :pswitch_5
    new-instance v0, LX/Gr4;

    invoke-direct {v0, p3, v1}, LX/Gr4;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 2397095
    :pswitch_6
    new-instance v0, LX/Gqq;

    invoke-direct {v0, p3, v1}, LX/Gqq;-><init>(LX/Ctg;LX/CrK;)V

    .line 2397096
    iget-boolean v1, p0, LX/Gqx;->a:Z

    .line 2397097
    iput-boolean v1, v0, LX/Gqq;->a:Z

    .line 2397098
    goto :goto_0

    .line 2397099
    :pswitch_7
    new-instance v0, LX/Gqt;

    invoke-direct {v0, p3, v1}, LX/Gqt;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final a(LX/CrN;Landroid/content/Context;LX/Ctg;ZZZ)LX/CqX;
    .locals 3

    .prologue
    .line 2397100
    iput-boolean p6, p0, LX/Gqx;->a:Z

    .line 2397101
    if-eqz p5, :cond_0

    .line 2397102
    new-instance v1, LX/CrK;

    invoke-direct {v1, p2}, LX/CrK;-><init>(Landroid/content/Context;)V

    .line 2397103
    sget-object v0, LX/Gqw;->a:[I

    invoke-virtual {p1}, LX/CrN;->ordinal()I

    move-result v2

    aget v0, v0, v2

    sparse-switch v0, :sswitch_data_0

    .line 2397104
    :cond_0
    invoke-virtual {p0, p1, p2, p3, p4}, LX/CrO;->a(LX/CrN;Landroid/content/Context;LX/Ctg;Z)LX/CqX;

    move-result-object v0

    :goto_0
    return-object v0

    .line 2397105
    :sswitch_0
    new-instance v0, LX/Gqz;

    invoke-direct {v0, p3, v1}, LX/Gqz;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    .line 2397106
    :sswitch_1
    new-instance v0, LX/Gr1;

    invoke-direct {v0, p3, v1}, LX/Gr1;-><init>(LX/Ctg;LX/CrK;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x7 -> :sswitch_1
        0xa -> :sswitch_0
    .end sparse-switch
.end method
