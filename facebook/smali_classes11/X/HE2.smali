.class public final LX/HE2;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

.field public final synthetic b:LX/HE3;


# direct methods
.method public constructor <init>(LX/HE3;Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;)V
    .locals 0

    .prologue
    .line 2441380
    iput-object p1, p0, LX/HE2;->b:LX/HE3;

    iput-object p2, p0, LX/HE2;->a:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x1

    const v1, 0x6fdd7d40

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2441381
    iget-object v1, p0, LX/HE2;->b:LX/HE3;

    iget-object v1, v1, LX/HE3;->h:LX/HDw;

    iget-object v2, p0, LX/HE2;->b:LX/HE3;

    iget-object v2, v2, LX/HE3;->g:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 2441382
    iget-object v4, v1, LX/HDw;->a:Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;

    .line 2441383
    new-instance v5, LX/4Hs;

    invoke-direct {v5}, LX/4Hs;-><init>()V

    iget-wide v7, v4, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->e:J

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    .line 2441384
    const-string v7, "page_id"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2441385
    move-object v5, v5

    .line 2441386
    invoke-virtual {v2}, Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;->toString()Ljava/lang/String;

    move-result-object v6

    .line 2441387
    const-string v7, "status"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2441388
    move-object v5, v5

    .line 2441389
    const-string v6, "PROFILE"

    .line 2441390
    const-string v7, "location"

    invoke-virtual {v5, v7, v6}, LX/0gS;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2441391
    move-object v5, v5

    .line 2441392
    new-instance v6, LX/HE5;

    invoke-direct {v6}, LX/HE5;-><init>()V

    move-object v6, v6

    .line 2441393
    const-string v7, "input"

    invoke-virtual {v6, v7, v5}, LX/0gW;->a(Ljava/lang/String;LX/0gS;)LX/0gW;

    move-result-object v5

    check-cast v5, LX/HE5;

    .line 2441394
    invoke-static {v5}, LX/0zO;->a(LX/0zP;)LX/399;

    move-result-object v7

    .line 2441395
    iget-object v5, v4, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->d:LX/HE1;

    iget-boolean v5, v5, LX/HE1;->a:Z

    iget-object v6, v4, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->d:LX/HE1;

    iget-object v6, v6, LX/HE1;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    invoke-static {v4, v5, v2, v6}, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->a$redex0(Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;ZLcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    .line 2441396
    iget-object v5, v4, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->d:LX/HE1;

    iget-object v8, v5, LX/HE1;->c:Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    .line 2441397
    iget-object v5, v4, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->d:LX/HE1;

    iget-object v9, v5, LX/HE1;->b:Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;

    .line 2441398
    iget-object v5, v4, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->a:LX/0Ot;

    invoke-interface {v5}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, LX/1Ck;

    const-string v10, "page_news_feed_status_mutation"

    iget-object v6, v4, Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;->b:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/0tX;

    invoke-virtual {v6, v7}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v6

    new-instance v7, LX/HDz;

    invoke-direct {v7, v4, v9, v8}, LX/HDz;-><init>(Lcom/facebook/pages/common/followpage/PagesSubscriptionSettingsFragment;Lcom/facebook/graphql/enums/GraphQLSecondarySubscribeStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)V

    invoke-virtual {v5, v10, v6, v7}, LX/1Ck;->a(Ljava/lang/Object;Lcom/google/common/util/concurrent/ListenableFuture;LX/0Ve;)V

    .line 2441399
    const v1, -0x6a3cc518

    invoke-static {v3, v3, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
