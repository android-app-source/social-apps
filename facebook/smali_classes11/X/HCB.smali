.class public final enum LX/HCB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/HCB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/HCB;

.field public static final enum BUTTONS:LX/HCB;

.field public static final enum TABS:LX/HCB;

.field public static final enum TEMPLATE:LX/HCB;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2438215
    new-instance v0, LX/HCB;

    const-string v1, "TEMPLATE"

    invoke-direct {v0, v1, v2}, LX/HCB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HCB;->TEMPLATE:LX/HCB;

    .line 2438216
    new-instance v0, LX/HCB;

    const-string v1, "BUTTONS"

    invoke-direct {v0, v1, v3}, LX/HCB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HCB;->BUTTONS:LX/HCB;

    .line 2438217
    new-instance v0, LX/HCB;

    const-string v1, "TABS"

    invoke-direct {v0, v1, v4}, LX/HCB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/HCB;->TABS:LX/HCB;

    .line 2438218
    const/4 v0, 0x3

    new-array v0, v0, [LX/HCB;

    sget-object v1, LX/HCB;->TEMPLATE:LX/HCB;

    aput-object v1, v0, v2

    sget-object v1, LX/HCB;->BUTTONS:LX/HCB;

    aput-object v1, v0, v3

    sget-object v1, LX/HCB;->TABS:LX/HCB;

    aput-object v1, v0, v4

    sput-object v0, LX/HCB;->$VALUES:[LX/HCB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2438219
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/HCB;
    .locals 1

    .prologue
    .line 2438220
    const-class v0, LX/HCB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/HCB;

    return-object v0
.end method

.method public static values()[LX/HCB;
    .locals 1

    .prologue
    .line 2438221
    sget-object v0, LX/HCB;->$VALUES:[LX/HCB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/HCB;

    return-object v0
.end method
