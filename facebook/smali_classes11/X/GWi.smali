.class public final LX/GWi;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

.field public final synthetic b:LX/GWj;


# direct methods
.method public constructor <init>(LX/GWj;Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)V
    .locals 0

    .prologue
    .line 2362676
    iput-object p1, p0, LX/GWi;->b:LX/GWj;

    iput-object p2, p0, LX/GWi;->a:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x1

    const v1, 0x5a221cc6

    invoke-static {v5, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2362677
    iget-object v1, p0, LX/GWi;->b:LX/GWj;

    iget-object v1, v1, LX/GWj;->b:LX/GMm;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, LX/GWi;->a:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    invoke-virtual {v3}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->t()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;->l()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, LX/GWi;->a:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    invoke-virtual {v4}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->r()Ljava/lang/String;

    move-result-object v4

    .line 2362678
    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    const-string p0, "pages_manager_activity_tab"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    .line 2362679
    sget-object v3, LX/8wL;->PROMOTE_PRODUCT:LX/8wL;

    const v4, 0x7f080b05

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2, v3, v4, p0}, LX/8wJ;->a(Landroid/content/Context;LX/8wL;Ljava/lang/Integer;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 2362680
    const-string v4, "page_id"

    invoke-virtual {v3, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2362681
    const-string v4, "promotion_target_id"

    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2362682
    move-object v6, v3

    .line 2362683
    iget-object p0, v1, LX/GMm;->a:Lcom/facebook/content/SecureContextHelper;

    invoke-interface {p0, v6, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2362684
    const v1, -0x19e47d2

    invoke-static {v5, v5, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
