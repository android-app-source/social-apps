.class public final enum LX/G91;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/G91;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/G91;

.field public static final enum BYTE_SEGMENTS:LX/G91;

.field public static final enum ERROR_CORRECTION_LEVEL:LX/G91;

.field public static final enum ISSUE_NUMBER:LX/G91;

.field public static final enum ORIENTATION:LX/G91;

.field public static final enum OTHER:LX/G91;

.field public static final enum PDF417_EXTRA_METADATA:LX/G91;

.field public static final enum POSSIBLE_COUNTRY:LX/G91;

.field public static final enum STRUCTURED_APPEND_PARITY:LX/G91;

.field public static final enum STRUCTURED_APPEND_SEQUENCE:LX/G91;

.field public static final enum SUGGESTED_PRICE:LX/G91;

.field public static final enum UPC_EAN_EXTENSION:LX/G91;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2321441
    new-instance v0, LX/G91;

    const-string v1, "OTHER"

    invoke-direct {v0, v1, v3}, LX/G91;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G91;->OTHER:LX/G91;

    .line 2321442
    new-instance v0, LX/G91;

    const-string v1, "ORIENTATION"

    invoke-direct {v0, v1, v4}, LX/G91;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G91;->ORIENTATION:LX/G91;

    .line 2321443
    new-instance v0, LX/G91;

    const-string v1, "BYTE_SEGMENTS"

    invoke-direct {v0, v1, v5}, LX/G91;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G91;->BYTE_SEGMENTS:LX/G91;

    .line 2321444
    new-instance v0, LX/G91;

    const-string v1, "ERROR_CORRECTION_LEVEL"

    invoke-direct {v0, v1, v6}, LX/G91;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G91;->ERROR_CORRECTION_LEVEL:LX/G91;

    .line 2321445
    new-instance v0, LX/G91;

    const-string v1, "ISSUE_NUMBER"

    invoke-direct {v0, v1, v7}, LX/G91;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G91;->ISSUE_NUMBER:LX/G91;

    .line 2321446
    new-instance v0, LX/G91;

    const-string v1, "SUGGESTED_PRICE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/G91;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G91;->SUGGESTED_PRICE:LX/G91;

    .line 2321447
    new-instance v0, LX/G91;

    const-string v1, "POSSIBLE_COUNTRY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/G91;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G91;->POSSIBLE_COUNTRY:LX/G91;

    .line 2321448
    new-instance v0, LX/G91;

    const-string v1, "UPC_EAN_EXTENSION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/G91;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G91;->UPC_EAN_EXTENSION:LX/G91;

    .line 2321449
    new-instance v0, LX/G91;

    const-string v1, "PDF417_EXTRA_METADATA"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/G91;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G91;->PDF417_EXTRA_METADATA:LX/G91;

    .line 2321450
    new-instance v0, LX/G91;

    const-string v1, "STRUCTURED_APPEND_SEQUENCE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, LX/G91;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G91;->STRUCTURED_APPEND_SEQUENCE:LX/G91;

    .line 2321451
    new-instance v0, LX/G91;

    const-string v1, "STRUCTURED_APPEND_PARITY"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, LX/G91;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G91;->STRUCTURED_APPEND_PARITY:LX/G91;

    .line 2321452
    const/16 v0, 0xb

    new-array v0, v0, [LX/G91;

    sget-object v1, LX/G91;->OTHER:LX/G91;

    aput-object v1, v0, v3

    sget-object v1, LX/G91;->ORIENTATION:LX/G91;

    aput-object v1, v0, v4

    sget-object v1, LX/G91;->BYTE_SEGMENTS:LX/G91;

    aput-object v1, v0, v5

    sget-object v1, LX/G91;->ERROR_CORRECTION_LEVEL:LX/G91;

    aput-object v1, v0, v6

    sget-object v1, LX/G91;->ISSUE_NUMBER:LX/G91;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/G91;->SUGGESTED_PRICE:LX/G91;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/G91;->POSSIBLE_COUNTRY:LX/G91;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/G91;->UPC_EAN_EXTENSION:LX/G91;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/G91;->PDF417_EXTRA_METADATA:LX/G91;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/G91;->STRUCTURED_APPEND_SEQUENCE:LX/G91;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/G91;->STRUCTURED_APPEND_PARITY:LX/G91;

    aput-object v2, v0, v1

    sput-object v0, LX/G91;->$VALUES:[LX/G91;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2321453
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/G91;
    .locals 1

    .prologue
    .line 2321454
    const-class v0, LX/G91;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/G91;

    return-object v0
.end method

.method public static values()[LX/G91;
    .locals 1

    .prologue
    .line 2321455
    sget-object v0, LX/G91;->$VALUES:[LX/G91;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/G91;

    return-object v0
.end method
