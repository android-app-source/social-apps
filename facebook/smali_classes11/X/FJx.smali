.class public LX/FJx;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static volatile e:LX/FJx;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/facebook/prefs/shared/FbSharedPreferences;

.field public final d:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2224199
    const-class v0, LX/FJx;

    sput-object v0, LX/FJx;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/concurrent/ExecutorService;)V
    .locals 0
    .param p3    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2224213
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2224214
    iput-object p1, p0, LX/FJx;->b:Landroid/content/Context;

    .line 2224215
    iput-object p2, p0, LX/FJx;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    .line 2224216
    iput-object p3, p0, LX/FJx;->d:Ljava/util/concurrent/ExecutorService;

    .line 2224217
    return-void
.end method

.method public static a(LX/0QB;)LX/FJx;
    .locals 6

    .prologue
    .line 2224218
    sget-object v0, LX/FJx;->e:LX/FJx;

    if-nez v0, :cond_1

    .line 2224219
    const-class v1, LX/FJx;

    monitor-enter v1

    .line 2224220
    :try_start_0
    sget-object v0, LX/FJx;->e:LX/FJx;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2224221
    if-eqz v2, :cond_0

    .line 2224222
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2224223
    new-instance p0, LX/FJx;

    const-class v3, Landroid/content/Context;

    invoke-interface {v0, v3}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/Context;

    invoke-static {v0}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v4

    check-cast v4, Lcom/facebook/prefs/shared/FbSharedPreferences;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v5

    check-cast v5, Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v3, v4, v5}, LX/FJx;-><init>(Landroid/content/Context;Lcom/facebook/prefs/shared/FbSharedPreferences;Ljava/util/concurrent/ExecutorService;)V

    .line 2224224
    move-object v0, p0

    .line 2224225
    sput-object v0, LX/FJx;->e:LX/FJx;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2224226
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2224227
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2224228
    :cond_1
    sget-object v0, LX/FJx;->e:LX/FJx;

    return-object v0

    .line 2224229
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2224230
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method private f()Lcom/facebook/messaging/model/threads/NotificationSetting;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2224211
    iget-object v0, p0, LX/FJx;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0db;->J:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threads/NotificationSetting;->b(J)Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v0

    return-object v0
.end method

.method private g()Lcom/facebook/messaging/model/threads/NotificationSetting;
    .locals 4
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2224212
    iget-object v0, p0, LX/FJx;->c:Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v1, LX/0db;->aD:LX/0Tn;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/facebook/messaging/model/threads/NotificationSetting;->b(J)Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final c()V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2224207
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/FJx;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2224208
    const-string v1, "NotificationsPrefsService.SYNC_GLOBAL_FROM_CLIENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2224209
    iget-object v1, p0, LX/FJx;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 2224210
    return-void
.end method

.method public final d()V
    .locals 3
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .prologue
    .line 2224201
    invoke-virtual {p0}, LX/FJx;->e()LX/FK4;

    move-result-object v0

    .line 2224202
    invoke-virtual {v0}, LX/FK4;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2224203
    :goto_0
    return-void

    .line 2224204
    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, LX/FJx;->b:Landroid/content/Context;

    const-class v2, Lcom/facebook/messaging/prefs/notifications/NotificationPrefsSyncService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2224205
    const-string v1, "NotificationsPrefsService.SYNC_GLOBAL_FROM_SERVER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 2224206
    iget-object v1, p0, LX/FJx;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0
.end method

.method public final e()LX/FK4;
    .locals 3

    .prologue
    .line 2224200
    new-instance v0, LX/FK4;

    invoke-direct {p0}, LX/FJx;->f()Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v1

    invoke-direct {p0}, LX/FJx;->g()Lcom/facebook/messaging/model/threads/NotificationSetting;

    move-result-object v2

    invoke-direct {v0, v1, v2}, LX/FK4;-><init>(Lcom/facebook/messaging/model/threads/NotificationSetting;Lcom/facebook/messaging/model/threads/NotificationSetting;)V

    return-object v0
.end method
