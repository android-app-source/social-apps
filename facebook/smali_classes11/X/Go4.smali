.class public LX/Go4;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static final c:Ljava/lang/String;

.field private static d:LX/0Xm;


# instance fields
.field public a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "LX/Go3;",
            ">;"
        }
    .end annotation
.end field

.field private b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2394068
    const-class v0, LX/Go4;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/Go4;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2394064
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2394065
    iput-object p1, p0, LX/Go4;->b:LX/0Ot;

    .line 2394066
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/Go4;->a:Ljava/util/HashMap;

    .line 2394067
    return-void
.end method

.method public static a(LX/0QB;)LX/Go4;
    .locals 4

    .prologue
    .line 2394053
    const-class v1, LX/Go4;

    monitor-enter v1

    .line 2394054
    :try_start_0
    sget-object v0, LX/Go4;->d:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2394055
    sput-object v2, LX/Go4;->d:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2394056
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2394057
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2394058
    new-instance v3, LX/Go4;

    const/16 p0, 0x259

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Go4;-><init>(LX/0Ot;)V

    .line 2394059
    move-object v0, v3

    .line 2394060
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2394061
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Go4;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2394062
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2394063
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 2394044
    iget-object v0, p0, LX/Go4;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2394045
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Go3;

    .line 2394046
    iget-boolean v2, v0, LX/Go3;->a:Z

    move v2, v2

    .line 2394047
    if-eqz v2, :cond_0

    .line 2394048
    invoke-static {v0}, LX/Go3;->j(LX/Go3;)V

    .line 2394049
    const/4 v2, 0x1

    iput-boolean v2, v0, LX/Go3;->c:Z

    .line 2394050
    invoke-static {v0}, LX/Go3;->i(LX/Go3;)V

    .line 2394051
    goto :goto_0

    .line 2394052
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2394024
    iget-object v0, p0, LX/Go4;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2394025
    iget-object v0, p0, LX/Go4;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Go3;

    .line 2394026
    iget-boolean v1, v0, LX/Go3;->b:Z

    move v1, v1

    .line 2394027
    if-eqz v1, :cond_0

    .line 2394028
    invoke-virtual {v0}, LX/Go3;->a()V

    .line 2394029
    :goto_0
    return-void

    .line 2394030
    :cond_0
    iget-object v0, p0, LX/Go4;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/Go4;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".setElementOnScreen"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error reporting element on screen. Element was not off screen to go on screen."

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    goto :goto_0

    .line 2394031
    :cond_1
    new-instance v0, LX/Go3;

    invoke-direct {v0}, LX/Go3;-><init>()V

    .line 2394032
    invoke-virtual {v0}, LX/Go3;->a()V

    .line 2394033
    iget-object v1, p0, LX/Go4;->a:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2394034
    iget-object v0, p0, LX/Go4;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2394035
    iget-object v0, p0, LX/Go4;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Go3;

    .line 2394036
    iget-boolean v1, v0, LX/Go3;->a:Z

    move v1, v1

    .line 2394037
    if-eqz v1, :cond_1

    .line 2394038
    invoke-static {v0}, LX/Go3;->j(LX/Go3;)V

    .line 2394039
    const/4 v1, 0x0

    iput-boolean v1, v0, LX/Go3;->a:Z

    .line 2394040
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/Go3;->b:Z

    .line 2394041
    invoke-static {v0}, LX/Go3;->i(LX/Go3;)V

    .line 2394042
    :cond_0
    :goto_0
    return-void

    .line 2394043
    :cond_1
    iget-object v0, p0, LX/Go4;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, LX/Go4;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".setElementOffScreen"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error reporting element off screen.Element was not on screen to go off screen."

    invoke-static {v1, v2}, LX/0VG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0VK;

    move-result-object v1

    invoke-virtual {v1}, LX/0VK;->g()LX/0VG;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/03V;->a(LX/0VG;)V

    goto :goto_0
.end method
