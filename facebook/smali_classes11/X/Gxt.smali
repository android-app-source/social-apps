.class public final enum LX/Gxt;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Gxt;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Gxt;

.field public static final enum SUGGESTIONS:LX/Gxt;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2408517
    new-instance v0, LX/Gxt;

    const-string v1, "SUGGESTIONS"

    invoke-direct {v0, v1, v2}, LX/Gxt;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Gxt;->SUGGESTIONS:LX/Gxt;

    .line 2408518
    const/4 v0, 0x1

    new-array v0, v0, [LX/Gxt;

    sget-object v1, LX/Gxt;->SUGGESTIONS:LX/Gxt;

    aput-object v1, v0, v2

    sput-object v0, LX/Gxt;->$VALUES:[LX/Gxt;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2408519
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Gxt;
    .locals 1

    .prologue
    .line 2408520
    const-class v0, LX/Gxt;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Gxt;

    return-object v0
.end method

.method public static values()[LX/Gxt;
    .locals 1

    .prologue
    .line 2408521
    sget-object v0, LX/Gxt;->$VALUES:[LX/Gxt;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Gxt;

    return-object v0
.end method
