.class public final LX/FQ7;
.super LX/2h0;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;


# direct methods
.method public constructor <init>(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;)V
    .locals 0

    .prologue
    .line 2238453
    iput-object p1, p0, LX/FQ7;->a:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    invoke-direct {p0}, LX/2h0;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceException(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 3

    .prologue
    .line 2238454
    iget-object v0, p0, LX/FQ7;->a:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->N:Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v0, v1, :cond_1

    .line 2238455
    iget-object v0, p0, LX/FQ7;->a:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v1, p0, LX/FQ7;->a:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v1, v1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->L:Ljava/lang/String;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-static {v0, v1, v2}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a$redex0(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 2238456
    iget-object v0, p0, LX/FQ7;->a:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2238457
    iput-object v1, v0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->N:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2238458
    iget-object v0, p0, LX/FQ7;->a:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->z:Lcom/facebook/resources/ui/FbImageButton;

    iget-object v1, p0, LX/FQ7;->a:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    invoke-virtual {v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->getUnsavedDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2238459
    :cond_0
    :goto_0
    iget-object v0, p0, LX/FQ7;->a:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/03V;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "updateSavedStateOnServer:onServiceException"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, LX/03V;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2238460
    iget-object v0, p0, LX/FQ7;->a:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->c:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0kL;

    new-instance v1, LX/27k;

    const v2, 0x7f080039

    invoke-direct {v1, v2}, LX/27k;-><init>(I)V

    invoke-virtual {v0, v1}, LX/0kL;->b(LX/27k;)LX/27l;

    .line 2238461
    return-void

    .line 2238462
    :cond_1
    iget-object v0, p0, LX/FQ7;->a:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->N:Lcom/facebook/graphql/enums/GraphQLSavedState;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->NOT_SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    if-ne v0, v1, :cond_0

    .line 2238463
    iget-object v0, p0, LX/FQ7;->a:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v1, p0, LX/FQ7;->a:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v1, v1, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->L:Ljava/lang/String;

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    invoke-static {v0, v1, v2}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->a$redex0(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLSavedState;)V

    .line 2238464
    iget-object v0, p0, LX/FQ7;->a:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    sget-object v1, Lcom/facebook/graphql/enums/GraphQLSavedState;->SAVED:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2238465
    iput-object v1, v0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->N:Lcom/facebook/graphql/enums/GraphQLSavedState;

    .line 2238466
    iget-object v0, p0, LX/FQ7;->a:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    iget-object v0, v0, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->z:Lcom/facebook/resources/ui/FbImageButton;

    iget-object v1, p0, LX/FQ7;->a:Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;

    invoke-virtual {v1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->getSavedDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final bridge synthetic onSuccessfulResult(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2238467
    return-void
.end method
