.class public final enum LX/GkB;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GkB;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GkB;

.field public static final enum EDIT_DONE:LX/GkB;

.field public static final enum REORDER:LX/GkB;

.field public static final enum STATUS_CHANGE:LX/GkB;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2388923
    new-instance v0, LX/GkB;

    const-string v1, "REORDER"

    invoke-direct {v0, v1, v2}, LX/GkB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GkB;->REORDER:LX/GkB;

    .line 2388924
    new-instance v0, LX/GkB;

    const-string v1, "STATUS_CHANGE"

    invoke-direct {v0, v1, v3}, LX/GkB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GkB;->STATUS_CHANGE:LX/GkB;

    .line 2388925
    new-instance v0, LX/GkB;

    const-string v1, "EDIT_DONE"

    invoke-direct {v0, v1, v4}, LX/GkB;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GkB;->EDIT_DONE:LX/GkB;

    .line 2388926
    const/4 v0, 0x3

    new-array v0, v0, [LX/GkB;

    sget-object v1, LX/GkB;->REORDER:LX/GkB;

    aput-object v1, v0, v2

    sget-object v1, LX/GkB;->STATUS_CHANGE:LX/GkB;

    aput-object v1, v0, v3

    sget-object v1, LX/GkB;->EDIT_DONE:LX/GkB;

    aput-object v1, v0, v4

    sput-object v0, LX/GkB;->$VALUES:[LX/GkB;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2388922
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GkB;
    .locals 1

    .prologue
    .line 2388920
    const-class v0, LX/GkB;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GkB;

    return-object v0
.end method

.method public static values()[LX/GkB;
    .locals 1

    .prologue
    .line 2388921
    sget-object v0, LX/GkB;->$VALUES:[LX/GkB;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GkB;

    return-object v0
.end method
