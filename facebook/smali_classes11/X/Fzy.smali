.class public LX/Fzy;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/4IT;

.field private static volatile d:LX/Fzy;


# instance fields
.field public b:Ljava/util/concurrent/ExecutorService;

.field public c:LX/0tX;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2308350
    new-instance v0, LX/4IT;

    invoke-direct {v0}, LX/4IT;-><init>()V

    sput-object v0, LX/Fzy;->a:LX/4IT;

    return-void
.end method

.method public constructor <init>(LX/0tX;Ljava/util/concurrent/ExecutorService;)V
    .locals 4
    .param p2    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/DefaultExecutorService;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 2308351
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2308352
    sget-object v0, LX/Fzy;->a:LX/4IT;

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4IT;->a(Ljava/lang/Double;)LX/4IT;

    .line 2308353
    sget-object v0, LX/Fzy;->a:LX/4IT;

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/4IT;->b(Ljava/lang/Double;)LX/4IT;

    .line 2308354
    iput-object p1, p0, LX/Fzy;->c:LX/0tX;

    .line 2308355
    iput-object p2, p0, LX/Fzy;->b:Ljava/util/concurrent/ExecutorService;

    .line 2308356
    return-void
.end method

.method public static a(LX/0QB;)LX/Fzy;
    .locals 5

    .prologue
    .line 2308357
    sget-object v0, LX/Fzy;->d:LX/Fzy;

    if-nez v0, :cond_1

    .line 2308358
    const-class v1, LX/Fzy;

    monitor-enter v1

    .line 2308359
    :try_start_0
    sget-object v0, LX/Fzy;->d:LX/Fzy;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2308360
    if-eqz v2, :cond_0

    .line 2308361
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2308362
    new-instance p0, LX/Fzy;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/0TH;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    invoke-direct {p0, v3, v4}, LX/Fzy;-><init>(LX/0tX;Ljava/util/concurrent/ExecutorService;)V

    .line 2308363
    move-object v0, p0

    .line 2308364
    sput-object v0, LX/Fzy;->d:LX/Fzy;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2308365
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2308366
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2308367
    :cond_1
    sget-object v0, LX/Fzy;->d:LX/Fzy;

    return-object v0

    .line 2308368
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2308369
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/Fzy;LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "LX/399",
            "<TT;>;)",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 2308370
    iget-object v0, p0, LX/Fzy;->c:LX/0tX;

    invoke-virtual {v0, p1}, LX/0tX;->a(LX/399;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v0

    return-object v0
.end method
