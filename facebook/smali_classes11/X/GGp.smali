.class public LX/GGp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GGc;
.implements LX/GGk;


# instance fields
.field public i:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

.field private j:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation
.end field

.field private final k:LX/GCB;

.field public l:LX/GF4;


# direct methods
.method public constructor <init>(LX/GID;LX/GKQ;LX/GIH;LX/GLj;LX/GIL;LX/GKv;LX/GEy;LX/GCB;LX/GF4;)V
    .locals 5
    .param p2    # LX/GKQ;
        .annotation runtime Lcom/facebook/adinterfaces/annotations/ForBoostedComponent;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2334569
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2334570
    iput-object p8, p0, LX/GGp;->k:LX/GCB;

    .line 2334571
    iput-object p9, p0, LX/GGp;->l:LX/GF4;

    .line 2334572
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    new-instance v1, LX/GEZ;

    const v2, 0x7f03006e

    sget-object v3, LX/GGp;->e:LX/GGX;

    sget-object v4, LX/8wK;->AD_PREVIEW:LX/8wK;

    invoke-direct {v1, v2, p2, v3, v4}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/GEZ;

    const v2, 0x7f030067

    sget-object v3, LX/GGp;->e:LX/GGX;

    sget-object v4, LX/8wK;->INFO_CARD:LX/8wK;

    invoke-direct {v1, v2, p3, v3, v4}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/GEZ;

    const v2, 0x7f030040

    sget-object v3, LX/GGp;->e:LX/GGX;

    sget-object v4, LX/8wK;->CALL_TO_ACTION:LX/8wK;

    invoke-direct {v1, v2, p1, v3, v4}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/GEZ;

    const v2, 0x7f030097

    sget-object v3, LX/GGp;->e:LX/GGX;

    sget-object v4, LX/8wK;->WEBSITE_URL:LX/8wK;

    invoke-direct {v1, v2, p4, v3, v4}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/GEZ;

    const v2, 0x7f03005e

    sget-object v3, LX/GGp;->g:LX/GGX;

    sget-object v4, LX/8wK;->ADDRESS:LX/8wK;

    invoke-direct {v1, v2, p5, v3, v4}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    new-instance v1, LX/GEZ;

    const v2, 0x7f030051

    sget-object v3, LX/GGp;->g:LX/GGX;

    sget-object v4, LX/8wK;->PHONE_NUMBER:LX/8wK;

    invoke-direct {v1, v2, p6, v3, v4}, LX/GEZ;-><init>(ILX/GHg;LX/GGX;LX/8wK;)V

    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    invoke-virtual {v0, p7}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    move-result-object v0

    .line 2334573
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    iput-object v0, p0, LX/GGp;->j:LX/0Px;

    .line 2334574
    return-void
.end method


# virtual methods
.method public final a()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/GEW;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2334575
    iget-object v0, p0, LX/GGp;->j:LX/0Px;

    return-object v0
.end method

.method public final a(Landroid/content/Intent;LX/GCY;)V
    .locals 1

    .prologue
    .line 2334576
    const-string v0, "data"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    iput-object v0, p0, LX/GGp;->i:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2334577
    iget-object v0, p0, LX/GGp;->i:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    invoke-interface {p2, v0}, LX/GCY;->a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    .line 2334578
    return-void
.end method

.method public final b()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;
    .locals 1

    .prologue
    .line 2334579
    iget-object v0, p0, LX/GGp;->k:LX/GCB;

    invoke-virtual {v0}, LX/GCB;->a()Lcom/facebook/widget/titlebar/TitleBarButtonSpec;

    move-result-object v0

    return-object v0
.end method

.method public final c()LX/GGh;
    .locals 1

    .prologue
    .line 2334580
    new-instance v0, LX/GGo;

    invoke-direct {v0, p0}, LX/GGo;-><init>(LX/GGp;)V

    return-object v0
.end method
