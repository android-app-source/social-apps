.class public LX/Ga2;
.super LX/1a1;
.source ""


# instance fields
.field private l:LX/Ga1;


# direct methods
.method public constructor <init>(LX/Ga1;)V
    .locals 0

    .prologue
    .line 2367811
    invoke-direct {p0, p1}, LX/1a1;-><init>(Landroid/view/View;)V

    .line 2367812
    iput-object p1, p0, LX/Ga2;->l:LX/Ga1;

    .line 2367813
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;Lcom/facebook/commerce/core/intent/MerchantInfoViewData;)V
    .locals 12

    .prologue
    .line 2367814
    iget-object v0, p0, LX/Ga2;->l:LX/Ga1;

    const/4 v4, 0x0

    .line 2367815
    iput-object p2, v0, LX/Ga1;->g:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    .line 2367816
    invoke-virtual {p1}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2367817
    invoke-virtual {p1}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v1, v4}, LX/15i;->j(II)I

    move-result v1

    .line 2367818
    iget-object v6, v0, LX/Ga1;->c:Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2367819
    const-string v6, ""

    .line 2367820
    div-int/lit8 v7, v1, 0xa

    int-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->floor(D)D

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmpl-double v7, v8, v10

    if-lez v7, :cond_2

    .line 2367821
    div-int/lit8 v6, v1, 0xa

    int-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-int v6, v6

    mul-int/lit8 v6, v6, 0xa

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, "+"

    invoke-virtual {v6, v7}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2367822
    :cond_0
    :goto_0
    iget-object v7, v0, LX/Ga1;->d:Landroid/widget/TextView;

    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2367823
    new-instance v1, LX/GaH;

    invoke-virtual {p1}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;->k()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/GaH;-><init>(Ljava/lang/String;)V

    iput-object v1, v0, LX/Ga1;->h:LX/GaH;

    .line 2367824
    iget-object v1, v0, LX/Ga1;->e:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v4}, Landroid/support/v7/widget/RecyclerView;->g_(I)V

    .line 2367825
    invoke-virtual {p1}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;->j()LX/1vs;

    move-result-object v1

    iget-object v2, v1, LX/1vs;->a:LX/15i;

    iget v1, v1, LX/1vs;->b:I

    .line 2367826
    iget-object v3, v0, LX/Ga1;->f:Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;

    iget-object v4, v0, LX/Ga1;->h:LX/GaH;

    iget-object v5, v0, LX/Ga1;->g:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    .line 2367827
    iput-object v5, v3, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->e:Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    .line 2367828
    const/4 v6, 0x1

    const v7, 0x6a396f09

    invoke-static {v2, v1, v6, v7}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-static {v6}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v6

    :goto_1
    iput-object v6, v3, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->i:LX/2uF;

    .line 2367829
    const/4 v6, 0x0

    invoke-virtual {v2, v1, v6}, LX/15i;->j(II)I

    move-result v6

    iput v6, v3, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->c:I

    .line 2367830
    iput-object v4, v3, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->f:LX/GaH;

    .line 2367831
    iget-object v6, v3, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->d:LX/GaB;

    if-eqz v6, :cond_1

    .line 2367832
    iget-object v6, v3, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->d:LX/GaB;

    iget-object v7, v3, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->f:LX/GaH;

    iget v8, v3, Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;->c:I

    invoke-virtual {v6, v7, v8}, LX/GaB;->a(LX/GaH;I)V

    .line 2367833
    :cond_1
    iget-object v1, v0, LX/Ga1;->f:Lcom/facebook/commerce/storefront/adapters/CommerceProductAdapter;

    invoke-virtual {v1}, LX/1OM;->notifyDataSetChanged()V

    .line 2367834
    return-void

    .line 2367835
    :cond_2
    const/4 v7, 0x1

    if-le v1, v7, :cond_0

    .line 2367836
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_0

    .line 2367837
    :cond_3
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v6

    goto :goto_1
.end method
