.class public final LX/Geh;
.super LX/1S3;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pk;",
        ">",
        "LX/1S3;"
    }
.end annotation


# static fields
.field private static a:LX/Geh;

.field public static final b:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Gef;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public c:LX/Gei;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2375953
    const/4 v0, 0x0

    sput-object v0, LX/Geh;->a:LX/Geh;

    .line 2375954
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Geh;->b:LX/0Zi;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 2375955
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2375956
    new-instance v0, LX/Gei;

    invoke-direct {v0}, LX/Gei;-><init>()V

    iput-object v0, p0, LX/Geh;->c:LX/Gei;

    .line 2375957
    return-void
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2375958
    const v0, 0x64ea6708

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized q()LX/Geh;
    .locals 2

    .prologue
    .line 2375959
    const-class v1, LX/Geh;

    monitor-enter v1

    :try_start_0
    sget-object v0, LX/Geh;->a:LX/Geh;

    if-nez v0, :cond_0

    .line 2375960
    new-instance v0, LX/Geh;

    invoke-direct {v0}, LX/Geh;-><init>()V

    sput-object v0, LX/Geh;->a:LX/Geh;

    .line 2375961
    :cond_0
    sget-object v0, LX/Geh;->a:LX/Geh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 2375962
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 1

    .prologue
    .line 2375963
    check-cast p2, LX/Geg;

    .line 2375964
    invoke-static {p1}, LX/1o2;->c(LX/1De;)LX/1o5;

    move-result-object v0

    const p0, 0x7f020aea

    invoke-virtual {v0, p0}, LX/1o5;->h(I)LX/1o5;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->c()LX/1Di;

    move-result-object v0

    const/16 p0, 0xd

    invoke-interface {v0, p0}, LX/1Di;->j(I)LX/1Di;

    move-result-object v0

    const/16 p0, 0xe

    invoke-interface {v0, p0}, LX/1Di;->r(I)LX/1Di;

    move-result-object v0

    .line 2375965
    const p0, 0x64ea6708

    const/4 p2, 0x0

    invoke-static {p1, p0, p2}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object p0

    move-object p0, p0

    .line 2375966
    invoke-interface {v0, p0}, LX/1Di;->a(LX/1dQ;)LX/1Di;

    move-result-object v0

    invoke-interface {v0}, LX/1Di;->k()LX/1Dg;

    move-result-object v0

    move-object v0, v0

    .line 2375967
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2375968
    invoke-static {}, LX/1dS;->b()V

    .line 2375969
    iget v0, p1, LX/1dQ;->b:I

    .line 2375970
    packed-switch v0, :pswitch_data_0

    .line 2375971
    :goto_0
    return-object v2

    .line 2375972
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2375973
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2375974
    check-cast v1, LX/Geg;

    .line 2375975
    iget-object p2, v1, LX/Geg;->a:Lcom/facebook/java2js/JSValue;

    .line 2375976
    const-string p0, "unit"

    invoke-virtual {p2, p0}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object p0

    const-string v1, "__native_object__"

    invoke-virtual {p0, v1}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object p0

    const-class v1, Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {p0, v1}, Lcom/facebook/java2js/JSValue;->to(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2375977
    const-string v1, "toolbox"

    invoke-virtual {p2, v1}, Lcom/facebook/java2js/JSValue;->getProperty(Ljava/lang/String;)Lcom/facebook/java2js/JSValue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/java2js/JSValue;->asJavaObject()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/1Pk;

    .line 2375978
    invoke-interface {v1}, LX/1Pk;->e()LX/1SX;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 2375979
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x64ea6708
        :pswitch_0
    .end packed-switch
.end method
