.class public LX/GAU;
.super Ljava/lang/Object;
.source ""


# static fields
.field public static final a:Ljava/lang/String;

.field private static b:Ljava/lang/String;

.field private static c:Ljava/util/regex/Pattern;

.field public static volatile q:Ljava/lang/String;


# instance fields
.field public d:Lcom/facebook/AccessToken;

.field public e:LX/GAZ;

.field public f:Ljava/lang/String;

.field public g:Lorg/json/JSONObject;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Z

.field public k:Landroid/os/Bundle;

.field public l:LX/GA2;

.field public m:Ljava/lang/String;

.field public n:Ljava/lang/Object;

.field public o:Ljava/lang/String;

.field public p:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2325357
    const-class v0, LX/GAU;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/GAU;->a:Ljava/lang/String;

    .line 2325358
    const-string v0, "^/?v\\d+\\.\\d+/(.*)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, LX/GAU;->c:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2325359
    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move-object v5, v1

    invoke-direct/range {v0 .. v5}, LX/GAU;-><init>(Lcom/facebook/AccessToken;Ljava/lang/String;Landroid/os/Bundle;LX/GAZ;LX/GA2;)V

    .line 2325360
    return-void
.end method

.method public constructor <init>(Lcom/facebook/AccessToken;Ljava/lang/String;Landroid/os/Bundle;LX/GAZ;LX/GA2;)V
    .locals 7

    .prologue
    .line 2325361
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, LX/GAU;-><init>(Lcom/facebook/AccessToken;Ljava/lang/String;Landroid/os/Bundle;LX/GAZ;LX/GA2;Ljava/lang/String;)V

    .line 2325362
    return-void
.end method

.method private constructor <init>(Lcom/facebook/AccessToken;Ljava/lang/String;Landroid/os/Bundle;LX/GAZ;LX/GA2;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2325363
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2325364
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GAU;->j:Z

    .line 2325365
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GAU;->p:Z

    .line 2325366
    iput-object p1, p0, LX/GAU;->d:Lcom/facebook/AccessToken;

    .line 2325367
    iput-object p2, p0, LX/GAU;->f:Ljava/lang/String;

    .line 2325368
    iput-object p6, p0, LX/GAU;->o:Ljava/lang/String;

    .line 2325369
    invoke-virtual {p0, p5}, LX/GAU;->a(LX/GA2;)V

    .line 2325370
    invoke-direct {p0, p4}, LX/GAU;->a(LX/GAZ;)V

    .line 2325371
    if-eqz p3, :cond_1

    .line 2325372
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, p3}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    iput-object v0, p0, LX/GAU;->k:Landroid/os/Bundle;

    .line 2325373
    :goto_0
    iget-object v0, p0, LX/GAU;->o:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 2325374
    const-string v0, "v2.5"

    move-object v0, v0

    .line 2325375
    iput-object v0, p0, LX/GAU;->o:Ljava/lang/String;

    .line 2325376
    :cond_0
    return-void

    .line 2325377
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, LX/GAU;->k:Landroid/os/Bundle;

    goto :goto_0
.end method

.method private static a(LX/GAU;)LX/GAY;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2325378
    new-array v0, v3, [LX/GAU;

    aput-object p0, v0, v2

    .line 2325379
    const-string v1, "requests"

    invoke-static {v0, v1}, LX/Gsd;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2325380
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 2325381
    new-instance v0, LX/GAX;

    invoke-direct {v0, v1}, LX/GAX;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, LX/GAU;->a(LX/GAX;)Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    .line 2325382
    move-object v0, v1

    .line 2325383
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-eq v1, v3, :cond_1

    .line 2325384
    :cond_0
    new-instance v0, LX/GAA;

    const-string v1, "invalid state: expected a single response"

    invoke-direct {v0, v1}, LX/GAA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2325385
    :cond_1
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GAY;

    return-object v0
.end method

.method public static a(LX/GAU;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2325386
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    .line 2325387
    iget-object v0, p0, LX/GAU;->k:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 2325388
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2325389
    iget-object v1, p0, LX/GAU;->k:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 2325390
    if-nez v1, :cond_1

    .line 2325391
    const-string v1, ""

    .line 2325392
    :cond_1
    invoke-static {v1}, LX/GAU;->e(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2325393
    invoke-static {v1}, LX/GAU;->f(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2325394
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0

    .line 2325395
    :cond_2
    iget-object v0, p0, LX/GAU;->e:LX/GAZ;

    sget-object v4, LX/GAZ;->GET:LX/GAZ;

    if-ne v0, v4, :cond_0

    .line 2325396
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Unsupported parameter type for GET request: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2325397
    :cond_3
    invoke-virtual {v2}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(LX/GAX;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/GAX;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/GAY;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2325398
    const-string v0, "requests"

    invoke-static {p0, v0}, LX/Gsd;->a(Ljava/util/Collection;Ljava/lang/String;)V

    .line 2325399
    :try_start_0
    invoke-static {p0}, LX/GAU;->c(LX/GAX;)Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 2325400
    :try_start_1
    invoke-static {v1, p0}, LX/GAU;->a(Ljava/net/HttpURLConnection;LX/GAX;)Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2325401
    invoke-static {v1}, LX/Gsc;->a(Ljava/net/URLConnection;)V

    :goto_0
    return-object v0

    .line 2325402
    :catch_0
    move-exception v0

    .line 2325403
    :try_start_2
    iget-object v2, p0, LX/GAX;->c:Ljava/util/List;

    move-object v2, v2

    .line 2325404
    const/4 v3, 0x0

    new-instance v4, LX/GAA;

    invoke-direct {v4, v0}, LX/GAA;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v2, v3, v4}, LX/GAY;->a(Ljava/util/List;Ljava/net/HttpURLConnection;LX/GAA;)Ljava/util/List;

    move-result-object v0

    .line 2325405
    invoke-static {p0, v0}, LX/GAU;->a(LX/GAX;Ljava/util/List;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2325406
    invoke-static {v1}, LX/Gsc;->a(Ljava/net/URLConnection;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1}, LX/Gsc;->a(Ljava/net/URLConnection;)V

    throw v0
.end method

.method public static a(Ljava/net/HttpURLConnection;LX/GAX;)Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/net/HttpURLConnection;",
            "LX/GAX;",
            ")",
            "Ljava/util/List",
            "<",
            "LX/GAY;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2325407
    invoke-static {p0, p1}, LX/GAY;->a(Ljava/net/HttpURLConnection;LX/GAX;)Ljava/util/List;

    move-result-object v0

    .line 2325408
    invoke-static {p0}, LX/Gsc;->a(Ljava/net/URLConnection;)V

    .line 2325409
    invoke-virtual {p1}, LX/GAX;->size()I

    move-result v1

    .line 2325410
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 2325411
    new-instance v2, LX/GAA;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "Received %d responses while expecting %d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v0

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, LX/GAA;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2325412
    :cond_0
    invoke-static {p1, v0}, LX/GAU;->a(LX/GAX;Ljava/util/List;)V

    .line 2325413
    invoke-static {}, LX/GA8;->a()LX/GA8;

    move-result-object v1

    .line 2325414
    const/4 v8, 0x0

    .line 2325415
    iget-object v9, v1, LX/GA8;->d:Lcom/facebook/AccessToken;

    if-nez v9, :cond_3

    .line 2325416
    :cond_1
    :goto_0
    move v7, v8

    .line 2325417
    if-nez v7, :cond_2

    .line 2325418
    :goto_1
    return-object v0

    .line 2325419
    :cond_2
    const/4 v7, 0x0

    .line 2325420
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v8

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2325421
    invoke-static {v1, v7}, LX/GA8;->b(LX/GA8;LX/G9z;)V

    .line 2325422
    :goto_2
    goto :goto_1

    .line 2325423
    :cond_3
    new-instance v9, Ljava/util/Date;

    invoke-direct {v9}, Ljava/util/Date;-><init>()V

    invoke-virtual {v9}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    .line 2325424
    iget-object v10, v1, LX/GA8;->d:Lcom/facebook/AccessToken;

    .line 2325425
    iget-object v11, v10, Lcom/facebook/AccessToken;->i:LX/GA9;

    move-object v10, v11

    .line 2325426
    invoke-virtual {v10}, LX/GA9;->canExtendToken()Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    iget-object v12, v1, LX/GA8;->f:Ljava/util/Date;

    invoke-virtual {v12}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    sub-long/2addr v10, v12

    const-wide/32 v12, 0x36ee80

    cmp-long v10, v10, v12

    if-lez v10, :cond_1

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    iget-object v9, v1, LX/GA8;->d:Lcom/facebook/AccessToken;

    .line 2325427
    iget-object v12, v9, Lcom/facebook/AccessToken;->j:Ljava/util/Date;

    move-object v9, v12

    .line 2325428
    invoke-virtual {v9}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    sub-long/2addr v10, v12

    const-wide/32 v12, 0x5265c00

    cmp-long v9, v10, v12

    if-lez v9, :cond_1

    const/4 v8, 0x1

    goto :goto_0

    .line 2325429
    :cond_4
    new-instance v8, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 2325430
    new-instance v9, Lcom/facebook/AccessTokenManager$1;

    invoke-direct {v9, v1, v7}, Lcom/facebook/AccessTokenManager$1;-><init>(LX/GA8;LX/G9z;)V

    const v10, -0x7c10cb82

    invoke-static {v8, v9, v10}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_2
.end method

.method private static a(LX/GAT;Ljava/util/Collection;Ljava/util/Map;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/GAT;",
            "Ljava/util/Collection",
            "<",
            "LX/GAU;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/GAQ;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2325578
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 2325579
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GAU;

    .line 2325580
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4}, Lorg/json/JSONObject;-><init>()V

    .line 2325581
    iget-object v3, v0, LX/GAU;->h:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 2325582
    const-string v3, "name"

    iget-object v5, v0, LX/GAU;->h:Ljava/lang/String;

    invoke-virtual {v4, v3, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2325583
    const-string v3, "omit_response_on_success"

    iget-boolean v5, v0, LX/GAU;->j:Z

    invoke-virtual {v4, v3, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 2325584
    :cond_0
    iget-object v3, v0, LX/GAU;->i:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 2325585
    const-string v3, "depends_on"

    iget-object v5, v0, LX/GAU;->i:Ljava/lang/String;

    invoke-virtual {v4, v3, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2325586
    :cond_1
    invoke-static {v0}, LX/GAU;->l(LX/GAU;)Ljava/lang/String;

    move-result-object v5

    .line 2325587
    const-string v3, "relative_url"

    invoke-virtual {v4, v3, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2325588
    const-string v3, "method"

    iget-object v6, v0, LX/GAU;->e:LX/GAZ;

    invoke-virtual {v4, v3, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2325589
    iget-object v3, v0, LX/GAU;->d:Lcom/facebook/AccessToken;

    if-eqz v3, :cond_2

    .line 2325590
    iget-object v3, v0, LX/GAU;->d:Lcom/facebook/AccessToken;

    .line 2325591
    iget-object v6, v3, Lcom/facebook/AccessToken;->h:Ljava/lang/String;

    move-object v3, v6

    .line 2325592
    invoke-static {v3}, LX/GsN;->a(Ljava/lang/String;)V

    .line 2325593
    :cond_2
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 2325594
    iget-object v3, v0, LX/GAU;->k:Landroid/os/Bundle;

    invoke-virtual {v3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    .line 2325595
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_3
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2325596
    iget-object v8, v0, LX/GAU;->k:Landroid/os/Bundle;

    invoke-virtual {v8, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 2325597
    invoke-static {v3}, LX/GAU;->d(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2325598
    sget-object v8, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v9, "%s%d"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string v12, "file"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v9, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 2325599
    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2325600
    new-instance v9, LX/GAQ;

    invoke-direct {v9, v0, v3}, LX/GAQ;-><init>(LX/GAU;Ljava/lang/Object;)V

    invoke-interface {p2, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 2325601
    :cond_4
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_5

    .line 2325602
    const-string v3, ","

    invoke-static {v3, v6}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    .line 2325603
    const-string v6, "attached_files"

    invoke-virtual {v4, v6, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2325604
    :cond_5
    iget-object v3, v0, LX/GAU;->g:Lorg/json/JSONObject;

    if-eqz v3, :cond_6

    .line 2325605
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2325606
    iget-object v6, v0, LX/GAU;->g:Lorg/json/JSONObject;

    new-instance v7, LX/GAP;

    invoke-direct {v7, v0, v3}, LX/GAP;-><init>(LX/GAU;Ljava/util/ArrayList;)V

    invoke-static {v6, v5, v7}, LX/GAU;->a(Lorg/json/JSONObject;Ljava/lang/String;LX/GAO;)V

    .line 2325607
    const-string v5, "&"

    invoke-static {v5, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    .line 2325608
    const-string v5, "body"

    invoke-virtual {v4, v5, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 2325609
    :cond_6
    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 2325610
    goto/16 :goto_0

    .line 2325611
    :cond_7
    const-string v0, "batch"

    const/4 v3, 0x0

    const/4 v9, 0x1

    const/4 v5, 0x0

    .line 2325612
    iget-object v2, p0, LX/GAT;->a:Ljava/io/OutputStream;

    instance-of v2, v2, LX/GAh;

    if-nez v2, :cond_9

    .line 2325613
    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, LX/GAT;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2325614
    :cond_8
    :goto_2
    return-void

    .line 2325615
    :cond_9
    iget-object v2, p0, LX/GAT;->a:Ljava/io/OutputStream;

    check-cast v2, LX/GAh;

    .line 2325616
    invoke-static {p0, v0, v3, v3}, LX/GAT;->a(LX/GAT;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2325617
    const-string v3, "["

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p0, v3, v4}, LX/GAT;->a(LX/GAT;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325618
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v5

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/GAU;

    .line 2325619
    invoke-virtual {v1, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 2325620
    invoke-interface {v2, v3}, LX/GAh;->a(LX/GAU;)V

    .line 2325621
    if-lez v4, :cond_a

    .line 2325622
    const-string v3, ",%s"

    new-array v8, v9, [Ljava/lang/Object;

    invoke-virtual {v7}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v8, v5

    invoke-static {p0, v3, v8}, LX/GAT;->a(LX/GAT;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325623
    :goto_4
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    .line 2325624
    goto :goto_3

    .line 2325625
    :cond_a
    const-string v3, "%s"

    new-array v8, v9, [Ljava/lang/Object;

    invoke-virtual {v7}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v8, v5

    invoke-static {p0, v3, v8}, LX/GAT;->a(LX/GAT;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_4

    .line 2325626
    :cond_b
    const-string v2, "]"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {p0, v2, v3}, LX/GAT;->a(LX/GAT;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2325627
    iget-object v2, p0, LX/GAT;->b:LX/GsN;

    if-eqz v2, :cond_8

    .line 2325628
    iget-object v2, p0, LX/GAT;->b:LX/GsN;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "    "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lorg/json/JSONArray;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/GsN;->a(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2
.end method

.method private static a(LX/GAX;LX/GsN;ILjava/net/URL;Ljava/io/OutputStream;Z)V
    .locals 7

    .prologue
    .line 2325431
    new-instance v1, LX/GAT;

    invoke-direct {v1, p4, p1, p5}, LX/GAT;-><init>(Ljava/io/OutputStream;LX/GsN;Z)V

    .line 2325432
    const/4 v0, 0x1

    if-ne p2, v0, :cond_7

    .line 2325433
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/GAX;->a(I)LX/GAU;

    move-result-object v2

    .line 2325434
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 2325435
    iget-object v0, v2, LX/GAU;->k:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2325436
    iget-object v5, v2, LX/GAU;->k:Landroid/os/Bundle;

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    .line 2325437
    invoke-static {v5}, LX/GAU;->d(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2325438
    new-instance v6, LX/GAQ;

    invoke-direct {v6, v2, v5}, LX/GAQ;-><init>(LX/GAU;Ljava/lang/Object;)V

    invoke-interface {v3, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2325439
    :cond_1
    if-eqz p1, :cond_2

    .line 2325440
    const-string v0, "  Parameters:\n"

    invoke-virtual {p1, v0}, LX/GsN;->b(Ljava/lang/String;)V

    .line 2325441
    :cond_2
    iget-object v0, v2, LX/GAU;->k:Landroid/os/Bundle;

    .line 2325442
    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v4

    .line 2325443
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2325444
    invoke-virtual {v0, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    .line 2325445
    invoke-static {v6}, LX/GAU;->e(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_3

    .line 2325446
    invoke-virtual {v1, v4, v6, v2}, LX/GAT;->a(Ljava/lang/String;Ljava/lang/Object;LX/GAU;)V

    goto :goto_1

    .line 2325447
    :cond_4
    if-eqz p1, :cond_5

    .line 2325448
    const-string v0, "  Attachments:\n"

    invoke-virtual {p1, v0}, LX/GsN;->b(Ljava/lang/String;)V

    .line 2325449
    :cond_5
    invoke-static {v3, v1}, LX/GAU;->a(Ljava/util/Map;LX/GAT;)V

    .line 2325450
    iget-object v0, v2, LX/GAU;->g:Lorg/json/JSONObject;

    if-eqz v0, :cond_6

    .line 2325451
    iget-object v0, v2, LX/GAU;->g:Lorg/json/JSONObject;

    invoke-virtual {p3}, Ljava/net/URL;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, LX/GAU;->a(Lorg/json/JSONObject;Ljava/lang/String;LX/GAO;)V

    .line 2325452
    :cond_6
    :goto_2
    return-void

    .line 2325453
    :cond_7
    invoke-static {p0}, LX/GAU;->g(LX/GAX;)Ljava/lang/String;

    move-result-object v0

    .line 2325454
    invoke-static {v0}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2325455
    new-instance v0, LX/GAA;

    const-string v1, "App ID was not specified at the request or Settings."

    invoke-direct {v0, v1}, LX/GAA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2325456
    :cond_8
    const-string v2, "batch_app_id"

    invoke-virtual {v1, v2, v0}, LX/GAT;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2325457
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2325458
    invoke-static {v1, p0, v0}, LX/GAU;->a(LX/GAT;Ljava/util/Collection;Ljava/util/Map;)V

    .line 2325459
    if-eqz p1, :cond_9

    .line 2325460
    const-string v2, "  Attachments:\n"

    invoke-virtual {p1, v2}, LX/GsN;->b(Ljava/lang/String;)V

    .line 2325461
    :cond_9
    invoke-static {v0, v1}, LX/GAU;->a(Ljava/util/Map;LX/GAT;)V

    goto :goto_2
.end method

.method private static a(LX/GAX;Ljava/net/HttpURLConnection;)V
    .locals 13

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 2325462
    new-instance v12, LX/GsN;

    sget-object v0, LX/GAb;->REQUESTS:LX/GAb;

    const-string v2, "Request"

    invoke-direct {v12, v0, v2}, LX/GsN;-><init>(LX/GAb;Ljava/lang/String;)V

    .line 2325463
    invoke-virtual {p0}, LX/GAX;->size()I

    move-result v2

    .line 2325464
    invoke-virtual {p0}, LX/GAX;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GAU;

    .line 2325465
    iget-object v3, v0, LX/GAU;->k:Landroid/os/Bundle;

    invoke-virtual {v3}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2325466
    iget-object v8, v0, LX/GAU;->k:Landroid/os/Bundle;

    invoke-virtual {v8, v3}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 2325467
    invoke-static {v3}, LX/GAU;->d(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2325468
    const/4 v0, 0x0

    .line 2325469
    :goto_0
    move v5, v0

    .line 2325470
    if-ne v2, v1, :cond_2

    invoke-virtual {p0, v6}, LX/GAX;->a(I)LX/GAU;

    move-result-object v0

    iget-object v0, v0, LX/GAU;->e:LX/GAZ;

    .line 2325471
    :goto_1
    invoke-virtual {v0}, LX/GAZ;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 2325472
    invoke-static {p1, v5}, LX/GAU;->a(Ljava/net/HttpURLConnection;Z)V

    .line 2325473
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getURL()Ljava/net/URL;

    move-result-object v3

    .line 2325474
    const-string v7, "Request:\n"

    invoke-virtual {v12, v7}, LX/GsN;->b(Ljava/lang/String;)V

    .line 2325475
    const-string v7, "Id"

    .line 2325476
    iget-object v8, p0, LX/GAX;->e:Ljava/lang/String;

    move-object v8, v8

    .line 2325477
    invoke-virtual {v12, v7, v8}, LX/GsN;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2325478
    const-string v7, "URL"

    invoke-virtual {v12, v7, v3}, LX/GsN;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2325479
    const-string v7, "Method"

    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getRequestMethod()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v12, v7, v8}, LX/GsN;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2325480
    const-string v7, "User-Agent"

    const-string v8, "User-Agent"

    invoke-virtual {p1, v8}, Ljava/net/HttpURLConnection;->getRequestProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v12, v7, v8}, LX/GsN;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2325481
    const-string v7, "Content-Type"

    const-string v8, "Content-Type"

    invoke-virtual {p1, v8}, Ljava/net/HttpURLConnection;->getRequestProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v12, v7, v8}, LX/GsN;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2325482
    iget v7, p0, LX/GAX;->d:I

    move v7, v7

    .line 2325483
    invoke-virtual {p1, v7}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 2325484
    iget v7, p0, LX/GAX;->d:I

    move v7, v7

    .line 2325485
    invoke-virtual {p1, v7}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 2325486
    sget-object v7, LX/GAZ;->POST:LX/GAZ;

    if-ne v0, v7, :cond_3

    move v0, v1

    .line 2325487
    :goto_2
    if-nez v0, :cond_4

    .line 2325488
    invoke-virtual {v12}, LX/GsN;->a()V

    .line 2325489
    :goto_3
    return-void

    .line 2325490
    :cond_2
    sget-object v0, LX/GAZ;->POST:LX/GAZ;

    goto :goto_1

    :cond_3
    move v0, v6

    .line 2325491
    goto :goto_2

    .line 2325492
    :cond_4
    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 2325493
    :try_start_0
    new-instance v7, Ljava/io/BufferedOutputStream;

    const v0, 0x6ea8a0b3

    invoke-static {p1, v0}, LX/04e;->c(Ljava/net/URLConnection;I)Ljava/io/OutputStream;

    move-result-object v0

    invoke-direct {v7, v0}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2325494
    if-eqz v5, :cond_5

    .line 2325495
    :try_start_1
    new-instance v0, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v0, v7}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    move-object v7, v0

    .line 2325496
    :cond_5
    invoke-static {p0}, LX/GAU;->d(LX/GAX;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 2325497
    new-instance v4, LX/GAi;

    .line 2325498
    iget-object v0, p0, LX/GAX;->b:Landroid/os/Handler;

    move-object v0, v0

    .line 2325499
    invoke-direct {v4, v0}, LX/GAi;-><init>(Landroid/os/Handler;)V

    .line 2325500
    const/4 v1, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v5}, LX/GAU;->a(LX/GAX;LX/GsN;ILjava/net/URL;Ljava/io/OutputStream;Z)V

    .line 2325501
    iget v0, v4, LX/GAi;->e:I

    move v0, v0

    .line 2325502
    iget-object v1, v4, LX/GAi;->a:Ljava/util/Map;

    move-object v9, v1

    .line 2325503
    new-instance v6, LX/GAj;

    int-to-long v10, v0

    move-object v8, p0

    invoke-direct/range {v6 .. v11}, LX/GAj;-><init>(Ljava/io/OutputStream;LX/GAX;Ljava/util/Map;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v4, v6

    :goto_4
    move-object v0, p0

    move-object v1, v12

    .line 2325504
    :try_start_2
    invoke-static/range {v0 .. v5}, LX/GAU;->a(LX/GAX;LX/GsN;ILjava/net/URL;Ljava/io/OutputStream;Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 2325505
    invoke-virtual {v4}, Ljava/io/OutputStream;->close()V

    .line 2325506
    invoke-virtual {v12}, LX/GsN;->a()V

    goto :goto_3

    .line 2325507
    :catchall_0
    move-exception v0

    move-object v7, v4

    :goto_5
    if-eqz v7, :cond_6

    .line 2325508
    invoke-virtual {v7}, Ljava/io/OutputStream;->close()V

    :cond_6
    throw v0

    .line 2325509
    :catchall_1
    move-exception v0

    goto :goto_5

    :catchall_2
    move-exception v0

    move-object v7, v4

    goto :goto_5

    :cond_7
    move-object v4, v7

    goto :goto_4

    :cond_8
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method private static a(LX/GAX;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/GAX;",
            "Ljava/util/List",
            "<",
            "LX/GAY;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2325510
    invoke-virtual {p0}, LX/GAX;->size()I

    move-result v1

    .line 2325511
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2325512
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 2325513
    invoke-virtual {p0, v0}, LX/GAX;->a(I)LX/GAU;

    move-result-object v3

    .line 2325514
    iget-object v4, v3, LX/GAU;->l:LX/GA2;

    if-eqz v4, :cond_0

    .line 2325515
    new-instance v4, Landroid/util/Pair;

    iget-object v3, v3, LX/GAU;->l:LX/GA2;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-direct {v4, v3, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2325516
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2325517
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 2325518
    new-instance v0, Lcom/facebook/GraphRequest$5;

    invoke-direct {v0, v2, p0}, Lcom/facebook/GraphRequest$5;-><init>(Ljava/util/ArrayList;LX/GAX;)V

    .line 2325519
    iget-object v1, p0, LX/GAX;->b:Landroid/os/Handler;

    move-object v1, v1

    .line 2325520
    if-nez v1, :cond_3

    .line 2325521
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 2325522
    :cond_2
    :goto_1
    return-void

    .line 2325523
    :cond_3
    const v2, 0x3e5edd37

    invoke-static {v1, v0, v2}, LX/03a;->a(Landroid/os/Handler;Ljava/lang/Runnable;I)Z

    goto :goto_1
.end method

.method private a(LX/GAZ;)V
    .locals 2

    .prologue
    .line 2325524
    iget-object v0, p0, LX/GAU;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    sget-object v0, LX/GAZ;->GET:LX/GAZ;

    if-eq p1, v0, :cond_0

    .line 2325525
    new-instance v0, LX/GAA;

    const-string v1, "Can\'t change HTTP method on request with overridden URL."

    invoke-direct {v0, v1}, LX/GAA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2325526
    :cond_0
    if-eqz p1, :cond_1

    :goto_0
    iput-object p1, p0, LX/GAU;->e:LX/GAZ;

    .line 2325527
    return-void

    .line 2325528
    :cond_1
    sget-object p1, LX/GAZ;->GET:LX/GAZ;

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/Object;LX/GAO;Z)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 2325542
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    .line 2325543
    const-class v2, Lorg/json/JSONObject;

    invoke-virtual {v2, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2325544
    check-cast p1, Lorg/json/JSONObject;

    .line 2325545
    if-eqz p3, :cond_0

    .line 2325546
    invoke-virtual {p1}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v2

    .line 2325547
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2325548
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2325549
    const-string v3, "%s[%s]"

    new-array v4, v8, [Ljava/lang/Object;

    aput-object p0, v4, v1

    aput-object v0, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2325550
    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v3, v0, p2, p3}, LX/GAU;->a(Ljava/lang/String;Ljava/lang/Object;LX/GAO;Z)V

    goto :goto_0

    .line 2325551
    :cond_0
    const-string v0, "id"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2325552
    const-string v0, "id"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p2, p3}, LX/GAU;->a(Ljava/lang/String;Ljava/lang/Object;LX/GAO;Z)V

    .line 2325553
    :cond_1
    :goto_1
    return-void

    .line 2325554
    :cond_2
    const-string v0, "url"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2325555
    const-string v0, "url"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p2, p3}, LX/GAU;->a(Ljava/lang/String;Ljava/lang/Object;LX/GAO;Z)V

    goto :goto_1

    .line 2325556
    :cond_3
    const-string v0, "fbsdk:create_object"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2325557
    invoke-virtual {p1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p2, p3}, LX/GAU;->a(Ljava/lang/String;Ljava/lang/Object;LX/GAO;Z)V

    goto :goto_1

    .line 2325558
    :cond_4
    const-class v2, Lorg/json/JSONArray;

    invoke-virtual {v2, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2325559
    check-cast p1, Lorg/json/JSONArray;

    .line 2325560
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    move v0, v1

    .line 2325561
    :goto_2
    if-ge v0, v2, :cond_1

    .line 2325562
    sget-object v3, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v4, "%s[%d]"

    new-array v5, v8, [Ljava/lang/Object;

    aput-object p0, v5, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2325563
    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->opt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-static {v3, v4, p2, p3}, LX/GAU;->a(Ljava/lang/String;Ljava/lang/Object;LX/GAO;Z)V

    .line 2325564
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2325565
    :cond_5
    const-class v1, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_6

    const-class v1, Ljava/lang/Number;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_6

    const-class v1, Ljava/lang/Boolean;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2325566
    :cond_6
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, p0, v0}, LX/GAO;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 2325567
    :cond_7
    const-class v1, Ljava/util/Date;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2325568
    check-cast p1, Ljava/util/Date;

    .line 2325569
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ssZ"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 2325570
    invoke-virtual {v0, p1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, p0, v0}, LX/GAO;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private static a(Ljava/net/HttpURLConnection;Z)V
    .locals 4

    .prologue
    .line 2325571
    if-eqz p1, :cond_0

    .line 2325572
    const-string v0, "Content-Type"

    const-string v1, "application/x-www-form-urlencoded"

    invoke-virtual {p0, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 2325573
    const-string v0, "Content-Encoding"

    const-string v1, "gzip"

    invoke-virtual {p0, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 2325574
    :goto_0
    return-void

    .line 2325575
    :cond_0
    const-string v0, "Content-Type"

    .line 2325576
    const-string v1, "multipart/form-data; boundary=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string p1, "3i2ndDfv2rTHiSisAbouNdArYfORhtTPEefj3q2f"

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v1, v1

    .line 2325577
    invoke-virtual {p0, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Ljava/util/Map;LX/GAT;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "LX/GAQ;",
            ">;",
            "LX/GAT;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2325348
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 2325349
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2325350
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/GAQ;

    .line 2325351
    iget-object v3, v1, LX/GAQ;->b:Ljava/lang/Object;

    move-object v3, v3

    .line 2325352
    invoke-static {v3}, LX/GAU;->d(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2325353
    iget-object v3, v1, LX/GAQ;->b:Ljava/lang/Object;

    move-object v3, v3

    .line 2325354
    iget-object v4, v1, LX/GAQ;->a:LX/GAU;

    move-object v1, v4

    .line 2325355
    invoke-virtual {p1, v0, v3, v1}, LX/GAT;->a(Ljava/lang/String;Ljava/lang/Object;LX/GAU;)V

    goto :goto_0

    .line 2325356
    :cond_1
    return-void
.end method

.method public static a(Lorg/json/JSONObject;Ljava/lang/String;LX/GAO;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2325529
    invoke-static {p1}, LX/GAU;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2325530
    const-string v0, ":"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    .line 2325531
    const-string v3, "?"

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v3

    .line 2325532
    const/4 v4, 0x3

    if-le v0, v4, :cond_1

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    if-ge v0, v3, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    move v3, v0

    .line 2325533
    :goto_1
    invoke-virtual {p0}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v5

    .line 2325534
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2325535
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2325536
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->opt(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    .line 2325537
    if-eqz v3, :cond_2

    const-string v4, "image"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    move v4, v1

    .line 2325538
    :goto_3
    invoke-static {v0, v6, p2, v4}, LX/GAU;->a(Ljava/lang/String;Ljava/lang/Object;LX/GAO;Z)V

    goto :goto_2

    :cond_1
    move v0, v2

    .line 2325539
    goto :goto_0

    :cond_2
    move v4, v2

    .line 2325540
    goto :goto_3

    .line 2325541
    :cond_3
    return-void

    :cond_4
    move v3, v2

    goto :goto_1
.end method

.method public static b(LX/GAX;)LX/GAV;
    .locals 3

    .prologue
    .line 2325203
    const-string v0, "requests"

    invoke-static {p0, v0}, LX/Gsd;->a(Ljava/util/Collection;Ljava/lang/String;)V

    .line 2325204
    new-instance v0, LX/GAV;

    invoke-direct {v0, p0}, LX/GAV;-><init>(LX/GAX;)V

    .line 2325205
    invoke-static {}, LX/GAK;->d()Ljava/util/concurrent/Executor;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/GAV;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2325206
    return-object v0
.end method

.method private static b(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2325216
    sget-object v1, LX/GAU;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 2325217
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2325218
    invoke-virtual {v1, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object p0

    .line 2325219
    :cond_0
    const-string v1, "me/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "/me/"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2325220
    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(LX/GAX;)Ljava/net/HttpURLConnection;
    .locals 10

    .prologue
    .line 2325221
    invoke-static {p0}, LX/GAU;->f(LX/GAX;)V

    .line 2325222
    :try_start_0
    invoke-virtual {p0}, LX/GAX;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 2325223
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, LX/GAX;->a(I)LX/GAU;

    move-result-object v1

    .line 2325224
    new-instance v0, Ljava/net/URL;

    .line 2325225
    iget-object v2, v1, LX/GAU;->m:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 2325226
    iget-object v2, v1, LX/GAU;->m:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2325227
    :goto_0
    move-object v1, v2

    .line 2325228
    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2325229
    :goto_1
    const/4 v1, 0x0

    .line 2325230
    :try_start_1
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;

    .line 2325231
    const-string v3, "User-Agent"

    const/4 v7, 0x2

    const/4 v0, 0x1

    const/4 v9, 0x0

    .line 2325232
    sget-object v4, LX/GAU;->q:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 2325233
    const-string v4, "%s.%s"

    new-array v5, v7, [Ljava/lang/Object;

    const-string v6, "FBAndroidSDK"

    aput-object v6, v5, v9

    const-string v6, "4.9.0"

    aput-object v6, v5, v0

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, LX/GAU;->q:Ljava/lang/String;

    .line 2325234
    sget-object v4, LX/GsK;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2325235
    invoke-static {v4}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2325236
    sget-object v5, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const-string v6, "%s/%s"

    new-array v7, v7, [Ljava/lang/Object;

    sget-object v8, LX/GAU;->q:Ljava/lang/String;

    aput-object v8, v7, v9

    aput-object v4, v7, v0

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    sput-object v4, LX/GAU;->q:Ljava/lang/String;

    .line 2325237
    :cond_0
    sget-object v4, LX/GAU;->q:Ljava/lang/String;

    move-object v4, v4

    .line 2325238
    invoke-virtual {v2, v3, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 2325239
    const-string v3, "Accept-Language"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 2325240
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setChunkedStreamingMode(I)V

    .line 2325241
    move-object v1, v2

    .line 2325242
    invoke-static {p0, v1}, LX/GAU;->a(LX/GAX;Ljava/net/HttpURLConnection;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 2325243
    return-object v1

    .line 2325244
    :cond_1
    :try_start_2
    new-instance v0, Ljava/net/URL;

    invoke-static {}, LX/GsW;->b()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/net/MalformedURLException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 2325245
    :catch_0
    move-exception v0

    .line 2325246
    new-instance v1, LX/GAA;

    const-string v2, "could not construct URL for request"

    invoke-direct {v1, v2, v0}, LX/GAA;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2325247
    :catch_1
    move-exception v0

    .line 2325248
    :goto_2
    invoke-static {v1}, LX/Gsc;->a(Ljava/net/URLConnection;)V

    .line 2325249
    new-instance v1, LX/GAA;

    const-string v2, "could not construct request body"

    invoke-direct {v1, v2, v0}, LX/GAA;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 2325250
    :catch_2
    move-exception v0

    goto :goto_2

    .line 2325251
    :cond_2
    iget-object v2, v1, LX/GAU;->e:LX/GAZ;

    move-object v2, v2

    .line 2325252
    sget-object v3, LX/GAZ;->POST:LX/GAZ;

    if-ne v2, v3, :cond_3

    iget-object v2, v1, LX/GAU;->f:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, v1, LX/GAU;->f:Ljava/lang/String;

    const-string v3, "/videos"

    invoke-virtual {v2, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2325253
    const-string v2, "https://graph-video.%s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 2325254
    sget-object v5, LX/GAK;->h:Ljava/lang/String;

    move-object v5, v5

    .line 2325255
    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object v2, v2

    .line 2325256
    :goto_3
    const-string v3, "%s/%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const/4 v2, 0x1

    invoke-static {v1}, LX/GAU;->n(LX/GAU;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2325257
    invoke-static {v1}, LX/GAU;->k(LX/GAU;)V

    .line 2325258
    invoke-static {v1, v2}, LX/GAU;->a(LX/GAU;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_0

    .line 2325259
    :cond_3
    invoke-static {}, LX/GsW;->b()Ljava/lang/String;

    move-result-object v2

    goto :goto_3
.end method

.method private static d(LX/GAX;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 2325207
    iget-object v0, p0, LX/GAX;->f:Ljava/util/List;

    move-object v0, v0

    .line 2325208
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GA5;

    .line 2325209
    instance-of v0, v0, LX/GAW;

    if-eqz v0, :cond_0

    move v0, v1

    .line 2325210
    :goto_0
    return v0

    .line 2325211
    :cond_1
    invoke-virtual {p0}, LX/GAX;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GAU;

    .line 2325212
    iget-object p0, v0, LX/GAU;->l:LX/GA2;

    move-object v0, p0

    .line 2325213
    instance-of v0, v0, LX/GAR;

    if-eqz v0, :cond_2

    move v0, v1

    .line 2325214
    goto :goto_0

    .line 2325215
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2325260
    instance-of v0, p0, Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    instance-of v0, p0, [B

    if-nez v0, :cond_0

    instance-of v0, p0, Landroid/net/Uri;

    if-nez v0, :cond_0

    instance-of v0, p0, Landroid/os/ParcelFileDescriptor;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/facebook/GraphRequest$ParcelableResourceWithMimeType;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 2325261
    instance-of v0, p0, Ljava/lang/String;

    if-nez v0, :cond_0

    instance-of v0, p0, Ljava/lang/Boolean;

    if-nez v0, :cond_0

    instance-of v0, p0, Ljava/lang/Number;

    if-nez v0, :cond_0

    instance-of v0, p0, Ljava/util/Date;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(Ljava/lang/Object;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2325262
    instance-of v0, p0, Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2325263
    check-cast p0, Ljava/lang/String;

    .line 2325264
    :goto_0
    return-object p0

    .line 2325265
    :cond_0
    instance-of v0, p0, Ljava/lang/Boolean;

    if-nez v0, :cond_1

    instance-of v0, p0, Ljava/lang/Number;

    if-eqz v0, :cond_2

    .line 2325266
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 2325267
    :cond_2
    instance-of v0, p0, Ljava/util/Date;

    if-eqz v0, :cond_3

    .line 2325268
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ssZ"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 2325269
    invoke-virtual {v0, p0}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 2325270
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported parameter type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static f(LX/GAX;)V
    .locals 8

    .prologue
    .line 2325271
    invoke-virtual {p0}, LX/GAX;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GAU;

    .line 2325272
    sget-object v2, LX/GAZ;->GET:LX/GAZ;

    .line 2325273
    iget-object v3, v0, LX/GAU;->e:LX/GAZ;

    move-object v3, v3

    .line 2325274
    invoke-virtual {v2, v3}, LX/GAZ;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v6, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2325275
    iget-object v2, v0, LX/GAU;->o:Ljava/lang/String;

    move-object v2, v2

    .line 2325276
    invoke-static {v2}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_4

    move v2, v3

    .line 2325277
    :goto_1
    move v2, v2

    .line 2325278
    if-eqz v2, :cond_0

    .line 2325279
    iget-object v2, v0, LX/GAU;->k:Landroid/os/Bundle;

    move-object v2, v2

    .line 2325280
    const-string v3, "fields"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v3, "fields"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2325281
    :cond_1
    sget-object v2, LX/GAb;->DEVELOPER_ERRORS:LX/GAb;

    const/4 v3, 0x5

    const-string v4, "Request"

    const-string v5, "starting with Graph API v2.4, GET requests for /%s should contain an explicit \"fields\" parameter."

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    .line 2325282
    iget-object p0, v0, LX/GAU;->f:Ljava/lang/String;

    move-object v0, p0

    .line 2325283
    aput-object v0, v6, v7

    .line 2325284
    invoke-static {v2}, LX/GAK;->a(LX/GAb;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2325285
    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2325286
    invoke-static {v2, v3, v4, v0}, LX/GsN;->a(LX/GAb;ILjava/lang/String;Ljava/lang/String;)V

    .line 2325287
    :cond_2
    goto :goto_0

    .line 2325288
    :cond_3
    return-void

    .line 2325289
    :cond_4
    const-string v5, "v"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2325290
    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 2325291
    :cond_5
    const-string v5, "\\."

    invoke-virtual {v2, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 2325292
    array-length v5, v2

    if-lt v5, v6, :cond_6

    aget-object v5, v2, v4

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    if-gt v5, v6, :cond_7

    :cond_6
    aget-object v5, v2, v4

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    if-lt v5, v6, :cond_8

    aget-object v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const/4 v5, 0x4

    if-lt v2, v5, :cond_8

    :cond_7
    move v2, v3

    goto :goto_1

    :cond_8
    move v2, v4

    goto :goto_1
.end method

.method private static g(LX/GAX;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2325293
    iget-object v0, p0, LX/GAX;->g:Ljava/lang/String;

    move-object v0, v0

    .line 2325294
    invoke-static {v0}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2325295
    iget-object v0, p0, LX/GAX;->g:Ljava/lang/String;

    move-object v0, v0

    .line 2325296
    :goto_0
    return-object v0

    .line 2325297
    :cond_0
    invoke-virtual {p0}, LX/GAX;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GAU;

    .line 2325298
    iget-object v0, v0, LX/GAU;->d:Lcom/facebook/AccessToken;

    .line 2325299
    if-eqz v0, :cond_1

    .line 2325300
    iget-object p0, v0, Lcom/facebook/AccessToken;->k:Ljava/lang/String;

    move-object v0, p0

    .line 2325301
    if-eqz v0, :cond_1

    goto :goto_0

    .line 2325302
    :cond_2
    sget-object v0, LX/GAU;->b:Ljava/lang/String;

    invoke-static {v0}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2325303
    sget-object v0, LX/GAU;->b:Ljava/lang/String;

    goto :goto_0

    .line 2325304
    :cond_3
    invoke-static {}, LX/GAK;->i()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static k(LX/GAU;)V
    .locals 3

    .prologue
    .line 2325305
    iget-object v0, p0, LX/GAU;->d:Lcom/facebook/AccessToken;

    if-eqz v0, :cond_2

    .line 2325306
    iget-object v0, p0, LX/GAU;->k:Landroid/os/Bundle;

    const-string v1, "access_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2325307
    iget-object v0, p0, LX/GAU;->d:Lcom/facebook/AccessToken;

    .line 2325308
    iget-object v1, v0, Lcom/facebook/AccessToken;->h:Ljava/lang/String;

    move-object v0, v1

    .line 2325309
    invoke-static {v0}, LX/GsN;->a(Ljava/lang/String;)V

    .line 2325310
    iget-object v1, p0, LX/GAU;->k:Landroid/os/Bundle;

    const-string v2, "access_token"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2325311
    :cond_0
    :goto_0
    iget-object v0, p0, LX/GAU;->k:Landroid/os/Bundle;

    const-string v1, "sdk"

    const-string v2, "android"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2325312
    iget-object v0, p0, LX/GAU;->k:Landroid/os/Bundle;

    const-string v1, "format"

    const-string v2, "json"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2325313
    iget-object v0, p0, LX/GAU;->k:Landroid/os/Bundle;

    const-string v1, "locale"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2325314
    sget-object v0, LX/GAb;->GRAPH_API_DEBUG_INFO:LX/GAb;

    invoke-static {v0}, LX/GAK;->a(LX/GAb;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2325315
    iget-object v0, p0, LX/GAU;->k:Landroid/os/Bundle;

    const-string v1, "debug"

    const-string v2, "info"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2325316
    :cond_1
    :goto_1
    return-void

    .line 2325317
    :cond_2
    iget-boolean v0, p0, LX/GAU;->p:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/GAU;->k:Landroid/os/Bundle;

    const-string v1, "access_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2325318
    invoke-static {}, LX/GAK;->i()Ljava/lang/String;

    move-result-object v0

    .line 2325319
    invoke-static {}, LX/Gsd;->a()V

    .line 2325320
    sget-object v1, LX/GAK;->f:Ljava/lang/String;

    move-object v1, v1

    .line 2325321
    invoke-static {v0}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, LX/Gsc;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2325322
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "|"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2325323
    iget-object v1, p0, LX/GAU;->k:Landroid/os/Bundle;

    const-string v2, "access_token"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2325324
    :cond_3
    sget-object v0, LX/GAb;->GRAPH_API_DEBUG_WARNING:LX/GAb;

    invoke-static {v0}, LX/GAK;->a(LX/GAb;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2325325
    iget-object v0, p0, LX/GAU;->k:Landroid/os/Bundle;

    const-string v1, "debug"

    const-string v2, "warning"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static l(LX/GAU;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 2325326
    iget-object v0, p0, LX/GAU;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2325327
    new-instance v0, LX/GAA;

    const-string v1, "Can\'t override URL for a batch request"

    invoke-direct {v0, v1}, LX/GAA;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2325328
    :cond_0
    invoke-static {p0}, LX/GAU;->n(LX/GAU;)Ljava/lang/String;

    move-result-object v0

    .line 2325329
    invoke-static {p0}, LX/GAU;->k(LX/GAU;)V

    .line 2325330
    invoke-static {p0, v0}, LX/GAU;->a(LX/GAU;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static n(LX/GAU;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 2325331
    sget-object v0, LX/GAU;->c:Ljava/util/regex/Pattern;

    iget-object v1, p0, LX/GAU;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 2325332
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2325333
    iget-object v0, p0, LX/GAU;->f:Ljava/lang/String;

    .line 2325334
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "%s/%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, LX/GAU;->o:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, LX/GAU;->f:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(LX/GA2;)V
    .locals 1

    .prologue
    .line 2325335
    sget-object v0, LX/GAb;->GRAPH_API_DEBUG_INFO:LX/GAb;

    invoke-static {v0}, LX/GAK;->a(LX/GAb;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, LX/GAb;->GRAPH_API_DEBUG_WARNING:LX/GAb;

    invoke-static {v0}, LX/GAK;->a(LX/GAb;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2325336
    :cond_0
    new-instance v0, LX/GAN;

    invoke-direct {v0, p0, p1}, LX/GAN;-><init>(LX/GAU;LX/GA2;)V

    .line 2325337
    iput-object v0, p0, LX/GAU;->l:LX/GA2;

    .line 2325338
    :goto_0
    return-void

    .line 2325339
    :cond_1
    iput-object p1, p0, LX/GAU;->l:LX/GA2;

    goto :goto_0
.end method

.method public final f()LX/GAY;
    .locals 1

    .prologue
    .line 2325340
    invoke-static {p0}, LX/GAU;->a(LX/GAU;)LX/GAY;

    move-result-object v0

    return-object v0
.end method

.method public final g()LX/GAV;
    .locals 2

    .prologue
    .line 2325341
    const/4 v0, 0x1

    new-array v0, v0, [LX/GAU;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    .line 2325342
    const-string v1, "requests"

    invoke-static {v0, v1}, LX/Gsd;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 2325343
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 2325344
    new-instance v0, LX/GAX;

    invoke-direct {v0, v1}, LX/GAX;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, LX/GAU;->b(LX/GAX;)LX/GAV;

    move-result-object v0

    move-object v1, v0

    .line 2325345
    move-object v0, v1

    .line 2325346
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2325347
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "{Request: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, " accessToken: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, LX/GAU;->d:Lcom/facebook/AccessToken;

    if-nez v0, :cond_0

    const-string v0, "null"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", graphPath: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/GAU;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", graphObject: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/GAU;->g:Lorg/json/JSONObject;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", httpMethod: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/GAU;->e:LX/GAZ;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", parameters: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, LX/GAU;->k:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, LX/GAU;->d:Lcom/facebook/AccessToken;

    goto :goto_0
.end method
