.class public final LX/Fq5;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:LX/Fq7;


# direct methods
.method public constructor <init>(LX/Fq7;Z)V
    .locals 0

    .prologue
    .line 2292342
    iput-object p1, p0, LX/Fq5;->b:LX/Fq7;

    iput-boolean p2, p0, LX/Fq5;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2292343
    iget-object v0, p0, LX/Fq5;->b:LX/Fq7;

    iget-object v0, v0, LX/Fq7;->k:LX/G4x;

    if-eqz v0, :cond_0

    .line 2292344
    iget-object v0, p0, LX/Fq5;->b:LX/Fq7;

    iget-object v0, v0, LX/Fq7;->k:LX/G4x;

    sget-object v1, LX/G5A;->FAILED:LX/G5A;

    invoke-virtual {v0, v1}, LX/G4x;->a(LX/G5A;)V

    .line 2292345
    iget-object v0, p0, LX/Fq5;->b:LX/Fq7;

    iget-object v0, v0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2292346
    :cond_0
    iget-object v0, p0, LX/Fq5;->b:LX/Fq7;

    iget-object v0, v0, LX/Fq7;->o:LX/FrV;

    if-eqz v0, :cond_1

    .line 2292347
    iget-object v0, p0, LX/Fq5;->b:LX/Fq7;

    iget-object v0, v0, LX/Fq7;->o:LX/FrV;

    invoke-virtual {v0}, LX/FrV;->b()LX/Frd;

    move-result-object v0

    invoke-virtual {v0}, LX/Frd;->e()LX/FsJ;

    move-result-object v0

    .line 2292348
    iget-object v1, p0, LX/Fq5;->b:LX/Fq7;

    iget-object v1, v1, LX/Fq7;->o:LX/FrV;

    invoke-virtual {v1}, LX/FrV;->b()LX/Frd;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/Frd;->b(LX/FsJ;)V

    .line 2292349
    :cond_1
    return-void
.end method

.method public final onSuccess(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2292350
    iget-object v0, p0, LX/Fq5;->b:LX/Fq7;

    iget-object v0, v0, LX/Fq7;->o:LX/FrV;

    if-eqz v0, :cond_0

    .line 2292351
    iget-object v0, p0, LX/Fq5;->b:LX/Fq7;

    iget-object v0, v0, LX/Fq7;->o:LX/FrV;

    invoke-virtual {v0}, LX/FrV;->c()V

    .line 2292352
    iget-object v0, p0, LX/Fq5;->b:LX/Fq7;

    iget-object v0, v0, LX/Fq7;->o:LX/FrV;

    invoke-virtual {v0}, LX/FrV;->b()LX/Frd;

    move-result-object v0

    invoke-virtual {v0}, LX/Frd;->e()LX/FsJ;

    move-result-object v0

    .line 2292353
    iget-object v1, p0, LX/Fq5;->b:LX/Fq7;

    iget-object v1, v1, LX/Fq7;->o:LX/FrV;

    invoke-virtual {v1}, LX/FrV;->b()LX/Frd;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/Frd;->b(LX/FsJ;)V

    .line 2292354
    :cond_0
    iget-boolean v0, p0, LX/Fq5;->a:Z

    if-eqz v0, :cond_1

    .line 2292355
    iget-object v0, p0, LX/Fq5;->b:LX/Fq7;

    iget-object v0, v0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    iget-object v1, p0, LX/Fq5;->b:LX/Fq7;

    iget-object v1, v1, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    .line 2292356
    iget-object p1, v1, Lcom/facebook/timeline/TimelineFragment;->ax:LX/G0n;

    move-object v1, p1

    .line 2292357
    invoke-virtual {v1}, LX/G0n;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/facebook/timeline/TimelineFragment;->d(I)V

    .line 2292358
    :cond_1
    iget-object v0, p0, LX/Fq5;->b:LX/Fq7;

    iget-object v0, v0, LX/Fq7;->k:LX/G4x;

    sget-object v1, LX/G5A;->COMPLETED:LX/G5A;

    invoke-virtual {v0, v1}, LX/G4x;->a(LX/G5A;)V

    .line 2292359
    iget-object v0, p0, LX/Fq5;->b:LX/Fq7;

    iget-object v0, v0, LX/Fq7;->l:Lcom/facebook/timeline/TimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2292360
    return-void
.end method
