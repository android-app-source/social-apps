.class public LX/FKy;
.super Ljava/lang/Object;
.source ""


# static fields
.field private static final a:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# instance fields
.field private final b:LX/5zm;

.field private final c:LX/2Mk;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2225743
    const-class v0, LX/FKy;

    sput-object v0, LX/FKy;->a:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(LX/5zm;LX/2Mk;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225744
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2225745
    iput-object p1, p0, LX/FKy;->b:LX/5zm;

    .line 2225746
    iput-object p2, p0, LX/FKy;->c:LX/2Mk;

    .line 2225747
    return-void
.end method

.method public static final a(Ljava/util/List;Lcom/facebook/messaging/model/messages/Message;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lorg/apache/http/NameValuePair;",
            ">;",
            "Lcom/facebook/messaging/model/messages/Message;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 2225748
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->b:Lcom/facebook/messaging/model/threadkey/ThreadKey;

    invoke-static {v0}, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d(Lcom/facebook/messaging/model/threadkey/ThreadKey;)Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2225749
    invoke-static {p1}, LX/2Mk;->n(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2225750
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "message"

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->f:Ljava/lang/String;

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225751
    :cond_0
    invoke-static {p1}, LX/2Mk;->q(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2225752
    new-instance v3, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 2225753
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->v:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2225754
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v1, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_1

    .line 2225755
    :cond_1
    move-object v0, v3

    .line 2225756
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "client_tags"

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225757
    :cond_2
    invoke-static {p1}, LX/2Mk;->r(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2225758
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "offline_threading_id"

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225759
    :cond_3
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->p:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 2225760
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 2225761
    const-string v1, "message_source"

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2225762
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "message_source_data"

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225763
    :cond_4
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    if-eqz v0, :cond_b

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->u:Lcom/facebook/messaging/model/share/SentShareAttachment;

    iget-object v0, v0, Lcom/facebook/messaging/model/share/SentShareAttachment;->b:Lcom/facebook/messaging/model/share/Share;

    .line 2225764
    :goto_2
    if-eqz v0, :cond_c

    iget-object v1, v0, Lcom/facebook/messaging/model/share/Share;->a:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 2225765
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "object_attachment"

    iget-object v0, v0, Lcom/facebook/messaging/model/share/Share;->a:Ljava/lang/String;

    invoke-direct {v1, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225766
    :cond_5
    :goto_3
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 2225767
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    if-eqz v0, :cond_1f

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    if-ne v0, v1, :cond_1f

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v0, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    iget-object v0, v0, Lcom/facebook/ui/media/attachments/MediaResource;->e:LX/5zj;

    invoke-virtual {v0}, LX/5zj;->isQuickCamSource()Z

    move-result v0

    if-eqz v0, :cond_1f

    move v0, v1

    :goto_4
    move v0, v0

    .line 2225768
    if-eqz v0, :cond_6

    .line 2225769
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "image_type"

    sget-object v3, LX/5dT;->QUICKCAM:LX/5dT;

    iget-object v3, v3, LX/5dT;->apiStringValue:Ljava/lang/String;

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225770
    :cond_6
    invoke-static {p1}, LX/FFj;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 2225771
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 2225772
    iget-object v1, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    .line 2225773
    if-eqz v1, :cond_7

    .line 2225774
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->t()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bG()Ljava/lang/String;

    move-result-object v3

    .line 2225775
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->t()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bF()Ljava/lang/String;

    move-result-object v1

    .line 2225776
    const-string v4, "lwa_type"

    invoke-virtual {v0, v4, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2225777
    invoke-static {p1}, LX/FFj;->a(Lcom/facebook/messaging/model/messages/Message;)Z

    move-result v3

    if-nez v3, :cond_20

    .line 2225778
    const/4 v3, 0x0

    .line 2225779
    :goto_5
    move v3, v3

    .line 2225780
    if-eqz v3, :cond_7

    .line 2225781
    const-string v3, "lwa_state"

    invoke-virtual {v0, v3, v1}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2225782
    :cond_7
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "lightweight_action_attachment"

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225783
    :cond_8
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2225784
    iget-object v4, p1, Lcom/facebook/messaging/model/messages/Message;->t:LX/0Px;

    invoke-virtual {v4}, LX/0Px;->size()I

    move-result v5

    move v1, v2

    :goto_6
    if-ge v1, v5, :cond_e

    invoke-virtual {v4, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/ui/media/attachments/MediaResource;

    .line 2225785
    invoke-virtual {v0}, Lcom/facebook/ui/media/attachments/MediaResource;->b()Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_9

    .line 2225786
    invoke-virtual {v0}, Lcom/facebook/ui/media/attachments/MediaResource;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225787
    :cond_9
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    :cond_a
    move v0, v2

    .line 2225788
    goto/16 :goto_0

    .line 2225789
    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 2225790
    :cond_c
    if-eqz v0, :cond_d

    iget-object v1, v0, Lcom/facebook/messaging/model/share/Share;->b:Ljava/lang/String;

    invoke-static {v1}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 2225791
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "shareable_attachment"

    iget-object v0, v0, Lcom/facebook/messaging/model/share/Share;->b:Ljava/lang/String;

    invoke-direct {v1, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 2225792
    :cond_d
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 2225793
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "object_attachment"

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->k:Ljava/lang/String;

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    .line 2225794
    :cond_e
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_f

    .line 2225795
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "media"

    const-string v4, ","

    invoke-static {v4}, LX/0PO;->on(Ljava/lang/String;)LX/0PO;

    move-result-object v4

    invoke-virtual {v4, v3}, LX/0PO;->join(Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225796
    :cond_f
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->x:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 2225797
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "copy_message"

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->x:Ljava/lang/String;

    invoke-static {v3}, LX/DoA;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225798
    :cond_10
    :goto_7
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->z:LX/0P1;

    if-eqz v0, :cond_14

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->z:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->keySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_14

    .line 2225799
    new-instance v4, LX/0m9;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v0}, LX/0m9;-><init>(LX/0mC;)V

    .line 2225800
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->z:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->entrySet()LX/0Rf;

    move-result-object v0

    invoke-virtual {v0}, LX/0Rf;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_8
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 2225801
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;

    .line 2225802
    iget-object v3, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->a:LX/5e9;

    sget-object v6, LX/5e9;->ONE_TO_ONE:LX/5e9;

    if-ne v3, v6, :cond_12

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "ct_"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->d:J

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    .line 2225803
    :goto_9
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_8

    .line 2225804
    :cond_11
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->y:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 2225805
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "copy_attachment"

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->y:Ljava/lang/String;

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 2225806
    :cond_12
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "gt_"

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, v0, Lcom/facebook/messaging/model/threadkey/ThreadKey;->b:J

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    goto :goto_9

    .line 2225807
    :cond_13
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "broadcast_recipients"

    invoke-virtual {v4}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225808
    :cond_14
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->E:Lcom/facebook/share/model/ComposerAppAttribution;

    .line 2225809
    if-eqz v0, :cond_15

    .line 2225810
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "proxied_app_id"

    invoke-virtual {v0}, Lcom/facebook/share/model/ComposerAppAttribution;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225811
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "android_key_hash"

    invoke-virtual {v0}, Lcom/facebook/share/model/ComposerAppAttribution;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225812
    :cond_15
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-static {v0}, LX/DgF;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 2225813
    invoke-static {p1}, LX/FKy;->c(Lcom/facebook/messaging/model/messages/Message;)LX/0m9;

    move-result-object v0

    .line 2225814
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "location_attachment"

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225815
    :cond_16
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-static {v0}, LX/FFt;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 2225816
    invoke-static {p1}, LX/FFs;->a(Lcom/facebook/messaging/model/messages/Message;)LX/0m9;

    move-result-object v0

    .line 2225817
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "live_location_attachment"

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225818
    :cond_17
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-static {v0}, LX/FEC;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 2225819
    invoke-static {p1}, LX/FKy;->d(Lcom/facebook/messaging/model/messages/Message;)LX/0m9;

    move-result-object v0

    .line 2225820
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "event_attachment"

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225821
    :cond_18
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    if-eqz v0, :cond_19

    .line 2225822
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "ttl"

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->J:Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225823
    :cond_19
    iget-boolean v0, p1, Lcom/facebook/messaging/model/messages/Message;->O:Z

    if-eqz v0, :cond_1a

    .line 2225824
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v1, "customizations"

    const-string v3, "{\"border\":\"flowers\"}"

    invoke-direct {v0, v1, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225825
    :cond_1a
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->P:Ljava/lang/String;

    if-eqz v0, :cond_1b

    .line 2225826
    new-instance v0, LX/0m9;

    sget-object v1, LX/0mC;->a:LX/0mC;

    invoke-direct {v0, v1}, LX/0m9;-><init>(LX/0mC;)V

    .line 2225827
    const-string v1, "message_id"

    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->P:Ljava/lang/String;

    invoke-virtual {v0, v1, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2225828
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string v3, "montage_reply_data"

    invoke-virtual {v0}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v3, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225829
    :cond_1b
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->R:LX/0P1;

    if-eqz v0, :cond_1c

    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->R:LX/0P1;

    invoke-virtual {v0}, LX/0P1;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1c

    .line 2225830
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->R:LX/0P1;

    invoke-static {v0}, LX/5dt;->b(LX/0P1;)Landroid/util/Pair;

    move-result-object v1

    .line 2225831
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-direct {v3, v0, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225832
    :cond_1c
    iget-object v0, p1, Lcom/facebook/messaging/model/messages/Message;->U:LX/0Px;

    invoke-static {v0}, LX/18h;->b(Ljava/util/Collection;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 2225833
    new-instance v1, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v0}, LX/162;-><init>(LX/0mC;)V

    .line 2225834
    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->U:LX/0Px;

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    :goto_a
    if-ge v2, v4, :cond_1d

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/messaging/model/messages/ProfileRange;

    .line 2225835
    invoke-virtual {v0}, Lcom/facebook/messaging/model/messages/ProfileRange;->a()LX/0m9;

    move-result-object v0

    invoke-virtual {v1, v0}, LX/162;->a(LX/0lF;)LX/162;

    .line 2225836
    add-int/lit8 v2, v2, 0x1

    goto :goto_a

    .line 2225837
    :cond_1d
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string v2, "prng"

    invoke-virtual {v1}, LX/162;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2225838
    :cond_1e
    return-void

    :cond_1f
    move v0, v3

    goto/16 :goto_4

    .line 2225839
    :cond_20
    iget-object v3, p1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->t()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bF()Ljava/lang/String;

    move-result-object v3

    .line 2225840
    const-string v4, "RECIPROCATED"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto/16 :goto_5
.end method

.method public static b(LX/0QB;)LX/FKy;
    .locals 3

    .prologue
    .line 2225841
    new-instance v2, LX/FKy;

    invoke-static {p0}, LX/5zm;->a(LX/0QB;)LX/5zm;

    move-result-object v0

    check-cast v0, LX/5zm;

    invoke-static {p0}, LX/2Mk;->a(LX/0QB;)LX/2Mk;

    move-result-object v1

    check-cast v1, LX/2Mk;

    invoke-direct {v2, v0, v1}, LX/FKy;-><init>(LX/5zm;LX/2Mk;)V

    .line 2225842
    return-object v2
.end method

.method private static c(Lcom/facebook/messaging/model/messages/Message;)LX/0m9;
    .locals 8

    .prologue
    .line 2225843
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->t()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    .line 2225844
    new-instance v1, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 2225845
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dJ()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLocationFragmentModel$PlaceModel;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dJ()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLocationFragmentModel$PlaceModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLocationFragmentModel$PlaceModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2225846
    const-string v2, "place_id"

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dJ()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLocationFragmentModel$PlaceModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLocationFragmentModel$PlaceModel;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2225847
    :goto_0
    return-object v1

    .line 2225848
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dh()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLocationFragmentModel$CoordinatesModel;

    move-result-object v2

    .line 2225849
    new-instance v4, LX/0m9;

    sget-object v5, LX/0mC;->a:LX/0mC;

    invoke-direct {v4, v5}, LX/0m9;-><init>(LX/0mC;)V

    .line 2225850
    const-string v5, "latitude"

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLocationFragmentModel$CoordinatesModel;->a()D

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2225851
    const-string v5, "longitude"

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLocationFragmentModel$CoordinatesModel;->b()D

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2225852
    move-object v2, v4

    .line 2225853
    const-string v3, "coordinates"

    invoke-virtual {v1, v3, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 2225854
    const-string v2, "is_current_location"

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bs()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    goto :goto_0
.end method

.method private static d(Lcom/facebook/messaging/model/messages/Message;)LX/0m9;
    .locals 10

    .prologue
    .line 2225855
    iget-object v0, p0, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->t()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v0

    .line 2225856
    new-instance v1, LX/0m9;

    sget-object v2, LX/0mC;->a:LX/0mC;

    invoke-direct {v1, v2}, LX/0m9;-><init>(LX/0mC;)V

    .line 2225857
    const-string v2, "title"

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ba()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2225858
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dm()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageEventFragmentModel$EventPlaceModel;

    move-result-object v2

    .line 2225859
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageEventFragmentModel$EventPlaceModel;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2225860
    const-string v3, "place_id"

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageEventFragmentModel$EventPlaceModel;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, LX/0m9;->a(Ljava/lang/String;Ljava/lang/String;)LX/0m9;

    .line 2225861
    :goto_0
    const-string v2, "is_all_day"

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->br()Z

    move-result v3

    invoke-virtual {v1, v2, v3}, LX/0m9;->a(Ljava/lang/String;Z)LX/0m9;

    .line 2225862
    const-string v2, "start_timestamp"

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cy()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 2225863
    const-string v2, "end_timestamp"

    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->aW()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, LX/0m9;->a(Ljava/lang/String;J)LX/0m9;

    .line 2225864
    return-object v1

    .line 2225865
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dl()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageEventFragmentModel$EventCoordinatesModel;

    move-result-object v2

    .line 2225866
    new-instance v6, LX/0m9;

    sget-object v7, LX/0mC;->a:LX/0mC;

    invoke-direct {v6, v7}, LX/0m9;-><init>(LX/0mC;)V

    .line 2225867
    const-string v7, "latitude"

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageEventFragmentModel$EventCoordinatesModel;->a()D

    move-result-wide v8

    invoke-virtual {v6, v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2225868
    const-string v7, "longitude"

    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageEventFragmentModel$EventCoordinatesModel;->b()D

    move-result-wide v8

    invoke-virtual {v6, v7, v8, v9}, LX/0m9;->a(Ljava/lang/String;D)LX/0m9;

    .line 2225869
    move-object v2, v6

    .line 2225870
    const-string v3, "coordinates"

    invoke-virtual {v1, v3, v2}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    goto :goto_0
.end method
