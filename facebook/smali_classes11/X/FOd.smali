.class public LX/FOd;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:LX/30I;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Xl;
    .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/FOa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/0TD;
    .annotation runtime Lcom/facebook/common/executors/ForNonUiThread;
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public f:LX/3MQ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2235433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2235434
    return-void
.end method

.method public static b(LX/0QB;)LX/FOd;
    .locals 7

    .prologue
    .line 2235435
    new-instance v0, LX/FOd;

    invoke-direct {v0}, LX/FOd;-><init>()V

    .line 2235436
    invoke-static {p0}, LX/30I;->b(LX/0QB;)LX/30I;

    move-result-object v1

    check-cast v1, LX/30I;

    invoke-static {p0}, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;->b(LX/0QB;)Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;

    move-result-object v2

    check-cast v2, Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;

    invoke-static {p0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v3

    check-cast v3, LX/0Xl;

    invoke-static {p0}, LX/FOa;->b(LX/0QB;)LX/FOa;

    move-result-object v4

    check-cast v4, LX/FOa;

    invoke-static {p0}, LX/0Zo;->a(LX/0QB;)LX/0Tf;

    move-result-object v5

    check-cast v5, LX/0TD;

    invoke-static {p0}, LX/3MQ;->b(LX/0QB;)LX/3MQ;

    move-result-object v6

    check-cast v6, LX/3MQ;

    .line 2235437
    iput-object v1, v0, LX/FOd;->a:LX/30I;

    iput-object v2, v0, LX/FOd;->b:Lcom/facebook/contactlogs/upload/ContactLogsUploadSettings;

    iput-object v3, v0, LX/FOd;->c:LX/0Xl;

    iput-object v4, v0, LX/FOd;->d:LX/FOa;

    iput-object v5, v0, LX/FOd;->e:LX/0TD;

    iput-object v6, v0, LX/FOd;->f:LX/3MQ;

    .line 2235438
    return-object v0
.end method
