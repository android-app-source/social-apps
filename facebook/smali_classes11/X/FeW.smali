.class public final LX/FeW;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ve;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6Ve",
        "<",
        "LX/EJ2;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FeY;


# direct methods
.method public constructor <init>(LX/FeY;)V
    .locals 0

    .prologue
    .line 2265879
    iput-object p1, p0, LX/FeW;->a:LX/FeY;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/graphql/model/GraphQLStory;IZ)V
    .locals 10

    .prologue
    .line 2265880
    iget-object v0, p0, LX/FeW;->a:LX/FeY;

    iget-object v7, v0, LX/FeY;->f:LX/CvY;

    iget-object v0, p0, LX/FeW;->a:LX/FeY;

    iget-object v8, v0, LX/FeY;->h:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    if-eqz p3, :cond_0

    sget-object v0, LX/8ch;->LIKED:LX/8ch;

    move-object v6, v0

    :goto_0
    iget-object v0, p0, LX/FeW;->a:LX/FeY;

    iget-object v0, v0, LX/FeY;->e:LX/CzE;

    invoke-virtual {v0, p1}, LX/CzE;->c(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/CvY;->a(Ljava/lang/String;)LX/CvV;

    move-result-object v9

    iget-object v0, p0, LX/FeW;->a:LX/FeY;

    iget-object v0, v0, LX/FeY;->h:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->g()Ljava/lang/String;

    move-result-object v2

    if-eqz p3, :cond_1

    sget-object v3, LX/8ch;->LIKED:LX/8ch;

    :goto_1
    iget-object v1, p0, LX/FeW;->a:LX/FeY;

    iget-object v1, v1, LX/FeY;->e:LX/CzE;

    invoke-virtual {v1, p1}, LX/CzE;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v4

    iget-object v1, p0, LX/FeW;->a:LX/FeY;

    iget-object v1, v1, LX/FeY;->e:LX/CzE;

    invoke-virtual {v1, p1}, LX/CzE;->b(Lcom/facebook/graphql/model/GraphQLStory;)Ljava/lang/String;

    move-result-object v5

    move v1, p2

    invoke-static/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;ILjava/lang/String;LX/8ch;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v5

    move-object v0, v7

    move-object v1, v8

    move-object v2, v6

    move v3, p2

    move-object v4, v9

    invoke-virtual/range {v0 .. v5}, LX/CvY;->a(Lcom/facebook/search/results/model/SearchResultsMutableContext;LX/8ch;ILX/CvV;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2265881
    return-void

    .line 2265882
    :cond_0
    sget-object v0, LX/8ch;->UNLIKED:LX/8ch;

    move-object v6, v0

    goto :goto_0

    :cond_1
    sget-object v3, LX/8ch;->UNLIKED:LX/8ch;

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 2265883
    check-cast p1, LX/EJ2;

    .line 2265884
    iget-object v0, p1, LX/EJ2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v0, v0

    .line 2265885
    iget-object v1, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v1

    .line 2265886
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2265887
    iget-object v1, p0, LX/FeW;->a:LX/FeY;

    iget-object v1, v1, LX/FeY;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/FgJ;

    .line 2265888
    iget-object v2, v1, LX/FgJ;->a:LX/189;

    .line 2265889
    iget-object v3, p1, LX/EJ2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v3, v3

    .line 2265890
    iget-object v4, v1, LX/FgJ;->b:Lcom/facebook/graphql/model/GraphQLActor;

    .line 2265891
    iget-boolean v5, p1, LX/EJ2;->b:Z

    move v5, v5

    .line 2265892
    invoke-virtual {v2, v3, v4, v5}, LX/189;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLActor;Z)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v2

    .line 2265893
    invoke-static {v2}, LX/182;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v2

    move-object v3, v2

    .line 2265894
    iget-object v1, p0, LX/FeW;->a:LX/FeY;

    iget-object v1, v1, LX/FeY;->e:LX/CzE;

    invoke-virtual {v1, v0}, LX/CzE;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/0am;

    move-result-object v2

    .line 2265895
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v1

    if-nez v1, :cond_3

    .line 2265896
    iget-object v1, p1, LX/EJ2;->a:Lcom/facebook/feed/rows/core/props/FeedProps;

    move-object v1, v1

    .line 2265897
    invoke-static {v1}, LX/182;->c(Lcom/facebook/feed/rows/core/props/FeedProps;)Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v1

    .line 2265898
    if-eqz v1, :cond_1

    iget-object v2, p0, LX/FeW;->a:LX/FeY;

    iget-object v2, v2, LX/FeY;->e:LX/CzE;

    invoke-virtual {v2, v1}, LX/CzE;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/0am;

    move-result-object v1

    move-object v2, v1

    .line 2265899
    :goto_0
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;

    invoke-interface {v1, v3}, Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;

    move-result-object v1

    move-object v3, v1

    .line 2265900
    :cond_0
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    .line 2265901
    :goto_1
    iget-object v2, p0, LX/FeW;->a:LX/FeY;

    iget-object v2, v2, LX/FeY;->e:LX/CzE;

    invoke-virtual {v2, v1}, LX/CzE;->b(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v2

    .line 2265902
    iget-boolean v4, p1, LX/EJ2;->b:Z

    move v4, v4

    .line 2265903
    invoke-direct {p0, v0, v2, v4}, LX/FeW;->a(Lcom/facebook/graphql/model/GraphQLStory;IZ)V

    .line 2265904
    iget-object v0, p0, LX/FeW;->a:LX/FeY;

    invoke-static {v0, p1, v1, v3}, LX/FeY;->a$redex0(LX/FeY;LX/EJ2;Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)V

    .line 2265905
    :goto_2
    return-void

    .line 2265906
    :cond_1
    invoke-static {}, LX/0am;->absent()LX/0am;

    move-result-object v1

    move-object v2, v1

    goto :goto_0

    :cond_2
    move-object v1, v0

    .line 2265907
    goto :goto_1

    .line 2265908
    :cond_3
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;

    invoke-interface {v1, v3}, Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/search/results/model/contract/SearchResultsGraphQLStoryFeedUnit;

    move-result-object v3

    .line 2265909
    iget-object v1, p0, LX/FeW;->a:LX/FeY;

    iget-object v4, v1, LX/FeY;->e:LX/CzE;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/graphql/model/FeedUnit;

    invoke-virtual {v4, v1}, LX/CzE;->b(Lcom/facebook/graphql/model/FeedUnit;)I

    move-result v1

    .line 2265910
    iget-boolean v4, p1, LX/EJ2;->b:Z

    move v4, v4

    .line 2265911
    invoke-direct {p0, v0, v1, v4}, LX/FeW;->a(Lcom/facebook/graphql/model/GraphQLStory;IZ)V

    .line 2265912
    iget-object v1, p0, LX/FeW;->a:LX/FeY;

    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    invoke-static {v1, p1, v0, v3}, LX/FeY;->a$redex0(LX/FeY;LX/EJ2;Lcom/facebook/graphql/model/FeedUnit;Lcom/facebook/graphql/model/FeedUnit;)V

    goto :goto_2
.end method
