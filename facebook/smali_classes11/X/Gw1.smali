.class public final LX/Gw1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0Vj;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0Vj",
        "<",
        "Ljava/lang/Void;",
        "Lcom/facebook/ipc/composer/intent/ComposerConfiguration$Builder;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/Gw8;


# direct methods
.method public constructor <init>(LX/Gw8;)V
    .locals 0

    .prologue
    .line 2406471
    iput-object p1, p0, LX/Gw1;->a:LX/Gw8;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 7

    .prologue
    .line 2406472
    iget-object v0, p0, LX/Gw1;->a:LX/Gw8;

    .line 2406473
    iget-object v1, v0, LX/Gw8;->n:LX/8PB;

    .line 2406474
    iget-object v2, v1, LX/8PB;->i:Ljava/util/ArrayList;

    move-object v3, v2

    .line 2406475
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 2406476
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2406477
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2406478
    const-string p0, "type"

    sget-object p1, LX/4hX;->PHOTO:LX/4hX;

    invoke-virtual {p1}, LX/4hX;->name()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v6, p0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2406479
    const-string p0, "uri"

    invoke-virtual {v6, p0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2406480
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2406481
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 2406482
    :cond_0
    invoke-static {v0, v4}, LX/Gw8;->a(LX/Gw8;Ljava/util/List;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    move-object v0, v1

    .line 2406483
    return-object v0
.end method
