.class public LX/GfQ;
.super LX/3mX;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pn;",
        ":",
        "LX/1Po;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Ps;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ">",
        "LX/3mX",
        "<",
        "Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/GfL;

.field private final d:I

.field private final e:Z

.field private f:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field private g:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pn;LX/25M;IZLX/GfL;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1Pn;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnit;",
            ">;TE;",
            "LX/25M;",
            "IZ",
            "LX/GfL;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2377322
    move-object v0, p4

    check-cast v0, LX/1Pq;

    invoke-direct {p0, p1, p2, v0, p5}, LX/3mX;-><init>(Landroid/content/Context;LX/0Px;LX/1Pq;LX/25M;)V

    .line 2377323
    iput-object p3, p0, LX/GfQ;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2377324
    iput-object p4, p0, LX/GfQ;->g:LX/1Pn;

    .line 2377325
    iput-object p8, p0, LX/GfQ;->c:LX/GfL;

    .line 2377326
    iput p6, p0, LX/GfQ;->d:I

    .line 2377327
    iput-boolean p7, p0, LX/GfQ;->e:Z

    .line 2377328
    return-void
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2377367
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 5

    .prologue
    .line 2377330
    check-cast p2, Lcom/facebook/graphql/model/GraphQLCreativePagesYouMayLikeFeedUnitItem;

    .line 2377331
    iget-object v0, p0, LX/GfQ;->g:LX/1Pn;

    check-cast v0, LX/1Pt;

    invoke-interface {v0}, LX/1Pt;->m()LX/1f9;

    move-result-object v0

    .line 2377332
    iget-object v1, p0, LX/GfQ;->c:LX/GfL;

    const/4 v2, 0x0

    .line 2377333
    new-instance v3, LX/GfK;

    invoke-direct {v3, v1}, LX/GfK;-><init>(LX/GfL;)V

    .line 2377334
    iget-object v4, v1, LX/GfL;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/GfJ;

    .line 2377335
    if-nez v4, :cond_0

    .line 2377336
    new-instance v4, LX/GfJ;

    invoke-direct {v4, v1}, LX/GfJ;-><init>(LX/GfL;)V

    .line 2377337
    :cond_0
    invoke-static {v4, p1, v2, v2, v3}, LX/GfJ;->a$redex0(LX/GfJ;LX/1De;IILX/GfK;)V

    .line 2377338
    move-object v3, v4

    .line 2377339
    move-object v2, v3

    .line 2377340
    move-object v1, v2

    .line 2377341
    iget-object v2, v1, LX/GfJ;->a:LX/GfK;

    iput-object p2, v2, LX/GfK;->c:LX/25E;

    .line 2377342
    iget-object v2, v1, LX/GfJ;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2377343
    move-object v1, v1

    .line 2377344
    iget-object v2, p0, LX/GfQ;->f:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2377345
    iget-object v3, v1, LX/GfJ;->a:LX/GfK;

    iput-object v2, v3, LX/GfK;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2377346
    iget-object v3, v1, LX/GfJ;->e:Ljava/util/BitSet;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Ljava/util/BitSet;->set(I)V

    .line 2377347
    move-object v1, v1

    .line 2377348
    iget-object v2, v1, LX/GfJ;->a:LX/GfK;

    iput-object v0, v2, LX/GfK;->a:LX/1f9;

    .line 2377349
    iget-object v2, v1, LX/GfJ;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2377350
    move-object v0, v1

    .line 2377351
    iget v1, p0, LX/GfQ;->d:I

    .line 2377352
    iget-object v2, v0, LX/GfJ;->a:LX/GfK;

    iput v1, v2, LX/GfK;->e:I

    .line 2377353
    iget-object v2, v0, LX/GfJ;->e:Ljava/util/BitSet;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2377354
    move-object v0, v0

    .line 2377355
    const/4 v1, 0x0

    .line 2377356
    iget-object v2, v0, LX/GfJ;->a:LX/GfK;

    iput-object v1, v2, LX/GfK;->f:LX/2dx;

    .line 2377357
    iget-object v2, v0, LX/GfJ;->e:Ljava/util/BitSet;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2377358
    move-object v0, v0

    .line 2377359
    iget-boolean v1, p0, LX/GfQ;->e:Z

    .line 2377360
    iget-object v2, v0, LX/GfJ;->a:LX/GfK;

    iput-boolean v1, v2, LX/GfK;->g:Z

    .line 2377361
    move-object v0, v0

    .line 2377362
    iget-object v1, p0, LX/GfQ;->g:LX/1Pn;

    .line 2377363
    iget-object v2, v0, LX/GfJ;->a:LX/GfK;

    iput-object v1, v2, LX/GfK;->b:LX/1Pn;

    .line 2377364
    iget-object v2, v0, LX/GfJ;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2377365
    move-object v0, v0

    .line 2377366
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 2377329
    const/4 v0, 0x0

    return v0
.end method
