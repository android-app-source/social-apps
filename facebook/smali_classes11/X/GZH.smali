.class public LX/GZH;
.super LX/1OM;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:LX/7iP;

.field public c:LX/GZ5;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:LX/GaE;

.field public e:Landroid/view/View;

.field public f:Landroid/view/View;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Landroid/view/View$OnClickListener;

.field public h:Landroid/view/View$OnClickListener;

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/GaE;LX/GZ5;LX/7iP;)V
    .locals 2
    .param p3    # LX/GZ5;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2366125
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2366126
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GZH;->i:Z

    .line 2366127
    iput-object p1, p0, LX/GZH;->a:Landroid/content/Context;

    .line 2366128
    iput-object p2, p0, LX/GZH;->d:LX/GaE;

    .line 2366129
    iput-object p3, p0, LX/GZH;->c:LX/GZ5;

    .line 2366130
    iput-object p4, p0, LX/GZH;->b:LX/7iP;

    .line 2366131
    iget-object v0, p0, LX/GZH;->a:Landroid/content/Context;

    .line 2366132
    new-instance v1, Landroid/view/View;

    invoke-direct {v1, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2366133
    new-instance p1, LX/1a3;

    const/4 p3, -0x1

    const/4 p4, 0x0

    invoke-direct {p1, p3, p4}, LX/1a3;-><init>(II)V

    invoke-virtual {v1, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2366134
    move-object v0, v1

    .line 2366135
    iput-object v0, p0, LX/GZH;->e:Landroid/view/View;

    .line 2366136
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2366137
    return-void
.end method

.method public static f(LX/GZH;I)I
    .locals 1

    .prologue
    .line 2366124
    invoke-virtual {p0}, LX/GZH;->d()I

    move-result v0

    sub-int v0, p1, v0

    return v0
.end method

.method public static f(LX/GZH;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "LX/GZG;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2366248
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2366249
    sget-object v1, LX/GZG;->ADDITIONAL_HEADER:LX/GZG;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2366250
    iget-boolean v1, p0, LX/GZH;->i:Z

    if-eqz v1, :cond_0

    .line 2366251
    sget-object v1, LX/GZG;->LOADING_VIEW:LX/GZG;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2366252
    :cond_0
    iget-object v1, p0, LX/GZH;->d:LX/GaE;

    .line 2366253
    iget-boolean v2, v1, LX/GaE;->a:Z

    move v1, v2

    .line 2366254
    if-eqz v1, :cond_1

    .line 2366255
    sget-object v1, LX/GZG;->CREATE_STORE_VIEW:LX/GZG;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2366256
    :goto_0
    return-object v0

    .line 2366257
    :cond_1
    iget-object v1, p0, LX/GZH;->d:LX/GaE;

    .line 2366258
    iget-boolean v2, v1, LX/GaE;->b:Z

    move v1, v2

    .line 2366259
    if-eqz v1, :cond_2

    invoke-direct {p0}, LX/GZH;->i()I

    move-result v1

    if-nez v1, :cond_2

    .line 2366260
    sget-object v1, LX/GZG;->EMPTY_STORE_VIEW:LX/GZG;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2366261
    :cond_2
    sget-object v1, LX/GZG;->STOREFRONT_MAIN_HEADER:LX/GZG;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private g()I
    .locals 1

    .prologue
    .line 2366247
    iget-object v0, p0, LX/GZH;->f:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(LX/GZH;I)Z
    .locals 1

    .prologue
    .line 2366246
    invoke-virtual {p0}, LX/GZH;->d()I

    move-result v0

    if-lt p1, v0, :cond_0

    invoke-direct {p0}, LX/GZH;->h()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()I
    .locals 2

    .prologue
    .line 2366245
    invoke-virtual {p0}, LX/1OM;->ij_()I

    move-result v0

    invoke-direct {p0}, LX/GZH;->g()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private i()I
    .locals 1

    .prologue
    .line 2366244
    iget-object v0, p0, LX/GZH;->c:LX/GZ5;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GZH;->c:LX/GZ5;

    invoke-virtual {v0}, LX/1OM;->ij_()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final C_(I)J
    .locals 2

    .prologue
    .line 2366262
    invoke-static {p0, p1}, LX/GZH;->g(LX/GZH;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2366263
    iget-object v0, p0, LX/GZH;->c:LX/GZ5;

    invoke-static {p0, p1}, LX/GZH;->f(LX/GZH;I)I

    move-result v1

    invoke-virtual {v0, v1}, LX/1OM;->C_(I)J

    move-result-wide v0

    .line 2366264
    :goto_0
    return-wide v0

    :cond_0
    int-to-long v0, p1

    goto :goto_0
.end method

.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 4

    .prologue
    .line 2366216
    sget-object v0, LX/GZG;->PRODUCT_GROUPING:LX/GZG;

    invoke-virtual {v0}, LX/GZG;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 2366217
    iget-object v0, p0, LX/GZH;->c:LX/GZ5;

    invoke-virtual {v0, p1, p2}, LX/1OM;->a(Landroid/view/ViewGroup;I)LX/1a1;

    move-result-object v0

    .line 2366218
    :goto_0
    return-object v0

    .line 2366219
    :cond_0
    sget-object v0, LX/GZG;->LOADING_VIEW:LX/GZG;

    invoke-virtual {v0}, LX/GZG;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_1

    .line 2366220
    new-instance v0, LX/GZC;

    iget-object v1, p0, LX/GZH;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0313d1

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/GZC;-><init>(Landroid/view/View;)V

    goto :goto_0

    .line 2366221
    :cond_1
    sget-object v0, LX/GZG;->CREATE_STORE_VIEW:LX/GZG;

    invoke-virtual {v0}, LX/GZG;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_2

    .line 2366222
    new-instance v0, LX/GZD;

    iget-object v1, p0, LX/GZH;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0313cb

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/GZD;-><init>(Landroid/view/View;)V

    move-object v0, v0

    .line 2366223
    goto :goto_0

    .line 2366224
    :cond_2
    sget-object v0, LX/GZG;->EMPTY_STORE_VIEW:LX/GZG;

    invoke-virtual {v0}, LX/GZG;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_3

    .line 2366225
    new-instance v0, LX/GZE;

    iget-object v1, p0, LX/GZH;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0313cc

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, LX/GZE;-><init>(Landroid/view/View;)V

    move-object v0, v0

    .line 2366226
    goto :goto_0

    .line 2366227
    :cond_3
    sget-object v0, LX/GZG;->STOREFRONT_MAIN_HEADER:LX/GZG;

    invoke-virtual {v0}, LX/GZG;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_5

    .line 2366228
    new-instance v0, LX/GaC;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/GaC;-><init>(Landroid/content/Context;)V

    .line 2366229
    iget-object v1, p0, LX/GZH;->b:LX/7iP;

    .line 2366230
    iget-object v2, v0, LX/GaC;->e:Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;

    invoke-virtual {v2, v1}, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->setRefType(LX/7iP;)V

    .line 2366231
    iget-object v2, v0, LX/GaC;->b:LX/7iw;

    .line 2366232
    iput-object v1, v2, LX/7iw;->d:LX/7iP;

    .line 2366233
    iget-object v1, p0, LX/GZH;->g:Landroid/view/View$OnClickListener;

    .line 2366234
    iget-object v2, v0, LX/GaC;->c:Lcom/facebook/resources/ui/FbTextView;

    if-eqz v2, :cond_4

    .line 2366235
    iget-object v2, v0, LX/GaC;->c:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v1}, Lcom/facebook/resources/ui/FbTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2366236
    :cond_4
    new-instance v1, LX/1a3;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, LX/1a3;-><init>(II)V

    invoke-virtual {v0, v1}, LX/GaC;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2366237
    new-instance v1, LX/GZF;

    invoke-direct {v1, v0}, LX/GZF;-><init>(LX/GaC;)V

    move-object v0, v1

    .line 2366238
    goto/16 :goto_0

    .line 2366239
    :cond_5
    sget-object v0, LX/GZG;->ADDITIONAL_HEADER:LX/GZG;

    invoke-virtual {v0}, LX/GZG;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_6

    .line 2366240
    new-instance v1, LX/GZC;

    iget-object v0, p0, LX/GZH;->e:Landroid/view/View;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-direct {v1, v0}, LX/GZC;-><init>(Landroid/view/View;)V

    move-object v0, v1

    goto/16 :goto_0

    .line 2366241
    :cond_6
    sget-object v0, LX/GZG;->FOOTER_VIEW:LX/GZG;

    invoke-virtual {v0}, LX/GZG;->ordinal()I

    move-result v0

    if-ne p2, v0, :cond_7

    .line 2366242
    new-instance v1, LX/GZC;

    iget-object v0, p0, LX/GZH;->f:Landroid/view/View;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-direct {v1, v0}, LX/GZC;-><init>(Landroid/view/View;)V

    move-object v0, v1

    goto/16 :goto_0

    .line 2366243
    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot create ViewHolder itemViewType: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(LX/1a1;I)V
    .locals 6

    .prologue
    .line 2366150
    invoke-static {p0, p2}, LX/GZH;->g(LX/GZH;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2366151
    iget-object v0, p0, LX/GZH;->c:LX/GZ5;

    invoke-static {p0, p2}, LX/GZH;->f(LX/GZH;I)I

    move-result v1

    invoke-virtual {v0, p1, v1}, LX/1OM;->a(LX/1a1;I)V

    .line 2366152
    :cond_0
    :goto_0
    return-void

    .line 2366153
    :cond_1
    invoke-virtual {p0, p2}, LX/1OM;->getItemViewType(I)I

    move-result v0

    .line 2366154
    sget-object v1, LX/GZG;->CREATE_STORE_VIEW:LX/GZG;

    invoke-virtual {v1}, LX/GZG;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 2366155
    check-cast p1, LX/GZD;

    iget-object v0, p0, LX/GZH;->h:Landroid/view/View$OnClickListener;

    .line 2366156
    iget-object v1, p1, LX/GZD;->m:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2366157
    goto :goto_0

    .line 2366158
    :cond_2
    sget-object v1, LX/GZG;->EMPTY_STORE_VIEW:LX/GZG;

    invoke-virtual {v1}, LX/GZG;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 2366159
    check-cast p1, LX/GZE;

    iget-object v0, p0, LX/GZH;->g:Landroid/view/View$OnClickListener;

    .line 2366160
    iget-object v1, p1, LX/GZE;->m:Lcom/facebook/widget/text/BetterButton;

    invoke-virtual {v1, v0}, Lcom/facebook/widget/text/BetterButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2366161
    goto :goto_0

    .line 2366162
    :cond_3
    sget-object v1, LX/GZG;->STOREFRONT_MAIN_HEADER:LX/GZG;

    invoke-virtual {v1}, LX/GZG;->ordinal()I

    move-result v1

    if-ne v0, v1, :cond_4

    .line 2366163
    check-cast p1, LX/GZF;

    iget-object v0, p0, LX/GZH;->d:LX/GaE;

    .line 2366164
    iget-object v1, p1, LX/GZF;->l:LX/GaC;

    const/4 v3, 0x1

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 2366165
    iget-object v2, v0, LX/GaE;->f:LX/0am;

    move-object v2, v2

    .line 2366166
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2366167
    iget-object v2, v0, LX/GaE;->f:LX/0am;

    move-object v2, v2

    .line 2366168
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 2366169
    :goto_1
    invoke-virtual {v1}, LX/GaC;->getPaddingLeft()I

    move-result p0

    invoke-virtual {v1}, LX/GaC;->getPaddingRight()I

    move-result p2

    invoke-virtual {v1}, LX/GaC;->getPaddingBottom()I

    move-result p1

    invoke-virtual {v1, p0, v2, p2, p1}, LX/GaC;->setPadding(IIII)V

    .line 2366170
    iget-object v2, v0, LX/GaE;->d:LX/0am;

    move-object v2, v2

    .line 2366171
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 2366172
    iget-object v2, v1, LX/GaC;->b:LX/7iw;

    invoke-virtual {v2, v3}, LX/7iw;->a(Z)V

    .line 2366173
    iget-object v2, v0, LX/GaE;->d:LX/0am;

    move-object v2, v2

    .line 2366174
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/commerce/core/intent/MerchantInfoViewData;

    iget-object p0, v1, LX/GaC;->b:LX/7iw;

    invoke-static {v2, p0}, LX/GaF;->a(Lcom/facebook/commerce/core/intent/MerchantInfoViewData;LX/7iw;)V

    .line 2366175
    :goto_2
    iget-object p0, v1, LX/GaC;->c:Lcom/facebook/resources/ui/FbTextView;

    .line 2366176
    iget-boolean v2, v0, LX/GaE;->e:Z

    move v2, v2

    .line 2366177
    if-eqz v2, :cond_7

    move v2, v4

    :goto_3
    invoke-virtual {p0, v2}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2366178
    iget-object v2, v0, LX/GaE;->g:LX/0am;

    move-object v2, v2

    .line 2366179
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2366180
    iget-object v2, v0, LX/GaE;->g:LX/0am;

    move-object v2, v2

    .line 2366181
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Ga8;

    invoke-virtual {v2}, LX/Ga8;->a()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 2366182
    iget-object v2, v1, LX/GaC;->d:Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;

    invoke-virtual {v2, v4}, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->setVisibility(I)V

    .line 2366183
    iget-object p0, v1, LX/GaC;->d:Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;

    .line 2366184
    iget-object v2, v0, LX/GaE;->g:LX/0am;

    move-object v2, v2

    .line 2366185
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/Ga8;

    invoke-virtual {p0, v2}, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->setViewController(LX/Ga8;)V

    .line 2366186
    iget-object v2, v1, LX/GaC;->d:Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;

    invoke-virtual {v2}, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->a()V

    .line 2366187
    :goto_4
    iget-object v2, v0, LX/GaE;->h:LX/0am;

    move-object v2, v2

    .line 2366188
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2366189
    iget-object v2, v0, LX/GaE;->h:LX/0am;

    move-object v2, v2

    .line 2366190
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    invoke-virtual {v2}, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;->j()LX/1vs;

    move-result-object v2

    iget-object p0, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2366191
    invoke-virtual {p0, v2, v4}, LX/15i;->j(II)I

    move-result v2

    if-lez v2, :cond_9

    move v2, v3

    :goto_5
    if-eqz v2, :cond_b

    .line 2366192
    iget-object v2, v1, LX/GaC;->e:Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;

    invoke-virtual {v2, v4}, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->setVisibility(I)V

    .line 2366193
    iget-object p0, v1, LX/GaC;->e:Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;

    .line 2366194
    iget-object v2, v0, LX/GaE;->h:LX/0am;

    move-object v2, v2

    .line 2366195
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;

    .line 2366196
    iget-object v3, v0, LX/GaE;->j:LX/0am;

    move-object v3, v3

    .line 2366197
    invoke-virtual {v3}, LX/0am;->orNull()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p0, v2, v3}, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->a(Lcom/facebook/commerce/core/graphql/CoreCommerceQueryFragmentsModels$CommerceStoreFragmentModel$CommerceCollectionsModel;Ljava/lang/String;)V

    .line 2366198
    :goto_6
    iget-object v2, v0, LX/GaE;->i:LX/0am;

    move-object v2, v2

    .line 2366199
    invoke-virtual {v2}, LX/0am;->isPresent()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 2366200
    iget-object v2, v0, LX/GaE;->i:LX/0am;

    move-object v2, v2

    .line 2366201
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-static {v2}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_c

    .line 2366202
    iget-object v2, v1, LX/GaC;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v4}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    .line 2366203
    iget-object v3, v1, LX/GaC;->f:Lcom/facebook/resources/ui/FbTextView;

    .line 2366204
    iget-object v2, v0, LX/GaE;->i:LX/0am;

    move-object v2, v2

    .line 2366205
    invoke-virtual {v2}, LX/0am;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v3, v2}, Lcom/facebook/resources/ui/FbTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2366206
    :goto_7
    goto/16 :goto_0

    .line 2366207
    :cond_4
    sget-object v1, LX/GZG;->LOADING_VIEW:LX/GZG;

    invoke-virtual {v1}, LX/GZG;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_0

    sget-object v1, LX/GZG;->ADDITIONAL_HEADER:LX/GZG;

    invoke-virtual {v1}, LX/GZG;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_0

    sget-object v1, LX/GZG;->FOOTER_VIEW:LX/GZG;

    invoke-virtual {v1}, LX/GZG;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 2366208
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot bind ViewHolder for position: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2366209
    :cond_5
    invoke-virtual {v1}, LX/GaC;->getPaddingTop()I

    move-result v2

    goto/16 :goto_1

    .line 2366210
    :cond_6
    iget-object v2, v1, LX/GaC;->b:LX/7iw;

    invoke-virtual {v2, v4}, LX/7iw;->a(Z)V

    goto/16 :goto_2

    :cond_7
    move v2, v5

    .line 2366211
    goto/16 :goto_3

    .line 2366212
    :cond_8
    iget-object v2, v1, LX/GaC;->d:Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;

    invoke-virtual {v2, v5}, Lcom/facebook/commerce/storefront/ui/MerchantSubscriptionView;->setVisibility(I)V

    goto/16 :goto_4

    :cond_9
    move v2, v4

    .line 2366213
    goto/16 :goto_5

    :cond_a
    move v2, v4

    goto/16 :goto_5

    .line 2366214
    :cond_b
    iget-object v2, v1, LX/GaC;->e:Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;

    invoke-virtual {v2, v5}, Lcom/facebook/commerce/storefront/ui/FeaturedProductCollectionView;->setVisibility(I)V

    goto :goto_6

    .line 2366215
    :cond_c
    iget-object v2, v1, LX/GaC;->f:Lcom/facebook/resources/ui/FbTextView;

    invoke-virtual {v2, v5}, Lcom/facebook/resources/ui/FbTextView;->setVisibility(I)V

    goto :goto_7
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 2366147
    iput-boolean p1, p0, LX/GZH;->i:Z

    .line 2366148
    invoke-virtual {p0}, LX/1OM;->notifyDataSetChanged()V

    .line 2366149
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 2366146
    invoke-static {p0}, LX/GZH;->f(LX/GZH;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2366139
    invoke-direct {p0}, LX/GZH;->h()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 2366140
    sget-object v0, LX/GZG;->FOOTER_VIEW:LX/GZG;

    invoke-virtual {v0}, LX/GZG;->ordinal()I

    move-result v0

    .line 2366141
    :goto_0
    return v0

    .line 2366142
    :cond_0
    invoke-static {p0, p1}, LX/GZH;->g(LX/GZH;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2366143
    sget-object v0, LX/GZG;->PRODUCT_GROUPING:LX/GZG;

    invoke-virtual {v0}, LX/GZG;->ordinal()I

    move-result v0

    goto :goto_0

    .line 2366144
    :cond_1
    invoke-static {p0}, LX/GZH;->f(LX/GZH;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GZG;

    move-object v0, v0

    .line 2366145
    invoke-virtual {v0}, LX/GZG;->ordinal()I

    move-result v0

    goto :goto_0
.end method

.method public final ij_()I
    .locals 2

    .prologue
    .line 2366138
    invoke-virtual {p0}, LX/GZH;->d()I

    move-result v0

    invoke-direct {p0}, LX/GZH;->g()I

    move-result v1

    add-int/2addr v0, v1

    invoke-direct {p0}, LX/GZH;->i()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
