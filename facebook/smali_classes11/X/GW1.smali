.class public final LX/GW1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GVw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GVw",
        "<",
        "LX/GWz;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2360304
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static c(Landroid/view/ViewGroup;)LX/GWz;
    .locals 2

    .prologue
    .line 2360305
    new-instance v0, LX/GWz;

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, LX/GWz;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2360306
    invoke-static {p1}, LX/GW1;->c(Landroid/view/ViewGroup;)LX/GWz;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Landroid/view/ViewGroup;LX/7iP;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2360307
    invoke-static {p1}, LX/GW1;->c(Landroid/view/ViewGroup;)LX/GWz;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;
    .locals 1

    .prologue
    .line 2360308
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;->POLICIES:Lcom/facebook/graphql/enums/GraphQLCommerceUIProductDetailSectionType;

    return-object v0
.end method

.method public final a(Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 2360309
    if-nez p1, :cond_1

    move v2, v1

    :goto_0
    if-eqz v2, :cond_3

    move v2, v1

    :goto_1
    if-eqz v2, :cond_4

    .line 2360310
    :cond_0
    :goto_2
    return v0

    .line 2360311
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->l()LX/1vs;

    move-result-object v2

    iget v2, v2, LX/1vs;->b:I

    .line 2360312
    if-nez v2, :cond_2

    move v2, v1

    goto :goto_0

    :cond_2
    move v2, v0

    goto :goto_0

    .line 2360313
    :cond_3
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2360314
    invoke-virtual {v3, v2, v0}, LX/15i;->j(II)I

    move-result v2

    invoke-static {v2}, LX/7j5;->a(I)Z

    move-result v2

    goto :goto_1

    .line 2360315
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;->l()LX/1vs;

    move-result-object v2

    iget-object v3, v2, LX/1vs;->a:LX/15i;

    iget v2, v2, LX/1vs;->b:I

    .line 2360316
    const/4 v4, 0x3

    invoke-virtual {v3, v2, v4}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_2
.end method
