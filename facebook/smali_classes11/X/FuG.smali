.class public final LX/FuG;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/6Ve;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/6Ve",
        "<",
        "LX/G1D;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FuI;


# direct methods
.method public constructor <init>(LX/FuI;)V
    .locals 0

    .prologue
    .line 2299855
    iput-object p1, p0, LX/FuG;->a:LX/FuI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/timeline/protiles/model/ProtileModel;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 2299856
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v1

    .line 2299857
    iget-object v2, p0, LX/FuG;->a:LX/FuI;

    iget-object v2, v2, LX/FuI;->d:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->G()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    .line 2299858
    :goto_0
    return-object v0

    .line 2299859
    :cond_0
    sget-object v2, LX/FuH;->b:[I

    invoke-virtual {v1}, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->ordinal()I

    move-result v1

    aget v1, v2, v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2299860
    :pswitch_0
    sget-object v0, LX/0ax;->bZ:Ljava/lang/String;

    iget-object v1, p0, LX/FuG;->a:LX/FuI;

    iget-object v1, v1, LX/FuI;->d:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->G()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2299861
    :pswitch_1
    sget-object v0, LX/0ax;->cb:Ljava/lang/String;

    iget-object v1, p0, LX/FuG;->a:LX/FuI;

    iget-object v1, v1, LX/FuI;->d:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->G()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2299862
    :pswitch_2
    sget-object v0, LX/0ax;->bO:Ljava/lang/String;

    iget-object v1, p0, LX/FuG;->a:LX/FuI;

    iget-object v1, v1, LX/FuI;->d:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->G()Ljava/lang/String;

    move-result-object v1

    .line 2299863
    iget-object v2, p0, LX/FuG;->a:LX/FuI;

    iget-object v2, v2, LX/FuI;->i:LX/G2X;

    const/4 p0, 0x0

    .line 2299864
    iget-boolean v3, v2, LX/G2X;->c:Z

    if-eqz v3, :cond_3

    .line 2299865
    iget-object v3, v2, LX/G2X;->a:LX/0ad;

    sget-short v4, LX/0wf;->r:S

    invoke-interface {v3, v4, p0}, LX/0ad;->a(SZ)Z

    move-result v3

    .line 2299866
    :goto_1
    move v2, v3

    .line 2299867
    if-nez v2, :cond_1

    .line 2299868
    sget-object v2, LX/DHs;->ALL_FRIENDS:LX/DHs;

    invoke-virtual {v2}, LX/DHs;->name()Ljava/lang/String;

    move-result-object v2

    .line 2299869
    :goto_2
    move-object v2, v2

    .line 2299870
    sget-object v3, LX/DHr;->TIMELINE_FRIENDS_PROTILE:LX/DHr;

    invoke-virtual {v3}, LX/DHr;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2299871
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/timeline/protiles/model/ProtileModel;->n()Ljava/lang/String;

    move-result-object v2

    .line 2299872
    invoke-static {v2}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v3, "\\d+[^()0-9]+"

    invoke-virtual {v2, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x1

    :goto_3
    move v2, v3

    .line 2299873
    if-eqz v2, :cond_2

    .line 2299874
    sget-object v2, LX/DHs;->MUTUAL_FRIENDS:LX/DHs;

    invoke-virtual {v2}, LX/DHs;->name()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    .line 2299875
    :cond_2
    sget-object v2, LX/DHs;->ALL_FRIENDS:LX/DHs;

    invoke-virtual {v2}, LX/DHs;->name()Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_3
    iget-object v3, v2, LX/G2X;->a:LX/0ad;

    sget-short v4, LX/0wf;->n:S

    invoke-interface {v3, v4, p0}, LX/0ad;->a(SZ)Z

    move-result v3

    goto :goto_1

    :cond_4
    const/4 v3, 0x0

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)V
    .locals 12

    .prologue
    .line 2299876
    check-cast p1, LX/G1D;

    .line 2299877
    iget-object v0, p1, LX/G1D;->b:Lcom/facebook/timeline/protiles/model/ProtileModel;

    move-object v0, v0

    .line 2299878
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/model/ProtileModel;->l()Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    move-result-object v6

    .line 2299879
    iget-object v0, p1, LX/G1D;->a:LX/G1C;

    move-object v0, v0

    .line 2299880
    sget-object v1, LX/G1C;->CLICK_HEADER:LX/G1C;

    if-eq v0, v1, :cond_0

    .line 2299881
    iget-object v0, p1, LX/G1D;->a:LX/G1C;

    move-object v0, v0

    .line 2299882
    sget-object v1, LX/G1C;->CLICK_FOOTER:LX/G1C;

    if-ne v0, v1, :cond_2

    .line 2299883
    :cond_0
    iget-object v0, p1, LX/G1D;->b:Lcom/facebook/timeline/protiles/model/ProtileModel;

    move-object v0, v0

    .line 2299884
    invoke-direct {p0, v0}, LX/FuG;->a(Lcom/facebook/timeline/protiles/model/ProtileModel;)Ljava/lang/String;

    move-result-object v0

    .line 2299885
    if-eqz v0, :cond_2

    .line 2299886
    iget-object v1, p0, LX/FuG;->a:LX/FuI;

    iget-object v1, v1, LX/FuI;->a:LX/17W;

    iget-object v2, p0, LX/FuG;->a:LX/FuI;

    iget-object v2, v2, LX/FuI;->c:Landroid/content/Context;

    .line 2299887
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 2299888
    const-string v9, "profile_name"

    iget-object v10, p0, LX/FuG;->a:LX/FuI;

    iget-object v10, v10, LX/FuI;->d:LX/BQ1;

    invoke-virtual {v10}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2299889
    sget-object v9, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-ne v6, v9, :cond_6

    .line 2299890
    iget-object v9, p0, LX/FuG;->a:LX/FuI;

    iget-object v9, v9, LX/FuI;->e:LX/G4m;

    invoke-static {v9, v8}, LX/G0z;->a(LX/G4m;Landroid/os/Bundle;)V

    .line 2299891
    const-string v9, "fullscreen_gallery_source"

    sget-object v10, LX/74S;->TIMELINE_PHOTOS_OF_USER:LX/74S;

    invoke-virtual {v10}, LX/74S;->name()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2299892
    const-string v9, "disable_camera_roll"

    const/4 v10, 0x1

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2299893
    :cond_1
    :goto_0
    move-object v3, v8

    .line 2299894
    invoke-virtual {v1, v2, v0, v3}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Z

    .line 2299895
    :cond_2
    iget-object v0, p0, LX/FuG;->a:LX/FuI;

    iget-object v0, v0, LX/FuI;->f:LX/5SB;

    invoke-virtual {v0}, LX/5SB;->i()Z

    move-result v0

    iget-object v1, p0, LX/FuG;->a:LX/FuI;

    iget-object v1, v1, LX/FuI;->d:LX/BQ1;

    invoke-virtual {v1}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v1

    iget-object v2, p0, LX/FuG;->a:LX/FuI;

    iget-object v2, v2, LX/FuI;->d:LX/BQ1;

    invoke-virtual {v2}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v2

    invoke-static {v0, v1, v2}, LX/9lQ;->getRelationshipType(ZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/9lQ;

    move-result-object v5

    .line 2299896
    sget-object v0, LX/FuH;->a:[I

    .line 2299897
    iget-object v1, p1, LX/G1D;->a:LX/G1C;

    move-object v1, v1

    .line 2299898
    invoke-virtual {v1}, LX/G1C;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2299899
    :cond_3
    :goto_1
    return-void

    .line 2299900
    :pswitch_0
    iget-object v0, p0, LX/FuG;->a:LX/FuI;

    iget-object v0, v0, LX/FuI;->g:LX/BQ9;

    iget-object v1, p0, LX/FuG;->a:LX/FuI;

    iget-object v1, v1, LX/FuI;->f:LX/5SB;

    .line 2299901
    iget-wide v8, v1, LX/5SB;->b:J

    move-wide v2, v8

    .line 2299902
    invoke-virtual {v0, v2, v3, v5, v6}, LX/BQ9;->a(JLX/9lQ;Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;)V

    goto :goto_1

    .line 2299903
    :pswitch_1
    iget-object v0, p0, LX/FuG;->a:LX/FuI;

    iget-object v0, v0, LX/FuI;->g:LX/BQ9;

    iget-object v1, p0, LX/FuG;->a:LX/FuI;

    iget-object v1, v1, LX/FuI;->f:LX/5SB;

    .line 2299904
    iget-wide v8, v1, LX/5SB;->b:J

    move-wide v2, v8

    .line 2299905
    invoke-virtual {v0, v2, v3, v5, v6}, LX/BQ9;->b(JLX/9lQ;Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;)V

    goto :goto_1

    .line 2299906
    :pswitch_2
    iget-object v0, p1, LX/G1D;->c:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    move-object v0, v0

    .line 2299907
    if-eqz v0, :cond_4

    .line 2299908
    iget-object v0, p1, LX/G1D;->c:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    move-object v0, v0

    .line 2299909
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 2299910
    iget-object v0, p1, LX/G1D;->c:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    move-object v0, v0

    .line 2299911
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->d()Ljava/lang/String;

    move-result-object v4

    .line 2299912
    :goto_2
    iget-object v0, p1, LX/G1D;->c:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    move-object v0, v0

    .line 2299913
    if-eqz v0, :cond_5

    .line 2299914
    iget-object v0, p1, LX/G1D;->c:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    move-object v0, v0

    .line 2299915
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 2299916
    iget-object v0, p1, LX/G1D;->c:Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;

    move-object v0, v0

    .line 2299917
    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel;->b()Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/timeline/protiles/protocol/FetchProtilesGraphQLModels$ProtileItemFieldsModel$NodeModel;->q()I

    move-result v7

    .line 2299918
    :goto_3
    iget-object v0, p0, LX/FuG;->a:LX/FuI;

    iget-object v1, v0, LX/FuI;->g:LX/BQ9;

    iget-object v0, p0, LX/FuG;->a:LX/FuI;

    iget-object v0, v0, LX/FuI;->f:LX/5SB;

    .line 2299919
    iget-wide v8, v0, LX/5SB;->b:J

    move-wide v2, v8

    .line 2299920
    invoke-virtual/range {v1 .. v7}, LX/BQ9;->a(JLjava/lang/String;LX/9lQ;Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;I)V

    .line 2299921
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->FRIENDS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-ne v6, v0, :cond_3

    .line 2299922
    iget-object v0, p0, LX/FuG;->a:LX/FuI;

    iget-object v0, v0, LX/FuI;->h:LX/0gh;

    const-string v1, "tap_protile_friend_to_timeline"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    goto :goto_1

    .line 2299923
    :cond_4
    const/4 v4, 0x0

    goto :goto_2

    .line 2299924
    :cond_5
    const/4 v7, 0x0

    goto :goto_3

    .line 2299925
    :cond_6
    sget-object v9, Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;->VIDEOS:Lcom/facebook/graphql/enums/GraphQLProfileTileSectionType;

    if-ne v6, v9, :cond_1

    .line 2299926
    const-string v9, "com.facebook.katana.profile.id"

    iget-object v10, p0, LX/FuG;->a:LX/FuI;

    iget-object v10, v10, LX/FuI;->d:LX/BQ1;

    invoke-virtual {v10}, LX/BQ1;->G()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2299927
    const-string v9, "profile_name"

    iget-object v10, p0, LX/FuG;->a:LX/FuI;

    iget-object v10, v10, LX/FuI;->d:LX/BQ1;

    invoke-virtual {v10}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
