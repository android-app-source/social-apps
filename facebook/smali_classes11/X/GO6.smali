.class public final enum LX/GO6;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GO6;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GO6;

.field public static final enum BUSINESS_ADDRESS:LX/GO6;

.field public static final enum CLIENT_ADDRESS:LX/GO6;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2347127
    new-instance v0, LX/GO6;

    const-string v1, "BUSINESS_ADDRESS"

    invoke-direct {v0, v1, v2}, LX/GO6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GO6;->BUSINESS_ADDRESS:LX/GO6;

    .line 2347128
    new-instance v0, LX/GO6;

    const-string v1, "CLIENT_ADDRESS"

    invoke-direct {v0, v1, v3}, LX/GO6;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GO6;->CLIENT_ADDRESS:LX/GO6;

    .line 2347129
    const/4 v0, 0x2

    new-array v0, v0, [LX/GO6;

    sget-object v1, LX/GO6;->BUSINESS_ADDRESS:LX/GO6;

    aput-object v1, v0, v2

    sget-object v1, LX/GO6;->CLIENT_ADDRESS:LX/GO6;

    aput-object v1, v0, v3

    sput-object v0, LX/GO6;->$VALUES:[LX/GO6;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2347132
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static find(Ljava/lang/String;)LX/GO6;
    .locals 5

    .prologue
    .line 2347133
    invoke-static {}, LX/GO6;->values()[LX/GO6;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 2347134
    invoke-virtual {v3}, LX/GO6;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2347135
    return-object v3

    .line 2347136
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2347137
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not a valid "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-class v2, LX/GO6;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/GO6;
    .locals 1

    .prologue
    .line 2347131
    const-class v0, LX/GO6;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GO6;

    return-object v0
.end method

.method public static values()[LX/GO6;
    .locals 1

    .prologue
    .line 2347130
    sget-object v0, LX/GO6;->$VALUES:[LX/GO6;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GO6;

    return-object v0
.end method
