.class public LX/Fgp;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/Fgb;
.implements LX/2SR;
.implements LX/2Sp;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FiH;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/FgU;

.field public final d:LX/Fid;

.field private final e:LX/FhF;

.field public final f:LX/Fi6;

.field private final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final h:LX/1DS;

.field public final i:LX/Cvy;

.field private final j:LX/1Db;

.field private final k:LX/Fht;

.field public l:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Fb5;",
            ">;"
        }
    .end annotation
.end field

.field private m:LX/Fi7;

.field public n:LX/2SP;

.field public o:LX/1Qq;

.field public p:LX/0g8;

.field public q:LX/Fhs;

.field public r:Lcom/facebook/search/api/GraphSearchQuery;

.field public s:LX/Fh9;

.field private t:Landroid/view/View;

.field private u:LX/Fgd;

.field public v:LX/7HZ;

.field private w:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/search/api/GraphSearchQuery;LX/0Ot;LX/FgU;LX/Fid;LX/FhF;LX/Fi6;LX/0Ot;LX/1DS;LX/Cvy;LX/1Db;LX/Fht;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/search/api/GraphSearchQuery;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/search/api/GraphSearchQuery;",
            "LX/0Ot",
            "<",
            "LX/FiH;",
            ">;",
            "LX/FgU;",
            "LX/Fid;",
            "LX/FhF;",
            "LX/Fi6;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/search/typeahead/rows/SearchTypeaheadRootGroupPartDefinition;",
            ">;",
            "LX/1DS;",
            "LX/Cvy;",
            "LX/1Db;",
            "LX/Fht;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2270942
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2270943
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2270944
    iput-object v0, p0, LX/Fgp;->l:LX/0Ot;

    .line 2270945
    sget-object v0, Lcom/facebook/search/api/GraphSearchQuery;->e:Lcom/facebook/search/api/GraphSearchQuery;

    iput-object v0, p0, LX/Fgp;->r:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2270946
    sget-object v0, LX/7HZ;->IDLE:LX/7HZ;

    iput-object v0, p0, LX/Fgp;->v:LX/7HZ;

    .line 2270947
    iput-object p1, p0, LX/Fgp;->a:Landroid/content/Context;

    .line 2270948
    iput-object p2, p0, LX/Fgp;->r:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2270949
    iput-object p3, p0, LX/Fgp;->b:LX/0Ot;

    .line 2270950
    iput-object p4, p0, LX/Fgp;->c:LX/FgU;

    .line 2270951
    iput-object p5, p0, LX/Fgp;->d:LX/Fid;

    .line 2270952
    iput-object p6, p0, LX/Fgp;->e:LX/FhF;

    .line 2270953
    iput-object p7, p0, LX/Fgp;->f:LX/Fi6;

    .line 2270954
    iput-object p8, p0, LX/Fgp;->g:LX/0Ot;

    .line 2270955
    iput-object p9, p0, LX/Fgp;->h:LX/1DS;

    .line 2270956
    iput-object p10, p0, LX/Fgp;->i:LX/Cvy;

    .line 2270957
    iput-object p11, p0, LX/Fgp;->j:LX/1Db;

    .line 2270958
    iput-object p12, p0, LX/Fgp;->k:LX/Fht;

    .line 2270959
    iget-object v0, p0, LX/Fgp;->r:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {p0, v0}, LX/Fgp;->a(Lcom/facebook/search/api/GraphSearchQuery;)V

    .line 2270960
    return-void
.end method

.method private b(Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 4

    .prologue
    .line 2270964
    iget-object v0, p0, LX/Fgp;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FiH;

    const/4 v3, 0x0

    .line 2270965
    iget-object v1, v0, LX/FiH;->k:LX/0Uh;

    sget v2, LX/2SU;->e:I

    invoke-virtual {v1, v2}, LX/0Uh;->a(I)LX/03R;

    move-result-object v1

    invoke-virtual {v1, v3}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, LX/FiH;->k:LX/0Uh;

    sget v2, LX/2SU;->y:I

    invoke-virtual {v1, v2, v3}, LX/0Uh;->a(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2270966
    :cond_0
    iget-object v1, v0, LX/FiH;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Fi7;

    .line 2270967
    :goto_0
    move-object v0, v1

    .line 2270968
    iput-object v0, p0, LX/Fgp;->m:LX/Fi7;

    .line 2270969
    return-void

    .line 2270970
    :cond_1
    invoke-static {p1}, LX/8ht;->b(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2270971
    iget-object v1, v0, LX/FiH;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Fi7;

    goto :goto_0

    .line 2270972
    :cond_2
    iget-object v1, v0, LX/FiH;->i:LX/8ht;

    invoke-virtual {v1, p1}, LX/8ht;->e(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2270973
    iget-object v1, v0, LX/FiH;->d:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Fi7;

    goto :goto_0

    .line 2270974
    :cond_3
    iget-object v1, v0, LX/FiH;->i:LX/8ht;

    invoke-virtual {v1, p1}, LX/8ht;->i(Lcom/facebook/search/api/GraphSearchQuery;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2270975
    iget-object v1, v0, LX/FiH;->j:LX/0ad;

    sget-short v2, LX/100;->aN:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, v0, LX/FiH;->f:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Fi8;

    goto :goto_0

    :cond_4
    iget-object v1, v0, LX/FiH;->e:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Fi8;

    goto :goto_0

    .line 2270976
    :cond_5
    iget-object v1, v0, LX/FiH;->j:LX/0ad;

    sget-short v2, LX/100;->bN:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2270977
    iget-object v1, v0, LX/FiH;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Fi7;

    goto :goto_0

    .line 2270978
    :cond_6
    iget-object v1, v0, LX/FiH;->j:LX/0ad;

    sget-short v2, LX/100;->bM:S

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2270979
    iget-object v1, v0, LX/FiH;->h:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Fi7;

    goto :goto_0

    .line 2270980
    :cond_7
    iget-object v1, v0, LX/FiH;->k:LX/0Uh;

    sget v2, LX/2SU;->n:I

    invoke-virtual {v1, v2}, LX/0Uh;->a(I)LX/03R;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, LX/03R;->asBoolean(Z)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2270981
    iget-object v1, v0, LX/FiH;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Fi7;

    goto/16 :goto_0

    .line 2270982
    :cond_8
    iget-object v1, v0, LX/FiH;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Fi7;

    goto/16 :goto_0
.end method

.method public static r(LX/Fgp;)Landroid/widget/ProgressBar;
    .locals 4

    .prologue
    .line 2270983
    iget-object v0, p0, LX/Fgp;->w:Landroid/widget/ProgressBar;

    if-nez v0, :cond_0

    .line 2270984
    iget-object v0, p0, LX/Fgp;->p:LX/0g8;

    invoke-interface {v0}, LX/0g8;->ih_()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0307ec

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, LX/Fgp;->w:Landroid/widget/ProgressBar;

    .line 2270985
    iget-object v0, p0, LX/Fgp;->p:LX/0g8;

    iget-object v1, p0, LX/Fgp;->w:Landroid/widget/ProgressBar;

    invoke-interface {v0, v1}, LX/0g8;->e(Landroid/view/View;)V

    .line 2270986
    :cond_0
    iget-object v0, p0, LX/Fgp;->w:Landroid/widget/ProgressBar;

    return-object v0
.end method


# virtual methods
.method public final a(LX/7Hi;)LX/0Px;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/7Hi",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2270987
    iget-object v0, p0, LX/Fgp;->m:LX/Fi7;

    iget-object v1, p0, LX/Fgp;->r:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2270988
    iget-object v2, p1, LX/7Hi;->b:LX/7Hc;

    move-object v2, v2

    .line 2270989
    iget-object v3, p0, LX/Fgp;->c:LX/FgU;

    invoke-virtual {v3}, LX/FgU;->h()Lcom/facebook/search/model/TypeaheadUnit;

    move-result-object v3

    iget-object v4, p0, LX/Fgp;->v:LX/7HZ;

    invoke-interface {v0, v1, v2, v3, v4}, LX/Fi7;->a(Lcom/facebook/search/api/GraphSearchQuery;LX/7Hc;Lcom/facebook/search/model/TypeaheadUnit;LX/7HZ;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)LX/Fid;
    .locals 1

    .prologue
    .line 2270990
    invoke-virtual {p0}, LX/Fgp;->l()LX/Fid;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 2270991
    iget-object v0, p0, LX/Fgp;->p:LX/0g8;

    if-eqz v0, :cond_0

    .line 2270992
    iget-object v0, p0, LX/Fgp;->p:LX/0g8;

    invoke-interface {v0, p1}, LX/0g8;->a(I)V

    .line 2270993
    :cond_0
    return-void
.end method

.method public final a(LX/0zw;Landroid/view/View;Z)V
    .locals 2
    .param p1    # LX/0zw;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p2    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0zw",
            "<",
            "Landroid/widget/ProgressBar;",
            ">;",
            "Landroid/view/View;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 2270907
    if-eqz p1, :cond_1

    :goto_0
    iget-object v0, p0, LX/Fgp;->p:LX/0g8;

    .line 2270908
    new-instance v1, LX/Fhs;

    invoke-direct {v1, p1, v0, p2}, LX/Fhs;-><init>(LX/0zw;LX/0g8;Landroid/view/View;)V

    .line 2270909
    move-object v0, v1

    .line 2270910
    iput-object v0, p0, LX/Fgp;->q:LX/Fhs;

    .line 2270911
    invoke-virtual {p0}, LX/Fgp;->j()LX/FgU;

    move-result-object v0

    invoke-virtual {v0}, LX/7HQ;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2270912
    iget-object v0, p0, LX/Fgp;->q:LX/Fhs;

    iget-object v1, p0, LX/Fgp;->n:LX/2SP;

    invoke-virtual {v1}, LX/2SP;->a()LX/7BE;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, LX/Fhs;->a(LX/7BE;Z)V

    .line 2270913
    :cond_0
    return-void

    .line 2270914
    :cond_1
    iget-object v0, p0, LX/Fgp;->u:LX/Fgd;

    .line 2270915
    iget-object v1, v0, LX/Fgd;->b:LX/0zw;

    move-object p1, v1

    .line 2270916
    goto :goto_0
.end method

.method public final a(LX/7BE;)V
    .locals 3

    .prologue
    .line 2270994
    iget-object v0, p0, LX/Fgp;->s:LX/Fh9;

    if-nez v0, :cond_0

    .line 2270995
    :goto_0
    return-void

    .line 2270996
    :cond_0
    iget-object v0, p0, LX/Fgp;->s:LX/Fh9;

    iget-object v1, p0, LX/Fgp;->q:LX/Fhs;

    iget-object v2, p0, LX/Fgp;->c:LX/FgU;

    .line 2270997
    iget-object p0, v0, LX/Fh9;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->D(Lcom/facebook/search/suggestions/SuggestionsFragment;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, LX/0YN;->d(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_1

    .line 2270998
    :goto_1
    goto :goto_0

    .line 2270999
    :cond_1
    iget-object p0, v0, LX/Fh9;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {p0}, Lcom/facebook/search/suggestions/SuggestionsFragment;->L$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;)Z

    move-result p0

    invoke-virtual {v1, p1, p0}, LX/Fhs;->a(LX/7BE;Z)V

    .line 2271000
    invoke-virtual {v2}, LX/FgU;->g()V

    goto :goto_1
.end method

.method public final a(LX/7HP;)V
    .locals 1

    .prologue
    .line 2271001
    iget-object v0, p0, LX/Fgp;->c:LX/FgU;

    .line 2271002
    iput-object p1, v0, LX/7HQ;->k:LX/7HP;

    .line 2271003
    return-void
.end method

.method public final a(LX/7HZ;)V
    .locals 8

    .prologue
    .line 2271004
    iget-object v0, p0, LX/Fgp;->s:LX/Fh9;

    if-nez v0, :cond_0

    .line 2271005
    :goto_0
    return-void

    .line 2271006
    :cond_0
    iget-object v0, p0, LX/Fgp;->s:LX/Fh9;

    invoke-static {p0}, LX/Fgp;->r(LX/Fgp;)Landroid/widget/ProgressBar;

    move-result-object v1

    iget-object v2, p0, LX/Fgp;->p:LX/0g8;

    .line 2271007
    iget-object v3, v0, LX/Fh9;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v3, v3, Lcom/facebook/search/suggestions/SuggestionsFragment;->L:LX/2Sd;

    sget-object v4, Lcom/facebook/search/suggestions/SuggestionsFragment;->b:Ljava/lang/String;

    sget-object v5, LX/7CQ;->FETCH_STATE_CHANGED:LX/7CQ;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Old: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2271008
    iget-object v7, p0, LX/Fgp;->v:LX/7HZ;

    move-object v7, v7

    .line 2271009
    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", New: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v4, v5, v6}, LX/2Sd;->a(Ljava/lang/String;LX/7CQ;Ljava/lang/String;)V

    .line 2271010
    iput-object p1, p0, LX/Fgp;->v:LX/7HZ;

    .line 2271011
    sget-object v3, LX/7HZ;->ACTIVE:LX/7HZ;

    if-eq p1, v3, :cond_1

    .line 2271012
    iget-object v3, v0, LX/Fh9;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    iget-object v4, v0, LX/Fh9;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v4}, Lcom/facebook/search/suggestions/SuggestionsFragment;->D(Lcom/facebook/search/suggestions/SuggestionsFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/facebook/search/suggestions/SuggestionsFragment;->c$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Ljava/lang/String;)V

    .line 2271013
    :cond_1
    iget-object v3, v0, LX/Fh9;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v3, v1, v2, p1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;Landroid/widget/ProgressBar;LX/0g8;LX/7HZ;)V

    .line 2271014
    iget-object v3, v0, LX/Fh9;->a:Lcom/facebook/search/suggestions/SuggestionsFragment;

    invoke-static {v3, p1}, Lcom/facebook/search/suggestions/SuggestionsFragment;->a$redex0(Lcom/facebook/search/suggestions/SuggestionsFragment;LX/7HZ;)V

    .line 2271015
    goto :goto_0
.end method

.method public final a(LX/Cvp;)V
    .locals 2

    .prologue
    .line 2271016
    iget-object v0, p0, LX/Fgp;->c:LX/FgU;

    iget-object v1, p0, LX/Fgp;->r:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, v1}, LX/7HQ;->a(LX/7B6;)V

    .line 2271017
    iget-object v0, p0, LX/Fgp;->c:LX/FgU;

    invoke-virtual {v0, p1}, LX/FgU;->a(LX/Cvp;)V

    .line 2271018
    return-void
.end method

.method public final a(LX/Fh9;)V
    .locals 3
    .param p1    # LX/Fh9;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2271019
    iput-object p1, p0, LX/Fgp;->s:LX/Fh9;

    .line 2271020
    iget-object v0, p0, LX/Fgp;->n:LX/2SP;

    if-eqz v0, :cond_0

    .line 2271021
    if-eqz p1, :cond_1

    .line 2271022
    iget-object v0, p0, LX/Fgp;->n:LX/2SP;

    invoke-virtual {v0, p0, p0}, LX/2SP;->a(LX/2SR;LX/2Sp;)V

    .line 2271023
    :cond_0
    :goto_0
    iget-object v2, p0, LX/Fgp;->c:LX/FgU;

    if-nez p1, :cond_2

    move-object v0, v1

    :goto_1
    invoke-virtual {v2, v0}, LX/FgU;->a(LX/Fgp;)V

    .line 2271024
    iget-object v0, p0, LX/Fgp;->c:LX/FgU;

    if-nez p1, :cond_3

    :goto_2
    invoke-virtual {v0, v1}, LX/7HQ;->a(LX/2Sp;)V

    .line 2271025
    return-void

    .line 2271026
    :cond_1
    iget-object v0, p0, LX/Fgp;->n:LX/2SP;

    invoke-virtual {v0, v1, v1}, LX/2SP;->a(LX/2SR;LX/2Sp;)V

    goto :goto_0

    :cond_2
    move-object v0, p0

    .line 2271027
    goto :goto_1

    :cond_3
    move-object v1, p0

    .line 2271028
    goto :goto_2
.end method

.method public final a(Landroid/content/Context;LX/Fh8;)V
    .locals 10

    .prologue
    .line 2271029
    sget-object v0, LX/Fip;->a:LX/Fip;

    move-object v0, v0

    .line 2271030
    new-instance v1, Lcom/facebook/search/suggestions/SingleSearchSuggestionListController$1;

    invoke-direct {v1, p0}, Lcom/facebook/search/suggestions/SingleSearchSuggestionListController$1;-><init>(LX/Fgp;)V

    iget-object v2, p0, LX/Fgp;->d:LX/Fid;

    .line 2271031
    new-instance v4, LX/FhE;

    move-object v5, p1

    move-object v6, v0

    move-object v7, v1

    move-object v8, p2

    move-object v9, v2

    invoke-direct/range {v4 .. v9}, LX/FhE;-><init>(Landroid/content/Context;LX/1PT;Ljava/lang/Runnable;LX/Fh8;LX/Fid;)V

    .line 2271032
    move-object v0, v4

    .line 2271033
    iget-object v1, p0, LX/Fgp;->h:LX/1DS;

    iget-object v2, p0, LX/Fgp;->g:LX/0Ot;

    iget-object v3, p0, LX/Fgp;->d:LX/Fid;

    invoke-virtual {v1, v2, v3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    .line 2271034
    iput-object v0, v1, LX/1Ql;->f:LX/1PW;

    .line 2271035
    move-object v0, v1

    .line 2271036
    const/4 v1, 0x1

    iput-boolean v1, v0, LX/1Ql;->l:Z

    .line 2271037
    move-object v0, v0

    .line 2271038
    invoke-virtual {v0}, LX/1Ql;->e()LX/1Qq;

    move-result-object v0

    iput-object v0, p0, LX/Fgp;->o:LX/1Qq;

    .line 2271039
    iget-object v0, p0, LX/Fgp;->o:LX/1Qq;

    new-instance v1, LX/Fgi;

    invoke-direct {v1, p0}, LX/Fgi;-><init>(LX/Fgp;)V

    invoke-interface {v0, v1}, LX/1Qq;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2271040
    return-void
.end method

.method public final a(Landroid/view/View;LX/FhA;Z)V
    .locals 5
    .param p1    # Landroid/view/View;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2271041
    if-nez p1, :cond_1

    .line 2271042
    new-instance v0, LX/Fgd;

    iget-object v1, p0, LX/Fgp;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p3}, LX/Fgd;-><init>(Landroid/content/Context;Z)V

    iput-object v0, p0, LX/Fgp;->u:LX/Fgd;

    .line 2271043
    iget-object v0, p0, LX/Fgp;->u:LX/Fgd;

    iput-object v0, p0, LX/Fgp;->t:Landroid/view/View;

    .line 2271044
    iget-object v0, p0, LX/Fgp;->u:LX/Fgd;

    .line 2271045
    iget-object v1, v0, LX/Fgd;->a:Landroid/view/View;

    move-object v0, v1

    .line 2271046
    :goto_0
    if-eqz p3, :cond_2

    .line 2271047
    check-cast v0, Lcom/facebook/widget/recyclerview/BetterRecyclerView;

    .line 2271048
    new-instance v1, LX/Fgo;

    invoke-virtual {v0}, Lcom/facebook/widget/recyclerview/BetterRecyclerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, LX/Fgo;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(LX/1OR;)V

    .line 2271049
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(LX/1Of;)V

    .line 2271050
    new-instance v1, LX/0g7;

    invoke-direct {v1, v0}, LX/0g7;-><init>(Lcom/facebook/widget/recyclerview/BetterRecyclerView;)V

    iput-object v1, p0, LX/Fgp;->p:LX/0g8;

    .line 2271051
    :goto_1
    iget-object v0, p0, LX/Fgp;->r:Lcom/facebook/search/api/GraphSearchQuery;

    sget-object v1, Lcom/facebook/search/api/GraphSearchQuery;->e:Lcom/facebook/search/api/GraphSearchQuery;

    if-ne v0, v1, :cond_0

    .line 2271052
    iget-object v0, p0, LX/Fgp;->l:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Fb5;

    iget-object v1, p0, LX/Fgp;->p:LX/0g8;

    .line 2271053
    const/4 v3, 0x0

    .line 2271054
    iget-object v2, v0, LX/Fb5;->b:LX/0iA;

    sget-object v4, LX/FaH;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-virtual {v2, v4}, LX/0iA;->b(Lcom/facebook/interstitial/manager/InterstitialTrigger;)Z

    move-result v2

    if-nez v2, :cond_5

    move v2, v3

    .line 2271055
    :goto_2
    move v2, v2

    .line 2271056
    if-eqz v2, :cond_3

    .line 2271057
    :cond_0
    :goto_3
    iget-object v0, p0, LX/Fgp;->p:LX/0g8;

    iget-object v1, p0, LX/Fgp;->o:LX/1Qq;

    invoke-interface {v0, v1}, LX/0g8;->a(Landroid/widget/ListAdapter;)V

    .line 2271058
    iget-object v0, p0, LX/Fgp;->p:LX/0g8;

    iget-object v1, p0, LX/Fgp;->j:LX/1Db;

    invoke-virtual {v1}, LX/1Db;->a()LX/1St;

    move-result-object v1

    invoke-interface {v0, v1}, LX/0g8;->a(LX/1St;)V

    .line 2271059
    iget-object v0, p0, LX/Fgp;->p:LX/0g8;

    new-instance v1, LX/Fgj;

    invoke-direct {v1, p0, p2}, LX/Fgj;-><init>(LX/Fgp;LX/FhA;)V

    invoke-interface {v0, v1}, LX/0g8;->a(LX/2ii;)V

    .line 2271060
    iget-object v0, p0, LX/Fgp;->p:LX/0g8;

    new-instance v1, LX/Fgk;

    invoke-direct {v1, p0, p2}, LX/Fgk;-><init>(LX/Fgp;LX/FhA;)V

    invoke-interface {v0, v1}, LX/0g8;->a(LX/3TV;)V

    .line 2271061
    iget-object v0, p0, LX/Fgp;->p:LX/0g8;

    new-instance v1, LX/Fgl;

    invoke-direct {v1, p0, p2}, LX/Fgl;-><init>(LX/Fgp;LX/FhA;)V

    invoke-interface {v0, v1}, LX/0g8;->a(LX/1Z7;)V

    .line 2271062
    iget-object v0, p0, LX/Fgp;->p:LX/0g8;

    new-instance v1, LX/Fgm;

    invoke-direct {v1, p0, p2}, LX/Fgm;-><init>(LX/Fgp;LX/FhA;)V

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fx;)V

    .line 2271063
    iget-object v0, p0, LX/Fgp;->p:LX/0g8;

    new-instance v1, LX/Fgn;

    invoke-direct {v1, p0, p2}, LX/Fgn;-><init>(LX/Fgp;LX/FhA;)V

    invoke-interface {v0, v1}, LX/0g8;->b(LX/0fu;)V

    .line 2271064
    return-void

    .line 2271065
    :cond_1
    iput-object p1, p0, LX/Fgp;->t:Landroid/view/View;

    move-object v0, p1

    goto/16 :goto_0

    .line 2271066
    :cond_2
    check-cast v0, Lcom/facebook/widget/listview/BetterListView;

    .line 2271067
    new-instance v1, LX/2iI;

    invoke-direct {v1, v0}, LX/2iI;-><init>(Lcom/facebook/widget/listview/BetterListView;)V

    iput-object v1, p0, LX/Fgp;->p:LX/0g8;

    .line 2271068
    invoke-static {p0}, LX/Fgp;->r(LX/Fgp;)Landroid/widget/ProgressBar;

    goto :goto_1

    .line 2271069
    :cond_3
    invoke-static {v0}, LX/Fb5;->b(LX/Fb5;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    move-result-object v2

    if-eqz v2, :cond_8

    const/4 v2, 0x1

    :goto_4
    move v2, v2

    .line 2271070
    if-nez v2, :cond_7

    .line 2271071
    :cond_4
    :goto_5
    goto :goto_3

    .line 2271072
    :cond_5
    iget-object v2, v0, LX/Fb5;->b:LX/0iA;

    const-string v4, "4446"

    const-class p1, LX/FaH;

    invoke-virtual {v2, v4, p1}, LX/0iA;->a(Ljava/lang/String;Ljava/lang/Class;)LX/0i1;

    move-result-object v2

    check-cast v2, LX/FaH;

    .line 2271073
    if-eqz v2, :cond_6

    iget-object v2, v0, LX/Fb5;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/15W;

    iget-object v4, v0, LX/Fb5;->a:Landroid/content/Context;

    sget-object p1, LX/FaH;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    const-class p3, LX/FaH;

    invoke-virtual {v2, v4, p1, p3, v1}, LX/15W;->a(Landroid/content/Context;Lcom/facebook/interstitial/manager/InterstitialTrigger;Ljava/lang/Class;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x1

    goto/16 :goto_2

    :cond_6
    move v2, v3

    goto/16 :goto_2

    .line 2271074
    :cond_7
    new-instance v2, LX/B9g;

    iget-object v3, v0, LX/Fb5;->a:Landroid/content/Context;

    invoke-direct {v2, v3}, LX/B9g;-><init>(Landroid/content/Context;)V

    .line 2271075
    invoke-static {v0}, LX/Fb5;->b(LX/Fb5;)Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;

    move-result-object v3

    .line 2271076
    if-nez v3, :cond_9

    .line 2271077
    :goto_6
    new-instance v3, Lcom/facebook/search/quickpromotion/SearchNullStateMegaphoneController$1;

    invoke-direct {v3, v0, v1, v2}, Lcom/facebook/search/quickpromotion/SearchNullStateMegaphoneController$1;-><init>(LX/Fb5;LX/0g8;LX/B9g;)V

    invoke-virtual {v2, v3}, LX/B9g;->setOnDismiss(Ljava/lang/Runnable;)V

    .line 2271078
    invoke-virtual {v2}, LX/B9g;->getVisibility()I

    move-result v3

    if-nez v3, :cond_4

    if-eqz v1, :cond_4

    .line 2271079
    invoke-interface {v1, v2}, LX/0g8;->d(Landroid/view/View;)V

    goto :goto_5

    :cond_8
    const/4 v2, 0x0

    goto :goto_4

    .line 2271080
    :cond_9
    const-string v4, "3191"

    sget-object p1, LX/FaI;->a:Lcom/facebook/interstitial/manager/InterstitialTrigger;

    invoke-virtual {v2, v3, v4, p1}, LX/B9g;->a(Lcom/facebook/quickpromotion/model/QuickPromotionDefinition;Ljava/lang/String;Lcom/facebook/interstitial/manager/InterstitialTrigger;)V

    goto :goto_6
.end method

.method public final a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V
    .locals 1

    .prologue
    .line 2271081
    iget-object v0, p0, LX/Fgp;->n:LX/2SP;

    invoke-virtual {v0, p1, p2}, LX/2SP;->a(Lcom/facebook/common/callercontext/CallerContext;LX/EPu;)V

    .line 2271082
    return-void
.end method

.method public final a(Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 2

    .prologue
    .line 2271083
    iput-object p1, p0, LX/Fgp;->r:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2271084
    const/4 v1, 0x0

    .line 2271085
    iget-object v0, p0, LX/Fgp;->n:LX/2SP;

    if-eqz v0, :cond_0

    .line 2271086
    iget-object v0, p0, LX/Fgp;->n:LX/2SP;

    invoke-virtual {v0, v1, v1}, LX/2SP;->a(LX/2SR;LX/2Sp;)V

    .line 2271087
    :cond_0
    iget-object v0, p0, LX/Fgp;->f:LX/Fi6;

    iget-object v1, p0, LX/Fgp;->r:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-virtual {v0, v1}, LX/Fi6;->a(Lcom/facebook/search/api/GraphSearchQuery;)LX/2SP;

    move-result-object v0

    iput-object v0, p0, LX/Fgp;->n:LX/2SP;

    .line 2271088
    iget-object v0, p0, LX/Fgp;->s:LX/Fh9;

    if-eqz v0, :cond_1

    .line 2271089
    iget-object v0, p0, LX/Fgp;->n:LX/2SP;

    invoke-virtual {v0, p0, p0}, LX/2SP;->a(LX/2SR;LX/2Sp;)V

    .line 2271090
    :cond_1
    invoke-direct {p0, p1}, LX/Fgp;->b(Lcom/facebook/search/api/GraphSearchQuery;)V

    .line 2271091
    return-void
.end method

.method public final a(Ljava/lang/String;LX/0Px;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "LX/0Px",
            "<",
            "Lcom/facebook/search/model/TypeaheadUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2270961
    iget-object v0, p0, LX/Fgp;->d:LX/Fid;

    invoke-virtual {v0, p2}, LX/Fid;->a(Ljava/util/List;)V

    .line 2270962
    iget-object v0, p0, LX/Fgp;->o:LX/1Qq;

    invoke-interface {v0}, LX/1Cw;->notifyDataSetChanged()V

    .line 2270963
    return-void
.end method

.method public final a(Ljava/lang/String;LX/7BE;Z)V
    .locals 2

    .prologue
    .line 2270904
    iget-object v0, p0, LX/Fgp;->q:LX/Fhs;

    if-eqz v0, :cond_0

    .line 2270905
    iget-object v0, p0, LX/Fgp;->q:LX/Fhs;

    sget-object v1, LX/7BE;->READY:LX/7BE;

    invoke-virtual {v0, v1, p3}, LX/Fhs;->a(LX/7BE;Z)V

    .line 2270906
    :cond_0
    return-void
.end method

.method public final c()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2270917
    iget-object v0, p0, LX/Fgp;->t:Landroid/view/View;

    invoke-static {v0}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v0

    return-object v0
.end method

.method public final d()LX/1Qq;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2270918
    iget-object v0, p0, LX/Fgp;->o:LX/1Qq;

    return-object v0
.end method

.method public final e()LX/7HZ;
    .locals 1

    .prologue
    .line 2270919
    iget-object v0, p0, LX/Fgp;->v:LX/7HZ;

    return-object v0
.end method

.method public final f()Landroid/view/View;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2270920
    iget-object v0, p0, LX/Fgp;->t:Landroid/view/View;

    return-object v0
.end method

.method public final g()LX/0g8;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2270921
    iget-object v0, p0, LX/Fgp;->p:LX/0g8;

    return-object v0
.end method

.method public final h()LX/2SP;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2270922
    iget-object v0, p0, LX/Fgp;->n:LX/2SP;

    return-object v0
.end method

.method public final i()Lcom/facebook/search/api/GraphSearchQuery;
    .locals 1

    .prologue
    .line 2270923
    iget-object v0, p0, LX/Fgp;->r:Lcom/facebook/search/api/GraphSearchQuery;

    return-object v0
.end method

.method public final j()LX/FgU;
    .locals 1

    .prologue
    .line 2270924
    iget-object v0, p0, LX/Fgp;->c:LX/FgU;

    return-object v0
.end method

.method public final k()LX/Fi7;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2270925
    iget-object v0, p0, LX/Fgp;->m:LX/Fi7;

    return-object v0
.end method

.method public final l()LX/Fid;
    .locals 1

    .prologue
    .line 2270926
    iget-object v0, p0, LX/Fgp;->d:LX/Fid;

    return-object v0
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 2270927
    iget-object v0, p0, LX/Fgp;->r:Lcom/facebook/search/api/GraphSearchQuery;

    sget-object v1, Lcom/facebook/search/api/GraphSearchQuery;->e:Lcom/facebook/search/api/GraphSearchQuery;

    if-ne v0, v1, :cond_0

    .line 2270928
    :goto_0
    return-void

    .line 2270929
    :cond_0
    sget-object v0, Lcom/facebook/search/api/GraphSearchQuery;->e:Lcom/facebook/search/api/GraphSearchQuery;

    iput-object v0, p0, LX/Fgp;->r:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2270930
    sget-object v0, Lcom/facebook/search/api/GraphSearchQuery;->e:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-direct {p0, v0}, LX/Fgp;->b(Lcom/facebook/search/api/GraphSearchQuery;)V

    goto :goto_0
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 2270931
    iget-object v0, p0, LX/Fgp;->o:LX/1Qq;

    invoke-interface {v0}, LX/0Vf;->dispose()V

    .line 2270932
    return-void
.end method

.method public final o()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2270933
    iput-object v0, p0, LX/Fgp;->p:LX/0g8;

    .line 2270934
    iput-object v0, p0, LX/Fgp;->w:Landroid/widget/ProgressBar;

    .line 2270935
    iget-object v0, p0, LX/Fgp;->q:LX/Fhs;

    invoke-virtual {v0}, LX/Fhs;->a()V

    .line 2270936
    return-void
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 2270937
    iget-object v0, p0, LX/Fgp;->c:LX/FgU;

    invoke-virtual {v0}, LX/FgU;->g()V

    .line 2270938
    return-void
.end method

.method public final q()V
    .locals 2

    .prologue
    .line 2270939
    iget-object v0, p0, LX/Fgp;->p:LX/0g8;

    if-nez v0, :cond_0

    .line 2270940
    :goto_0
    return-void

    .line 2270941
    :cond_0
    iget-object v0, p0, LX/Fgp;->p:LX/0g8;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, LX/0g8;->g(I)V

    goto :goto_0
.end method
