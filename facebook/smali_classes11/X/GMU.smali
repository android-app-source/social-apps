.class public LX/GMU;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:[LX/8wL;

.field private static volatile c:LX/GMU;


# instance fields
.field private final b:LX/2U3;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2344742
    const/4 v0, 0x3

    new-array v0, v0, [LX/8wL;

    const/4 v1, 0x0

    sget-object v2, LX/8wL;->PROMOTE_PRODUCT:LX/8wL;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, LX/8wL;->BOOST_POST:LX/8wL;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, LX/8wL;->BOOST_EVENT:LX/8wL;

    aput-object v2, v0, v1

    sput-object v0, LX/GMU;->a:[LX/8wL;

    return-void
.end method

.method public constructor <init>(LX/2U3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2344708
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2344709
    iput-object p1, p0, LX/GMU;->b:LX/2U3;

    .line 2344710
    return-void
.end method

.method public static a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)I
    .locals 4

    .prologue
    .line 2344711
    invoke-static {p0}, LX/GMU;->d(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2344712
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v1, v1

    .line 2344713
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    .line 2344714
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->D()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    .line 2344715
    invoke-static {v2}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-static {v1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v1

    .line 2344716
    const/4 v2, 0x0

    sget-object v3, Ljava/math/RoundingMode;->DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v1, v0, v2, v3}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->intValue()I

    move-result v0

    return v0
.end method

.method public static a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;I)I
    .locals 1

    .prologue
    .line 2344717
    invoke-static {p0}, LX/GMU;->a(Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;)I

    move-result v0

    .line 2344718
    sub-int/2addr v0, p1

    return v0
.end method

.method public static a(LX/0QB;)LX/GMU;
    .locals 4

    .prologue
    .line 2344719
    sget-object v0, LX/GMU;->c:LX/GMU;

    if-nez v0, :cond_1

    .line 2344720
    const-class v1, LX/GMU;

    monitor-enter v1

    .line 2344721
    :try_start_0
    sget-object v0, LX/GMU;->c:LX/GMU;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2344722
    if-eqz v2, :cond_0

    .line 2344723
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2344724
    new-instance p0, LX/GMU;

    invoke-static {v0}, LX/2U3;->a(LX/0QB;)LX/2U3;

    move-result-object v3

    check-cast v3, LX/2U3;

    invoke-direct {p0, v3}, LX/GMU;-><init>(LX/2U3;)V

    .line 2344725
    move-object v0, p0

    .line 2344726
    sput-object v0, LX/GMU;->c:LX/GMU;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2344727
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2344728
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2344729
    :cond_1
    sget-object v0, LX/GMU;->c:LX/GMU;

    return-object v0

    .line 2344730
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2344731
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;)Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;
    .locals 14

    .prologue
    .line 2344746
    new-instance v4, LX/0Pz;

    invoke-direct {v4}, LX/0Pz;-><init>()V

    .line 2344747
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 2344748
    if-nez p1, :cond_0

    .line 2344749
    const/4 v2, 0x0

    .line 2344750
    :goto_0
    move-object v0, v2

    .line 2344751
    new-instance v1, LX/A93;

    invoke-direct {v1}, LX/A93;-><init>()V

    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->e()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v1

    invoke-static {v1}, LX/A93;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;)LX/A93;

    move-result-object v1

    .line 2344752
    iput-object v0, v1, LX/A93;->i:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    .line 2344753
    move-object v0, v1

    .line 2344754
    invoke-virtual {v0}, LX/A93;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;

    move-result-object v0

    invoke-interface {p0, v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdminInfoModel;)V

    .line 2344755
    return-object p0

    .line 2344756
    :cond_0
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v2

    if-ge v3, v2, :cond_2

    .line 2344757
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel$EdgesModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationDataModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    .line 2344758
    invoke-static {v2}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v6

    invoke-virtual {v6}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 2344759
    invoke-static {v2}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2344760
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2, v3}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v4, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2344761
    :cond_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 2344762
    :cond_2
    invoke-virtual {v4}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2344763
    new-instance v3, LX/A9A;

    invoke-direct {v3}, LX/A9A;-><init>()V

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v4

    .line 2344764
    iput v4, v3, LX/A9A;->a:I

    .line 2344765
    move-object v3, v3

    .line 2344766
    iput-object v2, v3, LX/A9A;->b:LX/0Px;

    .line 2344767
    move-object v2, v3

    .line 2344768
    const/4 v12, 0x1

    const/4 v10, 0x0

    const/4 v13, 0x0

    .line 2344769
    new-instance v8, LX/186;

    const/16 v9, 0x80

    invoke-direct {v8, v9}, LX/186;-><init>(I)V

    .line 2344770
    iget-object v9, v2, LX/A9A;->b:LX/0Px;

    invoke-static {v8, v9}, LX/1k0;->a(LX/186;Ljava/util/List;)I

    move-result v9

    .line 2344771
    const/4 v11, 0x2

    invoke-virtual {v8, v11}, LX/186;->c(I)V

    .line 2344772
    iget v11, v2, LX/A9A;->a:I

    invoke-virtual {v8, v13, v11, v13}, LX/186;->a(III)V

    .line 2344773
    invoke-virtual {v8, v12, v9}, LX/186;->b(II)V

    .line 2344774
    invoke-virtual {v8}, LX/186;->d()I

    move-result v9

    .line 2344775
    invoke-virtual {v8, v9}, LX/186;->d(I)V

    .line 2344776
    invoke-virtual {v8}, LX/186;->e()[B

    move-result-object v8

    invoke-static {v8}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v9

    .line 2344777
    invoke-virtual {v9, v13}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2344778
    new-instance v8, LX/15i;

    move-object v11, v10

    move-object v13, v10

    invoke-direct/range {v8 .. v13}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2344779
    new-instance v9, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;

    invoke-direct {v9, v8}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BudgetRecommendationModel;-><init>(LX/15i;)V

    .line 2344780
    move-object v2, v9

    .line 2344781
    goto/16 :goto_0
.end method

.method public static a(Ljava/math/BigDecimal;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;
    .locals 2

    .prologue
    .line 2344732
    new-instance v0, LX/A9B;

    invoke-direct {v0}, LX/A9B;-><init>()V

    invoke-virtual {p0}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2344733
    iput-object v1, v0, LX/A9B;->c:Ljava/lang/String;

    .line 2344734
    move-object v0, v0

    .line 2344735
    invoke-static {p1}, LX/GG6;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)I

    move-result v1

    .line 2344736
    iput v1, v0, LX/A9B;->b:I

    .line 2344737
    move-object v0, v0

    .line 2344738
    invoke-static {p1}, LX/GMU;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;

    move-result-object v1

    invoke-virtual {v1}, Ljava/text/NumberFormat;->getCurrency()Ljava/util/Currency;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Currency;->getCurrencyCode()Ljava/lang/String;

    move-result-object v1

    .line 2344739
    iput-object v1, v0, LX/A9B;->a:Ljava/lang/String;

    .line 2344740
    move-object v0, v0

    .line 2344741
    invoke-virtual {v0}, LX/A9B;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    return-object v0
.end method

.method public static a(IJLjava/text/NumberFormat;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 2344660
    if-lez p0, :cond_0

    .line 2344661
    long-to-double v0, p1

    int-to-double v2, p0

    div-double/2addr v0, v2

    invoke-virtual {p3, v0, v1}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\s"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2344662
    :goto_0
    return-object v0

    .line 2344663
    :cond_0
    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    invoke-virtual {p3}, Ljava/text/NumberFormat;->getCurrency()Ljava/util/Currency;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Currency;->getDefaultFractionDigits()I

    move-result v2

    int-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    .line 2344664
    long-to-double v2, p1

    div-double v0, v2, v0

    invoke-virtual {p3, v0, v1}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\s"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;
    .locals 2

    .prologue
    .line 2344743
    if-nez p0, :cond_0

    .line 2344744
    new-instance v0, Ljava/math/BigDecimal;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    .line 2344745
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/math/BigDecimal;

    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a()Ljava/text/NumberFormat;
    .locals 2

    .prologue
    .line 2344699
    invoke-static {}, Ljava/text/NumberFormat;->getNumberInstance()Ljava/text/NumberFormat;

    move-result-object v0

    .line 2344700
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 2344701
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 2344702
    return-object v0
.end method

.method private static a(LX/8wL;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2344703
    move v0, v1

    :goto_0
    sget-object v2, LX/GMU;->a:[LX/8wL;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 2344704
    sget-object v2, LX/GMU;->a:[LX/8wL;

    aget-object v2, v2, v0

    if-ne p0, v2, :cond_1

    .line 2344705
    const/4 v1, 0x1

    .line 2344706
    :cond_0
    return v1

    .line 2344707
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 2

    .prologue
    .line 2344659
    invoke-static {p0}, LX/GMU;->b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v0

    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v1

    invoke-static {v1}, LX/GMU;->a(LX/8wL;)Z

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 2344665
    invoke-static {p0}, LX/GG6;->j(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2344666
    instance-of v1, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    if-eqz v1, :cond_0

    .line 2344667
    check-cast p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2344668
    iget-object v1, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v1, v1

    .line 2344669
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->o()Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    move-result-object v1

    sget-object v2, Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;->LIFETIME_BUDGET:Lcom/facebook/graphql/enums/GraphQLBoostedComponentBudgetType;

    if-ne v1, v2, :cond_1

    .line 2344670
    :cond_0
    :goto_0
    return v0

    .line 2344671
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 2344672
    :cond_2
    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b()LX/8wL;

    move-result-object v0

    invoke-static {v0}, LX/GMU;->a(LX/8wL;)Z

    move-result v0

    goto :goto_0
.end method

.method public static c(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/math/BigDecimal;
    .locals 1

    .prologue
    .line 2344673
    invoke-static {p0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->u()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-static {v0}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public static d(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/math/BigDecimal;
    .locals 1

    .prologue
    .line 2344674
    invoke-static {p0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->v()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-static {v0}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public static e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/math/BigDecimal;
    .locals 4

    .prologue
    .line 2344675
    invoke-static {p0}, LX/GMU;->d(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-interface {p0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->i()I

    move-result v1

    int-to-long v2, v1

    invoke-static {v2, v3}, Ljava/math/BigDecimal;->valueOf(J)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public static f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/text/NumberFormat;
    .locals 2

    .prologue
    .line 2344676
    invoke-static {p0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v1

    .line 2344677
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2344678
    :cond_0
    const/4 v0, 0x0

    .line 2344679
    :goto_0
    return-object v0

    .line 2344680
    :cond_1
    invoke-static {}, Ljava/text/NumberFormat;->getCurrencyInstance()Ljava/text/NumberFormat;

    move-result-object v0

    .line 2344681
    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setCurrency(Ljava/util/Currency;)V

    .line 2344682
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 2344683
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    goto :goto_0
.end method

.method public static g(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2344684
    invoke-static {p0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    .line 2344685
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v1, v2, :cond_0

    .line 2344686
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Currency;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2344687
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public static h(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)I
    .locals 2

    .prologue
    .line 2344688
    invoke-static {p0}, LX/GG6;->e(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;

    move-result-object v0

    .line 2344689
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$AdAccountModel;->m()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->j()I

    move-result v0

    goto :goto_0
.end method

.method public static k(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;
    .locals 1

    .prologue
    .line 2344690
    check-cast p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2344691
    iget-object v0, p0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->c:Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;

    move-object v0, v0

    .line 2344692
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentModel;->n()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;
    .locals 3

    .prologue
    .line 2344693
    invoke-static {p1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2344694
    invoke-static {p2}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/math/BigDecimal;->subtract(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2344695
    sget-object v1, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v1

    if-gtz v1, :cond_0

    .line 2344696
    iget-object v0, p0, LX/GMU;->b:LX/2U3;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    const-string v2, "The difference between the current total budget and the new budget must be greater than zero"

    invoke-virtual {v0, v1, v2}, LX/2U3;->a(Ljava/lang/Class;Ljava/lang/String;)V

    .line 2344697
    const/4 v0, 0x0

    .line 2344698
    :cond_0
    return-object v0
.end method
