.class public LX/FfR;
.super LX/FfQ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FfQ",
        "<",
        "Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/FfR;


# instance fields
.field private final a:LX/0ad;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/0ad;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2268487
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->APPS:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const v1, 0x7f082311

    invoke-direct {p0, p1, v0, v1}, LX/FfQ;-><init>(Landroid/content/res/Resources;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;I)V

    .line 2268488
    iput-object p2, p0, LX/FfR;->a:LX/0ad;

    .line 2268489
    return-void
.end method

.method public static a(LX/0QB;)LX/FfR;
    .locals 5

    .prologue
    .line 2268490
    sget-object v0, LX/FfR;->b:LX/FfR;

    if-nez v0, :cond_1

    .line 2268491
    const-class v1, LX/FfR;

    monitor-enter v1

    .line 2268492
    :try_start_0
    sget-object v0, LX/FfR;->b:LX/FfR;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2268493
    if-eqz v2, :cond_0

    .line 2268494
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2268495
    new-instance p0, LX/FfR;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-static {v0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v4

    check-cast v4, LX/0ad;

    invoke-direct {p0, v3, v4}, LX/FfR;-><init>(Landroid/content/res/Resources;LX/0ad;)V

    .line 2268496
    move-object v0, p0

    .line 2268497
    sput-object v0, LX/FfR;->b:LX/FfR;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2268498
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2268499
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2268500
    :cond_1
    sget-object v0, LX/FfR;->b:LX/FfR;

    return-object v0

    .line 2268501
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2268502
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()LX/Fdv;
    .locals 3

    .prologue
    .line 2268503
    iget-object v0, p0, LX/FfR;->a:LX/0ad;

    sget-short v1, LX/100;->ax:S

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, LX/0ad;->a(SZ)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/facebook/search/results/fragment/SearchResultsFragment;

    invoke-direct {v0}, Lcom/facebook/search/results/fragment/SearchResultsFragment;-><init>()V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;

    invoke-direct {v0}, Lcom/facebook/search/results/fragment/entities/SearchResultsEntitiesFragment;-><init>()V

    goto :goto_0
.end method
