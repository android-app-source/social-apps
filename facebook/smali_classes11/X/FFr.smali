.class public LX/FFr;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Lcom/facebook/messaging/model/messages/Message;

.field public c:LX/6km;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2217671
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2217672
    iput-object v0, p0, LX/FFr;->a:Ljava/lang/Long;

    .line 2217673
    iput-object v0, p0, LX/FFr;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 2217674
    iput-object v0, p0, LX/FFr;->c:LX/6km;

    .line 2217675
    iput-object v0, p0, LX/FFr;->d:Ljava/lang/String;

    return-void
.end method

.method public static b(J)D
    .locals 4

    .prologue
    .line 2217943
    long-to-double v0, p0

    const-wide v2, 0x4197d78400000000L    # 1.0E8

    div-double/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final b()Lcom/facebook/messaging/model/messages/Message;
    .locals 13

    .prologue
    .line 2217676
    iget-object v0, p0, LX/FFr;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v0, v0, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    .line 2217677
    iget-object v1, p0, LX/FFr;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v1, v1, Lcom/facebook/messaging/model/messages/Message;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    if-nez v1, :cond_0

    .line 2217678
    iget-object v0, p0, LX/FFr;->b:Lcom/facebook/messaging/model/messages/Message;

    .line 2217679
    :goto_0
    return-object v0

    .line 2217680
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v1

    .line 2217681
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->t()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v2

    .line 2217682
    new-instance v9, LX/5Xq;

    invoke-direct {v9}, LX/5Xq;-><init>()V

    .line 2217683
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->K()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2217684
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ag()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->b:Ljava/lang/String;

    .line 2217685
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ah()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->c:Ljava/lang/String;

    .line 2217686
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ai()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->d:Ljava/lang/String;

    .line 2217687
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->aj()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->e:Ljava/lang/String;

    .line 2217688
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cU()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$AmountModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->f:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$AmountModel;

    .line 2217689
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cV()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->g:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayCreationUpdateBubbleModel$AmountDueModel;

    .line 2217690
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cW()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceRetailItemModel$ApplicationModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->h:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceRetailItemModel$ApplicationModel;

    .line 2217691
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->am()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->i:Ljava/lang/String;

    .line 2217692
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->an()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->j:Ljava/lang/String;

    .line 2217693
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cX()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$BillAmountModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->k:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MfsBillPayAgentCashInUpdateBubbleModel$BillAmountModel;

    .line 2217694
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ap()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->l:Ljava/lang/String;

    .line 2217695
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cY()Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineBoardingPassBubbleModel$BoardingPassesModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->m:Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineBoardingPassBubbleModel$BoardingPassesModel;

    .line 2217696
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ar()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->n:Ljava/lang/String;

    .line 2217697
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->as()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->o:Ljava/lang/String;

    .line 2217698
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->at()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->p:Ljava/lang/String;

    .line 2217699
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->P()Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->q:Lcom/facebook/graphql/enums/GraphQLPagesPlatformNativeBookingStatus;

    .line 2217700
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cG_()Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->r:Lcom/facebook/graphql/enums/GraphQLMessengerCommerceBubbleType;

    .line 2217701
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cZ()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$BusinessMessageModel$BusinessItemsModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->s:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$BusinessMessageModel$BusinessItemsModel;

    .line 2217702
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->av()LX/0Px;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->t:LX/0Px;

    .line 2217703
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->d()LX/0Px;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->u:LX/0Px;

    .line 2217704
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->da()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$FundraiserToCharityFragmentModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->v:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$FundraiserToCharityFragmentModel;

    .line 2217705
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->L()Z

    move-result v10

    iput-boolean v10, v9, LX/5Xq;->w:Z

    .line 2217706
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ax()Z

    move-result v10

    iput-boolean v10, v9, LX/5Xq;->x:Z

    .line 2217707
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ay()Z

    move-result v10

    iput-boolean v10, v9, LX/5Xq;->y:Z

    .line 2217708
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->db()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderCancellationBubbleModel$CancelledItemsModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->z:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderCancellationBubbleModel$CancelledItemsModel;

    .line 2217709
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->x()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->A:Ljava/lang/String;

    .line 2217710
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->az()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->B:Ljava/lang/String;

    .line 2217711
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->aA()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->C:Ljava/lang/String;

    .line 2217712
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dc()Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$ClickActionModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->D:Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$ClickActionModel;

    .line 2217713
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dd()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->E:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;

    .line 2217714
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->de()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->F:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;

    .line 2217715
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->aC()J

    move-result-wide v11

    iput-wide v11, v9, LX/5Xq;->G:J

    .line 2217716
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->aD()LX/0Px;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->H:LX/0Px;

    .line 2217717
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->aE()Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->I:Lcom/facebook/graphql/enums/GraphQLConnectionStyle;

    .line 2217718
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->df()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$ConvenienceFeeModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->J:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$ConvenienceFeeModel;

    .line 2217719
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dg()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->K:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;

    .line 2217720
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dh()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLocationFragmentModel$CoordinatesModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->L:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLocationFragmentModel$CoordinatesModel;

    .line 2217721
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->di()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel$CoverPhotoModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->M:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel$CoverPhotoModel;

    .line 2217722
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->aJ()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->N:Ljava/lang/String;

    .line 2217723
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dj()Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->O:Lcom/facebook/messaging/business/common/calltoaction/graphql/PlatformCTAFragmentsModels$PlatformCallToActionModel;

    .line 2217724
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->A()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->P:Ljava/lang/String;

    .line 2217725
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->aK()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->Q:Ljava/lang/String;

    .line 2217726
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->aL()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->R:Ljava/lang/String;

    .line 2217727
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->aM()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->S:Ljava/lang/String;

    .line 2217728
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->aN()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->T:Ljava/lang/String;

    .line 2217729
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dk()Lcom/facebook/messaging/graphql/threads/business/RideThreadFragmentsModels$BusinessRideLocationModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->U:Lcom/facebook/messaging/graphql/threads/business/RideThreadFragmentsModels$BusinessRideLocationModel;

    .line 2217730
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->aP()LX/0Px;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->V:LX/0Px;

    .line 2217731
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->aQ()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->W:Ljava/lang/String;

    .line 2217732
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->aR()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->X:Ljava/lang/String;

    .line 2217733
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->aS()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->Y:Ljava/lang/String;

    .line 2217734
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->aT()D

    move-result-wide v11

    iput-wide v11, v9, LX/5Xq;->Z:D

    .line 2217735
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->aU()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aa:Ljava/lang/String;

    .line 2217736
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->aV()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->ab:Ljava/lang/String;

    .line 2217737
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->aW()J

    move-result-wide v11

    iput-wide v11, v9, LX/5Xq;->ac:J

    .line 2217738
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->B()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->ad:Ljava/lang/String;

    .line 2217739
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dl()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageEventFragmentModel$EventCoordinatesModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->ae:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageEventFragmentModel$EventCoordinatesModel;

    .line 2217740
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->aY()Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->af:Lcom/facebook/graphql/enums/GraphQLEventPrivacyType;

    .line 2217741
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dm()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageEventFragmentModel$EventPlaceModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->ag:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageEventFragmentModel$EventPlaceModel;

    .line 2217742
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ba()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->ah:Ljava/lang/String;

    .line 2217743
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bb()J

    move-result-wide v11

    iput-wide v11, v9, LX/5Xq;->ai:J

    .line 2217744
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bc()LX/0Px;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aj:LX/0Px;

    .line 2217745
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dn()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$FirstThreeMembersModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->ak:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerEventReminderFragmentModel$FirstThreeMembersModel;

    .line 2217746
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cC_()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->al:Ljava/lang/String;

    .line 2217747
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->be()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->am:Ljava/lang/String;

    .line 2217748
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->do()Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineFlightInfoModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->an:Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineFlightInfoModel;

    .line 2217749
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bg()LX/0Px;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->ao:LX/0Px;

    .line 2217750
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bh()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->ap:Ljava/lang/String;

    .line 2217751
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bi()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aq:Ljava/lang/String;

    .line 2217752
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bj()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->ar:Ljava/lang/String;

    .line 2217753
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bk()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->as:Ljava/lang/String;

    .line 2217754
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bl()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->at:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2217755
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dp()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$FundraiserToCharityFragmentModel$FundraiserDetailedProgressTextModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->au:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$FundraiserToCharityFragmentModel$FundraiserDetailedProgressTextModel;

    .line 2217756
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dq()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$FundraiserToCharityFragmentModel$FundraiserForCharityTextModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->av:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$FundraiserToCharityFragmentModel$FundraiserForCharityTextModel;

    .line 2217757
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dr()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aw:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PagesPlatformLeadGenInfoFragmentModel$GeocodeModel;

    .line 2217758
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ds()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel$GroupFriendMembersModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->ax:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel$GroupFriendMembersModel;

    .line 2217759
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dt()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel$GroupMembersModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->ay:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel$GroupMembersModel;

    .line 2217760
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->c()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->az:Ljava/lang/String;

    .line 2217761
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cD_()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aA:Ljava/lang/String;

    .line 2217762
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->du()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerInstantArticleFragmentModel$InstantArticleModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aB:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerInstantArticleFragmentModel$InstantArticleModel;

    .line 2217763
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bq()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aC:Ljava/lang/String;

    .line 2217764
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->br()Z

    move-result v10

    iput-boolean v10, v9, LX/5Xq;->aD:Z

    .line 2217765
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bs()Z

    move-result v10

    iput-boolean v10, v9, LX/5Xq;->aE:Z

    .line 2217766
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bt()Z

    move-result v10

    iput-boolean v10, v9, LX/5Xq;->aF:Z

    .line 2217767
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bu()Z

    move-result v10

    iput-boolean v10, v9, LX/5Xq;->aG:Z

    .line 2217768
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bv()Z

    move-result v10

    iput-boolean v10, v9, LX/5Xq;->aH:Z

    .line 2217769
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dv()Lcom/facebook/messaging/graphql/threads/business/AgentThreadFragmentsModels$AgentItemReceiptBubbleModel$ItemModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aI:Lcom/facebook/messaging/graphql/threads/business/AgentThreadFragmentsModels$AgentItemReceiptBubbleModel$ItemModel;

    .line 2217770
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bx()LX/0Px;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aJ:LX/0Px;

    .line 2217771
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dw()Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineConfirmationBubbleModel$ItineraryLegsModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aK:Lcom/facebook/messaging/graphql/threads/business/AirlineThreadFragmentsModels$AirlineConfirmationBubbleModel$ItineraryLegsModel;

    .line 2217772
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bz()Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aL:Lcom/facebook/graphql/enums/GraphQLLightweightEventStatus;

    .line 2217773
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bA()Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aM:Lcom/facebook/graphql/enums/GraphQLLightweightEventType;

    .line 2217774
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bB()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aN:Ljava/lang/String;

    .line 2217775
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bC()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aO:Ljava/lang/String;

    .line 2217776
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dx()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$LogoImageModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aP:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$LogoImageModel;

    .line 2217777
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dy()Lcom/facebook/messaging/graphql/threads/BotMessageQueriesModels$MovieImageFragmentModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aQ:Lcom/facebook/messaging/graphql/threads/BotMessageQueriesModels$MovieImageFragmentModel;

    .line 2217778
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bF()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aR:Ljava/lang/String;

    .line 2217779
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bG()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aS:Ljava/lang/String;

    .line 2217780
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cH_()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aT:Ljava/lang/String;

    .line 2217781
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bH()Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aU:Lcom/facebook/graphql/enums/GraphQLPagesPlatformMessageBubbleTypeEnum;

    .line 2217782
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bI()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aV:Ljava/lang/String;

    .line 2217783
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dz()Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$MessagingAttributionInfoModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aW:Lcom/facebook/messaging/graphql/threads/AppAttributionQueriesModels$MessagingAttributionInfoModel;

    .line 2217784
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dA()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aX:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;

    .line 2217785
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dB()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aY:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessengerRoomShareFragmentModel$MessengerThreadModel;

    .line 2217786
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dC()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MdotmeUserFragmentModel$MessengerUserModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->aZ:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MdotmeUserFragmentModel$MessengerUserModel;

    .line 2217787
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->O()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->ba:Ljava/lang/String;

    .line 2217788
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bM()LX/0Px;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bb:LX/0Px;

    .line 2217789
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bN()Lcom/facebook/graphql/enums/GraphQLMovieBotMovieListStyle;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bc:Lcom/facebook/graphql/enums/GraphQLMovieBotMovieListStyle;

    .line 2217790
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->j()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bd:Ljava/lang/String;

    .line 2217791
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dD()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$NativeComponentFlowBookingRequestFragmentModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->be:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$NativeComponentFlowBookingRequestFragmentModel;

    .line 2217792
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->k()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bf:Ljava/lang/String;

    .line 2217793
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bP()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bg:Ljava/lang/String;

    .line 2217794
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->C()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bh:Ljava/lang/String;

    .line 2217795
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cI_()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bi:Ljava/lang/String;

    .line 2217796
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dE()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$PageModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bj:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$PageModel;

    .line 2217797
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dF()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$PartnerLogoModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bk:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$PartnerLogoModel;

    .line 2217798
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bS()LX/0Px;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bl:LX/0Px;

    .line 2217799
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bT()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bm:Ljava/lang/String;

    .line 2217800
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bU()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bn:Ljava/lang/String;

    .line 2217801
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bV()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bo:Ljava/lang/String;

    .line 2217802
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bW()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bp:Ljava/lang/String;

    .line 2217803
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dG()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$PaymentModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bq:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$PaymentModel;

    .line 2217804
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bY()LX/0Px;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->br:LX/0Px;

    .line 2217805
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ad()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bs:Ljava/lang/String;

    .line 2217806
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->bZ()Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bt:Lcom/facebook/graphql/enums/GraphQLPaymentModulesClient;

    .line 2217807
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->Z()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bu:Ljava/lang/String;

    .line 2217808
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dH()Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentSnippetModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bv:Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentSnippetModel;

    .line 2217809
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dI()Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentTotalModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bw:Lcom/facebook/messaging/graphql/threads/PaymentPlatformBubbleQueriesModels$PaymentPlatformBubbleFragmentModel$PaymentTotalModel;

    .line 2217810
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cc()LX/0Px;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bx:LX/0Px;

    .line 2217811
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dJ()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLocationFragmentModel$PlaceModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->by:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLocationFragmentModel$PlaceModel;

    .line 2217812
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dK()Lcom/facebook/messaging/graphql/threads/InvoicesFragmentsModels$InvoicesFragmentModel$PlatformContextModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bz:Lcom/facebook/messaging/graphql/threads/InvoicesFragmentsModels$InvoicesFragmentModel$PlatformContextModel;

    .line 2217813
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dL()Lcom/facebook/messaging/graphql/threads/BotMessageQueriesModels$MovieDetailsFragmentModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bA:Lcom/facebook/messaging/graphql/threads/BotMessageQueriesModels$MovieDetailsFragmentModel;

    .line 2217814
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cg()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bB:Ljava/lang/String;

    .line 2217815
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ch()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bC:Ljava/lang/String;

    .line 2217816
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ci()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bD:Ljava/lang/String;

    .line 2217817
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dM()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$NativeComponentFlowBookingRequestFragmentModel$ProductItemModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bE:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$NativeComponentFlowBookingRequestFragmentModel$ProductItemModel;

    .line 2217818
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dN()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommercePromotionsModel$PromotionItemsModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bF:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommercePromotionsModel$PromotionItemsModel;

    .line 2217819
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dO()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceBaseOrderReceiptModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bG:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceBaseOrderReceiptModel;

    .line 2217820
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cy_()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bH:Ljava/lang/String;

    .line 2217821
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cJ_()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bI:Ljava/lang/String;

    .line 2217822
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dP()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerTransferFragmentModel$ReceiverModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bJ:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerTransferFragmentModel$ReceiverModel;

    .line 2217823
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cj()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bK:Ljava/lang/String;

    .line 2217824
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dQ()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerPaymentRequestFragmentModel$RequesteeModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bL:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerPaymentRequestFragmentModel$RequesteeModel;

    .line 2217825
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dR()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerPaymentRequestFragmentModel$RequesterModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bM:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$PeerToPeerPaymentRequestFragmentModel$RequesterModel;

    .line 2217826
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dS()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailCarrierModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bN:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailCarrierModel;

    .line 2217827
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dT()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderReceiptBubbleModel$RetailItemsModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bO:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceOrderReceiptBubbleModel$RetailItemsModel;

    .line 2217828
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dU()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailShipmentItemsModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bP:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$RetailShipmentItemsModel;

    .line 2217829
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ck()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bQ:Ljava/lang/String;

    .line 2217830
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dV()Lcom/facebook/messaging/graphql/threads/business/RideThreadFragmentsModels$BusinessRideReceiptFragmentModel$RideProviderModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bR:Lcom/facebook/messaging/graphql/threads/business/RideThreadFragmentsModels$BusinessRideReceiptFragmentModel$RideProviderModel;

    .line 2217831
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cm()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bS:Ljava/lang/String;

    .line 2217832
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cn()LX/0Px;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bT:LX/0Px;

    .line 2217833
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->l()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bU:Ljava/lang/String;

    .line 2217834
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->co()I

    move-result v10

    iput v10, v9, LX/5Xq;->bV:I

    .line 2217835
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dW()Lcom/facebook/messaging/graphql/threads/InvoicesFragmentsModels$InvoicesFragmentModel$SelectedTransactionPaymentOptionModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bW:Lcom/facebook/messaging/graphql/threads/InvoicesFragmentsModels$InvoicesFragmentModel$SelectedTransactionPaymentOptionModel;

    .line 2217836
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cq()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bX:Ljava/lang/String;

    .line 2217837
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dX()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$SenderModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bY:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$SenderModel;

    .line 2217838
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->S()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->bZ:Ljava/lang/String;

    .line 2217839
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->F()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->ca:Ljava/lang/String;

    .line 2217840
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cs()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cb:Ljava/lang/String;

    .line 2217841
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->G()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cc:Ljava/lang/String;

    .line 2217842
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dY()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cd:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel;

    .line 2217843
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->o()Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->ce:Lcom/facebook/graphql/enums/GraphQLShipmentTrackingEventType;

    .line 2217844
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->dZ()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$ShipmentTrackingEventsModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cf:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceShipmentBubbleModel$ShipmentTrackingEventsModel;

    .line 2217845
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ct()Z

    move-result v10

    iput-boolean v10, v9, LX/5Xq;->cg:Z

    .line 2217846
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cu()Z

    move-result v10

    iput-boolean v10, v9, LX/5Xq;->ch:Z

    .line 2217847
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cv()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->ci:Ljava/lang/String;

    .line 2217848
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cw()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cj:Ljava/lang/String;

    .line 2217849
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ea()Lcom/facebook/messaging/graphql/threads/business/RideThreadFragmentsModels$BusinessRideLocationModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->ck:Lcom/facebook/messaging/graphql/threads/business/RideThreadFragmentsModels$BusinessRideLocationModel;

    .line 2217850
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->m()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cl:Ljava/lang/String;

    .line 2217851
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->T()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cm:Ljava/lang/String;

    .line 2217852
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->U()J

    move-result-wide v11

    iput-wide v11, v9, LX/5Xq;->cn:J

    .line 2217853
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cy()J

    move-result-wide v11

    iput-wide v11, v9, LX/5Xq;->co:J

    .line 2217854
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cK_()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cp:Ljava/lang/String;

    .line 2217855
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cL_()Lcom/facebook/graphql/enums/GraphQLMessengerRetailItemStatus;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cq:Lcom/facebook/graphql/enums/GraphQLMessengerRetailItemStatus;

    .line 2217856
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->eb()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cr:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceLocationModel;

    .line 2217857
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ec()Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceProductSubscriptionBubbleModel$SubscribedItemModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cs:Lcom/facebook/messaging/graphql/threads/CommerceThreadFragmentsModels$CommerceProductSubscriptionBubbleModel$SubscribedItemModel;

    .line 2217858
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ed()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$NativeComponentFlowBookingRequestFragmentModel$SuggestedTimeRangeModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->ct:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$NativeComponentFlowBookingRequestFragmentModel$SuggestedTimeRangeModel;

    .line 2217859
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cN_()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cu:Ljava/lang/String;

    .line 2217860
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cz()LX/0Px;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cv:LX/0Px;

    .line 2217861
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->p()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cw:Ljava/lang/String;

    .line 2217862
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cA()J

    move-result-wide v11

    iput-wide v11, v9, LX/5Xq;->cx:J

    .line 2217863
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cB()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cy:Ljava/lang/String;

    .line 2217864
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cC()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cz:Ljava/lang/String;

    .line 2217865
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cO_()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cA:Ljava/lang/String;

    .line 2217866
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ee()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$TotalDueModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cB:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel$TotalDueModel;

    .line 2217867
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cE()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cC:Ljava/lang/String;

    .line 2217868
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cP_()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cD:Ljava/lang/String;

    .line 2217869
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->q()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cE:Ljava/lang/String;

    .line 2217870
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->I()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cF:Ljava/lang/String;

    .line 2217871
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cF()I

    move-result v10

    iput v10, v9, LX/5Xq;->cG:I

    .line 2217872
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ef()Lcom/facebook/messaging/graphql/threads/InvoicesFragmentsModels$InvoicesFragmentModel$TransactionPaymentModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cH:Lcom/facebook/messaging/graphql/threads/InvoicesFragmentsModels$InvoicesFragmentModel$TransactionPaymentModel;

    .line 2217873
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->eg()Lcom/facebook/messaging/graphql/threads/InvoicesFragmentsModels$InvoicesFragmentModel$TransactionProductsModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cI:Lcom/facebook/messaging/graphql/threads/InvoicesFragmentsModels$InvoicesFragmentModel$TransactionProductsModel;

    .line 2217874
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cI()Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cJ:Lcom/facebook/graphql/enums/GraphQLPageProductTransactionOrderStatusEnum;

    .line 2217875
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cJ()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cK:Ljava/lang/String;

    .line 2217876
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cK()I

    move-result v10

    iput v10, v9, LX/5Xq;->cL:I

    .line 2217877
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cL()I

    move-result v10

    iput v10, v9, LX/5Xq;->cM:I

    .line 2217878
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cM()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cN:Ljava/lang/String;

    .line 2217879
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cN()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cO:Ljava/lang/String;

    .line 2217880
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->eh()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$NativeComponentFlowBookingRequestFragmentModel$UserModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cP:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$NativeComponentFlowBookingRequestFragmentModel$UserModel;

    .line 2217881
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->X()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cQ:Ljava/lang/String;

    .line 2217882
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cO()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cR:Ljava/lang/String;

    .line 2217883
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cP()Ljava/lang/String;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cS:Ljava/lang/String;

    .line 2217884
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cQ()Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cT:Lcom/facebook/graphql/enums/GraphQLEventGuestStatus;

    .line 2217885
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->ei()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel$ViewerInviteToGroupModel;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cU:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$GroupFragmentModel$ViewerInviteToGroupModel;

    .line 2217886
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cS()Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cV:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2217887
    invoke-virtual {v2}, Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;->cT()Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    move-result-object v10

    iput-object v10, v9, LX/5Xq;->cW:Lcom/facebook/graphql/enums/GraphQLGroupVisibility;

    .line 2217888
    move-object v2, v9

    .line 2217889
    iget-object v3, p0, LX/FFr;->a:Ljava/lang/Long;

    if-eqz v3, :cond_1

    .line 2217890
    iget-object v3, p0, LX/FFr;->a:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 2217891
    iput-wide v4, v2, LX/5Xq;->ai:J

    .line 2217892
    :cond_1
    iget-object v3, p0, LX/FFr;->b:Lcom/facebook/messaging/model/messages/Message;

    iget-object v3, v3, Lcom/facebook/messaging/model/messages/Message;->n:Ljava/lang/String;

    .line 2217893
    iput-object v3, v2, LX/5Xq;->bg:Ljava/lang/String;

    .line 2217894
    iget-object v3, p0, LX/FFr;->c:LX/6km;

    if-eqz v3, :cond_2

    .line 2217895
    iget-object v3, p0, LX/FFr;->c:LX/6km;

    iget-object v3, v3, LX/6km;->latitude:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, LX/FFr;->b(J)D

    move-result-wide v4

    .line 2217896
    iget-object v3, p0, LX/FFr;->c:LX/6km;

    iget-object v3, v3, LX/6km;->longitude:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, LX/FFr;->b(J)D

    move-result-wide v6

    .line 2217897
    iget-object v3, p0, LX/FFr;->c:LX/6km;

    iget-object v3, v3, LX/6km;->timestampMilliseconds:Ljava/lang/Long;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2217898
    new-instance v8, LX/5XH;

    invoke-direct {v8}, LX/5XH;-><init>()V

    .line 2217899
    iput-wide v4, v8, LX/5XH;->e:D

    .line 2217900
    move-object v4, v8

    .line 2217901
    iput-wide v6, v4, LX/5XH;->f:D

    .line 2217902
    move-object v4, v4

    .line 2217903
    iput-object v3, v4, LX/5XH;->h:Ljava/lang/String;

    .line 2217904
    move-object v3, v4

    .line 2217905
    invoke-virtual {v3}, LX/5XH;->a()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;

    move-result-object v3

    .line 2217906
    iput-object v3, v2, LX/5Xq;->K:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$MessageLiveLocationFragmentModel$CoordinateModel;

    .line 2217907
    :cond_2
    iget-object v3, p0, LX/FFr;->d:Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 2217908
    iget-object v3, p0, LX/FFr;->d:Ljava/lang/String;

    .line 2217909
    iput-object v3, v2, LX/5Xq;->aO:Ljava/lang/String;

    .line 2217910
    :cond_3
    invoke-virtual {v2}, LX/5Xq;->a()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v2

    .line 2217911
    new-instance v3, LX/5Zg;

    invoke-direct {v3}, LX/5Zg;-><init>()V

    .line 2217912
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->a()LX/0Px;

    move-result-object v4

    iput-object v4, v3, LX/5Zg;->a:LX/0Px;

    .line 2217913
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->b()LX/0Px;

    move-result-object v4

    iput-object v4, v3, LX/5Zg;->b:LX/0Px;

    .line 2217914
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->c()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/5Zg;->c:Ljava/lang/String;

    .line 2217915
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->q()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonStoryAttachmentFieldsModel$DescriptionModel;

    move-result-object v4

    iput-object v4, v3, LX/5Zg;->d:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonStoryAttachmentFieldsModel$DescriptionModel;

    .line 2217916
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->r()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;

    move-result-object v4

    iput-object v4, v3, LX/5Zg;->e:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentMediaModel;

    .line 2217917
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->dg_()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/5Zg;->f:Ljava/lang/String;

    .line 2217918
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->s()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonStoryAttachmentFieldsModel$SourceModel;

    move-result-object v4

    iput-object v4, v3, LX/5Zg;->g:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$CommonStoryAttachmentFieldsModel$SourceModel;

    .line 2217919
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->j()LX/0Px;

    move-result-object v4

    iput-object v4, v3, LX/5Zg;->h:LX/0Px;

    .line 2217920
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->k()LX/0Px;

    move-result-object v4

    iput-object v4, v3, LX/5Zg;->i:LX/0Px;

    .line 2217921
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->l()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/5Zg;->j:Ljava/lang/String;

    .line 2217922
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->t()Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    move-result-object v4

    iput-object v4, v3, LX/5Zg;->k:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    .line 2217923
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->n()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/5Zg;->l:Ljava/lang/String;

    .line 2217924
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->o()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/5Zg;->m:Ljava/lang/String;

    .line 2217925
    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;->p()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, LX/5Zg;->n:Ljava/lang/String;

    .line 2217926
    move-object v1, v3

    .line 2217927
    iput-object v2, v1, LX/5Zg;->k:Lcom/facebook/messaging/graphql/threads/StoryAttachmentTargetModels$StoryAttachmentTargetFragmentModel;

    .line 2217928
    move-object v1, v1

    .line 2217929
    invoke-virtual {v1}, LX/5Zg;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v1

    .line 2217930
    new-instance v2, LX/5Zl;

    invoke-direct {v2}, LX/5Zl;-><init>()V

    .line 2217931
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->j()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieStoryAttachmentFieldsModel;

    move-result-object v3

    iput-object v3, v2, LX/5Zl;->a:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$GenieStoryAttachmentFieldsModel;

    .line 2217932
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->b()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, LX/5Zl;->b:Ljava/lang/String;

    .line 2217933
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->k()Z

    move-result v3

    iput-boolean v3, v2, LX/5Zl;->c:Z

    .line 2217934
    invoke-virtual {v0}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;->l()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    move-result-object v3

    iput-object v3, v2, LX/5Zl;->d:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    .line 2217935
    move-object v0, v2

    .line 2217936
    iput-object v1, v0, LX/5Zl;->d:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAAttachmentStoryFieldsModel;

    .line 2217937
    move-object v0, v0

    .line 2217938
    invoke-virtual {v0}, LX/5Zl;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    move-result-object v0

    .line 2217939
    invoke-static {}, Lcom/facebook/messaging/model/messages/Message;->newBuilder()LX/6f7;

    move-result-object v1

    iget-object v2, p0, LX/FFr;->b:Lcom/facebook/messaging/model/messages/Message;

    invoke-virtual {v1, v2}, LX/6f7;->a(Lcom/facebook/messaging/model/messages/Message;)LX/6f7;

    move-result-object v1

    .line 2217940
    iput-object v0, v1, LX/6f7;->G:Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$XMAModel;

    .line 2217941
    move-object v0, v1

    .line 2217942
    invoke-virtual {v0}, LX/6f7;->W()Lcom/facebook/messaging/model/messages/Message;

    move-result-object v0

    goto/16 :goto_0
.end method
