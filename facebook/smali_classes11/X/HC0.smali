.class public LX/HC0;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/facebook/content/SecureContextHelper;

.field public final c:LX/17Y;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/HQ9;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/facebook/content/SecureContextHelper;",
            "LX/17Y;",
            "LX/0Ot",
            "<",
            "LX/HQ9;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2438142
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2438143
    iput-object p1, p0, LX/HC0;->a:Landroid/content/Context;

    .line 2438144
    iput-object p2, p0, LX/HC0;->b:Lcom/facebook/content/SecureContextHelper;

    .line 2438145
    iput-object p3, p0, LX/HC0;->c:LX/17Y;

    .line 2438146
    iput-object p4, p0, LX/HC0;->d:LX/0Ot;

    .line 2438147
    iput-object p5, p0, LX/HC0;->e:LX/0Ot;

    .line 2438148
    return-void
.end method

.method public static b(LX/0QB;)LX/HC0;
    .locals 6

    .prologue
    .line 2438138
    new-instance v0, LX/HC0;

    const-class v1, Landroid/content/Context;

    invoke-interface {p0, v1}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    invoke-static {p0}, LX/0e0;->a(LX/0QB;)LX/0e0;

    move-result-object v2

    check-cast v2, Lcom/facebook/content/SecureContextHelper;

    invoke-static {p0}, LX/17X;->a(LX/0QB;)LX/17X;

    move-result-object v3

    check-cast v3, LX/17Y;

    const/16 v4, 0x2c04

    invoke-static {p0, v4}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0x12c4

    invoke-static {p0, v5}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, LX/HC0;-><init>(Landroid/content/Context;Lcom/facebook/content/SecureContextHelper;LX/17Y;LX/0Ot;LX/0Ot;)V

    .line 2438139
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/app/Activity;JLcom/facebook/graphql/enums/GraphQLPageActionType;LX/HBj;)V
    .locals 8

    .prologue
    .line 2438140
    iget-object v0, p0, LX/HC0;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/HQ9;

    const/4 v5, 0x0

    new-instance v6, LX/HBz;

    invoke-direct {v6, p0, p5, p4, p1}, LX/HBz;-><init>(LX/HC0;LX/HBj;Lcom/facebook/graphql/enums/GraphQLPageActionType;Landroid/app/Activity;)V

    move-wide v2, p2

    move-object v4, p4

    invoke-virtual/range {v1 .. v6}, LX/HQ9;->a(JLcom/facebook/graphql/enums/GraphQLPageActionType;Ljava/lang/String;LX/HBy;)V

    .line 2438141
    return-void
.end method
