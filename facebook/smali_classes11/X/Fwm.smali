.class public final LX/Fwm;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/timeline/protocol/ProfileIntroCardMutationModels$ProfileIntroCardBioMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:LX/4BY;

.field public final synthetic c:Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;Ljava/lang/String;LX/4BY;)V
    .locals 0

    .prologue
    .line 2303621
    iput-object p1, p0, LX/Fwm;->c:Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;

    iput-object p2, p0, LX/Fwm;->a:Ljava/lang/String;

    iput-object p3, p0, LX/Fwm;->b:LX/4BY;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2303622
    iget-object v0, p0, LX/Fwm;->c:Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0815ec

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2303623
    iget-object v0, p0, LX/Fwm;->c:Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2303624
    if-eqz v0, :cond_0

    .line 2303625
    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 2303626
    iget-object v2, p0, LX/Fwm;->b:LX/4BY;

    .line 2303627
    invoke-virtual {v2}, LX/4BY;->dismiss()V

    .line 2303628
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2303629
    :cond_0
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 2303630
    iget-object v0, p0, LX/Fwm;->c:Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 2303631
    if-eqz v0, :cond_1

    .line 2303632
    iget-object v1, p0, LX/Fwm;->c:Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2303633
    const-string v2, "saved_bio_result"

    iget-object v3, p0, LX/Fwm;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2303634
    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2303635
    iget-object v1, p0, LX/Fwm;->c:Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;

    iget-boolean v1, v1, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->v:Z

    if-eqz v1, :cond_2

    .line 2303636
    iget-object v1, p0, LX/Fwm;->c:Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;

    iget-object v2, p0, LX/Fwm;->b:LX/4BY;

    .line 2303637
    invoke-virtual {v2}, LX/4BY;->dismiss()V

    .line 2303638
    sget-object v3, LX/0ax;->bE:Ljava/lang/String;

    const/4 p0, 0x1

    new-array p0, p0, [Ljava/lang/Object;

    const/4 p1, 0x0

    iget-object v2, v1, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->e:LX/0Or;

    invoke-interface {v2}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v2

    aput-object v2, p0, p1

    invoke-static {v3, p0}, Lcom/facebook/common/stringformat/StringFormatUtil;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    .line 2303639
    iget-object v3, v1, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->c:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/17Y;

    invoke-interface {v3, v0, p0}, LX/17Y;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object p0

    .line 2303640
    if-eqz p0, :cond_0

    .line 2303641
    iget-object v3, v1, Lcom/facebook/timeline/header/intro/bio/edit/TimelineBioEditFragment;->d:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/content/SecureContextHelper;

    invoke-interface {v3, p0, v0}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2303642
    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2303643
    :cond_1
    :goto_0
    return-void

    .line 2303644
    :cond_2
    iget-object v2, p0, LX/Fwm;->b:LX/4BY;

    .line 2303645
    invoke-virtual {v2}, LX/4BY;->dismiss()V

    .line 2303646
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 2303647
    goto :goto_0
.end method
