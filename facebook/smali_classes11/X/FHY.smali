.class public final enum LX/FHY;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FHY;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FHY;

.field public static final enum FAILED:LX/FHY;

.field public static final enum IN_PROGRESS:LX/FHY;

.field public static final enum NOT_ALL_STARTED:LX/FHY;

.field public static final enum NO_MEDIA_ITEMS:LX/FHY;

.field public static final enum SUCCEEDED:LX/FHY;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2220609
    new-instance v0, LX/FHY;

    const-string v1, "NO_MEDIA_ITEMS"

    invoke-direct {v0, v1, v2}, LX/FHY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHY;->NO_MEDIA_ITEMS:LX/FHY;

    .line 2220610
    new-instance v0, LX/FHY;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v3}, LX/FHY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHY;->IN_PROGRESS:LX/FHY;

    .line 2220611
    new-instance v0, LX/FHY;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v4}, LX/FHY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHY;->FAILED:LX/FHY;

    .line 2220612
    new-instance v0, LX/FHY;

    const-string v1, "SUCCEEDED"

    invoke-direct {v0, v1, v5}, LX/FHY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHY;->SUCCEEDED:LX/FHY;

    .line 2220613
    new-instance v0, LX/FHY;

    const-string v1, "NOT_ALL_STARTED"

    invoke-direct {v0, v1, v6}, LX/FHY;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHY;->NOT_ALL_STARTED:LX/FHY;

    .line 2220614
    const/4 v0, 0x5

    new-array v0, v0, [LX/FHY;

    sget-object v1, LX/FHY;->NO_MEDIA_ITEMS:LX/FHY;

    aput-object v1, v0, v2

    sget-object v1, LX/FHY;->IN_PROGRESS:LX/FHY;

    aput-object v1, v0, v3

    sget-object v1, LX/FHY;->FAILED:LX/FHY;

    aput-object v1, v0, v4

    sget-object v1, LX/FHY;->SUCCEEDED:LX/FHY;

    aput-object v1, v0, v5

    sget-object v1, LX/FHY;->NOT_ALL_STARTED:LX/FHY;

    aput-object v1, v0, v6

    sput-object v0, LX/FHY;->$VALUES:[LX/FHY;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2220615
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FHY;
    .locals 1

    .prologue
    .line 2220616
    const-class v0, LX/FHY;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FHY;

    return-object v0
.end method

.method public static values()[LX/FHY;
    .locals 1

    .prologue
    .line 2220617
    sget-object v0, LX/FHY;->$VALUES:[LX/FHY;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FHY;

    return-object v0
.end method
