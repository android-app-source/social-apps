.class public final enum LX/FQv;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FQv;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FQv;

.field public static final enum CANCELED:LX/FQv;

.field public static final enum COMPLETED:LX/FQv;

.field public static final enum INITED:LX/FQv;

.field public static final enum PENDING:LX/FQv;

.field public static final enum UNKNOWN:LX/FQv;


# instance fields
.field private final mTextStringId:I
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2239867
    new-instance v0, LX/FQv;

    const-string v1, "INITED"

    const v2, 0x7f081e05

    invoke-direct {v0, v1, v3, v2}, LX/FQv;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/FQv;->INITED:LX/FQv;

    .line 2239868
    new-instance v0, LX/FQv;

    const-string v1, "PENDING"

    const v2, 0x7f081e05

    invoke-direct {v0, v1, v4, v2}, LX/FQv;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/FQv;->PENDING:LX/FQv;

    .line 2239869
    new-instance v0, LX/FQv;

    const-string v1, "COMPLETED"

    const v2, 0x7f081e06

    invoke-direct {v0, v1, v5, v2}, LX/FQv;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/FQv;->COMPLETED:LX/FQv;

    .line 2239870
    new-instance v0, LX/FQv;

    const-string v1, "CANCELED"

    const v2, 0x7f081e07

    invoke-direct {v0, v1, v6, v2}, LX/FQv;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/FQv;->CANCELED:LX/FQv;

    .line 2239871
    new-instance v0, LX/FQv;

    const-string v1, "UNKNOWN"

    const v2, 0x7f081e05

    invoke-direct {v0, v1, v7, v2}, LX/FQv;-><init>(Ljava/lang/String;II)V

    sput-object v0, LX/FQv;->UNKNOWN:LX/FQv;

    .line 2239872
    const/4 v0, 0x5

    new-array v0, v0, [LX/FQv;

    sget-object v1, LX/FQv;->INITED:LX/FQv;

    aput-object v1, v0, v3

    sget-object v1, LX/FQv;->PENDING:LX/FQv;

    aput-object v1, v0, v4

    sget-object v1, LX/FQv;->COMPLETED:LX/FQv;

    aput-object v1, v0, v5

    sget-object v1, LX/FQv;->CANCELED:LX/FQv;

    aput-object v1, v0, v6

    sget-object v1, LX/FQv;->UNKNOWN:LX/FQv;

    aput-object v1, v0, v7

    sput-object v0, LX/FQv;->$VALUES:[LX/FQv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3    # I
        .annotation build Landroid/support/annotation/StringRes;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 2239864
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2239865
    iput p3, p0, LX/FQv;->mTextStringId:I

    .line 2239866
    return-void
.end method

.method public static forValue(Ljava/lang/String;)LX/FQv;
    .locals 2

    .prologue
    .line 2239860
    const-class v0, LX/FQv;

    sget-object v1, LX/FQv;->UNKNOWN:LX/FQv;

    invoke-static {v0, p0, v1}, LX/47c;->a(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/Enum;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FQv;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)LX/FQv;
    .locals 1

    .prologue
    .line 2239863
    const-class v0, LX/FQv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FQv;

    return-object v0
.end method

.method public static values()[LX/FQv;
    .locals 1

    .prologue
    .line 2239862
    sget-object v0, LX/FQv;->$VALUES:[LX/FQv;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FQv;

    return-object v0
.end method


# virtual methods
.method public final getTextStringId()I
    .locals 1
    .annotation build Landroid/support/annotation/StringRes;
    .end annotation

    .prologue
    .line 2239861
    iget v0, p0, LX/FQv;->mTextStringId:I

    return v0
.end method
