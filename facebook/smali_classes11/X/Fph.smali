.class public LX/Fph;
.super Lcom/facebook/widget/CustomLinearLayout;
.source ""


# instance fields
.field private a:Landroid/graphics/drawable/Drawable;

.field private b:Landroid/graphics/drawable/Drawable;

.field private c:LX/Fpd;

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private f:LX/Fpb;

.field private g:LX/Fpg;

.field private h:LX/Fpf;

.field private i:Landroid/view/View;

.field private j:LX/Fpe;

.field private k:Z

.field private l:Z

.field private m:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 2291842
    invoke-direct {p0, p1}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;)V

    .line 2291843
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, LX/Fph;->a(Landroid/util/AttributeSet;I)V

    .line 2291844
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 2291839
    invoke-direct {p0, p1, p2}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 2291840
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, LX/Fph;->a(Landroid/util/AttributeSet;I)V

    .line 2291841
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 2291836
    invoke-direct {p0, p1, p2, p3}, Lcom/facebook/widget/CustomLinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 2291837
    invoke-direct {p0, p2, p3}, LX/Fph;->a(Landroid/util/AttributeSet;I)V

    .line 2291838
    return-void
.end method

.method private a(Landroid/graphics/drawable/Drawable;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 2291828
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2291829
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 2291830
    if-eq v0, v3, :cond_0

    .line 2291831
    :goto_0
    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, LX/Fph;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 2291832
    invoke-static {v1, p1}, LX/1r0;->b(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 2291833
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v3, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2291834
    return-object v1

    .line 2291835
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x3

    .line 2291820
    new-instance v0, Landroid/animation/LayoutTransition;

    invoke-direct {v0}, Landroid/animation/LayoutTransition;-><init>()V

    .line 2291821
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    .line 2291822
    invoke-virtual {v0, v3}, Landroid/animation/LayoutTransition;->disableTransitionType(I)V

    .line 2291823
    :goto_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1, v4, v5}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 2291824
    invoke-virtual {p0, v0}, LX/Fph;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 2291825
    return-void

    .line 2291826
    :cond_0
    invoke-virtual {v0, v3, v4, v5}, Landroid/animation/LayoutTransition;->setStartDelay(IJ)V

    .line 2291827
    invoke-virtual {v0, v3, v4, v5}, Landroid/animation/LayoutTransition;->setDuration(IJ)V

    goto :goto_0
.end method

.method private a(Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2291803
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, LX/Fph;->setOrientation(I)V

    .line 2291804
    const/4 v0, 0x0

    .line 2291805
    if-eqz p1, :cond_0

    .line 2291806
    invoke-virtual {p0}, LX/Fph;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/03r;->SideshowExpandableListView:[I

    invoke-virtual {v0, p1, v1, p2, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 2291807
    const/16 v0, 0x2

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, LX/Fph;->b:Landroid/graphics/drawable/Drawable;

    .line 2291808
    const/16 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 2291809
    const/16 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, LX/Fph;->a:Landroid/graphics/drawable/Drawable;

    .line 2291810
    const/16 v2, 0x3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, LX/Fph;->m:I

    .line 2291811
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 2291812
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, LX/Fph;->d:Ljava/util/List;

    .line 2291813
    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    iput-object v1, p0, LX/Fph;->e:Ljava/util/Stack;

    .line 2291814
    if-eqz v0, :cond_1

    .line 2291815
    invoke-direct {p0, v0}, LX/Fph;->a(Landroid/graphics/drawable/Drawable;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Fph;->addView(Landroid/view/View;)V

    .line 2291816
    :cond_1
    iget-object v0, p0, LX/Fph;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    .line 2291817
    iget-object v0, p0, LX/Fph;->a:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0}, LX/Fph;->a(Landroid/graphics/drawable/Drawable;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, LX/Fph;->addView(Landroid/view/View;)V

    .line 2291818
    :cond_2
    invoke-direct {p0}, LX/Fph;->a()V

    .line 2291819
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2291797
    iget-object v0, p0, LX/Fph;->j:LX/Fpe;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, LX/Fph;->l:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/Fph;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 2291798
    :goto_0
    iget-object v1, p0, LX/Fph;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    .line 2291799
    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, p1, v0}, LX/Fph;->addView(Landroid/view/View;I)V

    .line 2291800
    :goto_1
    return-void

    .line 2291801
    :cond_0
    invoke-virtual {p0}, LX/Fph;->getChildCount()I

    move-result v0

    goto :goto_0

    .line 2291802
    :cond_1
    invoke-virtual {p0, p1, v0}, LX/Fph;->addView(Landroid/view/View;I)V

    goto :goto_1
.end method

.method private b()V
    .locals 2

    .prologue
    .line 2291794
    iget-object v0, p0, LX/Fph;->j:LX/Fpe;

    invoke-virtual {p0}, LX/Fph;->getChildCount()I

    move-result v1

    invoke-virtual {p0, v0, v1}, LX/Fph;->addView(Landroid/view/View;I)V

    .line 2291795
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Fph;->l:Z

    .line 2291796
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 2291791
    iget-object v0, p0, LX/Fph;->j:LX/Fpe;

    invoke-virtual {p0, v0}, LX/Fph;->removeView(Landroid/view/View;)V

    .line 2291792
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Fph;->l:Z

    .line 2291793
    return-void
.end method

.method private c(I)V
    .locals 3

    .prologue
    .line 2291782
    iget-object v0, p0, LX/Fph;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 2291783
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-lt v1, p1, :cond_1

    .line 2291784
    iget-object v0, p0, LX/Fph;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2291785
    invoke-virtual {p0, v0}, LX/Fph;->removeView(Landroid/view/View;)V

    .line 2291786
    iget-object v2, p0, LX/Fph;->d:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2291787
    iget-object v0, p0, LX/Fph;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2291788
    iget-object v0, p0, LX/Fph;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, LX/Fph;->removeView(Landroid/view/View;)V

    .line 2291789
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 2291790
    :cond_1
    return-void
.end method

.method private d()V
    .locals 2

    .prologue
    .line 2291845
    iget-object v0, p0, LX/Fph;->b:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0}, LX/Fph;->a(Landroid/graphics/drawable/Drawable;)Landroid/view/View;

    move-result-object v0

    .line 2291846
    invoke-direct {p0, v0}, LX/Fph;->a(Landroid/view/View;)V

    .line 2291847
    iget-object v1, p0, LX/Fph;->e:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 2291848
    return-void
.end method

.method private d(I)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2291708
    move v1, v2

    :goto_0
    iget-object v0, p0, LX/Fph;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 2291709
    iget-object v0, p0, LX/Fph;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2291710
    iget-object v3, p0, LX/Fph;->f:LX/Fpb;

    invoke-virtual {v3, v1, v0, p0}, LX/Fpb;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 2291711
    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_1
    const-string v4, "Old view wasn\'t reused"

    invoke-static {v0, v4}, LX/0PB;->checkState(ZLjava/lang/Object;)V

    .line 2291712
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 2291713
    add-int/lit8 v3, p1, -0x1

    if-ne v1, v3, :cond_1

    .line 2291714
    iget v3, p0, LX/Fph;->m:I

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 2291715
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    move v0, v2

    .line 2291716
    goto :goto_1

    .line 2291717
    :cond_1
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    goto :goto_2

    .line 2291718
    :cond_2
    return-void
.end method

.method private e(I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2291727
    iget-object v0, p0, LX/Fph;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    move-object v1, v2

    :goto_0
    if-ge v0, p1, :cond_1

    .line 2291728
    iget-object v1, p0, LX/Fph;->f:LX/Fpb;

    invoke-virtual {v1, v0, v2, p0}, LX/Fpb;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 2291729
    iget-object v1, p0, LX/Fph;->f:LX/Fpb;

    invoke-virtual {v1, v0}, LX/Fpb;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .line 2291730
    new-instance v4, LX/Fpc;

    invoke-direct {v4, p0, v1}, LX/Fpc;-><init>(LX/Fph;Ljava/lang/Object;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2291731
    iget-object v1, p0, LX/Fph;->d:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2291732
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x1

    const/4 v5, -0x2

    invoke-direct {v1, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 2291733
    iget v4, p0, LX/Fph;->m:I

    iput v4, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 2291734
    if-eqz v0, :cond_0

    iget-object v4, p0, LX/Fph;->b:Landroid/graphics/drawable/Drawable;

    if-eqz v4, :cond_0

    .line 2291735
    invoke-direct {p0}, LX/Fph;->d()V

    .line 2291736
    :cond_0
    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 2291737
    invoke-direct {p0, v3}, LX/Fph;->a(Landroid/view/View;)V

    .line 2291738
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2291739
    :cond_1
    if-eqz v1, :cond_2

    .line 2291740
    iget v0, p0, LX/Fph;->m:I

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 2291741
    :cond_2
    return-void
.end method

.method public static e(LX/Fph;)V
    .locals 3

    .prologue
    .line 2291742
    iget-object v0, p0, LX/Fph;->f:LX/Fpb;

    invoke-static {v0}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2291743
    iget-object v0, p0, LX/Fph;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, LX/Fph;->f:LX/Fpb;

    invoke-virtual {v1}, LX/Fpb;->getCount()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 2291744
    invoke-direct {p0}, LX/Fph;->f()V

    .line 2291745
    :cond_0
    iget-object v0, p0, LX/Fph;->f:LX/Fpb;

    invoke-virtual {v0}, LX/Fpb;->getCount()I

    move-result v1

    .line 2291746
    iget-object v0, p0, LX/Fph;->f:LX/Fpb;

    invoke-virtual {v0}, LX/Fpb;->a()I

    move-result v2

    .line 2291747
    iget-boolean v0, p0, LX/Fph;->k:Z

    if-eqz v0, :cond_2

    move v0, v1

    .line 2291748
    :goto_0
    invoke-direct {p0, v0}, LX/Fph;->d(I)V

    .line 2291749
    invoke-direct {p0, v0}, LX/Fph;->e(I)V

    .line 2291750
    iget-object v0, p0, LX/Fph;->j:LX/Fpe;

    if-eqz v0, :cond_1

    .line 2291751
    if-le v1, v2, :cond_3

    iget-boolean v0, p0, LX/Fph;->l:Z

    if-nez v0, :cond_3

    .line 2291752
    invoke-direct {p0}, LX/Fph;->b()V

    .line 2291753
    :cond_1
    :goto_1
    return-void

    .line 2291754
    :cond_2
    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2291755
    invoke-direct {p0, v0}, LX/Fph;->c(I)V

    goto :goto_0

    .line 2291756
    :cond_3
    if-gt v1, v2, :cond_1

    iget-boolean v0, p0, LX/Fph;->l:Z

    if-eqz v0, :cond_1

    .line 2291757
    invoke-direct {p0}, LX/Fph;->c()V

    goto :goto_1
.end method

.method private f()V
    .locals 3

    .prologue
    .line 2291719
    iget-object v0, p0, LX/Fph;->f:LX/Fpb;

    invoke-virtual {v0}, LX/Fpb;->getCount()I

    move-result v1

    .line 2291720
    :cond_0
    :goto_0
    iget-object v0, p0, LX/Fph;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v1, :cond_1

    .line 2291721
    iget-object v0, p0, LX/Fph;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2291722
    invoke-virtual {p0, v0}, LX/Fph;->removeView(Landroid/view/View;)V

    .line 2291723
    iget-object v2, p0, LX/Fph;->d:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 2291724
    iget-object v0, p0, LX/Fph;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2291725
    iget-object v0, p0, LX/Fph;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, LX/Fph;->removeView(Landroid/view/View;)V

    goto :goto_0

    .line 2291726
    :cond_1
    return-void
.end method


# virtual methods
.method public getExpandButtonView()Landroid/view/View;
    .locals 1

    .prologue
    .line 2291758
    iget-object v0, p0, LX/Fph;->j:LX/Fpe;

    return-object v0
.end method

.method public getHeaderView()Landroid/view/View;
    .locals 1

    .prologue
    .line 2291759
    iget-object v0, p0, LX/Fph;->i:Landroid/view/View;

    return-object v0
.end method

.method public final onAttachedToWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x683aa798

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2291760
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onAttachedToWindow()V

    .line 2291761
    iget-object v1, p0, LX/Fph;->f:LX/Fpb;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/Fph;->c:LX/Fpd;

    if-nez v1, :cond_0

    .line 2291762
    new-instance v1, LX/Fpd;

    invoke-direct {v1, p0}, LX/Fpd;-><init>(LX/Fph;)V

    iput-object v1, p0, LX/Fph;->c:LX/Fpd;

    .line 2291763
    iget-object v1, p0, LX/Fph;->f:LX/Fpb;

    iget-object v2, p0, LX/Fph;->c:LX/Fpd;

    invoke-virtual {v1, v2}, LX/Fpb;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2291764
    invoke-static {p0}, LX/Fph;->e(LX/Fph;)V

    .line 2291765
    :cond_0
    const/16 v1, 0x2d

    const v2, 0x1e45455b

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public final onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/16 v0, 0x2c

    const v1, -0x468891d8

    invoke-static {v3, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2291766
    invoke-super {p0}, Lcom/facebook/widget/CustomLinearLayout;->onDetachedFromWindow()V

    .line 2291767
    iget-object v1, p0, LX/Fph;->f:LX/Fpb;

    if-eqz v1, :cond_0

    .line 2291768
    iget-object v1, p0, LX/Fph;->f:LX/Fpb;

    iget-object v2, p0, LX/Fph;->c:LX/Fpd;

    invoke-virtual {v1, v2}, LX/Fpb;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2291769
    const/4 v1, 0x0

    iput-object v1, p0, LX/Fph;->c:LX/Fpd;

    .line 2291770
    :cond_0
    const/16 v1, 0x2d

    const v2, -0x76ae0320

    invoke-static {v3, v1, v2, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method

.method public setAdapter(LX/Fpb;)V
    .locals 2

    .prologue
    .line 2291771
    iget-object v0, p0, LX/Fph;->f:LX/Fpb;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/Fph;->c:LX/Fpd;

    if-eqz v0, :cond_0

    .line 2291772
    iget-object v0, p0, LX/Fph;->f:LX/Fpb;

    iget-object v1, p0, LX/Fph;->c:LX/Fpd;

    invoke-virtual {v0, v1}, LX/Fpb;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2291773
    :cond_0
    iput-object p1, p0, LX/Fph;->f:LX/Fpb;

    .line 2291774
    new-instance v0, LX/Fpd;

    invoke-direct {v0, p0}, LX/Fpd;-><init>(LX/Fph;)V

    iput-object v0, p0, LX/Fph;->c:LX/Fpd;

    .line 2291775
    iget-object v0, p0, LX/Fph;->f:LX/Fpb;

    iget-object v1, p0, LX/Fph;->c:LX/Fpd;

    invoke-virtual {v0, v1}, LX/Fpb;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 2291776
    invoke-static {p0}, LX/Fph;->e(LX/Fph;)V

    .line 2291777
    return-void
.end method

.method public setOnExpansionListner(LX/Fpf;)V
    .locals 0

    .prologue
    .line 2291778
    iput-object p1, p0, LX/Fph;->h:LX/Fpf;

    .line 2291779
    return-void
.end method

.method public setOnItemClickedListener(LX/Fpg;)V
    .locals 0

    .prologue
    .line 2291780
    iput-object p1, p0, LX/Fph;->g:LX/Fpg;

    .line 2291781
    return-void
.end method
