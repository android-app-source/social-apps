.class public final LX/GcH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;)V
    .locals 0

    .prologue
    .line 2371600
    iput-object p1, p0, LX/GcH;->a:Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2371601
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2371602
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2371603
    iget-object v0, p0, LX/GcH;->a:Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2371604
    iget-object v0, p0, LX/GcH;->a:Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->f:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2371605
    iget-object v0, p0, LX/GcH;->a:Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->f:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2371606
    :cond_0
    :goto_0
    return-void

    .line 2371607
    :cond_1
    iget-object v0, p0, LX/GcH;->a:Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->f:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2371608
    iget-object v0, p0, LX/GcH;->a:Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordLoginFragment;->f:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
