.class public final LX/H3G;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static b(LX/15w;LX/186;)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2420693
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v0

    sget-object v2, LX/15z;->START_OBJECT:LX/15z;

    if-eq v0, v2, :cond_4

    .line 2420694
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2420695
    :goto_0
    return v1

    .line 2420696
    :cond_0
    invoke-virtual {p0}, LX/15w;->f()LX/15w;

    .line 2420697
    :cond_1
    :goto_1
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    move-result-object v3

    sget-object v4, LX/15z;->END_OBJECT:LX/15z;

    if-eq v3, v4, :cond_3

    .line 2420698
    invoke-virtual {p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v3

    .line 2420699
    invoke-virtual {p0}, LX/15w;->c()LX/15z;

    .line 2420700
    invoke-virtual {p0}, LX/15w;->g()LX/15z;

    move-result-object v4

    sget-object v5, LX/15z;->VALUE_NULL:LX/15z;

    if-eq v4, v5, :cond_1

    if-eqz v3, :cond_1

    .line 2420701
    const-string v4, "result_category"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2420702
    invoke-virtual {p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;->fromString(Ljava/lang/String;)Lcom/facebook/graphql/enums/GraphQLNearbySearchResultCategory;

    move-result-object v2

    invoke-virtual {p1, v2}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v2

    goto :goto_1

    .line 2420703
    :cond_2
    const-string v4, "topic_suggestions"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2420704
    invoke-static {p0, p1}, LX/H3F;->a(LX/15w;LX/186;)I

    move-result v0

    goto :goto_1

    .line 2420705
    :cond_3
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, LX/186;->c(I)V

    .line 2420706
    invoke-virtual {p1, v1, v2}, LX/186;->b(II)V

    .line 2420707
    const/4 v1, 0x1

    invoke-virtual {p1, v1, v0}, LX/186;->b(II)V

    .line 2420708
    invoke-virtual {p1}, LX/186;->d()I

    move-result v1

    goto :goto_0

    :cond_4
    move v0, v1

    move v2, v1

    goto :goto_1
.end method

.method public static b(LX/15i;ILX/0nX;LX/0my;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2420682
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2420683
    invoke-virtual {p0, p1, v1}, LX/15i;->g(II)I

    move-result v0

    .line 2420684
    if-eqz v0, :cond_0

    .line 2420685
    const-string v0, "result_category"

    invoke-virtual {p2, v0}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2420686
    invoke-virtual {p0, p1, v1}, LX/15i;->c(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2420687
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, LX/15i;->g(II)I

    move-result v0

    .line 2420688
    if-eqz v0, :cond_1

    .line 2420689
    const-string v1, "topic_suggestions"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2420690
    invoke-static {p0, v0, p2, p3}, LX/H3F;->a(LX/15i;ILX/0nX;LX/0my;)V

    .line 2420691
    :cond_1
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2420692
    return-void
.end method
