.class public LX/GPC;
.super LX/GP4;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GP4",
        "<",
        "LX/GQ9;",
        ">;"
    }
.end annotation


# static fields
.field private static final g:LX/0P1;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0P1",
            "<",
            "Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:LX/6yo;

.field public final d:LX/HXt;

.field public final e:LX/ADW;

.field public final f:Ljava/util/concurrent/ExecutorService;

.field public h:Lcom/facebook/resources/ui/FbEditText;

.field public i:Landroid/widget/ImageView;

.field private j:Lcom/facebook/resources/ui/FbTextView;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    .prologue
    .line 2348519
    sget-object v0, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->AMEX:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    const v1, 0x7f02160b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget-object v2, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->DISCOVER:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    const v3, 0x7f02160c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    sget-object v4, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->MASTER_CARD:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    const v5, 0x7f02160e

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    sget-object v6, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->VISA:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    const v7, 0x7f021611

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    sget-object v8, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->JCB:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    const v9, 0x7f02160d

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static/range {v0 .. v9}, LX/0P1;->of(Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)LX/0P1;

    move-result-object v0

    sput-object v0, LX/GPC;->g:LX/0P1;

    return-void
.end method

.method public constructor <init>(LX/6yo;LX/GQ9;LX/HXt;LX/ADW;Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;LX/GOy;LX/GNs;)V
    .locals 0
    .param p6    # Ljava/util/concurrent/ExecutorService;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2348513
    invoke-direct {p0, p2, p5, p7, p8}, LX/GP4;-><init>(LX/GQ7;Landroid/content/res/Resources;LX/GOy;LX/GNs;)V

    .line 2348514
    iput-object p1, p0, LX/GPC;->c:LX/6yo;

    .line 2348515
    iput-object p3, p0, LX/GPC;->d:LX/HXt;

    .line 2348516
    iput-object p4, p0, LX/GPC;->e:LX/ADW;

    .line 2348517
    iput-object p6, p0, LX/GPC;->f:Ljava/util/concurrent/ExecutorService;

    .line 2348518
    return-void
.end method

.method public static b(LX/0QB;)LX/GPC;
    .locals 9

    .prologue
    .line 2348507
    new-instance v0, LX/GPC;

    invoke-static {p0}, LX/6yo;->a(LX/0QB;)LX/6yo;

    move-result-object v1

    check-cast v1, LX/6yo;

    .line 2348508
    new-instance v2, LX/GQ9;

    invoke-direct {v2}, LX/GQ9;-><init>()V

    .line 2348509
    move-object v2, v2

    .line 2348510
    move-object v2, v2

    .line 2348511
    check-cast v2, LX/GQ9;

    invoke-static {p0}, LX/HXt;->a(LX/0QB;)LX/HXt;

    move-result-object v3

    check-cast v3, LX/HXt;

    invoke-static {p0}, LX/ADW;->a(LX/0QB;)LX/ADW;

    move-result-object v4

    check-cast v4, LX/ADW;

    invoke-static {p0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v5

    check-cast v5, Landroid/content/res/Resources;

    invoke-static {p0}, LX/0Tc;->a(LX/0QB;)LX/0Tf;

    move-result-object v6

    check-cast v6, Ljava/util/concurrent/ExecutorService;

    invoke-static {p0}, LX/GOy;->b(LX/0QB;)LX/GOy;

    move-result-object v7

    check-cast v7, LX/GOy;

    invoke-static {p0}, LX/GNs;->a(LX/0QB;)LX/GNs;

    move-result-object v8

    check-cast v8, LX/GNs;

    invoke-direct/range {v0 .. v8}, LX/GPC;-><init>(LX/6yo;LX/GQ9;LX/HXt;LX/ADW;Landroid/content/res/Resources;Ljava/util/concurrent/ExecutorService;LX/GOy;LX/GNs;)V

    .line 2348512
    return-object v0
.end method

.method private b(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;)V
    .locals 2

    .prologue
    .line 2348500
    sget-object v0, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->UNKNOWN:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    if-ne p1, v0, :cond_0

    .line 2348501
    const/4 v1, 0x0

    move v0, v1

    .line 2348502
    if-nez v0, :cond_1

    .line 2348503
    :cond_0
    iget-object v0, p0, LX/GPC;->i:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2348504
    :goto_0
    return-void

    .line 2348505
    :cond_1
    iget-object v0, p0, LX/GPC;->i:Landroid/widget/ImageView;

    const v1, 0x7f0201f4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2348506
    iget-object v0, p0, LX/GPC;->i:Landroid/widget/ImageView;

    new-instance v1, LX/GPB;

    invoke-direct {v1, p0}, LX/GPB;-><init>(LX/GPC;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 2348488
    const v0, 0x7f0d2480

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbEditText;

    iput-object v0, p0, LX/GPC;->h:Lcom/facebook/resources/ui/FbEditText;

    .line 2348489
    const v0, 0x7f0d2481

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/GPC;->i:Landroid/widget/ImageView;

    .line 2348490
    iget-object v0, p0, LX/GPC;->i:Landroid/widget/ImageView;

    const v1, 0x7f021610

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2348491
    sget-object v0, Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;->UNKNOWN:Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;

    invoke-direct {p0, v0}, LX/GPC;->b(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;)V

    .line 2348492
    const v0, 0x7f0d2482

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/resources/ui/FbTextView;

    iput-object v0, p0, LX/GPC;->j:Lcom/facebook/resources/ui/FbTextView;

    .line 2348493
    iget-object v0, p0, LX/GP4;->f:LX/GQ7;

    move-object v0, v0

    .line 2348494
    check-cast v0, LX/GQ9;

    new-instance v1, LX/GP8;

    invoke-direct {v1, p0}, LX/GP8;-><init>(LX/GPC;)V

    .line 2348495
    iput-object v1, v0, LX/GQ7;->a:LX/GP0;

    .line 2348496
    iget-object v0, p0, LX/GPC;->h:Lcom/facebook/resources/ui/FbEditText;

    new-instance v1, LX/GP9;

    invoke-direct {v1, p0}, LX/GP9;-><init>(LX/GPC;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 2348497
    iget-object v0, p0, LX/GPC;->h:Lcom/facebook/resources/ui/FbEditText;

    iget-object v1, p0, LX/GPC;->c:LX/6yo;

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2348498
    iget-object v0, p0, LX/GPC;->h:Lcom/facebook/resources/ui/FbEditText;

    new-instance v1, LX/GPA;

    invoke-direct {v1, p0}, LX/GPA;-><init>(LX/GPC;)V

    invoke-virtual {v0, v1}, Lcom/facebook/resources/ui/FbEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 2348499
    return-void
.end method

.method public final a(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;)V
    .locals 2

    .prologue
    .line 2348480
    iget-object v1, p0, LX/GPC;->i:Landroid/widget/ImageView;

    sget-object v0, LX/GPC;->g:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, LX/GPC;->g:LX/0P1;

    invoke-virtual {v0, p1}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2348481
    invoke-direct {p0, p1}, LX/GPC;->b(Lcom/facebook/payments/paymentmethods/model/FbPaymentCardType;)V

    .line 2348482
    return-void

    .line 2348483
    :cond_0
    const v0, 0x7f021610

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 2348487
    iget-object v0, p0, LX/GPC;->h:Lcom/facebook/resources/ui/FbEditText;

    invoke-virtual {v0}, Lcom/facebook/resources/ui/FbEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/GQ9;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2348486
    const-string v0, "card_number"

    return-object v0
.end method

.method public final c()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 2348485
    iget-object v0, p0, LX/GPC;->h:Lcom/facebook/resources/ui/FbEditText;

    return-object v0
.end method

.method public final d()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 2348484
    iget-object v0, p0, LX/GPC;->j:Lcom/facebook/resources/ui/FbTextView;

    return-object v0
.end method
