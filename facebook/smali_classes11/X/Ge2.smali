.class public final LX/Ge2;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

.field public final synthetic b:LX/Ge4;


# direct methods
.method public constructor <init>(LX/Ge4;Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;)V
    .locals 0

    .prologue
    .line 2374955
    iput-object p1, p0, LX/Ge2;->b:LX/Ge4;

    iput-object p2, p0, LX/Ge2;->a:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 2374956
    iget-object v0, p0, LX/Ge2;->b:LX/Ge4;

    iget-object v0, v0, LX/Ge4;->e:Ljava/util/Set;

    iget-object v1, p0, LX/Ge2;->a:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2374957
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 2374958
    check-cast p1, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;

    .line 2374959
    iget-object v0, p0, LX/Ge2;->b:LX/Ge4;

    iget-object v0, v0, LX/Ge4;->e:Ljava/util/Set;

    iget-object v1, p0, LX/Ge2;->a:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 2374960
    const/4 v1, 0x0

    .line 2374961
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;->a()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;->a()LX/0Px;

    move-result-object v0

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    move v0, v1

    .line 2374962
    :goto_0
    move v0, v0

    .line 2374963
    if-nez v0, :cond_1

    .line 2374964
    :goto_1
    return-void

    .line 2374965
    :cond_1
    iget-object v0, p0, LX/Ge2;->b:LX/Ge4;

    iget-object v0, v0, LX/Ge4;->h:LX/189;

    iget-object v1, p0, LX/Ge2;->a:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    .line 2374966
    new-instance v3, LX/0Pz;

    invoke-direct {v3}, LX/0Pz;-><init>()V

    .line 2374967
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 2374968
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->y()Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2374969
    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;->a()LX/0Px;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 2374970
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;->a()LX/0Px;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0Pz;->b(Ljava/lang/Iterable;)LX/0Pz;

    .line 2374971
    :cond_3
    invoke-static {v1}, LX/4Xn;->a(Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;)LX/4Xn;

    move-result-object v4

    new-instance v5, LX/4Xm;

    invoke-direct {v5}, LX/4Xm;-><init>()V

    invoke-virtual {v3}, LX/0Pz;->b()LX/0Px;

    move-result-object v3

    .line 2374972
    iput-object v3, v5, LX/4Xm;->b:LX/0Px;

    .line 2374973
    move-object v3, v5

    .line 2374974
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;->j()Lcom/facebook/graphql/model/GraphQLPageInfo;

    move-result-object v5

    .line 2374975
    iput-object v5, v3, LX/4Xm;->c:Lcom/facebook/graphql/model/GraphQLPageInfo;

    .line 2374976
    move-object v3, v3

    .line 2374977
    invoke-virtual {v3}, LX/4Xm;->a()Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;

    move-result-object v3

    .line 2374978
    iput-object v3, v4, LX/4Xn;->j:Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;

    .line 2374979
    move-object v3, v4

    .line 2374980
    iget-object v4, v0, LX/189;->i:LX/0SG;

    invoke-interface {v4}, LX/0SG;->a()J

    move-result-wide v5

    .line 2374981
    iput-wide v5, v3, LX/4Xn;->d:J

    .line 2374982
    move-object v3, v3

    .line 2374983
    invoke-virtual {v3}, LX/4Xn;->a()Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;

    move-result-object v3

    .line 2374984
    invoke-virtual {v1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeFeedUnit;->I_()I

    move-result v4

    invoke-static {v3, v4}, LX/1mc;->a(Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;I)V

    .line 2374985
    const/4 v4, 0x0

    invoke-static {v3, v4}, LX/0x1;->a(Lcom/facebook/graphql/model/FeedUnit;LX/0Rf;)V

    .line 2374986
    move-object v0, v3

    .line 2374987
    iget-object v1, p0, LX/Ge2;->b:LX/Ge4;

    iget-object v1, v1, LX/Ge4;->i:LX/0bH;

    new-instance v2, LX/1Ne;

    invoke-direct {v2, v0}, LX/1Ne;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    .line 2374988
    iget-object v1, p0, LX/Ge2;->b:LX/Ge4;

    iget-object v1, v1, LX/Ge4;->i:LX/0bH;

    new-instance v2, LX/1Nf;

    invoke-direct {v2, v0}, LX/1Nf;-><init>(Lcom/facebook/graphql/model/FeedUnit;)V

    invoke-virtual {v1, v2}, LX/0b4;->a(LX/0b7;)V

    goto :goto_1

    .line 2374989
    :cond_4
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeConnection;->a()LX/0Px;

    move-result-object v3

    invoke-virtual {v3}, LX/0Px;->size()I

    move-result v4

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_7

    invoke-virtual {v3, v2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;

    .line 2374990
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;->r()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;->r()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/graphql/model/GraphQLPage;->C()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPaginatedPagesYouMayLikeEdge;->r()Lcom/facebook/graphql/model/GraphQLPage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLPage;->Q()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    :cond_5
    move v0, v1

    .line 2374991
    goto/16 :goto_0

    .line 2374992
    :cond_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 2374993
    :cond_7
    const/4 v0, 0x1

    goto/16 :goto_0
.end method
