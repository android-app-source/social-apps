.class public final enum LX/FPZ;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FPZ;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FPZ;

.field public static final enum CELL:LX/FPZ;

.field public static final enum DISTANCE:LX/FPZ;

.field public static final enum FRIENDS_REVIEW:LX/FPZ;

.field public static final enum FRIENDS_WHO_VISITED:LX/FPZ;

.field public static final enum LIKES:LX/FPZ;

.field public static final enum OPEN_NOW:LX/FPZ;

.field public static final enum PHOTO_IN_COLLECTION:LX/FPZ;

.field public static final enum PRICE_RATING:LX/FPZ;

.field public static final enum REVIEW_RATING:LX/FPZ;

.field public static final enum SAVE_ACCESSORY:LX/FPZ;

.field public static final enum UNSAVE_ACCESSORY:LX/FPZ;


# instance fields
.field private final actionName:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2237546
    new-instance v0, LX/FPZ;

    const-string v1, "CELL"

    const-string v2, "cell"

    invoke-direct {v0, v1, v4, v2}, LX/FPZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FPZ;->CELL:LX/FPZ;

    .line 2237547
    new-instance v0, LX/FPZ;

    const-string v1, "REVIEW_RATING"

    const-string v2, "review_rating"

    invoke-direct {v0, v1, v5, v2}, LX/FPZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FPZ;->REVIEW_RATING:LX/FPZ;

    .line 2237548
    new-instance v0, LX/FPZ;

    const-string v1, "PRICE_RATING"

    const-string v2, "price_rating"

    invoke-direct {v0, v1, v6, v2}, LX/FPZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FPZ;->PRICE_RATING:LX/FPZ;

    .line 2237549
    new-instance v0, LX/FPZ;

    const-string v1, "DISTANCE"

    const-string v2, "distance"

    invoke-direct {v0, v1, v7, v2}, LX/FPZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FPZ;->DISTANCE:LX/FPZ;

    .line 2237550
    new-instance v0, LX/FPZ;

    const-string v1, "LIKES"

    const-string v2, "likes"

    invoke-direct {v0, v1, v8, v2}, LX/FPZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FPZ;->LIKES:LX/FPZ;

    .line 2237551
    new-instance v0, LX/FPZ;

    const-string v1, "OPEN_NOW"

    const/4 v2, 0x5

    const-string v3, "open_now"

    invoke-direct {v0, v1, v2, v3}, LX/FPZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FPZ;->OPEN_NOW:LX/FPZ;

    .line 2237552
    new-instance v0, LX/FPZ;

    const-string v1, "FRIENDS_WHO_VISITED"

    const/4 v2, 0x6

    const-string v3, "friends_who_visited"

    invoke-direct {v0, v1, v2, v3}, LX/FPZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FPZ;->FRIENDS_WHO_VISITED:LX/FPZ;

    .line 2237553
    new-instance v0, LX/FPZ;

    const-string v1, "FRIENDS_REVIEW"

    const/4 v2, 0x7

    const-string v3, "friends_review"

    invoke-direct {v0, v1, v2, v3}, LX/FPZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FPZ;->FRIENDS_REVIEW:LX/FPZ;

    .line 2237554
    new-instance v0, LX/FPZ;

    const-string v1, "PHOTO_IN_COLLECTION"

    const/16 v2, 0x8

    const-string v3, "photo_in_collection"

    invoke-direct {v0, v1, v2, v3}, LX/FPZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FPZ;->PHOTO_IN_COLLECTION:LX/FPZ;

    .line 2237555
    new-instance v0, LX/FPZ;

    const-string v1, "SAVE_ACCESSORY"

    const/16 v2, 0x9

    const-string v3, "save_accessory"

    invoke-direct {v0, v1, v2, v3}, LX/FPZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FPZ;->SAVE_ACCESSORY:LX/FPZ;

    .line 2237556
    new-instance v0, LX/FPZ;

    const-string v1, "UNSAVE_ACCESSORY"

    const/16 v2, 0xa

    const-string v3, "unsave_accessory"

    invoke-direct {v0, v1, v2, v3}, LX/FPZ;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, LX/FPZ;->UNSAVE_ACCESSORY:LX/FPZ;

    .line 2237557
    const/16 v0, 0xb

    new-array v0, v0, [LX/FPZ;

    sget-object v1, LX/FPZ;->CELL:LX/FPZ;

    aput-object v1, v0, v4

    sget-object v1, LX/FPZ;->REVIEW_RATING:LX/FPZ;

    aput-object v1, v0, v5

    sget-object v1, LX/FPZ;->PRICE_RATING:LX/FPZ;

    aput-object v1, v0, v6

    sget-object v1, LX/FPZ;->DISTANCE:LX/FPZ;

    aput-object v1, v0, v7

    sget-object v1, LX/FPZ;->LIKES:LX/FPZ;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, LX/FPZ;->OPEN_NOW:LX/FPZ;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/FPZ;->FRIENDS_WHO_VISITED:LX/FPZ;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/FPZ;->FRIENDS_REVIEW:LX/FPZ;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/FPZ;->PHOTO_IN_COLLECTION:LX/FPZ;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, LX/FPZ;->SAVE_ACCESSORY:LX/FPZ;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, LX/FPZ;->UNSAVE_ACCESSORY:LX/FPZ;

    aput-object v2, v0, v1

    sput-object v0, LX/FPZ;->$VALUES:[LX/FPZ;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2237558
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 2237559
    iput-object p3, p0, LX/FPZ;->actionName:Ljava/lang/String;

    .line 2237560
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FPZ;
    .locals 1

    .prologue
    .line 2237561
    const-class v0, LX/FPZ;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FPZ;

    return-object v0
.end method

.method public static values()[LX/FPZ;
    .locals 1

    .prologue
    .line 2237562
    sget-object v0, LX/FPZ;->$VALUES:[LX/FPZ;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FPZ;

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2237563
    iget-object v0, p0, LX/FPZ;->actionName:Ljava/lang/String;

    return-object v0
.end method
