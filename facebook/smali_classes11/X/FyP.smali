.class public final LX/FyP;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;


# direct methods
.method public constructor <init>(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)V
    .locals 0

    .prologue
    .line 2306536
    iput-object p1, p0, LX/FyP;->a:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    const v1, 0x74ec1b90

    invoke-static {v2, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2306537
    iget-object v1, p0, LX/FyP;->a:Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;

    const/4 v3, 0x0

    .line 2306538
    iget-object v4, v1, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v4}, LX/BQ1;->V()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, LX/1bf;->a(Ljava/lang/String;)LX/1bf;

    move-result-object v5

    .line 2306539
    invoke-static {v1}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->u(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2306540
    iget-object v4, v1, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v4}, LX/BQ1;->T()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderFocusedCoverPhotoFieldsModel;->b()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;

    move-result-object v6

    .line 2306541
    invoke-virtual {v6}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;->d()Ljava/lang/String;

    move-result-object v4

    .line 2306542
    invoke-virtual {v6}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;->c()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel$AlbumModel;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 2306543
    invoke-virtual {v6}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel;->c()Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel$AlbumModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$TimelineHeaderCoverPhotoFieldsModel$AlbumModel;->b()Ljava/lang/String;

    move-result-object v3

    .line 2306544
    :cond_0
    :goto_0
    iget-object v6, v1, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->G:LX/BP0;

    invoke-virtual {v6}, LX/5SB;->b()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2306545
    iget-object v6, v1, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->p:LX/0Ot;

    invoke-interface {v6}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, LX/Fxm;

    invoke-virtual {v1}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->getContext()Landroid/content/Context;

    move-result-object v7

    iget-object v8, v1, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->G:LX/BP0;

    .line 2306546
    iget-object v9, v1, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    .line 2306547
    iget-object p0, v9, LX/BQ1;->g:LX/BPw;

    .line 2306548
    iget-object v9, p0, LX/BPw;->e:Landroid/net/Uri;

    if-eqz v9, :cond_6

    const/4 v9, 0x1

    :goto_1
    move p0, v9

    .line 2306549
    move v9, p0

    .line 2306550
    if-nez v9, :cond_5

    iget-object v9, v1, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->H:LX/BQ1;

    invoke-virtual {v9}, LX/BQ1;->V()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_5

    const/4 v9, 0x1

    :goto_2
    move v9, v9

    .line 2306551
    new-instance p0, LX/FyT;

    invoke-direct {p0, v1, v4, v3, v5}, LX/FyT;-><init>(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;Ljava/lang/String;Ljava/lang/String;LX/1bf;)V

    .line 2306552
    new-instance p1, LX/6WS;

    invoke-direct {p1, v7}, LX/6WS;-><init>(Landroid/content/Context;)V

    .line 2306553
    invoke-virtual {p1}, LX/5OM;->c()LX/5OG;

    move-result-object v4

    .line 2306554
    const v3, 0x7f0815cc

    invoke-virtual {v4, v3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v3

    new-instance v5, LX/Fxk;

    invoke-direct {v5, v6}, LX/Fxk;-><init>(LX/Fxm;)V

    invoke-virtual {v3, v5}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2306555
    if-eqz v8, :cond_1

    .line 2306556
    iget-object v3, v8, LX/5SB;->e:LX/5SA;

    move-object v3, v3

    .line 2306557
    sget-object v5, LX/5SA;->USER:LX/5SA;

    if-ne v3, v5, :cond_1

    .line 2306558
    const v3, 0x7f0815d1

    invoke-virtual {v4, v3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v3

    new-instance v5, LX/Fxl;

    invoke-direct {v5, v6}, LX/Fxl;-><init>(LX/Fxm;)V

    invoke-virtual {v3, v5}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2306559
    :cond_1
    if-eqz v9, :cond_2

    .line 2306560
    const v3, 0x7f0815c1

    invoke-virtual {v4, v3}, LX/5OG;->a(I)LX/3Ai;

    move-result-object v3

    invoke-virtual {v3, p0}, LX/3Ai;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2306561
    :cond_2
    move-object v6, p1

    .line 2306562
    iget-object v7, v1, LX/Ban;->i:Lcom/facebook/caspian/ui/standardheader/StandardCoverPhotoView;

    invoke-virtual {v6, v7}, LX/0ht;->a(Landroid/view/View;)V

    .line 2306563
    :goto_3
    const v1, -0x2aed42c5

    invoke-static {v2, v2, v1, v0}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void

    .line 2306564
    :cond_3
    sget-object v6, LX/FyW;->COVER_PHOTO:LX/FyW;

    invoke-static {v1, v4, v3, v5, v6}, Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;->a$redex0(Lcom/facebook/timeline/header/ui/CaspianTimelineStandardHeader;Ljava/lang/String;Ljava/lang/String;LX/1bf;LX/FyW;)V

    goto :goto_3

    :cond_4
    move-object v4, v3

    goto/16 :goto_0

    :cond_5
    const/4 v9, 0x0

    goto :goto_2

    :cond_6
    const/4 v9, 0x0

    goto :goto_1
.end method
