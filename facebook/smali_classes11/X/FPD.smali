.class public final LX/FPD;
.super LX/FPC;
.source ""


# instance fields
.field public final synthetic a:LX/FPI;


# direct methods
.method public constructor <init>(LX/FPI;)V
    .locals 0

    .prologue
    .line 2236839
    iput-object p1, p0, LX/FPD;->a:LX/FPI;

    invoke-direct {p0}, LX/FPC;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;)V
    .locals 8

    .prologue
    .line 2236840
    invoke-super {p0, p1}, LX/FPC;->a(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;)V

    .line 2236841
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2236842
    iget-object v1, p0, LX/FPD;->a:LX/FPI;

    iget-object v1, v1, LX/FPI;->d:LX/FPU;

    const/4 v6, 0x0

    .line 2236843
    iget-object v2, v1, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-static {v2, v0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->d(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;I)Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    move-result-object v4

    .line 2236844
    if-nez v4, :cond_0

    .line 2236845
    :goto_0
    return-void

    .line 2236846
    :cond_0
    iget-object v2, v1, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    sget-object v5, LX/FPZ;->SAVE_ACCESSORY:LX/FPZ;

    move v3, v0

    move-object v7, v6

    invoke-static/range {v2 .. v7}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->a(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;ILcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;LX/FPZ;Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method public final a(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;I)V
    .locals 2

    .prologue
    .line 2236847
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2236848
    iget-object v1, p0, LX/FPD;->a:LX/FPI;

    iget-object v1, v1, LX/FPI;->d:LX/FPU;

    if-eqz v1, :cond_0

    .line 2236849
    iget-object v1, p0, LX/FPD;->a:LX/FPI;

    iget-object v1, v1, LX/FPI;->d:LX/FPU;

    invoke-virtual {v1, v0, p2}, LX/FPU;->a(II)V

    .line 2236850
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;)V
    .locals 8

    .prologue
    .line 2236851
    invoke-super {p0, p1}, LX/FPC;->b(Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;)V

    .line 2236852
    invoke-virtual {p1}, Lcom/facebook/nearby/v2/resultlist/views/SetSearchPlaceView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2236853
    iget-object v1, p0, LX/FPD;->a:LX/FPI;

    iget-object v1, v1, LX/FPI;->d:LX/FPU;

    const/4 v6, 0x0

    .line 2236854
    iget-object v2, v1, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    invoke-static {v2, v0}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->d(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;I)Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    move-result-object v4

    .line 2236855
    if-nez v4, :cond_0

    .line 2236856
    :goto_0
    return-void

    .line 2236857
    :cond_0
    iget-object v2, v1, LX/FPU;->a:Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;

    sget-object v5, LX/FPZ;->UNSAVE_ACCESSORY:LX/FPZ;

    move v3, v0

    move-object v7, v6

    invoke-static/range {v2 .. v7}, Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;->a(Lcom/facebook/nearby/v2/resultlist/NearbyPlacesV2ResultListFragment;ILcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;LX/FPZ;Ljava/lang/Integer;Ljava/lang/Integer;)V

    goto :goto_0
.end method
