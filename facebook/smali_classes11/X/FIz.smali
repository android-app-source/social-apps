.class public LX/FIz;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/0Zb;


# direct methods
.method public constructor <init>(LX/0Zb;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2222962
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2222963
    iput-object p1, p0, LX/FIz;->a:LX/0Zb;

    .line 2222964
    return-void
.end method

.method public static a(LX/0QB;)LX/FIz;
    .locals 4

    .prologue
    .line 2222965
    const-class v1, LX/FIz;

    monitor-enter v1

    .line 2222966
    :try_start_0
    sget-object v0, LX/FIz;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2222967
    sput-object v2, LX/FIz;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2222968
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2222969
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2222970
    new-instance p0, LX/FIz;

    invoke-static {v0}, LX/0ZY;->a(LX/0QB;)LX/0Zb;

    move-result-object v3

    check-cast v3, LX/0Zb;

    invoke-direct {p0, v3}, LX/FIz;-><init>(LX/0Zb;)V

    .line 2222971
    move-object v0, p0

    .line 2222972
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2222973
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FIz;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2222974
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2222975
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public static a(LX/6gC;Lcom/facebook/analytics/logger/HoneyClientEvent;)V
    .locals 2
    .param p0    # LX/6gC;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2222976
    const-string v0, "moments_invite"

    .line 2222977
    iput-object v0, p1, Lcom/facebook/analytics/logger/HoneyClientEvent;->c:Ljava/lang/String;

    .line 2222978
    if-eqz p0, :cond_1

    .line 2222979
    iget-object v0, p0, LX/6gC;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2222980
    if-eqz v0, :cond_0

    .line 2222981
    const-string v0, "share_xma_token"

    .line 2222982
    iget-object v1, p0, LX/6gC;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2222983
    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2222984
    :cond_0
    iget-object v0, p0, LX/6gC;->f:Ljava/lang/String;

    move-object v0, v0

    .line 2222985
    if-eqz v0, :cond_1

    .line 2222986
    const-string v0, "share_id"

    .line 2222987
    iget-object v1, p0, LX/6gC;->f:Ljava/lang/String;

    move-object v1, v1

    .line 2222988
    invoke-virtual {p1, v0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2222989
    :cond_1
    return-void
.end method
