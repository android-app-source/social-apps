.class public abstract LX/GME;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<D::",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">",
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;",
        "TD;>;"
    }
.end annotation


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field public b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TD;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2344135
    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LX/GME;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2344133
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2344134
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2344123
    const-string v0, "[-]?[0-9]*\\.?[0-9]+"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 2344124
    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 2344125
    invoke-static {p1}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    .line 2344126
    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 2344127
    :goto_0
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2344128
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v2

    .line 2344129
    invoke-static {v2}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 2344130
    invoke-virtual {v1, v4, v5}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v3

    .line 2344131
    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 2344132
    :cond_0
    return-object p0
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;)V
.end method

.method public bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2344122
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;

    invoke-virtual {p0, p1, p2}, LX/GME;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)V"
        }
    .end annotation

    .prologue
    .line 2344120
    iput-object p1, p0, LX/GME;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2344121
    return-void
.end method

.method public a(Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 12
    .param p2    # Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/16 v2, 0x8

    .line 2344082
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2344083
    invoke-virtual {p0}, LX/GME;->b()LX/0Px;

    move-result-object v0

    .line 2344084
    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2344085
    invoke-virtual {p1, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;->setVisibility(I)V

    .line 2344086
    if-eqz p2, :cond_0

    .line 2344087
    invoke-virtual {p2, v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setVisibility(I)V

    .line 2344088
    :cond_0
    iget-object v1, p1, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;->a:Lcom/facebook/widget/text/BetterTextView;

    move-object v1, v1

    .line 2344089
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesTargetingDescriptionView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {p0}, LX/GME;->d()Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    .line 2344090
    new-instance v7, Landroid/text/SpannableStringBuilder;

    invoke-direct {v7}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 2344091
    if-eqz v3, :cond_1

    .line 2344092
    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterTextView;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 2344093
    new-instance v5, Landroid/text/SpannableString;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f080b8d

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v8}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2344094
    new-instance v8, Landroid/text/style/TextAppearanceSpan;

    const v9, 0x7f0e0338

    invoke-direct {v8, v4, v9}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    const/4 v9, 0x0

    invoke-virtual {v5}, Landroid/text/SpannableString;->length()I

    move-result v10

    const/16 v11, 0x21

    invoke-virtual {v5, v8, v9, v10, v11}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2344095
    invoke-virtual {v7, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    const-string v8, "  "

    invoke-virtual {v5, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v5

    sget-object v8, LX/GME;->a:Ljava/lang/String;

    invoke-virtual {v5, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2344096
    :cond_1
    move v5, v6

    .line 2344097
    :goto_0
    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v4

    if-ge v5, v4, :cond_5

    .line 2344098
    if-lez v5, :cond_2

    .line 2344099
    sget-object v4, LX/GME;->a:Ljava/lang/String;

    invoke-virtual {v7, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2344100
    :cond_2
    invoke-virtual {v0, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetingDescriptionModel;

    .line 2344101
    new-instance v8, Landroid/text/SpannableString;

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetingDescriptionModel;->a()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 2344102
    new-instance v9, Landroid/text/style/TextAppearanceSpan;

    invoke-virtual {v1}, Lcom/facebook/widget/text/BetterTextView;->getContext()Landroid/content/Context;

    move-result-object v10

    const v11, 0x7f0e0338

    invoke-direct {v9, v10, v11}, Landroid/text/style/TextAppearanceSpan;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v8}, Landroid/text/SpannableString;->length()I

    move-result v10

    const/16 v11, 0x21

    invoke-virtual {v8, v9, v6, v10, v11}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 2344103
    invoke-virtual {v7, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2344104
    const-string v8, "  "

    invoke-virtual {v7, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2344105
    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetingDescriptionModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetingDescriptionModel$TargetingDescriptorsModel;

    move-result-object v8

    if-eqz v8, :cond_4

    .line 2344106
    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetingDescriptionModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetingDescriptionModel$TargetingDescriptorsModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetingDescriptionModel$TargetingDescriptorsModel;->a()LX/2uF;

    move-result-object v4

    invoke-virtual {v4}, LX/3Sa;->e()LX/3Sh;

    move-result-object v4

    .line 2344107
    invoke-interface {v4}, LX/2sN;->a()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 2344108
    invoke-interface {v4}, LX/2sN;->b()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    invoke-virtual {v9, v8, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v2}, LX/GME;->a(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 2344109
    :cond_3
    :goto_1
    invoke-interface {v4}, LX/2sN;->a()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 2344110
    invoke-interface {v4}, LX/2sN;->b()LX/1vs;

    move-result-object v8

    iget-object v9, v8, LX/1vs;->a:LX/15i;

    iget v8, v8, LX/1vs;->b:I

    const-string v10, ", "

    invoke-virtual {v7, v10}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v10

    invoke-virtual {v9, v8, v6}, LX/15i;->m(II)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v2}, LX/GME;->a(Ljava/lang/String;Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v10, v8}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_1

    .line 2344111
    :cond_4
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto/16 :goto_0

    .line 2344112
    :cond_5
    invoke-virtual {v1, v7}, Lcom/facebook/widget/text/BetterTextView;->setText(Ljava/lang/CharSequence;)V

    .line 2344113
    if-eqz p2, :cond_7

    iget-object v0, p0, LX/GME;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->ACTIVE:LX/GGB;

    if-eq v0, v1, :cond_6

    iget-object v0, p0, LX/GME;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->EXTENDABLE:LX/GGB;

    if-eq v0, v1, :cond_6

    iget-object v0, p0, LX/GME;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->PENDING:LX/GGB;

    if-eq v0, v1, :cond_6

    iget-object v0, p0, LX/GME;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v0

    sget-object v1, LX/GGB;->PAUSED:LX/GGB;

    if-ne v0, v1, :cond_7

    .line 2344114
    :cond_6
    new-instance v0, LX/GMC;

    invoke-direct {v0, p0}, LX/GMC;-><init>(LX/GME;)V

    .line 2344115
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2344116
    const/4 v2, 0x4

    invoke-virtual {v1, v2, v0}, LX/GCE;->a(ILX/GFR;)V

    .line 2344117
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080a9d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setCallToActionText(Ljava/lang/String;)V

    .line 2344118
    new-instance v0, LX/GMD;

    invoke-direct {v0, p0}, LX/GMD;-><init>(LX/GME;)V

    invoke-virtual {p2, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setCallToActionClickListener(Landroid/view/View$OnClickListener;)V

    .line 2344119
    :cond_7
    return-void
.end method

.method public abstract b()LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$TargetingDescriptionModel;",
            ">;"
        }
    .end annotation
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2344081
    const/4 v0, 0x0

    return-object v0
.end method
