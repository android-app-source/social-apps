.class public final LX/GJY;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field public final synthetic a:LX/GJb;


# direct methods
.method public constructor <init>(LX/GJb;)V
    .locals 0

    .prologue
    .line 2339153
    iput-object p1, p0, LX/GJY;->a:LX/GJb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2339154
    iget-object v0, p0, LX/GJY;->a:LX/GJb;

    invoke-static {v0, p2}, LX/GJb;->b(LX/GJb;Z)V

    .line 2339155
    if-eqz p2, :cond_0

    iget-object v0, p0, LX/GJY;->a:LX/GJb;

    iget-object v0, v0, LX/GJb;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GJY;->a:LX/GJb;

    iget-object v0, v0, LX/GJb;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2339156
    iget-object v0, p0, LX/GJY;->a:LX/GJb;

    iget-object v1, p0, LX/GJY;->a:LX/GJb;

    iget-object v1, v1, LX/GJb;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2339157
    iget-object v3, v1, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v1, v3

    .line 2339158
    iget-object v3, v1, Lcom/facebook/adinterfaces/model/CreativeAdModel;->i:Ljava/lang/String;

    move-object v1, v3

    .line 2339159
    invoke-static {v0, v2, v1}, LX/GJb;->b(LX/GJb;ZLjava/lang/String;)V

    .line 2339160
    :goto_0
    return-void

    .line 2339161
    :cond_0
    if-eqz p2, :cond_1

    .line 2339162
    iget-object v0, p0, LX/GJY;->a:LX/GJb;

    invoke-virtual {v0, v2, v3}, LX/GJb;->a(ZLjava/lang/String;)V

    goto :goto_0

    .line 2339163
    :cond_1
    iget-object v0, p0, LX/GJY;->a:LX/GJb;

    .line 2339164
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2339165
    new-instance v1, LX/GFc;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v3}, LX/GFc;-><init>(ZLjava/lang/String;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2339166
    iget-object v0, p0, LX/GJY;->a:LX/GJb;

    iget-object v0, v0, LX/GJb;->f:Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCreateOfferView;->b()V

    .line 2339167
    iget-object v0, p0, LX/GJY;->a:LX/GJb;

    iget-object v0, v0, LX/GJb;->e:Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;

    .line 2339168
    iget-object v1, v0, Lcom/facebook/adinterfaces/model/boostpost/AdInterfacesBoostedComponentDataModel;->b:Lcom/facebook/adinterfaces/model/CreativeAdModel;

    move-object v0, v1

    .line 2339169
    iput-object v3, v0, Lcom/facebook/adinterfaces/model/CreativeAdModel;->i:Ljava/lang/String;

    .line 2339170
    goto :goto_0
.end method
