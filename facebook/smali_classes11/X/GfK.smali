.class public final LX/GfK;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/GfL;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1f9;

.field public b:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public c:LX/25E;

.field public d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public e:I

.field public f:LX/2dx;

.field public g:Z

.field public h:Z

.field public final synthetic i:LX/GfL;


# direct methods
.method public constructor <init>(LX/GfL;)V
    .locals 1

    .prologue
    .line 2377160
    iput-object p1, p0, LX/GfK;->i:LX/GfL;

    .line 2377161
    move-object v0, p1

    .line 2377162
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2377163
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/GfK;->g:Z

    .line 2377164
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/GfK;->h:Z

    .line 2377165
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2377195
    const-string v0, "PageYouMayLikeSmallFormatComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2377166
    if-ne p0, p1, :cond_1

    .line 2377167
    :cond_0
    :goto_0
    return v0

    .line 2377168
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2377169
    goto :goto_0

    .line 2377170
    :cond_3
    check-cast p1, LX/GfK;

    .line 2377171
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2377172
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2377173
    if-eq v2, v3, :cond_0

    .line 2377174
    iget-object v2, p0, LX/GfK;->a:LX/1f9;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/GfK;->a:LX/1f9;

    iget-object v3, p1, LX/GfK;->a:LX/1f9;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2377175
    goto :goto_0

    .line 2377176
    :cond_5
    iget-object v2, p1, LX/GfK;->a:LX/1f9;

    if-nez v2, :cond_4

    .line 2377177
    :cond_6
    iget-object v2, p0, LX/GfK;->b:LX/1Pn;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/GfK;->b:LX/1Pn;

    iget-object v3, p1, LX/GfK;->b:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2377178
    goto :goto_0

    .line 2377179
    :cond_8
    iget-object v2, p1, LX/GfK;->b:LX/1Pn;

    if-nez v2, :cond_7

    .line 2377180
    :cond_9
    iget-object v2, p0, LX/GfK;->c:LX/25E;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/GfK;->c:LX/25E;

    iget-object v3, p1, LX/GfK;->c:LX/25E;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2377181
    goto :goto_0

    .line 2377182
    :cond_b
    iget-object v2, p1, LX/GfK;->c:LX/25E;

    if-nez v2, :cond_a

    .line 2377183
    :cond_c
    iget-object v2, p0, LX/GfK;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/GfK;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/GfK;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2377184
    goto :goto_0

    .line 2377185
    :cond_e
    iget-object v2, p1, LX/GfK;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_d

    .line 2377186
    :cond_f
    iget v2, p0, LX/GfK;->e:I

    iget v3, p1, LX/GfK;->e:I

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 2377187
    goto :goto_0

    .line 2377188
    :cond_10
    iget-object v2, p0, LX/GfK;->f:LX/2dx;

    if-eqz v2, :cond_12

    iget-object v2, p0, LX/GfK;->f:LX/2dx;

    iget-object v3, p1, LX/GfK;->f:LX/2dx;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_11
    move v0, v1

    .line 2377189
    goto :goto_0

    .line 2377190
    :cond_12
    iget-object v2, p1, LX/GfK;->f:LX/2dx;

    if-nez v2, :cond_11

    .line 2377191
    :cond_13
    iget-boolean v2, p0, LX/GfK;->g:Z

    iget-boolean v3, p1, LX/GfK;->g:Z

    if-eq v2, v3, :cond_14

    move v0, v1

    .line 2377192
    goto/16 :goto_0

    .line 2377193
    :cond_14
    iget-boolean v2, p0, LX/GfK;->h:Z

    iget-boolean v3, p1, LX/GfK;->h:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2377194
    goto/16 :goto_0
.end method
