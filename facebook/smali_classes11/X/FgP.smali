.class public LX/FgP;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/1ZF;

.field public final b:LX/63W;

.field public c:LX/5OO;

.field public d:LX/Fea;

.field public e:Lcom/facebook/widget/titlebar/TitleBarButtonSpec;


# direct methods
.method public constructor <init>(LX/1ZF;LX/Fea;)V
    .locals 1
    .param p1    # LX/1ZF;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/Fea;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2270260
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2270261
    iput-object p1, p0, LX/FgP;->a:LX/1ZF;

    .line 2270262
    iput-object p2, p0, LX/FgP;->d:LX/Fea;

    .line 2270263
    new-instance v0, LX/FgN;

    invoke-direct {v0, p0}, LX/FgN;-><init>(LX/FgP;)V

    iput-object v0, p0, LX/FgP;->b:LX/63W;

    .line 2270264
    return-void
.end method

.method public static a(LX/FgP;Landroid/content/Context;)LX/5OG;
    .locals 7

    .prologue
    .line 2270265
    new-instance v0, LX/5OG;

    invoke-direct {v0, p1}, LX/5OG;-><init>(Landroid/content/Context;)V

    .line 2270266
    iget-object v1, p0, LX/FgP;->d:LX/Fea;

    .line 2270267
    iget-object v2, v1, LX/Fea;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    invoke-static {v2}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->E(Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;)LX/FgM;

    move-result-object v2

    .line 2270268
    if-eqz v2, :cond_0

    .line 2270269
    iget-object v3, v1, LX/Fea;->a:Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    .line 2270270
    iget-object v1, v3, Lcom/facebook/search/results/fragment/SearchResultsBaseFragment;->i:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    move-object v1, v1

    .line 2270271
    move-object v3, v1

    .line 2270272
    invoke-virtual {v3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->h()LX/0P1;

    move-result-object v3

    const/4 v6, 0x1

    const/4 p0, 0x0

    .line 2270273
    sget-object v4, LX/7B4;->GROUP_COMMERCE:LX/7B4;

    invoke-virtual {v4}, LX/7B4;->name()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, LX/0P1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;

    .line 2270274
    if-eqz v4, :cond_0

    .line 2270275
    iget-boolean v5, v4, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->b:Z

    move v5, v5

    .line 2270276
    if-nez v5, :cond_1

    .line 2270277
    :cond_0
    :goto_0
    return-object v0

    .line 2270278
    :cond_1
    sget-object v5, LX/FgL;->ALL_POSTS:LX/FgL;

    invoke-virtual {v5}, LX/FgL;->ordinal()I

    move-result v5

    iget-object p1, v2, LX/FgM;->a:Landroid/content/res/Resources;

    const v1, 0x7f082328

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v5, p0, p1}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object p1

    .line 2270279
    const v5, 0x7f020984

    invoke-virtual {p1, v5}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2270280
    invoke-virtual {p1, v6}, LX/3Ai;->setCheckable(Z)Landroid/view/MenuItem;

    .line 2270281
    iget-boolean v5, v4, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->c:Z

    move v5, v5

    .line 2270282
    if-nez v5, :cond_2

    move v5, v6

    :goto_1
    invoke-virtual {p1, v5}, LX/3Ai;->setChecked(Z)Landroid/view/MenuItem;

    .line 2270283
    sget-object v5, LX/FgL;->SALE_POSTS:LX/FgL;

    invoke-virtual {v5}, LX/FgL;->ordinal()I

    move-result v5

    iget-object p1, v2, LX/FgM;->a:Landroid/content/res/Resources;

    const v1, 0x7f082329

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v5, p0, p1}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v5

    .line 2270284
    const p1, 0x7f0209f0

    invoke-virtual {v5, p1}, LX/3Ai;->setIcon(I)Landroid/view/MenuItem;

    .line 2270285
    invoke-virtual {v5, v6}, LX/3Ai;->setCheckable(Z)Landroid/view/MenuItem;

    .line 2270286
    iget-boolean v6, v4, Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;->c:Z

    move v6, v6

    .line 2270287
    invoke-virtual {v5, v6}, LX/3Ai;->setChecked(Z)Landroid/view/MenuItem;

    .line 2270288
    sget-object v5, LX/FgL;->AVAILABLE:LX/FgL;

    invoke-virtual {v5}, LX/FgL;->ordinal()I

    move-result v5

    iget-object v6, v2, LX/FgM;->a:Landroid/content/res/Resources;

    const p1, 0x7f08232b

    invoke-virtual {v6, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, p0, v6}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v5

    .line 2270289
    const-string v6, "available"

    invoke-static {v5, v6, v4}, LX/FgM;->a(LX/3Ai;Ljava/lang/String;Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;)V

    .line 2270290
    sget-object v5, LX/FgL;->SOLD:LX/FgL;

    invoke-virtual {v5}, LX/FgL;->ordinal()I

    move-result v5

    iget-object v6, v2, LX/FgM;->a:Landroid/content/res/Resources;

    const p1, 0x7f08232a

    invoke-virtual {v6, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, p0, v6}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v5

    .line 2270291
    const-string v6, "sold"

    invoke-static {v5, v6, v4}, LX/FgM;->a(LX/3Ai;Ljava/lang/String;Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;)V

    .line 2270292
    sget-object v5, LX/FgL;->ARCHIVED:LX/FgL;

    invoke-virtual {v5}, LX/FgL;->ordinal()I

    move-result v5

    iget-object v6, v2, LX/FgM;->a:Landroid/content/res/Resources;

    const p1, 0x7f08232c

    invoke-virtual {v6, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v5, p0, v6}, LX/5OG;->a(IILjava/lang/CharSequence;)LX/3Ai;

    move-result-object v5

    .line 2270293
    const-string v6, "expired"

    invoke-static {v5, v6, v4}, LX/FgM;->a(LX/3Ai;Ljava/lang/String;Lcom/facebook/search/api/GraphSearchQueryCommerceModifier;)V

    goto/16 :goto_0

    :cond_2
    move v5, p0

    .line 2270294
    goto :goto_1
.end method
