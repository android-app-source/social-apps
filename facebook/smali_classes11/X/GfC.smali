.class public LX/GfC;
.super LX/3mW;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pp;",
        ":",
        "LX/1Pq;",
        ":",
        "LX/1Pv;",
        ":",
        "LX/1Pk;",
        ":",
        "LX/1Pr;",
        ":",
        "LX/1Pt;",
        ":",
        "LX/1Pu;",
        ":",
        "LX/1Pj;",
        ">",
        "LX/3mW",
        "<",
        "Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final c:LX/Gf3;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Gf3",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final d:LX/Gep;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/Gep",
            "<TE;>;"
        }
    .end annotation
.end field

.field private final e:I

.field private final f:Z

.field private final g:LX/0dn;

.field private h:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field private i:LX/1Pp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field private j:LX/2dx;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Px;Lcom/facebook/feed/rows/core/props/FeedProps;LX/1Pp;LX/25M;LX/GfG;IZLX/0dn;LX/Gf3;LX/Gep;)V
    .locals 8
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/0Px;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # Lcom/facebook/feed/rows/core/props/FeedProps;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/1Pp;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/25M;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p6    # LX/GfG;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p7    # I
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Z
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Px",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;",
            ">;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;",
            ">;TE;",
            "LX/25M;",
            "LX/GfG;",
            "IZ",
            "LX/0dn;",
            "LX/Gf3;",
            "LX/Gep;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2376989
    move-object v5, p4

    check-cast v5, LX/1Pq;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p6

    move-object v6, p3

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, LX/3mW;-><init>(Landroid/content/Context;LX/0Px;LX/3mU;LX/1Pq;Lcom/facebook/feed/rows/core/props/FeedProps;LX/25M;)V

    .line 2376990
    iput-object p3, p0, LX/GfC;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2376991
    iput-object p4, p0, LX/GfC;->i:LX/1Pp;

    .line 2376992
    move-object/from16 v0, p10

    iput-object v0, p0, LX/GfC;->c:LX/Gf3;

    .line 2376993
    move-object/from16 v0, p11

    iput-object v0, p0, LX/GfC;->d:LX/Gep;

    .line 2376994
    iget-object v1, p0, LX/GfC;->i:LX/1Pp;

    check-cast v1, LX/1Pr;

    iget-object v2, p0, LX/GfC;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;

    invoke-static {v2}, LX/GfC;->a(Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;)LX/1KL;

    move-result-object v3

    iget-object v2, p0, LX/GfC;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2}, Lcom/facebook/feed/rows/core/props/FeedProps;->a()Lcom/facebook/flatbuffers/Flattenable;

    move-result-object v2

    check-cast v2, LX/0jW;

    invoke-interface {v1, v3, v2}, LX/1Pr;->a(LX/1KL;LX/0jW;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/2dx;

    iput-object v1, p0, LX/GfC;->j:LX/2dx;

    .line 2376995
    iput p7, p0, LX/GfC;->e:I

    .line 2376996
    move/from16 v0, p8

    iput-boolean v0, p0, LX/GfC;->f:Z

    .line 2376997
    move-object/from16 v0, p9

    iput-object v0, p0, LX/GfC;->g:LX/0dn;

    .line 2376998
    return-void
.end method

.method private static a(Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;)LX/1KL;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;",
            ")",
            "LX/1KL",
            "<",
            "Ljava/lang/String;",
            "LX/2dx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2376987
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, LX/GfC;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnit;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2376988
    new-instance v1, LX/GfB;

    invoke-direct {v1, v0}, LX/GfB;-><init>(Ljava/lang/String;)V

    return-object v1
.end method


# virtual methods
.method public final a(LX/1De;)LX/1X1;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1De;",
            ")",
            "LX/1X1",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 2376986
    const/4 v0, 0x0

    return-object v0
.end method

.method public final a(LX/1De;Ljava/lang/Object;)LX/1X1;
    .locals 9

    .prologue
    .line 2376945
    check-cast p2, Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    .line 2376946
    iget-object v0, p0, LX/GfC;->i:LX/1Pp;

    check-cast v0, LX/1Pt;

    invoke-interface {v0}, LX/1Pt;->m()LX/1f9;

    move-result-object v0

    .line 2376947
    iget-object v1, p0, LX/GfC;->g:LX/0dn;

    .line 2376948
    iget-object v3, v1, LX/0dn;->a:LX/0W3;

    sget-wide v5, LX/0X5;->ja:J

    invoke-interface {v3, v5, v6}, LX/0W4;->b(J)Z

    move-result v3

    .line 2376949
    const-string v4, "false"

    iget-object v5, v1, LX/0dn;->a:LX/0W3;

    sget-wide v7, LX/0X5;->jb:J

    invoke-interface {v5, v7, v8}, LX/0W4;->f(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 2376950
    iget-object v4, v1, LX/0dn;->a:LX/0W3;

    sget-wide v5, LX/0X5;->ja:J

    invoke-interface {v4, v5, v6}, LX/0W4;->i(J)V

    .line 2376951
    :cond_0
    move v1, v3

    .line 2376952
    if-eqz v1, :cond_2

    .line 2376953
    iget-object v1, p0, LX/GfC;->d:LX/Gep;

    const/4 v2, 0x0

    .line 2376954
    new-instance v3, LX/Geo;

    invoke-direct {v3, v1}, LX/Geo;-><init>(LX/Gep;)V

    .line 2376955
    iget-object v4, v1, LX/Gep;->b:LX/0Zi;

    invoke-virtual {v4}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, LX/Gen;

    .line 2376956
    if-nez v4, :cond_1

    .line 2376957
    new-instance v4, LX/Gen;

    invoke-direct {v4, v1}, LX/Gen;-><init>(LX/Gep;)V

    .line 2376958
    :cond_1
    invoke-static {v4, p1, v2, v2, v3}, LX/Gen;->a$redex0(LX/Gen;LX/1De;IILX/Geo;)V

    .line 2376959
    move-object v3, v4

    .line 2376960
    move-object v2, v3

    .line 2376961
    move-object v1, v2

    .line 2376962
    iget-object v2, v1, LX/Gen;->a:LX/Geo;

    iput-object p2, v2, LX/Geo;->a:Lcom/facebook/graphql/model/GraphQLPYMLWithLargeImageFeedUnitItem;

    .line 2376963
    iget-object v2, v1, LX/Gen;->e:Ljava/util/BitSet;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2376964
    move-object v1, v1

    .line 2376965
    iget-object v2, v1, LX/Gen;->a:LX/Geo;

    iput-object v0, v2, LX/Geo;->d:LX/1f9;

    .line 2376966
    iget-object v2, v1, LX/Gen;->e:Ljava/util/BitSet;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2376967
    move-object v0, v1

    .line 2376968
    iget-object v1, p0, LX/GfC;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2376969
    iget-object v2, v0, LX/Gen;->a:LX/Geo;

    iput-object v1, v2, LX/Geo;->b:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2376970
    iget-object v2, v0, LX/Gen;->e:Ljava/util/BitSet;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2376971
    move-object v0, v0

    .line 2376972
    iget v1, p0, LX/GfC;->e:I

    .line 2376973
    iget-object v2, v0, LX/Gen;->a:LX/Geo;

    iput v1, v2, LX/Geo;->c:I

    .line 2376974
    iget-object v2, v0, LX/Gen;->e:Ljava/util/BitSet;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2376975
    move-object v0, v0

    .line 2376976
    iget-object v1, p0, LX/GfC;->j:LX/2dx;

    .line 2376977
    iget-object v2, v0, LX/Gen;->a:LX/Geo;

    iput-object v1, v2, LX/Geo;->e:LX/2dx;

    .line 2376978
    iget-object v2, v0, LX/Gen;->e:Ljava/util/BitSet;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2376979
    move-object v1, v0

    .line 2376980
    iget-object v0, p0, LX/GfC;->i:LX/1Pp;

    check-cast v0, LX/1Pk;

    .line 2376981
    iget-object v2, v1, LX/Gen;->a:LX/Geo;

    iput-object v0, v2, LX/Geo;->f:LX/1Pk;

    .line 2376982
    iget-object v2, v1, LX/Gen;->e:Ljava/util/BitSet;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Ljava/util/BitSet;->set(I)V

    .line 2376983
    move-object v0, v1

    .line 2376984
    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    .line 2376985
    :goto_0
    return-object v0

    :cond_2
    iget-object v1, p0, LX/GfC;->c:LX/Gf3;

    invoke-virtual {v1, p1}, LX/Gf3;->c(LX/1De;)LX/Gf1;

    move-result-object v1

    invoke-virtual {v1, p2}, LX/Gf1;->a(LX/25E;)LX/Gf1;

    move-result-object v1

    iget-object v2, p0, LX/GfC;->h:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v1, v2}, LX/Gf1;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)LX/Gf1;

    move-result-object v1

    iget v2, p0, LX/GfC;->e:I

    invoke-virtual {v1, v2}, LX/Gf1;->h(I)LX/Gf1;

    move-result-object v1

    iget-object v2, p0, LX/GfC;->j:LX/2dx;

    invoke-virtual {v1, v2}, LX/Gf1;->a(LX/2dx;)LX/Gf1;

    move-result-object v1

    iget-boolean v2, p0, LX/GfC;->f:Z

    invoke-virtual {v1, v2}, LX/Gf1;->a(Z)LX/Gf1;

    move-result-object v1

    invoke-virtual {v1, v0}, LX/Gf1;->a(LX/1f9;)LX/Gf1;

    move-result-object v0

    iget-object v1, p0, LX/GfC;->i:LX/1Pp;

    invoke-virtual {v0, v1}, LX/Gf1;->a(LX/1Pp;)LX/Gf1;

    move-result-object v0

    invoke-virtual {v0}, LX/1X5;->d()LX/1X1;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V
    .locals 1

    .prologue
    .line 2376999
    invoke-super {p0, p1}, LX/3mW;->a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    .line 2377000
    iget-object v0, p0, LX/GfC;->j:LX/2dx;

    .line 2377001
    iput-object p1, v0, LX/2dx;->a:Landroid/view/View;

    .line 2377002
    return-void
.end method

.method public final b(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V
    .locals 2

    .prologue
    .line 2376941
    invoke-super {p0, p1}, LX/3mW;->b(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    .line 2376942
    iget-object v0, p0, LX/GfC;->j:LX/2dx;

    const/4 v1, 0x0

    .line 2376943
    iput-object v1, v0, LX/2dx;->a:Landroid/view/View;

    .line 2376944
    return-void
.end method

.method public final synthetic e(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2376940
    check-cast p1, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {p0, p1}, LX/GfC;->a(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    return-void
.end method

.method public final synthetic f(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 2376939
    check-cast p1, Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;

    invoke-virtual {p0, p1}, LX/GfC;->b(Lcom/facebook/widget/hscrollrecyclerview/HScrollRecyclerView;)V

    return-void
.end method

.method public final g()Z
    .locals 3

    .prologue
    .line 2376936
    iget-object v0, p0, LX/GfC;->g:LX/0dn;

    .line 2376937
    sget-wide v1, LX/0X5;->je:J

    invoke-static {v0, v1, v2}, LX/0dn;->a(LX/0dn;J)Z

    move-result v1

    move v0, v1

    .line 2376938
    return v0
.end method
