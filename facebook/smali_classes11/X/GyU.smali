.class public LX/GyU;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GyR;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0y3;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0y3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2410178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2410179
    iput-object p1, p0, LX/GyU;->a:Landroid/content/Context;

    .line 2410180
    iput-object p2, p0, LX/GyU;->b:LX/0y3;

    .line 2410181
    return-void
.end method

.method public static a(LX/0QB;)LX/GyU;
    .locals 3

    .prologue
    .line 2410175
    new-instance v2, LX/GyU;

    const-class v0, Landroid/content/Context;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {p0}, LX/0y3;->a(LX/0QB;)LX/0y3;

    move-result-object v1

    check-cast v1, LX/0y3;

    invoke-direct {v2, v0, v1}, LX/GyU;-><init>(Landroid/content/Context;LX/0y3;)V

    .line 2410176
    move-object v0, v2

    .line 2410177
    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 2410172
    sget-object v0, LX/GyT;->a:[I

    iget-object v1, p0, LX/GyU;->b:LX/0y3;

    invoke-virtual {v1}, LX/0y3;->b()LX/1rv;

    move-result-object v1

    iget-object v1, v1, LX/1rv;->a:LX/0yG;

    invoke-virtual {v1}, LX/0yG;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2410173
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2410174
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2410171
    iget-object v0, p0, LX/GyU;->a:Landroid/content/Context;

    const v1, 0x7f0837a7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2410167
    sget-object v0, LX/GyT;->a:[I

    iget-object v1, p0, LX/GyU;->b:LX/0y3;

    invoke-virtual {v1}, LX/0y3;->b()LX/1rv;

    move-result-object v1

    iget-object v1, v1, LX/1rv;->a:LX/0yG;

    invoke-virtual {v1}, LX/0yG;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2410168
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2410169
    :pswitch_0
    iget-object v0, p0, LX/GyU;->a:Landroid/content/Context;

    const v1, 0x7f08002e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 2410170
    :pswitch_1
    iget-object v0, p0, LX/GyU;->a:Landroid/content/Context;

    const v1, 0x7f08002f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2410166
    const-string v0, "location_services"

    return-object v0
.end method

.method public final e()Landroid/app/PendingIntent;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2410164
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.LOCATION_SOURCE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2410165
    iget-object v1, p0, LX/GyU;->a:Landroid/content/Context;

    invoke-static {v1, v2, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method
