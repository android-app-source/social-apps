.class public final LX/Gba;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0TF;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/0TF",
        "<",
        "Lcom/facebook/fbservice/service/OperationResult;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)V
    .locals 0

    .prologue
    .line 2370036
    iput-object p1, p0, LX/Gba;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 14

    .prologue
    .line 2370012
    iget-object v0, p0, LX/Gba;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-static {v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->q(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)LX/Gb4;

    move-result-object v11

    .line 2370013
    if-nez v11, :cond_0

    .line 2370014
    :goto_0
    return-void

    .line 2370015
    :cond_0
    invoke-virtual {p1}, Lcom/facebook/fbservice/service/OperationResult;->getResultData()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "result"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2370016
    const/4 v0, 0x0

    .line 2370017
    iget-object v1, p0, LX/Gba;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-virtual {v1}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "operation_type"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2370018
    const-string v2, "switch_to_dbl_with_pin"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2370019
    const/4 v0, 0x1

    move v12, v0

    .line 2370020
    :goto_1
    if-eqz v10, :cond_2

    .line 2370021
    iget-object v13, p0, LX/Gba;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    new-instance v0, Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v1, v10, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUserId:Ljava/lang/String;

    iget v2, v10, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mTime:I

    iget-object v3, v10, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mName:Ljava/lang/String;

    iget-object v4, v10, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mFullName:Ljava/lang/String;

    iget-object v5, v10, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mUsername:Ljava/lang/String;

    iget-object v6, p0, LX/Gba;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    iget-object v6, v6, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->C:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    iget-object v6, v6, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mPicUrl:Ljava/lang/String;

    iget-object v7, v10, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mNonce:Ljava/lang/String;

    iget-object v8, v10, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mIsPinSet:Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    iget-object v9, v10, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mAlternativeAccessToken:Ljava/lang/String;

    invoke-direct/range {v0 .. v9}, Lcom/facebook/auth/credentials/DBLFacebookCredentials;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 2370022
    iput-object v0, v13, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->C:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    .line 2370023
    iget-object v0, p0, LX/Gba;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    iget-object v0, v0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->r:LX/10M;

    iget-object v1, p0, LX/Gba;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    iget-object v1, v1, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->C:Lcom/facebook/auth/credentials/DBLFacebookCredentials;

    invoke-virtual {v0, v1}, LX/10M;->a(Lcom/facebook/auth/credentials/DBLFacebookCredentials;)V

    .line 2370024
    iget-object v0, p0, LX/Gba;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-static {v0, v12}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->c(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;Z)V

    .line 2370025
    iget-object v0, p0, LX/Gba;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    const-string v1, ""

    .line 2370026
    iput-object v1, v0, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->y:Ljava/lang/String;

    .line 2370027
    invoke-interface {v11}, LX/Gb4;->d()V

    .line 2370028
    if-eqz v12, :cond_1

    .line 2370029
    iget-object v0, p0, LX/Gba;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-static {v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->o(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;)V

    goto :goto_0

    .line 2370030
    :cond_1
    iget-object v0, p0, LX/Gba;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    iget-object v1, v10, Lcom/facebook/auth/credentials/DBLFacebookCredentials;->mIsPinSet:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 2370031
    invoke-static {v0, v1}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->b$redex0(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;Z)V

    .line 2370032
    iget-object v0, p0, LX/Gba;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-virtual {v0}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->a()V

    goto :goto_0

    .line 2370033
    :cond_2
    instance-of v0, v11, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;

    if-eqz v0, :cond_3

    move-object v0, v11

    .line 2370034
    check-cast v0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;

    const v1, 0x7f083473

    invoke-virtual {v0, v1}, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->d(I)V

    .line 2370035
    :cond_3
    iget-object v0, p0, LX/Gba;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    const v1, 0x7f08348f

    invoke-virtual {v0, v1}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v11, v0}, LX/Gb4;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    move v12, v0

    goto :goto_1
.end method


# virtual methods
.method public final onFailure(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 2370037
    iget-object v0, p0, LX/Gba;->a:Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;

    invoke-static {v0, p1}, Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;->b(Lcom/facebook/devicebasedlogin/settings/DBLPinSettingsActivity;Ljava/lang/Throwable;)V

    .line 2370038
    return-void
.end method

.method public final synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2370011
    check-cast p1, Lcom/facebook/fbservice/service/OperationResult;

    invoke-direct {p0, p1}, LX/Gba;->a(Lcom/facebook/fbservice/service/OperationResult;)V

    return-void
.end method
