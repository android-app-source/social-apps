.class public final LX/Eze;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/5P5;


# instance fields
.field public final synthetic a:Lcom/facebook/friends/model/PersonYouMayKnow;

.field public final synthetic b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

.field public final synthetic d:LX/Ezh;


# direct methods
.method public constructor <init>(LX/Ezh;Lcom/facebook/friends/model/PersonYouMayKnow;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V
    .locals 0

    .prologue
    .line 2187532
    iput-object p1, p0, LX/Eze;->d:LX/Ezh;

    iput-object p2, p0, LX/Eze;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    iput-object p3, p0, LX/Eze;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    iput-object p4, p0, LX/Eze;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 2187533
    return-void
.end method

.method public final b()V
    .locals 11

    .prologue
    .line 2187534
    iget-object v0, p0, LX/Eze;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    iget-object v1, p0, LX/Eze;->b:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    invoke-virtual {v0, v1}, Lcom/facebook/friends/model/PersonYouMayKnow;->b(Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;)V

    .line 2187535
    iget-object v0, p0, LX/Eze;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    iget-object v1, p0, LX/Eze;->c:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2187536
    iput-object v1, v0, Lcom/facebook/friends/model/PersonYouMayKnow;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2187537
    iget-object v0, p0, LX/Eze;->d:LX/Ezh;

    iget-object v0, v0, LX/Ezh;->c:LX/Ezl;

    iget-object v1, p0, LX/Eze;->a:Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v1}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v2

    .line 2187538
    iget-object v4, v0, LX/Ezl;->a:LX/Ezn;

    .line 2187539
    iget-object v5, v4, LX/Ezn;->a:Ljava/util/List;

    move-object v6, v5

    .line 2187540
    const/4 v4, 0x0

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    move v5, v4

    :goto_0
    if-ge v5, v7, :cond_2

    .line 2187541
    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/facebook/friends/model/PersonYouMayKnow;

    invoke-virtual {v4}, Lcom/facebook/friends/model/PersonYouMayKnow;->a()J

    move-result-wide v8

    cmp-long v4, v8, v2

    if-nez v4, :cond_1

    .line 2187542
    add-int/lit8 v4, v5, 0x1

    .line 2187543
    :goto_1
    move v0, v4

    .line 2187544
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2187545
    iget-object v1, p0, LX/Eze;->d:LX/Ezh;

    iget-object v1, v1, LX/Ezh;->b:LX/Ezb;

    invoke-virtual {v1, v0}, LX/1OM;->i_(I)V

    .line 2187546
    :cond_0
    return-void

    .line 2187547
    :cond_1
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 2187548
    :cond_2
    const/4 v4, -0x1

    goto :goto_1
.end method
