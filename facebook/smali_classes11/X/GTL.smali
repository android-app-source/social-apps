.class public LX/GTL;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/auth/userscope/UserScoped;
.end annotation


# static fields
.field private static final d:Ljava/lang/Object;


# instance fields
.field private a:LX/GTO;

.field private b:LX/0Xl;

.field public c:LX/GTP;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2355425
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, LX/GTL;->d:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(LX/GTO;LX/0Xl;)V
    .locals 3
    .param p2    # LX/0Xl;
        .annotation runtime Lcom/facebook/base/broadcast/LocalBroadcast;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2355420
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2355421
    iput-object p1, p0, LX/GTL;->a:LX/GTO;

    .line 2355422
    iput-object p2, p0, LX/GTL;->b:LX/0Xl;

    .line 2355423
    iget-object v0, p0, LX/GTL;->b:LX/0Xl;

    invoke-interface {v0}, LX/0Xl;->a()LX/0YX;

    move-result-object v0

    const-string v1, "com.facebook.common.appstate.AppStateManager.USER_LEFT_APP"

    new-instance v2, LX/GTI;

    invoke-direct {v2, p0}, LX/GTI;-><init>(LX/GTL;)V

    invoke-interface {v0, v1, v2}, LX/0YX;->a(Ljava/lang/String;LX/0YZ;)LX/0YX;

    move-result-object v0

    invoke-interface {v0}, LX/0YX;->a()LX/0Yb;

    move-result-object v0

    invoke-virtual {v0}, LX/0Yb;->b()V

    .line 2355424
    return-void
.end method

.method public static a(LX/0QB;)LX/GTL;
    .locals 8

    .prologue
    .line 2355426
    invoke-static {}, LX/0SD;->a()LX/0SD;

    move-result-object v2

    .line 2355427
    const-class v0, LX/0S2;

    invoke-interface {p0, v0}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0S2;

    .line 2355428
    invoke-interface {p0}, LX/0QB;->getScopeAwareInjector()LX/0R6;

    move-result-object v1

    invoke-interface {v1}, LX/0R6;->b()Landroid/content/Context;

    move-result-object v1

    .line 2355429
    if-nez v1, :cond_0

    .line 2355430
    new-instance v0, LX/4fr;

    const-string v1, "Called user scoped provider outside of context scope"

    invoke-direct {v0, v1}, LX/4fr;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2355431
    :cond_0
    invoke-virtual {v0, v1}, LX/0S2;->a(Landroid/content/Context;)LX/0op;

    move-result-object v3

    .line 2355432
    :try_start_0
    iget-object v1, v3, LX/0op;->c:Ljava/util/concurrent/ConcurrentMap;

    move-object v4, v1

    .line 2355433
    sget-object v1, LX/GTL;->d:Ljava/lang/Object;

    invoke-interface {v4, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 2355434
    sget-object v5, LX/0S2;->a:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    if-ne v1, v5, :cond_1

    .line 2355435
    invoke-virtual {v3}, LX/0op;->c()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 2355436
    :cond_1
    if-nez v1, :cond_4

    .line 2355437
    const/4 v1, 0x4

    :try_start_1
    invoke-virtual {v2, v1}, LX/0SD;->b(B)B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v5

    .line 2355438
    :try_start_2
    invoke-virtual {v0, v3}, LX/0S2;->a(LX/0op;)LX/0S7;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v6

    .line 2355439
    :try_start_3
    invoke-virtual {v6}, LX/0S7;->e()LX/0R6;

    move-result-object v0

    .line 2355440
    new-instance p0, LX/GTL;

    invoke-static {v0}, LX/GTO;->a(LX/0QB;)LX/GTO;

    move-result-object v1

    check-cast v1, LX/GTO;

    invoke-static {v0}, LX/0Xj;->a(LX/0QB;)LX/0Xj;

    move-result-object v7

    check-cast v7, LX/0Xl;

    invoke-direct {p0, v1, v7}, LX/GTL;-><init>(LX/GTO;LX/0Xl;)V

    .line 2355441
    move-object v1, p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2355442
    :try_start_4
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    .line 2355443
    if-nez v1, :cond_2

    .line 2355444
    sget-object v0, LX/GTL;->d:Ljava/lang/Object;

    sget-object v6, LX/0S2;->a:Ljava/lang/Object;

    invoke-interface {v4, v0, v6}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GTL;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2355445
    :goto_1
    if-eqz v0, :cond_3

    .line 2355446
    :goto_2
    :try_start_5
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2355447
    :goto_3
    check-cast v0, LX/GTL;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 2355448
    invoke-virtual {v3}, LX/0op;->c()V

    goto :goto_0

    .line 2355449
    :catchall_0
    move-exception v0

    :try_start_6
    invoke-static {v6}, LX/0S2;->a(LX/0S7;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 2355450
    :catchall_1
    move-exception v0

    .line 2355451
    :try_start_7
    iput-byte v5, v2, LX/0SD;->a:B

    .line 2355452
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 2355453
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, LX/0op;->c()V

    throw v0

    .line 2355454
    :cond_2
    :try_start_8
    sget-object v0, LX/GTL;->d:Ljava/lang/Object;

    invoke-interface {v4, v0, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/GTL;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_2

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method


# virtual methods
.method public final a(Ljava/lang/String;JLX/0TF;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "J",
            "LX/0TF",
            "<",
            "LX/GTP;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2355408
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-ltz v0, :cond_0

    const-wide/16 v0, 0x384

    cmp-long v0, p2, v0

    if-lez v0, :cond_1

    .line 2355409
    :cond_0
    new-instance v0, LX/GTK;

    invoke-direct {v0, p0}, LX/GTK;-><init>(LX/GTL;)V

    invoke-interface {p4, v0}, LX/0TF;->onFailure(Ljava/lang/Throwable;)V

    .line 2355410
    :goto_0
    return-void

    .line 2355411
    :cond_1
    iget-object v0, p0, LX/GTL;->c:LX/GTP;

    if-eqz v0, :cond_2

    iget-object v0, p0, LX/GTL;->c:LX/GTP;

    invoke-virtual {v0}, LX/GTP;->c()J

    move-result-wide v0

    cmp-long v0, v0, p2

    if-ltz v0, :cond_2

    .line 2355412
    iget-object v0, p0, LX/GTL;->c:LX/GTP;

    invoke-interface {p4, v0}, LX/0TF;->onSuccess(Ljava/lang/Object;)V

    goto :goto_0

    .line 2355413
    :cond_2
    iget-object v0, p0, LX/GTL;->a:LX/GTO;

    new-instance v1, LX/GTJ;

    invoke-direct {v1, p0, p4}, LX/GTJ;-><init>(LX/GTL;LX/0TF;)V

    .line 2355414
    iput-object v1, v0, LX/GTO;->f:LX/0TF;

    .line 2355415
    new-instance p0, Landroid/content/Intent;

    iget-object p2, v0, LX/GTO;->d:Landroid/content/Context;

    const-class p3, Lcom/facebook/auth/reauth/ReauthActivity;

    invoke-direct {p0, p2, p3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 2355416
    const-string p2, "message"

    invoke-virtual {p0, p2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2355417
    const/high16 p2, 0x10000000

    invoke-virtual {p0, p2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 2355418
    iget-object p2, v0, LX/GTO;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object p3, v0, LX/GTO;->d:Landroid/content/Context;

    invoke-interface {p2, p0, p3}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2355419
    goto :goto_0
.end method
