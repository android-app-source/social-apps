.class public LX/GqR;
.super LX/Cos;
.source ""

# interfaces
.implements LX/CnG;
.implements LX/Coc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/Cos",
        "<",
        "LX/Gpf;",
        "Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;",
        ">;",
        "Lcom/facebook/instantshopping/view/block/InstantShoppingFeedVideoBlockView;"
    }
.end annotation


# instance fields
.field private final A:LX/Ci3;

.field public a:LX/Bug;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/2mZ;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/1qa;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public d:LX/Chv;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public e:LX/Ciy;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/CIi;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/Cuw;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public m:LX/394;

.field public n:Lcom/facebook/video/player/RichVideoPlayer;

.field public o:Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

.field public p:LX/2pa;

.field public q:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;"
        }
    .end annotation
.end field

.field public r:LX/3FT;

.field public s:I

.field public t:I

.field public u:LX/04D;

.field public v:LX/04g;

.field private final w:LX/Cun;

.field public final x:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private final y:LX/ChN;

.field private final z:LX/Chm;


# direct methods
.method public constructor <init>(LX/Ctg;Landroid/view/View;)V
    .locals 7

    .prologue
    .line 2396460
    invoke-direct {p0, p1, p2}, LX/Cos;-><init>(LX/Ctg;Landroid/view/View;)V

    .line 2396461
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/GqR;->x:Ljava/util/List;

    .line 2396462
    new-instance v0, LX/GqN;

    invoke-direct {v0, p0}, LX/GqN;-><init>(LX/GqR;)V

    iput-object v0, p0, LX/GqR;->y:LX/ChN;

    .line 2396463
    new-instance v0, LX/GqO;

    invoke-direct {v0, p0}, LX/GqO;-><init>(LX/GqR;)V

    iput-object v0, p0, LX/GqR;->z:LX/Chm;

    .line 2396464
    new-instance v0, LX/GqP;

    invoke-direct {v0, p0}, LX/GqP;-><init>(LX/GqR;)V

    iput-object v0, p0, LX/GqR;->A:LX/Ci3;

    .line 2396465
    invoke-interface {p0}, LX/02k;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v0

    move-object v2, p0

    check-cast v2, LX/GqR;

    invoke-static {v0}, LX/Bug;->b(LX/0QB;)LX/Bug;

    move-result-object v3

    check-cast v3, LX/Bug;

    const-class v4, LX/2mZ;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/2mZ;

    invoke-static {v0}, LX/1qa;->a(LX/0QB;)LX/1qa;

    move-result-object v5

    check-cast v5, LX/1qa;

    invoke-static {v0}, LX/Chv;->a(LX/0QB;)LX/Chv;

    move-result-object v6

    check-cast v6, LX/Chv;

    invoke-static {v0}, LX/Ciy;->a(LX/0QB;)LX/Ciy;

    move-result-object p1

    check-cast p1, LX/Ciy;

    invoke-static {v0}, LX/CIi;->a(LX/0QB;)LX/CIi;

    move-result-object p2

    check-cast p2, LX/CIi;

    invoke-static {v0}, LX/Cuw;->a(LX/0QB;)LX/Cuw;

    move-result-object v0

    check-cast v0, LX/Cuw;

    iput-object v3, v2, LX/GqR;->a:LX/Bug;

    iput-object v4, v2, LX/GqR;->b:LX/2mZ;

    iput-object v5, v2, LX/GqR;->c:LX/1qa;

    iput-object v6, v2, LX/GqR;->d:LX/Chv;

    iput-object p1, v2, LX/GqR;->e:LX/Ciy;

    iput-object p2, v2, LX/GqR;->k:LX/CIi;

    iput-object v0, v2, LX/GqR;->l:LX/Cuw;

    .line 2396466
    invoke-virtual {p0}, LX/GqR;->f()Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    move-result-object v0

    iput-object v0, p0, LX/GqR;->o:Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    .line 2396467
    new-instance v0, LX/Cun;

    invoke-direct {v0}, LX/Cun;-><init>()V

    iput-object v0, p0, LX/GqR;->w:LX/Cun;

    .line 2396468
    iget-object v0, p0, LX/GqR;->d:LX/Chv;

    iget-object v1, p0, LX/GqR;->y:LX/ChN;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2396469
    iget-object v0, p0, LX/GqR;->d:LX/Chv;

    iget-object v1, p0, LX/GqR;->z:LX/Chm;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2396470
    iget-object v0, p0, LX/GqR;->d:LX/Chv;

    iget-object v1, p0, LX/GqR;->A:LX/Ci3;

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b2;)Z

    .line 2396471
    return-void
.end method

.method private static b(LX/GqR;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2pa;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)",
            "LX/2pa;"
        }
    .end annotation

    .prologue
    .line 2396477
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2396478
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    .line 2396479
    if-eqz p1, :cond_0

    if-nez v0, :cond_1

    .line 2396480
    :cond_0
    const/4 v0, 0x0

    .line 2396481
    :goto_0
    return-object v0

    .line 2396482
    :cond_1
    invoke-virtual {p1, v0}, Lcom/facebook/feed/rows/core/props/FeedProps;->a(Lcom/facebook/flatbuffers/Flattenable;)Lcom/facebook/feed/rows/core/props/FeedProps;

    move-result-object v1

    .line 2396483
    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v2

    invoke-static {v2}, LX/36q;->b(Lcom/facebook/graphql/model/GraphQLMedia;)Lcom/facebook/graphql/model/GraphQLVideo;

    move-result-object v2

    .line 2396484
    iget-object v3, p0, LX/GqR;->b:LX/2mZ;

    invoke-virtual {v3, v1, v2}, LX/2mZ;->a(Lcom/facebook/feed/rows/core/props/FeedProps;Lcom/facebook/graphql/model/GraphQLVideo;)LX/3Im;

    move-result-object v1

    .line 2396485
    invoke-virtual {v1}, LX/3Im;->a()Lcom/facebook/video/engine/VideoPlayerParams;

    move-result-object v3

    .line 2396486
    iget-object v1, p0, LX/GqR;->c:LX/1qa;

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStoryAttachment;->r()Lcom/facebook/graphql/model/GraphQLMedia;

    move-result-object v0

    sget-object v4, LX/26P;->Video:LX/26P;

    invoke-virtual {v1, v0, v4}, LX/1qa;->a(Lcom/facebook/graphql/model/GraphQLMedia;LX/26P;)LX/1bf;

    move-result-object v4

    .line 2396487
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->bh()I

    move-result v0

    .line 2396488
    invoke-virtual {v2}, Lcom/facebook/graphql/model/GraphQLVideo;->F()I

    move-result v1

    .line 2396489
    if-eqz v1, :cond_2

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    int-to-double v8, v0

    mul-double/2addr v6, v8

    int-to-double v0, v1

    div-double v0, v6, v0

    :goto_1
    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    .line 2396490
    new-instance v1, LX/0P2;

    invoke-direct {v1}, LX/0P2;-><init>()V

    .line 2396491
    const-string v2, "GraphQLStoryProps"

    invoke-virtual {v1, v2, p1}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    move-result-object v2

    const-string v5, "CoverImageParamsKey"

    invoke-virtual {v2, v5, v4}, LX/0P2;->b(Ljava/lang/Object;Ljava/lang/Object;)LX/0P2;

    .line 2396492
    new-instance v2, LX/2pZ;

    invoke-direct {v2}, LX/2pZ;-><init>()V

    .line 2396493
    iput-object v3, v2, LX/2pZ;->a:Lcom/facebook/video/engine/VideoPlayerParams;

    .line 2396494
    move-object v2, v2

    .line 2396495
    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    .line 2396496
    iput-wide v4, v2, LX/2pZ;->e:D

    .line 2396497
    move-object v0, v2

    .line 2396498
    invoke-virtual {v1}, LX/0P2;->b()LX/0P1;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2pZ;->a(LX/0P1;)LX/2pZ;

    move-result-object v0

    invoke-virtual {v0}, LX/2pZ;->b()LX/2pa;

    move-result-object v0

    goto :goto_0

    .line 2396499
    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_1
.end method

.method private static u(LX/GqR;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 2396475
    iget-object v0, p0, LX/GqR;->l:LX/Cuw;

    invoke-virtual {p0}, LX/GqR;->f()Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v1

    iget-object v2, p0, LX/GqR;->w:LX/Cun;

    iget-object v5, p0, LX/GqR;->p:LX/2pa;

    invoke-virtual {v5}, LX/2pa;->d()Z

    move-result v8

    const/4 v9, 0x0

    move v5, v4

    move v6, v3

    move v7, v4

    invoke-virtual/range {v0 .. v9}, LX/Cuw;->a(Lcom/facebook/video/player/RichVideoPlayer;LX/Cun;ZZZZZZLX/Cso;)V

    .line 2396476
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 2396472
    invoke-super {p0, p1}, LX/Cos;->a(Landroid/os/Bundle;)V

    .line 2396473
    iget-object v0, p0, LX/GqR;->l:LX/Cuw;

    invoke-virtual {v0}, LX/Cuw;->a()V

    .line 2396474
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 2396453
    invoke-super {p0, p1}, LX/Cos;->b(Landroid/os/Bundle;)V

    .line 2396454
    iget-object v0, p0, LX/GqR;->l:LX/Cuw;

    .line 2396455
    iput-object p1, v0, LX/Cuw;->d:Landroid/os/Bundle;

    .line 2396456
    new-instance v0, LX/GqQ;

    invoke-direct {v0, p0}, LX/GqQ;-><init>(LX/GqR;)V

    .line 2396457
    iget-object v1, p0, LX/Cos;->f:LX/CoR;

    move-object v1, v1

    .line 2396458
    invoke-virtual {p0}, LX/GqR;->f()Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    move-result-object v2

    new-instance v3, LX/CoQ;

    sget-object v4, LX/CoP;->PIXEL:LX/CoP;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, LX/CoQ;-><init>(LX/CoP;I)V

    invoke-virtual {v1, v2, v3, v0}, LX/CoR;->a(Landroid/view/View;LX/CoQ;LX/CoO;)V

    .line 2396459
    return-void
.end method

.method public final c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 2396448
    invoke-super {p0, p1}, LX/Cos;->c(Landroid/os/Bundle;)V

    .line 2396449
    iget-object v0, p0, LX/Cos;->f:LX/CoR;

    move-object v0, v0

    .line 2396450
    invoke-virtual {p0}, LX/GqR;->f()Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/CoR;->a(Landroid/view/View;)V

    .line 2396451
    invoke-virtual {p0}, LX/GqR;->o()V

    .line 2396452
    return-void
.end method

.method public final f()Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;
    .locals 1
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2396500
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    return-object v0
.end method

.method public final g()V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 2396427
    iget-object v0, p0, LX/GqR;->q:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-static {p0, v0}, LX/GqR;->b(LX/GqR;Lcom/facebook/feed/rows/core/props/FeedProps;)LX/2pa;

    move-result-object v0

    iput-object v0, p0, LX/GqR;->p:LX/2pa;

    .line 2396428
    iget-object v0, p0, LX/GqR;->a:LX/Bug;

    iget-object v1, p0, LX/GqR;->r:LX/3FT;

    iget-object v2, p0, LX/GqR;->o:Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    iget-object v3, p0, LX/GqR;->p:LX/2pa;

    sget-object v4, LX/Bue;->WATCH_IN_CANVAS:LX/Bue;

    const/4 v5, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, LX/Bug;->a(LX/3FT;LX/3FT;LX/2pa;LX/Bue;LX/7Lf;Ljava/lang/Boolean;)Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v0

    iput-object v0, p0, LX/GqR;->n:Lcom/facebook/video/player/RichVideoPlayer;

    .line 2396429
    iget-object v0, p0, LX/GqR;->n:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, LX/GqR;->v:LX/04g;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setOriginalPlayReason(LX/04g;)V

    .line 2396430
    iget-object v0, p0, LX/GqR;->m:LX/394;

    if-eqz v0, :cond_0

    .line 2396431
    iget-object v0, p0, LX/GqR;->m:LX/394;

    sget-object v1, LX/04g;->BY_USER:LX/04g;

    invoke-interface {v0, v1}, LX/394;->a(LX/04g;)V

    .line 2396432
    :cond_0
    invoke-static {p0}, LX/GqR;->u(LX/GqR;)V

    .line 2396433
    iget-object v0, p0, LX/GqR;->o:Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    iget v1, p0, LX/GqR;->s:I

    .line 2396434
    iput v1, v0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->k:I

    .line 2396435
    iget-object v0, p0, LX/GqR;->n:Lcom/facebook/video/player/RichVideoPlayer;

    iget-object v1, p0, LX/GqR;->u:LX/04D;

    invoke-virtual {v0, v1}, Lcom/facebook/video/player/RichVideoPlayer;->setPlayerOrigin(LX/04D;)V

    .line 2396436
    iget-object v1, p0, LX/GqR;->o:Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    iget-object v2, p0, LX/GqR;->p:LX/2pa;

    iget-object v0, p0, LX/GqR;->q:Lcom/facebook/feed/rows/core/props/FeedProps;

    .line 2396437
    iget-object v3, v0, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v3

    .line 2396438
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-static {v0}, LX/17E;->r(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLStoryAttachment;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->a(LX/2pa;Lcom/facebook/graphql/model/GraphQLStoryAttachment;)V

    .line 2396439
    iget-object v0, p0, LX/GqR;->o:Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    invoke-virtual {v0, v8}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->setVisibility(I)V

    .line 2396440
    iget-object v0, p0, LX/GqR;->k:LX/CIi;

    sget-object v1, LX/Gn0;->f:LX/0Tn;

    invoke-virtual {v0, v1}, LX/CIi;->a(LX/0Tn;)Z

    move-result v1

    .line 2396441
    iget-object v0, p0, LX/GqR;->k:LX/CIi;

    iget-object v2, p0, LX/GqR;->o:Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    sget-object v3, LX/Gn0;->f:LX/0Tn;

    invoke-virtual {v0, v2, v3}, LX/CIi;->a(LX/0dN;LX/0Tn;)V

    .line 2396442
    iget-object v2, p0, LX/GqR;->n:Lcom/facebook/video/player/RichVideoPlayer;

    if-nez v1, :cond_1

    move v0, v7

    :goto_0
    sget-object v3, LX/04g;->BY_USER:LX/04g;

    invoke-virtual {v2, v0, v3}, Lcom/facebook/video/player/RichVideoPlayer;->a(ZLX/04g;)V

    .line 2396443
    iget-object v0, p0, LX/GqR;->o:Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    if-nez v1, :cond_2

    :goto_1
    invoke-virtual {v0, v7}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->a(Z)V

    .line 2396444
    iget-object v0, p0, LX/GqR;->o:Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    sget-object v1, LX/04G;->CANVAS:LX/04G;

    sget-object v2, LX/04G;->INLINE_PLAYER:LX/04G;

    iget-object v3, p0, LX/GqR;->n:Lcom/facebook/video/player/RichVideoPlayer;

    invoke-virtual {v3}, Lcom/facebook/video/player/RichVideoPlayer;->getCurrentPositionMs()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->a(LX/04G;LX/04G;I)V

    .line 2396445
    return-void

    :cond_1
    move v0, v8

    .line 2396446
    goto :goto_0

    :cond_2
    move v7, v8

    .line 2396447
    goto :goto_1
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 2396419
    new-instance v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingFeedVideoBlockViewImpl$5;

    invoke-direct {v1, p0, p0}, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingFeedVideoBlockViewImpl$5;-><init>(LX/GqR;LX/Coc;)V

    .line 2396420
    iget-object v0, p0, LX/GqR;->e:LX/Ciy;

    invoke-virtual {v0}, LX/Ciy;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2396421
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2396422
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 2396423
    :goto_0
    return-void

    .line 2396424
    :cond_0
    iget-object v0, p0, LX/Cod;->a:LX/1a1;

    move-object v0, v0

    .line 2396425
    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 2396426
    :cond_1
    iget-object v0, p0, LX/GqR;->x:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final o()V
    .locals 2

    .prologue
    .line 2396397
    new-instance v1, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingFeedVideoBlockViewImpl$6;

    invoke-direct {v1, p0, p0}, Lcom/facebook/instantshopping/view/block/impl/InstantShoppingFeedVideoBlockViewImpl$6;-><init>(LX/GqR;LX/Coc;)V

    .line 2396398
    iget-object v0, p0, LX/GqR;->e:LX/Ciy;

    invoke-virtual {v0}, LX/Ciy;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2396399
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2396400
    invoke-virtual {p0}, LX/Cos;->j()LX/Ct1;

    move-result-object v0

    check-cast v0, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    invoke-virtual {v0}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 2396401
    :goto_0
    return-void

    .line 2396402
    :cond_0
    iget-object v0, p0, LX/Cod;->a:LX/1a1;

    move-object v0, v0

    .line 2396403
    iget-object v0, v0, LX/1a1;->a:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 2396404
    :cond_1
    iget-object v0, p0, LX/GqR;->x:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final p()Landroid/view/View;
    .locals 1

    .prologue
    .line 2396418
    invoke-virtual {p0}, LX/GqR;->f()Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    move-result-object v0

    return-object v0
.end method

.method public final q()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2396410
    invoke-virtual {p0}, LX/GqR;->f()Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->getRichVideoPlayer()Lcom/facebook/video/player/RichVideoPlayer;

    move-result-object v1

    .line 2396411
    invoke-virtual {p0}, LX/GqR;->f()Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2396412
    iget-object v2, v1, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v2, v2

    .line 2396413
    if-eqz v2, :cond_1

    .line 2396414
    iget-object v2, v1, Lcom/facebook/video/player/RichVideoPlayer;->E:LX/2pb;

    move-object v1, v2

    .line 2396415
    iget-object v2, v1, LX/2pb;->y:LX/2qV;

    move-object v1, v2

    .line 2396416
    sget-object v2, LX/2qV;->PREPARED:LX/2qV;

    if-eq v1, v2, :cond_0

    invoke-virtual {p0}, LX/GqR;->f()Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/instantshopping/view/widget/media/InstantShoppingFeedVideoPlayer;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 2396417
    :cond_1
    return v0
.end method

.method public final r()V
    .locals 5

    .prologue
    .line 2396405
    iget-object v0, p0, LX/GqR;->l:LX/Cuw;

    new-instance v1, LX/Cuk;

    sget-object v2, LX/Cqu;->COLLAPSED:LX/Cqu;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, LX/Cuk;-><init>(LX/Cqu;ZZ)V

    invoke-virtual {v0, v1}, LX/Cuw;->a(LX/Cuk;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2396406
    iget-object v0, p0, LX/GqR;->l:LX/Cuw;

    sget-object v1, LX/Cuj;->APPLICATION_AUTOPLAY_PAUSE:LX/Cuj;

    invoke-virtual {v0, v1}, LX/Cuw;->a(LX/Cuj;)Z

    .line 2396407
    :cond_0
    iget-object v0, p0, LX/GqR;->n:Lcom/facebook/video/player/RichVideoPlayer;

    if-eqz v0, :cond_1

    .line 2396408
    iget-object v0, p0, LX/GqR;->l:LX/Cuw;

    sget-object v1, LX/Cuj;->APPLICATION_AUTOPLAY:LX/Cuj;

    invoke-virtual {v0, v1}, LX/Cuw;->a(LX/Cuj;)Z

    .line 2396409
    :cond_1
    return-void
.end method
