.class public LX/GCE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Boolean;

.field private d:LX/GF4;

.field public e:LX/0ad;

.field public f:LX/GG3;

.field public g:LX/GGB;

.field public h:LX/GGB;

.field private i:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "LX/GG8;",
            ">;"
        }
    .end annotation
.end field

.field private j:LX/1B1;

.field public k:Lcom/facebook/adspayments/analytics/AdsPaymentsFlowContext;

.field public l:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "LX/GG8;",
            "Lcom/facebook/adinterfaces/AdInterfacesContext$DataValidationCallback;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/GF4;LX/GG3;LX/0ad;)V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 2328236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2328237
    iput-object v0, p0, LX/GCE;->g:LX/GGB;

    .line 2328238
    iput-object v0, p0, LX/GCE;->h:LX/GGB;

    .line 2328239
    iput-object p1, p0, LX/GCE;->d:LX/GF4;

    .line 2328240
    iput-object p2, p0, LX/GCE;->f:LX/GG3;

    .line 2328241
    iput-object p3, p0, LX/GCE;->e:LX/0ad;

    .line 2328242
    new-instance v0, LX/1B1;

    invoke-direct {v0}, LX/1B1;-><init>()V

    iput-object v0, p0, LX/GCE;->j:LX/1B1;

    .line 2328243
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, LX/GCE;->i:Ljava/util/HashSet;

    .line 2328244
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, LX/GCE;->l:Ljava/util/HashMap;

    .line 2328245
    return-void
.end method

.method public static a(LX/0QB;)LX/GCE;
    .locals 4

    .prologue
    .line 2328233
    new-instance v3, LX/GCE;

    invoke-static {p0}, LX/GF4;->a(LX/0QB;)LX/GF4;

    move-result-object v0

    check-cast v0, LX/GF4;

    invoke-static {p0}, LX/GG3;->a(LX/0QB;)LX/GG3;

    move-result-object v1

    check-cast v1, LX/GG3;

    invoke-static {p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v2

    check-cast v2, LX/0ad;

    invoke-direct {v3, v0, v1, v2}, LX/GCE;-><init>(LX/GF4;LX/GG3;LX/0ad;)V

    .line 2328234
    move-object v0, v3

    .line 2328235
    return-object v0
.end method


# virtual methods
.method public final a(ILX/GFR;)V
    .locals 1

    .prologue
    .line 2328201
    iget-object v0, p0, LX/GCE;->d:LX/GF4;

    invoke-virtual {v0, p1, p2}, LX/GF4;->a(ILX/GFR;)V

    .line 2328202
    return-void
.end method

.method public final a(LX/8wN;)V
    .locals 1

    .prologue
    .line 2328231
    iget-object v0, p0, LX/GCE;->d:LX/GF4;

    invoke-virtual {v0, p1}, LX/0b4;->a(LX/0b7;)V

    .line 2328232
    return-void
.end method

.method public final a(LX/8wQ;)V
    .locals 1

    .prologue
    .line 2328228
    iget-object v0, p0, LX/GCE;->j:LX/1B1;

    invoke-virtual {v0, p1}, LX/1B1;->a(LX/0b2;)Z

    .line 2328229
    iget-object v0, p0, LX/GCE;->d:LX/GF4;

    invoke-virtual {v0, p1}, LX/0b4;->a(LX/0b2;)Z

    .line 2328230
    return-void
.end method

.method public final a(LX/GG8;Z)V
    .locals 1

    .prologue
    .line 2328226
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, LX/GCE;->a(LX/GG8;ZLX/GLe;)V

    .line 2328227
    return-void
.end method

.method public final a(LX/GG8;ZLX/GLe;)V
    .locals 3

    .prologue
    .line 2328214
    iget-object v0, p0, LX/GCE;->i:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    .line 2328215
    :cond_0
    :goto_0
    return-void

    .line 2328216
    :cond_1
    if-eqz p2, :cond_2

    .line 2328217
    iget-object v0, p0, LX/GCE;->i:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 2328218
    iget-object v0, p0, LX/GCE;->l:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2328219
    iget-object v0, p0, LX/GCE;->i:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2328220
    iget-object v0, p0, LX/GCE;->d:LX/GF4;

    new-instance v1, LX/GFK;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, LX/GFK;-><init>(Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0

    .line 2328221
    :cond_2
    iget-object v0, p0, LX/GCE;->i:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    .line 2328222
    iget-object v1, p0, LX/GCE;->i:Ljava/util/HashSet;

    invoke-virtual {v1, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 2328223
    iget-object v1, p0, LX/GCE;->l:Ljava/util/HashMap;

    invoke-virtual {v1, p1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2328224
    if-eqz v0, :cond_0

    .line 2328225
    iget-object v0, p0, LX/GCE;->d:LX/GF4;

    new-instance v1, LX/GFK;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, LX/GFK;-><init>(Z)V

    invoke-virtual {v0, v1}, LX/0b4;->a(LX/0b7;)V

    goto :goto_0
.end method

.method public final a(LX/0Rf;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Rf",
            "<",
            "LX/GG8;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2328213
    iget-object v0, p0, LX/GCE;->i:Ljava/util/HashSet;

    invoke-static {v0, p1}, Ljava/util/Collections;->disjoint(Ljava/util/Collection;Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2328212
    iget-object v0, p0, LX/GCE;->i:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 2328207
    iget-object v0, p0, LX/GCE;->j:LX/1B1;

    iget-object v1, p0, LX/GCE;->d:LX/GF4;

    invoke-virtual {v0, v1}, LX/1B1;->b(LX/0b4;)V

    .line 2328208
    iget-object v0, p0, LX/GCE;->d:LX/GF4;

    .line 2328209
    iget-object v1, v0, LX/GF4;->a:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    .line 2328210
    iget-object v0, p0, LX/GCE;->l:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 2328211
    return-void
.end method

.method public final g()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2328203
    iget-object v1, p0, LX/GCE;->g:LX/GGB;

    if-eqz v1, :cond_0

    iget-object v1, p0, LX/GCE;->h:LX/GGB;

    if-nez v1, :cond_1

    .line 2328204
    :cond_0
    :goto_0
    return v0

    .line 2328205
    :cond_1
    sget-object v1, LX/GCD;->a:[I

    iget-object v2, p0, LX/GCE;->g:LX/GGB;

    invoke-virtual {v2}, LX/GGB;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 2328206
    :pswitch_0
    iget-object v1, p0, LX/GCE;->h:LX/GGB;

    sget-object v2, LX/GGB;->ACTIVE:LX/GGB;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, LX/GCE;->h:LX/GGB;

    sget-object v2, LX/GGB;->PENDING:LX/GGB;

    if-eq v1, v2, :cond_2

    iget-object v1, p0, LX/GCE;->h:LX/GGB;

    sget-object v2, LX/GGB;->CREATING:LX/GGB;

    if-ne v1, v2, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
