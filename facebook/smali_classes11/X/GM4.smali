.class public final LX/GM4;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public final synthetic c:LX/GJI;


# direct methods
.method public constructor <init>(LX/GJI;)V
    .locals 0

    .prologue
    .line 2343949
    iput-object p1, p0, LX/GM4;->c:LX/GJI;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 12

    .prologue
    const/4 v5, 0x0

    .line 2343950
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, LX/0YN;->a(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "^0.*"

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2343951
    :cond_0
    invoke-interface {p1}, Landroid/text/Editable;->clear()V

    .line 2343952
    iget-object v0, p0, LX/GM4;->c:LX/GJI;

    iget-object v0, v0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0, v5, v5}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V

    .line 2343953
    iget-object v0, p0, LX/GM4;->c:LX/GJI;

    iget-object v0, v0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0, v5, v5}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V

    .line 2343954
    iget-object v0, p0, LX/GM4;->c:LX/GJI;

    sget-object v1, LX/GMB;->EMPTY_SELECTED:LX/GMB;

    invoke-virtual {v0, v1}, LX/GJH;->a(LX/GMB;)V

    .line 2343955
    iget-object v0, p0, LX/GM4;->c:LX/GJI;

    invoke-virtual {v0}, LX/GJI;->t()V

    .line 2343956
    iget-object v0, p0, LX/GM4;->c:LX/GJI;

    .line 2343957
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2343958
    new-instance v1, LX/GFl;

    invoke-direct {v1, v5}, LX/GFl;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)V

    invoke-virtual {v0, v1}, LX/GCE;->a(LX/8wN;)V

    .line 2343959
    iget-object v0, p0, LX/GM4;->c:LX/GJI;

    .line 2343960
    iget-object v1, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v1

    .line 2343961
    sget-object v1, LX/GG8;->INVALID_BUDGET:LX/GG8;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, LX/GCE;->a(LX/GG8;Z)V

    .line 2343962
    :cond_1
    :goto_0
    return-void

    .line 2343963
    :cond_2
    new-instance v0, Ljava/math/BigDecimal;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/math/BigDecimal;

    iget-object v2, p0, LX/GM4;->c:LX/GJI;

    iget-object v2, v2, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v2}, LX/GG6;->f(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v0

    .line 2343964
    iget-object v1, p0, LX/GM4;->c:LX/GJI;

    iget-object v2, p0, LX/GM4;->c:LX/GJI;

    iget-object v2, v2, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-static {v0, v2}, LX/GMU;->a(Ljava/math/BigDecimal;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    invoke-virtual {v1, v2}, LX/GJI;->c(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v1

    .line 2343965
    iget-object v2, p0, LX/GM4;->c:LX/GJI;

    iget-object v2, v2, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->f()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-static {v1}, LX/GMU;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)Ljava/math/BigDecimal;

    move-result-object v2

    new-instance v3, Ljava/math/BigDecimal;

    iget-object v4, p0, LX/GM4;->c:LX/GJI;

    iget-object v4, v4, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v4}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->f()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v2

    if-eqz v2, :cond_1

    .line 2343966
    :cond_3
    iget-object v2, p0, LX/GM4;->c:LX/GJI;

    invoke-virtual {v2}, LX/GJI;->h()LX/GMT;

    move-result-object v2

    sget-object v3, LX/GMT;->MAX:LX/GMT;

    if-ne v2, v3, :cond_4

    iget-object v2, p0, LX/GM4;->c:LX/GJI;

    iget-object v2, v2, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v2}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->f()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v2

    if-eqz v2, :cond_4

    new-instance v2, Ljava/math/BigDecimal;

    iget-object v3, p0, LX/GM4;->c:LX/GJI;

    iget-object v3, v3, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v3}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->f()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;->k()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    if-lez v0, :cond_4

    .line 2343967
    iget v0, p0, LX/GM4;->b:I

    .line 2343968
    iget-object v1, p0, LX/GM4;->c:LX/GJI;

    iget-object v1, v1, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    iget-object v2, p0, LX/GM4;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setTextEditText(Ljava/lang/CharSequence;)V

    .line 2343969
    iget-object v1, p0, LX/GM4;->c:LX/GJI;

    iget-object v1, v1, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->setSelectionOnEditText(I)V

    goto/16 :goto_0

    .line 2343970
    :cond_4
    iget-object v0, p0, LX/GM4;->c:LX/GJI;

    iget-object v0, v0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0, v1, v5}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V

    .line 2343971
    iget-object v0, p0, LX/GM4;->c:LX/GJI;

    iget-object v0, v0, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0, v1, v5}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->b(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$IntervalModel;)V

    .line 2343972
    iget-object v0, p0, LX/GM4;->c:LX/GJI;

    .line 2343973
    iget-object v2, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v2

    .line 2343974
    iget-object v2, v0, LX/GCE;->f:LX/GG3;

    move-object v0, v2

    .line 2343975
    iget-object v2, p0, LX/GM4;->c:LX/GJI;

    iget-object v2, v2, LX/GJI;->p:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2343976
    const-string v8, "change_flow_option"

    const-string v9, "create"

    const-string v10, "budget"

    const/4 v11, 0x0

    move-object v6, v0

    move-object v7, v2

    invoke-static/range {v6 .. v11}, LX/GG3;->a(LX/GG3;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2343977
    iget-object v0, p0, LX/GM4;->c:LX/GJI;

    invoke-virtual {v0}, LX/GJI;->t()V

    .line 2343978
    iget-object v0, p0, LX/GM4;->c:LX/GJI;

    .line 2343979
    iget-object v2, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v2

    .line 2343980
    new-instance v2, LX/GFl;

    invoke-direct {v2, v1}, LX/GFl;-><init>(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)V

    invoke-virtual {v0, v2}, LX/GCE;->a(LX/8wN;)V

    .line 2343981
    iget-object v0, p0, LX/GM4;->c:LX/GJI;

    .line 2343982
    iget-object v2, v0, LX/GHg;->b:LX/GCE;

    move-object v0, v2

    .line 2343983
    sget-object v2, LX/GG8;->INVALID_BUDGET:LX/GG8;

    iget-object v3, p0, LX/GM4;->c:LX/GJI;

    invoke-virtual {v3}, LX/GJI;->s()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, LX/GCE;->a(LX/GG8;Z)V

    .line 2343984
    iget-object v0, p0, LX/GM4;->c:LX/GJI;

    invoke-static {v0, v1}, LX/GJI;->d(LX/GJI;Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$CurrencyQuantityModel;)V

    .line 2343985
    iget-object v0, p0, LX/GM4;->c:LX/GJI;

    sget-object v1, LX/GMB;->VALID:LX/GMB;

    invoke-virtual {v0, v1}, LX/GJH;->a(LX/GMB;)V

    .line 2343986
    iget-object v0, p0, LX/GM4;->c:LX/GJI;

    invoke-virtual {v0}, LX/GJI;->p()V

    goto/16 :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    .prologue
    .line 2343987
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LX/GM4;->a:Ljava/lang/String;

    .line 2343988
    iget-object v0, p0, LX/GM4;->c:LX/GJI;

    iget-object v0, v0, LX/GJH;->d:Lcom/facebook/adinterfaces/ui/EditableRadioButton;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/EditableRadioButton;->getCursorPositionOnEditText()I

    move-result v0

    iput v0, p0, LX/GM4;->b:I

    .line 2343989
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2343990
    return-void
.end method
