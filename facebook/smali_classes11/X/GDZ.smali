.class public final LX/GDZ;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/graphql/executor/GraphQLResult",
        "<",
        "Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/String;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:LX/GCa;

.field public final synthetic e:Landroid/content/Context;

.field public final synthetic f:LX/GDb;


# direct methods
.method public constructor <init>(LX/GDb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;LX/GCa;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 2329942
    iput-object p1, p0, LX/GDZ;->f:LX/GDb;

    iput-object p2, p0, LX/GDZ;->a:Ljava/lang/String;

    iput-object p3, p0, LX/GDZ;->b:Ljava/lang/String;

    iput-object p4, p0, LX/GDZ;->c:Ljava/lang/String;

    iput-object p5, p0, LX/GDZ;->d:LX/GCa;

    iput-object p6, p0, LX/GDZ;->e:Landroid/content/Context;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    .line 2329943
    iget-object v0, p0, LX/GDZ;->f:LX/GDb;

    const-string v1, "onNonCancellationFailure() after CouponClaimMutation"

    iget-object v2, p0, LX/GDZ;->a:Ljava/lang/String;

    iget-object v3, p0, LX/GDZ;->b:Ljava/lang/String;

    iget-object v4, p0, LX/GDZ;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, LX/GDb;->a$redex0(LX/GDb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2329944
    iget-object v0, p0, LX/GDZ;->d:LX/GCa;

    iget-object v1, p0, LX/GDZ;->e:Landroid/content/Context;

    invoke-static {v1}, LX/GDb;->a(Landroid/content/Context;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GCa;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;)V

    .line 2329945
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 5
    .param p1    # Ljava/lang/Object;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2329946
    check-cast p1, Lcom/facebook/graphql/executor/GraphQLResult;

    .line 2329947
    if-eqz p1, :cond_0

    .line 2329948
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2329949
    if-nez v0, :cond_1

    .line 2329950
    :cond_0
    iget-object v0, p0, LX/GDZ;->f:LX/GDb;

    const-string v1, "Received empty response from CouponClaimMutation"

    iget-object v2, p0, LX/GDZ;->a:Ljava/lang/String;

    iget-object v3, p0, LX/GDZ;->b:Ljava/lang/String;

    iget-object v4, p0, LX/GDZ;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, LX/GDb;->a$redex0(LX/GDb;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 2329951
    iget-object v0, p0, LX/GDZ;->d:LX/GCa;

    iget-object v1, p0, LX/GDZ;->e:Landroid/content/Context;

    invoke-static {v1}, LX/GDb;->a(Landroid/content/Context;)Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/GCa;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;)V

    .line 2329952
    :goto_0
    return-void

    .line 2329953
    :cond_1
    iget-object v0, p1, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2329954
    check-cast v0, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;

    .line 2329955
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/CouponClaimMutationModels$CouponClaimMutationModel;->a()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;

    move-result-object v0

    .line 2329956
    iget-object v1, p0, LX/GDZ;->d:LX/GCa;

    invoke-virtual {v1, v0}, LX/GCa;->a(Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentValidationMessageModel;)V

    goto :goto_0
.end method
