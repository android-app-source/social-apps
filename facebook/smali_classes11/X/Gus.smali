.class public final LX/Gus;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field public final synthetic a:Landroid/widget/Button;

.field public final synthetic b:Landroid/widget/EditText;

.field public final synthetic c:LX/Gui;


# direct methods
.method public constructor <init>(LX/Gui;Landroid/widget/Button;Landroid/widget/EditText;)V
    .locals 0

    .prologue
    .line 2403985
    iput-object p1, p0, LX/Gus;->c:LX/Gui;

    iput-object p2, p0, LX/Gus;->a:Landroid/widget/Button;

    iput-object p3, p0, LX/Gus;->b:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 2403986
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 2403987
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    .prologue
    .line 2403988
    iget-object v1, p0, LX/Gus;->a:Landroid/widget/Button;

    iget-object v0, p0, LX/Gus;->c:LX/Gui;

    iget-boolean v0, v0, LX/Gui;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, LX/Gus;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2403989
    return-void

    .line 2403990
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
