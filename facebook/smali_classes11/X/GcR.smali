.class public final LX/GcR;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GcK;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2371844
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/auth/credentials/DBLFacebookCredentials;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 2371843
    invoke-static {p1, p2, p3}, LX/GcU;->a(Ljava/lang/String;Ljava/lang/String;Lcom/facebook/auth/credentials/DBLFacebookCredentials;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public final a(LX/Gbb;II)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 2371845
    new-instance v0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;

    invoke-direct {v0}, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;-><init>()V

    .line 2371846
    iput-object p1, v0, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->b:LX/Gbb;

    .line 2371847
    if-nez p2, :cond_0

    const p2, 0x7f08347b

    :cond_0
    invoke-virtual {v0, p2}, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->a(I)V

    .line 2371848
    if-nez p3, :cond_1

    const p3, 0x7f083474

    :cond_1
    invoke-virtual {v0, p3}, Lcom/facebook/devicebasedlogin/ui/DBLPasswordVerificationFragment;->d(I)V

    .line 2371849
    return-object v0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2371841
    const-string v0, "change_nonce_using_password"

    return-object v0
.end method

.method public final b()LX/GcT;
    .locals 1

    .prologue
    .line 2371842
    sget-object v0, LX/GcT;->REMOVE_PIN_USING_PASSWORD:LX/GcT;

    return-object v0
.end method
