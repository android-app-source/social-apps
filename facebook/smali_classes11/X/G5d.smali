.class public final LX/G5d;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/facebook/search/api/SearchTypeaheadResult;",
        ">;>;"
    }
.end annotation


# instance fields
.field public final synthetic a:Lcom/facebook/search/api/GraphSearchQuery;

.field public final synthetic b:LX/G5b;


# direct methods
.method public constructor <init>(LX/G5b;Lcom/facebook/search/api/GraphSearchQuery;)V
    .locals 0

    .prologue
    .line 2319026
    iput-object p1, p0, LX/G5d;->b:LX/G5b;

    iput-object p2, p0, LX/G5d;->a:Lcom/facebook/search/api/GraphSearchQuery;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final call()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 2319027
    iget-object v0, p0, LX/G5d;->b:LX/G5b;

    iget-object v0, v0, LX/G5b;->a:LX/3fr;

    iget-object v1, p0, LX/G5d;->b:LX/G5b;

    iget-object v1, v1, LX/G5b;->c:LX/2RQ;

    invoke-virtual {v1}, LX/2RQ;->a()LX/2RR;

    move-result-object v1

    iget-object v2, p0, LX/G5d;->a:Lcom/facebook/search/api/GraphSearchQuery;

    .line 2319028
    iget-object v3, v2, LX/7B6;->b:Ljava/lang/String;

    move-object v2, v3

    .line 2319029
    iput-object v2, v1, LX/2RR;->e:Ljava/lang/String;

    .line 2319030
    move-object v1, v1

    .line 2319031
    iget-object v2, p0, LX/G5d;->b:LX/G5b;

    invoke-virtual {v2}, LX/G5b;->b()LX/0Px;

    move-result-object v2

    .line 2319032
    iput-object v2, v1, LX/2RR;->c:Ljava/util/Collection;

    .line 2319033
    move-object v1, v1

    .line 2319034
    sget-object v2, LX/2RS;->NAME:LX/2RS;

    .line 2319035
    iput-object v2, v1, LX/2RR;->n:LX/2RS;

    .line 2319036
    move-object v1, v1

    .line 2319037
    invoke-virtual {v0, v1}, LX/3fr;->a(LX/2RR;)LX/6N1;

    move-result-object v1

    .line 2319038
    :try_start_0
    iget-object v0, p0, LX/G5d;->b:LX/G5b;

    invoke-virtual {v0}, LX/G5b;->a()LX/0QK;

    move-result-object v0

    invoke-interface {v0, v1}, LX/0QK;->apply(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2319039
    invoke-interface {v1}, LX/6N1;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, LX/6N1;->close()V

    throw v0
.end method
