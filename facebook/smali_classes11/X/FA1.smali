.class public LX/FA1;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/0dN;


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile f:LX/FA1;


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/1h1;",
            ">;"
        }
    .end annotation
.end field

.field public final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0s5;",
            ">;"
        }
    .end annotation
.end field

.field public final c:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;"
        }
    .end annotation
.end field

.field public final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/F9z;",
            ">;"
        }
    .end annotation
.end field

.field public final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FA3;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/prefs/shared/FbSharedPreferences;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0s5;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/1h1;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/FA3;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/F9z;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2206537
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2206538
    iput-object p3, p0, LX/FA1;->a:LX/0Ot;

    .line 2206539
    iput-object p2, p0, LX/FA1;->b:LX/0Ot;

    .line 2206540
    iput-object p1, p0, LX/FA1;->c:LX/0Ot;

    .line 2206541
    iput-object p4, p0, LX/FA1;->e:LX/0Ot;

    .line 2206542
    iput-object p5, p0, LX/FA1;->d:LX/0Ot;

    .line 2206543
    return-void
.end method

.method public static a(LX/0QB;)LX/FA1;
    .locals 9

    .prologue
    .line 2206544
    sget-object v0, LX/FA1;->f:LX/FA1;

    if-nez v0, :cond_1

    .line 2206545
    const-class v1, LX/FA1;

    monitor-enter v1

    .line 2206546
    :try_start_0
    sget-object v0, LX/FA1;->f:LX/FA1;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2206547
    if-eqz v2, :cond_0

    .line 2206548
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2206549
    new-instance v3, LX/FA1;

    const/16 v4, 0xf9a

    invoke-static {v0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    const/16 v5, 0xb74

    invoke-static {v0, v5}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v5

    const/16 v6, 0xb71

    invoke-static {v0, v6}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v6

    const/16 v7, 0x250e

    invoke-static {v0, v7}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v7

    const/16 v8, 0x250b

    invoke-static {v0, v8}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, LX/FA1;-><init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;)V

    .line 2206550
    move-object v0, v3

    .line 2206551
    sput-object v0, LX/FA1;->f:LX/FA1;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2206552
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2206553
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2206554
    :cond_1
    sget-object v0, LX/FA1;->f:LX/FA1;

    return-object v0

    .line 2206555
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2206556
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a$redex0(LX/FA1;LX/0Tn;Z)V
    .locals 3

    .prologue
    .line 2206557
    iget-object v0, p0, LX/FA1;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0s5;

    invoke-virtual {v0}, LX/0s5;->c()Z

    move-result v2

    .line 2206558
    if-eqz v2, :cond_1

    iget-object v0, p0, LX/FA1;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0s5;

    invoke-virtual {v0}, LX/0s5;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 2206559
    :goto_0
    sget-object v0, LX/0sF;->c:LX/0Tn;

    invoke-virtual {p1, v0}, LX/0Tn;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2206560
    if-eqz v2, :cond_3

    .line 2206561
    iget-object v0, p0, LX/FA1;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FA3;

    invoke-virtual {v0}, LX/FA3;->a()V

    .line 2206562
    :goto_1
    invoke-direct {p0, v1}, LX/FA1;->b(Z)V

    .line 2206563
    if-eqz p2, :cond_0

    .line 2206564
    iget-object v0, p0, LX/FA1;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F9z;

    .line 2206565
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    if-eqz v2, :cond_4

    const-string v1, "onion_tor_enabled"

    :goto_2
    invoke-direct {p0, v1}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2206566
    invoke-static {v0, p0}, LX/F9z;->a(LX/F9z;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2206567
    :cond_0
    :goto_3
    return-void

    .line 2206568
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0

    .line 2206569
    :cond_2
    sget-object v0, LX/0sF;->b:LX/0Tn;

    invoke-virtual {p1, v0}, LX/0Tn;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2206570
    invoke-direct {p0, v1}, LX/FA1;->b(Z)V

    .line 2206571
    if-eqz p2, :cond_0

    .line 2206572
    iget-object v0, p0, LX/FA1;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/F9z;

    .line 2206573
    new-instance p0, Lcom/facebook/analytics/logger/HoneyClientEvent;

    if-eqz v1, :cond_5

    const-string v2, "onion_rewrite_enabled"

    :goto_4
    invoke-direct {p0, v2}, Lcom/facebook/analytics/logger/HoneyClientEvent;-><init>(Ljava/lang/String;)V

    .line 2206574
    invoke-static {v0, p0}, LX/F9z;->a(LX/F9z;Lcom/facebook/analytics/logger/HoneyClientEvent;)V

    .line 2206575
    goto :goto_3

    .line 2206576
    :cond_3
    iget-object v0, p0, LX/FA1;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FA3;

    invoke-virtual {v0}, LX/FA3;->b()V

    goto :goto_1

    .line 2206577
    :cond_4
    const-string v1, "onion_tor_disabled"

    goto :goto_2

    .line 2206578
    :cond_5
    const-string v2, "onion_rewrite_disabled"

    goto :goto_4
.end method

.method private b(Z)V
    .locals 1

    .prologue
    .line 2206579
    iget-object v0, p0, LX/FA1;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1h1;

    invoke-interface {v0, p1}, LX/1h1;->a(Z)V

    .line 2206580
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/prefs/shared/FbSharedPreferences;LX/0Tn;)V
    .locals 1

    .prologue
    .line 2206581
    const/4 v0, 0x1

    invoke-static {p0, p2, v0}, LX/FA1;->a$redex0(LX/FA1;LX/0Tn;Z)V

    .line 2206582
    return-void
.end method
