.class public final LX/G5G;
.super LX/D2y;
.source ""


# instance fields
.field public final synthetic c:LX/G5H;


# direct methods
.method public constructor <init>(LX/G5H;)V
    .locals 0

    .prologue
    .line 2318628
    iput-object p1, p0, LX/G5G;->c:LX/G5H;

    invoke-direct {p0, p1}, LX/D2y;-><init>(LX/D2z;)V

    return-void
.end method

.method public static a(LX/G5G;)Z
    .locals 1

    .prologue
    .line 2318651
    iget-object v0, p0, LX/G5G;->c:LX/G5H;

    iget-object v0, v0, LX/D2z;->a:LX/5SB;

    instance-of v0, v0, LX/BP0;

    if-nez v0, :cond_0

    .line 2318652
    const/4 v0, 0x0

    .line 2318653
    :goto_0
    return v0

    .line 2318654
    :cond_0
    iget-object v0, p0, LX/G5G;->c:LX/G5H;

    iget-object v0, v0, LX/D2z;->a:LX/5SB;

    check-cast v0, LX/BP0;

    .line 2318655
    iget-boolean p0, v0, LX/BP0;->d:Z

    move v0, p0

    .line 2318656
    goto :goto_0
.end method

.method public static b(LX/G5G;)V
    .locals 2

    .prologue
    .line 2318657
    iget-object v0, p0, LX/G5G;->c:LX/G5H;

    iget-object v0, v0, LX/G5H;->h:LX/0Xl;

    const-string v1, "com.facebook.intent.action.ACTION_REFRESH_TIMELINE"

    invoke-interface {v0, v1}, LX/0Xl;->a(Ljava/lang/String;)V

    .line 2318658
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 2318629
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2318630
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2318631
    invoke-static {p0}, LX/G5G;->a(LX/G5G;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->bf()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    move v1, v1

    .line 2318632
    if-eqz v1, :cond_0

    .line 2318633
    iget-object v1, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v1, v1

    .line 2318634
    check-cast v1, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2318635
    const v2, 0x7f08335e

    invoke-interface {p1, v2}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 2318636
    new-instance v3, LX/G5D;

    invoke-direct {v3, p0, v1}, LX/G5D;-><init>(LX/G5G;Lcom/facebook/graphql/model/GraphQLStory;)V

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2318637
    iget-object v3, p0, LX/G5G;->c:LX/G5H;

    .line 2318638
    const v4, 0x7f020995

    move v4, v4

    .line 2318639
    invoke-virtual {v3, v2, v4, v1}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 2318640
    :cond_0
    invoke-static {p0}, LX/G5G;->a(LX/G5G;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLStory;->bf()Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    move v0, v1

    .line 2318641
    if-eqz v0, :cond_1

    .line 2318642
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2318643
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2318644
    const v1, 0x7f08335f

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 2318645
    new-instance v2, LX/G5F;

    invoke-direct {v2, p0}, LX/G5F;-><init>(LX/G5G;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2318646
    iget-object v2, p0, LX/G5G;->c:LX/G5H;

    .line 2318647
    const v3, 0x7f020995

    move v3, v3

    .line 2318648
    invoke-virtual {v2, v1, v3, v0}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 2318649
    :cond_1
    invoke-super {p0, p1, p2, p3}, LX/D2y;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 2318650
    return-void

    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method
