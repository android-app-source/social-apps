.class public LX/FcB;
.super LX/1OM;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1OM",
        "<",
        "LX/FcA;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0Px;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Px",
            "<",
            "LX/FcT;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/FcC;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2262071
    invoke-direct {p0}, LX/1OM;-><init>()V

    .line 2262072
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)LX/1a1;
    .locals 3

    .prologue
    .line 2262073
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f031298

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 2262074
    new-instance v1, LX/FcA;

    invoke-direct {v1, v0}, LX/FcA;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final a(LX/1a1;I)V
    .locals 3

    .prologue
    .line 2262075
    check-cast p1, LX/FcA;

    .line 2262076
    iget-object v0, p0, LX/FcB;->a:LX/0Px;

    invoke-virtual {v0, p2}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FcT;

    .line 2262077
    iget-object v1, p1, LX/FcA;->l:Lcom/facebook/search/results/filters/ui/SearchResultPageFilterPill;

    iget-object v2, v0, LX/FcT;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterPill;->setFilterTitle(Ljava/lang/CharSequence;)V

    .line 2262078
    iget-object v1, p1, LX/FcA;->l:Lcom/facebook/search/results/filters/ui/SearchResultPageFilterPill;

    iget-object v2, v0, LX/FcT;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterPill;->setFilterIcon(Landroid/graphics/drawable/Drawable;)V

    .line 2262079
    iget-object v1, p1, LX/FcA;->l:Lcom/facebook/search/results/filters/ui/SearchResultPageFilterPill;

    new-instance v2, LX/Fc9;

    invoke-direct {v2, p0}, LX/Fc9;-><init>(LX/FcB;)V

    iget-object v0, v0, LX/FcT;->c:LX/4FP;

    invoke-virtual {v1, v2, v0}, Lcom/facebook/search/results/filters/ui/SearchResultPageFilterPill;->a(Landroid/view/View$OnClickListener;LX/4FP;)V

    .line 2262080
    return-void
.end method

.method public final ij_()I
    .locals 1

    .prologue
    .line 2262081
    iget-object v0, p0, LX/FcB;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    return v0
.end method
