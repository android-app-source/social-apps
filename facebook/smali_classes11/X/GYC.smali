.class public final enum LX/GYC;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/GYC;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/GYC;

.field public static final enum ANIMATING:LX/GYC;

.field public static final enum SET_UP_SHOP:LX/GYC;

.field public static final enum TURN_ON_MESSAGING:LX/GYC;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2364844
    new-instance v0, LX/GYC;

    const-string v1, "SET_UP_SHOP"

    invoke-direct {v0, v1, v2}, LX/GYC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GYC;->SET_UP_SHOP:LX/GYC;

    .line 2364845
    new-instance v0, LX/GYC;

    const-string v1, "TURN_ON_MESSAGING"

    invoke-direct {v0, v1, v3}, LX/GYC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GYC;->TURN_ON_MESSAGING:LX/GYC;

    .line 2364846
    new-instance v0, LX/GYC;

    const-string v1, "ANIMATING"

    invoke-direct {v0, v1, v4}, LX/GYC;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/GYC;->ANIMATING:LX/GYC;

    .line 2364847
    const/4 v0, 0x3

    new-array v0, v0, [LX/GYC;

    sget-object v1, LX/GYC;->SET_UP_SHOP:LX/GYC;

    aput-object v1, v0, v2

    sget-object v1, LX/GYC;->TURN_ON_MESSAGING:LX/GYC;

    aput-object v1, v0, v3

    sget-object v1, LX/GYC;->ANIMATING:LX/GYC;

    aput-object v1, v0, v4

    sput-object v0, LX/GYC;->$VALUES:[LX/GYC;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2364841
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/GYC;
    .locals 1

    .prologue
    .line 2364843
    const-class v0, LX/GYC;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/GYC;

    return-object v0
.end method

.method public static values()[LX/GYC;
    .locals 1

    .prologue
    .line 2364842
    sget-object v0, LX/GYC;->$VALUES:[LX/GYC;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/GYC;

    return-object v0
.end method
