.class public final LX/Get;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Geu;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

.field public b:LX/25E;

.field public final synthetic c:LX/Geu;


# direct methods
.method public constructor <init>(LX/Geu;)V
    .locals 1

    .prologue
    .line 2376182
    iput-object p1, p0, LX/Get;->c:LX/Geu;

    .line 2376183
    move-object v0, p1

    .line 2376184
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2376185
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2376186
    const-string v0, "PageYouMayLikeBodyHeaderComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2376187
    if-ne p0, p1, :cond_1

    .line 2376188
    :cond_0
    :goto_0
    return v0

    .line 2376189
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2376190
    goto :goto_0

    .line 2376191
    :cond_3
    check-cast p1, LX/Get;

    .line 2376192
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2376193
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2376194
    if-eq v2, v3, :cond_0

    .line 2376195
    iget-object v2, p0, LX/Get;->a:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Get;->a:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    iget-object v3, p1, LX/Get;->a:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2376196
    goto :goto_0

    .line 2376197
    :cond_5
    iget-object v2, p1, LX/Get;->a:Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;

    if-nez v2, :cond_4

    .line 2376198
    :cond_6
    iget-object v2, p0, LX/Get;->b:LX/25E;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/Get;->b:LX/25E;

    iget-object v3, p1, LX/Get;->b:LX/25E;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2376199
    goto :goto_0

    .line 2376200
    :cond_7
    iget-object v2, p1, LX/Get;->b:LX/25E;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
