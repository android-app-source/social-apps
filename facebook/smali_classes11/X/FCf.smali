.class public final enum LX/FCf;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FCf;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FCf;

.field public static final enum PLAYBACK_COMPLETED:LX/FCf;

.field public static final enum PLAYBACK_ERROR:LX/FCf;

.field public static final enum PLAYBACK_PAUSED:LX/FCf;

.field public static final enum PLAYBACK_POSITION_UPDATED:LX/FCf;

.field public static final enum PLAYBACK_RESUMED:LX/FCf;

.field public static final enum PLAYBACK_STARTED:LX/FCf;

.field public static final enum PLAYBACK_STOPPED:LX/FCf;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2210448
    new-instance v0, LX/FCf;

    const-string v1, "PLAYBACK_STARTED"

    invoke-direct {v0, v1, v3}, LX/FCf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FCf;->PLAYBACK_STARTED:LX/FCf;

    .line 2210449
    new-instance v0, LX/FCf;

    const-string v1, "PLAYBACK_ERROR"

    invoke-direct {v0, v1, v4}, LX/FCf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FCf;->PLAYBACK_ERROR:LX/FCf;

    .line 2210450
    new-instance v0, LX/FCf;

    const-string v1, "PLAYBACK_STOPPED"

    invoke-direct {v0, v1, v5}, LX/FCf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FCf;->PLAYBACK_STOPPED:LX/FCf;

    .line 2210451
    new-instance v0, LX/FCf;

    const-string v1, "PLAYBACK_COMPLETED"

    invoke-direct {v0, v1, v6}, LX/FCf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FCf;->PLAYBACK_COMPLETED:LX/FCf;

    .line 2210452
    new-instance v0, LX/FCf;

    const-string v1, "PLAYBACK_POSITION_UPDATED"

    invoke-direct {v0, v1, v7}, LX/FCf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FCf;->PLAYBACK_POSITION_UPDATED:LX/FCf;

    .line 2210453
    new-instance v0, LX/FCf;

    const-string v1, "PLAYBACK_PAUSED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/FCf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FCf;->PLAYBACK_PAUSED:LX/FCf;

    .line 2210454
    new-instance v0, LX/FCf;

    const-string v1, "PLAYBACK_RESUMED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/FCf;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FCf;->PLAYBACK_RESUMED:LX/FCf;

    .line 2210455
    const/4 v0, 0x7

    new-array v0, v0, [LX/FCf;

    sget-object v1, LX/FCf;->PLAYBACK_STARTED:LX/FCf;

    aput-object v1, v0, v3

    sget-object v1, LX/FCf;->PLAYBACK_ERROR:LX/FCf;

    aput-object v1, v0, v4

    sget-object v1, LX/FCf;->PLAYBACK_STOPPED:LX/FCf;

    aput-object v1, v0, v5

    sget-object v1, LX/FCf;->PLAYBACK_COMPLETED:LX/FCf;

    aput-object v1, v0, v6

    sget-object v1, LX/FCf;->PLAYBACK_POSITION_UPDATED:LX/FCf;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/FCf;->PLAYBACK_PAUSED:LX/FCf;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/FCf;->PLAYBACK_RESUMED:LX/FCf;

    aput-object v2, v0, v1

    sput-object v0, LX/FCf;->$VALUES:[LX/FCf;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2210447
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FCf;
    .locals 1

    .prologue
    .line 2210457
    const-class v0, LX/FCf;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FCf;

    return-object v0
.end method

.method public static values()[LX/FCf;
    .locals 1

    .prologue
    .line 2210456
    sget-object v0, LX/FCf;->$VALUES:[LX/FCf;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FCf;

    return-object v0
.end method
