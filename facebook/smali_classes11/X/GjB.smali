.class public LX/GjB;
.super LX/4Ae;
.source ""


# instance fields
.field private a:Landroid/content/Context;

.field private b:Landroid/app/NotificationManager;

.field private c:LX/03V;

.field private d:LX/2HB;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/NotificationManager;LX/03V;LX/2HB;)V
    .locals 0
    .param p4    # LX/2HB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2387264
    invoke-direct {p0}, LX/4Ae;-><init>()V

    .line 2387265
    iput-object p1, p0, LX/GjB;->a:Landroid/content/Context;

    .line 2387266
    iput-object p2, p0, LX/GjB;->b:Landroid/app/NotificationManager;

    .line 2387267
    iput-object p3, p0, LX/GjB;->c:LX/03V;

    .line 2387268
    iput-object p4, p0, LX/GjB;->d:LX/2HB;

    .line 2387269
    return-void
.end method


# virtual methods
.method public final onFailed(Lcom/facebook/fbservice/service/ServiceException;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2387270
    iget-object v0, p0, LX/GjB;->d:LX/2HB;

    iget-object v1, p0, LX/GjB;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0835ed

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/2HB;->a(Z)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v3, v3, v3}, LX/2HB;->a(IIZ)LX/2HB;

    .line 2387271
    iget-object v0, p0, LX/GjB;->b:Landroid/app/NotificationManager;

    const/16 v1, 0x9

    iget-object v2, p0, LX/GjB;->d:LX/2HB;

    invoke-virtual {v2}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 2387272
    iget-object v0, p0, LX/GjB;->c:LX/03V;

    const-string v1, "greeting_card_upload_failed"

    const-string v2, "Upload failed."

    invoke-virtual {v0, v1, v2, p1}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 2387273
    return-void
.end method

.method public final onSucceeded(Lcom/facebook/fbservice/service/OperationResult;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2387274
    iget-object v0, p0, LX/GjB;->d:LX/2HB;

    iget-object v1, p0, LX/GjB;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0835ee

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, LX/2HB;->b(Ljava/lang/CharSequence;)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v3}, LX/2HB;->a(Z)LX/2HB;

    move-result-object v0

    const v1, 0x7f0218e3

    invoke-virtual {v0, v1}, LX/2HB;->a(I)LX/2HB;

    move-result-object v0

    invoke-virtual {v0, v3, v3, v3}, LX/2HB;->a(IIZ)LX/2HB;

    .line 2387275
    iget-object v0, p0, LX/GjB;->b:Landroid/app/NotificationManager;

    const/16 v1, 0x9

    iget-object v2, p0, LX/GjB;->d:LX/2HB;

    invoke-virtual {v2}, LX/2HB;->c()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 2387276
    return-void
.end method
