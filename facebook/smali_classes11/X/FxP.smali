.class public LX/FxP;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile c:LX/FxP;


# instance fields
.field public final a:LX/0tX;

.field public final b:LX/Fx4;


# direct methods
.method public constructor <init>(LX/0tX;LX/Fx4;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2304869
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2304870
    iput-object p1, p0, LX/FxP;->a:LX/0tX;

    .line 2304871
    iput-object p2, p0, LX/FxP;->b:LX/Fx4;

    .line 2304872
    return-void
.end method

.method public static a(LX/0QB;)LX/FxP;
    .locals 5

    .prologue
    .line 2304873
    sget-object v0, LX/FxP;->c:LX/FxP;

    if-nez v0, :cond_1

    .line 2304874
    const-class v1, LX/FxP;

    monitor-enter v1

    .line 2304875
    :try_start_0
    sget-object v0, LX/FxP;->c:LX/FxP;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2304876
    if-eqz v2, :cond_0

    .line 2304877
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2304878
    new-instance p0, LX/FxP;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/Fx4;->a(LX/0QB;)LX/Fx4;

    move-result-object v4

    check-cast v4, LX/Fx4;

    invoke-direct {p0, v3, v4}, LX/FxP;-><init>(LX/0tX;LX/Fx4;)V

    .line 2304879
    move-object v0, p0

    .line 2304880
    sput-object v0, LX/FxP;->c:LX/FxP;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2304881
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2304882
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2304883
    :cond_1
    sget-object v0, LX/FxP;->c:LX/FxP;

    return-object v0

    .line 2304884
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2304885
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
