.class public final LX/GtQ;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/katana/InternSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/InternSettingsActivity;)V
    .locals 0

    .prologue
    .line 2400551
    iput-object p1, p0, LX/GtQ;->a:Lcom/facebook/katana/InternSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2400552
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.facebook.work.SETUP_WORK_PROFILE_ACTIVITY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2400553
    const-string v1, "SHOULD_DISPLAY_ALL_STEPS"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 2400554
    iget-object v1, p0, LX/GtQ;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v1, v1, Lcom/facebook/katana/InternSettingsActivity;->f:Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/GtQ;->a:Lcom/facebook/katana/InternSettingsActivity;

    iget-object v2, v2, Lcom/facebook/katana/InternSettingsActivity;->u:Landroid/content/Context;

    invoke-interface {v1, v0, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2400555
    return v3
.end method
