.class public LX/Gpa;
.super LX/CnT;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/CnT",
        "<",
        "Lcom/facebook/instantshopping/view/block/ExpandableBlockView;",
        "Lcom/facebook/instantshopping/model/data/ExpandableBlockData;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(LX/GqG;)V
    .locals 0

    .prologue
    .line 2395196
    invoke-direct {p0, p1}, LX/CnT;-><init>(LX/CnG;)V

    .line 2395197
    return-void
.end method


# virtual methods
.method public final a(LX/Clr;)V
    .locals 5

    .prologue
    .line 2395198
    check-cast p1, LX/Gp2;

    .line 2395199
    iget-object v0, p0, LX/CnT;->d:LX/CnG;

    move-object v0, v0

    .line 2395200
    check-cast v0, LX/GqG;

    .line 2395201
    iget-object v1, p1, LX/Gp2;->a:LX/CHh;

    invoke-interface {v1}, LX/CHh;->r()LX/CHZ;

    move-result-object v1

    move-object v1, v1

    .line 2395202
    new-instance v2, LX/GoW;

    invoke-direct {v2, v1}, LX/GoW;-><init>(LX/CHZ;)V

    .line 2395203
    new-instance v3, LX/GoU;

    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2}, LX/GoW;->f()LX/GoE;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/GoU;-><init>(Landroid/content/Context;LX/GoE;)V

    invoke-virtual {v3, v2}, LX/Cle;->a(LX/GoW;)LX/Cle;

    move-result-object v3

    invoke-virtual {v3}, LX/Cle;->a()LX/Clf;

    move-result-object v3

    .line 2395204
    iget-object v4, v0, LX/GqG;->c:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2395205
    iget-object p0, v4, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v4, p0

    .line 2395206
    iget-object p0, v3, LX/Clf;->a:Ljava/lang/CharSequence;

    move-object v3, p0

    .line 2395207
    invoke-virtual {v4, v3}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 2395208
    iget-object v3, v0, LX/GqG;->b:LX/CIh;

    iget-object v4, v0, LX/GqG;->c:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2395209
    iget-object p0, v4, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v4, p0

    .line 2395210
    invoke-virtual {v2}, LX/GoF;->e()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;

    move-result-object p0

    invoke-virtual {v2}, LX/GoW;->g()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;

    move-result-object v2

    invoke-virtual {v3, v4, p0, v2}, LX/CIh;->a(Landroid/widget/TextView;Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;)V

    .line 2395211
    iget-object v1, p1, LX/Gp2;->a:LX/CHh;

    invoke-interface {v1}, LX/CHh;->q()LX/CHZ;

    move-result-object v1

    move-object v1, v1

    .line 2395212
    new-instance v2, LX/GoW;

    invoke-direct {v2, v1}, LX/GoW;-><init>(LX/CHZ;)V

    .line 2395213
    new-instance v3, LX/GoU;

    invoke-virtual {v0}, LX/Cod;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v2}, LX/GoW;->f()LX/GoE;

    move-result-object p0

    invoke-direct {v3, v4, p0}, LX/GoU;-><init>(Landroid/content/Context;LX/GoE;)V

    invoke-virtual {v3, v2}, LX/Cle;->a(LX/GoW;)LX/Cle;

    move-result-object v3

    invoke-virtual {v3}, LX/Cle;->a()LX/Clf;

    move-result-object v3

    .line 2395214
    iget-object v4, v0, LX/GqG;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2395215
    iget-object p0, v4, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v4, p0

    .line 2395216
    iget-object p0, v3, LX/Clf;->a:Ljava/lang/CharSequence;

    move-object v3, p0

    .line 2395217
    invoke-virtual {v4, v3}, LX/CtG;->setText(Ljava/lang/CharSequence;)V

    .line 2395218
    iget-object v3, v0, LX/GqG;->b:LX/CIh;

    iget-object v4, v0, LX/GqG;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2395219
    iget-object p0, v4, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v4, p0

    .line 2395220
    invoke-virtual {v2}, LX/GoF;->e()Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;

    move-result-object p0

    invoke-virtual {v2}, LX/GoW;->g()Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;

    move-result-object v2

    invoke-virtual {v3, v4, p0, v2}, LX/CIh;->a(Landroid/widget/TextView;Lcom/facebook/graphql/enums/GraphQLInstantShoppingDocumentAlignmentDescriptorType;Lcom/facebook/instantshopping/model/graphql/InstantShoppingGraphQLModels$InstantShoppingTextMetricsDescriptorFragmentModel;)V

    .line 2395221
    invoke-interface {p1}, LX/GoY;->D()LX/GoE;

    move-result-object v1

    .line 2395222
    iget-object v2, v0, LX/GqG;->e:Lcom/facebook/richdocument/view/widget/RichTextView;

    .line 2395223
    iget-object v3, v2, Lcom/facebook/richdocument/view/widget/RichTextView;->e:LX/CtG;

    move-object v2, v3

    .line 2395224
    new-instance v3, LX/GqB;

    invoke-direct {v3, v0, v1}, LX/GqB;-><init>(LX/GqG;LX/GoE;)V

    invoke-virtual {v2, v3}, LX/CtG;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2395225
    iget-object v2, v0, LX/GqG;->d:Landroid/widget/ImageView;

    new-instance v3, LX/GqC;

    invoke-direct {v3, v0, v1}, LX/GqC;-><init>(LX/GqG;LX/GoE;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2395226
    iget-object v2, v0, LX/GqG;->f:Landroid/widget/RelativeLayout;

    new-instance v3, LX/GqD;

    invoke-direct {v3, v0, v1}, LX/GqD;-><init>(LX/GqG;LX/GoE;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2395227
    invoke-interface {p1}, LX/GoY;->D()LX/GoE;

    move-result-object v1

    .line 2395228
    iget-object v2, v0, LX/GqG;->a:LX/Go0;

    invoke-virtual {v2, v1}, LX/Go0;->a(LX/GoE;)V

    .line 2395229
    return-void
.end method
