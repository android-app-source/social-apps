.class public final LX/Gf2;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/Gf3;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/1Pp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public b:LX/1f9;

.field public c:LX/25E;

.field public d:Lcom/facebook/feed/rows/core/props/FeedProps;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/ScrollableItemListFeedUnit;",
            ">;"
        }
    .end annotation
.end field

.field public e:I

.field public f:LX/2dx;

.field public g:Z

.field public h:Z

.field public final synthetic i:LX/Gf3;


# direct methods
.method public constructor <init>(LX/Gf3;)V
    .locals 1

    .prologue
    .line 2376632
    iput-object p1, p0, LX/Gf2;->i:LX/Gf3;

    .line 2376633
    move-object v0, p1

    .line 2376634
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2376635
    const/4 v0, 0x1

    iput-boolean v0, p0, LX/Gf2;->g:Z

    .line 2376636
    const/4 v0, 0x0

    iput-boolean v0, p0, LX/Gf2;->h:Z

    .line 2376637
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2376638
    const-string v0, "PageYouMayLikeComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2376639
    if-ne p0, p1, :cond_1

    .line 2376640
    :cond_0
    :goto_0
    return v0

    .line 2376641
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2376642
    goto :goto_0

    .line 2376643
    :cond_3
    check-cast p1, LX/Gf2;

    .line 2376644
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2376645
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2376646
    if-eq v2, v3, :cond_0

    .line 2376647
    iget-object v2, p0, LX/Gf2;->a:LX/1Pp;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/Gf2;->a:LX/1Pp;

    iget-object v3, p1, LX/Gf2;->a:LX/1Pp;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2376648
    goto :goto_0

    .line 2376649
    :cond_5
    iget-object v2, p1, LX/Gf2;->a:LX/1Pp;

    if-nez v2, :cond_4

    .line 2376650
    :cond_6
    iget-object v2, p0, LX/Gf2;->b:LX/1f9;

    if-eqz v2, :cond_8

    iget-object v2, p0, LX/Gf2;->b:LX/1f9;

    iget-object v3, p1, LX/Gf2;->b:LX/1f9;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    .line 2376651
    goto :goto_0

    .line 2376652
    :cond_8
    iget-object v2, p1, LX/Gf2;->b:LX/1f9;

    if-nez v2, :cond_7

    .line 2376653
    :cond_9
    iget-object v2, p0, LX/Gf2;->c:LX/25E;

    if-eqz v2, :cond_b

    iget-object v2, p0, LX/Gf2;->c:LX/25E;

    iget-object v3, p1, LX/Gf2;->c:LX/25E;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    :cond_a
    move v0, v1

    .line 2376654
    goto :goto_0

    .line 2376655
    :cond_b
    iget-object v2, p1, LX/Gf2;->c:LX/25E;

    if-nez v2, :cond_a

    .line 2376656
    :cond_c
    iget-object v2, p0, LX/Gf2;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-eqz v2, :cond_e

    iget-object v2, p0, LX/Gf2;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    iget-object v3, p1, LX/Gf2;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    invoke-virtual {v2, v3}, Lcom/facebook/feed/rows/core/props/FeedProps;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    :cond_d
    move v0, v1

    .line 2376657
    goto :goto_0

    .line 2376658
    :cond_e
    iget-object v2, p1, LX/Gf2;->d:Lcom/facebook/feed/rows/core/props/FeedProps;

    if-nez v2, :cond_d

    .line 2376659
    :cond_f
    iget v2, p0, LX/Gf2;->e:I

    iget v3, p1, LX/Gf2;->e:I

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 2376660
    goto :goto_0

    .line 2376661
    :cond_10
    iget-object v2, p0, LX/Gf2;->f:LX/2dx;

    if-eqz v2, :cond_12

    iget-object v2, p0, LX/Gf2;->f:LX/2dx;

    iget-object v3, p1, LX/Gf2;->f:LX/2dx;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_11
    move v0, v1

    .line 2376662
    goto :goto_0

    .line 2376663
    :cond_12
    iget-object v2, p1, LX/Gf2;->f:LX/2dx;

    if-nez v2, :cond_11

    .line 2376664
    :cond_13
    iget-boolean v2, p0, LX/Gf2;->g:Z

    iget-boolean v3, p1, LX/Gf2;->g:Z

    if-eq v2, v3, :cond_14

    move v0, v1

    .line 2376665
    goto/16 :goto_0

    .line 2376666
    :cond_14
    iget-boolean v2, p0, LX/Gf2;->h:Z

    iget-boolean v3, p1, LX/Gf2;->h:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2376667
    goto/16 :goto_0
.end method
