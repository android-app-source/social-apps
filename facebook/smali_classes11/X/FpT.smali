.class public LX/FpT;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public a:Landroid/content/Context;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public b:LX/0wM;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "LX/FpS;",
            ">;"
        }
    .end annotation
.end field

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0wM;)V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2291597
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2291598
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LX/FpT;->c:Ljava/util/ArrayList;

    .line 2291599
    iput-object p1, p0, LX/FpT;->a:Landroid/content/Context;

    .line 2291600
    iput-object p2, p0, LX/FpT;->b:LX/0wM;

    .line 2291601
    return-void
.end method


# virtual methods
.method public final a(I)LX/FpS;
    .locals 1

    .prologue
    .line 2291636
    iget-object v0, p0, LX/FpT;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/FpS;

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 2291635
    iget-object v0, p0, LX/FpT;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2291634
    invoke-virtual {p0, p1}, LX/FpT;->a(I)LX/FpS;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2291633
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    .prologue
    .line 2291602
    invoke-virtual {p0, p1}, LX/FpT;->a(I)LX/FpS;

    move-result-object v1

    .line 2291603
    sget-object v0, LX/FpQ;->a:[I

    .line 2291604
    iget-object v2, v1, LX/FpS;->c:LX/FpR;

    move-object v2, v2

    .line 2291605
    invoke-virtual {v2}, LX/FpR;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 2291606
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized row type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 2291607
    iget-object v3, v1, LX/FpS;->c:LX/FpR;

    move-object v1, v3

    .line 2291608
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2291609
    :pswitch_0
    if-eqz p2, :cond_0

    instance-of v0, p2, Lcom/facebook/fig/listitem/FigListItem;

    if-nez v0, :cond_2

    .line 2291610
    :cond_0
    new-instance v0, Lcom/facebook/fig/listitem/FigListItem;

    iget-object v2, p0, LX/FpT;->a:Landroid/content/Context;

    const/16 v3, 0xb

    invoke-direct {v0, v2, v3}, Lcom/facebook/fig/listitem/FigListItem;-><init>(Landroid/content/Context;I)V

    .line 2291611
    :goto_0
    check-cast v0, Lcom/facebook/fig/listitem/FigListItem;

    .line 2291612
    iget-object v2, p0, LX/FpT;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f083324

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 2291613
    iget-object v3, v1, LX/FpS;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2291614
    iget-object v4, v1, LX/FpS;->a:Ljava/lang/String;

    move-object v4, v4

    .line 2291615
    invoke-static {v2, v3, v4}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setTitleText(Ljava/lang/CharSequence;)V

    .line 2291616
    iget-object v2, v1, LX/FpS;->a:Ljava/lang/String;

    move-object v2, v2

    .line 2291617
    iget-object v3, p0, LX/FpT;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 2291618
    const/4 v2, 0x1

    .line 2291619
    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 2291620
    iget-object v2, p0, LX/FpT;->b:LX/0wM;

    const v3, 0x7f0207d6

    const v4, -0xbf7f01

    invoke-virtual {v2, v3, v4}, LX/0wM;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/facebook/fig/listitem/FigListItem;->setActionDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 2291621
    :goto_1
    move-object v0, v0

    .line 2291622
    :goto_2
    return-object v0

    .line 2291623
    :pswitch_1
    new-instance v1, Landroid/widget/Space;

    iget-object v0, p0, LX/FpT;->a:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/Space;-><init>(Landroid/content/Context;)V

    .line 2291624
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView$LayoutParams;

    .line 2291625
    if-nez v0, :cond_1

    .line 2291626
    new-instance v0, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    iget-object v3, p0, LX/FpT;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b007b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 2291627
    :goto_3
    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object v0, v1

    .line 2291628
    goto :goto_2

    .line 2291629
    :cond_1
    iget-object v2, p0, LX/FpT;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b007b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, v0, Landroid/widget/AbsListView$LayoutParams;->height:I

    goto :goto_3

    :cond_2
    move-object v0, p2

    goto :goto_0

    .line 2291630
    :cond_3
    const/4 v2, 0x0

    .line 2291631
    invoke-virtual {v0, v2}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setShowAuxView(Z)V

    .line 2291632
    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
