.class public LX/G0x;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/G0u;


# instance fields
.field private final a:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/0gh;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field private final d:LX/BQ9;

.field public final e:LX/5SB;

.field public final f:LX/BQ1;

.field public final g:LX/G4m;

.field private final h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/BQ9;LX/5SB;LX/G4m;LX/BQ1;LX/0Or;LX/0Or;Ljava/lang/Boolean;)V
    .locals 1
    .param p1    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # LX/BQ9;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p3    # LX/5SB;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # LX/G4m;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p5    # LX/BQ1;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Ljava/lang/Boolean;
        .annotation runtime Lcom/facebook/common/build/IsWorkBuild;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/BQ9;",
            "LX/5SB;",
            "LX/G4m;",
            "LX/BQ1;",
            "LX/0Or",
            "<",
            "LX/17W;",
            ">;",
            "LX/0Or",
            "<",
            "LX/0gh;",
            ">;",
            "Ljava/lang/Boolean;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2310526
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2310527
    iput-object p1, p0, LX/G0x;->c:Landroid/content/Context;

    .line 2310528
    iput-object p2, p0, LX/G0x;->d:LX/BQ9;

    .line 2310529
    iput-object p3, p0, LX/G0x;->e:LX/5SB;

    .line 2310530
    iput-object p5, p0, LX/G0x;->f:LX/BQ1;

    .line 2310531
    iput-object p4, p0, LX/G0x;->g:LX/G4m;

    .line 2310532
    iput-object p7, p0, LX/G0x;->b:LX/0Or;

    .line 2310533
    iput-object p6, p0, LX/G0x;->a:LX/0Or;

    .line 2310534
    invoke-virtual {p8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, LX/G0x;->h:Z

    .line 2310535
    return-void
.end method

.method private c(Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 2310536
    sget-object v0, LX/G0w;->a:[I

    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 2310537
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported item type = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2310538
    :pswitch_0
    iget-boolean v0, p0, LX/G0x;->h:Z

    if-eqz v0, :cond_0

    sget-object v0, LX/0ax;->bG:Ljava/lang/String;

    :goto_0
    iget-object v1, p0, LX/G0x;->e:LX/5SB;

    .line 2310539
    iget-wide v4, v1, LX/5SB;->b:J

    move-wide v2, v4

    .line 2310540
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2310541
    :goto_1
    return-object v0

    .line 2310542
    :cond_0
    sget-object v0, LX/0ax;->bF:Ljava/lang/String;

    goto :goto_0

    .line 2310543
    :pswitch_1
    iget-object v0, p0, LX/G0x;->e:LX/5SB;

    invoke-virtual {v0}, LX/5SB;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2310544
    sget-object v0, LX/0ax;->bY:Ljava/lang/String;

    goto :goto_1

    .line 2310545
    :cond_1
    sget-object v0, LX/0ax;->bZ:Ljava/lang/String;

    iget-object v1, p0, LX/G0x;->e:LX/5SB;

    .line 2310546
    iget-wide v4, v1, LX/5SB;->b:J

    move-wide v2, v4

    .line 2310547
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2310548
    :pswitch_2
    iget-object v0, p0, LX/G0x;->e:LX/5SB;

    invoke-virtual {v0}, LX/5SB;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, LX/DHs;->ALL_FRIENDS:LX/DHs;

    invoke-virtual {v0}, LX/DHs;->name()Ljava/lang/String;

    move-result-object v0

    .line 2310549
    :goto_2
    sget-object v1, LX/0ax;->bO:Ljava/lang/String;

    iget-object v2, p0, LX/G0x;->e:LX/5SB;

    .line 2310550
    iget-wide v4, v2, LX/5SB;->b:J

    move-wide v2, v4

    .line 2310551
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    sget-object v3, LX/DHr;->TIMELINE_FRIENDS_NAVTILE:LX/DHr;

    invoke-virtual {v3}, LX/DHr;->name()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2310552
    :cond_2
    sget-object v0, LX/DHs;->SUGGESTIONS:LX/DHs;

    invoke-virtual {v0}, LX/DHs;->name()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 2310553
    :pswitch_3
    const-string v0, "https://m.facebook.com/profile.php?v=followers&id=%s"

    iget-object v1, p0, LX/G0x;->e:LX/5SB;

    .line 2310554
    iget-wide v4, v1, LX/5SB;->b:J

    move-wide v2, v4

    .line 2310555
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 2310556
    :pswitch_4
    sget-object v0, LX/0ax;->bL:Ljava/lang/String;

    iget-object v1, p0, LX/G0x;->e:LX/5SB;

    .line 2310557
    iget-wide v4, v1, LX/5SB;->b:J

    move-wide v2, v4

    .line 2310558
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/facebook/common/stringformat/StringFormatUtil;->formatStrLocaleSafe(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)V
    .locals 13

    .prologue
    .line 2310559
    iget-object v0, p0, LX/G0x;->d:LX/BQ9;

    iget-object v1, p0, LX/G0x;->e:LX/5SB;

    .line 2310560
    iget-wide v6, v1, LX/5SB;->b:J

    move-wide v2, v6

    .line 2310561
    iget-object v1, p0, LX/G0x;->e:LX/5SB;

    invoke-virtual {v1}, LX/5SB;->i()Z

    move-result v1

    iget-object v4, p0, LX/G0x;->f:LX/BQ1;

    invoke-virtual {v4}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v4

    iget-object v5, p0, LX/G0x;->f:LX/BQ1;

    invoke-virtual {v5}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v5

    invoke-static {v1, v4, v5}, LX/9lQ;->getRelationshipType(ZLcom/facebook/graphql/enums/GraphQLFriendshipStatus;Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;)LX/9lQ;

    move-result-object v1

    .line 2310562
    invoke-virtual {p1}, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->toString()Ljava/lang/String;

    move-result-object v6

    .line 2310563
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->UNSET_OR_UNRECOGNIZED_ENUM_VALUE:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-ne p1, v7, :cond_2

    .line 2310564
    const-string v6, "ACTIVITY_LOG"

    move-object v12, v6

    .line 2310565
    :goto_0
    const-string v7, "profile_nav_tile_click"

    const/4 v10, 0x0

    move-object v6, v0

    move-wide v8, v2

    move-object v11, v1

    invoke-static/range {v6 .. v11}, LX/BQ9;->a(LX/BQ9;Ljava/lang/String;JLjava/lang/String;LX/9lQ;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    move-result-object v6

    .line 2310566
    const-string v7, "nav_tile_type"

    invoke-virtual {v6, v7, v12}, Lcom/facebook/analytics/logger/HoneyClientEvent;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/facebook/analytics/logger/HoneyClientEvent;

    .line 2310567
    iget-object v7, v0, LX/BQ9;->b:LX/0Zb;

    invoke-interface {v7, v6}, LX/0Zb;->a(Lcom/facebook/analytics/HoneyAnalyticsEvent;)V

    .line 2310568
    iget-object v0, p0, LX/G0x;->b:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0gh;

    const-string v1, "tap_timeline_nav_item"

    invoke-virtual {v0, v1}, LX/0gh;->a(Ljava/lang/String;)LX/0gh;

    .line 2310569
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 2310570
    iget-object v7, p0, LX/G0x;->f:LX/BQ1;

    if-eqz v7, :cond_1

    .line 2310571
    const-string v7, "friendship_status"

    iget-object v8, p0, LX/G0x;->f:LX/BQ1;

    invoke-virtual {v8}, LX/BQ1;->C()Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2310572
    const-string v7, "subscribe_status"

    iget-object v8, p0, LX/G0x;->f:LX/BQ1;

    invoke-virtual {v8}, LX/BQ1;->E()Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;

    move-result-object v8

    invoke-virtual {v8}, Lcom/facebook/graphql/enums/GraphQLSubscribeStatus;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2310573
    const-string v7, "profile_name"

    iget-object v8, p0, LX/G0x;->f:LX/BQ1;

    invoke-virtual {v8}, LX/BQ1;->P()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2310574
    const-string v7, "first_name"

    iget-object v8, p0, LX/G0x;->f:LX/BQ1;

    invoke-virtual {v8}, LX/BQ1;->z()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2310575
    iget-object v7, p0, LX/G0x;->e:LX/5SB;

    if-eqz v7, :cond_0

    .line 2310576
    const-string v7, "session_id"

    iget-object v8, p0, LX/G0x;->e:LX/5SB;

    .line 2310577
    iget-object v9, v8, LX/5SB;->c:Ljava/lang/String;

    move-object v8, v9

    .line 2310578
    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2310579
    :cond_0
    sget-object v7, Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;->PHOTOS:Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;

    if-ne p1, v7, :cond_1

    .line 2310580
    const-string v7, "fullscreen_gallery_source"

    sget-object v8, LX/74S;->TIMELINE_PHOTOS_OF_USER:LX/74S;

    invoke-virtual {v8}, LX/74S;->name()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2310581
    iget-object v7, p0, LX/G0x;->e:LX/5SB;

    if-eqz v7, :cond_1

    .line 2310582
    const-string v7, "viewer_id"

    iget-object v8, p0, LX/G0x;->e:LX/5SB;

    .line 2310583
    iget-wide v10, v8, LX/5SB;->a:J

    move-wide v8, v10

    .line 2310584
    invoke-virtual {v6, v7, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 2310585
    const-string v7, "session_id"

    iget-object v8, p0, LX/G0x;->e:LX/5SB;

    .line 2310586
    iget-object v9, v8, LX/5SB;->d:Landroid/os/ParcelUuid;

    move-object v8, v9

    .line 2310587
    invoke-virtual {v8}, Landroid/os/ParcelUuid;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2310588
    :cond_1
    iget-object v7, p0, LX/G0x;->g:LX/G4m;

    invoke-static {v7, v6}, LX/G0z;->a(LX/G4m;Landroid/os/Bundle;)V

    .line 2310589
    move-object v1, v6

    .line 2310590
    iget-object v0, p0, LX/G0x;->a:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/17W;

    iget-object v2, p0, LX/G0x;->c:Landroid/content/Context;

    invoke-direct {p0, p1}, LX/G0x;->c(Lcom/facebook/graphql/enums/GraphQLTimelineAppSectionType;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v1, v4}, LX/17W;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;Ljava/util/Map;)Z

    .line 2310591
    return-void

    :cond_2
    move-object v12, v6

    goto/16 :goto_0
.end method
