.class public LX/FgX;
.super LX/7HO;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/7HO",
        "<",
        "Lcom/facebook/search/model/TypeaheadUnit;",
        ">;"
    }
.end annotation


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/8i9;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public b:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Cww;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public c:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0ad;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2270431
    invoke-direct {p0}, LX/7HO;-><init>()V

    .line 2270432
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2270433
    iput-object v0, p0, LX/FgX;->a:LX/0Ot;

    .line 2270434
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2270435
    iput-object v0, p0, LX/FgX;->b:LX/0Ot;

    .line 2270436
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2270437
    iput-object v0, p0, LX/FgX;->c:LX/0Ot;

    .line 2270438
    return-void
.end method

.method public static a(LX/FgX;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2270439
    iget-object v0, p0, LX/FgX;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8i9;

    invoke-virtual {v0, p1}, LX/8i9;->b(Ljava/lang/String;)LX/8i3;

    move-result-object v0

    .line 2270440
    invoke-virtual {v0, p2}, LX/8i3;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/7Hi;Ljava/lang/String;)Z
    .locals 7

    .prologue
    .line 2270441
    check-cast p1, Lcom/facebook/search/model/TypeaheadUnit;

    .line 2270442
    iget-object v0, p2, LX/7Hi;->a:LX/7B6;

    move-object v0, v0

    .line 2270443
    iget-object v0, v0, LX/7B6;->b:Ljava/lang/String;

    const/4 v4, 0x0

    .line 2270444
    instance-of v1, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    if-eqz v1, :cond_0

    .line 2270445
    check-cast p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2270446
    invoke-virtual {v0, p3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    .line 2270447
    :goto_0
    move v1, v1

    .line 2270448
    :goto_1
    move v0, v1

    .line 2270449
    return v0

    .line 2270450
    :cond_0
    instance-of v1, p1, Lcom/facebook/search/model/EntityTypeaheadUnit;

    if-eqz v1, :cond_3

    move-object v1, p1

    .line 2270451
    check-cast v1, Lcom/facebook/search/model/EntityTypeaheadUnit;

    .line 2270452
    iget-object v2, v1, Lcom/facebook/search/model/EntityTypeaheadUnit;->o:LX/0Px;

    move-object v2, v2

    .line 2270453
    if-eqz v2, :cond_c

    .line 2270454
    iget-object v2, v1, Lcom/facebook/search/model/EntityTypeaheadUnit;->o:LX/0Px;

    move-object v2, v2

    .line 2270455
    invoke-virtual {v2}, LX/0Px;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_c

    const/4 v2, 0x1

    :goto_2
    move v2, v2

    .line 2270456
    if-eqz v2, :cond_1

    .line 2270457
    iget-object v2, p0, LX/FgX;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8i9;

    invoke-virtual {v2, p3}, LX/8i9;->a(Ljava/lang/String;)LX/8i4;

    move-result-object v2

    .line 2270458
    iget-object v3, v1, Lcom/facebook/search/model/EntityTypeaheadUnit;->o:LX/0Px;

    move-object v1, v3

    .line 2270459
    iget-object v3, v2, LX/8i4;->b:LX/0Px;

    invoke-static {v1, v3}, LX/8i8;->a(Ljava/util/List;Ljava/util/List;)Z

    move-result v3

    move v1, v3

    .line 2270460
    goto :goto_1

    .line 2270461
    :cond_1
    iget-boolean v2, v1, Lcom/facebook/search/model/EntityTypeaheadUnit;->q:Z

    move v2, v2

    .line 2270462
    if-eqz v2, :cond_2

    .line 2270463
    iget-object v2, p0, LX/FgX;->a:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/8i9;

    .line 2270464
    iget-object v3, v2, LX/8i9;->f:Ljava/util/Map;

    invoke-interface {v3, p3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 2270465
    iget-object v3, v2, LX/8i9;->f:Ljava/util/Map;

    invoke-interface {v3, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8i5;

    .line 2270466
    :goto_3
    move-object v2, v3

    .line 2270467
    iget-object v3, v1, Lcom/facebook/search/model/EntityTypeaheadUnit;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2270468
    iget-object v5, v2, LX/8i5;->a:LX/7CV;

    invoke-virtual {v5, v3}, LX/7CV;->a(Ljava/lang/String;)LX/0Px;

    move-result-object v5

    iget-object v6, v2, LX/8i5;->b:LX/0Px;

    invoke-static {v5, v6}, LX/8i8;->a(Ljava/util/List;Ljava/util/List;)Z

    move-result v5

    move v2, v5

    .line 2270469
    if-eqz v2, :cond_2

    .line 2270470
    const/4 v1, 0x1

    goto :goto_1

    .line 2270471
    :cond_2
    iget-object v2, p0, LX/FgX;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget-short v3, LX/100;->bY:S

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, LX/FgX;->c:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, LX/0ad;

    sget-short v3, LX/100;->ci:S

    invoke-interface {v2, v3, v4}, LX/0ad;->a(SZ)Z

    move-result v2

    if-nez v2, :cond_3

    .line 2270472
    iget-object v2, v1, Lcom/facebook/search/model/EntityTypeaheadUnit;->b:Ljava/lang/String;

    move-object v1, v2

    .line 2270473
    invoke-static {p0, p3, v1}, LX/FgX;->a(LX/FgX;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    goto/16 :goto_1

    .line 2270474
    :cond_3
    iget-object v1, p0, LX/FgX;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8i9;

    invoke-virtual {v1, p3}, LX/8i9;->a(Ljava/lang/String;)LX/8i4;

    move-result-object v2

    .line 2270475
    iget-object v1, p0, LX/FgX;->b:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/Cwh;

    invoke-virtual {p1, v1}, Lcom/facebook/search/model/TypeaheadUnit;->a(LX/Cwh;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2270476
    invoke-virtual {v2, v1}, LX/8i4;->a(Ljava/lang/String;)Z

    move-result v1

    goto/16 :goto_1

    .line 2270477
    :cond_4
    invoke-virtual {p3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v3

    .line 2270478
    goto/16 :goto_0

    .line 2270479
    :cond_5
    invoke-virtual {p1}, Lcom/facebook/search/model/KeywordTypeaheadUnit;->a()Ljava/lang/String;

    move-result-object v4

    .line 2270480
    iget-object v1, p0, LX/FgX;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    sget-short v5, LX/100;->bY:S

    invoke-interface {v1, v5, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2270481
    iget-object v1, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->i:Lcom/facebook/search/api/model/GraphSearchTypeaheadEntityDataJson;

    move-object v1, v1

    .line 2270482
    if-eqz v1, :cond_6

    iget-object v1, p0, LX/FgX;->c:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/0ad;

    sget-short v5, LX/100;->ci:S

    invoke-interface {v1, v5, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2270483
    :cond_6
    invoke-static {p0, p3, v4}, LX/FgX;->a(LX/FgX;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 2270484
    :goto_4
    if-eqz v1, :cond_8

    move v1, v2

    .line 2270485
    goto/16 :goto_0

    .line 2270486
    :cond_7
    iget-object v1, p0, LX/FgX;->a:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/8i9;

    invoke-virtual {v1, p3}, LX/8i9;->a(Ljava/lang/String;)LX/8i4;

    move-result-object v1

    .line 2270487
    invoke-virtual {v1, v4}, LX/8i4;->a(Ljava/lang/String;)Z

    move-result v1

    move v1, v1

    .line 2270488
    goto :goto_4

    .line 2270489
    :cond_8
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v1

    .line 2270490
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    .line 2270491
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    .line 2270492
    iget v6, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->y:I

    move v6, v6

    .line 2270493
    iget p2, p1, Lcom/facebook/search/model/KeywordTypeaheadUnit;->y:I

    move p2, p2

    .line 2270494
    if-lez p2, :cond_a

    .line 2270495
    add-int/2addr v1, v6

    sub-int/2addr v1, v2

    .line 2270496
    if-ge v5, v1, :cond_9

    move v1, v3

    .line 2270497
    goto/16 :goto_0

    .line 2270498
    :cond_9
    invoke-virtual {p3, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p3

    .line 2270499
    invoke-virtual {v4, v6, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 2270500
    :goto_5
    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto/16 :goto_0

    .line 2270501
    :cond_a
    if-ge v5, v1, :cond_b

    move v1, v3

    .line 2270502
    goto/16 :goto_0

    .line 2270503
    :cond_b
    invoke-virtual {v4, v3, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 2270504
    :cond_d
    new-instance v5, LX/8i5;

    iget-object v3, v2, LX/8i9;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/7CV;

    iget-object v6, v2, LX/8i9;->c:LX/8i8;

    invoke-direct {v5, v3, v6, p3}, LX/8i5;-><init>(LX/7CV;LX/8i8;Ljava/lang/String;)V

    .line 2270505
    iget-object v3, v2, LX/8i9;->f:Ljava/util/Map;

    invoke-interface {v3, p3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v3, v5

    .line 2270506
    goto/16 :goto_3
.end method
