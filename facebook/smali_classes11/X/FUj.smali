.class public LX/FUj;
.super Lcom/facebook/fbui/widget/contentview/ContentView;
.source ""


# instance fields
.field public j:LX/17W;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public k:LX/FUf;
    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field

.field public l:LX/FUk;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/FUk;)V
    .locals 0

    .prologue
    .line 2249557
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;-><init>(Landroid/content/Context;)V

    .line 2249558
    const-class p1, LX/FUj;

    invoke-static {p1, p0}, LX/FUj;->a(Ljava/lang/Class;Landroid/view/View;)V

    .line 2249559
    iput-object p2, p0, LX/FUj;->l:LX/FUk;

    .line 2249560
    const p1, 0x7f0207fa

    invoke-virtual {p0, p1}, LX/FUj;->setBackgroundResource(I)V

    .line 2249561
    const p1, 0x7f020c23

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/layout/ViewGroupWithDraweeView;->setThumbnailResource(I)V

    .line 2249562
    sget-object p1, LX/6VF;->MEDIUM:LX/6VF;

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setThumbnailSize(LX/6VF;)V

    .line 2249563
    const p1, 0x7f08313c

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setTitleText(I)V

    .line 2249564
    const p1, 0x7f08313d

    invoke-virtual {p0, p1}, Lcom/facebook/fbui/widget/contentview/ContentView;->setSubtitleText(I)V

    .line 2249565
    new-instance p1, LX/FUi;

    invoke-direct {p1, p0}, LX/FUi;-><init>(LX/FUj;)V

    invoke-virtual {p0, p1}, LX/FUj;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2249566
    return-void
.end method

.method public static a(Ljava/lang/Class;Landroid/view/View;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;TT;)V"
        }
    .end annotation

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object p0

    check-cast p1, LX/FUj;

    invoke-static {p0}, LX/17W;->a(LX/0QB;)LX/17W;

    move-result-object v1

    check-cast v1, LX/17W;

    invoke-static {p0}, LX/FUf;->b(LX/0QB;)LX/FUf;

    move-result-object p0

    check-cast p0, LX/FUf;

    iput-object v1, p1, LX/FUj;->j:LX/17W;

    iput-object p0, p1, LX/FUj;->k:LX/FUf;

    return-void
.end method


# virtual methods
.method public setEntryPoint(LX/FUk;)V
    .locals 0

    .prologue
    .line 2249567
    iput-object p1, p0, LX/FUj;->l:LX/FUk;

    .line 2249568
    return-void
.end method
