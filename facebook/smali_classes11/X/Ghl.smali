.class public LX/Ghl;
.super LX/1S3;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field public static final a:LX/0Zi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Zi",
            "<",
            "LX/Ghj;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile c:LX/Ghl;


# instance fields
.field public b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/Ghm;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .prologue
    .line 2384758
    new-instance v0, LX/0Zi;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, LX/0Zi;-><init>(I)V

    sput-object v0, LX/Ghl;->a:LX/0Zi;

    return-void
.end method

.method public constructor <init>(LX/0Ot;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "LX/Ghm;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2384759
    invoke-direct {p0}, LX/1S3;-><init>()V

    .line 2384760
    iput-object p1, p0, LX/Ghl;->b:LX/0Ot;

    .line 2384761
    return-void
.end method

.method public static a(LX/0QB;)LX/Ghl;
    .locals 4

    .prologue
    .line 2384762
    sget-object v0, LX/Ghl;->c:LX/Ghl;

    if-nez v0, :cond_1

    .line 2384763
    const-class v1, LX/Ghl;

    monitor-enter v1

    .line 2384764
    :try_start_0
    sget-object v0, LX/Ghl;->c:LX/Ghl;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2384765
    if-eqz v2, :cond_0

    .line 2384766
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2384767
    new-instance v3, LX/Ghl;

    const/16 p0, 0x2309

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    invoke-direct {v3, p0}, LX/Ghl;-><init>(LX/0Ot;)V

    .line 2384768
    move-object v0, v3

    .line 2384769
    sput-object v0, LX/Ghl;->c:LX/Ghl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2384770
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2384771
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2384772
    :cond_1
    sget-object v0, LX/Ghl;->c:LX/Ghl;

    return-object v0

    .line 2384773
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2384774
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static onClick(LX/1X1;)LX/1dQ;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/1X1;",
            ")",
            "LX/1dQ",
            "<",
            "LX/3Ae;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2384775
    const v0, 0xbc85880

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, LX/1S3;->a(LX/1X1;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(LX/1De;LX/1X1;)LX/1Dg;
    .locals 7

    .prologue
    .line 2384776
    check-cast p2, LX/Ghk;

    .line 2384777
    iget-object v0, p0, LX/Ghl;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/Ghm;

    iget-object v1, p2, LX/Ghk;->a:LX/Ghu;

    iget-object v2, p2, LX/Ghk;->b:LX/Ghu;

    iget-object v3, p2, LX/Ghk;->c:LX/0Px;

    const/4 p2, 0x2

    .line 2384778
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v4, v5}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/Ghm;->a:LX/Ght;

    invoke-virtual {v5, p1}, LX/Ght;->c(LX/1De;)LX/Ghr;

    move-result-object v5

    invoke-virtual {v5, v1}, LX/Ghr;->a(LX/Ghu;)LX/Ghr;

    move-result-object v5

    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const/4 v6, 0x3

    const p0, 0x7f0b0062

    invoke-interface {v5, v6, p0}, LX/1Di;->g(II)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/Ghm;->a:LX/Ght;

    invoke-virtual {v5, p1}, LX/Ght;->c(LX/1De;)LX/Ghr;

    move-result-object v5

    invoke-virtual {v5, v2}, LX/Ghr;->a(LX/Ghu;)LX/Ghr;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1X5;)LX/1Dh;

    move-result-object v4

    .line 2384779
    invoke-static {p1}, LX/1n8;->a(LX/1De;)LX/1Dh;

    move-result-object v5

    invoke-interface {v5, p2}, LX/1Dh;->Q(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f020bf0

    invoke-interface {v5, v6}, LX/1Dh;->X(I)LX/1Dh;

    move-result-object v5

    const v6, 0x7f020bed

    invoke-interface {v5, v6}, LX/1Dh;->V(I)LX/1Dh;

    move-result-object v5

    const/16 v6, 0x8

    const p0, 0x7f0b0060

    invoke-interface {v5, v6, p0}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v5

    .line 2384780
    const v6, 0xbc85880

    const/4 p0, 0x0

    invoke-static {p1, v6, p0}, LX/1S3;->a(LX/1De;I[Ljava/lang/Object;)LX/1dQ;

    move-result-object v6

    move-object v6, v6

    .line 2384781
    invoke-interface {v5, v6}, LX/1Dh;->d(LX/1dQ;)LX/1Dh;

    move-result-object v5

    const v6, 0x7f0b0062

    invoke-interface {v4, p2, v6}, LX/1Dh;->u(II)LX/1Dh;

    move-result-object v4

    const v6, 0x3f19999a    # 0.6f

    invoke-interface {v4, v6}, LX/1Dh;->d(F)LX/1Dh;

    move-result-object v4

    invoke-interface {v5, v4}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    iget-object v5, v0, LX/Ghm;->b:LX/Ghp;

    const/4 v6, 0x0

    .line 2384782
    new-instance p0, LX/Gho;

    invoke-direct {p0, v5}, LX/Gho;-><init>(LX/Ghp;)V

    .line 2384783
    sget-object p2, LX/Ghp;->a:LX/0Zi;

    invoke-virtual {p2}, LX/0Zj;->a()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, LX/Ghn;

    .line 2384784
    if-nez p2, :cond_0

    .line 2384785
    new-instance p2, LX/Ghn;

    invoke-direct {p2}, LX/Ghn;-><init>()V

    .line 2384786
    :cond_0
    invoke-static {p2, p1, v6, v6, p0}, LX/Ghn;->a$redex0(LX/Ghn;LX/1De;IILX/Gho;)V

    .line 2384787
    move-object p0, p2

    .line 2384788
    move-object v6, p0

    .line 2384789
    move-object v5, v6

    .line 2384790
    iget-object v6, v5, LX/Ghn;->a:LX/Gho;

    iput-object v3, v6, LX/Gho;->a:LX/0Px;

    .line 2384791
    iget-object v6, v5, LX/Ghn;->d:Ljava/util/BitSet;

    const/4 p0, 0x0

    invoke-virtual {v6, p0}, Ljava/util/BitSet;->set(I)V

    .line 2384792
    move-object v5, v5

    .line 2384793
    invoke-virtual {v5}, LX/1X5;->c()LX/1Di;

    move-result-object v5

    const v6, 0x3ecccccd    # 0.4f

    invoke-interface {v5, v6}, LX/1Di;->a(F)LX/1Di;

    move-result-object v5

    invoke-interface {v4, v5}, LX/1Dh;->a(LX/1Di;)LX/1Dh;

    move-result-object v4

    invoke-interface {v4}, LX/1Di;->k()LX/1Dg;

    move-result-object v4

    move-object v0, v4

    .line 2384794
    return-object v0
.end method

.method public final a(LX/1dQ;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 2384795
    invoke-static {}, LX/1dS;->b()V

    .line 2384796
    iget v0, p1, LX/1dQ;->b:I

    .line 2384797
    packed-switch v0, :pswitch_data_0

    .line 2384798
    :goto_0
    return-object v2

    .line 2384799
    :pswitch_0
    check-cast p2, LX/3Ae;

    .line 2384800
    iget-object v0, p2, LX/3Ae;->a:Landroid/view/View;

    iget-object v1, p1, LX/1dQ;->a:LX/1X1;

    .line 2384801
    check-cast v1, LX/Ghk;

    .line 2384802
    iget-object v3, p0, LX/Ghl;->b:LX/0Ot;

    invoke-interface {v3}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/Ghm;

    iget-object p1, v1, LX/Ghk;->d:Ljava/lang/String;

    .line 2384803
    iget-object p2, v3, LX/Ghm;->c:LX/GiU;

    sget-object p0, LX/87b;->DASHBOARD:LX/87b;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p2, p1, p0, v1}, LX/GiU;->b(Ljava/lang/String;LX/87b;Landroid/content/Context;)V

    .line 2384804
    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xbc85880
        :pswitch_0
    .end packed-switch
.end method
