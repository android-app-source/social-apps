.class public LX/H96;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8Z;


# static fields
.field private static final a:I

.field private static final b:I


# instance fields
.field private c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/H8W;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lcom/facebook/content/SecureContextHelper;

.field private final f:Landroid/content/Context;

.field private final g:LX/E1i;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2433961
    const v0, 0x7f020837

    sput v0, LX/H96;->a:I

    .line 2433962
    const v0, 0x7f081509

    sput v0, LX/H96;->b:I

    return-void
.end method

.method public constructor <init>(Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;LX/0Ot;Lcom/facebook/content/SecureContextHelper;Landroid/content/Context;LX/E1i;)V
    .locals 0
    .param p1    # Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p4    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageActionData$Page;",
            "LX/0Ot",
            "<",
            "LX/H8W;",
            ">;",
            "Lcom/facebook/content/SecureContextHelper;",
            "Landroid/content/Context;",
            "LX/E1i;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2433947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2433948
    iput-object p1, p0, LX/H96;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    .line 2433949
    iput-object p2, p0, LX/H96;->d:LX/0Ot;

    .line 2433950
    iput-object p3, p0, LX/H96;->e:Lcom/facebook/content/SecureContextHelper;

    .line 2433951
    iput-object p4, p0, LX/H96;->f:Landroid/content/Context;

    .line 2433952
    iput-object p5, p0, LX/H96;->g:LX/E1i;

    .line 2433953
    return-void
.end method


# virtual methods
.method public final a()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2433958
    iget-object v0, p0, LX/H96;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->b()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AddressModel;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/H96;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v0}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->q()LX/1k1;

    move-result-object v0

    if-eqz v0, :cond_0

    move v5, v4

    .line 2433959
    :goto_0
    new-instance v0, LX/HA7;

    sget v2, LX/H96;->b:I

    sget v3, LX/H96;->a:I

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0

    :cond_0
    move v5, v1

    .line 2433960
    goto :goto_0
.end method

.method public final b()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2433963
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    sget v2, LX/H96;->b:I

    sget v3, LX/H96;->a:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0
.end method

.method public final c()V
    .locals 9

    .prologue
    .line 2433954
    iget-object v0, p0, LX/H96;->g:LX/E1i;

    iget-object v1, p0, LX/H96;->f:Landroid/content/Context;

    iget-object v2, p0, LX/H96;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->q()LX/1k1;

    move-result-object v2

    invoke-interface {v2}, LX/1k1;->a()D

    move-result-wide v2

    iget-object v4, p0, LX/H96;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v4}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->q()LX/1k1;

    move-result-object v4

    invoke-interface {v4}, LX/1k1;->b()D

    move-result-wide v4

    iget-object v6, p0, LX/H96;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->b()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AddressModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$AddressModel;->a()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, LX/H96;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v7}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->s()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, LX/H96;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v8}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v8

    invoke-virtual/range {v0 .. v8}, LX/E1i;->b(Landroid/content/Context;DDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)LX/Cfl;

    move-result-object v1

    .line 2433955
    iget-object v0, p0, LX/H96;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/H8W;

    sget-object v2, LX/9XI;->EVENT_TAPPED_GET_DIRECTION:LX/9XI;

    iget-object v3, p0, LX/H96;->c:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, LX/H8W;->a(LX/9X2;Ljava/lang/String;)V

    .line 2433956
    iget-object v0, p0, LX/H96;->e:Lcom/facebook/content/SecureContextHelper;

    iget-object v1, v1, LX/Cfl;->d:Landroid/content/Intent;

    iget-object v2, p0, LX/H96;->f:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2433957
    return-void
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2433946
    const/4 v0, 0x0

    return-object v0
.end method
