.class public LX/H8k;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/H8Z;


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I


# instance fields
.field private final d:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;"
        }
    .end annotation
.end field

.field public final f:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/intent_builder/IPageIdentityIntentBuilder;",
            ">;"
        }
    .end annotation
.end field

.field public final g:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/3iH;",
            ">;"
        }
    .end annotation
.end field

.field public final h:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;"
        }
    .end annotation
.end field

.field public final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;"
        }
    .end annotation
.end field

.field public j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

.field public k:Landroid/content/Context;

.field private l:LX/01T;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2433642
    const v0, 0x7f020855

    sput v0, LX/H8k;->a:I

    .line 2433643
    const v0, 0x7f081654

    sput v0, LX/H8k;->b:I

    .line 2433644
    const v0, 0x7f08183d

    sput v0, LX/H8k;->c:I

    return-void
.end method

.method public constructor <init>(LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/01T;Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;Landroid/content/Context;)V
    .locals 0
    .param p5    # LX/0Ot;
        .annotation runtime Lcom/facebook/common/executors/ForUiThread;
        .end annotation
    .end param
    .param p7    # LX/01T;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p8    # Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p9    # Landroid/content/Context;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0Ot",
            "<",
            "Lcom/facebook/content/SecureContextHelper;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/9XE;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/pages/common/intent_builder/IPageIdentityIntentBuilder;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/3iH;",
            ">;",
            "LX/0Ot",
            "<",
            "Ljava/util/concurrent/Executor;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0kL;",
            ">;",
            "LX/01T;",
            "Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLInterfaces$PageActionData$Page;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2433619
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2433620
    iput-object p1, p0, LX/H8k;->d:LX/0Ot;

    .line 2433621
    iput-object p2, p0, LX/H8k;->e:LX/0Ot;

    .line 2433622
    iput-object p3, p0, LX/H8k;->f:LX/0Ot;

    .line 2433623
    iput-object p4, p0, LX/H8k;->g:LX/0Ot;

    .line 2433624
    iput-object p5, p0, LX/H8k;->h:LX/0Ot;

    .line 2433625
    iput-object p6, p0, LX/H8k;->i:LX/0Ot;

    .line 2433626
    iput-object p7, p0, LX/H8k;->l:LX/01T;

    .line 2433627
    iput-object p8, p0, LX/H8k;->j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    .line 2433628
    iput-object p9, p0, LX/H8k;->k:Landroid/content/Context;

    .line 2433629
    return-void
.end method


# virtual methods
.method public final a()LX/HA7;
    .locals 6

    .prologue
    .line 2433639
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    iget-object v2, p0, LX/H8k;->l:LX/01T;

    sget-object v3, LX/01T;->PAA:LX/01T;

    if-ne v2, v3, :cond_0

    sget v2, LX/H8k;->b:I

    :goto_0
    sget v3, LX/H8k;->a:I

    const/4 v4, 0x1

    .line 2433640
    iget-object v5, p0, LX/H8k;->j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->E()LX/0Px;

    move-result-object v5

    invoke-static {v5}, LX/8A4;->c(LX/0Px;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, LX/H8k;->j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->s()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    iget-object v5, p0, LX/H8k;->j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->x()Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$ProfilePictureModel;

    move-result-object v5

    invoke-virtual {v5}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel$ProfilePictureModel;->a()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    const/4 v5, 0x1

    :goto_1
    move v5, v5

    .line 2433641
    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0

    :cond_0
    sget v2, LX/H8k;->c:I

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public final b()LX/HA7;
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 2433645
    new-instance v0, LX/HA7;

    const/4 v1, 0x0

    iget-object v2, p0, LX/H8k;->l:LX/01T;

    sget-object v3, LX/01T;->PAA:LX/01T;

    if-ne v2, v3, :cond_0

    sget v2, LX/H8k;->b:I

    :goto_0
    sget v3, LX/H8k;->a:I

    move v5, v4

    invoke-direct/range {v0 .. v5}, LX/HA7;-><init>(IIIIZ)V

    return-object v0

    :cond_0
    sget v2, LX/H8k;->c:I

    goto :goto_0
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 2433631
    iget-object v0, p0, LX/H8k;->e:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9XE;

    sget-object v1, LX/9X4;->EVENT_ADMIN_ADD_EVENT:LX/9X4;

    iget-object v2, p0, LX/H8k;->j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v2}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, LX/9XE;->b(LX/9X2;J)V

    .line 2433632
    iget-object v0, p0, LX/H8k;->l:LX/01T;

    sget-object v1, LX/01T;->PAA:LX/01T;

    if-ne v0, v1, :cond_0

    .line 2433633
    new-instance v0, LX/H8i;

    invoke-direct {v0, p0}, LX/H8i;-><init>(LX/H8k;)V

    .line 2433634
    new-instance v2, LX/4At;

    iget-object v1, p0, LX/H8k;->k:Landroid/content/Context;

    const v3, 0x7f0817c1

    invoke-direct {v2, v1, v3}, LX/4At;-><init>(Landroid/content/Context;I)V

    .line 2433635
    iget-object v1, p0, LX/H8k;->g:LX/0Ot;

    invoke-interface {v1}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, LX/3iH;

    iget-object v3, p0, LX/H8k;->j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v3}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/H8j;

    invoke-direct {v4, p0, v0, v2}, LX/H8j;-><init>(LX/H8k;LX/0Rl;LX/4At;)V

    iget-object v2, p0, LX/H8k;->h:LX/0Ot;

    invoke-interface {v2}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    invoke-virtual {v1, v3, v4, v2}, LX/3iH;->a(Ljava/lang/String;LX/8E8;Ljava/util/concurrent/Executor;)V

    .line 2433636
    :goto_0
    return-void

    .line 2433637
    :cond_0
    iget-object v0, p0, LX/H8k;->f:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/CSL;

    iget-object v1, p0, LX/H8k;->j:Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;

    invoke-virtual {v1}, Lcom/facebook/pages/data/graphql/actionchannel/PageActionDataGraphQLModels$PageActionDataModel$PageModel;->o()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    const-string v1, "pages_identity"

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lcom/facebook/events/common/ActionMechanism;->PAGES_ACTION_BAR:Lcom/facebook/events/common/ActionMechanism;

    invoke-virtual {v0, v2, v3, v1, v4}, LX/CSL;->a(JLjava/lang/String;Lcom/facebook/events/common/ActionMechanism;)Landroid/content/Intent;

    move-result-object v1

    .line 2433638
    iget-object v0, p0, LX/H8k;->d:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    iget-object v2, p0, LX/H8k;->k:Landroid/content/Context;

    invoke-interface {v0, v1, v2}, Lcom/facebook/content/SecureContextHelper;->a(Landroid/content/Intent;Landroid/content/Context;)V

    goto :goto_0
.end method

.method public final d()LX/0Px;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "LX/0Px",
            "<",
            "LX/H8E;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2433630
    const/4 v0, 0x0

    return-object v0
.end method
