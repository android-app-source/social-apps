.class public LX/FsN;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/FsN;


# instance fields
.field private final a:LX/FsP;

.field private final b:LX/0Sg;

.field public final c:LX/0tX;

.field private final d:LX/82T;


# direct methods
.method public constructor <init>(LX/FsP;LX/0Sg;LX/0tX;LX/82T;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2296788
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2296789
    iput-object p1, p0, LX/FsN;->a:LX/FsP;

    .line 2296790
    iput-object p2, p0, LX/FsN;->b:LX/0Sg;

    .line 2296791
    iput-object p3, p0, LX/FsN;->c:LX/0tX;

    .line 2296792
    iput-object p4, p0, LX/FsN;->d:LX/82T;

    .line 2296793
    return-void
.end method

.method private a(Lcom/facebook/common/callercontext/CallerContext;LX/0v6;)LX/1Zp;
    .locals 7
    .param p1    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/0v6;",
            ")",
            "LX/1Zp",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/timeline/protocol/FetchTimelineHeaderGraphQLModels$ViewerTopFriendsQueryModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2296843
    new-instance v3, LX/5wJ;

    invoke-direct {v3}, LX/5wJ;-><init>()V

    move-object v3, v3

    .line 2296844
    const-string v4, "num_top_friends"

    const/16 v5, 0xa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v3

    check-cast v3, LX/5wJ;

    move-object v3, v3

    .line 2296845
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    const-string v4, "timeline_fetch_header"

    invoke-static {v4}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v4

    .line 2296846
    iput-object v4, v3, LX/0zO;->d:Ljava/util/Set;

    .line 2296847
    move-object v3, v3

    .line 2296848
    iput-object p1, v3, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2296849
    move-object v3, v3

    .line 2296850
    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v3, v4}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->a:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    const-wide/32 v5, 0x3f480

    invoke-virtual {v3, v5, v6}, LX/0zO;->a(J)LX/0zO;

    move-result-object v3

    .line 2296851
    move-object v0, v3

    .line 2296852
    invoke-virtual {p2, v0}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2296853
    new-instance v2, LX/1Zp;

    invoke-direct {v2, v1, v0}, LX/1Zp;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0zO;)V

    return-object v2
.end method

.method private a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;Lcom/facebook/common/callercontext/CallerContext;LX/0v6;)LX/1Zp;
    .locals 11
    .param p2    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;",
            "Lcom/facebook/common/callercontext/CallerContext;",
            "LX/0v6;",
            ")",
            "LX/1Zp",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/querybuilder/common/CommonGraphQLModels$DefaultBigProfilePictureFieldsModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2296831
    invoke-static {}, LX/5wK;->b()LX/5wG;

    move-result-object v5

    const-string v6, "profile_id"

    .line 2296832
    iget-wide v9, p1, Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;->a:J

    move-wide v7, v9

    .line 2296833
    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v5

    check-cast v5, LX/5wG;

    move-object v3, v5

    .line 2296834
    invoke-static {v3}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v3

    const-string v4, "timeline_fetch_header"

    invoke-static {v4}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v4

    .line 2296835
    iput-object v4, v3, LX/0zO;->d:Ljava/util/Set;

    .line 2296836
    move-object v3, v3

    .line 2296837
    iput-object p2, v3, LX/0zO;->e:Lcom/facebook/common/callercontext/CallerContext;

    .line 2296838
    move-object v3, v3

    .line 2296839
    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v3, v4}, LX/0zO;->a(Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v3

    sget-object v4, LX/0zS;->c:LX/0zS;

    invoke-virtual {v3, v4}, LX/0zO;->a(LX/0zS;)LX/0zO;

    move-result-object v3

    .line 2296840
    move-object v0, v3

    .line 2296841
    invoke-virtual {p3, v0}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v1

    .line 2296842
    new-instance v2, LX/1Zp;

    invoke-direct {v2, v1, v0}, LX/1Zp;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0zO;)V

    return-object v2
.end method

.method public static a(LX/0QB;)LX/FsN;
    .locals 7

    .prologue
    .line 2296818
    sget-object v0, LX/FsN;->e:LX/FsN;

    if-nez v0, :cond_1

    .line 2296819
    const-class v1, LX/FsN;

    monitor-enter v1

    .line 2296820
    :try_start_0
    sget-object v0, LX/FsN;->e:LX/FsN;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2296821
    if-eqz v2, :cond_0

    .line 2296822
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2296823
    new-instance p0, LX/FsN;

    invoke-static {v0}, LX/FsP;->a(LX/0QB;)LX/FsP;

    move-result-object v3

    check-cast v3, LX/FsP;

    invoke-static {v0}, LX/0Sg;->a(LX/0QB;)LX/0Sg;

    move-result-object v4

    check-cast v4, LX/0Sg;

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v5

    check-cast v5, LX/0tX;

    invoke-static {v0}, LX/82T;->a(LX/0QB;)LX/82T;

    move-result-object v6

    check-cast v6, LX/82T;

    invoke-direct {p0, v3, v4, v5, v6}, LX/FsN;-><init>(LX/FsP;LX/0Sg;LX/0tX;LX/82T;)V

    .line 2296824
    move-object v0, p0

    .line 2296825
    sput-object v0, LX/FsN;->e:LX/FsN;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2296826
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2296827
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2296828
    :cond_1
    sget-object v0, LX/FsN;->e:LX/FsN;

    return-object v0

    .line 2296829
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2296830
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public static a(LX/36O;)Z
    .locals 1
    .param p0    # LX/36O;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2296854
    if-eqz p0, :cond_1

    invoke-interface {p0}, LX/36O;->cp_()LX/1Fb;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p0}, LX/36O;->cp_()LX/1Fb;

    move-result-object v0

    invoke-interface {v0}, LX/1Fb;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    move v0, v0

    .line 2296855
    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;LX/0zS;Lcom/facebook/common/callercontext/CallerContext;)LX/1Zp;
    .locals 2
    .param p3    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    .line 2296814
    iget-object v0, p0, LX/FsN;->a:LX/FsP;

    sget-object v1, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, p1, p2, p3, v1}, LX/FsP;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;LX/0zS;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 2296815
    iget-object v1, p0, LX/FsN;->c:LX/0tX;

    invoke-virtual {v1, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v0

    .line 2296816
    iget-object v1, p0, LX/FsN;->b:LX/0Sg;

    invoke-virtual {v1, v0}, LX/0Sg;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 2296817
    return-object v0
.end method

.method public final a(LX/0v6;Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;LX/36O;Lcom/facebook/common/callercontext/CallerContext;)LX/FsE;
    .locals 6
    .param p3    # LX/36O;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param
    .param p4    # Lcom/facebook/common/callercontext/CallerContext;
        .annotation build Ljavax/annotation/Nullable;
        .end annotation
    .end param

    .prologue
    const/4 v1, 0x0

    .line 2296803
    iget-object v0, p0, LX/FsN;->a:LX/FsP;

    sget-object v2, LX/0zS;->b:LX/0zS;

    sget-object v3, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, p2, v2, p4, v3}, LX/FsP;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;LX/0zS;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v0

    .line 2296804
    iget-object v2, p0, LX/FsN;->c:LX/0tX;

    invoke-virtual {v2, v0}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v2

    .line 2296805
    iget-object v0, p0, LX/FsN;->a:LX/FsP;

    sget-object v3, LX/0zS;->d:LX/0zS;

    sget-object v4, Lcom/facebook/http/interfaces/RequestPriority;->INTERACTIVE:Lcom/facebook/http/interfaces/RequestPriority;

    invoke-virtual {v0, p2, v3, p4, v4}, LX/FsP;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;LX/0zS;Lcom/facebook/common/callercontext/CallerContext;Lcom/facebook/http/interfaces/RequestPriority;)LX/0zO;

    move-result-object v3

    .line 2296806
    invoke-virtual {p1, v3}, LX/0v6;->b(LX/0zO;)Lcom/google/common/util/concurrent/ListenableFuture;

    move-result-object v4

    .line 2296807
    invoke-static {p3}, LX/FsN;->a(LX/36O;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2296808
    invoke-direct {p0, p2, p4, p1}, LX/FsN;->a(Lcom/facebook/timeline/protocol/FetchTimelineHeaderParams;Lcom/facebook/common/callercontext/CallerContext;LX/0v6;)LX/1Zp;

    move-result-object v0

    .line 2296809
    :goto_0
    iget-object v5, p0, LX/FsN;->d:LX/82T;

    invoke-virtual {v5}, LX/82T;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2296810
    invoke-direct {p0, p4, p1}, LX/FsN;->a(Lcom/facebook/common/callercontext/CallerContext;LX/0v6;)LX/1Zp;

    move-result-object v1

    .line 2296811
    :cond_0
    new-instance v5, LX/1Zp;

    invoke-direct {v5, v4, v3}, LX/1Zp;-><init>(Lcom/google/common/util/concurrent/ListenableFuture;LX/0zO;)V

    .line 2296812
    new-instance v3, LX/FsE;

    invoke-direct {v3, v2, v5, v0, v1}, LX/FsE;-><init>(LX/1Zp;LX/1Zp;LX/1Zp;LX/1Zp;)V

    return-object v3

    :cond_1
    move-object v0, v1

    .line 2296813
    goto :goto_0
.end method

.method public final a(LX/FsE;)V
    .locals 2

    .prologue
    .line 2296794
    iget-object v0, p1, LX/FsE;->a:LX/1Zp;

    if-eqz v0, :cond_0

    .line 2296795
    iget-object v0, p0, LX/FsN;->b:LX/0Sg;

    iget-object v1, p1, LX/FsE;->a:LX/1Zp;

    invoke-virtual {v0, v1}, LX/0Sg;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 2296796
    :cond_0
    iget-object v0, p1, LX/FsE;->b:LX/1Zp;

    if-eqz v0, :cond_1

    .line 2296797
    iget-object v0, p0, LX/FsN;->b:LX/0Sg;

    iget-object v1, p1, LX/FsE;->b:LX/1Zp;

    invoke-virtual {v0, v1}, LX/0Sg;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 2296798
    :cond_1
    iget-object v0, p1, LX/FsE;->c:LX/1Zp;

    if-eqz v0, :cond_2

    .line 2296799
    iget-object v0, p0, LX/FsN;->b:LX/0Sg;

    iget-object v1, p1, LX/FsE;->c:LX/1Zp;

    invoke-virtual {v0, v1}, LX/0Sg;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 2296800
    :cond_2
    iget-object v0, p1, LX/FsE;->d:LX/1Zp;

    if-eqz v0, :cond_3

    .line 2296801
    iget-object v0, p0, LX/FsN;->b:LX/0Sg;

    iget-object v1, p1, LX/FsE;->d:LX/1Zp;

    invoke-virtual {v0, v1}, LX/0Sg;->a(Lcom/google/common/util/concurrent/ListenableFuture;)V

    .line 2296802
    :cond_3
    return-void
.end method
