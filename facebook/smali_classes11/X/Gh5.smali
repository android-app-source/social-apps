.class public final LX/Gh5;
.super Ljava/lang/Object;
.source ""


# direct methods
.method public static a(LX/15w;LX/186;)I
    .locals 20

    .prologue
    .line 2383165
    const/4 v14, 0x0

    .line 2383166
    const/4 v13, 0x0

    .line 2383167
    const/4 v12, 0x0

    .line 2383168
    const/4 v11, 0x0

    .line 2383169
    const/4 v10, 0x0

    .line 2383170
    const/4 v9, 0x0

    .line 2383171
    const/4 v8, 0x0

    .line 2383172
    const-wide/16 v6, 0x0

    .line 2383173
    const/4 v5, 0x0

    .line 2383174
    const/4 v4, 0x0

    .line 2383175
    const/4 v3, 0x0

    .line 2383176
    const/4 v2, 0x0

    .line 2383177
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->START_OBJECT:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_e

    .line 2383178
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2383179
    const/4 v2, 0x0

    .line 2383180
    :goto_0
    return v2

    .line 2383181
    :cond_0
    invoke-virtual/range {p0 .. p0}, LX/15w;->f()LX/15w;

    .line 2383182
    :cond_1
    :goto_1
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    move-result-object v15

    sget-object v16, LX/15z;->END_OBJECT:LX/15z;

    move-object/from16 v0, v16

    if-eq v15, v0, :cond_9

    .line 2383183
    invoke-virtual/range {p0 .. p0}, LX/15w;->i()Ljava/lang/String;

    move-result-object v15

    .line 2383184
    invoke-virtual/range {p0 .. p0}, LX/15w;->c()LX/15z;

    .line 2383185
    invoke-virtual/range {p0 .. p0}, LX/15w;->g()LX/15z;

    move-result-object v16

    sget-object v17, LX/15z;->VALUE_NULL:LX/15z;

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    if-eq v0, v1, :cond_1

    if-eqz v15, :cond_1

    .line 2383186
    const-string v16, "active_team_with_ball"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_2

    .line 2383187
    invoke-static/range {p0 .. p1}, LX/Gh4;->a(LX/15w;LX/186;)I

    move-result v14

    goto :goto_1

    .line 2383188
    :cond_2
    const-string v16, "first_team_score"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_3

    .line 2383189
    const/4 v7, 0x1

    .line 2383190
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v13

    goto :goto_1

    .line 2383191
    :cond_3
    const-string v16, "has_match_started"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_4

    .line 2383192
    const/4 v6, 0x1

    .line 2383193
    invoke-virtual/range {p0 .. p0}, LX/15w;->H()Z

    move-result v12

    goto :goto_1

    .line 2383194
    :cond_4
    const-string v16, "id"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_5

    .line 2383195
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 2383196
    :cond_5
    const-string v16, "second_team_score"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_6

    .line 2383197
    const/4 v3, 0x1

    .line 2383198
    invoke-virtual/range {p0 .. p0}, LX/15w;->E()I

    move-result v10

    goto :goto_1

    .line 2383199
    :cond_6
    const-string v16, "status"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_7

    .line 2383200
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, LX/186;->b(Ljava/lang/String;)I

    move-result v9

    goto :goto_1

    .line 2383201
    :cond_7
    const-string v16, "status_text"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_8

    .line 2383202
    invoke-virtual/range {p0 .. p0}, LX/15w;->o()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, LX/186;->b(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_1

    .line 2383203
    :cond_8
    const-string v16, "updated_time"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_0

    .line 2383204
    const/4 v2, 0x1

    .line 2383205
    invoke-virtual/range {p0 .. p0}, LX/15w;->F()J

    move-result-wide v4

    goto/16 :goto_1

    .line 2383206
    :cond_9
    const/16 v15, 0x8

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, LX/186;->c(I)V

    .line 2383207
    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v15, v14}, LX/186;->b(II)V

    .line 2383208
    if-eqz v7, :cond_a

    .line 2383209
    const/4 v7, 0x1

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v13, v14}, LX/186;->a(III)V

    .line 2383210
    :cond_a
    if-eqz v6, :cond_b

    .line 2383211
    const/4 v6, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v12}, LX/186;->a(IZ)V

    .line 2383212
    :cond_b
    const/4 v6, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v11}, LX/186;->b(II)V

    .line 2383213
    if-eqz v3, :cond_c

    .line 2383214
    const/4 v3, 0x4

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v10, v6}, LX/186;->a(III)V

    .line 2383215
    :cond_c
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v9}, LX/186;->b(II)V

    .line 2383216
    const/4 v3, 0x6

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v8}, LX/186;->b(II)V

    .line 2383217
    if-eqz v2, :cond_d

    .line 2383218
    const/4 v3, 0x7

    const-wide/16 v6, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, LX/186;->a(IJJ)V

    .line 2383219
    :cond_d
    invoke-virtual/range {p1 .. p1}, LX/186;->d()I

    move-result v2

    goto/16 :goto_0

    :cond_e
    move/from16 v18, v4

    move/from16 v19, v5

    move-wide v4, v6

    move/from16 v6, v18

    move/from16 v7, v19

    goto/16 :goto_1
.end method

.method public static a(LX/15i;ILX/0nX;LX/0my;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    .line 2383220
    invoke-virtual {p2}, LX/0nX;->f()V

    .line 2383221
    invoke-virtual {p0, p1, v2}, LX/15i;->g(II)I

    move-result v0

    .line 2383222
    if-eqz v0, :cond_0

    .line 2383223
    const-string v1, "active_team_with_ball"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383224
    invoke-static {p0, v0, p2}, LX/Gh4;->a(LX/15i;ILX/0nX;)V

    .line 2383225
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2383226
    if-eqz v0, :cond_1

    .line 2383227
    const-string v1, "first_team_score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383228
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2383229
    :cond_1
    const/4 v0, 0x2

    invoke-virtual {p0, p1, v0}, LX/15i;->b(II)Z

    move-result v0

    .line 2383230
    if-eqz v0, :cond_2

    .line 2383231
    const-string v1, "has_match_started"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383232
    invoke-virtual {p2, v0}, LX/0nX;->a(Z)V

    .line 2383233
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2383234
    if-eqz v0, :cond_3

    .line 2383235
    const-string v1, "id"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383236
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2383237
    :cond_3
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0, v2}, LX/15i;->a(III)I

    move-result v0

    .line 2383238
    if-eqz v0, :cond_4

    .line 2383239
    const-string v1, "second_team_score"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383240
    invoke-virtual {p2, v0}, LX/0nX;->b(I)V

    .line 2383241
    :cond_4
    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2383242
    if-eqz v0, :cond_5

    .line 2383243
    const-string v1, "status"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383244
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2383245
    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, p1, v0}, LX/15i;->d(II)Ljava/lang/String;

    move-result-object v0

    .line 2383246
    if-eqz v0, :cond_6

    .line 2383247
    const-string v1, "status_text"

    invoke-virtual {p2, v1}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383248
    invoke-virtual {p2, v0}, LX/0nX;->b(Ljava/lang/String;)V

    .line 2383249
    :cond_6
    const/4 v0, 0x7

    invoke-virtual {p0, p1, v0, v4, v5}, LX/15i;->a(IIJ)J

    move-result-wide v0

    .line 2383250
    cmp-long v2, v0, v4

    if-eqz v2, :cond_7

    .line 2383251
    const-string v2, "updated_time"

    invoke-virtual {p2, v2}, LX/0nX;->a(Ljava/lang/String;)V

    .line 2383252
    invoke-virtual {p2, v0, v1}, LX/0nX;->a(J)V

    .line 2383253
    :cond_7
    invoke-virtual {p2}, LX/0nX;->g()V

    .line 2383254
    return-void
.end method
