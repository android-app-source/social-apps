.class public final LX/HBT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/8E8;


# instance fields
.field public final synthetic a:Landroid/view/View;

.field public final synthetic b:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

.field public final synthetic c:LX/4At;

.field public final synthetic d:LX/HBU;


# direct methods
.method public constructor <init>(LX/HBU;Landroid/view/View;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;LX/4At;)V
    .locals 0

    .prologue
    .line 2437199
    iput-object p1, p0, LX/HBT;->d:LX/HBU;

    iput-object p2, p0, LX/HBT;->a:Landroid/view/View;

    iput-object p3, p0, LX/HBT;->b:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    iput-object p4, p0, LX/HBT;->c:LX/4At;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2437200
    iget-object v0, p0, LX/HBT;->d:LX/HBU;

    iget-object v0, v0, LX/HBU;->a:Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2437201
    iget-object v0, p0, LX/HBT;->c:LX/4At;

    invoke-virtual {v0}, LX/4At;->beginShowingProgress()V

    .line 2437202
    :cond_0
    return-void
.end method

.method public final a(Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 3

    .prologue
    .line 2437203
    iget-object v0, p0, LX/HBT;->d:LX/HBU;

    iget-object v0, v0, LX/HBU;->a:Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;

    iget-object v1, p0, LX/HBT;->a:Landroid/view/View;

    iget-object v2, p0, LX/HBT;->b:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    .line 2437204
    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->a$redex0(Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;Landroid/view/View;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 2437205
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 2437206
    iget-object v0, p0, LX/HBT;->d:LX/HBU;

    iget-object v0, v0, LX/HBU;->a:Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2437207
    iget-object v0, p0, LX/HBT;->c:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2437208
    iget-object v0, p0, LX/HBT;->d:LX/HBU;

    iget-object v0, v0, LX/HBU;->a:Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;

    iget-object v0, v0, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->f:LX/0kL;

    new-instance v1, LX/27k;

    iget-object v2, p0, LX/HBT;->d:LX/HBU;

    iget-object v2, v2, LX/HBU;->a:Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;

    invoke-virtual {v2}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f081829

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, LX/27k;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1}, LX/0kL;->a(LX/27k;)LX/27l;

    .line 2437209
    :cond_0
    return-void
.end method

.method public final b(Lcom/facebook/auth/viewercontext/ViewerContext;)V
    .locals 3

    .prologue
    .line 2437210
    iget-object v0, p0, LX/HBT;->d:LX/HBU;

    iget-object v0, v0, LX/HBU;->a:Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;

    invoke-virtual {v0}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2437211
    iget-object v0, p0, LX/HBT;->c:LX/4At;

    invoke-virtual {v0}, LX/4At;->stopShowingProgress()V

    .line 2437212
    iget-object v0, p0, LX/HBT;->d:LX/HBU;

    iget-object v0, v0, LX/HBU;->a:Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;

    iget-object v1, p0, LX/HBT;->a:Landroid/view/View;

    iget-object v2, p0, LX/HBT;->b:Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;

    .line 2437213
    invoke-static {v0, v1, v2, p1}, Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;->a$redex0(Lcom/facebook/pages/common/contextitems/card/PageIdentityContextItemsHeaderCardView;Landroid/view/View;Lcom/facebook/entitycards/contextitems/graphql/ContextItemsQueryModels$ContextItemFieldsModel;Lcom/facebook/auth/viewercontext/ViewerContext;)V

    .line 2437214
    :cond_0
    return-void
.end method
