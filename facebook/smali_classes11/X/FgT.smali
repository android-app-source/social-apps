.class public LX/FgT;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/1qM;


# instance fields
.field public final a:LX/11H;

.field public final b:LX/9z9;


# direct methods
.method public constructor <init>(LX/11H;LX/9z9;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2270330
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2270331
    iput-object p1, p0, LX/FgT;->a:LX/11H;

    .line 2270332
    iput-object p2, p0, LX/FgT;->b:LX/9z9;

    .line 2270333
    return-void
.end method

.method public static b(LX/0QB;)LX/FgT;
    .locals 3

    .prologue
    .line 2270334
    new-instance v2, LX/FgT;

    invoke-static {p0}, Lcom/facebook/http/protocol/SingleMethodRunnerImpl;->a(LX/0QB;)Lcom/facebook/http/protocol/SingleMethodRunnerImpl;

    move-result-object v0

    check-cast v0, LX/11H;

    invoke-static {p0}, LX/9z9;->a(LX/0QB;)LX/9z9;

    move-result-object v1

    check-cast v1, LX/9z9;

    invoke-direct {v2, v0, v1}, LX/FgT;-><init>(LX/11H;LX/9z9;)V

    .line 2270335
    return-object v2
.end method


# virtual methods
.method public final handleOperation(LX/1qK;)Lcom/facebook/fbservice/service/OperationResult;
    .locals 4

    .prologue
    .line 2270336
    iget-object v0, p1, LX/1qK;->mType:Ljava/lang/String;

    move-object v0, v0

    .line 2270337
    const-string v1, "graph_search_query_result_data"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2270338
    iget-object v0, p1, LX/1qK;->mBundle:Landroid/os/Bundle;

    move-object v0, v0

    .line 2270339
    const-string v1, "FetchGraphSearchResultDataParams"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/facebook/search/protocol/FetchGraphSearchResultDataParams;

    .line 2270340
    iget-object v1, p0, LX/FgT;->a:LX/11H;

    iget-object v2, p0, LX/FgT;->b:LX/9z9;

    invoke-virtual {v1, v2, v0}, LX/11H;->a(LX/0e6;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/facebook/fbservice/service/OperationResult;->forSuccess(Ljava/lang/Object;)Lcom/facebook/fbservice/service/OperationResult;

    move-result-object v0

    move-object v0, v0

    .line 2270341
    return-object v0

    .line 2270342
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown operation type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
