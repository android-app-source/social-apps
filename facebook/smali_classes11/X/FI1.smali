.class public LX/FI1;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0QI;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0QI",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/concurrent/Future;",
            ">;"
        }
    .end annotation

    .annotation build Ljavax/annotation/concurrent/GuardedBy;
        value = "this"
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 2221346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2221347
    invoke-static {}, LX/0QN;->newBuilder()LX/0QN;

    move-result-object v0

    const-wide/32 v2, 0x7b98a000

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, LX/0QN;->a(JLjava/util/concurrent/TimeUnit;)LX/0QN;

    move-result-object v0

    invoke-virtual {v0}, LX/0QN;->q()LX/0QI;

    move-result-object v0

    iput-object v0, p0, LX/FI1;->a:LX/0QI;

    .line 2221348
    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;Ljava/util/concurrent/Future;)V
    .locals 1

    .prologue
    .line 2221349
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/FI1;->c(Ljava/lang/String;)V

    .line 2221350
    iget-object v0, p0, LX/FI1;->a:LX/0QI;

    invoke-interface {v0, p1, p2}, LX/0QI;->a(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2221351
    monitor-exit p0

    return-void

    .line 2221352
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 2221353
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FI1;->a:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    .line 2221354
    if-eqz v0, :cond_0

    .line 2221355
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2221356
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2221357
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2221358
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, LX/FI1;->c(Ljava/lang/String;)V

    .line 2221359
    iget-object v0, p0, LX/FI1;->a:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->b(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2221360
    monitor-exit p0

    return-void

    .line 2221361
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2221362
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, LX/FI1;->a:LX/0QI;

    invoke-interface {v0, p1}, LX/0QI;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    .line 2221363
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2221364
    new-instance v0, Ljava/util/concurrent/CancellationException;

    invoke-direct {v0}, Ljava/util/concurrent/CancellationException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2221365
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 2221366
    :cond_0
    monitor-exit p0

    return-void
.end method
