.class public final LX/FRc;
.super LX/0Vd;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0Vd",
        "<",
        "Lcom/facebook/payments/auth/pin/model/PaymentPin;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic a:LX/FRt;

.field public final synthetic b:LX/6zj;

.field public final synthetic c:Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;

.field public final synthetic d:LX/FRg;


# direct methods
.method public constructor <init>(LX/FRg;LX/FRt;LX/6zj;Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;)V
    .locals 0

    .prologue
    .line 2240547
    iput-object p1, p0, LX/FRc;->d:LX/FRg;

    iput-object p2, p0, LX/FRc;->a:LX/FRt;

    iput-object p3, p0, LX/FRc;->b:LX/6zj;

    iput-object p4, p0, LX/FRc;->c:Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;

    invoke-direct {p0}, LX/0Vd;-><init>()V

    return-void
.end method


# virtual methods
.method public final onNonCancellationFailure(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2240548
    iget-object v0, p0, LX/FRc;->d:LX/FRg;

    iget-object v1, p0, LX/FRc;->b:LX/6zj;

    iget-object v2, p0, LX/FRc;->c:Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;

    invoke-static {v0, v1, v2}, LX/FRg;->b(LX/FRg;LX/6zj;Lcom/facebook/payments/settings/PaymentSettingsPickerRunTimeData;)V

    .line 2240549
    return-void
.end method

.method public final onSuccessfulResult(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 2240550
    check-cast p1, Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 2240551
    iget-object v0, p0, LX/FRc;->a:LX/FRt;

    .line 2240552
    iput-object p1, v0, LX/FRt;->f:Lcom/facebook/payments/auth/pin/model/PaymentPin;

    .line 2240553
    return-void
.end method
