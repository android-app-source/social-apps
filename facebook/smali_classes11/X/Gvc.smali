.class public LX/Gvc;
.super Landroid/webkit/WebViewClient;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/katana/gdp/PlatformDialogActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/katana/gdp/PlatformDialogActivity;)V
    .locals 0

    .prologue
    .line 2405745
    iput-object p1, p0, LX/Gvc;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLoadResource(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 2405743
    iget-object v0, p0, LX/Gvc;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    iget-object v0, v0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->x:LX/44G;

    invoke-virtual {v0, p2}, LX/44G;->a(Ljava/lang/String;)V

    .line 2405744
    return-void
.end method

.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2405685
    invoke-super {p0, p1, p2}, Landroid/webkit/WebViewClient;->onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V

    .line 2405686
    invoke-static {p2}, LX/FC4;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2405687
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, LX/Gvc;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    invoke-static {v2}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->r(Lcom/facebook/katana/gdp/PlatformDialogActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2405688
    iget-object v1, p0, LX/Gvc;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    iget-object v1, v1, Lcom/facebook/katana/gdp/PlatformDialogActivity;->r:Lcom/facebook/performancelogger/PerformanceLogger;

    const v2, 0x380007

    invoke-interface {v1, v2, v0}, Lcom/facebook/performancelogger/PerformanceLogger;->c(ILjava/lang/String;)V

    .line 2405689
    iget-object v0, p0, LX/Gvc;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    invoke-static {v0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->u(Lcom/facebook/katana/gdp/PlatformDialogActivity;)V

    .line 2405690
    return-void
.end method

.method public final onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 2405739
    invoke-super {p0, p1, p2, p3}, Landroid/webkit/WebViewClient;->onPageStarted(Landroid/webkit/WebView;Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 2405740
    :try_start_0
    iget-object v0, p0, LX/Gvc;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    iget-object v0, v0, Lcom/facebook/katana/gdp/PlatformDialogActivity;->u:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2405741
    :goto_0
    return-void

    :catch_0
    goto :goto_0

    .line 2405742
    :catch_1
    goto :goto_0
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2405731
    invoke-super {p0, p1, p2, p3, p4}, Landroid/webkit/WebViewClient;->onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V

    .line 2405732
    iget-object v0, p0, LX/Gvc;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    invoke-static {v0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->t(Lcom/facebook/katana/gdp/PlatformDialogActivity;)V

    .line 2405733
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2405734
    const-string v1, "error"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405735
    const-string v1, "error_code"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2405736
    const-string v1, "failing_url"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405737
    iget-object v1, p0, LX/Gvc;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    invoke-virtual {v1, v0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->e(Landroid/os/Bundle;)V

    .line 2405738
    return-void
.end method

.method public final onReceivedSslError(Landroid/webkit/WebView;Landroid/webkit/SslErrorHandler;Landroid/net/http/SslError;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2405721
    iget-object v0, p0, LX/Gvc;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    invoke-static {v0}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->t(Lcom/facebook/katana/gdp/PlatformDialogActivity;)V

    .line 2405722
    iget-object v0, p0, LX/Gvc;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    invoke-static {v0}, LX/0QA;->get(Landroid/content/Context;)LX/0QA;

    move-result-object v1

    .line 2405723
    invoke-static {v1}, LX/0TG;->a(LX/0QB;)LX/0TG;

    move-result-object v0

    check-cast v0, Lcom/facebook/prefs/shared/FbSharedPreferences;

    sget-object v2, LX/0dU;->j:LX/0Tn;

    invoke-interface {v0, v2, v3}, Lcom/facebook/prefs/shared/FbSharedPreferences;->a(LX/0Tn;Z)Z

    move-result v0

    .line 2405724
    if-nez v0, :cond_0

    .line 2405725
    invoke-virtual {p2}, Landroid/webkit/SslErrorHandler;->proceed()V

    .line 2405726
    :goto_0
    return-void

    .line 2405727
    :cond_0
    sget-boolean v0, LX/007;->i:Z

    move v0, v0

    .line 2405728
    if-nez v0, :cond_1

    sget-object v0, LX/03R;->YES:LX/03R;

    invoke-static {v1}, LX/0XD;->b(LX/0QB;)LX/03R;

    move-result-object v1

    if-ne v0, v1, :cond_2

    .line 2405729
    :cond_1
    iget-object v0, p0, LX/Gvc;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    const v1, 0x7f082715

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 2405730
    :cond_2
    iget-object v0, p0, LX/Gvc;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    const v1, 0x7f082714

    invoke-virtual {v0, v1}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->getString(I)Ljava/lang/String;

    goto :goto_0
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 2405691
    const-string v1, "fbconnect://success"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2405692
    iget-object v1, p0, LX/Gvc;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    invoke-virtual {v1}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "return_via_intent_url"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2405693
    iget-object v1, p0, LX/Gvc;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    invoke-virtual {v1}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "client_id"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2405694
    iget-object v1, p0, LX/Gvc;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    invoke-virtual {v1}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v3, "activity_id"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2405695
    if-nez v1, :cond_0

    .line 2405696
    const-string v1, ""

    .line 2405697
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "fb"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "://authorize"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 2405698
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getEncodedFragment()Ljava/lang/String;

    move-result-object v2

    .line 2405699
    if-eqz v2, :cond_1

    .line 2405700
    const-string v3, "#"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2405701
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2405702
    iget-object v1, p0, LX/Gvc;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    invoke-virtual {v1, v2}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->startActivity(Landroid/content/Intent;)V

    .line 2405703
    :goto_0
    return v0

    .line 2405704
    :cond_2
    const-string v1, "touch"

    invoke-virtual {p2, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2405705
    const/4 v0, 0x0

    goto :goto_0

    .line 2405706
    :cond_3
    iget-object v1, p0, LX/Gvc;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v1, v2}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 2405707
    :cond_4
    const-string v1, "fbconnect"

    const-string v2, "http"

    invoke-virtual {p2, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v1

    .line 2405708
    :try_start_0
    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 2405709
    invoke-virtual {v3}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, LX/FC5;->a(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 2405710
    invoke-virtual {v3}, Ljava/net/URL;->getRef()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LX/FC5;->a(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2405711
    :goto_1
    move-object v2, v2

    .line 2405712
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 2405713
    const-string v3, "error_reason"

    invoke-virtual {v1, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2405714
    if-eqz v1, :cond_5

    const-string v3, "user_denied"

    invoke-virtual {v3, v1}, Ljava/lang/String;->contentEquals(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2405715
    iget-object v1, p0, LX/Gvc;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    invoke-virtual {v1, v2}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->e(Landroid/os/Bundle;)V

    goto :goto_0

    .line 2405716
    :cond_5
    iget-object v1, p0, LX/Gvc;->a:Lcom/facebook/katana/gdp/PlatformDialogActivity;

    .line 2405717
    const/4 v3, 0x1

    invoke-static {v1, v3, v2}, Lcom/facebook/katana/gdp/PlatformDialogActivity;->a(Lcom/facebook/katana/gdp/PlatformDialogActivity;ZLandroid/os/Bundle;)V

    .line 2405718
    goto :goto_0

    .line 2405719
    :catch_0
    const-string v2, "PlatformDialogActivity"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Caught malformed URL: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, LX/01m;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2405720
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    goto :goto_1
.end method
