.class public LX/G4v;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/G4x;

.field private final b:Lcom/facebook/timeline/BaseTimelineFragment;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/1My;",
            ">;"
        }
    .end annotation
.end field

.field private final d:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "LX/G4i;",
            ">;"
        }
    .end annotation
.end field

.field public e:LX/1My;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field private final f:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;>;"
        }
    .end annotation
.end field

.field private final g:LX/0TF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0TF",
            "<",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/graphql/model/GraphQLFeedback;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/G4x;Lcom/facebook/timeline/BaseTimelineFragment;LX/0Or;LX/0Or;)V
    .locals 1
    .param p1    # LX/G4x;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .param p2    # Lcom/facebook/timeline/BaseTimelineFragment;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/timeline/units/model/TimelineUnitsMutationCallbacks;",
            "Lcom/facebook/timeline/units/controller/TimelineUnitSubscriberImpl$Listener;",
            "LX/0Or",
            "<",
            "LX/1My;",
            ">;",
            "LX/0Or",
            "<",
            "LX/G4i;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2318182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2318183
    new-instance v0, LX/G4t;

    invoke-direct {v0, p0}, LX/G4t;-><init>(LX/G4v;)V

    iput-object v0, p0, LX/G4v;->f:LX/0TF;

    .line 2318184
    new-instance v0, LX/G4u;

    invoke-direct {v0, p0}, LX/G4u;-><init>(LX/G4v;)V

    iput-object v0, p0, LX/G4v;->g:LX/0TF;

    .line 2318185
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G4x;

    iput-object v0, p0, LX/G4v;->a:LX/G4x;

    .line 2318186
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/timeline/BaseTimelineFragment;

    iput-object v0, p0, LX/G4v;->b:Lcom/facebook/timeline/BaseTimelineFragment;

    .line 2318187
    iput-object p3, p0, LX/G4v;->c:LX/0Or;

    .line 2318188
    iput-object p4, p0, LX/G4v;->d:LX/0Or;

    .line 2318189
    return-void
.end method

.method private b(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 9

    .prologue
    .line 2318172
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 2318173
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLFeedback;->j()Ljava/lang/String;

    move-result-object v0

    .line 2318174
    invoke-direct {p0}, LX/G4v;->d()LX/1My;

    move-result-object v7

    iget-object v8, p0, LX/G4v;->g:LX/0TF;

    new-instance v1, Lcom/facebook/graphql/executor/GraphQLResult;

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v2

    sget-object v3, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const-wide/16 v4, 0x0

    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->e()Lcom/facebook/graphql/model/GraphQLFeedback;

    move-result-object v6

    invoke-virtual {v6}, Lcom/facebook/graphql/model/GraphQLFeedback;->k()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LX/0Rf;->of(Ljava/lang/Object;)LX/0Rf;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    invoke-virtual {v7, v8, v0, v1}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2318175
    :cond_0
    invoke-static {p1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {p1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 2318176
    invoke-static {p1}, LX/16y;->a(Lcom/facebook/graphql/model/GraphQLStory;)Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/graphql/model/GraphQLSubstoriesConnection;->j()LX/0Px;

    move-result-object v2

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2318177
    invoke-direct {p0, v0}, LX/G4v;->b(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2318178
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2318179
    :cond_1
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 2318180
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->J()Lcom/facebook/graphql/model/GraphQLStory;

    move-result-object v0

    invoke-direct {p0, v0}, LX/G4v;->b(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2318181
    :cond_2
    return-void
.end method

.method private d()LX/1My;
    .locals 1

    .prologue
    .line 2318169
    iget-object v0, p0, LX/G4v;->e:LX/1My;

    if-nez v0, :cond_0

    .line 2318170
    iget-object v0, p0, LX/G4v;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/1My;

    iput-object v0, p0, LX/G4v;->e:LX/1My;

    .line 2318171
    :cond_0
    iget-object v0, p0, LX/G4v;->e:LX/1My;

    return-object v0
.end method

.method public static e(LX/G4v;)V
    .locals 1

    .prologue
    .line 2318166
    iget-object v0, p0, LX/G4v;->d:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/G4i;

    invoke-virtual {v0}, LX/G4i;->a()V

    .line 2318167
    iget-object v0, p0, LX/G4v;->b:Lcom/facebook/timeline/BaseTimelineFragment;

    invoke-virtual {v0}, Lcom/facebook/timeline/BaseTimelineFragment;->lo_()V

    .line 2318168
    return-void
.end method


# virtual methods
.method public final a(Lcom/facebook/graphql/model/GraphQLStory;)V
    .locals 9

    .prologue
    .line 2318162
    invoke-virtual {p1}, Lcom/facebook/graphql/model/GraphQLStory;->ag()Ljava/lang/String;

    move-result-object v0

    .line 2318163
    invoke-direct {p0, p1}, LX/G4v;->b(Lcom/facebook/graphql/model/GraphQLStory;)V

    .line 2318164
    invoke-direct {p0}, LX/G4v;->d()LX/1My;

    move-result-object v7

    iget-object v8, p0, LX/G4v;->f:LX/0TF;

    new-instance v1, Lcom/facebook/graphql/executor/GraphQLResult;

    sget-object v3, LX/0ta;->FROM_CACHE_UP_TO_DATE:LX/0ta;

    const-wide/16 v4, 0x0

    invoke-static {p1}, LX/2vM;->a(Lcom/facebook/graphql/model/FeedUnit;)LX/0Rf;

    move-result-object v6

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/facebook/graphql/executor/GraphQLResult;-><init>(Ljava/lang/Object;LX/0ta;JLjava/util/Set;)V

    invoke-virtual {v7, v8, v0, v1}, LX/1My;->a(LX/0TF;Ljava/lang/String;Lcom/facebook/graphql/executor/GraphQLResult;)V

    .line 2318165
    return-void
.end method

.method public final a(Ljava/lang/Iterable;)V
    .locals 3

    .prologue
    .line 2318158
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 2318159
    instance-of v2, v0, Lcom/facebook/graphql/model/GraphQLStory;

    if-eqz v2, :cond_0

    .line 2318160
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    invoke-virtual {p0, v0}, LX/G4v;->a(Lcom/facebook/graphql/model/GraphQLStory;)V

    goto :goto_0

    .line 2318161
    :cond_1
    return-void
.end method
