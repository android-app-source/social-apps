.class public final LX/H5a;
.super LX/1X1;
.source ""

# interfaces
.implements Ljava/lang/Cloneable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1X1",
        "<",
        "LX/H5b;",
        ">;",
        "Ljava/lang/Cloneable;"
    }
.end annotation


# instance fields
.field public a:LX/2nq;

.field public b:LX/1Pn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field

.field public final synthetic c:LX/H5b;


# direct methods
.method public constructor <init>(LX/H5b;)V
    .locals 1

    .prologue
    .line 2424568
    iput-object p1, p0, LX/H5a;->c:LX/H5b;

    .line 2424569
    move-object v0, p1

    .line 2424570
    invoke-direct {p0, v0}, LX/1X1;-><init>(LX/1S3;)V

    .line 2424571
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2424586
    const-string v0, "PostFeedbackNotificationsComponent"

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2424572
    if-ne p0, p1, :cond_1

    .line 2424573
    :cond_0
    :goto_0
    return v0

    .line 2424574
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 2424575
    goto :goto_0

    .line 2424576
    :cond_3
    check-cast p1, LX/H5a;

    .line 2424577
    iget v2, p0, LX/1X1;->b:I

    move v2, v2

    .line 2424578
    iget v3, p1, LX/1X1;->b:I

    move v3, v3

    .line 2424579
    if-eq v2, v3, :cond_0

    .line 2424580
    iget-object v2, p0, LX/H5a;->a:LX/2nq;

    if-eqz v2, :cond_5

    iget-object v2, p0, LX/H5a;->a:LX/2nq;

    iget-object v3, p1, LX/H5a;->a:LX/2nq;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 2424581
    goto :goto_0

    .line 2424582
    :cond_5
    iget-object v2, p1, LX/H5a;->a:LX/2nq;

    if-nez v2, :cond_4

    .line 2424583
    :cond_6
    iget-object v2, p0, LX/H5a;->b:LX/1Pn;

    if-eqz v2, :cond_7

    iget-object v2, p0, LX/H5a;->b:LX/1Pn;

    iget-object v3, p1, LX/H5a;->b:LX/1Pn;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 2424584
    goto :goto_0

    .line 2424585
    :cond_7
    iget-object v2, p1, LX/H5a;->b:LX/1Pn;

    if-eqz v2, :cond_0

    goto :goto_1
.end method
