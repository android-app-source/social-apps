.class public LX/FKf;
.super LX/0ro;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/0ro",
        "<",
        "Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;",
        "Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:LX/3Mw;

.field private final c:LX/0Or;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(LX/0sO;LX/3Mw;LX/0Or;)V
    .locals 0
    .param p3    # LX/0Or;
        .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
        .end annotation
    .end param
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "LX/0sO;",
            "LX/3Mw;",
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;)V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2225411
    invoke-direct {p0, p1}, LX/0ro;-><init>(LX/0sO;)V

    .line 2225412
    iput-object p2, p0, LX/FKf;->b:LX/3Mw;

    .line 2225413
    iput-object p3, p0, LX/FKf;->c:LX/0Or;

    .line 2225414
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;LX/1pN;LX/15w;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2225403
    iget-object v0, p0, LX/FKf;->c:LX/0Or;

    invoke-interface {v0}, LX/0Or;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/user/model/User;

    .line 2225404
    if-nez v0, :cond_0

    .line 2225405
    sget-object v0, Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;->a:Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    .line 2225406
    :goto_0
    return-object v0

    .line 2225407
    :cond_0
    const-class v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$PinnedThreadsQueryModel;

    invoke-virtual {p3, v1}, LX/15w;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$PinnedThreadsQueryModel;

    .line 2225408
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$PinnedThreadsQueryModel;->a()Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$PinnedThreadsQueryModel$PinnedMessageThreadsModel;

    move-result-object v1

    .line 2225409
    :goto_1
    iget-object v2, p0, LX/FKf;->b:LX/3Mw;

    invoke-virtual {v2, v1, v0}, LX/3Mw;->a(Lcom/facebook/messaging/graphql/threads/ThreadQueriesModels$PinnedThreadsQueryModel$PinnedMessageThreadsModel;Lcom/facebook/user/model/User;)Lcom/facebook/messaging/service/model/FetchGroupThreadsResult;

    move-result-object v0

    goto :goto_0

    .line 2225410
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public final b(Ljava/lang/Object;LX/1pN;)I
    .locals 1

    .prologue
    .line 2225402
    const/4 v0, 0x1

    return v0
.end method

.method public final f(Ljava/lang/Object;)LX/0gW;
    .locals 6

    .prologue
    .line 2225400
    check-cast p1, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;

    .line 2225401
    invoke-static {}, LX/5ZC;->j()LX/5Z6;

    move-result-object v0

    const-string v1, "after_time_sec"

    iget-wide v2, p1, Lcom/facebook/messaging/service/model/FetchGroupThreadsParams;->c:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "thread_count"

    const/16 v2, 0x3c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v0

    const-string v1, "include_message_info"

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    const-string v1, "include_full_user_info"

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v0

    return-object v0
.end method
