.class public LX/FEl;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/FD1;


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public a:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/FEk;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2216647
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2216648
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2216649
    iput-object v0, p0, LX/FEl;->a:LX/0Ot;

    .line 2216650
    return-void
.end method

.method public static a(LX/0QB;)LX/FEl;
    .locals 4

    .prologue
    .line 2216651
    const-class v1, LX/FEl;

    monitor-enter v1

    .line 2216652
    :try_start_0
    sget-object v0, LX/FEl;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2216653
    sput-object v2, LX/FEl;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2216654
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2216655
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2216656
    new-instance v3, LX/FEl;

    invoke-direct {v3}, LX/FEl;-><init>()V

    .line 2216657
    const/16 p0, 0x2786

    invoke-static {v0, p0}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2216658
    iput-object p0, v3, LX/FEl;->a:LX/0Ot;

    .line 2216659
    move-object v0, v3

    .line 2216660
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2216661
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/FEl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2216662
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2216663
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
