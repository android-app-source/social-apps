.class public final LX/F24;
.super LX/1wH;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/1wH",
        "<",
        "Lcom/facebook/graphql/model/GraphQLStory;",
        ">;"
    }
.end annotation


# instance fields
.field public final synthetic b:LX/F25;


# direct methods
.method public constructor <init>(LX/F25;)V
    .locals 0

    .prologue
    .line 2192769
    iput-object p1, p0, LX/F24;->b:LX/F25;

    invoke-direct {p0, p1}, LX/1wH;-><init>(LX/1SX;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/Menu;",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 2192770
    iget-object v0, p2, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v4, v0

    .line 2192771
    check-cast v4, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2192772
    invoke-virtual {p3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 2192773
    iget-object v0, p0, LX/F24;->b:LX/F25;

    invoke-virtual {v0, v4}, LX/1SX;->i(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2192774
    sget-object v0, LX/Al1;->DELETE:LX/Al1;

    invoke-virtual {v0}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v0

    .line 2192775
    const v1, 0x7f08104c

    invoke-interface {p1, v1}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 2192776
    new-instance v2, LX/F20;

    invoke-direct {v2, p0, p2, v0, v5}, LX/F20;-><init>(LX/F24;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Landroid/content/Context;)V

    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2192777
    iget-object v2, p0, LX/F24;->b:LX/F25;

    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    .line 2192778
    invoke-virtual {v2, p2, v3, v0, v7}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 2192779
    iget-object v0, p0, LX/F24;->b:LX/F25;

    .line 2192780
    const v2, 0x7f020a0b

    move v2, v2

    .line 2192781
    invoke-virtual {v0, v1, v2, v4}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 2192782
    :cond_0
    iget-object v0, p0, LX/F24;->b:LX/F25;

    invoke-virtual {v0, v4}, LX/1SX;->g(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2192783
    sget-object v0, LX/Al1;->EDIT_POST:LX/Al1;

    invoke-virtual {v0}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v3

    .line 2192784
    const v0, 0x7f08106b

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 2192785
    new-instance v0, LX/F21;

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, LX/F21;-><init>(LX/F24;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    invoke-interface {v6, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2192786
    iget-object v0, p0, LX/F24;->b:LX/F25;

    invoke-interface {v6}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 2192787
    invoke-virtual {v0, p2, v1, v3, v7}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 2192788
    iget-object v0, p0, LX/F24;->b:LX/F25;

    .line 2192789
    const v1, 0x7f020952

    move v1, v1

    .line 2192790
    invoke-virtual {v0, v6, v1, v4}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 2192791
    :cond_1
    invoke-static {v4}, LX/1Sn;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    move v0, v1

    .line 2192792
    if-eqz v0, :cond_2

    .line 2192793
    sget-object v0, LX/Al1;->VIEW_EDIT_HISTORY:LX/Al1;

    invoke-virtual {v0}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v3

    .line 2192794
    const v0, 0x7f08106d

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 2192795
    new-instance v0, LX/F22;

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, LX/F22;-><init>(LX/F24;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    invoke-interface {v6, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2192796
    iget-object v0, p0, LX/F24;->b:LX/F25;

    invoke-interface {v6}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 2192797
    invoke-virtual {v0, p2, v1, v3, v7}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 2192798
    iget-object v0, p0, LX/F24;->b:LX/F25;

    .line 2192799
    const v1, 0x7f0207fd

    move v1, v1

    .line 2192800
    invoke-virtual {v0, v6, v1, v4}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 2192801
    :cond_2
    iget-object v0, p0, LX/F24;->b:LX/F25;

    .line 2192802
    invoke-virtual {v0, v4}, LX/1SX;->e(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    move v0, v1

    .line 2192803
    if-eqz v0, :cond_3

    .line 2192804
    sget-object v0, LX/Al1;->EDIT_PRIVACY:LX/Al1;

    invoke-virtual {v0}, LX/Al1;->name()Ljava/lang/String;

    move-result-object v3

    .line 2192805
    const v0, 0x7f081068

    invoke-interface {p1, v0}, Landroid/view/Menu;->add(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 2192806
    new-instance v0, LX/F23;

    move-object v1, p0

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, LX/F23;-><init>(LX/F24;Lcom/facebook/feed/rows/core/props/FeedProps;Ljava/lang/String;Lcom/facebook/graphql/model/GraphQLStory;Landroid/content/Context;)V

    invoke-interface {v6, v0}, Landroid/view/MenuItem;->setOnMenuItemClickListener(Landroid/view/MenuItem$OnMenuItemClickListener;)Landroid/view/MenuItem;

    .line 2192807
    iget-object v0, p0, LX/F24;->b:LX/F25;

    invoke-interface {v6}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 2192808
    invoke-virtual {v0, p2, v1, v3, v7}, LX/1SX;->a(Lcom/facebook/feed/rows/core/props/FeedProps;ILjava/lang/String;Z)V

    .line 2192809
    iget-object v0, p0, LX/F24;->b:LX/F25;

    .line 2192810
    const v1, 0x7f020952

    move v1, v1

    .line 2192811
    invoke-virtual {v0, v6, v1, v4}, LX/1SX;->a(Landroid/view/MenuItem;ILcom/facebook/graphql/model/FeedUnit;)V

    .line 2192812
    :cond_3
    iget-object v0, p0, LX/F24;->b:LX/F25;

    .line 2192813
    invoke-virtual {v0, p1, p2, p3}, LX/1SX;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 2192814
    invoke-super {p0, p1, p2, p3}, LX/1wH;->a(Landroid/view/Menu;Lcom/facebook/feed/rows/core/props/FeedProps;Landroid/view/View;)V

    .line 2192815
    return-void
.end method

.method public final a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<",
            "Lcom/facebook/graphql/model/GraphQLStory;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 2192762
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2192763
    check-cast v0, Lcom/facebook/graphql/model/GraphQLStory;

    .line 2192764
    invoke-super {p0, p1}, LX/1wH;->a(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p1}, LX/F24;->d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/F24;->b:LX/F25;

    invoke-virtual {v1, v0}, LX/1SX;->i(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, LX/F24;->b:LX/F25;

    .line 2192765
    invoke-virtual {v1, v0}, LX/1SX;->e(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result p1

    move v1, p1

    .line 2192766
    if-nez v1, :cond_0

    .line 2192767
    invoke-static {v0}, LX/1Sn;->d(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result p1

    move v1, p1

    .line 2192768
    if-nez v1, :cond_0

    iget-object v1, p0, LX/F24;->b:LX/F25;

    invoke-virtual {v1, v0}, LX/1SX;->g(Lcom/facebook/graphql/model/FeedUnit;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Lcom/facebook/feed/rows/core/props/FeedProps;)Z
    .locals 7
    .annotation build Lcom/google/common/annotations/VisibleForTesting;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/feed/rows/core/props/FeedProps",
            "<+",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 2192744
    iget-object v0, p1, Lcom/facebook/feed/rows/core/props/FeedProps;->a:Lcom/facebook/flatbuffers/Flattenable;

    move-object v0, v0

    .line 2192745
    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 2192746
    instance-of v2, v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    if-eqz v2, :cond_2

    .line 2192747
    check-cast v0, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;

    .line 2192748
    invoke-interface {v0}, Lcom/facebook/graphql/model/HideableUnit;->m()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, LX/F24;->b:LX/F25;

    const/4 v4, 0x0

    .line 2192749
    invoke-interface {v0}, Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;->s()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;

    move-result-object v3

    .line 2192750
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;->a()LX/0Px;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;->a()LX/0Px;

    move-result-object v5

    invoke-virtual {v5}, LX/0Px;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_0
    move v3, v4

    .line 2192751
    :goto_0
    move v0, v3

    .line 2192752
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 2192753
    :goto_1
    return v0

    :cond_1
    move v0, v1

    .line 2192754
    goto :goto_1

    :cond_2
    move v0, v1

    .line 2192755
    goto :goto_1

    .line 2192756
    :cond_3
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsConnection;->a()LX/0Px;

    move-result-object v6

    invoke-virtual {v6}, LX/0Px;->size()I

    move-result p0

    move v5, v4

    .line 2192757
    :goto_2
    if-ge v5, p0, :cond_5

    invoke-virtual {v6, v5}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;

    .line 2192758
    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object p1

    if-eqz p1, :cond_4

    iget-object p1, v2, LX/F25;->d:LX/1Sp;

    invoke-virtual {v3}, Lcom/facebook/graphql/model/GraphQLNegativeFeedbackActionsEdge;->a()Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;

    move-result-object v3

    invoke-interface {p1, v0, v3}, LX/1Sp;->a(Lcom/facebook/graphql/model/NegativeFeedbackActionsUnit;Lcom/facebook/graphql/model/GraphQLNegativeFeedbackAction;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2192759
    const/4 v3, 0x1

    goto :goto_0

    .line 2192760
    :cond_4
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_2

    :cond_5
    move v3, v4

    .line 2192761
    goto :goto_0
.end method
