.class public LX/Ffe;
.super LX/FfQ;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FfQ",
        "<",
        "Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/Ffe;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 2
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2268709
    sget-object v0, Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;->STORIES:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    const v1, 0x7f082318

    invoke-direct {p0, p1, v0, v1}, LX/FfQ;-><init>(Landroid/content/res/Resources;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;I)V

    .line 2268710
    return-void
.end method

.method public static a(LX/0QB;)LX/Ffe;
    .locals 4

    .prologue
    .line 2268711
    sget-object v0, LX/Ffe;->a:LX/Ffe;

    if-nez v0, :cond_1

    .line 2268712
    const-class v1, LX/Ffe;

    monitor-enter v1

    .line 2268713
    :try_start_0
    sget-object v0, LX/Ffe;->a:LX/Ffe;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2268714
    if-eqz v2, :cond_0

    .line 2268715
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2268716
    new-instance p0, LX/Ffe;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    invoke-direct {p0, v3}, LX/Ffe;-><init>(Landroid/content/res/Resources;)V

    .line 2268717
    move-object v0, p0

    .line 2268718
    sput-object v0, LX/Ffe;->a:LX/Ffe;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2268719
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2268720
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2268721
    :cond_1
    sget-object v0, LX/Ffe;->a:LX/Ffe;

    return-object v0

    .line 2268722
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2268723
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final b()LX/Fdv;
    .locals 1

    .prologue
    .line 2268724
    const-string v0, "graph_search_results_page_post"

    invoke-static {v0}, Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;->b(Ljava/lang/String;)Lcom/facebook/search/results/fragment/feed/SearchResultsFeedFragment;

    move-result-object v0

    return-object v0
.end method
