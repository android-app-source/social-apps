.class public final enum LX/Fcu;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/Fcu;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/Fcu;

.field public static final enum HAS_DELIVERY:LX/Fcu;

.field public static final enum HAS_PARKING:LX/Fcu;

.field public static final enum HAS_TAKEOUT:LX/Fcu;

.field public static final enum HAS_WIFI:LX/Fcu;

.field public static final enum OUTDOOR_SEATING:LX/Fcu;

.field public static final enum POPULAR_WITH_GROUPS:LX/Fcu;

.field public static final enum TAKES_CREDIT_CARDS:LX/Fcu;

.field public static final enum TAKES_RESERVATIONS:LX/Fcu;

.field public static final enum VISITED_BY_FRIENDS:LX/Fcu;


# direct methods
.method public static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2262870
    new-instance v0, LX/Fcu;

    const-string v1, "VISITED_BY_FRIENDS"

    invoke-direct {v0, v1, v3}, LX/Fcu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fcu;->VISITED_BY_FRIENDS:LX/Fcu;

    .line 2262871
    new-instance v0, LX/Fcu;

    const-string v1, "TAKES_RESERVATIONS"

    invoke-direct {v0, v1, v4}, LX/Fcu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fcu;->TAKES_RESERVATIONS:LX/Fcu;

    .line 2262872
    new-instance v0, LX/Fcu;

    const-string v1, "HAS_DELIVERY"

    invoke-direct {v0, v1, v5}, LX/Fcu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fcu;->HAS_DELIVERY:LX/Fcu;

    .line 2262873
    new-instance v0, LX/Fcu;

    const-string v1, "HAS_TAKEOUT"

    invoke-direct {v0, v1, v6}, LX/Fcu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fcu;->HAS_TAKEOUT:LX/Fcu;

    .line 2262874
    new-instance v0, LX/Fcu;

    const-string v1, "TAKES_CREDIT_CARDS"

    invoke-direct {v0, v1, v7}, LX/Fcu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fcu;->TAKES_CREDIT_CARDS:LX/Fcu;

    .line 2262875
    new-instance v0, LX/Fcu;

    const-string v1, "POPULAR_WITH_GROUPS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, LX/Fcu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fcu;->POPULAR_WITH_GROUPS:LX/Fcu;

    .line 2262876
    new-instance v0, LX/Fcu;

    const-string v1, "OUTDOOR_SEATING"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, LX/Fcu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fcu;->OUTDOOR_SEATING:LX/Fcu;

    .line 2262877
    new-instance v0, LX/Fcu;

    const-string v1, "HAS_PARKING"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, LX/Fcu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fcu;->HAS_PARKING:LX/Fcu;

    .line 2262878
    new-instance v0, LX/Fcu;

    const-string v1, "HAS_WIFI"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, LX/Fcu;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/Fcu;->HAS_WIFI:LX/Fcu;

    .line 2262879
    const/16 v0, 0x9

    new-array v0, v0, [LX/Fcu;

    sget-object v1, LX/Fcu;->VISITED_BY_FRIENDS:LX/Fcu;

    aput-object v1, v0, v3

    sget-object v1, LX/Fcu;->TAKES_RESERVATIONS:LX/Fcu;

    aput-object v1, v0, v4

    sget-object v1, LX/Fcu;->HAS_DELIVERY:LX/Fcu;

    aput-object v1, v0, v5

    sget-object v1, LX/Fcu;->HAS_TAKEOUT:LX/Fcu;

    aput-object v1, v0, v6

    sget-object v1, LX/Fcu;->TAKES_CREDIT_CARDS:LX/Fcu;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, LX/Fcu;->POPULAR_WITH_GROUPS:LX/Fcu;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, LX/Fcu;->OUTDOOR_SEATING:LX/Fcu;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, LX/Fcu;->HAS_PARKING:LX/Fcu;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, LX/Fcu;->HAS_WIFI:LX/Fcu;

    aput-object v2, v0, v1

    sput-object v0, LX/Fcu;->$VALUES:[LX/Fcu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2262880
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/Fcu;
    .locals 1

    .prologue
    .line 2262881
    const-class v0, LX/Fcu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/Fcu;

    return-object v0
.end method

.method public static values()[LX/Fcu;
    .locals 1

    .prologue
    .line 2262882
    sget-object v0, LX/Fcu;->$VALUES:[LX/Fcu;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/Fcu;

    return-object v0
.end method
