.class public final enum LX/G5A;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/G5A;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/G5A;

.field public static final enum COMPLETED:LX/G5A;

.field public static final enum FAILED:LX/G5A;

.field public static final enum LOADING:LX/G5A;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2318580
    new-instance v0, LX/G5A;

    const-string v1, "LOADING"

    invoke-direct {v0, v1, v2}, LX/G5A;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G5A;->LOADING:LX/G5A;

    .line 2318581
    new-instance v0, LX/G5A;

    const-string v1, "COMPLETED"

    invoke-direct {v0, v1, v3}, LX/G5A;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G5A;->COMPLETED:LX/G5A;

    .line 2318582
    new-instance v0, LX/G5A;

    const-string v1, "FAILED"

    invoke-direct {v0, v1, v4}, LX/G5A;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/G5A;->FAILED:LX/G5A;

    .line 2318583
    const/4 v0, 0x3

    new-array v0, v0, [LX/G5A;

    sget-object v1, LX/G5A;->LOADING:LX/G5A;

    aput-object v1, v0, v2

    sget-object v1, LX/G5A;->COMPLETED:LX/G5A;

    aput-object v1, v0, v3

    sget-object v1, LX/G5A;->FAILED:LX/G5A;

    aput-object v1, v0, v4

    sput-object v0, LX/G5A;->$VALUES:[LX/G5A;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2318579
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/G5A;
    .locals 1

    .prologue
    .line 2318577
    const-class v0, LX/G5A;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/G5A;

    return-object v0
.end method

.method public static values()[LX/G5A;
    .locals 1

    .prologue
    .line 2318578
    sget-object v0, LX/G5A;->$VALUES:[LX/G5A;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/G5A;

    return-object v0
.end method
