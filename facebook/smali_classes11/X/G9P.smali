.class public final LX/G9P;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public final a:LX/G96;

.field public b:LX/G9i;

.field public c:LX/G9d;

.field public d:Z


# direct methods
.method public constructor <init>(LX/G96;)V
    .locals 2

    .prologue
    .line 2322720
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2322721
    iget v0, p1, LX/G96;->b:I

    move v0, v0

    .line 2322722
    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    and-int/lit8 v0, v0, 0x3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 2322723
    :cond_0
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0

    .line 2322724
    :cond_1
    iput-object p1, p0, LX/G9P;->a:LX/G96;

    .line 2322725
    return-void
.end method

.method private a(III)I
    .locals 1

    .prologue
    .line 2322716
    iget-boolean v0, p0, LX/G9P;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/G9P;->a:LX/G96;

    invoke-virtual {v0, p2, p1}, LX/G96;->a(II)Z

    move-result v0

    .line 2322717
    :goto_0
    if-eqz v0, :cond_1

    shl-int/lit8 v0, p3, 0x1

    or-int/lit8 v0, v0, 0x1

    :goto_1
    return v0

    .line 2322718
    :cond_0
    iget-object v0, p0, LX/G9P;->a:LX/G96;

    invoke-virtual {v0, p1, p2}, LX/G96;->a(II)Z

    move-result v0

    goto :goto_0

    .line 2322719
    :cond_1
    shl-int/lit8 v0, p3, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a()LX/G9d;
    .locals 6

    .prologue
    const/4 v4, 0x7

    const/4 v1, 0x0

    const/16 v5, 0x8

    .line 2322688
    iget-object v0, p0, LX/G9P;->c:LX/G9d;

    if-eqz v0, :cond_0

    .line 2322689
    iget-object v0, p0, LX/G9P;->c:LX/G9d;

    .line 2322690
    :goto_0
    return-object v0

    :cond_0
    move v0, v1

    move v2, v1

    .line 2322691
    :goto_1
    const/4 v3, 0x6

    if-ge v0, v3, :cond_1

    .line 2322692
    invoke-direct {p0, v0, v5, v2}, LX/G9P;->a(III)I

    move-result v2

    .line 2322693
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2322694
    :cond_1
    invoke-direct {p0, v4, v5, v2}, LX/G9P;->a(III)I

    move-result v0

    .line 2322695
    invoke-direct {p0, v5, v5, v0}, LX/G9P;->a(III)I

    move-result v0

    .line 2322696
    invoke-direct {p0, v5, v4, v0}, LX/G9P;->a(III)I

    move-result v2

    .line 2322697
    const/4 v0, 0x5

    :goto_2
    if-ltz v0, :cond_2

    .line 2322698
    invoke-direct {p0, v5, v0, v2}, LX/G9P;->a(III)I

    move-result v2

    .line 2322699
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 2322700
    :cond_2
    iget-object v0, p0, LX/G9P;->a:LX/G96;

    .line 2322701
    iget v3, v0, LX/G96;->b:I

    move v3, v3

    .line 2322702
    add-int/lit8 v4, v3, -0x7

    .line 2322703
    add-int/lit8 v0, v3, -0x1

    :goto_3
    if-lt v0, v4, :cond_3

    .line 2322704
    invoke-direct {p0, v5, v0, v1}, LX/G9P;->a(III)I

    move-result v1

    .line 2322705
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .line 2322706
    :cond_3
    add-int/lit8 v0, v3, -0x8

    :goto_4
    if-ge v0, v3, :cond_4

    .line 2322707
    invoke-direct {p0, v0, v5, v1}, LX/G9P;->a(III)I

    move-result v1

    .line 2322708
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 2322709
    :cond_4
    invoke-static {v2, v1}, LX/G9d;->c(II)LX/G9d;

    move-result-object v0

    .line 2322710
    if-eqz v0, :cond_6

    .line 2322711
    :goto_5
    move-object v0, v0

    .line 2322712
    iput-object v0, p0, LX/G9P;->c:LX/G9d;

    .line 2322713
    iget-object v0, p0, LX/G9P;->c:LX/G9d;

    if-eqz v0, :cond_5

    .line 2322714
    iget-object v0, p0, LX/G9P;->c:LX/G9d;

    goto :goto_0

    .line 2322715
    :cond_5
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0

    :cond_6
    xor-int/lit16 v0, v2, 0x5412

    xor-int/lit16 v3, v1, 0x5412

    invoke-static {v0, v3}, LX/G9d;->c(II)LX/G9d;

    move-result-object v0

    goto :goto_5
.end method

.method public final b()LX/G9i;
    .locals 8

    .prologue
    const/4 v1, 0x5

    const/4 v2, 0x0

    .line 2322662
    iget-object v0, p0, LX/G9P;->b:LX/G9i;

    if-eqz v0, :cond_0

    .line 2322663
    iget-object v0, p0, LX/G9P;->b:LX/G9i;

    .line 2322664
    :goto_0
    return-object v0

    .line 2322665
    :cond_0
    iget-object v0, p0, LX/G9P;->a:LX/G96;

    .line 2322666
    iget v3, v0, LX/G96;->b:I

    move v5, v3

    .line 2322667
    add-int/lit8 v0, v5, -0x11

    div-int/lit8 v0, v0, 0x4

    .line 2322668
    const/4 v3, 0x6

    if-gt v0, v3, :cond_1

    .line 2322669
    invoke-static {v0}, LX/G9i;->b(I)LX/G9i;

    move-result-object v0

    goto :goto_0

    .line 2322670
    :cond_1
    add-int/lit8 v6, v5, -0xb

    move v4, v1

    move v3, v2

    .line 2322671
    :goto_1
    if-ltz v4, :cond_3

    .line 2322672
    add-int/lit8 v0, v5, -0x9

    :goto_2
    if-lt v0, v6, :cond_2

    .line 2322673
    invoke-direct {p0, v0, v4, v3}, LX/G9P;->a(III)I

    move-result v3

    .line 2322674
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    .line 2322675
    :cond_2
    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_1

    .line 2322676
    :cond_3
    invoke-static {v3}, LX/G9i;->c(I)LX/G9i;

    move-result-object v0

    .line 2322677
    if-eqz v0, :cond_4

    invoke-virtual {v0}, LX/G9i;->d()I

    move-result v3

    if-ne v3, v5, :cond_4

    .line 2322678
    iput-object v0, p0, LX/G9P;->b:LX/G9i;

    goto :goto_0

    :cond_4
    move v7, v1

    move v1, v2

    move v2, v7

    .line 2322679
    :goto_3
    if-ltz v2, :cond_6

    .line 2322680
    add-int/lit8 v0, v5, -0x9

    :goto_4
    if-lt v0, v6, :cond_5

    .line 2322681
    invoke-direct {p0, v2, v0, v1}, LX/G9P;->a(III)I

    move-result v1

    .line 2322682
    add-int/lit8 v0, v0, -0x1

    goto :goto_4

    .line 2322683
    :cond_5
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_3

    .line 2322684
    :cond_6
    invoke-static {v1}, LX/G9i;->c(I)LX/G9i;

    move-result-object v0

    .line 2322685
    if-eqz v0, :cond_7

    invoke-virtual {v0}, LX/G9i;->d()I

    move-result v1

    if-ne v1, v5, :cond_7

    .line 2322686
    iput-object v0, p0, LX/G9P;->b:LX/G9i;

    goto :goto_0

    .line 2322687
    :cond_7
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0
.end method

.method public final c()[B
    .locals 15

    .prologue
    const/4 v4, 0x0

    .line 2322629
    invoke-virtual {p0}, LX/G9P;->a()LX/G9d;

    move-result-object v0

    .line 2322630
    invoke-virtual {p0}, LX/G9P;->b()LX/G9i;

    move-result-object v9

    .line 2322631
    iget-byte v1, v0, LX/G9d;->d:B

    move v0, v1

    .line 2322632
    invoke-static {v0}, LX/G9R;->a(I)LX/G9R;

    move-result-object v0

    .line 2322633
    iget-object v1, p0, LX/G9P;->a:LX/G96;

    .line 2322634
    iget v2, v1, LX/G96;->b:I

    move v10, v2

    .line 2322635
    iget-object v1, p0, LX/G9P;->a:LX/G96;

    invoke-virtual {v0, v1, v10}, LX/G9R;->a(LX/G96;I)V

    .line 2322636
    invoke-virtual {v9}, LX/G9i;->e()LX/G96;

    move-result-object v11

    .line 2322637
    const/4 v1, 0x1

    .line 2322638
    iget v0, v9, LX/G9i;->f:I

    move v0, v0

    .line 2322639
    new-array v12, v0, [B

    .line 2322640
    add-int/lit8 v0, v10, -0x1

    move v3, v4

    move v5, v4

    move v6, v4

    move v8, v1

    :goto_0
    if-lez v0, :cond_6

    .line 2322641
    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 2322642
    add-int/lit8 v0, v0, -0x1

    :cond_0
    move v2, v4

    .line 2322643
    :goto_1
    if-ge v2, v10, :cond_5

    .line 2322644
    if-eqz v8, :cond_3

    add-int/lit8 v1, v10, -0x1

    sub-int/2addr v1, v2

    :goto_2
    move v7, v4

    .line 2322645
    :goto_3
    const/4 v13, 0x2

    if-ge v7, v13, :cond_4

    .line 2322646
    sub-int v13, v0, v7

    invoke-virtual {v11, v13, v1}, LX/G96;->a(II)Z

    move-result v13

    if-nez v13, :cond_2

    .line 2322647
    add-int/lit8 v3, v3, 0x1

    .line 2322648
    shl-int/lit8 v5, v5, 0x1

    .line 2322649
    iget-object v13, p0, LX/G9P;->a:LX/G96;

    sub-int v14, v0, v7

    invoke-virtual {v13, v14, v1}, LX/G96;->a(II)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 2322650
    or-int/lit8 v5, v5, 0x1

    .line 2322651
    :cond_1
    const/16 v13, 0x8

    if-ne v3, v13, :cond_2

    .line 2322652
    add-int/lit8 v3, v6, 0x1

    int-to-byte v5, v5

    aput-byte v5, v12, v6

    move v5, v4

    move v6, v3

    move v3, v4

    .line 2322653
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    :cond_3
    move v1, v2

    .line 2322654
    goto :goto_2

    .line 2322655
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2322656
    :cond_5
    xor-int/lit8 v1, v8, 0x1

    .line 2322657
    add-int/lit8 v0, v0, -0x2

    move v8, v1

    goto :goto_0

    .line 2322658
    :cond_6
    iget v0, v9, LX/G9i;->f:I

    move v0, v0

    .line 2322659
    if-eq v6, v0, :cond_7

    .line 2322660
    invoke-static {}, LX/G8v;->a()LX/G8v;

    move-result-object v0

    throw v0

    .line 2322661
    :cond_7
    return-object v12
.end method
