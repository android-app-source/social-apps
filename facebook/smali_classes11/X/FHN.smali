.class public final enum LX/FHN;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FHN;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FHN;

.field public static final enum ChunkedUDP:LX/FHN;

.field public static final enum Resumable:LX/FHN;

.field public static final enum UN_SPECIFIED:LX/FHN;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2220230
    new-instance v0, LX/FHN;

    const-string v1, "Resumable"

    invoke-direct {v0, v1, v2}, LX/FHN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHN;->Resumable:LX/FHN;

    .line 2220231
    new-instance v0, LX/FHN;

    const-string v1, "ChunkedUDP"

    invoke-direct {v0, v1, v3}, LX/FHN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHN;->ChunkedUDP:LX/FHN;

    .line 2220232
    new-instance v0, LX/FHN;

    const-string v1, "UN_SPECIFIED"

    invoke-direct {v0, v1, v4}, LX/FHN;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FHN;->UN_SPECIFIED:LX/FHN;

    .line 2220233
    const/4 v0, 0x3

    new-array v0, v0, [LX/FHN;

    sget-object v1, LX/FHN;->Resumable:LX/FHN;

    aput-object v1, v0, v2

    sget-object v1, LX/FHN;->ChunkedUDP:LX/FHN;

    aput-object v1, v0, v3

    sget-object v1, LX/FHN;->UN_SPECIFIED:LX/FHN;

    aput-object v1, v0, v4

    sput-object v0, LX/FHN;->$VALUES:[LX/FHN;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2220234
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FHN;
    .locals 1

    .prologue
    .line 2220235
    const-class v0, LX/FHN;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FHN;

    return-object v0
.end method

.method public static values()[LX/FHN;
    .locals 1

    .prologue
    .line 2220236
    sget-object v0, LX/FHN;->$VALUES:[LX/FHN;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FHN;

    return-object v0
.end method
