.class public LX/G0o;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation


# static fields
.field private static p:LX/0Xm;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:LX/0ad;

.field private final c:LX/Fv5;

.field private final d:LX/8p9;

.field private final e:LX/1DS;

.field private final f:LX/G1I;

.field private final g:LX/G2n;

.field private final h:LX/G4x;

.field private final i:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/protiles/rows/ProtilesRootGroupPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final j:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSelectorPartDefinition",
            "<",
            "LX/1Pf;",
            ">;>;"
        }
    .end annotation
.end field

.field private final k:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/feed/parts/TimelineFeedUnitRootPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final l:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/publisher/rows/PublisherRootGroupPartDefinition;",
            ">;"
        }
    .end annotation
.end field

.field private final m:LX/G2a;

.field private final n:LX/FuS;

.field private final o:LX/G2C;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0ad;LX/8p9;LX/Fv5;LX/1DS;LX/G1I;LX/G2n;LX/G4x;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/G2a;LX/FuS;LX/G2C;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0ad;",
            "LX/8p9;",
            "LX/Fv5;",
            "LX/1DS;",
            "LX/G1I;",
            "LX/G2n;",
            "LX/G4x;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/protiles/rows/ProtilesRootGroupPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/pymk/rows/PeopleYouMayKnowSelectorPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/feed/parts/TimelineFeedUnitRootPartDefinition;",
            ">;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/timeline/publisher/rows/PublisherRootGroupPartDefinition;",
            ">;",
            "LX/G2a;",
            "LX/FuS;",
            "LX/G2C;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2310317
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2310318
    iput-object p1, p0, LX/G0o;->a:Landroid/content/Context;

    .line 2310319
    iput-object p2, p0, LX/G0o;->b:LX/0ad;

    .line 2310320
    iput-object p3, p0, LX/G0o;->d:LX/8p9;

    .line 2310321
    iput-object p4, p0, LX/G0o;->c:LX/Fv5;

    .line 2310322
    iput-object p6, p0, LX/G0o;->f:LX/G1I;

    .line 2310323
    iput-object p7, p0, LX/G0o;->g:LX/G2n;

    .line 2310324
    iput-object p8, p0, LX/G0o;->h:LX/G4x;

    .line 2310325
    iput-object p5, p0, LX/G0o;->e:LX/1DS;

    .line 2310326
    iput-object p9, p0, LX/G0o;->i:LX/0Ot;

    .line 2310327
    iput-object p10, p0, LX/G0o;->j:LX/0Ot;

    .line 2310328
    iput-object p11, p0, LX/G0o;->k:LX/0Ot;

    .line 2310329
    iput-object p12, p0, LX/G0o;->l:LX/0Ot;

    .line 2310330
    iput-object p13, p0, LX/G0o;->m:LX/G2a;

    .line 2310331
    iput-object p14, p0, LX/G0o;->n:LX/FuS;

    .line 2310332
    iput-object p15, p0, LX/G0o;->o:LX/G2C;

    .line 2310333
    return-void
.end method

.method public static a(LX/0QB;)LX/G0o;
    .locals 3

    .prologue
    .line 2310334
    const-class v1, LX/G0o;

    monitor-enter v1

    .line 2310335
    :try_start_0
    sget-object v0, LX/G0o;->p:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2310336
    sput-object v2, LX/G0o;->p:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2310337
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2310338
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    invoke-static {v0}, LX/G0o;->b(LX/0QB;)LX/G0o;

    move-result-object v0

    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2310339
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/G0o;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2310340
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2310341
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method private static b(LX/0QB;)LX/G0o;
    .locals 17

    .prologue
    .line 2310342
    new-instance v1, LX/G0o;

    const-class v2, Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    invoke-static/range {p0 .. p0}, LX/0Yo;->a(LX/0QB;)LX/0ac;

    move-result-object v3

    check-cast v3, LX/0ad;

    invoke-static/range {p0 .. p0}, LX/8p9;->a(LX/0QB;)LX/8p9;

    move-result-object v4

    check-cast v4, LX/8p9;

    invoke-static/range {p0 .. p0}, LX/Fv5;->a(LX/0QB;)LX/Fv5;

    move-result-object v5

    check-cast v5, LX/Fv5;

    invoke-static/range {p0 .. p0}, LX/1DS;->a(LX/0QB;)LX/1DS;

    move-result-object v6

    check-cast v6, LX/1DS;

    invoke-static/range {p0 .. p0}, LX/G1I;->a(LX/0QB;)LX/G1I;

    move-result-object v7

    check-cast v7, LX/G1I;

    invoke-static/range {p0 .. p0}, LX/G2n;->a(LX/0QB;)LX/G2n;

    move-result-object v8

    check-cast v8, LX/G2n;

    invoke-static/range {p0 .. p0}, LX/G4x;->a(LX/0QB;)LX/G4x;

    move-result-object v9

    check-cast v9, LX/G4x;

    const/16 v10, 0x36c5

    move-object/from16 v0, p0

    invoke-static {v0, v10}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v10

    const/16 v11, 0x36dc

    move-object/from16 v0, p0

    invoke-static {v0, v11}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v11

    const/16 v12, 0x3654

    move-object/from16 v0, p0

    invoke-static {v0, v12}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v12

    const/16 v13, 0x36d3

    move-object/from16 v0, p0

    invoke-static {v0, v13}, LX/0UN;->a(LX/0QB;I)LX/0Ot;

    move-result-object v13

    invoke-static/range {p0 .. p0}, LX/G2a;->a(LX/0QB;)LX/G2a;

    move-result-object v14

    check-cast v14, LX/G2a;

    invoke-static/range {p0 .. p0}, LX/FuS;->a(LX/0QB;)LX/FuS;

    move-result-object v15

    check-cast v15, LX/FuS;

    invoke-static/range {p0 .. p0}, LX/G2C;->a(LX/0QB;)LX/G2C;

    move-result-object v16

    check-cast v16, LX/G2C;

    invoke-direct/range {v1 .. v16}, LX/G0o;-><init>(Landroid/content/Context;LX/0ad;LX/8p9;LX/Fv5;LX/1DS;LX/G1I;LX/G2n;LX/G4x;LX/0Ot;LX/0Ot;LX/0Ot;LX/0Ot;LX/G2a;LX/FuS;LX/G2C;)V

    .line 2310343
    return-object v1
.end method


# virtual methods
.method public final a(LX/BP0;LX/FrH;LX/BQ1;LX/G4m;LX/Fv9;ZLX/FtA;LX/0g8;)LX/G0n;
    .locals 23

    .prologue
    .line 2310344
    move-object/from16 v0, p0

    iget-object v1, v0, LX/G0o;->c:LX/Fv5;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/G0o;->a:Landroid/content/Context;

    invoke-static/range {p6 .. p6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    const/4 v9, 0x0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    invoke-virtual/range {v1 .. v9}, LX/Fv5;->a(Landroid/content/Context;LX/BP0;LX/FrH;LX/BQ1;LX/G4m;LX/Fv9;Ljava/lang/Boolean;Z)LX/FvG;

    move-result-object v4

    .line 2310345
    move-object/from16 v0, p0

    iget-object v1, v0, LX/G0o;->e:LX/1DS;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/G0o;->k:LX/0Ot;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/G0o;->h:LX/G4x;

    invoke-virtual {v1, v2, v3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    move-object/from16 v0, p7

    invoke-virtual {v1, v0}, LX/1Ql;->a(LX/1PW;)LX/1Ql;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, LX/G0o;->n:LX/FuS;

    invoke-virtual {v1, v2}, LX/1Ql;->a(LX/1DZ;)LX/1Ql;

    move-result-object v1

    move-object/from16 v0, p8

    invoke-virtual {v1, v0}, LX/1Ql;->b(LX/0g8;)LX/1Ql;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ql;->e()LX/1Qq;

    move-result-object v6

    .line 2310346
    move-object/from16 v0, p0

    iget-object v1, v0, LX/G0o;->b:LX/0ad;

    sget-short v2, LX/0wf;->X:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    .line 2310347
    move-object/from16 v0, p0

    iget-object v2, v0, LX/G0o;->e:LX/1DS;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/G0o;->i:LX/0Ot;

    move-object/from16 v0, p0

    iget-object v5, v0, LX/G0o;->f:LX/G1I;

    invoke-virtual {v2, v3, v5}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v2

    move-object/from16 v0, p7

    invoke-virtual {v2, v0}, LX/1Ql;->a(LX/1PW;)LX/1Ql;

    move-result-object v2

    if-eqz v1, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, LX/G0o;->o:LX/G2C;

    :goto_0
    invoke-virtual {v2, v1}, LX/1Ql;->a(LX/1DZ;)LX/1Ql;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ql;->e()LX/1Qq;

    move-result-object v5

    .line 2310348
    const/4 v13, 0x0

    .line 2310349
    move-object/from16 v0, p0

    iget-object v1, v0, LX/G0o;->d:LX/8p9;

    invoke-virtual {v1}, LX/8p9;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2310350
    move-object/from16 v0, p0

    iget-object v1, v0, LX/G0o;->e:LX/1DS;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/G0o;->j:LX/0Ot;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/G0o;->g:LX/G2n;

    invoke-virtual {v1, v2, v3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    move-object/from16 v0, p7

    invoke-virtual {v1, v0}, LX/1Ql;->a(LX/1PW;)LX/1Ql;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ql;->e()LX/1Qq;

    move-result-object v13

    .line 2310351
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, LX/G0o;->b:LX/0ad;

    sget-short v2, LX/0wf;->ap:S

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v7

    .line 2310352
    move-object/from16 v0, p0

    iget-object v1, v0, LX/G0o;->b:LX/0ad;

    sget-short v2, LX/0wf;->B:S

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, LX/0ad;->a(SZ)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2310353
    if-nez v13, :cond_2

    .line 2310354
    new-instance v1, LX/G0n;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/G0o;->f:LX/G1I;

    move-object/from16 v2, p3

    invoke-direct/range {v1 .. v7}, LX/G0n;-><init>(LX/BQ1;LX/G1I;LX/FvG;LX/1Qq;LX/1Qq;Z)V

    move-object v8, v1

    .line 2310355
    :goto_1
    return-object v8

    .line 2310356
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 2310357
    :cond_2
    new-instance v8, LX/G0n;

    move-object/from16 v0, p0

    iget-object v10, v0, LX/G0o;->f:LX/G1I;

    move-object/from16 v9, p3

    move-object v11, v4

    move-object v12, v5

    move-object v14, v6

    move v15, v7

    invoke-direct/range {v8 .. v15}, LX/G0n;-><init>(LX/BQ1;LX/G1I;LX/FvG;LX/1Qq;LX/1Qq;LX/1Qq;Z)V

    goto :goto_1

    .line 2310358
    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, LX/G0o;->e:LX/1DS;

    move-object/from16 v0, p0

    iget-object v2, v0, LX/G0o;->l:LX/0Ot;

    move-object/from16 v0, p0

    iget-object v3, v0, LX/G0o;->m:LX/G2a;

    invoke-virtual {v1, v2, v3}, LX/1DS;->a(LX/0Ot;LX/0g1;)LX/1Ql;

    move-result-object v1

    move-object/from16 v0, p7

    invoke-virtual {v1, v0}, LX/1Ql;->a(LX/1PW;)LX/1Ql;

    move-result-object v1

    invoke-virtual {v1}, LX/1Ql;->e()LX/1Qq;

    move-result-object v10

    .line 2310359
    if-nez v13, :cond_4

    .line 2310360
    new-instance v8, LX/G0n;

    move-object/from16 v0, p0

    iget-object v14, v0, LX/G0o;->f:LX/G1I;

    move-object v9, v4

    move-object v11, v5

    move-object v12, v6

    move-object/from16 v13, p3

    move v15, v7

    invoke-direct/range {v8 .. v15}, LX/G0n;-><init>(LX/FvG;LX/1Qq;LX/1Qq;LX/1Qq;LX/BQ1;LX/G1I;Z)V

    goto :goto_1

    .line 2310361
    :cond_4
    new-instance v14, LX/G0n;

    move-object/from16 v0, p0

    iget-object v0, v0, LX/G0o;->f:LX/G1I;

    move-object/from16 v21, v0

    move-object v15, v4

    move-object/from16 v16, v10

    move-object/from16 v17, v5

    move-object/from16 v18, v13

    move-object/from16 v19, v6

    move-object/from16 v20, p3

    move/from16 v22, v7

    invoke-direct/range {v14 .. v22}, LX/G0n;-><init>(LX/FvG;LX/1Qq;LX/1Qq;LX/1Qq;LX/1Qq;LX/BQ1;LX/G1I;Z)V

    move-object v8, v14

    goto :goto_1
.end method
