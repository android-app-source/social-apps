.class public LX/Fg6;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/CyQ;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/CyQ",
        "<",
        "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

.field public final b:Landroid/content/res/Resources;

.field private final c:LX/Cvk;


# direct methods
.method public constructor <init>(Lcom/facebook/search/results/model/SearchResultsMutableContext;Landroid/content/res/Resources;LX/Cvk;)V
    .locals 0

    .prologue
    .line 2269312
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2269313
    iput-object p1, p0, LX/Fg6;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2269314
    iput-object p2, p0, LX/Fg6;->b:Landroid/content/res/Resources;

    .line 2269315
    iput-object p3, p0, LX/Fg6;->c:LX/Cvk;

    .line 2269316
    return-void
.end method

.method private static b(LX/8cK;)Lcom/facebook/graphql/model/GraphQLNode;
    .locals 5

    .prologue
    .line 2269317
    new-instance v0, LX/173;

    invoke-direct {v0}, LX/173;-><init>()V

    .line 2269318
    iget-object v1, p0, LX/8cI;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2269319
    iput-object v1, v0, LX/173;->f:Ljava/lang/String;

    .line 2269320
    move-object v0, v0

    .line 2269321
    invoke-virtual {v0}, LX/173;->a()Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    move-result-object v0

    .line 2269322
    iget-object v1, p0, LX/8cI;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2269323
    invoke-static {v1}, LX/38I;->a(Ljava/lang/String;)I

    move-result v1

    .line 2269324
    new-instance v2, LX/4XR;

    invoke-direct {v2}, LX/4XR;-><init>()V

    .line 2269325
    iget-object v3, p0, LX/8cI;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2269326
    iput-object v3, v2, LX/4XR;->fO:Ljava/lang/String;

    .line 2269327
    move-object v2, v2

    .line 2269328
    new-instance v3, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;

    invoke-direct {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;-><init>()V

    .line 2269329
    iput v1, v3, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;->a:I

    .line 2269330
    move-object v3, v3

    .line 2269331
    invoke-virtual {v3}, Lcom/facebook/graphql/enums/GraphQLObjectType$Builder;->a()Lcom/facebook/graphql/enums/GraphQLObjectType;

    move-result-object v3

    .line 2269332
    iput-object v3, v2, LX/4XR;->qc:Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2269333
    move-object v2, v2

    .line 2269334
    iget-object v3, p0, LX/8cI;->b:Ljava/lang/String;

    move-object v3, v3

    .line 2269335
    iput-object v3, v2, LX/4XR;->iB:Ljava/lang/String;

    .line 2269336
    move-object v2, v2

    .line 2269337
    new-instance v3, LX/2dc;

    invoke-direct {v3}, LX/2dc;-><init>()V

    .line 2269338
    iget-object v4, p0, LX/8cK;->c:Ljava/lang/String;

    move-object v4, v4

    .line 2269339
    iput-object v4, v3, LX/2dc;->h:Ljava/lang/String;

    .line 2269340
    move-object v3, v3

    .line 2269341
    invoke-virtual {v3}, LX/2dc;->a()Lcom/facebook/graphql/model/GraphQLImage;

    move-result-object v3

    .line 2269342
    iput-object v3, v2, LX/4XR;->kR:Lcom/facebook/graphql/model/GraphQLImage;

    .line 2269343
    move-object v2, v2

    .line 2269344
    iget-boolean v3, p0, LX/8cK;->d:Z

    move v3, v3

    .line 2269345
    iput-boolean v3, v2, LX/4XR;->hi:Z

    .line 2269346
    move-object v2, v2

    .line 2269347
    sparse-switch v1, :sswitch_data_0

    .line 2269348
    :goto_0
    invoke-virtual {v2}, LX/4XR;->a()Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v0

    return-object v0

    .line 2269349
    :sswitch_0
    iget-object v1, p0, LX/8cK;->f:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    move-object v1, v1

    .line 2269350
    iput-object v1, v2, LX/4XR;->fb:Lcom/facebook/graphql/enums/GraphQLFriendshipStatus;

    .line 2269351
    move-object v1, v2

    .line 2269352
    iput-object v0, v1, LX/4XR;->aA:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2269353
    goto :goto_0

    .line 2269354
    :sswitch_1
    new-instance v0, LX/0Pz;

    invoke-direct {v0}, LX/0Pz;-><init>()V

    .line 2269355
    iget-object v1, p0, LX/8cK;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2269356
    invoke-static {v1}, LX/0XM;->isNullOrEmpty(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2269357
    iget-object v1, p0, LX/8cK;->b:Ljava/lang/String;

    move-object v1, v1

    .line 2269358
    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    .line 2269359
    :cond_0
    :goto_1
    invoke-virtual {v0}, LX/0Pz;->b()LX/0Px;

    move-result-object v0

    .line 2269360
    iput-object v0, v2, LX/4XR;->mm:LX/0Px;

    .line 2269361
    move-object v0, v2

    .line 2269362
    iget-object v1, p0, LX/8cK;->e:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    move-object v1, v1

    .line 2269363
    iput-object v1, v0, LX/4XR;->pd:Lcom/facebook/graphql/enums/GraphQLPageVerificationBadge;

    .line 2269364
    move-object v0, v0

    .line 2269365
    iget-boolean v1, p0, LX/8cK;->g:Z

    move v1, v1

    .line 2269366
    iput-boolean v1, v0, LX/4XR;->ds:Z

    .line 2269367
    goto :goto_0

    .line 2269368
    :cond_1
    iget-object v1, p0, LX/8cI;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2269369
    if-eqz v1, :cond_0

    .line 2269370
    iget-object v1, p0, LX/8cI;->c:Ljava/lang/String;

    move-object v1, v1

    .line 2269371
    invoke-virtual {v0, v1}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2269372
    :sswitch_2
    iget-object v1, p0, LX/8cK;->h:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    move-object v1, v1

    .line 2269373
    iput-object v1, v2, LX/4XR;->pC:Lcom/facebook/graphql/enums/GraphQLGroupJoinState;

    .line 2269374
    move-object v1, v2

    .line 2269375
    iput-object v0, v1, LX/4XR;->mL:Lcom/facebook/graphql/model/GraphQLTextWithEntities;

    .line 2269376
    goto :goto_0

    .line 2269377
    :sswitch_3
    iget-object v0, p0, LX/8cI;->c:Ljava/lang/String;

    move-object v0, v0

    .line 2269378
    iput-object v0, v2, LX/4XR;->oe:Ljava/lang/String;

    .line 2269379
    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x25d6af -> :sswitch_1
        0x285feb -> :sswitch_0
        0x403827a -> :sswitch_3
        0x41e065f -> :sswitch_2
        0x499e8e7 -> :sswitch_1
    .end sparse-switch
.end method

.method private c(Ljava/util/List;)Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "LX/8cK;",
            ">;)",
            "Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;"
        }
    .end annotation

    .prologue
    .line 2269380
    new-instance v10, LX/0Pz;

    invoke-direct {v10}, LX/0Pz;-><init>()V

    .line 2269381
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, LX/8cK;

    .line 2269382
    iget-object v0, v9, LX/8cI;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2269383
    invoke-static {v0}, LX/38I;->a(Ljava/lang/String;)I

    move-result v11

    .line 2269384
    invoke-static {v9}, LX/CyV;->b(LX/8cK;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    move-result-object v1

    .line 2269385
    invoke-static {v9}, LX/CyV;->a(LX/8cK;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v12

    .line 2269386
    iget-object v0, p0, LX/Fg6;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v0}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v12, v0}, LX/7BG;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2269387
    new-instance v5, Lcom/facebook/graphql/enums/GraphQLObjectType;

    .line 2269388
    iget-object v0, v9, LX/8cI;->d:Ljava/lang/String;

    move-object v0, v0

    .line 2269389
    invoke-direct {v5, v0}, Lcom/facebook/graphql/enums/GraphQLObjectType;-><init>(Ljava/lang/String;)V

    .line 2269390
    new-instance v7, LX/162;

    sget-object v0, LX/0mC;->a:LX/0mC;

    invoke-direct {v7, v0}, LX/162;-><init>(LX/0mC;)V

    .line 2269391
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 2269392
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/8cK;

    .line 2269393
    iget-object v3, v0, LX/8cI;->d:Ljava/lang/String;

    move-object v3, v3

    .line 2269394
    invoke-static {v3}, LX/38I;->a(Ljava/lang/String;)I

    move-result v3

    if-ne v11, v3, :cond_0

    .line 2269395
    iget-object v3, v0, LX/8cI;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2269396
    invoke-virtual {v7, v3}, LX/162;->g(Ljava/lang/String;)LX/162;

    .line 2269397
    iget-object v3, v0, LX/8cI;->a:Ljava/lang/String;

    move-object v3, v3

    .line 2269398
    invoke-virtual {v13, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2269399
    :cond_1
    new-instance v0, LX/Cw2;

    invoke-virtual {v13}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v12}, LX/7CJ;->a(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, LX/Cw4;

    iget-object v8, p0, LX/Fg6;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-direct {v4, v8}, LX/Cw4;-><init>(Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    invoke-static {v5}, LX/7CN;->a(Lcom/facebook/graphql/enums/GraphQLObjectType;)Ljava/lang/String;

    move-result-object v5

    const-string v8, "bootstrap"

    invoke-direct/range {v0 .. v8}, LX/Cw2;-><init>(Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;Ljava/lang/Integer;Ljava/lang/String;LX/Cw4;Ljava/lang/String;Ljava/lang/String;LX/162;Ljava/lang/String;)V

    .line 2269400
    invoke-virtual {v13}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 2269401
    invoke-virtual {v13, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, LX/8cK;

    .line 2269402
    iget-object v5, v3, LX/8cI;->d:Ljava/lang/String;

    move-object v5, v5

    .line 2269403
    invoke-static {v5}, LX/38I;->a(Ljava/lang/String;)I

    move-result v5

    if-ne v11, v5, :cond_2

    .line 2269404
    iget-object v5, p0, LX/Fg6;->c:LX/Cvk;

    new-instance v8, LX/Cw0;

    invoke-direct {v8, v2, v0}, LX/Cw0;-><init>(Ljava/lang/String;LX/Cw2;)V

    invoke-virtual {v5, v8}, LX/Cvk;->a(LX/Cw0;)Ljava/lang/String;

    move-result-object v2

    .line 2269405
    new-instance v5, LX/9zN;

    invoke-direct {v5}, LX/9zN;-><init>()V

    .line 2269406
    iput-object v2, v5, LX/9zN;->d:Ljava/lang/String;

    .line 2269407
    move-object v2, v5

    .line 2269408
    invoke-static {v3}, LX/Fg6;->b(LX/8cK;)Lcom/facebook/graphql/model/GraphQLNode;

    move-result-object v3

    .line 2269409
    iput-object v3, v2, LX/9zN;->f:Lcom/facebook/graphql/model/GraphQLNode;

    .line 2269410
    move-object v2, v2

    .line 2269411
    invoke-virtual {v2}, LX/9zN;->a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$ModuleResultEdgeModel;

    move-result-object v2

    invoke-virtual {v10, v2}, LX/0Pz;->c(Ljava/lang/Object;)LX/0Pz;

    goto :goto_1

    .line 2269412
    :cond_3
    invoke-virtual {v10}, LX/0Pz;->b()LX/0Px;

    move-result-object v2

    .line 2269413
    new-instance v3, LX/0m9;

    sget-object v4, LX/0mC;->a:LX/0mC;

    invoke-direct {v3, v4}, LX/0m9;-><init>(LX/0mC;)V

    .line 2269414
    const-string v4, "bootstrap_item_count"

    invoke-virtual {v2}, LX/0Px;->size()I

    move-result v5

    invoke-virtual {v3, v4, v5}, LX/0m9;->a(Ljava/lang/String;I)LX/0m9;

    move-result-object v4

    const-string v5, "bootstrap_item_ids"

    invoke-virtual {v4, v5, v7}, LX/0m9;->c(Ljava/lang/String;LX/0lF;)LX/0lF;

    .line 2269415
    iget-object v4, p0, LX/Fg6;->c:LX/Cvk;

    invoke-virtual {v4, v0}, LX/Cvk;->a(LX/Cw2;)Ljava/lang/String;

    move-result-object v0

    .line 2269416
    new-instance v4, LX/9zL;

    invoke-direct {v4}, LX/9zL;-><init>()V

    new-instance v5, LX/9zG;

    invoke-direct {v5}, LX/9zG;-><init>()V

    .line 2269417
    iput-object v0, v5, LX/9zG;->k:Ljava/lang/String;

    .line 2269418
    move-object v0, v5

    .line 2269419
    invoke-static {v12}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v5

    .line 2269420
    iput-object v5, v0, LX/9zG;->a:LX/0Px;

    .line 2269421
    move-object v0, v0

    .line 2269422
    iput-object v1, v0, LX/9zG;->n:Lcom/facebook/graphql/enums/GraphQLGraphSearchResultRole;

    .line 2269423
    move-object v0, v0

    .line 2269424
    new-instance v1, LX/9zH;

    invoke-direct {v1}, LX/9zH;-><init>()V

    .line 2269425
    iput-object v2, v1, LX/9zH;->a:LX/0Px;

    .line 2269426
    move-object v1, v1

    .line 2269427
    invoke-virtual {v1}, LX/9zH;->a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;

    move-result-object v1

    .line 2269428
    iput-object v1, v0, LX/9zG;->m:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel$ResultsModel;

    .line 2269429
    move-object v0, v0

    .line 2269430
    invoke-virtual {v3}, LX/0m9;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2269431
    iput-object v1, v0, LX/9zG;->e:Ljava/lang/String;

    .line 2269432
    move-object v0, v0

    .line 2269433
    iget-object v1, v9, LX/8cI;->d:Ljava/lang/String;

    move-object v1, v1

    .line 2269434
    invoke-static {v1}, LX/38I;->a(Ljava/lang/String;)I

    move-result v1

    .line 2269435
    sparse-switch v1, :sswitch_data_0

    .line 2269436
    const/4 v1, 0x0

    :goto_2
    move-object v1, v1

    .line 2269437
    iput-object v1, v0, LX/9zG;->r:Ljava/lang/String;

    .line 2269438
    move-object v0, v0

    .line 2269439
    new-instance v1, LX/8fi;

    invoke-direct {v1}, LX/8fi;-><init>()V

    .line 2269440
    iput-object v6, v1, LX/8fi;->b:Ljava/lang/String;

    .line 2269441
    move-object v1, v1

    .line 2269442
    new-instance v2, LX/8fj;

    invoke-direct {v2}, LX/8fj;-><init>()V

    iget-object v3, p0, LX/Fg6;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v3}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v3

    .line 2269443
    iput-object v3, v2, LX/8fj;->a:Ljava/lang/String;

    .line 2269444
    move-object v2, v2

    .line 2269445
    invoke-virtual {v2}, LX/8fj;->a()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;

    move-result-object v2

    .line 2269446
    iput-object v2, v1, LX/8fi;->d:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel$QueryTitleModel;

    .line 2269447
    move-object v1, v1

    .line 2269448
    invoke-virtual {v1}, LX/8fi;->a()Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    move-result-object v1

    .line 2269449
    iput-object v1, v0, LX/9zG;->o:Lcom/facebook/search/results/protocol/SearchResultsSeeMoreQueryModels$SearchResultsSeeMoreQueryModel;

    .line 2269450
    move-object v0, v0

    .line 2269451
    invoke-virtual {v0}, LX/9zG;->a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;

    move-result-object v0

    .line 2269452
    iput-object v0, v4, LX/9zL;->f:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;

    .line 2269453
    move-object v0, v4

    .line 2269454
    invoke-virtual {v0}, LX/9zL;->a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;

    move-result-object v0

    return-object v0

    .line 2269455
    :sswitch_0
    iget-object v1, p0, LX/Fg6;->b:Landroid/content/res/Resources;

    const v2, 0x7f0822bf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 2269456
    :sswitch_1
    iget-object v1, p0, LX/Fg6;->b:Landroid/content/res/Resources;

    const v2, 0x7f0822c0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 2269457
    :sswitch_2
    iget-object v1, p0, LX/Fg6;->b:Landroid/content/res/Resources;

    const v2, 0x7f0822c1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 2269458
    :sswitch_3
    iget-object v1, p0, LX/Fg6;->b:Landroid/content/res/Resources;

    const v2, 0x7f0822c4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 2269459
    :sswitch_4
    iget-object v1, p0, LX/Fg6;->b:Landroid/content/res/Resources;

    const v2, 0x7f0822c3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    .line 2269460
    :sswitch_5
    iget-object v1, p0, LX/Fg6;->b:Landroid/content/res/Resources;

    const v2, 0x7f0822c2

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        -0x3ff252d0 -> :sswitch_5
        0x25d6af -> :sswitch_1
        0x285feb -> :sswitch_0
        0x403827a -> :sswitch_3
        0x41e065f -> :sswitch_2
        0x499e8e7 -> :sswitch_4
    .end sparse-switch
.end method


# virtual methods
.method public final a(Ljava/util/List;)Ljava/lang/Object;
    .locals 11

    .prologue
    .line 2269461
    new-instance v0, LX/9zK;

    invoke-direct {v0}, LX/9zK;-><init>()V

    .line 2269462
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2269463
    const/4 v1, 0x0

    .line 2269464
    iput v1, v0, LX/9zK;->b:I

    .line 2269465
    :goto_0
    iget-object v1, p0, LX/Fg6;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    .line 2269466
    iget-object v2, v1, Lcom/facebook/search/results/model/SearchResultsMutableContext;->t:Ljava/lang/String;

    move-object v1, v2

    .line 2269467
    iput-object v1, v0, LX/9zK;->h:Ljava/lang/String;

    .line 2269468
    move-object v1, v0

    .line 2269469
    iget-object v2, p0, LX/Fg6;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v2}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->u()Ljava/lang/String;

    move-result-object v2

    .line 2269470
    iput-object v2, v1, LX/9zK;->f:Ljava/lang/String;

    .line 2269471
    iget-object v1, p0, LX/Fg6;->c:LX/Cvk;

    new-instance v2, LX/Cw4;

    iget-object v3, p0, LX/Fg6;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-direct {v2, v3}, LX/Cw4;-><init>(Lcom/facebook/search/results/model/SearchResultsMutableContext;)V

    invoke-virtual {v1, v2}, LX/Cvk;->a(LX/Cw4;)Ljava/lang/String;

    move-result-object v1

    .line 2269472
    new-instance v2, LX/9zI;

    invoke-direct {v2}, LX/9zI;-><init>()V

    new-instance v3, LX/9zJ;

    invoke-direct {v3}, LX/9zJ;-><init>()V

    iget-object v4, p0, LX/Fg6;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v4}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->b()Ljava/lang/String;

    move-result-object v4

    .line 2269473
    iput-object v4, v3, LX/9zJ;->f:Ljava/lang/String;

    .line 2269474
    move-object v3, v3

    .line 2269475
    iput-object v1, v3, LX/9zJ;->c:Ljava/lang/String;

    .line 2269476
    move-object v1, v3

    .line 2269477
    new-instance v3, LX/9zM;

    invoke-direct {v3}, LX/9zM;-><init>()V

    iget-object v4, p0, LX/Fg6;->a:Lcom/facebook/search/results/model/SearchResultsMutableContext;

    invoke-virtual {v4}, Lcom/facebook/search/results/model/SearchResultsMutableContext;->a()Ljava/lang/String;

    move-result-object v4

    .line 2269478
    iput-object v4, v3, LX/9zM;->a:Ljava/lang/String;

    .line 2269479
    move-object v3, v3

    .line 2269480
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 2269481
    new-instance v5, LX/186;

    const/16 v6, 0x80

    invoke-direct {v5, v6}, LX/186;-><init>(I)V

    .line 2269482
    iget-object v6, v3, LX/9zM;->a:Ljava/lang/String;

    invoke-virtual {v5, v6}, LX/186;->b(Ljava/lang/String;)I

    move-result v6

    .line 2269483
    invoke-virtual {v5, v9}, LX/186;->c(I)V

    .line 2269484
    invoke-virtual {v5, v8, v6}, LX/186;->b(II)V

    .line 2269485
    invoke-virtual {v5}, LX/186;->d()I

    move-result v6

    .line 2269486
    invoke-virtual {v5, v6}, LX/186;->d(I)V

    .line 2269487
    invoke-virtual {v5}, LX/186;->e()[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 2269488
    invoke-virtual {v6, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2269489
    new-instance v5, LX/15i;

    move-object v8, v7

    move-object v10, v7

    invoke-direct/range {v5 .. v10}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2269490
    new-instance v6, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$QueryTitleModel;

    invoke-direct {v6, v5}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$QueryTitleModel;-><init>(LX/15i;)V

    .line 2269491
    move-object v3, v6

    .line 2269492
    iput-object v3, v1, LX/9zJ;->g:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$QueryTitleModel;

    .line 2269493
    move-object v1, v1

    .line 2269494
    invoke-virtual {v0}, LX/9zK;->a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;

    move-result-object v0

    .line 2269495
    iput-object v0, v1, LX/9zJ;->d:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel;

    .line 2269496
    move-object v0, v1

    .line 2269497
    invoke-virtual {v0}, LX/9zJ;->a()Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel;

    move-result-object v0

    .line 2269498
    iput-object v0, v2, LX/9zI;->a:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel;

    .line 2269499
    move-object v0, v2

    .line 2269500
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 2269501
    new-instance v5, LX/186;

    const/16 v6, 0x80

    invoke-direct {v5, v6}, LX/186;-><init>(I)V

    .line 2269502
    iget-object v6, v0, LX/9zI;->a:Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel;

    invoke-static {v5, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2269503
    invoke-virtual {v5, v9}, LX/186;->c(I)V

    .line 2269504
    invoke-virtual {v5, v8, v6}, LX/186;->b(II)V

    .line 2269505
    invoke-virtual {v5}, LX/186;->d()I

    move-result v6

    .line 2269506
    invoke-virtual {v5, v6}, LX/186;->d(I)V

    .line 2269507
    invoke-virtual {v5}, LX/186;->e()[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v6

    .line 2269508
    invoke-virtual {v6, v8}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2269509
    new-instance v5, LX/15i;

    move-object v8, v7

    move-object v10, v7

    invoke-direct/range {v5 .. v10}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2269510
    new-instance v6, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;

    invoke-direct {v6, v5}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel;-><init>(LX/15i;)V

    .line 2269511
    move-object v0, v6

    .line 2269512
    return-object v0

    .line 2269513
    :cond_0
    const/4 v1, 0x1

    .line 2269514
    iput v1, v0, LX/9zK;->b:I

    .line 2269515
    move-object v1, v0

    .line 2269516
    invoke-direct {p0, p1}, LX/Fg6;->c(Ljava/util/List;)Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchQueryModel$FilteredQueryModel$ModulesModel$EdgesModel;

    move-result-object v2

    invoke-static {v2}, LX/0Px;->of(Ljava/lang/Object;)LX/0Px;

    move-result-object v2

    .line 2269517
    iput-object v2, v1, LX/9zK;->d:LX/0Px;

    .line 2269518
    goto/16 :goto_0
.end method
