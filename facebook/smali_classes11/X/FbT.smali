.class public LX/FbT;
.super LX/FbD;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/FbD",
        "<",
        "Lcom/facebook/search/results/model/unit/SearchResultsEmptyEntityUnit;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile a:LX/FbT;


# direct methods
.method public constructor <init>()V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2260334
    invoke-direct {p0}, LX/FbD;-><init>()V

    .line 2260335
    return-void
.end method

.method public static a(LX/0QB;)LX/FbT;
    .locals 3

    .prologue
    .line 2260337
    sget-object v0, LX/FbT;->a:LX/FbT;

    if-nez v0, :cond_1

    .line 2260338
    const-class v1, LX/FbT;

    monitor-enter v1

    .line 2260339
    :try_start_0
    sget-object v0, LX/FbT;->a:LX/FbT;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2260340
    if-eqz v2, :cond_0

    .line 2260341
    :try_start_1
    new-instance v0, LX/FbT;

    invoke-direct {v0}, LX/FbT;-><init>()V

    .line 2260342
    move-object v0, v0

    .line 2260343
    sput-object v0, LX/FbT;->a:LX/FbT;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2260344
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2260345
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2260346
    :cond_1
    sget-object v0, LX/FbT;->a:LX/FbT;

    return-object v0

    .line 2260347
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2260348
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/search/model/SearchResultsBaseFeedUnit;
    .locals 4
    .annotation build Ljavax/annotation/Nullable;
    .end annotation

    .prologue
    .line 2260336
    new-instance v0, Lcom/facebook/search/results/model/unit/SearchResultsEmptyEntityUnit;

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->q()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, LX/Fbf;->a(Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;)Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;

    move-result-object v2

    invoke-virtual {p1}, Lcom/facebook/search/protocol/FetchKeywordSearchResultsGraphQLModels$KeywordSearchModuleFragmentModel;->e()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/facebook/search/results/model/unit/SearchResultsEmptyEntityUnit;-><init>(Ljava/lang/String;Lcom/facebook/graphql/enums/GraphQLGraphSearchResultsDisplayStyle;Ljava/lang/String;)V

    return-object v0
.end method
