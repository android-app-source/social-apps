.class public abstract LX/GOr;
.super Landroid/widget/ArrayAdapter;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        "V:",
        "Landroid/view/View;",
        ">",
        "Landroid/widget/ArrayAdapter",
        "<TT;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 2348015
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 2348016
    return-void
.end method


# virtual methods
.method public abstract a()Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TV;"
        }
    .end annotation
.end method

.method public abstract a(Landroid/view/View;Ljava/lang/Object;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TV;TT;)V"
        }
    .end annotation
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 2348011
    if-eqz p2, :cond_0

    .line 2348012
    :goto_0
    invoke-virtual {p0, p1}, LX/GOr;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, p2, v0}, LX/GOr;->a(Landroid/view/View;Ljava/lang/Object;)V

    .line 2348013
    return-object p2

    .line 2348014
    :cond_0
    invoke-virtual {p0}, LX/GOr;->a()Landroid/view/View;

    move-result-object p2

    goto :goto_0
.end method
