.class public final LX/GVm;
.super LX/5pb;
.source ""


# annotations
.annotation runtime Lcom/facebook/react/module/annotations/ReactModule;
    name = "RKBuildInfo"
.end annotation


# direct methods
.method public constructor <init>(LX/5pY;)V
    .locals 0

    .prologue
    .line 2359905
    invoke-direct {p0, p1}, LX/5pb;-><init>(LX/5pY;)V

    .line 2359906
    return-void
.end method


# virtual methods
.method public final b()Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2359907
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2359908
    iget-object v1, p0, LX/5pb;->a:LX/5pY;

    move-object v1, v1

    .line 2359909
    sget-object v2, LX/1Mr;->a:LX/1gy;

    if-nez v2, :cond_0

    .line 2359910
    new-instance v2, LX/1Mr;

    new-instance v3, LX/04P;

    invoke-direct {v3, v1}, LX/04P;-><init>(Landroid/content/Context;)V

    invoke-direct {v2, v1, v3}, LX/1Mr;-><init>(Landroid/content/Context;LX/04P;)V

    .line 2359911
    invoke-virtual {v2}, LX/1Mr;->a()LX/1gy;

    move-result-object v2

    sput-object v2, LX/1Mr;->a:LX/1gy;

    .line 2359912
    :cond_0
    sget-object v2, LX/1Mr;->a:LX/1gy;

    move-object v2, v2

    .line 2359913
    new-instance v3, LX/GVn;

    invoke-direct {v3, v1}, LX/GVn;-><init>(Landroid/content/Context;)V

    .line 2359914
    const-string v4, "appVersion"

    .line 2359915
    iget-object v5, v3, LX/GVn;->c:Ljava/lang/String;

    move-object v5, v5

    .line 2359916
    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2359917
    const-string v4, "buildBranchName"

    iget-object v5, v2, LX/1gy;->b:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2359918
    const-string v4, "buildRevision"

    iget-object v5, v2, LX/1gy;->a:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2359919
    const-string v4, "buildTime"

    iget-wide v6, v2, LX/1gy;->c:J

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v4, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2359920
    const-string v2, "buildVersion"

    .line 2359921
    iget v4, v3, LX/GVn;->d:I

    move v3, v4

    .line 2359922
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2359923
    const-string v2, "bundleIdentifier"

    invoke-virtual {v1}, LX/5pY;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2359924
    return-object v0
.end method

.method public final getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2359925
    const-string v0, "RKBuildInfo"

    return-object v0
.end method
