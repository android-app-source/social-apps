.class public final enum LX/FQR;
.super Ljava/lang/Enum;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "LX/FQR;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[LX/FQR;

.field public static final enum ONAVO_PROTECT:LX/FQR;

.field public static final enum PROMOTION:LX/FQR;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2239368
    new-instance v0, LX/FQR;

    const-string v1, "ONAVO_PROTECT"

    invoke-direct {v0, v1, v2}, LX/FQR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FQR;->ONAVO_PROTECT:LX/FQR;

    .line 2239369
    new-instance v0, LX/FQR;

    const-string v1, "PROMOTION"

    invoke-direct {v0, v1, v3}, LX/FQR;-><init>(Ljava/lang/String;I)V

    sput-object v0, LX/FQR;->PROMOTION:LX/FQR;

    .line 2239370
    const/4 v0, 0x2

    new-array v0, v0, [LX/FQR;

    sget-object v1, LX/FQR;->ONAVO_PROTECT:LX/FQR;

    aput-object v1, v0, v2

    sget-object v1, LX/FQR;->PROMOTION:LX/FQR;

    aput-object v1, v0, v3

    sput-object v0, LX/FQR;->$VALUES:[LX/FQR;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 2239371
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)LX/FQR;
    .locals 1

    .prologue
    .line 2239372
    const-class v0, LX/FQR;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, LX/FQR;

    return-object v0
.end method

.method public static values()[LX/FQR;
    .locals 1

    .prologue
    .line 2239373
    sget-object v0, LX/FQR;->$VALUES:[LX/FQR;

    invoke-virtual {v0}, [Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [LX/FQR;

    return-object v0
.end method
