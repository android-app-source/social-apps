.class public LX/FSE;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;"
        }
    .end annotation
.end field

.field private final b:LX/0Ot;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;"
        }
    .end annotation
.end field

.field private final c:LX/FSO;

.field private final d:LX/1R0;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/1R0",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            "LX/1Pf;",
            ">;"
        }
    .end annotation
.end field

.field private final e:LX/03V;


# direct methods
.method public constructor <init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/1R0;LX/FSP;LX/FSF;LX/03V;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "LX/0Ot",
            "<",
            "Lcom/facebook/common/executors/AndroidThreadUtil;",
            ">;",
            "LX/0Ot",
            "<",
            "LX/0Uo;",
            ">;",
            "LX/1R0;",
            "LX/FSP;",
            "LX/FSF;",
            "Lcom/facebook/common/errorreporting/FbErrorReporter;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2241667
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2241668
    iput-object p2, p0, LX/FSE;->a:LX/0Ot;

    .line 2241669
    iput-object p3, p0, LX/FSE;->b:LX/0Ot;

    .line 2241670
    iput-object p4, p0, LX/FSE;->d:LX/1R0;

    .line 2241671
    const-string v0, "native_newsfeed"

    invoke-virtual {p5, p1, v0, p6}, LX/FSP;->a(Landroid/content/Context;Ljava/lang/String;LX/1PT;)LX/FSO;

    move-result-object v0

    iput-object v0, p0, LX/FSE;->c:LX/FSO;

    .line 2241672
    iput-object p7, p0, LX/FSE;->e:LX/03V;

    .line 2241673
    return-void
.end method

.method public static a(LX/0QB;)LX/FSE;
    .locals 9

    .prologue
    .line 2241674
    new-instance v1, LX/FSE;

    const-class v2, Landroid/content/Context;

    invoke-interface {p0, v2}, LX/0QC;->getInstance(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/Context;

    const/16 v3, 0x271

    invoke-static {p0, v3}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v3

    const/16 v4, 0x245

    invoke-static {p0, v4}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object v4

    invoke-static {p0}, LX/FSD;->b(LX/0QB;)LX/1R0;

    move-result-object v5

    check-cast v5, LX/1R0;

    const-class v6, LX/FSP;

    invoke-interface {p0, v6}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v6

    check-cast v6, LX/FSP;

    invoke-static {p0}, LX/FSF;->a(LX/0QB;)LX/FSF;

    move-result-object v7

    check-cast v7, LX/FSF;

    invoke-static {p0}, LX/0VB;->a(LX/0QB;)LX/03U;

    move-result-object v8

    check-cast v8, LX/03V;

    invoke-direct/range {v1 .. v8}, LX/FSE;-><init>(Landroid/content/Context;LX/0Ot;LX/0Ot;LX/1R0;LX/FSP;LX/FSF;LX/03V;)V

    .line 2241675
    move-object v0, v1

    .line 2241676
    return-object v0
.end method

.method public static b(LX/FSE;Ljava/util/Collection;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2241677
    iget-object v0, p0, LX/FSE;->c:LX/FSO;

    .line 2241678
    iget-object v1, v0, LX/FSO;->e:LX/FSM;

    invoke-virtual {v1}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->c()V

    .line 2241679
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/graphql/model/FeedUnit;

    .line 2241680
    iget-object v2, p0, LX/FSE;->d:LX/1R0;

    iget-object v3, p0, LX/FSE;->c:LX/FSO;

    invoke-interface {v2, v0, v3}, LX/1R0;->a(Ljava/lang/Object;LX/1PW;)LX/1RA;

    move-result-object v2

    .line 2241681
    invoke-interface {v0}, Lcom/facebook/graphql/model/interfaces/FeedUnitCommon;->g()Ljava/lang/String;

    move-result-object v3

    .line 2241682
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, LX/1RA;->a()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 2241683
    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, LX/1RA;->a(I)LX/1Rb;

    move-result-object v4

    if-eqz v4, :cond_1

    .line 2241684
    iget-object v4, p0, LX/FSE;->c:LX/FSO;

    invoke-virtual {v2, v0}, LX/1RA;->a(I)LX/1Rb;

    move-result-object v5

    .line 2241685
    iget-object v6, v4, LX/FSO;->e:LX/FSM;

    invoke-virtual {v6, v3, v5}, Lcom/facebook/prefetch/feed/generatedenvironment/AbstractMultiRowPrefetcherImpl;->a(Ljava/lang/String;LX/1Rb;)V

    .line 2241686
    :cond_1
    :try_start_0
    invoke-virtual {v2, v0}, LX/1RA;->e(I)Z
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2241687
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2241688
    :catch_0
    move-exception v4

    .line 2241689
    iget-object v5, p0, LX/FSE;->e:LX/03V;

    const-string v6, "MultiRowBackgroundPrefetcher_ensurePreparedException"

    invoke-virtual {v4}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v4

    const/16 v7, 0x64

    invoke-virtual {v5, v6, v4, v7}, LX/03V;->a(Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1

    .line 2241690
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Lcom/facebook/graphql/model/FeedUnit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2241691
    iget-object v0, p0, LX/FSE;->b:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Uo;

    invoke-virtual {v0}, LX/0Uo;->j()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2241692
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Background prefetch can only run when app is background"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 2241693
    :cond_0
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2241694
    :cond_1
    :goto_0
    return-void

    .line 2241695
    :cond_2
    iget-object v0, p0, LX/FSE;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    invoke-virtual {v0}, LX/0Sh;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2241696
    invoke-static {p0, p1}, LX/FSE;->b(LX/FSE;Ljava/util/Collection;)V

    goto :goto_0

    .line 2241697
    :cond_3
    iget-object v0, p0, LX/FSE;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/0Sh;

    new-instance v1, Lcom/facebook/prefetch/feed/MultiRowBackgroundPrefetcher$1;

    invoke-direct {v1, p0, p1}, Lcom/facebook/prefetch/feed/MultiRowBackgroundPrefetcher$1;-><init>(LX/FSE;Ljava/util/Collection;)V

    invoke-virtual {v0, v1}, LX/0Sh;->b(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
