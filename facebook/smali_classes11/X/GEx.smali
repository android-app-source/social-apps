.class public LX/GEx;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements LX/GEW;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "LX/GEW",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private a:LX/GMH;

.field private b:LX/0ad;


# direct methods
.method public constructor <init>(LX/GMH;LX/0ad;)V
    .locals 0
    .param p1    # LX/GMH;
        .annotation runtime Lcom/facebook/inject/Assisted;
        .end annotation
    .end param
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2332183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2332184
    iput-object p1, p0, LX/GEx;->a:LX/GMH;

    .line 2332185
    iput-object p2, p0, LX/GEx;->b:LX/0ad;

    .line 2332186
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2332189
    const v0, 0x7f030077

    return v0
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2332190
    invoke-static {p1}, LX/GG6;->g(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2332191
    :cond_0
    :goto_0
    return v0

    .line 2332192
    :cond_1
    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->a()LX/GGB;

    move-result-object v1

    .line 2332193
    iget-object v2, p0, LX/GEx;->b:LX/0ad;

    sget-object v3, LX/0c0;->Live:LX/0c0;

    sget-object v4, LX/0c1;->Off:LX/0c1;

    sget-short v5, LX/GDK;->c:S

    invoke-interface {v2, v3, v4, v5, v0}, LX/0ad;->a(LX/0c0;LX/0c1;SZ)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v1}, LX/GEw;->a(LX/GGB;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2332194
    :cond_2
    sget-object v2, LX/GGB;->ACTIVE:LX/GGB;

    if-eq v1, v2, :cond_3

    sget-object v2, LX/GGB;->NEVER_BOOSTED:LX/GGB;

    if-eq v1, v2, :cond_3

    sget-object v2, LX/GGB;->PAUSED:LX/GGB;

    if-eq v1, v2, :cond_3

    sget-object v2, LX/GGB;->EXTENDABLE:LX/GGB;

    if-eq v1, v2, :cond_3

    sget-object v2, LX/GGB;->FINISHED:LX/GGB;

    if-eq v1, v2, :cond_3

    sget-object v2, LX/GGB;->PENDING:LX/GGB;

    if-ne v1, v2, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()LX/GHg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesViewController",
            "<",
            "Lcom/facebook/adinterfaces/ui/AdInterfacesPromotionDetailsView;",
            "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2332188
    iget-object v0, p0, LX/GEx;->a:LX/GMH;

    return-object v0
.end method

.method public final c()LX/8wK;
    .locals 1

    .prologue
    .line 2332187
    sget-object v0, LX/8wK;->PROMOTION_DETAILS:LX/8wK;

    return-object v0
.end method
