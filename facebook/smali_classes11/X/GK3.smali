.class public LX/GK3;
.super LX/GHg;
.source ""


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "LX/GHg",
        "<",
        "Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;",
        "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:LX/GG6;

.field private b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

.field private c:Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;


# direct methods
.method public constructor <init>(LX/GG6;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2340258
    invoke-direct {p0}, LX/GHg;-><init>()V

    .line 2340259
    iput-object p1, p0, LX/GK3;->a:LX/GG6;

    .line 2340260
    return-void
.end method

.method private a(Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 2

    .prologue
    .line 2340249
    invoke-super {p0, p1, p2}, LX/GHg;->a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    .line 2340250
    iget-object v0, p0, LX/GK3;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, LX/GK3;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->z()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    move-result-object v0

    if-nez v0, :cond_1

    .line 2340251
    :cond_0
    :goto_0
    return-void

    .line 2340252
    :cond_1
    iput-object p1, p0, LX/GK3;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;

    .line 2340253
    iget-object v0, p0, LX/GK3;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->z()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;->a()I

    move-result v0

    iget-object v1, p0, LX/GK3;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->z()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;->a()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/GK3;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->z()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;->a()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/GK3;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->A()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->j()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, LX/GK3;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->A()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->iu_()I

    move-result v1

    add-int/2addr v0, v1

    if-nez v0, :cond_2

    .line 2340254
    invoke-virtual {p1}, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->a()V

    goto :goto_0

    .line 2340255
    :cond_2
    invoke-direct {p0}, LX/GK3;->c()V

    .line 2340256
    iget-object v0, p0, LX/GK3;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f080ab1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setCallToActionText(Ljava/lang/String;)V

    .line 2340257
    new-instance v0, LX/GK2;

    invoke-direct {v0, p0}, LX/GK2;-><init>(LX/GK3;)V

    invoke-virtual {p2, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;->setCallToActionClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public static b(LX/0QB;)LX/GK3;
    .locals 2

    .prologue
    .line 2340247
    new-instance v1, LX/GK3;

    invoke-static {p0}, LX/GG6;->a(LX/0QB;)LX/GG6;

    move-result-object v0

    check-cast v0, LX/GG6;

    invoke-direct {v1, v0}, LX/GK3;-><init>(LX/GG6;)V

    .line 2340248
    return-object v1
.end method

.method public static b(LX/GK3;)V
    .locals 4

    .prologue
    .line 2340226
    iget-object v0, p0, LX/GK3;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, LX/8wL;->BOOST_POST_INSIGHTS:LX/8wL;

    const v2, 0x7f080aa5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, LX/GK3;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v3}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->n()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, LX/8wJ;->a(Landroid/content/Context;LX/8wL;Ljava/lang/Integer;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 2340227
    const-string v1, "data"

    iget-object v2, p0, LX/GK3;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2340228
    iget-object v1, p0, LX/GHg;->b:LX/GCE;

    move-object v1, v1

    .line 2340229
    new-instance v2, LX/GFS;

    invoke-direct {v2, v0}, LX/GFS;-><init>(Landroid/content/Intent;)V

    invoke-virtual {v1, v2}, LX/GCE;->a(LX/8wN;)V

    .line 2340230
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    .line 2340237
    iget-object v0, p0, LX/GK3;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v0}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->A()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;

    move-result-object v0

    .line 2340238
    iget-object v1, p0, LX/GK3;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    invoke-interface {v1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->z()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;

    move-result-object v1

    .line 2340239
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 2340240
    :cond_0
    :goto_0
    return-void

    .line 2340241
    :cond_1
    iget-object v2, p0, LX/GK3;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->j()I

    move-result v3

    iget-object v4, p0, LX/GK3;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v3, v4}, LX/GG6;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->setFirstDataValue(Ljava/lang/String;)V

    .line 2340242
    iget-object v2, p0, LX/GK3;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->j()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ReactorsModel;->a()I

    move-result v3

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->k()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;

    move-result-object v4

    invoke-virtual {v4}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$ResharesModel;->a()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel;->l()Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryFeedbackModel$TopLevelCommentsModel;->a()I

    move-result v1

    add-int/2addr v1, v3

    iget-object v3, p0, LX/GK3;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;

    invoke-virtual {v3}, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v1, v3}, LX/GG6;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->setSecondDataValue(Ljava/lang/String;)V

    .line 2340243
    iget-object v1, p0, LX/GK3;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;

    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$StoryInsightsModel;->iu_()I

    move-result v0

    iget-object v2, p0, LX/GK3;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;

    invoke-virtual {v2}, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v0, v2}, LX/GG6;->a(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->setThirdDataValue(Ljava/lang/String;)V

    .line 2340244
    iget-object v0, p0, LX/GK3;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;

    iget-object v1, p0, LX/GK3;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080aa8

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->setFirstDataLabel(Ljava/lang/String;)V

    .line 2340245
    iget-object v0, p0, LX/GK3;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;

    iget-object v1, p0, LX/GK3;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080aa7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->setSecondDataLabel(Ljava/lang/String;)V

    .line 2340246
    iget-object v0, p0, LX/GK3;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;

    iget-object v1, p0, LX/GK3;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;

    invoke-virtual {v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f080aa9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;->setThirdDataLabel(Ljava/lang/String;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 2340234
    invoke-super {p0}, LX/GHg;->a()V

    .line 2340235
    const/4 v0, 0x0

    iput-object v0, p0, LX/GK3;->c:Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;

    .line 2340236
    return-void
.end method

.method public final bridge synthetic a(Landroid/view/View;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V
    .locals 0

    .prologue
    .line 2340233
    check-cast p1, Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;

    invoke-direct {p0, p1, p2}, LX/GK3;->a(Lcom/facebook/adinterfaces/ui/AdInterfacesInsightsSummaryView;Lcom/facebook/adinterfaces/ui/AdInterfacesCardLayout;)V

    return-void
.end method

.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V
    .locals 0

    .prologue
    .line 2340231
    iput-object p1, p0, LX/GK3;->b:Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;

    .line 2340232
    return-void
.end method
