.class public LX/FyY;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile b:LX/FyY;


# instance fields
.field private final a:LX/Fwe;


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;LX/Fwf;)V
    .locals 3
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2306849
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2306850
    const v0, 0x7f0a00d2

    const v1, 0x7f0b0df7

    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2306851
    new-instance p1, LX/Fwe;

    invoke-static {p2}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v2

    check-cast v2, Landroid/content/res/Resources;

    invoke-direct {p1, v0, v1, v2}, LX/Fwe;-><init>(IILandroid/content/res/Resources;)V

    .line 2306852
    move-object v0, p1

    .line 2306853
    iput-object v0, p0, LX/FyY;->a:LX/Fwe;

    .line 2306854
    return-void
.end method

.method public static a(LX/0QB;)LX/FyY;
    .locals 5

    .prologue
    .line 2306855
    sget-object v0, LX/FyY;->b:LX/FyY;

    if-nez v0, :cond_1

    .line 2306856
    const-class v1, LX/FyY;

    monitor-enter v1

    .line 2306857
    :try_start_0
    sget-object v0, LX/FyY;->b:LX/FyY;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2306858
    if-eqz v2, :cond_0

    .line 2306859
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2306860
    new-instance p0, LX/FyY;

    invoke-static {v0}, LX/0kz;->a(LX/0QB;)Landroid/content/res/Resources;

    move-result-object v3

    check-cast v3, Landroid/content/res/Resources;

    const-class v4, LX/Fwf;

    invoke-interface {v0, v4}, LX/0QB;->getOnDemandAssistedProviderForStaticDi(Ljava/lang/Class;)LX/0Wl;

    move-result-object v4

    check-cast v4, LX/Fwf;

    invoke-direct {p0, v3, v4}, LX/FyY;-><init>(Landroid/content/res/Resources;LX/Fwf;)V

    .line 2306861
    move-object v0, p0

    .line 2306862
    sput-object v0, LX/FyY;->b:LX/FyY;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2306863
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2306864
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2306865
    :cond_1
    sget-object v0, LX/FyY;->b:LX/FyY;

    return-object v0

    .line 2306866
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2306867
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/view/View;LX/Fwd;)V
    .locals 5

    .prologue
    .line 2306868
    iget-object v0, p0, LX/FyY;->a:LX/Fwe;

    const/4 v2, 0x0

    .line 2306869
    iget-boolean v1, p2, LX/Fwd;->a:Z

    if-eqz v1, :cond_1

    iget v1, v0, LX/Fwe;->b:I

    .line 2306870
    :goto_0
    iget-boolean v3, p2, LX/Fwd;->b:Z

    if-eqz v3, :cond_2

    iget v3, v0, LX/Fwe;->b:I

    .line 2306871
    :goto_1
    iget-boolean v4, p2, LX/Fwd;->c:Z

    if-eqz v4, :cond_3

    iget v4, v0, LX/Fwe;->b:I

    .line 2306872
    :goto_2
    iget-boolean p0, p2, LX/Fwd;->d:Z

    if-eqz p0, :cond_0

    iget v2, v0, LX/Fwe;->b:I

    .line 2306873
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getPaddingLeft()I

    move-result p0

    add-int/2addr v1, p0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingTop()I

    move-result p0

    add-int/2addr v4, p0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingRight()I

    move-result p0

    add-int/2addr v3, p0

    invoke-virtual {p1}, Landroid/view/View;->getPaddingBottom()I

    move-result p0

    add-int/2addr v2, p0

    invoke-virtual {p1, v1, v4, v3, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 2306874
    return-void

    :cond_1
    move v1, v2

    .line 2306875
    goto :goto_0

    :cond_2
    move v3, v2

    .line 2306876
    goto :goto_1

    :cond_3
    move v4, v2

    .line 2306877
    goto :goto_2
.end method

.method public final a(Landroid/view/View;Landroid/graphics/Canvas;LX/Fwd;)V
    .locals 11

    .prologue
    .line 2306878
    iget-object v0, p0, LX/FyY;->a:LX/Fwe;

    const/4 v2, 0x0

    .line 2306879
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    add-int/lit8 v9, v1, -0x1

    .line 2306880
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v1

    add-int/lit8 v10, v1, -0x1

    .line 2306881
    iget-boolean v1, p3, LX/Fwd;->a:Z

    if-eqz v1, :cond_0

    .line 2306882
    int-to-float v5, v10

    iget-object v6, v0, LX/Fwe;->a:Landroid/graphics/Paint;

    move-object v1, p2

    move v3, v2

    move v4, v2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2306883
    :cond_0
    iget-boolean v1, p3, LX/Fwd;->b:Z

    if-eqz v1, :cond_1

    .line 2306884
    int-to-float v4, v9

    int-to-float v6, v9

    int-to-float v7, v10

    iget-object v8, v0, LX/Fwe;->a:Landroid/graphics/Paint;

    move-object v3, p2

    move v5, v2

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2306885
    :cond_1
    iget-boolean v1, p3, LX/Fwd;->c:Z

    if-eqz v1, :cond_2

    .line 2306886
    int-to-float v4, v9

    iget-object v6, v0, LX/Fwe;->a:Landroid/graphics/Paint;

    move-object v1, p2

    move v3, v2

    move v5, v2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2306887
    :cond_2
    iget-boolean v1, p3, LX/Fwd;->d:Z

    if-eqz v1, :cond_3

    .line 2306888
    int-to-float v3, v10

    int-to-float v4, v9

    int-to-float v5, v10

    iget-object v6, v0, LX/Fwe;->a:Landroid/graphics/Paint;

    move-object v1, p2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 2306889
    :cond_3
    return-void
.end method
