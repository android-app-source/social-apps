.class public final LX/F6W;
.super LX/6oO;
.source ""


# instance fields
.field public final synthetic a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;


# direct methods
.method public constructor <init>(Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;)V
    .locals 0

    .prologue
    .line 2200442
    iput-object p1, p0, LX/F6W;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    invoke-direct {p0}, LX/6oO;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 2200443
    iget-object v0, p0, LX/F6W;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    iget-object v0, v0, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->w:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2200444
    iget-object v1, p0, LX/F6W;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    iget-object v1, v1, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->y:Landroid/widget/Button;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 2200445
    iget-object v1, p0, LX/F6W;->a:Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;

    iget-object v1, v1, Lcom/facebook/growth/addcontactpoint/AddContactpointActivity;->y:Landroid/widget/Button;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setAlpha(F)V

    .line 2200446
    return-void

    .line 2200447
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2200448
    :cond_1
    const/high16 v0, 0x3f000000    # 0.5f

    goto :goto_1
.end method
