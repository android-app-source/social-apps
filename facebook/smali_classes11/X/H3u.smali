.class public LX/H3u;
.super Landroid/widget/BaseAdapter;
.source ""


# instance fields
.field public a:Landroid/content/Context;

.field private b:Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;

.field public c:LX/FOz;

.field public d:LX/H1i;

.field public e:Ljava/lang/String;

.field public f:LX/H3o;

.field public g:Landroid/view/View$OnClickListener;

.field public h:LX/H3w;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;LX/FOz;LX/H1i;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 2422081
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 2422082
    invoke-static {p1}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2422083
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2422084
    invoke-static {p3}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2422085
    iput-object p1, p0, LX/H3u;->a:Landroid/content/Context;

    .line 2422086
    iput-object p2, p0, LX/H3u;->b:Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;

    .line 2422087
    iput-object p3, p0, LX/H3u;->c:LX/FOz;

    .line 2422088
    iput-object p4, p0, LX/H3u;->d:LX/H1i;

    .line 2422089
    iput-object p5, p0, LX/H3u;->e:Ljava/lang/String;

    .line 2422090
    return-void
.end method

.method private a(I)LX/H3t;
    .locals 2

    .prologue
    .line 2421988
    invoke-static {}, LX/H3t;->values()[LX/H3t;

    move-result-object v0

    .line 2421989
    const/4 v1, 0x3

    if-ge p1, v1, :cond_0

    .line 2421990
    aget-object v0, v0, p1

    .line 2421991
    :goto_0
    return-object v0

    .line 2421992
    :cond_0
    invoke-direct {p0}, LX/H3u;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, LX/H3t;->CELL_TURN_ON_LOCATION_SERVICES:LX/H3t;

    invoke-virtual {v0}, LX/H3t;->ordinal()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 2421993
    sget-object v0, LX/H3t;->CELL_TURN_ON_LOCATION_SERVICES:LX/H3t;

    goto :goto_0

    .line 2421994
    :cond_1
    iget-object v0, p0, LX/H3u;->b:Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2421995
    sget-object v0, LX/H3t;->LOADING_CELL:LX/H3t;

    goto :goto_0

    .line 2421996
    :cond_2
    sget-object v0, LX/H3t;->PLACES_HUGE_CELL:LX/H3t;

    goto :goto_0
.end method

.method public static a(LX/H3u;Landroid/view/View;I)Landroid/view/View;
    .locals 1

    .prologue
    .line 2422073
    if-nez p1, :cond_0

    .line 2422074
    new-instance p1, Lcom/facebook/fbui/widget/header/SectionHeaderView;

    iget-object v0, p0, LX/H3u;->a:Landroid/content/Context;

    invoke-direct {p1, v0}, Lcom/facebook/fbui/widget/header/SectionHeaderView;-><init>(Landroid/content/Context;)V

    .line 2422075
    const v0, 0x7f0a0121

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/header/SectionHeaderView;->setBackgroundResource(I)V

    .line 2422076
    const v0, 0x7f0a06b5

    invoke-virtual {p1, v0}, Lcom/facebook/fbui/widget/header/SectionHeaderView;->setTopDivider(I)V

    .line 2422077
    invoke-virtual {p1, p2}, Lcom/facebook/fbui/widget/header/SectionHeaderView;->setTitleText(I)V

    .line 2422078
    :goto_0
    return-object p1

    .line 2422079
    :cond_0
    instance-of v0, p1, Lcom/facebook/fbui/widget/header/SectionHeaderView;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2422080
    check-cast p1, Lcom/facebook/fbui/widget/header/SectionHeaderView;

    goto :goto_0
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 2422072
    iget-object v0, p0, LX/H3u;->c:LX/FOz;

    invoke-interface {v0}, LX/FOz;->c()Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final getCount()I
    .locals 2

    .prologue
    const/4 v0, 0x4

    .line 2422068
    invoke-direct {p0}, LX/H3u;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2422069
    :cond_0
    :goto_0
    return v0

    .line 2422070
    :cond_1
    iget-object v1, p0, LX/H3u;->b:Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;

    iget-object v1, v1, Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;->a:LX/0Px;

    invoke-virtual {v1}, LX/0Px;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2422071
    iget-object v0, p0, LX/H3u;->b:Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;->a:LX/0Px;

    invoke-virtual {v0}, LX/0Px;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x3

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 2422059
    invoke-direct {p0, p1}, LX/H3u;->a(I)LX/H3t;

    move-result-object v0

    .line 2422060
    sget-object v1, LX/H3t;->PLACES_HUGE_CELL:LX/H3t;

    if-ne v0, v1, :cond_1

    .line 2422061
    invoke-direct {p0}, LX/H3u;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2422062
    iget-object v0, p0, LX/H3u;->b:Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;

    iget-object v0, v0, Lcom/facebook/nearby/v2/intent/model/FBNearbyPlacesIntentModel;->a:LX/0Px;

    add-int/lit8 v1, p1, -0x3

    invoke-virtual {v0, v1}, LX/0Px;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 2422063
    :goto_1
    return-object v0

    .line 2422064
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2422065
    :cond_1
    if-nez v0, :cond_2

    .line 2422066
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0

    .line 2422067
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 2422055
    invoke-direct {p0, p1}, LX/H3u;->a(I)LX/H3t;

    move-result-object v0

    .line 2422056
    if-eqz v0, :cond_0

    .line 2422057
    invoke-virtual {v0}, LX/H3t;->getCellId()I

    move-result v0

    int-to-long v0, v0

    return-wide v0

    .line 2422058
    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 2422051
    invoke-direct {p0, p1}, LX/H3u;->a(I)LX/H3t;

    move-result-object v0

    .line 2422052
    if-eqz v0, :cond_0

    .line 2422053
    invoke-virtual {v0}, LX/H3t;->getCellViewType()I

    move-result v0

    return v0

    .line 2422054
    :cond_0
    new-instance v0, Ljava/lang/ArrayIndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;-><init>()V

    throw v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 2421998
    invoke-direct {p0, p1}, LX/H3u;->a(I)LX/H3t;

    move-result-object v0

    .line 2421999
    if-nez v0, :cond_0

    .line 2422000
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2422001
    :cond_0
    sget-object v1, LX/H3s;->a:[I

    invoke-virtual {v0}, LX/H3t;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    .line 2422002
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 2422003
    :pswitch_0
    const v0, 0x7f0820da

    invoke-static {p0, p2, v0}, LX/H3u;->a(LX/H3u;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    move-object v0, v0

    .line 2422004
    :goto_0
    return-object v0

    .line 2422005
    :pswitch_1
    if-nez p2, :cond_2

    .line 2422006
    new-instance p2, LX/H41;

    iget-object v0, p0, LX/H3u;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/H41;-><init>(Landroid/content/Context;)V

    .line 2422007
    new-instance v0, LX/H3p;

    invoke-direct {v0, p0}, LX/H3p;-><init>(LX/H3u;)V

    .line 2422008
    iput-object v0, p2, LX/H41;->c:LX/H3o;

    .line 2422009
    :goto_1
    move-object v0, p2

    .line 2422010
    goto :goto_0

    .line 2422011
    :pswitch_2
    const v0, 0x7f0820db

    invoke-static {p0, p2, v0}, LX/H3u;->a(LX/H3u;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    move-object v0, v0

    .line 2422012
    goto :goto_0

    .line 2422013
    :pswitch_3
    if-nez p2, :cond_3

    .line 2422014
    new-instance p2, LX/H4A;

    iget-object v0, p0, LX/H3u;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/H4A;-><init>(Landroid/content/Context;)V

    .line 2422015
    new-instance v0, LX/H3q;

    invoke-direct {v0, p0}, LX/H3q;-><init>(LX/H3u;)V

    invoke-virtual {p2, v0}, LX/H4A;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2422016
    :goto_2
    sget-object v0, LX/H3s;->b:[I

    iget-object v1, p0, LX/H3u;->c:LX/FOz;

    invoke-interface {v1}, LX/FOz;->c()Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-result-object v1

    .line 2422017
    iget-object p1, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->a:LX/FOw;

    move-object v1, p1

    .line 2422018
    invoke-virtual {v1}, LX/FOw;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 2422019
    :goto_3
    move-object v0, p2

    .line 2422020
    goto :goto_0

    .line 2422021
    :pswitch_4
    if-nez p2, :cond_4

    .line 2422022
    new-instance p2, LX/FQK;

    iget-object v0, p0, LX/H3u;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/FQK;-><init>(Landroid/content/Context;)V

    .line 2422023
    :goto_4
    invoke-virtual {p0, p1}, LX/H3u;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;

    .line 2422024
    iget-object v1, p0, LX/H3u;->c:LX/FOz;

    invoke-interface {v1}, LX/FOz;->c()Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;

    move-result-object v1

    .line 2422025
    iget-object v2, v1, Lcom/facebook/nearby/v2/model/NearbyPlacesSearchDataModel;->c:Landroid/location/Location;

    move-object v1, v2

    .line 2422026
    const/4 v2, 0x1

    invoke-virtual {p2, v0, v1, v2}, LX/FQK;->a(Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;Landroid/location/Location;Z)V

    .line 2422027
    new-instance v1, LX/H3r;

    invoke-direct {v1, p0, p1, v0}, LX/H3r;-><init>(LX/H3u;ILcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;)V

    .line 2422028
    iput-object v1, p2, LX/FQK;->i:LX/FPE;

    .line 2422029
    iget-object v1, p0, LX/H3u;->d:LX/H1i;

    iget-object v2, p0, LX/H3u;->e:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/facebook/nearby/v2/model/NearbyPlacesPlaceModel;->b()Ljava/lang/String;

    move-result-object v0

    .line 2422030
    iget-object v3, v1, LX/H1i;->a:LX/0Zb;

    const-string p3, "nearby_places_here_card_impression"

    const/4 p0, 0x1

    invoke-interface {v3, p3, p0}, LX/0Zb;->a(Ljava/lang/String;Z)LX/0oG;

    move-result-object v3

    .line 2422031
    invoke-virtual {v3}, LX/0oG;->a()Z

    move-result p3

    if-eqz p3, :cond_1

    .line 2422032
    const-string p3, "places_recommendations"

    invoke-virtual {v3, p3}, LX/0oG;->a(Ljava/lang/String;)LX/0oG;

    move-result-object p3

    const-string p0, "target_id"

    invoke-virtual {p3, p0, v0}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p3

    const-string p0, "mechanism"

    const-string p1, "here_card"

    invoke-virtual {p3, p0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p3

    const-string p0, "event_target"

    const-string p1, "place"

    invoke-virtual {p3, p0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p3

    const-string p0, "event_type"

    const-string p1, "impression"

    invoke-virtual {p3, p0, p1}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    move-result-object p3

    const-string p0, "session_id"

    invoke-virtual {p3, p0, v2}, LX/0oG;->a(Ljava/lang/String;Ljava/lang/String;)LX/0oG;

    .line 2422033
    invoke-virtual {v3}, LX/0oG;->d()V

    .line 2422034
    :cond_1
    move-object v0, p2

    .line 2422035
    goto/16 :goto_0

    .line 2422036
    :pswitch_5
    if-nez p2, :cond_5

    .line 2422037
    new-instance p2, LX/FPs;

    iget-object v0, p0, LX/H3u;->a:Landroid/content/Context;

    invoke-direct {p2, v0}, LX/FPs;-><init>(Landroid/content/Context;)V

    .line 2422038
    :goto_5
    move-object v0, p2

    .line 2422039
    goto/16 :goto_0

    .line 2422040
    :cond_2
    instance-of v0, p2, LX/H41;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2422041
    check-cast p2, LX/H41;

    goto/16 :goto_1

    .line 2422042
    :cond_3
    instance-of v0, p2, LX/H4A;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2422043
    check-cast p2, LX/H4A;

    goto/16 :goto_2

    .line 2422044
    :pswitch_6
    iget-object v0, p0, LX/H3u;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0820d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/H4A;->setTitle(Ljava/lang/CharSequence;)V

    .line 2422045
    iget-object v0, p0, LX/H3u;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0820d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/H4A;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 2422046
    :pswitch_7
    iget-object v0, p0, LX/H3u;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0820d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/H4A;->setTitle(Ljava/lang/CharSequence;)V

    .line 2422047
    iget-object v0, p0, LX/H3u;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0820d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, LX/H4A;->setPrimaryButtonText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 2422048
    :cond_4
    instance-of v0, p2, LX/FQK;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    .line 2422049
    check-cast p2, LX/FQK;

    goto/16 :goto_4

    .line 2422050
    :cond_5
    instance-of v0, p2, LX/FPs;

    invoke-static {v0}, LX/0PB;->checkArgument(Z)V

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 2421997
    invoke-static {}, LX/H3t;->values()[LX/H3t;

    move-result-object v0

    array-length v0, v0

    return v0
.end method
