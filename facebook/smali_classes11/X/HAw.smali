.class public final LX/HAw;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;


# direct methods
.method public constructor <init>(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;)V
    .locals 0

    .prologue
    .line 2436277
    iput-object p1, p0, LX/HAw;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v5, 0x1

    const v0, -0xccf6409

    invoke-static {v7, v5, v0}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v1

    .line 2436278
    iget-object v0, p0, LX/HAw;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->h:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, LX/9XE;

    iget-object v2, p0, LX/HAw;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    iget-object v2, v2, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->k:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 2436279
    sget-object v4, LX/9X6;->EVENT_ADMIN_CONTACT_INBOX_TAPPED_SEND_EMAIL:LX/9X6;

    invoke-virtual {v0, v4, v2, v3}, LX/9XE;->a(LX/9X2;J)V

    .line 2436280
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEND"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 2436281
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 2436282
    const-string v2, "vnd.android.cursor.item/email"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 2436283
    const-string v2, "android.intent.extra.EMAIL"

    new-array v3, v5, [Ljava/lang/String;

    iget-object v4, p0, LX/HAw;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    iget-object v4, v4, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->j:Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;

    invoke-virtual {v4}, Lcom/facebook/pages/common/contactinbox/graphql/PagesContactInboxGraphQLModels$PageContactUsLeadFieldsModel;->l()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 2436284
    const-string v2, "android.intent.extra.SUBJECT"

    iget-object v3, p0, LX/HAw;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f083698

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, LX/HAw;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    iget-object v6, v6, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->l:Ljava/lang/String;

    aput-object v6, v5, v8

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2436285
    const-string v2, "android.intent.extra.TEXT"

    iget-object v3, p0, LX/HAw;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    invoke-static {v3}, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->b(Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 2436286
    iget-object v2, p0, LX/HAw;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f08369c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v2

    .line 2436287
    iget-object v0, p0, LX/HAw;->a:Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;

    iget-object v0, v0, Lcom/facebook/pages/common/contactinbox/fragments/PagesContactInboxRequestDetailFragment;->a:LX/0Ot;

    invoke-interface {v0}, LX/0Ot;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/facebook/content/SecureContextHelper;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/facebook/content/SecureContextHelper;->b(Landroid/content/Intent;Landroid/content/Context;)V

    .line 2436288
    const v0, -0x3dd7e1c9

    invoke-static {v7, v7, v0, v1}, Lcom/facebook/loom/logger/Logger;->a(IIII)I

    return-void
.end method
