.class public LX/GDy;
.super Ljava/lang/Object;
.source ""


# instance fields
.field private final a:LX/0tX;

.field private final b:LX/2U4;

.field public final c:LX/2U3;


# direct methods
.method public constructor <init>(LX/0tX;LX/2U4;LX/2U3;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2330659
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2330660
    iput-object p1, p0, LX/GDy;->a:LX/0tX;

    .line 2330661
    iput-object p2, p0, LX/GDy;->b:LX/2U4;

    .line 2330662
    iput-object p3, p0, LX/GDy;->c:LX/2U3;

    .line 2330663
    return-void
.end method

.method private static a(Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel;LX/8wL;)LX/1vs;
    .locals 7
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "getPromotionAvailableAudienceFragment"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .prologue
    const/4 v1, 0x0

    const v6, -0xf25bc95

    const/4 v5, 0x0

    .line 2330664
    invoke-virtual {p0}, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel;->j()LX/1vs;

    move-result-object v0

    iget-object v2, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    sget-object v3, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2330665
    if-nez v0, :cond_0

    .line 2330666
    invoke-static {v1, v5}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    .line 2330667
    :goto_0
    return-object v0

    .line 2330668
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2330669
    :cond_0
    sget-object v3, LX/GDx;->a:[I

    invoke-virtual {p1}, LX/8wL;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    move-object v0, v1

    .line 2330670
    :goto_1
    if-eqz v0, :cond_4

    invoke-virtual {v0}, LX/39O;->c()I

    move-result v2

    if-lez v2, :cond_4

    .line 2330671
    invoke-virtual {v0, v5}, LX/2uF;->a(I)LX/1vs;

    move-result-object v0

    iget-object v1, v0, LX/1vs;->a:LX/15i;

    iget v0, v0, LX/1vs;->b:I

    invoke-static {v1, v0}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    goto :goto_0

    .line 2330672
    :pswitch_0
    const/4 v3, 0x1

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v0

    invoke-static {v2, v0, v5, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_1

    .line 2330673
    :pswitch_1
    invoke-virtual {v2, v0, v5}, LX/15i;->g(II)I

    move-result v0

    invoke-static {v2, v0, v5, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    goto :goto_1

    :cond_2
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_1

    .line 2330674
    :pswitch_2
    const/4 v3, 0x2

    invoke-virtual {v2, v0, v3}, LX/15i;->g(II)I

    move-result v0

    invoke-static {v2, v0, v5, v6}, LX/4A9;->a(LX/15i;III)LX/4A9;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, LX/2uF;->b(LX/39P;)LX/2uF;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-static {}, LX/2uF;->h()LX/2uF;

    move-result-object v0

    goto :goto_1

    .line 2330675
    :cond_4
    invoke-static {v1, v5}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lcom/facebook/graphql/executor/GraphQLResult;LX/8wL;)LX/1vs;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/graphql/executor/GraphQLResult",
            "<",
            "Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel;",
            ">;",
            "LX/8wL;",
            ")",
            "LX/1vs;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 2330676
    iget-object v0, p0, Lcom/facebook/graphql/executor/GraphQLResult;->d:Ljava/lang/Object;

    move-object v0, v0

    .line 2330677
    check-cast v0, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel;

    .line 2330678
    if-nez v0, :cond_0

    .line 2330679
    invoke-static {v6, v2}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    .line 2330680
    :goto_0
    return-object v0

    .line 2330681
    :cond_0
    invoke-virtual {v0}, Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel;->j()LX/1vs;

    move-result-object v3

    iget v3, v3, LX/1vs;->b:I

    .line 2330682
    sget-object v4, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2330683
    if-nez v3, :cond_1

    .line 2330684
    invoke-static {v6, v2}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    goto :goto_0

    .line 2330685
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 2330686
    :cond_1
    invoke-static {v0, p1}, LX/GDy;->a(Lcom/facebook/adinterfaces/protocol/FetchAvailableAudiencesQueryModels$FetchAvailableAudiencesQueryModel;LX/8wL;)LX/1vs;

    move-result-object v0

    iget-object v3, v0, LX/1vs;->a:LX/15i;

    iget v4, v0, LX/1vs;->b:I

    .line 2330687
    sget-object v5, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v5

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2330688
    if-eqz v4, :cond_3

    invoke-virtual {v3, v4, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_5

    invoke-virtual {v3, v4, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    if-eqz v0, :cond_4

    :goto_2
    if-eqz v1, :cond_6

    .line 2330689
    invoke-virtual {v3, v4, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-virtual {v3, v0, v2}, LX/15i;->g(II)I

    move-result v0

    invoke-static {v3, v0}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    goto :goto_0

    .line 2330690
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    :cond_2
    move v0, v2

    .line 2330691
    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_2

    .line 2330692
    :cond_6
    invoke-static {v6, v2}, LX/1vs;->a(LX/15i;I)LX/1vs;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;Ljava/lang/String;Ljava/lang/String;ILX/8wL;)Lcom/google/common/util/concurrent/ListenableFuture;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "LX/8wL;",
            ")",
            "Lcom/google/common/util/concurrent/ListenableFuture",
            "<",
            "LX/0Px",
            "<",
            "Lcom/facebook/adinterfaces/protocol/AdInterfacesQueryFragmentsModels$BoostedComponentAudienceModel;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 2330693
    invoke-static {p2}, LX/0PB;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2330694
    sget-object v0, LX/8wL;->BOOST_EVENT:LX/8wL;

    if-eq p5, v0, :cond_0

    sget-object v0, LX/8wL;->BOOST_POST:LX/8wL;

    if-ne p5, v0, :cond_1

    .line 2330695
    :cond_0
    iget-object v0, p0, LX/GDy;->b:LX/2U4;

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->d()Ljava/lang/String;

    move-result-object v1

    .line 2330696
    new-instance v2, LX/ABj;

    invoke-direct {v2}, LX/ABj;-><init>()V

    move-object v2, v2

    .line 2330697
    const-string v3, "story_id"

    invoke-virtual {v2, v3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "ad_account_id"

    invoke-virtual {v2, v3, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "after_audience_id"

    invoke-virtual {v2, v3, p3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v2

    const-string v3, "audience_count"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object v2

    const-string v3, "remove_audience_targeting_sentences"

    iget-object v4, v0, LX/2U4;->b:LX/0ad;

    sget-short v5, LX/GDK;->y:S

    const/4 v6, 0x0

    invoke-interface {v4, v5, v6}, LX/0ad;->a(SZ)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v2

    move-object v2, v2

    .line 2330698
    invoke-static {v2}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v2

    move-object v1, v2

    .line 2330699
    new-instance v0, LX/GDu;

    invoke-direct {v0, p0, p1, p5}, LX/GDu;-><init>(LX/GDy;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;LX/8wL;)V

    .line 2330700
    :goto_0
    iget-object v2, p0, LX/GDy;->a:LX/0tX;

    invoke-virtual {v2, v1}, LX/0tX;->a(LX/0zO;)LX/1Zp;

    move-result-object v1

    .line 2330701
    new-instance v2, LX/GDw;

    invoke-direct {v2, p0, v1, v0}, LX/GDw;-><init>(LX/GDy;Lcom/google/common/util/concurrent/ListenableFuture;LX/0QK;)V

    return-object v2

    .line 2330702
    :cond_1
    iget-object v0, p0, LX/GDy;->b:LX/2U4;

    invoke-interface {p1}, Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;->c()Ljava/lang/String;

    move-result-object v1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    .line 2330703
    const/4 v7, 0x1

    const/4 p2, 0x0

    .line 2330704
    new-instance v6, LX/ABQ;

    invoke-direct {v6}, LX/ABQ;-><init>()V

    move-object v6, v6

    .line 2330705
    const-string p3, "page_id"

    invoke-virtual {v6, p3, v1}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string p3, "ad_account_id"

    invoke-virtual {v6, p3, v2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string p3, "after_audience_id"

    invoke-virtual {v6, p3, v3}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/String;)LX/0gW;

    move-result-object v6

    const-string p3, "audience_count"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    invoke-virtual {v6, p3, p4}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Number;)LX/0gW;

    move-result-object p3

    const-string p4, "is_page_like"

    sget-object v6, LX/8wL;->PAGE_LIKE:LX/8wL;

    if-ne v5, v6, :cond_2

    move v6, v7

    :goto_1
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {p3, p4, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object p3

    const-string p4, "is_promote_website"

    sget-object v6, LX/8wL;->PROMOTE_WEBSITE:LX/8wL;

    if-ne v5, v6, :cond_3

    move v6, v7

    :goto_2
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {p3, p4, v6}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v6

    const-string p3, "is_cta"

    sget-object p4, LX/8wL;->PROMOTE_CTA:LX/8wL;

    if-ne v5, p4, :cond_4

    :goto_3
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v6, p3, v7}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v6

    const-string v7, "remove_audience_targeting_sentences"

    iget-object p3, v0, LX/2U4;->b:LX/0ad;

    sget-short p4, LX/GDK;->y:S

    invoke-interface {p3, p4, p2}, LX/0ad;->a(SZ)Z

    move-result p2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {v6, v7, p2}, LX/0gW;->a(Ljava/lang/String;Ljava/lang/Boolean;)LX/0gW;

    move-result-object v6

    move-object v6, v6

    .line 2330706
    invoke-static {v6}, LX/0zO;->a(LX/0gW;)LX/0zO;

    move-result-object v6

    move-object v1, v6

    .line 2330707
    new-instance v0, LX/GDv;

    invoke-direct {v0, p0, p5, p1}, LX/GDv;-><init>(LX/GDy;LX/8wL;Lcom/facebook/adinterfaces/model/AdInterfacesDataModel;)V

    goto/16 :goto_0

    :cond_2
    move v6, p2

    goto :goto_1

    :cond_3
    move v6, p2

    goto :goto_2

    :cond_4
    move v7, p2

    goto :goto_3
.end method
