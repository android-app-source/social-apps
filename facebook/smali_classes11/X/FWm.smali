.class public LX/FWm;
.super Lcom/facebook/fbui/widget/layout/ImageBlockLayout;
.source ""

# interfaces
.implements Landroid/widget/Checkable;


# static fields
.field private static final j:[I

.field private static final k:[I


# instance fields
.field public l:Landroid/widget/ImageView;

.field public m:Lcom/facebook/fbui/widget/text/BadgeTextView;

.field public n:Z

.field private o:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2252605
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, LX/FWm;->j:[I

    .line 2252606
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x101009f

    aput v2, v0, v1

    sput-object v0, LX/FWm;->k:[I

    return-void

    .line 2252607
    :array_0
    .array-data 4
        0x101009f
        0x10100a0
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 2252608
    invoke-direct {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;-><init>(Landroid/content/Context;)V

    .line 2252609
    iput-boolean v0, p0, LX/FWm;->n:Z

    .line 2252610
    iput-boolean v0, p0, LX/FWm;->o:Z

    .line 2252611
    invoke-virtual {p0}, LX/FWm;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 2252612
    const v1, 0x7f0b1164

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0, v1}, LX/FWm;->setMinimumHeight(I)V

    .line 2252613
    const v1, 0x7f0b01d8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 2252614
    const p1, 0x7f0b01d9

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 2252615
    invoke-virtual {p0, v1, v0, v1, v0}, LX/FWm;->setPadding(IIII)V

    .line 2252616
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->setGravity(I)V

    .line 2252617
    const v0, 0x7f030611

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->setContentView(I)V

    .line 2252618
    const v0, 0x7f0d10ec

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/facebook/fbui/widget/text/BadgeTextView;

    iput-object v0, p0, LX/FWm;->m:Lcom/facebook/fbui/widget/text/BadgeTextView;

    .line 2252619
    const v0, 0x7f0d10eb

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, LX/FWm;->l:Landroid/widget/ImageView;

    .line 2252620
    const v0, 0x7f0d10ed

    invoke-virtual {p0, v0}, Lcom/facebook/widget/CustomViewGroup;->getView(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2252621
    return-void
.end method


# virtual methods
.method public final isChecked()Z
    .locals 1

    .prologue
    .line 2252622
    iget-boolean v0, p0, LX/FWm;->o:Z

    return v0
.end method

.method public final onCreateDrawableState(I)[I
    .locals 2

    .prologue
    .line 2252623
    iget-boolean v0, p0, LX/FWm;->n:Z

    if-nez v0, :cond_0

    .line 2252624
    invoke-super {p0, p1}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onCreateDrawableState(I)[I

    move-result-object v0

    .line 2252625
    :goto_0
    return-object v0

    .line 2252626
    :cond_0
    add-int/lit8 v0, p1, 0x2

    invoke-super {p0, v0}, Lcom/facebook/fbui/widget/layout/ImageBlockLayout;->onCreateDrawableState(I)[I

    move-result-object v1

    .line 2252627
    iget-boolean v0, p0, LX/FWm;->o:Z

    if-eqz v0, :cond_1

    sget-object v0, LX/FWm;->j:[I

    :goto_1
    invoke-static {v1, v0}, Landroid/view/View;->mergeDrawableStates([I[I)[I

    move-object v0, v1

    .line 2252628
    goto :goto_0

    .line 2252629
    :cond_1
    sget-object v0, LX/FWm;->k:[I

    goto :goto_1
.end method

.method public setChecked(Z)V
    .locals 0

    .prologue
    .line 2252630
    iput-boolean p1, p0, LX/FWm;->o:Z

    .line 2252631
    invoke-virtual {p0}, LX/FWm;->refreshDrawableState()V

    .line 2252632
    return-void
.end method

.method public final toggle()V
    .locals 1

    .prologue
    .line 2252633
    iget-boolean v0, p0, LX/FWm;->o:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, LX/FWm;->o:Z

    .line 2252634
    invoke-virtual {p0}, LX/FWm;->refreshDrawableState()V

    .line 2252635
    return-void

    .line 2252636
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
