.class public final LX/EzH;
.super Ljava/lang/Object;
.source ""

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public final synthetic a:Lcom/facebook/friends/model/FriendRequest;

.field public final synthetic b:LX/3UI;


# direct methods
.method public constructor <init>(LX/3UI;Lcom/facebook/friends/model/FriendRequest;)V
    .locals 0

    .prologue
    .line 2186975
    iput-object p1, p0, LX/EzH;->b:LX/3UI;

    iput-object p2, p0, LX/EzH;->a:Lcom/facebook/friends/model/FriendRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 11

    .prologue
    const/4 v0, 0x2

    const/4 v1, 0x1

    const v2, 0x7d034a46

    invoke-static {v0, v1, v2}, Lcom/facebook/loom/logger/Logger;->a(III)I

    move-result v0

    .line 2186976
    iget-object v1, p0, LX/EzH;->a:Lcom/facebook/friends/model/FriendRequest;

    invoke-virtual {v1}, Lcom/facebook/friends/model/FriendRequest;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2186977
    iget-object v1, p0, LX/EzH;->b:LX/3UI;

    iget-object v2, p0, LX/EzH;->a:Lcom/facebook/friends/model/FriendRequest;

    sget-object v3, LX/2na;->CONFIRM:LX/2na;

    .line 2186978
    sget-object v4, LX/2na;->REJECT:LX/2na;

    invoke-virtual {v4, v3}, LX/2na;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2186979
    iget-object v4, v1, LX/3UI;->b:LX/2hs;

    invoke-virtual {v4}, LX/2hs;->b()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2186980
    :goto_0
    iget-object v4, v2, Lcom/facebook/friends/model/FriendRequest;->j:LX/2lu;

    move-object v4, v4

    .line 2186981
    invoke-static {v3}, LX/2nY;->a(LX/2na;)LX/2lu;

    move-result-object v5

    .line 2186982
    iput-object v5, v2, Lcom/facebook/friends/model/FriendRequest;->j:LX/2lu;

    .line 2186983
    iget-object v5, v1, LX/3UI;->g:LX/3UH;

    invoke-virtual {v2}, Lcom/facebook/friends/model/FriendRequest;->a()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, LX/3UH;->a(JZ)V

    .line 2186984
    iget-object v5, v1, LX/3UI;->c:LX/3UJ;

    invoke-virtual {v2}, Lcom/facebook/friends/model/FriendRequest;->a()J

    move-result-wide v6

    new-instance v8, LX/EzK;

    invoke-direct {v8, v1, v2, v4}, LX/EzK;-><init>(LX/3UI;Lcom/facebook/friends/model/FriendRequest;LX/2lu;)V

    invoke-virtual {v5, v6, v7, v3, v8}, LX/3UJ;->a(JLX/2na;LX/84H;)V

    .line 2186985
    :goto_1
    const v1, -0x9f3f656

    invoke-static {v1, v0}, LX/02F;->a(II)V

    return-void

    .line 2186986
    :cond_0
    iget-object v1, p0, LX/EzH;->b:LX/3UI;

    iget-object v2, p0, LX/EzH;->a:Lcom/facebook/friends/model/FriendRequest;

    sget-object v3, LX/2na;->CONFIRM:LX/2na;

    .line 2186987
    iget-object v4, v1, LX/3UI;->b:LX/2hs;

    invoke-virtual {v4}, LX/2hs;->d()Lcom/google/common/util/concurrent/ListenableFuture;

    .line 2186988
    iget-object v4, v2, Lcom/facebook/friends/model/FriendRequest;->j:LX/2lu;

    move-object v4, v4

    .line 2186989
    invoke-static {v3}, LX/2nY;->a(LX/2na;)LX/2lu;

    move-result-object v5

    .line 2186990
    iput-object v5, v2, Lcom/facebook/friends/model/FriendRequest;->j:LX/2lu;

    .line 2186991
    iget-object v5, v1, LX/3UI;->g:LX/3UH;

    invoke-virtual {v2}, Lcom/facebook/friends/model/FriendRequest;->a()J

    move-result-wide v6

    const/4 v8, 0x0

    invoke-virtual {v5, v6, v7, v8}, LX/3UH;->a(JZ)V

    .line 2186992
    iget-object v5, v1, LX/3UI;->c:LX/3UJ;

    invoke-virtual {v2}, Lcom/facebook/friends/model/FriendRequest;->a()J

    move-result-wide v6

    iget-object v8, v1, LX/3UI;->f:LX/2h7;

    iget-object v8, v8, LX/2h7;->friendRequestResponseRef:LX/2hA;

    new-instance v10, LX/EzJ;

    invoke-direct {v10, v1, v2, v4}, LX/EzJ;-><init>(LX/3UI;Lcom/facebook/friends/model/FriendRequest;LX/2lu;)V

    move-object v9, v3

    invoke-virtual/range {v5 .. v10}, LX/3UJ;->a(JLX/2hA;LX/2na;LX/84H;)V

    .line 2186993
    goto :goto_1

    .line 2186994
    :cond_1
    iget-object v4, v1, LX/3UI;->b:LX/2hs;

    invoke-virtual {v4}, LX/2hs;->d()Lcom/google/common/util/concurrent/ListenableFuture;

    goto :goto_0
.end method
