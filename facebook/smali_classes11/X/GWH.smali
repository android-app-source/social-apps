.class public final LX/GWH;
.super Ljava/lang/Object;
.source ""


# instance fields
.field public a:Lcom/facebook/graphql/enums/GraphQLObjectType;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public b:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public c:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "commerceInsights"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public d:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "commerceInsights"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public e:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "commerceMerchantSettings"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public f:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "commerceMerchantSettings"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public g:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public h:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "commerceUiDetailSections"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public i:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "commerceUiDetailSections"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public j:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public k:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public l:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public m:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public n:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public o:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public p:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "productPromotions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public q:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "productPromotions"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public r:LX/15i;
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "recommendedProductItems"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public s:I
    .annotation build Lcom/facebook/annotationprocessors/transformer/api/Clone;
        from = "recommendedProductItems"
        processor = "com.facebook.dracula.transformer.Transformer"
    .end annotation

    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field

.field public t:Ljava/lang/String;
    .annotation build Ljavax/annotation/Nullable;
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2360757
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2360758
    return-void
.end method


# virtual methods
.method public final a()Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;
    .locals 20

    .prologue
    .line 2360759
    new-instance v3, LX/186;

    const/16 v4, 0x80

    invoke-direct {v3, v4}, LX/186;-><init>(I)V

    .line 2360760
    move-object/from16 v0, p0

    iget-object v4, v0, LX/GWH;->a:Lcom/facebook/graphql/enums/GraphQLObjectType;

    invoke-static {v3, v4}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v4

    .line 2360761
    move-object/from16 v0, p0

    iget-object v5, v0, LX/GWH;->b:Lcom/facebook/graphql/enums/GraphQLCommerceCheckoutStyle;

    invoke-virtual {v3, v5}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v5

    .line 2360762
    sget-object v6, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v6

    :try_start_0
    move-object/from16 v0, p0

    iget-object v7, v0, LX/GWH;->c:LX/15i;

    move-object/from16 v0, p0

    iget v8, v0, LX/GWH;->d:I

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const v6, 0x6eda260f

    invoke-static {v7, v8, v6}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;

    move-result-object v6

    invoke-static {v3, v6}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v6

    .line 2360763
    sget-object v7, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v7

    :try_start_1
    move-object/from16 v0, p0

    iget-object v8, v0, LX/GWH;->e:LX/15i;

    move-object/from16 v0, p0

    iget v9, v0, LX/GWH;->f:I

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const v7, 0x471ba6fe

    invoke-static {v8, v9, v7}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;

    move-result-object v7

    invoke-static {v3, v7}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v7

    .line 2360764
    move-object/from16 v0, p0

    iget-object v8, v0, LX/GWH;->g:Lcom/facebook/graphql/enums/GraphQLCommerceProductVisibility;

    invoke-virtual {v3, v8}, LX/186;->a(Ljava/lang/Enum;)I

    move-result v8

    .line 2360765
    sget-object v9, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v9

    :try_start_2
    move-object/from16 v0, p0

    iget-object v10, v0, LX/GWH;->h:LX/15i;

    move-object/from16 v0, p0

    iget v11, v0, LX/GWH;->i:I

    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const v9, -0x4f141f07

    invoke-static {v10, v11, v9}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;

    move-result-object v9

    invoke-static {v3, v9}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v9

    .line 2360766
    move-object/from16 v0, p0

    iget-object v10, v0, LX/GWH;->j:Ljava/lang/String;

    invoke-virtual {v3, v10}, LX/186;->b(Ljava/lang/String;)I

    move-result v10

    .line 2360767
    move-object/from16 v0, p0

    iget-object v11, v0, LX/GWH;->k:Ljava/lang/String;

    invoke-virtual {v3, v11}, LX/186;->b(Ljava/lang/String;)I

    move-result v11

    .line 2360768
    move-object/from16 v0, p0

    iget-object v12, v0, LX/GWH;->l:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$GroupModel;

    invoke-static {v3, v12}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v12

    .line 2360769
    move-object/from16 v0, p0

    iget-object v13, v0, LX/GWH;->m:Ljava/lang/String;

    invoke-virtual {v3, v13}, LX/186;->b(Ljava/lang/String;)I

    move-result v13

    .line 2360770
    move-object/from16 v0, p0

    iget-object v14, v0, LX/GWH;->n:Ljava/lang/String;

    invoke-virtual {v3, v14}, LX/186;->b(Ljava/lang/String;)I

    move-result v14

    .line 2360771
    move-object/from16 v0, p0

    iget-object v15, v0, LX/GWH;->o:Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel$PageModel;

    invoke-static {v3, v15}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v15

    .line 2360772
    sget-object v16, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v16

    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, LX/GWH;->p:LX/15i;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/GWH;->q:I

    move/from16 v18, v0

    monitor-exit v16
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    const v16, 0x273f6519

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v16

    invoke-static {v0, v1, v2}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v16

    .line 2360773
    sget-object v17, LX/1vu;->a:Ljava/lang/Object;

    monitor-enter v17

    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, LX/GWH;->r:LX/15i;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget v0, v0, LX/GWH;->s:I

    move/from16 v19, v0

    monitor-exit v17
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    const v17, 0x252a5612

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v17

    invoke-static {v0, v1, v2}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;->a(LX/15i;II)Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$DraculaImplementation;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-static {v3, v0}, LX/1k0;->a(LX/186;Lcom/facebook/flatbuffers/MutableFlattenable;)I

    move-result v17

    .line 2360774
    move-object/from16 v0, p0

    iget-object v0, v0, LX/GWH;->t:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, LX/186;->b(Ljava/lang/String;)I

    move-result v18

    .line 2360775
    const/16 v19, 0xf

    move/from16 v0, v19

    invoke-virtual {v3, v0}, LX/186;->c(I)V

    .line 2360776
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v3, v0, v4}, LX/186;->b(II)V

    .line 2360777
    const/4 v4, 0x1

    invoke-virtual {v3, v4, v5}, LX/186;->b(II)V

    .line 2360778
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v6}, LX/186;->b(II)V

    .line 2360779
    const/4 v4, 0x3

    invoke-virtual {v3, v4, v7}, LX/186;->b(II)V

    .line 2360780
    const/4 v4, 0x4

    invoke-virtual {v3, v4, v8}, LX/186;->b(II)V

    .line 2360781
    const/4 v4, 0x5

    invoke-virtual {v3, v4, v9}, LX/186;->b(II)V

    .line 2360782
    const/4 v4, 0x6

    invoke-virtual {v3, v4, v10}, LX/186;->b(II)V

    .line 2360783
    const/4 v4, 0x7

    invoke-virtual {v3, v4, v11}, LX/186;->b(II)V

    .line 2360784
    const/16 v4, 0x8

    invoke-virtual {v3, v4, v12}, LX/186;->b(II)V

    .line 2360785
    const/16 v4, 0x9

    invoke-virtual {v3, v4, v13}, LX/186;->b(II)V

    .line 2360786
    const/16 v4, 0xa

    invoke-virtual {v3, v4, v14}, LX/186;->b(II)V

    .line 2360787
    const/16 v4, 0xb

    invoke-virtual {v3, v4, v15}, LX/186;->b(II)V

    .line 2360788
    const/16 v4, 0xc

    move/from16 v0, v16

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 2360789
    const/16 v4, 0xd

    move/from16 v0, v17

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 2360790
    const/16 v4, 0xe

    move/from16 v0, v18

    invoke-virtual {v3, v4, v0}, LX/186;->b(II)V

    .line 2360791
    invoke-virtual {v3}, LX/186;->d()I

    move-result v4

    .line 2360792
    invoke-virtual {v3, v4}, LX/186;->d(I)V

    .line 2360793
    invoke-virtual {v3}, LX/186;->e()[B

    move-result-object v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 2360794
    const/4 v3, 0x0

    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 2360795
    new-instance v3, LX/15i;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v3 .. v8}, LX/15i;-><init>(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;ZLX/15j;)V

    .line 2360796
    new-instance v4, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;

    invoke-direct {v4, v3}, Lcom/facebook/commerce/productdetails/graphql/FetchProductGroupQueryModels$FetchProductGroupQueryModel;-><init>(LX/15i;)V

    .line 2360797
    return-object v4

    .line 2360798
    :catchall_0
    move-exception v3

    :try_start_5
    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v3

    .line 2360799
    :catchall_1
    move-exception v3

    :try_start_6
    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    throw v3

    .line 2360800
    :catchall_2
    move-exception v3

    :try_start_7
    monitor-exit v9
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v3

    .line 2360801
    :catchall_3
    move-exception v3

    :try_start_8
    monitor-exit v16
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    throw v3

    .line 2360802
    :catchall_4
    move-exception v3

    :try_start_9
    monitor-exit v17
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    throw v3
.end method
