.class public LX/FNW;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Ljavax/inject/Singleton;
.end annotation


# static fields
.field private static volatile e:LX/FNW;


# instance fields
.field public a:LX/0Or;
    .annotation runtime Lcom/facebook/auth/annotations/LoggedInUser;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Or",
            "<",
            "Lcom/facebook/user/model/User;",
            ">;"
        }
    .end annotation
.end field

.field public b:LX/0tX;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public c:Ljava/util/concurrent/ExecutorService;
    .annotation runtime Lcom/facebook/common/executors/BackgroundExecutorService;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation
.end field

.field public d:LX/0Ot;
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .annotation build Lcom/facebook/ultralight/Lazy;
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "LX/0Ot",
            "<",
            "LX/6jL;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1
    .annotation build Lcom/facebook/ultralight/Inject;
    .end annotation

    .prologue
    .line 2232661
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2232662
    sget-object v0, LX/0Op;->b:LX/0Ot;

    move-object v0, v0

    .line 2232663
    iput-object v0, p0, LX/FNW;->d:LX/0Ot;

    .line 2232664
    return-void
.end method

.method public static a(LX/0QB;)LX/FNW;
    .locals 7

    .prologue
    .line 2232665
    sget-object v0, LX/FNW;->e:LX/FNW;

    if-nez v0, :cond_1

    .line 2232666
    const-class v1, LX/FNW;

    monitor-enter v1

    .line 2232667
    :try_start_0
    sget-object v0, LX/FNW;->e:LX/FNW;

    invoke-static {v0, p0}, LX/0SC;->a(Ljava/lang/Object;LX/0QB;)LX/0SC;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2232668
    if-eqz v2, :cond_0

    .line 2232669
    :try_start_1
    iget-object v0, v2, LX/0SC;->a:LX/0QB;

    .line 2232670
    new-instance v5, LX/FNW;

    invoke-direct {v5}, LX/FNW;-><init>()V

    .line 2232671
    const/16 v3, 0x12cb

    invoke-static {v0, v3}, LX/0T5;->a(LX/0QB;I)LX/0Or;

    move-result-object v6

    invoke-static {v0}, LX/0tX;->a(LX/0QB;)LX/0tX;

    move-result-object v3

    check-cast v3, LX/0tX;

    invoke-static {v0}, LX/0Vo;->a(LX/0QB;)LX/0TD;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/ExecutorService;

    const/16 p0, 0x2987

    invoke-static {v0, p0}, LX/0SL;->b(LX/0QB;I)LX/0Ot;

    move-result-object p0

    .line 2232672
    iput-object v6, v5, LX/FNW;->a:LX/0Or;

    iput-object v3, v5, LX/FNW;->b:LX/0tX;

    iput-object v4, v5, LX/FNW;->c:Ljava/util/concurrent/ExecutorService;

    iput-object p0, v5, LX/FNW;->d:LX/0Ot;

    .line 2232673
    move-object v0, v5

    .line 2232674
    sput-object v0, LX/FNW;->e:LX/FNW;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2232675
    :try_start_2
    invoke-virtual {v2}, LX/0SC;->a()V

    .line 2232676
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2232677
    :cond_1
    sget-object v0, LX/FNW;->e:LX/FNW;

    return-object v0

    .line 2232678
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v2}, LX/0SC;->a()V

    throw v0

    .line 2232679
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
