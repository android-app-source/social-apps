.class public LX/Gf7;
.super Ljava/lang/Object;
.source ""


# annotations
.annotation runtime Lcom/facebook/components/annotations/LayoutSpec;
.end annotation

.annotation runtime Lcom/facebook/inject/ContextScoped;
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E::",
        "LX/1Pk;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static b:LX/0Xm;


# instance fields
.field public final a:LX/Gdi;


# direct methods
.method public constructor <init>(LX/Gdi;)V
    .locals 0
    .annotation runtime Ljavax/inject/Inject;
    .end annotation

    .prologue
    .line 2376854
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2376855
    iput-object p1, p0, LX/Gf7;->a:LX/Gdi;

    .line 2376856
    return-void
.end method

.method public static a(LX/0QB;)LX/Gf7;
    .locals 4

    .prologue
    .line 2376857
    const-class v1, LX/Gf7;

    monitor-enter v1

    .line 2376858
    :try_start_0
    sget-object v0, LX/Gf7;->b:LX/0Xm;

    invoke-static {v0}, LX/0Xm;->a(LX/0Xm;)LX/0Xm;

    move-result-object v2

    .line 2376859
    sput-object v2, LX/Gf7;->b:LX/0Xm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 2376860
    :try_start_1
    invoke-virtual {v2, p0}, LX/0Xm;->a(LX/0QB;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2376861
    invoke-virtual {v2}, LX/0Xm;->a()LX/0R6;

    move-result-object v0

    .line 2376862
    new-instance p0, LX/Gf7;

    invoke-static {v0}, LX/Gdi;->b(LX/0QB;)LX/Gdi;

    move-result-object v3

    check-cast v3, LX/Gdi;

    invoke-direct {p0, v3}, LX/Gf7;-><init>(LX/Gdi;)V

    .line 2376863
    move-object v0, p0

    .line 2376864
    iput-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    .line 2376865
    :cond_0
    iget-object v0, v2, LX/0Xm;->a:Ljava/lang/Object;

    check-cast v0, LX/Gf7;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2376866
    :try_start_2
    invoke-virtual {v2}, LX/0Xm;->b()V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, LX/0Xm;->b()V

    throw v0

    .line 2376867
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method
